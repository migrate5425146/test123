﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Peercore.CRM.Shared
{
    public sealed class ApplicationService
    {
        private static ApplicationService instance;
        public static ApplicationService Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ApplicationService();
                }

                return instance;
            }

        }

        private ApplicationService()
        {
            PopulateConfigSettings();
        }

        private void PopulateConfigSettings()
        {
            this.ConnectionString = ConfigurationManager.ConnectionStrings["PeercoreCRM"].ConnectionString;

            string[] strConnection = this.ConnectionString.Split(new char[] { ';' });

            bool hasMaxPool = false;
            foreach (var str in strConnection)
            {
                if (str.ToLower().Contains("max pool size"))
                {
                    hasMaxPool = true;
                }
            }

            if (hasMaxPool == false)
            {
                this.ConnectionString += "Max Pool Size=1000;";
            }

            this.DbProvider = ConfigurationManager.ConnectionStrings["PeercoreCRM"].ProviderName;
        }

        public string ConnectionString { get; set; }

        public string DbProvider { get; set; }

    }
}
