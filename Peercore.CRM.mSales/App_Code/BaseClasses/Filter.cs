﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

/// <summary>
/// Represents a filter expression of Kendo DataSource.
/// </summary>
[DataContract]
public class Filter
{
    /// <summary>
    /// Gets or sets the name of the sorted field (property). Set to <c>null</c> if the <c>Filters</c> property is set.
    /// </summary>
    [DataMember(Name="field")]
    public string Field { get; set; }

    /// <summary>
    /// Gets or sets the filtering operator. Set to <c>null</c> if the <c>Filters</c> property is set.
    /// </summary>
    [DataMember(Name="operator")]
    public string Operator { get; set; }

    /// <summary>
    /// Gets or sets the filtering value. Set to <c>null</c> if the <c>Filters</c> property is set.
    /// </summary>
    [DataMember(Name="value")]
    public object Value { get; set; }

    /// <summary>
    /// Gets or sets the filtering logic. Can be set to "or" or "and". Set to <c>null</c> unless <c>Filters</c> is set.
    /// </summary>
    [DataMember(Name="logic")]
    public string Logic { get; set; }

    /// <summary>
    /// Gets or sets the child filter expressions. Set to <c>null</c> if there are no child expressions.
    /// </summary>
    [DataMember(Name="filters")]
    public IEnumerable<Filter> Filters { get; set; }

    /// <summary>
    /// Mapping of Kendo DataSource filtering operators to Dynamic Linq
    /// </summary>
    private static readonly IDictionary<string, string> operators = new Dictionary<string, string>
    {
        {"eq", "="},
        {"neq", "!="},
        {"lt", "<"},
        {"lte", "<="},
        {"gt", ">"},
        {"gte", ">="},
        {"startswith", "StartsWith"},
        {"endswith", "EndsWith"},
        {"contains", "Contains"}
    };

    /// <summary>
    /// Get a flattened list of all child filter expressions.
    /// </summary>
    public IList<Filter> All()
    {
        var filters = new List<Filter>();

        Collect(filters);

        return filters;
    }

    private void Collect(IList<Filter> filters)
    {
        if (Filters != null && Filters.Any())
        {
            foreach (Filter filter in Filters)
            {
                filters.Add(filter);

                filter.Collect(filters);
            }
        }
        else
        {
            filters.Add(this);
        }
    }

    /// <summary>
    /// Converts the filter expression to a predicate suitable for Dynamic Linq e.g. "Field1 = @1 and Field2.Contains(@2)"
    /// </summary>
    /// <param name="filters">A list of flattened filters.</param>
    public string ToExpression(IList<Filter> filters)
    {
        if (Filters != null && Filters.Any())
        {
            return "(" + String.Join(" " + Logic + " ", Filters.Select(filter => filter.ToExpression(filters)).ToArray()) + ")";
        }

        int index = filters.IndexOf(this);

        //string comparison = operators[Operator];

        //if (comparison == "StartsWith" || comparison == "EndsWith" || comparison == "Contains")
        //{
        //    return String.Format("{0}.{1}(@{2})", Field, comparison, index);
        //}
        return GetFilterMarks();
        //return String.Format("{0} {1} @{2}", Field, comparison, index);
    }
    private string GetFilterMarks()
    {
        StringBuilder sb = new StringBuilder();

        if (Operator == "contains")
        {
            sb.Append(Field + " like lower('%" + Value + "%') ");
        }
        else if (Operator == "doesnotcontain")
        {
            sb.Append(Field +" not like lower('%" + Value + "%') ");
        }
        else if (Operator == "startswith")
        {
            sb.Append(Field + " like " + "lower('" + Value + "%')");
        }
        else if (Operator == "endswith")
        {
            sb.Append(Field + " like lower('%" + Value + "')");
        }
        else if (Operator == "eq")
        {
            sb.Append(Field + " = " + "lower(('" + Value + "'))");
        }
        else if (Operator == "neq")
        {
            sb.Append(Field + " <> " + "lower(('" + Value + "'))");
        }

        else if (Operator == "lt")
        {
            sb.Append(Field + " < " +  Value );
        }
        else if (Operator == "lte")
        {
            sb.Append(Field + " <= " +  Value );
        }

        else if (Operator == "gt")
        {
            sb.Append(Field + " > " +  Value );
        }
        else if (Operator == "gte")
        {
            sb.Append(Field + " >= " +  Value );
        }


        //else if (filterfuntion == GridKnownFunction.IsEmpty)
        //{
        //    sb.Append(" in " + "lower(('" + fieldValue + "'))");
        //}
        //else if (filterfuntion == GridKnownFunction.NotIsEmpty)
        //{
        //    sb.Append(" not in " + "lower(('" + fieldValue + "'))");
        //}
        //else if (filterfuntion == GridKnownFunction.GreaterThan)
        //{
        //    sb.Append(" > " + "'" + fieldValue + "'");
        //}
        //else if (filterfuntion == GridKnownFunction.GreaterThanOrEqualTo)
        //{
        //    sb.Append(" >= " + "'" + fieldValue + "'");
        //}
        //else if (filterfuntion == GridKnownFunction.LessThan)
        //{
        //    sb.Append(" < " + "'" + fieldValue + "'");
        //}
        //else if (filterfuntion == GridKnownFunction.LessThanOrEqualTo)
        //{
        //    sb.Append(" <= " + "'" + fieldValue + "'");
        //}

        //if (dataTypeName == "System.String")
        //{
        //    if (filterfuntion == GridKnownFunction.Contains)
        //    {
        //        sb.Append(" like lower('%" + fieldValue + "%') ");
        //    }
        //    else if (filterfuntion == GridKnownFunction.DoesNotContain)
        //    {
        //        sb.Append(" not like lower('%" + fieldValue + "%') ");
        //    }
        //    else if (filterfuntion == GridKnownFunction.StartsWith)
        //    {
        //        sb.Append(" like " + "lower('" + fieldValue + "%')");
        //    }
        //    else if (filterfuntion == GridKnownFunction.EndsWith)
        //    {
        //        sb.Append(" like lower('%" + fieldValue + "')");
        //    }
        //    else if (filterfuntion == GridKnownFunction.EqualTo)
        //    {
        //        sb.Append(" in " + "lower(('" + fieldValue + "'))");
        //    }
        //    else if (filterfuntion == GridKnownFunction.NotEqualTo)
        //    {
        //        sb.Append(" not in " + "lower(('" + fieldValue + "'))");
        //    }
        //    else if (filterfuntion == GridKnownFunction.IsEmpty)
        //    {
        //        sb.Append(" in " + "lower(('" + fieldValue + "'))");
        //    }
        //    else if (filterfuntion == GridKnownFunction.NotIsEmpty)
        //    {
        //        sb.Append(" not in " + "lower(('" + fieldValue + "'))");
        //    }
        //    else if (filterfuntion == GridKnownFunction.GreaterThan)
        //    {
        //        sb.Append(" > " + "'" + fieldValue + "'");
        //    }
        //    else if (filterfuntion == GridKnownFunction.GreaterThanOrEqualTo)
        //    {
        //        sb.Append(" >= " + "'" + fieldValue + "'");
        //    }
        //    else if (filterfuntion == GridKnownFunction.LessThan)
        //    {
        //        sb.Append(" < " + "'" + fieldValue + "'");
        //    }
        //    else if (filterfuntion == GridKnownFunction.LessThanOrEqualTo)
        //    {
        //        sb.Append(" <= " + "'" + fieldValue + "'");
        //    }
        //}
        //else if (dataTypeName == "System.Boolean")
        //{
        //    if (filterfuntion == GridKnownFunction.EqualTo)
        //    {
        //        sb.Append(" in " + "(" + fieldValue + ")");
        //    }
        //    else if (filterfuntion == GridKnownFunction.NotEqualTo)
        //    {
        //        sb.Append(" not in " + "(" + fieldValue + ")");
        //    }
        //}
        //else if (dataTypeName == "System.DateTime")
        //{
        //    //string date = fieldValue.ToString("dd-MMM-yyyy HH:mm:ss");

        //    CultureInfo culture = new CultureInfo("en-AU", false); // use your culture info

        //    //Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-AU");

        //    DateTime datetime = DateTime.ParseExact(fieldValue, culture.DateTimeFormat.ShortDatePattern, CultureInfo.InvariantCulture);

        //    //DateTime datetime = DateTime.ParseExact(fieldValue.Trim(), "dd-MMM-yyyy HH:mm:ss", CultureInfo.InvariantCulture);



        //    if (filterfuntion == GridKnownFunction.EqualTo)
        //    {
        //        sb.Append(" = '" + datetime.ToString() + "'");
        //    }
        //    else if (filterfuntion == GridKnownFunction.NotEqualTo)
        //    {
        //        sb.Append(" != '" + datetime.ToString("dd/MM/yyyy HH:mm:ss tt") + "'");
        //    }
        //    else if (filterfuntion == GridKnownFunction.GreaterThan)
        //    {
        //        sb.Append(" > '" + datetime.ToString("dd/MM/yyyy HH:mm:ss tt") + "'");
        //    }
        //    else if (filterfuntion == GridKnownFunction.GreaterThanOrEqualTo)
        //    {
        //        sb.Append(" >= '" + datetime.ToString("dd/MM/yyyy HH:mm:ss tt") + "'");
        //    }
        //    else if (filterfuntion == GridKnownFunction.LessThan)
        //    {
        //        sb.Append(" < '" + datetime.ToString("dd/MM/yyyy HH:mm:ss tt") + "'");
        //    }
        //    else if (filterfuntion == GridKnownFunction.LessThanOrEqualTo)
        //    {
        //        sb.Append(" <= '" + datetime.ToString("dd/MM/yyyy HH:mm:ss tt") + "'");
        //    }
        //}
        //else if (dataTypeName == "System.Int32" || dataTypeName == "System.Int64" || dataTypeName == "System.Double")
        //{
        //    if (filterfuntion == GridKnownFunction.EqualTo)
        //    {
        //        sb.Append(" in " + "(" + fieldValue + ")");
        //    }
        //    else if (filterfuntion == GridKnownFunction.NotEqualTo)
        //    {
        //        sb.Append(" not in " + "(" + fieldValue + ")");
        //    }
        //    else if (filterfuntion == GridKnownFunction.GreaterThan)
        //    {
        //        sb.Append(" > " + fieldValue);
        //    }
        //    else if (filterfuntion == GridKnownFunction.GreaterThanOrEqualTo)
        //    {
        //        sb.Append(" >= " + fieldValue);
        //    }
        //    else if (filterfuntion == GridKnownFunction.LessThan)
        //    {
        //        sb.Append(" < " + fieldValue);
        //    }
        //    else if (filterfuntion == GridKnownFunction.LessThanOrEqualTo)
        //    {
        //        sb.Append(" <= " + fieldValue);
        //    }
        //}

        //if (filterfuntion == GridKnownFunction.IsNull)
        //{
        //    // sb.Append(" = ISNULL");
        //    // return value.Equals(null);
        //}
        //else if (filterfuntion == GridKnownFunction.NotIsNull)
        //{
        //    // return !value.Equals(null);
        //}

        return sb.ToString();
    }
}