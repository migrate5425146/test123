﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ServiceModel;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using CRMServiceReference;

/// <summary>
/// Summary description for PageBase
/// </summary>
public class PageBase : System.Web.UI.Page
{
    public struct  ScheduleStruct
    {
        //public List<CallCycleContactDTO> DayList_1;
        //public List<CallCycleContactDTO> DayList_2;
        //public List<CallCycleContactDTO> DayList_3;
        //public List<CallCycleContactDTO> DayList_4;
        //public List<CallCycleContactDTO> DayList_5;
        public List<DayCallCycleDTO> DayCallCycleList;

        public List<RouteDTO> DRRoutes;
        public int CallCycleId;
        public string RepCode;
        public string OriginatorType;

        public bool HasChanged;
    }

    #region Properties
    /// <summary>
    /// Gets or Sets the CurrentUserSettings to the Session
    /// </summary>
    public ArgsDTO CurrentUserSettings
    {
        get
        {
            if (Session[CommonUtility.GLOBAL_SETTING] != null)
                return ((ArgsDTO)(Session[CommonUtility.GLOBAL_SETTING]));

            return null;
        }
        set
        {
            if (value != null)
                Session[CommonUtility.GLOBAL_SETTING] = value;
            else
                Session.Remove(CommonUtility.GLOBAL_SETTING);
        }
    }

    /// <summary>
    /// Gets or Sets the CurrentUserSettings to the Session
    /// </summary>
    public List<string> BackButtonForAll
    {
        get
        {
            if (Session[CommonUtility.BACK_BUTTON_FOR_ALL] != null)
                return ((List<string>)(Session[CommonUtility.BACK_BUTTON_FOR_ALL]));

            return null;
        }
        set
        {
            if (value != null)
                Session[CommonUtility.BACK_BUTTON_FOR_ALL] = value;
            else
                Session.Remove(CommonUtility.BACK_BUTTON_FOR_ALL);
        }
    }
    #endregion

    #region Events
    public void Page_Error(object sender, EventArgs e)
    {
        //Use this event only details pages, for entry pages display error page itself.

        string errorpage = Request.Url.Segments.Last();
        string pagecollection = ConfigUtil.ErrorRedirectionPages;

        if (pagecollection.Split(',').Any(s => s.Contains(errorpage.ToLower())))
        {
            Exception objErr = Server.GetLastError().GetBaseException();
            objErr.Source = Request.Url.ToString().Trim();
            Session[CommonUtility.ERROR_LOG] = objErr;
            Server.ClearError();

            Response.Redirect(ConfigUtil.ApplicationPath+"networkstatus.aspx");
        }
    }
    #endregion

    #region Methods
    public PageBase()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    private void DeleteTempDocument()
    {
        try
        {
            System.IO.DirectoryInfo di = new System.IO.DirectoryInfo(Request.PhysicalApplicationPath + "\\docs\\");
            if (!di.Exists) return;

            string pattern = "tmp_" + Session.SessionID.Trim();
            System.IO.FileInfo[] rgFiles = di.GetFiles(pattern + "*.*");
            foreach (System.IO.FileInfo file in rgFiles)
            {
                try
                {
                    System.IO.File.Delete(file.FullName);
                }
                catch
                {
                }
            }
        }
        catch { }
    }

    protected override void InitializeCulture()
    {
       if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            ///<remarks><REMARKS>
            ///Check if PostBack occured. Cannot use IsPostBack in this method
            ///as this property is not set yet.
            ///</remarks>
            ///

            DeleteTempDocument();
            base.InitializeCulture();
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }
    #endregion

    
}

public class DayCallCycleDTO
{
    public List<CallCycleContactDTO> DayList { get; set; }
    public int WeekDayId { get; set; }
}