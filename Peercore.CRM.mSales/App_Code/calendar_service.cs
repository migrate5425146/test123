﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using Telerik.Web.UI;
using CRMServiceReference;
using Peercore.CRM.Shared;
using System.Drawing;
using Peercore.CRM.Common;
using System.ServiceModel;
using System.Web.Script.Serialization;

[WebService]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class calendar_service : System.Web.Services.WebService {
    private WebServiceAppointmentController _controller;

    [WebMethod(EnableSession = true)]
    public IEnumerable<Telerik.Web.UI.AppointmentData> GetAppointments(SchedulerInfo schedulerInfo)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        ArgsDTO args = new ArgsDTO();
        ActivityClient activityClient = new ActivityClient();
        AppointmentClient appointmentClient = new AppointmentClient(); 
        List<Telerik.Web.UI.AppointmentData> appointmentDataList = null;
        if (Session != null && Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
            args.Originator = string.IsNullOrEmpty(UserSession.Instance.FilterByUserName) ? UserSession.Instance.UserName : UserSession.Instance.FilterByUserName;
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.SToday = string.IsNullOrEmpty(args.SToday) ? "" : args.SToday;
            args.SStartDate = schedulerInfo.ViewStart.ToString("yyyy/MM/dd"); // Added a day to get the data for day view
            args.SEndDate = schedulerInfo.ViewEnd.ToString("yyyy/MM/dd"); // Added a day to get the data for day view
            args.SSource = "";
            args.Floor = string.IsNullOrEmpty(args.Floor) ? "" : args.Floor;
            args.Status = string.IsNullOrEmpty(args.Status) ? "" : args.Status;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.SMonth = 1;
            appointmentDataList = LoadData(appointmentClient.GetAllAppointmentsByType(args));
            //appointmentDataList = LoadActivitiesToShedular(activityClient.GetAllActivities(args, args.ActivityStatus));

        }
        return appointmentDataList;

    }

    private List<Telerik.Web.UI.AppointmentData> LoadData(List<AppointmentViewDTO> appList)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        List<Telerik.Web.UI.AppointmentData> lstAppoinment = new List<Telerik.Web.UI.AppointmentData>();

        foreach (AppointmentViewDTO item in appList)
        {
            //if (cmbRepTerritory.SelectedValue != null)
            //{
            //    if (!cmbRepTerritory.SelectedValue.ToString().Equals("- All -"))
            //    {
            //        if (!dbAppointment["state"].ToString().Trim().Equals(cmbRepTerritory.SelectedValue.ToString()))
            //            continue;
            //    }
            //}

            Telerik.Web.UI.AppointmentData newApp = new Telerik.Web.UI.AppointmentData();
            //ContactAppointment newApp = new ContactAppointment();
            //newApp.ContactName = !string.IsNullOrEmpty(activity.LeadName) ? activity.LeadName + " - " : string.Empty;
            newApp.Start = item.StartTime;
            newApp.End = item.EndTime;
            //newApp.Subject = item.Subject;

            if (!string.IsNullOrEmpty(item.LeadName))
                newApp.Subject = item.LeadName + " - " + item.Subject;
            else
                newApp.Subject = item.Subject;

            //newApp.Body = activity.Comments;


            newApp.Attributes.Add("Location", item.CreatedBy);
            newApp.Attributes.Add("Url", item.LeadName + " - ");
            newApp.Attributes.Add("Body", item.Body);
            //newApp.Attributes.Add("ActivityId", item.ActivityId.ToString());

            newApp.ID = item.AppointmentID.ToString();
            newApp.Attributes.Add("Category", item.Category);
            newApp.Description = item.Body;
            newApp.ID = item.AppointmentID.ToString();

            newApp.Resources.Add(new ResourceData() { Type = "ActivityTypes", Text = item.Category, Key = item.Category });
            newApp.Resources.Add(new ResourceData() { Type = "ActivityId", Text = item.ActivityId.ToString(), Key = item.ActivityId.ToString() });
            newApp.Resources.Add(new ResourceData() { Type = "LeadId", Text = item.LeadId.ToString(), Key = item.LeadId.ToString() });
            newApp.Resources.Add(new ResourceData() { Type = "CustCode", Text = item.CustCode, Key = item.CustCode });
            

            //DataRow[] drChild = dsApp.Tables["Table1"].Select("appointment_id=" + dbAppointment["appointment_id"].ToString());

            //foreach (DataRow drRes in drChild)
            //{
            //    DataRow drB = drRes.GetParentRow("relation");

            //    Telerik.Windows.Controls.Resource resource = new Telerik.Windows.Controls.Resource();
            //    resource.DisplayName = drB["name"].ToString();
            //    resource.ResourceName = drB["originator_id"].ToString();
            //    resource.ResourceType = "Reps";
            //    newApp.Resources.Add(resource);
            //}
            lstAppoinment.Add(newApp);
        }
        return lstAppoinment;
    }


    [WebMethod(EnableSession = true)]
    public IEnumerable<AppointmentData> InsertAppointment(SchedulerInfo schedulerInfo, AppointmentData appointmentData)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        AppointmentAdded(appointmentData);
        return GetAppointments(schedulerInfo);
    }
    private void AppointmentAdded(AppointmentData app)
    {

        bool iNoRecs = false;
        int iRow = 0;

        AppointmentClient oAppointment = new AppointmentClient();
        AppointmentDTO appointment = new AppointmentDTO();

        //****** Setting Location - Created Originator
        //app.Location = GlobalValues.GetInstance().UserName;

        appointment.Subject = app.Subject;
        appointment.Body = app.Description;
        appointment.StartTime = app.Start;
        appointment.EndTime = app.End;

        appointment.Category = app.Resources[0].Key.ToString();


        appointment.CreatedBy = UserSession.Instance.UserName;

        // Resources
        //AppointmentResource[] oAppResource = new AppointmentResource[app.Resources.Count];

        try
        {
            //int id=0;
            //iNoRecs = oAppointment.AppointmentInsert(out id, appointment, false, appointment.ActivityId);

            //if (iNoRecs)
            //{
            //    //app.UniqueId = appointment.AppointmentID.ToString();
            //    //app.Category = scheduler.Categories.GetCategoryByName(appointment.Category);

            //    //foreach (Resource resource in app.Resources)
            //    //{
            //    //    oAppResource[iRow] = new AppointmentResource();
            //    //    oAppResource[iRow].AppointmentID = Convert.ToInt32(app.UniqueId);
            //    //    oAppResource[iRow].ResourceID = Convert.ToInt32(resource.ResourceName);
            //    //    iRow++;
            //    //}

            //    // Insert Resources
            //    //iNoRecs = oAppointment.InsertResources(oAppResource);

            //    //GlobalValues.GetInstance().ShowMessage("Successfully Saved.", GlobalValues.MessageImageType.Information);
            //    //MessageBox.Show("Successfully Saved.");
            //}
        }
        catch (Exception oException)
        {
        }
    }

    [WebMethod(EnableSession = true)]
    public IEnumerable<AppointmentData> UpdateAppointment(SchedulerInfo schedulerInfo, AppointmentData appointmentData)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        ChangeAppointment(appointmentData);
        return GetAppointments(schedulerInfo);
        //return Controller.UpdateAppointment(schedulerInfo, appointmentData);
    }

    private void ChangeAppointment(AppointmentData editedAppt)
    {
        bool iNoRecs = false;
        int iRow = 0;

        AppointmentClient oAppointment = new AppointmentClient();
        AppointmentDTO appointment = new AppointmentDTO();


        appointment.Subject = editedAppt.Subject;

        string[] aSubject = editedAppt.Subject.Split('-');
        if (aSubject.Length == 2)
        {
            appointment.Subject = aSubject[1];
        }
        else if (aSubject.Length == 1)
        {
            appointment.Subject = aSubject[0];
        }
        else
        {
            appointment.Subject = "";
        }

        appointment.Body = editedAppt.Description;
        appointment.StartTime = editedAppt.Start;
        appointment.EndTime = editedAppt.End;
        foreach (Telerik.Web.UI.ResourceData item in editedAppt.Resources)
        {
            if (item.Type == "ActivityTypes")
            {
                appointment.Category = item.Key.ToString();
                //break;
            }
            else if (item.Type == "ActivityId")
            {
                appointment.ActivityId = string.IsNullOrEmpty(item.Key.ToString()) ? 0 : Int32.Parse(item.Key.ToString());
            }
        }


       // appointment.Category = editedAppt.Resources[3].Key.ToString();
        appointment.AppointmentID = Convert.ToInt32(editedAppt.ID);


        ActivityClient oActivity = new ActivityClient();
        ActivityDTO activity = new ActivityDTO();
        activity.ActivityID = appointment.ActivityId;
        activity.Subject = appointment.Subject;
        activity.Comments = editedAppt.Description;
        activity.StartDate = editedAppt.Start;
        activity.EndDate = editedAppt.End;
        activity.AppointmentID = appointment.AppointmentID;
        activity.ActivityType = appointment.Category;
        activity.LastModifiedBy = UserSession.Instance.UserName;
        activity.LastModifiedDate = DateTime.Now;

        // Resources
        ////AppointmentResource[] oAppResource = new AppointmentResource[editedAppt.Resources.Count];
        ////foreach (Resource resource in editedAppt.Resources)
        ////{
        ////    oAppResource[iRow] = new AppointmentResource();
        ////    oAppResource[iRow].AppointmentID = Convert.ToInt32(editedAppt.UniqueId);
        ////    oAppResource[iRow].ResourceID = Convert.ToInt32(resource.ResourceName);
        ////    iRow++;
        ////}

        try
        {
            iNoRecs = oAppointment.AppointmentUpdate(appointment,false,false);

            if (iNoRecs)
            {
                oActivity.ChangeActivityDate(activity, false);
                oActivity.ChangeActivityDate(activity, true);

                // Delete Old Reps
                //oAppointment.DeleteResources(appointment);

                // Insert new Reps
                //oAppointment.InsertResources(oAppResource);
            }

            //GlobalValues.GetInstance().ShowMessage("Successfully Saved.", GlobalValues.MessageImageType.Information);
            //MessageBox.Show("Successfully Saved.");
        }
        catch (Exception oException)
        {
        }
    }

    [WebMethod]
    public IEnumerable<AppointmentData> CreateRecurrenceException(SchedulerInfo schedulerInfo, AppointmentData recurrenceExceptionData)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        return GetAppointments(schedulerInfo);
    }

    [WebMethod]
    public IEnumerable<AppointmentData> RemoveRecurrenceExceptions(SchedulerInfo schedulerInfo, AppointmentData masterAppointmentData)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        return GetAppointments(schedulerInfo);
    }

    [WebMethod(EnableSession = true)]
    public IEnumerable<AppointmentData> DeleteAppointment(SchedulerInfo schedulerInfo, AppointmentData appointmentData, bool deleteSeries)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        //CustomerActivitiesDTO customerActivities = new CustomerActivitiesDTO();
        //ActivityDTO activity = new ActivityDTO();
        //activity.ActivityID = int.Parse(appointmentData.ID.ToString());
        //ActivityClient oActivity = new ActivityClient();
        //AppointmentClient oAppointment = new AppointmentClient();
        //customerActivities = oActivity.GetActivityAppoitmentId(activity.ActivityID);

        //bool iNoofRecs = oActivity.DeleteActivity(activity);

        //if (iNoofRecs)
        //{
        //    if (customerActivities.AppointmentId > 0 && customerActivities.DelFlag != "Y")
        //    {
        //        activity.AppointmentID = customerActivities.AppointmentId;

        //        //if (clsGlobal.GetInstance().ConfirmMessageBox("Do you want to delete the related appointment for this activity?") == true)
        //        //{
        //            iNoofRecs = oActivity.ResetAppointment(activity);
        //            if (iNoofRecs)
        //            {
        //                AppointmentDTO crmAppointment = new AppointmentDTO();
        //                crmAppointment.AppointmentID = customerActivities.AppointmentId;
        //                iNoofRecs = oAppointment.AppointmentUpdate(crmAppointment, false, true);
        //                if (iNoofRecs)
        //                    ;//GlobalValues.GetInstance().ShowMessage("Successfully deleted.", GlobalValues.MessageImageType.Information);
        //                else
        //                    ;// GlobalValues.GetInstance().ShowMessage("Delete failed.", GlobalValues.MessageImageType.Error);
        //            }
        //            else
        //                ;// GlobalValues.GetInstance().ShowMessage("Delete failed.", GlobalValues.MessageImageType.Error);

        //        //}
        //    }
        //    else
        //        ;// GlobalValues.GetInstance().ShowMessage("Successfully deleted.", GlobalValues.MessageImageType.Information);
        //}

        return GetAppointments(schedulerInfo);
    }



    [WebMethod(EnableSession = true)]
    public IEnumerable<ResourceData> GetResources(SchedulerInfo schedulerInfo)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        ArgsDTO args1 = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
        List<ResourceData> resourceDataList = new List<ResourceData>();
        ActivityClient activityClient = new ActivityClient();
        //List<ActivityDTO> activity = activityClient.GetAllActivityTypes(UserSession.Instance.DefaultDepartmentId, true);
        List<ActivityDTO> activity = activityClient.GetAllActivityTypes("PS", true);

        foreach (ActivityDTO item in activity)
        {
            //if (item.ShowInPlanner == "Y")
            //{
                ResourceData resourceData = new ResourceData();
                resourceData.Text = item.Description;
                resourceData.Key = item.Code;
                resourceData.Available = true;
                resourceData.Type = "ActivityTypes";

                //Dictionary<string, System.Drawing.Color> dictionary = new Dictionary<string, System.Drawing.Color>();
                //dictionary.Add("BackColor", ColorTranslator.FromHtml(item.ActivityTypeColour));

                resourceDataList.Add(resourceData);
            //}
        }

        //OriginatorClient originatorClient = new OriginatorClient();
        //ArgsDTO args = new ArgsDTO();
        //args.Originator = "cpereira";
        //List<OriginatorDTO> originatorList = originatorClient.GetChildOriginatorsList(args);

        //foreach (OriginatorDTO item in originatorList)
        //{
        //    //if (item.ShowInPlanner == "Y")
        //    //{
        //    ResourceData resourceData = new ResourceData();
        //    resourceData.Text = item.Name;
        //    resourceData.Key = item.UserName;
        //    resourceData.Available = true;
        //    resourceData.Type = "Rep";

        //    //Dictionary<string, System.Drawing.Color> dictionary = new Dictionary<string, System.Drawing.Color>();
        //    //dictionary.Add("BackColor", ColorTranslator.FromHtml(item.ActivityTypeColour));

        //    resourceDataList.Add(resourceData);
        //    //}
        //}

        return resourceDataList;
        //return Controller.GetResources(schedulerInfo);
    }





    [WebMethod]
    public List<CalendarHolidayDTO> GetHolidays(DateTime startdate, DateTime enddate, string username)
    {


        ArgsDTO args = new ArgsDTO();
        CommonClient commonClient = new CommonClient();
        List<CalendarHolidayDTO> holidayDataList = new List<CalendarHolidayDTO>();
        //if (Session != null && Session[CommonUtility.GLOBAL_SETTING] != null)
        //{
        //  args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
        args.SToday = string.IsNullOrEmpty(args.SToday) ? "" : args.SToday;
        args.SStartDate = "01/01/1900";//startdate.ToString("yyyy/MM/dd");
        args.SEndDate = "12/31/2099";//enddate.ToString("yyyy/MM/dd");;
        args.Originator = username;

        holidayDataList = commonClient.GetAllHolidays(args);
        //      }
        CalendarHolidayDTO testdto = null;
        List<CalendarHolidayDTO> testdtolist = new List<CalendarHolidayDTO>();

        foreach (CalendarHolidayDTO holiday in holidayDataList)
        {
            testdto = new CalendarHolidayDTO();
            testdto.CreatedBy = holiday.CreatedBy;
            testdto.Date = holiday.Date;
            testdto.Description = holiday.Description;
            testdto.End = holiday.End;
            testdto.EndDate = holiday.EndDate;
            testdto.EndTimezone = holiday.EndTimezone;
            testdto.HolidayId = holiday.HolidayId;
            testdto.HolidayType = holiday.HolidayType;
            testdto.IsAllDay = holiday.IsAllDay;
            testdto.OwnerID = holiday.OwnerID;
            testdto.RecurrenceException = holiday.RecurrenceException;
            testdto.RecurrenceID = holiday.RecurrenceID;
            // testdto.Start = holiday.Start;
            testdto.StartTimezone = holiday.StartTimezone;
            testdto.TaskID = holiday.TaskID;
            testdto.Title = holiday.Title;
            testdto.TypeColor = holiday.TypeColor;
            testdto.TypeDescription = holiday.TypeDescription;
            //testdto.LastChangedDate = holiday.LastChangedDate;

            testdtolist.Add(testdto);

        }

        return testdtolist;
        //  return holidayDataList;
    }

    [WebMethod(EnableSession = true)]
    public void update(List<CalendarHolidayDTO> models, string username, int test)
    {
        CommonClient commonClient = new CommonClient();
        models[0].CreatedBy = username;
        int b = test;

        bool a = commonClient.UpdateHoliday(models[0]);

        if (Session != null)
        {
            if (Session["InsertLogAndSendMail"] != null)
            {
                if ((Session["InsertLogAndSendMail"]).Equals("1"))
                {
                    //Get Session Values
                    bool isHolidayDateChanged = false;
                    int holidayId = models[0].HolidayId;
                    int actId = 0;
                    DateTime fromDate = DateTime.Today;
                    DateTime toDate = DateTime.Today;

                    if (Session["holidayChanged"] != null && !(Session["holidayChanged"]).Equals(""))
                        isHolidayDateChanged = Session["holidayChanged"].Equals("1") ? true : false;
                    if (Session["ActId"] != null && !(Session["ActId"]).Equals(""))
                        actId = Convert.ToInt32(Session["ActId"]);
                    if (Session["fromDate"] != null && !(Session["fromDate"]).Equals(""))
                        fromDate = Convert.ToDateTime(Session["fromDate"]);
                    if (Session["toDate"] != null && !(Session["toDate"]).Equals(""))
                        toDate = Convert.ToDateTime(Session["toDate"]);

                    bool c = InsertToLogAndSendMail(isHolidayDateChanged, fromDate, toDate, holidayId, actId);
                }
            }
        }
    }

    [WebMethod(EnableSession = true)]
    public void create(List<CalendarHolidayDTO> models, string username)
    {
        CommonClient commonClient = new CommonClient();
        models[0].CreatedBy = username;

        if (Session != null)
        {
            if (Session["InsertLogAndSendMail"] != null)
            {
                if ((Session["InsertLogAndSendMail"]).Equals("1"))
                {
                    //Insert holiday and get inserted ID
                    int holidayId = commonClient.InsertHolidayReturnId(models[0]);

                    //Get Session Values
                    bool isHolidayDateChanged = false;
                    int actId = 0;
                    DateTime fromDate = DateTime.Today;
                    DateTime toDate = DateTime.Today;

                    if (Session["holidayChanged"] != null && !(Session["holidayChanged"]).Equals(""))
                        isHolidayDateChanged = Session["holidayChanged"].Equals("1") ? true : false;
                    if (Session["ActId"] != null && !(Session["ActId"]).Equals(""))
                        actId = Convert.ToInt32(Session["ActId"]);
                    if (Session["fromDate"] != null && !(Session["fromDate"]).Equals(""))
                        fromDate = Convert.ToDateTime(Session["fromDate"]);
                    if (Session["toDate"] != null && !(Session["toDate"]).Equals(""))
                        toDate = Convert.ToDateTime(Session["toDate"]);

                    bool a = InsertToLogAndSendMail(isHolidayDateChanged, fromDate, toDate, holidayId, actId);
                }
                else
                {
                    bool a = commonClient.InsertHoliday(models[0]);
                }
            }
            else
            {
                bool a = commonClient.InsertHoliday(models[0]);
            }
        }
        else
        {
            bool a = commonClient.InsertHoliday(models[0]);
        }

        JavaScriptSerializer serializer = new JavaScriptSerializer();
    }


    [WebMethod]
    public void destroy(List<CalendarHolidayDTO> models)
    {
        CommonClient commonClient = new CommonClient();
        bool a = commonClient.DeleteHoliday(models[0].HolidayId);
    }

    [WebMethod]
    public List<CalendarHolidayDTO> GetHolidayTypes()
    {

        ArgsDTO args = new ArgsDTO();
        CommonClient commonClient = new CommonClient();
        List<CalendarHolidayDTO> holidayDataList = null;


        holidayDataList = commonClient.GetAllHolidaytypes();
        // }
        return holidayDataList;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string IsActivityExistsForTheSavedHoliday(string holidayDate)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        ActivityClient activityClient = new ActivityClient();
        CallCycleClient callCycleClient = new CallCycleClient();

        ArgsDTO args = new ArgsDTO();
        args.Originator = string.IsNullOrEmpty(UserSession.Instance.FilterByUserName) ? UserSession.Instance.UserName : UserSession.Instance.FilterByUserName;
        args.ChildOriginators = UserSession.Instance.ChildOriginators;
        args.SToday = string.IsNullOrEmpty(args.SToday) ? "" : args.SToday;
        args.SStartDate = Convert.ToDateTime(holidayDate).ToString("yyyy/MM/dd");
        args.SEndDate = Convert.ToDateTime(holidayDate).ToString("yyyy/MM/dd");
        args.SSource = "Planner";
        args.Floor = string.IsNullOrEmpty(args.Floor) ? "" : args.Floor;
        args.Status = string.IsNullOrEmpty(args.Status) ? "" : args.Status;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        List<CustomerActivitiesDTO> activities = new List<CustomerActivitiesDTO>();

        string returnvalue = "false";
        bool isActivityExists = false;
        bool isHeaderApproved = false;
        ItineraryHeaderDTO itineraryHeaderDTO = new ItineraryHeaderDTO();
        try
        {
            //get Itinerary Header by User
            DateTime effDate = Convert.ToDateTime(holidayDate);
            itineraryHeaderDTO = callCycleClient.GetItineraryHeaderByUser(UserSession.Instance.UserName, effDate);
            if (!String.IsNullOrEmpty(itineraryHeaderDTO.Status))
            {
                if (itineraryHeaderDTO.Status.Trim().Equals("APRV"))
                    isHeaderApproved = true;
            }

            if (isHeaderApproved)
            {
                //get existing activity for the saved holiday (Only for a Approved Itinerary)
                activities = activityClient.GetAllActivities(args, args.ActivityStatus);
                if (activities.Count > 0)
                    isActivityExists = true;
            }

            Session["InsertLogAndSendMail"] = "0";
            if (isActivityExists && isHeaderApproved)
            {
                returnvalue = "true";
                Session["InsertLogAndSendMail"] = "1";
            }


        }
        catch (Exception)
        {
            returnvalue = "error";
        }
        return returnvalue;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string DeactivateActivityForTheSavedHoliday(string holidayDate, string holidayChangedFromDate, string holidayChangedToDate, string holidayId)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        ActivityClient activityClient = new ActivityClient();
        CommonClient commonClient = new CommonClient();

        int hId = Convert.ToInt32(holidayId);
        //insert hId to Session
        //Session["holidayId"] = hId;

        DateTime holiday = new DateTime();
        holiday = Convert.ToDateTime(holidayDate);

        bool isSuccess = false;
        bool mailSent = false;
        bool isHolidayDateChanged = false;

        DateTime fromDate = holiday;
        DateTime toDate = holiday;

        if (!String.IsNullOrEmpty(holidayChangedFromDate) && !String.IsNullOrEmpty(holidayChangedToDate))
        {
            fromDate = Convert.ToDateTime(holidayChangedFromDate);
            toDate = Convert.ToDateTime(holidayChangedToDate);

            if (!fromDate.Equals(toDate))
            {
                isHolidayDateChanged = true;
            }
        }

        //insert isHolidayDateChanged to Session
        if (isHolidayDateChanged)
            Session["holidayChanged"] = "1";
        else
            Session["holidayChanged"] = "0";

        Session["fromDate"] = fromDate.ToShortDateString();
        Session["toDate"] = toDate.ToShortDateString();

        string returnvalue = "false";
        int actId = 0;
        try
        {
            List<ActivityDTO> activityList = new List<ActivityDTO>();
            ActivityDTO activityDTO = null;
            activityDTO = new ActivityDTO();
            activityDTO.Status = "Y";//to indicate as Deactivated
            activityDTO.StartDate = new DateTime(holiday.Date.Year, holiday.Date.Month, holiday.Date.Day);
            activityDTO.EndDate = new DateTime(holiday.Date.Year, holiday.Date.Month, holiday.Date.Day);
            activityDTO.LastModifiedBy = UserSession.Instance.UserName;

            activityList.Add(activityDTO);
            //call UpdateActivityStatusForTME in activityService  
            actId = activityClient.UpdateActivityStatusForTME(activityList);
            //Insert actId to Session
            Session["ActId"] = actId;
            if (actId > 0)
                isSuccess = true;

            //mailSent = InsertToLogAndSendMail(isHolidayDateChanged, fromDate, toDate, hId, actId);

            if (isSuccess)
                returnvalue = "true";
            //else if (isSuccess)
            //    returnvalue = "mailNotSent";
        }
        catch (Exception)
        {
            returnvalue = "error";
        }
        return returnvalue;
    }

    //private bool SendMailToRM(bool isHolidayDateChanged, DateTime fromDate, DateTime toDate, int logId, int acLogId)
    //{
    //    CallCycleClient callCycleClient = new CallCycleClient();
    //    bool status = false;
    //    CommonUtility commmonUtil = new CommonUtility();
    //    CommonClient commonClient = new CRMServiceReference.CommonClient();

    //    //Setting To List
    //    List<string> tolist = new List<string>();
    //    OriginatorClient originatorService = new OriginatorClient();
    //    List<OriginatorDTO> rmList = new List<OriginatorDTO>();
    //    string originator = UserSession.Instance.UserName;
    //    rmList = originatorService.GetLevelThreeParentOriginators(originator);
    //    foreach (OriginatorDTO rm in rmList)
    //    {
    //        tolist.Add(rm.Email);
    //    }

    //    List<string> cclist = new List<string>();

    //    ////generate an access token for the RM (mail reciever) 
    //    //string newAccessToken = commmonUtil.GetNewAccessToken(rmList[0].UserName);
    //    ////update access token for the RM
    //    //bool isUpdateTokenSuccess = false;
    //    //OriginatorDTO originatorDTO = new OriginatorDTO();
    //    //originatorDTO.UserName = rmList[0].UserName;
    //    //originatorDTO.AccessToken = newAccessToken;
    //    //isUpdateTokenSuccess = originatorService.UpdateOriginatorAccessToken(originatorDTO);

    //    //generate an access token for the RM (mail reciever) 
    //    string newAccessToken = commmonUtil.GetNewAccessToken(rmList[0].UserName);

    //    //Get AccessToken For the User
    //    OriginatorDTO oriRM = new OriginatorDTO();
    //    oriRM = originatorService.GetOriginator(rmList[0].UserName);
    //    bool isUpdateTokenSuccess = false;
    //    if (String.IsNullOrEmpty(oriRM.AccessToken))
    //    {
    //        //update access token for the RM
    //        OriginatorDTO originatorDTO = new OriginatorDTO();
    //        originatorDTO.UserName = rmList[0].UserName;
    //        originatorDTO.AccessToken = newAccessToken;
    //        isUpdateTokenSuccess = originatorService.UpdateOriginatorAccessToken(originatorDTO);
    //    }
    //    else
    //    {
    //        newAccessToken = oriRM.AccessToken;
    //    }

    //    string encodedUsername = rmList[0].UserName;
    //    encodedUsername = Server.UrlEncode(commmonUtil.GetEncodedString(encodedUsername));

    //    string MessageBody = "";
    //    StringBuilder str = new StringBuilder();

    //    string ApproveLink = ConfigUtil.PublicIp + "activity_planner/transaction/planner_status_update.aspx?logid=" + logId + "&acid=" + acLogId + "&status=a&ori=" + encodedUsername + "&key=" + newAccessToken;
    //    string RejectLink = ConfigUtil.PublicIp + "activity_planner/transaction/planner_status_update.aspx?logid=" + logId + "&acid=" + acLogId + "&status=r&ori=" + encodedUsername + "&key=" + newAccessToken;

    //    str.Append("TME     :   " + UserSession.Instance.Originator);
    //    str.Append("</br>");
    //    str.Append("Month   :   " + fromDate.Date.ToString("MMMM yyyy"));
    //    str.Append("</br>");
    //    str.Append("</br>");
    //    string subject = "";
    //    if (isHolidayDateChanged)
    //    {
    //        str.Append("A Holiday has been changed from " + fromDate.ToString("dd MMM yyyy") + " to " + toDate.ToString("dd MMM yyyy") + ".");
    //        subject = "Holiday Changed by " + UserSession.Instance.Originator;
    //    }
    //    else
    //    {
    //        str.Append("A New Holiday has been Added to " + fromDate.ToString("dd MMM yyyy") + ".");
    //        subject = "New Holiday Added by " + UserSession.Instance.Originator;
    //    }
    //    str.Append("</br>");
    //    str.Append("</br>");
    //    str.Append(" Click the following link to Approve the Change.");
    //    str.Append("</br>");
    //    str.Append(ApproveLink);
    //    str.Append("</br>");
    //    str.Append("Click the following link to Reject the Change.");
    //    str.Append("</br>");
    //    str.Append(RejectLink);
    //    str.Append("</br>");
    //    str.Append("</br>");
    //    str.Append("</br>");
    //    str.Append("<div style=\"Color:#b7b7b7\">* This is a system generated mail, Please do not reply.</div>");

    //    MessageBody = str.ToString();

    //    List<string> attachementPathList = new List<string>();
    //    //ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "$('#div_loader').show();", true);
    //    status = commmonUtil.SendEmailOut(tolist, cclist, subject, MessageBody, attachementPathList);
    //    //ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "$('#div_loader').hide();", true);
    //    return status;
    //}

    private bool InsertToLogAndSendMail(bool isHolidayDateChanged, DateTime fromDate, DateTime toDate, int holidayId, int actId)
    {
        CommonClient commonClient = new CRMServiceReference.CommonClient();

        //Insert to Planner/Holiday Change Log
        HolidayPlannerChangeLogDTO holidayPlannerChangeLogDTO = new HolidayPlannerChangeLogDTO();
        holidayPlannerChangeLogDTO.KeyId = holidayId;
        holidayPlannerChangeLogDTO.FormType = "HLDE";
        if (isHolidayDateChanged)
        {
            holidayPlannerChangeLogDTO.ChangedFromDate = fromDate;
            holidayPlannerChangeLogDTO.ChangedToDate = toDate;
        }
        else
        {
            holidayPlannerChangeLogDTO.ChangedFromDate = fromDate;
            holidayPlannerChangeLogDTO.ChangedToDate = fromDate;
        }
        holidayPlannerChangeLogDTO.Status = "PEND";
        holidayPlannerChangeLogDTO.CreatedBy = UserSession.Instance.UserName;

        //Get inserted Log ID
        int logId = commonClient.InsertHolidayPlannerChangeLog(holidayPlannerChangeLogDTO);

        //Insert Deactivating Activity
        //Insert to Planner/Holiday Change Log
        HolidayPlannerChangeLogDTO holidayPlannerChangeLogDTOActivity = new HolidayPlannerChangeLogDTO();
        holidayPlannerChangeLogDTOActivity.KeyId = actId;
        holidayPlannerChangeLogDTOActivity.FormType = "RTPL";
        if (isHolidayDateChanged)
        {
            holidayPlannerChangeLogDTOActivity.ChangedFromDate = toDate;
            holidayPlannerChangeLogDTOActivity.ChangedToDate = toDate;
        }
        else
        {
            holidayPlannerChangeLogDTOActivity.ChangedFromDate = fromDate;
            holidayPlannerChangeLogDTOActivity.ChangedToDate = fromDate;
        }
        holidayPlannerChangeLogDTOActivity.Status = "PEND";
        holidayPlannerChangeLogDTOActivity.CreatedBy = UserSession.Instance.UserName;
        //Get inserted Log ID
        int acLogId = commonClient.InsertHolidayPlannerChangeLog(holidayPlannerChangeLogDTOActivity);

        bool mailSent = false;
        //Send mail to RM notifying the change
        //mailSent = SendMailToRM(isHolidayDateChanged, fromDate, toDate, logId, acLogId);

        return mailSent;
    }

}
