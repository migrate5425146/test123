﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SortString
/// </summary>
public class SortString
{
    //public SortString()
    //{
    //    //
    //    // TODO: Add constructor logic here
    //    //
    //}

    public static string GetAllRSMSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " rsm_id asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "RsmID":
                sortStr = " rsm_id " + sortoption;
                break;
            case "RsmCode":
                sortStr = " rsm_code " + sortoption;
                break;
            case "RsmName":
                sortStr = " rsm_name " + sortoption;
                break;
            case "RsmUserName":
                sortStr = " rsm_user_name " + sortoption;
                break;
            case "RegionName":
                sortStr = " region_name " + sortoption;
                break;
            case "RsmTel1":
                sortStr = " rsm_tel1 " + sortoption;
                break;
            case "RsmEmail":
                sortStr = " rsm_email " + sortoption;
                break;
            default:
                sortStr = " rsm_id asc ";
                break;
        }

        return sortStr;
    }

    public static string GetAllSRSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " rep_id asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "RepId":
                sortStr = " rep_id " + sortoption;
                break;
            case "RepCode":
                sortStr = " rep_code " + sortoption;
                break;
            case "RepName":
                sortStr = " rep_name " + sortoption;
                break;
            default:
                sortStr = " rep_id asc ";
                break;
        }

        return sortStr;
    }

    public static string GetAllAreaSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " area_id asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "AreaId":
                sortStr = " area_id " + sortoption;
                break;
            case "AreaCode":
                sortStr = " area_code " + sortoption;
                break;
            case "AreaName":
                sortStr = " area_name " + sortoption;
                break;
            default:
                sortStr = " area_id asc ";
                break;
        }

        return sortStr;
    }
    
    public static string GetAllDistributerTerritoryAssignSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " territory_code asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "TerritoryCode":
                sortStr = " territory_code " + sortoption;
                break;
            case "TerritoryName":
                sortStr = " territory_name " + sortoption;
                break;
            case "AreaName":
                sortStr = " area_name " + sortoption;
                break;
            case "AsmName":
                sortStr = " asm_name " + sortoption;
                break;
            case "DistributerName":
                sortStr = " db_name " + sortoption;
                break;
            default:
                sortStr = " territory_code asc ";
                break;
        }

        return sortStr;
    } 
    
    public static string GetAllSalesRepWithSalesTypeSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " rep_code asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "TerritoryCode":
                sortStr = " territory_code " + sortoption;
                break;
            case "TerritoryName":
                sortStr = " territory_name " + sortoption;
                break;
            case "AreaName":
                sortStr = " area_name " + sortoption;
                break;
            case "AsmName":
                sortStr = " asm_name " + sortoption;
                break;
            case "DistributerName":
                sortStr = " db_name " + sortoption;
                break;
            default:
                sortStr = " territory_code asc ";
                break;
        }

        return sortStr;
    } 
    
    public static string GetAllSRTerritoryAssignSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " rep_name asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "RepCode":
                sortStr = " rep_code " + sortoption;
                break;
            case "RepName":
                sortStr = " rep_name " + sortoption;
                break;
            case "TerritoryId":
                sortStr = " territory_name " + sortoption;
                break;
            default:
                sortStr = " rep_name asc ";
                break;
        }

        return sortStr;
    }
}