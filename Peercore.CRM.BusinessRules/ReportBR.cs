﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using SpreadsheetLight;
using SpreadsheetLight.Drawing;
using System.Data;
using System.Xml;
using System.Xml.Linq;
using System.Collections;
using System.IO;


namespace Peercore.CRM.BusinessRules
{
    public class ReportBR
    {
        private static ReportBR instance = null;
        private static object syncLock = new Object();
        private ReportDAO reportDataService;

        public static ReportBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new ReportBR();
                }
                return instance;
            }
        }

        //#region - Init Object -

        //public DocumentEntity CreateObject()
        //{
        //    return reportDataService.CreateObject();
        //}

        //public DocumentEntity CreateObject(int entityId)
        //{
        //    return reportDataService.CreateObject(entityId);
        //}

        //#endregion

        #region - Constructor -

        private ReportBR()
        {
            reportDataService = new ReportDAO();
        }

        #endregion


        public bool SendDERSummeryReportMail(string firstDay, int month, int year, string tmeCode, int marketId, string filename, string reportXmlFilePath)
        {
            return this.GenerateDERSummaryReport(firstDay,month, year, tmeCode, marketId, filename, reportXmlFilePath);
        }

      // public DataSet GetDERSummaryHandlersData(ArgsEntity args)
        public List<string> GetDERSummaryHandlersData(ArgsEntity args)
        {
            return reportDataService.GetDERSummaryHandlersData(args);
        }

        public DataSet GetDERSummaryHandlersData1(ArgsEntity args)
        {
            return reportDataService.GetDERSummaryHandlersData1(args);
        }

        public DataSet GetDERSummaryDataTablePart (string query)
        {
            return reportDataService.GetDERSummaryDataTablePart(query);
        }

        public DataSet GetDERSummaryAvailbleData(ArgsEntity args)
        {
            return reportDataService.GetDERSummaryAvailbleData(args);
        }

        public List<string> GetDERSummaryObjectivesData(ArgsEntity args)
        {
            return reportDataService.GetDERSummaryObjectivesData(args);
        }

        public List<string> GetDERSummaryGeneralData(ArgsEntity args)
        {
            return reportDataService.GetDERSummaryGeneralData(args);
        }

        public List<string> GetDERSummaryHeaderInfo(string originator, int marketId)
        {
            return reportDataService.GetDERSummaryHeaderInfo(originator, marketId);
        }


        private bool GenerateDERSummaryReport(string firstDay,int month, int year, string tmeCode, int marketId, string fileName, string reportXmlFilePath)
        {
            bool status = false;
            DERSummaryReportEntity reportEntity = null;

            try
            {
                SLDocument exlDoc = new SLDocument();

                //------------------------------------------ Report Data filling ----------------------
                reportEntity = new DERSummaryReportEntity();
                List<DERSummaryReportSectionEntity> ReportSectionList = new List<DERSummaryReportSectionEntity>();

                //Read from the xml file.
                XDocument xdoc = XDocument.Load(reportXmlFilePath);
                var danhilVariantsList =  xdoc.Descendants("Report").Where(r => r.Attribute("Title").Value.ToLower() == "dersummary").Descendants("Section").Where(t => t.Attribute("Name").Value.ToUpper() == "DHVAR").Descendants("Columns").Descendants("Column").ToList();
                var anyVariantsList = xdoc.Descendants("Report").Where(r => r.Attribute("Title").Value.ToLower() == "dersummary").Descendants("Section").Where(t => t.Attribute("Name").Value.ToUpper() == "ANVAR").Descendants("Columns").Descendants("Column").ToList();

                string[] danhilVariantsArr = new string[danhilVariantsList.Count];
                string[] anyVariantsArr = new string[anyVariantsList.Count];

                for (int i = 0; i < danhilVariantsArr.Length; i++)
                {
                    danhilVariantsArr[i] = danhilVariantsList[i].Value;
                }

                for (int j = 0; j < anyVariantsArr.Length; j++)
                {
                    anyVariantsArr[j] = anyVariantsList[j].Value;
                }

                List<KeyValuePair<string, int>> handlerTotalList = new List<KeyValuePair<string, int>>();
                List<KeyValuePair<string, int>> availabilityTotalList = new List<KeyValuePair<string, int>>();
                List<KeyValuePair<string, int>> generalTotalList = new List<KeyValuePair<string, int>>();
                List<KeyValuePair<string, int>> objectiveTotalList = new List<KeyValuePair<string, int>>();
               
                string danhilVariants = string.Empty;
                for (int i = 0; i < danhilVariantsArr.Length; i++)
			    {
                    danhilVariants = (i == danhilVariantsArr.Length - 1) ? danhilVariants + danhilVariantsArr[i] : danhilVariants + danhilVariantsArr[i] + ",";            
			    }

                string anyVariants = string.Empty;
                for (int x = 0; x < anyVariantsArr.Length; x++)
                {
                    anyVariants = (x == anyVariantsArr.Length - 1) ? anyVariants + anyVariantsArr[x] : anyVariants = anyVariants + anyVariantsArr[x] + ",";
                }

                ArgsEntity args = new ArgsEntity();
                args.SMonth = month;
                args.SYear = year;
                args.MarketId = marketId;
                args.SStartDate = firstDay;
                args.Variants1 = danhilVariants;
                args.Variants2 = anyVariants;

                List<string> dataStringList = this.GetDERSummaryHandlersData(args);
                string dataListString1 = CreateDERSummaryRecordString(dataStringList,0,10);
               // dataListString1 = dataListString1.Replace("'","''");
                string dataListString2 = CreateDERSummaryRecordString(dataStringList,10,10);
              //  dataListString2 = dataListString1.Replace("'", "''");
                string dataListString3 = CreateDERSummaryRecordString(dataStringList,20,(dataStringList.Count-20));
              //  dataListString3 = dataListString1.Replace("'", " '' ");

                DataTable dtDERSummarySet1 = this.GetDERSummaryDataTablePart(dataListString1).Tables[0];
                DataTable dtDERSummarySet2 = this.GetDERSummaryDataTablePart(dataListString2).Tables[0];
                DataTable dtDERSummarySet3 = this.GetDERSummaryDataTablePart(dataListString3).Tables[0];

                dtDERSummarySet1.Merge(dtDERSummarySet2);
                dtDERSummarySet1.Merge(dtDERSummarySet3);

                List<string> columnHeadersList = GetDataTableColumnHeaderList(dtDERSummarySet1);
                //string headrs = "\"DHFF-H\"" +"," + "\"DHFF-A\"" ;
                //DataView view = new System.Data.DataView(dtDERSummarySet1);
                //DataTable selected = view.ToTable("Selected", false, "DHFF-H" , "DHFF-A");

                string[] handlerColumns = GetGroupedColumnHeaders(columnHeadersList, "-H");
                string[] availabilityColumns = GetGroupedColumnHeaders(columnHeadersList, "-A");
                string[] availabilityPercColumns = GetGroupedColumnHeaders(columnHeadersList, "-AP");
                string[] oosPercColumns = GetGroupedColumnHeaders(columnHeadersList, "-OP");

                DataTable handlerTable = new DataView(dtDERSummarySet1).ToTable(false, handlerColumns);
                DataTable availabilityTable = new DataView(dtDERSummarySet1).ToTable(false, availabilityColumns);
                DataTable availPercTable = new DataView(dtDERSummarySet1).ToTable(false, availabilityPercColumns);
                DataTable oosPercTable = new DataView(dtDERSummarySet1).ToTable(false, oosPercColumns);

               // DataTable dtHandlersData = this.GetDERSummaryHandlersData1(args).Tables[0];
               // List<string> finalColumnHeadersList = GetDataTableColumnHeaderList(dtHandlersData);

                //for (int i = 1; i < handlerColumns.Length; i++)
                //{
                //    string str = handlerColumns[i].ToString();
                //    handlerTable.Columns[i].ColumnName = str.Substring(0, str.Length - 2); 
                //}
               
                //foreach (string str in finalColumnHeadersList)
                //{
                //    dtHandlersData.Columns[cc].ColumnName = str.Substring(0, str.Length - 2); 
                //         cc++;
                //}

                setFinalColumnHeaders(handlerColumns, handlerTable, 2);
                setFinalColumnHeaders(availabilityColumns, availabilityTable, 2);
                setFinalColumnHeaders(availabilityPercColumns, availPercTable, 3);
                setFinalColumnHeaders(oosPercColumns, oosPercTable, 3);

                #region Section-1 - (General Section)

                List<string> sqlGeneralList = this.GetDERSummaryGeneralData(args);
                string sqlGeneralListPart1 = CreateDERSummaryRecordString(sqlGeneralList, 0, 10);
                string sqlGeneralListPart2 = CreateDERSummaryRecordString(sqlGeneralList, 10, 10);
                string sqlGeneralListPart3 = CreateDERSummaryRecordString(sqlGeneralList, 20, (sqlGeneralList.Count - 20));

                DataTable dtDERSummaryGeneral1 = this.GetDERSummaryDataTablePart(sqlGeneralListPart1).Tables[0];
                DataTable dtDERSummaryGeneral2 = this.GetDERSummaryDataTablePart(sqlGeneralListPart2).Tables[0];
                DataTable dtDERSummaryGeneral3 = this.GetDERSummaryDataTablePart(sqlGeneralListPart3).Tables[0];

                dtDERSummaryGeneral1.Merge(dtDERSummaryGeneral2);
                dtDERSummaryGeneral1.Merge(dtDERSummaryGeneral3);

                DERSummaryReportSectionEntity section1 = new DERSummaryReportSectionEntity();
                section1.StartHeaderRowIndex = 10;
                section1.StartDataRowIndex = 13;
                section1.StartColumnIndex = 4;
                section1.Total_ColumnCount = 6;
                section1.DHV_ColumnCount = 0;

                List<string> DateRowValues = new List<string>(); List<int> PVRowValues = new List<int>();
                List<int> AVRowValues = new List<int>(); List<int> PRRowValues = new List<int>();
                List<int> ARRowValues = new List<int>(); List<int> DCRowValues = new List<int>();

                for (int i = 0; i < dtDERSummaryGeneral1.Rows.Count; i++)
                {
                    DateTime dt = Convert.ToDateTime(dtDERSummaryGeneral1.Rows[i]["Date"].ToString());
                    string format = "d-MMM";
                    DateRowValues.Add(dt.ToString(format));
                    PVRowValues.Add(-1);
                    AVRowValues.Add(-1); 
                    PRRowValues.Add(-1);
                    ARRowValues.Add(-1);
                    DCRowValues.Add(Convert.ToInt32(dtDERSummaryGeneral1.Rows[i]["DER Completed"].ToString()));
                }

                DateRowValues.Add(string.Empty);
                PVRowValues.Add(-1); AVRowValues.Add(-1); PRRowValues.Add(-1); ARRowValues.Add(-1);
                int DCTotal = DCRowValues.Sum();
                DCRowValues.Add(DCTotal);
                generalTotalList.Add(new KeyValuePair<string, int>("DC", DCTotal));

                List<ExcelColumnEntity> generalReportColumnList = new List<ExcelColumnEntity>();

                generalReportColumnList.Add(FillExcelColumn(exlDoc, "Date", section1.StartHeaderRowIndex, section1.StartDataRowIndex, section1.StartColumnIndex,false,DateRowValues, true, 10, 12, 4, 4));
                generalReportColumnList.Add(FillExcelColumn(exlDoc, "Planned Visited", section1.StartHeaderRowIndex, section1.StartDataRowIndex, section1.StartColumnIndex + 1, false, PVRowValues, true, 10, 12, 5, 5));
                generalReportColumnList.Add(FillExcelColumn(exlDoc, "Actual Visits", section1.StartHeaderRowIndex, section1.StartDataRowIndex, section1.StartColumnIndex + 2, false, AVRowValues, true, 10, 12, 6, 6));
                generalReportColumnList.Add(FillExcelColumn(exlDoc, "Planned Route", section1.StartHeaderRowIndex, section1.StartDataRowIndex, section1.StartColumnIndex + 3, false, PRRowValues, true, 10, 12, 7, 7));
                generalReportColumnList.Add(FillExcelColumn(exlDoc, "Actual Route", section1.StartHeaderRowIndex, section1.StartDataRowIndex, section1.StartColumnIndex + 4, false, ARRowValues, true, 10, 12, 8, 8));
                generalReportColumnList.Add(FillExcelColumn(exlDoc, "DER Completed", section1.StartHeaderRowIndex, section1.StartDataRowIndex, section1.StartColumnIndex + 5, false, DCRowValues, true, 10, 12, 9, 9));

                section1.ReportColumnList = generalReportColumnList;
                ReportSectionList.Add(section1);

                #endregion


                #region Section-2 - (HANDLER Section)

                DERSummaryReportSectionEntity section2 = new DERSummaryReportSectionEntity();
                section2.StartHeaderRowIndex = 10;
                section2.StartDataRowIndex = 13;
                section2.StartColumnIndex = 10;
                section2.Total_ColumnCount = 11;
                section2.DHV_ColumnCount = 4;

                BaseExcelCellEntity d1 = new BaseExcelCellEntity();
                d1.ExcelDocument = exlDoc;
                d1.RowIndex = 11;
                d1.ColumnIndex = 10;
                d1.CellText = "DUNHILL VARIANTS";
                d1.MergeStartRowIndex = 11;
                d1.MergeEndRowIndex = 11;
                d1.MergeStartColumnIndex = 10;
                d1.MergeEndColumnIndex = 13;
                d1.IsMerged = true;
                this.DrawExcelHeaderCellWithAutoFit(d1);

                BaseExcelCellEntity d2 = new BaseExcelCellEntity();
                d2.ExcelDocument = exlDoc;
                d2.RowIndex = 10;
                d2.ColumnIndex = 10;
                d2.CellText = "HANDLERS (ND)";
                d2.MergeStartRowIndex = 10;
                d2.MergeEndRowIndex = 10;
                d2.MergeStartColumnIndex = 10;
                d2.MergeEndColumnIndex = 20;
                d2.IsMerged = true;
                this.DrawExcelHeaderCellWithAutoFit(d2);

                List<ExcelColumnEntity> handlerReportColumnList = new List<ExcelColumnEntity>();

                for (int c = 0; c < danhilVariantsArr.Length; c++)
                {
                    List<int> rowValues = new List<int>();
                    string colName = danhilVariantsArr[c];

                    for (int j = 0; j < handlerTable.Rows.Count; j++)
                    {
                        rowValues.Add(Convert.ToInt32(handlerTable.Rows[j][colName].ToString())); 
                    }

                    int total = rowValues.Sum();
                    rowValues.Add(total);
                    colName = danhilVariantsArr[c].Substring(2, 2);
                    handlerTotalList.Add(new KeyValuePair<string, int>(colName, total));
                    handlerReportColumnList.Add(FillExcelColumn(exlDoc, colName, section2.StartHeaderRowIndex + 2, section2.StartDataRowIndex, section2.StartColumnIndex + c, false, rowValues, false, 0, 0, 0, 0));
                }


                for (int c = 0; c < anyVariantsArr.Length; c++)
                {
                    List<int> rowValues = new List<int>();
                    string colName = anyVariantsArr[c];

                    for (int j = 0; j < handlerTable.Rows.Count; j++)
                    {
                        rowValues.Add(Convert.ToInt32(handlerTable.Rows[j][colName].ToString()));
                    }

                    int total = rowValues.Sum();
                    rowValues.Add(total);
                    handlerTotalList.Add(new KeyValuePair<string, int>(colName, total));
                    handlerReportColumnList.Add(FillExcelColumn(exlDoc, colName, section2.StartHeaderRowIndex + 1, section2.StartDataRowIndex, (section2.StartColumnIndex + 4 + c), false, rowValues, true, 11, 12, (14 + c), (14 + c)));
                }

                section2.ReportColumnList = handlerReportColumnList;
                ReportSectionList.Add(section2);
                reportEntity.ReportSectionList = ReportSectionList;

               

                #endregion


                #region Section-3 - (AVAILABILITY Section)

                DERSummaryReportSectionEntity section3 = new DERSummaryReportSectionEntity();
                section3.StartHeaderRowIndex = 10;
                section3.StartDataRowIndex = 13;
                section3.StartColumnIndex = 21;
                section3.Total_ColumnCount = 11;
                section3.DHV_ColumnCount = 4;

                BaseExcelCellEntity d3 = new BaseExcelCellEntity();
                d3.ExcelDocument = exlDoc;
                d3.RowIndex = 11;
                d3.ColumnIndex = 21;
                d3.CellText = "DUNHILL VARIANTS";
                d3.MergeStartRowIndex = 11;
                d3.MergeEndRowIndex = 11;
                d3.MergeStartColumnIndex = 21;
                d3.MergeEndColumnIndex = 24;
                d3.IsMerged = true;
                this.DrawExcelHeaderCellWithAutoFit(d3);

                BaseExcelCellEntity d4 = new BaseExcelCellEntity();
                d4.ExcelDocument = exlDoc;
                d4.RowIndex = 10;
                d4.ColumnIndex = 21;
                d4.CellText = "AVAILABILITY";
                d4.MergeStartRowIndex = 10;
                d4.MergeEndRowIndex = 10;
                d4.MergeStartColumnIndex = 21;
                d4.MergeEndColumnIndex = 31;
                d4.IsMerged = true;
                this.DrawExcelHeaderCellWithAutoFit(d4);

                ArgsEntity args2 = new ArgsEntity();
                args2.SMonth = month;
                args2.SYear = year;
                args2.MarketId = marketId;
              //  DataTable dtAvailableData = this.GetDERSummaryAvailbleData(args2).Tables[0];

                List<ExcelColumnEntity> availbleReportColumnList = new List<ExcelColumnEntity>();

                for (int c = 0; c < danhilVariantsArr.Length; c++)
                {
                    List<int> rowValues = new List<int>();
                    string colName = danhilVariantsArr[c];

                    for (int r = 0; r < availabilityTable.Rows.Count; r++)
                    {
                        rowValues.Add(Convert.ToInt32(availabilityTable.Rows[r][colName].ToString()));
                    }

                    int total = rowValues.Sum();
                    rowValues.Add(total);
                    colName = danhilVariantsArr[c].Substring(2, 2);
                    availabilityTotalList.Add(new KeyValuePair<string, int>(colName, total));
                    availbleReportColumnList.Add(FillExcelColumn(exlDoc, colName, section3.StartHeaderRowIndex + 2, section3.StartDataRowIndex, section3.StartColumnIndex + c, false, rowValues, false, 0, 0, 0, 0));
                }


                for (int c = 0; c < anyVariantsArr.Length; c++)
                {
                    List<int> rowValues = new List<int>();
                    string colName = anyVariantsArr[c];

                    for (int j = 0; j < availabilityTable.Rows.Count; j++)
                    {
                        rowValues.Add(Convert.ToInt32(availabilityTable.Rows[j][colName].ToString()));
                    }

                    int total = rowValues.Sum();
                    rowValues.Add(total);
                    availabilityTotalList.Add(new KeyValuePair<string, int>(colName, total));
                    availbleReportColumnList.Add(FillExcelColumn(exlDoc, colName, section3.StartHeaderRowIndex + 1, section3.StartDataRowIndex, (section3.StartColumnIndex + 4 + c), false, rowValues, true, 11, 12, (25 + c), (25 + c)));
                }

                section3.ReportColumnList = availbleReportColumnList;
                ReportSectionList.Add(section3);
                reportEntity.ReportSectionList = ReportSectionList;

                #endregion

                int DERValue = generalTotalList.Where(y => y.Key.Trim().Equals("DC")).SingleOrDefault().Value;

                #region Section-4 - (AVAILABILITY Percentage Section)

                DERSummaryReportSectionEntity section4 = new DERSummaryReportSectionEntity();
                section4.StartHeaderRowIndex = 10;
                section4.StartDataRowIndex = 13;
                section4.StartColumnIndex = 32;
                section4.Total_ColumnCount = 11;
                section4.DHV_ColumnCount = 4;

                BaseExcelCellEntity d5 = new BaseExcelCellEntity();
                d5.ExcelDocument = exlDoc;
                d5.RowIndex = 11;
                d5.ColumnIndex = 32;
                d5.CellText = "DUNHILL VARIANTS";
                d5.MergeStartRowIndex = 11;
                d5.MergeEndRowIndex = 11;
                d5.MergeStartColumnIndex = 32;
                d5.MergeEndColumnIndex = 35;
                d5.IsMerged = true;
                this.DrawExcelHeaderCellWithAutoFit(d5);

                BaseExcelCellEntity d6 = new BaseExcelCellEntity();
                d6.ExcelDocument = exlDoc;
                d6.RowIndex = 10;
                d6.ColumnIndex = 32;
                d6.CellText = "AVAILABILITY (%)";
                d6.MergeStartRowIndex = 10;
                d6.MergeEndRowIndex = 10;
                d6.MergeStartColumnIndex = 32;
                d6.MergeEndColumnIndex = 42;
                d6.IsMerged = true;
                this.DrawExcelHeaderCellWithAutoFit(d6);

                ArgsEntity args4 = new ArgsEntity();
                args4.SMonth = month;
                args4.SYear = year;
                args4.MarketId = marketId;
             //   DataTable dtAvailabilityPercentageData = this.GetDERSummaryAvailbleData(args4).Tables[0];

                List<ExcelColumnEntity> availabilityPercentageReportColumnList = new List<ExcelColumnEntity>();

                for (int c = 0; c < danhilVariantsArr.Length; c++)
                {
                    List<int> rowValues = new List<int>();
                    string colName = danhilVariantsArr[c].Trim();

                    for (int r = 0; r < availPercTable.Rows.Count; r++)
                    {
                        rowValues.Add(Convert.ToInt32(availPercTable.Rows[r][colName].ToString()));
                    }

                   colName = danhilVariantsArr[c].Substring(2, 2);
                   int AvlValue =  availabilityTotalList.Where(x => x.Key.Trim().Equals(colName)).SingleOrDefault().Value;

                   double AvlPercentage = 0;
                   if(DERValue > 0)
                        AvlPercentage = (AvlValue * 100) / DERValue;

                   rowValues.Add(Convert.ToInt32(Math.Round(AvlPercentage)));
                   availabilityPercentageReportColumnList.Add(FillExcelColumn(exlDoc, colName, section4.StartHeaderRowIndex + 2, section4.StartDataRowIndex, section4.StartColumnIndex + c, true, rowValues, false, 0, 0, 0, 0));
                }


                for (int c = 0; c < anyVariantsArr.Length; c++)
                {
                    List<int> rowValues = new List<int>();
                    string colName = anyVariantsArr[c].Trim();

                    for (int r = 0; r < availPercTable.Rows.Count; r++)
                    {
                        rowValues.Add(Convert.ToInt32(availPercTable.Rows[r][colName].ToString()));
                    }

                    int AvlValue = availabilityTotalList.Where(x => x.Key.Trim().Equals(colName)).SingleOrDefault().Value;

                    double AvlPercentage = 0;
                    if (DERValue > 0)
                        AvlPercentage = (AvlValue * 100) / DERValue;

                    rowValues.Add(Convert.ToInt32(Math.Round(AvlPercentage)));
                    availabilityPercentageReportColumnList.Add(FillExcelColumn(exlDoc, colName, section4.StartHeaderRowIndex + 1, section4.StartDataRowIndex, (section4.StartColumnIndex + 4 + c), true, rowValues, true, 11, 12, (36 + c), (36 + c)));
                }

                section4.ReportColumnList = availabilityPercentageReportColumnList;
                ReportSectionList.Add(section4);
                reportEntity.ReportSectionList = ReportSectionList;
               

                #endregion


                #region Section-5 - (OOS Out OF Stock Section)

                DERSummaryReportSectionEntity section5 = new DERSummaryReportSectionEntity();
                section5.StartHeaderRowIndex = 10;
                section5.StartDataRowIndex = 13;
                section5.StartColumnIndex = 43;
                section5.Total_ColumnCount = 11;
                section5.DHV_ColumnCount = 4;

                BaseExcelCellEntity d7 = new BaseExcelCellEntity();
                d7.ExcelDocument = exlDoc;
                d7.RowIndex = 11;
                d7.ColumnIndex = 43;
                d7.CellText = "DUNHILL VARIANTS";
                d7.MergeStartRowIndex = 11;
                d7.MergeEndRowIndex = 11;
                d7.MergeStartColumnIndex = 43;
                d7.MergeEndColumnIndex = 46;
                d7.IsMerged = true;
                this.DrawExcelHeaderCellWithAutoFit(d7);

                BaseExcelCellEntity d8 = new BaseExcelCellEntity();
                d8.ExcelDocument = exlDoc;
                d8.RowIndex = 10;
                d8.ColumnIndex = 43;
                d8.CellText = "OOS (%)";
                d8.MergeStartRowIndex = 10;
                d8.MergeEndRowIndex = 10;
                d8.MergeStartColumnIndex = 43;
                d8.MergeEndColumnIndex = 53;
                d8.IsMerged = true;
                this.DrawExcelHeaderCellWithAutoFit(d8);

                ArgsEntity args5 = new ArgsEntity();
                args5.SMonth = month;
                args5.SYear = year;
                args5.MarketId = marketId;
             //   DataTable dtOOSPercentageData = this.GetDERSummaryAvailbleData(args5).Tables[0];

                List<ExcelColumnEntity> OOSPercentageReportColumnList = new List<ExcelColumnEntity>();
            
                for (int c = 0; c < danhilVariantsArr.Length; c++)
                {
                    List<int> rowValues = new List<int>();
                    string colName = danhilVariantsArr[c].Trim();

                    for (int r = 0; r < oosPercTable.Rows.Count; r++)
                    {
                        rowValues.Add(Convert.ToInt32(oosPercTable.Rows[r][colName].ToString()));
                    }

                    colName = danhilVariantsArr[c].Substring(2, 2);
                    int handlerValue = handlerTotalList.Where(x => x.Key.Trim().Equals(colName)).SingleOrDefault().Value;
                    int avlValue = availabilityTotalList.Where(x => x.Key.Trim().Equals(colName)).SingleOrDefault().Value;

                    double OOSPercentage = 0;
                    if (handlerValue == 0)
                        OOSPercentage = 0;
                    else
                        OOSPercentage = (handlerValue - avlValue) * 100 / handlerValue;

                    rowValues.Add(Convert.ToInt32(Math.Round(OOSPercentage)));
                    OOSPercentageReportColumnList.Add(FillExcelColumn(exlDoc, colName, section5.StartHeaderRowIndex + 2, section5.StartDataRowIndex, section5.StartColumnIndex + c, true, rowValues, false, 0, 0, 0, 0));
                }


                for (int c = 0; c < anyVariantsArr.Length; c++)
                {
                    List<int> rowValues = new List<int>();
                    string colName = anyVariantsArr[c].Trim();

                    for (int r = 0; r < oosPercTable.Rows.Count; r++)
                    {
                        rowValues.Add(Convert.ToInt32(oosPercTable.Rows[r][colName].ToString()));
                    }

                    int handlerValue = handlerTotalList.Where(x => x.Key.Trim().Equals(colName)).SingleOrDefault().Value;
                    int avlValue = availabilityTotalList.Where(x => x.Key.Trim().Equals(colName)).SingleOrDefault().Value;

                    double OOSPercentage = 0;
                    if (handlerValue == 0)
                        OOSPercentage = 0;
                    else
                        OOSPercentage = (handlerValue - avlValue) * 100 / handlerValue;

                    rowValues.Add(Convert.ToInt32(Math.Round(OOSPercentage)));
                    OOSPercentageReportColumnList.Add(FillExcelColumn(exlDoc, colName, section5.StartHeaderRowIndex + 1, section5.StartDataRowIndex, (section5.StartColumnIndex + 4 + c), true, rowValues, true, 11, 12, (47 + c), (47 + c)));
                }

                section5.ReportColumnList = OOSPercentageReportColumnList;
                ReportSectionList.Add(section5);
                reportEntity.ReportSectionList = ReportSectionList;
              

                #endregion


                #region Summary Section1

                DERSummaryReportSectionEntity summarySection1 = new DERSummaryReportSectionEntity();
                summarySection1.StartHeaderRowIndex = 3;
                summarySection1.StartDataRowIndex = 6;
                summarySection1.StartColumnIndex = 10;
                summarySection1.Total_ColumnCount = 11;
                summarySection1.DHV_ColumnCount = 4;

                BaseExcelCellEntity d9 = new BaseExcelCellEntity();
                d9.ExcelDocument = exlDoc;
                d9.RowIndex = 4;
                d9.ColumnIndex = 10;
                d9.CellText = "DUNHILL VARIANTS";
                d9.MergeStartRowIndex = 4;
                d9.MergeEndRowIndex = 4;
                d9.MergeStartColumnIndex = 10;
                d9.MergeEndColumnIndex = 13;
                d9.IsMerged = true;
                this.DrawExcelHeaderCellWithAutoFit(d9);

                BaseExcelCellEntity d10 = new BaseExcelCellEntity();
                d10.ExcelDocument = exlDoc;
                d10.RowIndex = 3;
                d10.ColumnIndex = 10;
                d10.CellText = "HANDLERS (ND) %";
                d10.MergeStartRowIndex = 3;
                d10.MergeEndRowIndex = 3;
                d10.MergeStartColumnIndex = 10;
                d10.MergeEndColumnIndex = 20;
                d10.IsMerged = true;
                this.DrawExcelHeaderCellWithAutoFit(d10);

                List<ExcelColumnEntity> summaryReportColumnList1 = new List<ExcelColumnEntity>();
                List<string> ttValues = new List<string>();
                ttValues.Add(DERValue.ToString());
                ttValues.Add(string.Empty);

                summaryReportColumnList1.Add(FillExcelColumn(exlDoc, "TT OUTLETS", summarySection1.StartHeaderRowIndex, summarySection1.StartDataRowIndex, summarySection1.StartColumnIndex - 1, false, ttValues, true, summarySection1.StartHeaderRowIndex, summarySection1.StartHeaderRowIndex + 2, summarySection1.StartColumnIndex - 1, summarySection1.StartColumnIndex - 1));
                //summaryReportColumnList.Add(FillExcelColumn(exlDoc, colName, summarySection1.StartHeaderRowIndex + 1, summarySection1.StartDataRowIndex, (summarySection1.StartColumnIndex + 4 + c), false, rowValues, true, 4, 5, (14 + c), (14 + c)));

                for (int c = 0; c < danhilVariantsArr.Length; c++)
                {
                    List<string> rowValues = new List<string>();
                    string colName = danhilVariantsArr[c].Substring(2, 2);

                    int handlerValue = handlerTotalList.Where(x => x.Key.Trim().Equals(colName)).SingleOrDefault().Value;
                    rowValues.Add(handlerValue.ToString());

                    int percentValue = 0;
                    if(DERValue > 0)
                      percentValue = (handlerValue * 100) / DERValue;

                    rowValues.Add(percentValue.ToString() + "%");
                    summaryReportColumnList1.Add(FillExcelColumn(exlDoc, colName, summarySection1.StartHeaderRowIndex + 2, summarySection1.StartDataRowIndex, summarySection1.StartColumnIndex + c, false, rowValues, false, 0, 0, 0, 0));
                }


                for (int c = 0; c < anyVariantsArr.Length; c++)
                {
                    List<string> rowValues = new List<string>();
                    string colName = anyVariantsArr[c].Trim();

                    int handlerValue = handlerTotalList.Where(x => x.Key.Trim().Equals(colName)).SingleOrDefault().Value;
                    rowValues.Add(handlerValue.ToString());

                    int percentValue = 0;
                    if (DERValue > 0)
                        percentValue = (handlerValue * 100) / DERValue;

                    rowValues.Add(percentValue.ToString() + "%");
                    summaryReportColumnList1.Add(FillExcelColumn(exlDoc, colName, summarySection1.StartHeaderRowIndex + 1, summarySection1.StartDataRowIndex, (summarySection1.StartColumnIndex + 4 + c), false, rowValues, true, 4, 5, (14 + c), (14 + c)));
                }

                summarySection1.ReportColumnList = summaryReportColumnList1;
                ReportSectionList.Add(summarySection1);
                reportEntity.ReportSectionList = ReportSectionList;


                #endregion


                #region Summary Section2

                DERSummaryReportSectionEntity summarySection2 = new DERSummaryReportSectionEntity();
                summarySection2.StartHeaderRowIndex = 3;
                summarySection2.StartDataRowIndex = 6;
                summarySection2.StartColumnIndex = 21;
                summarySection2.Total_ColumnCount = 11;
                summarySection2.DHV_ColumnCount = 4;

                BaseExcelCellEntity d11 = new BaseExcelCellEntity();
                d11.ExcelDocument = exlDoc;
                d11.RowIndex = 4;
                d11.ColumnIndex = 21;
                d11.CellText = "DUNHILL VARIANTS";
                d11.MergeStartRowIndex = 4;
                d11.MergeEndRowIndex = 4;
                d11.MergeStartColumnIndex = 21;
                d11.MergeEndColumnIndex = 24;
                d11.IsMerged = true;
                this.DrawExcelHeaderCellWithAutoFit(d11);

                BaseExcelCellEntity d12 = new BaseExcelCellEntity();
                d12.ExcelDocument = exlDoc;
                d12.RowIndex = 3;
                d12.ColumnIndex = 21;
                d12.CellText = "OOS %";
                d12.MergeStartRowIndex = 3;
                d12.MergeEndRowIndex = 3;
                d12.MergeStartColumnIndex = 21;
                d12.MergeEndColumnIndex = 31;
                d12.IsMerged = true;
                this.DrawExcelHeaderCellWithAutoFit(d12);

                List<ExcelColumnEntity> summaryReportColumnList2 = new List<ExcelColumnEntity>();

                for (int c = 0; c < danhilVariantsArr.Length; c++)
                {
                    List<string> rowValues = new List<string>();
                    string colName = danhilVariantsArr[c].Substring(2, 2);

                    int handlerValue = handlerTotalList.Where(x => x.Key.Trim().Equals(colName)).SingleOrDefault().Value;
                    int avlValue = availabilityTotalList.Where(x => x.Key.Trim().Equals(colName)).SingleOrDefault().Value;
                    int result1 = handlerValue - avlValue;
                    rowValues.Add(result1.ToString());

                    double OOSPercentage = 0;
                    if (handlerValue == 0)
                        OOSPercentage = 0;
                    else
                        OOSPercentage = (handlerValue - avlValue) * 100 / handlerValue;

                    int result2 = Convert.ToInt32(Math.Round(OOSPercentage));
                    rowValues.Add(result2.ToString() + "%");
                    summaryReportColumnList2.Add(FillExcelColumn(exlDoc, colName, summarySection2.StartHeaderRowIndex + 2, summarySection2.StartDataRowIndex, summarySection2.StartColumnIndex + c, false, rowValues, false, 0, 0, 0, 0));
                }


                for (int c = 0; c < anyVariantsArr.Length; c++)
                {
                    List<string> rowValues = new List<string>();
                    string colName = anyVariantsArr[c].Trim();

                    int handlerValue = handlerTotalList.Where(x => x.Key.Trim().Equals(colName)).SingleOrDefault().Value;
                    int avlValue = availabilityTotalList.Where(x => x.Key.Trim().Equals(colName)).SingleOrDefault().Value;
                    int result1 = handlerValue - avlValue;
                    rowValues.Add(result1.ToString());

                    double OOSPercentage = 0;
                    if (handlerValue == 0)
                        OOSPercentage = 0;
                    else
                        OOSPercentage = (handlerValue - avlValue) * 100 / handlerValue;

                    int result2 = Convert.ToInt32(Math.Round(OOSPercentage));
                    rowValues.Add(result2.ToString() + "%");
                    summaryReportColumnList2.Add(FillExcelColumn(exlDoc, colName, summarySection2.StartHeaderRowIndex + 1, summarySection2.StartDataRowIndex, (summarySection2.StartColumnIndex + 4 + c), false, rowValues, true, 4, 5, (25 + c), (25 + c)));
                }

                summarySection2.ReportColumnList = summaryReportColumnList2;
                ReportSectionList.Add(summarySection2);
                reportEntity.ReportSectionList = ReportSectionList;


                #endregion


                #region Section - 6 - (Objectives Section)

                ArgsEntity paramargs = new ArgsEntity();
                paramargs.OrderBy = " id asc ";
                paramargs.AdditionalParams = string.Empty;
                paramargs.StartIndex = 1;
                paramargs.RowCount = 2000;

                //Get all objectiveCode list
                // string[] objectivesArr = new string[] { "OB1", "OB2", "OB3", "OB4" };
                List<ObjectiveEntity> objectiveAllList = ObjectiveBR.Instance.GetAllObjectives(paramargs);
                List<string> objCodeList = objectiveAllList.Select(b => b.Code).ToList();

                SLStyle obstyle1 = exlDoc.CreateStyle();
                obstyle1.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
                obstyle1.Font.Bold = false;
                obstyle1.Font.Underline = UnderlineValues.None;
                obstyle1.Font.SetFont("Verdana", 8.0);

                for (int rw = 0; rw < objectiveAllList.Count; rw++)
                {
                    int val = rw + 3;
                    exlDoc.MergeWorksheetCells(val, 56, val, 56 + 5);
                    exlDoc.SetCellStyle(val, 56, val, 56 + 5, obstyle1);
                    exlDoc.SetCellValue(val, 56, (rw + 1).ToString() + ") " +  objectiveAllList[rw].Description);
                }

                SLStyle obstyle2 = exlDoc.CreateStyle();
                obstyle2.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
                obstyle2.Font.Bold = true;
                obstyle2.Font.Underline = UnderlineValues.Single;
                obstyle2.Font.SetFont("Verdana", 8.0);

                exlDoc.MergeWorksheetCells(2, 56, 2, 56 + 5);
                exlDoc.SetCellStyle(2, 56, 2, 56 + 5, obstyle2);
                exlDoc.SetCellValue(2, 56, "Cycle Objectives");



                string objCodeStr = string.Empty;
                for (int k = 0; k < objCodeList.Count; k++)
			    {
                    objCodeStr = (k == objCodeList.Count - 1) ? objCodeStr + objCodeList[k] : objCodeStr + objCodeList[k] + ",";            
			    }

                args.Variants1 = objCodeStr;
                List<string> sqlList = this.GetDERSummaryObjectivesData(args);
                string sqlListPart1 = CreateDERSummaryRecordString(sqlList, 0, 10);
                string sqlListPart2 = CreateDERSummaryRecordString(sqlList, 10, 10);
                string sqlListPart3 = CreateDERSummaryRecordString(sqlList, 20, (sqlList.Count - 20));

                DataTable dtDERSummaryObj1 = this.GetDERSummaryDataTablePart(sqlListPart1).Tables[0];
                DataTable dtDERSummaryObj2 = this.GetDERSummaryDataTablePart(sqlListPart2).Tables[0];
                DataTable dtDERSummaryObj3 = this.GetDERSummaryDataTablePart(sqlListPart3).Tables[0];

                dtDERSummaryObj1.Merge(dtDERSummaryObj2);
                dtDERSummaryObj1.Merge(dtDERSummaryObj3);
                
                DERSummaryReportSectionEntity ObjSection = new DERSummaryReportSectionEntity();
                ObjSection.StartHeaderRowIndex = 11;
                ObjSection.StartDataRowIndex = 13;
                ObjSection.StartColumnIndex = 56;
                ObjSection.Total_ColumnCount = 4;
                ObjSection.DHV_ColumnCount = 4;

                BaseExcelCellEntity objHeader = new BaseExcelCellEntity();
                objHeader.ExcelDocument = exlDoc;
                objHeader.RowIndex = 11;
                objHeader.ColumnIndex = 56;
                objHeader.CellText = "Cycle Objectives";
                objHeader.MergeStartRowIndex = 11;
                objHeader.MergeEndRowIndex = 11;
                objHeader.MergeStartColumnIndex = 56;
                objHeader.MergeEndColumnIndex = objHeader.MergeStartColumnIndex + objectiveAllList.Count - 1;    //Objective Count
                objHeader.IsMerged = true;
                this.DrawExcelHeaderCellWithAutoFit(objHeader);

                List<ExcelColumnEntity> objectiveReportColumnList = new List<ExcelColumnEntity>();

                for (int c = 0; c < objectiveAllList.Count; c++)
                {
                    List<int> rowValues = new List<int>();
                    string colName = objectiveAllList[c].Code.Trim();

                    for (int j = 0; j < dtDERSummaryObj1.Rows.Count; j++)
                    {
                        rowValues.Add(Convert.ToInt32(dtDERSummaryObj1.Rows[j][colName].ToString()));
                    }

                    int total = rowValues.Sum();
                    rowValues.Add(total);
                    objectiveTotalList.Add(new KeyValuePair<string, int>(colName, total));
                    objectiveReportColumnList.Add(FillExcelColumn(exlDoc, colName, ObjSection.StartHeaderRowIndex + 1, ObjSection.StartDataRowIndex, ObjSection.StartColumnIndex + c, false, rowValues, false, 0, 0, 0, 0));
                }

                ObjSection.ReportColumnList = objectiveReportColumnList;
                ReportSectionList.Add(ObjSection);
                reportEntity.ReportSectionList = ReportSectionList;


                #endregion


                #region Report info 

                SLStyle hstyle1 = exlDoc.CreateStyle();
                hstyle1.SetHorizontalAlignment(HorizontalAlignmentValues.Center);
                hstyle1.Font.Bold = false;
                hstyle1.Font.Underline = UnderlineValues.Single;
                hstyle1.Font.SetFont("Arial", 14.0);

                exlDoc.MergeWorksheetCells(2, 4, 2, 7);
                exlDoc.SetCellStyle(2, 4, 2, 7, hstyle1);
                exlDoc.SetCellValue(2, 4, "Daily Coverage Monitoring Format");

                List<string> result = this.GetDERSummaryHeaderInfo(tmeCode, marketId);
                DateTime inputDate = new DateTime(year, month, 1);
                string monthStr = inputDate.ToString("MMM-yyyy"); 

                SLStyle hstyle2 = exlDoc.CreateStyle();
                hstyle2.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
                hstyle2.Font.Bold = true;
                hstyle2.Font.Underline = UnderlineValues.None;
                hstyle2.Font.SetFont("Arial", 10.0);

                exlDoc.MergeWorksheetCells(4, 4, 4, 7);
                exlDoc.SetCellStyle(4, 4, 4, 7, hstyle2);
                exlDoc.SetCellValue(4, 4, "Month :      " + monthStr);

                exlDoc.MergeWorksheetCells(6, 4, 6, 7);
                exlDoc.SetCellStyle(6, 4, 6, 7, hstyle2);
                exlDoc.SetCellValue(6, 4, "ASE :        " + result[0]);

                exlDoc.MergeWorksheetCells(8, 4, 8, 7);
                exlDoc.SetCellStyle(8, 4, 8, 7, hstyle2);
                exlDoc.SetCellValue(8, 4, "Market :     " + result[1]);

                #endregion


                //---------------------Draw Excel File --------------------------------------

                foreach (DERSummaryReportSectionEntity sec in reportEntity.ReportSectionList)
                {
                    foreach (ExcelColumnEntity item in sec.ReportColumnList)
                    {
                        //Print the header
                        this.DrawExcelHeaderCellWithAutoFit(item.Header);

                        //Print row values for each column
                        foreach (BaseExcelCellEntity rowValue in item.RowValues)
                        {
                            this.DrawExcelDataCellWithAutoFit(rowValue);
                        }
                        //Print the Footer cell.
                        this.DrawExcelFooterCellWithAutoFit(item.RowValues.LastOrDefault());
                    }
                }

                exlDoc.SaveAs(fileName);
                status = true;
            }
            catch (Exception ex)
            {
                status = false;
            }

            return status;
        }


        #region Excel Creation Methods

        private string CreateDERSummaryRecordString(List<string> dataList,int startIndex,int count) {
            string str="";

            for (int i = startIndex; i < (startIndex+count);i++ )
            {
                if (i == ((startIndex + count )- 1))
                str = str +" " +dataList[i].ToString();
                else
                    str = str + " " + dataList[i].ToString() +" UNION ALL ";
            }

            return str;
        }

        private BaseExcelCellEntity CreateReportCell(SLDocument doc, int rowIndex, int columnIndex, string data,bool isMerged,
                                                     int mergeStartRowIndex,int mergeEndRowIndex,int mergeStartColumnIndex,int mergeEndColumnIndex)
        {
            BaseExcelCellEntity cell = new BaseExcelCellEntity();
            cell.ExcelDocument = doc;
            cell.RowIndex = rowIndex;
            cell.ColumnIndex = columnIndex;
            cell.CellText = data;
            cell.IsMerged = isMerged;
            cell.MergeStartRowIndex = mergeStartRowIndex;
            cell.MergeEndRowIndex = mergeEndRowIndex;
            cell.MergeStartColumnIndex = mergeStartColumnIndex;
            cell.MergeEndColumnIndex = mergeEndColumnIndex;

            return cell;
        }

        private void DrawExcelHeaderCellWithAutoFit(BaseExcelCellEntity headercell)
        {
            if (headercell != null)
            {
                SLDocument doc = headercell.ExcelDocument;

                SLStyle style = doc.CreateStyle();
                style.SetTopBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
                style.SetBottomBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
                style.SetRightBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
                style.SetLeftBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);

                style.SetHorizontalAlignment(HorizontalAlignmentValues.Center);
                style.Font.Bold = true;
                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.Orange, System.Drawing.Color.Orange);
                style.SetWrapText(true);

                if (headercell.IsMerged)
                {
                    doc.MergeWorksheetCells(headercell.MergeStartRowIndex, headercell.MergeStartColumnIndex, headercell.MergeEndRowIndex, headercell.MergeEndColumnIndex);
                    doc.SetCellStyle(headercell.MergeStartRowIndex, headercell.MergeStartColumnIndex, headercell.MergeEndRowIndex, headercell.MergeEndColumnIndex, style);
                }
                else
                {
                    doc.SetCellStyle(headercell.RowIndex, headercell.ColumnIndex, style);
                }

                doc.SetCellValue(headercell.RowIndex, headercell.ColumnIndex, headercell.CellText);
                //doc.AutoFitColumn(headercell.ColumnIndex);
                doc.SetColumnWidth(headercell.ColumnIndex, 10.0);
                
                
            }    
        }

        private void DrawExcelGeneralHeaderCell(BaseExcelCellEntity headercell)
        {
            if (headercell != null)
            {
                SLDocument doc = headercell.ExcelDocument;

                SLStyle style = doc.CreateStyle();
                //style.SetTopBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
                //style.SetBottomBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
                //style.SetRightBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
                //style.SetLeftBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);

                style.SetHorizontalAlignment(HorizontalAlignmentValues.Center);
                style.Font.Bold = true;
                //style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.Orange, System.Drawing.Color.Orange);
                style.SetWrapText(true);

                if (headercell.IsMerged)
                {
                    doc.MergeWorksheetCells(headercell.MergeStartRowIndex, headercell.MergeStartColumnIndex, headercell.MergeEndRowIndex, headercell.MergeEndColumnIndex);
                    doc.SetCellStyle(headercell.MergeStartRowIndex, headercell.MergeStartColumnIndex, headercell.MergeEndRowIndex, headercell.MergeEndColumnIndex, style);
                }
                else
                {
                    doc.SetCellStyle(headercell.RowIndex, headercell.ColumnIndex, style);
                }

                doc.SetCellValue(headercell.RowIndex, headercell.ColumnIndex, headercell.CellText);
                //doc.AutoFitColumn(headercell.ColumnIndex);
                doc.SetColumnWidth(headercell.ColumnIndex, 10.0);


            }
        }

        private void DrawExcelDataCellWithAutoFit(BaseExcelCellEntity datacell)
        {
            if (datacell != null)
            {
                SLDocument doc = datacell.ExcelDocument;

                SLStyle style = doc.CreateStyle();
                style.SetTopBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                style.SetBottomBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                style.SetRightBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                style.SetLeftBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);

                style.SetHorizontalAlignment(HorizontalAlignmentValues.Center);
                style.Font.Bold = false;

                //if (datacell.IsMerged)
                //{
                //    doc.MergeWorksheetCells(datacell.MergeStartRowIndex, datacell.MergeStartColumnIndex, datacell.MergeEndRowIndex, datacell.MergeEndColumnIndex);
                //    doc.SetCellStyle(datacell.MergeStartRowIndex, datacell.MergeStartColumnIndex, datacell.MergeEndRowIndex, datacell.MergeEndColumnIndex, style);
                //}
                //else
                //{
                //    
                //}

                doc.SetCellStyle(datacell.RowIndex, datacell.ColumnIndex, style);
                doc.SetCellValue(datacell.RowIndex, datacell.ColumnIndex, datacell.CellText);
                //doc.AutoFitColumn(datacell.ColumnIndex);
                doc.SetColumnWidth(datacell.ColumnIndex, 10.0);

            }
        }

        private void DrawExcelFooterCellWithAutoFit(BaseExcelCellEntity footercell)
        {
            if (footercell != null)
            {
                SLDocument doc = footercell.ExcelDocument;

                SLStyle style = doc.CreateStyle();
                style.SetTopBorder(BorderStyleValues.Thick, System.Drawing.Color.Black);
                style.SetBottomBorder(BorderStyleValues.Thick, System.Drawing.Color.Black);
                style.SetRightBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);
                style.SetLeftBorder(BorderStyleValues.Thin, System.Drawing.Color.Black);

                style.SetHorizontalAlignment(HorizontalAlignmentValues.Center);
                style.Font.Bold = true;
                style.Fill.SetPattern(PatternValues.Solid, System.Drawing.Color.LightGray, System.Drawing.Color.LightGray);

                //if (datacell.IsMerged)
                //{
                //    doc.MergeWorksheetCells(datacell.MergeStartRowIndex, datacell.MergeStartColumnIndex, datacell.MergeEndRowIndex, datacell.MergeEndColumnIndex);
                //    doc.SetCellStyle(datacell.MergeStartRowIndex, datacell.MergeStartColumnIndex, datacell.MergeEndRowIndex, datacell.MergeEndColumnIndex, style);
                //}
                //else
                //{
                //    
                //}

                doc.SetCellStyle(footercell.RowIndex, footercell.ColumnIndex, style);
                doc.SetCellValue(footercell.RowIndex, footercell.ColumnIndex, footercell.CellText);
                //doc.AutoFitColumn(datacell.ColumnIndex);
                doc.SetColumnWidth(footercell.ColumnIndex, 10.0);

            }
        }


        private ExcelColumnEntity FillExcelColumn(SLDocument exlDoc, string headerText,int headerRowIndex,int dataRowIndex, int columnIndex, string dataCellValue,
                                                             bool isMerged,int mergeStartRowIndex,int mergeEndRowIndex,int mergeStartColumnIndex,int mergeEndColumnIndex)
        {
            ExcelColumnEntity exlColumn = new ExcelColumnEntity();
            exlColumn.Header = CreateReportCell(exlDoc, headerRowIndex, columnIndex, headerText,isMerged,mergeStartRowIndex,mergeEndRowIndex,mergeStartColumnIndex,mergeEndColumnIndex);

            List<BaseExcelCellEntity> rowValueList = new List<BaseExcelCellEntity>();
            for (int i = 1; i < 32; i++)
            {
                rowValueList.Add(CreateReportCell(exlDoc, dataRowIndex, columnIndex, dataCellValue, isMerged, mergeStartRowIndex, mergeEndRowIndex, mergeStartColumnIndex, mergeEndColumnIndex));
                dataRowIndex++;
            }

            exlColumn.RowValues = rowValueList;

            return exlColumn;
        }

        private ExcelColumnEntity FillExcelColumn(SLDocument exlDoc, string headerText, int headerRowIndex, int dataRowIndex, int columnIndex,bool isPercentage, List<int> rowValues,
                                                            bool isMerged, int mergeStartRowIndex, int mergeEndRowIndex, int mergeStartColumnIndex, int mergeEndColumnIndex)
        {
            ExcelColumnEntity exlColumn = new ExcelColumnEntity();
            exlColumn.Header = CreateReportCell(exlDoc, headerRowIndex, columnIndex, headerText,isMerged,mergeStartRowIndex,mergeEndRowIndex,mergeStartColumnIndex,mergeEndColumnIndex);

            List<BaseExcelCellEntity> rowValueList = new List<BaseExcelCellEntity>();

            foreach (int item in rowValues)
            {
                string val = string.Empty;

                if (item == -1)
                { val = string.Empty; }
                else
                { val = (isPercentage == true) ? item.ToString() + "%" : item.ToString(); }
               
                rowValueList.Add(CreateReportCell(exlDoc, dataRowIndex, columnIndex, val, isMerged, mergeStartRowIndex, mergeEndRowIndex, mergeStartColumnIndex, mergeEndColumnIndex));
                dataRowIndex++;
            }

            exlColumn.RowValues = rowValueList;

            return exlColumn;
        }

        private ExcelColumnEntity FillExcelColumn(SLDocument exlDoc, string headerText, int headerRowIndex, int dataRowIndex, int columnIndex, bool isPercentage, List<string> rowValues,
                                                           bool isMerged, int mergeStartRowIndex, int mergeEndRowIndex, int mergeStartColumnIndex, int mergeEndColumnIndex)
        {
            ExcelColumnEntity exlColumn = new ExcelColumnEntity();
            exlColumn.Header = CreateReportCell(exlDoc, headerRowIndex, columnIndex, headerText, isMerged, mergeStartRowIndex, mergeEndRowIndex, mergeStartColumnIndex, mergeEndColumnIndex);

            List<BaseExcelCellEntity> rowValueList = new List<BaseExcelCellEntity>();

            foreach (string item in rowValues)
            {
                string val = (isPercentage == true) ? item.ToString() + "%" : item.ToString();
                rowValueList.Add(CreateReportCell(exlDoc, dataRowIndex, columnIndex, val, isMerged, mergeStartRowIndex, mergeEndRowIndex, mergeStartColumnIndex, mergeEndColumnIndex));
                dataRowIndex++;
            }

            exlColumn.RowValues = rowValueList;

            return exlColumn;
        }

        private List<string> GetReportColumnList(string reportName, string sectionId, string reportXmlFilePath)
        {
            List<string> colList = null;

            try
            {
                colList = new List<string>();
                XDocument xdoc = XDocument.Load(reportXmlFilePath);

                //var items = from item in xdoc.Descendants("item")
                //            select new
                //            {
                //                Title = item.Element("title").Value,
                //                Year = item.Element("year").Value,
                //                Categories = item.Descendants("categories").Descendants().Select(x => x.Value).ToList(),
                //                Count = item.Element("count").Value
                //            };
               
                 //var selectedBookAttribute = (from r in document.Descendants("book").Where
                 //                       (r => (string)r.Attribute("id") == "bk102")
                 //                       select r.Element("author").Attribute("id").Value).FirstOrDefault();

                //var items = from report in xdoc.Descendants("Report").Where(r => r.Attribute("Title").Equals(reportName) && r.Element("Section").Attribute("Id").Equals(sectionId))
                //            select new
                //            {
                //                Columns = from c in report.Element("Columns").Elements()
                //                             select (string)c,

                //            };


                //var columns = (from report in xdoc.Descendants("Report").Where(r => r.Attribute("Title").Equals(reportName) && r.Element("Section").Attribute("Id").Equals(sectionId))
                //               select r.Value).ToList();

                //IEnumerable<XElement> rptQuery = from rpt in xdoc.Descendants("Report")
                //                                 where rpt.Attribute("Title").Value == "DERSummary" && rpt.Element("Section").Attribute("Id").Value == "1"
                //                                 select rpt;
                              


                //var items = (from report in xdoc.Descendants("Report").Where
                //                        (t => t.Attribute("Title").Equals(reportName) && t.Element("Section").Attribute("Id").Equals(sectionId)).ToList();


                //var tagnodes = (from m in xdoc.Elements("Reports").Descendants("Report")
                //                where (m.Attribute("Title").Value.Equals("DERSummary"))
                //                && m.Element("Section").Attribute("Id").Value.Equals("1")
                //                // && bool.Parse(m.Element(CommonConstants.tags).Element(CommonConstants.tag).Value)
                //                select m.Element("Columns")).ToList();


                //var items = from item in xdoc.Descendants("item")
                //            select new
                //            {
                //                Title = (string)item.Element("title"),
                //                Year = (int)item.Element("year"),
                //                Count = (int)item.Element("count"),
                //                Categories = from c in item.Element("categories").Elements()
                //                             select (string)c
                //            };


                IEnumerable<XElement> rptQuery = from rpt in xdoc.Descendants("Report")
                                                 where rpt.Attribute("Title").Value == "DERSummary" && rpt.Element("Section").Attribute("Id").Value == "1"
                                                 select rpt;

                var companies = xdoc.Descendants("Company").Select(c => (string)c).ToArray();





            }
            catch (Exception)
            {
                throw;
            }

            return colList;
        }

        private List<string> GetDataTableColumnHeaderList(DataTable dataList) {
            List<string> columnHeaderList = new List<string>();

            foreach (DataColumn column in dataList.Columns)
                {
                    columnHeaderList.Add(column.ColumnName);
                }
            
            return columnHeaderList;
        }

        private string[] GetGroupedColumnHeaders(List<string> headerlist,string suffix)
        {
            List<string> ArrayList = new List<string>();
            ArrayList.Add("Date");
            foreach (string header in headerlist)
            {
                if (header.EndsWith(suffix))
                    ArrayList.Add(header);
            }
            
            string[] strArray = ArrayList.ToArray();
           
            return strArray;
        }

        private void setFinalColumnHeaders (string[] handlerColumns,DataTable handlerTable,int count)
        {
               for (int i = 1; i < handlerColumns.Length; i++)
                {
                    string str = handlerColumns[i].ToString();
                    handlerTable.Columns[i].ColumnName = str.Substring(0, str.Length - count); 
                }
        }

        //public static IEnumerable GetPropertyFields(string reportName, int sectionId, string reportXmlFilePath)
        //{
        //    try
        //    {
        //        XDocument xDocument = null;

        //        if (File.Exists(reportXmlFilePath))
        //        {
        //            xDocument = XDocument.Load(reportXmlFilePath);
        //        }

        //        var tagnodes = (from m in xDocument.Elements(CommonConstants.sites).Descendants(CommonConstants.site)
        //                        where (m.Attribute(CommonConstants.name).Value.Equals(sitename))
        //                        // && bool.Parse(m.Element(CommonConstants.tags).Element(CommonConstants.tag).Value)
        //                        select m.Element(CommonConstants.tags).Descendants(CommonConstants.tag)).ToList();

        //        if (tagnodes != null && tagnodes.Count() > 0)
        //        {
        //            return from d in tagnodes[0]
        //                   select d;
        //        }

        //    }
        //    catch (Exception exc)
        //    {
        //        throw exc;
        //    }
        //    return null;
        //}
 
        #endregion



    }
}
