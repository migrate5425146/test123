﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.CRM.Model;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class ActivityBR
    {
        private static ActivityBR instance = null;
        private static object syncLock = new Object();

        private ActivityDAO ActivityDataService;

        public static ActivityBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new ActivityBR();
                }
                return instance;
            }
        }

        private ActivityBR()
        {
            ActivityDataService = new ActivityDAO();
        }

        #region - Init Object -

        public ActivityEntity CreateObject()
        {
            return ActivityDataService.CreateObject();
        }

        public ActivityEntity CreateObject(int entityId)
        {
            return ActivityDataService.CreateObject(entityId);
        }

        #endregion

        public bool Save(ActivityEntity activity, ref int reminderActivityId)
        {
            DbWorkflowScope transactionScope = ActivityDataService.WorkflowScope;
            bool isSuccess = true;

            try
            {
                using (transactionScope)
                {
                    ActivityDAO activityService = new ActivityDAO(transactionScope);

                    if (activity.ActivityID > 0)
                    {
                        reminderActivityId = activity.ActivityID;
                        isSuccess = activityService.UpdateActivity(activity);
                    }
                    else
                    {
                        isSuccess = activityService.InsertActivity(activity, ref reminderActivityId);
                    }

                    if (activity.ReminderCreated == false && (activity.SendReminder.ToUpper() == "Y")
                            && (activity.ReminderDate > activity.StartDate) && isSuccess)
                    {
                        activity.ReminderCreated = true;
                        string SubjectforReminder = activity.Subject;
                        activity.Subject = activity.Subject.IndexOf("FU:") >= 0 ? activity.Subject : "FU:" + activity.Subject;
                        activity.ParentActivityID = (activity.ParentActivityID == 0 ? activity.ActivityID : activity.ParentActivityID);
                        activity.Comments = "";
                        activity.SendReminder = "N";
                        activity.SentMail = "N";

                        isSuccess = activityService.InsertActivity(activity, ref reminderActivityId);

                    }

                    if (isSuccess && (!string.IsNullOrEmpty( activity.CustomerCode) || activity.LeadID > 0 ))
                        isSuccess = activityService.UpdateLeadCustomer(activity);

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool InsertCustomerActivity(ActivityEntity customerActivityEntity, string originator, out int appointmentID)
        {
            DbWorkflowScope transactionScope = ActivityDataService.WorkflowScope;
            bool success = true;

            try
            {
                using (transactionScope)
                {
                    AppointmentDAO appointmentService = new AppointmentDAO(transactionScope);

                    success = ActivityDataService.InsertCustomerActivity(customerActivityEntity, originator, out appointmentID);

                    if (success)
                    {
                        customerActivityEntity.AppointmentID = appointmentID;
                        success = ActivityDataService.UpdateActivityWithAppointment(customerActivityEntity);
                    }

                    if (success)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return success;
        }

        public string InsertConversionQuery(ActivityEntity activity)
        {
            try
            {
                return ActivityDataService.InsertConversionQuery(activity);
            }
            catch
            {
                throw;
            }
        }

        public List<ActivityEntity> GetRelatedActivity(ArgsEntity args)
        {
            try
            {
                return ActivityDataService.GetRelatedActivity(args);
            }
            catch
            {
                throw;
            }
        }

        public ActivityEntity GetActivity(int iActivityID)
        {
            try
            {
                return ActivityDataService.GetActivity(iActivityID);
            }
            catch
            {
                throw;
            }
        }

        public List<ActivityEntity> GetActivitiesForCustomer(ArgsEntity args)
        {
            try
            {
                return ActivityDataService.GetActivitiesForCustomer(args);
            }
            catch
            {
                throw;
            }
        }

        public List<CustomerActivityEntity> GetAllActivities(ArgsEntity args, string repTerritory = "")
        {
            
            try
            {
                return ActivityDataService.GetAllActivities(args, repTerritory);
            }
            catch
            {
                throw;
            }
        }
        
        public List<CustomerActivityModel> GetAllActivitiesNew(ArgsModel args, string repTerritory = "")
        {
            try
            {
                return ActivityDataService.GetAllActivitiesNew(args, repTerritory);
            }
            catch
            {
                throw;
            }
        }

        public List<CustomerActivityEntity> GetActivityCountByType(ArgsEntity args)
        {
            try
            {
                return ActivityDataService.GetActivityCountByType(args);
            }
            catch
            {
                throw;
            }
        }

        public List<CustomerActivityEntity> GetAllActivitiesByType(ArgsEntity args, string sActivityType = "")
        {
            try
            {
                return ActivityDataService.GetAllActivitiesByType(args, sActivityType);
            }
            catch
            {
                throw;
            }
        }

        public int ChangeActivityDate(ActivityEntity activity, bool UpdateFromCalendar = false)
        {
            try
            {
                return ActivityDataService.ChangeActivityDate(activity,UpdateFromCalendar);
            }
            catch
            {
                throw;
            }
        }

        public List<LeadCustomerViewEntity> GetNotCalledCustomers(ArgsEntity args)
        {
            try
            {
                return ActivityDataService.GetNotCalledCustomers(args);
            }
            catch
            {
                throw;
            }
        }

        public List<LeadCustomerViewEntity> GetCalledCustomers(ArgsEntity args)
        {
            try
            {
                return ActivityDataService.GetCalledCustomers(args); ;
            }
            catch
            {
                throw;
            }
        }

        public CustomerActivityEntity GetActivityAppoitmentId(int iActivityId)
        {
            try
            {
                return ActivityDataService.GetActivityAppoitmentId(iActivityId); ;
            }
            catch
            {
                throw;
            }
        }

        public ActivityEntity GetActivityByAppoitmentId(int iAppointmentId)
        {
            try
            {
                return ActivityDataService.GetActivityByAppoitmentId(iAppointmentId);
            }
            catch
            {
                throw;
            }
        }

        public int ResetAppointment(ActivityEntity activity)
        {
            try
            {
                return ActivityDataService.ResetAppointment(activity);
            }
            catch
            {
                throw;
            }
        }

        public int Delete(ActivityEntity activity)
        {
            try
            {
                return ActivityDataService.Delete(activity);
            }
            catch
            {
                throw;
            }
        }

        public List<ActivityEntity> GetActivitiesForEndUser(ArgsEntity args)
        {
            try
            {
                return ActivityDataService.GetActivitiesForEndUser(args);
            }
            catch
            {
                throw;
            }
        }

        public List<CustomerActivityEntity> GetOutstandingActivities(ArgsEntity args)
        {
            try
            {
                return ActivityDataService.GetOutstandingActivities(args);
            }
            catch
            {
                throw;
            }
        }

        public int UpdateActivityFromPlanner(ActivityEntity activity , string OriginatorType)
        {
            try
            {
                if (OriginatorType == "ASE")
                {
                    return ActivityDataService.UpdateTMEActivityFromPlanner(activity);
                }

                else
                {
                    return ActivityDataService.UpdateActivityFromPlanner(activity);
                }
            }
            catch
            {
                throw;
            }
        }

        public int GetScheduledActivitiesForHomeCount(ArgsEntity args)
        {
            try
            {
                int activities = ActivityDataService.GetAllActivitiesForHomeCount(args);

                return activities;
            }
            catch
            {
                throw;
            }
        }

        public int GetOutstandingActivitiesCount(ArgsEntity args)
        {
            try
            {
                int activities = ActivityDataService.GetOutstandingActivitiesCount(args);

                return activities;
            }
            catch
            {
                throw;
            }
        }

        public AppointmentCountViewEntity GetAllActivitiesByTypeCount(ArgsEntity args)
        {
            try
            {
                return ActivityDataService.GetAllActivitiesByTypeCount(args);
            }
            catch
            {
                throw;
            }
        }

        public List<CustomerActivityEntity> GetAllActivitiesForHome(ArgsEntity args)
        {
            try
            {
                return ActivityDataService.GetAllActivitiesForHome(args);
            }
            catch
            {
                throw;
            }
        }
        public int GetRemindersForHomeCount(ArgsEntity args)
        {
            try
            {
                return ActivityDataService.GetRemindersForHomeCount(args);
            }
            catch
            {
                throw;
            }
        }

        public List<CallCycleContactEntity> GetAllCallCyclesCountById(ArgsEntity args)
        {
            try
            {
                return ActivityDataService.GetAllCallCyclesCountById(args);
            }
            catch
            {
                throw;
            }
        }

        public List<CallCycleContactEntity> GetAllCallCycles(ArgsEntity args)
        {
            try
            {
                return ActivityDataService.GetAllCallCycles(args);
            }
            catch
            {
                throw;
            }
        }

        public int UpdateActivityStatusForTME(List<ActivityEntity> activityList)
        {
            int isSuccess = 0;
            //DbWorkflowScope transactionScope = ActivityDataService.WorkflowScope;
            //ActivityDAO activityDAOService = new ActivityDAO(transactionScope);
            try
            {
                if (activityList.Count > 0)
                {
                    foreach (ActivityEntity activityEntity in activityList)
                    {
                        isSuccess = ActivityDataService.UpdateActivityStatusForTME(activityEntity);
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }
    }
}
