﻿using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.CRM.Model;
using Peercore.Workflow.Common;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.BusinessRules
{
    public class RegionBR
    {
        private static RegionBR instance = null;
        private static object syncLock = new Object();

        private RegionDAO RegionDataService;
        private OriginatorDAO OriginatorDataService;

        public static RegionBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new RegionBR();
                }
                return instance;
            }
        }

        private RegionBR()
        {
            RegionDataService = new RegionDAO();
            OriginatorDataService = new OriginatorDAO();
        }

        #region "Region"

        public List<RegionModel> GetAllRegions(ArgsModel args)
        {
            try
            {
                return RegionDataService.GetAllRegions(args);
            }
            catch
            {
                throw;
            }
        }

        public List<RegionModel> GetAllRegionsByRegionCode(ArgsModel args, string RegionCode)
        {
            try
            {
                return RegionDataService.GetAllRegionsByRegionCode(args, RegionCode);
            }
            catch
            {
                throw;
            }
        }

        public List<RegionModel> GetAllRegionsByRegionName(ArgsModel args, string RegionName)
        {
            try
            {
                return RegionDataService.GetAllRegionsByRegionName(args, RegionName);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteRegion(string region_id)
        {
            try
            {
                return RegionDataService.DeleteRegion(region_id);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveRegion(string comp_id, string region_id, string region_code, string region_name)
        {
            DbWorkflowScope transactionScope = RegionDataService.WorkflowScope;

            bool isSuccess = true;

            try
            {
                using (SqlConnection conn = RegionDataService.SqlConnOpen(transactionScope))
                {
                    using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                    {
                        isSuccess = RegionDataService.SaveRegion(sqlTransaction, comp_id, region_id, region_code, region_name);

                        if (isSuccess)
                            sqlTransaction.Commit();
                        else
                            sqlTransaction.Rollback();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return isSuccess;
        }

        #endregion

        #region "RSM"

        public List<RsmModel> GetAllRsm(ArgsModel args)
        {
            try
            {
                return RegionDataService.GetAllRsm(args);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateRsmPassword(string rsmId, string password)
        {
            bool retVal = false;

            try
            {
                RsmModel objAsm = new RsmModel();
                objAsm = RegionDataService.GetRsmDetailsByRsmId(rsmId);

                if (objAsm.RsmId > 0)
                {
                    retVal = OriginatorDataService.UpdateOriginatorPassword(objAsm.RsmUserName, password);
                }
                else
                {
                    retVal = false;
                }
            }
            catch
            {
                throw;
            }

            return retVal;
        }

        public List<RsmModel> GetAllRsmByRsmCode(ArgsModel args, string RsmCode)
        {
            try
            {
                return RegionDataService.GetAllRsmByRsmCode(args, RsmCode);
            }
            catch
            {
                throw;
            }
        }

        public List<RsmModel> GetRsmDetailsByRegionId(ArgsModel args, string RegionId)
        {
            try
            {
                return RegionDataService.GetRsmDetailsByRegionId(args, RegionId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteRsmByRsmId(string rsm_id)
        {
            try
            {
                return RegionDataService.DeleteRsmByRsmId(rsm_id);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveRsm(string region_id,
                            string rsm_id,
                            string rsm_code,
                            string rsm_name,
                            string rsm_username,
                            string rsm_tel1,
                            string rsm_email,
                            string created_by)
        {
            try
            {
                return RegionDataService.SaveRsm(region_id,
                            rsm_id,
                            rsm_code,
                            rsm_name,
                            rsm_username,
                            rsm_tel1,
                            rsm_email,
                            created_by);
            }
            catch
            {
                throw;
            }
        }

        #endregion
    }
}
