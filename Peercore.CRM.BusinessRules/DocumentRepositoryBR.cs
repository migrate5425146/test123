﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class DocumentRepositoryBR
    {
        
        private static DocumentRepositoryBR instance = null;
        private static object syncLock = new Object();
        private DocumentRepositoryDAO documentRepositoryDataService;

        public static DocumentRepositoryBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new DocumentRepositoryBR();
                }
                return instance;
            }
        }

        #region - Init Object -

        public DocumentRepositoryEntity CreateObject()
        {
            return documentRepositoryDataService.CreateObject();
        }

        public DocumentRepositoryEntity CreateObject(int entityId)
        {
            return documentRepositoryDataService.CreateObject(entityId);
        }

        #endregion

        #region - Constructor -

        private DocumentRepositoryBR()
        {
            documentRepositoryDataService = new DocumentRepositoryDAO();
        }
        #endregion

        #region - Public Methods -

        public bool Save(DocumentEntity documentEntity)
        {
            DbWorkflowScope transactionScope = documentRepositoryDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    if (documentEntity.DocumentID != 0)
                    {
                        isSuccess = documentRepositoryDataService.UpdateDocumentRepository(documentEntity);
                    }
                    else
                    {
                        documentEntity.DocumentID = documentRepositoryDataService.GetNextDocumentRepositoryID();
                        isSuccess = documentRepositoryDataService.InsertDocumentRepository(documentEntity);
                    }


                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public List<DocumentEntity> GetDocumentDetails()
        {
            try
            {
                return documentRepositoryDataService.GetDocumentDetails();
            }
            catch
            {
                throw;
            }
        }

        public DocumentEntity GetDocumentDetails(int repositoryId)
        {
            try
            {
               return  documentRepositoryDataService.GetDocumentDetails(repositoryId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteDocument(int repositoryID)
        {
            try
            {
                return documentRepositoryDataService.DeleDocumentRepository(repositoryID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion
    }
}
