﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;

namespace Peercore.CRM.BusinessRules
{
    public class HolidayPlannerChangeLogBR
    {
        private static HolidayPlannerChangeLogBR instance = null;
        private static object syncLock = new Object();

        private HolidayPlannerChangeLogDAO ChangeLogDataService;

        public static HolidayPlannerChangeLogBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new HolidayPlannerChangeLogBR();
                }
                return instance;
            }
        }

        private HolidayPlannerChangeLogBR()
        {
            ChangeLogDataService = new HolidayPlannerChangeLogDAO();
        }

        #region - Init Object -

        public HolidayPlannerChangeLogEntity CreateObject()
        {
            return ChangeLogDataService.CreateObject();
        }

        public HolidayPlannerChangeLogEntity CreateObject(int entityId)
        {
            return ChangeLogDataService.CreateObject(entityId);
        }

        #endregion

        public int InsertHolidayPlannerChangeLog(HolidayPlannerChangeLogEntity holidayPlannerChangeLogEntity)
        {
            int changeLogId = 0;
            try
            {
                changeLogId = ChangeLogDataService.InsertHolidayPlannerChangeLog(holidayPlannerChangeLogEntity);
            }
            catch
            {
                throw;
            }
            return changeLogId;
        }

        public bool UpdateHolidayPlannerChangeLogStatus(HolidayPlannerChangeLogEntity holidayPlannerChangeLogEntity)
        {
            bool isSuccess = true;
            try
            {
                isSuccess = ChangeLogDataService.UpdateHolidayPlannerChangeLogStatus(holidayPlannerChangeLogEntity);
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public HolidayPlannerChangeLogEntity GetHolidayPlannerChangeLogById(int holidayPlannerChangeLogId)
        {
            try
            {
                return ChangeLogDataService.GetHolidayPlannerChangeLogById(holidayPlannerChangeLogId);
            }
            catch
            {
                throw;
            }
        }

    }
}
