﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.DataAccess.datasets;
using Peercore.CRM.Entities;
using Peercore.CRM.Model;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class SchemeHeaderBR
    {
        private static SchemeHeaderBR instance = null;
        private static object syncLock = new Object();

        private SchemeHeaderDAO SchemeHeaderDataService;

        public static SchemeHeaderBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new SchemeHeaderBR();
                }
                return instance;
            }
        }

        private SchemeHeaderBR()
        {
            SchemeHeaderDataService = new SchemeHeaderDAO();
        }

        #region - Init Object -

        public SchemeHeaderEntity CreateObject()
        {
            return SchemeHeaderDataService.CreateObject();
        }

        public SchemeHeaderEntity CreateObject(int entityId)
        {
            return SchemeHeaderDataService.CreateObject(entityId);
        }

        #endregion

        public bool UpdateSchemeHeader(SchemeHeaderEntity schemeHeader)
        {
            try
            {
                return SchemeHeaderDataService.UpdateSchemeHeader(schemeHeader);
            }
            catch
            {
                throw;
            }
        }

        public bool InsertSchemeHeader(out int schemeHeaderId, SchemeHeaderEntity schemeHeader)
        {
            try
            {
                return SchemeHeaderDataService.InsertSchemeHeader(out schemeHeaderId,schemeHeader);
            }
            catch
            {
                throw;
            }
        }

        public List<SchemeHeaderEntity> GetAllSchemeHeader(ArgsEntity args)
        {
            try
            {
                return SchemeHeaderDataService.GetAllSchemeHeader(args);
            }
            catch
            {
                throw;
            }
        }

        public List<SchemeHeaderEntity> GetAllSchemeHeader()
        {
            try
            {
                return SchemeHeaderDataService.GetAllSchemeHeader();
            }
            catch
            {
                throw;
            }
        }

        public SchemeHeaderEntity GetSchemeHeaderById(int schemeHeaderId)
        {
            SchemeHeaderDAO schemeHeaderService = new SchemeHeaderDAO();
            SchemeDetailsDAO schemeDetailsService = new SchemeDetailsDAO();
            OptionLevelDAO optionLevelService = new OptionLevelDAO();
            OptionLevelCombinationDAO optionLevelCombinationService = new OptionLevelCombinationDAO();
            FreeProductDAO freeProductService = new FreeProductDAO();

            SchemeHeaderEntity schemeHeader = new SchemeHeaderEntity();
            List<SchemeDetailsEntity> schemeDetailsList = new List<SchemeDetailsEntity>();
            List<OptionLevelEntity> optionLevelList = new List<OptionLevelEntity>();
            List<OptionLevelCombinationEntity> optionLevelCombinationList = new List<OptionLevelCombinationEntity>();
                
            try
            {
                schemeHeader =  SchemeHeaderDataService.GetSchemeHeaderById(schemeHeaderId);
                schemeDetailsList = schemeDetailsService.GetSchemeDetailsBySchemeHeaderId(schemeHeaderId);

                foreach (SchemeDetailsEntity schemeentity in schemeDetailsList)
                {
                    optionLevelList = optionLevelService.GetOptionLevelByDetailsId(schemeentity.SchemeDetailsId);

                    foreach (OptionLevelEntity optionLevelentity in optionLevelList)
                    {
                        optionLevelCombinationList = optionLevelCombinationService.GetOptionLevelCombinationByOptionLevelId(optionLevelentity.OptionLevelId);
                        optionLevelentity.OptionLevelCombinationList = optionLevelCombinationList;
                    }

                    schemeentity.FreeProductList = freeProductService.GeFreeProductsByDetailsId(schemeentity.SchemeDetailsId);

                    schemeentity.OptionLevelList = optionLevelList;
                }
                schemeHeader.SchemeDetailsList = schemeDetailsList;
                return schemeHeader;
            }
            catch
            {
                throw;
            }
        }

        public SchemeHeaderEntity GetSchemeHeaderDetailsById(int schemeHeaderId)
        {
            SchemeHeaderDAO schemeHeaderService = new SchemeHeaderDAO();
            SchemeHeaderEntity schemeHeader = new SchemeHeaderEntity();

            try
            {
                schemeHeader = SchemeHeaderDataService.GetSchemeHeaderById(schemeHeaderId);
                
                return schemeHeader;
            }
            catch
            {
                throw;
            }
        }

        public bool SchemeHeaderSave(out int schemeHeaderId, SchemeHeaderEntity schemeHeader)
        {
            DbWorkflowScope transactionScope = SchemeHeaderDataService.WorkflowScope;
            bool isSuccess = true;

            try
            {
                using (transactionScope)
                {
                    SchemeHeaderDAO schemeHeaderService = new SchemeHeaderDAO(transactionScope);
                    SchemeDetailsDAO schemeDetailsService = new SchemeDetailsDAO(transactionScope);
                    OptionLevelDAO optionLevelService = new OptionLevelDAO(transactionScope);
                    OptionLevelCombinationDAO optionLevelCombinationService = new OptionLevelCombinationDAO(transactionScope);
                    FreeProductDAO freeProductService = new FreeProductDAO(transactionScope);

                    if (schemeHeader.SchemeHeaderId > 0)
                    {
                        schemeHeaderId = schemeHeader.SchemeHeaderId;
                        isSuccess = schemeHeaderService.UpdateSchemeHeader(schemeHeader);
                    }
                    else
                    {
                        isSuccess = schemeHeaderService.InsertSchemeHeader(out schemeHeaderId, schemeHeader); 
                    }

                    if (isSuccess)
                    {
                        schemeDetailsService.UpdateSchemeDetailsInActive(schemeHeaderId);
                        optionLevelService.UpdateOptionLevelInActive(schemeHeaderId);
                        optionLevelCombinationService.UpdateOLCombinationInActive(schemeHeaderId);
                        freeProductService.UpdateFreeProductInActive(schemeHeaderId);
                    }

                    foreach (var item in schemeHeader.SchemeDetailsList)
                    {
                        int schemeDetailId = 0;

                        item.SchemeHeaderId = schemeHeaderId;
                        item.LastModifiedBy = schemeHeader.LastModifiedBy;
                        item.CreatedBy = schemeHeader.LastModifiedBy;
                        if (item.FreeIssueCondition == null) { item.FreeIssueCondition = "and"; }
                        schemeDetailsService.InsertSchemeDetails(out schemeDetailId, item);
                        item.SchemeDetailsId = schemeDetailId;
                        
                        if (item.OptionLevelList != null)
                        {
                            foreach (var optionLevel in item.OptionLevelList)
                            {
                                int optionLevelId = 0;
                                optionLevel.SchemeDetailId = schemeDetailId;
                                optionLevelService.InsertOptionLevel(out optionLevelId, optionLevel);
                                if(optionLevel.OptionLevelCombinationList!=null){
                                    foreach (var product in optionLevel.OptionLevelCombinationList)
                                    {
                                        product.OptionLevelId = optionLevelId;
                                        optionLevelCombinationService.InsertOptionLevelCombination(product);
                                    }
                                }
                            }
                        }

                        if (item.FreeProductList != null)
                        {
                            foreach (var freep in item.FreeProductList)
                            {
                                int optionLevelId = 0;
                                freep.SchemeDetailsId = schemeDetailId;
                                freeProductService.InsertFreeProduct(out optionLevelId, freep);                                
                            }
                        }
                    }

                    
                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public List<SchemeHeaderEntity> GetSchemeHeaderByName(string name,bool isActive)
        {
            try
            {
                return SchemeHeaderDataService.GetSchemeHeaderByName(name,isActive);
            }
            catch
            {
                throw;
            }
        }

        public bool IsSchemeHeaderNameExists(SchemeHeaderEntity schemeHeaderEntity)
        {
            try
            {
                return SchemeHeaderDataService.IsSchemeHeaderNameExists(schemeHeaderEntity);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateSchemeHeaderIsActive(SchemeHeaderEntity schemeHeader)
        {
            try
            {
                return SchemeHeaderDataService.UpdateSchemeHeaderIsActive(schemeHeader);
            }
            catch
            {
                throw;
            }
        }

        public SchemeHeaderEntity GetAllSchemeHeaderByDivisionId(int divisionId)
        {
            try
            {
                return SchemeHeaderDataService.GetAllSchemeHeaderByDivisionId(divisionId);
            }
            catch
            {
                throw;
            }
        }

        public SchemeHeaderEntity GetSchemeHeaderByAccessToken(string accessToken)
        {
            SchemeHeaderDAO schemeHeaderService = new SchemeHeaderDAO();
            SchemeDetailsDAO schemeDetailsService = new SchemeDetailsDAO();
            OptionLevelDAO optionLevelService = new OptionLevelDAO();
            OptionLevelCombinationDAO optionLevelCombinationService = new OptionLevelCombinationDAO();
            FreeProductDAO freeProductService = new FreeProductDAO();

            SchemeHeaderEntity schemeHeader = new SchemeHeaderEntity();
            List<SchemeDetailsEntity> schemeDetailsList = new List<SchemeDetailsEntity>();
            List<OptionLevelEntity> optionLevelList = new List<OptionLevelEntity>();
            List<OptionLevelCombinationEntity> optionLevelCombinationList = new List<OptionLevelCombinationEntity>();
            int schemeHeaderId = 0;

            try
            {
                schemeHeader = SchemeHeaderDataService.GetSchemeHeaderByAccessToken(accessToken);
                schemeHeaderId = schemeHeader.SchemeHeaderId;
                schemeDetailsList = schemeDetailsService.GetSchemeDetailsBySchemeHeaderId(schemeHeaderId);

                foreach (SchemeDetailsEntity schemeentity in schemeDetailsList)
                {
                    optionLevelList = optionLevelService.GetOptionLevelByDetailsId(schemeentity.SchemeDetailsId);
                    foreach (OptionLevelEntity optionLevelentity in optionLevelList)
                    {
                        optionLevelCombinationList = optionLevelCombinationService.GetOptionLevelCombinationByOptionLevelId(optionLevelentity.OptionLevelId);
                        optionLevelentity.OptionLevelCombinationList = optionLevelCombinationList;
                    }

                    schemeentity.FreeProductList = freeProductService.GeFreeProductsByDetailsId(schemeentity.SchemeDetailsId);
                    schemeentity.OptionLevelList = optionLevelList;
                }

                schemeHeader.SchemeDetailsList = schemeDetailsList;
                return schemeHeader;
            }
            catch
            {
                throw;
            }
        }
        
        public SchemeHeaderEntity GetDiscountSchemeForPreSalesResponse(string accessToken)
        {
            SchemeHeaderDAO schemeHeaderService = new SchemeHeaderDAO();
            SchemeDetailsDAO schemeDetailsService = new SchemeDetailsDAO();
            OptionLevelDAO optionLevelService = new OptionLevelDAO();
            OptionLevelCombinationDAO optionLevelCombinationService = new OptionLevelCombinationDAO();
            FreeProductDAO freeProductService = new FreeProductDAO();

            SchemeHeaderEntity schemeHeader = new SchemeHeaderEntity();
            List<SchemeDetailsEntity> schemeDetailsList = new List<SchemeDetailsEntity>();
            List<OptionLevelEntity> optionLevelList = new List<OptionLevelEntity>();
            List<OptionLevelCombinationEntity> optionLevelCombinationList = new List<OptionLevelCombinationEntity>();
            int schemeHeaderId = 0;

            try
            {
                schemeHeader = SchemeHeaderDataService.GetSchemeHeaderForPreSalesByAccessToken(accessToken);
                schemeHeaderId = schemeHeader.SchemeHeaderId;
                schemeDetailsList = schemeDetailsService.GetSchemeDetailsBySchemeHeaderId(schemeHeaderId);

                foreach (SchemeDetailsEntity schemeentity in schemeDetailsList)
                {
                    optionLevelList = optionLevelService.GetOptionLevelByDetailsId(schemeentity.SchemeDetailsId);
                    foreach (OptionLevelEntity optionLevelentity in optionLevelList)
                    {
                        optionLevelCombinationList = optionLevelCombinationService.GetOptionLevelCombinationByOptionLevelId(optionLevelentity.OptionLevelId);
                        optionLevelentity.OptionLevelCombinationList = optionLevelCombinationList;
                    }

                    schemeentity.FreeProductList = freeProductService.GeFreeProductsByDetailsId(schemeentity.SchemeDetailsId);
                    schemeentity.OptionLevelList = optionLevelList;
                }

                schemeHeader.SchemeDetailsList = schemeDetailsList;
                return schemeHeader;
            }
            catch
            {
                throw;
            }
        }

        public TradeLoadDataSet GetTradeLoadSummary(ArgsEntity args)
        {
            try
            {
                return SchemeHeaderDataService.GetTradeLoadSummary(args);
            }
            catch
            {
                throw;
            }
        }

        public List<SchemeHeaderModel> GetAllDiscountSchemeHeader(ArgsModel args)
        {
            try
            {
                return SchemeHeaderDataService.GetAllDiscountSchemeHeader(args);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveUpdateDiscountTerritory(string originator,
                                                       string scheme_header_id,
                                                       string territory_id,
                                                       string is_checked)
        {
            try
            {
                return SchemeHeaderDataService.SaveUpdateDiscountTerritory(originator,
                                                        scheme_header_id,
                                                        territory_id,
                                                        is_checked);
            }
            catch
            {
                throw;
            }
        }
    }
}
