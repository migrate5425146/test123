﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;

namespace Peercore.CRM.BusinessRules
{
    public class FrameMonitorBR
    {
        private static FrameMonitorBR instance = null;
        private static object syncLock = new Object();

        private FrameMonitorDAO FrameMonitorDataService;

        public static FrameMonitorBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new FrameMonitorBR();
                }
                return instance;
            }
        }

        private FrameMonitorBR()
        {
            FrameMonitorDataService = new FrameMonitorDAO();
        }

        #region - Init Object -

        public FrameMonitorEntity CreateObject()
        {
            return FrameMonitorDataService.CreateObject();
        }

        public FrameMonitorEntity CreateObject(int entityId)
        {
            return FrameMonitorDataService.CreateObject(entityId);
        }

        #endregion

        public bool InsertFrameMonitor(FrameMonitorEntity frameMonitorEntity)
        {
            bool isSuccess = false;
            try
            {
                isSuccess = FrameMonitorDataService.InsertFrameMonitor(frameMonitorEntity);
            }
            catch
            {
                throw;
            }
            return (isSuccess);
        }

        public bool UpdateFrameMonitor(FrameMonitorEntity frameMonitorEntity)
        {
            bool isSuccess = false;
            try
            {
                isSuccess = FrameMonitorDataService.UpdateFrameMonitor(frameMonitorEntity);
            }
            catch
            {
                throw;
            }
            return (isSuccess);
        }
    }
}
