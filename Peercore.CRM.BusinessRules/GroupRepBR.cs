﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class GroupRepBR
    {
        private static GroupRepBR instance = null;
        private static object syncLock = new Object();

        private GroupRepDAO GroupRepDataService;

        public static GroupRepBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new GroupRepBR();
                }
                return instance;
            }
        }

        private GroupRepBR()
        {
            GroupRepDataService = new GroupRepDAO();
        }

        #region - Init Object -

        public GroupRepEntity CreateObject()
        {
            return GroupRepDataService.CreateObject();
        }

        public GroupRepEntity CreateObject(int entityId)
        {
            return GroupRepDataService.CreateObject(entityId);
        }

        #endregion

        public List<GroupRepEntity> GetAllGroupsReps(int RepGroupId)
        {
            try
            {
                return GroupRepDataService.GetAllGroupsReps(RepGroupId);
            }
            catch
            {
                throw;
            }
        }
    }
}
