﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class CallCycleBR
    {
        private static CallCycleBR instance = null;
        private static object syncLock = new Object();

        private CallCycleDAO CallCycleService;

        public static CallCycleBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new CallCycleBR();
                }
                return instance;
            }
        }

        private CallCycleBR()
        {
            CallCycleService = new CallCycleDAO();
        }

        #region - Init Object -

        public CallCycleEntity CreateObject()
        {
            return CallCycleService.CreateObject();
        }

        public CallCycleEntity CreateObject(int entityId)
        {
            return CallCycleService.CreateObject(entityId);
        }

        #endregion

        public bool GetCallCycleState(int callCycleId)
        {
            try
            {
                return CallCycleService.GetCallCycleState(callCycleId);
            }
            catch
            {
                throw;
            }
        }

        public List<CallCycleEntity> GetCallCycle(ArgsEntity args, bool bInActive = false,
            string CallCycleName = "", int CallCycleId = 0)
        {
            try
            {
                return CallCycleService.GetCallCycle(args, bInActive, CallCycleId, CallCycleName);
            }
            catch
            {
                throw;
            }
        }

        public List<CallCycleContactEntity> GetCallCycleContacts(ArgsEntity args, int iCallCycleID, bool bInActive = false, string sCallCycleType = "")
        {
            try
            {
                List<CallCycleContactEntity> callCycleContacts =  CallCycleService.GetAllCallCycles(args, bInActive, iCallCycleID, sCallCycleType);
                return callCycleContacts;
            }
            catch
            {
                throw;
            }
        }

        public bool Save(CallCycleEntity callCycle, List<CallCycleContactEntity> oCustomers, string originator, TimeSpan tsStartTime, out int callCycleId)
        {
            DbWorkflowScope transactionScope = CallCycleService.WorkflowScope;
            bool isSuccess = true;

            try
            {
                using (transactionScope)
                {
                    CallCycleDAO callCycleDAOService = new CallCycleDAO(transactionScope);

                    isSuccess = callCycleDAOService.Save(callCycle, oCustomers, originator, tsStartTime,out callCycleId);


                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool DeactivateCallCycle(int callCycleId, string isDelete)
        {
            try
            {
                return CallCycleService.DeactivateCallCycle(callCycleId, isDelete);
            }
            catch
            {
                throw;
            }
        }

        public bool Delete(CallCycleContactEntity callCycleContact, int callCycleId)
        {
            try
            {
                return CallCycleService.Delete(callCycleContact,callCycleId);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveSchedule(List<CallCycleContactEntity> callCycleContactEntityList)
        {
            DbWorkflowScope transactionScope = CallCycleService.WorkflowScope;
            bool isSuccess = true;

            try
            {
                using (transactionScope)
                {
                    CallCycleDAO callCycleDAOService = new CallCycleDAO(transactionScope);

                    foreach (CallCycleContactEntity item in callCycleContactEntityList)
                    {
                        if (item.LeadStage.StageName.Equals("End User"))
                        {
                            isSuccess = callCycleDAOService.SaveScheduleEndUser(item);
                        }
                        else if (item.LeadStage.StageName.Equals("Customer"))
                        {
                            isSuccess = callCycleDAOService.SaveScheduleCustomer(item);
                        }
                        else
                        {
                            isSuccess = callCycleDAOService.SaveScheduleLead(item);
                        }
                    }

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool CreateCallCycleActivitiesFromTemplate(CallCycleEntity callCycle, List<CallCycleContactEntity> callCycleContact, ArgsEntity argsEntity)
        {
            DbWorkflowScope transactionScope = CallCycleService.WorkflowScope;
            bool isSuccess = true;

            try
            {
                using (transactionScope)
                {
                    CallCycleDAO callCycleDAOService = new CallCycleDAO(transactionScope);

                    foreach (CallCycleContactEntity item in callCycleContact)
                    {
                        if (!isSuccess)
                            break;
                        if (item.CreateActivity)
                        {
                            isSuccess = callCycleDAOService.CreateCallCycleActivitiesFromTemplate(callCycle, item, argsEntity);
                        }
                    }

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public List<LookupEntity> GetAllCallCyclesList(ArgsEntity args)
        {
            try
            {
                List<LookupEntity> callCycleContacts = CallCycleService.GetAllCallCyclesList(args);
                return callCycleContacts;
            }
            catch
            {
                throw;
            }
        }


        public bool SaveRoute(RouteEntity route)
        {
            try
            {
                bool status = CallCycleService.SaveRoute(route);
                return status;
            }
            catch
            {
                throw;
            }
        }

        public List<RouteEntity> GetRoutesByRepCode(ArgsEntity args)
        {
            try
            {

                List<RouteEntity> routes = CallCycleService.GetRoutesByRepCode(args);
                return routes;
            }
            catch
            {
                throw;
            }
        }

        public List<RouteEntity> GetRoutesForTME(ArgsEntity args)
        {
            try
            {

                List<RouteEntity> routes = CallCycleService.GetRoutesForTME(args);
                return routes;
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateRouteSequence(List<RouteEntity> routelist)
        {
            DbWorkflowScope transactionScope = CallCycleService.WorkflowScope;
            bool isSuccess = true;

            try
            {
                using (transactionScope)
                {
                    CallCycleDAO callCycleDAOService = new CallCycleDAO(transactionScope);

                    foreach (RouteEntity item in routelist)
                    {
                            isSuccess = callCycleDAOService.UpdateRouteSequence(item);

                            if (!isSuccess)
                                break;
                    }

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                return false;
            }

            return isSuccess;
        }

        public bool SchedulePlanedRoutesForDR(ActivityEntity activity , string originatorType)
        {
            try
            {
                if (originatorType == "ASE")
                {

                    bool status = CallCycleService.SchedulePlanedRoutesForTME(activity);
                    return status;
                }
                else
                {
                    bool status = CallCycleService.SchedulePlanedRoutesForDR(activity);
                    return status;
                }
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteRoutesByRepCode(RouteEntity route)
        {
            try
            {
                if (route.OriginatorType == "ASE")
                {
                    bool status = CallCycleService.DeleteRoutesForTME(route);
                    return status;
                }
                else {
                    bool status = CallCycleService.DeleteRoutesByRepCode(route);
                    return status;
                }
            }
            catch
            {
                throw;
            }
        }

        public List<RouteEntity> GetAssignedRoutes(string parentOriginator, ArgsEntity args)
        {
            try
            {
                return CallCycleService.GetAssignedRoutes(parentOriginator, args);
            }
            catch
            {
                throw;
            }
        }

        public List<RouteEntity> GetRouteNames(string routeName)
        {
            try
            {
                return CallCycleService.GetRouteNames(routeName);
            }
            catch
            {
                throw;
            }
        }

        public List<CallCycleEntity> GetCallCyclesForRep(string repcode)
        {
            try
            {
                return CallCycleService.GetCallCyclesForRep(repcode);
            }
            catch
            {
                throw;
            }
        }

        public List<MobileRouteEntity> GetTodayRoutesCustomersForTME(string tmeCode)
        {
            try
            {
                return CallCycleService.GetTodayRoutesCustomersForTME(tmeCode);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateTMERouteSequence(List<MobileRouteEntity> routelist)
        {
            DbWorkflowScope transactionScope = CallCycleService.WorkflowScope;
            bool isSuccess = true;

            try
            {
                using (transactionScope)
                {
                    CallCycleDAO callCycleDAOService = new CallCycleDAO(transactionScope);

                    foreach (MobileRouteEntity item in routelist)
                    {
                        isSuccess = callCycleDAOService.UpdateTMERouteSequence(item);

                        if (!isSuccess)
                            break;
                    }

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public List<MobileRouteEntity> GetRouteSummaryForTME(int routeId)
        {
            try
            {
                return CallCycleService.GetRouteSummaryForTME(routeId);
            }
            catch
            {
                throw;
            }
        }

        public List<CallCycleContactEntity> GetCallCycleContactsForNewEntry(ArgsEntity args, string repCode)
        {
            try
            {
                List<CallCycleContactEntity> callCycleContacts = CallCycleService.GetCallCycleContactsForNewEntry(args, repCode);
                return callCycleContacts;
            }
            catch
            {
                throw;
            }
        }

        public bool IsCallCycleContactInAnyOtherSchedule(string customerCode)
        {
            try
            {
                return CallCycleService.IsCallCycleContactInAnyOtherSchedule(customerCode);
            }
            catch
            {
                throw;
            }
        }

        public string GetRepCodeForCallCycleId(int callCycleId)
        {
            try
            {
                return CallCycleService.GetRepCodeForCallCycleId(callCycleId);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveOriginatorRouteOutlets(OriginatorRouteOutletsEntity originatorRouteOutletsEntity)
        {
            bool isSuccessAssign = false;
            bool isSuccessCallCycle = false;
            int callCycleId = 0;
            DbWorkflowScope transactionScope = CallCycleService.WorkflowScope;
            try
            {
                CallCycleDAO callCycleDAOService = new CallCycleDAO(transactionScope);
                using (transactionScope)
                {
                    if (!String.IsNullOrEmpty(originatorRouteOutletsEntity.Originator.RepCode))//Rep
                    {
                        if (originatorRouteOutletsEntity.Route.IsRouteAssignChanged)
                            isSuccessAssign = callCycleDAOService.SaveRoute(originatorRouteOutletsEntity.Route);
                        else
                            isSuccessAssign = true;
                        //check for a existing call cycle for Rep - if not callCycleId would be 0
                        callCycleId = callCycleDAOService.GetCallCycleForRepOrTme(originatorRouteOutletsEntity.Originator.RepCode, null);
                    }
                    else if (originatorRouteOutletsEntity.Originator.DeptString.Equals("ASE"))//TME
                    {
                        isSuccessAssign = true;
                        //check for a existing call cycle for TME - if not callCycleId would be 0
                        callCycleId = callCycleDAOService.GetCallCycleForRepOrTme(null, originatorRouteOutletsEntity.Originator.UserName);
                    }


                    ////For Callcycle
                    if (isSuccessAssign)
                    {
                        if (originatorRouteOutletsEntity.Outlets.Count > 0)
                        {
                            int newCallCycleId = 0;
                            CallCycleEntity callCycleEntity = CallCycleEntity.CreateObject();
                            callCycleEntity.CallCycleID = callCycleId;
                            callCycleEntity.Description = originatorRouteOutletsEntity.Originator.Name;
                            callCycleEntity.DueOn = DateTime.Now;
                            callCycleEntity.DelFlag = originatorRouteOutletsEntity.DelFlag;
                            callCycleEntity.CCType = originatorRouteOutletsEntity.CcType;
                            callCycleEntity.PrefixCode = originatorRouteOutletsEntity.Originator.RepCode;

                            foreach (CallCycleContactEntity item in originatorRouteOutletsEntity.Outlets)
                            {
                                item.RouteId = originatorRouteOutletsEntity.Route.RouteMasterId;
                                item.WeekDayId = 0;
                                item.DayOrder = 0;
                            }

                            isSuccessCallCycle = callCycleDAOService.Save(callCycleEntity, originatorRouteOutletsEntity.Outlets, originatorRouteOutletsEntity.CreatedBy, new TimeSpan(), out newCallCycleId);
                        }
                        else
                        {
                            isSuccessCallCycle = true;
                        }
                    }

                    if (isSuccessAssign && isSuccessCallCycle)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }


            }
            catch
            {
                throw;
            }
            return (isSuccessAssign && isSuccessCallCycle);
        }

        #region "Perfetti 2nd Phase"

        public bool UpdateSRTerritory(string Originator, string RepId, string TerritoryId)
        {
            DbWorkflowScope transactionScope = CallCycleService.WorkflowScope;

            bool isSuccess = true;

            try
            {
                using (transactionScope)
                {
                    isSuccess = CallCycleService.UpdateSRTerritory(transactionScope, Originator, RepId, TerritoryId);

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }

            return isSuccess;
        }

        public bool UpdateDBTerritory(string DistributerId, string TerritoryId, string CreatedBy)
        {
            DbWorkflowScope transactionScope = CallCycleService.WorkflowScope;

            bool isSuccess = true;

            try
            {
                using (transactionScope)
                {
                    isSuccess = CallCycleService.UpdateDBTerritory(transactionScope, DistributerId, TerritoryId, CreatedBy);

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }

            return isSuccess;
        }

        #endregion
    }
}
