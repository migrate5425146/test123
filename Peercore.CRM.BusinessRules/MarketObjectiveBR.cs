﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class MarketObjectiveBR
    {
        private static MarketObjectiveBR instance = null;
        private static object syncLock = new Object();

        private MarketObjectiveDAO MarketObjectiveDataService;

        public static MarketObjectiveBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new MarketObjectiveBR();
                }
                return instance;
            }
        }

        private MarketObjectiveBR()
        {
            MarketObjectiveDataService = new MarketObjectiveDAO();
        }

        #region - Init Object -

        public MarketObjectiveEntity CreateObject()
        {
            return MarketObjectiveDataService.CreateObject();
        }

        public MarketObjectiveEntity CreateObject(int entityId)
        {
            return MarketObjectiveDataService.CreateObject(entityId);
        }

        #endregion

        public List<MobileMarketObjectiveEntity> GetAllObjectivesMarketWise(string tme, int routeMasterID)
        {
            try
            {
                return MarketObjectiveDataService.GetAllObjectivesMarketWise(tme, routeMasterID);
            }
            catch
            {
                throw;
            }
        }

        public List<ObjectiveEntity> GetAllMarketObjectives(int marketId, ArgsEntity args)
        {
            try
            {
                return MarketObjectiveDataService.GetAllMarketObjectives(marketId, args);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveMarketObjectives(List<MarketObjectiveEntity> marketObjectiveDTOList)
        {
            DbWorkflowScope transactionScope = MarketObjectiveDataService.WorkflowScope;
            ObjectiveDAO objectiveDAO = new ObjectiveDAO();
            int objectiveID = 0;
            bool isSuccess = true;
            try
            {
                if (marketObjectiveDTOList.Count != 0)
                {
                    using (transactionScope)
                    {
                        foreach (MarketObjectiveEntity marketObjectiveEntity in marketObjectiveDTOList)
                        {
                            if (marketObjectiveEntity.Objective.ObjectiveId > 0)
                            {
                                isSuccess = objectiveDAO.UpdateObjective(marketObjectiveEntity.Objective);
                            }
                            else
                            {
                                isSuccess = objectiveDAO.InsertObjective(out objectiveID, marketObjectiveEntity.Objective);
                                if (objectiveID > 0)
                                {
                                    marketObjectiveEntity.Objective.ObjectiveId = objectiveID;
                                    isSuccess = MarketObjectiveDataService.InsertMarketObjective(marketObjectiveEntity);
                                }
                            }
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }
    }
}
