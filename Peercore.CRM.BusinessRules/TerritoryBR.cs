﻿using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.CRM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.BusinessRules
{
    public class TerritoryBR
    {
        private static TerritoryBR instance = null;
        private static object syncLock = new Object();

        private TerritoryDAO TerritoryDataService;

        public static TerritoryBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new TerritoryBR();
                }
                return instance;
            }
        }

        private TerritoryBR()
        {
            TerritoryDataService = new TerritoryDAO();
        }

        #region "Territory"

        public List<TerritoryModel> GetAllTerritory(ArgsModel args)
        {
            try
            {
                return TerritoryDataService.GetAllTerritory(args);
            }
            catch
            {
                throw;
            }
        }

        public List<TerritoryModel> GetAllTerritoryByTerritoryCode(ArgsModel args, string TerritoryCode)
        {
            try
            {
                return TerritoryDataService.GetAllTerritoryByTerritoryCode(args, TerritoryCode);
            }
            catch
            {
                throw;
            }
        }      

        public List<TerritoryModel> GetAllTerritoryByTerritoryName(ArgsModel args, string TerritoryName)
        {
            try
            {
                return TerritoryDataService.GetAllTerritoryByTerritoryName(args, TerritoryName);
            }
            catch
            {
                throw;
            }
        }        
        
        public List<TerritoryModel> GetAllTerritoryByOriginator(ArgsModel args, string OriginatorType, string Originator)
        {
            try
            {
                return TerritoryDataService.GetAllTerritoryByOriginator(args, OriginatorType, Originator);
            }
            catch
            {
                throw;
            }
        }
        
        public List<TerritoryModel> GetAllTerritoryByDistributerId(ArgsModel args, string DistributerId)
        {
            try
            {
                return TerritoryDataService.GetAllTerritoryByDistributerId(args, DistributerId);
            }
            catch
            {
                throw;
            }
        }
        
        public List<TerritoryModel> GetAllTerritoryByDiscountScheme(ArgsModel args, string ShemeHeaderId)
        {
            try
            {
                return TerritoryDataService.GetAllTerritoryByDiscountScheme(args, ShemeHeaderId);
            }
            catch
            {
                throw;
            }
        }

        public List<TerritoryModel> GetAllTerritoryByAreaId(ArgsModel args, string AreaId)
        {
            try
            {
                return TerritoryDataService.GetAllTerritoryByAreaId(args, AreaId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteTerritory(string territory_id)
        {
            try
            {
                return TerritoryDataService.DeleteTerritory(territory_id);
            }
            catch
            {
                throw;
            }
        }
        
        public bool DeleteTerritoryFromAreaByTerritoryId(string territory_id)
        {
            try
            {
                return TerritoryDataService.DeleteTerritoryFromAreaByTerritoryId(territory_id);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveTerritory(string area_id, string territory_id, string territory_code, string territory_name, string created_by)
        {
            try
            {
                return TerritoryDataService.SaveTerritory(area_id,
                    territory_id,
                    territory_code,
                    territory_name,
                    created_by);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateTerritoryCanAddOutlet(string territory_id, bool can_add_outlet)
        {
            try
            {
                return TerritoryDataService.UpdateTerritoryCanAddOutlet(territory_id, can_add_outlet);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateTerritoryGeoFence(string territory_id, bool is_geofence)
        {
            try
            {
                return TerritoryDataService.UpdateTerritoryGeoFence(territory_id, is_geofence);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool IsTerritoryAssigned(string territoryId, out string RepCode)
        {
            try
            {
                return TerritoryDataService.IsTerritoryAssigned(territoryId, out RepCode);
            }
            catch
            {
                throw;
            }
        }
        
        public bool DeleteConfirmSRFromTerritory(string territoryId, string repId)
        {
            try
            {
                return TerritoryDataService.DeleteConfirmSRFromTerritory(territoryId, repId);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        public List<DistributerTerritoryModel> GetAllTerritoriesForDistributer(ArgsModel args, string OriginatorType, string Originator)
        {
            try
            {
                return TerritoryDataService.GetAllTerritoriesForDistributer(args, OriginatorType, Originator);
            }
            catch
            {
                throw;
            }
        }
    }
}
