﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class SchemeDetailsBR
    {
        private static SchemeDetailsBR instance = null;
        private static object syncLock = new Object();

        private SchemeDetailsDAO SchemeDetailsDataService;

        public static SchemeDetailsBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new SchemeDetailsBR();
                }
                return instance;
            }
        }

        private SchemeDetailsBR()
        {
            SchemeDetailsDataService = new SchemeDetailsDAO();
        }

        #region - Init Object -

        public SchemeDetailsEntity CreateObject()
        {
            return SchemeDetailsDataService.CreateObject();
        }

        public SchemeDetailsEntity CreateObject(int entityId)
        {
            return SchemeDetailsDataService.CreateObject(entityId);
        }

        #endregion

        public bool UpdateSchemeDetails(SchemeDetailsEntity schemeDetails)
        {
            try
            {
                return SchemeDetailsDataService.UpdateSchemeDetails(schemeDetails);
            }
            catch
            {
                throw;
            }
        }

        public bool InsertSchemeDetails(out int schemeDetailId, SchemeDetailsEntity schemeDetails)
        {
            try
            {
                return SchemeDetailsDataService.InsertSchemeDetails(out schemeDetailId, schemeDetails);
            }
            catch
            {
                throw;
            }
        }

        public List<SchemeDetailsEntity> GetSchemeDetailsBySchemeHeaderId(ArgsEntity args , int schemeHeaderId)
        {
            try
            {
                return SchemeDetailsDataService.GetSchemeDetailsBySchemeHeaderId(args , schemeHeaderId);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateSchemeDetailsInActive(int schemeHeaderId)
        {
            try
            {
                return SchemeDetailsDataService.UpdateSchemeDetailsInActive(schemeHeaderId);
            }
            catch
            {
                throw;
            }
        }

        public List<SchemeDetailsEntity> GetSchemeDetailsByDivisionId(int divisionId)
        {
            try
            {
                return SchemeDetailsDataService.GetSchemeDetailsByDivisionId(divisionId);
            }
            catch
            {
                throw;
            }
        }
    }
}
