﻿using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.CRM.Model;
using Peercore.Workflow.Common;
using System;
using System.Collections.Generic;

namespace Peercore.CRM.BusinessRules
{
    public class OrderHeaderBR
    {
        private static OrderHeaderBR instance = null;
        private static object syncLock = new Object();

        private OrderHeaderDAO OrderHeaderDataService;

        public static OrderHeaderBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new OrderHeaderBR();
                }
                return instance;
            }
        }

        private OrderHeaderBR()
        {
            OrderHeaderDataService = new OrderHeaderDAO();
        }

        public bool InsertOrder(string accessToken, OrderModel orderHeader)
        {
            bool isSuccess = false;

            DbWorkflowScope transactionScope = OrderHeaderDataService.WorkflowScope;

            int orderId = 0;

            try
            {
                using (transactionScope)
                {
                    try
                    {
                        OrderHeaderDAO orderHeaderService = new OrderHeaderDAO(transactionScope);
                        OrderDetailsDAO orderDetailsService = new OrderDetailsDAO(transactionScope);
                        OrderSchemeDAO orderSchemeService = new OrderSchemeDAO(transactionScope);
                        CustomerDAO customerService = new CustomerDAO(transactionScope);

                        OriginatorDAO originatorService = new OriginatorDAO();

                        OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);

                        if (originator == null)
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        if (orderHeader.OrderNo == null)
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        if (orderHeader.OrderDate == null)
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        if (orderHeader.CustomerCode != null)
                        {
                            if (orderHeader.CustomerCode.Contains("TEMP"))
                            {
                                orderHeader.CustomerCode = customerService.GetCustomerCodeByTempCustCode(orderHeader.CustomerCode);
                            }
                        }
                        else
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        //Insert new Order 
                        isSuccess = orderHeaderService.InsertOrderHeader(out orderId, orderHeader);

                        if (isSuccess)
                        {
                            orderDetailsService.DeleteOrderDetailsByOrderId(orderId);
                            orderSchemeService.DeleteOrderDiscountSchemeByOrderId(orderId);

                            if (isSuccess)
                            {
                                if (orderHeader.OrderLineItemCollection.Count == 0)
                                {
                                    transactionScope.Rollback();
                                    return false;
                                }
                                else
                                {
                                    foreach (var item in orderHeader.OrderLineItemCollection)
                                    {
                                        item.OrderId = orderId;
                                        if (item.ProductID == 0)
                                        {
                                            transactionScope.Rollback();
                                            return false;
                                        }

                                        isSuccess = orderDetailsService.InsertOrderDetails(item);
                                        if (!isSuccess)
                                        {
                                            transactionScope.Rollback();
                                            return false;
                                        }
                                    }

                                    if (isSuccess)
                                    {
                                        foreach (var item in orderHeader.DiscountSchemeGroupCollection)
                                        {
                                            item.OrderId = orderId;
                                            int schemeGroupId = 0;

                                            isSuccess = orderSchemeService.InsertOrderDiscountSchemeGroup(out schemeGroupId, item);
                                            if (!isSuccess)
                                            {
                                                transactionScope.Rollback();
                                                return false;
                                            }

                                            foreach (var orderScheme in item.DiscountSchemeCollection)
                                            {
                                                orderScheme.OrderId = orderId;
                                                orderScheme.SchemeGroupId = schemeGroupId;

                                                isSuccess = orderSchemeService.InsertOrderDiscountScheme(orderScheme);
                                                if (!isSuccess)
                                                {
                                                    transactionScope.Rollback();
                                                    return false;
                                                }
                                            }
                                        }
                                    }
                                }

                                try
                                {
                                    isSuccess = orderHeaderService.InsertOrderProductDiscount(orderId);
                                    if (!isSuccess)
                                    {
                                        transactionScope.Rollback();
                                        return false;
                                    }
                                }
                                catch
                                {
                                    transactionScope.Rollback();
                                    return false;
                                }
                            }
                        }
                    }
                    catch(Exception ex)
                    {
                        transactionScope.Rollback();
                        return false;
                    }

                    if (isSuccess)
                    {
                        transactionScope.Commit();
                    }
                    else
                    {
                        transactionScope.Rollback();
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return isSuccess;
        }

        public bool UpdateOrder(string accessToken, OrderModel orderHeader)
        {
            bool isSuccess = false;

            DbWorkflowScope transactionScope = OrderHeaderDataService.WorkflowScope;

            try
            {
                using (transactionScope)
                {
                    try
                    {
                        OrderHeaderDAO orderHeaderService = new OrderHeaderDAO(transactionScope);
                        OrderDetailsDAO orderDetailsService = new OrderDetailsDAO(transactionScope);
                        OrderSchemeDAO orderSchemeService = new OrderSchemeDAO(transactionScope);
                        CustomerDAO customerService = new CustomerDAO(transactionScope);

                        OriginatorDAO originatorService = new OriginatorDAO();

                        OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);

                        if (originator == null)
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        if (orderHeader.OrderNo == null)
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        if (orderHeader.OrderDate == null)
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        if (orderHeader.CustomerCode != null)
                        {
                            if (orderHeader.CustomerCode.Contains("TEMP"))
                            {
                                orderHeader.CustomerCode = customerService.GetCustomerCodeByTempCustCode(orderHeader.CustomerCode);
                            }
                        }
                        else
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        //Insert new Order 
                        isSuccess = orderHeaderService.UpdateOrderHeader(orderHeader);

                        if (isSuccess)
                        {
                            orderDetailsService.DeleteOrderDetailsByOrderId(orderHeader.OrderId);
                            orderSchemeService.DeleteOrderDiscountSchemeByOrderId(orderHeader.OrderId);

                            if (isSuccess)
                            {
                                if (orderHeader.OrderLineItemCollection.Count == 0)
                                {
                                    transactionScope.Rollback();
                                    return false;
                                }
                                else
                                {
                                    foreach (var item in orderHeader.OrderLineItemCollection)
                                    {
                                        item.OrderId = orderHeader.OrderId;
                                        if (item.ProductID == 0)
                                        {
                                            transactionScope.Rollback();
                                            return false;
                                        }

                                        isSuccess = orderDetailsService.InsertOrderDetails(item);
                                        if (!isSuccess)
                                        {
                                            transactionScope.Rollback();
                                            return false;
                                        }
                                    }

                                    if (isSuccess)
                                    {
                                        foreach (var item in orderHeader.DiscountSchemeGroupCollection)
                                        {
                                            item.OrderId = orderHeader.OrderId;
                                            int schemeGroupId = 0;

                                            isSuccess = orderSchemeService.InsertOrderDiscountSchemeGroup(out schemeGroupId, item);
                                            if (!isSuccess)
                                            {
                                                transactionScope.Rollback();
                                                return false;
                                            }

                                            foreach (var orderScheme in item.DiscountSchemeCollection)
                                            {
                                                orderScheme.OrderId = orderHeader.OrderId;
                                                orderScheme.SchemeGroupId = schemeGroupId;

                                                isSuccess = orderSchemeService.InsertOrderDiscountScheme(orderScheme);
                                                if (!isSuccess)
                                                {
                                                    transactionScope.Rollback();
                                                    return false;
                                                }
                                            }
                                        }
                                    }
                                }

                                try
                                {
                                    isSuccess = orderHeaderService.InsertOrderProductDiscount(orderHeader.OrderId);
                                    if (!isSuccess)
                                    {
                                        transactionScope.Rollback();
                                        return false;
                                    }
                                }
                                catch
                                {
                                    transactionScope.Rollback();
                                    return false;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        transactionScope.Rollback();
                        return false;
                    }

                    if (isSuccess)
                    {
                        transactionScope.Commit();
                    }
                    else
                    {
                        transactionScope.Rollback();
                    }
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return isSuccess;
        }

        public List<OrderModel> GetOrdersByAccessToken(string accessToken)
        {
            List<OrderModel> orderList = new List<OrderModel>();

            try
            {
                OrderDetailsDAO OrderDetailService = new OrderDetailsDAO();
                OrderSchemeDAO OrderSchemeService = new OrderSchemeDAO();

                orderList = OrderHeaderDataService.GetOrdersByAccessToken(accessToken);

                foreach (var item in orderList)
                {
                    item.OrderLineItemCollection = OrderDetailService.GetOrderDetailsByOrderId(item.OrderId,"N");
                    item.DiscountSchemeGroupCollection = OrderSchemeService.GetOrderSchemeGroupByOrderId(item.OrderId);
                }
            }
            catch
            {
                throw;
            }

            return orderList;
        }

        public List<OrderModel> GetAllOrders(ArgsModel args, string Originator, string status)
        {
            List<OrderModel> orderList = new List<OrderModel>();

            try
            {
                OrderDetailsDAO OrderDetailService = new OrderDetailsDAO();
                OrderSchemeDAO OrderSchemeService = new OrderSchemeDAO();

                orderList = OrderHeaderDataService.GetAllOrders(args, Originator, status);

                foreach (var item in orderList)
                {
                    item.OrderLineItemCollection = OrderDetailService.GetOrderDetailsByOrderId(item.OrderId, args.SQuantitytype);
                    item.DiscountSchemeGroupCollection = OrderSchemeService.GetOrderSchemeGroupByOrderId(item.OrderId);
                }
            }
            catch
            {
                return orderList;
            }

            return orderList;
        }

        public OrderModel GetOrdersByOrderId(int OrderId)
        {
            OrderModel order = new OrderModel();

            try
            {
                order = OrderHeaderDataService.GetOrderHeaderByOrderId(OrderId);
            }
            catch
            {
                throw;
            }

            return order;
        }
        
        public OrderModel GetOrdersByOrderNo(string OrderNo)
        {
            OrderModel order = new OrderModel();

            try
            {
                order = OrderHeaderDataService.GetOrderHeaderByOrderNo(OrderNo);
            }
            catch
            {
                throw;
            }

            return order;
        }

        public bool DeleteOrderHeader(OriginatorEntity originator, string accessToken, string orderNo)
        {
            DbWorkflowScope transactionScope = OrderHeaderDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    try
                    {
                        OrderHeaderDAO orderHeaderService = new OrderHeaderDAO(transactionScope);

                        //OriginatorDAO originatorService = new OriginatorDAO();

                        //OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);
                        if (originator == null)
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        OrderModel orderHeader = new OrderModel();
                        orderHeader = orderHeaderService.GetOrderHeaderByOrderNo(orderNo);

                        if (orderHeader.OrderId > 0)
                        {
                            //Delete Invoice Header
                            orderHeader.CreateBy = originator.Originator;
                            isSuccess = orderHeaderService.DeleteOrderHeader(orderHeader);
                        }
                    }
                    catch (Exception ex)
                    {
                        transactionScope.Rollback();
                        throw;
                    }
                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool DeleteOrderHeaderByOrderNo(string originator, string orderNo)
        {
            try
            {
                return OrderHeaderDataService.DeleteOrderHeaderByOrderNo(originator, orderNo);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateOrderIsInvoiceActivate(OrderIsInvoiceActivate request)
        {
            DbWorkflowScope transactionScope = OrderHeaderDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    try
                    {
                        OrderHeaderDAO orderHeaderService = new OrderHeaderDAO(transactionScope);

                        isSuccess = orderHeaderService.UpdateOrderIsInvoiceActivate(request);
                    }
                    catch (Exception ex)
                    {
                        transactionScope.Rollback();
                        throw;
                    }
                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool DeleteOrderHeaderById(OriginatorEntity originator, string accessToken, int orderId)
        {
            DbWorkflowScope transactionScope = OrderHeaderDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    try
                    {
                        OrderHeaderDAO orderHeaderService = new OrderHeaderDAO(transactionScope);

                        //OriginatorDAO originatorService = new OriginatorDAO();

                        //OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);
                        if (originator == null)
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        OrderModel orderHeader = new OrderModel();
                        orderHeader = orderHeaderService.GetOrderHeaderByOrderId(orderId);

                        if (orderHeader.OrderId > 0)
                        {
                            //Delete Invoice Header
                            orderHeader.CreateBy = originator.Originator;
                            isSuccess = orderHeaderService.DeleteOrderHeader(orderHeader);
                        }
                    }
                    catch (Exception ex)
                    {
                        transactionScope.Rollback();
                        throw;
                    }
                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool ArchiveOrderHeader(OriginatorEntity originator, string accessToken, int orderId)
        {
            DbWorkflowScope transactionScope = OrderHeaderDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    try
                    {
                        OrderHeaderDAO orderHeaderService = new OrderHeaderDAO(transactionScope);

                        //OriginatorDAO originatorService = new OriginatorDAO();

                        //OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);
                        if (originator == null)
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        OrderModel orderHeader = new OrderModel();
                        orderHeader = orderHeaderService.GetOrderHeaderByOrderId(orderId);

                        if (orderHeader.OrderId > 0)
                        {
                            //Delete Invoice Header
                            orderHeader.CreateBy = originator.Originator;
                            isSuccess = orderHeaderService.ArchiveOrderHeader(orderHeader);
                        }
                    }
                    catch (Exception ex)
                    {
                        transactionScope.Rollback();
                        throw;
                    }
                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        //public bool DeleteOrderByOrderNo(string accessToken, string orderNo)
        //{
        //    DbWorkflowScope transactionScope = InvoiceHeaderDataService.WorkflowScope;
        //    bool isSuccess = true;
        //    try
        //    {
        //        using (transactionScope)
        //        {
        //            try
        //            {
        //                OrderHeaderDAO orderHeaderService = new OrderHeaderDAO(transactionScope);
        //                OriginatorDAO originatorService = new OriginatorDAO();

        //                OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);

        //                if (originator == null)
        //                {
        //                    transactionScope.Rollback();
        //                    return false;
        //                }

        //                OrderModel orderHeader = new OrderModel();
        //                orderHeader = orderHeaderService.GetOrderHeaderByOrderNo(orderNo);

        //                if (invoiceHeader.IvceId > 0)
        //                {
        //                    //Delete Invoice Header
        //                    invoiceHeader.LastModifiedBy = originator.Originator;
        //                    isSuccess = invoiceHeaderService.DeleteInvoiceHeader(invoiceHeader);
        //                    invoiceHeaderService.UpdateOutstandingForInvoice(invoiceHeader.IvceId);
        //                }
        //            }
        //            catch
        //            {
        //                transactionScope.Rollback();
        //                throw;
        //            }
        //            if (isSuccess)
        //                transactionScope.Commit();
        //            else
        //                transactionScope.Rollback();
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return isSuccess;
        //}
















        //public List<InvoiceHeaderEntity> GetOutstandingInvoicesForCustomer(string custCode, DateTime fromDate, DateTime toDate)
        //{
        //    try
        //    {
        //        return InvoiceHeaderDataService.GetOutstandingInvoicesForCustomer(custCode, fromDate, toDate);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //public List<InvoiceHeaderEntity> GetInvoicesByAccessToken(string accessToken)
        //{
        //    List<InvoiceHeaderEntity> InvoiceHeaderList = new List<InvoiceHeaderEntity>();

        //    try
        //    {
        //        InvoiceDetailsDAO InvoiceDetailService = new InvoiceDetailsDAO();
        //        InvoiceSchemeDAO InvoiceSchemeService = new InvoiceSchemeDAO();

        //        InvoiceHeaderList = InvoiceHeaderDataService.GetInvoicesByAccessToken(accessToken);

        //        foreach (var item in InvoiceHeaderList)
        //        {
        //            item.InvoiceDetailList = InvoiceDetailService.GetInvoiceDetailsByInvoiceId(item.IvceId);
        //            item.InvoiceSchemeGroupList = InvoiceSchemeService.GetInvoiceSchemeGroupByInvoiceId(item.IvceId);
        //            item.InvoiceSettlementList = InvoiceHeaderDataService.GetInvoiceSettlementListByInvoiceId(item.IvceId);

        //            if (item.InvoiceSettlementList != null)
        //            {
        //                foreach (InvoiceSettlementEntity invSetEntity in item.InvoiceSettlementList)
        //                {
        //                    invSetEntity.InvoiceSettlementDetailsList =
        //                        InvoiceHeaderDataService.GetInvoiceSettlementDetailsBySettlementIdAndInvoiceId(invSetEntity.InvoiceSettlementId, item.IvceId);

        //                    invSetEntity.InvoiceSettlementChequeList =
        //                        InvoiceHeaderDataService.GetInvoiceSettlementChequeBySettlementId(invSetEntity.InvoiceSettlementId);
        //                }
        //            }

        //            ////This is for Return Invoices 
        //            ////UnComment for Perfetti needs returns
        //            //item.ReturnInvoiceList = InvoiceHeaderDataService.GetReturnInvoiceByInvoiceId(item.IvceId);

        //            //if (item.ReturnInvoiceList != null)
        //            //{
        //            //    foreach (ReturnInvoiceEntity invSetEntity in item.ReturnInvoiceList)
        //            //    {
        //            //        invSetEntity.ReturnInvoiceLineList =
        //            //            InvoiceDetailService.GetReturnInvoiceLinesByReturnIdAndInvoiceId(invSetEntity.Id, item.IvceId);
        //            //
        //            //        invSetEntity.InvoiceDetailList =
        //            //            InvoiceDetailService.GetReturnInvoiceDetailsByReturnIdAndInvoiceId(invSetEntity.Id, item.IvceId);
        //            //    }
        //            //}
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }

        //    return InvoiceHeaderList;
        //}

        //public List<InvoiceHeaderEntity> GetInvoicesByAccessTokenforASM(string accessToken, string date)
        //{
        //    List<InvoiceHeaderEntity> InvoiceHeaderList = new List<InvoiceHeaderEntity>();
        //    try
        //    {
        //        InvoiceDetailsDAO InvoiceDetailService = new InvoiceDetailsDAO();
        //        InvoiceSchemeDAO InvoiceSchemeService = new InvoiceSchemeDAO();

        //        InvoiceHeaderList = InvoiceHeaderDataService.GetInvoicesByAccessTokenForASM(accessToken, date);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return InvoiceHeaderList;
        //}

        //public List<RepsTrackingDetailsEntity> RepsTrackingDetailsforASM(string accessToken, string date)
        //{
        //    List<RepsTrackingDetailsEntity> InvoiceHeaderList = new List<RepsTrackingDetailsEntity>();
        //    try
        //    {
        //        InvoiceHeaderList = InvoiceHeaderDataService.RepsTrackingDetailsforASM(accessToken, date);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return InvoiceHeaderList;
        //}


        //public List<InvoiceHeaderEntity> GetAllInvoicesDataAndCount(ArgsEntity args)
        //{
        //    List<InvoiceHeaderEntity> InvoiceHeaderList = new List<InvoiceHeaderEntity>();
        //    try
        //    {
        //        InvoiceDetailsDAO InvoiceDetailService = new InvoiceDetailsDAO();
        //        InvoiceSchemeDAO InvoiceSchemeService = new InvoiceSchemeDAO();

        //        InvoiceHeaderList = InvoiceHeaderDataService.GetAllInvoicesDataAndCount(args);
        //        foreach (var item in InvoiceHeaderList)
        //        {
        //            item.InvoiceDetailList = InvoiceDetailService.GetInvoiceDetailsByInvoiceId(item.IvceId);
        //            item.InvoiceSchemeGroupList = InvoiceSchemeService.GetInvoiceSchemeTotDiscountByInvoiceId(item.IvceId);

        //            foreach (var schemeGroup in item.InvoiceSchemeGroupList)
        //            {
        //                schemeGroup.InvoiceSchemeList = InvoiceSchemeService.GetDiscountSchemeByInvoiceId(schemeGroup.SchemeGroupId);
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return InvoiceHeaderList;
        //}

        //public bool DeleteInvoiceHeader(string accessToken, string invoiceNo)
        //{
        //    DbWorkflowScope transactionScope = InvoiceHeaderDataService.WorkflowScope;
        //    bool isSuccess = true;
        //    try
        //    {
        //        using (transactionScope)
        //        {
        //            try
        //            {
        //                InvoiceHeaderDAO invoiceHeaderService = new InvoiceHeaderDAO(transactionScope);

        //                OriginatorDAO originatorService = new OriginatorDAO();

        //                OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);
        //                if (originator == null)
        //                {
        //                    transactionScope.Rollback();
        //                    return false;
        //                }

        //                InvoiceHeaderEntity invoiceHeader = new InvoiceHeaderEntity();
        //                invoiceHeader = invoiceHeaderService.GetInvoiceHeaderByInvoiceNo(invoiceNo);

        //                if (invoiceHeader.IvceId > 0)
        //                {
        //                    //Delete Invoice Header
        //                    invoiceHeader.LastModifiedBy = originator.Originator;
        //                    isSuccess = invoiceHeaderService.DeleteInvoiceHeader(invoiceHeader);
        //                    invoiceHeaderService.UpdateOutstandingForDeleteInvoice(invoiceHeader.IvceId);
        //                }
        //            }
        //            catch
        //            {
        //                transactionScope.Rollback();
        //                throw;
        //            }
        //            if (isSuccess)
        //                transactionScope.Commit();
        //            else
        //                transactionScope.Rollback();
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return isSuccess;
        //}

        //public bool DeleteInvoiceHeaderForAddInvoice(string accessToken, string invoiceNo)
        //{
        //    DbWorkflowScope transactionScope = InvoiceHeaderDataService.WorkflowScope;
        //    bool isSuccess = true;
        //    try
        //    {
        //        using (transactionScope)
        //        {
        //            try
        //            {
        //                InvoiceHeaderDAO invoiceHeaderService = new InvoiceHeaderDAO(transactionScope);

        //                OriginatorDAO originatorService = new OriginatorDAO();

        //                OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);
        //                if (originator == null)
        //                {
        //                    transactionScope.Rollback();
        //                    return false;
        //                }

        //                InvoiceHeaderEntity invoiceHeader = new InvoiceHeaderEntity();
        //                invoiceHeader = invoiceHeaderService.GetInvoiceHeaderByInvoiceNo(invoiceNo);

        //                if (invoiceHeader.IvceId > 0)
        //                {
        //                    //Delete Invoice Header
        //                    invoiceHeader.LastModifiedBy = originator.Originator;
        //                    isSuccess = invoiceHeaderService.DeleteInvoiceHeader(invoiceHeader);
        //                    invoiceHeaderService.UpdateOutstandingForInvoice(invoiceHeader.IvceId);
        //                }
        //            }
        //            catch
        //            {
        //                transactionScope.Rollback();
        //                throw;
        //            }
        //            if (isSuccess)
        //                transactionScope.Commit();
        //            else
        //                transactionScope.Rollback();
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return isSuccess;
        //}

        //public bool DeleteInvoiceByInvID(InvoiceHeaderEntity tmeInvoiceHeader)
        //{
        //    DbWorkflowScope transactionScope = InvoiceHeaderDataService.WorkflowScope;
        //    bool successHeader = false;
        //    bool successDetails = false;

        //    try
        //    {
        //        using (transactionScope)
        //        {
        //            InvoiceHeaderDAO invHeaderDataService = new InvoiceHeaderDAO(transactionScope);
        //            InvoiceDetailsDAO invDetailsDataService = new InvoiceDetailsDAO(transactionScope);

        //            successHeader = invHeaderDataService.DeleteInvoiceHeader(tmeInvoiceHeader);

        //            if (successHeader)
        //            {
        //                successDetails = invDetailsDataService.DeleteInvoiceDetailsPhysical(tmeInvoiceHeader.IvceId);
        //                invHeaderDataService.UpdateOutstandingForDeleteInvoice(tmeInvoiceHeader.IvceId);
        //            }

        //            if (successDetails)
        //                transactionScope.Commit();
        //            else
        //                transactionScope.Rollback();
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }

        //    return successDetails;
        //}

        public bool DeleteBulkOrders(string userType, string userName, string ordList)
        {
            try
            {
                return OrderHeaderDataService.DeleteBulkOrders(userType, userName, ordList);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<RepsSKUMasterEntity> GetRepsSKUWiseOrderDetailsByAccessToken(string accessToken, string date)
        {
            List<RepsSKUMasterEntity> headerList = new List<RepsSKUMasterEntity>();

            try
            {
                headerList = OrderHeaderDataService.GetAllRepsSKUWiseDetails(accessToken, date);
            }
            catch
            {
                throw;
            }

            return headerList;
        }

        public List<RepsOutletWiseDetailsEntity> GetRepsOutletWiseOrderDetailsByAccessToken(string accessToken, string date)
        {
            List<RepsOutletWiseDetailsEntity> headerList = new List<RepsOutletWiseDetailsEntity>();

            try
            {
                headerList = OrderHeaderDataService.GetAllRepsOutletWiseOrderDetails(accessToken, date);
            }
            catch
            {
                throw;
            }

            return headerList;
        }


    }
}
