﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class VisitNoteBR
    {
        private static VisitNoteBR instance = null;
        private static object syncLock = new Object();

        private VisitNoteDAO VisitNoteDataService;

        public static VisitNoteBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new VisitNoteBR();
                }
                return instance;
            }
        }

        private VisitNoteBR()
        {
            VisitNoteDataService = new VisitNoteDAO();
        }

        #region - Init Object -

        public VisitNoteEntity CreateObject()
        {
            return VisitNoteDataService.CreateObject();
        }
        
        public VisitNoteEntity CreateObject(int entityId)
        {
            return VisitNoteDataService.CreateObject(entityId);
        }

        #endregion

        public List<VisitNoteEntity> GetVisitChecklist(int routeMasterID)
        {
            try
            {
                return VisitNoteDataService.GetVisitNote(routeMasterID); 
            }
            catch
            {
                throw;
            }
        }

        public List<VisitNoteEntity> GetVisitNotesByAccessTokenforASM(string accessToken, string date)
        {
            List<VisitNoteEntity> InvoiceHeaderList = new List<VisitNoteEntity>();
            try
            {
                VisitNoteDAO InvoiceDetailService = new VisitNoteDAO();
                InvoiceSchemeDAO InvoiceSchemeService = new InvoiceSchemeDAO();

                InvoiceHeaderList = InvoiceDetailService.GetVisitNotesByAccessTokenforASM(accessToken, date);
            }
            catch
            {
                throw;
            }
            return InvoiceHeaderList;
        }
        
        public List<VisitNoteEntity> GetVisitNotesByAccessToken(string accessToken)
        {
            List<VisitNoteEntity> InvoiceHeaderList = new List<VisitNoteEntity>();
            try
            {
                VisitNoteDAO InvoiceDetailService = new VisitNoteDAO();
                InvoiceSchemeDAO InvoiceSchemeService = new InvoiceSchemeDAO();

                InvoiceHeaderList = InvoiceDetailService.GetVisitNotesByAccessToken(accessToken);
            }
            catch
            {
                throw;
            }
            return InvoiceHeaderList;
        }

        //public bool SaveVisitNote(VisitNoteEntity visitNoteEntity)
        //{
        //    bool isSuccess = false;
        //    try
        //    {
        //        CustomerEntity customerEntity = new CustomerEntity();
        //        customerEntity.CustomerCode = visitNoteEntity.CustomerCode;
        //        customerEntity.Remarks = visitNoteEntity.Remarks;
        //        customerEntity.LastModifiedBy = visitNoteEntity.CreatedBy;

        //        isSuccess = CustomerBR.Instance.UpdateCustomerRemarks(customerEntity);

        //        if (isSuccess)
        //        {
        //            isSuccess = VisitNoteDataService.InsertVisitNote(visitNoteEntity);
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return (isSuccess);
        //}

        public bool InsertVisitNote(string accessToken, VisitNoteEntity visitNoteEntity)
        {
            try
            {
                return VisitNoteDataService.InsertVisitNote(accessToken, visitNoteEntity);
            }
            catch
            {
                throw;
            }
        }
        public List<VisitNoteReasonEntity> GetVisitNoteReason()
        {
            try
            {
                return VisitNoteDataService.GetVisitNoteReason();
            }
            catch
            {
                throw;
            }
        }
    }
}
