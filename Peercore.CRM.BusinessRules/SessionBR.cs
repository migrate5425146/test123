﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using System.IO;
using System.Configuration;

namespace Peercore.CRM.BusinessRules
{
    public class SessionBR
    {
        private static SessionBR instance = null;
        private static object syncLock = new Object();

        private SessionDAO SessionDataService;

        public static SessionBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new SessionBR();
                }
                return instance;
            }
        }

        private SessionBR()
        {
            SessionDataService = new SessionDAO();
        }

        #region - Init Object -

        public SessionEntity CreateObject()
        {
            return SessionDataService.CreateObject();
        }

        public SessionEntity CreateObject(int entityId)
        {
            return SessionDataService.CreateObject(entityId);
        }

        #endregion

        public int SaveSession(string userName)
        {
            try
            {
                SessionEntity session = SessionEntity.CreateObject();
                session.UserName = userName;
                session.LoginDateTime = DateTime.Now;
                session.LogoutDateTime = DateTime.Now;
                return SessionDataService.InitiateSession(session);
            }
            catch (Exception)
            {
                throw;
            }
        }

        // Used by Web Application
        public int UpdateLogoutTime(int sessionId)
        {
            try
            {
                SessionEntity session = SessionEntity.CreateObject();
                session.SessionId = sessionId;
                session.LogoutDateTime = DateTime.Now;
                return SessionDataService.UpdateLogoutTime(session);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public void CreateTransactionLogMobile(string accessToken, string transTime, string transType, string transModule, string transDesc, string locLatitude, string locLongitude)
        {
            OriginatorDAO originatorService = new OriginatorDAO();

            try
            {
                OriginatorEntity originatorObj = originatorService.GetOriginatorByAccessToken(accessToken);
                SessionDataService.CreateTransactionLog(originatorObj.Originator, transTime, transType, transModule, transDesc, locLatitude, locLongitude, "mobile");
            }
            catch { }
        }

        public bool CreateTransactionLog(string originator, string transTime, string transType, string transModule, string transDesc)
        {
            try
            {
                SessionDataService.CreateTransactionLog(originator, transTime, transType, transModule, transDesc, "", "", "pc");
            }
            catch { }

            return true;
        }
        
        public void CreateLog(string originator, string desc, string status)
        {
            string filename = DateTime.Now.ToString("yyyyMMdd");
            var path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filePath = ConfigurationManager.AppSettings["CreateLogPath"].ToString() + filename + ".txt";

            try
            {
                // Check if file already exists. If yes, delete it. 
                using (StreamWriter sw = new StreamWriter(filePath, true))
                {
                    string sr = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss:fff tt") + "  " + originator + "  |  " + desc + "  |  " + status + Environment.NewLine;
                    sw.Write(sr);
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }  
        }

        public void CreateLogInvoice(string originator, string desc, string status)
        {
            string filename = DateTime.Now.ToString("yyyyMMdd");
            var path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filePath = ConfigurationManager.AppSettings["CreateLogPath"].ToString() + filename + "_Invoice.txt";

            try
            {
                // Check if file already exists. If yes, delete it. 
                using (StreamWriter sw = new StreamWriter(filePath, true))
                {
                    string sr = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss:fff tt") + "  " + originator + "  |  " + desc + "  |  " + status + Environment.NewLine;
                    sw.Write(sr);
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }

        public void CreateLogOrder(string originator, string desc, string status)
        {
            string filename = DateTime.Now.ToString("yyyyMMdd");
            var path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filePath = ConfigurationManager.AppSettings["CreateLogPath"].ToString() + filename + "_Order.txt";

            try
            {
                // Check if file already exists. If yes, delete it. 
                using (StreamWriter sw = new StreamWriter(filePath, true))
                {
                    string sr = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss:fff tt") + "  " + originator + "  |  " + desc + "  |  " + status + Environment.NewLine;
                    sw.Write(sr);
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }

        public void CreateLogInvoiceSettlement(string originator, string desc, string status)
        {
            string filename = DateTime.Now.ToString("yyyyMMdd");
            var path = System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
            string filePath = ConfigurationManager.AppSettings["CreateLogPath"].ToString() + filename + "_InvoiceSettlement.txt";

            try
            {
                // Check if file already exists. If yes, delete it. 
                using (StreamWriter sw = new StreamWriter(filePath, true))
                {
                    string sr = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss:fff tt") + "  " + originator + "  |  " + desc + "  |  " + status + Environment.NewLine;
                    sw.Write(sr);
                }
            }
            catch (Exception Ex)
            {
                Console.WriteLine(Ex.ToString());
            }
        }
    }
}
