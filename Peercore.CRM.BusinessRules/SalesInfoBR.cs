﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class SalesInfoBR
    {
        private static SalesInfoBR instance = null;
        private static object syncLock = new Object();

        private SalesInfoDAO SalesInfoDataService;

        public static SalesInfoBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new SalesInfoBR();
                }
                return instance;
            }
        }

        private SalesInfoBR()
        {
            SalesInfoDataService = new SalesInfoDAO();
        }

        #region - Init Object -

        //public ActivityEntity CreateObject()
        //{
        //    return ActivityDataService.CreateObject();
        //}

        //public ActivityEntity CreateObject(int entityId)
        //{
        //    return ActivityDataService.CreateObject(entityId);
        //}

        #endregion

       
        public List<AtblEntity> GetTrendData(SalesInfoDetailSearchCriteriaEntity busSalesEnqSrc, ref List<string> lstCostPeriods, string sCode)
        {
            try
            {
                List<AtblEntity> lstTrendData = new List<AtblEntity>();

                //SalesInfoDataService.GetTrendData(ref lstTrendData, ref lstCostPeriods, busSalesEnqSrc, sCode);

                return lstTrendData;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public SalesInfoDetailViewStateEntity GetSalesInfoDetailViewState(SalesInfoDetailSearchCriteriaEntity busSalesEnqSrc,ArgsEntity args,string type ="grid")
        {
            try
            {
                SalesInfoDetailViewStateEntity oSalesInfoDetailViewState = new SalesInfoDetailViewStateEntity();

                //clsReps oclsRep = new clsReps();
                //clsCatalogueGroup oclsCatalogGroup = new clsCatalogueGroup();
                //clsCostPeriods opCostPeriods = new clsCostPeriods();
                //clsState oclsState = new clsState();

                int iCostYear;

                string sEnquiryTitle = "";

                //string sPayMethod = OriginatorBR.Instance.GetPayMethod(busSalesEnqSrc.RepCode);
                //string sUsername = OriginatorBR.Instance.GetUserName(busSalesEnqSrc.RepCode);


                //if (sUsername.Equals(args.Originator))
                //{
                //    if (sPayMethod.Equals("D"))
                //    {
                //        busSalesEnqSrc.cDisplayOption = 'D';
                //        // display_opt = 1;
                //        // callproc get_tbl_totals(screenmode = FALSE);
                //    }
                //    else if (sPayMethod.Equals("T"))
                //    {
                //        busSalesEnqSrc.cDisplayOption = 'T';
                //        // display_opt = 2;
                //        // callproc get_tbl_totals(screenmode = FALSE);
                //    }
                //}

                //if (busSalesEnqSrc.cDisplayOption.Equals('D'))
                //    oSalesInfoDetailViewState.DisplayOptionIndex = 0; // Sales in Dollars
                //else if (busSalesEnqSrc.cDisplayOption.Equals('T'))
                //    oSalesInfoDetailViewState.DisplayOptionIndex = 1; // Sales in Tonnes
                //else
                //    oSalesInfoDetailViewState.DisplayOptionIndex = 2; // Sales in Units

                // 19-09-2011 | Set default to Tonnes after the 2nd test note 
                oSalesInfoDetailViewState.DisplayOptionIndex = 1;
                oSalesInfoDetailViewState.SortFieldOptionIndex = 3;

                //if (busSalesEnqSrc.sLastFlag.Equals("n"))
                //{
                //    // year_lbl = 'This Year';
                //    busSalesEnqSrc.sLcSumTable = "infoset_summary";
                //    iCostYear = SalesBR.Instance.GetCurrentCostPeriod().Year;
                //}
                //else
                //{
                //    // year_lbl = 'Last Year';
                //    busSalesEnqSrc.sLcSumTable = "infoset_summary_last";
                //    iCostYear = SalesBR.Instance.GetCurrentCostPeriod().Year - 1;
                //}

                oSalesInfoDetailViewState.sBrand = busSalesEnqSrc.Brand;
                //oSalesInfoDetailViewState.sBusArea = busSalesEnqSrc.BusinessArea;
                //oSalesInfoDetailViewState.sCatalogueGroup = busSalesEnqSrc.CategoryGroup;
                //oSalesInfoDetailViewState.sCatalogueSubGroup = busSalesEnqSrc.CategorySubGroup;
                oSalesInfoDetailViewState.sCustomer = busSalesEnqSrc.CustomerDescription;
                //oSalesInfoDetailViewState.sCustomerGroup = busSalesEnqSrc.CustomerGroup;
                oSalesInfoDetailViewState.sMarket = busSalesEnqSrc.Market;
                oSalesInfoDetailViewState.sMonth = busSalesEnqSrc.Month;
                //oSalesInfoDetailViewState.sProduct = busSalesEnqSrc.CatalogDescripion;
                //oSalesInfoDetailViewState.sProductType = SalesBR.Instance.GetDescription(busSalesEnqSrc.CatalogType, "CT");
                oSalesInfoDetailViewState.sRep = busSalesEnqSrc.RepDescription;
                //oSalesInfoDetailViewState.sState = StateBR.Instance.GetState(args.Sector, busSalesEnqSrc.State).StateAbbri;
                //oSalesInfoDetailViewState.sSubParent = busSalesEnqSrc.SubParentCustomerDescription;

                switch (busSalesEnqSrc.DetailType)
                {
                    case "market":
                        sEnquiryTitle = "Market";
                        oSalesInfoDetailViewState.sSubDetailType = "brand";
                        break;
                    case "brand":
                        sEnquiryTitle = "Brand";
                        oSalesInfoDetailViewState.sSubDetailType = "product";
                        break;
                    case "product":
                        sEnquiryTitle = "Product";
                        oSalesInfoDetailViewState.sSubDetailType = "customer";
                        break;
                    case "customer":
                        //sEnquiryTitle = "Customer";
                        sEnquiryTitle = "Distributor";
                        oSalesInfoDetailViewState.sSubDetailType = "product";
                        break;
                    case "rep":
                        sEnquiryTitle = "Sales Rep.";
                        oSalesInfoDetailViewState.sSubDetailType = "customer";
                        break;
                    default:
                        break;
                }

                //if (type == "grid")
                //{
                //    List<SalesInfoEntity> SalesInfoList = SalesInfoDataService.GetSalesInfoDetails(busSalesEnqSrc, args);
                //    oSalesInfoDetailViewState.LstSalesInfoDetailGrid = SalesInfoList;
                //    oSalesInfoDetailViewState.LstCustomizedTableGrid = GetCustomizedTableForGrid(SalesInfoList, sEnquiryTitle, busSalesEnqSrc.DetailType, iCostYear, busSalesEnqSrc.SortField);
                //}
                //else if (type == "chart")
                //{
                //    oSalesInfoDetailViewState.lstPieChartValues = SalesInfoDataService.GetSalesInfoPieChart(busSalesEnqSrc, args);
                //}
                
                

                return oSalesInfoDetailViewState;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CustomizedTableEntity> GetCustomizedTableForGrid(List<SalesInfoEntity> dtOriginalTable, string sEnquiryTitle, string sDetailType, int iCostYear, string sSortField)
        {
            try
            {
                bool IsProduct = (sDetailType == "product");

                //DataTable dtCustomizedTable = new DataTable();

                List<CustomizedTableEntity> lstCustomizedTable = new List<CustomizedTableEntity>();


                //clsState oclsState = new clsState();

                foreach (SalesInfoEntity item in dtOriginalTable)
                {
                    CustomizedTableEntity customizedTableEntity = new CustomizedTableEntity();
                   // DataRow dNewRow = dtCustomizedTable.NewRow();
                    //object[] oRowItems = item.ItemArray;

                    if (sDetailType.Equals("state"))
                    {
                        string sStateAbbri = StateBR.Instance.GetStateByCode(item.Code).StateAbbri;//oclsState.GetState(Global.clsGlobal.GetInstance().gCountry, Convert.ToString(oRowItems[0])).StateAbbri.Trim();

                        if (string.IsNullOrEmpty(sStateAbbri))
                            continue;
                        else
                        {
                            item.StateCode = sStateAbbri;
                            item.MD = sStateAbbri;
                        }
                            //oRowItems[1] = sStateAbbri;
                    }
                    else if (sDetailType.Equals("custgroup"))
                    {
                        if (string.IsNullOrEmpty(item.CatGroup.Trim()))
                            continue;
                    }


                    double dConversion = 1;
                    if (IsProduct)
                        double.TryParse(item.Conversion, out dConversion);

                    double dUnitTotal = 0;
                    double dThisYrYTD_U = 0;
                    double dThisYrYTD_T = 0;
                    double dThisYrYTD_D = 0;

                    double dLastYrYTD_U = 0;
                    double dLastYrYTD_T = 0;
                    double dLastYrYTD_D = 0;

                    customizedTableEntity.MC = item.Code;

                    customizedTableEntity.M1D = item.Sum1;
                    customizedTableEntity.M1T = item.Sum2;
                    customizedTableEntity.M1U = (IsProduct ? DoUnitCalculation(item.Sum2, dConversion, ref dUnitTotal) : 0);

                    customizedTableEntity.M2D = item.Sum3;
                    customizedTableEntity.M2T = item.Sum4;
                    customizedTableEntity.M2U = (IsProduct ? DoUnitCalculation(item.Sum4, dConversion, ref dUnitTotal) : 0);

                    customizedTableEntity.M3D = item.Sum5;
                    customizedTableEntity.M3T = item.Sum6;
                    customizedTableEntity.M3U = (IsProduct ? DoUnitCalculation(item.Sum6, dConversion, ref dUnitTotal) : 0);

                    customizedTableEntity.M4D = item.Sum7;
                    customizedTableEntity.M4T = item.Sum8;
                    customizedTableEntity.M4U = (IsProduct ? DoUnitCalculation(item.Sum8, dConversion, ref dUnitTotal) : 0);

                    customizedTableEntity.M5D = item.Sum9;
                    customizedTableEntity.M5T = item.Sum10;
                    customizedTableEntity.M5U = (IsProduct ? DoUnitCalculation(item.Sum10, dConversion, ref dUnitTotal) : 0);

                    customizedTableEntity.M6D = item.Sum11;
                    customizedTableEntity.M6T = item.Sum12;
                    customizedTableEntity.M6U = (IsProduct ? DoUnitCalculation(item.Sum12, dConversion, ref dUnitTotal) : 0);

                    customizedTableEntity.MD = item.MD;

                    customizedTableEntity.MTYTDD = dThisYrYTD_D = item.Sum14;
                    customizedTableEntity.MTYTDT = dThisYrYTD_T = item.Sum13;
                    customizedTableEntity.MTYTDU = dThisYrYTD_U = IgnoreComplexValues(IsProduct ? (dThisYrYTD_T / dConversion) : 0);

                    customizedTableEntity.MLYTDD = dLastYrYTD_D = item.Sum16;
                    customizedTableEntity.MLYTDT = dLastYrYTD_T = item.Sum15;
                    customizedTableEntity.MLYTDU = dLastYrYTD_U = IgnoreComplexValues(IsProduct ? (dThisYrYTD_T / dConversion) : 0);

                    customizedTableEntity.MPD = IgnoreComplexValues((dThisYrYTD_D / dLastYrYTD_D) * 100);
                    customizedTableEntity.MPT =IgnoreComplexValues((dThisYrYTD_T / dLastYrYTD_T)*100);
                    customizedTableEntity.MPU = IgnoreComplexValues((dThisYrYTD_U / dLastYrYTD_U) * 100);

                    customizedTableEntity.MY1D = item.ValYear1;
                    customizedTableEntity.MY1T = item.VolYear1;
                    customizedTableEntity.MY1U = IgnoreComplexValues(Convert.ToDouble(item.VolYear1) / dConversion);

                    customizedTableEntity.MY2D = item.ValYear2;
                    customizedTableEntity.MY2T = item.VolYear2;
                    customizedTableEntity.MY2U = IgnoreComplexValues(Convert.ToDouble(item.VolYear2) / dConversion);

                    customizedTableEntity.MY3D = item.ValYear3;
                    customizedTableEntity.MY3T = item.VolYear3;
                    customizedTableEntity.MY3U = IgnoreComplexValues(Convert.ToDouble(item.VolYear3) / dConversion);

                    customizedTableEntity.TotalCount = item.TotalCount;


                    customizedTableEntity.M1DTot = item.Sum1Tot;
                    customizedTableEntity.M1TTot = item.Sum2Tot;
                    customizedTableEntity.M1UTot = (IsProduct ? DoUnitCalculation(item.Sum2Tot, dConversion, ref dUnitTotal) : 0);

                    customizedTableEntity.M2DTot = item.Sum3Tot;
                    customizedTableEntity.M2TTot = item.Sum4Tot;
                    customizedTableEntity.M2UTot = (IsProduct ? DoUnitCalculation(item.Sum4Tot, dConversion, ref dUnitTotal) : 0);

                    customizedTableEntity.M3DTot = item.Sum5Tot;
                    customizedTableEntity.M3TTot = item.Sum6Tot;
                    customizedTableEntity.M3UTot = (IsProduct ? DoUnitCalculation(item.Sum6Tot, dConversion, ref dUnitTotal) : 0);

                    customizedTableEntity.M4DTot = item.Sum7Tot;
                    customizedTableEntity.M4TTot = item.Sum8Tot;
                    customizedTableEntity.M4UTot = (IsProduct ? DoUnitCalculation(item.Sum8Tot, dConversion, ref dUnitTotal) : 0);

                    customizedTableEntity.M5DTot = item.Sum9Tot;
                    customizedTableEntity.M5TTot = item.Sum10Tot;
                    customizedTableEntity.M5UTot = (IsProduct ? DoUnitCalculation(item.Sum10Tot, dConversion, ref dUnitTotal) : 0);

                    customizedTableEntity.M6DTot = item.Sum11Tot;
                    customizedTableEntity.M6TTot = item.Sum12Tot;
                    customizedTableEntity.M6UTot = (IsProduct ? DoUnitCalculation(item.Sum12Tot, dConversion, ref dUnitTotal) : 0);

                    double dThisYrYTD_UTot = 0;
                    double dThisYrYTD_TTot = 0;
                    double dThisYrYTD_DTot = 0;

                    double dLastYrYTD_UTot = 0;
                    double dLastYrYTD_TTot = 0;
                    double dLastYrYTD_DTot = 0;

                    customizedTableEntity.MTYTDDTot = dThisYrYTD_DTot = item.Sum14Tot;
                    customizedTableEntity.MTYTDTTot = dThisYrYTD_TTot = item.Sum13Tot;
                    customizedTableEntity.MTYTDUTot = dThisYrYTD_UTot = IgnoreComplexValues(IsProduct ? (dThisYrYTD_TTot / dConversion) : 0);

                    customizedTableEntity.MLYTDDTot = dLastYrYTD_DTot = item.Sum16Tot;
                    customizedTableEntity.MLYTDTTot = dLastYrYTD_TTot = item.Sum15Tot;
                    customizedTableEntity.MLYTDUTot = dLastYrYTD_UTot = IgnoreComplexValues(IsProduct ? (dThisYrYTD_TTot / dConversion) : 0);

                    customizedTableEntity.MPDTot = IgnoreComplexValues((dThisYrYTD_DTot / dLastYrYTD_DTot) * 100);
                    customizedTableEntity.MPTTot = IgnoreComplexValues((dThisYrYTD_TTot / dLastYrYTD_TTot) * 100);
                    customizedTableEntity.MPUTot = IgnoreComplexValues((dThisYrYTD_UTot / dLastYrYTD_UTot) * 100);

                    customizedTableEntity.MY1DTot = item.ValYear1Tot;
                    customizedTableEntity.MY1TTot = item.VolYear1Tot;
                    customizedTableEntity.MY1UTot = IgnoreComplexValues(Convert.ToDouble(item.VolYear1Tot) / dConversion);

                    customizedTableEntity.MY2DTot = item.ValYear2Tot;
                    customizedTableEntity.MY2TTot = item.VolYear2Tot;
                    customizedTableEntity.MY2UTot = IgnoreComplexValues(Convert.ToDouble(item.VolYear2Tot) / dConversion);

                    customizedTableEntity.MY3DTot = item.ValYear3Tot;
                    customizedTableEntity.MY3TTot = item.VolYear3Tot;
                    customizedTableEntity.MY3UTot = IgnoreComplexValues(Convert.ToDouble(item.VolYear3Tot) / dConversion);
                    customizedTableEntity.MDTot = item.TotalCount;
                    lstCustomizedTable.Add(customizedTableEntity);
                }

                return lstCustomizedTable;
            }
            catch (Exception)
            {
                throw;
            }
        }

        private double DoUnitCalculation(object value, double dConversion, ref double dSummation)
        {
            try
            {
                double dValue = IgnoreComplexValues(Convert.ToDouble(value) / dConversion);
                dSummation += dValue;
                return dValue;
            }
            catch (Exception)
            {
                throw;
            }

        }
        private double IgnoreComplexValues(double value)
        {
            try
            {
                if (double.IsInfinity(value) || double.IsNaN(value) || double.IsNegativeInfinity(value) || double.IsPositiveInfinity(value))
                    return 0;
                else
                    return Math.Round(value, 1);
            }
            catch
            {
                return 0;
            }
        }

        public List<SalesDataEntity> GetAllSales(ArgsEntity args, string IsASMOrProduct, int ProductId)
        {
            try
            {
                return SalesInfoDataService.GetAllSales(args, IsASMOrProduct, ProductId);
            }
            catch
            {
                throw;
            }

        }

        public List<SalesDataEntity> getASMSalesData(ArgsEntity args)
        {
            try
            {
                return SalesInfoDataService.getASMSalesData(args);
            }
            catch
            {
                throw;
            }

        }

        public List<SalesDataEntity> GetRepSales(ArgsEntity args)
        {
            try
            {
                return SalesInfoDataService.GetRepSales(args);
            }
            catch
            {
                throw;
            }

        }
    }
}
