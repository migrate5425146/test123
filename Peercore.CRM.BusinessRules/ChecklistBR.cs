﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class ChecklistBR
    {
        private static ChecklistBR instance = null;
        private static object syncLock = new Object();

        private ChecklistDAO ChecklistService;

        public static ChecklistBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new ChecklistBR();
                }
                return instance;
            }
        }

        private ChecklistBR()
        {
            ChecklistService = new ChecklistDAO();
        }

        #region - Init Object -

        public ChecklistEntity CreateObject()
        {
            return ChecklistService.CreateObject();
        }

        public ChecklistEntity CreateObject(int entityId)
        {
            return ChecklistService.CreateObject(entityId);
        }

        #endregion

        public List<ChecklistEntity> GetChecklist(int iLeadStageID)
        {
            try
            {
                return ChecklistService.GetChecklist(iLeadStageID);
            }
            catch
            {
                throw;
            }
        }

        public List<ChecklistImageEntity> GetChecklistImageById(int CheckListId)
        {
            try
            {
                return ChecklistService.GetChecklistImageById(CheckListId);
            }
            catch
            {
                throw;
            }
        }

        public bool Save(List<ChecklistEntity> leadChecklistEntity)
        {
            DbWorkflowScope transactionScope = ChecklistService.WorkflowScope;
            bool success = true;

            try
            {
                using (transactionScope)
                {
                    ChecklistDAO checklistDataService = new ChecklistDAO(transactionScope);

                    foreach (ChecklistEntity objleadChecklistEntity in leadChecklistEntity)
                    {
                        success = checklistDataService.Save(objleadChecklistEntity);

                        if (!success)
                            break;
                    }

                    if (success)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }

            return success;
        }

        public string SaveQuery(ChecklistEntity leadChecklist)
        {
            try
            {
                return ChecklistService.SaveQuery(leadChecklist);
            }
            catch
            {
                throw;
            }
        }

        public List<ChecklistTypeEntity> GetChecklistTypeList()
        {
            try
            {
                return ChecklistService.GetChecklistTypeList();
            }
            catch
            {
                throw;
            }
        }

        public List<ChecklistMasterEntity> GetChecklistMaster(string accessToken, ref int routeID)
        {
            try
            {
                OriginatorEntity originator = new OriginatorEntity();
                originator = OriginatorBR.Instance.GetOriginatorRouteByAccessToken(accessToken);
                routeID = originator.RouteId;
                return ChecklistService.GetDailyChecklistForRep(originator.RouteId);
            }
            catch
            {
                throw;
            }
        }

        public List<ChecklistImageEntity> GetChecklistImageList(string strChecklistIds)
        {
            try
            {
                return ChecklistService.GetDailyChecklistImages(strChecklistIds);
            }
            catch
            {
                throw;
            }
        }

        public List<ChecklistDetailEntity> GetChecklistDetailList(string strChecklistIds)
        {
            try
            {
                return ChecklistService.GetDailyChecklistDetails(strChecklistIds);
            }
            catch
            {
                throw;
            }
        }

        public List<ChecklistEntity> GetAllCheckListDataAndCount(ArgsEntity args)
        {
            try
            {
                return ChecklistService.GetChecklistDataAndCount(args);
            }
            catch
            {
                throw;
            }
        }


        public bool SaveCustomerChecklists(ChecklistDetailEntity objleadChecklistEntity)
        {
            DbWorkflowScope transactionScope = ChecklistService.WorkflowScope;
            bool success = false;

            try
            {
                using (transactionScope)
                {
                    ChecklistDAO checklistDataService = new ChecklistDAO(transactionScope);
                    success = checklistDataService.InsertCustomerChecklist(objleadChecklistEntity);

                    if (success)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }

            return success;
        }

        public List<ChecklistCustomerEntity> GetDailyChecklistCustomers(int routeID)
        {
            try
            {
                return ChecklistService.GetDailyChecklistCustomers(routeID);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveChecklistMasterDetails(ChecklistMasterEntity ChecklistEntity)
        {
            DbWorkflowScope transactionScope = ChecklistService.WorkflowScope;
            bool success = false;
            int checklistId = ChecklistEntity.ChecklistId;

            try
            {
                using (transactionScope)
                {
                    ChecklistDAO checklistDataService = new ChecklistDAO(transactionScope);

                    if (checklistId == 0)
                        checklistId = checklistDataService.InsertCheklistItem(ChecklistEntity);
                    else
                        checklistDataService.UpdateCheklistItem(ChecklistEntity);

                    if (checklistId > 0)
                    {
                        success = true;
                        if (ChecklistEntity.ImageList != null && ChecklistEntity.ImageList.Count > 0)
                        {
                            foreach (ChecklistImageEntity item in ChecklistEntity.ImageList)
                            {
                                item.ChecklistId = checklistId;
                                success = checklistDataService.InsertCheklistImage(item);
                                if (!success)
                                    break;
                            }
                        }

                        //if (ChecklistEntity.CustomerList != null && ChecklistEntity.CustomerList.Count > 0)
                        //{
                        //    foreach (ChecklistCustomerEntity item in ChecklistEntity.CustomerList)
                        //    {
                        //        success = checklistDataService.InsertCheklistCustomer(item);
                        //        if (!success)
                        //            break;
                        //    }
                        //}

                        //if (ChecklistEntity.RepList != null && ChecklistEntity.RepList.Count > 0)
                        //{
                        //    foreach (ChecklistRepEntity item in ChecklistEntity.RepList)
                        //    {
                        //        success = checklistDataService.InsertCheklistRep(item);
                        //        if (!success)
                        //            break;
                        //    }
                        //}

                        //if (ChecklistEntity.RouteList != null && ChecklistEntity.RouteList.Count > 0)
                        //{
                        //    foreach (ChecklistRouteEntity item in ChecklistEntity.RouteList)
                        //    {
                        //        success = checklistDataService.InsertCheklistRoute(item);
                        //        if (!success)
                        //            break;
                        //    }
                        //}
                    }

                    if (success)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }

            return success;
        }
    }
}
