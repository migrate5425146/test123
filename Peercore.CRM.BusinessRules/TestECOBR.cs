﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.CRM.Model;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class TestEcoBR
    {
        private static TestEcoBR instance = null;
        private static object syncLock = new Object();

        private TestEcoBR BrandDataService;

        public static TestEcoBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new TestEcoBR();
                }
                return instance;
            }
        }

        private TestEcoBR()
        {
            BrandDataService = new TestEcoBR();
        }

      





        public List<BrandECOModel> GetBrandECOCumulativeByAccessToken(string accessToken, string strDate, string endDate)
        {
            try
            {
                return BrandDataService.GetBrandECOCumulativeByAccessToken(accessToken, strDate, endDate);
            }
            catch
            {
                throw;
            }
        }



        public List<CategoryECOModel> GetCategoryECOCumulativeByAccessToken(string accessToken, string strDate, string endDate)
        {
            try
            {
                return ProductDataService.GetCategoryECOCumulativeByAccessToken(accessToken, strDate, endDate);
            }
            catch
            {
                throw;
            }
        }




        public List<CustomerECOModel> GetCustomerECOCumulativeByAccessToken(string accessToken, string strDate, string endDate)
        {
            try
            {
                return customerDataService.GetCustomerECOCumulativeByAccessToken(accessToken, strDate, endDate);
            }
            catch
            {
                throw;
            }
        }




        public List<ProductECOModel> GetProductECOCumulativeByAccessToken(string accessToken, string strDate, string endDate)
        {
            try
            {
                return ProductDataService.GetProductECOCumulativeByAccessToken(accessToken, strDate, endDate);
            }
            catch
            {
                throw;
            }
        }




    }
}
