﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class EndUserContactPersonBR
    {
        private static EndUserContactPersonBR instance = null;
        private static object syncLock = new Object();

        private EndUserContactPersonDAO EndUserContactPersonDataService;

        public static EndUserContactPersonBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new EndUserContactPersonBR();
                }
                return instance;
            }
        }

        private EndUserContactPersonBR()
        {
            EndUserContactPersonDataService = new EndUserContactPersonDAO();
        }

        #region - Init Object -

        public EndUserContactPersonEntity CreateObject()
        {
            return EndUserContactPersonDataService.CreateObject();
        }

        public EndUserContactPersonEntity CreateObject(int entityId)
        {
            return EndUserContactPersonDataService.CreateObject(entityId);
        }

        #endregion

        public bool SaveEndUserContact(EndUserContactPersonEntity endUserContactPerson, ref int contactPersonId)
        {
            DbWorkflowScope transactionScope = EndUserContactPersonDataService.WorkflowScope;
            bool isSuccess = true;

            try
            {
                using (transactionScope)
                {
                    EndUserContactPersonDAO endUserContactPersonService = new EndUserContactPersonDAO(transactionScope);

                    if (endUserContactPerson.KeyContact == "Y")
                        endUserContactPersonService.UpdateKeyContact(endUserContactPerson.EndUserCode, endUserContactPerson.ContactPersonID);

                    if (endUserContactPerson.ContactPersonID > 0)
                    {
                        endUserContactPersonService.UpdateEndUserContactPerson(endUserContactPerson);
                        contactPersonId = endUserContactPerson.ContactPersonID;
                    }
                    else
                    {
                        endUserContactPersonService.InsertEndUserContactPerson(endUserContactPerson, ref contactPersonId);
                    }


                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public EndUserContactPersonEntity GetEndUserContactPerson(int iContactPersonID)
        {
            try
            {
                return EndUserContactPersonDataService.GetEndUserContactPerson(iContactPersonID);
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserContactPersonEntity> GetEndUserContactPerson(ArgsEntity args)
        {
            try
            {
                return EndUserContactPersonDataService.GetEndUserContactPerson(args);
            }
            catch
            {
                throw;
            }
        }
    }
}
