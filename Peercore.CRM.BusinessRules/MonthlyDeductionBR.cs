﻿using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.BusinessRules
{
    public class MonthlyDeductionBR
    {
        private static MonthlyDeductionBR instance = null;
        private static object syncLock = new Object();

        private MonthlyDeductionDAO DeductionDataService;

        public static MonthlyDeductionBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new MonthlyDeductionBR();
                }
                return instance;
            }
        }

        private MonthlyDeductionBR()
        {
            DeductionDataService = new MonthlyDeductionDAO();
        }

        #region - Monthly Deduction -

        public List<MonthlyDeductionModel> GetAllDeductions(ArgsModel args)
        {
            try
            {
                return DeductionDataService.GetAllDeductions(args);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveDeduction(MonthlyDeductionModel deductionDetail)
        {
            try
            {
                return DeductionDataService.SaveDeduction(deductionDetail);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateDeduction(MonthlyDeductionModel deductionDetail)
        {
            try
            {
                return DeductionDataService.UpdateDeduction(deductionDetail);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteDeduction(int deductId, string originator)
        {
            try
            {
                return DeductionDataService.DeleteDeduction(deductId, originator);
            }
            catch
            {
                throw;
            }
        }

        public bool UploadDeductionExcelfile(string FilePath, string UserName, string logPath)
        {
            try
            {
                return DeductionDataService.UploadDeductionExcelfile(FilePath, UserName, logPath);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public bool IsRepCodeAvailable(string repCode)
        {
            try
            {
                return DeductionDataService.IsRepCodeAvailable(repCode);
            }
            catch
            {
                throw;
            }
        }

        #endregion
    }
}
