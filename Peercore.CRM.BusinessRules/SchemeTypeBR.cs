﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class SchemeTypeBR
    {
        private static SchemeTypeBR instance = null;
        private static object syncLock = new Object();

        private SchemeTypeDAO SchemeTypeDataService;

        public static SchemeTypeBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new SchemeTypeBR();
                }
                return instance;
            }
        }

        private SchemeTypeBR()
        {
            SchemeTypeDataService = new SchemeTypeDAO();
        }

        #region - Init Object -

        public SchemeTypeEntity CreateObject()
        {
            return SchemeTypeDataService.CreateObject();
        }

        public SchemeTypeEntity CreateObject(int entityId)
        {
            return SchemeTypeDataService.CreateObject(entityId);
        }

        #endregion

        public List<SchemeTypeEntity> GetAllSchemeType()
        {
            try
            {
                return SchemeTypeDataService.GetAllSchemeType();
            }
            catch
            {
                throw;
            }
        }
    }
}
