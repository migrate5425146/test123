﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class OptionLevelBR 
    {
        private static OptionLevelBR instance = null;
        private static object syncLock = new Object();

        private OptionLevelDAO OptionLevelDataService;

        public static OptionLevelBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new OptionLevelBR();
                }
                return instance;
            }
        }

        private OptionLevelBR()
        {
            OptionLevelDataService = new OptionLevelDAO();
        }

        #region - Init Object -

        public OptionLevelEntity CreateObject()
        {
            return OptionLevelDataService.CreateObject();
        }

        public OptionLevelEntity CreateObject(int entityId)
        {
            return OptionLevelDataService.CreateObject(entityId);
        }

        #endregion


        public List<OptionLevelEntity> GetOptionLevelByDetailsId(int detailsId)
        {
            try
            {
                return OptionLevelDataService.GetOptionLevelByDetailsId(detailsId);
            }
            catch
            {
                throw;
            }
        }

        public List<OptionLevelEntity> GetOptionLevelByDivisionId(int divisionId)
        {
            try
            {
                return OptionLevelDataService.GetOptionLevelByDivisionId(divisionId);
            }
            catch
            {
                throw;
            }
        }
    }
}
