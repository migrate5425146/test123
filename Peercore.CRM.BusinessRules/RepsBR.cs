﻿using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Model;
using Peercore.Workflow.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.BusinessRules
{
    public class RepsBR
    {
        private static RepsBR instance = null;
        private static object syncLock = new Object();

        private RepDAO RepsDataService;

        public static RepsBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new RepsBR();
                }
                return instance;
            }
        }

        private RepsBR()
        {
            RepsDataService = new RepDAO();
        }

        public List<SRModel> GetAllSalesRepsByTerritoryId(ArgsModel args, string TerritoryId)
        {
            try
            {
                return RepsDataService.GetAllSalesRepsByTerritoryId(args, TerritoryId);
            }
            catch
            {
                throw;
            }
        }
        
        public List<SRModel> GetAllSalesRepsByAreaId(ArgsModel args, string AreaId)
        {
            try
            {
                return RepsDataService.GetAllSalesRepsByAreaId(args, AreaId);
            }
            catch
            {
                throw;
            }
        }
        
        public List<SRModel> GetAllSalesReps(ArgsModel args)
        {
            try
            {
                return RepsDataService.GetAllSalesReps(args);
            }
            catch
            {
                throw;
            }
        }
        
        public List<SRModel> GetAllSalesRepsByOriginator(ArgsModel args, string Originator)
        {
            try
            {
                return RepsDataService.GetAllSalesRepsByOriginator(args, Originator);
            }
            catch
            {
                throw;
            }
        }
        
        public List<SRModel> GetAllSalesRepsForRouteAssign(ArgsModel args)
        {
            try
            {
                return RepsDataService.GetAllSalesRepsForRouteAssign(args);
            }
            catch
            {
                throw;
            }
        }
        
        public List<SRModel> GetAllSRTerritoryByOriginator(ArgsModel args, string OriginatorType, string Originator)
        {
            try
            {
                return RepsDataService.GetAllSRTerritoryByOriginator(args, OriginatorType, Originator);
            }
            catch
            {
                throw;
            }
        }

        public List<SRModel> GetAllSRDetails()
        {
            try
            {
                return RepsDataService.GetAllSRDetails();
            }
            catch
            {
                throw;
            }
        }
        
        public SRModel GetSRDetailsByOriginator(string originator)
        {
            try
            {
                return RepsDataService.GetSRDetailsByOriginator(originator);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveSR(SRModel srEntity)
        {
            bool isSuccess = false;

            try
            {
                if (srEntity.RepId > 0)
                {
                    isSuccess = RepsDataService.UpdateSR(srEntity);
                }
                else
                {
                    isSuccess = RepsDataService.SaveSR(srEntity);
                }
            }
            catch (Exception)
            {
                isSuccess = false;
            }

            return isSuccess;
        }

        public bool UpdateSRSalesType(string RepId, string SalesType, string Originator)
        {
            DbWorkflowScope transactionScope = RepsDataService.WorkflowScope;

            bool isSuccess = true;

            try
            {
                using (transactionScope)
                {
                    isSuccess = RepsDataService.UpdateSRSalesType(transactionScope, RepId, SalesType, Originator);

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }

            return isSuccess;
        }

        public List<SalesRepSalesTypeModel> GetAllSalesRepWithSalesType(ArgsModel args, string OriginatorType, string Originator)
        {
            try
            {
                return RepsDataService.GetAllSalesRepWithSalesType(args, OriginatorType, Originator);
            }
            catch
            {
                throw;
            }
        }

        public List<SalesRepRouteSequenceModel> GetAllSRRouteLocations(string selectRep, string selectDate)
        {
            try
            {
                return RepsDataService.GetAllSRRouteLocations(selectRep, selectDate);
            }
            catch
            {
                throw;
            }
        }
    }
}
