﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;

namespace Peercore.CRM.BusinessRules
{
    public class LookupTableBR
    {
        private static LookupTableBR instance = null;
        private static object syncLock = new Object();
        private LookupTableDAO LookupTableDataService;

        public static LookupTableBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new LookupTableBR();
                }
                return instance;
            }
        }

        private LookupTableBR()
        {
            LookupTableDataService = new LookupTableDAO();
        }

        public List<LookupTableEntity> GetLookupEntries(string sTableID, string defaultDeptID)
        {
            try
            {
                return LookupTableDataService.GetLookupEntries(sTableID, defaultDeptID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public LookupTableEntity GetDefaultLookupEntry(string sTableID, string defaultDeptID)
        {
            try
            {
                return LookupTableDataService.GetDefaultLookupEntry(sTableID, defaultDeptID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LookupTableEntity> GetRepTerritories( ArgsEntity args)
        {
            try
            {
                return LookupTableDataService.GetRepTerritories(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LookupTableEntity> GetSalesMarkets()
        {
            try
            {
                return LookupTableDataService.GetSalesMarkets();
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region - Init Object -

        public LookupTableEntity CreateObject()
        {
            return LookupTableDataService.CreateObject();
        }

        public LookupTableEntity CreateObject(int entityId)
        {
            return LookupTableDataService.CreateObject(entityId);
        }

        #endregion
    }
}
