﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class ProductPackingBR
    {
        private static ProductPackingBR instance = null;
        private static object syncLock = new Object();

        private ProductPackingDAO PackingDataService;

        public static ProductPackingBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new ProductPackingBR();
                }
                return instance;
            }
        }

        private ProductPackingBR()
        {
            PackingDataService = new ProductPackingDAO();
        }

        #region - Init Object -

        public ProductPackingEntity CreateObject()
        {
            return PackingDataService.CreateObject();
        }

        public ProductPackingEntity CreateObject(int entityId)
        {
            return PackingDataService.CreateObject(entityId);
        }

        #endregion

        public List<ProductPackingEntity> GetProductPackingByAccessToken(string accessToken)
        {
            try
            {
                return PackingDataService.GetProductPackingByAccessToken(accessToken);
            }
            catch
            {
                throw;
            }
        }

        public List<ProductPackingEntity> GetAllProductPackings(ArgsEntity args)
        {
            try
            {
                return PackingDataService.GetAllProductPackings(args);
            }
            catch
            {
                throw;
            }
        }

        public bool SavePackingName(List<ProductPackingEntity> ProductPackingEntityList)
        {
            DbWorkflowScope transactionScope = PackingDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                if (ProductPackingEntityList.Count != 0)
                {
                    using (transactionScope)
                    {
                        foreach (ProductPackingEntity packingEntity in ProductPackingEntityList)
                        {
                            if (packingEntity.PackingId > 0)
                            {
                                isSuccess = PackingDataService.UpdateProductPacking(packingEntity);
                            }
                            else
                            {
                                isSuccess = PackingDataService.InsertProductPacking(packingEntity);
                            }
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool IsProductPackingNameExists(ProductPackingEntity packingEntity)
        {
            try
            {
                return PackingDataService.IsProductPackingNameExists(packingEntity);
            }
            catch
            {
                throw;
            }
        }

        public bool DeletePacking(int packingId)
        {
            try
            {
                return PackingDataService.DeletePacking(packingId);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateCheckListCustomer(int CheckListId, string CustomerCode, bool HasSelect)
        {
            try
            {
                return PackingDataService.UpdateCheckListCustomer(CheckListId, CustomerCode, HasSelect);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateCheckListRoute(int CheckListId, int RouteId, bool HasSelect)
        {
            try
            {
                return PackingDataService.UpdateCheckListRoute(CheckListId, RouteId, HasSelect);
            }
            catch
            {
                throw;
            }
        }
    }
}
