﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class EndUserBR
    {
        private static EndUserBR instance = null;
        private static object syncLock = new Object();

        private EndUserDAO EndUserDataService;

        public static EndUserBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new EndUserBR();
                }
                return instance;
            }
        }

        private EndUserBR()
        {
            EndUserDataService = new EndUserDAO();
        }

        #region - Init Object -

        public EndUserEntity CreateObject()
        {
            return EndUserDataService.CreateObject();
        }

        public EndUserEntity CreateObject(string entityId)
        {
            return EndUserDataService.CreateObject(entityId);
        }

        #endregion

        public bool Save(EndUserEntity endUser)
        {
            DbWorkflowScope transactionScope = EndUserDataService.WorkflowScope;
            bool isSuccess = true;

            try
            {
                using (transactionScope)
                {
                    EndUserDAO endUserService = new EndUserDAO(transactionScope);
                    ArgsEntity args = new ArgsEntity();
                    args.CustomerCode = endUser.CustomerCode;
                    args.EnduserCode = endUser.EndUserCode;

                    EndUserEntity item = endUserService.GetEndUser(args);

                    if (item != null)
                    {
                        isSuccess = endUserService.UpdateEndUser(endUser);
                    }
                    else
                    {
                        isSuccess = endUserService.InsertEndUser(endUser);
                    }

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public EndUserEntity GetEndUser(ArgsEntity args)
        {
            try
            {
                return EndUserDataService.GetEndUser(args);
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserEntity> GetEndUsersByCode(string enduserCodes)
        {
            try
            {
                return EndUserDataService.GetEndUsersByCode(enduserCodes);
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserEntity> GetEndUsers(ArgsEntity args, string sStatus)
        {
            try
            {
                return EndUserDataService.GetEndUsers(args, sStatus);
            }
            catch
            {
                throw;
            }
        }



        //public List<EndUserEntity> GetEndUserListForTME(int tmeCode, ArgsEntity args)
        //{
        //    try
        //    {
        //       
        //        string sStatus="";
               
        //      return EndUserDataService.GetEndUsers(args, sStatus);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}




        public int SetActiveStatus(ArgsEntity args, bool isActive)
        {
            try
            {
                return EndUserDataService.SetActiveStatus(args, isActive);
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserEntity> GetAllEndUsers(ArgsEntity args)
        {
            try
            {
                return EndUserDataService.GetAllEndUsers(args);
            }
            catch
            {
                throw;
            }
        }

        public EndUserEntity IsEndUserExistForCustomer(ArgsEntity args)
        {
            try
            {
                return EndUserDataService.IsEndUserExistForCustomer(args);
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserEntity> IsEndUserExistForOtherCustomers(ArgsEntity args)
        {
            try
            {
                return EndUserDataService.IsEndUserExistForOtherCustomers(args);
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserPriceEntity> GetEndUserPricing(ArgsEntity args, string effectiveDate)
        {
            try
            {
                return EndUserDataService.GetEndUserPricing(args, effectiveDate);
            }
            catch
            {
                throw;
            }
        }

        public List<EnduserEnquiryEntity> GetEnduserSalesWithAllProducts(ArgsEntity args, bool bIncludeProductsWithNoSales, bool bActiveOnly)
        {
            try
            {
                return EndUserDataService.GetEnduserSalesWithAllProducts(args, bIncludeProductsWithNoSales, bActiveOnly);
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserSalesEntity> GetEnduserSales(ArgsEntity args, bool bActiveOnly)
        {
            try
            {
                return EndUserDataService.GetEnduserSales(args, bActiveOnly);
            }
            catch
            {
                throw;
            }
        }

        public List<CustomerEntity> GetEndUserCustomers(ArgsEntity args)
        {
            try
            {
                return EndUserDataService.GetEndUserCustomers(args);
            }
            catch
            {
                throw;
            }
        }

        public bool Transfer(EndUserEntity endUser, ArgsEntity args)
        {
            DbWorkflowScope transactionScope = EndUserDataService.WorkflowScope;
            bool isSuccess = true;

            try
            {
                using (transactionScope)
                {
                    EndUserDAO endUserService = new EndUserDAO(transactionScope);
                    isSuccess = endUserService.Transfer(endUser, args);

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                transactionScope.Rollback();
                throw;
            }
            return isSuccess;
        }

        public EndUserPriceEntity GetEndUserPrice(ArgsEntity args, DateTime effectiveDate, string catalogCode)
        {
            try
            {
                return EndUserDataService.GetEndUserPrice(args,effectiveDate,catalogCode);
            }
            catch
            {
                throw;
            }
        }

        public bool SavePriceList(ArgsEntity args, DateTime dtmEffectiveDate, List<EndUserPriceEntity> listEndUserPrice)
        {
            DbWorkflowScope transactionScope = EndUserDataService.WorkflowScope;
            bool isSuccess = true;

            try
            {
                using (transactionScope)
                {
                    EndUserDAO endUserService = new EndUserDAO(transactionScope);
                    isSuccess = endUserService.SavePriceList(args, dtmEffectiveDate, listEndUserPrice);

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                transactionScope.Rollback();
                throw;
            }
            return isSuccess;
        }
    }
}
