﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;

namespace Peercore.CRM.BusinessRules
{
    public class SearchBR
    {
        private static SearchBR instance = null;
        private static object syncLock = new Object();

        private SearchDAO SearchDataService;

        public static SearchBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new SearchBR();
                }
                return instance;
            }
        }

        private SearchBR()
        {
            SearchDataService = new SearchDAO();
        }

        #region - Init Object -

        public LeadContactPersonViewEntity CreateObject()
        {
            return SearchDataService.CreateObject();
        }

        public LeadContactPersonViewEntity CreateObject(int entityId)
        {
            return SearchDataService.CreateObject(entityId);
        }

        #endregion

        public List<LeadContactPersonViewEntity> GetContactCollection(ArgsEntity  argsEntity,bool bIncludeContact, bool bIncludeLead,
           bool bIncludeCustomer, bool bIncludeEndUser, string clientType,           
           string sSearchText = "",
           string childOriginators = "")
        {
            try
            {
                OriginatorDAO originatorDAO = new OriginatorDAO();
                List<OriginatorEntity> listOriginator = originatorDAO.GetChildOriginatorsList(argsEntity.Originator);

                return SearchDataService.GetContactCollection(  argsEntity, 
                                                                bIncludeContact, 
                                                                bIncludeLead,
                                                                bIncludeCustomer,
                                                                bIncludeEndUser, 
                                                                clientType,
                                                                listOriginator,
                                                                sSearchText,
                                                                childOriginators);
            }
            catch
            {
                throw;
            }
        }
    }
}
