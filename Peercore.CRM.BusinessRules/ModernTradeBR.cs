﻿using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.CRM.Model;
using Peercore.Workflow.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.BusinessRules
{
    public class ModernTradeBR
    {
        private static ModernTradeBR instance = null;
        private static object syncLock = new Object();

        private ModernTradeDAO ModernTradeDataService;

        public static ModernTradeBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new ModernTradeBR();
                }
                return instance;
            }
        }

        private ModernTradeBR()
        {
            ModernTradeDataService = new ModernTradeDAO();
        }

        public ModernTradeMasterModel InsertModernTrade(string accessToken, ModernTradeMasterModel modernTrade)
        {
            bool isSuccess = false;
            modernTrade.IsSuccess = false;

            DbWorkflowScope transactionScope = ModernTradeDataService.WorkflowScope;

            int modernTradeId = 0;

            try
            {
                using (transactionScope)
                {
                    try
                    {
                        OriginatorDAO originatorService = new OriginatorDAO();
                        ModernTradeDAO modernTradeService = new ModernTradeDAO(transactionScope);

                        //OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);

                        //if (originator == null)
                        //{
                        //    transactionScope.Rollback();
                        //    return false;
                        //}

                        if (modernTrade.CustomerCode == null)
                        {
                            transactionScope.Rollback();
                        }
                        else if (modernTrade.ModernTradeDate == null)
                        {
                            transactionScope.Rollback();
                        }

                        isSuccess = modernTradeService.InsertModernTradeHeader(out modernTradeId, modernTrade);

                        if (isSuccess)
                        {
                            modernTrade.ModernTradeId = modernTradeId;

                            modernTradeService.DeleteModernTradeDetails(modernTradeId);

                            foreach (var item in modernTrade.ModernTradeItemCollection)
                            {
                                item.ModernTradeId = modernTradeId;

                                isSuccess = modernTradeService.InsertModernTradeDetails(item);
                                if (!isSuccess)
                                {
                                    transactionScope.Rollback();
                                }
                            }
                        }
                    }
                    catch
                    {
                        transactionScope.Rollback();
                    }

                    if (isSuccess)
                    {
                        modernTrade.IsSuccess = true;
                        transactionScope.Commit();
                    }
                    else
                    {
                        transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                modernTrade.IsSuccess = false;
            }

            return modernTrade;
        }

        public bool DeleteModernTradeHeader(string accessToken, decimal ModernTradeId)
        {
            bool isSuccess = false;
            bool IsSuccess = false;

            DbWorkflowScope transactionScope = ModernTradeDataService.WorkflowScope;

            try
            {
                using (transactionScope)
                {
                    try
                    {
                        OriginatorDAO originatorService = new OriginatorDAO();
                        ModernTradeDAO modernTradeService = new ModernTradeDAO(transactionScope);

                        OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);

                        if (originator == null)
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        isSuccess = modernTradeService.DeleteModernTradeHeader(originator.Originator, ModernTradeId);
                    }
                    catch
                    {
                        transactionScope.Rollback();
                    }

                    if (isSuccess)
                    {
                        IsSuccess = true;
                        transactionScope.Commit();
                    }
                    else
                    {
                        transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                IsSuccess = false;
            }

            return IsSuccess;
        }

        public List<ModernTradeMasterModel> GetAllModernTradesByAccessToken(string accessToken, string date)
        {
            ModernTradeDAO modernTradeService = new ModernTradeDAO();
            List<ModernTradeMasterModel> MTList = new List<ModernTradeMasterModel>();

            try
            {
                MTList = modernTradeService.GetAllModernTradesByAccessToken(accessToken, date);
            }
            catch
            {
                MTList = new List<ModernTradeMasterModel>();
            }

            return MTList;
        }
        
        public List<ModernTradeMasterModel> GetAllModernTradesByAccessTokenAndPeriod(string accessToken, string startDate, string endDate)
        {
            ModernTradeDAO modernTradeService = new ModernTradeDAO();
            List<ModernTradeMasterModel> MTList = new List<ModernTradeMasterModel>();

            try
            {
                MTList = modernTradeService.GetAllModernTradesByAccessTokenAndPeriod(accessToken, startDate, endDate);
            }
            catch
            {
                MTList = new List<ModernTradeMasterModel>();
            }

            return MTList;
        }
    }
}
