﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class SalesBR
    {
        private static SalesBR instance = null;
        private static object syncLock = new Object();
        private SalesDAO salesDataService;
        private int iMonth = 7;

        public static SalesBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new SalesBR();
                }
                return instance;
            }
        }

        #region - Init Object -

        /*public SalesEntity CreateObject()
        {
            return salesDataService.CreateObject();
        }

        public SalesEntity CreateObject(int entityId)
        {
            return salesDataService.CreateObject(entityId);
        }
        */
        #endregion

        #region - Constructor -

        private SalesBR()
        {
            salesDataService = new SalesDAO();
        }
        #endregion

        #region - Public Methods -

        public EndUserSalesEntity GetCurrentCostPeriod()
        {
            try
            {
                return salesDataService.GetCurrentCostPeriod();
            }
            catch
            {
                throw;
            }
        }

        public EndUserSalesEntity GetCostPeriod(string dtDate)
        {
            try
            {
                return salesDataService.GetCostPeriod(dtDate);
            }
            catch
            {
                throw;
            }
        }

        public string GetMonthForCostPeriod(int costYear, int costPeriod)
        {
            try
            {
                return salesDataService.GetMonthForCostPeriod(costYear, costPeriod);
            }
            catch
            {
                throw;
            }
        }

        public string GetDescription(string sCatalogGrouping, string sGroupType)
        {
            try
            {
                return salesDataService.GetDescription(sGroupType, sCatalogGrouping);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public EndUserSalesEntity GetSalesData(string sCustCode,string date)
        {
            try
            {
                EndUserSalesEntity endUserSalesEntity = GetCostPeriod(date);

                #region Parameter Setting
                string[] sArrMonths = { "", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };
                string[] sArrSalesMonths = new string[3];
                int iLastYear = 0;


                #endregion

                if (endUserSalesEntity != null)
                {
                    iLastYear = endUserSalesEntity.Year - 1;

                    if ((endUserSalesEntity.Period - 1) <= 6)
                        sArrSalesMonths[0] = sArrMonths[(endUserSalesEntity.Period - 1) + 6] + " " + iLastYear.ToString();
                    else
                        sArrSalesMonths[0] = sArrMonths[(endUserSalesEntity.Period - 1) - 6] + " " + endUserSalesEntity.Year.ToString();

                    if ((endUserSalesEntity.Period - 2) <= 6)
                        sArrSalesMonths[1] = sArrMonths[(endUserSalesEntity.Period - 2) + 6] + " " + iLastYear.ToString();
                    else
                        sArrSalesMonths[1] = sArrMonths[(endUserSalesEntity.Period - 2) - 6] + " " + endUserSalesEntity.Year.ToString();

                    if ((endUserSalesEntity.Period - 3) <= 6)
                        sArrSalesMonths[2] = sArrMonths[(endUserSalesEntity.Period - 3) + 6] + " " + iLastYear.ToString();
                    else
                        sArrSalesMonths[2] = sArrMonths[(endUserSalesEntity.Period - 3) - 6] + " " + endUserSalesEntity.Year.ToString();

                    return salesDataService.GetSalesData(sCustCode, endUserSalesEntity.Period, sArrSalesMonths);
                }
                else
                {
                    return salesDataService.GetSalesData(sCustCode, 0, sArrSalesMonths);
                }
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserSalesEntity> GetDebtorsDetails(string sCustCode)
        {
            try
            {
                return salesDataService.GetDebtorsDetails(sCustCode);
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserSalesEntity> GetCostYears()
        {
            try
            {
                return salesDataService.GetCostYears();
            }
            catch
            {
                throw;
            }
        }

        public List<EndUserSalesEntity> GetCostPeriod(ArgsEntity args)
        {
            try
            {
                return salesDataService.GetCostPeriod(args);
            }
            catch
            {
                throw;
            }
        }

        public void LoadCostPeriods(ref List<string> lstCostPeriods, int iCostYear)
        {
            try
            {
                salesDataService.LoadCostPeriods(ref lstCostPeriods, iCostYear);
            }
            catch
            {
                throw;
            }
        }
        
        
        #endregion

        #region - Business All -

        public string getPeriod() {
            if (iMonth > 12)
                iMonth = 1;
            DateTime dtDate = new DateTime(2011, iMonth, 1);
            string month= String.Format("{0:MMM}", dtDate).ToString();
            iMonth++;
            return month;
        }

        public List<BusAllEntity> GetBusAllDetails(SalesInfoDetailSearchCriteriaEntity busSalesEnqSrc,string historyType)
        {
            //BusSalesEntity busSalesDetails = salesDataService.GetBusSalesDetails(busSalesEnqSrc);
            //BusSalesEntity busBudgetDetails = salesDataService.GetBusBudgetDetails(busSalesEnqSrc);
            //BusSalesEntity busProfitDetails = null ;
            //if (historyType.Equals("Profit"))
            //{
            //     busProfitDetails = salesDataService.GetBusProfitDetails(busSalesEnqSrc);
            //}
            ////BusAllEntity busAllDetails = new BusAllEntity();
            //List<BusAllEntity> BusAllList= new List<BusAllEntity>();

            //for (int i = 1; i <= 12; i++)
            //{
            //    BusAllList.Add(new BusAllEntity()
            //    {
            //        Period = getPeriod(),
            //        Tonne = (double)busSalesDetails.GetType().GetProperty("SalesVolP" + i).GetValue(busSalesDetails, null),
            //        LastYearTonne = (double)busSalesDetails.GetType().GetProperty("SalesVolPp" + i).GetValue(busSalesDetails, null),
            //        Sales = (double)busSalesDetails.GetType().GetProperty("SalesValP" + i).GetValue(busSalesDetails, null),
            //        LastYearSales = (double)busSalesDetails.GetType().GetProperty("SalesValPp" + i).GetValue(busSalesDetails, null),
            //        BudgetTonne = (double)busBudgetDetails.GetType().GetProperty("SalesVolP" + i).GetValue(busBudgetDetails, null),
            //        LastYearBudgetTonne = (double)busBudgetDetails.GetType().GetProperty("SalesVolPp" + i).GetValue(busBudgetDetails, null),
            //        BudgetSales = (double)busBudgetDetails.GetType().GetProperty("SalesValP" + i).GetValue(busBudgetDetails, null),
            //        LastYearBudgetSales = (double)busBudgetDetails.GetType().GetProperty("SalesValPp" + i).GetValue(busBudgetDetails, null),
            //        Profit = (busProfitDetails != null ? (double)busProfitDetails.GetType().GetProperty("SalesValYr" + i).GetValue(busProfitDetails, null) : 0),
            //        LastYearProfit = (busProfitDetails != null ? (double)busProfitDetails.GetType().GetProperty("SalesValYr" + i).GetValue(busProfitDetails, null) : 0)
            //    });

                
            //}

            //return BusAllList;
            return null;
        }

        public List<BusAllYearEntity> GetBusAllYearDetails(SalesInfoDetailSearchCriteriaEntity busSalesEnqSrc, string historyType)
        {
            //BusSalesEntity busSalesDetails = salesDataService.GetBusSalesDetails(busSalesEnqSrc);
            //BusSalesEntity busBudgetDetails = salesDataService.GetBusBudgetDetails(busSalesEnqSrc);
            //BusSalesEntity busProfitDetails = null;
            //if (historyType.Equals("Profit"))
            //{
            //    busProfitDetails = salesDataService.GetBusProfitDetails(busSalesEnqSrc);
            //}

            //List<BusAllYearEntity> BusAllYearList = new List<BusAllYearEntity>();

            //BusAllYearList.Add(new BusAllYearEntity()
            //{
            //   Header = "Tonnes:",
            //   CurrentYear = busSalesDetails.SalesVolP1,
            //   LastYear = busSalesDetails.SalesVolPp1, 
            //   Year1 = busSalesDetails.SalesVolYr1,
            //   Year2 = busSalesDetails.SalesVolYr2,
            //   Year3 = busSalesDetails.SalesVolYr3,
            //   Year4 = busSalesDetails.SalesVolYr4
            //});

            //BusAllYearList.Add(new BusAllYearEntity()
            //{
            //    Header = "Sales:",
            //    CurrentYear = busSalesDetails.SalesValP1,
            //    LastYear = busSalesDetails.SalesValPp1,
            //    Year1 = busSalesDetails.SalesValYr1,
            //    Year2 = busSalesDetails.SalesValYr2,
            //    Year3 = busSalesDetails.SalesValYr3,
            //    Year4 = busSalesDetails.SalesValYr4
            //});

            //BusAllYearList.Add(new BusAllYearEntity()
            //{
            //    Header = "Budget (T):",
            //    CurrentYear = busBudgetDetails.SalesVolP1,
            //    LastYear = busBudgetDetails.SalesVolPp1,
            //    Year1 = busBudgetDetails.SalesVolYr1,
            //    Year2 = busBudgetDetails.SalesVolYr2,
            //    Year3 = busBudgetDetails.SalesVolYr3,
            //    Year4 = busBudgetDetails.SalesVolYr4
            //});

            //BusAllYearList.Add(new BusAllYearEntity()
            //{
            //    Header = "Budget ($):",
            //    CurrentYear = busBudgetDetails.SalesValP1,
            //    LastYear = busBudgetDetails.SalesValPp1,
            //    Year1 = busBudgetDetails.SalesValYr1,
            //    Year2 = busBudgetDetails.SalesValYr2,
            //    Year3 = busBudgetDetails.SalesValYr3,
            //    Year4 = busBudgetDetails.SalesValYr4
            //});

            //if (historyType.Equals("Profit"))
            //{
            //    BusAllYearList.Add(new BusAllYearEntity()
            //    {
            //        Header = "Profit:",
            //        CurrentYear = busProfitDetails.SalesValP1,
            //        LastYear = busProfitDetails.SalesValPp1,
            //        Year1 = busProfitDetails.SalesValYr1,
            //        Year2 = busProfitDetails.SalesValYr2,
            //        Year3 = busProfitDetails.SalesValYr3,
            //        Year4 = busProfitDetails.SalesValYr4
            //    });
            //}

            //return BusAllYearList;
            return null;
        }

        #endregion

        public int UploadSalesTargets(string FilePath, string Extension, string UserName)
        {
            try
            {
                //return customerDataService.UploadExcelfile(FilePath, Extension, UserName);
                return salesDataService.UploadSalesTargets(FilePath, Extension, UserName);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public List<VisitNoteEntity> GetDailyRepsSalesforASM(string accessToken, string date)
        {
            List<VisitNoteEntity> InvoiceHeaderList = new List<VisitNoteEntity>();
            try
            {
                VisitNoteDAO InvoiceDetailService = new VisitNoteDAO();
                InvoiceSchemeDAO InvoiceSchemeService = new InvoiceSchemeDAO();

                InvoiceHeaderList = InvoiceDetailService.GetVisitNotesByAccessTokenforASM(accessToken, date);
            }
            catch
            {
                throw;
            }

            return InvoiceHeaderList;
        }

        public double GetTodaySales(string userName)
        {
            double salesVal = 0;
            try
            {
                //salesVal = salesDataService.GetVisitNotesByAccessTokenforASM(accessToken, date);
                salesVal = 5000;
            }
            catch
            {
                throw;
            }

            return salesVal;
        }
    }
}
