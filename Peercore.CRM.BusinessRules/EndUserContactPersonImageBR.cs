﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class EndUserContactPersonImageBR
    {
        private static EndUserContactPersonImageBR instance = null;
        private static object syncLock = new Object();

        private EndUserContactPersonImageDAO EndUserContactPersonImageDataService;

        public static EndUserContactPersonImageBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new EndUserContactPersonImageBR();
                }
                return instance;
            }
        }

        private EndUserContactPersonImageBR()
        {
            EndUserContactPersonImageDataService = new EndUserContactPersonImageDAO();
        }

        #region - Init Object -

        public EndUserContactPersonImageEntity CreateObject()
        {
            return EndUserContactPersonImageDataService.CreateObject();
        }

        public EndUserContactPersonImageEntity CreateObject(int entityId)
        {
            return EndUserContactPersonImageDataService.CreateObject(entityId);
        }

        #endregion

        public bool Save(EndUserContactPersonImageEntity contactImage,ref int contactPersonImageId)
        {
            try
            {
                bool isSuccess = false;
                if (contactImage.ContactPersonImageId > 0)
                {
                    isSuccess = EndUserContactPersonImageDataService.UpdateEndUserContactImage(contactImage);
                    contactPersonImageId = contactImage.ContactPersonImageId;
                }
                else
                {
                    isSuccess = EndUserContactPersonImageDataService.InsertEndUserContactImage(contactImage,ref contactPersonImageId);
                }
                return isSuccess;
            }
            catch
            {
                throw;
            }
        }

        public EndUserContactPersonImageEntity GetContactPersonImage(int iContactPersonID, string iEndUserCode)
        {
            try
            {
                return EndUserContactPersonImageDataService.GetContactPersonImage(iContactPersonID, iEndUserCode);
            }
            catch
            {
                throw;
            }
        }
    }
}
