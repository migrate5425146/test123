﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class ObjectiveBR
    {
        private static ObjectiveBR instance = null;
        private static object syncLock = new Object();

        private ObjectiveDAO ObjectiveDataService;

        public static ObjectiveBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new ObjectiveBR();
                }
                return instance;
            }
        }

        private ObjectiveBR()
        {
            ObjectiveDataService = new ObjectiveDAO();
        }

        #region - Init Object -

        public ObjectiveEntity CreateObject()
        {
            return ObjectiveDataService.CreateObject();
        }

        public ObjectiveEntity CreateObject(int entityId)
        {
            return ObjectiveDataService.CreateObject(entityId);
        }

        #endregion

        public List<ObjectiveEntity> GetAllObjectives(ArgsEntity args)
        {
            try
            {
                return ObjectiveDataService.GetAllObjectives(args);
            }
            catch
            {
                throw;
            }
        }

        public List<ObjectiveEntity> GetCustomerObjectives(ArgsEntity args)
        {
            try
            {
                return ObjectiveDataService.GetCustomerObjectives(args);
            }
            catch
            {
                throw;
            }
        }

        public List<MobileObjectiveEntity> GetAllNationalObjectives()
        {
            try
            {
                return ObjectiveDataService.GetAllNationalObjectives();
            }
            catch
            {
                throw;
            }
        }

        public List<MobileObjectiveEntity> GetAllObjectivesForMarket(int marketId)
        {
            try
            {
                return ObjectiveDataService.GetAllObjectivesForMarket(marketId);
            }
            catch
            {
                throw;
            }
        }

        public bool IsObjectiveCodeExists(ObjectiveEntity objectiveEntity)
        {
            try
            {
                return ObjectiveDataService.IsObjectiveCodeExists(objectiveEntity);
            }
            catch
            {
                throw;
            }
        }

        public bool InsertCustomerObjectives(List<MobileObjectiveEntity> ObjectiveEntityList)
        {
            DbWorkflowScope transactionScope = ObjectiveDataService.WorkflowScope;
            ObjectiveDAO objectiveDAO = new ObjectiveDAO(transactionScope);
            bool isSuccess = false;
            try
            {
                if (ObjectiveEntityList.Count != 0)
                {
                    using (transactionScope)
                    {
                        isSuccess = objectiveDAO.DeleteAllCustomerObjectivesForToday(ObjectiveEntityList[0]);
                        foreach (MobileObjectiveEntity objectiveEntity in ObjectiveEntityList)
                        {
                            isSuccess = objectiveDAO.InsertCustomerObjective(objectiveEntity);
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }
    }
}
