﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using System.Data;
using System.Data.OleDb;
using Peercore.CRM.Model;

namespace Peercore.CRM.BusinessRules
{
    public class LoadStockBR
    {
        private static LoadStockBR instance = null;
        private static object syncLock = new Object();
        private LoadStockDAO LoadStockService;


        public static LoadStockBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new LoadStockBR();
                }
                return instance;
            }
        }

        #region - Init Object -

        public LoadStockEntity CreateObject()
        {
            return LoadStockService.CreateObject();
        }

        //public OriginatorEntity CreateObject(int entityId)
        //{
        //    return LoadStockService.CreateObject(entityId);
        //}

        #endregion

        #region - Constructor -

        private LoadStockBR()
        {
            LoadStockService = new LoadStockDAO();
        }

        #endregion

        //public bool UnLoadStock(LoadStockDetailEntity loadStockDetailEntity, DateTime allocDate)
        //{
        //    bool successful1 = false;
        //    bool successful2 = false;
        //    DbWorkflowScope StockDetailScope = LoadStockService.WorkflowScope;
        //    try
        //    {
        //        using (StockDetailScope)
        //        {
        //            if (loadStockDetailEntity != null)
        //            {
        //                if (loadStockDetailEntity.StockHeaderId != 0)
        //                {
        //                    //Update Stock Detail
        //                    successful1 = LoadStockService.UpdateStockDetail(loadStockDetailEntity);

        //                    //Insert to Stock Movement an Unload Entry
        //                    successful2 = LoadStockService.InsertStockLoaddedMovement(loadStockDetailEntity.StockHeaderId, loadStockDetailEntity, "UNLOADED", allocDate);
        //                }
        //            }

        //            if (successful1 && successful2)
        //                StockDetailScope.Commit();
        //            else
        //                StockDetailScope.Rollback();
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return (successful1 && successful2);
        //}



        public LoadStockEntity GetProductForStockAdjustment(DateTime allocDate, int distributorId, int divisionId, ArgsEntity args)
        {
            try
            {
                LoadStockEntity loadStockEntity = LoadStockService.GetProductForStockAdjustment(allocDate, distributorId, divisionId, args);
                foreach (LoadStockDetailEntity item in loadStockEntity.lstLoadStockDetail)
                {
                    item.StockHeaderId = (LoadStockService.GetStockHeaderIdByTerritoryId(allocDate, distributorId).StockId);
                }
                return loadStockEntity;
            }
            catch
            {
                throw;
            }
        }

        public List<RouteEntity> GetAllRoutes(string name)
        {
            try
            {
                return LoadStockService.GetAllRoutes(name);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<OriginatorEntity> GetRepsByRouteMasterId(string name, int routeMasterId)
        {
            try
            {
                return LoadStockService.GetRepsByRouteMasterId(name, routeMasterId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<LoadStockDetailEntity> GetStockUpdateForRep(string repCode)
        {
            try
            {
                return LoadStockService.GetStockUpdateForRep(repCode);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<RouteEntity> GetRouteRepLikeRouteName(string name, int isTemp, string allocationDate)
        {
            try
            {
                return LoadStockService.GetRouteRepLikeRouteName(name, isTemp, allocationDate);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<StockTotalEntity> GetStockByAccessToken(string accessToken)
        {
            try
            {
                return LoadStockService.GetStockByAccessToken(accessToken);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool UploadStockToDistributor(List<ProductEntity> lstProduct, int distributorId, LoadStockEntity lstLoadStock)
        {
            //LoadStockEntity loadStockEntity = LoadStockEntity.CreateObject();
            //LoadStockDetailEntity loadstockdetentity = LoadStockDetailEntity.CreateObject();

            //DbWorkflowScope transactionScope = LoadStockService.WorkflowScope;
            //bool isSuccess = true;
            //int stockId = 0;
            //bool successful = false;

            //try
            //{
            //    loadStockEntity = null;// LoadStockService.GetStockHeaderIdByTerritoryId(lstLoadStock.AllocDate, lstLoadStock.DistributorId);

            //    if (lstLoadStock != null && lstProduct.Count > 0)
            //    {
            //        if (loadStockEntity.StockId == 0)//New Allocation
            //        {
            //            using (transactionScope)
            //            {
            //                ProductDAO ProductService = new ProductDAO(transactionScope);
            //                LoadStockDAO loadStockService = new LoadStockDAO(transactionScope);
            //                try
            //                {
            //                    //Insert to Stock Header and returns stock_header_id
            //                    LoadStockService.SaveStockHeader(out stockId, lstLoadStock);

            //                    if (stockId > 0)
            //                    {
            //                        foreach (ProductEntity item in lstProduct)
            //                        {
            //                            if (item.Qty != 0)
            //                            {
            //                                loadstockdetentity.CatalogCode = item.Code;
            //                                loadstockdetentity.AllocQty = item.Qty;
            //                                loadstockdetentity.LoadQty = 0;
            //                                loadstockdetentity.BalanceQty = item.Qty;
            //                                loadstockdetentity.VehicleBalanceQty = 0;
            //                                loadstockdetentity.VehicleReturnQty = 0;
            //                                loadstockdetentity.ReturnQty = 0;
            //                                loadstockdetentity.StockHeaderId = stockId;

            //                                successful = LoadStockService.InsertStockDetail(stockId, loadstockdetentity);

            //                                ProductEntity product = ProductService.GetProductByCode(item.Code);
            //                                StockTotalEntity stocktotEntity = StockTotalEntity.CreateObject();

            //                                if (product.ProductId != 0)
            //                                {
            //                                    stocktotEntity.CatalogId = product.ProductId;

            //                                    //item.AllocQty = row.Qty;
            //                                    stocktotEntity.DistributorId = distributorId;

            //                                    //item.StockTotalId = loadStockService.GetStockTotalByDistributorIdAndProductId(distributorId, product.ProductId);
            //                                    StockTotalEntity stockTotalEntity = loadStockService.GetStockTotalByDistributorIdAndProductId(distributorId, product.ProductId);

            //                                    if (stockTotalEntity != null)
            //                                    {
            //                                        stocktotEntity.StockTotalId = stockTotalEntity.StockTotalId;
            //                                        stocktotEntity.AllocQty = item.Qty + stockTotalEntity.AllocQty;
            //                                    }
            //                                    else
            //                                    {
            //                                        stocktotEntity.AllocQty = item.Qty;
            //                                    }

            //                                    if (stocktotEntity.StockTotalId == 0)
            //                                    {
            //                                        loadStockService.InsertStockTotal(stocktotEntity);
            //                                    }
            //                                    else
            //                                    {
            //                                        loadStockService.UpdateStockTotal(stocktotEntity);
            //                                    }
            //                                }
            //                            }
            //                        }
            //                    }
            //                }
            //                catch
            //                {
            //                    transactionScope.Rollback();
            //                    throw;
            //                }
            //                if (isSuccess)
            //                    transactionScope.Commit();
            //                else
            //                    transactionScope.Rollback();
            //            }
            //        }
            //        else//Update
            //        {
            //            using (transactionScope)
            //            {
            //                ProductDAO ProductService = new ProductDAO(transactionScope);
            //                LoadStockDAO loadStockService = new LoadStockDAO(transactionScope);
            //                try
            //                {
            //                    foreach (ProductEntity item in lstProduct)
            //                    {
            //                        if (item.Qty != 0)
            //                        {
            //                            loadstockdetentity.CatalogCode = item.Code;
            //                            loadstockdetentity.AllocQty = item.Qty;
            //                            loadstockdetentity.LoadQty = 0;
            //                            loadstockdetentity.BalanceQty = item.Qty;
            //                            loadstockdetentity.VehicleBalanceQty = 0;
            //                            loadstockdetentity.VehicleReturnQty = 0;
            //                            loadstockdetentity.ReturnQty = 0;
            //                            loadstockdetentity.StockHeaderId = loadStockEntity.StockId;

            //                            successful = LoadStockService.InsertStockDetail(loadStockEntity.StockId, loadstockdetentity);

            //                            ProductEntity product = ProductService.GetProductByCode(item.Code);
            //                            StockTotalEntity stocktotEntity = StockTotalEntity.CreateObject();

            //                            if (product.ProductId != 0)
            //                            {
            //                                stocktotEntity.CatalogId = product.ProductId;

            //                                //item.AllocQty = row.Qty;
            //                                stocktotEntity.DistributorId = distributorId;

            //                                //item.StockTotalId = loadStockService.GetStockTotalByDistributorIdAndProductId(distributorId, product.ProductId);
            //                                StockTotalEntity stockTotalEntity = loadStockService.GetStockTotalByDistributorIdAndProductId(distributorId, product.ProductId);

            //                                if (stockTotalEntity != null)
            //                                {
            //                                    stocktotEntity.StockTotalId = stockTotalEntity.StockTotalId;
            //                                    stocktotEntity.AllocQty = item.Qty + stockTotalEntity.AllocQty;
            //                                }
            //                                else
            //                                {
            //                                    stocktotEntity.AllocQty = item.Qty;
            //                                }

            //                                if (stocktotEntity.StockTotalId == 0)
            //                                {
            //                                    loadStockService.InsertStockTotal(stocktotEntity);
            //                                }
            //                                else
            //                                {
            //                                    loadStockService.UpdateStockTotal(stocktotEntity);
            //                                }
            //                            }
            //                        }
            //                    }
            //                }
            //                catch
            //                {
            //                    transactionScope.Rollback();
            //                    throw;
            //                }
            //                if (isSuccess)
            //                    transactionScope.Commit();
            //                else
            //                    transactionScope.Rollback();
            //            }
            //        }
            //    }
            //}
            //catch
            //{
            //    throw;
            //}
            //return isSuccess;

            //Irosh 2nd Phase
            return false;
        }



        #region "2nd Phase Development"

        public ProductStockModel GetAllProductStockByTerritoryId(ArgsModel args, DateTime allocDate, int territoryId)
        {
            try
            {
                ProductStockModel loadStockEntity = LoadStockService.GetAllProductStockByTerritoryId(args, allocDate, territoryId);

                //foreach (LoadStockDetailModel item in loadStockEntity.LstLoadStockDetail)
                //{
                //    item.StockHeaderId = (LoadStockService.GetStockHeaderDetail(allocDate, 0).StockId);
                //}

                return loadStockEntity;
            }
            catch
            {
                throw;
            }
        }

        public bool SaveLoadStock(ProductStockModel lstLoadStock)
        {
            bool successful = false;
            int stockId = 0;
            ProductStockModel loadStockEntity = new ProductStockModel();

            try
            {
                loadStockEntity = LoadStockService.GetStockHeaderIdByTerritoryId(lstLoadStock.AllocDate, lstLoadStock.TerritoryId);

                if (lstLoadStock != null && lstLoadStock.LstLoadStockDetail.Count > 0)
                {
                    if (loadStockEntity.StockId == 0)//New Allocation
                    {
                        //Insert to Stock Header and returns stock_header_id
                        LoadStockService.SaveStockHeader(out stockId, lstLoadStock);

                        if (stockId > 0)
                        {
                            foreach (ProductStockDetailModel item in lstLoadStock.LstLoadStockDetail)
                            {
                                if (item.AllocQty != 0)
                                {
                                    successful = LoadStockService.InsertStockDetail(stockId, item);

                                    //Stock Total
                                    StockTotalModel stockTotal = new StockTotalModel();
                                    stockTotal.StockTotalId = item.StockTotalId;
                                    stockTotal.AllocQty = item.AllocationQtyTotal + item.AllocQty;
                                    stockTotal.CatalogId = item.CatalogId;
                                    stockTotal.CreatedBy = lstLoadStock.CreatedBy;
                                    stockTotal.CreatedBy = lstLoadStock.LastModifiedBy;
                                    stockTotal.TerritoryId = lstLoadStock.TerritoryId;

                                    if (stockTotal.StockTotalId > 0)
                                    {
                                        successful = LoadStockService.UpdateStockTotal(stockTotal);
                                    }
                                    else
                                    {
                                        successful = LoadStockService.InsertStockTotal(stockTotal);
                                    }

                                    if (successful)
                                    {
                                        StockMovementModel stockMovementTotal = new StockMovementModel();
                                        stockMovementTotal.TerritoryId = item.TerritoryId;
                                        stockMovementTotal.CatalogId = item.CatalogId;
                                        stockMovementTotal.AllocQty = item.AllocQty;
                                        stockMovementTotal.CatalogPrice = 0;
                                        stockMovementTotal.TransRefId = loadStockEntity.StockId;
                                        stockMovementTotal.AllocDate = lstLoadStock.AllocDate;
                                        if (lstLoadStock.StockAllocType == "G")
                                            stockMovementTotal.TransType = "GRN";
                                        else if (lstLoadStock.StockAllocType == "A")
                                            stockMovementTotal.TransType = "ADJ";

                                        successful = LoadStockService.InsertStockMovement(stockMovementTotal);
                                    }
                                }
                            }
                        }
                    }
                    else//Update
                    {
                        foreach (ProductStockDetailModel item in lstLoadStock.LstLoadStockDetail)
                        {
                            if (item.StockDetailId == 0)//New Stock Deatil
                            {
                                //Insert to Stock Detail
                                if (item.AllocQty != 0)
                                {
                                    successful = LoadStockService.InsertStockDetail(loadStockEntity.StockId, item);
                                }
                            }
                            else//Stock Detail Exists
                            {
                                //Update existing stock detail
                                successful = LoadStockService.UpdateStockDetail(item);
                            }

                            //Stock Total
                            if (item.AllocQty != 0)
                            {
                                StockTotalModel stockTotal = new StockTotalModel();
                                stockTotal.StockTotalId = item.StockTotalId;
                                stockTotal.AllocQty = item.AllocationQtyTotal + item.AllocQty;
                                stockTotal.CatalogId = item.CatalogId;
                                stockTotal.CreatedBy = lstLoadStock.CreatedBy;
                                stockTotal.CreatedBy = lstLoadStock.LastModifiedBy;
                                stockTotal.TerritoryId = lstLoadStock.TerritoryId;

                                if (stockTotal.StockTotalId > 0)
                                {
                                    successful = LoadStockService.UpdateStockTotal(stockTotal);
                                }
                                else
                                {
                                    successful = LoadStockService.InsertStockTotal(stockTotal);
                                }

                                if (successful)
                                {
                                    StockMovementModel stockMovementTotal = new StockMovementModel();
                                    stockMovementTotal.TerritoryId = item.TerritoryId;
                                    stockMovementTotal.CatalogId = item.CatalogId;
                                    stockMovementTotal.AllocQty = item.AllocQty;
                                    stockMovementTotal.CatalogPrice = 0;
                                    stockMovementTotal.TransRefId = loadStockEntity.StockId;
                                    stockMovementTotal.AllocDate = lstLoadStock.AllocDate;
                                    if (lstLoadStock.StockAllocType == "G")
                                        stockMovementTotal.TransType = "GRN";
                                    else if (lstLoadStock.StockAllocType == "A")
                                        stockMovementTotal.TransType = "ADJ";

                                    successful = LoadStockService.InsertStockMovement(stockMovementTotal);
                                }
                            }
                        }
                    }
                }
            }
            catch
            {
                throw;
            }
            return successful;
        }

        public bool UploadStockToBulkTerritory(List<ProductModel> lstProduct, ProductStockModel lstLoadStock)
        {
            ProductStockModel loadStockEntity = new ProductStockModel();
            ProductStockDetailModel loadstockdetentity = new ProductStockDetailModel();

            //DbWorkflowScope transactionScope = LoadStockService.WorkflowScope;
            bool isSuccess = true;
            int stockId = 0;
            bool successful = false;

            try
            {
                TerritoryDAO TerritoryDataService = new TerritoryDAO();
                ProductDAO ProductService = new ProductDAO();
                LoadStockDAO loadStockService = new LoadStockDAO();

                //using (transactionScope)
                //{
                if (lstLoadStock != null && lstProduct.Count > 0)
                {
                    foreach (ProductModel item in lstProduct)
                    {
                        if (item.CustomerCode != "territory")
                        {
                            try
                            {
                                TerritoryModel terrEntity = TerritoryDataService.GetTerritoryByTerritoryCode(item.CustomerCode);
                                loadStockEntity = LoadStockService.GetStockHeaderIdByTerritoryId(lstLoadStock.AllocDate, terrEntity.TerritoryId);

                                if (loadStockEntity.StockId == 0)//New Allocation
                                {
                                    try
                                    {
                                        lstLoadStock.TerritoryId = terrEntity.TerritoryId;
                                        lstLoadStock.StockAllocType = "G";

                                        successful = LoadStockService.SaveStockHeader(out stockId, lstLoadStock);

                                        if (successful)
                                        {
                                            if (stockId > 0)
                                            {
                                                if (item.Qty != 0)
                                                {
                                                    loadstockdetentity.CatalogCode = item.Code;
                                                    loadstockdetentity.AllocQty = item.Qty;
                                                    loadstockdetentity.LoadQty = 0;
                                                    loadstockdetentity.BalanceQty = item.Qty;
                                                    loadstockdetentity.VehicleBalanceQty = 0;
                                                    loadstockdetentity.VehicleReturnQty = 0;
                                                    loadstockdetentity.ReturnQty = 0;
                                                    loadstockdetentity.StockHeaderId = stockId;

                                                    successful = LoadStockService.InsertStockDetail(stockId, loadstockdetentity);

                                                    ProductModel product = ProductService.GetProductByProductCode(item.Code);
                                                    StockTotalModel stocktotEntity = new StockTotalModel();

                                                    if (product.ProductId != 0)
                                                    {
                                                        stocktotEntity.CatalogId = product.ProductId;
                                                        stocktotEntity.TerritoryId = terrEntity.TerritoryId;
                                                        stocktotEntity.CreatedBy = lstLoadStock.CreatedBy;
                                                        stocktotEntity.LastModifiedBy = lstLoadStock.LastModifiedBy;

                                                        StockTotalModel stockTotalEntity = loadStockService.GetStockTotalByTerritoryIdAndProductId(terrEntity.TerritoryId, product.ProductId);

                                                        if (stockTotalEntity != null)
                                                        {
                                                            stocktotEntity.StockTotalId = stockTotalEntity.StockTotalId;
                                                            stocktotEntity.AllocQty = item.Qty + stockTotalEntity.AllocQty;
                                                        }
                                                        else
                                                        {
                                                            stocktotEntity.AllocQty = item.Qty;
                                                        }

                                                        if (stocktotEntity.StockTotalId == 0)
                                                        {
                                                            successful = loadStockService.InsertStockTotal(stocktotEntity);
                                                        }
                                                        else
                                                        {
                                                            successful = loadStockService.UpdateStockTotal(stocktotEntity);
                                                        }

                                                        if (successful)
                                                        {
                                                            StockMovementModel stockMovementTotal = new StockMovementModel();
                                                            stockMovementTotal.TerritoryId = terrEntity.TerritoryId;
                                                            stockMovementTotal.CatalogId = product.ProductId;
                                                            stockMovementTotal.AllocQty = item.Qty;
                                                            stockMovementTotal.CatalogPrice = 0;
                                                            stockMovementTotal.TransRefId = stockId;
                                                            stockMovementTotal.AllocDate = lstLoadStock.AllocDate;
                                                            lstLoadStock.StockAllocType = "G";
                                                            if (lstLoadStock.StockAllocType == "G")
                                                                stockMovementTotal.TransType = "GRN";
                                                            else if (lstLoadStock.StockAllocType == "A")
                                                                stockMovementTotal.TransType = "ADJ";

                                                            successful = LoadStockService.InsertStockMovement(stockMovementTotal);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                    catch
                                    {
                                        //transactionScope.Rollback();
                                        throw;
                                    }
                                }
                                else//Update
                                {
                                    try
                                    {
                                        if (item.Qty != 0)
                                        {
                                            loadstockdetentity.CatalogCode = item.Code;
                                            loadstockdetentity.AllocQty = item.Qty;
                                            loadstockdetentity.LoadQty = 0;
                                            loadstockdetentity.BalanceQty = item.Qty;
                                            loadstockdetentity.VehicleBalanceQty = 0;
                                            loadstockdetentity.VehicleReturnQty = 0;
                                            loadstockdetentity.ReturnQty = 0;
                                            loadstockdetentity.StockHeaderId = loadStockEntity.StockId;

                                            successful = LoadStockService.InsertStockDetail(loadStockEntity.StockId, loadstockdetentity);

                                            ProductModel product = ProductService.GetProductByProductCode(item.Code);
                                            StockTotalModel stocktotEntity = new StockTotalModel();

                                            if (product.ProductId != 0)
                                            {
                                                stocktotEntity.CatalogId = product.ProductId;
                                                stocktotEntity.TerritoryId = terrEntity.TerritoryId;
                                                stocktotEntity.CreatedBy = lstLoadStock.CreatedBy;
                                                stocktotEntity.LastModifiedBy = lstLoadStock.LastModifiedBy;

                                                StockTotalModel stockTotalEntity = loadStockService.GetStockTotalByTerritoryIdAndProductId(terrEntity.TerritoryId, product.ProductId);

                                                if (stockTotalEntity != null)
                                                {
                                                    stocktotEntity.StockTotalId = stockTotalEntity.StockTotalId;
                                                    stocktotEntity.AllocQty = item.Qty + stockTotalEntity.AllocQty;
                                                }
                                                else
                                                {
                                                    stocktotEntity.AllocQty = item.Qty;
                                                }

                                                if (stocktotEntity.StockTotalId == 0)
                                                {
                                                    successful = loadStockService.InsertStockTotal(stocktotEntity);
                                                }
                                                else
                                                {
                                                    successful = loadStockService.UpdateStockTotal(stocktotEntity);
                                                }

                                                if (successful)
                                                {
                                                    StockMovementModel stockMovementTotal = new StockMovementModel();
                                                    stockMovementTotal.TerritoryId = terrEntity.TerritoryId;
                                                    stockMovementTotal.CatalogId = product.ProductId;
                                                    stockMovementTotal.AllocQty = item.Qty;
                                                    stockMovementTotal.CatalogPrice = 0;
                                                    stockMovementTotal.TransRefId = stockId;
                                                    stockMovementTotal.AllocDate = lstLoadStock.AllocDate;
                                                    lstLoadStock.StockAllocType = "G";
                                                    if (lstLoadStock.StockAllocType == "G")
                                                        stockMovementTotal.TransType = "GRN";
                                                    else if (lstLoadStock.StockAllocType == "A")
                                                        stockMovementTotal.TransType = "ADJ";

                                                    successful = LoadStockService.InsertStockMovement(stockMovementTotal);
                                                }
                                            }
                                        }
                                    }
                                    catch
                                    {
                                        //transactionScope.Rollback();
                                        throw;
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                ///transactionScope.Rollback();
                                throw;
                            }
                        }
                    }

                    //if (isSuccess)
                    //{
                    //    transactionScope.Commit();
                    //}
                    //else
                    //{
                    //    transactionScope.Rollback();
                    //}
                }
                //}
            }
            catch { throw; }

            return isSuccess;
        }

        #endregion
        
        public List<DBClaimInvoiceDetailsModel> GetAllDBClaimInvoiceDetailsByInvoiceId(ArgsModel args, string invoiceId)
        {
            try
            {
                return LoadStockService.GetAllDBClaimInvoiceDetailsByInvoiceId(args, invoiceId);
            }
            catch
            {
                throw;
            }
        }
    }
}
