﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class OutletTypeBR
    {
        private static OutletTypeBR instance = null;
        private static object syncLock = new Object();

        private OutletTypeDAO OutletTypeDataService;

        public static OutletTypeBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new OutletTypeBR();
                }
                return instance;
            }
        }

        private OutletTypeBR()
        {
            OutletTypeDataService = new OutletTypeDAO();
        }

        #region - Init Object -

        public OutletTypeEntity CreateObject()
        {
            return OutletTypeDataService.CreateObject();
        }

        public OutletTypeEntity CreateObject(int entityId)
        {
            return OutletTypeDataService.CreateObject(entityId);
        }

        #endregion

        public List<OutletTypeEntity> GetAllOutletTypeByAccessToken(string accessToken)
        {
            try
            {
                return OutletTypeDataService.GetAllOutletTypeByAccessToken(accessToken);
            }
            catch
            {
                throw;
            }
        }

        public List<OutletTypeEntity> GetAllOutletType()
        {
            try
            {
                return OutletTypeDataService.GetAllOutletType();
            }
            catch
            {
                throw;
            }
        }

        public bool IsOutletTypeExists(OutletTypeEntity outlettypeEntity)
        {
            try
            {
                return OutletTypeDataService.IsOutletTypeExists(outlettypeEntity);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveOutletType(List<OutletTypeEntity> OutletTypeEntityList)
        {
            DbWorkflowScope transactionScope = OutletTypeDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                if (OutletTypeEntityList.Count != 0)
                {
                    using (transactionScope)
                    {
                        foreach (OutletTypeEntity OutletTypeEntity in OutletTypeEntityList)
                        {
                            if (OutletTypeEntity.OutletTypeId > 0)
                            {
                                isSuccess = OutletTypeDataService.UpdateOutletType(OutletTypeEntity);
                            }
                            else
                            {
                                isSuccess = OutletTypeDataService.InsertOutletType(OutletTypeEntity);
                            }
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool DeleteOutletType(int outletTypeId)
        {
            try
            {
                return OutletTypeDataService.DeleteOutletType(outletTypeId);
            }
            catch
            {
                throw;
            }
        }
    }
}
