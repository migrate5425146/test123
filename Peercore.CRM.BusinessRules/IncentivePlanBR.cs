﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class IncentivePlanBR
    {
        private static IncentivePlanBR instance = null;
        private static object syncLock = new Object();

        private IncentivePlanDAO IncentivePlanDataService;

        public static IncentivePlanBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new IncentivePlanBR();
                }
                return instance;
            }
        }

        private IncentivePlanBR()
        {
            IncentivePlanDataService = new IncentivePlanDAO();
        }

        public List<IncentivePlanHeaderEntity> GetAllIncentivePlans(ArgsEntity args)
        {
            try
            {
                List<IncentivePlanHeaderEntity> headerList = new List<IncentivePlanHeaderEntity>();

                //Get All Headers
                headerList = IncentivePlanDataService.GetAllIncentivePlanHeader(args);
                if (headerList.Count > 0)
                {
                    foreach (IncentivePlanHeaderEntity header in headerList)
                    {
                        List<IncentivePlanDetailsEntity> detailList = new List<IncentivePlanDetailsEntity>();
                        //Get All details for each header
                        detailList = IncentivePlanDataService.GetIncentivePlanDetailByHeaderId(header.IncentivePlanHeaderId);

                        if (detailList.Count > 0)
                        {
                            foreach (IncentivePlanDetailsEntity detail in detailList)
                            {
                                List<IncentiveSecondarySalesWeeklyEntity> weeklyList = new List<IncentiveSecondarySalesWeeklyEntity>();
                                //Get All Weekly Records for each detail
                                weeklyList = IncentivePlanDataService.GetIncentiveSecondarySalesWeeklyByDetailId(detail.IncentivePlanDetailsId);

                                List<IncentiveSecondarySalesPartialEntity> partialList = new List<IncentiveSecondarySalesPartialEntity>();
                                //Get All partial Records for each detail
                                partialList = IncentivePlanDataService.GetIncentiveSecondarySalesPartialByDetailId(detail.IncentivePlanDetailsId);

                                //Set Properties for Detail Entity
                                detail.IncentiveSecondarySalesWeeklyList = weeklyList;
                                detail.IncentiveSecondarySalesPartialList = partialList;
                            }
                        }

                        //Set Properties for Header Entity
                        header.IncentivePlanDetailsList = detailList;
                    }
                }

                return headerList;
            }
            catch
            {
                throw;
            }
        }

        public bool SaveTargetAndIncentiveNew(string TargetBreakDay, string First14Days, string Last14Days)
        {
            IncentivePlanDAO incentivePlanDAO = new IncentivePlanDAO();

            bool isSuccess = false;

            try
            {
                    isSuccess = incentivePlanDAO.SaveTargetAndIncentiveNew(TargetBreakDay, First14Days, Last14Days);
            }
            catch
            {
                throw;
            }

            return isSuccess;
        }

        public List<TargetEntity> LoadTargetMaster()
        {
            try
            {
                List<TargetEntity> headerList = new List<TargetEntity>();

                //Get All Headers
                headerList = IncentivePlanDataService.LoadTargetMaster();

                return headerList;
            }
            catch
            {
                throw;
            }
        }


        public bool SaveIncentivePlans(List<IncentivePlanHeaderEntity> incentivePlanHeaderList)
        {
            DbWorkflowScope transactionScope = IncentivePlanDataService.WorkflowScope;
            IncentivePlanDAO incentivePlanDAO = new IncentivePlanDAO(transactionScope);
            bool isHeaderSuccess = true;
            bool isDetailSuccess = true;
            bool isWeeklySuccess = true;
            bool isPartialSuccess = true;

            int incentiveHeaderId = 0;
            int incentiveDetailId = 0;

            try
            {
                if (incentivePlanHeaderList.Count != 0)
                {
                    using (transactionScope)
                    {
                        foreach (IncentivePlanHeaderEntity header in incentivePlanHeaderList)
                        {
                            if (header.IncentivePlanHeaderId > 0)
                            {
                                //Update Header
                                isHeaderSuccess = incentivePlanDAO.UpdateIncentivePlanHeader(header);
                            }
                            else
                            {
                                //Insert Header
                                isHeaderSuccess = incentivePlanDAO.InsertIncentivePlanHeader(out incentiveHeaderId, header);
                                header.IncentivePlanHeaderId = incentiveHeaderId;
                            }

                            if (isHeaderSuccess)
                            {
                                if (header.IncentivePlanDetailsList.Count > 0)
                                {
                                    foreach (IncentivePlanDetailsEntity detail in header.IncentivePlanDetailsList)
                                    {
                                        //Set header ID of Detail obj
                                        detail.IncentivePlanHeaderId = header.IncentivePlanHeaderId;

                                        detail.CreatedBy = header.CreatedBy;
                                        detail.LastModifiedBy = header.LastModifiedBy;

                                        if (detail.IncentivePlanDetailsId > 0)
                                        {
                                            //Update Detail
                                            isDetailSuccess = incentivePlanDAO.UpdateIncentivePlanDetail(detail);
                                        }
                                        else
                                        {
                                            //Insert Detail
                                            isDetailSuccess = incentivePlanDAO.InsertIncentivePlanDetail(out incentiveDetailId, detail);
                                            detail.IncentivePlanDetailsId = incentiveDetailId;
                                        }

                                        if (isDetailSuccess)
                                        {
                                            //Inserting Weekly List
                                            if (detail.IncentiveSecondarySalesWeeklyList.Count > 0)
                                            {
                                                foreach (IncentiveSecondarySalesWeeklyEntity weeklyEntry in detail.IncentiveSecondarySalesWeeklyList)
                                                {
                                                    //Set Detail ID of Weekly obj
                                                    weeklyEntry.IncentivePlanDetailsId = detail.IncentivePlanDetailsId;

                                                    weeklyEntry.CreatedBy = detail.CreatedBy;
                                                    weeklyEntry.LastModifiedBy = detail.LastModifiedBy;

                                                    if (weeklyEntry.SecondarySalesWeeklyId > 0)
                                                    {
                                                        //Update Weekly Entry
                                                        isWeeklySuccess = incentivePlanDAO.UpdateIncentiveSecondarySalesWeekly(weeklyEntry);
                                                    }
                                                    else
                                                    {
                                                        //Insert Weekly Entry
                                                        isWeeklySuccess = incentivePlanDAO.InsertIncentiveSecondarySalesWeekly(weeklyEntry);
                                                    }

                                                    if (!isWeeklySuccess)
                                                    {
                                                        isDetailSuccess = false;
                                                        break;
                                                    }
                                                }
                                            }

                                            //Inserting Partial List
                                            if (detail.IncentiveSecondarySalesPartialList.Count > 0)
                                            {
                                                foreach (IncentiveSecondarySalesPartialEntity partialEntry in detail.IncentiveSecondarySalesPartialList)
                                                {
                                                    //Set Detail ID of Partial obj
                                                    partialEntry.IncentivePlanDetailsId = detail.IncentivePlanDetailsId;

                                                    partialEntry.CreatedBy = detail.CreatedBy;
                                                    partialEntry.LastModifiedBy = detail.LastModifiedBy;

                                                    if (partialEntry.SecondarySalesPartialId > 0)
                                                    {
                                                        //Update Partial Entry
                                                        isPartialSuccess = incentivePlanDAO.UpdateIncentiveSecondarySalesPartial(partialEntry);
                                                    }
                                                    else
                                                    {
                                                        //Insert Partial Entry
                                                        isPartialSuccess = incentivePlanDAO.InsertIncentiveSecondarySalesPartial(partialEntry);
                                                    }

                                                    if (!isPartialSuccess)
                                                    {
                                                        isDetailSuccess = false;
                                                        break;
                                                    }
                                                }
                                            }
                                        }

                                        if (!isDetailSuccess)
                                        {
                                            isHeaderSuccess = false;
                                            break;
                                        }
                                    }
                                }
                            }

                            if (!isHeaderSuccess)
                                break;
                        }

                        if (isHeaderSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isHeaderSuccess;
        }

        public bool DeactivateSecondarySalesWeekly(IncentiveSecondarySalesWeeklyEntity weeklyEntity)
        {
            bool isSuccess = true;
            try
            {
                isSuccess = IncentivePlanDataService.DeactivateSecondarySalesWeekly(weeklyEntity);
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool DeactivateSecondarySalesPartial(IncentiveSecondarySalesPartialEntity partialEntity)
        {
            bool isSuccess = true;
            try
            {
                isSuccess = IncentivePlanDataService.DeactivateSecondarySalesPartial(partialEntity);
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }
    }
}
