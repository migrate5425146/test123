﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class ContactPersonImageBR
    {
        private static ContactPersonImageBR instance = null;
        private static object syncLock = new Object();
        private ContactPersonImageDAO contactPersonImageDataService;

        public static ContactPersonImageBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new ContactPersonImageBR();
                }
                return instance;
            }
        }

        #region - Init Object -

        public ContactPersonImageEntity CreateObject()
        {
            return contactPersonImageDataService.CreateObject();
        }

        public ContactPersonImageEntity CreateObject(int entityId)
        {
            return contactPersonImageDataService.CreateObject(entityId);
        }

        #endregion

        #region - Constructor -

        private ContactPersonImageBR()
        {
            contactPersonImageDataService = new ContactPersonImageDAO();
        } 
        #endregion

        #region - Public Methods - 
        
        public bool Save(ContactPersonImageEntity contactPersonImageEntity)
        {
            DbWorkflowScope transactionScope = contactPersonImageDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    if (contactPersonImageEntity.ContactPersonImageId == 0)
                    {
                        contactPersonImageEntity.ContactPersonImageId = contactPersonImageDataService.GetNextContactPersonImageID();
                        isSuccess = contactPersonImageDataService.InsertContactPersonImage(contactPersonImageEntity);
                    }
                    else
                    {
                        isSuccess = contactPersonImageDataService.UpdateContactPersonImage(contactPersonImageEntity);
                    }

                    
                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public ContactPersonImageEntity GetContactPersonImage(int contactPersonID, int leadId)
        {          
            try
            {
                return contactPersonImageDataService.GetContactPersonImage(contactPersonID, leadId);
            }
            catch
            {
                throw;
            }
            finally
            {
               
            }
        }

        public ContactPersonImageEntity GetContactPersonImage(int contactNo, string custCode)
        {          
            try
            {
                return contactPersonImageDataService.GetContactPersonImage(contactNo, custCode);
            }
            catch
            {
                throw;
            }
            finally
            {
                
            }
        }

        #endregion
    }
}
