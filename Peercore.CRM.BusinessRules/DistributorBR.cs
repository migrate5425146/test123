﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Entities;
using Peercore.CRM.DataAccess.DAO;
using Peercore.Workflow.Common;
using Peercore.CRM.Model;
using System.Data.SqlClient;

namespace Peercore.CRM.BusinessRules
{
    public class DistributorBR
    {
        private static DistributorBR instance = null;
        private static object syncLock = new Object();

        private DistributorDAO DistributorDataService;

        public static DistributorBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new DistributorBR();
                }
                return instance;
            }
        }

        private DistributorBR()
        {
            DistributorDataService = new DistributorDAO();
        }

        #region - Init Object -

        public DistributorEntity CreateObject()
        {
            return DistributorDataService.CreateObject();
        }

        public DistributorEntity CreateObject(int entityId)
        {
            return DistributorDataService.CreateObject(entityId);
        }

        #endregion

        public DistributorEntity GetDistributorTargetById(ArgsEntity args)
        {
            try
            {
                return DistributorDataService.GetDistributorTargetById(args);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveDistributorTargets(List<DistributorEntity> distributorTargetList)
        {
            DbWorkflowScope transactionScope = DistributorDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                if (distributorTargetList.Count != 0)
                {
                    using (transactionScope)
                    {
                        DistributorDataService = new DistributorDAO(transactionScope);

                        foreach (DistributorEntity distributorEntity in distributorTargetList)
                        {
                            if (distributorEntity.TargetId > 0)
                            {
                                isSuccess = DistributorDataService.UpdateDistributorTarget(distributorEntity);
                            }
                            else
                            {
                                if (distributorEntity.Target > 0)
                                    isSuccess = DistributorDataService.InsertDistributorTarget(distributorEntity);
                            }
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        //public bool SaveDistributor(DistributorEntity distributor)
        //{
        //    DbWorkflowScope transactionScope = DistributorDataService.WorkflowScope;
        //    bool isSuccess = true;

        //    try
        //    {
        //        using (transactionScope)
        //        {
        //            if (distributor.DistributorId > 0)
        //            {
        //                if (distributor.DeptString.Equals("DR"))
        //                {
        //                    isSuccess = DistributorDataService.UpdateRep(distributor);
        //                }
        //                else
        //                {
        //                    isSuccess = DistributorDataService.UpdateDistributor(distributor);
        //                }
        //            }
        //            else
        //            {
        //                if (distributor.DeptString.Equals("DR"))
        //                {
        //                    isSuccess = DistributorDataService.SaveRep(distributor);
        //                }
        //                else if (distributor.DeptString.Equals("ASE"))
        //                {
        //                    //distributor.ParentOriginator = "perfetti";
        //                    isSuccess = DistributorDataService.SaveDistributor(distributor);
        //                }
        //                else
        //                {
        //                    isSuccess = DistributorDataService.SaveDistributor(distributor);
        //                }
        //            }


        //            if (isSuccess)
        //            {
        //                if (distributor.DeptString.Equals("DR"))
        //                {
        //                    OriginatorEntity ori = OriginatorBR.Instance.GetOriginator(distributor.Originator);
        //                    distributor.DistributorId = ori.OriginatorId;
        //                }
        //                else
        //                {
        //                    distributor.DistributorId = DistributorDataService.GetDistributorIdByOriginatorName(distributor.Originator);
        //                }
        //                isSuccess = DistributorDataService.DeleteDistributerHolidays(distributor);
        //                foreach (string holiday in distributor.HolidaysList)
        //                {
        //                    distributor.Holiday = holiday;
        //                    isSuccess = DistributorDataService.SaveDistributorHoliday(distributor);
        //                }
        //            }

        //            if (isSuccess)
        //                transactionScope.Commit();
        //            else
        //                transactionScope.Rollback();
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return isSuccess;
        //}

        public string GetDistributorHoliday(int originatorId, string originator, string repCode)
        {
            try
            {
                return DistributorDataService.GetDistributorHoliday(originatorId, originator, repCode);
            }
            catch
            {
                throw;
            }
        }

        public DistributorEntity GetDistributorDetails(string originator)
        {
            try
            {
                DistributorEntity distributor = DistributorEntity.CreateObject();
                distributor = DistributorDataService.GetDistributorDetails(originator);

                List<string> holidayList = new List<string>();
                holidayList = DistributorDataService.GetDistributorHolidaysList(originator);

                List<BankAccountEntity> bankAccountsList = new List<BankAccountEntity>();
                bankAccountsList = BankAccountBR.Instance.GetBankAccountsForUser(distributor.DeptString, originator);

                distributor.HolidaysList = holidayList;
                distributor.BankAccountsList = bankAccountsList;

                return distributor;
            }
            catch
            {
                throw;
            }
        }

        #region "Perfetti 2nd Phase Development"
        public List<DistributerModel> GetAllDistributerByASM(string Asm)
        {
            try
            {
                return DistributorDataService.GetAllDistributerByASM(Asm);
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public List<DistributerModel> GetAllDistributerByOriginatorAndOriginatorType(ArgsModel args, string OriginatorType, string Originator)
        {
            try
            {
                return DistributorDataService.GetAllDistributerByOriginatorAndOriginatorType(OriginatorType, Originator);
            }
            catch
            {
                throw;
            }
        }

        public List<TradeDiscountUploadModel> GetAllTradeDiscounts(ArgsModel args, string dbCode)
        {
            try
            {
                return DistributorDataService.GetAllTradeDiscounts(args, dbCode);
            }
            catch
            {
                throw;
            }
        }

        public DistributerModel GetDistributorDetailsByOriginator(string Originator)
        {
            DbWorkflowScope transactionScope = DistributorDataService.WorkflowScope;

            try
            {
                return DistributorDataService.GetDistributorDetailsByOriginator(transactionScope, Originator);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveDistributor(DistributerModel distributor)
        {
            DbWorkflowScope transactionScope = DistributorDataService.WorkflowScope;

            bool isSuccess = true;

            try
            {
                using (SqlConnection conn = DistributorDataService.SqlConnOpen(transactionScope))
                {
                    using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                    {
                        if (distributor.DistributorId > 0)
                        {
                            isSuccess = DistributorDataService.UpdateDistributor(sqlTransaction, distributor);
                        }
                        else
                        {
                            isSuccess = DistributorDataService.SaveDistributor(sqlTransaction, distributor);
                        }

                        if (isSuccess)
                            sqlTransaction.Commit();
                        else
                            sqlTransaction.Rollback();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return isSuccess;
        }

        public bool SaveDistributorTerritory(string territory_id, string distributor_id, string UpdateBy)
        {
            DbWorkflowScope transactionScope = DistributorDataService.WorkflowScope;

            bool isSuccess = true;

            try
            {
                using (SqlConnection conn = DistributorDataService.SqlConnOpen(transactionScope))
                {
                    using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                    {
                        isSuccess = DistributorDataService.SaveDistributorTerritory(sqlTransaction, territory_id, distributor_id, UpdateBy);

                        if (isSuccess)
                            sqlTransaction.Commit();
                        else
                            sqlTransaction.Rollback();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return isSuccess;
        }

        #endregion

        public List<DBClaimInvoiceHeaderModel> GetDBClaimInvoicesByDistributerId(string distributerId)
        {
            DbWorkflowScope transactionScope = DistributorDataService.WorkflowScope;

            try
            {
                return DistributorDataService.GetDBClaimInvoicesByDistributerId(transactionScope, distributerId);
            }
            catch
            {
                throw;
            }
        }

        public List<TradeDiscountUploadModel> SaveTradeDiscount(List<TradeDiscountUploadModel> tradeDiscountList)
        {
            DbWorkflowScope transactionScope = DistributorDataService.WorkflowScope;

            bool isSuccess = true;

            try
            {
                using (SqlConnection conn = DistributorDataService.SqlConnOpen(transactionScope))
                {
                    using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                    {
                        try
                        {
                            foreach (var item in tradeDiscountList)
                            {
                                isSuccess = DistributorDataService.SaveTradeDiscount(sqlTransaction, item);
                                if (isSuccess)
                                {
                                    item.uploadStatus = "Success";
                                }
                            }

                            sqlTransaction.Commit();
                        }
                        catch (Exception ex) { sqlTransaction.Rollback(); }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return tradeDiscountList;
        }

        public bool DeleteTradeDiscount(int trdId, string originator)
        {
            DbWorkflowScope transactionScope = DistributorDataService.WorkflowScope;

            bool isSuccess = true;

            try
            {
                using (SqlConnection conn = DistributorDataService.SqlConnOpen(transactionScope))
                {
                    using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                    {
                        isSuccess = DistributorDataService.DeleteTradeDiscount(sqlTransaction, trdId, originator);

                        if (isSuccess)
                            sqlTransaction.Commit();
                        else
                            sqlTransaction.Rollback();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return isSuccess;
        }

        public bool UpdateTradeDiscount(TradeDiscountUploadModel tradeDiscount)
        {
            DbWorkflowScope transactionScope = DistributorDataService.WorkflowScope;

            bool isSuccess = true;

            try
            {
                using (SqlConnection conn = DistributorDataService.SqlConnOpen(transactionScope))
                {
                    using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                    {
                        isSuccess = DistributorDataService.UpdateTradeDiscount(sqlTransaction, tradeDiscount);

                        if (isSuccess)
                            sqlTransaction.Commit();
                        else
                            sqlTransaction.Rollback();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return isSuccess;
        } 
        
        public bool UpdateTradeDiscountApprovedProcess(string dtpMonth)
        {
            DbWorkflowScope transactionScope = DistributorDataService.WorkflowScope;

            bool isSuccess = true;

            try
            {
                using (SqlConnection conn = DistributorDataService.SqlConnOpen(transactionScope))
                {
                    using (SqlTransaction sqlTransaction = conn.BeginTransaction())
                    {
                        isSuccess = DistributorDataService.UpdateTradeDiscountApprovedProcess(sqlTransaction, dtpMonth);

                        if (isSuccess)
                            sqlTransaction.Commit();
                        else
                            sqlTransaction.Rollback();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return isSuccess;
        }
    }
}
