﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class ErrorBR
    {
        private static ErrorBR instance = null;
        private static object syncLock = new Object();
        private ErrorDAO ErrorDataService;

        public static ErrorBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new ErrorBR();
                }
                return instance;
            }
        }

        #region - Init Object -

        public ErrorEntity CreateObject()
        {
            return ErrorDataService.CreateObject();
        }

        public ErrorEntity CreateObject(int entityId)
        {
            return ErrorDataService.CreateObject(entityId);
        }

        #endregion

        #region - Constructor -

        private ErrorBR()
        {
            ErrorDataService = new ErrorDAO();
        }
        #endregion

        public int GetErrorCountForCurrentDate(string errorsource, string referer, string shortDesc, string methodname)
        {
            try
            {
                return ErrorDataService.GetErrorCountForCurrentDate(errorsource, referer, shortDesc, methodname);
            }
            catch
            {
                throw;
            }
        }

        public int SaveError(ErrorEntity error)
        {
            try
            {
                return ErrorDataService.SaveError(error);
            }
            catch
            {
                throw;
            }
        }
    }
}
