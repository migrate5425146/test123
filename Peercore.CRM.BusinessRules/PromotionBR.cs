﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Entities;
using Peercore.CRM.DataAccess.DAO;

namespace Peercore.CRM.BusinessRules
{
    public class PromotionBR
    {

        private static PromotionBR instance = null;
        private static object syncLock = new Object();
        private PromotionDAO PromotionDataService;

        public static PromotionBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new PromotionBR();
                }
                return instance;
            }
        }

        #region - Init Object -

        public PromotionEntity CreateObject()
        {
            return PromotionDataService.CreateObject();
        }

        public PromotionEntity CreateObject(int entityId)
        {
            return PromotionDataService.CreateObject(entityId);
        }

        #endregion

        #region - Constructor -

        private PromotionBR()
        {
            PromotionDataService = new PromotionDAO();
        }
        #endregion


        public List<PromotionEntity> GetAllPromotions()
        {
            try
            {
                return PromotionDataService.GetAllPromotions();
            }
            catch
            {
                throw;
            }
        }
    }
}
