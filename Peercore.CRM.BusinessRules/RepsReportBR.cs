﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;

namespace Peercore.CRM.BusinessRules
{
    public class RepsReportBR
    {
         private static RepsReportBR instance = null;
        private static object syncLock = new Object();

        private RepsReportDAO RepsReportDataService;

        public static RepsReportBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new RepsReportBR();
                }
                return instance;
            }
        }

        private RepsReportBR()
        {
            RepsReportDataService = new RepsReportDAO();
        }

        #region - Init Object -

        public RepsOutletWiseDetailsEntity CreateObject()
        {
            return RepsReportDataService.CreateObject();
        }

        #endregion

        public List<RepsOutletWiseDetailsEntity> GetRepsOutletWiseDetailsByAccessToken(string accessToken, string date)
        {
            try
            {
                RepsReportDAO repsReportService = new RepsReportDAO();

                RepsOutletWiseDetailsEntity header = new RepsOutletWiseDetailsEntity();
                List<RepsOutletWiseDetailsEntity> headerList = new List<RepsOutletWiseDetailsEntity>();

                headerList = repsReportService.GetAllRepsOutletWiseDetails(accessToken, date);
                return headerList;
            }
            catch
            {
                throw;
            }
        }
    }
}
