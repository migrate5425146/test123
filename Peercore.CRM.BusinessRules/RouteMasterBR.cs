﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.CRM.Model;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class RouteMasterBR
    {
        private static RouteMasterBR instance = null;
        private static object syncLock = new Object();

        private RouteMasterDAO RouteMasterDataService;

        public static RouteMasterBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new RouteMasterBR();
                }
                return instance;
            }
        }

        private RouteMasterBR()
        {
            RouteMasterDataService = new RouteMasterDAO();
        }

        #region - Init Object -

        public RouteMasterEntity CreateObject()
        {
            return RouteMasterDataService.CreateObject();
        }

        public RouteMasterEntity CreateObject(int entityId)
        {
            return RouteMasterDataService.CreateObject(entityId);
        }

        #endregion

        public List<RouteMasterEntity> GetAllRoutesMaster(string routeNameLike)
        {
            try
            {
                return RouteMasterDataService.GetAllRoutesMaster(routeNameLike);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveRoutesMaster(List<RouteMasterEntity> routeMasterList)
        {
            DbWorkflowScope transactionScope = RouteMasterDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                if (routeMasterList.Count != 0)
                {
                    using (transactionScope)
                    {
                        foreach (RouteMasterEntity routeMasterEntity in routeMasterList)
                        {
                            if (routeMasterEntity.RouteMasterId > 0)
                            {
                                isSuccess = RouteMasterDataService.UpdateRouteMaster(routeMasterEntity);
                            }
                            else
                            {
                                isSuccess = RouteMasterDataService.InsertRouteMaster(routeMasterEntity);
                            }
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public List<RouteMasterEntity> GetAllRoutesMaster(string parentOriginator, ArgsEntity args)
        {
            try
            {
                if (args.ROriginator == "ASE")
                {
                    return RouteMasterDataService.GetAllTMERoutesMaster(args.Originator, args);
                }
                else
                {
                    return RouteMasterDataService.GetAllRoutesMaster(parentOriginator, args);
                }
            }
            catch
            {
                throw;
            }
        }

        public List<RouteMasterEntity> GetAllRoutesMasterForCheckList(int CheckListId, string parentOriginator, ArgsEntity args)
        {
            try
            {
                if (args.ROriginator == "ASE")
                {
                    return RouteMasterDataService.GetAllTMERoutesMasterCheckList(CheckListId, args.Originator, args);
                }
                else
                {
                    return RouteMasterDataService.GetAllRoutesMasterCheckList(CheckListId, parentOriginator, args);

                }
            }
            catch
            {
                throw;
            }
        }


        public List<RouteMasterEntity> GetAllTMERoutesMaster(string tmeCode, ArgsEntity args)
        {
            try
            {
                return RouteMasterDataService.GetAllTMERoutesMaster(tmeCode, args);
            }
            catch
            {
                throw;
            }
        }

        public List<RouteMasterEntity> GetRouteTargetsByDistributorId(ArgsEntity args)
        {
            try
            {
                return RouteMasterDataService.GetRouteTargetsByDistributorId(args);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveRouteTargets(List<RouteMasterEntity> routeTargetList)
        {
            DbWorkflowScope transactionScope = RouteMasterDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                if (routeTargetList.Count != 0)
                {
                    using (transactionScope)
                    {
                        RouteMasterDataService = new RouteMasterDAO(transactionScope);

                        foreach (RouteMasterEntity routeEntity in routeTargetList)
                        {
                            if (routeEntity.TargetId > 0)
                            {
                                isSuccess = RouteMasterDataService.UpdateRouteTarget(routeEntity);
                            }
                            else
                            {
                                if (routeEntity.Target > 0)
                                    isSuccess = RouteMasterDataService.InsertRouteTarget(routeEntity);
                            }
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool IsRouteMasterExists(RouteMasterEntity routeMasterEntity)
        {
            try
            {
                return RouteMasterDataService.IsRouteMasterExists(routeMasterEntity);
            }
            catch
            {
                throw;
            }
        }

        public RouteMasterEntity GetRouteTargetFromRouteId(int routeMasterId)
        {
            try
            {
                return RouteMasterDataService.GetRouteTargetFromRouteId(routeMasterId);
            }
            catch
            {
                throw;
            }
        }

        public List<RouteEntity> GetRouteRepLikeRouteNameForCustomer(string name, int isTemp, string originator, string chlidOriginators)
        {
            try
            {
                return RouteMasterDataService.GetRouteRepLikeRouteNameForCustomer(name, isTemp, originator, chlidOriginators);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool DeleteRouteMaster(RouteMasterEntity routeMasterEntity)
        {
            try
            {
                return RouteMasterDataService.DeleteRouteMaster(routeMasterEntity);
            }
            catch
            {
                throw;
            }
        }

        #region "For 2nd Phase developemtn"

        public List<RouteMasterModel> GetAllRouteMaster(ArgsModel args)
        {
            try
            {
                return RouteMasterDataService.GetAllRouteMaster(args);
            }
            catch
            {
                throw;
            }
        }

        public List<RouteMasterModel> GetAllRouteMasterByTerritoryId(ArgsModel args, string TerritoryId)
        {
            try
            {
                return RouteMasterDataService.GetAllRouteMasterByTerritoryId(args, TerritoryId);
            }
            catch
            {
                throw;
            }
        }

        public RouteMasterModel GetRouteByRouteCode(string RouteCode)
        {
            RouteMasterModel retRoute;
            DbWorkflowScope transactionScope = RouteMasterDataService.WorkflowScope;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    retRoute = RouteMasterDataService.GetRouteByRouteCode(conn, RouteCode);
                }
            }
            catch
            {
                throw;
            }

            return retRoute;
        }

        public RouteMasterModel GetRouteByRouteName(string RouteName)
        {
            RouteMasterModel retRoute;
            DbWorkflowScope transactionScope = RouteMasterDataService.WorkflowScope;

            try
            {
                using (SqlConnection conn = new SqlConnection(transactionScope.Connection.ConnectionString))
                {
                    retRoute = RouteMasterDataService.GetRouteByRouteName(conn, RouteName);
                }
            }
            catch
            {
                throw;
            }

            return retRoute;
        }

        public bool DeleteRouteFromTerritoryByRouteId(string routeId)
        {
            try
            {
                return RouteMasterDataService.DeleteRouteFromTerritoryByRouteId(routeId);
            }
            catch
            {
                throw;
            }
        }

        public bool InsertUpdateRouteMaster(RouteMasterModel routeMaster)
        {
            DbWorkflowScope transactionScope = RouteMasterDataService.WorkflowScope;
            bool isSuccess = true;

            try
            {
                using (transactionScope)
                {
                    if (routeMaster.RouteMasterId > 0)
                    {
                        isSuccess = RouteMasterDataService.UpdateRouteMaster(transactionScope, routeMaster);
                    }
                    else
                    {
                        isSuccess = RouteMasterDataService.InsertRouteMaster(transactionScope, routeMaster);
                    }

                    //if (isSuccess)
                    //    transactionScope.Commit();
                    //else
                    //    transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }
        
        public bool DeleteRouteMasterByRouteId(string routeId, string DeletedBy)
        {
            DbWorkflowScope transactionScope = RouteMasterDataService.WorkflowScope;
            bool isSuccess = true;

            try
            {
                using (transactionScope)
                {
                    isSuccess = RouteMasterDataService.DeleteRouteMasterByRouteId(transactionScope, routeId, DeletedBy);

                    //if (isSuccess)
                    //    transactionScope.Commit();
                    //else
                    //    transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        #endregion
    }
}
