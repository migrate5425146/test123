﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class RepTargetBR
    {
        private static RepTargetBR instance = null;
        private static object syncLock = new Object();

        private RepTargetDAO RepTargetDataService;

        public static RepTargetBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new RepTargetBR();
                }
                return instance;
            }
        }

        private RepTargetBR()
        {
            RepTargetDataService = new RepTargetDAO();
        }

        #region - Init Object -

        public RepTargetEntity CreateObject()
        {
            return RepTargetDataService.CreateObject();
        }

        public RepTargetEntity CreateObject(int entityId)
        {
            return RepTargetDataService.CreateObject(entityId);
        }

        #endregion

        public bool SaveRepTarget(List<RepTargetEntity> repTargetList)
        {
            DbWorkflowScope transactionScope = RepTargetDataService.WorkflowScope;
            RepTargetDAO repTargetDAO = new RepTargetDAO(transactionScope);
            bool isSuccess = false;
            try
            {
                if (repTargetList.Count != 0)
                {
                    using (transactionScope)
                    {
                        foreach (RepTargetEntity item in repTargetList)
                        {
                            if (item.RepTargetId > 0)
                            {
                                isSuccess = repTargetDAO.DeactivateRepTarget(item);
                                if(isSuccess)
                                    isSuccess = repTargetDAO.InsertRepTarget(item);
                            }
                            else
                            {
                                isSuccess = repTargetDAO.InsertRepTarget(item);
                            }

                            if (!isSuccess)
                                break;
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public List<RepTargetEntity> GetAllRepTargets(string Originator, string OriginatorType, ArgsEntity args)
        {
            try
            {
                return RepTargetDataService.GetAllRepTargets(Originator, OriginatorType, args);
            }
            catch
            {
                throw;
            }
        }

        public bool DeactivateRepTarget(RepTargetEntity repTarget)
        {
            try
            {
                return RepTargetDataService.DeactivateRepTarget(repTarget);
            }
            catch
            {
                throw;
            }
        }

        public List<RepTargetEntity> GetRepTargetVsActualByAccessToken(string accessToken)
        {
            try
            {
                RepTargetEntity repTargetEntity = new RepTargetEntity();
                List<RepTargetEntity> repTargetList = new List<RepTargetEntity>();
                repTargetList = RepTargetDataService.GetRepTargetVsActualByAccessToken(accessToken);
                return repTargetList;
            }
            catch
            {
                throw;
            }
        } 
        
        public List<RepTargetEntity> GetRepTargetVsActualByAccessTokenNew(string accessToken)
        {
            try
            {
                RepTargetEntity repTargetEntity = new RepTargetEntity();
                List<RepTargetEntity> repTargetList = new List<RepTargetEntity>();
                repTargetList = RepTargetDataService.GetRepTargetVsActualByAccessTokenNew(accessToken);
                return repTargetList;
            }
            catch
            {
                throw;
            }
        }
    }
}
