﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.CRM.Model;

namespace Peercore.CRM.BusinessRules
{
    public class ASMDashBoardBR
    {
        private static ASMDashBoardBR instance = null;
        private static object syncLock = new Object();

        private ASMDashboardDAO ASMDashboardDataService;

        public static ASMDashBoardBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new ASMDashBoardBR();
                }
                return instance;
            }
        }

        private ASMDashBoardBR()
        {
            ASMDashboardDataService = new ASMDashboardDAO();
        }

        public List<ASMDashboardEntity> GetDailyRepsSalesforASM(string AccessToken, string date)
        {
            try
            {
                return ASMDashboardDataService.GetDailyRepsSalesforASM(AccessToken, date);
            }
            catch
            {
                throw;
            }
        }
        
        public List<ASMDashboardModel> GetDailyRepsSalesforASMNew(string AccessToken, string date)
        {
            try
            {
                return ASMDashboardDataService.GetDailyRepsSalesforASMNew(AccessToken, date);
            }
            catch
            {
                throw;
            }
        }

    }
}
