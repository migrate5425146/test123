﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class TargetBR
    {
        private static TargetBR instance = null;
        private static object syncLock = new Object();
        private TargetDAO TargetDataService;

        public static TargetBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new TargetBR();
                }
                return instance;
            }
        }

        #region - Init Object -

        public TargetEntity CreateObject()
        {
            return TargetDataService.CreateObject();
        }

        #endregion

        #region - Constructor -

        private TargetBR()
        {
            TargetDataService = new TargetDAO();
        }
        #endregion


        public TargetEntity GetDRTargetActualsByRoute(int routeId)
        {
            try
            {
                return TargetDataService.GetDRTargetActualsByRoute(routeId);
            }
            catch
            {
                throw;
            }
        }

        public List<TargetEntity> GetTargetsVsActualsBrandWise(int programId, DateTime fromDate, DateTime toDate)
        {
            try
            {
                double otherActualTotal = 0;
                double otherTargetTotal = 0;

                List<TargetEntity> targetsList = new List<TargetEntity>();
                List<TargetEntity> otherTargetsList = new List<TargetEntity>();
                List<TargetEntity> returnTargetsList = new List<TargetEntity>();

                targetsList = TargetDataService.GetTargetsVsActualsBrandWise(programId, fromDate, toDate);

                otherTargetsList = targetsList.Where(a => (!a.BrandCode.Equals("JPGL") && !a.BrandCode.Equals("DH"))).ToList();
                returnTargetsList = targetsList.Where(a => (a.BrandCode.Equals("JPGL") || a.BrandCode.Equals("DH"))).ToList();

                TargetEntity otherTargetEntity = new TargetEntity();
                otherTargetEntity.BrandCode = "Other";

                foreach (TargetEntity item in otherTargetsList)
                {
                    otherActualTotal = otherActualTotal + item.Actual;
                    otherTargetTotal = otherTargetTotal + item.Target;
                }

                otherTargetEntity.Actual = otherActualTotal;
                otherTargetEntity.Target = otherTargetTotal;

                returnTargetsList.Add(otherTargetEntity);

                return returnTargetsList;
            }
            catch
            {
                throw;
            }
        }

        public TargetEntity GetTargetsVsActualsForOutlet(string custCode, DateTime fromDate, DateTime todate)
        {
            try
            {
                return TargetDataService.GetTargetsVsActualsForOutlet(custCode, fromDate, todate);
            }
            catch
            {
                throw;
            }
        }

        public List<TargetEntity> GetTargetsVsActualsOfOutletsByRep(DateTime fromDate, DateTime toDate, string repcode) {
            try
            {
                return TargetDataService.GetTargetsVsActualsOfOutletsByRep(fromDate, toDate, repcode);
            }
            catch
            {
                throw;
            }
        }

        public List<TargetEntity> GetTargetsVsActualsOfRoutes(DateTime fromDate, DateTime toDate)
        {
            try
            {
                return TargetDataService.GetTargetsVsActualsOfRoutes(fromDate, toDate);
            }
            catch
            {
                throw;
            }
        }

        public List<TargetEntity> GetRouteSummaryBrandwise(int routeId) {
            try
            {
                return TargetDataService.GetRouteSummaryBrandwise(routeId);
            }
            catch
            {
                throw;
            }
        }

        public List<MobileAbishekaSummaryEntity> GetAbishekaRouteSummaryByRoute(int routeId)
        {
            try
            {
                return TargetDataService.GetAbishekaRouteSummaryByRoute(routeId);
            }
            catch
            {
                throw;
            }
        }

        public List<MobileAbishekaSummaryEntity> GetAllVolumeRouteSummaryByRoute(int routeId)
        {
            try
            {
                return TargetDataService.GetAllVolumeRouteSummaryByRoute(routeId);
            }
            catch
            {
                throw;
            }
        }

        public List<MobileCustomerPerformanceEntity> GetCustomerVolumePerformance(string custCode)
        {
            try
            {  

                return TargetDataService.GetCustomerVolumePerformance(custCode);

            }
            catch
            {
                throw;
            }
        }

        public List<MobileCustomerPerformanceEntity> GetAbishekaCustomerPerformance(string custCode)
        {
            try
            {

                return TargetDataService.GetAbishekaCustomerPerformance(custCode);

            }
            catch
            {
                throw;
            }
        }

        public List<MobileMonthlyActualsAndEarnings> GetCustomerTargetAchievedAndEarningsMonthly(string custCode)
        {
            try
            {

                return TargetDataService.GetCustomerTargetAchievedAndEarningsMonthly(custCode);

            }
            catch
            {
                throw;
            }
        }


        public List<MobileAbishekaSummaryEntity> GetAbishekaBonusAndTargetByRoute(int routeId)
        {
            try
            {
                return TargetDataService.GetAbishekaBonusAndTargetByRoute(routeId);
            }
            catch
            {
                throw;
            }
        }

        
    }
}
