﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;


namespace Peercore.CRM.BusinessRules
{
    public class RepWizeActivityAnalysisBR
    {
        private static RepWizeActivityAnalysisBR instance = null;
        private static object syncLock = new Object();
        private RepWizeActivityAnalysisDAO RepWizeActivityAnalysisDataService;

        public static RepWizeActivityAnalysisBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new RepWizeActivityAnalysisBR();
                }
                return instance;
            }
        }

        #region - Init Object -

        public RepWizeActivityAnalysisEntity CreateObject()
        {
            return RepWizeActivityAnalysisDataService.CreateObject();
        }

        public RepWizeActivityAnalysisEntity CreateObject(int entityId)
        {
            return RepWizeActivityAnalysisDataService.CreateObject(entityId);
        }

        #endregion

        #region - Constructor -

        private RepWizeActivityAnalysisBR()
        {
            RepWizeActivityAnalysisDataService = new RepWizeActivityAnalysisDAO();
        }
        #endregion

        #region - Public Methods -

        public List<RepWizeActivityAnalysisEntity> GetRepWideActivity(DateTime FromDate, DateTime ToDate, List<LeadStageEntity> LeadStageList, ArgsEntity argsEntity, List<OriginatorEntity> OriginatorList)
        {
            try
            {
                return  RepWizeActivityAnalysisDataService.GetRepWideActivity(FromDate, ToDate, LeadStageList, argsEntity, OriginatorList)  ;
            }
            catch 
            {                
                throw;
            }
        }

        #endregion
    }
}
