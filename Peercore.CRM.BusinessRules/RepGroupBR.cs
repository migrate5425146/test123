﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class RepGroupBR
    {
        private static RepGroupBR instance = null;
        private static object syncLock = new Object();
        private RepGroupDAO repGroupDataService;

        public static RepGroupBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new RepGroupBR();
                }
                return instance;
            }
        }

        #region - Init Object -

        public RepGroupEntity CreateObject()
        {
            return repGroupDataService.CreateObject();
        }

        public RepGroupEntity CreateObject(int entityId)
        {
            return repGroupDataService.CreateObject(entityId);
        }

        #endregion

        #region - Constructor -

        private RepGroupBR()
        {
            repGroupDataService = new RepGroupDAO();
        }
        #endregion

        #region - Public Methods -

        public List<RepGroupEntity> GetAllRepGroups()
        {
            try
            {
                return repGroupDataService.GetAllRepGroups();
            }
            catch
            {
                throw;
            }
        }

        //public bool Save(List<RepGroupEntity> repGroups, List<GroupRepEntity> delReps, List<int> delGroups)
        //{
        //    DbWorkflowScope transactionScope = repGroupDataService.WorkflowScope;
        //    bool isSuccess = true;
        //    try
        //    {
        //        using (transactionScope)
        //        {

        //            foreach (RepGroupEntity repGroup in repGroups)
        //            {
        //                if (repGroup.IsDirty) 
        //                {
        //                    if (repGroup.RepGroupId == 0)
        //                    {
        //                        repGroup.RepGroupId = repGroupDataService.GetNextRepGroupId();
        //                        isSuccess = repGroupDataService.InsertRepGroup(repGroup);
        //                    }
        //                    else 
        //                    {
        //                        isSuccess = repGroupDataService.UpdateRepGroup(repGroup);
        //                    }   
        //                }
                       
        //                if (isSuccess)
        //                {
        //                    foreach (GroupRepEntity groupRepEntity in repGroup.RepList)
        //                    {
        //                      isSuccess = repGroupDataService.InsertGroupRep(repGroup.RepGroupId, groupRepEntity.Originator);                             
        //                    } 
        //                }                       
        //            }

        //            if (delReps.Count > 0)
        //            {
        //                foreach (GroupRepEntity groupRep in delReps)
        //                {                            
        //                  isSuccess = repGroupDataService.DeleteGroupRep(groupRep.RepGroupId, groupRep.Originator);
        //                }
        //            }

        //            if (delGroups.Count > 0)
        //            {
        //                foreach (int repGroupID in delGroups)
        //                {
        //                    isSuccess = repGroupDataService.DeleteRepGroup(repGroupID);

        //                    if (isSuccess)
        //                    {
        //                        isSuccess = repGroupDataService.DeleteGroupRepById(repGroupID);
        //                    }
        //                }
        //            }

        //            if (isSuccess)
        //                transactionScope.Commit();
        //            else
        //                transactionScope.Rollback();
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    return isSuccess;
        //}

        //public bool CheckExistsToDeleteGroups(int? GroupId)
        //{
        //    try
        //    {
        //       return repGroupDataService.CheckExistsToDeleteGroups(GroupId);
        //    }
        //    catch 
        //    {                
        //        throw;
        //    }
        //}
        #endregion
    }
}
