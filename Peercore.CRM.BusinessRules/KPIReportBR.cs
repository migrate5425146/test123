﻿using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.BusinessRules
{
    public class KPIReportBR
    {
        private static KPIReportBR instance = null;
        private static object syncLock = new Object();

        private KPIReportDAO KPIReportBRService;

        public static KPIReportBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new KPIReportBR();
                }
                return instance;
            }
        }

        private KPIReportBR()
        {
            KPIReportBRService = new KPIReportDAO();
        }

        #region KPIReport

        public List<KPIReportEmailRecipientsModel> GetAllEmailRecipients(ArgsModel args)
        {
            List<KPIReportEmailRecipientsModel> emailList = new List<KPIReportEmailRecipientsModel>();
            try
            {
                string emailVal = KPIReportBRService.GetAllEmailRecipients(args);
                string[] emailLst = emailVal.Replace(" ", "").Split(';');
                foreach (string s in emailLst)
                {
                    var email = new KPIReportEmailRecipientsModel();
                    email.Email = s;
                    emailList.Add(email);
                }
            }
            catch
            {
                emailList = new List<KPIReportEmailRecipientsModel>();
            }

            return emailList;
        }

        public List<KPIReportModel> GetAllKPIReports(ArgsModel args, int month, int year)
        {
            try
            {
                return KPIReportBRService.GetAllKPIReports(args, month, year);
            }
            catch
            {
                throw;
            }
        }

        public List<KPIReportModel> GetAllKPIReportWeeklyTargetsByMonth(ArgsModel args, int month, int year)
        {
            try
            {
                return KPIReportBRService.GetAllKPIReportWeeklyTargetsByMonth(args, month, year);
            }
            catch
            {
                throw;
            }
        }

        public bool UploadKPIReportExcel(string FilePath, string UserName, string key, DateTime targetDate)
        {
            try
            {
                return KPIReportBRService.UploadPayments(FilePath, UserName, key, targetDate);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public bool UploadKPIReportTargetWeekly(string FilePath, string UserName, string key, DateTime targetDate)
        {
            try
            {
                return KPIReportBRService.UploadKPIReportTargetWeekly(FilePath, UserName, key, targetDate);
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        #endregion

        #region KPIReportSettings-Brands

        public List<KPIReportSettingsModel> GetKPIReportBrands(ArgsModel args)
        {
            try
            {
                return KPIReportBRService.GetKPIReportBrands(args);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateKPIReportBrands(KPIReportSettingsModel brandsDetail)
        {
            try
            {
                return KPIReportBRService.UpdateKPIReportBrands(brandsDetail);
            }
            catch
            {
                throw;
            }
        }

        public List<KPIReportSettingsModel> KPIReportBrands()
        {
            try
            {
                return KPIReportBRService.KPIReportBrands();
            }
            catch
            {
                throw;
            }
        }

        #endregion

        public bool UpdateKPIEmailRecipients(string emailRecipints)
        {
            string emailVal = string.Empty;
            ArgsModel args = new ArgsModel();
            List<KPIReportEmailRecipientsModel> emailList = new List<KPIReportEmailRecipientsModel>();
            try
            {
                emailVal = KPIReportBRService.GetAllEmailRecipients(args);
                string[] emailLst = emailVal.Replace(" ", "").Split(';');
                foreach (string s in emailLst)
                {
                    var email = new KPIReportEmailRecipientsModel();
                    email.Email = s;
                    emailList.Add(email);
                }
            }
            catch
            {
                emailList = new List<KPIReportEmailRecipientsModel>();
            }

            try
            {
                foreach (var email in emailList)
                {
                    if (email.Email.ToLower() == emailRecipints.ToLower())
                        return false;
                }

                emailVal = emailVal + ";" + emailRecipints;
                return KPIReportBRService.UpdateKPIEmailRecipients(emailVal);
            }
            catch
            {
                return false;
            }
        }

        public bool DeleteKPIEmailRecipient(string emailRecipint)
        {
            string emailVal = string.Empty;
            ArgsModel args = new ArgsModel();
            string newEmail = string.Empty;
            List<KPIReportEmailRecipientsModel> emailList = new List<KPIReportEmailRecipientsModel>();

            emailVal = KPIReportBRService.GetAllEmailRecipients(args);
            string[] emailLst = emailVal.ToLower().Replace(" ", "").Split(';');

            try
            {
				foreach (var email in emailLst)
				{
                    if (email.ToLower() != emailRecipint.ToLower())
                    {
                        if (newEmail == "")
                        {
                            newEmail = email;
                        }
                        else
                        {
                            newEmail = newEmail + ";" + email;
                        }
                    }
				}

				return KPIReportBRService.UpdateKPIEmailRecipients(newEmail);
            }
            catch
            {
                return false;
            }
        }

        public List<KPIReportBrandsModel> GetAllKPIReportsBrands()
        {
            try
            {
                return KPIReportBRService.GetAllKPIReportsBrands();
            }
            catch
            {
                throw;
            }
        }

        public List<KPIReportBrandItemModel> GetKPIBrandItemsByBrandId(ArgsModel args, int kpiBrandId)
        {
            try
            {
                return KPIReportBRService.GetKPIBrandItemsByBrandId(args, kpiBrandId);
            }
            catch
            {
                throw;
            }
        }

        public bool AssignKPIBrandItems(string originator,
                                       string kpi_brand_id,
                                       string product_id,
                                       string is_checked)
        {
            try
            {
                return KPIReportBRService.AssignKPIBrandItems(originator,
                                       kpi_brand_id,
                                       product_id,
                                       is_checked);
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public bool UpdateKPIBrandDisplayName(string kpi_brand_id, string displayName)
        {
            try
            {
                return KPIReportBRService.UpdateKPIBrandDisplayName(kpi_brand_id,
                                       displayName);
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public KPIReportBrandsModel GetKPIReportBrandByBrandId(string kpi_brand_id)
        {
            try
            {
                return KPIReportBRService.GetKPIReportBrandByBrandId(kpi_brand_id);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
