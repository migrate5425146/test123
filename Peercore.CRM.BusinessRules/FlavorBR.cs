﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class FlavorBR
    {
        private static FlavorBR instance = null;
        private static object syncLock = new Object();

        private FlavorDAO FlavorDataService;

        public static FlavorBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new FlavorBR();
                }
                return instance;
            }
        }

        private FlavorBR()
        {
            FlavorDataService = new FlavorDAO();
        }

        #region - Init Object -

        public FlavorEntity CreateObject()
        {
            return FlavorDataService.CreateObject();
        }

        public FlavorEntity CreateObject(int entityId)
        {
            return FlavorDataService.CreateObject(entityId);
        }

        #endregion

        public List<FlavorEntity> GetAllFlavor()
        {
            try
            {
                return FlavorDataService.GetAllFlavor();
            }
            catch
            {
                throw;
            }
        }

        public List<FlavorEntity> GetFlavorByAccessToken(string accessToken)
        {
            try
            {
                return FlavorDataService.GetFlavorByAccessToken(accessToken);
            }
            catch
            {
                throw;
            }
        }

        public bool IsProductFlavorCodeExists(FlavorEntity flavorEntity)
        {
            try
            {
                return FlavorDataService.IsProductFlavorNameExists(flavorEntity);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveFlavor(List<FlavorEntity> FlavorEntityList)
        {
            DbWorkflowScope transactionScope = FlavorDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                if (FlavorEntityList.Count != 0)
                {
                    using (transactionScope)
                    {
                        foreach (FlavorEntity flavorEntity in FlavorEntityList)
                        {
                            if (flavorEntity.FlavorId > 0)
                            {
                                isSuccess = FlavorDataService.UpdateFlavor(flavorEntity);
                            }
                            else
                            {
                                isSuccess = FlavorDataService.InsertFlavor(flavorEntity);
                            }
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool DeleteProductFlavor(int flavorId)
        {
            try
            {
                return FlavorDataService.DeleteProductFlavor(flavorId);
            }
            catch
            {
                throw;
            }
        }
    }
}
