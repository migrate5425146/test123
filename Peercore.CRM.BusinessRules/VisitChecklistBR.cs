﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class VisitChecklistBR
    {
        private static VisitChecklistBR instance = null;
        private static object syncLock = new Object();

        private VisitChecklistDAO VisitChecklistDataService;

        public static VisitChecklistBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new VisitChecklistBR();
                }
                return instance;
            }
        }

        private VisitChecklistBR()
        {
            VisitChecklistDataService = new VisitChecklistDAO();
        }

        #region - Init Object -

        public VisitChecklistEntity CreateObject()
        {
            return VisitChecklistDataService.CreateObject();
        }

        public VisitChecklistEntity CreateObject(int entityId)
        {
            return VisitChecklistDataService.CreateObject(entityId);
        }

        #endregion

        //public List<MobileVisitCheckListEntity> GetVisitChecklist(int routeMasterID, ArgsEntity args)
        //{
        //    try
        //    {
        //       // List<VisitChecklistEntity> visistChecklistEntityList = new List<VisitChecklistEntity>();
        //        MobileVisitCheckListEntity visitChecklistEntity = null;
        //        List<MobileVisitCheckListEntity> mobileChecklist = new List<MobileVisitCheckListEntity>();
        //        List<VisitChecklistMasterEntity> visistChecklistMasterEntityList = new List<VisitChecklistMasterEntity>();

        //        visistChecklistMasterEntityList = VisitChecklistDataService.GetAllVisitCheckListMaster(args);

        //        foreach (VisitChecklistMasterEntity masterList in visistChecklistMasterEntityList)
        //        {
        //            visitChecklistEntity = new MobileVisitCheckListEntity();

        //            visitChecklistEntity.VisitChecklistMasterId = masterList.VisitChecklistMasterId;
        //            visitChecklistEntity.Name = masterList.Name;

        //            mobileChecklist.Add(visitChecklistEntity);

        //        }

        //        //foreach(VisitChecklistEntity item in visistChecklistEntityList)
        //        //{
        //        //    item.NationalObjectivesList = ObjectiveBR.Instance.GetAllNationalObjectives();
        //        //    item.MarketWiseObjectivesList = MarketObjectiveBR.Instance.GetAllObjectivesMarketWise(null, routeMasterID);
        //        //    item.VisitCheckListMasterList = mobileChecklist;
        //        //}

        //        return mobileChecklist;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        public bool SaveVisitChecklist(VisitChecklistEntity visitChecklistEntity)
        {
            bool isSuccess = false;
            //DbWorkflowScope transactionScope = VisitChecklistDataService.WorkflowScope;
            //VisitChecklistDAO VisitChecklistDAO = new VisitChecklistDAO(transactionScope);
            //if (visitChecklistEntity.CustCode != null)
            //{
                
            //    try
            //    {
                   
            //    }
            //    catch
            //    {
            //        throw;
            //    }
            //    return isSuccess;
            //}
            //else
            //{
            //    using (transactionScope)
            //    {
            //        foreach (MobileVisitCheckListEntity vistChecklistItem in visitChecklistEntity.VisitCheckListMasterList)
            //        {
            //            isSuccess = VisitChecklistDataService.InsertVisitChecklistForRoute(vistChecklistItem, visitChecklistEntity.RouteMasterId);

            //            if (!isSuccess)
            //                break;
            //        }

            //        if (isSuccess)
            //            transactionScope.Commit();
            //        else
            //            transactionScope.Rollback();

            //    }

            //    if (visitChecklistEntity.NationalObjectivesList != null)
            //    {

            //        if (isSuccess)
            //        {
            //            isSuccess = ObjectiveBR.Instance.SaveRouteNationalObjectives(visitChecklistEntity.NationalObjectivesList, visitChecklistEntity.RouteMasterId);
            //        }

            //    }

            //    if (visitChecklistEntity.MarketWiseObjectivesList != null)
            //    {

            //        if (isSuccess)
            //        {
            //            isSuccess = ObjectiveBR.Instance.SaveRouteMarketObjectives(visitChecklistEntity.MarketWiseObjectivesList, visitChecklistEntity.RouteMasterId);
            //        }
            //    }
                    return isSuccess;
            //}
        }

        public List<VisitChecklistMasterEntity> GetAllVisitChecklistMaster(ArgsEntity args)
        {
            try
            {
                return VisitChecklistDataService.GetAllVisitCheckListMaster(args);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveVisitChecklistMaster(List<VisitChecklistMasterEntity> visitChecklistMasterEntityList)
        {
            DbWorkflowScope transactionScope = VisitChecklistDataService.WorkflowScope;
            VisitChecklistDAO VisitChecklistDAO = new VisitChecklistDAO(transactionScope);
            bool isSuccess = true;
            try
            {
                if (visitChecklistMasterEntityList.Count != 0)
                {
                    using (transactionScope)
                    {
                        foreach (VisitChecklistMasterEntity visitChecklistEntity in visitChecklistMasterEntityList)
                        {
                            if (visitChecklistEntity.VisitChecklistMasterId > 0)
                            {
                                isSuccess = VisitChecklistDAO.UpdateVisitCheckListMaster(visitChecklistEntity);
                            }
                            else
                            {
                                isSuccess = VisitChecklistDAO.InsertVisitCheckListMaster(visitChecklistEntity);
                            }
                        }

                        if (isSuccess)
                            transactionScope.Commit();
                        else
                            transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool DeleteVisitChecklistMaster(VisitChecklistMasterEntity visitChecklistMasterEntity)
        {
            try
            {
                return VisitChecklistDataService.DeleteVisitCheckListMaster(visitChecklistMasterEntity);
            }
            catch
            {
                throw;
            }
        }

       
    }
}
