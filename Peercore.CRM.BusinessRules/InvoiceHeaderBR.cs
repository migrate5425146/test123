﻿using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using System;
using System.Collections.Generic;

namespace Peercore.CRM.BusinessRules
{
    public class InvoiceHeaderBR
    {
        private static InvoiceHeaderBR instance = null;
        private static object syncLock = new Object();

        private InvoiceHeaderDAO InvoiceHeaderDataService;

        public static InvoiceHeaderBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new InvoiceHeaderBR();
                }
                return instance;
            }
        }

        private InvoiceHeaderBR()
        {
            InvoiceHeaderDataService = new InvoiceHeaderDAO();
        }

        #region - Init Object -

        public InvoiceHeaderEntity CreateObject()
        {
            return InvoiceHeaderDataService.CreateObject();
        }

        #endregion

        public List<InvoiceHeaderEntity> GetOutstandingInvoicesForCustomer(string custCode, DateTime fromDate, DateTime toDate)
        {
            try
            {
                return InvoiceHeaderDataService.GetOutstandingInvoicesForCustomer(custCode, fromDate, toDate);
            }
            catch
            {
                throw;
            }
        }

        public List<InvoiceHeaderEntity> GetInvoicesByAccessToken(string accessToken)
        {
            List<InvoiceHeaderEntity> InvoiceHeaderList = new List<InvoiceHeaderEntity>();

            try
            {
                InvoiceDetailsDAO InvoiceDetailService = new InvoiceDetailsDAO();
                InvoiceSchemeDAO InvoiceSchemeService = new InvoiceSchemeDAO();

                InvoiceHeaderList = InvoiceHeaderDataService.GetInvoicesByAccessToken(accessToken);

                foreach (var item in InvoiceHeaderList)
                {
                    item.InvoiceDetailList = InvoiceDetailService.GetInvoiceDetailsByInvoiceId(item.IvceId,"N");
                    item.InvoiceSchemeGroupList = InvoiceSchemeService.GetInvoiceSchemeGroupByInvoiceId(item.IvceId);
                    item.InvoiceSettlementList = InvoiceHeaderDataService.GetInvoiceSettlementListByInvoiceId(item.IvceId);

                    if (item.InvoiceSettlementList != null)
                    {
                        foreach (InvoiceSettlementEntity invSetEntity in item.InvoiceSettlementList)
                        {
                            invSetEntity.InvoiceSettlementDetailsList =
                                InvoiceHeaderDataService.GetInvoiceSettlementDetailsBySettlementIdAndInvoiceId(invSetEntity.InvoiceSettlementId, item.IvceId);

                            invSetEntity.InvoiceSettlementChequeList =
                                InvoiceHeaderDataService.GetInvoiceSettlementChequeBySettlementId(invSetEntity.InvoiceSettlementId);
                        }
                    }

                    ////This is for Return Invoices 
                    ////UnComment for Perfetti needs returns
                    //item.ReturnInvoiceList = InvoiceHeaderDataService.GetReturnInvoiceByInvoiceId(item.IvceId);

                    //if (item.ReturnInvoiceList != null)
                    //{
                    //    foreach (ReturnInvoiceEntity invSetEntity in item.ReturnInvoiceList)
                    //    {
                    //        invSetEntity.ReturnInvoiceLineList =
                    //            InvoiceDetailService.GetReturnInvoiceLinesByReturnIdAndInvoiceId(invSetEntity.Id, item.IvceId);
                    //
                    //        invSetEntity.InvoiceDetailList =
                    //            InvoiceDetailService.GetReturnInvoiceDetailsByReturnIdAndInvoiceId(invSetEntity.Id, item.IvceId);
                    //    }
                    //}
                }
            }
            catch
            {
                throw;
            }

            return InvoiceHeaderList;
        }

        public List<InvoiceHeaderEntity> GetInvoicesByAccessTokenforASM(string accessToken, string date)
        {
            List<InvoiceHeaderEntity> InvoiceHeaderList = new List<InvoiceHeaderEntity>();
            try
            {
                InvoiceDetailsDAO InvoiceDetailService = new InvoiceDetailsDAO();
                InvoiceSchemeDAO InvoiceSchemeService = new InvoiceSchemeDAO();

                InvoiceHeaderList = InvoiceHeaderDataService.GetInvoicesByAccessTokenForASM(accessToken, date);
                //foreach (var item in InvoiceHeaderList)
                //{
                //    item.InvoiceDetailList = InvoiceDetailService.GetInvoiceDetailsByInvoiceId(item.IvceId);
                //    item.InvoiceSchemeGroupList = InvoiceSchemeService.GetInvoiceSchemeGroupByInvoiceId(item.IvceId);

                //    //Comment By Irosh for Add Features from Generic Vertion to Perfetti 2017/02/22
                //    ////////////////////////////////////////////////////////////////////////////
                //    //item.InvoiceSettlement = InvoiceHeaderDataService.GetInvoiceSettlementByInvoiceId(item.IvceId);
                //    //if (item.InvoiceSettlement != null)
                //    //    item.InvoiceSettlement.InvoiceSettlementChequeList =
                //    //        InvoiceHeaderDataService.GetInvoiceSettlementChequeBySettlementId(item.InvoiceSettlement.InvoiceSettlementId);

                //    //Add By Irosh for Add Features from Generic Vertion to Perfetti 2017/02/22
                //    ////////////////////////////////////////////////////////////////////////////

                //    item.InvoiceSettlementList = InvoiceHeaderDataService.GetInvoiceSettlementListByInvoiceId(item.IvceId);
                //    if (item.InvoiceSettlementList != null)
                //    {
                //        foreach (InvoiceSettlementEntity invSetEntity in item.InvoiceSettlementList)
                //        {
                //            invSetEntity.InvoiceSettlementDetailsList =
                //                InvoiceHeaderDataService.GetInvoiceSettlementDetailsBySettlementIdAndInvoiceId(invSetEntity.InvoiceSettlementId, item.IvceId);

                //            invSetEntity.InvoiceSettlementChequeList =
                //                InvoiceHeaderDataService.GetInvoiceSettlementChequeBySettlementId(invSetEntity.InvoiceSettlementId);

                //            if (item.IvceId == 98868)
                //            {
                //            }
                //        }
                //    }

                //    item.ReturnInvoiceList = InvoiceHeaderDataService.GetReturnInvoiceByInvoiceId(item.IvceId);
                //    if (item.ReturnInvoiceList != null)
                //    {
                //        foreach (ReturnInvoiceEntity invSetEntity in item.ReturnInvoiceList)
                //        {
                //            invSetEntity.ReturnInvoiceLineList =
                //                InvoiceDetailService.GetReturnInvoiceLinesByReturnIdAndInvoiceId(invSetEntity.Id, item.IvceId);
                //            invSetEntity.InvoiceDetailList =
                //                InvoiceDetailService.GetReturnInvoiceDetailsByReturnIdAndInvoiceId(invSetEntity.Id, item.IvceId);
                //        }
                //    }
                //}
            }
            catch
            {
                throw;
            }
            return InvoiceHeaderList;
        }

        public List<RepsTrackingDetailsEntity> RepsTrackingDetailsforASM(string accessToken, string date)
        {
            List<RepsTrackingDetailsEntity> InvoiceHeaderList = new List<RepsTrackingDetailsEntity>();
            try
            {
                InvoiceHeaderList = InvoiceHeaderDataService.RepsTrackingDetailsforASM(accessToken, date);
            }
            catch
            {
                throw;
            }
            return InvoiceHeaderList;
        }

        public bool InsertInvoice(string accessToken, InvoiceHeaderEntity invoiceHeader)
        {
            bool isSuccess = false;

            DbWorkflowScope transactionScope = InvoiceHeaderDataService.WorkflowScope;

            int invoiceId = 0;
            try
            {
                using (transactionScope)
                {
                    try
                    {
                        InvoiceHeaderDAO invoiceHeaderService = new InvoiceHeaderDAO(transactionScope);
                        InvoiceDetailsDAO invoiceDetailsService = new InvoiceDetailsDAO(transactionScope);
                        InvoiceSchemeDAO invoiceSchemeService = new InvoiceSchemeDAO(transactionScope);
                        CustomerDAO customerService = new CustomerDAO(transactionScope);

                        OriginatorDAO originatorService = new OriginatorDAO();

                        OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);

                        if (originator == null)
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        //invoiceHeader.RepCode = originator.RepCode;

                        //if (invoiceHeaderService.IsInvoiceExist(invoiceHeader.InvoiceNo))
                        //{
                        //    isSuccess = invoiceHeaderService.UpdateInvoiceHeader(out invoiceId, invoiceHeader);
                        //}
                        //else
                        //{

                        if (invoiceHeader.CustCode != null)
                        {
                            if (invoiceHeader.CustCode.Contains("TEMP"))
                            {
                                invoiceHeader.CustCode = customerService.GetCustomerCodeByTempCustCode(invoiceHeader.CustCode);
                            }
                        }
                        else
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        if (invoiceHeader.IvceDate == null)
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        try
                        {
                            if (invoiceHeader.InvoiceNo != null)
                            {
                                if (invoiceHeaderService.IsInvoiceExist(invoiceHeader.InvoiceNo.Trim()))
                                {
                                    if (invoiceHeader.Status == "D")
                                    {
                                        DeleteInvoiceHeaderForAddInvoice(accessToken, invoiceHeader.InvoiceNo.Trim());
                                        //InvoiceHeaderEntity invoiceHeaderTemp = new InvoiceHeaderEntity();
                                        //invoiceHeaderTemp = invoiceHeaderService.GetInvoiceHeaderByInvoiceNo(invoiceHeader.InvoiceNo);
                                        //invoiceHeaderService.UpdateOutstandingForInvoice(invoiceHeaderTemp.IvceId);
                                        transactionScope.Commit();
                                        return true;
                                    }
                                    else
                                    {
                                        transactionScope.Rollback();
                                        return true;
                                    }
                                }
                            }
                            else
                            {
                                transactionScope.Rollback();
                                return false;
                            }
                        }
                        catch
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        isSuccess = invoiceHeaderService.InsertInvoiceHeader(out invoiceId, invoiceHeader);

                        // Want add isSuccess == true conttion
                        if (invoiceHeader.Status == "A")
                        {
                            invoiceHeaderService.UpdateOutstandingForInvoice(invoiceId);
                        }

                        if (isSuccess)
                        {
                            invoiceDetailsService.DeleteInvoiceDetails(invoiceId);
                            invoiceSchemeService.DeleteInvoiceDiscountScheme(invoiceId);

                            if (isSuccess)
                            {
                                if (invoiceHeader.InvoiceDetailList.Count == 0)
                                {
                                    transactionScope.Rollback();
                                    return false;
                                }
                                else
                                {
                                    foreach (var item in invoiceHeader.InvoiceDetailList)
                                    {
                                        item.InvoiceId = invoiceId;
                                        if (item.ProductId == 0)
                                        {
                                            transactionScope.Rollback();
                                            return false;
                                        }

                                        isSuccess = invoiceDetailsService.InsertInvoiceDetails(item, invoiceHeader.Status);
                                        if (!isSuccess)
                                        {
                                            transactionScope.Rollback();
                                            return false;
                                        }
                                    }

                                    if (isSuccess)
                                    {
                                        foreach (var item in invoiceHeader.InvoiceSchemeGroupList)
                                        {
                                            item.InvoiceId = invoiceId;
                                            int schemeGroupId = 0;

                                            isSuccess = invoiceSchemeService.InsertInvoiceDiscountSchemeGroup(out schemeGroupId, item);
                                            if (!isSuccess)
                                            {
                                                transactionScope.Rollback();
                                                return false;
                                            }

                                            foreach (var invoiceScheme in item.InvoiceSchemeList)
                                            {
                                                invoiceScheme.SchemeGroupId = schemeGroupId;
                                                isSuccess = invoiceSchemeService.InsertInvoiceDiscountScheme(invoiceScheme);
                                                if (!isSuccess)
                                                {
                                                    transactionScope.Rollback();
                                                    return false;
                                                }
                                            }
                                        }
                                    }
                                }

                                try
                                {
                                    isSuccess = invoiceHeaderService.InsertInvoiceProductDiscount(invoiceId);
                                    if (!isSuccess)
                                    {
                                        transactionScope.Rollback();
                                        return false;
                                    }
                                }
                                catch {
                                    transactionScope.Rollback();
                                    return false;
                                }
                            }
                        }
                    }
                    catch
                    {
                        transactionScope.Rollback();
                        return false;
                    }

                    if (isSuccess)
                    {
                        transactionScope.Commit();
                    }
                    else
                    {
                        transactionScope.Rollback();
                    }
                }
            }
            catch
            {
                return false;
            }

            return isSuccess;
        }

        /// <summary>
        /// Adds a Credit Settment to existing Invoice
        /// </summary>
        public bool InsertCreditSettlement(string accessToken, InvoiceSettlementEntity invoiceSettlementEntity)
        {
            DbWorkflowScope transactionScope = InvoiceHeaderDataService.WorkflowScope;
            bool isSuccess = false;

            try
            {
                using (transactionScope)
                {
                    try
                    {
                        InvoiceHeaderDAO invoiceHeaderService = new InvoiceHeaderDAO(transactionScope);
                        //InvoiceDetailsDAO invoiceDetailsService = new InvoiceDetailsDAO(transactionScope);
                        //InvoiceSchemeDAO invoiceSchemeService = new InvoiceSchemeDAO(transactionScope);

                        OriginatorDAO originatorService = new OriginatorDAO();

                        OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);
                        if (originator == null)
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        //InvoiceHeaderEntity invoiceHeader = new InvoiceHeaderEntity();
                        //invoiceHeader = invoiceHeaderService.GetInvoiceHeaderByInvoiceNo(invoiceSettlementEntity.InvoiceNo);

                        //if (invoiceHeader.IvceId > 0)
                        //{
                        //    //Insert new Settlements
                        //    if (invoiceSettlementEntity != null)
                        //    {
                        //        int invoiceSettlementId = 0;
                        //        invoiceSettlementEntity.InvoiceId = invoiceId;
                        //        isSuccess = invoiceHeaderService.InsertInvoiceSettlement(out invoiceSettlementId, invoiceSettlementEntity);
                        //        if (isSuccess)
                        //        {
                        //            foreach (var item in invoiceHeader.InvoiceSettlement.InvoiceSettlementDetailsList)
                        //            {
                        //                item.CreditSettlementId = invoiceSettlementId;
                        //                isSuccess = invoiceHeaderService.InsertInvoiceSettlementDetails(item);
                        //                if (!isSuccess)
                        //                {
                        //                    break;
                        //                }
                        //            }

                        //            foreach (var item in invoiceSettlementEntity.InvoiceSettlementChequeList)
                        //            {
                        //                item.InvoiceSettlementId = invoiceSettlementId;
                        //                isSuccess = invoiceHeaderService.InsertInvoiceSettlementCheque(item);
                        //                if (!isSuccess)
                        //                {
                        //                    break;
                        //                }
                        //            }
                        //        }
                        //    }
                        //}

                        //Insert new Settlements
                        if (invoiceSettlementEntity != null)
                        {
                            int invoiceSettlementId = 0;
                            //Comment By Irosh for Add Features from Generic Vertion to Perfetti 2017/02/22
                            ////////////////////////////////////////////////////////////////////////////
                            //InvoiceHeaderEntity invoiceHeader = new InvoiceHeaderEntity();
                            //invoiceHeader = invoiceHeaderService.GetInvoiceHeaderByInvoiceNo(invoiceSettlementEntity.InvoiceNo);
                            //invoiceSettlementEntity.InvoiceId = invoiceHeader.IvceId;



                            if (invoiceSettlementEntity.InvoiceSettlementDetailsList.Count > 0)
                            {
                                try
                                {
                                    if (invoiceHeaderService.IsInvoiceSettlementExist(invoiceSettlementEntity.credit_settle_no))
                                    {
                                        if (invoiceSettlementEntity.IsCancel == "Y")
                                        {
                                            CancelCreditSettlement(accessToken, invoiceSettlementEntity.credit_settle_no);
                                            //InvoiceHeaderEntity invoiceHeaderTemp = new InvoiceHeaderEntity();
                                            //invoiceHeaderTemp = invoiceHeaderService.GetInvoiceHeaderByInvoiceNo(invoiceHeader.InvoiceNo);
                                            //invoiceHeaderService.UpdateOutstandingForInvoice(invoiceHeaderTemp.IvceId);
                                            transactionScope.Commit();
                                            return true;
                                        }
                                        else
                                        {
                                            transactionScope.Rollback();
                                            return true;
                                        }
                                    }
                                }
                                catch { return false; }

                                isSuccess = invoiceHeaderService.InsertInvoiceSettlement(out invoiceSettlementId, invoiceSettlementEntity);

                                if (isSuccess)
                                {
                                    foreach (var item in invoiceSettlementEntity.InvoiceSettlementDetailsList)
                                    {
                                        InvoiceHeaderEntity invoiceHeader = new InvoiceHeaderEntity();
                                        invoiceHeader = invoiceHeaderService.GetInvoiceHeaderByInvoiceNo(item.InvoiceNo);
                                        item.InvoiceId = invoiceHeader.IvceId;
                                        item.CreditSettlementId = invoiceSettlementId;
                                        isSuccess = invoiceHeaderService.InsertInvoiceSettlementDetails(item);
                                        if (!isSuccess)
                                        {
                                            transactionScope.Rollback();
                                            return false;
                                        }
                                    }

                                    foreach (var item in invoiceSettlementEntity.InvoiceSettlementChequeList)
                                    {
                                        item.InvoiceSettlementId = invoiceSettlementId;
                                        isSuccess = invoiceHeaderService.InsertInvoiceSettlementCheque(item);
                                        if (!isSuccess)
                                        {
                                            transactionScope.Rollback();
                                            return false;
                                        }
                                    }
                                }

                                if (invoiceSettlementEntity.IsCancel == "N")
                                {
                                    invoiceHeaderService.UpdateOutstandingForInvoiceSettlement(invoiceSettlementId);
                                }
                            }
                            else
                            {
                                transactionScope.Rollback();
                                return false;
                            }
                        }
                    }
                    catch
                    {
                        transactionScope.Rollback();
                        return false;
                    }
                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                return false;
            }

            return isSuccess;
        }

        public bool InsertCreditSettlement_old(string accessToken, InvoiceSettlementEntity invoiceSettlementEntity)
        {
            DbWorkflowScope transactionScope = InvoiceHeaderDataService.WorkflowScope;
            bool isSuccess = true;
            int invoiceId = 0;

            try
            {
                using (transactionScope)
                {
                    try
                    {
                        InvoiceHeaderDAO invoiceHeaderService = new InvoiceHeaderDAO(transactionScope);
                        //InvoiceDetailsDAO invoiceDetailsService = new InvoiceDetailsDAO(transactionScope);
                        //InvoiceSchemeDAO invoiceSchemeService = new InvoiceSchemeDAO(transactionScope);

                        OriginatorDAO originatorService = new OriginatorDAO();

                        OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);
                        if (originator == null)
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        //InvoiceHeaderEntity invoiceHeader = new InvoiceHeaderEntity();
                        //invoiceHeader = invoiceHeaderService.GetInvoiceHeaderByInvoiceNo(invoiceSettlementEntity.InvoiceNo);

                        //if (invoiceHeader.IvceId > 0)
                        //{
                        //    //Insert new Settlements
                        //    if (invoiceSettlementEntity != null)
                        //    {
                        //        int invoiceSettlementId = 0;
                        //        invoiceSettlementEntity.InvoiceId = invoiceId;
                        //        isSuccess = invoiceHeaderService.InsertInvoiceSettlement(out invoiceSettlementId, invoiceSettlementEntity);
                        //        if (isSuccess)
                        //        {
                        //            foreach (var item in invoiceHeader.InvoiceSettlement.InvoiceSettlementDetailsList)
                        //            {
                        //                item.CreditSettlementId = invoiceSettlementId;
                        //                isSuccess = invoiceHeaderService.InsertInvoiceSettlementDetails(item);
                        //                if (!isSuccess)
                        //                {
                        //                    break;
                        //                }
                        //            }

                        //            foreach (var item in invoiceSettlementEntity.InvoiceSettlementChequeList)
                        //            {
                        //                item.InvoiceSettlementId = invoiceSettlementId;
                        //                isSuccess = invoiceHeaderService.InsertInvoiceSettlementCheque(item);
                        //                if (!isSuccess)
                        //                {
                        //                    break;
                        //                }
                        //            }
                        //        }
                        //    }
                        //}

                        //Insert new Settlements
                        if (invoiceSettlementEntity != null)
                        {
                            int invoiceSettlementId = 0;
                            //Comment By Irosh for Add Features from Generic Vertion to Perfetti 2017/02/22
                            ////////////////////////////////////////////////////////////////////////////
                            //InvoiceHeaderEntity invoiceHeader = new InvoiceHeaderEntity();
                            //invoiceHeader = invoiceHeaderService.GetInvoiceHeaderByInvoiceNo(invoiceSettlementEntity.InvoiceNo);
                            //invoiceSettlementEntity.InvoiceId = invoiceHeader.IvceId;



                            if (invoiceSettlementEntity.InvoiceSettlementDetailsList.Count > 0)
                            {
                                try
                                {
                                    if (invoiceHeaderService.IsInvoiceSettlementExist(invoiceSettlementEntity.credit_settle_no))
                                    {
                                        if (invoiceSettlementEntity.IsCancel == "Y")
                                        {
                                            CancelCreditSettlement(accessToken, invoiceSettlementEntity.credit_settle_no);
                                            //InvoiceHeaderEntity invoiceHeaderTemp = new InvoiceHeaderEntity();
                                            //invoiceHeaderTemp = invoiceHeaderService.GetInvoiceHeaderByInvoiceNo(invoiceHeader.InvoiceNo);
                                            //invoiceHeaderService.UpdateOutstandingForInvoice(invoiceHeaderTemp.IvceId);
                                            transactionScope.Commit();
                                            return true;
                                        }
                                        else
                                        {
                                            transactionScope.Rollback();
                                            return true;
                                        }
                                    }
                                }
                                catch { }

                                isSuccess = invoiceHeaderService.InsertInvoiceSettlement(out invoiceSettlementId, invoiceSettlementEntity);

                                if (isSuccess)
                                {
                                    foreach (var item in invoiceSettlementEntity.InvoiceSettlementDetailsList)
                                    {
                                        InvoiceHeaderEntity invoiceHeader = new InvoiceHeaderEntity();
                                        invoiceHeader = invoiceHeaderService.GetInvoiceHeaderByInvoiceNo(item.InvoiceNo);
                                        item.InvoiceId = invoiceHeader.IvceId;
                                        item.CreditSettlementId = invoiceSettlementId;
                                        isSuccess = invoiceHeaderService.InsertInvoiceSettlementDetails(item);
                                        if (!isSuccess)
                                        {
                                            break;
                                        }
                                    }

                                    foreach (var item in invoiceSettlementEntity.InvoiceSettlementChequeList)
                                    {
                                        item.InvoiceSettlementId = invoiceSettlementId;
                                        isSuccess = invoiceHeaderService.InsertInvoiceSettlementCheque(item);
                                        if (!isSuccess)
                                        {
                                            break;
                                        }
                                    }
                                }

                                if (invoiceSettlementEntity.IsCancel == "N")
                                {
                                    invoiceHeaderService.UpdateOutstandingForInvoiceSettlement(invoiceSettlementId);
                                }
                            }
                            else
                            {
                                transactionScope.Rollback();
                                return false;
                            }
                        }
                    }
                    catch
                    {
                        transactionScope.Rollback();
                        throw;
                    }
                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public List<InvoiceHeaderEntity> GetAllInvoicesDataAndCount(ArgsEntity args)
        {
            List<InvoiceHeaderEntity> invoiceHeaderList = new List<InvoiceHeaderEntity>();
            try
            {
                InvoiceDetailsDAO invoiceDetailService = new InvoiceDetailsDAO();
                InvoiceSchemeDAO invoiceSchemeService = new InvoiceSchemeDAO();

                invoiceHeaderList = InvoiceHeaderDataService.GetAllInvoicesDataAndCount(args);
                foreach (var item in invoiceHeaderList)
                {
                    item.InvoiceDetailList = invoiceDetailService.GetInvoiceDetailsByInvoiceId(item.IvceId,args.SQuantitytype);
                    item.InvoiceSchemeGroupList = invoiceSchemeService.GetInvoiceSchemeTotDiscountByInvoiceId(item.IvceId);

                    foreach (var schemeGroup in item.InvoiceSchemeGroupList)
                    {
                        schemeGroup.InvoiceSchemeList = invoiceSchemeService.GetDiscountSchemeByInvoiceId(schemeGroup.SchemeGroupId);
                    }
                }
            }
            catch
            {
                throw;
            }
            return invoiceHeaderList;
        }



        public bool InvoiceSettlementSave(string accessToken, InvoiceSettlementEntity invoiceSettlementEntity)
        {
            DbWorkflowScope transactionScope = InvoiceHeaderDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    try
                    {
                        InvoiceHeaderDAO invoiceHeaderService = new InvoiceHeaderDAO(transactionScope);
                        InvoiceDetailsDAO invoiceDetailsService = new InvoiceDetailsDAO(transactionScope);
                        InvoiceSchemeDAO invoiceSchemeService = new InvoiceSchemeDAO(transactionScope);

                        OriginatorDAO originatorService = new OriginatorDAO();

                        OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);
                        if (originator == null)
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        if (invoiceHeaderService.IsInvoiceSettlementExist(invoiceSettlementEntity.SettlementNo))
                        {
                            isSuccess = invoiceHeaderService.UpdateInvoiceSettlement(invoiceSettlementEntity);
                        }
                        else
                        {
                            int id = 0;
                            isSuccess = invoiceHeaderService.InsertInvoiceSettlement(out id, invoiceSettlementEntity);
                        }


                    }
                    catch
                    {
                        transactionScope.Rollback();
                        throw;
                    }
                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public List<InvoiceSettlementEntity> GetInvoiceSettlementByAccessToken(string accessToken)
        {
            try
            {
                return InvoiceHeaderDataService.GetInvoiceSettlementByAccessToken(accessToken);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteInvoiceHeader(string accessToken, string invoiceNo)
        {
            DbWorkflowScope transactionScope = InvoiceHeaderDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    try
                    {
                        InvoiceHeaderDAO invoiceHeaderService = new InvoiceHeaderDAO(transactionScope);

                        OriginatorDAO originatorService = new OriginatorDAO();

                        OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);
                        if (originator == null)
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        InvoiceHeaderEntity invoiceHeader = new InvoiceHeaderEntity();
                        invoiceHeader = invoiceHeaderService.GetInvoiceHeaderByInvoiceNo(invoiceNo);

                        if (invoiceHeader.IvceId > 0)
                        {
                            //Delete Invoice Header
                            invoiceHeader.LastModifiedBy = originator.Originator;
                            isSuccess = invoiceHeaderService.DeleteInvoiceHeader(invoiceHeader);
                            invoiceHeaderService.UpdateOutstandingForDeleteInvoice(invoiceHeader.IvceId);
                        }
                    }
                    catch
                    {
                        transactionScope.Rollback();
                        throw;
                    }
                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool DeleteInvoiceHeaderByInvoiceNo(string originator, string invoiceNo)
        {
            DbWorkflowScope transactionScope = InvoiceHeaderDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    try
                    {
                        InvoiceHeaderDAO invoiceHeaderService = new InvoiceHeaderDAO(transactionScope);

                        InvoiceHeaderEntity invoiceHeader = new InvoiceHeaderEntity();
                        invoiceHeader = invoiceHeaderService.GetInvoiceHeaderByInvoiceNo(invoiceNo);

                        if (invoiceHeader.IvceId > 0)
                        {
                            //Delete Invoice Header
                            invoiceHeader.LastModifiedBy = originator;
                            isSuccess = invoiceHeaderService.DeleteInvoiceHeader(invoiceHeader);
                            invoiceHeaderService.UpdateOutstandingForDeleteInvoice(invoiceHeader.IvceId);
                        }
                    }
                    catch
                    {
                        transactionScope.Rollback();
                        throw;
                    }
                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool DeleteInvoiceHeaderForAddInvoice(string accessToken, string invoiceNo)
        {
            DbWorkflowScope transactionScope = InvoiceHeaderDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    try
                    {
                        InvoiceHeaderDAO invoiceHeaderService = new InvoiceHeaderDAO(transactionScope);

                        OriginatorDAO originatorService = new OriginatorDAO();

                        OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);
                        if (originator == null)
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        InvoiceHeaderEntity invoiceHeader = new InvoiceHeaderEntity();
                        invoiceHeader = invoiceHeaderService.GetInvoiceHeaderByInvoiceNo(invoiceNo);

                        if (invoiceHeader.IvceId > 0)
                        {
                            //Delete Invoice Header
                            invoiceHeader.LastModifiedBy = originator.Originator;
                            isSuccess = invoiceHeaderService.DeleteInvoiceHeader(invoiceHeader);
                            invoiceHeaderService.UpdateOutstandingForInvoice(invoiceHeader.IvceId);
                        }
                    }
                    catch
                    {
                        transactionScope.Rollback();
                        throw;
                    }
                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool DeleteInvoiceByInvID(InvoiceHeaderEntity tmeInvoiceHeader)
        {
            DbWorkflowScope transactionScope = InvoiceHeaderDataService.WorkflowScope;
            bool successHeader = false;
            bool successDetails = false;

            try
            {
                using (transactionScope)
                {
                    InvoiceHeaderDAO invHeaderDataService = new InvoiceHeaderDAO(transactionScope);
                    InvoiceDetailsDAO invDetailsDataService = new InvoiceDetailsDAO(transactionScope);

                    successHeader = invHeaderDataService.DeleteInvoiceHeader(tmeInvoiceHeader);

                    if (successHeader)
                    {
                        successDetails = invDetailsDataService.DeleteInvoiceDetailsPhysical(tmeInvoiceHeader.IvceId);
                        invHeaderDataService.UpdateOutstandingForDeleteInvoice(tmeInvoiceHeader.IvceId);
                    }

                    if (successDetails)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }

            return successDetails;
        }

        //Add By Irosh for Add Features from Generic Vertion to Perfetti 2017/02/22
        ///////////////////////////////////////////////////////////////////////////
        public bool CancelCreditSettlement(string accessToken, string settlementNo)
        {
            DbWorkflowScope transactionScope = InvoiceHeaderDataService.WorkflowScope;
            bool isSuccess = true;
            try
            {
                using (transactionScope)
                {
                    try
                    {
                        InvoiceHeaderDAO invoiceHeaderService = new InvoiceHeaderDAO(transactionScope);

                        OriginatorDAO originatorService = new OriginatorDAO();
                        OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);
                        if (originator == null)
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        InvoiceSettlementEntity invoiceSettlementHeader = new InvoiceSettlementEntity();
                        invoiceSettlementHeader = invoiceHeaderService.GetInvoiceSettlementBySettlementNo(settlementNo);

                        if (invoiceSettlementHeader.Id > 0)
                        {
                            //Delete Invoice Header
                            invoiceSettlementHeader.SettlementNo = settlementNo;
                            isSuccess = invoiceHeaderService.CancelInvoiceSettlementNo(invoiceSettlementHeader);
                            invoiceHeaderService.UpdateOutstandingForCancelInvoiceSettlement(invoiceSettlementHeader.Id);
                        }
                    }
                    catch
                    {
                        transactionScope.Rollback();
                        throw;
                    }
                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool ReturnInvoices(string accessToken, ReturnInvoiceEntity invoiceHeader)
        {
            DbWorkflowScope transactionScope = InvoiceHeaderDataService.WorkflowScope;
            bool isSuccess = true;
            int returnId = 0;
            int invoiceid = 0;

            try
            {
                using (transactionScope)
                {
                    try
                    {
                        InvoiceHeaderDAO invoiceHeaderService = new InvoiceHeaderDAO(transactionScope);
                        InvoiceDetailsDAO invoiceDetailsService = new InvoiceDetailsDAO(transactionScope);
                        OriginatorDAO originatorService = new OriginatorDAO();
                        OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);

                        if (originator == null)
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        isSuccess = invoiceHeaderService.ReturnInvoiceHeader(out returnId, invoiceHeader);

                        if (isSuccess)
                        {
                            ////invoiceDetailsService.DeleteInvoiceDetails(returnId);
                            foreach (var item in invoiceHeader.ReturnInvoiceLineList)
                            {
                                item.returnid = returnId;
                                invoiceid = 0;
                                invoiceid = invoiceHeaderService.GetInvoiceHeaderByInvoiceNo(item.invoiceno).IvceId;
                                isSuccess = invoiceDetailsService.ReturnInvoiceLine(invoiceid, item);

                                if (!isSuccess)
                                {
                                    break;
                                }
                            }

                            foreach (var item in invoiceHeader.InvoiceDetailList)
                            {
                                item.returnid = returnId;
                                invoiceid = 0;
                                try
                                {
                                    invoiceid = invoiceHeaderService.GetInvoiceHeaderByInvoiceNo(item.invoiceno).IvceId;
                                }
                                catch { }
                                isSuccess = invoiceDetailsService.ReturnInvoiceDetails(invoiceid, item);

                                if (!isSuccess)
                                {
                                    break;
                                }
                            }
                        }
                    }
                    catch
                    {
                        transactionScope.Rollback();
                        throw;
                    }
                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public bool CancelReturnInvoices(string accessToken, string ReturnInvoiceNo)
        {
            DbWorkflowScope transactionScope = InvoiceHeaderDataService.WorkflowScope;
            bool isSuccess = true;
            int returnId = 0;
            int invoiceid = 0;

            try
            {
                using (transactionScope)
                {
                    try
                    {
                        InvoiceHeaderDAO invoiceHeaderService = new InvoiceHeaderDAO(transactionScope);
                        OriginatorDAO originatorService = new OriginatorDAO();
                        OriginatorEntity originator = originatorService.GetOriginatorByAccessToken(accessToken);

                        if (originator == null)
                        {
                            transactionScope.Rollback();
                            return false;
                        }

                        isSuccess = invoiceHeaderService.CancelReturnInvoiceHeader(ReturnInvoiceNo);
                    }
                    catch
                    {
                        transactionScope.Rollback();
                        throw;
                    }

                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public List<RepsOutletWiseDetailsEntity> GetRepsOutletWiseDetailsByAccessToken(string accessToken, string date)
        {
            List<RepsOutletWiseDetailsEntity> headerList = new List<RepsOutletWiseDetailsEntity>();

            try
            {
                headerList = InvoiceHeaderDataService.GetAllRepsOutletWiseDetails(accessToken, date);
            }
            catch
            {
                throw;
            }

            return headerList;
        }

        public List<RepsSKUMasterEntity> GetRepsSKUWiseDetailsByAccessToken(string accessToken, string date)
        {
            List<RepsSKUMasterEntity> headerList = new List<RepsSKUMasterEntity>();

            try
            {
                headerList = InvoiceHeaderDataService.GetAllRepsSKUWiseDetails(accessToken, date);
            }
            catch
            {
                throw;
            }

            return headerList;
        }

        public bool DeleteBulkInvoices(string userType, string userName, string invList)
        {
            try
            {
                return InvoiceHeaderDataService.DeleteBulkInvoices(userType, userName, invList);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
