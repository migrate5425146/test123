﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using System.Data;

namespace Peercore.CRM.BusinessRules
{
    public class LookupBR
    {
        private static LookupBR instance = null;
        private static object syncLock = new Object();
        private LookupDAO LookupDataService;

         public static LookupBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new LookupBR();
                }
                return instance;
            }
        }

         private LookupBR()
        {
            LookupDataService = new LookupDAO();
        }

         #region - Init Object -

         public LookupEntity CreateObject()
         {
             return LookupDataService.CreateObject();
         }

         #endregion

         public List<LookupEntity> GetLookups(string lookupSqlKey, string whereCls = "", object[] parameters = null, ArgsEntity args = null, bool hasArgs = false)
         {
             LookupTransferEntity lookupTransferEntity = LookupDataService.GetLookUpStatment(lookupSqlKey, whereCls, parameters);

             return GetValues(lookupTransferEntity, args);
         }

         public void GetValues(ref DataTable dtLookup, string sTableName, string sColumnNames, string sDatabase, string sWhereCls)
         {
             try
             {
                 LookupDataService.GetValues(ref dtLookup, sTableName, sColumnNames, sDatabase, sWhereCls);
             }
             catch
             {
                 throw;
             }
         }

         public void GetValues(ref DataTable dtLookup, string sql)
         {
             try
             {
                 LookupDataService.GetValues(ref dtLookup, sql);
             }
             catch
             {
                 throw;
             }
         }

         public List<LookupEntity> GetValues(string sql)
         {
             try
             {
                 return LookupDataService.GetValues(sql);
             }
             catch
             {
                 throw;
             }
         }

         public List<LookupEntity> GetValues(LookupTransferEntity lookupTransferComponent, ArgsEntity args = null)
         {
             try
             {
                 List<LookupEntity> lookupList = LookupDataService.GetValues(lookupTransferComponent, args);
                 
                 return lookupList;
             }
             catch
             {
                 throw;
             }
         }

         public List<LookupEntity> GetValues(LookupTransferEntity lookupTransferComponent)
         {
             try
             {
                 List<LookupEntity> lookupList = LookupDataService.GetValues(lookupTransferComponent);

                 return lookupList;
             }
             catch
             {
                 throw;
             }
         }

         public List<LookupEntity> GetPeercoreLookupValues(string sTableId)
         {
             try
             {
                 return LookupDataService.GetValues(sTableId);
             }
             catch
             {
                 throw;
             }
         }

         public List<LookupEntity> GetLookupsForBDM(KeyValuePair<string, string> descriptionColumn, KeyValuePair<string, string>? codeColumn,
            ArgsEntity args, string primaryKey = "")
         {
             LookupTransferEntity LookupSqlInfo = LookupDataService.GenerateSelect(descriptionColumn, codeColumn, primaryKey);
             return LookupDataService.GetValues(LookupSqlInfo, args);
         }
    }
}
