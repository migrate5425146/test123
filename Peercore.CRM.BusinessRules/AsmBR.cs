﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.CRM.Model;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class AsmBR 
    {
        private static AsmBR instance = null;
        private static object syncLock = new Object();

        private AsmDAO AsmDataService;
        private OriginatorDAO OriginatorDataService;

        public static AsmBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new AsmBR();
                }
                return instance;
            }
        }

        private AsmBR()
        {
            AsmDataService = new AsmDAO();
            OriginatorDataService = new OriginatorDAO();
        }

        //public bool DeleteArea(int areaId)
        //{
        //    try
        //    {
        //        return AsmDataService.DeleteArea(areaId);
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        public List<AsmModel> GetAllAsm(ArgsModel args)
        {
            try
            {
                return AsmDataService.GetAllAsm(args);
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorImeiModel> GetAllOriginatorImeiByAsmId(ArgsModel args, string asmId)
        {
            List<OriginatorImeiModel> objList = new List<OriginatorImeiModel>();
            try
            {
                AsmModel objAsm = new AsmModel();
                objAsm = AsmDataService.GetAsmByAsmId(asmId);

                if (objAsm.AreaId > 0)
                {
                    objList = OriginatorDataService.GetAllOriginatorImeiByOriginator(args, objAsm.AsmUserName);
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public List<AsmModel> GetAllAsmByAreaId(ArgsModel args, string AreaId)
        {
            try
            {
                return AsmDataService.GetAllAsmByAreaId(args, AreaId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteAsmByAsmId(string asmId)
        {
            try
            {
                return AsmDataService.DeleteAsmByAsmId(asmId);
            }
            catch
            {
                throw;
            }
        }
        
        public bool DeleteIMEIByAsmId(string asm_id, string emei_no)
        {
            bool retVal = false;

            try
            {
                AsmModel objAsm = new AsmModel();
                objAsm = AsmDataService.GetAsmByAsmId(asm_id);

                if (objAsm.AreaId > 0)
                {
                    retVal = OriginatorDataService.DeleteEMEIFromOriginator(objAsm.AsmUserName, emei_no);
                }
                else
                {
                    retVal = false;
                }
            }
            catch
            {
                throw;
            }

            return retVal;
        }
        
        public bool UpdateOriginatorIsIMEIByAsmId(string asmId, bool isImei)
        {
            bool retVal = false;

            try
            {
                AsmModel objAsm = new AsmModel();
                objAsm = AsmDataService.GetAsmByAsmId(asmId);

                if (objAsm.AreaId > 0)
                {
                    retVal = OriginatorDataService.UpdateOriginatorIsIMEIByOriginator(objAsm.AsmUserName, isImei);
                }
                else
                {
                    retVal = false;
                }
            }
            catch
            {
                throw;
            }

            return retVal;
        }
        
        public bool UpdateAsmPassword(string asmId, string password)
        {
            bool retVal = false;

            try
            {
                AsmModel objAsm = new AsmModel();
                objAsm = AsmDataService.GetAsmByAsmId(asmId);

                if (objAsm.AsmId > 0)
                {
                    retVal = OriginatorDataService.UpdateOriginatorPassword(objAsm.AsmUserName, password);
                }
                else
                {
                    retVal = false;
                }
            }
            catch
            {
                throw;
            }

            return retVal;
        }

        public bool InsertUpdateArea(string area_id,
                            string asm_id,
                            string asm_code,
                            string asm_name,
                            string asm_username,
                            string asm_tel1,
                            string asm_email,
                            string created_by)
        {
            try
            {
                return AsmDataService.InsertUpdateAsm(area_id,
                            asm_id,
                            asm_code,
                            asm_name,
                            asm_username,
                            asm_tel1,
                            asm_email,
                            created_by);
            }
            catch
            {
                throw;
            }
        }
    }
}
