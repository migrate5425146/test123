﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;

namespace Peercore.CRM.BusinessRules
{
    public class ActivityTypeBR
    {
        private static ActivityTypeBR instance = null;
        private static object syncLock = new Object();

        private ActivityTypeDAO ActivityTypeDataService;

        public static ActivityTypeBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new ActivityTypeBR();
                }
                return instance;
            }
        }

        private ActivityTypeBR()
        {
            ActivityTypeDataService = new ActivityTypeDAO();
        }

        #region - Init Object -

        public ActivityTypeEntity CreateObject()
        {
            return ActivityTypeDataService.CreateObject();
        }

        public ActivityTypeEntity CreateObject(int entityId)
        {
            return ActivityTypeDataService.CreateObject(entityId);
        }

        #endregion

        public List<ActivityTypeEntity> GetAllActivityTypes(string defaultDeptID, bool IsSchedular = false)
        {
            try
            {
                return ActivityTypeDataService.GetAllActivityTypes(defaultDeptID, IsSchedular);
            }
            catch
            {
                throw;
            }
        }

        public ActivityTypeEntity GetActivityType(string code, ArgsEntity args)
        {
            try
            {
                return ActivityTypeDataService.GetActivityType(code, args);
            }
            catch
            {
                throw;
            }
        }

        public List<ActivityTypeEntity> GetAllFixedActivityTypes(bool bShownInPlanner = false)
        {
            try
            {
                return ActivityTypeDataService.GetAllFixedActivityTypes(bShownInPlanner);
            }
            catch
            {
                throw;
            }
        }

        public List<ActivityTypeEntity> GetColdCallActyvityTypes()
        {
            try
            {
                return ActivityTypeDataService.GetColdCallActyvityTypes();
            }
            catch
            {
                throw;
            }
        }

        
    }
}
