﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class DivisionBR
    {
        private static DivisionBR instance = null;
        private static object syncLock = new Object();

        private DivisionDAO DivisionDataService;

        public static DivisionBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new DivisionBR();
                }
                return instance;
            }
        }

        private DivisionBR()
        {
            DivisionDataService = new DivisionDAO();
        }

        #region - Init Object -

        public DivisionEntity CreateObject()
        {
            return DivisionDataService.CreateObject();
        }

        public DivisionEntity CreateObject(int entityId)
        {
            return DivisionDataService.CreateObject(entityId);
        }

        #endregion

        public List<DivisionEntity> GetAllDivisions(ArgsEntity args)
        {
            try
            {
                return DivisionDataService.GetAllDivisions(args);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveDivision(out int divisionId,DivisionEntity divisionEntity)
        {
            DbWorkflowScope transactionScope = DivisionDataService.WorkflowScope;
            bool isSuccess = true;
            divisionId = 0;
            try
            {
                using (transactionScope)
                {
                    if (divisionEntity.DivisionId > 0)
                    {
                        isSuccess = DivisionDataService.UpdateDivision(divisionEntity);
                    }
                    else
                    {
                        isSuccess = DivisionDataService.InsertDivision(out divisionId,divisionEntity);
                    }
                    if (isSuccess)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();

                }
            }
            catch
            {
                throw;
            }
            return isSuccess;
        }

        public DivisionEntity GetDivisionById(int divisionId)
        {
            try
            {
                return DivisionDataService.GetDivisionById(divisionId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteDivision(int divisionId)
        {
            try
            {
                return DivisionDataService.DeleteDivision(divisionId);
            }
            catch
            {
                throw;
            }
        }
    }
}
