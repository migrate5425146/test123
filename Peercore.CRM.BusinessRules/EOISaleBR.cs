﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class EOISaleBR
    {
        private static EOISaleBR instance = null;
        private static object syncLock = new Object();

        private EOISaleDAO EOISaleDataService;

        public static EOISaleBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new EOISaleBR();
                }
                return instance;
            }
        }

        private EOISaleBR()
        {
            EOISaleDataService = new EOISaleDAO();
        }

        #region - Init Object -

        public EOISalesEntity CreateObject()
        {
            return EOISaleDataService.CreateObject();
        }

        public EOISalesEntity CreateObject(int entityId)
        {
            return EOISaleDataService.CreateObject(entityId);
        }

        #endregion

        public List<EOISalesEntity> GetEOISales(ArgsEntity args)
        {
            try
            {
                return EOISaleDataService.GetEOISales(args);
            }
            catch
            {
                throw;
            }
        }
    }
}
