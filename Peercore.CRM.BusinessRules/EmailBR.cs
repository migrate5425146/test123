﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.CRM.Model;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class EmailBR
    {
        private static EmailBR instance = null;
        private static object syncLock = new Object();

        private EmailDAO EmailDAOService;

        public static EmailBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new EmailBR();
                }
                return instance;
            }
        }

        private EmailBR()
        {
            EmailDAOService = new EmailDAO();
        }

        public List<KPIReports> GetAllKPIReports()
        {
            try
            {
                return EmailDAOService.GetAllKPIReports();
            }
            catch
            {
                return new List<KPIReports>();
            }
        }
        
        public List<KPIReportUser> GetAllKPIReportUsersByReportId(int ReportId)
        {
            try
            {
                return EmailDAOService.GetAllKPIReportUsersByReportId(ReportId);
            }
            catch
            {
                return new List<KPIReportUser>();
            }
        }

        public bool UpdateKPIReport(KPIReports kpiReport)
        {
            bool isSuccess = false;

            try
            {
                EmailDAOService.UpdateKPIReport(kpiReport);

                foreach (KPIReportUser user in kpiReport.ReportUserList)
                {
                    EmailDAOService.UpdateKPIReportUser(user);
                }

                isSuccess = true;
            }
            catch
            {
                isSuccess = false;
            }

            return isSuccess;
        }


    }
}
