﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.CRM.Model;
using Peercore.Workflow.Common;

namespace Peercore.CRM.BusinessRules
{
    public class CommonBR
    {
        private static CommonBR instance = null;
        private static object syncLock = new Object();

        private CommonDAO CommonDAOService;

        public static CommonBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new CommonBR();
                }
                return instance;
            }
        }

        private CommonBR()
        {
            CommonDAOService = new CommonDAO();
        }

        public MobileRepsDashboardModel LoadMobileRepsDashboard()
        {
            try
            {
                return CommonDAOService.LoadMobileRepsDashboard();
            }
            catch
            {
                return new MobileRepsDashboardModel();
            }
        }

        public bool SaveMobileRepDashboard(MobileRepsDashboardModel mobileDashboard)
        {
            bool isSuccess = false;

            try
            {
                isSuccess = CommonDAOService.SaveMobileRepDashboard(mobileDashboard);
            }
            catch
            {
                throw;
            }

            return isSuccess;
        }

        public List<CustomerCategory> GetAllCustomerCategory()
        {
            try
            {
                return CommonDAOService.GetAllCustomerCategory();
            }
            catch (Exception)
            {
                return new List<CustomerCategory>();
            }
        }

        public bool UploadOutletBulkTransfer(string Originator, int NewRouteId, string CustomerCode, string CustomerName)
        {
            bool isSuccess = false;

            try
            {
                isSuccess = CommonDAOService.UploadOutletBulkTransfer(Originator, NewRouteId, CustomerCode, CustomerName);
            }
            catch (Exception)
            {
                isSuccess = false;
            }

            return isSuccess;
        }

    }
}
