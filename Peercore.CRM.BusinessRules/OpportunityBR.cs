﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.DataAccess.DAO;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using System.Data;

namespace Peercore.CRM.BusinessRules
{
    public class OpportunityBR
    {
        private static OpportunityBR instance = null;
        private static object syncLock = new Object();
        private OpportunityDAO OpportunityService;
        private CustomerOpportunityDAO CustomerOpportunityService;

        public static OpportunityBR Instance
        {
            get
            {
                lock (syncLock)
                {
                    if (instance == null)
                        instance = new OpportunityBR();
                }
                return instance;
            }
        }

        private OpportunityBR()
        {
            OpportunityService = new OpportunityDAO();
            CustomerOpportunityService = new CustomerOpportunityDAO();
        }

        #region Old Methods
        /*
        public OpportunityCompositeEntity CreateObject()
        {
            return OpportunityService.CreateObject();
        }

        public OpportunityCompositeEntity CreateObject(Guid entityId)
        {
            return OpportunityService.CreateObject(entityId);
        }

        public List<OpportunityEntity> GetOpportunities(Guid LeadId, Guid OriginatorId)
        {
            try
            {
                return OpportunityService.GetOpportunities(LeadId, OriginatorId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool InsertOpportunities(OpportunityEntity opportunity)
        {
            try
            {
                return OpportunityService.InsertOpportunities(opportunity);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool UpdateOpportunities(OpportunityEntity opportunity)
        {
            try
            {
                return OpportunityService.UpdateOpportunities(opportunity);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public OpportunityCompositeEntity GetOpportunity(string OpportunityId)
        {
            try
            {
                return OpportunityService.GetOpportunity(OpportunityId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<OpportunityCompositeEntity> GetContactOpportunities(int pipelineStageID, string Originator, DateTime dtmCloseDate, string orderby)
        {
            try
            {
                return OpportunityService.GetContactOpportunities(pipelineStageID, Originator, dtmCloseDate, orderby);
            }
            catch (Exception)
            {

                throw;
            }
        }
        */
        #endregion

        public bool InsertOpportunity(OpportunityEntity opportunity, out int opportunityid)
        {
            try
            {
                return OpportunityService.Insert(opportunity, out opportunityid);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateOpportunity(OpportunityEntity opportunity)
        {
            try
            {
                return OpportunityService.Update(opportunity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OpportunityEntity> GetOpportunities(ArgsEntity args, int contactID)
        {
            try
            {
                return OpportunityService.GetOpportunities(args,contactID);
            }
            catch 
            {                
                throw;
            }
        }

        public List<OpportunityEntity> GetOpportunitiesForCustomer(string customerCode, ArgsEntity args)
        {
            try
            {
                return OpportunityService.GetOpportunitiesForCustomer(customerCode,args);
            }
            catch
            {                
                throw;
            }
        }

        public int GetOpportunityCountForHome(int iPipelineStageID, ArgsEntity args)
        {
            try
            {
                return OpportunityService.GetOpportunityCountForHome(iPipelineStageID, args);
            }
            catch
            {                
                throw;
            }
        }

        public List<CustomerOpportunityEntity> GetAllOpportunities(int iPipelineStageID, ArgsEntity args)
        {
            try
            {
                return OpportunityService.GetContactOpportunities(iPipelineStageID, args);

            }
            catch
            {
                throw;
            }
        }

        public List<ClientOpportunityEntity> GetAllClientOpportunity(int iPipelineStageID, ArgsEntity args)
        {
            try
            {
                return OpportunityService.GetAllClientOpportunity(iPipelineStageID, args);

            }
            catch
            {
                throw;
            }
        }

        public bool Delete(int opportunityId)
        {
            try
            {
                return OpportunityService.Delete(opportunityId);
            }
            catch 
            {
                throw;
            }
        }

        public List<CatalogEntity> GetOppProducts(int opportunityId, ArgsEntity args)
        {
            try
            {
                return OpportunityService.GetOppProducts(opportunityId, args);
            }
            catch
            {
                throw;
            }
        }

        public CustomerOpportunityEntity GetOpportunity(int iOpportunityID)
        {
            try
            {
                return OpportunityService.GetOpportunity(iOpportunityID);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveOpportunity(OpportunityEntity opportunity, List<CatalogEntity> lstProducts, out int opportunityid)
        {
            bool successful = false;

            DbWorkflowScope transactionScope = OpportunityService.WorkflowScope;

            try
            {
                using (transactionScope)
                {
                    OpportunityDAO opportunityDAO = new OpportunityDAO(transactionScope);
                    CatalogDAO catalogDAO = new CatalogDAO(transactionScope);

                    if (opportunity.OpportunityID == 0)
                    {
                        successful = opportunityDAO.Insert(opportunity, out opportunityid);
                    }
                    else
                    {
                        opportunityid = opportunity.OpportunityID;
                        successful = opportunityDAO.Update(opportunity);
                    }

                    if (successful)
                    {

                        foreach (CatalogEntity product in lstProducts)
                        {
                            product.OpportunityID = opportunityid;
                            successful = catalogDAO.Insert(product);

                            if (!successful)
                                break;
                        }
                    }


                    if (successful)
                        transactionScope.Commit();
                    else
                        transactionScope.Rollback();
                }
            }
            catch
            {
                throw;
            }

            return successful;
        }

        public List<CustomerOpportunityEntity> GetOpportunitiesForEndUser(ArgsEntity args)
        {
            try
            {
                return CustomerOpportunityService.GetOpportunitiesForEndUser(args);
            }
            catch
            {
                throw;
            }
        }

        /*
        

        public List<OpportunityEntity> GetContactOpportunities(int pipelineStageID, string originator, ArgsEntity args, DateTime? dtmCloseDate)
        {
            try
            {
                string repGroups = string.Empty;

                OriginatorDAO originatorDAO = new OriginatorDAO();
                repGroups = originatorDAO.GetRepGroups(originator);
                return OpportunityService.GetContactOpportunities(pipelineStageID, originator, args, dtmCloseDate, repGroups);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<OpportunityEntity> GetOpportunitiesForPipeGraph(int pipelineStageID, ArgsEntity args)
        {
            try
            {
                string repGroups = string.Empty;

                OriginatorDAO originatorDAO = new OriginatorDAO();
                repGroups = originatorDAO.GetRepGroups(args.Originator);
                return OpportunityService.GetCustomerOpportunities(pipelineStageID, repGroups,args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<OpportunityEntity> GetOpportunitiesForCustomer(string cutomerCode, ArgsEntity args)
        {
            try
            {
                return OpportunityService.GetOpportunitiesForCustomer(cutomerCode, args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool DeleteOpportunity(int opportunityId)
        {           
            try
            {
                return OpportunityService.DeleteOpportunity(opportunityId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<OpportunityEntity> GetAllOpportunitiesCount(string sOriginator, string defaultDepartmentID)
        {
            try
            {
                return OpportunityService.GetContactOpportunitiesCount(sOriginator, defaultDepartmentID);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<OpportunityEntity> GetOpportunitiesForCustomerCount(string originator, string defaultDepartmentID)
        {          
            try
            {
                string childOriginators = string.Empty;
                string repGroups = string.Empty;

                OriginatorDAO originatorDAO = new OriginatorDAO();
              
                childOriginators = originatorDAO.GetChildOriginators(originator,false,false); //Cannot use global child users since its filtered in the UI itself
                repGroups = originatorDAO.GetRepGroups(originator);

                return OpportunityService.GetOpportunitiesForCustomerCount(originator, defaultDepartmentID, childOriginators, repGroups);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int GetOpportunityForWindow(ArgsEntity args, DateTime? dtmCloseDate = null)
        {         
            try
            {
                string repGroups = string.Empty;
                OriginatorDAO originatorDAO = new OriginatorDAO();
                repGroups = originatorDAO.GetRepGroups(args.Originator);
                return OpportunityService.GetOpportunityForWindow(args,repGroups, dtmCloseDate);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public double GetOpportunitySum(ArgsEntity args)
        {         
            try
            {
                return OpportunityService.GetOpportunitySum(args);
            }
            catch (Exception)
            {

                throw;
            }
        }
       
        public OpportunityEntity GetOpportunity(int opportunityID)
        {
            try
            {
                return OpportunityService.GetOpportunity(opportunityID);
            }
            catch
            {
                throw;
            }
        }

        public List<OpportunityEntity> GetOpportunities(ArgsEntity args, int cntactID)
        {
            try
            {
                return OpportunityService.GetOpportunities(args, cntactID);               
            }
            catch
            {
                throw;
            }
        }

        public List<CatalogEntity> GetOppProducts(int opportunityID)
        {
            DataTable dtOppCatalog = new DataTable();
            DataTable dtCatalog = new DataTable();

            try
            {


                OpportunityService.GetProducts(ref dtOppCatalog, opportunityID);
                OpportunityService.GetAllProducts(ref dtCatalog);

                DataSet dsProducts = new DataSet();
                dsProducts.Tables.Add(dtOppCatalog);
                dsProducts.Tables.Add(dtCatalog);

                DataRelation relation = new DataRelation("relation", dsProducts.Tables["Table2"].Columns["catlog_code"],
                        dsProducts.Tables["Table1"].Columns["catlog_code"]);
                dsProducts.Relations.Add(relation);

                List<CatalogEntity> productList = new List<CatalogEntity>();
                foreach (DataRow drOpp in dsProducts.Tables["Table1"].Rows)
                {
                    DataRow drCatalog = drOpp.GetParentRow("relation");

                    productList.Add(new CatalogEntity()
                    {
                        CatlogCode = drOpp["catlog_code"].ToString(),
                        Description = drCatalog["description"].ToString(),
                        Price = Convert.ToDouble(drOpp["price"])
                    });
                }

                return productList;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (dtOppCatalog != null)
                    dtOppCatalog.Dispose();
                if (dtCatalog != null)
                    dtCatalog.Dispose();
            }
        }
         */
    }
}
