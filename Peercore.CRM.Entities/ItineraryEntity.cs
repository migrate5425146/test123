﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ItineraryEntity : BaseEntity
    {
        private ItineraryEntity()
        {
            this.IsNew = true;
        }

        private ItineraryEntity(int entityId)
        {
            this.ItineraryId = entityId;
        }

        public static ItineraryEntity CreateObject()
        {
            ItineraryEntity entity = new ItineraryEntity();
            return entity;
        }

        public static ItineraryEntity CreateObject(int ItineraryId)
        {
            ItineraryEntity entity = new ItineraryEntity(ItineraryId);
            return entity;
        }

        public int ItineraryId { get; set;}
        public string ItineraryName { get; set;}
        public int Month { get; set;}
        public DateTime Date { get; set;}
        public string TownsToVisit { get; set;}
        public int Sequence { get; set;}
        public int DirectMiles { get; set;}
        public int LocalMiles { get; set; }
        public int TotalMiles { get; set; }
        public string WorkProgram { get; set;}
        public string NightAt { get; set;}
        public int ActivityId { get; set; }
        public int RouteId { get; set; }
        public string BaseTown { get; set; }
    }
}
