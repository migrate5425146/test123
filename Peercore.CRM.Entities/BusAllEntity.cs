﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class BusAllEntity :BaseEntity
    {
        public BusAllEntity()
        {
        }

        public BusAllEntity(int entityId)
        {
        }

        public static BusAllEntity CreateObject()
        {
            BusAllEntity entity = new BusAllEntity();
            return entity;
        }

        public static BusAllEntity CreateObject(int entityId)
        {
            BusAllEntity entity = new BusAllEntity(entityId);
            return entity;
        }

        public string Period { get; set; }
        public double Tonne { get; set; }
        public double BudgetTonne { get; set; }
        public double Sales { get; set; }
        public double BudgetSales { get; set; }

        private double st = 0;
        public double ST
        {
            get
            {
                if (Tonne > 0)
                    st = Sales / Tonne;
                return st;
            }
        }
        public double Profit { get; set; }

        private double pt = 0;
        public double PT 
        {
            get
            {
                if (Tonne > 0)
                    pt = Profit / Tonne;
                return pt;
            }
        }

        private double percentage = 0;
        public double Percentage 
        {
            get
            {
                if (Sales > 0)
                    percentage = Profit * 100 / Sales;
                return percentage;
            }
        }
        public double LastYearTonne { get; set; }
        public double LastYearBudgetTonne { get; set; }
        public double LastYearSales { get; set; }
        public double LastYearBudgetSales { get; set; }

        private double lastYearST = 0;
        public double LastYearST
        {
            get
            {
                if (Sales > 0)
                    lastYearST = LastYearSales / LastYearTonne;
                return lastYearST;
            }
        }
        public double LastYearProfit { get; set; }

        private double lastYearPT = 0;
        public double LastYearPT 
        {
            get
            {
                if (Sales > 0)
                    lastYearPT = LastYearProfit / LastYearTonne;
                return lastYearPT;
            }
        }

        private double lastYearPercentage = 0;
        public double LastYearPercentage
        {
            get
            {
                if (Sales > 0)
                    lastYearPercentage = LastYearProfit * 100 / LastYearSales;
                return lastYearPercentage;
            }
        }



    }
}
