﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using SpreadsheetLight;
using SpreadsheetLight.Drawing;
using System.Data;

namespace Peercore.CRM.Entities
{
    public class BaseExcelCellEntity
    {
         public SLDocument ExcelDocument { get; set; }
         public int RowIndex { get; set; }
         public int ColumnIndex { get; set; }
         public string CellText { get; set; }
         public int MergeStartRowIndex { get; set; }
         public int MergeEndRowIndex { get; set; }
         public int MergeStartColumnIndex { get; set; }
         public int MergeEndColumnIndex { get; set; }
         public bool IsMerged { get; set; }
    }
}
