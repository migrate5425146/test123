﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class LeadEntity : BaseEntity
    {
        private LeadEntity()
        {
            this.IsNew = true;
        }

        private LeadEntity(int entityId)
        {
            this.LeadID = entityId;
        }

        public static LeadEntity CreateObject()
        {
            LeadEntity entity = new LeadEntity();
            return entity;
        }

        public static LeadEntity CreateObject(int entityId)
        {
            LeadEntity entity = new LeadEntity(entityId);
            return entity;
        }

        public string LeadName { get; set; }
        public string Originator { get; set; }
        public string LeadSource { get; set; }
        public string Company { get; set; }
        public string Business { get; set; }
        public string Website { get; set; }
        public string Industry { get; set; }
        public string LeadStatus { get; set; }
        public int NoOfEmployees { get; set; }
        public double AnnualRevenue { get; set; }
        public int LeadStageID { get; set; }
        public int LeadID { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
        public string ReferredBy { get; set; }
        public double Probability { get; set; }
        public string Description { get; set; }
        public string PreferredContact { get; set; }
        public string BusinessPotential { get; set; }
        public string Country { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Mobile { get; set; }
        public string EmailAddress { get; set; }
        public int Rating { get; set; }
        public string CustCode { get; set; }
        public DateTime? LastCalledDate { get; set; }
        public string LitersBy { get; set; }
        public double PotentialLiters { get; set; }
        public int RepGroupID { get; set; }

        // NOT IN TABLE
        public string RepGroupName { get; set; }

        //public string CreatedBy { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public string LastModifiedBy { get; set; }
        //public DateTime LastModifiedDate { get; set; }
        public string DelFalg { get; set; }
    }
}
