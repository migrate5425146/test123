﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class LeadStageGraphDataEntity : BaseEntity
    {
        private LeadStageGraphDataEntity()
        {
            this.IsNew = true;
        }

        private LeadStageGraphDataEntity(int entityId)
        {
            this.LeadStageId = entityId;
        }

        public static LeadStageGraphDataEntity CreateObject()
        {
            LeadStageGraphDataEntity entity = new LeadStageGraphDataEntity();
            return entity;
        }

        public static LeadStageGraphDataEntity CreateObject(int entityId)
        {
            LeadStageGraphDataEntity entity = new LeadStageGraphDataEntity(entityId);
            return entity;
        }
        public int LeadStageId { get; set; }
        public string LeadStage { get; set; }
        public int LeadsCount { get; set; }
    }
}
