﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ChecklistRepEntity : BaseEntity
    {
        public int ChecklistId { get; set; }
        public string RepCode { get; set; }
    }
}
