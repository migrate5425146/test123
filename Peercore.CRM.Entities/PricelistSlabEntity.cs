﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class PricelistSlabEntity : BaseEntity
    {
        private PricelistSlabEntity()
        {
            this.IsNew = true;
        }

        private PricelistSlabEntity(int entityId)
        {
            this.PricelistSlabId = entityId;
        }

        public static PricelistSlabEntity CreateObject()
        {
            PricelistSlabEntity entity = new PricelistSlabEntity();
            return entity;
        }

        public static PricelistSlabEntity CreateObject(int entityId)
        {
            PricelistSlabEntity entity = new PricelistSlabEntity(entityId);
            return entity;
        }

        private int? pricelistSlabId;
        private int? pricelistDetailId;
        private double slabFrom;
        private double slabTo;
        private double price;
        private double cost;

        public int? PricelistSlabId 
        {
            get { return pricelistSlabId; }
            set
            {
                pricelistSlabId = value;
                IsDirty = true;
            }
        }
        public int? PricelistDetailId 
        {
            get { return pricelistDetailId; }
            set
            {
                pricelistDetailId = value;
                IsDirty = true;
            }
        }
        public double SlabFrom 
        {
            get { return slabFrom; }
            set
            {
                slabFrom = value;
                IsDirty = true;
            }
        }
        public double SlabTo 
        {
            get { return slabTo; }
            set
            {
                slabTo = value;
                IsDirty = true;
            }
        }
        public double Price 
        {
            get { return price; }
            set
            {
                price = value;
                IsDirty = true;
            }
        }
        public double Cost 
        {
            get { return cost; }
            set
            {
                cost = value;
                IsDirty = true;
            }
        }

    }
}
