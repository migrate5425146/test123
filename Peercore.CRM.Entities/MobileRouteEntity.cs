﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class MobileRouteEntity
    {
        private MobileRouteEntity()
        {
            
        }

        public static MobileRouteEntity CreateObject()
        {
            MobileRouteEntity entity = new MobileRouteEntity();
            return entity;
        }

         public string TMECode { get; set; }
         public int RouteMasterId { get; set; }
         public string RouteName { get; set; }
         public int RouteSequence { get; set; }
         public string CustomerCode { get; set; }
         public string CustomerName { get; set; }
         public DateTime LastVisitedDate { get; set; }
         public int TotalOutlets { get; set; }
         public int VisitedOutlet { get; set; }
       //  public List<SalesForceCustomerEntity> customerList { get; set; }
    }
}
