﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class QuotationBranchSummaryEntity : BaseEntity
    {
        private QuotationBranchSummaryEntity()
        {
            this.IsNew = true;
        }

        private QuotationBranchSummaryEntity(int entityId)
        {
            this.QuotationBranchSummaryId = entityId;
        }

        public static QuotationBranchSummaryEntity CreateObject()
        {
            QuotationBranchSummaryEntity entity = new QuotationBranchSummaryEntity();
            return entity;
        }

        public static QuotationBranchSummaryEntity CreateObject(int entityId)
        {
            QuotationBranchSummaryEntity entity = new QuotationBranchSummaryEntity(entityId);
            return entity;
        }
        private int? quotationBranchsummaryId;
        private int? quotationId;
        private string areaFrom;
        private string areaTo;
        private decimal indirectCost;
        private decimal directCost;
        private decimal cost;

        public decimal Cost
        {
            get { return cost; }
            set
            {
                cost = value;
                IsDirty = true;
            }
        }

        public decimal DirectCost
        {
            get { return directCost; }
            set
            {
                directCost = value;
                IsDirty = true;
            }
        }

        public decimal IndirectCost
        {
            get { return indirectCost; }
            set
            {
                indirectCost = value;
                IsDirty = true;
            }
        }

        public string AreaTo
        {
            get { return areaTo; }
            set
            {
                areaTo = value;
                IsDirty = true;
            }
        }

        public string AreaFrom
        {
            get { return areaFrom; }
            set
            {
                areaFrom = value;
                IsDirty = true;
            }
        }

        public int? QuotationId
        {
            get { return quotationId; }
            set
            {
                quotationId = value;
                IsDirty = true;
            }
        }

        public int? QuotationBranchSummaryId
        {
            get { return quotationBranchsummaryId; }
            set
            {
                quotationBranchsummaryId = value;
                IsDirty = true;
            }
        }

    }
}
