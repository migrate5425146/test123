﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class VisitNoteEntity:BaseEntity
    {
        public VisitNoteEntity()
        {
            this.IsNew = true;
        }

        public VisitNoteEntity(int entityId)
        {
            this.VisitNoteId = entityId;
        }

        public static VisitNoteEntity CreateObject()
        {
            VisitNoteEntity entity = new VisitNoteEntity();
            return entity;
        }

        public static VisitNoteEntity CreateObject(int entityId)
        {
            VisitNoteEntity entity = new VisitNoteEntity(entityId);
            return entity;
        }
        
        public int VisitNoteId { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string CustomerAddress { get; set; }
        public int RouteMasterId { get; set; }
        public bool IsCheckNote { get; set; }
        public string Remarks { get; set; }
        public string Notes { get; set; }
        public string Status { get; set; }
        public int ActivityId { get; set; }
        public string VisitDate { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public string RepCode { get; set; }
    }
}
