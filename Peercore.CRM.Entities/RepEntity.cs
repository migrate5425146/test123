﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class RepEntity : BaseEntity
    {
        public RepEntity()
        {            
        }

        public static RepEntity CreateObject()
        {
            RepEntity entity = new RepEntity();
            return entity;
        }
        public string RepCode { get; set; }
        public string Name { get; set; }
        public string UserName { get; set; }
        public string SupervisorCode { get; set; }
        public string State { get; set; }
        public int Include { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Mobile { get; set; }
        public string Status { get; set; }
        public string AbnNo { get; set; }
        public int GstStatus { get; set; }
        public string CompanyName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public string PayMethod { get; set; }
        public string BrowseAvailable { get; set; }
        public string Originator { get; set; }
        public int DispInCrm { get; set; }
        public string RepType { get; set; }
        public int BusManagerFlag { get; set; }
        public int Version { get; set; }
        public double StickTarget { get; set; }
        public int TargetYear { get; set; }
        public int TargetMonth { get; set; }
        public string TargetMonthText { get; set; }
    }

    public class RepGeoLocation
    {
        public DateTime DateTime { get; set; }
        public string Originator { get; set; }
        public string CurrLat { get; set; }
        public string CurrLong { get; set; }
    }
}
