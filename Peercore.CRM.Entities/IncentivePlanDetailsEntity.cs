﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class IncentivePlanDetailsEntity:BaseEntity
    {
        public IncentivePlanDetailsEntity()
        {
            this.IsNew = true;
        }

        public IncentivePlanDetailsEntity(int entityId)
        {
            this.IncentivePlanDetailsId = entityId;
        }

        public static IncentivePlanDetailsEntity CreateObject()
        {
            IncentivePlanDetailsEntity entity = new IncentivePlanDetailsEntity();
            return entity;
        }

        public static IncentivePlanDetailsEntity CreateObject(int entityId)
        {
            IncentivePlanDetailsEntity entity = new IncentivePlanDetailsEntity(entityId);
            return entity;
        }

        public int IncentivePlanDetailsId { get; set; }
        public int IncentivePlanHeaderId { get; set; }
        public string PlanDetails { get; set; }
        public int IpDetailsTypeId { get; set; }
        public bool IsActive { get; set; }

        public List<IncentiveOptionLevelEntity> IncentiveOptionLevelList { get; set; }
        public List<IncentiveSecondarySalesWeeklyEntity> IncentiveSecondarySalesWeeklyList { get; set; }
        public List<IncentiveSecondarySalesPartialEntity> IncentiveSecondarySalesPartialList { get; set; }
    }
}
