﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class KeyAccountEntity : BaseEntity
    {
        private KeyAccountEntity()
        {
            this.IsNew = true;
        }

        private KeyAccountEntity(int entityId)
        {
            //this.OpportunityID = entityId;
        }

        public static KeyAccountEntity CreateObject()
        {
            KeyAccountEntity entity = new KeyAccountEntity();
            return entity;
        }

        public static KeyAccountEntity CreateObject(int entityId)
        {
            KeyAccountEntity entity = new KeyAccountEntity(entityId);
            return entity;
        }

        public string AccountName { get; set; }

        public double Amount { get; set; }
    }
}
