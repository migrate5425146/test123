﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class MobileVolumePerformanceEntity
    {
        public MobileVolumePerformanceEntity()
        {
        }



        public static MobileVolumePerformanceEntity CreateObject()
        {
            MobileVolumePerformanceEntity entity = new MobileVolumePerformanceEntity();
            return entity;
        }


        public string MarketName { get; set; }
        public int MarketId { get; set; }
        public string BrandCode { get; set; }

        public int Target { get; set; }
        public int Actual { get; set; }

        public int Outlets { get; set; }
        public double AchievementPerc { get; set; }
        public int TargetAchivers { get; set; }
        public int Investment { get; set; }
        public double CostPerMille { get; set;}
        public int tier { get; set; }

        public double ReqiredStrikeRate{get; set;}
        public double CurrentStrikeRate { get; set; }
    }
}
