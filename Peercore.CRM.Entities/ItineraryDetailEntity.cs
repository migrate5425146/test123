﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ItineraryDetailEntity : BaseEntity
    {
        private ItineraryDetailEntity()
        {
            this.IsNew = true;
        }

        private ItineraryDetailEntity(int entityId)
        {
            this.ItineraryDetailId = entityId;
        }

        public static ItineraryDetailEntity CreateObject()
        {
            ItineraryDetailEntity entity = new ItineraryDetailEntity();
            return entity;
        }

        public static ItineraryDetailEntity CreateObject(int ItineraryId)
        {
            ItineraryDetailEntity entity = new ItineraryDetailEntity(ItineraryId);
            return entity;
        }

        public int ItineraryDetailId { get; set; }
        public int ItineraryHeaderId { get; set; }
        public DateTime Date { get; set; }
        public string TownsToVisit { get; set; }
        public int Sequence { get; set; }
        public int DirectMiles { get; set; }
        public int LocalMiles { get; set; }
        public int TotalMiles { get; set; }
        public string WorkProgram { get; set; }
        public string NightAt { get; set; }
        public int ActivityId { get; set; }
        public int RouteId { get; set; }
        public string SequenceRoute { get; set; }
        public string DayTypeColor { get; set; }

    }
}
