﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class InvoiceDetailEntity : BaseEntity
    {
        public InvoiceDetailEntity()
        {
        }

        public static InvoiceDetailEntity CreateObject()
        {
            InvoiceDetailEntity entity = new InvoiceDetailEntity();
            return entity;
        }

        public int InvoiceDetailId { get; set; }
        public int InvoiceId { get; set; }
        public int ProductId { get; set; }
        public double Quantity { get; set; }
        public double Discount { get; set; }
        public double DiscountValue { get; set; }
        public double SubTotal { get; set; }
        public string Status { get; set; }
        public double ItemPrice { get; set; }
        public string ProductName { get; set; }

        public double QtyCasess { get; set; } // added by rangan

        public double QtyTonnage { get; set; } // added by rangan

    }
}
