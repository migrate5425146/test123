﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class QuotationDetailEntity : BaseEntity
    {
        private QuotationDetailEntity()
        {
            this.IsNew = true;
        }

        private QuotationDetailEntity(int entityId)
        {
            this.QuotationID = entityId;
        }

        public static QuotationDetailEntity CreateObject()
        {
            QuotationDetailEntity entity = new QuotationDetailEntity();
            return entity;
        }

        public static QuotationDetailEntity CreateObject(int entityId)
        {
            QuotationDetailEntity entity = new QuotationDetailEntity(entityId);
            return entity;
        }

        public int QuotationID { get; set; }
        public string CatalogCode { get; set; }
        public string UOM { get; set; }
        public int Units { get; set; }
        public string Commodity { get; set; }
        public double Length { get; set; }
        public double Width { get; set; }
        public double Height { get; set; }
        public double CubicMeters { get; set; }
        public double Weight { get; set; }
        public string Hazardous { get; set; }
        public double Price { get; set; }

        // Properties which are not in the table
        public string Description { get; set; }
        public double Amount { get; set; }
    }
}
