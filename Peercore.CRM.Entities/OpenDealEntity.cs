﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class OpenDealEntity : BaseEntity
    {
        private OpenDealEntity()
        {
            this.IsNew = true;
        }

        private OpenDealEntity(int entityId)
        {
            //this.OpportunityID = entityId;
        }

        public static OpenDealEntity CreateObject()
        {
            OpenDealEntity entity = new OpenDealEntity();
            return entity;
        }

        public static OpenDealEntity CreateObject(int entityId)
        {
            OpenDealEntity entity = new OpenDealEntity(entityId);
            return entity;
        }
        public string OpportunityName { get; set; }

        public double Amount { get; set; }

        public string Rep { get; set; }

        public string Customer { get; set; }
    }
}
