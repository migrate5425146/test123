﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class OpportunityViewEntity : BaseEntity
    {
        private OpportunityViewEntity()
        {
            this.IsNew = true;
        }

        private OpportunityViewEntity(int entityId)
        {
            this.OpportunityID = entityId;
        }

        public static OpportunityViewEntity CreateObject()
        {
            OpportunityViewEntity entity = new OpportunityViewEntity();
            return entity;
        }

        public static OpportunityViewEntity CreateObject(int entityId)
        {
            OpportunityViewEntity entity = new OpportunityViewEntity(entityId);
            return entity;
        }

        public int OpportunityID { get; set; }
        public int LeadID { get; set; }
        public string Originator { get; set; }
        public string Name { get; set; }
        public DateTime CloseDate { get; set; }
        public int Stage { get; set; }
        public double Probability { get; set; }
        public double Amount { get; set; }
        public double Units { get; set; }
        public string Description { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public string CreatedBy { get; set; }
        //public DateTime LastModifiedDate { get; set; }
        //public string LastModifiedBy { get; set; }
        public string CustCode { get; set; }
        public string PipelineStage { get; set; }
        public int Count { get; set; }
    }
}
