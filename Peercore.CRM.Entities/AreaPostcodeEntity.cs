﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class AreaPostcodeEntity : BaseEntity
    {
        public AreaPostcodeEntity()
        {
            this.IsNew = true;
        }

        public AreaPostcodeEntity(int entityId)
        {
            this.AreaPostcodeID = entityId;
        }

        public static AreaPostcodeEntity CreateObject()
        {
            AreaPostcodeEntity entity = new AreaPostcodeEntity();

            return entity;
        }

        public static AreaPostcodeEntity CreateObject(int entityId)
        {
            AreaPostcodeEntity entity = new AreaPostcodeEntity(entityId);

            return entity;
        }

        private int? areaPostcodeID;
        public int? AreaPostcodeID
        {
            get { return areaPostcodeID; }
            set { areaPostcodeID = value; }
        }

        private int? areaID;
        public int? AreaID
        {
            get { return areaID; }
            set { areaID = value; }
        }

        private string suburbName;
        public string SuburbName
        {
            get { return suburbName; }
            set
            {
                suburbName = value;
                IsDirty = true;
            }
        }

        private string postcodeName;
        public string PostcodeName
        {
            get { return postcodeName; }
            set
            {
                postcodeName = value;
                IsDirty = true;
            }
        }
    }
}
