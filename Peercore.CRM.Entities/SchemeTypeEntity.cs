﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class SchemeTypeEntity : BaseEntity
    {
        public SchemeTypeEntity()
        {
        }

        public SchemeTypeEntity(int entityId)
        {
            this.SchemeTypeId = entityId;
        }

        public static SchemeTypeEntity CreateObject()
        {
            SchemeTypeEntity entity = new SchemeTypeEntity();
            return entity;
        }

        public static SchemeTypeEntity CreateObject(int entityId)
        {
            SchemeTypeEntity entity = new SchemeTypeEntity(entityId);
            return entity;
        }

        public int SchemeTypeId { get; set; }
        public string SchemeType { get; set; }
        public string Description { get; set; }
    }
}
