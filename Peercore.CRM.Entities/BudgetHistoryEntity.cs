﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class BudgetHistoryEntity : BaseEntity
    {
        private BudgetHistoryEntity()
        {
            this.IsNew = true;
        }


        public static BudgetHistoryEntity CreateObject()
        {
            BudgetHistoryEntity entity = new BudgetHistoryEntity();
            return entity;
        }


        public string State { get; set; }
        public string CustCode { get; set; }
        public string CatlogCode { get; set; }
        public int FinYear { get; set; }
        public double BudgetVolP1 { get; set; }
        public double BudgetVolP2 { get; set; }
        public double BudgetVolP3 { get; set; }
        public double BudgetVolP4 { get; set; }
        public double BudgetVolP5 { get; set; }
        public double BudgetVolP6 { get; set; }
        public double BudgetVolP7 { get; set; }
        public double BudgetVolP8 { get; set; }
        public double BudgetVolP9 { get; set; }
        public double BudgetVolP10 { get; set; }
        public double BudgetVolP11 { get; set; }
        public double BudgetVolP12 { get; set; }
        public double BudgetVolP13 { get; set; }
        public double BudgetValP1 { get; set; }
        public double BudgetValP2 { get; set; }
        public double BudgetValP3 { get; set; }
        public double BudgetValP4 { get; set; }
        public double BudgetValP5 { get; set; }
        public double BudgetValP6 { get; set; }
        public double BudgetValP7 { get; set; }
        public double BudgetValP8 { get; set; }
        public double BudgetValP9 { get; set; }
        public double BudgetValP10 { get; set; }
        public double BudgetValP11 { get; set; }
        public double BudgetValP12 { get; set; }
        public double BudgetValP13 { get; set; }
        public double BudgetVolPp1 { get; set; }
        public double BudgetVolPp2 { get; set; }
        public double BudgetVolPp3 { get; set; }
        public double BudgetVolPp4 { get; set; }
        public double BudgetVolPp5 { get; set; }
        public double BudgetVolPp6 { get; set; }
        public double BudgetVolPp7 { get; set; }
        public double BudgetVolPp8 { get; set; }
        public double BudgetVolPp9 { get; set; }
        public double BudgetVolPp10 { get; set; }
        public double BudgetVolPp11 { get; set; }
        public double BudgetVolPp12 { get; set; }
        public double BudgetVolPp13 { get; set; }
        public double BudgetValPp1 { get; set; }
        public double BudgetValPp2 { get; set; }
        public double BudgetValPp3 { get; set; }
        public double BudgetValPp4 { get; set; }
        public double BudgetValPp5 { get; set; }
        public double BudgetValPp6 { get; set; }
        public double BudgetValPp7 { get; set; }
        public double BudgetValPp8 { get; set; }
        public double BudgetValPp9 { get; set; }
        public double BudgetValPp10 { get; set; }
        public double BudgetValPp11 { get; set; }
        public double BudgetValPp12 { get; set; }
        public double BudgetValPp13 { get; set; }
        public double BudgetVolYr1 { get; set; }
        public double BudgetVolYr2 { get; set; }
        public double BudgetVolYr3 { get; set; }
        public double BudgetVolYr4 { get; set; }
        public double BudgetValYr1 { get; set; }
        public double BudgetValYr2 { get; set; }
        public double BudgetValYr3 { get; set; }
        public double BudgetValYr4 { get; set; }

    }
}
