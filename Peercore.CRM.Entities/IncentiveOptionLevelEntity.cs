﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class IncentiveOptionLevelEntity:BaseEntity
    {
        public IncentiveOptionLevelEntity()
        {
            this.IsNew = true;
        }

        public IncentiveOptionLevelEntity(int entityId)
        {
            this.IpOptionLevelId = entityId;
        }

        public static IncentiveOptionLevelEntity CreateObject()
        {
            IncentiveOptionLevelEntity entity = new IncentiveOptionLevelEntity();

            return entity;
        }

        public static IncentiveOptionLevelEntity CreateObject(int entityId)
        {
            IncentiveOptionLevelEntity entity = new IncentiveOptionLevelEntity(entityId);
            return entity;
        }

        public int IpOptionLevelId { get; set; }
        public int IncentivePlanDetailsId { get; set; }
        public double MinQuantity { get; set; }
        public double MinValue { get; set; }
        public double MaxQuantity { get; set; }
        public double MaxValue { get; set; }
        public double IncentiveValue { get; set; }
        public double IncentivePercentage { get; set; }
        public bool IsMonthlyAllowanceIncluded { get; set; }
        public bool IsActive { get; set; }

        public List<IncentiveOptionLevelCombinationEntity> IncentiveOptionLevelCombinationList { get; set; }
        public List<IncentiveDiscountSchemeEntity> IncentiveDiscountSchemeList { get; set; }
    }
}
