﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ChecklistTypeEntity :BaseEntity
    {
        private ChecklistTypeEntity()
        {
            this.IsNew = true;
        }

        private ChecklistTypeEntity(int entityId)
        {
            this.Id = entityId;
        }

        public static ChecklistTypeEntity CreateObject()
        {
            ChecklistTypeEntity entity = new ChecklistTypeEntity();
            return entity;
        }

        public static ChecklistTypeEntity CreateObject(int entityId)
        {
            ChecklistTypeEntity entity = new ChecklistTypeEntity(entityId);
            return entity;
        }

        public int Id { get; set; }
        public string Name { get; set; }
    }
}
