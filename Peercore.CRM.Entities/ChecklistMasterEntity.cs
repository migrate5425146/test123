﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ChecklistMasterEntity :BaseEntity
    {
        private ChecklistMasterEntity()
        {
            this.IsNew = true;
        }

        private ChecklistMasterEntity(int entityId)
        {
            this.ChecklistId = entityId;
        }

        public static ChecklistMasterEntity CreateObject()
        {
            ChecklistMasterEntity entity = new ChecklistMasterEntity();
            return entity;
        }

        public static ChecklistMasterEntity CreateObject(int entityId)
        {
            ChecklistMasterEntity entity = new ChecklistMasterEntity(entityId);
            return entity;
        }
        public int ChecklistId { get; set; }
        public int ChecklistTypeId { get; set; }
        public string ChecklistType { get; set; }
        public string Task { get; set; }
        public int IsHidden { get; set; }
        public string CustomerCode { get; set; }

        public List<ChecklistCustomerEntity> CustomerList { get; set; }
        public List<ChecklistRepEntity> RepList { get; set; }
        public List<ChecklistRouteEntity> RouteList { get; set; }
        public List<ChecklistImageEntity> ImageList { get; set; }

       // public List<ChecklistImageEntity> ImageList { get; set; }
    }
}
