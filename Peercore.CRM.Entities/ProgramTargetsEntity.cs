﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ProgramTargetsEntity:BaseEntity
    {
        private ProgramTargetsEntity()
        {
            
        }
        public ProgramTargetsEntity(int entityId)
        {
            this.ProgramTargetId = entityId;
        }

        public static ProgramTargetsEntity CreateObject()
        {
            ProgramTargetsEntity entity = new ProgramTargetsEntity();

            return entity;
        }

        public static ProgramTargetsEntity CreateObject(int entityId)
        {
            ProgramTargetsEntity entity = new ProgramTargetsEntity(entityId);

            return entity;
        }

        public int ProgramTargetId { get; set; }
        public ProgramEntity Program { get; set; }

        public List<CustomerEntity> customerList { get; set; }

        public string Status { get; set; }

        public double? TargetPercentage { get; set; }
        public int? TargetSticks { get; set; }
        public double? TargetIncentiveAmount { get; set; }
        public int? TargetIncentivePreIssueProductId { get; set; }
        public string TargetIncentivePreIssueProductName { get; set; }
        public int? TargetIncentivePreIssueProductSku { get; set; }
        public int? TargetIncentivePreIssueAmountSticks { get; set; }

        public double? BonusPercentage { get; set; }
        public int? BonusSticks { get; set; }
        public double? BonusIncentiveAmount { get; set; }
        public int? BonusIncentivePreIssueProductId { get; set; }
        public string BonusIncentivePreIssueProductName { get; set; }
        public int? BonusIncentivePreIssueProductSku { get; set; }
        public int? BonusIncentivePreIssueAmountSticks { get; set; }

        public DateTime EffStartDate { get; set; }
        public DateTime EffEndDate { get; set; }
    }
}
