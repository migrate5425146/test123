﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class AreaListEntity :BaseEntity
    {
        private AreaListEntity()
        {
            this.IsNew = true;
        }

        private AreaListEntity(int entityId)
        {
            //this.AreaID = entityId;
        }

        public static AreaListEntity CreateObject()
        {
            AreaListEntity entity = new AreaListEntity();
            return entity;
        }

        public static AreaListEntity CreateObject(int entityId)
        {
            AreaListEntity entity = new AreaListEntity(entityId);
            return entity;
        }

        public string Area { get; set; }
        public string City { get; set; }
        public string Suburb { get; set; }
        public string PostCode { get; set; }
    }
}
