﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class BankAccountEntity : BaseEntity
    {
        public BankAccountEntity()
        {
            this.IsNew = true;
        }

        public BankAccountEntity(int entityId)
        {
            this.BankAccountId = entityId;
        }

        public static BankAccountEntity CreateObject()
        {
            BankAccountEntity entity = new BankAccountEntity();
            return entity;
        }

        public static BankAccountEntity CreateObject(int entityId)
        {
            BankAccountEntity entity = new BankAccountEntity(entityId);
            return entity;
        }

        public int BankAccountId { get; set; }
        public int SourceId { get; set; }
        public int SourceType { get; set; }
        public string AccountNumber { get; set; }
        public string Bank { get; set; }
        public string Branch { get; set; }
        public string SourceTypeText { get; set; }

        public bool isDefault { get; set; }
        public string Status { get; set; }

        //public string CreatedBy { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public string LastModifiedBy { get; set; }
        //public DateTime LastModifiedDate { get; set; }
    }
}
