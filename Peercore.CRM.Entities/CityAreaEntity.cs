﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class CityAreaEntity : BaseEntity
    {
        private CityAreaEntity()
        {
            this.IsNew = true;
        }

        private CityAreaEntity(int entityId)
        {
            this.AreaID = entityId;
        }

        public static CityAreaEntity CreateObject()
        {
            CityAreaEntity entity = new CityAreaEntity();
            return entity;
        }

        public static CityAreaEntity CreateObject(int entityId)
        {
            CityAreaEntity entity = new CityAreaEntity(entityId);
            return entity;
        }

        private int? areaID;
        public int? AreaID
        {
            get { return areaID; }
            set { areaID = value; }
        }

        private int? cityID;
        public int? CityID
        {
            get { return cityID; }
            set { cityID = value; }
        }

        private string areaName;
        public string AreaName
        {
            get { return areaName; }
            set
            {
                areaName = value;
                IsDirty = true;
            }
        }

        public string AreaCode { get; set; }

        private List<AreaPostcodeEntity> areaPostcodeList;
        public List<AreaPostcodeEntity> AreaPostcodeList
        {
            get
            {
                if (areaPostcodeList == null)
                    areaPostcodeList = new List<AreaPostcodeEntity>();
                return areaPostcodeList;
            }
            set
            {
                areaPostcodeList = value;
            }
        }

        private int startIndex;
        public int StartIndex
        {
            get { return startIndex; }
            set { startIndex = value; }
        }

        private int rowCount;
        public int RowCount
        {
            get { return rowCount; }
            set { rowCount = value; }
        }

        private int showCount;
        public int ShowCount
        {
            get { return showCount; }
            set { showCount = value; }
        }

    }
}
