﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class BrandEntity:BaseEntity
    {
        private BrandEntity()
        {
            
        }
        public BrandEntity(int entityId)
        {
            this.BrandId = entityId;
        }

        public static BrandEntity CreateObject()
        {
            BrandEntity entity = new BrandEntity();

            return entity;
        }

        public static BrandEntity CreateObject(int entityId)
        {
            BrandEntity entity = new BrandEntity(entityId);

            return entity;
        }


        public int BrandId { get; set; }
        public string BrandCode { get; set; }
        public string BrandName { get; set; }
        public string Status { get; set; }
        public byte[] ImageContent { get; set; }

    }
}
