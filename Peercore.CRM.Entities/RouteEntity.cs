﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class RouteEntity : BaseEntity
    {
        private RouteEntity()
        {
            
        }

        public static RouteEntity CreateObject()
        {
            RouteEntity entity = new RouteEntity();

            return entity;
        }

        public int RouteId { get; set; }
        public int RouteMasterId { get; set; }
        public string RouteName { get; set; }
        public string RepCode { get; set; }
        public string RepName { get; set; }
        public bool IsTempRep { get; set; }
        public int routeSequence { get; set; }
        public DateTime TempRepAssignDate { get; set; }
        public int Created { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public string CreatedBy { get; set; }
        //public DateTime LastModifiedDate { get; set; }
        //public string LastModifiedBy { get; set; }
        public string OriginatorType { get; set; }
        public string TempRepName { get; set; }
        public string TMECode { get; set; }
        public bool IsRouteAssignChanged { get; set; }

    }
}
