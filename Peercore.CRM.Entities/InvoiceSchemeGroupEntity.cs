﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class InvoiceSchemeGroupEntity : BaseEntity
    {
        public InvoiceSchemeGroupEntity()
        {            
        }

        public static InvoiceSchemeGroupEntity CreateObject()
        {
            InvoiceSchemeGroupEntity entity = new InvoiceSchemeGroupEntity();
            return entity;
        }

        public int SchemeGroupId { get; set; }
        public int InvoiceId { get; set; }
        public int SchemeDetailsId { get; set; }
        public double DiscountValue { get; set; }

        public string SchemeDetailName { get; set; }

        public List<InvoiceSchemeEntity> InvoiceSchemeList { get; set; }
    }
}
