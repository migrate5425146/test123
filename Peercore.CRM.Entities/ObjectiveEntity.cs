﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ObjectiveEntity : BaseEntity
    {
        public ObjectiveEntity()
        {
            this.IsNew = true;
        }

        public ObjectiveEntity(int entityId)
        {
            this.ObjectiveId = entityId;
        }

        public static ObjectiveEntity CreateObject()
        {
            ObjectiveEntity entity = new ObjectiveEntity();
            return entity;
        }

        public static ObjectiveEntity CreateObject(int entityId)
        {
            ObjectiveEntity entity = new ObjectiveEntity(entityId);
            return entity;
        }

        public int ObjectiveId { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }
        public string Status { get; set; }

        //public string CreatedBy { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public string LastModifiedBy { get; set; }
        //public DateTime LastModifiedDate { get; set; }

        public string IsAchieved { get; set; }
        public string CustomerCode { get; set; }

        public string Type { get; set; }

    }
}
