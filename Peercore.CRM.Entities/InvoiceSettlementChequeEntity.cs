﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class InvoiceSettlementChequeEntity : BaseEntity
    {
        public InvoiceSettlementChequeEntity()
        {
        }

        public InvoiceSettlementChequeEntity(int entityId)
        {
        }

        public static InvoiceSettlementChequeEntity CreateObject()
        {
            InvoiceSettlementChequeEntity entity = new InvoiceSettlementChequeEntity();

            return entity;
        }

        public static InvoiceSettlementChequeEntity CreateObject(int entityId)
        {
            InvoiceSettlementChequeEntity entity = new InvoiceSettlementChequeEntity(entityId);

            return entity;
        }

        public int InvoiceSettlementChequeId { get; set; }
        public int InvoiceSettlementId { get; set; }
        public double ChequeAmount { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeBank { get; set; }
        public string ChequeDate { get; set; }
    }
}
