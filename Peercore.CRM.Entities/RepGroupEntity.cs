﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class RepGroupEntity : BaseEntity
    {
        public RepGroupEntity()
        {
            this.IsNew = true;
        }

        public RepGroupEntity(int entityId)
        {
        }

        public static RepGroupEntity CreateObject()
        {
            RepGroupEntity entity = new RepGroupEntity();

            return entity;
        }

        public static RepGroupEntity CreateObject(int entityId)
        {
            RepGroupEntity entity = new RepGroupEntity(entityId);

            return entity;
        }

        private int? repGroupId;

        public int? RepGroupId
        {
            get { return repGroupId; }
            set { repGroupId = value; }
        }

        private string repGroupName;

        public string RepGroupName
        {
            get { return repGroupName; }
            set
            {
                repGroupName = value;
                IsDirty = true;
            }
        }


        private List<GroupRepEntity> repList;
        public List<GroupRepEntity> RepList
        {
            get
            {
                if (repList == null)
                    repList = new List<GroupRepEntity>();
                return repList;
            }
            set
            {
                repList = value;
            }
        }

    }
}
