﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    //public enum SourceTypes:int
    //{
    //    Lead=1,
    //    Customer=2
    //}

    [Serializable]
    public class BaseEntity
    {
        //private SourceTypes sourceTypes;
        //public SourceTypes SourceTypes
        //{
        //    get
        //    {
        //        return sourceTypes;
        //    }
        //    set
        //    {
        //        sourceTypes = value;  
        //    }
        //}

        private bool isDirty;
        public bool IsDirty
        {
            get { return isDirty; }
            set
            {
                isDirty = value;
            }
        }

        private bool isNew;
        public bool IsNew
        {
            get { return isNew; }
            set { isNew = value; }
        }

        private int totalCount;
        public int TotalCount
        {
            get { return totalCount; }
            set { totalCount = value; }
        }

        private string createdBy;
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        private DateTime? createdDate;
        public DateTime? CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        private string lastmodifiedBy;
        public string LastModifiedBy
        {
            get { return lastmodifiedBy; }
            set { lastmodifiedBy = value; }
        }

        private DateTime? lastmodifiedDate;
        public DateTime? LastModifiedDate
        {
            get { return lastmodifiedDate; }
            set { lastmodifiedDate = value; }
        }

        public string PrimaryDist { get; set; }
    }
}
