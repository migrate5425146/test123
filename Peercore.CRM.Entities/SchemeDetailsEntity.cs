﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class SchemeDetailsEntity : BaseEntity
    {
        public SchemeDetailsEntity()
        {
        }

        public SchemeDetailsEntity(int entityId)
        {
            this.SchemeDetailsId = entityId;
        }

        public static SchemeDetailsEntity CreateObject()
        {
            SchemeDetailsEntity entity = new SchemeDetailsEntity();

            return entity;
        }

        public static SchemeDetailsEntity CreateObject(int entityId)
        {
            SchemeDetailsEntity entity = new SchemeDetailsEntity(entityId);
            return entity;
        }

        public int SchemeDetailsId { get; set; }
        public int SchemeHeaderId { get; set; }

        public string SchemeHeaderName { get; set; }
        public string DetailsTypeName { get; set; }
        public string SchemeTypeName { get; set; }

        public int DetailsTypeId { get; set; }
        public int ProductPackingId { get; set; }
        public int SchemeTypeId { get; set; }
        public string SchemeDetails { get; set; }
        public double FreeQty { get; set; }
        public double DiscountValue { get; set; }
        public double DiscountPercentage { get; set; }
        public bool? IsRetail { get; set; }
        public bool IsActive { get; set; }

        public string FreeIssueCondition { get; set; }
        public List<OptionLevelEntity> OptionLevelList { get; set; }
        public List<FreeProductEntity> FreeProductList { get; set; }
    }
}
