﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class AppointmentViewEntity:BaseEntity
    {
         public AppointmentViewEntity()
        {
            this.AppointmentID = 0;
        }

         private AppointmentViewEntity(int entityId)
        {
            this.AppointmentID = entityId;
        }

         public static AppointmentViewEntity CreateObject()
         {
             AppointmentViewEntity entity = new AppointmentViewEntity();
             return entity;
         }

         public static AppointmentViewEntity CreateObject(int entityId)
         {
             AppointmentViewEntity entity = new AppointmentViewEntity(entityId);
             return entity;
         }

         private int _AppointmentID;
         public int AppointmentID
         {
             get { return _AppointmentID; }
             set { _AppointmentID = value; }
         }

         private string _Subject;
         public string Subject
         {
             get { return _Subject; }
             set { _Subject = value; }
         }

         private string _Body;
         public string Body
         {
             get { return _Body; }
             set { _Body = value; }
         }

         private DateTime _StartTime;
         public DateTime StartTime
         {
             get { return _StartTime; }
             set { _StartTime = value; }
         }

         private DateTime _EndTime;
         public DateTime EndTime
         {
             get { return _EndTime; }
             set { _EndTime = value; }
         }

         private int _AllDayEvent;
         public int AllDayEvent
         {
             get { return _AllDayEvent; }
             set { _AllDayEvent = value; }
         }

         private string _Location;
         public string Location
         {
             get { return _Location; }
             set { _Location = value; }
         }

         private string _URL;
         public string URL
         {
             get { return _URL; }
             set { _URL = value; }
         }

         private int _Type;
         public int Type
         {
             get { return _Type; }
             set { _Type = value; }
         }

         private string _RecurrencePattern;
         public string RecurrencePattern
         {
             get { return _RecurrencePattern; }
             set { _RecurrencePattern = value; }
         }

         private string _TimeZoneString;
         public string TimeZoneString
         {
             get { return _TimeZoneString; }
             set { _TimeZoneString = value; }
         }

         private string _Developer;
         public string Developer
         {
             get { return _Developer; }
             set { _Developer = value; }
         }

         private string _ProductLine;
         public string ProductLine
         {
             get { return _ProductLine; }
             set { _ProductLine = value; }
         }

         private string _ExceptionAppointments;
         public string ExceptionAppointments
         {
             get { return _ExceptionAppointments; }
             set { _ExceptionAppointments = value; }
         }

         private string _Importance;
         public string Importance
         {
             get { return _Importance; }
             set { _Importance = value; }
         }

         private string _TimeMaker;
         public string TimeMaker
         {
             get { return _TimeMaker; }
             set { _TimeMaker = value; }
         }

         private string _Category;
         public string Category
         {
             get { return _Category; }
             set { _Category = value; }
         }

         private string _CreatedBy;
         public string CreatedBy
         {
             get { return _CreatedBy; }
             set { _CreatedBy = value; }
         }

         private int _Count;
         public int Count
         {
             get { return _Count; }
             set { _Count = value; }
         }

         private string _Title;
         public string Title
         {
             get { return _Title; }
             set { _Title = value; }
         }

         private string _CategoryDescription;
         public string CategoryDescription
         {
             get { return _CategoryDescription; }
             set { _CategoryDescription = value; }
         }

         private string _LeadName;
         public string LeadName
         {
             get { return _LeadName; }
             set { _LeadName = value; }
         }

         private string _LeadStage;
         public string LeadStage
         {
             get { return _LeadStage; }
             set { _LeadStage = value; }
         }

         private int _ActivityID;
         public int ActivityID
         {
             get { return _ActivityID; }
             set { _ActivityID = value; }
         }

         private int _LeadID;
         public int LeadID
         {
             get { return _LeadID; }
             set { _LeadID = value; }
         }

         private string _CustCode;
         public string CustCode
         {
             get { return _CustCode; }
             set { _CustCode = value; }
         }

         private string _EndUserCode;
         public string EndUserCode
         {
             get { return _EndUserCode; }
             set { _EndUserCode = value; }
         }
    }
}
