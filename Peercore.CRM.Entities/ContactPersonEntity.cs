﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ContactPersonEntity : BaseEntity 
    {
        public ContactPersonEntity()
        {
            this.IsNew = true;
        }

        public ContactPersonEntity(int entityId)
        {
            this.ContactPersonID = entityId;
        }

        public static ContactPersonEntity CreateObject()
        {
            ContactPersonEntity entity = new ContactPersonEntity();
            return entity;
        }

        public static ContactPersonEntity CreateObject(int entityId)
        {
            ContactPersonEntity entity = new ContactPersonEntity(entityId);
            return entity;
        }

        public int ContactPersonID { get; set; }
        public int LeadID { get; set; }
        public string Originator { get; set; }
        public string Title { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Position { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Mobile { get; set; }
        public string EmailAddress { get; set; }
        public string ReportsTo { get; set; }
        public string Likes { get; set; }
        public string DisLikes { get; set; }
        public string MailingAddress { get; set; }
        public string MailingCity { get; set; }
        public string MailingState { get; set; }
        public string MailingPostcode { get; set; }
        public string MailingCountry { get; set; }
        public string OtherAddress { get; set; }
        public string OtherCity { get; set; }
        public string OtherState { get; set; }
        public string OtherPostCode { get; set; }
        public string OtherCountry { get; set; }
        public string Description { get; set; }
        //public string CreatedBy { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public string LastModifiedBy { get; set; }
        //public DateTime LastModifiedDate { get; set; }
        public string LeadName { get; set; }
        public string SpecialInterests { get; set; }
        public string ImagePath { get; set; }
        public string KeyContact { get; set; }

        public bool KeyContactChecked
        {
            get
            {
                if (KeyContact == "Y")
                    return true;
                else
                    return false;
            }
        }

        public string CustCode { get; set; }    // Not In Table
        public string Contact { get; set; }
        public string Origin { get; set; }
        public string Status { get; set; }
        public string Note { get; set; }

        public string CompanyName { get; set; }

        public string CompanyAddress { get; set; }

        public string CompanyTelephone { get; set; }

        public string ContactType { get; set; }
        public string TypeDescription { get; set; }

        public int RowCount { get; set; }


        public string InternetAdd { get; set; }
        public string SiteCode { get; set; }
        public int ContactNo { get; set; }
        public string OtherPostcode { get; set; }
        public string SpecialInterest { get; set; }
        public DateTime DelDate { get; set; }
        public string SourceType { get; set; }
        public string Ethnicity { get; set; }
        public List<string> Language { get; set; }


    }
}
