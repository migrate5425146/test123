﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ASMDashboardEntity : BaseEntity
    {
        public ASMDashboardEntity()
        {
            this.IsNew = true;
        }

        public ASMDashboardEntity(int entityId)
        {

        }

        public static ASMDashboardEntity CreateObject()
        {
            ASMDashboardEntity entity = new ASMDashboardEntity();
            return entity;
        }

        public static ASMDashboardEntity CreateObject(int entityId)
        {
            ASMDashboardEntity entity = new ASMDashboardEntity(entityId);
            return entity;
        }

        public string RepName { get; set; }

        public string RepCode { get; set; }

        public string RouteName { get; set; }

        public int AvailableOutlets { get; set; }

        public int CoveredOutlets { get; set; }

        public int PCCount { get; set; }

        public double SalesValue { get; set; }

        public double GumSales { get; set; }

        public double CandySales { get; set; }

        public double JellySales { get; set; }

        public double HVPSales { get; set; }

        public double Sales1300 { get; set; }

        public int Qty1300 { get; set; }

        public double SalesMLines { get; set; }
        
        public int QtyMLines { get; set; }

        public string FirstBillTime { get; set; }

        public string Echo { get; set; }

        public int VCCount { get; set; }

        public double GrossTotal { get; set; }

        public double DiscountTotal { get; set; }
    }
}
