﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class CostingParameterEntity : BaseEntity
    {
        private CostingParameterEntity()
        {
            this.IsNew = true;
        }

        private CostingParameterEntity(int entityId)
        {
            this.ParameterId = entityId;
        }

        public static CostingParameterEntity CreateObject()
        {
            CostingParameterEntity entity = new CostingParameterEntity();
            return entity;
        }

        public static CostingParameterEntity CreateObject(int entityId)
        {
            CostingParameterEntity entity = new CostingParameterEntity(entityId);
            return entity;
        }

        private int? parameterId;
        private string parameterName;

        public int? ParameterId
        {
            get
            {
                return parameterId;
            }
            set
            {
                parameterId = value;
                IsDirty = true;
            }
        }
        public string ParameterName
        {
            get
            {
                return parameterName;
            }
            set
            {
                parameterName = value;
                IsDirty = true;
            }
        }
    }
}
