﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class LookupTableEntity : BaseEntity
    {
        private LookupTableEntity()
        {
            this.IsNew = true;
        }

        private LookupTableEntity(int entityId)
        {
            //this.ChecklistID = entityId;
        }

        public static LookupTableEntity CreateObject()
        {
            LookupTableEntity entity = new LookupTableEntity();
            return entity;
        }

        public static LookupTableEntity CreateObject(int entityId)
        {
            LookupTableEntity entity = new LookupTableEntity(entityId);
            return entity;
        }

        public string TableID { get; set; }
        public string TableCode { get; set; }
        public string TableDescription { get; set; }
        public string Description { get; set; }
        public string DefaultValue { get; set; }
    }
}
