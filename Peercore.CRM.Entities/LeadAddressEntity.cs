﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Peercore.CRM.Entities
{
    public class LeadAddressEntity : BaseEntity
    {
        private LeadAddressEntity()
        {
            this.IsNew = true;
        }

        private LeadAddressEntity(int entityId)
        {
            this.LeadAddressID = entityId;
        }

        public static LeadAddressEntity CreateObject()
        {
            LeadAddressEntity entity = new LeadAddressEntity();
            return entity;
        }

        public static LeadAddressEntity CreateObject(int entityId)
        {
            LeadAddressEntity entity = new LeadAddressEntity(entityId);
            return entity;
        }

        public int LeadAddressID { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }
        public string Country { get; set; }
        public int LeadID { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }

        public string AssignedTo { get; set; }
        public int AssigneeNo { get; set; }
        public string AddressType { get; set; }
        public string AddressTypeDescription 
        { 
            get
            {
                if (AddressType == "A")//clsGlobal.GetInstance().StreetAddressType
                    return "Street";
                else return "Postal";
            }
        }
        public string Name { get; set; }
        public string AreaCode { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Contact { get; set; }
        public string CustCode { get; set; }


        public string AssignTo { get; set; }
        public string AssigneeCode { get; set; }
        public string AddressCode { get; set; }
        public string Postcode { get; set; }
        public string DestArea { get; set; }
        public string DefWarehouse { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string Notes { get; set; }
        public DateTime ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string Status { get; set; }
        public int Version { get; set; }
    }
}
