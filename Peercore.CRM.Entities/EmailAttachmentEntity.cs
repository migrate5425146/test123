﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class EmailAttachmentEntity:BaseEntity
    {
        public EmailAttachmentEntity()
        {
            
        }

        public EmailAttachmentEntity(int entityId)
        {
            this.AttachmentID = entityId;
        }

        public static EmailAttachmentEntity CreateObject()
        {
            EmailAttachmentEntity entity = new EmailAttachmentEntity();

            return entity;
        }

        public static EmailAttachmentEntity CreateObject(int entityId)
        {
            EmailAttachmentEntity entity = new EmailAttachmentEntity(entityId);

            return entity;
        }

        private int _AttachmentID;
        public int AttachmentID
        {
            get { return _AttachmentID; }
            set { _AttachmentID = value; }
        }

        private int _EmailId;
        public int EmailId
        {
            get { return _EmailId; }
            set { _EmailId = value; }
        }

        private string _Path;
        public string Path
        {
            get { return _Path; }
            set { _Path = value; }
        }

        private string _FileName;
        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; IsDirty = true; }
        }

        private byte[] _FileContent;
        public byte[] FileContent
        {
            get { return _FileContent; }
            set
            {
                _FileContent = value;
                IsDirty = true;
            }
        }
    }
}
