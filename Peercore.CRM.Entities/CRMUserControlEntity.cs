﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class CRMUserControlEntity : BaseEntity
    {
        private CRMUserControlEntity()
        {
            this.IsNew = true;
        }

        private CRMUserControlEntity(int entityId)
        {
            //this.AppointmentID = entityId;
        }

        public static CRMUserControlEntity CreateObject()
        {
            CRMUserControlEntity entity = new CRMUserControlEntity();
            return entity;
        }

        public static CRMUserControlEntity CreateObject(int entityId)
        {
            CRMUserControlEntity entity = new CRMUserControlEntity(entityId);
            return entity;
        }

        private string userControlCode;
        public string UserControlCode
        {
            get { return userControlCode; }
            set
            {
                userControlCode = value;
                IsDirty = true;
            }
        }

        private string userControlName;
        public string UserControlName
        {
            get { return userControlName; }
            set
            {
                userControlName = value;
                IsDirty = true;
            }
        }

        private bool isViewOnly;
        public bool IsViewOnly
        {
            get { return isViewOnly; }
            set
            {
                isViewOnly = value;
                IsDirty = true;
            }
        }
    }
}
