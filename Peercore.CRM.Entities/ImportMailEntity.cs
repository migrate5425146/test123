﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ImportMailEntity : BaseEntity
    {
        private ImportMailEntity()
        {
            this.IsNew = true;
        }

        private ImportMailEntity(int entityId)
        {
            this.ImportMailID = entityId;
        }

        public static ImportMailEntity CreateObject()
        {
            ImportMailEntity entity = new ImportMailEntity();
            return entity;
        }

        public static ImportMailEntity CreateObject(int entityId)
        {
            ImportMailEntity entity = new ImportMailEntity(entityId);
            return entity;
        }

        private int _ImportMailID;
        public int ImportMailID
        {
            get { return _ImportMailID; }
            set { _ImportMailID = value; }
        }

        private string _EntryID;
        public string EntryID
        {
            get { return _EntryID; }
            set { _EntryID = value; }
        }

        private string _To;
        public string To
        {
            get { return _To; }
            set { _To = value; }
        }

        private string _From;
        public string From
        {
            get { return _From; }
            set { _From = value; }
        }

        private string _Subject;
        public string Subject
        {
            get { return _Subject; }
            set { _Subject = value; }
        }

        private string _Content;
        public string Content
        {
            get { return _Content; }
            set { _Content = value; }
        }

        private string _FileName;
        public string FileName
        {
            get { return _FileName; }
            set { _FileName = value; }
        }

        private DateTime _Date;
        public DateTime Date
        {
            get { return _Date; }
            set { _Date = value; }
        }
        
        private object _OutlookObject;
        public object OutlookObject
        {
            get { return _OutlookObject; }
            set { _OutlookObject = value; }
        }

        private int _LeadID;
        public int LeadID
        {
            get { return _LeadID; }
            set { _LeadID = value; }
        }

        private string _CustCode;
        public string CustCode
        {
            get { return _CustCode; }
            set { _CustCode = value; }
        }

        private string _LeadName;
        public string LeadName
        {
            get { return _LeadName; }
            set { _LeadName = value; }
        }

        private string _ContactPersonName;
        public string ContactPersonName
        {
            get { return _ContactPersonName; }
            set { _ContactPersonName = value; }
        }

        private byte[] fileContent;
        public byte[] FileContent
        {
            get { return fileContent; }
            set
            {
                fileContent = value;
            }
        }

        private string endUserCode;
        public string EndUserCode
        {
            get { return endUserCode; }
            set { endUserCode = value; }
        }


        public int RowCount { get; set; }
    }
}
