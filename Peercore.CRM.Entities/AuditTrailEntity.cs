﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class AuditTrail : BaseEntity
    {
        public AuditTrail()
        {
            this.IsNew = true;
        }

        public AuditTrail(int entityId)
        {
           // this.AuditTrail = entityId;
        }

        public static AuditTrail CreateObject()
        {
            AuditTrail entity = new AuditTrail();
            return entity;
        }

        public static AuditTrail CreateObject(int entityId)
        {
            AuditTrail entity = new AuditTrail(entityId);

            return entity;
        }

        public int SessionId { get; set; }
        public string Originator { get; set; }
        public DateTime LoginTime { get; set; }
        public DateTime LogoutTime { get; set; }
        public string ScreenName { get; set; }
        public DateTime ScreenOpenTime { get; set; }
        public DateTime ScreenCloseTime { get; set; }
    }
}
