﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class FrameMonitorEntity:BaseEntity
    {
        public FrameMonitorEntity()
        {
            this.IsNew = true;
        }

        public FrameMonitorEntity(int entityId)
        {
            this.FrameMonitorId = entityId;
        }

        public static FrameMonitorEntity CreateObject()
        {
            FrameMonitorEntity entity = new FrameMonitorEntity();
            return entity;
        }

        public static FrameMonitorEntity CreateObject(int entityId)
        {
            FrameMonitorEntity entity = new FrameMonitorEntity(entityId);
            return entity;
        }
        
        public int FrameMonitorId { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string RepCode { get; set; }
        public string FrameName { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string Status { get; set; }
    }
}
