﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class SessionActivityEntity : BaseEntity
    {
        private SessionActivityEntity()
        {
            this.IsNew = true;
        }

        private SessionActivityEntity(int entityId)
        {
            this.SessionActivityId = entityId;
        }

        public static SessionActivityEntity CreateObject()
        {
            SessionActivityEntity entity = new SessionActivityEntity();
            return entity;
        }

        public static SessionActivityEntity CreateObject(int entityId)
        {
            SessionActivityEntity entity = new SessionActivityEntity(entityId);
            return entity;
        }

        public int? SessionActivityId { get; set; }
        public int SessionId { get; set; }
        public string UserControl { get; set; }
        public DateTime StartDateTime { get; set; }
        public DateTime EndDateTime { get; set; }
    }
}
