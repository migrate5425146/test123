﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
   
    public class SchemeHeaderEntity : BaseEntity
    {
        public SchemeHeaderEntity()
        {
        }

        public SchemeHeaderEntity(int entityId)
        {
            this.SchemeHeaderId = entityId;
        }

        public static SchemeHeaderEntity CreateObject()
        {
            SchemeHeaderEntity entity = new SchemeHeaderEntity();

            return entity;
        }

        public static SchemeHeaderEntity CreateObject(int entityId)
        {
            SchemeHeaderEntity entity = new SchemeHeaderEntity(entityId);

            return entity;
        }

        public int SchemeHeaderId { get; set; }
        public string SchemeName { get; set; }        
        public bool IsActive { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }

        public List<SchemeDetailsEntity> SchemeDetailsList { get; set; }
    }
}
