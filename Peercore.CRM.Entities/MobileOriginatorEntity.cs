﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class MobileOriginatorEntity : BaseEntity
    {
        public MobileOriginatorEntity()
        {
            this.IsNew = true;
        }

        public static MobileOriginatorEntity CreateObject()
        {
            MobileOriginatorEntity entity = new MobileOriginatorEntity();
            return entity;
        }

        public int OriginatorId { get; set; }
        public string OriginatorName { get; set; }
        public int RouteId { get; set; }
        public string RouteName { get; set; }

    }
}
