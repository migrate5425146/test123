﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class AppointmentCountViewEntity : BaseEntity
    {
        private AppointmentCountViewEntity()
        {
            this.IsNew = true;
        }
        
        public static AppointmentCountViewEntity CreateObject()
        {
            AppointmentCountViewEntity entity = new AppointmentCountViewEntity();

            return entity;
        }

        public string Category { get; set; }
        public string Color  { get; set; }
        public string Description  { get; set; }
        public int Week1  { get; set; }
        public int Week2 { get; set; }
        public int Week3 { get; set; }
        public int Week4 { get; set; }
        public int Week5 { get; set; }

        public int Month1 { get; set; }
        public int Month2 { get; set; }
        public int Month3 { get; set; }
    }
}
