﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class InfosetSummaryEntity : BaseEntity
    {
        public InfosetSummaryEntity()
        {            
        }

        public static InfosetSummaryEntity CreateObject()
        {
            InfosetSummaryEntity entity = new InfosetSummaryEntity();
            return entity;
        }

        public string CatlogType { get; set; }
        public string BusArea { get; set; }
        public string Market { get; set; }
        public string Brand { get; set; }
        public string CatlogCode { get; set; }
        public string RepCode { get; set; }
        public string CustCode { get; set; }
        public string ParentCustomer { get; set; }
        public string StateCode { get; set; }
        public string CustName { get; set; }
        public string RepName { get; set; }
        public string CatGroup { get; set; }
        public string CatSubGroup { get; set; }
        public string Description { get; set; }
        public double MtdTonnesP1 { get; set; }
        public double MtdValueP1 { get; set; }
        public double YtdTonnesP1 { get; set; }
        public double YtdValueP1 { get; set; }
        public double MtdTonnesP2 { get; set; }
        public double MtdValueP2 { get; set; }
        public double YtdTonnesP2 { get; set; }
        public double YtdValueP2 { get; set; }
        public double MtdTonnesP3 { get; set; }
        public double MtdValueP3 { get; set; }
        public double YtdTonnesP3 { get; set; }
        public double YtdValueP3 { get; set; }
        public double MtdTonnesP4 { get; set; }
        public double MtdValueP4 { get; set; }
        public double YtdTonnesP4 { get; set; }
        public double YtdValueP4 { get; set; }
        public double MtdTonnesP5 { get; set; }
        public double MtdValueP5 { get; set; }
        public double YtdTonnesP5 { get; set; }
        public double YtdValueP5 { get; set; }
        public double MtdTonnesP6 { get; set; }
        public double MtdValueP6 { get; set; }
        public double YtdTonnesP6 { get; set; }
        public double YtdValueP6 { get; set; }
        public double MtdTonnesP7 { get; set; }
        public double MtdValueP7 { get; set; }
        public double YtdTonnesP7 { get; set; }
        public double YtdValueP7 { get; set; }
        public double MtdTonnesP8 { get; set; }
        public double MtdValueP8 { get; set; }
        public double YtdTonnesP8 { get; set; }
        public double YtdValueP8 { get; set; }
        public double MtdTonnesP9 { get; set; }
        public double MtdValueP9 { get; set; }
        public double YtdTonnesP9 { get; set; }
        public double YtdValueP9 { get; set; }
        public double MtdTonnesP10 { get; set; }
        public double MtdValueP10 { get; set; }
        public double YtdTonnesP10 { get; set; }
        public double YtdValueP10 { get; set; }
        public double MtdTonnesP11 { get; set; }
        public double MtdValueP11 { get; set; }
        public double YtdTonnesP11 { get; set; }
        public double YtdValueP11 { get; set; }
        public double MtdTonnesP12 { get; set; }
        public double MtdValueP12 { get; set; }
        public double YtdTonnesP12 { get; set; }
        public double YtdValueP12 { get; set; }
        public double MtdTonnesPp1 { get; set; }
        public double MtdValuePp1 { get; set; }
        public double YtdTonnesPp1 { get; set; }
        public double YtdValuePp1 { get; set; }
        public double MtdTonnesPp2 { get; set; }
        public double MtdValuePp2 { get; set; }
        public double YtdTonnesPp2 { get; set; }
        public double YtdValuePp2 { get; set; }
        public double MtdTonnesPp3 { get; set; }
        public double MtdValuePp3 { get; set; }
        public double YtdTonnesPp3 { get; set; }
        public double YtdValuePp3 { get; set; }
        public double MtdTonnesPp4 { get; set; }
        public double MtdValuePp4 { get; set; }
        public double YtdTonnesPp4 { get; set; }
        public double YtdValuePp4 { get; set; }
        public double MtdTonnesPp5 { get; set; }
        public double MtdValuePp5 { get; set; }
        public double YtdTonnesPp5 { get; set; }
        public double YtdValuePp5 { get; set; }
        public double MtdTonnesPp6 { get; set; }
        public double MtdValuePp6 { get; set; }
        public double YtdTonnesPp6 { get; set; }
        public double YtdValuePp6 { get; set; }
        public double MtdTonnesPp7 { get; set; }
        public double MtdValuePp7 { get; set; }
        public double YtdTonnesPp7 { get; set; }
        public double YtdValuePp7 { get; set; }
        public double MtdTonnesPp8 { get; set; }
        public double MtdValuePp8 { get; set; }
        public double YtdTonnesPp8 { get; set; }
        public double YtdValuePp8 { get; set; }
        public double MtdTonnesPp9 { get; set; }
        public double MtdValuePp9 { get; set; }
        public double YtdTonnesPp9 { get; set; }
        public double YtdValuePp9 { get; set; }
        public double MtdTonnesPp10 { get; set; }
        public double MtdValuePp10 { get; set; }
        public double YtdTonnesPp10 { get; set; }
        public double YtdValuePp10 { get; set; }
        public double MtdTonnesPp11 { get; set; }
        public double MtdValuePp11 { get; set; }
        public double YtdTonnesPp11 { get; set; }
        public double YtdValuePp11 { get; set; }
        public double MtdTonnesPp12 { get; set; }
        public double MtdValuePp12 { get; set; }
        public double YtdTonnesPp12 { get; set; }
        public double YtdValuePp12 { get; set; }
        public double SalesVolYr1 { get; set; }
        public double SalesVolYr2 { get; set; }
        public double SalesVolYr3 { get; set; }
        public double SalesVolYr4 { get; set; }
        public double SalesValYr1 { get; set; }
        public double SalesValYr2 { get; set; }
        public double SalesValYr3 { get; set; }
        public double SalesValYr4 { get; set; }

    }
}
