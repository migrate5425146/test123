﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class CRMAppointmentEntity : BaseEntity
    {
        private CRMAppointmentEntity()
        {
            this.IsNew = true;
        }

        private CRMAppointmentEntity(int entityId)
        {
            this.AppointmentID = entityId;
        }

        public static CRMAppointmentEntity CreateObject()
        {
            CRMAppointmentEntity entity = new CRMAppointmentEntity();
            return entity;
        }

        public static CRMAppointmentEntity CreateObject(int entityId)
        {
            CRMAppointmentEntity entity = new CRMAppointmentEntity(entityId);
            return entity;
        }

        public int AppointmentID { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
        public string Category { get; set; }
        public string CreatedBy { get; set; }
    }
}
