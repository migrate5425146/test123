﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class LeadChecklistEntity : BaseEntity
    {
        private LeadChecklistEntity()
        {
            this.IsNew = true;
        }

        private LeadChecklistEntity(int entityId)
        {
            this.ChecklistID = entityId;
        }

        public static LeadChecklistEntity CreateObject()
        {
            LeadChecklistEntity entity = new LeadChecklistEntity();
            return entity;
        }

        public static LeadChecklistEntity CreateObject(int entityId)
        {
            LeadChecklistEntity entity = new LeadChecklistEntity(entityId);
            return entity;
        }

        public int ChecklistID { get; set; }
        public int LeadID { get; set; }
        public int PiepelineStageID { get; set; }

    }
}
