﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ComponentParameterEntity : BaseEntity
    {
        private ComponentParameterEntity()
        {
            this.IsNew = true;
        }

        private ComponentParameterEntity(int entityId)
        {
            this.ComponentParameterId = entityId;
        }

        public static ComponentParameterEntity CreateObject()
        {
            ComponentParameterEntity entity = new ComponentParameterEntity();
            return entity;
        }

        public static ComponentParameterEntity CreateObject(int entityId)
        {
            ComponentParameterEntity entity = new ComponentParameterEntity(entityId);
            return entity;
        }

        private CostingParameterEntity parameter;
        private int? componentParameterId;
        private int? componentId;
        private int? parameterId;

        public int? ComponentParameterId 
        { 
            get 
            { 
                return componentParameterId; 
            } 
            set 
            { 
                componentParameterId = value; 
                IsDirty = true; 
            } 
        }
        public int? ComponentId 
        { 
            get 
            { 
                return componentId; 
            } 
            set 
            { 
                componentId = value; 
                IsDirty = true; 
            } 
        }
        public int? ParameterId 
        { 
            get 
            { 
                return parameterId; 
            } 
            set 
            { 
                parameterId = value; 
                IsDirty = true; 
            } 
        }

        public CostingParameterEntity Parameter 
        {
            get
            {
                if (parameter == null)
                    parameter = CostingParameterEntity.CreateObject();

                return parameter;
            }
            set
            {
                parameter = value;
            }
        }
    }
}
