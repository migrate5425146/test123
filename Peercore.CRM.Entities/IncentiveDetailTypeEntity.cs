﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class IncentiveDetailTypeEntity:BaseEntity
    {
        public IncentiveDetailTypeEntity()
        {
        }

        public IncentiveDetailTypeEntity(int entityId)
        {
            this.IpDetailsTypeId = entityId;
        }

        public static IncentiveDetailTypeEntity CreateObject()
        {
            IncentiveDetailTypeEntity entity = new IncentiveDetailTypeEntity();
            return entity;
        }

        public static IncentiveDetailTypeEntity CreateObject(int entityId)
        {
            IncentiveDetailTypeEntity entity = new IncentiveDetailTypeEntity(entityId);
            return entity;
        }

        public int IpDetailsTypeId { get; set; }
        public string SchemeType { get; set; }
        public string Description { get; set; }
        public bool IsActive { get; set; }
    }
}
