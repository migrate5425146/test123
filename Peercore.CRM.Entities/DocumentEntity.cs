﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class DocumentEntity:BaseEntity
    {
        public DocumentEntity()
        {
            
        }

        public DocumentEntity(int entityId)
        {
            this.DocumentID = entityId;
        }

        public static DocumentEntity CreateObject()
        {
            DocumentEntity entity = new DocumentEntity();

            return entity;
        }

        public static DocumentEntity CreateObject(int entityId)
        {
            DocumentEntity entity = new DocumentEntity(entityId);

            return entity;
        }

        public int DocumentID { get; set; }
        public int LeadID { get; set; }

        private string fileName;
        public string DocumentName
        {
            get { return fileName; }
            set
            {
                fileName = value;
                IsDirty = true;
            }
        }

        private byte[] fileContent;
        public byte[] Content
        {
            get { return fileContent; }
            set
            {
                fileContent = value;
                IsDirty = true;
            }
        }

        public string Path { get; set; }
        public string CustCode { get; set; }
        public DateTime AttachedDate { get; set; }
        public string AttachedBy { get; set; }

        public string EnduserCode { get; set; }
        public string CustomerType { get; set; }

        public int RowCount { get; set; }
    }
}
