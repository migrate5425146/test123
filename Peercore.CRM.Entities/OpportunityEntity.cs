﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class OpportunityEntity : BaseEntity
    {
        private OpportunityEntity()
        {
            this.IsNew = true;
        }

        private OpportunityEntity(int entityId)
        {
            this.OpportunityID = entityId;
        }

        public static OpportunityEntity CreateObject()
        {
            OpportunityEntity entity = new OpportunityEntity();
            return entity;
        }

        public static OpportunityEntity CreateObject(int entityId)
        {
            OpportunityEntity entity = new OpportunityEntity(entityId);
            return entity;
        }

        public int OpportunityID { get; set; }
        public int LeadID { get; set; }
        public string Originator { get; set; }
        public int RepGroupID { get; set; }
        public string Name { get; set; }
        public DateTime CloseDate { get; set; }
        public int Stage { get; set; }
        public double Probability { get; set; }
        public double Amount { get; set; }
        public double Units { get; set; }
        public string Description { get; set; }
        //public DateTime? CreatedDate { get; set; }
        //public string CreatedBy { get; set; }
        //public DateTime? LastModifiedDate { get; set; }
        //public string LastModifiedBy { get; set; }
        public string CustCode { get; set; }
        public string PipelineStage { get; set; }

        public string EndUserCode { get; set; }
        public double Tonnes { get; set; }
        
    }
}
