﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class OpportunityOwnerEntity : BaseEntity
    {
        private OpportunityOwnerEntity()
        {
            this.IsNew = true;
        }

        private OpportunityOwnerEntity(int entityId)
        {
            //this.OpportunityID = entityId;
        }

        public static OpportunityOwnerEntity CreateObject()
        {
            OpportunityOwnerEntity entity = new OpportunityOwnerEntity();
            return entity;
        }

        public static OpportunityOwnerEntity CreateObject(int entityId)
        {
            OpportunityOwnerEntity entity = new OpportunityOwnerEntity(entityId);
            return entity;
        }

        public string OwnerName { get; set; }

        public double Amount { get; set; }

        public double Units { get; set; }

        public int Count { get; set; }

        public int Ratio { get; set; }

        public string Originator { get; set; }
    }
}
