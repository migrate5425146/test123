﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class VisitChecklistEntity: BaseEntity
    {
        public VisitChecklistEntity()
        {
            this.IsNew = true;
        }

        public VisitChecklistEntity(int entityId)
        {
            this.VisitChecklistId = entityId;
        }

        public static VisitChecklistEntity CreateObject()
        {
            VisitChecklistEntity entity = new VisitChecklistEntity();
            return entity;
        }

        public static VisitChecklistEntity CreateObject(int entityId)
        {
            VisitChecklistEntity entity = new VisitChecklistEntity(entityId);
            return entity;
        }
        
        public int VisitChecklistId { get; set; }
        public bool HasCarStock { get; set; }
        public bool HasMerchandise { get; set; }
        public bool IsVehicleCheck { get; set; }
        public int RouteMasterId { get; set; }
        public string Status { get; set; }
        public List<MobileObjectiveEntity> NationalObjectivesList { get; set; }
        public List<MobileMarketObjectiveEntity> MarketWiseObjectivesList { get; set; }
    }
}
