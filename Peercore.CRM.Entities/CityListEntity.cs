﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class CityListEntity : BaseEntity
    {
        private CityListEntity()
        {
            this.IsNew = true;
        }

        private CityListEntity(int entityId)
        {
            //this.AreaID = entityId;
        }

        public static CityListEntity CreateObject()
        {
            CityListEntity entity = new CityListEntity();
            return entity;
        }

        public static CityListEntity CreateObject(int entityId)
        {
            CityListEntity entity = new CityListEntity(entityId);
            return entity;
        }

        public string City { get; set; }

        private List<AreaListEntity> areas = null;

        public List<AreaListEntity> Areas
        {
            get
            {
                if (this.areas == null)
                {
                    this.areas = new List<AreaListEntity>();
                }
                return this.areas;
            }
        }
    }
}
