﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class LoadStockEntity : BaseEntity
    {
        private LoadStockEntity()
        {
            this.IsNew = true;
        }

        //private LoadStockEntity(int entityId)
        //{
        //    this.LeadStageId = entityId;
        //}

        public static LoadStockEntity CreateObject()
        {
            LoadStockEntity entity = new LoadStockEntity();
            return entity;
        }

        //public static LoadStockEntity CreateObject(int entityId)
        //{
        //    LoadStockEntity entity = new LoadStockEntity(entityId);
        //    return entity;
        //}

        public int StockId;
        public int CatalogId;
        public string CatalogCode;
        public string CatalogName;
        public int AllocQty;
        public int LoadQty;
        public int BalanceQty;
        public DateTime AllocDate;
        public string Originator;
        public int RouteId;
        public string RepCode;
        public int RouteAssignID;
        public List<LoadStockDetailEntity> lstLoadStockDetail;
        public int DistributorId;
        public string StockAllocType;
    }
}
