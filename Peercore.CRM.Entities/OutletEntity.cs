﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class OutletEntity
    {
        public OutletEntity()
        {
        }

        public OutletEntity(int outletId)
        {
            this.OutletId = outletId;
        }

        public static OutletEntity CreateObject()
        {
            OutletEntity entity = new OutletEntity();
            return entity;
        }

        public static OutletEntity CreateObject(int outletId)
        {
            OutletEntity outlet = new OutletEntity(outletId);
            return outlet;
        }

        public int OutletId { get; set; }
        public string CustomerCode { get; set; }
        public string Name { get; set; }
        public string Specify { get; set; }
        public string Status { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }


    }
}
