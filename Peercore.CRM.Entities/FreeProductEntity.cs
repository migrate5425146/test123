﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class FreeProductEntity:BaseEntity
    {
        public FreeProductEntity()
        {

        }

        public FreeProductEntity(int entityId)
        {
            this.FreeProductId = entityId;
        }

        public static FreeProductEntity CreateObject()
        {
            FreeProductEntity entity = new FreeProductEntity();

            return entity;
        }

        public static FreeProductEntity CreateObject(int entityId)
        {
            FreeProductEntity entity = new FreeProductEntity(entityId);

            return entity;
        }

        public int FreeProductId { get; set; }
        public int SchemeDetailsId { get; set; }
        public int ProductId { get; set; }
        public double Qty { get; set; }
        public int ProductPackingId { get; set; }
        
        public int CatlogId { get; set; }
        public int BrandId { get; set; }
        public int ProductCatagoryId { get; set; }
        public int ItemTypeId { get; set; }

        public string ProductName { get; set; }
        public string ProductPackingName { get; set; }
        public string CatlogName { get; set; }
        public string BrandName { get; set; }
        public string ProductCatagoryName { get; set; }
        public string ItemTypeName { get; set; }
          
            
            
    }
}
