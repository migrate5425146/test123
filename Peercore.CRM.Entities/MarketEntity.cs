﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class MarketEntity : BaseEntity
    {
        private MarketEntity()
        {
            this.IsNew = true;
        }

        private MarketEntity(int entityId)
        {
            this.MarketId = entityId;
        }

        public static MarketEntity CreateObject()
        {
            MarketEntity entity = new MarketEntity();
            return entity;
        }

        public static MarketEntity CreateObject(int entityId)
        {
            MarketEntity entity = new MarketEntity(entityId);
            return entity;
        }

        private int marketId;
        public int MarketId
        {
            get { return marketId; }
            set { marketId = value; IsDirty = true; }
        }

        private string marketName;
        public string MarketName
        {
            get { return marketName; }
            set { marketName = value; IsDirty = true; }
        }

        private string status;
        public string Status
        {
            get { return status; }
            set { status = value; IsDirty = true; }
        }

        private string originator;
        public string Originator
        {
            get { return originator; }
            set { originator = value; IsDirty = true; }
        }

        #region For the purpose of Target Allocation
        private double target;
        public double Target
        {
            get { return target; }
            set { target = value; }
        }

        private int targetId;
        public int TargetId
        {
            get { return targetId; }
            set { targetId = value; }
        }

        private DateTime effStartDate;
        public DateTime EffStartDate
        {
            get { return effStartDate; }
            set { effStartDate = value; }
        }

        private DateTime effEndDate;
        public DateTime EffEndDate
        {
            get { return effEndDate; }
            set { effEndDate = value; }
        }

        private int distributorId;
        public int DistributorId
        {
            get { return distributorId; }
            set { distributorId = value; }
        }

        private DateTime createdDate;
        public DateTime CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        private DateTime lastModifiedDate;
        public DateTime LastModifiedDate
        {
            get { return lastModifiedDate; }
            set { lastModifiedDate = value; }
        }

        private string createdBy;
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        private string lastModifiedBy;
        public string LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; }
        }

        #endregion
    }
}
