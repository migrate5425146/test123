﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class GridStructHeaderEntity : BaseEntity
    {
        private GridStructHeaderEntity()
        {
            this.IsNew = true;
        }

        private GridStructHeaderEntity(int entityId)
        {
            this.IdNo = entityId;
        }

        public static GridStructHeaderEntity CreateObject()
        {
            GridStructHeaderEntity entity = new GridStructHeaderEntity();
            return entity;
        }

        public static GridStructHeaderEntity CreateObject(int entityId)
        {
            GridStructHeaderEntity entity = new GridStructHeaderEntity(entityId);
            return entity;
        }

        int idNo = 0;
        string originator = string.Empty;
        string userControl = string.Empty;
        string gridName = string.Empty;
        string filtername = string.Empty;

        public int IdNo 
        {
            get { return idNo; }
            set { idNo = value; }
        }

        public string Originator
        {
            get { return originator; }
            set { originator = value; }
        }

        public string UserControl
        {
            get { return userControl; }
            set { userControl = value; }
        }

        public string GridName
        {
            get { return gridName; }
            set { gridName = value; }
        }

        public string FilterName
        {
            get { return filtername; }
            set { filtername = value; }
        }
    }
}
