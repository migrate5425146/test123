﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class BulkSchedEntity : BaseEntity
    {
        public BulkSchedEntity()
        {
        }

        public static BulkSchedEntity CreateObject()
        {
            BulkSchedEntity entity = new BulkSchedEntity();
            return entity;
        }

        public int PlistNo { get; set; }
        public string CatlogCode { get; set; }
        public DateTime LoadDate { get; set; }
        public DateTime LoadTime { get; set; }
        public DateTime DelTime { get; set; }
        public string CarrierCode { get; set; }
        public string StockOk { get; set; }
        public string StockReason { get; set; }        
        public int TypeOpt { get; set; }
	    public string TankNo { get; set; }
        public int TranNo { get; set; }
        public string BatchNo { get; set; }
        public string SealNo { get; set; }
        public string LeftNote { get; set; }
        public string RightNote { get; set; }
        
    }
}
