﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class IncentiveDiscountSchemeEntity:BaseEntity
    {
         public IncentiveDiscountSchemeEntity()
        {
            this.IsNew = true;
        }

         public IncentiveDiscountSchemeEntity(int entityId)
        {
            this.IpOptionLevelId = entityId;
        }

         public static IncentiveDiscountSchemeEntity CreateObject()
        {
            IncentiveDiscountSchemeEntity entity = new IncentiveDiscountSchemeEntity();

            return entity;
        }

         public static IncentiveDiscountSchemeEntity CreateObject(int entityId)
        {
            IncentiveDiscountSchemeEntity entity = new IncentiveDiscountSchemeEntity(entityId);
            return entity;
        }

        public int IpIncentiveDiscountSchemeId { get; set; }
        public int IpOptionLevelId { get; set; }
        public int DiscountSchemeDetailsId { get; set; }
        public bool IsActive { get; set; }
    }
}
