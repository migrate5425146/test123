﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class RouteMasterEntity : BaseEntity
    {
        private RouteMasterEntity()
        {
            
        }
        public RouteMasterEntity(int entityId)
        {
            this.RouteMasterId = entityId;
        }
        
        public static RouteMasterEntity CreateObject()
        {
            RouteMasterEntity entity = new RouteMasterEntity();

            return entity;
        }

        public static RouteMasterEntity CreateObject(int entityId)
        {
            RouteMasterEntity entity = new RouteMasterEntity(entityId);

            return entity;
        }


        public int RouteMasterId { get; set; }
        public string RouteMasterName { get; set; }
        public string Status { get; set; }

        //public string CreatedBy { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public string LastModifiedBy { get; set; }
        //public DateTime LastModifiedDate { get; set; }
        public string RouteType { get; set; }

        public string RepCode { get; set; }
        public string RepName { get; set; }
        public string CreatedByName { get; set; }
        public int CallCycleId { get; set; }
        public int RouteSequence { get; set; }

        public bool HasSelect { get; set; }

        #region For the purpose of Target Allocation
        public double Target { get; set; }
        public int TargetId { get; set; }
        public DateTime EffStartDate { get; set; }
        public DateTime EffEndDate { get; set; }
        #endregion
    }
}
