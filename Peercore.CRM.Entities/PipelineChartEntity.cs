﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class PipelineChartEntity : BaseEntity
    {
        private PipelineChartEntity()
        {
            this.IsNew = true;
        }

        private PipelineChartEntity(int entityId)
        {
            this.PipelineStageID = entityId;
        }

        public static PipelineChartEntity CreateObject()
        {
            PipelineChartEntity entity = new PipelineChartEntity();
            return entity;
        }

        public static PipelineChartEntity CreateObject(int entityId)
        {
            PipelineChartEntity entity = new PipelineChartEntity(entityId);
            return entity;
        }

        public int PipelineStageID { get; set; }
        public string PipelineStage { get; set; }
        public Int16 Probability { get; set; }
        public int NoOfOpportunities { get; set; }
    }
}
