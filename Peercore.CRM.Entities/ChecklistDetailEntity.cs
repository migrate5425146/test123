﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ChecklistDetailEntity : BaseEntity
    {
        private ChecklistDetailEntity()
        {
            this.IsNew = true;
        }

        private ChecklistDetailEntity(int entityId)
        {
            this.ChecklistId = entityId;
        }

        public static ChecklistDetailEntity CreateObject()
        {
            ChecklistDetailEntity entity = new ChecklistDetailEntity();
            return entity;
        }

        public static ChecklistDetailEntity CreateObject(int entityId)
        {
            ChecklistDetailEntity entity = new ChecklistDetailEntity(entityId);
            return entity;
        }

        public int ChecklistId { get; set; }
        public string Remarks { get; set; }
        public string CustomerCode { get; set; }
        public string RepCode { get; set; }
        public int RouteId { get; set; }
    }
}
