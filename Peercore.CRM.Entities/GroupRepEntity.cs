﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class GroupRepEntity : BaseEntity
    {
        private GroupRepEntity()
        {
            this.IsNew = true;
        }

        private GroupRepEntity(int entityId)
        {
            this.RepGroupId = entityId;
        }

        public static GroupRepEntity CreateObject()
        {
            GroupRepEntity entity = new GroupRepEntity();
            return entity;
        }

        public static GroupRepEntity CreateObject(int entityId)
        {
            GroupRepEntity entity = new GroupRepEntity(entityId);
            return entity;
        }

        private int? repGroupId;

        public int? RepGroupId
        {
            get { return repGroupId; }
            set
            {
                repGroupId = value;
            }
        }
        private string originator;

        public string Originator
        {
            get { return originator; }
            set { originator = value; IsDirty = true; }
        }

    }
}
