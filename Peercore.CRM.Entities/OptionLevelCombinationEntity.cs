﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class OptionLevelCombinationEntity : BaseEntity
    {
        public OptionLevelCombinationEntity()
        {
        }

        public OptionLevelCombinationEntity(int entityId)
        {
        }

        public static OptionLevelCombinationEntity CreateObject()
        {
            OptionLevelCombinationEntity entity = new OptionLevelCombinationEntity();
            return entity;
        }

        public static OptionLevelCombinationEntity CreateObject(int entityId)
        {
            OptionLevelCombinationEntity entity = new OptionLevelCombinationEntity(entityId);
            return entity;
        }

        public int OptionLevelCombinationId { get; set; }
        public int OptionLevelId { get; set; }
        public int ProductId { get; set; }
        public bool IsExceptProduct { get; set; }
        public int ProductCategoryId { get; set; }
        public int ProductPackingId { get; set; }
        public int BrandId { get; set; }
        public int ItemTypeId { get; set; }
        public bool IsActive { get; set; }
        public bool? IsHvp { get; set; }
        public int FlavorId { get; set; }

        public string ProductName { get; set; }
        public string ProductPackingName { get; set; }
        public string FlavorName { get; set; }
        public string BrandName { get; set; }
        public string ProductCatagoryName { get; set; }

        public string conditionType { get; set; }
    }
}
