﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Entities
{
    public class ProductUOMEntity
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Conversion { get; set; }
        public string Status { get; set; }
        public string FgCode { get; set; }
        public string Code { get; set; }
    }
}
