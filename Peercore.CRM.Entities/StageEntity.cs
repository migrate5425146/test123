﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class StageEntity:BaseEntity
    {
        private StageEntity()
        {
            this.IsNew = true;
        }

        private StageEntity(int entityId)
        {
           // this.SOrderNo = entityId;
        }

        public static StageEntity CreateObject()
        {
            StageEntity entity = new StageEntity();
            return entity;
        }

        public static StageEntity CreateObject(int entityId)
        {
            StageEntity entity = new StageEntity(entityId);
            return entity;
        }

        public string RepName { get; set; }
        public string StageName { get; set; }
        //public Int32 Count { get; set; }
        private int count = 0;

        public int Count
        {
            get
            {
                if (count == 0)
                    return 0;
                else
                    return count;
            }
            set { count = value; }
        }
    }
}
