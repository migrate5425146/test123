﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    [Serializable]
    public class SchedularTransferComponentEntity : BaseEntity
    {
        public SchedularTransferComponentEntity()
        {
            //this.ActivityType = 0;
            this.IsNew = true;
        }

        public SchedularTransferComponentEntity(int entityId)
        {
           // this.ActivityType = entityId;
        }

        public static SchedularTransferComponentEntity CreateObject()
        {
            SchedularTransferComponentEntity entity = new SchedularTransferComponentEntity();

            return entity;
        }

        public static SchedularTransferComponentEntity CreateObject(int entityId)
        {
            SchedularTransferComponentEntity entity = new SchedularTransferComponentEntity(entityId);

            return entity;
        }

        private DateTime selectedDate;
        public DateTime SelectedDate
        {
            get { return selectedDate; }
            set { selectedDate = value; }
        }

        private TimeSpan selectedTime;
        public TimeSpan SelectedTime
        {
            get { return selectedTime; }
            set { selectedTime = value; }
        }

        private bool createActivity;
        public bool CreateActivity
        {
            get { return createActivity; }
            set { createActivity = value; }
        }

        private int weekDay;
        public int WeekDay
        {
            get { return weekDay; }
            set { weekDay = value; }
        }
    }
}
