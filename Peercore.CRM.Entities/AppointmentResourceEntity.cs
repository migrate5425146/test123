﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class AppointmentResourceEntity : BaseEntity
    {
        private AppointmentResourceEntity()
        {
            this.IsNew = true;
        }

        private AppointmentResourceEntity(int entityId)
        {
            this.ResourceID = entityId;
        }

        public static AppointmentResourceEntity CreateObject()
        {
            AppointmentResourceEntity entity = new AppointmentResourceEntity();

            return entity;
        }

        public static AppointmentResourceEntity CreateObject(int entityId)
        {
            AppointmentResourceEntity entity = new AppointmentResourceEntity(entityId);

            return entity;
        }

        public int AppointmentID { get; set; }
        public int ResourceID { get; set; }
    }
}
