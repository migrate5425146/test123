﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Entities
{
    public class CompanyEntity : BaseEntity
    {
        private CompanyEntity()
        {
            this.IsNew = true;
        }

        private CompanyEntity(int entityId)
        {
            //this.CallCycleID = entityId;
        }

        public static CompanyEntity CreateObject()
        {
            CompanyEntity entity = new CompanyEntity();
            return entity;
        }

        public static CompanyEntity CreateObject(int entityId)
        {
            CompanyEntity entity = new CompanyEntity(entityId);
            return entity;
        }

        public decimal CompId { get; set; }
        public string CompCode { get; set; }
        public string CompName { get; set; }
    }
}
