﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class VolumeAssesmentEntity : BaseEntity
    {
            public VolumeAssesmentEntity()
            {
                this.IsNew = true;
            }

            public static VolumeAssesmentEntity CreateObject()
            {
                VolumeAssesmentEntity entity = new VolumeAssesmentEntity();
                return entity;
            }

            public int VolumeAssesementId { get; set; }
            public string CustCode { get; set; }
            public int ProductId { get; set; }
            public int Value { get; set; }
            public string Status { get; set; }
    }
}
