﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ProgramEntity:BaseEntity
    {
        private ProgramEntity()
        {
            
        }
        public ProgramEntity(int entityId)
        {
            this.ProgramId = entityId;
        }

        public static ProgramEntity CreateObject()
        {
            ProgramEntity entity = new ProgramEntity();

            return entity;
        }

        public static ProgramEntity CreateObject(int entityId)
        {
            ProgramEntity entity = new ProgramEntity(entityId);

            return entity;
        }

        public int ProgramId { get; set; }
        public string ProgramName { get; set; }
        public string SelectionType { get; set; }
 
        public List<int> VolumeList { get; set; }
        public List<string> CustomerList { get; set; }
        public string Status { get; set; }
    }
}
