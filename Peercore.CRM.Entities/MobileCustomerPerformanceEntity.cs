﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class MobileCustomerPerformanceEntity
    {

        public MobileCustomerPerformanceEntity()
        {
        }



        public static MobileCustomerPerformanceEntity CreateObject()
        {
            MobileCustomerPerformanceEntity entity = new MobileCustomerPerformanceEntity();
            return entity;
        }


        public string CustCode{ get; set; }
        public string BrandCode { get; set; }

        public int Target { get; set; }
        public int Actual { get; set; }
        public double AchievementPerc { get; set; }
        public double ReqiredStrikeRate{get; set;}
        public double CurrentStrikeRate { get; set; }

        public int TotalTarget { get; set; }
        public int TotalActual { get; set; }
        public double TotalAchievementPerc { get; set; }
        public double TotalReqiredStrikeRate { get; set; }
        public double TotalCurrentStrikeRate { get; set; }

    }
}
