﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class QuotationComponentEntity : BaseEntity
    {
        private QuotationComponentEntity()
        {
            this.IsNew = true;
        }

        private QuotationComponentEntity(int entityId)
        {
            this.QuotationComponentId = entityId;
        }

        public static QuotationComponentEntity CreateObject()
        {
            QuotationComponentEntity entity = new QuotationComponentEntity();
            return entity;
        }

        public static QuotationComponentEntity CreateObject(int entityId)
        {
            QuotationComponentEntity entity = new QuotationComponentEntity(entityId);
            return entity;
        }

        private int? quotationComponentId;
        private int? quotationId;
        private int? templateId;
        private int? componentId;
        private CostingComponentEntity component;
        private List<QuotationComponentBranchEntity> quotationComponentBranchList;

        public List<QuotationComponentBranchEntity> QuotationComponentBranchList
        {
            get
            {
                if (quotationComponentBranchList == null)
                    quotationComponentBranchList = new List<QuotationComponentBranchEntity>();

                return quotationComponentBranchList;
            }
            set
            {
                quotationComponentBranchList = value;
                IsDirty = true;
            }
        }


        public int? ComponentId
        {
            get { return componentId; }
            set
            {
                componentId = value;
                IsDirty = true;
            }
        }

        public int? TemplateId
        {
            get { return templateId; }
            set
            {
                templateId = value;
                IsDirty = true;
            }
        }

        public int? QuotationId
        {
            get { return quotationId; }
            set
            {
                quotationId = value;
                IsDirty = true;
            }
        }

        public int? QuotationComponentId
        {
            get { return quotationComponentId; }
            set
            {
                quotationComponentId = value;
                IsDirty = true;
            }
        }

        public CostingComponentEntity Component
        {
            get
            {
                if (component == null)
                    component = CostingComponentEntity.CreateObject();

                return component;
            }
            set
            {
                component = value;
            }
        }

    }
}
