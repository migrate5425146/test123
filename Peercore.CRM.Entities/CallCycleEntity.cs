﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class CallCycleEntity : BaseEntity
    {
        private CallCycleEntity()
        {
            this.IsNew = true;
        }

        private CallCycleEntity(int entityId)
        {
            this.CallCycleID = entityId;
        }

        public static CallCycleEntity CreateObject()
        {
            CallCycleEntity entity = new CallCycleEntity();

            return entity;
        }

        public static CallCycleEntity CreateObject(int entityId)
        {
            CallCycleEntity entity = new CallCycleEntity(entityId);

            return entity;
        }

        int iCallCycleID = 0;
        string sDescription = string.Empty;
        DateTime dDueOn = DateTime.Today;
        string cComments = "";
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string DelFlag { get; set; }
        public string CCType { get; set; }
        public string RepName { get; set; }
        // Not in Table
        public string PrefixCode { get; set; }


        #region Public Properties

        public int CallCycleID
        {
            get { return iCallCycleID; }
            set { iCallCycleID = value; }
        }

        public string Description
        {
            get { return sDescription; }
            set { sDescription = value; }
        }

        public DateTime DueOn
        {
            get { return dDueOn; }
            set { dDueOn = value; }
        }


        public string Comments
        {
            get { return cComments; }
            set { cComments = value; }
        }

        public int NoOfLeadCustomers { get; set; }

        #endregion
    }
}
