﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class CountryEntity : BaseEntity
    {
        private CountryEntity()
        {
            this.IsNew = true;
        }

        private CountryEntity(int entityId)
        {
            this.CountryId = entityId;
        }

        public static CountryEntity CreateObject()
        {
            CountryEntity entity = new CountryEntity();
            return entity;
        }

        public static CountryEntity CreateObject(int entityId)
        {
            CountryEntity entity = new CountryEntity(entityId);
            return entity;
        }

        private int? countryId;
        private string country;

        public string CountryName
        {
            get { return country; }
            set { country = value; }
        }

        public int? CountryId
        {
            get { return countryId; }
            set { countryId = value; }
        }
    }
}
