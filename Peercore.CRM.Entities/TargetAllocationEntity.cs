﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class TargetAllocationEntity:BaseEntity
    {
        private TargetAllocationEntity()
        {
            
        }

        public static TargetAllocationEntity CreateObject()
        {
            TargetAllocationEntity entity = new TargetAllocationEntity();

            return entity;
        }

        private List<DistributorEntity> distributorTargetList;
        public List<DistributorEntity> DistributorTargetList
        {
            get { return distributorTargetList; }
            set { distributorTargetList = value; }
        }

        private List<MarketEntity> marketTargetList;
        public List<MarketEntity> MarketTargetList
        {
            get { return marketTargetList; }
            set { marketTargetList = value; }
        }

        private List<RouteMasterEntity> routeTargetList;
        public List<RouteMasterEntity> RouteTargetList
        {
            get { return routeTargetList; }
            set { routeTargetList = value; }
        }
    }
}
