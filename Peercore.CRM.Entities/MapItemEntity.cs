﻿using System;
//using Telerik.Windows.Controls.Map;
using System.Collections.ObjectModel;

namespace Peercore.CRM.Entities
{
    public class MapItemEntity : BaseEntity
	{
        private MapItemEntity()
        {
            this.IsNew = true;
        }

        private MapItemEntity(int entityId)
        {
            //this.ChecklistID = entityId;
        }

        public static MapItemEntity CreateObject()
        {
            MapItemEntity entity = new MapItemEntity();
            return entity;
        }

        public static MapItemEntity CreateObject(int entityId)
        {
            MapItemEntity entity = new MapItemEntity(entityId);
            return entity;
        }

        //public Location Location
        //{
        //    get;
        //    set;
        //}
		public string Title
		{
			get;
			set;
		}
		public string Description
		{
			get;
			set;
		}
	}

    public class MapItemsCollection : ObservableCollection<MapItemEntity>
    {
    }
}
