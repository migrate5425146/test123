﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class CustRepMktEntity : BaseEntity
    {
        public CustRepMktEntity()
        {
        }

        public static CustRepMktEntity CreateObject()
        {
            CustRepMktEntity entity = new CustRepMktEntity();
            return entity;
        }

        public string CustCode { get; set; }
        public string RepCode { get; set; }
        public string Market { get; set; }
    }
}
