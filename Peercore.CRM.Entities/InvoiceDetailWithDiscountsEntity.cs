﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class InvoiceDetailWithDiscountsEntity : BaseEntity
    {
        public InvoiceDetailWithDiscountsEntity()
        {            
        }

        public static InvoiceDetailWithDiscountsEntity CreateObject()
        {
            InvoiceDetailWithDiscountsEntity entity = new InvoiceDetailWithDiscountsEntity();
            return entity;
        }

        public int InvoiceDetailId { get; set; }
        public int InvoiceId { get; set; }
        public int ProductId { get; set; }
        public int SchemeDetailId { get; set; }
        public int Quantity { get; set; }
        public double Discount { get; set; }
        public double DiscountValue { get; set; }
        public double SubTotal { get; set; }
        public double ItemPrice { get; set; }
        public string ProductName { get; set; }

    }
}
