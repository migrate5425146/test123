﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class CostingTemplateEntity : BaseEntity
    {
        private CostingTemplateEntity()
        {
            this.IsNew = true;
        }

        private CostingTemplateEntity(int entityId)
        {
            this.TemplateId = entityId;
        }

        public static CostingTemplateEntity CreateObject()
        {
            CostingTemplateEntity entity = new CostingTemplateEntity();
            return entity;
        }

        public static CostingTemplateEntity CreateObject(int entityId)
        {
            CostingTemplateEntity entity = new CostingTemplateEntity(entityId);
            return entity;
        }

        private int? templateId;
        private string templateName;
        private List<TemplateComponentEntity> templateComponentList;

        public int? TemplateId
        {
            get
            {
                return templateId;
            }
            set
            {
                templateId = value;
                IsDirty = true;
            }
        }

        public string TemplateName
        {
            get
            {
                return templateName;
            }
            set
            {
                templateName = value;
                IsDirty = true;
            }
        }

        public List<TemplateComponentEntity> TemplateComponentList
        {
            get
            {
                return templateComponentList;
            }
            set
            {
                templateComponentList = value;
                IsDirty = true;
            }
        }

    }
}
