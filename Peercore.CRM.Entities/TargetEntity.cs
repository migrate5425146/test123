﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class TargetEntity : BaseEntity 
    {
        public TargetEntity()
        {
            this.IsNew = true;
        }

        private TargetEntity(int entityId)
        {
            //this.AreaID = entityId;
        }

        public static TargetEntity CreateObject()
        {
            TargetEntity entity = new TargetEntity();
            return entity;
        }

        public static TargetEntity CreateObject(int entityId)
        {
            TargetEntity entity = new TargetEntity(entityId);
            return entity;
        }

        public string RepCode { get; set; }
        public double Target { get; set; }
        public double Actual { get; set; }
        public int RouteId { get; set; }
        public int DistributorId { get; set; }
        public string DistributorName { get; set; }
        public string BrandCode { get; set; }
        public string CustomerCode { get; set; }
        public int MarketId { get; set; }
        public string MarketName { get; set; }
        public string RouteName { get; set; }
        public double TargetActualPercentage { get; set; }
        public string BrandName { get; set; }
        public double AverageSale { get; set; }
        public int BrandId { get; set; }

        private double targetInMils;
        public double TargetInMils
        {
            get { return Target != 0 ? Target / 1000 : 0; }
            set { targetInMils = value; }
        }

        private double actualInMils;
        public double ActualInMils
        {
            get { return Actual != 0 ? Actual / 1000 : 0; }
            set { actualInMils = value; }
        }

        public double AmountTarget { get; set; }
        public double AmountActual { get; set; }

        public decimal TargetInvId { get; set; }
        public string TargetInvType { get; set; }
        public string TargetInvDesc { get; set; }
        public double TargetInvVal { get; set; }
        public double TargetBreakDownDay { get; set; }
    }
}
