﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class QuotationParameterEntity : BaseEntity
    {
         private QuotationParameterEntity()
        {
            this.IsNew = true;
        }

         private QuotationParameterEntity(int entityId)
        {
            this.QuoationParameterId = entityId;
        }

         public static QuotationParameterEntity CreateObject()
        {
            QuotationParameterEntity entity = new QuotationParameterEntity();
            return entity;
        }

         public static QuotationParameterEntity CreateObject(int entityId)
        {
            QuotationParameterEntity entity = new QuotationParameterEntity(entityId);
            return entity;
        }

        private int? quotationParameterId;
        private int? quotationBranchId;
        private int? parameterId;
        private decimal amount;
        private CostingParameterEntity parameter;

        public decimal Amount
        {
            get { return amount; }
            set
            {
                amount = value;
                IsDirty = true;
            }
        }

        public int? ParameterId
        {
            get { return parameterId; }
            set
            {
                parameterId = value;
                IsDirty = true;
            }
        }

        public int? QuotationBranchId
        {
            get { return quotationBranchId; }
            set
            {
                quotationBranchId = value; 
                IsDirty = true;
            }
        }

        public int? QuoationParameterId
        {
            get { return quotationParameterId; }
            set
            {
                quotationParameterId = value;
                IsDirty = true;
            }
        }

        public CostingParameterEntity Parameter 
        {
            get
            {
                return parameter;
            }
            set
            {
                parameter = value;
            }
        }

    }
}
