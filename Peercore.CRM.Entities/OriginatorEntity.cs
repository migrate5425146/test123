﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class OriginatorEntity : BaseEntity
    {
        public OriginatorEntity()
        {
            this.IsNew = true;
        }

        private OriginatorEntity(int entityId)
        {
            this.UserId = entityId;
        }

        public static OriginatorEntity CreateObject()
        {
            OriginatorEntity entity = new OriginatorEntity();
            return entity;
        }

        public static OriginatorEntity CreateObject(int entityId)
        {
            OriginatorEntity entity = new OriginatorEntity(entityId);
            return entity;
        }

        /// <summary>
        /// Originator - The Login ID
        /// </summary>
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string FullName { get; set; }
        public string Email { get; set; }
        private string clientType;

        public string ClientType
        {
            get { return clientType; }
            set { clientType = value; }
        }
        public int OriginatorId { get; set; }
        public int AuthorizationGroupId { get; set; }
        public List<UserPermissionEntity> PermissionCollection{ get; set; }

        public string RepCode { get; set; }
        public string RepType { get; set; }

        public bool ManagerMode { get; set; }
        public bool HasChildReps { get; set; }
        public int CRMAuthLevel { get; set; }

        public string DefaultDepartmentId { get; set; }

        public string Originator { get; set; }
        public string PrefixCode { get; set; }
        public DateTime ExpiryDate { get; set; }
        public int AuthLevel { get; set; }
        public double OrderLimit { get; set; }
        public double InvoiceLimit { get; set; }
        public string ParentOriginator { get; set; }
        public int PersonalAuthNo { get; set; }
        public string DefaultDeptId { get; set; }
        public string DeliverTo { get; set; }
        public string PictureId { get; set; }
        public int ApproveMand { get; set; }
        public string Password { get; set; }
        public string PrintDevice { get; set; }
        public string RefInvPrinter { get; set; }
        public string RendInvPrinter { get; set; }
        public string InternetAdd { get; set; }
        public string BitmapPath { get; set; }
        public string EmployeeNo { get; set; }
        public string Alias { get; set; }
        public string DeptString { get; set; }
        public string Telephone { get; set; }
        public string Status { get; set; }
        public string LeaveDept { get; set; }
        public int LeaveDeptMgr { get; set; }
        public int CrmAuthLevel { get; set; }
        public string AccessToken { get; set; }       
        public string Adderess { get; set; }
        public string Mobile { get; set; }
        public int InvoiceCount { get; set; }
        public int user_type_id { get; set; }
        public string user_type { get; set; }
        public string type_des { get; set; }

        public string designation { get; set; }
        public int rowCount { get; set; }

        public int RouteId { get; set; }
        public string RouteName { get; set; }
        public int TerritoryId { get; set; }
        public string TerritoryCode { get; set; }
        public string TerritoryName { get; set; }
        public int AreaId { get; set; }
        public string AreaCode { get; set; }
        public string AreaName { get; set; }
        public int RegionId { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }

        public int distributorId { get; set; }
        public string distributorcode { get; set; }
        public string distributorname { get; set; }
        public string distributorDisplayName { get; set; }
        public string distributoraddress { get; set; }
        public string distributortel { get; set; }
        public int AsmId { get; set; }
        public string AsmCode { get; set; }
        public string Asmname { get; set; }
        public int RsmId { get; set; }
        public string RsmCode { get; set; }
        public string Rsmname { get; set; }
        
        public string LastInvoiceNumber { get; set; }
        public string LastOrderNumber { get; set; }
        public int SalesType { get; set; }
        public int CanAddOutlet { get; set; }

        public bool IsAttendanceCheckIn { get; set; }
        public bool IsAttendanceCheckOut { get; set; }
        public bool IsGeoFenceEnable { get; set; }
        public bool IsMTUser { get; set; }
        public DynamicReportEntity DashboardParam { get; set; }
        public string TFAPin { get; set; }
        public bool IsTFAUser { get; set; }
        public bool TFAIsAuthenticated { get; set; }
        public string OtpMobile { get; set; }
    }

    public class DynamicReportEntity
    {
        public string reportValue { get; set; }
    }
}
