﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class SorderHeaderEntity : BaseEntity
    {
        public SorderHeaderEntity()
        {            
        }

        public static SorderHeaderEntity CreateObject()
        {
            SorderHeaderEntity entity = new SorderHeaderEntity();
            return entity;
        }
        public int SorderNo { get; set; }
        public string CustCode { get; set; }
        public DateTime OrderDate { get; set; }
        public string OrderedBy { get; set; }
        public string OrderType { get; set; }
        public DateTime DateRequired { get; set; }
        public string CustOrderNo { get; set; }
        public int AssigneeNo { get; set; }
        public string SourceCode { get; set; }
        public int SourceNo { get; set; }
        public string TaxCert { get; set; }
        public string PickSlipReqd { get; set; }
        public double Discount { get; set; }
        public string PriceList { get; set; }
        public string RunNo { get; set; }
        public string CreditPolicy { get; set; }
        public string SpecialInst { get; set; }
        public string CarrierCode { get; set; }
        public string Freight { get; set; }
        public string RepCode { get; set; }
        public string ForCur { get; set; }
        public double ForRate { get; set; }
        public string FreightType { get; set; }
        public DateTime EnteredTime { get; set; }
        public string EdiFlag { get; set; }
        public string ImageNo { get; set; }
        public string Status { get; set; }
        public int Version { get; set; }
        public int CbdFlag { get; set; }
        public string CustFrom { get; set; }
        public int TransportChilled { get; set; }
    }
}
