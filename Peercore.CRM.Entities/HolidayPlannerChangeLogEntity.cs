﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class HolidayPlannerChangeLogEntity:BaseEntity
    {
        private HolidayPlannerChangeLogEntity()
        {
            
        }
        public HolidayPlannerChangeLogEntity(int entityId)
        {
            this.HolidayPlannerChangeLogId = entityId;
        }

        public static HolidayPlannerChangeLogEntity CreateObject()
        {
            HolidayPlannerChangeLogEntity entity = new HolidayPlannerChangeLogEntity();
            return entity;
        }

        public static HolidayPlannerChangeLogEntity CreateObject(int entityId)
        {
            HolidayPlannerChangeLogEntity entity = new HolidayPlannerChangeLogEntity(entityId);
            return entity;
        }

        public int HolidayPlannerChangeLogId { get; set; }
        public int KeyId { get; set; }
        public string FormType { get; set; }
 
        public DateTime ChangedFromDate { get; set; }
        public DateTime ChangedToDate { get; set; }
        public string Comment { get; set; }
        public string Status { get; set; }
    }
}
