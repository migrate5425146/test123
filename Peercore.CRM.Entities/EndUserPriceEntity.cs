﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class EndUserPriceEntity:BaseEntity
    {
        public EndUserPriceEntity()
        {
            
        }

        public EndUserPriceEntity(string entityId)
        {
            this.EndUserCode = entityId;
        }

        public static EndUserPriceEntity CreateObject()
        {
            EndUserPriceEntity entity = new EndUserPriceEntity();

            return entity;
        }

        public static EndUserPriceEntity CreateObject(string entityId)
        {
            EndUserPriceEntity entity = new EndUserPriceEntity(entityId);

            return entity;
        }

        private string catlogCode;
        public string CatlogCode
        {
            get { return catlogCode; }
            set { catlogCode = value; }
        }

        private string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        private string custCode;
        public string CustCode
        {
            get { return custCode; }
            set { custCode = value; }
        }

        private string endUserCode;
        public string EndUserCode
        {
            get { return endUserCode; }
            set { endUserCode = value; }
        }

        private DateTime effectiveDate;
        public DateTime EffectiveDate
        {
            get { return effectiveDate; }
            set { effectiveDate = value; }
        }

        private DateTime lastUpdated;
        public DateTime LastUpdated
        {
            get { return lastUpdated; }
            set { lastUpdated = value; }
        }

        private string originator;
        public string Originator
        {
            get { return originator; }
            set { originator = value; }
        }

        private double price;
        public double Price
        {
            get { return price; }
            set { price = value; }
        }

        private int version;
        public int Version
        {
            get { return version; }
            set { version = value; }
        }

        private string customerName;
        public string CustomerName
        {
            get { return customerName; }
            set { customerName = value; }
        }

        private string endUserName;
        public string EndUserName
        {
            get { return endUserName; }
            set { endUserName = value; }
        }

        private string effectiveDateString;
        public string EffectiveDateString
        {
            get { return effectiveDateString; }
            set { effectiveDateString = value; }
        }

        private string lastUpdatedString;
        public string LastUpdatedString
        {
            get { return lastUpdatedString; }
            set { lastUpdatedString = value; }
        }
    }
}
