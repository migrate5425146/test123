﻿
namespace Peercore.CRM.Entities
{
    public class AnalysisGraphEntity : BaseEntity
    {
        public AnalysisGraphEntity() { }
        
        public string AnalyseFrom { get; set; }
        public double NoOfLeadCustomers { get; set; }
        public double SumOfLeadCustomers { get; set; }
    }
}
