﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class SalesDataEntity:BaseEntity
    {
        public SalesDataEntity()
        {            
        }

        public static SalesDataEntity CreateObject()
        {
            SalesDataEntity entity = new SalesDataEntity();
            return entity;
        }

        public int CostYear { get; set; }
        public int CostPeriod { get; set; }
        public double TradeDays { get; set; }
        public double TradeWeeks { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
        public string Originator { get; set; }
        public string Status { get; set; }
        public string CompanyCode { get; set; }
        public string MonthName { get; set; }
        public string MonthAbbri { get; set; }
        public int Quarter { get; set; }
        public int AssetCostYear { get; set; }
        public int AssetCostPeriod { get; set; }
        public double sales { get; set; }
        public double discount { get; set; }

    }
}
