﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class PipelineStageEntity : BaseEntity
    {
        private PipelineStageEntity()
        {
            this.IsNew = true;
        }

        private PipelineStageEntity(int entityId)
        {
            this.PipelineStageID = entityId;
        }

        public static PipelineStageEntity CreateObject()
        {
            PipelineStageEntity entity = new PipelineStageEntity();
            return entity;
        }

        public static PipelineStageEntity CreateObject(int entityId)
        {
            PipelineStageEntity entity = new PipelineStageEntity(entityId);
            return entity;
        }

        int? iPipelineStageID;
        string sPipelineStage = string.Empty;
        int iOrder;

        #region Public Properties

        public int? PipelineStageID
        {
            get { return iPipelineStageID; }
            set { iPipelineStageID = value; }
        }

        public string PipelineStageName
        {
            get { return sPipelineStage; }
            set { sPipelineStage = value; }
        }

        public int Order
        {
            get { return iOrder; }
            set { iOrder = value; }
        }

        #endregion

        #region Pipeline Chart
        private double totalAmount;
        public double TotalAmount
        {
            get { return totalAmount; }
            set { totalAmount = value; }
        }

        private double totalUnits;
        public double TotalUnits
        {
            get { return totalUnits; }
            set { totalUnits = value; }
        }
        #endregion
    }
}
