﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class SalesLeaderEntity : BaseEntity
    {
        private SalesLeaderEntity()
        {
            this.IsNew = true;
        }

        private SalesLeaderEntity(int entityId)
        {
            //this.OpportunityID = entityId;
        }

        public static SalesLeaderEntity CreateObject()
        {
            SalesLeaderEntity entity = new SalesLeaderEntity();
            return entity;
        }

        public static SalesLeaderEntity CreateObject(int entityId)
        {
            SalesLeaderEntity entity = new SalesLeaderEntity(entityId);
            return entity;
        }

        public string OwnerName { get; set; }

        public double Amount { get; set; }

        public double Units { get; set; }

        public string Data { get; set; }
    }
}
