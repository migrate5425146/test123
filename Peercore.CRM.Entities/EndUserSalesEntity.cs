﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class EndUserSalesEntity:BaseEntity
    {
        private EndUserSalesEntity()
        {
            this.IsNew = true;
        }

       /* private EndUserSalesEntity(int entityId)
        {
            this.ActivityID = entityId;
        }
        */

        public static EndUserSalesEntity CreateObject()
        {
            EndUserSalesEntity entity = new EndUserSalesEntity();

            return entity;
        }

      /*  public static EndUserSalesEntity CreateObject(int entityId)
        {
            EndUserSalesEntity entity = new EndUserSalesEntity(entityId);

            return entity;
        }*/


        public string Market { get; set; }
        public string Description { get; set; }
        public int Year { get; set; }
        public int Period { get; set; }
        public int Month { get; set; }
        public double Cases { get; set; }

        public double Tonnes1 { get; set; }
        public double Tonnes2 { get; set; }
        public double Tonnes3 { get; set; }

        public decimal Dollar1 { get; set; }
        public decimal Dollar2 { get; set; }
        public decimal Dollar3 { get; set; }

        public int GracePeriod { get; set; }
        public DateTime Documentdate { get; set; }
        public decimal DueAmount { get; set; }
        public int TransType { get; set; }
        public string Status { get; set; }
        public decimal Discount { get; set; }

        public double TotalSales { get; set; }
        public double SalesSummary { get; set; }

        public string XLabel_FirstMonth { get; set; }
        public string XLabel_SecondMonth { get; set; }
        public string XLabel_ThirdMonth { get; set; }
    }
}
