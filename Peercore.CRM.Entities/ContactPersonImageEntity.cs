﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ContactPersonImageEntity : BaseEntity
    {
        private ContactPersonImageEntity()
        {
            this.IsNew = true;
        }

        private ContactPersonImageEntity(int entityId)
        {
            this.ContactPersonImageId = entityId;
        }

        public static ContactPersonImageEntity CreateObject()
        {
            ContactPersonImageEntity entity = new ContactPersonImageEntity();
            return entity;
        }

        public static ContactPersonImageEntity CreateObject(int entityId)
        {
            ContactPersonImageEntity entity = new ContactPersonImageEntity(entityId);
            return entity;
        }

        public int ContactPersonImageId { get; set; }

        public int ContactPersonId { get; set; }

        public int ContactNo { get; set; }

        public string FileName { get; set; }
        private byte[] fileContent;
        public byte[] FileContent
        {
            get { return fileContent; }
            set
            {
                fileContent = value;
                IsDirty = true;
            }
        }

        public int LeadID { get; set; }

        public string CustomerCode { get; set; }

 
    }
}
