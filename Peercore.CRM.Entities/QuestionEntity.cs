﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class QuestionEntity
    {
        public QuestionEntity()
        {
        }

        public static QuestionEntity CreateObject()
        {
            QuestionEntity entity = new QuestionEntity();
            return entity;
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Quesion { get; set; }
        
    }
}
