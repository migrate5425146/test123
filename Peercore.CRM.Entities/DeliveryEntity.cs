﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class DeliveryEntity:BaseEntity
    {
        private DeliveryEntity()
        {
            this.IsNew = true;
        }

        private DeliveryEntity(int entityId)
        {
        }

        public static DeliveryEntity CreateObject()
        {
            DeliveryEntity entity = new DeliveryEntity();

            return entity;
        }

        public static DeliveryEntity CreateObject(int entityId)
        {
            DeliveryEntity entity = new DeliveryEntity(entityId);

            return entity;
        }

        public string Status { get; set; }
        public int DelCount { get; set; }
    }
}
