﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class InvoiceSettlementEntity:BaseEntity
    {
        public InvoiceSettlementEntity()
        {
        }

        public InvoiceSettlementEntity(int entityId)
        {
        }

        public static InvoiceSettlementEntity CreateObject()
        {
            InvoiceSettlementEntity entity = new InvoiceSettlementEntity();

            return entity;
        }

        public static InvoiceSettlementEntity CreateObject(int entityId)
        {
            InvoiceSettlementEntity entity = new InvoiceSettlementEntity(entityId);

            return entity;
        }

        public int InvoiceSettlementId { get; set; }
        public string SettlementNo { get; set; }
        public string AddedOn { get; set; }
        public double CashAmount { get; set; }
        public double ChequeAmount { get; set; }
        public string ChequeNo { get; set; }
        public string ChequeBank { get; set; }
        public string ChequeDate { get; set; }
        public string CustCode { get; set; }
        public int InvoiceId { get; set; }
        public int SettlementTypeId { get; set; }

        public List<InvoiceSettlementChequeEntity> InvoiceSettlementChequeList { get; set; }
        public string InvoiceNo { get; set; }

        public double CreditAmount { get; set; } 

        public string DueDate { get; set; }

        //Add Date : 2016/6/24
        public int Id { get; set; }
        public string status_pay { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string remarks { get; set; }
        public string credit_settle_no { get; set; }
        public string IsCancel { get; set; }
        public int routeId { get; set; }
        public string routeName { get; set; }
        public string RepCode { get; set; }

        public List<InvoiceSettlementDetailsEntity> InvoiceSettlementDetailsList { get; set; }
    }

    public class InvoiceSettlementDetailsEntity : BaseEntity
    {
        public InvoiceSettlementDetailsEntity()
        {
        }

        public InvoiceSettlementDetailsEntity(int entityId)
        {
        }

        public static InvoiceSettlementDetailsEntity CreateObject()
        {
            InvoiceSettlementDetailsEntity entity = new InvoiceSettlementDetailsEntity();
            return entity;
        }

        public static InvoiceSettlementDetailsEntity CreateObject(int entityId)
        {
            InvoiceSettlementDetailsEntity entity = new InvoiceSettlementDetailsEntity(entityId);
            return entity;
        }

        public int Id { get; set; }
        public int CreditSettlementId { get; set; }
        public string InvoiceNo { get; set; }
        public int InvoiceId { get; set; }
        public double OutstandAmount { get; set; }
        public double PayAmount { get; set; }
    }
}
