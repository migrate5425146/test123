﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class PricelistTermEntity : BaseEntity
    {
        private PricelistTermEntity()
        {
            this.IsNew = true;
        }

        private PricelistTermEntity(int entityId)
        {
            this.PricelistTermId = entityId;
        }

        public static PricelistTermEntity CreateObject()
        {
            PricelistTermEntity entity = new PricelistTermEntity();
            return entity;
        }

        public static PricelistTermEntity CreateObject(int entityId)
        {
            PricelistTermEntity entity = new PricelistTermEntity(entityId);
            return entity;
        }

        private string pricelistName;
        private string term;
        private int? pricelistTermId;

        public string PricelistName 
        {
            get { return pricelistName; }
            set
            {
                pricelistName = value;
                IsDirty = true;
            }
        }

        public string Term 
        {
            get { return term; }
            set
            {
                term = value;
                IsDirty = true;
            }
        }

        public int? PricelistTermId 
        {
            get { return pricelistTermId; }
            set
            {
                pricelistTermId = value;
            }
        }

        public double Amount { get; set; }
    }
}
