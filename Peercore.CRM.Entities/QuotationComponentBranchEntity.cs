﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class QuotationComponentBranchEntity : BaseEntity
    {
        private QuotationComponentBranchEntity()
        {
            this.IsNew = true;
        }

        private QuotationComponentBranchEntity(int entityId)
        {
            this.ComponentBranchId = entityId;
        }

        public static QuotationComponentBranchEntity CreateObject()
        {
            QuotationComponentBranchEntity entity = new QuotationComponentBranchEntity();
            return entity;
        }

        public static QuotationComponentBranchEntity CreateObject(int entityId)
        {
            QuotationComponentBranchEntity entity = new QuotationComponentBranchEntity(entityId);
            return entity;
        }

        private int? componentAmountId;
        private int? quotationComponentId;
        private decimal amount;
        private string branchFrom;
        private string branchTo;
        private List<QuotationParameterEntity> quotationParameterList;

        public string AreaTo
        {
            get { return branchTo; }
            set
            {
                branchTo = value;
                IsDirty = true;
            }
        }

        public string AreaFrom
        {
            get { return branchFrom; }
            set
            {
                branchFrom = value;
                IsDirty = true;
            }
        }

        public decimal Amount
        {
            get { return amount; }
            set
            {
                amount = value;
                IsDirty = true;
            }
        }

        public int? QuotationComponentId
        {
            get { return quotationComponentId; }
            set
            {
                quotationComponentId = value;
                IsDirty = true;
            }
        }

        public int? ComponentBranchId
        {
            get { return componentAmountId; }
            set
            {
                componentAmountId = value;
                IsDirty = true;
            }
        }

        public List<QuotationParameterEntity> QuotationParameterList
        {
            get
            {
                if (quotationParameterList == null)
                    quotationParameterList = new List<QuotationParameterEntity>();

                return quotationParameterList;
            }
            set
            {
                quotationParameterList = value;
                IsDirty = true;
            }
        }

    }
}
