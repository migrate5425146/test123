﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class SalesTrendEntity : BaseEntity
    {
        private SalesTrendEntity()
        {
            this.IsNew = true;
        }

        private SalesTrendEntity(int entityId)
        {
            //this.OpportunityID = entityId;
        }

        public static SalesTrendEntity CreateObject()
        {
            SalesTrendEntity entity = new SalesTrendEntity();
            return entity;
        }

        public static SalesTrendEntity CreateObject(int entityId)
        {
            SalesTrendEntity entity = new SalesTrendEntity(entityId);
            return entity;
        }

        public List<KeyValuePair<string, double>> TrendData { get; set; }

        public string Channel { get; set; }

        public string ChannelDescription { get; set; }

        public double TotalAmount { get; set; }

        public string Period { get; set; }

        public double Sales_Vol_P { get; set; }
        public double Sales_Vol_PP { get; set; }

        public double Sales_Val_P { get; set; }
        public double Sales_Val_PP { get; set; }

        public double Sales_Val_Tonval_P { get; set; }
        public double Sales_Val_Tonval_PP { get; set; }

        public double Sales_Profit_Tonval_P { get; set; }
        public double Sales_Profit_Tonval_PP { get; set; }

        public double Sales_Profit_Perc_P { get; set; }
        public double Sales_Profit_Perc_PP { get; set; }

        public double Sales_Profit_P { get; set; }
        public double Sales_Profit_PP { get; set; }

        public double Budget_Val_P { get; set; }
        public double Budget_Val_PP { get; set; }

        public double Budget_Vol_PP { get; set; }
        public double Budget_Vol_P { get; set; }
    }
}
