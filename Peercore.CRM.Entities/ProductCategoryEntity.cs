﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ProductCategoryEntity : BaseEntity
    {
                public ProductCategoryEntity()
        {
            this.IsNew = true;
        }

        public ProductCategoryEntity(int entityId)
        {
            this.Id = entityId;
        }

        public static ProductCategoryEntity CreateObject()
        {
            ProductCategoryEntity entity = new ProductCategoryEntity();
            return entity;
        }

        public static ProductCategoryEntity CreateObject(int entityId)
        {
            ProductCategoryEntity entity = new ProductCategoryEntity(entityId);
            return entity;
        }

        public int Id { get; set; }
        public string Code { get; set; }
        public string Description { get; set; }

    }
}
