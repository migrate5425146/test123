﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class TraderCustomerEntity : BaseEntity
    {
        public TraderCustomerEntity()
        {
        }

        public TraderCustomerEntity(int entityId)
        {
        }

        public static TraderCustomerEntity CreateObject()
        {
            TraderCustomerEntity entity = new TraderCustomerEntity();

            return entity;
        }

        public static TraderCustomerEntity CreateObject(int entityId)
        {
            TraderCustomerEntity entity = new TraderCustomerEntity(entityId);
            return entity;
        }

        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string Address1 { get; set; }
        public string PhoneNo { get; set; }
        public double CustomerLongitude { get; set; }
        public double CustomerLatitude { get; set; }
        public bool Istlp { get; set; }
        public int PheriheryId { get; set; }
        public int OutletTypeId { get; set; }
        public int VolumeId { get; set; }
        public int MarketId { get; set; }
        public int CatrgoryId { get; set; }

        //public string Pherihery { get; set; }
        //public string OutletType { get; set; }
        public string CustomerVolume { get; set; }

        public string MarketName { get; set; }
        public string ContactPersonName { get; set; }
        public string PheripheryCode { get; set; }
        //public string Address2 { get; set; }
        public string Town { get; set; }
        public string Status { get; set; }
        public string Remarks { get; set; }
        public string CDR { get; set; }
        public string Display { get; set; }

        public List<ProductEntity> ProductList { get; set; }
        public List<MerchandiseEntity> MerchadiseList { get; set; }
        public List<ObjectiveEntity> ObjectiveList { get; set; }
        public string RDF { get; set; }
      
        public List<PromotionEntity> Promotions { get; set; }
    //    public string Rep { get; set; }

        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string Province { get; set; }
        public string GeoLocation { get; set; }
        public string PeripheryCode { get; set; }
        public string ProductsHandled { get; set; }
        public string CanvassingFrequency { get; set; }
        public string AsuShare { get; set; }
        public string AuthorityAssessment { get; set; }
        public string VisitReason { get; set; }
        public string SpentTime { get; set; }

        public double OutstandingBalance { get; set; }
        public double CreditLimit { get; set; }
        public double CashLimit { get; set; }
    }
}
