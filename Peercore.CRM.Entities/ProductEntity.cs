﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ProductEntity : BaseEntity
    {
        public ProductEntity()
        {
            this.IsNew = true;
        }

        public ProductEntity(int entityId)
        {
            this.ProductId = entityId;
        }

        public static ProductEntity CreateObject()
        {
            ProductEntity entity = new ProductEntity();
            return entity;
        }

        public static ProductEntity CreateObject(int entityId)
        {
            ProductEntity entity = new ProductEntity(entityId);
            return entity;
        }

        public int ProductId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }

        //public string CreatedBy { get; set; }
        //public DateTime CreatedDate { get; set; }
        //public string LastModifiedBy { get; set; }
        //public DateTime LastModifiedDate { get; set; }

        public string IsAvailable { get; set; }
        public string CustomerCode { get; set; }
        public string ProductStatus { get; set; }
        public double RetailPrice { get; set; }
        public double OutletPrice { get; set; }
        public int VanStock { get; set; }
        public double MaxDiscount { get; set; }
        public int Sku { get; set; }
        public int BrandId { get; set; }
        public int CategoryId { get; set; }

        public string BrandName { get; set; }
        public string CategoryName { get; set; }

        public int ProductPriceId { get; set; }
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }

        public double Weight { get; set; }
        public bool IsHighValue { get; set; }
        public int FlavourId { get; set; }
        public int PackingId { get; set; }

        public byte[] ImageContent { get; set; }
        public double DistributorPrice { get; set; }

        public string FgCode { get; set; }
        public bool IsPromotion { get; set; }
        public int Qty { get; set; }

        public float CaseConfiguration { get; set; }

        public bool IsMTItem { get; set; }
        public bool IsHalfQtyItem { get; set; }

        public float TonnageConfiguration { get; set; } //Added By Rangan

    }
}
