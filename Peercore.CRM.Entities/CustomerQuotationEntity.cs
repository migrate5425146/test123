﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class CustomerQuotationEntity:BaseEntity
    {
        private CustomerQuotationEntity()
        {
            this.IsNew = true;
        }

        private CustomerQuotationEntity(int entityId)
        {
            this.QuotationID = entityId;
        }

        public static CustomerQuotationEntity CreateObject()
        {
            CustomerQuotationEntity entity = new CustomerQuotationEntity();

            return entity;
        }

        public static CustomerQuotationEntity CreateObject(int entityId)
        {
            CustomerQuotationEntity entity = new CustomerQuotationEntity(entityId);

            return entity;
        }

        public int QuotationID { get; set; }
        public int ContactPersonID { get; set; }
        public int LeadID { get; set; }
        public string ContainerLoad { get; set; }
        public string FreightType { get; set; }
        public int PricelistID { get; set; }
        public string PricelistName { get; set; }

        // Deliver To
        public string DelCustomer { get; set; }
        public string DelAddress { get; set; }
        public string DelCity { get; set; }
        public string DelState { get; set; }
        public string DelPostcode { get; set; }

        //From
        public string FromName { get; set; }
        public string FromAddress { get; set; }
        public string FromCity { get; set; }
        public string FromState { get; set; }
        public string FromPostcode { get; set; }

        // Lead Details
        public string LeadName { get; set; } //Default LeadName
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostCode { get; set; }

        // Contact Person Details
        public string FirstName { get; set; }
        public string Telephone { get; set; }
        public string Mobile { get; set; }
        public string EmailAddress { get; set; }
        public string MailingAddress { get; set; }
        public string MailingCity { get; set; }
        public string MailingState { get; set; }
        public string MailingPostcode { get; set; }
        public int TemplateId { get; set; }
        public string CustomerCode { get; set; }
    }
}
