﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class MobileObjectiveEntity:BaseEntity
    {
        public MobileObjectiveEntity()
        {
            this.IsNew = true;
        }

        public static MobileObjectiveEntity CreateObject()
        {
            MobileObjectiveEntity entity = new MobileObjectiveEntity();
            return entity;
        }

        private string objectiveCode;
        public string ObjectiveCode
        {
            get { return objectiveCode; }
            set { objectiveCode = value; }
        }

        private string description;
        public string Description
        {
            get { return description; }
            set { description = value; }
        }

        private string customerCode;
        public string CustomerCode
        {
            get { return customerCode; }
            set { customerCode = value; }
        }

        private int objectiveId;
        public int ObjectiveId
        {
            get { return objectiveId; }
            set { objectiveId = value; }
        }
    }
}
