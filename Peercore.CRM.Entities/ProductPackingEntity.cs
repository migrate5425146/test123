﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class ProductPackingEntity : BaseEntity
    {
        private ProductPackingEntity()
        {
            this.IsNew = true;
        }

        private ProductPackingEntity(int entityId)
        {
            this.PackingId = entityId;
        }

        public static ProductPackingEntity CreateObject()
        {
            ProductPackingEntity entity = new ProductPackingEntity();

            return entity;
        }

        public static ProductPackingEntity CreateObject(int entityId)
        {
            ProductPackingEntity entity = new ProductPackingEntity(entityId);

            return entity;
        }

        public int PackingId { get; set; }
        public string PackingName { get; set; }
    }
}
