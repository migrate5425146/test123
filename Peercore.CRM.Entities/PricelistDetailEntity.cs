﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class PricelistDetailEntity : BaseEntity
    {
        private PricelistDetailEntity()
        {
            this.IsNew = true;
        }

        private PricelistDetailEntity(int entityId)
        {
            this.PricelistDetailId = entityId;
        }

        public static PricelistDetailEntity CreateObject()
        {
            PricelistDetailEntity entity = new PricelistDetailEntity();
            return entity;
        }

        public static PricelistDetailEntity CreateObject(int entityId)
        {
            PricelistDetailEntity entity = new PricelistDetailEntity(entityId);
            return entity;
        }

        private int? pricelistDetailId;
        private int? pricelistId;
        private string service;
        private string areaFrom;
        private string areaTo;
        private string uom;

        private List<PricelistSlabEntity> pricelistSlabList;

        public int? PricelistDetailId 
        {
            get { return pricelistDetailId; }
            set
            {
                pricelistDetailId = value;
                IsDirty = true;
            }
        }
        public int? PricelistId 
        {
            get { return pricelistId; }
            set
            {
                pricelistId = value;
                IsDirty = true;
            }
        }
        public string Service 
        {
            get { return service; }
            set
            {
                service = value;
                IsDirty = true;
            }
        }
        public string AreaFrom 
        {
            get { return areaFrom; }
            set
            {
                areaFrom = value;
                IsDirty = true;
            }
        }
        public string AreaTo 
        {
            get { return areaTo; }
            set
            {
                areaTo = value;
                IsDirty = true;
            }
        }
        public string UOM 
        {
            get { return uom; }
            set
            {
                uom = value;
                IsDirty = true;
            }
        }
        public List<PricelistSlabEntity> PricelistSlabList 
        {
            get
            {
                if (pricelistSlabList == null)
                    pricelistSlabList = new List<PricelistSlabEntity>();

                return pricelistSlabList;
            }
            set
            {
                pricelistSlabList = value;
            }
        }

        public int PlistNo { get; set; }
        public int ItemNo { get; set; }
        public string CatlogCode { get; set; }
        public int ManifestNo { get; set; }
        public int IvceNo { get; set; }
        public string WarehouseId { get; set; }
        public double ReqdQty { get; set; }
        public double PickedQty { get; set; }
        public int PickedFullQty { get; set; }
        public double PalletQty { get; set; }
        public double BoQty { get; set; }
        public double DiscPercent { get; set; }
        public DateTime DateRequired { get; set; }
        public DateTime NewDateRequired { get; set; }
        public DateTime DatePicked { get; set; }
        public string AnalysisCode { get; set; }
        public string Status { get; set; }
        public int Sequence { get; set; }
        public string Promised { get; set; }

    }
}
