﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class MessagesEntity : BaseEntity
    {
        private MessagesEntity()
        {
            this.IsNew = true;
        }

        private MessagesEntity(int entityId)
        {
            this.MessageID = entityId;
        }

        public static MessagesEntity CreateObject()
        {
            MessagesEntity entity = new MessagesEntity();
            return entity;
        }

        public static MessagesEntity CreateObject(int entityId)
        {
            MessagesEntity entity = new MessagesEntity(entityId);
            return entity;
        }

        public int MessageID { get; set; }
        public string Message { get; set; }
        public string Originator { get; set; }
        public DateTime MessageDate { get; set; }
        public bool IsNew { get; set; }

        private DateTime expiryDate;
        public DateTime ExpiryDate
        {
            get { return expiryDate; }
            set
            {
                expiryDate = value; 
                if (DateTime.Today > expiryDate)
                    IsExpired = true;
            }
        }

        public bool IsExpired { get; set; }

        private string delFlag;
        public string DelFlag
        {
            get
            {
                return delFlag;
            }
            set
            {
                if (delFlag == value)
                    return;

                delFlag = value;
            }
        }
    }
}
