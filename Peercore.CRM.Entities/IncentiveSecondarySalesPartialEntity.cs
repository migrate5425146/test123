﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Entities
{
    public class IncentiveSecondarySalesPartialEntity:BaseEntity
    {
        public IncentiveSecondarySalesPartialEntity()
        {
            this.IsNew = true;
        }

        public IncentiveSecondarySalesPartialEntity(int entityId)
        {
            this.SecondarySalesPartialId = entityId;
        }

        public static IncentiveSecondarySalesPartialEntity CreateObject()
        {
            IncentiveSecondarySalesPartialEntity entity = new IncentiveSecondarySalesPartialEntity();
            return entity;
        }

        public static IncentiveSecondarySalesPartialEntity CreateObject(int entityId)
        {
            IncentiveSecondarySalesPartialEntity entity = new IncentiveSecondarySalesPartialEntity(entityId);
            return entity;
        }

        public int SecondarySalesPartialId { get; set; }
        public int IncentivePlanDetailsId { get; set; }
        public double AchievementPerc { get; set; }
        public double IncentivePerc { get; set; }
        public bool IsActive { get; set; }
    }
}
