﻿using ConnectionManager;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DataHandlers
{
    public abstract class TempleteDataHandler
    {
        protected SqlConnection SqlCon;
        protected SqlCommand SqlCmd;
        protected DataTable dataTable;

        protected TempleteDataHandler()
        {
            try
            {
                SqlCon = Connection.getConnectionString();
                SqlCmd = SqlCon.CreateCommand();
                dataTable = new DataTable();
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (SqlCon.State != ConnectionState.Closed)
                {
                    SqlCon.Close();
                }
            }

        }


    }
}
