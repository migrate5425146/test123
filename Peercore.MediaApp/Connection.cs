﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace ConnectionManager
{
    public class Connection
    {
        private static SqlConnection SqlCon;

        static Connection()
        {
            Connection.SqlCon = null;
        }

        public static SqlConnection getConnectionString()
        {
            try
            {
                if (SqlCon == null || SqlCon.State == ConnectionState.Closed)
                {
                    SqlCon = null;
                    SqlCon = new SqlConnection();

                    string strConnString = ConfigurationManager.ConnectionStrings["DBConnect"].ConnectionString;

                    SqlCon.ConnectionString = strConnString;
                }

                return SqlCon;
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
