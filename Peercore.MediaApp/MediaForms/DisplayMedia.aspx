﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DisplayMedia.aspx.cs" Inherits="MediaApp.MediaForms.DisplayMedia" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.22/css/dataTables.bootstrap.min.css" />
    <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.22/js/dataTables.bootstrap.min.js"></script>



    <%--    <link href="../Scripts/css/bootstrap.min.css" rel="stylesheet" />
    <link href="../Scripts/css/dataTables.bootstrap.min.css" rel="stylesheet" />

    <script src="../Scripts/js/dataTables.bootstrap.min.js"></script>
    <script src="../Scripts/js/jquery-3.5.1.js"></script>
    <script src="../Scripts/js/jquery.dataTables.min.js"></script>--%>

    <style type="text/css">
        .styled-select {
            width: 200px;
            height: 30px;
            overflow: hidden;
            border: 2px solid;
            border-radius: 3px;
        }

            .styled-select select {
                background: transparent;
                width: 180px;
                padding: 3px;
                font-size: 16px;
                line-height: 1;
                border: 0;
                border-radius: 0;
                height: 30px;
                -webkit-appearance: none;
                font-family: Andalus;
                /*color: #7d6754;*/
            }
    </style>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
    <nav class="navbar navbar-default navbar-fixed-top">
        <div class="container">
            <div class="navbar-header">
                <a class="navbar-brand" href="#myPage">Media Galary</a>
            </div>

        </div>
    </nav>
    <div class="container table-responsive" style="margin-top: 5em;">
        <form id="form1" runat="server">

            <div class="table-responsive">

                <div style="margin-top: 10px">
                    <asp:DropDownList ID="ddlCategory" class="styled-select" Style="margin-left: 10px; margin-top: 10px;" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCategory_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:Literal ID="ltTable" runat="server" />
                </div>
            </div>
        </form>
    </div>


    <script>
        function searchFunction() {
            var input, filter, table, tr, td, i, txtValue;
            input = document.getElementById("mediaSearch");
            filter = input.value.toUpperCase();
            table = document.getElementById("mediaGrid");
            tr = table.getElementsByTagName("tr");
            for (i = 0; i < tr.length; i++) {
                td = tr[i].getElementsByTagName("td")[1];
                if (td) {
                    txtValue = td.textContent || td.innerText;
                    if (txtValue.toUpperCase().indexOf(filter) > -1) {
                        tr[i].style.display = "";
                    } else {
                        tr[i].style.display = "none";
                    }
                }
            }
        }
    </script>

    <script>
        $(document).ready(function () {
            $('#mediaGrid').DataTable({
                "paging": true,
                "pagingType": "simple",
                "bLengthChange": false,
                "pageLength": 10
            });
            $('.dataTables_length').addClass('bs-select');
        });
    </script>


</body>
</html>
