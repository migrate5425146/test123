﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UploadMedia.aspx.cs" Inherits="MediaApp.MediaForms.UploadMedia" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <table>
                <tr>
                    <td>Rep : </td>
                    <td>
                        <asp:DropDownList ID="ddlRepList" runat="server"></asp:DropDownList></td>
                </tr>
                <tr>
                    <td style="width: 200px; text-align: right">Upload : </td>
                    <td>
                        <asp:FileUpload ID="FileUploader" runat="server" /></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td>Description :</td>
                    <td>
                        <asp:TextBox ID="txtDescription" runat="server" Height="75px" Width="334px" TextMode="MultiLine"></asp:TextBox></td>
                </tr>
                <tr>
                    <td></td>
                    <td>
                        <asp:Button ID="btnSave" runat="server" Text="Save" Width="100px" OnClick="btnSave_Click" />
                        <asp:Button ID="btnClear" runat="server" Text="Clear" Width="100px" OnClick="btnClear_Click" />
                    </td>
                </tr>
            </table>
        </div>

        <asp:GridView ID="grdImg" runat="server" AutoGenerateColumns="false">
            <Columns>
                <asp:BoundField HeaderText="Image Name" DataField="MediaFileName" />
                <asp:BoundField HeaderText="Description" DataField="Description" />
                <%--<asp:ImageField HeaderText="Image" DataImageUrlField="ThumbnailPath" NullDisplayText="No image on file." ControlStyle-Height="70" ControlStyle-Width="60"></asp:ImageField>
                --%>
                <asp:TemplateField HeaderText="Image">
                    <ItemTemplate>
                        <asp:Image ID="Image1" runat="server" ImageUrl='<%# Eval("MediaPath") %>' Height="150px" Width="150px" />
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField HeaderText="Created Date" DataField="CreatedDate" />
                <asp:BoundField HeaderText="Created By" DataField="CreatedBy" />

            </Columns>
        </asp:GridView>
        <div id="dialog" style="display: none"></div>
    </form>
</body>
</html>
