﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;

namespace DataHandlers.MediaDataHandler
{
    public class RepDataHandler: TempleteDataHandler
    {
        public DataTable GetRepList()
        {
            try
            {
                if (SqlCon.State == ConnectionState.Closed)
                {
                    SqlCon.Open();
                }
                string Qry = @"SELECT rep_code,
                                        name 
                              FROM rep;";
                SqlCmd.CommandText = Qry;
                SqlDataAdapter dataAdapter = new SqlDataAdapter(SqlCmd);
                dataAdapter.Fill(dataTable);
                return dataTable;
            }
            finally
            {
                SqlCmd.Parameters.Clear();
                if (SqlCon.State == ConnectionState.Open)
                {
                    SqlCon.Close();
                }
            }
        }
    }
}
