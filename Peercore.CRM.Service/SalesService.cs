﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.ServiceContracts;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.Common.Adapter;

namespace Peercore.CRM.Service
{
    public partial class CRMService : ISales
    {
        SalesAdapter SalesAdapter = null;

        public List<EndUserSalesDTO> GetCostYears()
        {
            SalesAdapter = new SalesAdapter();

            try
            {
                return SalesAdapter.GetCostYears();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public EndUserSalesDTO GetCurrentCostPeriod()
        {
            SalesAdapter = new SalesAdapter();

            try
            {
                return SalesAdapter.GetCurrentCostPeriod();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public EndUserSalesDTO GetCurrentCostPeriodForDateTime(string dtDate)
        {
            SalesAdapter = new SalesAdapter();

            try
            {
                return SalesAdapter.GetCurrentCostPeriodForDateTime(dtDate);
            }
            catch (Exception)
            {

                throw;
            }
        }

        //public EndUserSalesDTO GetCostPeriod(string dtDate)
        //{
        //    SalesAdapter = new SalesAdapter();

        //    try
        //    {
        //        return SalesAdapter.GetCurrentCostPeriodForDateTime(dtDate);
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        public List<EndUserSalesDTO> GetDebtorsDetails(string custCode)
        {
            SalesAdapter = new SalesAdapter();

            try
            {
                return SalesAdapter.GetDebtorsDetails(custCode);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetMonthForCostPeriod(int costYear, int costPeriod)
        {
            SalesAdapter = new SalesAdapter();

            try
            {
                return SalesAdapter.GetMonthForCostPeriod( costYear, costPeriod);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public EndUserSalesDTO GetSalesData(string custCode, string date)
        {
            SalesAdapter = new SalesAdapter();

            try
            {
                return SalesAdapter.GetSalesData(custCode, date);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<EndUserSalesDTO> GetCostPeriod(ArgsDTO args)
        {
            SalesAdapter = new SalesAdapter();

            try
            {
                return SalesAdapter.GetCostPeriod(args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<EOISalesDTO> GetEOISales(ArgsDTO args)
        {
            SalesAdapter = new SalesAdapter();

            try
            {
                return SalesAdapter.GetEOISales(args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public SalesInfoDetailViewStateDTO GetSalesInfoDetailViewState(SalesInfoDetailSearchCriteriaDTO busSalesEnqSrc, ArgsDTO args, string type = "grid")
        {
            SalesAdapter = new SalesAdapter();

            try
            {
                return SalesAdapter.GetSalesInfoDetailViewState(busSalesEnqSrc,args,type);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<AtblDTO> GetTrendData(SalesInfoDetailSearchCriteriaDTO busSalesEnqSrc, ref List<string> lstCostPeriods, string sCode)
        {
            SalesAdapter = new SalesAdapter();

            try
            {
                return SalesAdapter.GetTrendData(busSalesEnqSrc, ref lstCostPeriods,sCode);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public SalesInfoDetailViewStateDTO GetEndUserSalesInfoDetails(SalesInfoDetailSearchCriteriaDTO searchCriteria, ArgsDTO args, string type = "grid")
        {
            SalesAdapter = new SalesAdapter();
            try
            {
                return SalesAdapter.GetEndUserSalesInfoDetails(searchCriteria, args,type);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<string> LoadCostPeriods(int iCostYear)
        {
            SalesAdapter = new SalesAdapter();
            try
            {
                return SalesAdapter.LoadCostPeriods(iCostYear);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<BusAllDTO> GetBusAllDetails(SalesInfoDetailSearchCriteriaDTO busSalesEnqSrc,string historyType)
        {
            SalesAdapter = new SalesAdapter();
            try
            {
                return SalesAdapter.GetBusAllDetails(busSalesEnqSrc, historyType);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<BusAllYearDTO> GetBusAllYearDetails(SalesInfoDetailSearchCriteriaDTO busSalesEnqSrc, string historyType)
        {
            SalesAdapter = new SalesAdapter();
            try
            {
                return SalesAdapter.GetBusAllYearDetails(busSalesEnqSrc, historyType);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<PaymentSettlementDTO> GetCheques(string createdBy, DateTime createdDate, DateTime toDate, int distributerAccountId, string status, ArgsDTO args)
        {
            PaymentSettlementAdapter objPaymentSettlementAdapter = new PaymentSettlementAdapter();
            try
            {
                return objPaymentSettlementAdapter.GetCheques(createdBy, createdDate, toDate, distributerAccountId, status, args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SavePaymentSettlements(List<PaymentSettlementDTO> paymentSettlementsList)
        {
            PaymentSettlementAdapter objPaymentSettlementAdapter = new PaymentSettlementAdapter();
            try
            {
                return objPaymentSettlementAdapter.SavePaymentSettlements(paymentSettlementsList);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<PaymentSettlementDTO> GetDatedCheques(string createdBy, DateTime createdDate, DateTime toDate, ArgsDTO args)
        {
            PaymentSettlementAdapter objPaymentSettlementAdapter = new PaymentSettlementAdapter();
            try
            {
                return objPaymentSettlementAdapter.GetDatedCheques(createdBy, createdDate, toDate, args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int UploadSalesTargets(string FilePath, string Extension, string UserName)
        {
            SalesAdapter = new SalesAdapter();
            try
            {
                return SalesAdapter.UploadSalesTargets(FilePath, Extension, UserName);

            }
            catch (Exception)
            {
                throw;
            }

        }

    }
}
