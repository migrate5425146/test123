﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.ServiceContracts;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.Common.Adapter;

namespace Peercore.CRM.Service
{
    public partial class CRMService : ILead,ICustomer
    {
        DocumentAdapter DocumentAdapter = null;

        #region - Document -

        public bool SaveDocument(DocumentDTO documentDto)
        {
            DocumentAdapter = new DocumentAdapter();

            try
            {
                return DocumentAdapter.SaveDocument(documentDto);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<DocumentDTO> GetContactDocumentsForCustomer(ArgsDTO args, string path, string sessionID, bool issavedocument = false)
        {
            DocumentAdapter = new DocumentAdapter();

            try
            {
                return DocumentAdapter.GetContactDocumentsForCustomer(args, path, sessionID, issavedocument);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public List<DocumentDTO> GetLeadDocuments(ArgsDTO args)
        {
            DocumentAdapter = new DocumentAdapter();

            try
            {
                return DocumentAdapter.GetLeadDocuments(args);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public DocumentDTO GetDocument(int documentID)
        {
            DocumentAdapter = new DocumentAdapter();

            try
            {
                return DocumentAdapter.GetDocument(documentID);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool DeleteLeadDocument(int documentId)
        {
            DocumentAdapter = new DocumentAdapter();

            try
            {
                return DocumentAdapter.DeleteLeadDocument(documentId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<DocumentDTO> GetDocumentsForEntryPages(ArgsDTO args, string path, string sessionID, bool issavedocument = false)
        {
            DocumentAdapter = new DocumentAdapter();
            try
            {
                return DocumentAdapter.GetDocumentsForEntryPages(args, path, sessionID, issavedocument);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<DocumentDTO> GetLeadDocumentsForView(ArgsDTO args, string path, string sessionID, bool issavedocument = false)
        {
            DocumentAdapter = new DocumentAdapter();

            try
            {
                return DocumentAdapter.GetLeadDocumentsForView(args, path, sessionID, issavedocument);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region - Resource Document -

        public bool SaveResourceDocument(DocumentDTO documentDto)
        {
            DocumentAdapter = new DocumentAdapter();

            try
            {
                return DocumentAdapter.SaveResourceDocument(documentDto);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<DocumentDTO> GetResourceDocumentDetails(ArgsDTO args, string path, string sessionID, bool issavedocument = false)
        {
            DocumentAdapter = new DocumentAdapter();

            try
            {
                return DocumentAdapter.GetResourceDocumentDetails(args, path, sessionID, issavedocument);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public DocumentDTO GetResourceDocument(int documentID, string path, string sessionID, bool issavedocument = false)
        {
            DocumentAdapter = new DocumentAdapter();

            try
            {
                return DocumentAdapter.GetResourceDocument(documentID, path, sessionID, issavedocument);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool DeleteResourceDocument(int documentId)
        {
            DocumentAdapter = new DocumentAdapter();

            try
            {
                return DocumentAdapter.DeleteResourceDocument(documentId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        /*
        
        
        public DocumentDTO GetDocumentForView(int documentID, string path, string sessionID, bool issavedocument = false)
        {
            DocumentAdapter = new DocumentAdapter();

            try
            {
                return DocumentAdapter.GetDocumentForView(documentID, path,sessionID, issavedocument);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<DocumentDTO> GetContactDocumentsForView(string customerCode, ArgsDTO args, string path, string sessionID, bool issavedocument = false)
        {
            DocumentAdapter = new DocumentAdapter();

            try
            {
                return DocumentAdapter.GetCustomerDocumentForCustomer(customerCode, args, path,sessionID,issavedocument);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public DocumentDTO GeContactDocumentForView(int documentID, string path, string sessionID, bool issavedocument = false)
        {
            DocumentAdapter = new DocumentAdapter();

            try
            {
                return DocumentAdapter.GetDocumentForView(documentID, path, sessionID, issavedocument);
            }
            catch (Exception)
            {

                throw;
            }
        }

        

        

        */ 
    }
}
