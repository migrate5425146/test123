﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.ServiceContracts;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.Common.Adapter;
using System.Xml.Linq;
using System.IO;
using System.Reflection;
using Peercore.CRM.Service.DTO.TransferComponents;
//using Global.TransferComponents;
using Peercore.CRM.Service.DTO.CompositeEntities;
using System.ServiceModel;
using Peercore.CRM.Entities;
using System.Data;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.Model;
using System.Configuration;
using System.Net;
using System.Web;

namespace Peercore.CRM.Service
{
    public partial class CRMService : ICommon
    {
        private static string GenerateWhereCls(string whereCls, object[] parameters, XElement section)
        {
            string sql = section.Element("Sql").Value;
            string formattedSql = sql;
            if (!string.IsNullOrWhiteSpace(whereCls) && (parameters == null || parameters.Count() <= 0))
            {
                formattedSql = string.Format(sql, whereCls);
            }
            else if (string.IsNullOrWhiteSpace(whereCls) && parameters != null && parameters.Count() > 0)
            {
                formattedSql = string.Format(sql, parameters);
            }
            return formattedSql;
        }

        private LookupTransferComponentDTO GenerateSelect(string tableName, KeyValuePair<string, string> descriptionColumn, KeyValuePair<string, string>? codeColumn = null,
            string primaryKey = "", string whereCls = "", List<KeyValuePair<string, object>> filterList = null)
        {

            StringBuilder selectBuiler = new StringBuilder();

            try
            {
                selectBuiler.Append("SELECT DISTINCT ");

                if (!string.IsNullOrWhiteSpace(primaryKey))
                    selectBuiler.Append(primaryKey).Append(", ");

                if (codeColumn.HasValue && !string.IsNullOrWhiteSpace(codeColumn.Value.Key))
                    selectBuiler.Append(codeColumn.Value.Key).Append(", ");

                selectBuiler.Append(descriptionColumn.Key);

                selectBuiler.Append(" FROM ").Append(tableName);

                if (!string.IsNullOrWhiteSpace(whereCls))
                {
                    selectBuiler.Append(" WHERE ");
                    selectBuiler.Append(whereCls);
                }
                else if (filterList != null)
                {
                    selectBuiler.Append(" WHERE ");

                    foreach (KeyValuePair<string, object> item in filterList)
                    {
                        selectBuiler.Append(item.Key).Append(" = ").Append(item.Value);
                    }
                }

                if (codeColumn.HasValue && !string.IsNullOrWhiteSpace(codeColumn.Value.Key))
                {
                    selectBuiler.Append(" ORDER BY ").Append(codeColumn.Value.Key);
                }
                else
                {
                    selectBuiler.Append(" ORDER BY ").Append(descriptionColumn.Key);
                }

                LookupTransferComponentDTO LookupSqlInfo = new LookupTransferComponentDTO()
                {
                    Sql = selectBuiler.ToString(),
                    IdColumn = !string.IsNullOrWhiteSpace(primaryKey) ? new KeyValuePair<string, string>(primaryKey, "") : (KeyValuePair<string, string>?)null,
                    CodeColumn = codeColumn,
                    DescriptionColumn = descriptionColumn
                };

                return LookupSqlInfo;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public EmailAndAttachmentsDTO GetEmailbyId(int emailId, string directoryPath, string sessionId)
        {
            EmailAdapter lookupAdapter = new EmailAdapter();

            try
            {
                return lookupAdapter.GetEmailbyId(emailId, directoryPath, sessionId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        #region - Error -
        public int GetErrorCount(ExceptionDTO exceptionDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.GetErrorCountForCurrentDate(exceptionDTO);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool SaveError(ExceptionDTO exceptionDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.SaveErrorDetails(exceptionDTO);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region - Email Attachment -
        public List<EmailAttachmentDTO> GetEmailAttachment(int emailId, string emailAttachmentLocation, string sessionId)
        {
            CommonAdapter lookupAdapter = new CommonAdapter();

            try
            {
                return lookupAdapter.GetEmailAttachment(emailId, emailAttachmentLocation, sessionId);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region - Group Rep -
        public List<GroupRepDTO> GetGroupRepByGroup(int RepGroupId)
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();

            try
            {
                return objCommonAdapter.GetGroupRepByGroup(RepGroupId);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region - Home Page Summary -

        public List<OpenDealDTO> GetTopFiveOpportunities(string sBusiness, ArgsDTO args)
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();
            try
            {
                return objCommonAdapter.GetTopFiveOpportunities(sBusiness, args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OpportunityOwnerDTO> GetLeaderBoard(string cutoffDate, string closeDate, string businessCode, string sPeriod, ArgsDTO args)
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();
            try
            {
                return objCommonAdapter.GetLeaderBoard(cutoffDate, closeDate, businessCode, sPeriod, args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LeadCustomerOpportunityDAO> GetLeaderCustomerOpportunity(ArgsDTO args)
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();
            try
            {
                return objCommonAdapter.GetLeaderCustomerOpportunity(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LeadCustomerOpportunityDAO> GetLeaderCustomerOpportunityCount(ArgsDTO args)
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();
            try
            {
                return objCommonAdapter.GetLeaderCustomerOpportunityCount(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SalesTrendDAO> GetSalesTrend(string sBusiness)
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();
            try
            {
                return objCommonAdapter.GetSalesTrend(sBusiness);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<KeyAccountDAO> GetSalesKeyAccounts(string repCode, ArgsDTO args)
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();
            try
            {
                return objCommonAdapter.GetSalesKeyAccounts(repCode, args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SalesLeaderDAO> GetSalesLeaderBoard(string businessCode, string sPeriod,ArgsDTO args)
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();
            try
            {
                return objCommonAdapter.GetSalesLeaderBoard(businessCode, sPeriod, args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<KeyAccountDAO> GetTopSalesReps(ArgsDTO args)
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();
            try
            {
                return objCommonAdapter.GetTopSalesReps(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<KeyAccountDAO> GetTopSalesDistributors(ArgsDTO args)
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();
            try
            {
                return objCommonAdapter.GetTopSalesDistributors(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<KeyAccountDAO> GetTopSalesProducts(ArgsDTO args)
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();
            try
            {
                return objCommonAdapter.GetTopSalesProducts(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<KeyAccountDAO> GetTopSalesMarkets(ArgsDTO args)
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();
            try
            {
                return objCommonAdapter.GetTopSalesMarkets(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region - Area -

        public List<CityAreaDTO> GetAllAreaCodes()
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();
            try
            {
                return objCommonAdapter.GetAllAreaCodes();
            }
            catch
            {
                throw;
            }
        }

        public List<CityAreaDTO> GetAllCityAreas(ArgsDTO args)
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();
            try
            {
                return objCommonAdapter.GetAllCityAreas(args);
            }
            catch
            {
                throw;
            }
        }

        public List<OutletTypeDTO> GetAllOutletType()
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();
            try
            {
                return objCommonAdapter.GetAllOutletType();
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region - CheckList Methods -

        public List<CheckListDTO> GetChecklist(int iLeadStageID)
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();
            try
            {
                return objCommonAdapter.GetChecklist(iLeadStageID);
            }
            catch
            {
                throw;
            }
        }

        public List<ChecklistImageDTO> GetChecklistImageById(int CheckListId)
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();
            try
            {
                return objCommonAdapter.GetChecklistImageById(CheckListId);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveCheckList(List<CheckListDTO> leadChecklistDto)
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();
            try
            {
                return objCommonAdapter.SaveCheckList(leadChecklistDto);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveChecklistMaster(ChecklistMasterDTO checklistMasterDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();

            try
            {
                return commonAdapter.SaveChecklistMaster(checklistMasterDTO);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool PushNotifyGetSyncDetails()
        {
            CommonAdapter commonAdapter = new CommonAdapter();

            try
            {
                return commonAdapter.PushNotifyGetSyncDetails();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string SaveQuery(CheckListDTO leadChecklistDto)
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();
            try
            {
                return objCommonAdapter.SaveQuery(leadChecklistDto);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region - Contact Person Image -

        public bool SaveContactPersonImage(ContactPersonImageDTO contactPersonImage)
        {
            CommonAdapter lookupAdapter = new CommonAdapter();

            try
            {
                return lookupAdapter.SaveContactPersonImage(contactPersonImage);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region - Country -
        public List<CountryDTO> GetCountry()
        {
            CommonAdapter lookupAdapter = new CommonAdapter();

            try
            {
                return lookupAdapter.GetCountry();
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion
        
        #region - States -
        public List<StateDTO> GetStates()
        {
            CommonAdapter lookupAdapter = new CommonAdapter();

            try
            {
                return lookupAdapter.GetStates();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<StateDTO> GetAllStates()
        {
            CommonAdapter lookupAdapter = new CommonAdapter();

            try
            {
                return lookupAdapter.GetAllStates();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public StateDTO GetStateByCode(string sStateCode)
        {
            CommonAdapter lookupAdapter = new CommonAdapter();

            try
            {
                return lookupAdapter.GetStateByCode(sStateCode);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region - Rep Group -
        public List<RepGroupDTO> GetAllRepGroups()
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();

            try
            {
                return objCommonAdapter.GetAllRepGroups();
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region - Lookup -

        public List<LookupDTO> GetLookups(string lookupSqlKey, string whereCls = "", object[] parameters = null, ArgsDTO args = null, bool hasArgs = false)
        {
            try
            {
                CommonAdapter lookupAdapter = new CommonAdapter();

                Stream s = Assembly.GetExecutingAssembly().GetManifestResourceStream("Peercore.CRM.Service.LookupSQL.xml");

                XDocument data = XDocument.Load(s);

                LookupTransferComponentDTO LookupSqlInfo = (from section in data.Descendants("LookupSection")
                                                            where section.Attribute("key").Value.Equals(lookupSqlKey)
                                                            select new LookupTransferComponentDTO()
                                                            {
                                                                Sql = GenerateWhereCls(whereCls, parameters, section),

                                                                IdColumn = new KeyValuePair<string, string>
                                                                    (section.Element("KeyFieldColumn").Attribute("columnName").Value, section.Element("KeyFieldColumn").Attribute("displayAlias").Value),

                                                                CodeColumn = new KeyValuePair<string, string>
                                                                    (section.Element("CodeColumn").Attribute("columnName").Value, section.Element("CodeColumn").Attribute("displayAlias").Value),

                                                                DescriptionColumn = new KeyValuePair<string, string>
                                                                    (section.Element("DescriptionColumn").Attribute("columnName").Value, section.Element("DescriptionColumn").Attribute("displayAlias").Value),

                                                                AdditionalColumns = (from column in section.Element("AdditionalColumns").Descendants("AddedColumn")
                                                                                     select new KeyValuePair<string, string>
                                                                                     (column.Attribute("columnName").Value, column.Attribute("displayAlias").Value)).ToList()
                                                            }).FirstOrDefault();

                return lookupAdapter.GetValues(LookupSqlInfo, args, hasArgs);

            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LookupDTO> GetValuesbySql(string sql)
        {
            CommonAdapter lookupAdapter = new CommonAdapter();

            try
            {
                return lookupAdapter.GetValuesbySql(sql);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<LookupDTO> GetPeercoreLookupValues(string sTableId)
        {
            CommonAdapter lookupAdapter = new CommonAdapter();

            try
            {
                return lookupAdapter.GetPeercoreLookupValues(sTableId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LookupDTO> GetLookupsForBDM(string tableName, KeyValuePair<string, string> descriptionColumn, KeyValuePair<string, string>? codeColumn,
            string whereCls, ArgsDTO args, string primaryKey = "")
        {
            CommonAdapter lookupAdapter = new CommonAdapter();

            string sqlString = string.Empty;
            try
            {
                LookupTransferComponentDTO LookupSqlInfo = GenerateSelect(tableName, descriptionColumn, codeColumn, primaryKey, whereCls);
                return lookupAdapter.GetValues(LookupSqlInfo, args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region - LookupTable -

        public List<LookupTableDTO> GetLookupTables(string tableId, ArgsDTO args)
        {
            CommonAdapter lookupAdapter = new CommonAdapter();
            try
            {
                return lookupAdapter.GetLookupTables(tableId, args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public LookupTableDTO GetDefaultLookupEntry(string sTableID, string defaultDeptID)
        {
            CommonAdapter lookupAdapter = new CommonAdapter();
            try
            {
                return lookupAdapter.GetDefaultLookupEntry(sTableID, defaultDeptID);
            }
            catch
            {
                throw;
            }
        }

        public List<LookupTableDTO> GetRepTerritories(ArgsDTO args)
        {
            CommonAdapter lookupAdapter = new CommonAdapter();
            try
            {
                return lookupAdapter.GetRepTerritories(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LookupTableDTO> GetSalesMarkets()
        {
            CommonAdapter lookupAdapter = new CommonAdapter();
            try
            {
                return lookupAdapter.GetSalesMarkets();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion - LookupTable -





        #region - Search -
        public List<LeadContactPersonDTO> GetContactCollection(ArgsDTO args, bool bIncludeContact, bool bIncludeLead,
                bool bIncludeCustomer, bool bIncludeEndUser, string sSearchText = "", string clientType = "")
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();

            try
            {
                return objCommonAdapter.GetContactCollection(args, bIncludeContact, bIncludeLead,
                                                    bIncludeCustomer, bIncludeEndUser, sSearchText, clientType);
            }
            catch (Exception)
            {
                return new List<LeadContactPersonDTO>();
                //throw;
            }
        }

        #endregion

        #region - Column Setting -
        public ColumnSettingDTO GetColumnSetting(string originator, string gridName)
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();

            try
            {
                return objCommonAdapter.GetColumnSetting(originator, gridName);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool InserColumnSetting(ColumnSettingDTO objColumnSettings)
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();

            try
            {
                return objCommonAdapter.InserColumnSetting(objColumnSettings);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ColumnSettingDTO GetSortItems(string originator, string gridName)
        {
            CommonAdapter objCommonAdapter = new CommonAdapter();

            try
            {
                return objCommonAdapter.GetSortItems(originator, gridName);
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region - Country -
        public List<CatalogDTO> GetProductsLookup(ArgsDTO args)
        {
            CommonAdapter lookupAdapter = new CommonAdapter();

            try
            {
                return lookupAdapter.GetProductsLookup(args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CatalogDTO> GetAllCatalog(ArgsDTO args)
        {
            CommonAdapter lookupAdapter = new CommonAdapter();

            try
            {
                return lookupAdapter.GetAllCatalog(args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CatalogDTO> GetCatalogLookup(ArgsDTO args)
        {
            CommonAdapter lookupAdapter = new CommonAdapter();

            try
            {
                return lookupAdapter.GetCatalogLookup(args);
            }
            catch (Exception)
            {

                throw;
            }
        }
    
        #endregion

        #region - Promotions -


        public List<PromotionDTO> GetAllPromotions()
        {
            CommonAdapter lookupAdapter = new CommonAdapter();

            try
            {
                return lookupAdapter.GetAllPromotions();
            }
            catch (Exception)
            {
                throw;
            }
        }



        #endregion

        public SalesInfoDetailBackDTO TestSalesInfoDetailBack()
        {
            return new SalesInfoDetailBackDTO();
        }

        #region - Market -

        public List<MarketDTO> GetAllMarket(ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();

            try
            {
                return commonAdapter.GetAllMarket(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveMarket(MarketDTO marketDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();

            try
            {
                return commonAdapter.SaveMarket(marketDTO);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool AssignTMEMarkets(List<MarketDTO> marketDTOList)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.AssignTMEMarkets(marketDTOList);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteTMEMarkets(MarketDTO marketDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.DeleteTMEMarkets(marketDTO);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteProductCategory(ProductCategoryDTO productcategoryDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.DeleteProductCategory(productcategoryDTO);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteInvoice(InvoiceHeaderDTO invoiceDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.DeleteInvoice(invoiceDTO);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteCustomerByCusCode(CustomerDTO delCustomer)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.DeleteCustomerByCusCode(delCustomer);
            }
            catch { return false; }
        }

        public bool DeleteAddCustomerRoute(CustomerDTO delCustomer)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.DeleteAddCustomerRoute(delCustomer);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool IsMarketExists(MarketDTO marketDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.IsMarketExists(marketDTO); ;
            }
            catch (Exception)
            {
                return true;
            }
        }

        #endregion

        #region - Volume Performance -

        public bool SaveVolumePerformanceData(List<VolumePerformanceDTO> volumePerformanceList)
        {
            CommonAdapter commonAdapter = new CommonAdapter();

            try
            {
                return commonAdapter.SaveVolumePerformanceData(volumePerformanceList);
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion

        #region - Product -

        //public LoadStockDTO GetAllProductsForStockLoad(string allocDate, int distributorId, int divisionId, ArgsDTO args)
        //{
        //    CommonAdapter commonAdapter = new CommonAdapter();

        //    try
        //    {
        //        return commonAdapter.GetAllProductsForStockLoad(allocDate, distributorId, divisionId, args);
        //    }
        //    catch (Exception)
        //    {
        //        LoadStockDTO errObj = new LoadStockDTO();
        //        return errObj;
        //    }
        //}

        public LoadStockDTO GetProductForStockAdjustment(string allocDate, int distributorId, int divisionId, ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();

            try
            {
                return commonAdapter.GetProductForStockAdjustment(allocDate, distributorId, divisionId, args);
            }
            catch (Exception)
            {
                LoadStockDTO errObj = new LoadStockDTO();
                return errObj;
            }
        }

        //public bool SaveLoadStock(LoadStockDTO loadStockDTO)
        //{
        //    CommonAdapter commonAdapter = new CommonAdapter();

        //    try
        //    {
        //        return commonAdapter.SaveLoadStock(loadStockDTO);
        //    }
        //    catch (Exception)
        //    {
                
        //        throw;
        //    }
        //}

        //public bool UnLoadStock(LoadStockDetailDTO loadStockDetailDTO, DateTime allocDate)
        //{
        //    CommonAdapter commonAdapter = new CommonAdapter();
        //    try
        //    {
        //        return commonAdapter.UnLoadStock(loadStockDetailDTO, allocDate);
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        public List<OriginatorDTO> GetAllOriginators(string name)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.GetAllOriginators(name);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public List<RouteDTO> GetAllRoutes(string name)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.GetAllRoutes(name);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        public List<OriginatorDTO> GetRepsByRouteMasterId(string name, int routeMasterId)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.GetRepsByRouteMasterId(name, routeMasterId);
            }
            catch
            {
                throw;
            }
        }

        public List<RouteDTO> GetRouteRepLikeRouteName(string name, int isTemp, string allocationDate)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.GetRouteRepLikeRouteName(name, isTemp, allocationDate);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<MarketDTO> GetMarketTargetsByDistributorId(ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.GetMarketTargetsByDistributorId(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ProductCategoryDTO> GetAllProductCategories(ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.GetAllProductCategories(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveProduct(ProductDTO productDTO) 
        {
            CommonAdapter commonAdapter = new CommonAdapter();

            try
            {
                return commonAdapter.SaveProduct(productDTO);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool InactiveProduct(ProductDTO productDTO) 
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.InactiveProduct(productDTO);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool IsProductCodeExists(ProductDTO productDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.IsProductCodeExists(productDTO); ;
            }
            catch (Exception)
            {
                return true;
            }
        }

        public bool IsProductNameExists(ProductDTO productDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.IsProductNameExists(productDTO); ;
            }
            catch (Exception)
            {
                return true;
            }
        }


        public bool SaveProductDivisions(int divisionId,List<ProductDTO> productDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.InsertProductDivisions(divisionId,productDTO); ;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveProductForDivision(int divisionId, ProductDTO productDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.InsertProductForDivision(divisionId, productDTO); ;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region - Brands -

        public List<BrandDTO> GetAllBrandsDataAndCount(ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            return commonAdapter.GetAllBrandsDataAndCount(args);
        }

        public List<ProductCategoryDTO> GetAllProductCategoryDataAndCount(ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            return commonAdapter.GetAllProductCategoryDataAndCount(args);
        }

        public List<CheckListDTO> GetAllCheckListDataAndCount(ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            return commonAdapter.GetAllCheckListDataAndCount(args);
        }

        public bool SaveBrand(BrandDTO brandDTO)
        {
            try
            {
                CommonAdapter commonAdapter = new CommonAdapter();
                return commonAdapter.SaveBrand(brandDTO);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveArea(CityAreaDTO areaDTO)
        {
            try
            {
                CommonAdapter commonAdapter = new CommonAdapter();
                return commonAdapter.SaveArea(areaDTO);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveOutletType(OutletTypeDTO outlettypeDTO)
        {
            try
            {
                CommonAdapter commonAdapter = new CommonAdapter();
                return commonAdapter.SaveOutletType(outlettypeDTO);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveProductFlavor(FlavorDTO flavorDTO)
        {
            try
            {
                CommonAdapter commonAdapter = new CommonAdapter();
                return commonAdapter.SaveProductFlavor(flavorDTO);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveProductPacking(ProductPackingDTO packingDTO)
        {
            try
            {
                CommonAdapter commonAdapter = new CommonAdapter();
                return commonAdapter.SaveProductPacking(packingDTO);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool IsProductPackingNameExists(ProductPackingDTO packingDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.IsProductPackingNameExists(packingDTO); ;
            }
            catch (Exception)
            {
                return true;
            }
        }

        public bool SaveProductCategory(ProductCategoryDTO productcategoryDTO)
        {
            try
            {
                CommonAdapter commonAdapter = new CommonAdapter();
                return commonAdapter.SaveProductCategory(productcategoryDTO);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool IsBrandCodeExists(BrandDTO brandDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.IsBrandCodeExists(brandDTO); ;
            }
            catch (Exception)
            {
                return true;
            }
        }

        public bool IsAreaNameExists(CityAreaDTO areaDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.IsAreaNameExists(areaDTO); ;
            }
            catch (Exception)
            {
                return true;
            }
        }

        public bool IsOutletTypeExists(OutletTypeDTO outlettypeDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.IsOutletTypeExists(outlettypeDTO);
            }
            catch (Exception)
            {
                return true;
            }
        }

        public bool IsBrandNameExists(BrandDTO brandDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.IsBrandNameExists(brandDTO);
            }
            catch (Exception)
            {
                return true;
            }
        }

        public bool IsProductFlavorNameExists(FlavorDTO flavorDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.IsProductFlavorNameExists(flavorDTO); ;
            }
            catch (Exception)
            {
                return true;
            }
        }

        public bool IsProductCategoryCodeExists(ProductCategoryDTO productcategoryDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.IsProductCategoryCodeExists(productcategoryDTO);
            }
            catch (Exception)
            {
                return true;
            }
        }

        public bool IsProductCategoryNameExists(ProductCategoryDTO productcategoryDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.IsProductCategoryNameExists(productcategoryDTO);
            }
            catch (Exception)
            {
                return true;
            }
        }

        public int IsProductExsistInDiscountScheme(int product_id)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.IsProductExsistInDiscountScheme(product_id);
            }
            catch (Exception)
            {
                return -1;
            }
        }

        #endregion

        public List<ProductDTO> GetAllProductsDataAndCount(ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            return commonAdapter.GetAllProductsDataAndCount(args);
        }

        public List<ProductDTO> GetAllProductsDataAndCountWithOutImage(ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            return commonAdapter.GetAllProductsDataAndCountWithOutImage(args);
        }

        public List<ProductDTO> GetAllProductsDataAndCountWithOutImageForMaster(ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            return commonAdapter.GetAllProductsDataAndCountWithOutImageForMaster(args);
        }

        public List<ProductDTO> GetAllProductsWithNoPrice(ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            return commonAdapter.GetAllProductsWithNoPrice(args);
        }

        public List<ProductDTO> GetAllProductPricesDataAndCount(ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            return commonAdapter.GetAllProductPricesDataAndCount(args);
        }

        public bool SaveProductPrice(ProductDTO productDTO)
        {
            try
            {
                CommonAdapter commonAdapter = new CommonAdapter();
                return commonAdapter.SaveProductPrice(productDTO);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ProductPackingDTO> GetAllProductPackings(ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.GetAllProductPackings(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ProductDTO GetProductById(int productId)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            return commonAdapter.GetProductById(productId);
        }
        #endregion

        #region - Holiday -

        public bool InsertHoliday(CalendarHolidayDTO holidayDTO)
        {
            CommonAdapter Adapter = new CommonAdapter();

            try
            {
                return Adapter.InsertHoliday(holidayDTO);
            }
            catch
            {
                throw;
            }
        }

        public int InsertHolidayReturnId(CalendarHolidayDTO holidayDTO)
        {
            CommonAdapter Adapter = new CommonAdapter();

            try
            {
                return Adapter.InsertHolidayReturnId(holidayDTO);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateHoliday(CalendarHolidayDTO holidayDTO)
        {
            CommonAdapter Adapter = new CommonAdapter();

            try
            {
                return Adapter.UpdateHoliday(holidayDTO);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteHoliday(int holidayId)
        {
            CommonAdapter Adapter = new CommonAdapter();

            try
            {
                return Adapter.DeleteHoliday(holidayId);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteProductDevision(int divisionId, int prodId)
        {
            CommonAdapter Adapter = new CommonAdapter();

            try
            {
                return Adapter.DeleteProductDevision(divisionId, prodId);
            }
            catch
            {
                throw;
            }
        }

        public List<CalendarHolidayDTO> GetAllHolidays(ArgsDTO args)
        {
            CommonAdapter Adapter = new CommonAdapter();

            try
            {
                return Adapter.GetAllHolidays(args);
            }
            catch
            {
                throw;
            }
        }

        public CalendarHolidayDTO GetHolidayTypeByCode(string holidayType)
        {
            CommonAdapter Adapter = new CommonAdapter();

            try
            {
                return Adapter.GetHolidayTypeByCode(holidayType);
            }
            catch
            {
                throw;
            }
        }

        public List<CalendarHolidayDTO> GetAllHolidaytypes()
        {
            CommonAdapter Adapter = new CommonAdapter();

            try
            {
                return Adapter.GetAllHolidaytypes();
            }
            catch
            {
                throw;
            }
        }




        public HolidayCalDTO TestdtoReturn()
        {
            return new HolidayCalDTO();
        }

        public CalendarHolidayDTO Test()
        {
            CalendarHolidayDTO dd = new CalendarHolidayDTO();
            return dd;
        }

        public bool IsHolidayExistsForTheDay(string createdBy, DateTime effDate)
        {
            CommonAdapter Adapter = new CommonAdapter();

            try
            {
                return Adapter.IsHolidayExistsForTheDay(createdBy, effDate);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateHolidayLastChangedDate(CalendarHolidayDTO calendarHolidayDTO)
        {
            CommonAdapter Adapter = new CommonAdapter();
            try
            {
                return Adapter.UpdateHolidayLastChangedDate(calendarHolidayDTO);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateHolidayDate(CalendarHolidayDTO calendarHolidayDTO)
        {
            CommonAdapter Adapter = new CommonAdapter();
            try
            {
                return Adapter.UpdateHolidayDate(calendarHolidayDTO);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region TargetAllocation

        public bool SaveTargetAllocation(TargetAllocationDTO targetAllocationDTO)
        {
            TargetAllocationAdapter targetAllocationAdapter = new TargetAllocationAdapter();

            try
            {
                return targetAllocationAdapter.SaveTargets(targetAllocationDTO);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region - Pragram - 

        public List<ProgramTargetsDTO> GetProgramTargetsByProgram(string programNameLike, int programTargetId, int programId)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            return commonAdapter.GetProgramTargetsByProgram(programNameLike, programTargetId, programId);
        }

        public List<ProductDTO> GetProductsByName(string productNameLike)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            return commonAdapter.GetProductsByName(productNameLike);
        }

        public bool SaveProgramTargets(List<ProgramTargetsDTO> programTargetsDTOList)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.SaveProgramTargets(programTargetsDTOList);
            }
            catch
            {
                throw;
            }
        }        

        public bool SaveProgram(ProgramDTO program)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.SaveProgram(program);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteProgram(int programId)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.DeleteProgram(programId);
            }
            catch
            {
                throw;
            }
        }

        public List<ProgramDTO> GetAllProgram(ArgsDTO args)
        {
            List<ProgramDTO> proList = new List<ProgramDTO>();
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                proList = commonAdapter.GetAllPrograms();
                return proList;
            }
            catch
            {
                throw;
            }
        }

        public List<ProgramCustomerDTO> GetAllProgramCustomer(ArgsDTO args)
        {
            return new List<ProgramCustomerDTO>();
        }

        public ProgramDTO GetProgramDetailsById(int programId) 
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.GetProgramDetailsById(programId);
            }
            catch
            {
                throw;
            }
        }

        public List<ProgramTargetsDTO> GetProgramTargets(int programId, DateTime fromDate, DateTime toDate, ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            return commonAdapter.GetProgramTargets(programId, fromDate, toDate, args);
        }

        public bool IsProgramNameExists(ProgramDTO programDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.IsProgramNameExists(programDTO); ;
            }
            catch (Exception)
            {
                return true;
            }
        }
        #endregion

        public List<RouteDTO> GetRouteRepLikeRouteNameForCustomer(string name, int isTemp, string originator, string chlidOriginators)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.GetRouteRepLikeRouteNameForCustomer(name, isTemp, originator, chlidOriginators);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<ProductDTO> GetProductsByDivisionId(int divisionId, ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.GetProductsByDivisionId(divisionId,args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        //public bool InsertProductDivisions(List<ProductDTO> productDtoList)
        //{
        //    CommonAdapter commonAdapter = new CommonAdapter();
        //    try
        //    {
        //        return commonAdapter.InsertProductDivisions(productDtoList);
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        #region - Flavor -
        public List<FlavorDTO> GetAllFlavor()
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.GetAllFlavor();
            }
            catch (Exception)
            {

                throw;
            }
        }

        #endregion 

        #region - InvoiceHeader -
        public List<InvoiceHeaderDTO> GetAllInvoicesDataAndCount(ArgsDTO args)
        {


      
            
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.GetAllInvoicesDataAndCount(args);
            }
            catch (Exception)
            {

                throw;
            }
        }





        #endregion

        #region - Holiday Planner Change Log -

        public int InsertHolidayPlannerChangeLog(HolidayPlannerChangeLogDTO holidayPlannerChangeLogDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.InsertHolidayPlannerChangeLog(holidayPlannerChangeLogDTO);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateHolidayPlannerChangeLogStatus(HolidayPlannerChangeLogDTO holidayPlannerChangeLogDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.UpdateHolidayPlannerChangeLogStatus(holidayPlannerChangeLogDTO);
            }
            catch
            {
                throw;
            }
        }

        public HolidayPlannerChangeLogDTO GetHolidayPlannerChangeLogById(int holidayPlannerChangeLogId)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.GetHolidayPlannerChangeLogById(holidayPlannerChangeLogId);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region - Rep Target -

        public bool SaveRepTarget(List<RepTargetDTO> repTargetDTOList)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.SaveRepTarget(repTargetDTOList);
            }
            catch
            {
                throw;
            }
        }

        public List<RepTargetDTO> GetAllRepTargets(string Originator, string OriginatorType, ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.GetAllRepTargets(Originator, OriginatorType, args);
            }
            catch
            {
                throw;
            }
        }

        public bool DeactivateRepTarget(RepTargetDTO repTarget)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.DeactivateRepTarget(repTarget);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        #region - Incentive Plan -

        public List<IncentivePlanHeaderDTO> GetAllIncentivePlans(ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.GetAllIncentivePlans(args);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveIncentivePlans(List<IncentivePlanHeaderDTO> incentivePlanHeaderList)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.SaveIncentivePlans(incentivePlanHeaderList);
            }
            catch
            {
                throw;
            }
        }

        public bool DeactivateSecondarySalesWeekly(IncentiveSecondarySalesWeeklyDTO weeklyDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.DeactivateSecondarySalesWeekly(weeklyDTO);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeactivateSecondarySalesPartial(IncentiveSecondarySalesPartialDTO partialDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.DeactivateSecondarySalesPartial(partialDTO);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region - Checklist -

        public List<ChecklistTypeDTO> GetChecklistTypeList()
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.GetChecklistTypeList();
            }
            catch
            {
                throw;
            }
        }

        #endregion

        public bool UploadStockToDistributor(List<ProductDTO> lstProduct, int distributorId, LoadStockDTO loadStockDTO)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            return commonAdapter.UploadStockToDistributor(lstProduct, distributorId, loadStockDTO);
        }

        //public bool UploadStockToBulkDistributor(List<ProductDTO> lstProduct, LoadStockDTO loadStockDTO)
        //{
        //    CommonAdapter commonAdapter = new CommonAdapter();
        //    return commonAdapter.UploadStockToBulkDistributor(lstProduct, loadStockDTO);
        //}

        public bool SaveTargetAndIncentiveNew(string TargetBreakDay, string First14Days, string Last14Days)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            return commonAdapter.SaveTargetAndIncentiveNew(TargetBreakDay, First14Days, Last14Days);
        }

        public List<TargetEntity> LoadTargetMaster()
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.LoadTargetMaster();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool DeleteProductFlavor(int flavourId)
        {
            try
            {
                CommonAdapter commonAdapter = new CommonAdapter();
                return commonAdapter.DeleteProductFlavor(flavourId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteOutletType(int outletTypeId)
        {
            try
            {
                CommonAdapter commonAdapter = new CommonAdapter();
                return commonAdapter.DeleteOutletType(outletTypeId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetAdminPassword(string password)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.GetAdminPassword(password);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteBrand(int brandId)
        {
            try
            {
                CommonAdapter commonAdapter = new CommonAdapter();
                return commonAdapter.DeleteBrand(brandId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeletePacking(int packingId)
        {
            try
            {
                CommonAdapter commonAdapter = new CommonAdapter();
                return commonAdapter.DeletePacking(packingId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateCheckListCustomer(int CheckListId, string CustomerCode, bool HasSelect)
        {
            try
            {
                CommonAdapter commonAdapter = new CommonAdapter();
                return commonAdapter.UpdateCheckListCustomer(CheckListId, CustomerCode, HasSelect);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateCheckListRoute(int CheckListId, int RouteId, bool HasSelect)
        {
            try
            {
                CommonAdapter commonAdapter = new CommonAdapter();
                return commonAdapter.UpdateCheckListRoute(CheckListId, RouteId, HasSelect);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteDivision(int divisionId)
        {
            try
            {
                CommonAdapter commonAdapter = new CommonAdapter();
                return commonAdapter.DeleteDivision(divisionId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteArea(int areaId)
        {
            try
            {
                CommonAdapter commonAdapter = new CommonAdapter();
                return commonAdapter.DeleteArea(areaId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteEMEIFromOriginator(string originator, string emei_no)
        {
            try
            {
                CommonAdapter commonAdapter = new CommonAdapter();
                return commonAdapter.DeleteEMEIFromOriginator(originator, emei_no);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool ClearDeviceEMEIFromOriginator(string originator, string emei_no)
        {
            try
            {
                CommonAdapter commonAdapter = new CommonAdapter();
                return commonAdapter.ClearDeviceEMEIFromOriginator(originator, emei_no);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SalesDataEntity> GetAllSales(ArgsDTO args, string IsASMOrProduct, int ProductId)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.GetAllSales(args, IsASMOrProduct, ProductId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<SalesDataEntity> getASMSalesData(ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.getASMSalesData(args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<SalesDataEntity> GetRepSales(ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            return commonAdapter.GetRepSales(args);
        }

        //for originator
        public List<OriginatorEntity> GetOriginators(ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            return commonAdapter.GetOriginators(args);
        } 
        
        public List<OriginatorEntity> GetOriginatorsFor2FA(ArgsDTO args)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            return commonAdapter.GetOriginatorsFor2FA(args);
        }

        public bool UpdateIsTFAUser(string originator, bool isTFA)
        {
            CommonAdapter commonAdapter = new CommonAdapter();

            try
            {
                return OriginatorBR.Instance.UpdateIsTFAUser(originator, isTFA);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool UpdateIsTFAAuth(string originator, bool isTFAAuth)
        {
            CommonAdapter commonAdapter = new CommonAdapter();

            try
            {
                return OriginatorBR.Instance.UpdateIsTFAAuth(originator, isTFAAuth);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string addNewUser(string originator, string userName, string designation, string password, string userType, string mobile)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.addNewUser(originator, userName, designation, password, userType, mobile);
            }
            catch (Exception)
            {

                throw;
            }
        }

        //for update user
        public string updateUser(string originator, string userName, string designation, string password, string userType, int UserID, string mobile)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.updateUser(originator, userName, designation, password, userType, UserID, mobile);
            }
            catch (Exception)
            {

                throw;
            }
        }

        //for deleting user
        public string deleteUser(int UserId)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            try
            {
                return commonAdapter.deleteUser(UserId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        // originator types for dropdown
        public List<OriginatorEntity> getAllOriginatorTypesForDropdown()
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            return commonAdapter.getAllOriginatorTypesForDropdown();
        }
        
        // originator types for dropdown
        public List<SalesDataEntity> getRepsLiveSales(string originator)
        {
            CommonAdapter commonAdapter = new CommonAdapter();
            return commonAdapter.getRepsLiveSales(originator);
        }

        public bool DeleteBulkInvoices(string userType, string userName, string invList)
        {
            try
            {
                return InvoiceHeaderBR.Instance.DeleteBulkInvoices(userType, userName, invList);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteInvoiceHeaderByInvoiceNo(string originator, string invoiceNo)
        {
            try
            {
                return InvoiceHeaderBR.Instance.DeleteInvoiceHeaderByInvoiceNo(originator, invoiceNo);
            }
            catch { return false; }
        }

        #region "Sync APIS"

        public int UpdateRepSyncData(SyncRequestModel request)
        {
            int status = 0;
            string server_api_key = ConfigurationManager.AppSettings["FCM_SERVER_API_KEY"];
            string sender_id = ConfigurationManager.AppSettings["FCM_SENDER_ID"];
            string notify_type = "syncdet";

            int requestId = 0;

            try
            {
                ArgsModel args = new ArgsModel();
                args.StartIndex = 0;
                args.RowCount = 10000;
                args.StartIndex = 1;
                args.OrderBy = " rep_name asc";

                requestId = SyncBR.Instance.InserNewSyncMaster(request.originator, request.originatorType);

                if (SyncBR.Instance.UpdateRepIMEISyncStatus(request.originator, request.originatorType, request.DeviceIMEI))
                {
                    List<SalesRepSyncModel> salesVal = SyncBR.Instance.GetAllSalesRepSyncDataByIMEI(args, request.originator, request.originatorType, request.DeviceIMEI);

                    foreach (SalesRepSyncModel device in salesVal)
                    {
                        NotificationResponse response = SendNotification(sender_id,
                            server_api_key,
                            device.DeviceToken,
                            notify_type,
                            notify_type,
                            "",
                            "A",
                            requestId);

                        if (response.Status == true)
                        {
                            SyncBR.Instance.UpdateFCMDeliveryStatus(device.RepCode, device.EmeiNo, "A");
                        }
                        else
                        {
                            SyncBR.Instance.UpdateFCMDeliveryStatus(device.RepCode, device.EmeiNo, "I");
                        }

                        status = requestId;
                    }
                }
            }
            catch (Exception ex) { }

            return status;
        }

        public bool UpdateOrderIsInvoiceActivate(OrderIsInvoiceActivate request)
        {
            bool status = false;

            try
            {
                status = OrderHeaderBR.Instance.UpdateOrderIsInvoiceActivate(request);
            }
            catch (Exception ex) { status = false; }

            return status;
        }

        public NotificationResponse SendNotification(string senderId, string apiKey, string deviceId, string notifyType, string contentTitle, string message, string msgStatus, int requestId)
        {
            NotificationResponse result = new NotificationResponse();

            try
            {
                result.Status = false;
                result.Error = null;

                var value = message;
                HttpWebRequest tRequest = (HttpWebRequest)HttpWebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/x-www-form-urlencoded;charset=UTF-8";
                tRequest.Headers.Add(string.Format("Authorization: key={0}", apiKey));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));

                string postData = "collapse_key=score_update&time_to_live=108&delay_while_idle=1&priority=high" +
                    "&data.message.notifyType=" + notifyType +
                    "&data.message.contentTitle=" + contentTitle +
                    "&data.message.message=" + message +
                    "&data.message.msgstatus=" + msgStatus +
                    "&data.message.requestId=" + requestId +
                    "&data.time=" + System.DateTime.Now.ToString("dd.MM.yyyy HH:mm:ss") + "&registration_id=" + deviceId + "";

                Byte[] byteArray = Encoding.UTF8.GetBytes(postData);
                tRequest.ContentLength = byteArray.Length;

                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);

                    using (HttpWebResponse tResponse = (HttpWebResponse)tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();

                                if (tResponse.StatusDescription == "OK")
                                {
                                    result.Status = true;
                                    result.Message = sResponseFromServer;
                                    result.Error = null;
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Status = false;
                result.Message = null;
                result.Error = ex;
            }

            return result;
        }

        #endregion

        #region "Reports"

        public List<SalesRepRouteSequenceModel> GetAllSRRouteLocations(SRRouteSequenceRequestModel request)
        {
            List<SalesRepRouteSequenceModel> srRouteSeqList = new List<SalesRepRouteSequenceModel>();

            try
            {
                srRouteSeqList = RepsBR.Instance.GetAllSRRouteLocations(request.selectRep, request.selectDate);
            }
            catch (Exception ex) { srRouteSeqList = new List<SalesRepRouteSequenceModel>(); }

            return srRouteSeqList;
        }

        #endregion

        #region "3rd Phase"

        // originator types for dropdown
        public List<SalesRepAttendanceHoursModel> GetRepsAttendanceHours(string originator, string date)
        {
            return OriginatorBR.Instance.GetRepsAttendanceHours(originator, date);
        }

        public List<AttendanceReasons> GetAttendanceReasons()
        {
            return OriginatorBR.Instance.GetAttendanceReasons();
        }

        public bool InsertAttendanceReason(AttendanceReasons attendanceDet)
        {
            try
            {
                OriginatorBR.Instance.InsertAttendanceReason(attendanceDet);
            }
            catch (Exception)
            {

            }

            return true;
        }

        public bool deleteAttendanceReason(string originator, int id)
        {
            CommonAdapter commonAdapter = new CommonAdapter();

            try
            {
                return OriginatorBR.Instance.deleteAttendanceReason(originator, id);
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public List<DashboardAttendanceByRepType> GetDashboardAttendanceByRepType(string originator, string date)
        {
            return OriginatorBR.Instance.GetDashboardAttendanceByRepType(originator, date);
        }

        public List<DashboardAttendanceDetails> GetDashboardAttendanceDetails(ArgsModel args, string originator, string date, string salesType, string attendType)
        {
            return OriginatorBR.Instance.GetDashboardAttendanceDetails(originator, date, salesType, attendType);
        }

        public MobileRepsDashboardModel LoadMobileRepsDashboard()
        {
            return CommonBR.Instance.LoadMobileRepsDashboard();
        }

        public bool SaveMobileRepDashboard(MobileRepsDashboardModel mobileDashboard)
        {
            return CommonBR.Instance.SaveMobileRepDashboard(mobileDashboard);
        }

        public List<KPIReports> GetAllKPIReports()
        {
            return EmailBR.Instance.GetAllKPIReports();
        }
        
        public List<KPIReportUser> GetAllKPIReportUsersByReportId(int ReportId)
        {
            return EmailBR.Instance.GetAllKPIReportUsersByReportId(ReportId);
        }

        public bool UpdateKPIReport(KPIReports kpiReport)
        {
            return EmailBR.Instance.UpdateKPIReport(kpiReport);
        }

        public bool CreateTransactionLog(string originator, string transTime, string transType, string transModule, string transDesc)
        {
            try
            {
                SessionBR.Instance.CreateTransactionLog(originator, transTime, transType, transModule, transDesc);
            }
            catch (Exception)
            {

            }

            return true;
        }

        public List<CustomerCategory> GetAllCustomerCategory()
        {
            try
            {
                return CommonBR.Instance.GetAllCustomerCategory();
            }
            catch (Exception)
            {
                return new List<CustomerCategory>();
            }
        }

        #endregion

        public bool UploadOutletBulkTransfer(string Originator, int NewRouteId, string CustomerCode, string CustomerName)
        {
            try
            {
                return CommonBR.Instance.UploadOutletBulkTransfer(Originator, NewRouteId, CustomerCode, CustomerName);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
