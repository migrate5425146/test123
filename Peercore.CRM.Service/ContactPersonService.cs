﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.Common.Adapter;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.ServiceContracts;
namespace Peercore.CRM.Service
{
    public partial class CRMService :ILead, ICustomer
    {
        ContactPersonAdapter contactPersonAdapter = null;

       /* public bool InsertAppointment(AppointmentDTO appointmentDto)
        {
            return contactPersonAdapter.InsertAppointments(appointmentDto);
        }*/

        public bool SaveContactPerson(ContactPersonDTO oContactPersonDTO, out int contactPersonId)
        {
            try
            {
                contactPersonAdapter = new ContactPersonAdapter();
                return contactPersonAdapter.SaveContactPerson(oContactPersonDTO, out contactPersonId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool SaveCustomerContact(ContactPersonDTO oContactPersonDTO, out int contactPersonId)
        {
            try
            {
                contactPersonAdapter = new ContactPersonAdapter();
                return contactPersonAdapter.SaveCustomerContact(oContactPersonDTO, out contactPersonId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public ContactPersonDTO GetContactPerson(int contactPersonId)
        {
            try
            {
                contactPersonAdapter = new ContactPersonAdapter();
                return contactPersonAdapter.GetContactPerson(contactPersonId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public ContactPersonDTO GetCustomerContactPerson(int contactPersonId)
        {
            try
            {
                contactPersonAdapter = new ContactPersonAdapter();
                return contactPersonAdapter.GetCustomerContactPerson(contactPersonId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public List<ContactPersonDTO> GetCustomerContacts(string sCustCode, ArgsDTO args, string clientType, bool includeChildReps = false)
        {
            contactPersonAdapter = new ContactPersonAdapter();
            try
            {
                return contactPersonAdapter.GetCustomerContacts(sCustCode, args, clientType, includeChildReps);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public ContactPersonDTO GetCustomerContact(string custCode, string contactType, string origin)
        {
            try
            {
                contactPersonAdapter = new ContactPersonAdapter();
                return contactPersonAdapter.GetCustomerContact(custCode, contactType, origin);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteContactPerson(int contactPersonId)
        {
            contactPersonAdapter = new ContactPersonAdapter();
            try
            {
                return contactPersonAdapter.DeleteContactPerson(contactPersonId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<ContactPersonDTO> GetContactsByOrigin(ArgsDTO args, bool includeChildReps = false)
        {
            contactPersonAdapter = new ContactPersonAdapter();
            try
            {
                return contactPersonAdapter.GetContactsByOrigin(args, includeChildReps);
            }
            catch (Exception)
            {
                throw;
            }
        }
        /*
        public List<ContactPersonDTO> GetContactPersons(string parameter, ArgsDTO args)
        {
            contactPersonAdapter = new ContactPersonAdapter();
            return contactPersonAdapter.GetContactPersons(parameter, args);
        }

        public bool ToggleLeadsKeyContacts(int leadID, int contactPersonID)
        {
            try
            {
                contactPersonAdapter = new ContactPersonAdapter();
                return contactPersonAdapter.ToggleLeadsKeyContacts(leadID, contactPersonID);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
         */
    }
}
