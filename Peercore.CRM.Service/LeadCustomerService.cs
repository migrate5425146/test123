﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.ServiceContracts;
using Peercore.CRM.Service.Common.Adapter;
using Peercore.CRM.Service.DTO.CompositeEntities;
using Peercore.CRM.Service.DTO;

namespace Peercore.CRM.Service
{
    public partial class CRMService : ILeadCustomer
    {
        #region - Activity -
        public List<LeadCustomerDTO> GetNotCalledCustomers(ArgsDTO args)
        {
            leadCustomerAdapter = new LeadCustomerAdapter();
            try
            {
                return leadCustomerAdapter.GetNotCalledCustomers(args);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<LeadCustomerDTO> GetCalledCustomers(ArgsDTO args)
        {
            leadCustomerAdapter = new LeadCustomerAdapter();
            try
            {
                return leadCustomerAdapter.GetCalledCustomers(args);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        //LeadCustomerAdapter leadCustomerAdapter = null;

        //public List<LeadCustomerDTO> LeadExist(LeadDTO lead, ArgsDTO args)
        //{
        //    leadCustomerAdapter = new LeadCustomerAdapter();
        //    try
        //    {
        //        return leadCustomerAdapter.LeadExist(lead, args);
        //    }
        //    catch (Exception)
        //    {
                
        //        throw;
        //    }
        //}


        //public List<LeadCustomerDTO> GetCombinedCollection(ArgsDTO args,
        //    string sLeadOrCustomer = "",
        //    bool retreiveActive = true,
        //    bool isReqSentVisible = false)
        //{
        //    leadCustomerAdapter = new LeadCustomerAdapter();
        //    try
        //    {
        //        return leadCustomerAdapter.GetCombinedCollection(args, sLeadOrCustomer, retreiveActive, isReqSentVisible);
        //    }
        //    catch (Exception)
        //    {
                
        //        throw;
        //    }
        //}

        LeadCustomerAdapter leadCustomerAdapter = null;
        
        #region - Lead -
        public List<LeadCustomerDTO> GetCombinedCollection(ArgsDTO args)
        {
            leadCustomerAdapter = new LeadCustomerAdapter();

            try
            {
                return leadCustomerAdapter.GetCombinedCollection(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LeadCustomerDTO> GetLeadCollectionForCheckList(int CheckListId, ArgsDTO args)
        {
            leadCustomerAdapter = new LeadCustomerAdapter();
            try
            {
                return leadCustomerAdapter.GetLeadCollectionForCheckList(CheckListId, args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<LeadCustomerDTO> GetPendingCustomers(ArgsDTO args)
        {
            leadCustomerAdapter = new LeadCustomerAdapter();
            try
            {
                return leadCustomerAdapter.GetPendingCustomerCollection(args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<LeadCustomerDTO> GetCombinedCollectionbyleadStageName(ArgsDTO args)
        {
            leadCustomerAdapter = new LeadCustomerAdapter();
            try
            {
                return leadCustomerAdapter.GetCombinedCollectionbyleadStageName(args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<LeadCustomerDTO> GetLeadCustomerCountByState(ArgsDTO args)
        {
            leadCustomerAdapter = new LeadCustomerAdapter();
            try
            {
                return leadCustomerAdapter.GetLeadCustomerCountByState(args);
            }
            catch
            {
                throw;
            }
        }

        public List<LeadCustomerDTO> GetCombinedCollectionByState(ArgsDTO args, string sLeadOrCustomer = "")
        {
            leadCustomerAdapter = new LeadCustomerAdapter();
            try
            {
                return leadCustomerAdapter.GetCombinedCollectionByState(args, sLeadOrCustomer);
            }
            catch
            {
                throw;
            }
        }

        public List<LeadCustomerDTO> GetLeadCustomerCountByRep(ArgsDTO args)
        {
            leadCustomerAdapter = new LeadCustomerAdapter();
            try
            {
                return leadCustomerAdapter.GetLeadCustomerCountByRep(args);
            }
            catch
            {
                throw;
            }
        }

        public List<LeadCustomerDTO> GetLeadCustomerCollectionByRep(ArgsDTO args)
        {
            leadCustomerAdapter = new LeadCustomerAdapter();
            try
            {
                return leadCustomerAdapter.GetLeadCustomerCollectionByRep(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LeadCustomerDTO> LeadExist(LeadDTO lead, ArgsDTO args)
        {
            leadCustomerAdapter = new LeadCustomerAdapter();
            try
            {
                return leadCustomerAdapter.LeadExist(lead, args);
            }
            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        public List<ColumnSettingDTO> testMethod()
        {
            leadCustomerAdapter = new LeadCustomerAdapter();
            try
            {
                return leadCustomerAdapter.testMethod();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int GetPendingCustomersCount(ArgsDTO args)
        {
            leadCustomerAdapter = new LeadCustomerAdapter();
            try
            {
                return leadCustomerAdapter.GetPendingCustomersCount(args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<LeadCustomerDTO> GetOutletsForLookupInScheduleEntry(ArgsDTO args)
        {
            leadCustomerAdapter = new LeadCustomerAdapter();
            try
            {
                return leadCustomerAdapter.GetOutletsForLookupInScheduleEntry(args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        #region Market Objectives

        public List<ObjectiveDTO> GetAllMarketObjectives(int marketId, ArgsDTO args)
        {
            ObjectiveAdapter objectiveAdapter = new ObjectiveAdapter();
            try
            {
                return objectiveAdapter.GetAllMarketObjectives(marketId, args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool SaveMarketObjective(MarketObjectiveDTO marketObjectiveDTO)
        {
            ObjectiveAdapter objectiveAdapter = new ObjectiveAdapter();

            try
            {
                return objectiveAdapter.SaveMarketObjective(marketObjectiveDTO);
            }
            catch
            {
                throw;
            }
        }

        public bool UnassignedRoutefromRep(int RouteId, string RepCode)
        {
            LeadCustomerAdapter objLeadCustomerAdapter = new LeadCustomerAdapter();
            try
            {
                return objLeadCustomerAdapter.UnassignedRoutefromRep(RouteId, RepCode);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool IsObjectiveCodeExists(ObjectiveDTO objectiveDTO)
        {
            ObjectiveAdapter objectiveAdapter = new ObjectiveAdapter();

            try
            {
                return objectiveAdapter.IsObjectiveCodeExists(objectiveDTO);
            }
            catch
            {
                throw;
            }
        }

        #endregion

        public List<LeadCustomerDTO> GetAllNotInvoicedUsers(ArgsDTO args)
        {
            leadCustomerAdapter = new LeadCustomerAdapter();
            try
            {
                return leadCustomerAdapter.GetAllNotInvoicedUsers(args);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
