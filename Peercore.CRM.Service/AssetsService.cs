﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.ServiceContracts;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.Common.Adapter;
using System.Xml.Linq;
using System.IO;
using System.Reflection;
using Peercore.CRM.Service.DTO.TransferComponents;
using Peercore.CRM.Service.DTO.CompositeEntities;
using System.ServiceModel;
using Peercore.CRM.Model;
using Peercore.CRM.BusinessRules;
using System.Data;

namespace Peercore.CRM.Service
{
    public partial class CRMService : IAssets
    {
        #region  - Asset Type - 

        public List<AssetTypeModel> GetAllAssetTypes(ArgsModel args)
        {
            try
            {
                List<AssetTypeModel> objList = AssetBR.Instance.GetAllAssetTypes(args);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public bool SaveAssetType(AssetTypeModel assetTypeDetail)
        {
            try
            {
                bool isTrue = AssetBR.Instance.SaveAssetType(assetTypeDetail);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateAssetType(int assetTypeId, string typeName, string description, string ModifiedBy)
        {
            try
            {
                bool isTrue = AssetBR.Instance.UpdateAssetType(assetTypeId, typeName, description, ModifiedBy);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteAssetType(int assetTypeId, string originator)
        {
            try
            {
                bool isTrue = AssetBR.Instance.DeleteAssetType(assetTypeId, originator);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool IsAssetTypeAvailable(string assetType)
        {
            try
            {
                bool isTrue = AssetBR.Instance.IsAssetTypeAvailable(assetType);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region - Asset Master -

        public List<AssetModel> GetAllAssets(ArgsModel args)
        {
            try
            {
                List<AssetModel> objList = AssetBR.Instance.GetAllAssets(args);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public bool SaveAsset(AssetModel assetDetail)
        {
            try
            {
                bool isTrue = AssetBR.Instance.SaveAsset(assetDetail);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateAsset(AssetModel assetDetail)
        {
            try
            {
                bool isTrue = AssetBR.Instance.UpdateAsset(assetDetail);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteAsset(int assetId, string originator)
        {
            try
            {
                bool isTrue = AssetBR.Instance.DeleteAsset(assetId, originator);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool excelData(string fileName, string originator, string logPath)
        {
            try
            {
                return AssetBR.Instance.UploadExcelfile(fileName, originator, logPath);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool UploadAssetExcel(string fileName, string originator, string logPath)
        {
            try
            {
                return AssetBR.Instance.UploadExcelfile(fileName, originator, logPath);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static void LogError(string error)
        {
            try
            {
                //string logFilePath = @"C:\Logs\Log-" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";
                string logFilePath = @"D:\Logs\ErrorLog_" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";
                FileInfo logFileInfo = new FileInfo(logFilePath);
                DirectoryInfo logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
                if (!logDirInfo.Exists) logDirInfo.Create();
                File.WriteAllText(logFilePath, String.Empty);

                using (FileStream fileStream = new FileStream(logFilePath, FileMode.Append))
                {
                    using (StreamWriter log = new StreamWriter(fileStream))
                    {
                        log.Write(error);
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        public bool IsAssetCodeAvailable(string assetCode)
        {
            try
            {
                bool isTrue = AssetBR.Instance.IsAssetCodeAvailable(assetCode);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region - Asset Assign -

        public List<AssetAssignModel> GetAllAssignAssets(ArgsModel args, string Token)
        {
            try
            {
                List<AssetAssignModel> objList = AssetBR.Instance.GetAllAssignAssets(args, Token);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public bool SaveAssignAsset(AssetAssignModel assignDetail)
        {
            try
            {
                bool isTrue = AssetBR.Instance.SaveAssignAsset(assignDetail);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateAssignAsset(AssetAssignModel assignDetail)
        {
            try
            {
                bool isTrue = AssetBR.Instance.UpdateAssignAsset(assignDetail);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteAssignAsset(int assignId, string originator)
        {
            try
            {
                bool isTrue = AssetBR.Instance.DeleteAssignAsset(assignId, originator);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UploadAssetAssignData(string fileName, string originator, string logPath, bool IsCustomer)
        {
            try
            {
                return AssetBR.Instance.UploadAssetAssignDetails(fileName, originator, logPath, IsCustomer);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<OriginatorModel> GetUserCodesByUserType(string userType)
        {
            try
            {
                List<OriginatorModel> dt = AssetBR.Instance.GetUserCodesByUserType(userType);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<AssetModel> GetAssetsCodesByAssetType(string assetType)
        {
            try
            {
                List<AssetModel> dt = AssetBR.Instance.GetAssetsCodesByAssetType(assetType);
                return dt;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<AssetAssignModel> GetAllAssignAssetsByUserCode(string userCode)
        {
            try
            {
                List<AssetAssignModel> objList = AssetBR.Instance.GetAllAssignAssetsByUser(userCode);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public bool SaveAssignAssetList(string Originator, List<AssetAssignModel> data)
        {
            try
            {
                bool isTrue = AssetBR.Instance.SaveAssignAssetList(Originator, data);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string LoadCustomerName(string custCode)
        {
            return AssetBR.Instance.LoadCustomerName(custCode);
        }

        public bool IsAssetAvailable(string assetCode, string cusCode)
        {
            try
            {
                bool isTrue = AssetBR.Instance.IsAssetAvailable(assetCode, cusCode);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<AssetAssignModel> GetAllAssignAssetsByAssetCode(string assetCode)
        {
            try
            {
                List<AssetAssignModel> objList = AssetBR.Instance.GetAllAssignAssetsByAssetCode(assetCode);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public List<AssetAssignModel> GetAllAssignAssetsByAssetId(int assetId)
        {
            try
            {
                List<AssetAssignModel> objList = AssetBR.Instance.GetAllAssignAssetsByAssetId(assetId);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public bool IsLostOrDamaged(string assetCode, string cusCode)
        {
            try
            {
                bool isTrue = AssetBR.Instance.IsLostOrDamaged(assetCode, cusCode);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<AssetStatusModel> GetAssetStatus()
        {
            try
            {
                List<AssetStatusModel> objList = AssetBR.Instance.GetAllassetStatus();

                return objList;
            }
            catch
            {
                throw;
            }
        }

        #endregion

    }
}
