﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.ServiceContracts;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.Common.Adapter;
using System.Xml.Linq;
using System.IO;
using System.Reflection;
using Peercore.CRM.Service.DTO.TransferComponents;
using Peercore.CRM.Service.DTO.CompositeEntities;
using System.ServiceModel;
using Peercore.CRM.BusinessRules;

namespace Peercore.CRM.Service
{
    public partial class CRMService : ICallCycle
    {
        CallCycleAdapter callCycleAdapter = null;

        public List<CallCycleContactDTO> GetCallCycleContacts(ArgsDTO args, int iCallCycleID = 0, bool bInActive = false, string CallCycleName = "")
        {
            callCycleAdapter = new CallCycleAdapter();
            List<CallCycleContactDTO> callcyclecontactList = new List<CallCycleContactDTO>();
            callcyclecontactList=callCycleAdapter.GetCallCycleContacts(args, iCallCycleID, bInActive, CallCycleName);
            return callcyclecontactList;
        }

        public bool GetCallCycleState(int callCycleId)
        {
            callCycleAdapter = new CallCycleAdapter();
            return callCycleAdapter.GetCallCycleState(callCycleId);
        }

        public List<CallCycleDTO> GetCallCycle(ArgsDTO args, bool bInActive = false, int iCallCycleID = 0, string CallCycleName = "")
        {
            callCycleAdapter = new CallCycleAdapter();
            return callCycleAdapter.GetCallCycle(args, bInActive,iCallCycleID, CallCycleName);
        }

        public bool CallCycleSave(CallCycleDTO callCycle, List<CallCycleContactDTO> oCustomers, string originator, TimeSpan tsStartTime, out int callCycleId)
        {
            callCycleAdapter = new CallCycleAdapter();
            return callCycleAdapter.CallCycleSave(callCycle, oCustomers, originator, tsStartTime, out callCycleId);
        }

        public bool DeactivateCallCycle(int callCycleId, string isDelete)
        {
            callCycleAdapter = new CallCycleAdapter();
            return callCycleAdapter.DeactivateCallCycle(callCycleId,isDelete);
        }

        public bool CallCycleContactDelete(CallCycleContactDTO callCycleContact, int callCycleId)
        {
            callCycleAdapter = new CallCycleAdapter();
            return callCycleAdapter.CallCycleContactDelete(callCycleContact,callCycleId);
        }

        public bool TestMe(SchedularTransferComponentDTO schedularTransferComponentDTO)
        {
            return true;
        }

        public bool SaveSchedule(List<CallCycleContactDTO> callCycleContactList)
        {
            callCycleAdapter = new CallCycleAdapter();
            return callCycleAdapter.SaveSchedule(callCycleContactList);
        }

        public bool CreateCallCycleActivitiesFromTemplate(CallCycleDTO callCycle, List<CallCycleContactDTO> callCycleContact, ArgsDTO args)
        {
            callCycleAdapter = new CallCycleAdapter();
            return callCycleAdapter.CreateCallCycleActivitiesFromTemplate(callCycle, callCycleContact, args);
        }

        public List<LookupDTO> GetAllCallCyclesList(ArgsDTO args)
        {
            callCycleAdapter = new CallCycleAdapter();
            return callCycleAdapter.GetAllCallCyclesList(args);
        }

        public bool SaveRoute(RouteDTO route)
        {
            try
            {
                callCycleAdapter = new CallCycleAdapter();
                return callCycleAdapter.SaveRoute(route);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteRoutesByRepCode(RouteDTO route)
        {
            try
            {
                callCycleAdapter = new CallCycleAdapter();
                return callCycleAdapter.DeleteRoutesByRepCode(route);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateRouteSequence(List<RouteDTO> routeList)
        {
            try
            {
                callCycleAdapter = new CallCycleAdapter();
                return callCycleAdapter.UpdateRouteSequence(routeList);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public List<RouteDTO> GetRoutesByRepCode(ArgsDTO args)
        {
            callCycleAdapter = new CallCycleAdapter();
            return callCycleAdapter.GetRoutesByRepCode(args);
        }

        public List<RouteDTO> GetRoutesForTME(ArgsDTO args)
        {
            callCycleAdapter = new CallCycleAdapter();
            return callCycleAdapter.GetRoutesForTME(args);
        }

        public bool SchedulePlanedRoutesForDR(ActivityDTO activity, string originatorType)
        {
            callCycleAdapter = new CallCycleAdapter();
            return callCycleAdapter.SchedulePlanedRoutesForDR(activity, originatorType);
        }

        public List<RouteDTO> GetAssignedRoutes(string parentOriginator, ArgsDTO args)
        {
            callCycleAdapter = new CallCycleAdapter();
            return callCycleAdapter.GetAssignedRoutes(parentOriginator, args);
        }

        public List<RouteMasterDTO> GetRouteNames(string routName)
        {
            callCycleAdapter = new CallCycleAdapter();
            return callCycleAdapter.GetAllRoutes(routName);
        }

        public List<RouteMasterDTO> GetAllRouteMaster(string parentOriginator, ArgsDTO args)
        {
            callCycleAdapter = new CallCycleAdapter();
            return callCycleAdapter.GetAllRouteMaster(parentOriginator, args);
        }

        public List<RouteMasterDTO> GetAllRouteMasterForCheckList(int CheckListId, string parentOriginator, ArgsDTO args)
        {
            callCycleAdapter = new CallCycleAdapter();
            return callCycleAdapter.GetAllRouteMasterForCheckList(CheckListId, parentOriginator, args);
        }

        public List<RouteMasterDTO> GetAllTMERoutesMaster(string tmeCode, ArgsDTO args)
        {
            callCycleAdapter = new CallCycleAdapter();
            return callCycleAdapter.GetAllTMERoutesMaster(tmeCode, args);
        }

        public bool SaveRouteMaster(RouteMasterDTO routeMasterDTO)
        {
            try
            {
                callCycleAdapter = new CallCycleAdapter();
                return callCycleAdapter.SaveRouteMaster(routeMasterDTO);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<RouteMasterDTO> GetRouteTargetsByDistributorId(ArgsDTO args)
        {
            callCycleAdapter = new CallCycleAdapter();
            try
            {
                return callCycleAdapter.GetRouteTargetsByDistributorId(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CallCycleDTO> GetCallCyclesForRep(string repcode)
        {
            callCycleAdapter = new CallCycleAdapter();
            try
            {
                return callCycleAdapter.GetCallCyclesForRep(repcode);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool IsRouteMasterExists(RouteMasterDTO routeMasterDTO)
        {
            callCycleAdapter = new CallCycleAdapter();
            try
            {
                return callCycleAdapter.IsRouteMasterExists(routeMasterDTO); ;
            }
            catch (Exception)
            {
                return true;
            }
        }

        public RouteMasterDTO GetRouteTargetFromRouteId(int routeMasterId)
        {
            callCycleAdapter = new CallCycleAdapter();
            try
            {
                return callCycleAdapter.GetRouteTargetFromRouteId(routeMasterId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CallCycleContactDTO> GetCallCycleContactsForNewEntry(ArgsDTO args, string repCode)
        {
            callCycleAdapter = new CallCycleAdapter();
            List<CallCycleContactDTO> callcyclecontactList = new List<CallCycleContactDTO>();
            callcyclecontactList = callCycleAdapter.GetCallCycleContactsForNewEntry(args, repCode);
            foreach (CallCycleContactDTO item in callcyclecontactList)
                item.CreateActivity = true;
            return callcyclecontactList;
        }

        public bool IsCallCycleContactInAnyOtherSchedule(string customerCode)
        {
            callCycleAdapter = new CallCycleAdapter();
            try
            {
                return callCycleAdapter.IsCallCycleContactInAnyOtherSchedule(customerCode); ;
            }
            catch (Exception)
            {
                return true;
            }
        }

        public string GetRepCodeForCallCycleId(int callCycleId)
        {
            callCycleAdapter = new CallCycleAdapter();
            return callCycleAdapter.GetRepCodeForCallCycleId(callCycleId);
        }

        public bool SaveOriginatorRouteOutlets(OriginatorRouteOutletsDTO originatorRouteOutletsDTO)
        {
            try
            {
                callCycleAdapter = new CallCycleAdapter();
                return callCycleAdapter.SaveOriginatorRouteOutlets(originatorRouteOutletsDTO);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ItineraryHeaderDTO GetItineraryDetailsForUser(string tme, DateTime date)
        {
            try
            {
                callCycleAdapter = new CallCycleAdapter();
                return callCycleAdapter.GetItineraryDetailsForUser(tme, date);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveItinerary(ItineraryHeaderDTO itineraryHeaderdto)
        {
            try
            {
                callCycleAdapter = new CallCycleAdapter();
                return callCycleAdapter.SaveItinerary(itineraryHeaderdto);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int InsertItineraryLogEntry(ItineraryHeaderDTO itineraryHeaderDTO)
        {
            try
            {
                callCycleAdapter = new CallCycleAdapter();
                return callCycleAdapter.InsertItineraryLogEntry(itineraryHeaderDTO);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateItineraryHeaderStatus(ItineraryHeaderDTO itineraryHeaderDTO)
        {
            try
            {
                callCycleAdapter = new CallCycleAdapter();
                return callCycleAdapter.UpdateItineraryHeaderStatus(itineraryHeaderDTO);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ItineraryHeaderDTO GetItineraryLogEntryById(int itineraryLogEntryId)
        {
            try
            {
                callCycleAdapter = new CallCycleAdapter();
                return callCycleAdapter.GetItineraryLogEntryById(itineraryLogEntryId);
            }
            catch
            {
                throw;
            }
        }

        public OriginatorDTO GetItineraryAssignedToByHeaderId(int itineraryHeaderId)
        {
            try
            {
                callCycleAdapter = new CallCycleAdapter();
                return callCycleAdapter.GetItineraryAssignedToByHeaderId(itineraryHeaderId);
            }
            catch
            {
                throw;
            }
        }

        public List<VisitChecklistMasterDTO> GetAllVisitChecklistMaster(ArgsDTO args)
        {
            callCycleAdapter = new CallCycleAdapter();
            try
            {
                return callCycleAdapter.GetAllVisitChecklistMaster(args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool SaveAllVisitChecklistMaster(VisitChecklistMasterDTO visitChecklistDTO)
        {
            callCycleAdapter = new CallCycleAdapter();
            try
            {
                return callCycleAdapter.SaveVisitChecklistMaster(visitChecklistDTO);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteVisitChecklistMaster(VisitChecklistMasterDTO visitChecklistDTO)
        {
            callCycleAdapter = new CallCycleAdapter();
            try
            {
                return callCycleAdapter.DeleteVisitChecklistMaster(visitChecklistDTO);
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteRouteMaster(RouteMasterDTO routeMasterDTO)
        {
            callCycleAdapter = new CallCycleAdapter();
            try
            {
                return callCycleAdapter.DeleteRouteMaster(routeMasterDTO);
            }
            catch (Exception)
            {
                return true;
            }
        }

        public ItineraryHeaderDTO GetItineraryHeaderByUser(string tme, DateTime date)
        {
            try
            {
                callCycleAdapter = new CallCycleAdapter();
                return callCycleAdapter.GetItineraryHeaderByUser(tme, date);
            }
            catch
            {
                throw;
            }
        }

        public ItineraryHeaderDTO GetItineraryHeaderByHeaderId(int itineraryHeaderId)
        {
            try
            {
                callCycleAdapter = new CallCycleAdapter();
                return callCycleAdapter.GetItineraryHeaderByHeaderId(itineraryHeaderId);
            }
            catch
            {
                throw;
            }
        }

        #region "Perfetti 2nd Phase"

        public bool UpdateSRTerritory(string Originator, string RepId, string TerritoryId)
        {
            try
            {
                return CallCycleBR.Instance.UpdateSRTerritory(Originator, RepId, TerritoryId);
            }
            catch (Exception)
            {
                return false;
            }
        } 
        
        public bool UpdateDBTerritory(string DistributerId, string TerritoryId, string CreatedBy)
        {
            try
            {
                return CallCycleBR.Instance.UpdateDBTerritory(DistributerId, TerritoryId, CreatedBy);
            }
            catch (Exception)
            {
                return false;
            }
        }

        #endregion
    }
}
