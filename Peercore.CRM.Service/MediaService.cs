﻿using Peercore.CRM.BusinessRules;
using Peercore.CRM.Model;
using Peercore.CRM.Service.ServiceContracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Service
{
    public partial class CRMService : IMedia
    {


        #region "Media Upload"

        public List<UploadMediaModel> GetAllMedia(string repCode, string category, string custCode, ArgsModel args)
        {
            try
            {
                List<UploadMediaModel> objList = MediaBR.Instance.GetAllMedia(repCode, category, custCode, args);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public UploadMediaModel GetMedia(string mediaFileId)
        {
            throw new NotImplementedException();
        }

        public bool SaveUpdateMedia(UploadMediaModel mediaDetail)
        {
            try
            {
                bool isTrue = MediaBR.Instance.SaveUpdateMediaFiles(mediaDetail);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteMediaFiles(int mediaFileId, string modifiedBy)
        {
            try
            {
                bool isTrue = MediaBR.Instance.DeleteMediaFiles(mediaFileId, modifiedBy);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<MediaCategoryModel> GetAllCategoriesForddl(ArgsModel args)
        {
            List<MediaCategoryModel> objList = MediaBR.Instance.GetAllMediaCategoriesForDDL(args);

            return objList;
        }

        public string LoadMTCustomerName(string custCode)
        {
            return MediaBR.Instance.LoadCustomerName(custCode);
        }


        #endregion

        #region - Media Catagory -

        public List<MediaCategoryModel> GetAllCategories(ArgsModel args)
        {
            List<MediaCategoryModel> objList = MediaBR.Instance.GetAllMediaCategories(args);

            return objList;
        }

        public bool SaveMediaCategory(MediaCategoryModel categoryDetail)
        {
            try
            {
                bool isTrue = MediaBR.Instance.SaveMediaCategory(categoryDetail);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateMediaCategory(int catId, string catName, string description, string originator)
        {
            try
            {
                bool isTrue = MediaBR.Instance.UpdateMediaCategory(catId, catName, description, originator);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteMediaCategory(int catId, string originator)
        {
            try
            {
                bool isTrue = MediaBR.Instance.DeleteMediaCategory(catId, originator);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool IsMediaCategoryAvailable(string catName)
        {
            throw new NotImplementedException();
        }


        #endregion

    }
}
