﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.Common.Adapter;
using System.ServiceModel;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.ServiceContracts;
using Peercore.CRM.Model;
using Peercore.CRM.BusinessRules;

namespace Peercore.CRM.Service
{
    public partial class CRMService : IDistributor
    {
        DistributorAdapter distributorAdapter = null;

        #region BankAccount

        public List<BankAccountDTO> GetBankAccountsForUser(string sourceTypeText, string originator)
        {
            distributorAdapter = new DistributorAdapter();
            try
            {
                return distributorAdapter.GetBankAccountsForUser(sourceTypeText, originator);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public DistributorDTO GetDistributorTargetById(ArgsDTO args)
        {
            distributorAdapter = new DistributorAdapter();
            try
            {
                return distributorAdapter.GetDistributorTargetById(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion



        public bool SaveBankAccounts(BankAccountDTO account)
        {
            distributorAdapter = new DistributorAdapter();

            try
            {
                return distributorAdapter.SaveBankAccounts(account);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string GetDistributorHoliday(int originatorId, string originator, string repCode)
        {
            distributorAdapter = new DistributorAdapter();

            try
            {
                return distributorAdapter.GetDistributorHoliday(originatorId, originator, repCode);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DistributorDTO GetDistributorDetails(string originator)
        {
            distributorAdapter = new DistributorAdapter();

            try
            {
                return distributorAdapter.GetDistributorDetails(originator);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region "Perfetti 2nd Phase Development"

        public DistributerModel GetDistributorDetailsByOriginator(string Originator)
        {
            try
            {
                return DistributorBR.Instance.GetDistributorDetailsByOriginator(Originator);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveDistributor(DistributerModel distributor)
        {
            try
            {
                return DistributorBR.Instance.SaveDistributor(distributor);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        
        public bool SaveDistributorTerritory(string territory_id, string distributor_id, string UpdateBy)
        {
            try
            {
                return DistributorBR.Instance.SaveDistributorTerritory(territory_id, distributor_id, UpdateBy);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region "For Bangaladesh Demo"

        public List<DBClaimInvoiceHeaderModel> GetDBClaimInvoicesByDistributerId(string distributerId)
        {
            distributorAdapter = new DistributorAdapter();

            try
            {
                return DistributorBR.Instance.GetDBClaimInvoicesByDistributerId(distributerId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion
        
        public List<TradeDiscountUploadModel> SaveTradeDiscount(List<TradeDiscountUploadModel> tradeDiscountList)
        {
            try
            {
                return DistributorBR.Instance.SaveTradeDiscount(tradeDiscountList);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<TradeDiscountUploadModel> GetAllTradeDiscounts(ArgsModel args, string dbCode)
        {
            try
            {
                List<TradeDiscountUploadModel> objList = DistributorBR.Instance.GetAllTradeDiscounts(args, dbCode);

                return objList;
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteTradeDiscount(int trdId, string originator)
        {
            try
            {
                bool isTrue = DistributorBR.Instance.DeleteTradeDiscount(trdId, originator);
                return isTrue;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateTradeDiscount(TradeDiscountUploadModel tradeDiscount)
        {
            try
            {
                return DistributorBR.Instance.UpdateTradeDiscount(tradeDiscount);
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public bool UpdateTradeDiscountApprovedProcess(string dtpMonth)
        {
            try
            {
                return DistributorBR.Instance.UpdateTradeDiscountApprovedProcess(dtpMonth);
            }
            catch (Exception)
            {
                throw;
            }
        }
    }
}
