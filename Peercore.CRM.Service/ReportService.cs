﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.ServiceContracts;
using Peercore.CRM.Service.DTO.CompositeEntities;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.Common.Adapter;
using System.Configuration;
using Peercore.CRM.Model;
using Peercore.CRM.BusinessRules;
using System.Net;
using System.IO;

namespace Peercore.CRM.Service
{
    public partial class CRMService : ICommon
    {
        public List<RepWizeActivityAnalysisDTO> GetRepWideActivity(DateTime FromDate,
            DateTime ToDate,
            List<LeadStageDTO> leadStageList,
            ArgsDTO args,
            List<OriginatorDTO> OriginatorList)
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GetRepWideActivity(FromDate, ToDate, leadStageList, args, OriginatorList);
            }
            catch (Exception)
            {
                throw;
            }
        }




     







        /*
        public List<LeadCustomerDTO> GetCombinedCollectionCountByState(int iLeadStageId,
            string sFromDate,
            string sEndDate,
            ArgsDTO args,
            string sLeadStage = "")
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GetCombinedCollectionCountByState(iLeadStageId, sFromDate, sEndDate, args, sLeadStage);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LeadCustomerDTO> GetCombinedCollectionByState(int iLeadStageId,
            string sFromDate,
            string sEndDate,
            ArgsDTO args,
            string sLeadStage = "")
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GetCombinedCollectionByState(iLeadStageId, sFromDate, sEndDate, args, sLeadStage);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LeadCustomerDTO> GetLeadCustomerCountByRep(int iLeadStageId,
            string sFromDate,
            string sEndDate,
            ArgsDTO args,
            string sLeadStage = "")
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GetLeadCustomerCountByRep(iLeadStageId, sFromDate, sEndDate, args, sLeadStage);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LeadCustomerDTO> GetLeadCustomerCollectionByRep(int iLeadStageId,
            string sFromDate,
            string sEndDate,
            ArgsDTO args,
            string sLeadStage = "")
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GetLeadCustomerCollectionByRep(iLeadStageId, sFromDate, sEndDate, args, sLeadStage);
            }
            catch (Exception)
            {
                throw;
            }
        }

        

        

        public List<CallCycleGraphDTO> GetCallCycleList(ArgsDTO args,
            string sFromDate = "",
            string sToDate = "",
            string sFilter = "All",
            string activityStatus = "")
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GetCallCycleList(args, sFromDate, sToDate, sFilter, activityStatus);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CallCycleGraphDTO> GetCallCycleCountList(ArgsDTO args,
            string sFromDate = "",
            string sToDate = "",
            string sFilter = "All",
            string activityStatus = "")
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GetCallCycleCountList(args, sFromDate, sToDate, sFilter, activityStatus);
            }
            catch (Exception)
            {
                throw;
            }
        }

        

        public List<BDMWideServiceCallAnalysisDTO> GetCustomerServiceCallActivities(ArgsDTO args,
            DateTime? FromDate,
            DateTime? ToDate,
            bool IsAllCustomers,
            bool IsNotServiceCalledCustomers,
            string activityStatus = "",
            string filterText = "")
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GetCustomerServiceCallActivities(args, FromDate, ToDate, IsAllCustomers, IsNotServiceCalledCustomers, activityStatus,filterText);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public List<LeadDTO> GetNewCustomersForSales(ArgsDTO args,
            int year,
            int month)
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GetNewCustomersForSales(args, year, month);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<BDMWideSalesAnalysisDTO> GeActualLiters(string sTotalSql,
            string sWhereColumns,
            string CustCodes,
            ArgsDTO args)
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GeActualLiters(sTotalSql, sWhereColumns, CustCodes, args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<BDMWideSalesAnalysisDTO> GeActualLiters(ArgsDTO args)
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GeActualLiters(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CustomerOpportunitiesDTO> GetCustomerOpportunities(ArgsDTO args)
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GetCustomerOpportunities(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CustomerOpportunitiesDTO> GetCustomerOpportunitiesCount(ArgsDTO args)
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GetCustomerOpportunitiesCount(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CustomerActivitiesDTO> GetA20Customers(DateTime dtmStartDate,
            DateTime dtmEndDate,
            ArgsDTO args)
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GetA20Customers(dtmStartDate, dtmEndDate, args);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public List<CustomerActivitiesDTO> GetA20CustomersCount(DateTime dtmStartDate,
            DateTime dtmEndDate,
            ArgsDTO args)
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GetA20CustomersCount(dtmStartDate, dtmEndDate, args);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public string GetCustomerServiceCallActivitiesExport(ArgsDTO args,
            DateTime? FromDate,
            DateTime? ToDate,
            bool IsAllCustomers,
            bool IsNotServiceCalledCustomers,
            string activityStatus = "",
            string filterText ="" ,
            string path="")
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GetCustomerServiceCallActivitiesExport(args, FromDate, ToDate, IsAllCustomers, IsNotServiceCalledCustomers, activityStatus, filterText,path);
            }
            catch (Exception)
            {
                return "";
            }
        }

        public string GetCombinedCollectionByStateExport(int iLeadStageId,
            string sFromDate,
            string sEndDate,
            ArgsDTO args,
            string sLeadStage = "",
            string path = "")
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GetCombinedCollectionByStateExport(iLeadStageId, sFromDate, sEndDate, args, sLeadStage, path);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string GetLeadCustomerCollectionByRepExport(int iLeadStageId,
            string sFromDate,
            string sEndDate,
            ArgsDTO args,
            string sLeadStage = "",
            string path = "")
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GetLeadCustomerCollectionByRepExport(iLeadStageId, sFromDate, sEndDate, args, sLeadStage,path);
            }
            catch (Exception)
            {
                throw;
            }
        }



        public string GetCallCycleListExport(ArgsDTO args,
            string sFromDate = "",
            string sToDate = "",
            string sFilter = "All",
            string activityStatus = "",
            string path = "")
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GetCallCycleListExport(args, sFromDate, sToDate, sFilter, activityStatus, path);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string GetAllActivitiesByTypeExport(ArgsDTO args,
            string originator,
            string sFromDate = "",
            string sEndDate = "",
            string activityStatus = "",
            string path = "")
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GetAllActivitiesByTypeExport(args, originator, sFromDate, sEndDate, activityStatus, path);
            }
            catch (Exception)
            {
                throw;
            }
        }


        public string GeActualLitersExport(ArgsDTO args,string path)
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GeActualLitersExport(args,path);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string GetA20CustomersExport(DateTime dtmStartDate,
            DateTime dtmEndDate,
            ArgsDTO args,
            string path = "")
        {
            ReportAdapter reportAdapter = new ReportAdapter();
            try
            {
                return reportAdapter.GetA20CustomersExport(dtmStartDate, dtmEndDate, args,path);
            }
            catch (Exception)
            {
                throw;
            }
        }

        */
    }
}
