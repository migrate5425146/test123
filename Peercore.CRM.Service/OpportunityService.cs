﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.ServiceContracts;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.Common.Adapter;

namespace Peercore.CRM.Service
{
    public partial class CRMService : IOpportunity
    {
        OpportunityAdapter OpportunityAdapter = null;

        public bool InsertOpportunity(OpportunityDTO opportunity, out int opportunityID)
        {
            OpportunityAdapter = new OpportunityAdapter();

            try
            {
                return OpportunityAdapter.InsertOpportunity(opportunity, out opportunityID);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateOpportunity(OpportunityDTO opportunity)
        {
            OpportunityAdapter = new OpportunityAdapter();

            try
            {
                return OpportunityAdapter.UpdateOpportunity(opportunity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OpportunityDTO> GetOpportunities(ArgsDTO args, int contactID)
        {
             OpportunityAdapter = new OpportunityAdapter();

            try
            {
                return OpportunityAdapter.GetOpportunities(args,contactID);
            }
            catch
            {                
                throw;
            }
        }

        public List<OpportunityDTO> GetOpportunitiesForCustomer(string customerCode, ArgsDTO args)
        {
            OpportunityAdapter = new OpportunityAdapter();

            try
            {
                return OpportunityAdapter.GetOpportunitiesForCustomer(customerCode,args);
            }
            catch
            {                
                throw;
            }
        }

        public bool DeleteOpportunity(int opportunityId)
        {
            OpportunityAdapter = new OpportunityAdapter();

            try
            {
                return OpportunityAdapter.DeleteOpportunity(opportunityId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int GetOpportunityCountForHome(int pipelineStageID, ArgsDTO args)
        {
            OpportunityAdapter = new OpportunityAdapter();

            try
            {
                return OpportunityAdapter.GetOpportunityCountForHome(pipelineStageID, args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CustomerOpportunitiesDTO> GetAllOpportunities(int iPipelineStageID, ArgsDTO args)
        {
            OpportunityAdapter = new OpportunityAdapter();
            try
            {
                return OpportunityAdapter.GetAllOpportunities(iPipelineStageID, args);
            }
            catch
            {
                throw;
            }
        }

        public List<ClientOpportunityDTO> GetAllClientOpportunity(int iPipelineStageID, ArgsDTO args)
        {
            OpportunityAdapter = new OpportunityAdapter();
            try
            {
                return OpportunityAdapter.GetAllClientOpportunity(iPipelineStageID, args);

            }
            catch
            {
                throw;
            }
        }

        public List<CatalogDTO> GetOppProducts(int opportunityId, ArgsDTO args)
        {
            OpportunityAdapter = new OpportunityAdapter();
            try
            {
                return OpportunityAdapter.GetOppProducts(opportunityId, args);
            }
            catch
            {
                throw;
            }
        }

        public CustomerOpportunitiesDTO GetOpportunity(int opportunityId)
        {
            OpportunityAdapter = new OpportunityAdapter();
            try
            {
                return OpportunityAdapter.GetOpportunity(opportunityId);
            }
            catch (Exception)
            {
                throw;
            }

        }

        public bool SaveOpportunity(OpportunityDTO opportunity, List<CatalogDTO> lstProducts, out int opportunityId)
        {
            OpportunityAdapter = new OpportunityAdapter();

            try
            {
                return OpportunityAdapter.SaveOpportunity(opportunity, lstProducts,out opportunityId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CustomerOpportunitiesDTO> GetOpportunitiesForEndUser(ArgsDTO args)
        {
            OpportunityAdapter = new OpportunityAdapter();
            try
            {
                return OpportunityAdapter.GetOpportunitiesForEndUser(args);
            }
            catch
            {
                throw;
            }
        }


        /*
        public List<OpportunityDTO> GetOpportunities(ArgsDTO args, int iContactID)
        {
            OpportunityAdapter = new OpportunityAdapter();

            try
            {
                return OpportunityAdapter.GetOpportunities(args, iContactID);
            }
            catch (Exception)
            {
                throw;
            }

        }

        public List<OpportunityDTO> GetAllOpportunities(int iPipelineStageID, string sOriginator, ArgsDTO args, DateTime? dtmCloseDate = null)
        {
            OpportunityAdapter = new OpportunityAdapter();

            try
            {
                return OpportunityAdapter.GetAllOpportunities(iPipelineStageID, sOriginator, args, dtmCloseDate);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int SaveOpportunity(OpportunityDTO opportunity, List<CatalogDTO> lstProducts)
        {
            OpportunityAdapter = new OpportunityAdapter();

            try
            {
                return OpportunityAdapter.Save(opportunity, lstProducts);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public CustomerOpportunitiesDTO GetOpportunity(int opportunityId)
        {
            OpportunityAdapter = new OpportunityAdapter();
            try
            {
                return OpportunityAdapter.GetOpportunity(opportunityId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CatalogDTO> GetProducts(int iOpportunityID)
        {
            OpportunityAdapter = new OpportunityAdapter();
            try
            {
                return OpportunityAdapter.GetProducts(iOpportunityID);
            }
            catch
            {
                throw;
            }
        }

        public List<CatalogDTO> GetAllProducts(string sCatlogCode)
        {
            OpportunityAdapter = new OpportunityAdapter();
            try
            {
                return OpportunityAdapter.GetAllProducts(sCatlogCode);
            }
            catch
            {
                throw;
            }
        }

        public List<CustomerOpportunitiesDTO> GetOpportunitiesForCustomer(string customerCode, ArgsDTO args)
        {
            OpportunityAdapter = new OpportunityAdapter();

            try
            {
                return OpportunityAdapter.GetOpportunitiesForCustomer(customerCode, args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool DeleteOpportunity(int opportunityId)
        {
            OpportunityAdapter = new OpportunityAdapter();

            try
            {
                return OpportunityAdapter.DeleteOpportunity(opportunityId);
            }
            catch (Exception)
            {

                throw;
            }
        }

       
        */

        /*
        public List<OpportunityDTO> GetOpportunitiesForPipeGraph(ArgsDTO args, int pipeLineId, bool pipeFlag)
        {
            OpportunityAdapter = new OpportunityAdapter();

            try
            {
                return OpportunityAdapter.GetOpportunitiesForPipeGraph(args, pipeLineId, pipeFlag);
            }
            catch (Exception)
            {
                
                throw;
            }
        }


        

        


        public List<CustomerOpportunitiesDTO> GetAllOpportunitiesCount(string sOriginator, string defaultDepartmentID)
        {
            OpportunityAdapter = new OpportunityAdapter();

            try
            {
                return OpportunityAdapter.GetAllOpportunitiesCount(sOriginator, defaultDepartmentID);
            }
            catch (Exception)
            {

                throw;
            }
        }


        public List<CustomerOpportunitiesDTO> GetOpportunitiesForCustomerCount(string orginator, string defDepId)
        {
            OpportunityAdapter = new OpportunityAdapter();

            try
            {
                return OpportunityAdapter.GetOpportunitiesForCustomerCount(orginator, defDepId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int GetOpportunityForWindow(ArgsDTO args, DateTime? dtmCloseDate = null)
        {
            OpportunityAdapter = new OpportunityAdapter();
            try
            {
                return OpportunityAdapter.GetOpportunityForWindow(args,dtmCloseDate);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public double GetOpportunitySum(ArgsDTO args)
        {
            OpportunityAdapter = new OpportunityAdapter();

            try
            {
                return OpportunityAdapter.GetOpportunitySum(args);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public List<CatalogDTO> GetOppProducts(int oppId)
        {
            OpportunityAdapter = new OpportunityAdapter();

            try
            {
                return OpportunityAdapter.GetOppProducts(oppId);
            }
            catch (Exception)
            {

                throw;
            }
        }
        */
    }
}
