﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class DebtorsBarometerDTO:BaseDTO
    {
        private int li_GracePeriod;
        public int Li_GracePeriod
        {
            get { return li_GracePeriod; }
            set { li_GracePeriod = value; IsDirty = true; }
        }

        private int li_TransType;
        public int Li_TransType
        {
            get { return li_TransType; }
            set { li_TransType = value; IsDirty = true; }
        }
        
        private DateTime ld_doc_date;
        public DateTime Ld_doc_date
        {
            get { return ld_doc_date; }
            set { ld_doc_date = value; IsDirty = true; }
        }

        private decimal lv_amount_due;
        public decimal Lv_amount_due
        {
            get { return lv_amount_due; }
            set { lv_amount_due = value; IsDirty = true; }
        }

        private string lv_Status;
        public string Lv_Status
        {
            get { return lv_Status; }
            set { lv_Status = value; IsDirty = true; }
        }

        private decimal lv_UnallocDiscount;
        public decimal Lv_UnallocDiscount
        {
            get { return lv_UnallocDiscount; }
            set { lv_UnallocDiscount = value; IsDirty = true; }
        }

        private DateTime ld_datethen;
        public DateTime Ld_datethen
        {
            get { return ld_datethen; }
            set { ld_datethen = value; IsDirty = true; }
        }

        private TimeSpan ld_datediff;
        public TimeSpan Ld_datediff
        {
            get { return ld_datediff; }
            set { ld_datediff = value; IsDirty = true; }
        }

        private decimal dCur;
        public decimal DCur
        {
            get { return dCur; }
            set { dCur = value; IsDirty = true; }
        }

        private decimal d30;
        public decimal D30
        {
            get { return d30; }
            set { d30 = value; IsDirty = true; }
        }

        private decimal d60;
        public decimal D60
        {
            get { return d60; }
            set { d60 = value; IsDirty = true; }
        }

        private decimal d90;
        public decimal D90
        {
            get { return d90; }
            set { d90 = value; IsDirty = true; }
        }

        private decimal dOver;
        public decimal DOver
        {
            get { return dOver; }
            set { dOver = value; IsDirty = true; }
        }

        private decimal dTotal;
        public decimal DTotal
        {
            get { return dTotal; }
            set { dTotal = value; IsDirty = true; }
        }

        private DateTime ld_datenow;
        public DateTime Ld_datenow
        {
            get { return ld_datenow; }
            set { ld_datenow = value; IsDirty = true; }
        }

        private DateTime ld_as_at_date;
        public DateTime Ld_as_at_date
        {
            get { return ld_as_at_date; }
            set { ld_as_at_date = value; IsDirty = true; }
        }
                      
    }
}
