﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class FreeProductDTO:BaseDTO
    {
        public int FreeProductId { get; set; }
        public int ProductId { get; set; }
        public double Qty { get; set; }
        public int ProductPackingId { get; set; }
        public int SchemeDetailsId { get; set; }

        public int CatlogId { get; set; }
        public int BrandId { get; set; }
        public int ProductCatagoryId { get; set; }
        public int ItemTypeId { get; set; }

        public string ProductName { get; set; }
        public string ProductPackingName { get; set; }
        public string CatlogName { get; set; }
        public string BrandName { get; set; }
        public string ProductCatagoryName { get; set; }
        public string ItemTypeName { get; set; }

        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
    }
}
