﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class CountryDTO
    {
        private int? countryId;
        private string country;

        public string CountryName
        {
            get { return country; }
            set { country = value; }
        }

        public int? CountryId
        {
            get { return countryId; }
            set { countryId = value; }
        }
    }
}
