﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public enum GridKnownFilters
    {
        NoFilter = 0,
        Contains = 1,
        DoesNotContain = 2,
        StartsWith = 3,
        EndsWith = 4,
        EqualTo = 5,
        NotEqualTo = 6,
        GreaterThan = 7,
        LessThan = 8,
        GreaterThanOrEqualTo = 9,
        LessThanOrEqualTo = 10,
        Between = 11,
        NotBetween = 12,
        IsEmpty = 13,
        NotIsEmpty = 14,
        IsNull = 15,
        NotIsNull = 16,
        Custom = 17,
    }

    public class GridFilterExpressionDTO
    {
        public GridFilterExpressionDTO() { }

        public string ColumnUniqueName { get; set; }
        public string DataTypeName { get; set; }
        public string FieldName { get; set; }
        public string FieldValue { get; set; }
        public string FilterFunction { get; set; }
    }
}
