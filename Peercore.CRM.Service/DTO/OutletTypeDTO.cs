﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class OutletTypeDTO : BaseDTO
    {
        private int outlettype_id;
        public int Outlettype_id
        {
            get { return outlettype_id; }
            set { outlettype_id = value; }
        }

        private string outlettype_name;
        public string Outlettype_name
        {
            get { return outlettype_name; }
            set { outlettype_name = value; }
        }

        private string parent_type;
        public string Parent_type
        {
            get { return parent_type; }
            set
            {
                parent_type = value;
            }
        }
    }
}
