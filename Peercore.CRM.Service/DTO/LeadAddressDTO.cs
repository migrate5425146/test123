﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class LeadAddressDTO
    {
        #region - DateTime -
        private DateTime createdDate;
        public DateTime CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        private DateTime lastModifiedDate;
        public DateTime LastModifiedDate
        {
            get { return lastModifiedDate; }
            set { lastModifiedDate = value; }
        }
        #endregion

        #region - int -
        private int assigneeNo;
        public int AssigneeNo
        {
            get { return assigneeNo; }
            set { assigneeNo = value; }
        }

        private int leadAddressID;
        public int LeadAddressID
        {
            get { return leadAddressID; }
            set { leadAddressID = value; }
        }

        private int leadID;
        public int LeadID
        {
            get { return leadID; }
            set { leadID = value; }
        }
        #endregion

        #region - string -
        private string address1;
        public string Address1
        {
            get { return address1; }
            set { address1 = value; }
        }

        private string address2;
        public string Address2
        {
            get { return address2; }
            set { address2 = value; }
        }

        private string addressType;
        public string AddressType
        {
            get { return addressType; }
            set
            {
                addressType = value; 
                
                if (!string.IsNullOrWhiteSpace(addressType))
                {
                    AddressTypeDescription = addressType.Equals("A") ? "Street" : "Postal";
                }
            }
        }

        private string addressTypeDescription;
        public string AddressTypeDescription
        {
            get { return addressTypeDescription; }
            set { addressTypeDescription = value; }
        }

        private string areaCode;
        public string AreaCode
        {
            get { return areaCode; }
            set { areaCode = value; }
        }

        private string assignedTo;
        public string AssignedTo
        {
            get { return assignedTo; }
            set { assignedTo = value; }
        }

        private string city;
        public string City
        {
            get { return city; }
            set { city = value; }
        }

        private string contact;
        public string Contact
        {
            get { return contact; }
            set { contact = value; }
        }

        private string country;
        public string Country
        {
            get { return country; }
            set { country = value; }
        }

        private string createdBy;
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        private string custCode;
        public string CustCode
        {
            get { return custCode; }
            set { custCode = value; }
        }

        private string fax;
        public string Fax
        {
            get { return fax; }
            set { fax = value; }
        }

        private string lastModifiedBy;
        public string LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string postCode;
        public string PostCode
        {
            get { return postCode; }
            set { postCode = value; }
        }

        private string state;
        public string State
        {
            get { return state; }
            set { state = value; }
        }

        private string telephone;
        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; }
        }
        #endregion
    }
}
