﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class EnduserEnquiryDTO : BaseDTO
    {
        public string CustomerCode { get; set; }

        public string EndUserCode { get; set; }

        public string CatlogCode { get; set; }

        public string Description { get; set; }

        public double Price { get; set; }

        public double CasesSupplied { get; set; }

        public DateTime RebateMonth { get; set; }

        public string CustomerName { get; set; }

        public string RepCode { get; set; }

        public string RepName { get; set; }

        public string EndUserName { get; set; }

        public double RebatePerc { get; set; }

        public double CurPrice { get; set; }

        public double TotalCases { get; set; }

        #region Sales Section
        public string Header1 { get; set; }
        public string Header2 { get; set; }
        public string Header3 { get; set; }
        public string Header4 { get; set; }
        public string Header5 { get; set; }
        public string Header6 { get; set; }
        public string Header7 { get; set; }
        public string Header8 { get; set; }
        public string Header9 { get; set; }
        public string Header10 { get; set; }
        public string Header11 { get; set; }
        public string Header12 { get; set; }

        public double Cases1 { get; set; }
        public double Cases2 { get; set; }
        public double Cases3 { get; set; }
        public double Cases4 { get; set; }
        public double Cases5 { get; set; }
        public double Cases6 { get; set; }
        public double Cases7 { get; set; }
        public double Cases8 { get; set; }
        public double Cases9 { get; set; }
        public double Cases10 { get; set; }
        public double Cases11 { get; set; }
        public double Cases12 { get; set; }
        #endregion

        public string CustomerDisplay
        {
            get
            {
                return this.CustomerCode + " - " + this.CustomerName;
            }
            set
            {
                ;
            }
        }

        public string EndUserDisplay
        {
            get
            {
                return this.EndUserCode + " - " + this.EndUserName + "     - " + this.RepCode;
            }
            set
            {
                ;
            }
        }
    }
}
