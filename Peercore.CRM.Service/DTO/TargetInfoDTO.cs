﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service
{
    public class TargetInfoDTO
    {
        public string Code { get; set; }
        public string Name { get; set; }

        public string RepCode { get; set; }
        public string RepName { get; set; }

        public string CustCode { get; set; }
        public string CustName { get; set; }

        public double TargetMonth1 { get; set; }
        public double TargetMonth2 { get; set; }
        public double TargetMonth3 { get; set; }
        public double TargetMonth4 { get; set; }
        public double TargetMonth5 { get; set; }
        public double TargetMonth6 { get; set; }
        public double TargetMonth7 { get; set; }
        public double TargetMonth8 { get; set; }
        public double TargetMonth9 { get; set; }
        public double TargetMonth10 { get; set; }
        public double TargetMonth11 { get; set; }
        public double TargetMonth12 { get; set; }


        public double TargetYear1 { get; set; }
        public double TargetYear2 { get; set; }
        public double TargetTYTD { get; set; }
        public double TargetLYTD { get; set; }

        public double TotalTarget { get; set; }

        public double TargetPercentage { get; set; }

        //This is use for grid binding.
        public double Target1 { get; set; }
        public double Target2 { get; set; }
        public double Target3 { get; set; }
        public double Target1Tot { get; set; }
        public double Target2Tot { get; set; }
        public double Target3Tot { get; set; }

        public List<int> SelectedMonth { get; set; }
        public double TargetTotal { get; set; }
        //{
        //    get
        //    {
        //        return Target1 + Target2 + Target3;
        //    }
        //}
        public string FlowContent { get; set; }

        public double MDTot { get; set; }
        public double TargetTot { get; set; }
        public double TargetYear1Tot { get; set; }
        public double TargetYear2Tot { get; set; }
        public double TargetTYTDTot { get; set; }
        public double TargetLYTDTot { get; set; }


        public double Actual1 { get; set; }
        public double Actual2 { get; set; }
        public double Actual3 { get; set; }


        public double ActualYear1 { get; set; }
        public double ActualYear2 { get; set; }
        public double ActualTYTD { get; set; }
        public double ActualLYTD { get; set; }

        public double TotalActual { get; set; }
        public double ActualPercentage { get; set; }

        public double Actual1Tot { get; set; }
        public double Actual2Tot { get; set; }
        public double Actual3Tot { get; set; }

        public double ActualTotal { get; set; }
        //{
        //    get
        //    {
        //        return Actual1 + Actual2 + Actual3;
        //    }
        //}

        //public double MDTot { get; set; }
        public double ActualTot { get; set; }
        public double ActualYear1Tot { get; set; }
        public double ActualYear2Tot { get; set; }
        public double ActualTYTDTot { get; set; }
        public double ActualLYTDTot { get; set; }

        public int RowCount { get; set; }


        public double ActualMonth1 { get; set; }
        public double ActualMonth2 { get; set; }
        public double ActualMonth3 { get; set; }
        public double ActualMonth4 { get; set; }
        public double ActualMonth5 { get; set; }
        public double ActualMonth6 { get; set; }
        public double ActualMonth7 { get; set; }
        public double ActualMonth8 { get; set; }
        public double ActualMonth9 { get; set; }
        public double ActualMonth10 { get; set; }
        public double ActualMonth11 { get; set; }
        public double ActualMonth12 { get; set; }


        public double TyTodayActual { get; set; }
        public double TyTtmtdActual { get; set; }
        public double TyLmtdActual { get; set; }
        public double LyTmtdActual { get; set; }
        public double TyTodayActualTot { get; set; }
        public double TyTtmtdActualTot { get; set; }
        public double TyLmtdActualTot { get; set; }
        public double LyTmtdActualTot { get; set; }
    }
}
