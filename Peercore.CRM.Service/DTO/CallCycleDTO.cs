﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class CallCycleDTO : BaseDTO
    {
        int iCallCycleID = 0;
        string sDescription = string.Empty;
        DateTime dDueOn = DateTime.Today;
        string cComments = "";
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string DelFlag { get; set; }
        public string CCType { get; set; }


        // Not in Table
        public string PrefixCode { get; set; }
        public string RepName { get; set; }

        private string lastModifiedBy;
        public string LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; IsDirty = true; }
        }

        private DateTime? lastModifiedDate;
        public DateTime? LastModifiedDate
        {
            get { return lastModifiedDate; }
            set { lastModifiedDate = value; IsDirty = true; }
        }

        #region Public Properties

        public int CallCycleID
        {
            get { return iCallCycleID; }
            set { iCallCycleID = value; }
        }

        public string Description
        {
            get { return sDescription; }
            set { sDescription = value; }
        }

        public DateTime DueOn
        {
            get { return dDueOn; }
            set { dDueOn = value; }
        }


        public string Comments
        {
            get { return cComments; }
            set { cComments = value; }
        }

        public int NoOfLeadCustomers { get; set; }

        private string dueOnString = "";
        public string DueOnString
        {
            get { return dDueOn.ToString("yyyy-MMM-dd"); }
            set { dueOnString = value; }
        }


        private string createdDateString = "";
        public string CreatedDateString
        {
            get { return CreatedDate.Value.ToString("dd-MMM-yyyy HH:mm:ss"); }
            set { createdDateString = value; }
        }
        #endregion
    }
}
