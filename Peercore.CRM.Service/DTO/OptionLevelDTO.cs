﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class OptionLevelDTO : BaseDTO
    {
        public int OptionLevelId { get; set; }
        public int SchemeDetailId { get; set; }
        public int ProductCategoryId { get; set; }
        public int ProductPackingId { get; set; }
        public int ProductId { get; set; }
        public int BrandId { get; set; }
        public bool IsHvp { get; set; }
        public int CatlogId { get; set; }
        public double LineCount { get; set; }
        public double MinQuantity { get; set; }
        public double MinValue { get; set; }
        public double MaxQuantity { get; set; }
        public double MaxValue { get; set; }
        public bool IsActive { get; set; }

        public string ProductName { get; set; }
        public string ProductPackingName { get; set; }
        public string CatlogName { get; set; }
        public string BrandName { get; set; }
        public string ProductCatagoryName { get; set; }
        public string ItemTypeName { get; set; }

        public int IndexId { get; set; }
        public int OperatorId { get; set; }
        public bool IsBill { get; set; }
        public bool IsForEvery { get; set; }
        
        public List<OptionLevelCombinationDTO> OptionLevelCombinationList { get; set; }
    }
}
