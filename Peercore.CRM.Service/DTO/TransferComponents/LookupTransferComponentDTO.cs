﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO.TransferComponents
{
    public class LookupTransferComponentDTO
    {
        public string Sql { get; set; }

        public KeyValuePair<string, string>? IdColumn { get; set; }

        public KeyValuePair<string, string>? CodeColumn { get; set; }

        public KeyValuePair<string, string> DescriptionColumn { get; set; }

        public List<KeyValuePair<string, string>> AdditionalColumns { get; set; }
    }
}
