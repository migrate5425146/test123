﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class RepWizeActivityAnalysisDTO
    {
        public string RepName { get; set; }
        public Int64 LeadCount { get; set; }
        public Int64 ProspectCount { get; set; }
        public Int64 OppotunityCount { get; set; }
        public Int64 ActivityCount { get; set; }
        public Int64 ActivityCountNotCompleted { get; set; }
        public string ActivityStatus { get; set; }


        public List<StageDTO> StagesList { get; set; }

        public decimal ConversionRatio { get; set; }
        public int RowCount { get; set; }
    }

    public class StageDTO
    {
        public string RepName { get; set; }
        public string StageName { get; set; }
        //public Int32 Count { get; set; }
        private int count = 0;

        public int Count
        {
            get
            {
                if (count == 0)
                    return 0;
                else
                    return count;
            }
            set { count = value; }
        }

    }
}
