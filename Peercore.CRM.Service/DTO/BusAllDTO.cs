﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class BusAllDTO :BaseDTO
    {
        public string Period { get; set; }
        public double Tonne { get; set; }
        public double BudgetTonne { get; set; }
        public double Sales { get; set; }
        public double BudgetSales { get; set; }
        public double ST { get; set; }
        public double Profit { get; set; }
        public double PT { get; set; }
        public double Percentage { get; set; }
        public double LastYearTonne { get; set; }
        public double LastYearBudgetTonne { get; set; }
        public double LastYearSales { get; set; }
        public double LastYearBudgetSales { get; set; }
        public double LastYearST { get; set; }
        public double LastYearProfit { get; set; }
        public double LastYearPT { get; set; }
        public double LastYearPercentage { get; set; }
    }
}
