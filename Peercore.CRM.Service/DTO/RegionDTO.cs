﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Service.DTO
{
    public class RegionDTO
    {
        public int RegionId { get; set; }
        public int CompId { get; set; }
        public string RegionCode { get; set; }
        public string RegionName { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int rowCount { get; set; }
    }

    public class RsmDTO
    {
        public int RsmId { get; set; }
        public int RegionId { get; set; }
        public string RsmCode { get; set; }
        public string RsmName { get; set; }
        public string Originator { get; set; }
        public string RsmTel1 { get; set; }
        public string RsmTel2 { get; set; }
        public string RsmEmail { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public int ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
    }
}
