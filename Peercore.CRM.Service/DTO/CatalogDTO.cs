﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class CatalogDTO:BaseDTO
    {

        private int catlogId;
        public int CatlogId
        {
            get { return catlogId; }
            set { catlogId = value; IsDirty = true; }
        }
        private string catlogCode;
        public string CatlogCode
        {
            get { return catlogCode; }
            set { catlogCode = value; IsDirty = true; }
        }

        private string description;
        public string Description
        {
            get { return description; }
            set { description = value; IsDirty = true; }
        }

        private double price;
        public double Price
        {
            get { return price; }
            set { price = value; IsDirty = true; }
        }

        private DateTime? nextDue;
        public DateTime? NextDue
        {
            get { return nextDue; }
            set { nextDue = value; IsDirty = true; }
        }

        private string deliveryFrequency;
        public string DeliveryFrequency
        {
            get { return deliveryFrequency; }
            set { deliveryFrequency = value; IsDirty = true; }
        }

        public double Conversion { get; set; }
    }
}
