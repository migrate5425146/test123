﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class VisitChecklistMasterDTO:BaseDTO
    {
        public int VisitChecklistMasterId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public DateTime EffectiveStartDate { get; set; }
        public DateTime EffectiveEndDate { get; set; }
        public string Status { get; set; }

        public string CreatedBy;
        public DateTime? CreatedDate;
        public string LastModifiedBy;
        public DateTime? LastModifiedDate;
    }
}
