﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class ItineraryDetailDTO :BaseDTO
    {
        public int ItineraryDetailId { get; set; }
        public int ItineraryHeaderId { get; set; }
        public DateTime Date { get; set; }
        public string TownsToVisit { get; set; }
        public int Sequence { get; set; }
        public int DirectMiles { get; set; }
        public int LocalMiles { get; set; }
        public int TotalMiles { get; set; }
        public string WorkProgram { get; set; }
        public string NightAt { get; set; }
        public int ActivityId { get; set; }
        public int RouteId { get; set; }
        public string SequenceRoute { get; set; }
        public string DayTypeColor { get; set; }

        private DateTime? createdDate;
        public DateTime? CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                if (createdDate == value)
                    return;

                createdDate = value;
                IsDirty = true;
            }
        }
        private string createdBy;
        public string CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                if (createdBy == value)
                    return;

                createdBy = value;
                IsDirty = true;
            }
        }
        private DateTime? lastModifiedDate;
        public DateTime? LastModifiedDate
        {
            get
            {
                return lastModifiedDate;
            }
            set
            {
                if (lastModifiedDate == value)
                    return;

                lastModifiedDate = value;
                IsDirty = true;
            }
        }
        private string lastModifiedBy;
        public string LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                if (lastModifiedBy == value)
                    return;

                lastModifiedBy = value;
                IsDirty = true;
            }
        }
        public int OfficialMiles { get; set; }
    }
}
