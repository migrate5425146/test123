﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class OpenDealDTO : BaseDTO
    {
        public string OpportunityName { get; set; }

        public double Amount { get; set; }

        public string Rep { get; set; }

        public string Customer { get; set; }
    }
}
