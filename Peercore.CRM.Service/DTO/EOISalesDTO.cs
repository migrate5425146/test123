﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class EOISalesDTO : BaseDTO
    {
        public string Market { get; set; }

        public string Description { get; set; }

        public string CostYear { get; set; }

        public string UOM { get; set; }

        private double? cases1;

        private double? cases2;

        private double? cases3;

        private double? cases4;

        private double? cases5;

        private double? cases6;

        private double? cases7;

        private double? cases8;

        private double? cases9;

        private double? cases10;

        private double? cases11;

        private double? cases12;

        public double? Quarter1Rate { get; set; }
        public double? Quarter2Rate { get; set; }
        public double? MidYearRate { get; set; }
        public double? Quarter3Rate { get; set; }
        public double? ThirdQuarterRate { get; set; }
        public double? Quarter4Rate { get; set; }
        public double? EndOfYearRate { get; set; }

        public double? Cases1
        {
            get
            {
                return cases1;
            }
            set
            {
                cases1 = value;
            }
        }

        public double? Cases2
        {
            get
            {
                return cases2;
            }
            set
            {
                cases2 = value;
            }
        }

        public double? Cases3
        {
            get
            {
                return cases3;
            }
            set
            {
                cases3 = value;
            }
        }

        public double? Cases4
        {
            get
            {
                return cases4;
            }
            set
            {
                cases4 = value;
            }
        }

        public double? Cases5
        {
            get
            {
                return cases5;
            }
            set
            {
                cases5 = value;
            }
        }

        public double? Cases6
        {
            get
            {
                return cases6;
            }
            set
            {
                cases6 = value;
            }
        }

        public double? Cases7
        {
            get
            {
                return cases7;
            }
            set
            {
                cases7 = value;
            }
        }

        public double? Cases8
        {
            get
            {
                return cases8;
            }
            set
            {
                cases8 = value;
            }
        }

        public double? Cases9
        {
            get
            {
                return cases9;
            }
            set
            {
                cases9 = value;
            }
        }

        public double? Cases11
        {
            get
            {
                return cases11;
            }
            set
            {
                cases11 = value;
            }
        }
        public double? Cases10
        {
            get
            {
                return cases10;
            }
            set
            {
                cases10 = value;
            }
        }


        public double? Cases12
        {
            get
            {
                return cases12;
            }
            set
            {
                cases12 = value;
            }
        }

        public double? Quarter1
        {
            get
            {
                double? Total = null;
                if (this.CostYear == "")
                    Total = Quarter1Rate;
                else
                    Total = Convert.ToDouble((cases7 == null ? 0 : cases7) +
                        (cases8 == null ? 0 : cases8) + (cases9 == null ? 0 : cases9));
                return Total == 0 ? null : Total;
            }
            set
            {
                ;
            }
        }

        public double? Quarter2
        {
            get
            {
                double? Total = null;
                if (this.CostYear == "")
                    Total = Quarter2Rate;
                else
                    Total = Convert.ToDouble((cases10 == null ? 0 : cases10) +
                    (cases11 == null ? 0 : cases11) +
                    (cases12 == null ? 0 : cases12));
                return Total == 0 ? null : Total;
            }
            set
            {
                ;
            }
        }

        public double? MidYearSales
        {
            get
            {
                double? Total = null;
                if (this.CostYear == "")
                    Total = MidYearRate;
                else
                    Total =  Convert.ToDouble((cases7 == null ? 0 : cases7) + (cases8 == null ? 0 : cases8) +
                    (cases9 == null ? 0 : cases9) + (cases10 == null ? 0 : cases10) + (cases11 == null ? 0 : cases11) +
                    (cases12 == null ? 0 : cases12));
                return Total == 0 ? null : Total;
            }
            set
            {
                ;
            }
        }

        public double? Quarter3
        {
            get
            {
                double? Total = null;
                if (this.CostYear == "")
                    Total = Quarter3Rate;
                else
                    Total = Convert.ToDouble((cases1 == null ? 0 : cases1) + (cases2 == null ? 0 : cases2) +
                    (cases3 == null ? 0 : cases3));
                return Total == 0 ? null : Total;
            }
            set
            {
                ;
            }
        }

        public double? ThirdQtrSales
        {
            get
            {
                double? Total = null;
                if (this.CostYear == "")
                    Total = ThirdQuarterRate;
                else
                    Total = Convert.ToDouble((cases1 == null ? 0 : cases1) + (cases2 == null ? 0 : cases2) +
                        (cases3 == null ? 0 : cases3) +
                        (cases7 == null ? 0 : cases7) + (cases8 == null ? 0 : cases8) +
                        (cases9 == null ? 0 : cases9) + (cases10 == null ? 0 : cases10) +
                        (cases11 == null ? 0 : cases11) + (cases12 == null ? 0 : cases12));
                return Total == 0 ? null : Total;
            }
            set
            {
                ;
            }
        }

        public double? Quarter4
        {
            get
            {
                double? Total = null;
                if (this.CostYear == "")
                    Total = Quarter4Rate;
                else
                    Total = Convert.ToDouble((cases4 == null ? 0 : cases4) + (cases5 == null ? 0 : cases5) +
                    (cases6 == null ? 0 : cases6));
                return Total == 0 ? null : Total;
            }
            set
            {
                ;
            }
        }

        public double? EndOfYearSales
        {
            get
            {
                double? Total = null;
                if (this.CostYear == "")
                    Total = EndOfYearRate;
                else
                    Total = Convert.ToDouble((cases1 == null ? 0 : cases1) + (cases2 == null ? 0 : cases2) +
                        (cases3 == null ? 0 : cases3) + (cases4 == null ? 0 : cases4) + (cases5 == null ? 0 : cases5) +
                        (cases6 == null ? 0 : cases6) +
                        (cases7 == null ? 0 : cases7) + (cases8 == null ? 0 : cases8) +
                        (cases9 == null ? 0 : cases9) + (cases10 == null ? 0 : cases10) +
                        (cases11 == null ? 0 : cases11) + (cases12 == null ? 0 : cases12));
                return Total == 0 ? null : Total;
            }
            set
            {
                ;
            }
        }
    }
}
