﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO.CompositeEntities
{
    using System.Runtime.Serialization;
    [KnownType(typeof(LeadCustomerDTO))]
    public class LeadCustomerDTO : BaseDTO
    {

        private int sourceId;
        public int SourceId
        {
            get
            {
                return sourceId;
            }
            set
            {
                if (sourceId == value)
                    return;

                sourceId = value;
                IsDirty = true;
            }
        }

        private string name;
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                if (name == value)
                    return;

                name = value;
                IsDirty = true;
            }
        }

        /*private LeadCustomerTypes leadCustomerType;
        public LeadCustomerTypes LeadCustomerType
        {
            get
            {
                return leadCustomerType;
            }
            set
            {
                if (leadCustomerType == value)
                    return;

                leadCustomerType = value;
                IsDirty = true;
            }
        }*/

        private string company;
        public string Company
        {
            get
            {
                return company;
            }
            set
            {
                if (company == value)
                    return;

                company = value;
                IsDirty = true;
            }
        }

        private string business;
        public string Business
        {
            get
            {
                return business;
            }
            set
            {
                if (business == value)
                    return;

                business = value;
                IsDirty = true;
            }
        }

        private string industry;
        public string Industry
        {
            get
            {
                return industry;
            }
            set
            {
                if (industry == value)
                    return;

                industry = value;
                IsDirty = true;
            }
        }

        private string industryDescription;
        public string IndustryDescription
        {
            get
            {
                return industryDescription;
            }
            set
            {
                if (industryDescription == value)
                    return;

                industryDescription = value;
                IsDirty = true;
            }
        }

        private string leadStatus;
        public string LeadStatus
        {
            get
            {
                return leadStatus;
            }
            set
            {
                if (leadStatus == value)
                    return;

                leadStatus = value;
                IsDirty = true;
            }
        }

        private string state;
        public string State
        {
            get
            {
                return state;
            }
            set
            {
                if (state == value)
                    return;

                state = value;
                IsDirty = true; 
            }
        }
        private string city;
        public string City
        {
            get
            {
                return city;
            }
            set
            {
                if (city == value)
                    return;

                city = value;
                IsDirty = true;
            }
        }

        private string address;
        public string Address
        {
            get
            {
                return address;
            }
            set
            {
                if (address == value)
                    return;

                address = value;
                IsDirty = true;
            }
        }

        private string postalCode;
        public string PostalCode
        {
            get
            {
                return postalCode;
            }
            set
            {
                if (postalCode == value)
                    return;

                postalCode = value;
                IsDirty = true;
            }
        }


        private string telephone;
        public string Telephone
        {
            get
            {
                return telephone;
            }
            set
            {
                if (telephone == value)
                    return;

                telephone = value;
                IsDirty = true;
            }
        }

        private string mobile;
        public string Mobile
        {
            get
            {
                return mobile;
            }
            set
            {
                if (mobile == value)
                    return;

                mobile = value;
                IsDirty = true;
            }
        }

        private string email;
        public string Email
        {
            get
            {
                return email;
            }
            set
            {
                if (email == value)
                    return;

                email = value;
                IsDirty = true;
            }
        }

        private string customerCode;
        public string CustomerCode
        {
            get
            {
                return customerCode;
            }
            set
            {
                if (customerCode == value)
                    return;

                customerCode = value;
                IsDirty = true;
            }
        }

        private string leadStage;
        public string LeadStage
        {
            get
            {
                return leadStage;
            }
            set
            {
                if (leadStage == value)
                    return;

                leadStage = value;
                IsDirty = true;
            }
        }

        private string fax;
        public string Fax
        {
            get
            {
                return fax;
            }
            set
            {
                if (fax == value)
                    return;

                fax = value;
                IsDirty = true;
            }
        }

        private string leadSource;
        public string LeadSource
        {
            get
            {
                return leadSource;
            }
            set
            {
                if (leadSource == value)
                    return;

                leadSource = value;
                IsDirty = true;
            }
        }

        private string originator;
        public string Originator
        {
            get
            {
                return originator;
            }
            set
            {
                if (originator == value)
                    return;

                originator = value;
                IsDirty = true;
            }
        }

        private double? annualRevenue;
        public double? AnnualRevenue
        {
            get
            {
                return annualRevenue;
            }
            set
            {
                if (annualRevenue == value)
                    return;

                annualRevenue = value;
                IsDirty = true;
            }
        }

        private int? noOfEmployees;
        public int? NoOfEmployees
        {
            get
            {
                return noOfEmployees;
            }
            set
            {
                if (noOfEmployees == value)
                    return;

                noOfEmployees = value;
                IsDirty = true;
            }
        }

        private int? rating;
        public int? Rating
        {
            get
            {
                return rating;
            }
            set
            {
                if (rating == value)
                    return;

                rating = value;
                IsDirty = true;
            }
        }

        private string webiste;
        public string Webiste
        {
            get
            {
                return webiste;
            }
            set
            {
                if (webiste == value)
                    return;

                webiste = value;
                IsDirty = true;
            }
        }

        private string description;
        public string Description
        {
            get
            {
                return description;
            }
            set
            {
                if (description == value)
                    return;

                description = value;
                IsDirty = true;
            }
        }

        private string businessPotential;
        public string BusinessPotential
        {
            get
            {
                return businessPotential;
            }
            set
            {
                if (businessPotential == value)
                    return;

                businessPotential = value;
                IsDirty = true;
            }
        }

        private string referredBy;
        public string ReferredBy
        {
            get
            {
                return referredBy;
            }
            set
            {
                if (referredBy == value)
                    return;

                referredBy = value;
                IsDirty = true;
            }
        }

        private string country;
        public string Country
        {
            get
            {
                return country;
            }
            set
            {
                if (country == value)
                    return;

                country = value;
                IsDirty = true;
            }
        }

        private string preferredContact;
        public string PreferredContact
        {
            get
            {
                return preferredContact;
            }
            set
            {
                if (preferredContact == value)
                    return;

                preferredContact = value;
                IsDirty = true;
            }
        }

        private string preferredContactDescription;
        public string PreferredContactDescription
        {
            get
            {
                return preferredContactDescription;
            }
            set
            {
                if (preferredContactDescription == value)
                    return;

                preferredContactDescription = value;
                IsDirty = true;
            }
        }

        private string channelDescription;
        public string ChannelDescription
        {
            get
            {
                return channelDescription;
            }
            set
            {
                if (channelDescription == value)
                    return;

                channelDescription = value;
                IsDirty = true;
            }
        }

        private int noOfLeadCustomers;
        public int NoOfLeadCustomers
        {
            get
            {
                return noOfLeadCustomers;
            }
            set
            {
                if (noOfLeadCustomers == value)
                    return;

                noOfLeadCustomers = value;
                IsDirty = true;
            }
        }

        private string isDeleted;
        public string IsDeleted
        {
            get
            {
                return isDeleted;
            }
            set
            {
                if (isDeleted == value)
                    return;

                isDeleted = value;
                IsDirty = true;
            }
        }

        private string customerGroup;
        public string CustomerGroup
        {
            get
            {
                return customerGroup;
            }
            set
            {
                if (customerGroup == value)
                    return;

                customerGroup = value;
                IsDirty = true;
            }
        }
        private string selection;
        public string Selection
        {
            get
            {
                return selection;
            }
            set
            {
                if (selection == value)
                    return;

                selection = value;
                IsDirty = true;
            }
        }

        private DateTime? lastCalledDate;
        public DateTime? LastCalledDate
        {
            get
            {
                return lastCalledDate;
            }
            set
            {
                if (lastCalledDate == value)
                    return;

                lastCalledDate = value;
                IsDirty = true;
            }
        }

        private DateTime? startDate;
        public DateTime? StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                if (startDate == value)
                    return;

                startDate = value;
                IsDirty = true;
            }
        }

        private string serviceChecklistColor;
        public string ServiceChecklistColor
        {
            get
            {
                return serviceChecklistColor;
            }
            set
            {
                if (serviceChecklistColor == value)
                    return;

                serviceChecklistColor = value;
                IsDirty = true;
            }
        }

        private string serviceChecklistTooltip;
        public string ServiceChecklistTooltip
        {
            get
            {
                return serviceChecklistTooltip;
            }
            set
            {
                if (serviceChecklistTooltip == value)
                    return;

                serviceChecklistTooltip = value;
                IsDirty = true;
            }
        }

        
        private  DateTime lastServiceChecklistDate;
        private DateTime LastServiceChecklistDate
        {
            get { return lastServiceChecklistDate; }
            set
            {
                lastServiceChecklistDate = value;
                if (LeadStage.Equals("Customer"))
                {
                    if (lastServiceChecklistDate == DateTime.MinValue)
                    {
                        // NEW CUSTOMER
                        if (StartDate != null && Convert.ToDateTime(StartDate) >= DateTime.Today.AddMonths(-6))
                        {
                            ServiceChecklistColor = "#D20BA70B";
                            serviceChecklistTooltip = "New Customer";
                        }
                        else
                        {
                            ServiceChecklistColor = "#FFE21A1A";
                            serviceChecklistTooltip = "No Service Call done";
                        }
                    }
                    else
                    {
                        if (DateTime.Today.AddMonths(-6) < lastServiceChecklistDate)
                            ServiceChecklistColor = "#D20BA70B";
                        else
                            ServiceChecklistColor = "#FFE21A1A";

                        serviceChecklistTooltip = "Last Service Call On " + lastServiceChecklistDate.ToString("dd-MMM-yyyy");
                    }
                }
            }
        }

        public Entities.LeadCustomerTypes LeadCustomerType { get; set; }

        public string EndUserCode { get; set; }

        private string selectedToCall="";
        public string SelectedToCall
        {
            get
            {
                return selectedToCall;
            }
            set
            {
                if (selectedToCall == value)
                    return;

                selectedToCall = value;
            }
        }

        private bool hasSelect;
        public bool HasSelect
        {
            get { return hasSelect; }
            set { hasSelect = value; }
        }

        public string RepCode { get; set; }
        public string TME { get; set; }
        public string CustType { get; set; }
        public string Market { get; set; }

        public int DistributorId { get; set; }
        public string DistributorCode { get; set; }
        public string DistributorName { get; set; }

        public string LastInvoiceDate { get; set; }

        public bool IsCheckList { get; set; }
        public string RouteName { get; set; }
        public string RepName { get; set; }
    }
}
