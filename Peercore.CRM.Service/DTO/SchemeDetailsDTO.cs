﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class SchemeDetailsDTO : BaseDTO
    {
        public int SchemeDetailsId { get; set; }
        public int SchemeHeaderId { get; set; }
        public int DetailsTypeId { get; set; }
        public int SchemeTypeId { get; set; }
        public string SchemeDetails { get; set; }
        public double DiscountValue { get; set; }
        public double DiscountPercentage { get; set; }
        public bool? IsRetail { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool IsActive { get; set; }
        public List<OptionLevelDTO> OptionLevelList { get; set; }


        public string SchemeHeaderName { get; set; }
        public string DetailsTypeName { get; set; }
        public string SchemeTypeName { get; set; }

        public List<FreeProductDTO> FreeProductList { get; set; }

        public int IndexId { get; set; }
        
        public string FreeProdCond { get; set; }

        public string FreeIssueCondition { get; set; }

    }
}
