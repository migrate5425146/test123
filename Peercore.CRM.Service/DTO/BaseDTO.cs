﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    [Serializable]
    public class BaseDTO
    {
        private bool isDirty;
        public bool IsDirty
        {
            get { return isDirty; }
            set { isDirty = value; }
        }

        //private bool isNew;
        //public bool IsNew
        //{
        //    get { return isNew; }
        //    set { isNew = value; }
        //}

        private int rowCount;
        public int RowCount
        {
            get { return rowCount; }
            set { rowCount = value; }
        }
    }
}
