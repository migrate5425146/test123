﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class AreaPostcodeDTO
    {
        private int? areaPostcodeID;
        public int? AreaPostcodeID
        {
            get { return areaPostcodeID; }
            set { areaPostcodeID = value; }
        }

        private int? areaID;
        public int? AreaID
        {
            get { return areaID; }
            set { areaID = value; }
        }

        private string suburbName;
        public string SuburbName
        {
            get { return suburbName; }
            set
            {
                suburbName = value;
            }
        }

        private string postcodeName;
        public string PostcodeName
        {
            get { return postcodeName; }
            set
            {
                postcodeName = value;
            }
        }
    }
}
