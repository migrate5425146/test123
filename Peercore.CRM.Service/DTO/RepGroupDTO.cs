﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class RepGroupDTO : BaseDTO
    {

        private int? repGroupId;
        public int? RepGroupId
        {
            get { return repGroupId; }
            set { repGroupId = value; }
        }

        private string repGroupName;
        public string RepGroupName
        {
            get { return repGroupName; }
            set
            {
                repGroupName = value;
                IsDirty = true;
            }
        }

        private List<GroupRepDTO> repList;
        public List<GroupRepDTO> RepList
        {
            get
            {
                if (repList == null)
                    repList = new List<GroupRepDTO>();
                return repList;
            }
            set
            {
                repList = value;
            }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set
            {
                name = value;
                IsDirty = true;
            }
        }

        private int count;
        public int Count
        {
            get { return count; }
            set { count = value; }
        }
    }
}
