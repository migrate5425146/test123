﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class LeadStageDTO : BaseDTO
    {
        private int stageId;
        public int StageId
        {
            get
            {
                return stageId;
            }
            set
            {
                if (stageId == value)
                    return;

                stageId = value;
                IsDirty = true;
            }
        }

        private string stageName;
        public string StageName
        {
            get
            {
                return stageName;
            }
            set
            {
                if (stageName == value)
                    return;

                stageName = value;
                IsDirty = true;
            }
        }

        private int stageOrder;
        public int StageOrder
        {
            get
            {
                return stageOrder;
            }
            set
            {
                if (stageOrder == value)
                    return;

                stageOrder = value;
                IsDirty = true;
            }
        }

        private bool opportunityAllowed;
        public bool OpportunityAllowed
        {
            get
            {
                return opportunityAllowed;
            }
            set
            {
                if (opportunityAllowed == value)
                    return;

                opportunityAllowed = value;
                IsDirty = true;
            }
        }

        private int maxOpportunities;
        public int MaxOpportunities
        {
            get
            {
                return maxOpportunities;
            }
            set
            {
                if (maxOpportunities == value)
                    return;

                maxOpportunities = value;
                IsDirty = true;
            }
        }

        private string emailAddress;
        public string EmailAddress
        {
            get
            {
                return emailAddress;
            }
            set
            {
                if (emailAddress == value)
                    return;

                emailAddress = value;
                IsDirty = true;
            }
        }
        //Count of Each Stage
        private int stageCount;
        public int StageCount
        {
            get
            {
                return stageCount;
            }
            set
            {
                if (stageCount == value)
                    return;

                stageCount = value;
                IsDirty = true;
            }
        }

        //darshaka
        private bool isSelected;
        public bool IsSelected
        {
            get
            {
                return isSelected;
            }
            set
            {
                if (isSelected == value)
                    return;

                isSelected = value;
                IsDirty = true;
            }
        }

        private string colour;
        public string Colour
        {
            get
            {
                return colour;
            }
            set
            {
                if (colour == value)
                    return;

                colour = value;
                IsDirty = true;
            }
        }

        public int LeadStageId { get; set; }
        public string LeadStage { get; set; }
        public int LeadsCount { get; set; }

    }
}
