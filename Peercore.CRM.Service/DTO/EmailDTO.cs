﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class EmailDTO : BaseDTO
    {
        #region - byte[] -
        private byte[] fileContent;
        public byte[] FileContent
        {
            get { return fileContent; }
            set
            {
                fileContent = value; IsDirty = true;
            }
        }
        #endregion

        private int emailID;
        public int EmailID
        {
            get { return emailID; }
            set { emailID = value; IsDirty = true; }
        }

        private string entryId;
        public string EntryID
        {
            get { return entryId; }
            set { entryId = value; IsDirty = true; }
        }

        private string to;
        public string To
        {
            get { return to; }
            set { to = value; IsDirty = true; }
        }

        private string from;
        public string From
        {
            get { return from; }
            set { from = value; IsDirty = true; }
        }

        private string subject;
        public string Subject
        {
            get { return subject; }
            set { subject = value; IsDirty = true; }
        }

        private string content;
        public string Content
        {
            get { return content; }
            set { content = value; IsDirty = true; }
        }

        private string filename;
        public string FileName
        {
            get { return filename; }
            set { filename = value; IsDirty = true; }
        }

        private DateTime date;
        public DateTime Date
        {
            get { return date; }
            set { date = value; IsDirty = true; }
        }

        private object outlookObject;
        public object OutlookObject
        {
            get { return outlookObject; }
            set { outlookObject = value; IsDirty = true; }
        }

        private int leadId;
        public int LeadID
        {
            get { return leadId; }
            set { leadId = value; IsDirty = true; }
        }

        private string custCode;
        public string CustCode
        {
            get { return custCode; }
            set { custCode = value; IsDirty = true; }
        }

        private string leadName;
        public string LeadName
        {
            get { return leadName; }
            set { leadName = value; IsDirty = true; }
        }

        private string contactPersonName;
        public string ContactPersonName
        {
            get { return contactPersonName; }
            set { contactPersonName = value; IsDirty = true; }
        }

        //private int rowCount;
        //public int RowCount
        //{
        //    get { return rowCount; }
        //    set { rowCount = value; }
        //}

        private string fileLocation;
        public string FileLocation
        {
            get { return fileLocation; }
            set { fileLocation = value; }
        }
    }
}
