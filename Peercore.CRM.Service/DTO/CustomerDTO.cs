﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class CustomerDTO : BaseDTO
    {
        #region - Windows -

        #region - string -
        private string address;
        public string Address
        {
            get { return address; }
            set { address = value; IsDirty = true; }
        }

        private string address2;
        public string Address2
        {
            get { return address2; }
            set { address2 = value; IsDirty = true; }
        }

        private string business;
        public string Business
        {
            get { return business; }
            set { business = value; IsDirty = true; }
        }

        private string businessPotential;
        public string BusinessPotential
        {
            get { return businessPotential; }
            set { businessPotential = value; IsDirty = true; }
        }

        private string city;
        public string City
        {
            get { return city; }
            set { city = value; IsDirty = true; }
        }

        private string company;
        public string Company
        {
            get { return company; }
            set { company = value; IsDirty = true; }
        }

        private string country;
        public string Country
        {
            get { return country; }
            set { country = value; IsDirty = true; }
        }

        private string createdBy;
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; IsDirty = true; }
        }

        private string creditStatus;
        public string CreditStatus
        {
            get { return creditStatus; }
            set { creditStatus = value; IsDirty = true; }
        }

        private string customerCode;
        public string CustomerCode
        {
            get { return customerCode; }
            set { customerCode = value; IsDirty = true; }
        }

        private string enduserCode;
        public string EndUserCode
        {
            get { return enduserCode; }
            set { enduserCode = value; IsDirty = true; }
        }

        private string customerGroup;
        public string CustomerGroup
        {
            get { return customerGroup; }
            set { customerGroup = value; IsDirty = true; }
        }

        private string description;
        public string Description
        {
            get { return description; }
            set { description = value; IsDirty = true; }
        }

        private string email;
        public string Email
        {
            get { return email; }
            set { email = value; IsDirty = true; }
        }

        private string fax;
        public string Fax
        {
            get { return fax; }
            set { fax = value; IsDirty = true; }
        }

        private string industry;
        public string Industry
        {
            get { return industry; }
            set { industry = value; IsDirty = true; }
        }

        private string leadSource;
        public string LeadSource
        {
            get { return leadSource; }
            set { leadSource = value; IsDirty = true; }
        }

        private string leadStatus;
        public string LeadStatus
        {
            get { return leadStatus; }
            set { leadStatus = value; IsDirty = true; }
        }

        private string litersBy;
        public string LitersBy
        {
            get { return litersBy; }
            set { litersBy = value; IsDirty = true; }
        }

        private string mobile;
        public string Mobile
        {
            get { return mobile; }
            set { mobile = value; IsDirty = true; }
        }

        private string modifiedBy;
        public string ModifiedBy
        {
            get { return modifiedBy; }
            set { modifiedBy = value; IsDirty = true; }
        }

        private string name;
        public string Name
        {
            get { return name; }
            set { name = value; IsDirty = true; }
        }

        private string postalCode;
        public string PostalCode
        {
            get { return postalCode; }
            set { postalCode = value; IsDirty = true; }
        }

        private string preferredContact;
        public string PreferredContact
        {
            get { return preferredContact; }
            set { preferredContact = value; IsDirty = true; }
        }

        private string previousCustomerCode;
        public string PreviousCustomerCode
        {
            get { return previousCustomerCode; }
            set { previousCustomerCode = value; IsDirty = true; }
        }

        private string previousCustomerName;
        public string PreviousCustomerName
        {
            get { return previousCustomerName; }
            set { previousCustomerName = value; IsDirty = true; }
        }

        private string referredBy;
        public string ReferredBy
        {
            get { return referredBy; }
            set { referredBy = value; IsDirty = true; }
        }

        private string repCode;
        public string RepCode
        {
            get { return repCode; }
            set { repCode = value; IsDirty = true; }
        }

        private string shortName;
        public string ShortName
        {
            get { return shortName; }
            set { shortName = value; IsDirty = true; }
        }

        private string state;
        public string State
        {
            get { return state; }
            set { state = value; IsDirty = true; }
        }

        private string status;
        public string Status
        {
            get { return status; }
            set { status = value; IsDirty = true; }
        }

        private string telephone;
        public string Telephone
        {
            get { return telephone; }
            set { telephone = value; IsDirty = true; }
        }

        private string website;
        public string Website
        {
            get { return website; }
            set { website = value; IsDirty = true; }
        }

        private string primaryRepName;
        public string PrimaryRepName
        {
            get { return primaryRepName; }
            set { primaryRepName = value; IsDirty = true; }
        }

        private string secondaryRepName;
        public string SecondaryRepName
        {
            get { return secondaryRepName; }
            set { secondaryRepName = value; IsDirty = true; }
        }

        #endregion

        #region - int -
        private int noOfEmployees;
        public int NoOfEmployees
        {
            get { return noOfEmployees; }
            set { noOfEmployees = value; IsDirty = true; }
        }

        private int rating;
        public int Rating
        {
            get { return rating; }
            set { rating = value; IsDirty = true; }
        }
        #endregion

        #region - double -
        private double annualRevenue;
        public double AnnualRevenue
        {
            get { return annualRevenue; }
            set { annualRevenue = value; IsDirty = true; }
        }

        private double potentialLiters;
        public double PotentialLiters
        {
            get { return potentialLiters; }
            set { potentialLiters = value; IsDirty = true; }
        }

        private double probability;
        public double Probability
        {
            get { return probability; }
            set { probability = value; IsDirty = true; }
        }
        #endregion

        #region - DateTime -
        private DateTime? createdDate;
        public DateTime? CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; IsDirty = true; }
        }

        private DateTime? lastCalledDate;
        public DateTime? LastCalledDate
        {
            get { return lastCalledDate; }
            set { lastCalledDate = value; IsDirty = true; }
        }

        private DateTime? modifiedDate;
        public DateTime? ModifiedDate
        {
            get { return modifiedDate; }
            set { modifiedDate = value; IsDirty = true; }
        }

        private DateTime startDate;
        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; IsDirty = true; }
        }
        #endregion

        #region - Boolean -
        private bool isPrimaryDist;
        public bool IsPrimaryDist
        {
            get { return isPrimaryDist; }
            set { isPrimaryDist = value; IsDirty = true; }
        }
        #endregion

        #endregion

        #region CTC new properties

        public string MarketName { get; set; }

        public int MarketId { get; set; }

        public int VolumeId { get; set; }

        public int CategoryId { get; set; }

        public string Category { get; set; }

        public string Volume { get; set; }

        public int PeripheryId { get; set; }

        public string Periphery { get; set; }

        public int OutletTypeId { get; set; }

        public string OutletType { get; set; }

        public bool isTLP { get; set; }

        public double Longitude { get; set; }

        public double Latitiude { get; set; }

        public int PromotionId { get; set; }

        public string PromotionName { get; set; }

        public string CustomerTypeOfApprove { get; set; }

        public string Province { get; set; }
        public string GeoLocation { get; set; }
        public string ProductsHandled { get; set; }
        public string CanvassingFrequency { get; set; }
        public string AsuShare { get; set; }
        public string AuthorityAssessment { get; set; }

        public double OutstandingBalance { get; set; }
        public double CreditLimit { get; set; }
        public double CashLimit { get; set; }

        public double StickTarget { get; set; }
        public int TargetYear { get; set; }
        public int TargetMonth { get; set; }
        public string TargetMonthText { get; set; }
        public int TargetId { get; set; }
        public DateTime EffStartDate { get; set; }
        public DateTime EffEndDate { get; set; }
        public string AssignedRouteName { get; set; }
        public int RouteAssignId { get; set; }

        public byte[] ImageContent { get; set; }
        public string ImageName { get; set; }
        public string ImagePath { get; set; }
        public bool IsRetail { get; set; }
        public string OutletUserCode { get; set; }
        #endregion
    }
}
