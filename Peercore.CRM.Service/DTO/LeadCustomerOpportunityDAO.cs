﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class LeadCustomerOpportunityDAO : BaseDTO
    {
        public string Name { get; set; }

        public string Opportunity { get; set; }

        public string Originator { get; set; }

        public double Amount { get; set; }

        public double Tonnes { get; set; }

        public double Ratio { get; set; }

        public double Count { get; set; }

        public int Won { get; set; }

        public double Total { get; set; }

        public string Stage { get; set; }

        public string CatalogCode { get; set; }

        public string State { get; set; }

        public int OpportunityId { get; set; }

        public string RepCode { get; set; }

        public string RepName { get; set; }

        public string Market { get; set; }

        public int LeadId { get; set; }

        public DateTime CloseDate { get; set; }

        public string CustomerCode { get; set; }

        public DateTime CreatedDate { get; set; }

        public string MarketDescription { get; set; }

        public string EndUserCode { get; set; }

    }
}
