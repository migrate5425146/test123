﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class InvoiceDetailDTO : BaseDTO
    {
        public int InvoiceDetailId { get; set; }
        public int InvoiceId { get; set; }
        public int ProductId { get; set; }
        public double Quantity { get; set; }
        public double Discount { get; set; }
        public double DiscountValue { get; set; }
        public double SubTotal { get; set; }
        public string Status { get; set; }
        public double ItemPrice { get; set; }
        public string ProductName { get; set; }

    }
}
