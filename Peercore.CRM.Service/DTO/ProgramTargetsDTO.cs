﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class ProgramTargetsDTO:BaseDTO
    {
        public int ProgramTargetId { get; set; }
        public ProgramDTO Program { get; set; }

        public List<CustomerDTO> customerList { get; set; }

        public string Status { get; set; }

        public double? TargetPercentage { get; set; }
        public double? TargetMilles { get; set; }
        public double? TargetIncentiveAmount { get; set; }
        public int? TargetIncentivePreIssueProductId { get; set; }
        public string TargetIncentivePreIssueProductName { get; set; }
        public int? TargetIncentivePreIssueProductSku { get; set; }
        public int? TargetIncentivePreIssueAmountPacks { get; set; }

        public double? BonusPercentage { get; set; }
        public double? BonusMilles { get; set; }
        public double? BonusIncentiveAmount { get; set; }
        public int? BonusIncentivePreIssueProductId { get; set; }
        public string BonusIncentivePreIssueProductName { get; set; }
        public int? BonusIncentivePreIssueProductSku { get; set; }
        public int? BonusIncentivePreIssueAmountPacks { get; set; }

        public DateTime EffStartDate { get; set; }
        public DateTime EffEndDate { get; set; }
        public string EffStartDateString { get; set; }
        public string EffEndDateString { get; set; }

        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime? LastModifiedDate { get; set; }
    }
}
