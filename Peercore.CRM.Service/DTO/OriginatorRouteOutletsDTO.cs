﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class OriginatorRouteOutletsDTO:BaseDTO
    {
        private OriginatorDTO originator = new OriginatorDTO();
        public OriginatorDTO Originator
        {
            get { return originator; }
            set { originator = value; }
        }

        private RouteDTO route = new RouteDTO();
        public RouteDTO Route
        {
            get { return route; }
            set { route = value; }
        }

        private List<CallCycleContactDTO> outlets = new List<CallCycleContactDTO>();
        public List<CallCycleContactDTO> Outlets
        {
            get { return outlets; }
            set { outlets = value; }
        }

        private string createdBy;
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        private DateTime? createdDate;
        public DateTime? CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        private string lastmodifiedBy;
        public string LastModifiedBy
        {
            get { return lastmodifiedBy; }
            set { lastmodifiedBy = value; }
        }

        private DateTime? lastmodifiedDate;
        public DateTime? LastModifiedDate
        {
            get { return lastmodifiedDate; }
            set { lastmodifiedDate = value; }
        }

        private string delFlag;
        public string DelFlag
        {
            get { return delFlag; }
            set { delFlag = value; }
        }

        private string ccType;
        public string CcType
        {
            get { return ccType; }
            set { ccType = value; }
        }
    }
}
