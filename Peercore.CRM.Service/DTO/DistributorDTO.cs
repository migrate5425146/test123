﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class DistributorDTO:BaseDTO
    {
        public int DistributorId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Originator { get; set; }
        public string Status { get; set; }

        public double Target { get; set; }
        public int TargetId { get; set; }

        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string LastModifiedBy { get; set; }
        public DateTime LastModifiedDate { get; set; }

        public DateTime EffStartDate { get; set; }
        public DateTime EffEndDate { get; set; }

        public string PrefixCode { get; set; }
        public string ParentOriginator { get; set; }
        public string DeptString { get; set; }

        public string Password { get; set; }
        public string Adderess { get; set; }
        public string Mobile { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public string Holiday { get; set; }
        private List<string> holidaysList = new List<string>();
        public List<string> HolidaysList
        {
            get { return holidaysList; }
            set { holidaysList = value; }
        }

        private List<BankAccountDTO> bankAccountsList = new List<BankAccountDTO>();
        public List<BankAccountDTO> BankAccountsList
        {
            get { return bankAccountsList; }
            set { bankAccountsList = value; }
        }

        public string GroupCode { get; set; }
        public DateTime? JoinedDate { get; set; }
        public int DivisionId { get; set; }
        public string PrimaryDist { get; set; }
        public int AreaId { get; set; }
        public bool IsNotEMEIUser { get; set; }
    }
}
