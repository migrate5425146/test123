﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class EquipmentDTO : BaseDTO
    {
        private string _CustomerCode;
        public string CustomerCode
        {
            get { return _CustomerCode; }
            set { _CustomerCode = value; IsDirty = true; }
        }

        private string _CustomerName;
        public string CustomerName
        {
            get { return _CustomerName; }
            set { _CustomerName = value; IsDirty = true; }
        }

        private string _AssetNumber;
        public string AssetNumber
        {
            get { return _AssetNumber; }
            set { _AssetNumber = value; IsDirty = true; }
        }

        private string _AssetType;
        public string AssetType
        {
            get { return _AssetType; }
            set { _AssetType = value; IsDirty = true; }
        }

        private string _AssetDescription;
        public string AssetDescription
        {
            get { return _AssetDescription; }
            set { _AssetDescription = value; IsDirty = true; }
        }

        private string _AssetSize;
        public string AssetSize
        {
            get { return _AssetSize; }
            set { _AssetSize = value; IsDirty = true; }
        }

        private int _Rent;
        public int Rent
        {
            get { return _Rent; }
            set { _Rent = value; IsDirty = true; }
        }

        private decimal _Amount;
        public decimal Amount
        {
            get { return _Amount; }
            set { _Amount = value; IsDirty = true; }
        }

        private string _Note;
        public string Note
        {
            get { return _Note; }
            set { _Note = value; IsDirty = true; }
        }

        private string _Address1A;
        public string Address1A
        {
            get { return _Address1A; }
            set { _Address1A = value; IsDirty = true; }
        }

        private string _Address2A;
        public string Address2A
        {
            get { return _Address2A; }
            set { _Address2A = value; IsDirty = true; }
        }

        private string _CityA;
        public string CityA
        {
            get { return _CityA; }
            set { _CityA = value; IsDirty = true; }
        }

        private string _StateA;
        public string StateA
        {
            get { return _StateA; }
            set { _StateA = value; IsDirty = true; }
        }

        private string _PostcodeA;
        public string PostcodeA
        {
            get { return _PostcodeA; }
            set { _PostcodeA = value; IsDirty = true; }
        }

        private string _Status;
        public string Status
        {
            get { return _Status; }
            set { _Status = value; IsDirty = true; }
        }

        // If an equipment is deleted the delete button should be diabled
        public bool IsEnabled
        {
            get
            {
                return Status.Equals("D") ? false : true;
            }
        }

        // Not in Table
        private string oldAssetNumber;
        public string OldAssetNumber
        {
            get
            {
                return oldAssetNumber;
            }
            set
            {
                if (oldAssetNumber == value)
                    return;

                oldAssetNumber = value;
                IsDirty = true;
            }
        }

        private int rowCount;
        public int RowCount
        {
            get { return rowCount; }
            set { rowCount = value; IsDirty = true; }
        }

    }
}
