﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class OpportunityOwnerDTO : BaseDTO
    {
        public string OwnerName { get; set; }

        public double Amount { get; set; }

        public double Units { get; set; }

        public int Count { get; set; }

        public int Ratio { get; set; }

        public string Originator { get; set; }
    }
}
