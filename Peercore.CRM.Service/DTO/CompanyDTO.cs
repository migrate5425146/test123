﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Service.DTO
{
    public class CompanyDTO
    {
        public decimal CompId { get; set; }
        public string CompCode { get; set; }
        public string CompName { get; set; }
    }
}
