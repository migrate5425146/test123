﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class ServiceChecklistDTO : BaseDTO
    {
        private int checklistID;
        public int ChecklistID
        {
            get
            {
                return checklistID;
            }
            set
            {
                if (checklistID == value)
                    return;

                checklistID = value;
                IsDirty = true;
            }
        }

        private int activityID;
        public int ActivityID
        {
            get
            {
                return activityID;
            }
            set
            {
                if (activityID == value)
                    return;

                activityID = value;
                IsDirty = true;
            }
        }

        private string conductedBy;
        public string ConductedBy
        {
            get
            {
                return conductedBy;
            }
            set
            {
                if (conductedBy == value)
                    return;

                conductedBy = value;
                IsDirty = true;
            }
        }

        private DateTime assessmentDate;
        public DateTime AssessmentDate
        {
            get
            {
                return assessmentDate;
            }
            set
            {
                if (assessmentDate == value)
                    return;

                assessmentDate = value;
                IsDirty = true;
            }
        }

        private string personCalledOn;
        public string PersonCalledOn
        {
            get
            {
                return personCalledOn;
            }
            set
            {
                if (personCalledOn == value)
                    return;

                personCalledOn = value;
                IsDirty = true;
            }
        }

        private string venueName;
        public string VenueName
        {
            get
            {
                return venueName;
            }
            set
            {
                if (venueName == value)
                    return;

                venueName = value;
                IsDirty = true;
            }
        }

        private string address1;
        public string Address1
        {
            get
            {
                return address1;
            }
            set
            {
                if (address1 == value)
                    return;

                address1 = value;
                IsDirty = true;
            }
        }

        private string address2;
        public string Address2
        {
            get
            {
                return address2;
            }
            set
            {
                if (address2 == value)
                    return;

                address2 = value;
                IsDirty = true;
            }
        }

        private string city;
        public string City
        {
            get
            {
                return city;
            }
            set
            {
                if (city == value)
                    return;

                city = value;
                IsDirty = true;
            }
        }

        private string state;
        public string State
        {
            get
            {
                return state;
            }
            set
            {
                if (state == value)
                    return;

                state = value;
                IsDirty = true;
            }
        }

        private string postcode;
        public string Postcode
        {
            get
            {
                return postcode;
            }
            set
            {
                if (postcode == value)
                    return;

                postcode = value;
                IsDirty = true;
            }
        }

        private string channel;
        public string Channel
        {
            get
            {
                return channel;
            }
            set
            {
                if (channel == value)
                    return;

                channel = value;
                IsDirty = true;
            }
        }

        private string keyContactFirstName;
        public string KeyContactFirstName
        {
            get
            {
                return keyContactFirstName;
            }
            set
            {
                if (keyContactFirstName == value)
                    return;

                keyContactFirstName = value;
                IsDirty = true;
            }
        }

        private string keyContactLastName;
        public string KeyContactLastName
        {
            get
            {
                return keyContactLastName;
            }
            set
            {
                if (keyContactLastName == value)
                    return;

                keyContactLastName = value;
                IsDirty = true;
            }
        }

        private string keyContactPosition;
        public string KeyContactPosition
        {
            get
            {
                return keyContactPosition;
            }
            set
            {
                if (keyContactPosition == value)
                    return;

                keyContactPosition = value;
                IsDirty = true;
            }
        }

        private string keyContactTelephone;
        public string KeyContactTelephone
        {
            get
            {
                return keyContactTelephone;
            }
            set
            {
                if (keyContactTelephone == value)
                    return;

                keyContactTelephone = value;
                IsDirty = true;
            }
        }

        private string keyContactMobile;
        public string KeyContactMobile
        {
            get
            {
                return keyContactMobile;
            }
            set
            {
                if (keyContactMobile == value)
                    return;

                keyContactMobile = value;
                IsDirty = true;
            }
        }

        private string secondContactFirstName;
        public string SecondContactFirstName
        {
            get
            {
                return secondContactFirstName;
            }
            set
            {
                if (secondContactFirstName == value)
                    return;

                secondContactFirstName = value;
                IsDirty = true;
            }
        }

        private string secondContactLastName;
        public string SecondContactLastName
        {
            get
            {
                return secondContactLastName;
            }
            set
            {
                if (secondContactLastName == value)
                    return;

                secondContactLastName = value;
                IsDirty = true;
            }
        }

        private string secondContactPosition;
        public string SecondContactPosition
        {
            get
            {
                return secondContactPosition;
            }
            set
            {
                if (secondContactPosition == value)
                    return;

                secondContactPosition = value;
                IsDirty = true;
            }
        }

        private string secondContactTelephone;
        public string SecondContactTelephone
        {
            get
            {
                return secondContactTelephone;
            }
            set
            {
                if (secondContactTelephone == value)
                    return;

                secondContactTelephone = value;
                IsDirty = true;
            }
        }

        private string secondContactMobile;
        public string SecondContactMobile
        {
            get
            {
                return secondContactMobile;
            }
            set
            {
                if (secondContactMobile == value)
                    return;

                secondContactMobile = value;
                IsDirty = true;
            }
        }

        private string fTSerialNo;
        public string FTSerialNo
        {
            get
            {
                return fTSerialNo;
            }
            set
            {
                if (fTSerialNo == value)
                    return;

                fTSerialNo = value;
                IsDirty = true;
            }
        }

        private string fTSieze;
        public string FTSieze
        {
            get
            {
                return fTSieze;
            }
            set
            {
                if (fTSieze == value)
                    return;

                fTSieze = value;
                IsDirty = true;
            }
        }

        private string wTSerialNo;
        public string WTSerialNo
        {
            get
            {
                return wTSerialNo;
            }
            set
            {
                if (wTSerialNo == value)
                    return;

                wTSerialNo = value;
                IsDirty = true;
            }
        }

        private string wTSize;
        public string WTSize
        {
            get
            {
                return wTSize;
            }
            set
            {
                if (wTSize == value)
                    return;

                wTSize = value;
                IsDirty = true;
            }
        }

        private string fMSerialNo;
        public string FMSerialNo
        {
            get
            {
                return fMSerialNo;
            }
            set
            {
                if (fMSerialNo == value)
                    return;

                fMSerialNo = value;
                IsDirty = true;
            }
        }

        private string fMSize;
        public string FMSize
        {
            get
            {
                return fMSize;
            }
            set
            {
                if (fMSize == value)
                    return;

                fMSize = value;
                IsDirty = true;
            }
        }

        private string otherEquipment;
        public string OtherEquipment
        {
            get
            {
                return otherEquipment;
            }
            set
            {
                if (otherEquipment == value)
                    return;

                otherEquipment = value;
                IsDirty = true;
            }
        }

        private string presentOliveOil;
        public string PresentOliveOil
        {
            get
            {
                return presentOliveOil;
            }
            set
            {
                if (presentOliveOil == value)
                    return;

                presentOliveOil = value;
                IsDirty = true;
            }
        }

        private string canolaOpportunity;
        public string CanolaOpportunity
        {
            get
            {
                return canolaOpportunity;
            }
            set
            {
                if (canolaOpportunity == value)
                    return;

                canolaOpportunity = value;
                IsDirty = true;
            }
        }

        private string thermometerLeft;
        public string ThermometerLeft
        {
            get
            {
                return thermometerLeft;
            }
            set
            {
                if (thermometerLeft == value)
                    return;

                thermometerLeft = value;
                IsDirty = true;
            }
        }

        private string referral;
        public string Referral
        {
            get
            {
                return referral;
            }
            set
            {
                if (referral == value)
                    return;

                referral = value;
                IsDirty = true;
            }
        }

        private string approved;
        public string Approved
        {
            get
            {
                return approved;
            }
            set
            {
                if (approved == value)
                    return;

                approved = value;
                IsDirty = true;
            }
        }

        private string cRMUpdated;
        public string CRMUpdated
        {
            get
            {
                return cRMUpdated;
            }
            set
            {
                if (cRMUpdated == value)
                    return;

                cRMUpdated = value;
                IsDirty = true;
            }
        }

        private string hOFollowUp;
        public string HOFollowUp
        {
            get
            {
                return hOFollowUp;
            }
            set
            {
                if (hOFollowUp == value)
                    return;

                hOFollowUp = value;
                IsDirty = true;
            }
        }


        private string operatingInstructions;
        public string OperatingInstructions
        {
            get
            {
                return operatingInstructions;
            }
            set
            {
                if (operatingInstructions == value)
                    return;

                operatingInstructions = value;
                IsDirty = true;
            }
        }

        private int operatingInstrRating;
        public int OperatingInstrRating
        {
            get
            {
                return operatingInstrRating;
            }
            set
            {
                if (operatingInstrRating == value)
                    return;

                operatingInstrRating = value;
                IsDirty = true;
            }
        }

        private string tankCleaning;
        public string TankCleaning
        {
            get
            {
                return tankCleaning;
            }
            set
            {
                if (tankCleaning == value)
                    return;

                tankCleaning = value;
                IsDirty = true;
            }
        }

        private int tankCleaningRating;
        public int TankCleaningRating
        {
            get
            {
                return tankCleaningRating;
            }
            set
            {
                if (tankCleaningRating == value)
                    return;

                tankCleaningRating = value;
                IsDirty = true;
            }
        }

        private int fTConditionRating;
        public int FTConditionRating
        {
            get
            {
                return fTConditionRating;
            }
            set
            {
                if (fTConditionRating == value)
                    return;

                fTConditionRating = value;
                IsDirty = true;
            }
        }

        private string followUpOnFT;
        public string FollowUpOnFT
        {
            get
            {
                return followUpOnFT;
            }
            set
            {
                if (followUpOnFT == value)
                    return;

                followUpOnFT = value;
                IsDirty = true;
            }
        }

        private int followUpOnFTRating;
        public int FollowUpOnFTRating
        {
            get
            {
                return followUpOnFTRating;
            }
            set
            {
                if (followUpOnFTRating == value)
                    return;

                followUpOnFTRating = value;
                IsDirty = true;
            }
        }

        private string wTCondition;
        public string WTCondition
        {
            get
            {
                return wTCondition;
            }
            set
            {
                if (wTCondition == value)
                    return;

                wTCondition = value;
                IsDirty = true;
            }
        }

        private int wTConditionRating;
        public int WTConditionRating
        {
            get
            {
                return wTConditionRating;
            }
            set
            {
                if (wTConditionRating == value)
                    return;

                wTConditionRating = value;
                IsDirty = true;
            }
        }

        private string followUpOnWT;
        public string FollowUpOnWT
        {
            get
            {
                return followUpOnWT;
            }
            set
            {
                if (followUpOnWT == value)
                    return;

                followUpOnWT = value;
                IsDirty = true;
            }
        }

        private int followUpOnWTRating;
        public int FollowUpOnWTRating
        {
            get
            {
                return followUpOnWTRating;
            }
            set
            {
                if (followUpOnWTRating == value)
                    return;

                followUpOnWTRating = value;
                IsDirty = true;
            }
        }


        private string checkOnFM;
        public string CheckOnFM
        {
            get
            {
                return checkOnFM;
            }
            set
            {
                if (checkOnFM == value)
                    return;

                checkOnFM = value;
                IsDirty = true;
            }
        }


        private int checkOnFMRating;
        public int CheckOnFMRating
        {
            get
            {
                return checkOnFMRating;
            }
            set
            {
                if (checkOnFMRating == value)
                    return;

                checkOnFMRating = value;
                IsDirty = true;
            }
        }

        private string commentOnService;
        public string CommentOnService
        {
            get
            {
                return commentOnService;
            }
            set
            {
                if (commentOnService == value)
                    return;

                commentOnService = value;
                IsDirty = true;
            }
        }

        private int commentOnServiceRating;
        public int CommentOnServiceRating
        {
            get
            {
                return commentOnServiceRating;
            }
            set
            {
                if (commentOnServiceRating == value)
                    return;

                commentOnServiceRating = value;
                IsDirty = true;
            }
        }

        private string fryer1Temp;
        public string Fryer1Temp
        {
            get
            {
                return fryer1Temp;
            }
            set
            {
                if (fryer1Temp == value)
                    return;

                fryer1Temp = value;
                IsDirty = true;
            }
        }

        private string fryer2Temp;
        public string Fryer2Temp
        {
            get
            {
                return fryer2Temp;
            }
            set
            {
                if (fryer2Temp == value)
                    return;

                fryer2Temp = value;
                IsDirty = true;
            }
        }

        private string fryer3Temp;
        public string Fryer3Temp
        {
            get
            {
                return fryer3Temp;
            }
            set
            {
                if (fryer3Temp == value)
                    return;

                fryer3Temp = value;
                IsDirty = true;
            }
        }


        private string fryer4Temp;
        public string Fryer4Temp
        {
            get
            {
                return fryer4Temp;
            }
            set
            {
                if (fryer4Temp == value)
                    return;

                fryer4Temp = value;
                IsDirty = true;
            }
        }

        private string fTCondition;
        public string FTCondition
        {
            get
            {
                return fTCondition;
            }
            set
            {
                if (fTCondition == value)
                    return;

                fTCondition = value;
                IsDirty = true;
            }
        }

        private string fTSize;
        public string FTSize
        {
            get
            {
                return fTSize;
            }
            set
            {
                if (fTSize == value)
                    return;

                fTSize = value;
                IsDirty = true;
            }
        }

        private double fryer1OilLife;
        public double Fryer1OilLife
        {
            get
            {
                return fryer1OilLife;
            }
            set
            {
                if (fryer1OilLife == value)
                    return;

                fryer1OilLife = value;
                IsDirty = true;
            }
        }

        private double fryer2OilLife;
        public double Fryer2OilLife
        {
            get
            {
                return fryer2OilLife;
            }
            set
            {
                if (fryer2OilLife == value)
                    return;

                fryer2OilLife = value;
                IsDirty = true;
            }
        }

        private double fryer3OilLife;
        public double Fryer3OilLife
        {
            get
            {
                return fryer3OilLife;
            }
            set
            {
                if (fryer3OilLife == value)
                    return;

                fryer3OilLife = value;
                IsDirty = true;
            }
        }

        private double fryer4OilLife;
        public double Fryer4OilLife
        {
            get
            {
                return fryer4OilLife;
            }
            set
            {
                if (fryer4OilLife == value)
                    return;

                fryer4OilLife = value;
                IsDirty = true;
            }
        }

        private string additionalComments;
        public string AdditionalComments
        {
            get
            {
                return additionalComments;
            }
            set
            {
                if (additionalComments == value)
                    return;

                additionalComments = value;
                IsDirty = true;
            }
        }

        private string customerComments;
        public string CustomerComments
        {
            get
            {
                return customerComments;
            }
            set
            {
                if (customerComments == value)
                    return;

                customerComments = value;
                IsDirty = true;
            }
        }

        private string generalComments;
        public string GeneralComments
        {
            get
            {
                return generalComments;
            }
            set
            {
                if (generalComments == value)
                    return;

                generalComments = value;
                IsDirty = true;
            }
        }


        private string createdBy;
        public string CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                if (createdBy == value)
                    return;

                createdBy = value;
                IsDirty = true;
            }
        }


        private DateTime createdDate;
        public DateTime CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                if (createdDate == value)
                    return;

                createdDate = value;
                IsDirty = true;
            }
        }

        private string lastModifiedBy;
        public string LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                if (lastModifiedBy == value)
                    return;

                lastModifiedBy = value;
                IsDirty = true;
            }
        }

        private DateTime lastModifiedDate;
        public DateTime LastModifiedDate
        {
            get
            {
                return lastModifiedDate;
            }
            set
            {
                if (lastModifiedDate == value)
                    return;

                lastModifiedDate = value;
                IsDirty = true;
            }
        }

        private string sellWTServiceCurrentlyUsing;
        public string SellWTServiceCurrentlyUsing
        {
            get
            {
                return sellWTServiceCurrentlyUsing;
            }
            set
            {
                if (sellWTServiceCurrentlyUsing == value)
                    return;

                sellWTServiceCurrentlyUsing = value;
                IsDirty = true;
            }
        }


        private string bDMName;
        public string BDMName
        {
            get
            {
                return bDMName;
            }
            set
            {
                if (bDMName == value)
                    return;

                bDMName = value;
                IsDirty = true;
            }
        }

        private string isWasteCustomer;
        public string IsWasteCustomer
        {
            get
            {
                return isWasteCustomer;
            }
            set
            {
                isWasteCustomer = value;
            }
        }

        private string oilAge;
        public string OilAge
        {
            get
            {
                return oilAge;
            }
            set
            {
                oilAge = value;
            }
        }
        
    }
}
