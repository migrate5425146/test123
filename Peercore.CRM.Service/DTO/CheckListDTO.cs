﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class CheckListDTO : BaseDTO
    {
        #region - bool -
        private bool isChecked = false;
        public bool IsChecked
        {
            get { return isChecked; }
            set { isChecked = value; }
        }

        private bool isMandatory = false;
        public bool IsMandatory
        {
            get { return isMandatory; }
            set { isMandatory = value; }
        }

        #endregion

        #region - int -
        public int ChecklistID { get; set; }
        public int PipelineStageID { get; set; }
        private int rowCount;
        public int RowCount
        {
            get { return rowCount; }
            set { rowCount = value; IsDirty = true; }
        }

        private int leadId;
        public int LeadId
        {
            get { return leadId; }
            set { leadId = value; }
        }

        #endregion

        #region - string -
        public string ChecklistName { get; set; }

        private string mandatory;
        public string Mandatory
        {
            get
            {
                return mandatory;
            }
            set
            {
                mandatory = value;
            }
        }
        #endregion

        public int checklist_type_id { get; set; }
        public string CheckListType { get; set; }

    }
}
