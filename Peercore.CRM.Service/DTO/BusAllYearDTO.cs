﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class BusAllYearDTO :BaseDTO
    {
        public string Header { get; set; }
        public double CurrentYear { get; set; }
        public double LastYear { get; set; }
        public double Year1 { get; set; }
        public double Year2 { get; set; }
        public double Year3 { get; set; }
        public double Year4 { get; set; }
    }
}
