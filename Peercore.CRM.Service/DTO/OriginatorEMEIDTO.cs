﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class OriginatorEMEIDTO
    {
        public int id { get; set; }
        public string originator { get; set; }
        public string mobile_no { get; set; }
        public string emei_no { get; set; }
    }
}
