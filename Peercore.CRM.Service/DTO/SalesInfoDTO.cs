﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class SalesInfoDTO :BaseDTO 
    {
        public string Brand { get; set; }
        public string Description { get; set; }
        public string CatlogCode { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public string RepCode { get; set; }
        public string RepName { get; set; }
        public string StateCode { get; set; }
        public string StateName { get; set; }

        public double Sum1 { get; set; }
        public double Sum2 { get; set; }
        public double Sum3 { get; set; }
        public double Sum4 { get; set; }
        public double Sum5 { get; set; }
        public double Sum6 { get; set; }
        public double Sum7 { get; set; }
        public double Sum8 { get; set; }
        public double Sum9 { get; set; }
        public double Sum10 { get; set; }
        public double Sum11 { get; set; }
        public double Sum12 { get; set; }
        public double Sum13 { get; set; }
        public double Sum14 { get; set; }
        public double Sum15 { get; set; }
        public double Sum16 { get; set; }
        public double Sum17 { get; set; }
        public double Sum18 { get; set; }

        public string Col1 { get; set; }
        public string Col2 { get; set; }
        public string Col3 { get; set; }
        public string Col4 { get; set; }
        public string Col5 { get; set; }
        public string Col6 { get; set; }
        public string Col7 { get; set; }
        public string Col8 { get; set; }
        public string Col9 { get; set; }
        public string Col10 { get; set; }

        public double Dollar1 { get; set; }
        public double Dollar2 { get; set; }
        public double Dollar3 { get; set; }
        public double Dollar4 { get; set; }
        public double Dollar5 { get; set; }
        public double Dollar6 { get; set; }
        public double Tonnes1 { get; set; }
        public double Tonnes2 { get; set; }
        public double Tonnes3 { get; set; }
        public double Tonnes4 { get; set; }
        public double Tonnes5 { get; set; }
        public double Tonnes6 { get; set; }

        public double VolYear1 { get; set; }
        public double VolYear2 { get; set; }
        public double VolYear3 { get; set; }

        public double ValYear1 { get; set; }
        public double ValYear2 { get; set; }
        public double ValYear3 { get; set; }
    }
}
