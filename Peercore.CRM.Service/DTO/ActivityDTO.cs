﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class ActivityDTO : BaseDTO
    {
        #region - bool -
        private bool hasChecklist;
        public bool HasChecklist
        {
            get { return hasChecklist; }
            set { hasChecklist = value; IsDirty = true; }
        }
        private bool isServiceCall;
        public bool IsServiceCall
        {
            get { return isServiceCall; }
            set { isServiceCall = value; IsDirty = true; }
        }
        private bool reminderCreated;
        public bool ReminderCreated
        {
            get { return reminderCreated; }
            set { reminderCreated = value; IsDirty = true; }
        }
        #endregion

        #region - DateTime -
        private DateTime? createdDate;
        public DateTime? CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; IsDirty = true; }
        }
        private DateTime endDate;
        public DateTime EndDate
        {
            get { return endDate; }
            set { endDate = value; IsDirty = true; }
        }
        private DateTime? lastModifiedDate;
        public DateTime? LastModifiedDate
        {
            get { return lastModifiedDate; }
            set { lastModifiedDate = value; IsDirty = true; }
        }
        private DateTime? reminderDate;
        public DateTime? ReminderDate
        {
            get { return reminderDate; }
            set { reminderDate = value; IsDirty = true; }
        }
        private DateTime startDate;
        public DateTime StartDate
        {
            get { return startDate; }
            set { startDate = value; IsDirty = true; }
        }
        #endregion

        #region - int -
        private int activityCount;
        public int ActivityCount
        {
            get { return activityCount; }
            set { activityCount = value; IsDirty = true; }
        }
        private int activityID;
        public int ActivityID
        {
            get { return activityID; }
            set { activityID = value; IsDirty = true; }
        }
        private int appointmentID;
        public int AppointmentID
        {
            get { return appointmentID; }
            set { appointmentID = value; IsDirty = true; }
        }
        private int checklistID;
        public int ChecklistID
        {
            get { return checklistID; }
            set { checklistID = value; IsDirty = true; }
        }
        private int leadID;
        public int LeadID
        {
            get { return leadID; }
            set { leadID = value; IsDirty = true; }
        }
        private int leadStageID;
        public int LeadStageID
        {
            get { return leadStageID; }
            set { leadStageID = value; IsDirty = true; }
        }
        private int parentActivityID;
        public int ParentActivityID
        {
            get { return parentActivityID; }
            set { parentActivityID = value; IsDirty = true; }
        }
        private int repGroupID;
        public int RepGroupID
        {
            get { return repGroupID; }
            set { repGroupID = value; IsDirty = true; }
        }
        private int callCycleID;
        public int CallCycleID
        {
            get { return callCycleID; }
            set { callCycleID = value; IsDirty = true; }
        }
        //private int rowCount;
        //public int RowCount
        //{
        //    get { return rowCount; }
        //    set { rowCount = value; IsDirty = true; }
        //}
        #endregion

        #region - string -
        private string activityType;
        public string ActivityType
        {
            get { return activityType; }
            set { activityType = value; IsDirty = true; }
        }
        private string activityTypeDescription;
        public string ActivityTypeDescription
        {
            get { return activityTypeDescription; }
            set { activityTypeDescription = value; IsDirty = true; }
        }
        private string assignedTo;
        public string AssignedTo
        {
            get { return assignedTo; }
            set { assignedTo = value; IsDirty = true; }
        }
        private string comments;
        public string Comments
        {
            get { return comments; }
            set { comments = value; IsDirty = true; }
        }
        private string createdBy;
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; IsDirty = true; }
        }
        private string customerCode;
        public string CustomerCode
        {
            get { return customerCode; }
            set { customerCode = value; IsDirty = true; }
        }
        private string lastModifiedBy;
        public string LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; IsDirty = true; }
        }
        private string priority;
        public string Priority
        {
            get { return priority; }
            set { priority = value; IsDirty = true; }
        }
        private string priorityDescription;
        public string PriorityDescription
        {
            get { return priorityDescription; }
            set { priorityDescription = value; IsDirty = true; }
        }
        private string repGroupName;
        public string RepGroupName
        {
            get { return repGroupName; }
            set { repGroupName = value; IsDirty = true; }
        }
        private string sendReminder;
        public string SendReminder
        {
            get { return sendReminder; }
            set { sendReminder = value; IsDirty = true; }
        }
        private string sentMail;
        public string SentMail
        {
            get { return sentMail; }
            set { sentMail = value; IsDirty = true; }
        }
        private string status;
        public string Status
        {
            get { return status; }
            set { status = value; IsDirty = true; }
        }
        private string statusDescription;
        public string StatusDescription
        {
            get { return statusDescription; }
            set { statusDescription = value; IsDirty = true; }
        }
        private string subject;
        public string Subject
        {
            get { return subject; }
            set { subject = value; IsDirty = true; }
        }

        private string endUserCode;
        public string EndUserCode
        {
            get { return endUserCode; }
            set { endUserCode = value; IsDirty = true; }
        } 
        #endregion


        #region - Activity Types - 

        public string Code { get; set; }
        public string Description { get; set; }
        public string EmailAddresses { get; set; }
        public string EmailTemplate { get; set; }
        public string ActivityTypeColour { get; set; }
        public string SetAsDefault { get; set; }
        public string ColdCallActivity { get; set; }
        public bool SetAsDefaultChecked
        {
            get
            {
                if (SetAsDefault == "Y")
                    return true;
                else
                    return false;
            }
        }

        public string ShowInPlanner { get; set; }
        public bool ColdCallActivityChecked
        {
            get
            {
                if (ColdCallActivity == "Y")
                    return true;
                else
                    return false;
            }
        }
        public bool ShowInPlannerChecked
        {
            get
            {
                if (ShowInPlanner == "Y")
                    return true;
                else
                    return false;
            }
            set { }
        }

        public string RepCode { get; set; }
        public int RouteID {get; set;}

        #endregion - Activity Types -


    }
}
