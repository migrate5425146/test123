﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class ArgsDTO
    {
        /*private string chiildOriginators;
        public string ChiildOriginators
        {
            get { return chiildOriginators; }
            set { chiildOriginators = value; }
        }

        private string originator;
        public string Originator
        {
            get { return originator; }
            set { originator = value; }
        }

        private string defaultDepartmentId;
        public string DefaultDepartmentId
        {
            get { return defaultDepartmentId; }
            set { defaultDepartmentId = value; }
        }

        private int startIndex;
        public int StartIndex
        {
            get { return startIndex; }
            set { startIndex = value; }
        }

        private int rowCount;
        public int RowCount
        {
            get { return rowCount; }
            set { rowCount = value; }
        }

        private string orderBy;
        public string OrderBy
        {
            get { return orderBy; }
            set { orderBy = value; }
        }

        private string additionalParams;
        public string AdditionalParams
        {
            get { return additionalParams; }
            set { additionalParams = value; }
        }

        private string addressType;
        public string AddressType
        {
            get { return addressType; }
            set { addressType = value; }
        }

        private bool activeInactiveChecked;
        public bool ActiveInactiveChecked
        {
            get
            {
                return activeInactiveChecked;
            }
            set
            {
                if (activeInactiveChecked == value)
                    return;

                activeInactiveChecked = value;
            }
        }

        private bool reqSentIsChecked;
        public bool ReqSentIsChecked
        {
            get
            {
                return reqSentIsChecked;
            }
            set
            {
                if (reqSentIsChecked == value)
                    return;

                reqSentIsChecked = value;
            }
        }

        private int crmAuthLevel;
        public int CRMAuthLevel
        {
            get
            {
                return crmAuthLevel;
            }
            set
            {
                if (crmAuthLevel == value)
                    return;

                crmAuthLevel = value;
            }
        }

        private string repTerritory;
        public string RepTerritory
        {
            get { return repTerritory; }
            set { repTerritory = value; }
        }

        #region Activity Scheduler

        private string sSource = string.Empty;
        public string SSource
        {
            get
            {
                return sSource;
            }
            set
            {
                if (sSource == value)
                    return;

                sSource = value;
            }
        }

        private string floor;
        public string Floor
        {
            get
            {
                return floor;
            }
            set
            {
                if (floor == value)
                    return;

                floor = value;
            }
        }

        private string sToday = string.Empty;
        public string SToday
        {
            get
            {
                return sToday;
            }
            set
            {
                if (sToday == value)
                    return;

                sToday = value;
            }
        }

        private string sStartDate = string.Empty;
        public string SStartDate
        {
            get
            {
                return sStartDate;
            }
            set
            {
                if (sStartDate == value)
                    return;

                sStartDate = value;
            }
        }

        private string sEndDate = string.Empty;
        public string SEndDate
        {
            get
            {
                return sEndDate;
            }
            set
            {
                if (sEndDate == value)
                    return;

                sEndDate = value;
            }
        }

        private string status = string.Empty;
        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                if (status == value)
                    return;

                status = value;
            }
        }

        private bool managerMode;
        public bool ManagerMode
        {
            get
            {
                return managerMode;
            }
            set
            {
                if (managerMode == value)
                    return;

                managerMode = value;
            }
        }

        private int originatorId;
        public int OriginatorId
        {
            get
            {
                return originatorId;
            }
            set
            {
                if (originatorId == value)
                    return;

                originatorId = value;
            }
        }

        private int leadStageId;
        public int LeadStageId
        {
            get { return leadStageId; }
            set { leadStageId = value; }
        }

        private string leadStageName;
        public string LeadStageName
        {
            get { return leadStageName; }
            set { leadStageName = value; }
        }

        private string activityStatus;
        public string ActivityStatus
        {
            get { return activityStatus; }
            set { activityStatus = value; }
        }

        private string activityType;
        public string ActivityType
        {
            get { return activityType; }
            set { activityType = value; }
        }

        private string rOriginator;
        public string ROriginator
        {
            get { return rOriginator; }
            set { rOriginator = value; }
        }

        private string sFilter;
        public string SFilter
        {
            get { return sFilter; }
            set { sFilter = value; }
        }

        private DateTime dtStartDate;
        public DateTime DtStartDate
        {
            get
            {
                return dtStartDate;
            }
            set
            {
                dtStartDate = value;
            }
        }

        private DateTime dtEndDate;
        public DateTime DtEndDate
        {
            get
            {
                return dtEndDate;
            }
            set
            {
                dtEndDate = value;
            }
        }

        private int sYear;
        public int SYear
        {
            get { return sYear; }
            set { sYear = value; }
        }

        private int sMonth;
        public int SMonth
        {
            get { return sMonth; }
            set { sMonth = value; }
        }

        private int sNoofMonths;
        public int SNoofMonths
        {
            get { return sNoofMonths; }
            set { sNoofMonths = value; }
        }

        //public List<BusinessRules.WeekInfo> WeekInfo { get; set; }


        private bool isIncludeContact;
        public bool IsIncludeContact
        {
            get
            {
                return isIncludeContact;
            }
            set
            {
                isIncludeContact = value;
            }
        }

        private bool isIncludeLead;
        public bool IsIncludeLead
        {
            get
            {
                return isIncludeLead;
            }
            set
            {
                isIncludeLead = value;
            }
        }

        private bool isIncludeCustomer;
        public bool IsIncludeCustomer
        {
            get
            {
                return isIncludeCustomer;
            }
            set
            {
                isIncludeCustomer = value;
            }
        }

        private string leadstage;
        public string LeadStage
        {
            get { return leadstage; }
            set { leadstage = value; }
        }

        private string sector;
        public string Sector
        {
            get { return sector; }
            set { sector = value; }
        }

        private string enduserCode;
        public string EnduserCode
        {
            get { return enduserCode; }
            set { enduserCode = value; }
        }

        private string repCode;
        public string RepCode
        {
            get { return repCode; }
            set { repCode = value; }
        }

        private string customerCode;
        public string CustomerCode
        {
            get { return customerCode; }
            set { customerCode = value; }
        }

        private string repType;
        public string RepType
        {
            get { return repType; }
            set { repType = value; }
        }

        private string childRepCodelist;
        public string ChildRepCodelist
        {
            get { return childRepCodelist; }
            set { childRepCodelist = value; }
        }

        private List<string> repCodeList = null;
        public List<string> RepCodeList
        {
            get
            {
                return repCodeList;
            }
            set
            {
                repCodeList = value;
            }
        }

        #endregion
         */

        private string childOriginators;
        public string ChildOriginators
        {
            get { return childOriginators; }
            set { childOriginators = value; }
        }

        private string originator;
        public string Originator
        {
            get { return originator; }
            set { originator = value; }
        }

        private string originatorType;
        public string OriginatorType
        {
            get { return originatorType; }
            set { originatorType = value; }
        }

        private string defaultDepartmentId;
        public string DefaultDepartmentId
        {
            get { return defaultDepartmentId; }
            set { defaultDepartmentId = value; }
        }

        private int startIndex;
        public int StartIndex
        {
            get { return startIndex; }
            set { startIndex = value; }
        }

        private int rowCount;
        public int RowCount
        {
            get { return rowCount; }
            set { rowCount = value; }
        }

        private int showCount;
        public int ShowCount
        {
            get { return showCount; }
            set { showCount = value; }
        }

        private string orderBy;
        public string OrderBy
        {
            get { return orderBy; }
            set { orderBy = value; }
        }

        private string additionalParams;
        public string AdditionalParams
        {
            get { return additionalParams; }
            set { additionalParams = value; }
        }

        private string addressType;
        public string AddressType
        {
            get { return addressType; }
            set { addressType = value; }
        }

        private bool activeInactiveChecked;
        public bool ActiveInactiveChecked
        {
            get
            {
                return activeInactiveChecked;
            }
            set
            {
                if (activeInactiveChecked == value)
                    return;

                activeInactiveChecked = value;
            }
        }

        private bool reqSentIsChecked;
        public bool ReqSentIsChecked
        {
            get
            {
                return reqSentIsChecked;
            }
            set
            {
                if (reqSentIsChecked == value)
                    return;

                reqSentIsChecked = value;
            }
        }

        private int crmAuthLevel;
        public int CRMAuthLevel
        {
            get
            {
                return crmAuthLevel;
            }
            set
            {
                if (crmAuthLevel == value)
                    return;

                crmAuthLevel = value;
            }
        }

        #region Activity Scheduler

        private string sSource = string.Empty;
        public string SSource
        {
            get
            {
                return sSource;
            }
            set
            {
                if (sSource == value)
                    return;

                sSource = value;
            }
        }

        private string floor;
        public string Floor
        {
            get
            {
                return floor;
            }
            set
            {
                if (floor == value)
                    return;

                floor = value;
            }
        }

        private string sToday = string.Empty;
        public string SToday
        {
            get
            {
                return sToday;
            }
            set
            {
                if (sToday == value)
                    return;

                sToday = value;
            }
        }

        private string sStartDate = string.Empty;
        public string SStartDate
        {
            get
            {
                return sStartDate;
            }
            set
            {
                if (sStartDate == value)
                    return;

                sStartDate = value;
            }
        }

        private string sEndDate = string.Empty;
        public string SEndDate
        {
            get
            {
                return sEndDate;
            }
            set
            {
                if (sEndDate == value)
                    return;

                sEndDate = value;
            }
        }

        private string status = string.Empty;
        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                if (status == value)
                    return;

                status = value;
            }
        }

        private bool managerMode;
        public bool ManagerMode
        {
            get
            {
                return managerMode;
            }
            set
            {
                if (managerMode == value)
                    return;

                managerMode = value;
            }
        }

        private int originatorId;
        public int OriginatorId
        {
            get
            {
                return originatorId;
            }
            set
            {
                if (originatorId == value)
                    return;

                originatorId = value;
            }
        }

        private int leadStageId;
        public int LeadStageId
        {
            get { return leadStageId; }
            set { leadStageId = value; }
        }

        private string leadStageName;
        public string LeadStageName
        {
            get { return leadStageName; }
            set { leadStageName = value; }
        }

        private string activityStatus;
        public string ActivityStatus
        {
            get { return activityStatus; }
            set { activityStatus = value; }
        }

        private string rOriginator;
        public string ROriginator
        {
            get { return rOriginator; }
            set { rOriginator = value; }
        }

        private string sFilter;
        public string SFilter
        {
            get { return sFilter; }
            set { sFilter = value; }
        }

        private DateTime dtStartDate;
        public DateTime DtStartDate
        {
            get
            {
                return dtStartDate;
            }
            set
            {
                dtStartDate = value;
            }
        }

        private DateTime dtEndDate;
        public DateTime DtEndDate
        {
            get
            {
                return dtEndDate;
            }
            set
            {
                dtEndDate = value;
            }
        }

        private int sYear;
        public int SYear
        {
            get { return sYear; }
            set { sYear = value; }
        }

        private int sMonth;
        public int SMonth
        {
            get { return sMonth; }
            set { sMonth = value; }
        }

        private int sNoofMonths;
        public int SNoofMonths
        {
            get { return sNoofMonths; }
            set { sNoofMonths = value; }
        }

        //public List<BusinessRules.WeekInfo> WeekInfo { get; set; }

        private bool isIncludeContact;
        public bool IsIncludeContact
        {
            get
            {
                return isIncludeContact;
            }
            set
            {
                isIncludeContact = value;
            }
        }

        private bool isIncludeLead;
        public bool IsIncludeLead
        {
            get
            {
                return isIncludeLead;
            }
            set
            {
                isIncludeLead = value;
            }
        }

        private bool isIncludeCustomer;
        public bool IsIncludeCustomer
        {
            get
            {
                return isIncludeCustomer;
            }
            set
            {
                isIncludeCustomer = value;
            }
        }

        private string leadstage;
        public string LeadStage
        {
            get { return leadstage; }
            set { leadstage = value; }
        }

        #endregion

        private bool isOpportunityAutoGenerated;
        public bool IsOpportunityAutoGenerated
        {
            get { return isOpportunityAutoGenerated; }
            set { isOpportunityAutoGenerated = value; }
        }

        private string activityDayStartDate = string.Empty;
        public string ActivityDayStartDate
        {
            get
            {
                return activityDayStartDate;
            }
            set
            {
                if (activityDayStartDate == value)
                    return;

                activityDayStartDate = value;
            }
        }

        private string activityDayEndDate = string.Empty;
        public string ActivityDayEndDate
        {
            get
            {
                return activityDayEndDate;
            }
            set
            {
                if (activityDayEndDate == value)
                    return;

                activityDayEndDate = value;
            }
        }

        private string sector;
        public string Sector
        {
            get { return sector; }
            set { sector = value; }
        }

        private string enduserCode;
        public string EnduserCode
        {
            get { return enduserCode; }
            set { enduserCode = value; }
        }

        private string customerCode;
        public string CustomerCode
        {
            get { return customerCode; }
            set { customerCode = value; }
        }

        public int LeadId { get; set; }

        public string BDMList { get; set; }

        public string ClientType { get; set; } // Originator Client Type

        public string RepCode { get; set; }
        public string RepType { get; set; }         // Rep Type - Bakery / Food Services
        public List<string> RepCodeList { get; set; } // Originator can have multiple Rep Codes

        private string childReps = string.Empty;
        public string ChildReps
        {
            get
            {
                return childReps;
            }
            set
            {
                childReps = value;
            }
        }

        public string CustomerType { get; set; } // Lead/Customer/EndUser

        public int Period { get; set; }


        public string Market { get; set; }

        public string State { get; set; }

        public int MarketId { get; set; }

        public string Brand { get; set; }

        //public string Quantitytype { get; set; }

        private string sQuantitytype = string.Empty;
        public string SQuantitytype
        {
            get
            {
                return sQuantitytype;
            }
            set
            {
                if (sQuantitytype == value)
                    return;

                sQuantitytype = value;
            }
        }
    }
}
