﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class IncentiveOptionLevelDTO:BaseDTO
    {
        public int IpOptionLevelId { get; set; }
        public int IncentivePlanDetailsId { get; set; }
        public double MinQuantity { get; set; }
        public double MinValue { get; set; }
        public double MaxQuantity { get; set; }
        public double MaxValue { get; set; }
        public double IncentiveValue { get; set; }
        public double IncentivePercentage { get; set; }
        public bool IsMonthlyAllowanceIncluded { get; set; }
        public bool IsActive { get; set; }

        public List<IncentiveOptionLevelCombinationDTO> IncentiveOptionLevelCombinationList { get; set; }
        public List<IncentiveDiscountSchemeDTO> IncentiveDiscountSchemeList { get; set; }

        private string createdBy;
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        private DateTime? createdDate;
        public DateTime? CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        private string lastModifiedBy;
        public string LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; }
        }

        private DateTime? lastModifiedDate;
        public DateTime? LastModifiedDate
        {
            get { return lastModifiedDate; }
            set { lastModifiedDate = value; }
        }
    }
}
