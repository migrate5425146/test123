﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class LoadStockDetailDTO : BaseDTO 
    {
        private string catalogCode;
        public string CatalogCode
        {
            get { return catalogCode; }
            set { IsDirty = true; catalogCode = value; }
        }

        private string catalogName;
        public string CatalogName
        {
            get { return catalogName; }
            set { IsDirty = true; catalogName = value; }
        }

        private double allocQty;
        public double AllocQty
        {
            get { return allocQty; }
            set { IsDirty = true; allocQty = value; }
        }

        private double adjustQty;
        public double AdjustQty
        {
            get { return adjustQty; }
            set { IsDirty = true; adjustQty = value; }
        }

        private double loadQty;
        public double LoadQty
        {
            get { return loadQty; }
            set { IsDirty = true; loadQty = value; }
        }

        private double balanceQty;
        public double BalanceQty
        {
            get { return balanceQty; }
            set { IsDirty = true; balanceQty = value; }
        }

        private double returnQty;
        public double ReturnQty
        {
            get { return returnQty; }
            set { IsDirty = true; returnQty = value; }
        }

        private string returnReason;
        public string ReturnReason
        {
            get { return returnReason; }
            set { IsDirty = true; returnReason = value; }
        }

        private string movementType;
        public string MovementType
        {
            get { return movementType; }
            set { IsDirty = true; movementType = value; }
        }

        private int stockHeaderId;
        public int StockHeaderId
        {
            get { return stockHeaderId; }
            set { IsDirty = true; stockHeaderId = value; }
        }

        private int sku;
        public int Sku
        {
            get { return sku; }
            set { IsDirty = true; sku = value; }
        }

        private int stockDetailId;
        public int StockDetailId
        {
            get { return stockDetailId; }
            set { IsDirty = true; stockDetailId = value; }
        }

        //To hold returning amount of units at a time: for the purpose of inserting to Stock Movement
        private double returningQty;
        public double ReturningQty
        {
            get { return returningQty; }
            set { 
                IsDirty = true;                 
                returningQty = value; 
            }
        }

        private double vehicleBalanceQty;
        public double VehicleBalanceQty
        {
            get { return vehicleBalanceQty; }
            set { IsDirty = true; vehicleBalanceQty = value; }
        }

        private double loadQtyOld;
        public double LoadQtyOld
        {
            get { return loadQtyOld; }
            set { loadQtyOld = value; IsDirty = true; }
        }

        private double vehicleReturnQty;
        public double VehicleReturnQty
        {
            get { return vehicleReturnQty; }
            set { vehicleReturnQty = value; IsDirty = true; }
        }

        private double vehicleReturningQty;
        public double VehicleReturningQty
        {
            get { return vehicleReturningQty; }
            set { vehicleReturningQty = value; IsDirty = true; }
        }

        private int stockTotalId;
        public int StockTotalId
        {
            get { return stockTotalId; }
            set { stockTotalId = value; }
        }

        private double allocationQtyTotal;
        public double AllocationQtyTotal
        {
            get { return allocationQtyTotal; }
            set { allocationQtyTotal = value; }
        }

        private int catalogId;
        public int CatalogId
        {
            get { return catalogId; }
            set { catalogId = value; }
        }
        
        private int recordId;
        public int RecordId
        {
            get { return recordId; }
            set { recordId = value; }
        }

    }
}
