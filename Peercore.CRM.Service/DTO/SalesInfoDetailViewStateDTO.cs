﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class SalesInfoDetailViewStateDTO : BaseDTO
    {
        public int DisplayOptionIndex { get; set; }

        public int SortFieldOptionIndex { get; set; }
        public string CodeDescription { get; set; }
        //public eSalesInfoDetailType SalesInfoDetailType { get; set; }
        //public List<SalesInfoDTO> LstSalesInfoDetailGrid { get; set; }
        //public List<CustomizedTableDTO> LstCustomizedTableGrid { get; set; }
        //public List<string> lstSalesInfoDetailGridColumnHeaders { get; set; }
        //public List<BePieChartDTO> lstPieChartValues { get; set; }
        public string sMonth { get; set; }
        public string sProductType { get; set; }
        //public string sState { get; set; }
        public string sRep { get; set; }
        public string sCustomer { get; set; }
        public string sProduct { get; set; }
        //public string sBusArea { get; set; }
        //public string sCustomerGroup { get; set; }
        //public string sSubParent { get; set; }
        public string sBrand { get; set; }
        public string sMarket { get; set; }
        //public string sCatalogueGroup { get; set; }
        //public string sCatalogueSubGroup { get; set; }
        public string sSubDetailType { get; set; }

        //public List<string> ColumnHeaders { get; set; }
        public List<TargetInfoDTO> TargetInfoEntityList { get; set; }
    }
}
