﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class LoadStockDTO : BaseDTO
    {
        private DateTime allocDate;
        public DateTime AllocDate
        {
            get { return allocDate; }
            set { allocDate = value; IsDirty = true; }
        }

        private int routeId;
        public int RouteId
        {
            get { return routeId; }
            set { routeId = value; IsDirty = true; }
        }

        private string repCode;
        public string RepCode
        {
            get { return repCode; }
            set { repCode = value; IsDirty = true; }
        }

        private int routeAssignID;
        public int RouteAssignID
        {
            get { return routeAssignID; }
            set { routeAssignID = value; IsDirty = true; }
        }

        private int stockId;
        public int StockId
        {
            get { return stockId; }
            set { stockId = value; IsDirty = true; }
        }

        
        private List<LoadStockDetailDTO> lstLoadStockDetail;
        public List<LoadStockDetailDTO> LstLoadStockDetail
        {
            get { return lstLoadStockDetail; }
            set { lstLoadStockDetail = value; IsDirty = true; }
        }

        private int distributorId;
        public int DistributorId
        {
            get { return distributorId; }
            set { distributorId = value; }
        }

        private string stockAllocType;
        public string StockAllocType
        {
            get { return stockAllocType; }
            set { stockAllocType = value; }
        }

        private string createdBy;
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        private DateTime? createdDate;
        public DateTime? CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        private string lastModifiedBy;
        public string LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; }
        }

        private DateTime? lastModifiedDate;
        public DateTime? LastModifiedDate
        {
            get { return lastModifiedDate; }
            set { lastModifiedDate = value; }
        }

        private int recordId;
        public int RecordId
        {
            get { return recordId; }
            set { recordId = value; }
        }
    }
}
