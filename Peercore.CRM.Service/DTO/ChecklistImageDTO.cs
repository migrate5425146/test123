﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class ChecklistImageDTO :BaseDTO
    {
        public int Id { get; set; }
        public int ChecklistId { get; set; }
        public string ImageName { get; set; }
        public byte[] ImageContent { get; set; }
        public string ImagePath { get; set; }
    }
}
