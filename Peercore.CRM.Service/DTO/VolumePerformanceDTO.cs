﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class VolumePerformanceDTO:BaseDTO
    {
        public int RecordId { get; set; }
        public int No { get; set; }
        public string Region { get; set; }
        public string Market { get; set; }
        public string Originator { get; set; }
        public string Route { get; set; }
        public string CustName { get; set; }
        public int Tier { get; set; }
        public string CustCode { get; set; }
        public string ProductType { get; set; }
        public double MonthleyTarget { get; set; }
        public double MonthleyActual { get; set; }
        public double TLPTarget { get; set; }
        public double AdditionalTarget { get; set; }
        public double TotalTarget { get; set; }
        public double TotalActual { get; set; }
        public double TLPAbishekaVariable { get; set; }
        public double TLPIncentive { get; set; }
        public double AbishekaIncentive { get; set; }
        public double TotalIncentive { get; set; }
    }
}
