﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class StockTotalDTO:BaseDTO
    {
        private int stockTotalId;
        public int StockTotalId
        {
            get { return stockTotalId; }
            set { stockTotalId = value; }
        }

        private int distributorId;
        public int DistributorId
        {
            get { return distributorId; }
            set { distributorId = value; }
        }

        private int catalogId;
        public int CatalogId
        {
            get { return catalogId; }
            set { catalogId = value; }
        }

        private string catalogCode;
        public string CatalogCode
        {
            get { return catalogCode; }
            set { catalogCode = value; IsDirty = true; }
        }

        private string catalogName;
        public string CatalogName
        {
            get { return catalogName; }
            set { catalogName = value; IsDirty = true; }
        }

        private double allocQty;
        public double AllocQty
        {
            get { return allocQty; }
            set { allocQty = value; IsDirty = true; }
        }

        private double loadQty;
        public double LoadQty
        {
            get { return loadQty; }
            set { loadQty = value; IsDirty = true; }
        }

        private double balanceQty;
        public double BalanceQty
        {
            get { return balanceQty; }
            set { balanceQty = value; IsDirty = true; }
        }

        private double returnQty;
        public double ReturnQty
        {
            get { return returnQty; }
            set { returnQty = value; IsDirty = true; }
        }

        private double vehicleBalanceQty;
        public double VehicleBalanceQty
        {
            get { return vehicleBalanceQty; }
            set { vehicleBalanceQty = value; IsDirty = true; }
        }

        private double vehicleReturnQty;
        public double VehicleReturnQty
        {
            get { return vehicleReturnQty; }
            set { vehicleReturnQty = value; IsDirty = true; }
        }

        private string status;
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        private string createdBy;
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        private DateTime? createdDate;
        public DateTime? CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        private string lastModifiedBy;
        public string LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; }
        }

        private DateTime? lastModifiedDate;
        public DateTime? LastModifiedDate
        {
            get { return lastModifiedDate; }
            set { lastModifiedDate = value; }
        }
    }
}
