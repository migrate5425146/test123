﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class MarketObjectiveDTO:BaseDTO
    {
        private int marketObjectiveId;
        public int MarketObjectiveId
        {
            get { return marketObjectiveId; }
            set { marketObjectiveId = value; }
        }

        private MarketDTO market;
        public MarketDTO Market
        {
            get { return market; }
            set { market = value; }
        }

        private ObjectiveDTO objective;
        public ObjectiveDTO Objective
        {
            get { return objective; }
            set { objective = value; }
        }

        private string status;
        public string Status
        {
            get { return status; }
            set { status = value; }
        }

        private string createdBy;
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }
        private DateTime? createdDate;
        public DateTime? CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }
        private string lastModifiedBy;
        public string LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; }
        }
        private DateTime? lastModifiedDate;
        public DateTime? LastModifiedDate
        {
            get { return lastModifiedDate; }
            set { lastModifiedDate = value; }
        }
    }
}
