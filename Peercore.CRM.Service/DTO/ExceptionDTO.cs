﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class ExceptionDTO:BaseDTO
    {
        private int _id = 0;
        public int ID
        {
            get { return _id; }
            set { _id = value; }
        }

        private int _originatorID = 0;
        public int OriginatorID
        {
            get { return _originatorID; }
            set { _originatorID = value; }
        }

        private string _shortDescription = string.Empty;
        public string ShortDescription
        {
            get { return _shortDescription; }
            set { _shortDescription = value; }
        }

        private string _longDescription = string.Empty;
        public string LongDescription
        {
            get { return _longDescription; }
            set { _longDescription = value; }
        }

        private string _ipAddress = string.Empty;
        public string IpAddress
        {
            get { return _ipAddress; }
            set { _ipAddress = value; }
        }

        private string _referer = string.Empty;
        public string Referer
        {
            get { return _referer; }
            set { _referer = value; }
        }

        private string _errorlink = string.Empty;
        public string Errorlink
        {
            get { return _errorlink; }
            set { _errorlink = value; }
        }

        private string _methodName = string.Empty;
        public string MethodName
        {
            get { return _methodName; }
            set { _methodName = value; }
        }

        private string _errorLineNumber = string.Empty;
        public string ErrorLineNumber
        {
            get { return _errorLineNumber; }
            set { _errorLineNumber = value; }
        }

        private DateTime? _errorDate = null;
        public DateTime? ErrorDate
        {
            get { return _errorDate; }
            set { _errorDate = value; }
        }

        private string _host=string.Empty;
        public string Host
        {
            get { return _host; }
            set { _host=value; }
        }

        private string _userLogin=string.Empty;
        public string UserLogin
        {
            get { return _userLogin; }
            set { _userLogin=value; }
        }
    }
}
