﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class RepTargetDTO:BaseDTO
    {
        public int RepTargetId { get; set; }
        public string RepCode { get; set; }
        public string RepName { get; set; }
        public double PrimaryTarAmt { get; set; }
        public double PrimaryIncAmt { get; set; }
        public DateTime? EffStartDate { get; set; }
        public DateTime? EffEndDate { get; set; }
        public string Status { get; set; }

        private string createdBy;
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        private DateTime? createdDate;
        public DateTime? CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        private string lastModifiedBy;
        public string LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; }
        }

        private DateTime? lastModifiedDate;
        public DateTime? LastModifiedDate
        {
            get { return lastModifiedDate; }
            set { lastModifiedDate = value; }
        }
    }
}
