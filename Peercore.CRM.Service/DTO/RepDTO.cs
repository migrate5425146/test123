﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class RepDTO : BaseDTO
    {

        public int Id { get; set; }
        public string RepCode { get; set; }
        public string RepName { get; set; }
        public string TempRepName { get; set; }
        public double StickTarget { get; set; }
        public int TargetYear { get; set; }
        public int TargetMonth { get; set; }
        public string TargetMonthText { get; set; }
        public double PeriodTarget { get; set; }
    }
}
