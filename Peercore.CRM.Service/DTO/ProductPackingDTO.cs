﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class ProductPackingDTO:BaseDTO
    {
        public int PackingId { get; set; }
        public string PackingName { get; set; }
    }
}
