﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class LookupDTO : BaseDTO
    {
        public object SourceId { get; set; }

        public object Code { get; set; }

        public string Description { get; set; }

        public List<KeyValuePair<string, object>> AdditionalFields { get; set; }
    }
}
