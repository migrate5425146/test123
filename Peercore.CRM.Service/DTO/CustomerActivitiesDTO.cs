﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class CustomerActivitiesDTO : BaseDTO
    {

        private int activityID;
        public int ActivityID
        {
            get
            {
                return activityID;
            }
            set
            {
                if (activityID == value)
                    return;

                activityID = value;
                IsDirty = true;
            }
        }
        private string subject;
        public string Subject
        {
            get
            {
                return subject;
            }
            set
            {
                if (subject == value)
                    return;

                subject = value;
                IsDirty = true;
            }
        }
        private DateTime startDate;
        public DateTime StartDate
        {
            get
            {
                return startDate;
            }
            set
            {
                if (startDate == value)
                    return;

                startDate = value;
                IsDirty = true;
            }
        }
        private DateTime endDate;
        public DateTime EndDate
        {
            get
            {
                return endDate;
            }
            set
            {
                if (endDate == value)
                    return;

                endDate = value;
                IsDirty = true;
            }
        }
        private string status;
        public string Status
        {
            get
            {
                return status;
            }
            set
            {
                if (status == value)
                    return;

                status = value;
                IsDirty = true;
            }
        }
        private string assignedTo;
        public string AssignedTo
        {
            get
            {
                return assignedTo;
            }
            set
            {
                if (assignedTo == value)
                    return;

                assignedTo = value;
                IsDirty = true;
            }
        }
        private int leadId;
        public int LeadId
        {
            get
            {
                return leadId;
            }
            set
            {
                if (leadId == value)
                    return;

                leadId = value;
                IsDirty = true;
            }
        }
        private string comments;
        public string Comments
        {
            get
            {
                return comments;
            }
            set
            {
                if (comments == value)
                    return;

                comments = value;
                IsDirty = true;
            }
        }
        private string sentMail;
        public string SentMail
        {
            get
            {
                return sentMail;
            }
            set
            {
                if (sentMail == value)
                    return;

                sentMail = value;
                IsDirty = true;
            }
        }
        private string priority;
        public string Priority
        {
            get
            {
                return priority;
            }
            set
            {
                if (priority == value)
                    return;

                priority = value;
                IsDirty = true;
            }
        }
        private string sendReminder;
        public string SendReminder
        {
            get
            {
                return sendReminder;
            }
            set
            {
                if (sendReminder == value)
                    return;

                sendReminder = value;
                IsDirty = true;
            }
        }
        private DateTime reminderDate;
        public DateTime ReminderDate
        {
            get
            {
                return reminderDate;
            }
            set
            {
                if (reminderDate == value)
                    return;

                reminderDate = value;
                IsDirty = true;
            }
        }
        private int pipelineStageID;
        public int PipelineStageID
        {
            get
            {
                return pipelineStageID;
            }
            set
            {
                if (pipelineStageID == value)
                    return;

                pipelineStageID = value;
                IsDirty = true;
            }
        }
        private string activityType;
        public string ActivityType
        {
            get
            {
                return activityType;
            }
            set
            {
                if (activityType == value)
                    return;

                activityType = value;
                IsDirty = true;
            }
        }
        private DateTime createdDate;
        public DateTime CreatedDate
        {
            get
            {
                return createdDate;
            }
            set
            {
                if (createdDate == value)
                    return;

                createdDate = value;
                IsDirty = true;
            }
        }
        private string createdBy;
        public string CreatedBy
        {
            get
            {
                return createdBy;
            }
            set
            {
                if (createdBy == value)
                    return;

                createdBy = value;
                IsDirty = true;
            }
        }
        private DateTime lastModifiedDate;
        public DateTime LastModifiedDate
        {
            get
            {
                return lastModifiedDate;
            }
            set
            {
                if (lastModifiedDate == value)
                    return;

                lastModifiedDate = value;
                IsDirty = true;
            }
        }
        private string lastModifiedBy;
        public string LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                if (lastModifiedBy == value)
                    return;

                lastModifiedBy = value;
                IsDirty = true;
            }
        }
        private string custCode;
        public string CustCode
        {
            get
            {
                return custCode;
            }
            set
            {
                if (custCode == value)
                    return;

                custCode = value;
                IsDirty = true;
            }
        }
        private string leadName;
        public string LeadName
        {
            get
            {
                return leadName;
            }
            set
            {
                if (leadName == value)
                    return;

                leadName = value;
                IsDirty = true;
            }
        }
        private string leadStage;
        public string LeadStage
        {
            get
            {
                return leadStage;
            }
            set
            {
                if (leadStage == value)
                    return;

                leadStage = value;
                IsDirty = true;
            }
        }
        private string statusDesc;
        public string StatusDesc
        {
            get
            {
                return statusDesc;
            }
            set
            {
                if (statusDesc == value)
                    return;

                statusDesc = value;
                IsDirty = true;
            }
        }
        private string typeDesc;
        public string TypeDesc
        {
            get
            {
                return typeDesc;
            }
            set
            {
                if (typeDesc == value)
                    return;

                typeDesc = value;
                IsDirty = true;
            }
        }
        private int noOfActivities;
        public int NoOfActivities
        {
            get
            {
                return noOfActivities;
            }
            set
            {
                if (noOfActivities == value)
                    return;

                noOfActivities = value;
                IsDirty = true;
            }
        }
        private int appointmentId;
        public int AppointmentId
        {
            get
            {
                return appointmentId;
            }
            set
            {
                if (appointmentId == value)
                    return;

                appointmentId = value;
                IsDirty = true;
            }
        }
        private string delFlag;
        public string DelFlag
        {
            get
            {
                return delFlag;
            }
            set
            {
                if (delFlag == value)
                    return;

                delFlag = value;
                IsDirty = true;
            }
        }
        private string typeColour;
        public string TypeColour
        {
            get
            {
                return typeColour;
            }
            set
            {
                if (typeColour == value)
                    return;

                typeColour = value;
                IsDirty = true;
            }
        }
        private string originatorName;
        public string OriginatorName
        {
            get
            {
                return originatorName;
            }
            set
            {
                if (originatorName == value)
                    return;

                originatorName = value;
                IsDirty = true;
            }
        }
        private string customerCity;
        public string CustomerCity
        {
            get
            {
                return customerCity;
            }
            set
            {
                if (customerCity == value)
                    return;

                customerCity = value;
                IsDirty = true;
            }
        }

        private string customerPostCode;
        public string CustomerPostCode
        {
            get
            {
                return customerPostCode;
            }
            set
            {
                if (customerPostCode == value)
                    return;

                customerPostCode = value;
                IsDirty = true;
            }
        }



        private int delCount;
        public int DelCount
        {
            get
            {
                return delCount;
            }
            set
            {
                if (delCount == value)
                    return;

                delCount = value;
                IsDirty = true;
            }
        }

        //private int rowCount;
        //public int RowCount
        //{
        //    get
        //    {
        //        return rowCount;
        //    }
        //    set
        //    {
        //        rowCount = value;
        //    }
        //}


        public string EndUserCode { get; set; }
        public string RepName { get; set; }
        public string RepCode { get; set; }
        public int routeId { get; set; }
    }
}
