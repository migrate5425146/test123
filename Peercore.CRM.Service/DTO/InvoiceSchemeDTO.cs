﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class InvoiceSchemeDTO : BaseDTO
    {
        public int InvoiceSchemeId { get; set; }
        public int SchemeDetailsId { get; set; }
        public int InvoiceId { get; set; }
        public int ProductId { get; set; }
        public double UsedQuantity { get; set; }
        public int SchemeGroupId { get; set; }
        public string ProductName { get; set; }
    }
}
