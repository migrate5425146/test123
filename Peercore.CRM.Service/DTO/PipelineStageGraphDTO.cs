﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class PipelineStageGraphDTO:BaseDTO
    {
        private int pipelineStageID;
        public int PipelineStageID
        {
            get { return pipelineStageID; }
            set { pipelineStageID = value; IsDirty = true; }
        }

        private string pipelineStage;
        public string PipelineStage
        {
            get { return pipelineStage; }
            set { pipelineStage = value; IsDirty = true; }
        }

        private int stageOrder;
        public int StageOrder
        {
            get { return stageOrder; }
            set { stageOrder = value; IsDirty = true; }
        }

        private double totalAmount;
        public double TotalAmount
        {
            get { return totalAmount; }
            set { totalAmount = value; IsDirty = true; }
        }

        private double totalUnits;
        public double TotalUnits
        {
            get { return totalUnits; }
            set { totalUnits = value; IsDirty = true; }
        }

        private int code;
        public int Code
        {
            get { return code; }
            set { code = value; IsDirty = true; }
        }
               
    }
}
