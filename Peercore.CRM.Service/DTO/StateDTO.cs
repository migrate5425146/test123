﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class StateDTO
    {
        private int? stateID;
        public int? StateID
        {
            get { return stateID; }
            set { stateID = value; }
        }

        private string stateName;
        public string StateName
        {
            get { return stateName; }
            set { stateName = value; }
        }

        private int count;
        public int Count
        {
            get { return count; }
            set { count = value; }
        }

        public string CountryCode { get; set; }
        public string StateAbbri { get; set; }
        public string StateCode { get; set; }
    }
}
