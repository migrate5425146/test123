﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class TargetDTO : BaseDTO
    {
        public string RepCode { get; set; }
        public double Target { get; set; }
        public double Actual { get; set; }
        public int RouteId { get; set; }
        public int DistributorId { get; set; }
        public string DistributorName { get; set; }
        public int MarketId { get; set; }
        public string MarketName { get; set; }
        public string RouteName { get; set; }
        public double TargetInMils { get; set; }
        public double ActualInMils { get; set; }
        public int BrandId { get; set; }
    }
}
