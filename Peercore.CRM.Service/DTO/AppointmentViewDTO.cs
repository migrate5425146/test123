﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class AppointmentViewDTO : BaseDTO
    {
        private int _AppointmentID;
        public int AppointmentID
        {
            get { return _AppointmentID; }
            set { _AppointmentID = value; IsDirty = true; }
        }

        private string _Subject;
        public string Subject
        {
            get { return _Subject; }
            set { _Subject = value; IsDirty = true; }
        }

        private string _Body;
        public string Body
        {
            get { return _Body; }
            set { _Body = value; IsDirty = true; }
        }

        private DateTime _StartTime;
        public DateTime StartTime
        {
            get { return _StartTime; }
            set { _StartTime = value; IsDirty = true; }
        }

        private DateTime _EndTime;
        public DateTime EndTime
        {
            get { return _EndTime; }
            set { _EndTime = value; IsDirty = true; }
        }

        private int _AllDayEvent;
        public int AllDayEvent
        {
            get { return _AllDayEvent; }
            set { _AllDayEvent = value; IsDirty = true; }
        }

        private string _Location;
        public string Location
        {
            get { return _Location; }
            set { _Location = value; IsDirty = true; }
        }

        private string _URL;
        public string URL
        {
            get { return _URL; }
            set { _URL = value; IsDirty = true; }
        }

        private int _Type;
        public int Type
        {
            get { return _Type; }
            set { _Type = value; IsDirty = true; }
        }

        private string _RecurrencePattern;
        public string RecurrencePattern
        {
            get { return _RecurrencePattern; }
            set { _RecurrencePattern = value; IsDirty = true; }
        }

        private string _TimeZoneString;
        public string TimeZoneString
        {
            get { return _TimeZoneString; }
            set { _TimeZoneString = value; IsDirty = true; }
        }

        private string _Developer;
        public string Developer
        {
            get { return _Developer; }
            set { _Developer = value; IsDirty = true; }
        }

        private string _ProductLine;
        public string ProductLine
        {
            get { return _ProductLine; }
            set { _ProductLine = value; IsDirty = true; }
        }

        private string _ExceptionAppointments;
        public string ExceptionAppointments
        {
            get { return _ExceptionAppointments; }
            set { _ExceptionAppointments = value; IsDirty = true; }
        }

        private string _Importance;
        public string Importance
        {
            get { return _Importance; }
            set { _Importance = value; IsDirty = true; }
        }

        private string _TimeMaker;
        public string TimeMaker
        {
            get { return _TimeMaker; }
            set { _TimeMaker = value; IsDirty = true; }
        }

        private string _Category;
        public string Category
        {
            get { return _Category; }
            set { _Category = value; IsDirty = true; }
        }

        private string _CreatedBy;
        public string CreatedBy
        {
            get { return _CreatedBy; }
            set { _CreatedBy = value; IsDirty = true; }
        }

        private int _Count;
        public int Count
        {
            get { return _Count; }
            set { _Count = value; IsDirty = true; }
        }

        private string _Title;
        public string Title
        {
            get { return _Title; }
            set { _Title = value; IsDirty = true; }
        }

        public string CustomerName { get; set; }

        private string _LeadName;
        public string LeadName
        {
            get { return _LeadName; }
            set { _LeadName = value; IsDirty = true; }
        }

        private string _Comments = "";
        public string Comments
        {
            get { return _Comments; }
            set { _Comments = value; IsDirty = true; }
        }

        private string _ActivitiyStatus = "";
        public string ActivitiyStatus
        {
            get { return _ActivitiyStatus; }
            set { _ActivitiyStatus = value; IsDirty = true; }
        }

        private int _ActivityId = 0;
        public int ActivityId
        {
            get { return _ActivityId; }
            set { _ActivityId = value; IsDirty = true; }
        }

        private int leadId;
        public int LeadId
        {
            get
            {
                return leadId;
            }
            set
            {
                if (leadId == value)
                    return;

                leadId = value;
            }
        }

        private string custCode;
        public string CustCode
        {
            get
            {
                return custCode;
            }
            set
            {
                if (custCode == value)
                    return;

                custCode = value;
            }
        }

        private string leadStage;
        public string LeadStage
        {
            get
            {
                return leadStage;
            }
            set
            {
                if (leadStage == value)
                    return;

                leadStage = value;
            }
        }


        private string categoryDescription;
        public string CategoryDescription
        {
            get
            {
                return categoryDescription;
            }
            set
            {
                if (categoryDescription == value)
                    return;

                categoryDescription = value;
            }
        }
        
    }
}
