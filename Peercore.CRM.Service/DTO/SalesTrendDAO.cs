﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class SalesTrendDAO : BaseDTO
    {
        public List<KeyValuePair<string, double>> TrendData { get; set; }

        public string Channel { get; set; }

        public string ChannelDescription { get; set; }

        public double TotalAmount { get; set; }
    }
}
