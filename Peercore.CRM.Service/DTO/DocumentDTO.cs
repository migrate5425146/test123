﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class DocumentDTO : BaseDTO
    {

        #region - byte[] -
        private byte[] fileContent;
        public byte[] Content
        {
            get { return fileContent; }
            set
            {
                fileContent = value;
                IsDirty = true;
            }
        } 
        #endregion

        #region - DateTime -
        private DateTime attachedDate;
        public DateTime AttachedDate
        {
            get
            {
                return attachedDate;
            }
            set
            {
                if (attachedDate == value)
                    return;

                attachedDate = value;
                IsDirty = true;
            }
        }

        private DateTime? lastModifiedDate;
        public DateTime? LastModifiedDate
        {
            get
            {
                return lastModifiedDate;
            }
            set
            {
                if (lastModifiedDate == value)
                    return;

                lastModifiedDate = value;
                IsDirty = true;
            }
        } 
        #endregion

        #region - int -
        private int documentID;
        public int DocumentID
        {
            get
            {
                return documentID;
            }
            set
            {
                if (documentID == value)
                    return;

                documentID = value;
                IsDirty = true;
            }
        }

        private int leadID;
        public int LeadID
        {
            get
            {
                return leadID;
            }
            set
            {
                if (leadID == value)
                    return;

                leadID = value;
                IsDirty = true;
            }
        }

 
        #endregion

        #region - string -
        private string attachedBy;
        public string AttachedBy
        {
            get
            {
                return attachedBy;
            }
            set
            {
                if (attachedBy == value)
                    return;

                attachedBy = value;
                IsDirty = true;
            }
        }

        private string custCode;
        public string CustCode
        {
            get
            {
                return custCode;
            }
            set
            {
                if (custCode == value)
                    return;

                custCode = value;
                IsDirty = true;
            }
        }

        private string fileName;
        public string DocumentName
        {
            get { return fileName; }
            set
            {
                fileName = value;
                IsDirty = true;
            }
        }

        private string lastModifiedBy;
        public string LastModifiedBy
        {
            get
            {
                return lastModifiedBy;
            }
            set
            {
                if (lastModifiedBy == value)
                    return;

                lastModifiedBy = value;
                IsDirty = true;
            }
        }

        private string path;
        public string Path
        {
            get
            {
                return path;
            }
            set
            {
                if (path == value)
                    return;

                path = value;
                IsDirty = true;
            }
        }

        private string extention;
        public string Extention
        {
            get
            {
                return extention;
            }
            set
            {
                if (extention == value)
                    return;

                extention = value;
                IsDirty = true;
            }
        }

        private string tempFileName;
        public string TempFileName
        {
            get
            {
                return tempFileName;
            }
            set
            {
                if (tempFileName == value)
                    return;

                tempFileName = value;
                IsDirty = true;
            }
        }

        private string fileLocation;
        public string FileLocation
        {
            get { return fileLocation; }
            set { fileLocation = value; }
        }

        private string enduserCode;
        public string EnduserCode
        {
            get { return enduserCode; }
            set
            {
                if (enduserCode == value)
                    return;

                enduserCode = value;
                IsDirty = true;
            }
        }

        public string CustomerType { get; set; }
        #endregion
    }
}
