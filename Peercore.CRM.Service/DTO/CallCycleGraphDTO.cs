﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class CallCycleGraphDTO
    {
        public string ActivityType { get; set; }
        public string ActivityTypeDesc { get; set; }
        public int ActivityTypeCount { get; set; }
        public string CallCycleName { get; set; }


        public int ActivityId { get; set; }
        public string AssignTo { get; set; }
        public int CallCycleId { get; set; }
        public string Comments { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedDate { get; set; }
        public string CustomerCode { get; set; }
        public string DeleteFlag { get; set; }
        public DateTime EndDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedDate { get; set; }
        public int LeadId { get; set; }
        public int LeadStageId { get; set; }
        public int ParentActivityId { get; set; }
        public string Priority { get; set; }
        public DateTime ReminderDate { get; set; }
        public int RepGroupId { get; set; }
        public string SendReminder { get; set; }
        public string SentMail { get; set; }
        public DateTime StartDate { get; set; }
        public string Status { get; set; }
        public string Subject { get; set; }
        public string SourceName { get; set; }
        public string LeadStage { get; set; }
        public string StatusDescription { get; set; }

        public int RowCount { get; set; }
    }
}
