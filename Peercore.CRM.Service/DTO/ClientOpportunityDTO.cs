﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class ClientOpportunityDTO : BaseDTO
    {
        public string OpportunityOwner { get; set; }

        public int Count { get; set; }

        public double Value { get; set; }

        public double Tonnes { get; set; }

        public double Units { get; set; }
    }
}
