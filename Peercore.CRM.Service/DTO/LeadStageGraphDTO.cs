﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class LeadStageGraphDTO : BaseDTO
    {
        private int leadsCount;
        public int LeadsCount
        {
            get { return leadsCount; }
            set { leadsCount = value; IsDirty = true; }
        }

        private int leadStageId;
        public int LeadStageId
        {
            get { return leadStageId; }
            set { leadStageId = value; IsDirty = true; }
        }

        private string colour;
        public string Colour
        {
            get { return colour; }
            set { colour = value; IsDirty = true; }
        }

        private string leadStage;
        public string LeadStage
        {
            get { return leadStage; }
            set { leadStage = value; IsDirty = true; }
        }
    }
}
