﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.CRM.Service.DTO
{
    public class IncentivePlanDetailsDTO:BaseDTO
    {
        public int IncentivePlanDetailsId { get; set; }
        public int IncentivePlanHeaderId { get; set; }
        public string PlanDetails { get; set; }
        public int IpDetailsTypeId { get; set; }
        public bool IsActive { get; set; }

        public List<IncentiveOptionLevelDTO> IncentiveOptionLevelList { get; set; }
        public List<IncentiveSecondarySalesWeeklyDTO> IncentiveSecondarySalesWeeklyList { get; set; }
        public List<IncentiveSecondarySalesPartialDTO> IncentiveSecondarySalesPartialList { get; set; }

        private string createdBy;
        public string CreatedBy
        {
            get { return createdBy; }
            set { createdBy = value; }
        }

        private DateTime? createdDate;
        public DateTime? CreatedDate
        {
            get { return createdDate; }
            set { createdDate = value; }
        }

        private string lastModifiedBy;
        public string LastModifiedBy
        {
            get { return lastModifiedBy; }
            set { lastModifiedBy = value; }
        }

        private DateTime? lastModifiedDate;
        public DateTime? LastModifiedDate
        {
            get { return lastModifiedDate; }
            set { lastModifiedDate = value; }
        }
    }
}
