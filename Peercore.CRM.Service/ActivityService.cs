﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.ServiceContracts;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.Common.Adapter;
using System.ServiceModel;
using Peercore.CRM.Service.DTO.CompositeEntities;
using Peercore.CRM.Model;
using Peercore.CRM.BusinessRules;

namespace Peercore.CRM.Service
{
    
    public partial class CRMService : IActivity
    {
        ActivityAdapter activityAdapter = null;

        #region Activity Types
        public List<ActivityDTO> GetAllActivityTypes(string defaultDeptID, bool IsSchedular = false)
        {
            activityAdapter = new ActivityAdapter();

            try
            {
                return activityAdapter.GetAllActivityTypes(defaultDeptID, IsSchedular);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActivityDTO GetActivityType(string code, ArgsDTO args)
        {
            activityAdapter = new ActivityAdapter();

            try
            {
                return activityAdapter.GetActivityType(code, args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<ActivityDTO> GetAllFixedActivityTypes(bool bShownInPlanner = false)
        {
            activityAdapter = new ActivityAdapter();

            try
            {
                return activityAdapter.GetAllFixedActivityTypes(bShownInPlanner);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ActivityDTO> GetColdCallActyvityTypes()
        {
            activityAdapter = new ActivityAdapter();

            try
            {
                return activityAdapter.GetColdCallActyvityTypes();
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region - Activity -
        public bool SaveActivity(ActivityDTO activityDto, ref int iReminderActivityID)
        {
            activityAdapter = new ActivityAdapter();
            try
            {
                return activityAdapter.Save(activityDto, ref iReminderActivityID);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string InsertConversionQuery(ActivityDTO activity)
        {
            activityAdapter = new ActivityAdapter();
            try
            {
                return activityAdapter.InsertConversionQuery(activity);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<ActivityDTO> GetRelatedActivity(ArgsDTO args)
        {
            activityAdapter = new ActivityAdapter();
            try
            {
                return activityAdapter.GetRelatedActivity(args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public ActivityDTO GetActivity(int iActivityID)
        {
            activityAdapter = new ActivityAdapter();
            try
            {
                return activityAdapter.GetActivity(iActivityID);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<ActivityDTO> GetActivitiesForCustomer(ArgsDTO args)
        {
            activityAdapter = new ActivityAdapter();

            try
            {
                return activityAdapter.GetActivitiesForCustomer(args);
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public List<CustomerActivitiesDTO> GetAllActivities(ArgsDTO args, string repTerritory = "")
        {
            activityAdapter = new ActivityAdapter();
            try
            {
                return activityAdapter.GetAllActivities(args, repTerritory);
            }
            catch
            {
                throw;
            }
        } 
        
        public List<CustomerActivityModel> GetAllActivitiesNew(ArgsModel args, string repTerritory = "")
        {
            try
            {
                return ActivityBR.Instance.GetAllActivitiesNew(args, repTerritory);
            }
            catch
            {
                throw;
            }
        }

        public List<CustomerActivitiesDTO> GetActivityCountByType(ArgsDTO args)
        {
            activityAdapter = new ActivityAdapter();
            try
            {
                return activityAdapter.GetActivityCountByType(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CustomerActivitiesDTO> GetAllActivitiesByType(ArgsDTO args)
        {
            activityAdapter = new ActivityAdapter();

            try
            {
                return activityAdapter.GetAllActivitiesByType(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool ChangeActivityDate(ActivityDTO activity, bool UpdateFromCalendar = false)
        {
            activityAdapter = new ActivityAdapter();

            try
            {
                return activityAdapter.ChangeActivityDate(activity, UpdateFromCalendar);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public CustomerActivitiesDTO GetActivityAppoitmentId(int iActivityId)
        {
            activityAdapter = new ActivityAdapter();

            try
            {
                return activityAdapter.GetActivityAppoitmentId(iActivityId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ActivityDTO GetActivityByAppoitmentId(int iAppointmentId)
        {
            activityAdapter = new ActivityAdapter();
            try
            {
                return activityAdapter.GetActivityByAppoitmentId(iAppointmentId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool ResetAppointment(ActivityDTO activity)
        {
            activityAdapter = new ActivityAdapter();
            try
            {
                return activityAdapter.ResetAppointment(activity);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool DeleteActivity(ActivityDTO activity)
        {
            activityAdapter = new ActivityAdapter();
            try
            {
                return activityAdapter.DeleteActivity(activity);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ActivityDTO> GetActivitiesForEndUser(ArgsDTO args)
        {
            activityAdapter = new ActivityAdapter();
            try
            {
                return activityAdapter.GetActivitiesForEndUser(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CustomerActivitiesDTO> GetOutstandingActivities(ArgsDTO args)
        {
            activityAdapter = new ActivityAdapter();
            try
            {
                return activityAdapter.GetOutstandingActivities(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateActivityFromPlanner(ActivityDTO activity, string OriginatorType)
        {
            activityAdapter = new ActivityAdapter();
            try
            {
                return activityAdapter.UpdateActivityFromPlanner(activity , OriginatorType);
            }
            catch (Exception)
            {
                throw;
            }
        }



        public int GetScheduledActivitiesForHomeCount(ArgsDTO args)
        {
            activityAdapter = new ActivityAdapter();
            try
            {
                return activityAdapter.GetAllHomeActivitiesByCategoryCount(args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int GetOutstandingActivitiesCount(ArgsDTO args)
        {
            activityAdapter = new ActivityAdapter();
            try
            {
                return activityAdapter.GetOutstandingActivitiesCount(args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public AppointmentCountViewDTO GetAllActivitiesByTypeCount(ArgsDTO args)
        {
            activityAdapter = new ActivityAdapter();
            try
            {
                return activityAdapter.GetAllActivitiesByTypeCount(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CustomerActivitiesDTO> GetAllActivitiesForHome(ArgsDTO args)
        {
            activityAdapter = new ActivityAdapter();
            try
            {
                return activityAdapter.GetAllActivitiesForHome(args);
            }
            catch (Exception)
            {
                throw;
            }
        }
        public int GetRemindersForHomeCount(ArgsDTO args)
        {
            activityAdapter = new ActivityAdapter();
            try
            {
                return activityAdapter.GetRemindersForHomeCount(args);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CallCycleContactDTO> GetAllCallCyclesCountById(ArgsDTO args)
        {
            activityAdapter = new ActivityAdapter();

            try
            {
                return activityAdapter.GetAllCallCyclesCountById(args);
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<CallCycleViewDTO> GetAllCallCycles(ArgsDTO args)
        {
            activityAdapter = new ActivityAdapter();

            try
            {
                return activityAdapter.GetAllCallCycles(args);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int UpdateActivityStatusForTME(List<ActivityDTO> activityList)
        {
            activityAdapter = new ActivityAdapter();

            try
            {
                return activityAdapter.UpdateActivityStatusForTME(activityList);
            }
            catch (Exception)
            {
                throw;
            }
        }
        #endregion

        #region Old Methods
        //public List<CustomerActivitiesDTO> GetScheduledActivities(ArgsDTO argsDTO)
        //{
        //    activityAdapter = new ActivityAdapter();
        //    try
        //    {
        //        return activityAdapter.GetScheduledActivities(argsDTO);
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        //public List<CustomerActivitiesDTO> GetAllHomeActivitiesByCategory(ArgsDTO argsDTO, string category)
        //{
        //    activityAdapter = new ActivityAdapter();
        //    try
        //    {
        //        return activityAdapter.GetAllHomeActivitiesByCategory(argsDTO, category);
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        //public int GetAllHomeActivitiesByCategoryCount(ArgsDTO argsDTO, string category)
        //{
        //    activityAdapter = new ActivityAdapter();
        //    try
        //    {
        //        return activityAdapter.GetAllHomeActivitiesByCategoryCount(argsDTO, category);
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        //public ActivityDTO GetLastServiceCall(string custCode)
        //{
        //    activityAdapter = new ActivityAdapter();
        //    try
        //    {
        //        return activityAdapter.GetLastServiceCall(custCode);
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        //public List<CustomerActivitiesDTO> TestMethod(ArgsDTO argsDTO)
        //{

        //    activityAdapter = new ActivityAdapter();
        //    try
        //    {
        //        return activityAdapter.TestMethod(argsDTO);
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        //public List<CustomerActivitiesDTO> GetActivitiesByCategory(ArgsDTO argsDTO)
        //{
        //    activityAdapter = new ActivityAdapter();
        //    try
        //    {
        //        return activityAdapter.GetActivitiesByCategory(argsDTO);
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        //public CustomerActivitiesDTO GetActivityAppointmentId(int activityId)
        //{
        //    activityAdapter = new ActivityAdapter();
        //    try
        //    {
        //        return activityAdapter.GetActivityAppointmentId(activityId);
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}
        #endregion
    }
}
