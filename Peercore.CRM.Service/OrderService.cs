﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.ServiceContracts;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.Common.Adapter;
using Peercore.CRM.Model;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.Entities;

namespace Peercore.CRM.Service
{
    public partial class CRMService : IOrder
    {
        public List<OrderModel> GetAllOrders(ArgsModel args, string Originator, string status)
        {
            List<OrderModel> lst = new List<OrderModel>();
            try
            {
                lst = OrderHeaderBR.Instance.GetAllOrders(args, Originator, status);

                return lst;
            }
            catch (Exception)
            {
                return lst;
            }
        }

        public bool DeleteOrder(string originator, int orderId)
        {
            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginator(originator);

                if (orgEntity != null)
                {
                    return OrderHeaderBR.Instance.DeleteOrderHeaderById(orgEntity, "", orderId);
                }
            }
            catch (Exception)
            {
                return false;
            }

            return false;
        }
        
        public bool ArchiveOrderHeader(string originator, int orderId)
        {
            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginator(originator);

                if (orgEntity != null)
                {
                    return OrderHeaderBR.Instance.ArchiveOrderHeader(orgEntity, "", orderId);
                }
            }
            catch (Exception)
            {
                return false;
            }

            return false;
        }

        public bool DeleteBulkOrders(string userType, string userName, string ordList)
        {
            try
            {
                return OrderHeaderBR.Instance.DeleteBulkOrders(userType, userName, ordList);
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public bool DeleteOrderHeaderByOrderNo(string originator, string orderNo)
        {
            try
            {
                return OrderHeaderBR.Instance.DeleteOrderHeaderByOrderNo(originator, orderNo);
            }
            catch { return false; }
        }
    }
}
