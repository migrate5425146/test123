﻿using Peercore.CRM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "MonthlyDeduction")]
    public interface IMonthlyDeduction
    {
        #region - Monthly Deduction -

        [OperationContract]
        List<MonthlyDeductionModel> GetAllDeductions(ArgsModel args);

        [OperationContract]
        bool SaveDeduction(MonthlyDeductionModel assetDetail);

        [OperationContract]
        bool UpdateDeduction(MonthlyDeductionModel assetDetail);

        [OperationContract]
        bool DeleteDeduction(int deductId, string originator);

        [OperationContract]
        bool excelDeductionData(string fileName, string originator, string logPath);

        [OperationContract]
        bool IsRepCodeAvailable(string repcode);

        #endregion
    }
}
