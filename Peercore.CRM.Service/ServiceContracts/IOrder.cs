﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Model;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "Order")]
    public interface IOrder
    {
        [OperationContract]
        List<OrderModel> GetAllOrders(ArgsModel args, string Originator, string status);

        [OperationContract]
        bool DeleteOrder(string originator, int orderId);
        
        /*
        [OperationContract]
        int SaveLeadStageList(List<LeadStageDTO> leadStages, List<LeadStageDTO> deletedStages, string defaultDeptID);

        [OperationContract]
        bool CanDeleteLeadStage(int leadStageId);

        [OperationContract]
        LeadStageDTO GetLeadStage(int SourceId);
        */
        
        [OperationContract]
        bool DeleteBulkOrders(string userType, string userName, string ordList);
        
        [OperationContract]
        bool ArchiveOrderHeader(string originator, int orderId);
        
        [OperationContract]
        bool DeleteOrderHeaderByOrderNo(string originator, string orderNo);
    }
}
