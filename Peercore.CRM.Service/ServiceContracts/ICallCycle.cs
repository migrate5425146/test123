﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO.CompositeEntities;
using Peercore.CRM.Service.DTO;
using System.ServiceModel;
using System.Collections.ObjectModel;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "CallCycle")]
    public interface ICallCycle
    {
        [OperationContract]
        List<CallCycleContactDTO> GetCallCycleContacts(ArgsDTO args, int iCallCycleID = 0, bool bInActive = false, string CallCycleName = "");

        [OperationContract]
        bool GetCallCycleState(int callCycleId);

        [OperationContract]
        List<CallCycleDTO> GetCallCycle(ArgsDTO args, bool bInActive = false, int iCallCycleID = 0, string CallCycleName = "");

        [OperationContract]
        bool CallCycleSave(CallCycleDTO callCycle, List<CallCycleContactDTO> oCustomers, string originator, TimeSpan tsStartTime,out int callCycleId);

        [OperationContract]
        bool DeactivateCallCycle(int callCycleId, string isDelete);

        [OperationContract]
        bool CallCycleContactDelete(CallCycleContactDTO callCycleContact, int callCycleId);

        [OperationContract]
        bool TestMe(SchedularTransferComponentDTO schedularTransferComponentDTO);

        [OperationContract]
        bool SaveSchedule(List<CallCycleContactDTO> callCycleContactList);

        [OperationContract]
        bool CreateCallCycleActivitiesFromTemplate(CallCycleDTO callCycle, List<CallCycleContactDTO> callCycleContact, ArgsDTO args);

        [OperationContract]
        List<LookupDTO> GetAllCallCyclesList(ArgsDTO args);

        [OperationContract]
        bool SaveRoute(RouteDTO route);

        [OperationContract]
        List<RouteDTO> GetRoutesByRepCode(ArgsDTO args);

        [OperationContract]
        bool SchedulePlanedRoutesForDR(ActivityDTO activity, string originatorType);

        [OperationContract]
        List<RouteDTO> GetAssignedRoutes(string parentOriginator, ArgsDTO args);

        [OperationContract]
        List<RouteMasterDTO> GetRouteNames(string routName);

        [OperationContract]
        List<RouteMasterDTO> GetAllRouteMaster(string parentOriginator, ArgsDTO args);

        [OperationContract]
        bool SaveRouteMaster(RouteMasterDTO routeMasterDTO);

        [OperationContract]
        bool DeleteRoutesByRepCode(RouteDTO route);

        [OperationContract]
        bool UpdateRouteSequence(List<RouteDTO> routeList);

        [OperationContract]
        List<RouteMasterDTO> GetRouteTargetsByDistributorId(ArgsDTO args);

        [OperationContract]
        List<CallCycleDTO> GetCallCyclesForRep(string repcode);

        [OperationContract]
        List<RouteDTO> GetRoutesForTME(ArgsDTO args);
        


        [OperationContract]
        bool IsRouteMasterExists(RouteMasterDTO routeMasterDTO);

        [OperationContract]
        RouteMasterDTO GetRouteTargetFromRouteId(int routeMasterId);

        [OperationContract]
        List<RouteMasterDTO> GetAllTMERoutesMaster(string tmeCode, ArgsDTO args); 

        [OperationContract]
        List<CallCycleContactDTO> GetCallCycleContactsForNewEntry(ArgsDTO args, string repCode);

        [OperationContract]
        bool IsCallCycleContactInAnyOtherSchedule(string customerCode);

        [OperationContract]
        string GetRepCodeForCallCycleId(int callCycleId);

        [OperationContract]
        bool SaveOriginatorRouteOutlets(OriginatorRouteOutletsDTO originatorRouteOutletsDTO);

        [OperationContract]
        ItineraryHeaderDTO GetItineraryDetailsForUser(string tme, DateTime date);

        [OperationContract]
        bool SaveItinerary(ItineraryHeaderDTO itineraryHeaderdto);

        [OperationContract]
        int InsertItineraryLogEntry(ItineraryHeaderDTO itineraryHeaderDTO);

        [OperationContract]
        bool UpdateItineraryHeaderStatus(ItineraryHeaderDTO itineraryHeaderDTO);

        [OperationContract]
        ItineraryHeaderDTO GetItineraryLogEntryById(int itineraryLogEntryId);

        [OperationContract]
        OriginatorDTO GetItineraryAssignedToByHeaderId(int itineraryHeaderId);

        [OperationContract]
        List<VisitChecklistMasterDTO> GetAllVisitChecklistMaster(ArgsDTO args);

        [OperationContract]
        bool SaveAllVisitChecklistMaster(VisitChecklistMasterDTO visitChecklistDTO);

        [OperationContract]
        bool DeleteVisitChecklistMaster(VisitChecklistMasterDTO visitChecklistDTO);

        [OperationContract]
        bool DeleteRouteMaster(RouteMasterDTO routeMasterDTO);

        [OperationContract]
        ItineraryHeaderDTO GetItineraryHeaderByUser(string tme, DateTime date);

        [OperationContract]
        ItineraryHeaderDTO GetItineraryHeaderByHeaderId(int itineraryHeaderId);
        
        [OperationContract]
        List<RouteMasterDTO> GetAllRouteMasterForCheckList(int CheckListId, string parentOriginator, ArgsDTO args);

        #region "Perfetti 2nd Phase"

        [OperationContract]
        bool UpdateSRTerritory(string Originator, string RepId, string TerritoryId);
        
        [OperationContract]
        bool UpdateDBTerritory(string DistributerId, string TerritoryId, string CreatedBy);

        #endregion
    }
}
