﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Model;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "Distributor")]
    public interface IDistributor
    {
        [OperationContract]
        List<BankAccountDTO> GetBankAccountsForUser(string sourceTypeText, string originator);

        [OperationContract]
        DistributorDTO GetDistributorTargetById(ArgsDTO args);

        [OperationContract]
        bool SaveBankAccounts(BankAccountDTO account);

        [OperationContract]
        string GetDistributorHoliday(int originatorId, string originator, string repCode);

        [OperationContract]
        DistributorDTO GetDistributorDetails(string originator);

        #region "Perfetti 2nd Phase Development"

        [OperationContract]
        DistributerModel GetDistributorDetailsByOriginator(string Originator);

        [OperationContract]
        bool SaveDistributor(DistributerModel distributor);

        [OperationContract]
        bool SaveDistributorTerritory(string territory_id, string distributor_id, string UpdateBy);


        #endregion

        [OperationContract]
        List<DBClaimInvoiceHeaderModel> GetDBClaimInvoicesByDistributerId(string distributerId);

        [OperationContract]
        List<TradeDiscountUploadModel> SaveTradeDiscount(List<TradeDiscountUploadModel> tradeDiscountList);

        [OperationContract]
        List<TradeDiscountUploadModel> GetAllTradeDiscounts(ArgsModel args, string dbCode);

        [OperationContract]
        bool DeleteTradeDiscount(int trdId, string originator);

        [OperationContract]
        bool UpdateTradeDiscount(TradeDiscountUploadModel tradeDiscount); 
        
        [OperationContract]
        bool UpdateTradeDiscountApprovedProcess(string dtpMonth);
    }
}
