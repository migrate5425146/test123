﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.DTO.CompositeEntities;
using Peercore.CRM.Model;

namespace Peercore.CRM.Service.ServiceContracts
{

    [ServiceContract(Name = "Stock")]
    public interface IStock
    {
        [OperationContract]
        ProductStockModel GetAllProductStockByTerritoryId(ArgsModel args, DateTime allocDate, int territoryId);

        [OperationContract]
        bool SaveLoadStock(ProductStockModel loadStockModel);
        
        [OperationContract]
        bool UploadStockToBulkTerritory(List<ProductModel> lstProduct, ProductStockModel loadStock);

        [OperationContract]
        List<DBClaimInvoiceDetailsModel> GetAllDBClaimInvoiceDetailsByInvoiceId(ArgsModel args, string invoiceId);
    }
}
