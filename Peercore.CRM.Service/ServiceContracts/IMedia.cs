﻿using Peercore.CRM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "Media")]
    public interface IMedia
    {
        #region - Media Upload -

        [OperationContract]
        List<UploadMediaModel> GetAllMedia(string repCode, string category, string custCode, ArgsModel args);

        [OperationContract]
        UploadMediaModel GetMedia(string mediaFileId);

        [OperationContract]
        bool SaveUpdateMedia(UploadMediaModel mediaDetail);

        [OperationContract]
        bool DeleteMediaFiles(int mediaFileId, string modifiedBy);

        [OperationContract]
        List<MediaCategoryModel> GetAllCategoriesForddl(ArgsModel args);

        [OperationContract]
        string LoadMTCustomerName(string custCode);


        #endregion

        #region - Media Category -

        [OperationContract]
        List<MediaCategoryModel> GetAllCategories(ArgsModel args);

        [OperationContract]
        bool SaveMediaCategory(MediaCategoryModel categoryDetail);

        [OperationContract]
        bool UpdateMediaCategory(int catId, string catName, string description, string originator);

        [OperationContract]
        bool DeleteMediaCategory(int catId, string originator);

        [OperationContract]
        bool IsMediaCategoryAvailable(string catName);

        #endregion

    }
}
