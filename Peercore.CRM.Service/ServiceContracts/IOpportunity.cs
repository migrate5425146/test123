﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Peercore.CRM.Service.DTO;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "Opportunity")]
    public interface IOpportunity
    {
        
        [OperationContract]
        bool InsertOpportunity(OpportunityDTO opportunity, out int opportunityID);
        
        [OperationContract]
        bool UpdateOpportunity(OpportunityDTO opportunity);

        [OperationContract]
        List<OpportunityDTO> GetOpportunities(ArgsDTO args, int contactID);

        [OperationContract]
        List<OpportunityDTO> GetOpportunitiesForCustomer(string customerCode, ArgsDTO args);

        [OperationContract]
        bool DeleteOpportunity(int opportunityId);

        [OperationContract]
        int GetOpportunityCountForHome(int pipelineStageID, ArgsDTO args);

        [OperationContract]
        List<CustomerOpportunitiesDTO> GetAllOpportunities(int pipelineStageID, ArgsDTO args);

        [OperationContract]
        List<ClientOpportunityDTO> GetAllClientOpportunity(int pipelineStageID, ArgsDTO args);

        [OperationContract]
        List<CatalogDTO> GetOppProducts(int opportunityId, ArgsDTO args);

        [OperationContract]
        CustomerOpportunitiesDTO GetOpportunity(int opportunityId);

        [OperationContract]
        bool SaveOpportunity(OpportunityDTO opportunity, List<CatalogDTO> lstProducts, out int opportunityId);

        [OperationContract]
        List<CustomerOpportunitiesDTO> GetOpportunitiesForEndUser(ArgsDTO args);

        /*
        [OperationContract]
        List<OpportunityDTO> GetOpportunities(ArgsDTO args, int iContactID);

        [OperationContract]
        List<OpportunityDTO> GetAllOpportunities(int iPipelineStageID, string sOriginator, ArgsDTO args, DateTime? dtmCloseDate = null);

        

        [OperationContract]
        CustomerOpportunitiesDTO GetOpportunity(int opportunityId);

        [OperationContract]
        List<CatalogDTO> GetProducts(int iOpportunityID);

        [OperationContract]
        List<CatalogDTO> GetAllProducts(string sCatlogCode);        

        [OperationContract]
        List<CustomerOpportunitiesDTO> GetOpportunitiesForCustomer(string customerCode, ArgsDTO args);

        [OperationContract]
        bool DeleteOpportunity(int opportunityId);

        

        */



        //[OperationContract]
        //List<CustomerOpportunitiesDTO> GetOpportunitiesForCustomerCount(string orginator, string defDepId);

        //[OperationContract]
        //List<CustomerOpportunitiesDTO> GetAllOpportunitiesCount(string sOriginator, string defaultDepartmentID);

        /*
        [OperationContract]
        int GetOpportunityForWindow(ArgsDTO args, DateTime? dtmCloseDate = null);

        [OperationContract]
        double GetOpportunitySum(ArgsDTO args);

        [OperationContract]
        List<CatalogDTO> GetOppProducts(int oppId);

        

        [OperationContract]
        List<OpportunityDTO> GetOpportunitiesForPipeGraph(ArgsDTO args, int pipeLineId, bool pipeFlag);
        */
    }
}
