﻿using Peercore.CRM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "KPIReport")]
    public interface IKPIReport
    {

        #region - KPIReports -

        [OperationContract]
        List<KPIReportEmailRecipientsModel> GetAllEmailRecipients(ArgsModel args);

        [OperationContract]
        List<KPIReportModel> GetAllKPIs(ArgsModel args, int month, int year);

        [OperationContract]
        bool UploadKPIReport(string fileName, string originator, string key, DateTime targetDate);

        [OperationContract]
        bool UploadKPIReportTargetWeekly(string fileName, string originator, string key, DateTime targetDate);

        #endregion

        #region - KPIReportsBrands -

        [OperationContract]
        List<KPIReportSettingsModel> GetKPIReportBrands(ArgsModel args);

        [OperationContract]
        bool UpdateKPIReportBrands(KPIReportSettingsModel brandsDetail);

        [OperationContract]
        List<KPIReportSettingsModel> KPIReportBrands();

        [OperationContract]
        List<KPIReportModel> GetAllKPIReportWeeklyTargetsByMonth(ArgsModel args, int month, int year);

        #endregion

        [OperationContract]
        bool UpdateKPIEmailRecipients(string emailRecipints);

        [OperationContract]
        bool DeleteKPIEmailRecipient(string emailRecipint);
        
        [OperationContract]
        List<KPIReportBrandsModel> GetAllKPIReportsBrands();
        
        [OperationContract]
        List<KPIReportBrandItemModel> GetKPIBrandItemsByBrandId(ArgsModel args, int kpiBrandId);
        
        [OperationContract]
        bool AssignKPIBrandItems(string originator, string kpi_brand_id, string product_id, string is_checked);
        
        [OperationContract]
        bool UpdateKPIBrandDisplayName(string kpi_brand_id, string displayName);
        
        [OperationContract]
        KPIReportBrandsModel GetKPIReportBrandByBrandId(string kpi_brand_id);
    }
}
