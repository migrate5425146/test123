﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.DTO.CompositeEntities;
using System.Collections.ObjectModel;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "Lead")]
    public interface ILead
    {
        #region Lead
        [OperationContract]
        bool SaveLead(LeadDTO lead, ArgsDTO args, out int leadid);

        [OperationContract]
        LeadDTO GetLead(string sLeadID);

        [OperationContract]
        string UpdatePipelineStageQuery(int iLeadID, int iPipelineStageID);
        
        [OperationContract]
        int GetTotalCustomerCount(ArgsDTO args);

        [OperationContract]
        bool DeleteLead(LeadDTO lead, ActivityDTO activity);

        [OperationContract]
        bool ConvertLeadToCustomer(List<string> sqlList);

        [OperationContract]
        bool ActivateLead(int leadId, ActivityDTO activity);

        [OperationContract]
        List<LeadDTO> GetLeadMiniViewCollection(ArgsDTO args);

        #endregion


        #region - ImportMail -
        [OperationContract]
        List<EmailDTO> GetLeadEmails(ArgsDTO args,string directoryPath, string sessionId);
        #endregion

        /*
        [OperationContract]
        List<ContactPersonDTO> GetContactPersons(string parameter, ArgsDTO args);

        

        

        

        [OperationContract]
        DocumentDTO GetDocumentForView(int documentID, string path, string sessionID, bool issavedocument = false);
        */

        #region - Document -
        [OperationContract]
        bool SaveDocument(DocumentDTO documentDto);

        [OperationContract]
        List<DocumentDTO> GetLeadDocuments(ArgsDTO args);

        [OperationContract]
        DocumentDTO GetDocument(int documentID);

        [OperationContract]
        bool DeleteLeadDocument(int documentId);

        [OperationContract]
        List<DocumentDTO> GetLeadDocumentsForView(ArgsDTO args, string path, string sessionID, bool issavedocument = false);

        #endregion

        

        #region LeadAddress

        [OperationContract]
        List<LeadAddressDTO> GetLeadAddreses(int leadId);

        [OperationContract]
        LeadAddressDTO GetLeadTableAddress(int leadId);

        [OperationContract]
        bool SaveLeadAddreses(LeadAddressDTO leadAddressDto, ref int leadAddressId);

        //[OperationContract]
        //bool SaveLeadAndLeadAddress(LeadAddressDTO leadAddressDto, LeadDTO leadDto);

        [OperationContract]
        bool TransferAddressToCustomer(List<LeadAddressDTO> customerAddressList);

        //[OperationContract]
        //bool DeleteLeadAddress(int addressId);

        #endregion

        #region - ContactPerson -
        [OperationContract]
        bool SaveCustomerContact(ContactPersonDTO oContactPersonDTO, out int contactPersonId);

        [OperationContract]
        List<ContactPersonDTO> GetContactsByOrigin(ArgsDTO args, bool includeChildReps = false);
        #endregion

        //[OperationContract]
        //bool ToggleLeadsKeyContacts(int leadID, int contactPersonID);

        #region - Contact Person Image -
        [OperationContract]
        ContactPersonImageDTO GetLeadContactPersonImage(int contactPersonId, int leadId, string directoryPath, string sessionId);

        #endregion

        //[OperationContract]
        //List<LeadDTO> GetLeadCustomerMiniViewCollection(ArgsDTO args);

        #region CheckList Methods
        //[OperationContract]
        //List<CheckListDTO> GetChecklist(int iLeadStageID);

        //[OperationContract]
        //bool Save(List<CheckListDTO> checklist);

        //[OperationContract]
        //bool CanDeletChecklist(int checklistId);

        //[OperationContract]
        //List<CheckListDTO> GetNonManditoryChecklist();

        //[OperationContract]
        //int SaveChecklist(CheckListDTO leadChecklist);

        //[OperationContract]
        //string SaveQuery(CheckListDTO leadChecklist);

        /*
        [OperationContract]
        string DeleteQuery(int iLeadID, int iPipelineStageID);

        [OperationContract]
        List<LeadCustomerDTO> GetLeadCountByCheckListItem(string sOriginator, string sFromDate, string sEndDate,
            int leadStageId, int checkListId);

        [OperationContract]
        List<LeadCustomerDTO> GetLeadCollectionByCheckList(string sOriginator, string sFromDate, string sEndDate,
            int leadStageId, int checkListId, bool selected);

        [OperationContract]
        string UpdatePipelineStageQuery(int iLeadID, int StageId);

        [OperationContract]
        string GetLeadStartDate(int LeadID, DateTime date);

        [OperationContract]
        string ChangeCustomerOwnershipQuery(int LeadID, string IsOwnershipChange, string customerCode);

        [OperationContract]
        string InsertConversionQuery(ActivityDTO activity);

        [OperationContract]
        List<CheckListDTO> GetChecklist(int iLeadStageID);

        [OperationContract]
        int SaveLeadChecklist(List<CheckListDTO> checklist, List<CheckListDTO> deletedList);

        [OperationContract]
        bool CanDeleteChecklist(int checklistId);

        [OperationContract]
        List<CheckListDTO> GetNonManditoryChecklist();

        
        */
        #endregion
    }
}
