﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.DTO.CompositeEntities;

namespace Peercore.CRM.Service.ServiceContracts
{

    [ServiceContract(Name = "Sales")]
    public interface ISales
    {
        [OperationContract]
        List<EndUserSalesDTO> GetCostYears();

        [OperationContract]
        EndUserSalesDTO GetCurrentCostPeriod();

        [OperationContract]
        List<EndUserSalesDTO> GetCostPeriod(ArgsDTO args);

        [OperationContract]
        EndUserSalesDTO GetCurrentCostPeriodForDateTime(string dtDate);

        [OperationContract]
        List<EndUserSalesDTO> GetDebtorsDetails(string custCode);

        [OperationContract]
        string GetMonthForCostPeriod(int costYear, int costPeriod);

        [OperationContract]
        EndUserSalesDTO GetSalesData(string custCode, string date);

        [OperationContract]
        List<EOISalesDTO> GetEOISales(ArgsDTO args);

        [OperationContract]
        SalesInfoDetailViewStateDTO GetSalesInfoDetailViewState(SalesInfoDetailSearchCriteriaDTO busSalesEnqSrc, ArgsDTO args, string type = "grid");

        [OperationContract]
        List<AtblDTO> GetTrendData(SalesInfoDetailSearchCriteriaDTO busSalesEnqSrc, ref List<string> lstCostPeriods, string sCode);

        [OperationContract]
        SalesInfoDetailViewStateDTO GetEndUserSalesInfoDetails(SalesInfoDetailSearchCriteriaDTO searchCriteria, ArgsDTO args, string type = "grid");

        [OperationContract]
        List<string> LoadCostPeriods(int iCostYear);

        [OperationContract]
        List<BusAllDTO> GetBusAllDetails(SalesInfoDetailSearchCriteriaDTO busSalesEnqSrc, string historyType);

        [OperationContract]
        List<BusAllYearDTO> GetBusAllYearDetails(SalesInfoDetailSearchCriteriaDTO busSalesEnqSrc, string historyType);

        [OperationContract]
        List<PaymentSettlementDTO> GetCheques(string createdBy, DateTime createdDate, DateTime toDate, int distributerAccountId, string status, ArgsDTO args);

        [OperationContract]
        bool SavePaymentSettlements(List<PaymentSettlementDTO> paymentSettlementsList);

        [OperationContract]
        List<PaymentSettlementDTO> GetDatedCheques(string createdBy, DateTime createdDate, DateTime toDate, ArgsDTO args);

        //[OperationContract]
        //EndUserSalesDTO GetCostPeriod(string dtDate);

        [OperationContract]
        int UploadSalesTargets(string FilePath, string Extension, string UserName); 

    }
}
