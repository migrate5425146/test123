﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.DTO.CompositeEntities;
using Peercore.CRM.Model;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "Activity")]
    public interface IActivity
    {
        #region Activity Types
        [OperationContract]
        List<ActivityDTO> GetAllActivityTypes(string defaultDeptID, bool IsSchedular = false);

        [OperationContract]
        ActivityDTO GetActivityType(string code, ArgsDTO args);

        [OperationContract]
        List<ActivityDTO> GetAllFixedActivityTypes(bool bShownInPlanner = false);

        [OperationContract]
        List<ActivityDTO> GetColdCallActyvityTypes();
        #endregion

        #region - Activity -
        [OperationContract]
        bool SaveActivity(ActivityDTO activityDto, ref int iReminderActivityID);

        [OperationContract]
        string InsertConversionQuery(ActivityDTO activity);

        [OperationContract]
        List<ActivityDTO> GetRelatedActivity(ArgsDTO args);

        [OperationContract]
        ActivityDTO GetActivity(int iActivityID);

        [OperationContract]
        List<ActivityDTO> GetActivitiesForCustomer(ArgsDTO args);

        [OperationContract]
        List<CustomerActivitiesDTO> GetAllActivities(ArgsDTO args, string repTerritory);
        
        [OperationContract]
        List<CustomerActivityModel> GetAllActivitiesNew(ArgsModel args, string repTerritory = "");

        [OperationContract]
        List<CustomerActivitiesDTO> GetActivityCountByType(ArgsDTO args);

        [OperationContract]
        List<CustomerActivitiesDTO> GetAllActivitiesByType(ArgsDTO args);

        [OperationContract]
        CustomerActivitiesDTO GetActivityAppoitmentId(int iActivityId);

        [OperationContract]
        ActivityDTO GetActivityByAppoitmentId(int iAppointmentId);

        [OperationContract]
        bool ResetAppointment(ActivityDTO activity);

        [OperationContract]
        bool DeleteActivity(ActivityDTO activity);

        [OperationContract]
        List<ActivityDTO> GetActivitiesForEndUser(ArgsDTO args);

        [OperationContract]
        List<CustomerActivitiesDTO> GetOutstandingActivities(ArgsDTO args);

        [OperationContract]
        bool UpdateActivityFromPlanner(ActivityDTO activity, string OriginatorType);

        [OperationContract]
        int GetScheduledActivitiesForHomeCount(ArgsDTO args);

        [OperationContract]
        int GetOutstandingActivitiesCount(ArgsDTO args);

        [OperationContract]
        AppointmentCountViewDTO GetAllActivitiesByTypeCount(ArgsDTO args);

        [OperationContract]
        List<CustomerActivitiesDTO> GetAllActivitiesForHome(ArgsDTO args);

        [OperationContract]
        int GetRemindersForHomeCount(ArgsDTO args);

        [OperationContract]
        bool ChangeActivityDate(ActivityDTO activity, bool UpdateFromCalendar = false);

        [OperationContract]
        List<CallCycleContactDTO> GetAllCallCyclesCountById(ArgsDTO args);

        [OperationContract]
        List<CallCycleViewDTO> GetAllCallCycles(ArgsDTO args);

        [OperationContract]
        int UpdateActivityStatusForTME(List<ActivityDTO> activityList);
        #endregion

        #region Old Methods
        //

        //[OperationContract]
        //List<CustomerActivitiesDTO> GetScheduledActivities(ArgsDTO argsDTO);

        //[OperationContract]
        //List<CustomerActivitiesDTO> GetAllHomeActivitiesByCategory(ArgsDTO argsDTO, string category);

        //[OperationContract]
        //ActivityDTO GetLastServiceCall(string custCode);

        //[OperationContract]
        //List<CustomerActivitiesDTO> TestMethod(ArgsDTO argsDTO);

        //[OperationContract]
        //List<CustomerActivitiesDTO> GetActivitiesByCategory(ArgsDTO argsDTO);

        //[OperationContract]
        //CustomerActivitiesDTO GetActivityAppointmentId(int activityId);

        //[OperationContract]
        //int GetAllHomeActivitiesByCategoryCount(ArgsDTO argsDTO, string category);
        #endregion
    }
}
