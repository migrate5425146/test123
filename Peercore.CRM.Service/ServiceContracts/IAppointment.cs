﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ServiceModel;
using Peercore.CRM.Service.DTO;

namespace Peercore.CRM.Service.ServiceContracts
{
    [ServiceContract(Name = "Appointment")]
    public interface IAppointment
    {
        [OperationContract]
        List<AppointmentViewDTO> GetOncomingAppointments(ArgsDTO args);

        [OperationContract]
        List<KeyValuePair<string, int>> GetAppointmentsCountForHome(ArgsDTO args);

        [OperationContract]
        bool AppointmentUpdate(AppointmentDTO appointment, bool isUpdateDTRangeOnly = false, bool isUpdateDelFlagOnly = false);

        [OperationContract]
        bool InsertAppointmentList(List<CustomerActivitiesDTO> Listactivity, string originator);

        [OperationContract]
        bool AppointmentInsert(AppointmentDTO appointment, out int appointmentID, bool isUpdateActivity = false, int activityId = 0);

        [OperationContract]
        List<AppointmentViewDTO> GetTodaysAppointments(ArgsDTO ArgsDTO);

        [OperationContract]
        List<AppointmentViewDTO> GetPendingAppointments(ArgsDTO ArgsDTO);

        [OperationContract]
        List<AppointmentViewDTO> GetAllAppointmentsByType(ArgsDTO ArgsDTO);

        [OperationContract]
        List<AppointmentCountViewDTO> GetAllAppointmentsByTypeCount(ArgsDTO args);
        /*
        #region New Methods
        [OperationContract]
        List<AppointmentDTO> GetAppointments(ArgsDTO ArgsDTO);

        [OperationContract]
        bool Insert(AppointmentDTO appointment, out int appointmentID, bool isUpdateActivity = false, int activityId = 0);

        [OperationContract]
        bool Update(AppointmentDTO appointment, bool isUpdateDTRangeOnly = false, bool isUpdateDelFlagOnly = false);

        [OperationContract]
        bool DeleteResources(AppointmentDTO appointment);

        [OperationContract]
        List<AppointmentViewDTO> GetOncomingAppointments(ArgsDTO args);

        [OperationContract]
        List<AppointmentViewDTO> GetTodaysAppointments(ArgsDTO ArgsDTO);

        [OperationContract]
        List<AppointmentViewDTO> GetPendingAppointments(ArgsDTO ArgsDTO);

        
        #endregion 
         * */
    }
}
