﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.Common.Adapter;
using Peercore.CRM.Service.ServiceContracts;


namespace Peercore.CRM.Service
{
    public partial class CRMService : ILead
    {
        LeadAddressAdapter leadAddressAdapter = null;

        public List<LeadAddressDTO> GetLeadAddreses(int leadId)
        {
            leadAddressAdapter = new LeadAddressAdapter();

            try
            {
                return leadAddressAdapter.GetLeadAddresses(leadId);
            }
            catch (Exception)
            {
                throw;
            }
            
        }

        public LeadAddressDTO GetLeadTableAddress(int leadId)
        {
            leadAddressAdapter = new LeadAddressAdapter();
            try
            {
                return leadAddressAdapter.GetLeadTableAddress(leadId);
            }
            catch
            {
                throw;
            }
        }

        public bool SaveLeadAddreses(LeadAddressDTO leadAddressDto, ref int leadAddressId)
        {
            leadAddressAdapter = new LeadAddressAdapter();

            try
            {
                return leadAddressAdapter.SaveLeadAddress(leadAddressDto, ref leadAddressId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        //public bool SaveLeadAndLeadAddress(LeadAddressDTO leadAddressDto, LeadDTO leadDto)
        //{
        //    leadAddressAdapter = new LeadAddressAdapter();

        //    try
        //    {
        //        return leadAddressAdapter.SaveLeadAndLeadAddress(leadAddressDto, leadDto);
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        public bool TransferAddressToCustomer(List<LeadAddressDTO> customerAddressList)
        {
            leadAddressAdapter = new LeadAddressAdapter();
            try
            {
                return leadAddressAdapter.TransferAddressToCustomer(customerAddressList);
            }
            catch
            {
                throw;
            }
        }

        //public bool DeleteLeadAddress(int leadAddressId)
        //{
        //    leadAddressAdapter = new LeadAddressAdapter();

        //    try
        //    {
        //        return leadAddressAdapter.DeleteLeadAddress(leadAddressId);
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

    }
}
