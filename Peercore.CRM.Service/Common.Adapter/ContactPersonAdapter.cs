﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Entities;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.BusinessRules;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class ContactPersonAdapter
    {
        #region - Converters -

        public static ContactPersonEntity ConvertToContactPerson(ContactPersonDTO ContactPersonDto)
        {
            ContactPersonEntity oContactPerson = ContactPersonEntity.CreateObject();

            try
            {
                oContactPerson.CompanyAddress = ContactPersonDto.CompanyAddress;
                oContactPerson.CompanyName = ContactPersonDto.CompanyName;
                oContactPerson.CompanyTelephone = ContactPersonDto.CompanyTelephone;
                oContactPerson.Contact = ContactPersonDto.Contact;
                oContactPerson.ContactPersonID = ContactPersonDto.ContactPersonID;
                oContactPerson.CreatedBy = ContactPersonDto.CreatedBy;
                oContactPerson.CreatedDate = ContactPersonDto.CreatedDate;
                oContactPerson.CustCode = ContactPersonDto.CustCode;
                oContactPerson.Description = ContactPersonDto.Description;
                oContactPerson.DisLikes = ContactPersonDto.DisLikes;
                oContactPerson.EmailAddress = ContactPersonDto.EmailAddress;
                oContactPerson.Fax = ContactPersonDto.Fax;
                oContactPerson.FirstName = ContactPersonDto.FirstName;
                oContactPerson.ImagePath = ContactPersonDto.ImagePath;
                //oContactPerson.IsOwner = ContactPersonDto.IsOwner;
                oContactPerson.KeyContact = ContactPersonDto.KeyContact;
                //oContactPerson.KeyContactChecked = ContactPersonDto.KeyContactChecked;
                oContactPerson.LastModifiedBy = ContactPersonDto.LastModifiedBy;
                oContactPerson.LastModifiedDate = ContactPersonDto.LastModifiedDate;
                oContactPerson.LastName = ContactPersonDto.LastName;
                oContactPerson.LeadID = ContactPersonDto.LeadID;
                oContactPerson.LeadName = ContactPersonDto.LeadName;
                oContactPerson.Likes = ContactPersonDto.Likes;
                oContactPerson.MailingAddress = ContactPersonDto.MailingAddress;
                oContactPerson.MailingCity = ContactPersonDto.MailingCity;
                oContactPerson.MailingCountry = ContactPersonDto.MailingCountry;
                oContactPerson.MailingPostcode = ContactPersonDto.MailingPostcode;
                oContactPerson.MailingState = ContactPersonDto.MailingState;
                oContactPerson.Mobile = ContactPersonDto.Mobile;
                oContactPerson.Note = ContactPersonDto.Note;
                oContactPerson.Origin = ContactPersonDto.Origin;
                oContactPerson.Originator = ContactPersonDto.Originator;
                oContactPerson.OtherAddress = ContactPersonDto.OtherAddress;
                oContactPerson.OtherCity = ContactPersonDto.OtherCity;
                oContactPerson.OtherCountry = ContactPersonDto.OtherCountry;
                oContactPerson.OtherPostCode = ContactPersonDto.OtherPostCode;
                oContactPerson.OtherState = ContactPersonDto.OtherState;
                oContactPerson.Position = ContactPersonDto.Position;
                oContactPerson.ReportsTo = ContactPersonDto.ReportsTo;
                oContactPerson.SpecialInterests = ContactPersonDto.SpecialInterests;
                oContactPerson.Status = ContactPersonDto.Status;
                oContactPerson.Telephone = ContactPersonDto.Telephone;
                oContactPerson.Title = ContactPersonDto.Title;
                oContactPerson.RowCount = ContactPersonDto.RowCount;
                oContactPerson.ContactType = ContactPersonDto.ContactType;
            }
            catch (Exception)
            {

                throw;
            }
            return oContactPerson;
        }

        public static ContactPersonDTO ConvertToContactPersonDTO(ContactPersonEntity contactPerson)
        {

            ContactPersonDTO oContactPersonDto = new ContactPersonDTO();

            try
            {
                oContactPersonDto.CompanyAddress = contactPerson.CompanyAddress;
                oContactPersonDto.CompanyName = contactPerson.CompanyName;
                oContactPersonDto.CompanyTelephone = contactPerson.CompanyTelephone;
                oContactPersonDto.Contact = contactPerson.Contact;
                oContactPersonDto.ContactPersonID = contactPerson.ContactPersonID;
                oContactPersonDto.CreatedBy = contactPerson.CreatedBy;
                oContactPersonDto.CreatedDate = contactPerson.CreatedDate;
                oContactPersonDto.CustCode = contactPerson.CustCode;
                oContactPersonDto.Description = contactPerson.Description;
                oContactPersonDto.DisLikes = contactPerson.DisLikes;
                oContactPersonDto.EmailAddress = contactPerson.EmailAddress;
                oContactPersonDto.Fax = contactPerson.Fax;
                oContactPersonDto.FirstName = contactPerson.FirstName;
                oContactPersonDto.ImagePath = contactPerson.ImagePath;
                //oContactPersonDto.IsOwner = contactPerson.IsOwner;
                oContactPersonDto.KeyContact = contactPerson.KeyContact;
                oContactPersonDto.KeyContactChecked = contactPerson.KeyContactChecked;
                oContactPersonDto.LastModifiedBy = contactPerson.LastModifiedBy;
                oContactPersonDto.LastModifiedDate = contactPerson.LastModifiedDate;
                oContactPersonDto.LastName = contactPerson.LastName;
                oContactPersonDto.LeadID = contactPerson.LeadID;
                oContactPersonDto.LeadName = contactPerson.LeadName;
                oContactPersonDto.Likes = contactPerson.Likes;
                oContactPersonDto.MailingAddress = contactPerson.MailingAddress;
                oContactPersonDto.MailingCity = contactPerson.MailingCity;
                oContactPersonDto.MailingCountry = contactPerson.MailingCountry;
                oContactPersonDto.MailingPostcode = contactPerson.MailingPostcode;
                oContactPersonDto.MailingState = contactPerson.MailingState;
                oContactPersonDto.Mobile = contactPerson.Mobile;
                oContactPersonDto.Note = contactPerson.Note;
                oContactPersonDto.Origin = contactPerson.Origin;
                oContactPersonDto.Originator = contactPerson.Originator;
                oContactPersonDto.OtherAddress = contactPerson.OtherAddress;
                oContactPersonDto.OtherCity = contactPerson.OtherCity;
                oContactPersonDto.OtherCountry = contactPerson.OtherCountry;
                oContactPersonDto.OtherPostCode = contactPerson.OtherPostCode;
                oContactPersonDto.OtherState = contactPerson.OtherState;
                oContactPersonDto.Position = contactPerson.Position;
                oContactPersonDto.ReportsTo = contactPerson.ReportsTo;
                oContactPersonDto.SpecialInterests = contactPerson.SpecialInterests;
                oContactPersonDto.Status = contactPerson.Status;
                oContactPersonDto.Telephone = contactPerson.Telephone;
                oContactPersonDto.RowCount = contactPerson.TotalCount;
                oContactPersonDto.Title = contactPerson.Title;
                oContactPersonDto.ContactType = contactPerson.ContactType;
                oContactPersonDto.TypeDescription = contactPerson.TypeDescription;
            }
            catch (Exception)
            {

                throw;
            }
            return oContactPersonDto;
        }

        #endregion

        #region - Properties -

       // private brContactPerson oBrContactPerson = null;

        #endregion

        #region - Constructor -

        public ContactPersonAdapter()
        {
            //if (oBrContactPerson == null)
            //    oBrContactPerson = new brContactPerson();
        }

        #endregion

        #region - Methods -

        public bool SaveContactPerson(ContactPersonDTO oContactPersonDTO, out int contactPersonId)
        {
            try
            {
                return ContactPersonBR.Instance.Save(ConvertToContactPerson(oContactPersonDTO), out contactPersonId);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool SaveCustomerContact(ContactPersonDTO oContactPersonDTO, out int contactPersonId)
        {
            try
            {
                return ContactPersonBR.Instance.SaveCustomerContact(ConvertToContactPerson(oContactPersonDTO), out contactPersonId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ContactPersonDTO GetContactPerson(int contactPersonId)
        {
            try
            {
                return ConvertToContactPersonDTO(ContactPersonBR.Instance.GetContactPerson(contactPersonId));
            }
            catch (Exception)
            {

                throw;
            }
        }
        public ContactPersonDTO GetCustomerContactPerson(int contactPersonId)
        {
            try
            {
                return ConvertToContactPersonDTO(ContactPersonBR.Instance.GetCustomerContactPerson(contactPersonId));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<ContactPersonDTO> GetCustomerContacts(string sCustCode, ArgsDTO args, string clientType, bool includeChildReps = false)
        {
            List<ContactPersonDTO> contactPersonsDtoList = new List<ContactPersonDTO>();

            try
            {

                List<ContactPersonEntity> contactsPersonList = ContactPersonBR.Instance.GetCustomerContacts(ActivityAdapter.ConvertToArgsEntity(args), sCustCode, clientType, includeChildReps);

                foreach (ContactPersonEntity item in contactsPersonList)
                {
                    contactPersonsDtoList.Add(ConvertToContactPersonDTO(item));
                }
            }
            catch (Exception)
            {

                throw;
            }
            return contactPersonsDtoList;
        }

        public ContactPersonDTO GetCustomerContact(string custCode, string contactType, string origin)
        {
            try
            {
                return ConvertToContactPersonDTO(ContactPersonBR.Instance.GetCustomerContact( custCode, contactType, origin));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public bool DeleteContactPerson(int contactPersonId)
        {
            try
            {
                return ContactPersonBR.Instance.DeleteCustomerContact(contactPersonId);

            }
            catch (Exception)
            {

                throw;
            }
        }


        public List<ContactPersonDTO> GetContactsByOrigin(ArgsDTO args, bool includeChildReps = false)
        {
            List<ContactPersonDTO> contactPersonsDtoList = new List<ContactPersonDTO>();
            try
            {
                List<ContactPersonEntity> contactsPersonList = ContactPersonBR.Instance.GetContactsByOrigin(ActivityAdapter.ConvertToArgsEntity(args), includeChildReps);

                foreach (ContactPersonEntity item in contactsPersonList)
                {
                    contactPersonsDtoList.Add(ConvertToContactPersonDTO(item));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return contactPersonsDtoList;
        }

        /*
        public List<ContactPersonDTO> GetContactPersons(string Parameter, ArgsDTO args)
        {
            try
            {
                List<ContactPerson> contactsPersonList = new List<ContactPerson>();
                List<ContactPersonDTO> contactPersonsDtoList = new List<ContactPersonDTO>();
                contactsPersonList = oBrContactPerson.GetContacts(Parameter, args.OrderBy, args.StartIndex, args.RowCount);

                foreach (ContactPerson item in contactsPersonList)
                {
                    contactPersonsDtoList.Add(ConvertToContactPersonDTO(item));
                }

                return contactPersonsDtoList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool ToggleLeadsKeyContacts(int leadID, int contactPersonID)
        {
            try
            {
                brContactPerson oContact = new brContactPerson();
                return oContact.toggleKeyContacts(leadID, contactPersonID);
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        */
        #endregion
    }
}
