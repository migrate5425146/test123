﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.Entities;
using System.Data;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class AppointmentAdapter
    {
        #region - Converters -

        public static AppointmentEntity ConvertToAppointment(AppointmentDTO appointmentDto)
        {
            AppointmentEntity crmAppointment =AppointmentBR.Instance.CreateObject();
            try
            {
                crmAppointment.AppointmentID = appointmentDto.AppointmentID;
                crmAppointment.Body = appointmentDto.Body;
                crmAppointment.StartTime = appointmentDto.StartTime;
                crmAppointment.EndTime = appointmentDto.EndTime;
                crmAppointment.Category = appointmentDto.Category;
                crmAppointment.CreatedBy = appointmentDto.CreatedBy;
                crmAppointment.Subject = appointmentDto.Subject;
                crmAppointment.CreatedBy = appointmentDto.CreatedBy;
            }
            catch (Exception)
            {
                throw;
            }
            return crmAppointment;
        }

        public static AppointmentDTO ConvertToAppointmentDTO(CRMAppointmentEntity crmAppointment)
        {
            AppointmentDTO appointmentDto = new AppointmentDTO();
            try
            {
                appointmentDto.AppointmentID = crmAppointment.AppointmentID;
                appointmentDto.Body = crmAppointment.Body;
                appointmentDto.Category = crmAppointment.Category;
                appointmentDto.CreatedBy = crmAppointment.CreatedBy;
                appointmentDto.EndTime = crmAppointment.EndTime;
                appointmentDto.StartTime = crmAppointment.StartTime;
                appointmentDto.Subject = crmAppointment.Subject;
            }
            catch (Exception)
            {

                throw;
            }
            return appointmentDto;
        }

        public List<AppointmentViewDTO> ConvertAppointmentViewEntityToDTOList(List<AppointmentViewEntity> lstAppointmentView)
        {
            List<AppointmentViewDTO> lstAppointmentViewDTO = new List<AppointmentViewDTO>();
            AppointmentViewDTO appointmentViewDTO = null;
            try
            {
                foreach (AppointmentViewEntity appointmentView in lstAppointmentView)
                {
                    appointmentViewDTO = new AppointmentViewDTO();
                    //appointmentViewDTO.ActivitiyStatus = appointmentView.ActivitiyStatus;
                    appointmentViewDTO.ActivityId = appointmentView.ActivityID;
                    appointmentViewDTO.AllDayEvent = appointmentView.AllDayEvent;
                    appointmentViewDTO.AppointmentID = appointmentView.AppointmentID;
                    appointmentViewDTO.Body = appointmentView.Body;
                    appointmentViewDTO.Category = appointmentView.Category;
                    //appointmentViewDTO.Comments = appointmentView.Comments;
                    appointmentViewDTO.Count = appointmentView.Count;
                    appointmentViewDTO.CreatedBy = appointmentView.CreatedBy;
                    //appointmentViewDTO.CustomerName = appointmentView.CustomerName;
                    appointmentViewDTO.Developer = appointmentView.Developer;
                    appointmentViewDTO.EndTime = appointmentView.EndTime.ToLocalTime();
                    appointmentViewDTO.ExceptionAppointments = appointmentView.ExceptionAppointments;
                    appointmentViewDTO.Importance = appointmentView.Importance;
                    appointmentViewDTO.LeadName = appointmentView.LeadName;
                    appointmentViewDTO.Location = appointmentView.Location;
                    appointmentViewDTO.ProductLine = appointmentView.ProductLine;
                    appointmentViewDTO.RecurrencePattern = appointmentView.RecurrencePattern;
                    appointmentViewDTO.StartTime = appointmentView.StartTime.ToLocalTime();
                    appointmentViewDTO.Subject = appointmentView.Subject;
                    appointmentViewDTO.TimeMaker = appointmentView.TimeMaker;
                    appointmentViewDTO.TimeZoneString = appointmentView.TimeZoneString;
                    appointmentViewDTO.Title = appointmentView.Title;
                    appointmentViewDTO.Type = appointmentView.Type;
                    appointmentViewDTO.URL = appointmentView.URL;
                    appointmentViewDTO.LeadStage = appointmentView.LeadStage;
                    appointmentViewDTO.RowCount = appointmentView.TotalCount;
                    appointmentViewDTO.LeadId = appointmentView.LeadID;
                    appointmentViewDTO.CustCode = appointmentView.CustCode;
                    appointmentViewDTO.CategoryDescription = appointmentView.CategoryDescription;
                    if (appointmentView.CustCode == null)
                    {
                        appointmentViewDTO.CustCode = string.Empty;
                    }
                    else
                    {
                        appointmentViewDTO.CustCode = appointmentView.CustCode;
                    }
                    
                    lstAppointmentViewDTO.Add(appointmentViewDTO);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstAppointmentViewDTO;
        }

        private List<CustomerActivityEntity> ConvertToCustomerActivityList(List<CustomerActivitiesDTO> customerActivitiesDTOList)
        {
            List<CustomerActivityEntity> customerActivitiesList = new List<CustomerActivityEntity>();
            CustomerActivityEntity customerActivities = null;
            try
            {
                foreach (CustomerActivitiesDTO ocustomerActivitiesDTO in customerActivitiesDTOList)
                {
                    customerActivities = CustomerActivityEntity.CreateObject();
                    customerActivities.ActivityID = ocustomerActivitiesDTO.ActivityID;
                    customerActivities.ActivityType = ocustomerActivitiesDTO.ActivityType;
                    customerActivities.AppointmentId = ocustomerActivitiesDTO.AppointmentId;
                    customerActivities.AssignedTo = ocustomerActivitiesDTO.AssignedTo;
                    customerActivities.Comments = ocustomerActivitiesDTO.Comments;
                    customerActivities.CreatedBy = ocustomerActivitiesDTO.CreatedBy;
                    customerActivities.CreatedDate = ocustomerActivitiesDTO.CreatedDate;
                    customerActivities.CustCode = ocustomerActivitiesDTO.CustCode;
                    //customerActivities.CustomerCity = ocustomerActivitiesDTODTO.CustomerCity;
                    //customerActivities.CustomerPostCode = ocustomerActivitiesDTODTO.CustomerPostCode;
                    customerActivities.DelFlag = ocustomerActivitiesDTO.DelFlag;
                    customerActivities.EndDate = ocustomerActivitiesDTO.EndDate;
                    customerActivities.LastModifiedBy = ocustomerActivitiesDTO.LastModifiedBy;
                    customerActivities.LastModifiedDate = ocustomerActivitiesDTO.LastModifiedDate;
                    customerActivities.LeadId = ocustomerActivitiesDTO.LeadId;
                    customerActivities.LeadName = ocustomerActivitiesDTO.LeadName;
                    customerActivities.LeadStage = ocustomerActivitiesDTO.LeadStage;
                    customerActivities.NoOfActivities = ocustomerActivitiesDTO.NoOfActivities;
                    //customerActivities.OriginatorName = ocustomerActivitiesDTODTO.OriginatorName;
                    customerActivities.PipelineStageID = ocustomerActivitiesDTO.PipelineStageID;
                    customerActivities.Priority = ocustomerActivitiesDTO.Priority;
                    customerActivities.ReminderDate = ocustomerActivitiesDTO.ReminderDate;
                    customerActivities.SendReminder = ocustomerActivitiesDTO.SendReminder;
                    customerActivities.SentMail = ocustomerActivitiesDTO.SentMail;
                    customerActivities.StartDate = ocustomerActivitiesDTO.StartDate;
                    customerActivities.Status = ocustomerActivitiesDTO.Status;
                    customerActivities.StatusDesc = ocustomerActivitiesDTO.StatusDesc;
                    customerActivities.Subject = ocustomerActivitiesDTO.Subject;
                    //customerActivities.TypeColour = ocustomerActivitiesDTODTO.TypeColour;
                    customerActivities.TypeDesc = ocustomerActivitiesDTO.TypeDesc;

                    customerActivitiesList.Add(customerActivities);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return customerActivitiesList;
        }

        public List<AppointmentCountViewDTO> ConvertAppointmentCountViewEntityToDTOList(List<AppointmentCountViewEntity> lstAppointmentCountView)
        {
            List<AppointmentCountViewDTO> lstAppointmentCountViewDTO = new List<AppointmentCountViewDTO>();
            AppointmentCountViewDTO appointmentViewDTO = null;
            try
            {
                foreach (AppointmentCountViewEntity appointmentView in lstAppointmentCountView)
                {
                    appointmentViewDTO = new AppointmentCountViewDTO();
                    appointmentViewDTO.Category = appointmentView.Category;
                    appointmentViewDTO.Color = appointmentView.Color;
                    appointmentViewDTO.Description = appointmentView.Description;
                    appointmentViewDTO.Week1 = appointmentView.Week1;
                    appointmentViewDTO.Week2 = appointmentView.Week2;
                    appointmentViewDTO.Week3 = appointmentView.Week3;
                    appointmentViewDTO.Week4 = appointmentView.Week4;
                    appointmentViewDTO.Week5 = appointmentView.Week5;

                    appointmentViewDTO.Month1 = appointmentView.Month1;
                    appointmentViewDTO.Month2 = appointmentView.Month2;
                    appointmentViewDTO.Month3 = appointmentView.Month3;


                    lstAppointmentCountViewDTO.Add(appointmentViewDTO);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstAppointmentCountViewDTO;
        }
        
        //private List<AppointmentResource> ConvertToAppointmentResourceList(List<AppointmentResourceDTO> appointmentResourceDTOList)
        //{
        //    List<AppointmentResource> appointmentResourceList = new List<AppointmentResource>();
        //    AppointmentResource appointmentResource = null;
        //    try
        //    {
        //        foreach (AppointmentResourceDTO oAppointmentResourceDTO in appointmentResourceDTOList)
        //        {
        //            appointmentResource = new AppointmentResource();
        //            appointmentResource.AppointmentID = oAppointmentResourceDTO.AppointmentID;
        //            appointmentResource.ResourceID = oAppointmentResourceDTO.ResourceID;

        //            appointmentResourceList.Add(appointmentResource);
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    return appointmentResourceList;
        //}
        #endregion

        #region - Constructor -

        public AppointmentAdapter()
        {
            //if (oBrAppointment == null)
            //    oBrAppointment = new brAppointment();
        } 

        #endregion

        #region New Methods
        public List<AppointmentViewDTO> GetOncomingAppointments(ArgsDTO args)
        {
            try
            {
                return ConvertAppointmentViewEntityToDTOList(AppointmentBR.Instance.GetOncomingAppointments(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<KeyValuePair<string, int>> GetAppointmentsCountForHome(ArgsDTO args)
        {
            try
            {
                return AppointmentBR.Instance.GetAppointmentsCount(ActivityAdapter.ConvertToArgsEntity(args));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool AppointmentUpdate(AppointmentDTO appointment, bool isUpdateDTRangeOnly = false, bool isUpdateDelFlagOnly = false)
        {
            bool update = false;
            try
            {
                update = AppointmentBR.Instance.Update(ConvertToAppointment(appointment), isUpdateDelFlagOnly, isUpdateDelFlagOnly);
            }
            catch (Exception)
            {
                throw;
            }
            return update;
        }

        public bool InsertAppointmentList(List<CustomerActivitiesDTO> Listactivity, string originator)
        {
            bool update = false;
            try
            {
                update = AppointmentBR.Instance.InsertAppointmentList(ConvertToCustomerActivityList(Listactivity), originator);
            }
            catch (Exception)
            {
                throw;
            }
            return update;
        }

        public bool AppointmentInsert(AppointmentDTO appointment, out int appointmentID, bool isUpdateActivity = false, int activityId = 0)
        {
            bool update = false;
            try
            {
                update = AppointmentBR.Instance.Insert(ConvertToAppointment(appointment),out appointmentID, isUpdateActivity, activityId);
            }
            catch (Exception)
            {
                throw;
            }
            return update;
        }

        public List<AppointmentViewDTO> GetTodaysAppointments(ArgsDTO args)
        {
            try
            {
                return ConvertAppointmentViewEntityToDTOList(AppointmentBR.Instance.GetTodaysAppointments(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<AppointmentViewDTO> GetPendingAppointments(ArgsDTO args)
        {
            try
            {
                return ConvertAppointmentViewEntityToDTOList(AppointmentBR.Instance.GetPendingAppointments(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<AppointmentViewDTO> GetAllAppointmentsByType(ArgsDTO args)
        {
            try
            {
                return ConvertAppointmentViewEntityToDTOList(AppointmentBR.Instance.GetAllAppointmentsByType(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<AppointmentCountViewDTO> GetAllAppointmentsByTypeCount(ArgsDTO args)
        {
            try
            {
                return ConvertAppointmentCountViewEntityToDTOList(AppointmentBR.Instance.GetAllAppointmentsByTypeCount(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {

                throw;
            }
        }
        
        #endregion

        /*
        #region - New Methods -
        public List<AppointmentDTO> GetAppointments(ArgsDTO ArgsDTO)
        {
            DataTable dtAppointment = new DataTable();
            List<AppointmentDTO> appointmentList = new List<AppointmentDTO>();

            try
            {
                AppointmentBR.Instance.GetAppointments(ref dtAppointment,ActivityAdapter.ConvertToArgsEntity(ArgsDTO));

                AppointmentDTO appointment = null;

                foreach (DataRow item in dtAppointment.Rows)
                {
                    appointment = new AppointmentDTO();
                    
                    appointment.ID = Convert.ToInt32(item["appointment_id"]);
                    appointment.Subject = Convert.ToString(item["subject"]);
                    appointment.Description = Convert.ToString(item["body"]);
                    appointment.Start = Convert.ToDateTime(item["start_time"]);
                    appointment.End = Convert.ToDateTime(item["end_time"]);
                    appointment.Category = Convert.ToString(item["category"]);
                    appointment.CreatedBy = Convert.ToString(item["created_by"]);
                    appointment.LeadName = Convert.ToString(item["lead_name"]);
                    appointment.LeadState = Convert.ToString(item["state"]);

                    appointment.TypeDescription = Convert.ToString(item["type_description"]);
                    appointment.RepCode = Convert.ToString(item["rep_code"]);
                    appointment.LeadStage = Convert.ToString(item["lead_stage"]);
                    appointment.ActivityId = Convert.ToInt32(item["activity_id"]);
                    appointment.LeadId = Convert.ToInt32(item["lead_id"]);
                    appointment.CustomerCode = Convert.ToString(item["cust_code"]);
                    appointment.EnduserCode = Convert.ToString(item["enduser_code"]);
        
                    appointmentList.Add(appointment);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dtAppointment != null)
                    dtAppointment.Dispose();
            }

            return appointmentList;
        }

        public bool Insert(AppointmentDTO appointment, out int appointmentID, bool isUpdateActivity = false, int activityId = 0)
        {
            bool save = false;
            try
            {
                save = AppointmentBR.Instance.Insert(ConvertToAppointment(appointment), out appointmentID, isUpdateActivity, activityId);
            }
            catch (Exception)
            {

                throw;
            }
            return save;
        }

        public bool Update(AppointmentDTO appointment, bool isUpdateDTRangeOnly = false, bool isUpdateDelFlagOnly = false)
        {
            bool update = false;
            try
            {
                update = AppointmentBR.Instance.Update(ConvertToAppointment(appointment), isUpdateDelFlagOnly, isUpdateDelFlagOnly);
            }
            catch (Exception)
            {
                throw;
            }
            return update;
        }

        public bool DeleteResources(AppointmentDTO appointment)
        {
            bool delete = false;
            try
            {
                delete = oBrAppointment.DeleteResources(ConvertToAppointment(appointment));
            }
            catch (Exception)
            {
                throw;
            }
            return delete;
        }

        public List<AppointmentViewDTO> GetOncomingAppointments(ArgsDTO args)
        {
            try
            {
                return ConvertAppointmentViewEntityToDTOList(AppointmentBR.Instance.GetOncomingAppointments(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<AppointmentViewDTO> GetTodaysAppointments(ArgsDTO args)
        {
            try
            {
                return ConvertAppointmentViewEntityToDTOList(AppointmentBR.Instance.GetTodaysAppointments(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<AppointmentViewDTO> GetPendingAppointments(ArgsDTO args)
        {
            try
            {
                return ConvertAppointmentViewEntityToDTOList(AppointmentBR.Instance.GetPendingAppointments(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {

                throw;
            }
        }

        
        #endregion
        */
        #region Old Methods
        //public bool InsertAppointments(AppointmentDTO appointmentDto)
        //{
        //    bool status = false;

        //    try
        //    {
        //        if(oBrAppointment.Insert(ConvertToAppointment(appointmentDto)) > 0)
        //            status = true;
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }

        //    return status;
        //}

        //public List<AppointmentViewDTO> GetPendingAppointments(string date, ArgsDTO ArgsDTO)
        //{
        //    brAppointment oAppointment = new brAppointment();

        //    try
        //    {
        //        return AppointmentViewList(oAppointment.GetPendingAppointments(date, ArgsDTO.ChiildOriginators, ArgsDTO.OriginatorId, ArgsDTO.StartIndex, ArgsDTO.RowCount));
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        //public List<AppointmentViewDTO> GetFutureAppointments(string date, ArgsDTO ArgsDTO)
        //{
        //    brAppointment oAppointment = new brAppointment();
        //    try
        //    {
        //        return AppointmentViewList(oAppointment.GetFutureAppointments(date, ArgsDTO.ChiildOriginators, ArgsDTO.OriginatorId, ArgsDTO.StartIndex, ArgsDTO.RowCount));
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        //public List<AppointmentViewDTO> GetTodaysAppointments(string date, ArgsDTO ArgsDTO)
        //{
        //    brAppointment oAppointment = new brAppointment();
        //    try
        //    {
        //        return AppointmentViewList(oAppointment.GetTodaysAppointments(date, ArgsDTO.ChiildOriginators, ArgsDTO.OriginatorId, ArgsDTO.StartIndex, ArgsDTO.RowCount));
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        //public bool InsertAppointment(List<CustomerActivitiesDTO> appointmentlist, ArgsDTO args)
        //{
        //    bool save = false;
        //    try
        //    {
        //        int i = oBrAppointment.Insert(ConvertToCustomerActivityList(appointmentlist), args.Originator);
        //        if (i > 0)
        //            save = true;
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //    return save;
        //}

        //public bool InsertResources(List<AppointmentResourceDTO> appResource)
        //{
        //    bool save = false;
        //    try
        //    {
        //        int i = oBrAppointment.InsertResources(ConvertToAppointmentResourceList(appResource));
        //        if (i > 0)
        //            save = true;
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //    return save;
        //}

        //public List<AppointmentViewDTO> GetAppointmentsByCategory(ArgsDTO ArgsDTO, string category)
        //{
        //    brAppointment oAppointment = new brAppointment();
        //    List<AppointmentViewDTO> appointments = new List<AppointmentViewDTO>();

        //    try
        //    {
        //        switch (category)
        //        {
        //            case "today":
        //                appointments = AppointmentViewList(oAppointment.GetTodaysAppointments(ArgsDTO.SStartDate, ArgsDTO.SEndDate, ArgsDTO.Originator, ArgsDTO.StartIndex, ArgsDTO.RowCount));
        //                break;
        //            /*case "future":
        //                appointments = AppointmentViewList(oAppointment.GetFutureAppointments(ArgsDTO.SStartDate,ArgsDTO.ChiildOriginators, ArgsDTO.OriginatorId, ArgsDTO.StartIndex, ArgsDTO.RowCount));
        //                break;*/
        //            case "pending":
        //                appointments = AppointmentViewList(oAppointment.GetPendingAppointments(ArgsDTO.SStartDate, ArgsDTO.SEndDate, ArgsDTO.Originator, ArgsDTO.StartIndex, ArgsDTO.RowCount));
        //                break;
        //            default:
        //                break;
        //        }
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }

        //    return appointments;
        //}
        #endregion
    }
}
