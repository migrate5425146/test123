﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO;

using System.Data;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.Entities;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class LeadStageAdapter
    {
        #region - Converters -

        public static LeadStageEntity ConvertToLeadStage(LeadStageDTO leadStageDto)
        {

            LeadStageEntity LeadStage = LeadStageBR.Instance.CreateObject();

            try
            {
                //LeadStage.Colour = leadStageDto.Colour;
                LeadStage.EmailAddress = leadStageDto.EmailAddress;
                //LeadStage.IsNew = leadStageDto.IsNew;
                LeadStage.IsSelected = leadStageDto.IsSelected;
                LeadStage.MaxOpportunities = leadStageDto.MaxOpportunities;
                LeadStage.OpportunityAllowed = leadStageDto.OpportunityAllowed;
                //LeadStage.StageCount = leadStageDto.StageCount;
                LeadStage.StageId = leadStageDto.StageId;
                LeadStage.StageName = leadStageDto.StageName;
                LeadStage.StageOrder = leadStageDto.StageOrder;
            }
            catch (Exception)
            {

                throw;
            }
            return LeadStage;
        }

        public static LeadStageDTO ConvertToLeadStageDTO(LeadStageEntity leadStage)
        {

            LeadStageDTO leadStageDto = new LeadStageDTO();

            try
            {
                //leadStageDto.Colour = leadStage.Colour;
                leadStageDto.EmailAddress = leadStage.EmailAddress;
                //leadStageDto.IsNew = leadStage.IsNew;
                leadStageDto.IsSelected = leadStage.IsSelected;
                leadStageDto.MaxOpportunities = leadStage.MaxOpportunities;
                leadStageDto.OpportunityAllowed = leadStage.OpportunityAllowed;
                //leadStageDto.StageCount = leadStage.StageCount;
                leadStageDto.StageId = leadStage.StageId;
                leadStageDto.StageName = leadStage.StageName;
                leadStageDto.StageOrder = leadStage.StageOrder;
            }
            catch (Exception)
            {

                throw;
            }
            return leadStageDto;
        }

        public static List<LeadStageDTO> ConvertToLeadStageDTOList(List<LeadStageEntity> leadStage)
        {
            List<LeadStageDTO> lstLeadStage = new List<LeadStageDTO>();
            LeadStageDTO leadStageDto = null;

            try
            {
                foreach (LeadStageEntity boj in leadStage)
                {
                    leadStageDto = new LeadStageDTO();
                    //leadStageDto.Colour = boj.Colour;
                    leadStageDto.EmailAddress = boj.EmailAddress;
                    //leadStageDto.IsNew = boj.IsNew;
                    leadStageDto.IsSelected = boj.IsSelected;
                    leadStageDto.MaxOpportunities = boj.MaxOpportunities;
                    leadStageDto.OpportunityAllowed = boj.OpportunityAllowed;
                    //leadStageDto.StageCount = boj.StageCount;
                    leadStageDto.StageId = boj.StageId;
                    leadStageDto.StageName = boj.StageName;
                    leadStageDto.StageOrder = boj.StageOrder;
                    leadStageDto.LeadStage = boj.LeadStage;
                    leadStageDto.LeadsCount = boj.LeadsCount;
                    leadStageDto.LeadStageId = boj.LeadStageId;
                    lstLeadStage.Add(leadStageDto);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstLeadStage;
        }


        #endregion

        #region - Properties -

        //private brLeadStage oBrLeadStage = null;

        #endregion

        #region - Constructor -

        public LeadStageAdapter()
        {
            //if (oBrLeadStage == null)
            //    oBrLeadStage = new brLeadStage();
        }

        #endregion


        #region - Lead Stage -


        public List<LeadStageDTO> GetLeadStageChartData(ArgsDTO args)
        {
            try
            {
                return ConvertToLeadStageDTOList(LeadStageBR.Instance.GetLeadStageChartData(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<LeadStageDTO> GetLeadStages(ArgsDTO args)
        {
            try
            {
                return ConvertToLeadStageDTOList(LeadStageBR.Instance.GetLeadStages(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {
                
                throw;
            }
        }
        #endregion

        #region - Methods -
        /*
        public bool Save(List<LeadStageDTO> lstStages, string defaultDeptID)
        {
            bool save = false;
            DataTable dtTable = new DataTable();
            try
            {
                if (oBrLeadStage.Save(dtTable,defaultDeptID) > 0)
                    save = true;
            }
            catch (Exception)
            {
                
                throw;
            }
            return save;
        }

        public int Save(List<LeadStageDTO> leadStages, List<LeadStageDTO> deletedStages, string defaultDeptID)
        {
            List<LeadStage> leadStageList = new List<LeadStage>();
            List<LeadStage> deletedLeadStageList = new List<LeadStage>();
            try
            {
                foreach (LeadStageDTO item in leadStages)
                {
                    leadStageList.Add(ConvertToLeadStage(item));
                }

                foreach (LeadStageDTO item in deletedStages)
                {
                    deletedLeadStageList.Add(ConvertToLeadStage(item));
                }

                return oBrLeadStage.Save(leadStageList, deletedLeadStageList, defaultDeptID);
            }
            catch
            {
                throw;
            }
        }

        public bool CanDeleteLeadStage(int leadStageId)
        {
            if (!oBrLeadStage.CanDeleteLeadStage(leadStageId))
            {
                return false;
            }

            return true;
        }

        public LeadStageDTO GetLeadStage(int SourceId)
        {
            try
            {
                return ConvertToLeadStageDTO( oBrLeadStage.GetLeadStage(SourceId));
            }
            catch
            {
                throw;
            }
        }
        */

        //public bool SaveLeadStage(List<LeadStageDTO> lstStages, string defaultDeptID)
        //{
        //    bool save = false;
        //    List<LeadStage> leadStageList = new List<LeadStage>();
        //    try
        //    {
        //        foreach (LeadStageDTO item in lstStages)
        //        {
        //            leadStageList.Add(ConvertToLeadStage(item));
        //        }
        //        if (oBrLeadStage.Save(leadStageList, defaultDeptID) > 0)
        //            save = true;
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //    return save;
        //}
        #endregion
    }
}
