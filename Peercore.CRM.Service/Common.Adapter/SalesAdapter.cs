﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.Entities;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class SalesAdapter
    {
        #region - Converters -

        public static EndUserSalesEntity ConvertToSales(EndUserSalesDTO endUserSalesDTO)
        {
            EndUserSalesEntity endUserSalesEntity = EndUserSalesEntity.CreateObject();

            try
            {
                endUserSalesEntity.Cases = endUserSalesDTO.Cases;     
                endUserSalesEntity.Discount = endUserSalesDTO.Discount;
                endUserSalesEntity.Documentdate = endUserSalesDTO.Documentdate;
                endUserSalesEntity.Dollar1 = endUserSalesDTO.Dollar1;
                endUserSalesEntity.Dollar2 = endUserSalesDTO.Dollar2;
                endUserSalesEntity.Dollar3 = endUserSalesDTO.Dollar3;
                endUserSalesEntity.DueAmount = endUserSalesDTO.DueAmount;
                endUserSalesEntity.GracePeriod = endUserSalesDTO.GracePeriod;
                endUserSalesEntity.Market = endUserSalesDTO.Market;
                endUserSalesEntity.Month = endUserSalesDTO.Month;
                endUserSalesEntity.Period = endUserSalesDTO.Period;
                endUserSalesEntity.Status = endUserSalesDTO.Status;
                endUserSalesEntity.Tonnes1 = endUserSalesDTO.Tonnes1;
                endUserSalesEntity.Tonnes2 = endUserSalesDTO.Tonnes2;
                endUserSalesEntity.Tonnes3 = endUserSalesDTO.Tonnes3;
                endUserSalesEntity.TransType = endUserSalesDTO.TransType;
                endUserSalesEntity.Year = endUserSalesDTO.Year;
            }
            catch (Exception)
            {
                throw;
            }
            return endUserSalesEntity;
        }

        public static EndUserSalesDTO ConvertToSalesDTO(EndUserSalesEntity endUserSalesEntity)
        {
            EndUserSalesDTO endUserSalesDTO = new EndUserSalesDTO();

            try
            {
                if (endUserSalesEntity != null)
                {
                    endUserSalesDTO.Cases = endUserSalesEntity.Cases;
                    endUserSalesDTO.Discount = endUserSalesEntity.Discount;
                    endUserSalesDTO.Documentdate = endUserSalesEntity.Documentdate;
                    endUserSalesDTO.Dollar1 = endUserSalesEntity.Dollar1;
                    endUserSalesDTO.Dollar2 = endUserSalesEntity.Dollar2;
                    endUserSalesDTO.Dollar3 = endUserSalesEntity.Dollar3;
                    endUserSalesDTO.DueAmount = endUserSalesEntity.DueAmount;
                    endUserSalesDTO.GracePeriod = endUserSalesEntity.GracePeriod;
                    endUserSalesDTO.Market = endUserSalesEntity.Market;
                    endUserSalesDTO.Month = endUserSalesEntity.Month;
                    endUserSalesDTO.Period = endUserSalesEntity.Period;
                    endUserSalesDTO.Status = endUserSalesEntity.Status;
                    endUserSalesDTO.Tonnes1 = endUserSalesEntity.Tonnes1;
                    endUserSalesDTO.Tonnes2 = endUserSalesEntity.Tonnes2;
                    endUserSalesDTO.Tonnes3 = endUserSalesEntity.Tonnes3;
                    endUserSalesDTO.TransType = endUserSalesEntity.TransType;
                    endUserSalesDTO.Year = endUserSalesEntity.Year;
                    endUserSalesDTO.TotalSales = endUserSalesEntity.TotalSales;
                    endUserSalesDTO.SalesSummary = endUserSalesEntity.SalesSummary;
                    endUserSalesDTO.XLabel_FirstMonth = endUserSalesEntity.XLabel_FirstMonth;
                    endUserSalesDTO.XLabel_SecondMonth = endUserSalesEntity.XLabel_SecondMonth;
                    endUserSalesDTO.XLabel_ThirdMonth = endUserSalesEntity.XLabel_ThirdMonth;
                    endUserSalesDTO.Description = endUserSalesEntity.Description;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return endUserSalesDTO;
        }

        public static List<EndUserSalesDTO> ConvertToSalesDTOList(List<EndUserSalesEntity> listendUserSalesEntity)
        {
            EndUserSalesDTO endUserSalesDTO = null ;
            List<EndUserSalesDTO> lstEndUserSales = new List<EndUserSalesDTO>();
            try
            {
                foreach (EndUserSalesEntity endUserSalesEntity in listendUserSalesEntity)
                {
                    endUserSalesDTO = new EndUserSalesDTO();
                    endUserSalesDTO.Cases = endUserSalesEntity.Cases;
                    endUserSalesDTO.Discount = endUserSalesEntity.Discount;
                    endUserSalesDTO.Documentdate = endUserSalesEntity.Documentdate;
                    endUserSalesDTO.Dollar1 = endUserSalesEntity.Dollar1;
                    endUserSalesDTO.Dollar2 = endUserSalesEntity.Dollar2;
                    endUserSalesDTO.Dollar3 = endUserSalesEntity.Dollar3;
                    endUserSalesDTO.DueAmount = endUserSalesEntity.DueAmount;
                    endUserSalesDTO.GracePeriod = endUserSalesEntity.GracePeriod;
                    endUserSalesDTO.Market = endUserSalesEntity.Market;
                    endUserSalesDTO.Month = endUserSalesEntity.Month;
                    endUserSalesDTO.Period = endUserSalesEntity.Period;
                    endUserSalesDTO.Status = endUserSalesEntity.Status;
                    endUserSalesDTO.Tonnes1 = endUserSalesEntity.Tonnes1;
                    endUserSalesDTO.Tonnes2 = endUserSalesEntity.Tonnes2;
                    endUserSalesDTO.Tonnes3 = endUserSalesEntity.Tonnes3;
                    endUserSalesDTO.TransType = endUserSalesEntity.TransType;
                    endUserSalesDTO.Year = endUserSalesEntity.Year;

                    lstEndUserSales.Add(endUserSalesDTO);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstEndUserSales;
        }

        public static EOISalesDTO ConvertToEOISalesDTO(EOISalesEntity eoiSalesEntity)
        {
            EOISalesDTO eoiSalesDTO = new EOISalesDTO();

            try
            {
                if (eoiSalesEntity != null)
                {
                    eoiSalesDTO = new EOISalesDTO()
                    {
                        Cases1 = eoiSalesEntity.Cases1,
                        Cases2 = eoiSalesEntity.Cases2,
                        Cases3 = eoiSalesEntity.Cases3,
                        Cases4 = eoiSalesEntity.Cases4,
                        Cases5 = eoiSalesEntity.Cases5,
                        Cases6 = eoiSalesEntity.Cases6,
                        Cases7 = eoiSalesEntity.Cases7,
                        Cases8 = eoiSalesEntity.Cases8,
                        Cases9 = eoiSalesEntity.Cases9,
                        Cases10 = eoiSalesEntity.Cases10,
                        Cases11 = eoiSalesEntity.Cases11,
                        Cases12 = eoiSalesEntity.Cases12,
                        CostYear = eoiSalesEntity.CostYear,
                        Description = eoiSalesEntity.Description,
                        EndOfYearRate = eoiSalesEntity.EndOfYearRate,
                        Market = eoiSalesEntity.Market,
                        MidYearRate = eoiSalesEntity.MidYearRate,
                        Quarter1Rate = eoiSalesEntity.Quarter1Rate,
                        Quarter2Rate = eoiSalesEntity.Quarter2Rate,
                        Quarter3Rate = eoiSalesEntity.Quarter3Rate,
                        Quarter4Rate = eoiSalesEntity.Quarter4Rate,
                        ThirdQuarterRate = eoiSalesEntity.ThirdQuarterRate,
                        UOM = eoiSalesEntity.UOM,
                        RowCount = eoiSalesEntity.TotalCount
                    };
                }
            }
            catch (Exception)
            {

                throw;
            }
            return eoiSalesDTO;
        }

        public static List<SalesInfoDTO> ConvertToSalesInfoDTOList(List<SalesInfoEntity> listSalesInfoEntity)
        {
            SalesInfoDTO salesInfoDTO = null;
            List<SalesInfoDTO> lstSalesInfo = new List<SalesInfoDTO>();
            try
            {
                if (listSalesInfoEntity == null)
                    return null;

                foreach (SalesInfoEntity item in listSalesInfoEntity)
                {
                    salesInfoDTO = new SalesInfoDTO()
                    {
                        Brand = item.Brand,
                        CatlogCode = item.CatlogCode,
                        Col1 = item.Col1,
                        Col2 = item.Col2,
                        Col3 = item.Col3,
                        Col4 = item.Col4,
                        Col5 = item.Col5,
                        Col6 = item.Col6,
                        Col7 = item.Col7,
                        Col8 = item.Col8,
                        Col9 = item.Col9,
                        Col10 = item.Col10,
                        CustomerCode = item.CustomerCode,
                        CustomerName = item.CustomerName,
                        Description = item.Description,
                        Dollar1 = item.Dollar1,
                        Dollar2 = item.Dollar2,
                        Dollar3 = item.Dollar3,
                        Dollar4 = item.Dollar4,
                        Dollar5 = item.Dollar5,
                        Dollar6 = item.Dollar6,
                        RepCode = item.RepCode,
                        RepName = item.RepName,
                        RowCount = item.TotalCount,
                        StateCode = item.StateCode,
                        StateName = item.StateName,
                        Sum1 = item.Sum1,
                        Sum2 = item.Sum2,
                        Sum3 = item.Sum3,
                        Sum4 = item.Sum4,
                        Sum5 = item.Sum5,
                        Sum6 = item.Sum6,
                        Sum7 = item.Sum7,
                        Sum8 = item.Sum8,
                        Sum9 = item.Sum9,
                        Sum10 = item.Sum10,
                        Sum11 = item.Sum11,
                        Sum12 = item.Sum12,
                        Sum13 = item.Sum13,
                        Sum14 = item.Sum14,
                        Sum15 = item.Sum15,
                        Sum16 = item.Sum16,
                        Sum17 = item.Sum17,
                        Sum18 = item.Sum18,
                        Tonnes1 = item.Tonnes1,
                        Tonnes2 = item.Tonnes2,
                        Tonnes3 = item.Tonnes3,
                        Tonnes4 = item.Tonnes4,
                        Tonnes5 = item.Tonnes5,
                        Tonnes6 = item.Tonnes6,
                        ValYear1 = item.ValYear1,
                        ValYear2 = item.ValYear2,
                        ValYear3 = item.ValYear3,
                        VolYear1 = item.VolYear1,
                        VolYear2 = item.VolYear2,
                        VolYear3 = item.VolYear3                        
                    };

                    lstSalesInfo.Add(salesInfoDTO);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstSalesInfo;
        }

        public static List<BePieChartDTO> ConvertToBePieChartDTOList(List<BePieChartEntity> listSalesInfoEntity)
        {
            BePieChartDTO salesInfoDTO = null;
            List<BePieChartDTO> lstSalesInfo = new List<BePieChartDTO>();
            try
            {
                if (listSalesInfoEntity == null)
                    return null;

                foreach (BePieChartEntity item in listSalesInfoEntity)
                {
                    salesInfoDTO = new BePieChartDTO()
                    {
                        Name = item.Name,
                        Value = item.Value
                    };

                    lstSalesInfo.Add(salesInfoDTO);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstSalesInfo;
        }

        public static SalesInfoDetailViewStateDTO ConvertToSalesInfoDetailViewStateDTO(SalesInfoDetailViewStateEntity sIDetailViewStateEntity)
        {
            SalesInfoDetailViewStateDTO item = new SalesInfoDetailViewStateDTO();

            try
            {
                if (sIDetailViewStateEntity != null)
                {
                    item = new SalesInfoDetailViewStateDTO()
                    {
                        CodeDescription = sIDetailViewStateEntity.CodeDescription,
                        DisplayOptionIndex = sIDetailViewStateEntity.DisplayOptionIndex,
                        //LstSalesInfoDetailGrid = ConvertToSalesInfoDTOList(sIDetailViewStateEntity.LstSalesInfoDetailGrid),
                        //LstCustomizedTableGrid = ConvertToCustomizedTableDTOList(sIDetailViewStateEntity.LstCustomizedTableGrid),
                        //lstSalesInfoDetailGridColumnHeaders = sIDetailViewStateEntity.lstSalesInfoDetailGridColumnHeaders,
                        //lstPieChartValues = ConvertToBePieChartDTOList(sIDetailViewStateEntity.lstPieChartValues),
                        sBrand = sIDetailViewStateEntity.sBrand,
                        //sCatalogueGroup = sIDetailViewStateEntity.sCatalogueGroup,
                        sCustomer = sIDetailViewStateEntity.sCustomer,
                        //sBusArea= sIDetailViewStateEntity.sBusArea,
                        //sCatalogueSubGroup = sIDetailViewStateEntity.sCatalogueSubGroup,
                        //sCustomerGroup = sIDetailViewStateEntity.sCustomerGroup,
                        sMarket=sIDetailViewStateEntity.sMarket,
                        sMonth=sIDetailViewStateEntity.sMonth,
                        sProduct = sIDetailViewStateEntity.sProduct,
                        sProductType = sIDetailViewStateEntity.sProductType,
                        SortFieldOptionIndex = sIDetailViewStateEntity.SortFieldOptionIndex,
                        sRep = sIDetailViewStateEntity.sRep,
                        //sState = sIDetailViewStateEntity.sState,
                        sSubDetailType = sIDetailViewStateEntity.sSubDetailType,
                        //sSubParent = sIDetailViewStateEntity.sSubParent,
                        RowCount = sIDetailViewStateEntity.TotalCount,

                        
                        
                    };
                }
            }
            catch (Exception)
            {

                throw;
            }
            return item;
        }

        public static SalesInfoDetailSearchCriteriaEntity ConvertToSalesInfoDetailSearchCriteria(SalesInfoDetailSearchCriteriaDTO salesInfoDetailSearchCriteriaDTO)
        {
            SalesInfoDetailSearchCriteriaEntity endUserSalesEntity = SalesInfoDetailSearchCriteriaEntity.CreateObject();

            try
            {
                endUserSalesEntity.AdditionalCondition = salesInfoDetailSearchCriteriaDTO.AdditionalCondition;
                //endUserSalesEntity.BackeryOption = salesInfoDetailSearchCriteriaDTO.BackeryOption;
                endUserSalesEntity.Brand = salesInfoDetailSearchCriteriaDTO.Brand;
                endUserSalesEntity.BrandDescription = salesInfoDetailSearchCriteriaDTO.BrandDescription;
                //endUserSalesEntity.BusinessArea = salesInfoDetailSearchCriteriaDTO.BusinessArea;
                //endUserSalesEntity.BusinessAreaList = salesInfoDetailSearchCriteriaDTO.BusinessAreaList;
                //endUserSalesEntity.CatalogCode = salesInfoDetailSearchCriteriaDTO.CatalogCode;
                //endUserSalesEntity.CatalogDescripion = salesInfoDetailSearchCriteriaDTO.CatalogDescripion;
                //endUserSalesEntity.CatalogType = salesInfoDetailSearchCriteriaDTO.CatalogType;
                //endUserSalesEntity.CategoryGroup = salesInfoDetailSearchCriteriaDTO.CategoryGroup;
                //endUserSalesEntity.CategorySubGroup = salesInfoDetailSearchCriteriaDTO.CategorySubGroup;
                endUserSalesEntity.cDisplayOption = salesInfoDetailSearchCriteriaDTO.cDisplayOption;
                endUserSalesEntity.CustomerCode = salesInfoDetailSearchCriteriaDTO.CustomerCode;
                endUserSalesEntity.CustomerDescription = salesInfoDetailSearchCriteriaDTO.CustomerDescription;
                //endUserSalesEntity.CustomerGroup = salesInfoDetailSearchCriteriaDTO.CustomerGroup;
                //endUserSalesEntity.CustomerSubGroup = salesInfoDetailSearchCriteriaDTO.CustomerSubGroup;
                endUserSalesEntity.DetailType = salesInfoDetailSearchCriteriaDTO.DetailType;
                endUserSalesEntity.DisplayFullName = salesInfoDetailSearchCriteriaDTO.DisplayFullName;
                endUserSalesEntity.iCostPeriod = salesInfoDetailSearchCriteriaDTO.iCostPeriod;
                endUserSalesEntity.Market = salesInfoDetailSearchCriteriaDTO.MarketId;
                endUserSalesEntity.Month = salesInfoDetailSearchCriteriaDTO.Month;
                //endUserSalesEntity.ParentCustomerCode = salesInfoDetailSearchCriteriaDTO.ParentCustomerCode;
                //endUserSalesEntity.parentCustomerDescription = salesInfoDetailSearchCriteriaDTO.parentCustomerDescription;
                endUserSalesEntity.RepCode = salesInfoDetailSearchCriteriaDTO.RepCode;
                //endUserSalesEntity.RepCodes = salesInfoDetailSearchCriteriaDTO.RepCodes;
                endUserSalesEntity.RepDescription = salesInfoDetailSearchCriteriaDTO.RepDescription;
                endUserSalesEntity.RepType = salesInfoDetailSearchCriteriaDTO.RepType;
                //endUserSalesEntity.sEndUserTable = salesInfoDetailSearchCriteriaDTO.sEndUserTable;
                endUserSalesEntity.showAllReps = salesInfoDetailSearchCriteriaDTO.showAllReps;
                endUserSalesEntity.sLastFlag = salesInfoDetailSearchCriteriaDTO.sLastFlag;
                //endUserSalesEntity.sLcSumTable = salesInfoDetailSearchCriteriaDTO.sLcSumTable;
                endUserSalesEntity.SortField = salesInfoDetailSearchCriteriaDTO.SortField;
                //endUserSalesEntity.State = salesInfoDetailSearchCriteriaDTO.State;
                //endUserSalesEntity.SubParentCustomerCode = salesInfoDetailSearchCriteriaDTO.SubParentCustomerCode;
                //endUserSalesEntity.SubParentCustomerDescription = salesInfoDetailSearchCriteriaDTO.SubParentCustomerDescription;
                endUserSalesEntity.TotalCount = salesInfoDetailSearchCriteriaDTO.RowCount;
                //endUserSalesEntity.ChildReps = salesInfoDetailSearchCriteriaDTO.ChildReps;

                endUserSalesEntity.Route = salesInfoDetailSearchCriteriaDTO.Route;
                endUserSalesEntity.RouteDescription = salesInfoDetailSearchCriteriaDTO.RouteDescription;
                endUserSalesEntity.IsCMP = salesInfoDetailSearchCriteriaDTO.IsCMP;
            }
            catch (Exception)
            {
                throw;
            }
            return endUserSalesEntity;
        }

        public static List<AtblDTO> ConvertToSalesInfoDTOList(List<AtblEntity> listAtblEntity)
        {
            AtblDTO atblDTO = null;
            List<AtblDTO> lstAtbl = new List<AtblDTO>();
            try
            {
                foreach (AtblEntity item in listAtblEntity)
                {
                    atblDTO = new AtblDTO()
                    {
                        budget_val_p = item.budget_val_p,
                        budget_val_pp = item.budget_val_pp,
                        budget_vol_p = item.budget_vol_p,
                        budget_vol_pp = item.budget_vol_pp,
                        period = item.period,
                        RowCount=item.TotalCount,
                        sales_profit_p = item.sales_profit_p,
                        sales_profit_perc_p = item.sales_profit_perc_p,
                        sales_profit_perc_pp = item.sales_profit_perc_pp,
                        sales_profit_pp = item.sales_profit_pp,
                        sales_profit_tonval_p = item.sales_profit_tonval_p,
                        sales_profit_tonval_pp = item.sales_profit_tonval_pp,
                        sales_val_p = item.sales_val_p,
                        sales_val_pp = item.sales_val_pp,
                        sales_val_tonval_p = item.sales_val_tonval_p,
                        sales_val_tonval_pp = item.sales_val_tonval_pp,
                        sales_vol_p = item.sales_vol_p,
                        sales_vol_pp = item.sales_vol_pp                        
                    };

                    lstAtbl.Add(atblDTO);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstAtbl;
        }

        public static List<CustomizedTableDTO> ConvertToCustomizedTableDTOList(List<CustomizedTableEntity> listCustomizedTableEntity)
        {
            CustomizedTableDTO customizedTableDTO = null;
            List<CustomizedTableDTO> lstCustomizedTable = new List<CustomizedTableDTO>();
            try
            {
                if (listCustomizedTableEntity == null)
                    return null;

                foreach (CustomizedTableEntity item in listCustomizedTableEntity)
                {
                    customizedTableDTO = new CustomizedTableDTO()
                    {
                         M1D = item.M1D,
                         M1T = item.M1T,
                         M1U = item.M1U,

                         M2D = item.M2D,
                         M2T = item.M2T,
                         M2U = item.M2U,

                         M3D = item.M3D,
                         M3T = item.M3T,
                         M3U = item.M3U,

                         M4D = item.M4D,
                         M4T = item.M4T,
                         M4U = item.M4U,

                         M5D = item.M5D,
                         M5T = item.M5T,
                         M5U = item.M5U,

                         M6D = item.M6D,
                         M6T = item.M6T,
                         M6U = item.M6U,

                         MC = item.MC,
                         MD = item.MD,

                         MLYTDD = item.MLYTDD,
                         MLYTDT = item.MLYTDT,
                         MLYTDU = item.MLYTDU,

                         MPD = item.MPD,
                         MPT = item.MPT,
                         MPU = item.MPU,

                         MTD = item.MTD,
                         MTT = item.MTT,
                         MTU = item.MTU,

                         MTYTDD = item.MTYTDD,
                         MTYTDT = item.MTYTDT,
                         MTYTDU = item.MTYTDU,

                         MY1D = item.MY1D,
                         MY1T = item.MY1T,
                         MY1U = item.MY1U,

                         MY2D = item.MY2D,
                         MY2T = item.MY2T,
                         MY2U = item.MY2U,

                         MY3D = item.MY3D,
                         MY3T = item.MY3T,
                         MY3U = item.MY3U,
                         RowCount = item.TotalCount,
                         //ColumnHeaders = columnHeaders

                         M1DTot = Math.Round(item.M1DTot,2),
                         M1TTot = Math.Round(item.M1TTot,2),
                         M1UTot = Math.Round(item.M1UTot,2),

                         M2DTot = Math.Round(item.M2DTot,2),
                         M2TTot = Math.Round(item.M2TTot,2),
                         M2UTot = Math.Round(item.M2UTot,2),

                         M3DTot = Math.Round(item.M3DTot,2),
                         M3TTot = Math.Round(item.M3TTot,2),
                         M3UTot = Math.Round(item.M3UTot,2),

                         M4DTot = Math.Round(item.M4DTot,2),
                         M4TTot = Math.Round(item.M4TTot,2),
                         M4UTot = Math.Round(item.M4UTot,2),

                         M5DTot = Math.Round(item.M5DTot,2),
                         M5TTot = Math.Round(item.M5TTot,2),
                         M5UTot = Math.Round(item.M5UTot,2),

                         M6DTot = Math.Round(item.M6DTot,2),
                         M6TTot = Math.Round(item.M6TTot,2),
                         M6UTot = Math.Round(item.M6UTot,2),

                         MLYTDDTot = Math.Round(item.MLYTDDTot,2),
                         MLYTDTTot = Math.Round(item.MLYTDTTot,2),
                         MLYTDUTot = Math.Round(item.MLYTDUTot,2),

                         MPDTot = Math.Round(item.MPDTot,2),
                         MPTTot = Math.Round(item.MPTTot,2),
                         MPUTot = Math.Round(item.MPUTot,2),

                         MTDTot = Math.Round(item.MTDTot,2),
                         MTTTot = Math.Round(item.MTTTot,2),
                         MTUTot = Math.Round(item.MTUTot,2),

                         MTYTDDTot = Math.Round(item.MTYTDDTot,2),
                         MTYTDTTot = Math.Round(item.MTYTDTTot,2),
                         MTYTDUTot = Math.Round(item.MTYTDUTot,2),

                         MY1DTot = Math.Round(item.MY1DTot,2),
                         MY1TTot = Math.Round(item.MY1TTot,2),
                         MY1UTot = Math.Round(item.MY1UTot,2),

                         MY2DTot = Math.Round(item.MY2DTot,2),
                         MY2TTot = Math.Round(item.MY2TTot,2),
                         MY2UTot = Math.Round(item.MY2UTot,2),

                         MY3DTot = Math.Round(item.MY3DTot,2),
                         MY3TTot = Math.Round(item.MY3TTot,2),
                         MY3UTot = Math.Round(item.MY3UTot, 2),

                         MDTot = item.MDTot
                    };

                    lstCustomizedTable.Add(customizedTableDTO);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstCustomizedTable;
        }

        public static List<BusAllDTO> ConvertToBusAllDTOList(List<BusAllEntity> listBusAllEntity)
        {
            BusAllDTO busAllDTO = null;
            List<BusAllDTO> lstBusAll = new List<BusAllDTO>();
            try
            {
                foreach (BusAllEntity item in listBusAllEntity)
                {
                    busAllDTO = new BusAllDTO()
                    {
                        BudgetSales = item.BudgetSales,
                        BudgetTonne = item.BudgetTonne,
                        LastYearBudgetSales = item.LastYearBudgetSales,
                        LastYearBudgetTonne = item.LastYearBudgetTonne,
                        LastYearPercentage = item.LastYearPercentage,
                        LastYearProfit = item.LastYearProfit,
                        LastYearPT = item.LastYearPT,
                        LastYearST = item.LastYearST,
                        LastYearSales = item.LastYearSales,
                        LastYearTonne = item.LastYearTonne,
                        Percentage = item.Percentage,
                        Period = item.Period,
                        Profit = item.Profit,
                        PT = item.PT,
                        RowCount = item.TotalCount,
                        Sales = item.Sales,
                        ST = item.ST,
                        Tonne = item.Tonne                        
                    };

                    lstBusAll.Add(busAllDTO);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstBusAll;
        }

        public static List<BusAllYearDTO> ConvertToBusAllYearDTOList(List<BusAllYearEntity> listBusAllYearEntity)
        {
            BusAllYearDTO busAllYearDTO = null;
            List<BusAllYearDTO> lstBusYearAll = new List<BusAllYearDTO>();
            try
            {
                foreach (BusAllYearEntity item in listBusAllYearEntity)
                {
                    busAllYearDTO = new BusAllYearDTO()
                    {
                        Header = item.Header,
                        CurrentYear = item.CurrentYear,
                        LastYear = item.LastYear,
                        Year1 = item.Year1,
                        Year2 = item.Year2,
                        Year3 = item.Year3,
                        Year4 = item.Year4
                    };

                    lstBusYearAll.Add(busAllYearDTO);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstBusYearAll;
        }

        public static TargetEntity ConvertToTargetEntity(TargetDTO targetDTO)
        {
            TargetEntity targetEntity = TargetEntity.CreateObject();

            try
            {
                targetEntity.RepCode = targetDTO.RepCode;
                targetEntity.Target = targetDTO.Target;
                targetEntity.Actual = targetDTO.Actual;
                targetEntity.RouteId = targetDTO.RouteId;
                targetEntity.DistributorId = targetDTO.DistributorId;
                targetEntity.DistributorName = targetDTO.DistributorName;
                targetEntity.MarketId = targetDTO.MarketId;
                targetEntity.MarketName = targetDTO.MarketName;
                targetEntity.RouteName = targetDTO.RouteName;
            }
            catch (Exception)
            {
                throw;
            }
            return targetEntity;
        }

        public static TargetDTO ConvertToTargetDTO(TargetEntity targetEntity)
        {
            TargetDTO targetDTO = new TargetDTO();

            try
            {
                if (targetEntity != null)
                {
                    targetDTO.RepCode = targetEntity.RepCode;
                    targetDTO.Target = targetEntity.Target;
                    targetDTO.TargetInMils = targetEntity.Target != 0 ? targetEntity.Target / 1000 : 0;
                    targetDTO.Actual = targetEntity.Actual;
                    targetDTO.TargetInMils = targetEntity.Actual != 0 ? targetEntity.Actual / 1000 : 0;
                    targetDTO.RouteId = targetEntity.RouteId;
                    targetDTO.DistributorId = targetEntity.DistributorId;
                    targetDTO.DistributorName = targetEntity.DistributorName;
                    targetDTO.MarketId = targetEntity.MarketId;
                    targetDTO.MarketName = targetEntity.MarketName;
                    targetDTO.RouteName = targetEntity.RouteName;

                }
            }
            catch (Exception)
            {

                throw;
            }
            return targetDTO;
        }

        public static List<TargetDTO> ConvertToTargetDTOList(List<TargetEntity> listIn)
        {
            List<TargetDTO> listOut = new List<TargetDTO>();

            foreach(TargetEntity item in listIn)
            {
                listOut.Add(ConvertToTargetDTO(item));
            }

            return listOut;
        }

        public static List<TargetEntity> ConvertToTargetEntityList(List<TargetDTO> listIn)
        {
            List<TargetEntity> listOut = new List<TargetEntity>();

            foreach (TargetDTO item in listIn)
            {
                listOut.Add(ConvertToTargetEntity(item));
            }

            return listOut;
        }
        
        #endregion

        public EndUserSalesDTO GetCurrentCostPeriod()
        {          
            try
            {
                return ConvertToSalesDTO(SalesBR.Instance.GetCurrentCostPeriod());               
            }
            catch (Exception)
            {

                throw;
            }           
        }

        public EndUserSalesDTO GetCurrentCostPeriodForDateTime(string dtDate)
        {
            try
            {
                return ConvertToSalesDTO(SalesBR.Instance.GetCostPeriod(dtDate));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetMonthForCostPeriod(int costYear, int costPeriod)
        {
            try
            {
                return SalesBR.Instance.GetMonthForCostPeriod(costYear, costPeriod);
            }
            catch
            {                
                throw;
            }
        }

        public EndUserSalesDTO GetSalesData(string custCode,string date)
        {
            try
            {
                return ConvertToSalesDTO(SalesBR.Instance.GetSalesData(custCode,date));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<EndUserSalesDTO> GetDebtorsDetails(string custCode)
        {
            try
            {
                return ConvertToSalesDTOList(SalesBR.Instance.GetDebtorsDetails(custCode));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<EndUserSalesDTO> GetCostYears()
        {
            List<EndUserSalesDTO> SalesDTOList = new List<EndUserSalesDTO>();

            try
            {
                List<EndUserSalesEntity> SalesEntityList = SalesBR.Instance.GetCostYears();

                foreach (EndUserSalesEntity EndUserSalesEntity in SalesEntityList)
                {
                    SalesDTOList.Add(ConvertToSalesDTO(EndUserSalesEntity));
                }
            }
            catch (Exception)
            {

                throw;
            }
            return SalesDTOList;
        }

        public List<EndUserSalesDTO> GetCostPeriod(ArgsDTO args)
        {
            List<EndUserSalesDTO> SalesDTOList = new List<EndUserSalesDTO>();

            try
            {
                List<EndUserSalesEntity> SalesEntityList = SalesBR.Instance.GetCostPeriod(ActivityAdapter.ConvertToArgsEntity(args));

                foreach (EndUserSalesEntity EndUserSalesEntity in SalesEntityList)
                {
                    SalesDTOList.Add(ConvertToSalesDTO(EndUserSalesEntity));
                }
            }
            catch (Exception)
            {

                throw;
            }
            return SalesDTOList;
        }


        public List<EOISalesDTO> GetEOISales(ArgsDTO args)
        {
            List<EOISalesDTO> SalesDTOList = new List<EOISalesDTO>();

            try
            {
                List<EOISalesEntity> SalesEntityList = EOISaleBR.Instance.GetEOISales(ActivityAdapter.ConvertToArgsEntity(args));

                foreach (EOISalesEntity EndUserSalesEntity in SalesEntityList)
                {
                    SalesDTOList.Add(ConvertToEOISalesDTO(EndUserSalesEntity));
                }
            }
            catch (Exception)
            {

                throw;
            }
            return SalesDTOList;
        }

        public SalesInfoDetailViewStateDTO GetSalesInfoDetailViewState(SalesInfoDetailSearchCriteriaDTO busSalesEnqSrc, ArgsDTO args, string type = "grid")
        {
            try
            {
                return ConvertToSalesInfoDetailViewStateDTO(
                    SalesInfoBR.Instance.GetSalesInfoDetailViewState(
                                ConvertToSalesInfoDetailSearchCriteria(busSalesEnqSrc), 
                                                    ActivityAdapter.ConvertToArgsEntity(args),type));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<AtblDTO> GetTrendData(SalesInfoDetailSearchCriteriaDTO busSalesEnqSrc, ref List<string> lstCostPeriods, string sCode)
        {
            try
            {
                return ConvertToSalesInfoDTOList(
                    SalesInfoBR.Instance.GetTrendData(ConvertToSalesInfoDetailSearchCriteria(busSalesEnqSrc), ref lstCostPeriods,sCode));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public SalesInfoDetailViewStateDTO GetEndUserSalesInfoDetails(SalesInfoDetailSearchCriteriaDTO searchCriteria, ArgsDTO args, string type = "grid")
        {
            try
            {
                return ConvertToSalesInfoDetailViewStateDTO(EndUserSalesInfoBR.Instance.GetEndUserSalesInfoDetails(ConvertToSalesInfoDetailSearchCriteria(searchCriteria),
                    ActivityAdapter.ConvertToArgsEntity(args), type));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<string> LoadCostPeriods(int iCostYear)
        {
            try
            {
                List<string> lstCostPeriods = new List<string>();
                SalesBR.Instance.LoadCostPeriods(ref lstCostPeriods, iCostYear);
                return lstCostPeriods;
            }
            catch
            {
                throw;
            }
        }

        public List<BusAllDTO> GetBusAllDetails(SalesInfoDetailSearchCriteriaDTO busSalesEnqSrc,string historyType)
        {
            try
            {
                return ConvertToBusAllDTOList(SalesBR.Instance.GetBusAllDetails(ConvertToSalesInfoDetailSearchCriteria(busSalesEnqSrc), historyType));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<BusAllYearDTO> GetBusAllYearDetails(SalesInfoDetailSearchCriteriaDTO busSalesEnqSrc, string historyType)
        {
            try
            {
                return ConvertToBusAllYearDTOList(SalesBR.Instance.GetBusAllYearDetails(ConvertToSalesInfoDetailSearchCriteria(busSalesEnqSrc), historyType));
            }
            catch (Exception)
            {
                throw;
            }
        }

        //public TargetDTO GetDistributorTargetById(ArgsDTO args)
        //{
        //    try
        //    {
        //        return ConvertToTargetDTO(TargetBR.Instance.GetDistributorTargetById(ActivityAdapter.ConvertToArgsEntity(args)));
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        //public List<TargetDTO> GetMarketTargetsByDistributorId(ArgsDTO args)
        //{
        //    try
        //    {
        //        return ConvertToTargetDTOList(MarketBR.Instance.GetMarketTargetsByDistributorId(ActivityAdapter.ConvertToArgsEntity(args)));
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}

        //public List<TargetDTO> GetRouteTargetsByDistributorId(ArgsDTO args)
        //{
        //    try
        //    {
        //        return ConvertToTargetDTOList(RouteMasterBR.Instance.GetRouteTargetsByDistributorId(ActivityAdapter.ConvertToArgsEntity(args)));
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}             

        /*
        #region - Converters -

        public static DebtorsBarometer ConvertToActivity(DebtorsBarometerDTO debtorsBarometerDTO)
        {            
            return new DebtorsBarometer();
        }

        public static DebtorsBarometerDTO ConvertToDebtorsBarometerDTO(DebtorsBarometer debtorsBarometer)
        {
            DebtorsBarometerDTO oDebtorsBarometerDTO = new DebtorsBarometerDTO();
            try
            {

                if (debtorsBarometer != null)
                {
                    oDebtorsBarometerDTO.Li_GracePeriod = debtorsBarometer.Li_GracePeriod;
                    oDebtorsBarometerDTO.Ld_doc_date = debtorsBarometer.Ld_doc_date;
                    oDebtorsBarometerDTO.Lv_amount_due = debtorsBarometer.Lv_amount_due;
                    oDebtorsBarometerDTO.Li_TransType = debtorsBarometer.Li_TransType;
                    oDebtorsBarometerDTO.Lv_Status = debtorsBarometer.Lv_Status;
                    oDebtorsBarometerDTO.Lv_UnallocDiscount = debtorsBarometer.Lv_UnallocDiscount;
                    oDebtorsBarometerDTO.Ld_datethen = debtorsBarometer.Ld_datethen;
                    oDebtorsBarometerDTO.Ld_datediff = debtorsBarometer.Ld_datediff;
                    oDebtorsBarometerDTO.DCur = debtorsBarometer.DCur;
                    oDebtorsBarometerDTO.D30 = debtorsBarometer.D30;
                    oDebtorsBarometerDTO.D60 = debtorsBarometer.D60;
                    oDebtorsBarometerDTO.D90 = debtorsBarometer.D90;
                    oDebtorsBarometerDTO.DOver = debtorsBarometer.DOver;
                    oDebtorsBarometerDTO.DTotal = debtorsBarometer.DTotal;
                    oDebtorsBarometerDTO.Ld_datenow = debtorsBarometer.Ld_datenow;
                    oDebtorsBarometerDTO.Ld_as_at_date = debtorsBarometer.Ld_as_at_date; 
                }
            }
            catch (Exception)
            {

                throw;
            }
            return oDebtorsBarometerDTO;
        }

        public static SalesGraphDataDTO ConvertToSalesGraphDataDTO(SalesGraphData salesGraphData)
        {
            SalesGraphDataDTO oSalesGraphDataDTO = new SalesGraphDataDTO();
            try
            {
                oSalesGraphDataDTO.YValue_FirstMonth = salesGraphData.YValue_FirstMonth;
                oSalesGraphDataDTO.XLabel_FirstMonth = salesGraphData.XLabel_FirstMonth;

                oSalesGraphDataDTO.YValue_SecondMonth = salesGraphData.YValue_SecondMonth;
                oSalesGraphDataDTO.XLabel_SecondMonth = salesGraphData.XLabel_SecondMonth;

                oSalesGraphDataDTO.YValue_ThirdMonth = salesGraphData.YValue_ThirdMonth;
                oSalesGraphDataDTO.XLabel_ThirdMonth = salesGraphData.XLabel_ThirdMonth;

                oSalesGraphDataDTO.TotalSales = salesGraphData.TotalSales;
                oSalesGraphDataDTO.SalesSummary = salesGraphData.SalesSummary;                
            }
            catch (Exception)
            {

                throw;
            }
            return oSalesGraphDataDTO;
        }
        #endregion

        #region - Properties -

        private brSales oBrSales = null;
       
        #endregion

        #region - Constructor -

        public SalesAdapter()
        {
            if (oBrSales == null)
            {
                oBrSales = new brSales();
            }
                
        }

        #endregion

        #region - Methods -

        public DebtorsBarometerDTO GetDebtorsDetails(string sCustCode)
        {
            try
            {
                return ConvertToDebtorsBarometerDTO(oBrSales.GetDebtorsDetails(sCustCode));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public SalesGraphDataDTO GetSalesDataForCompleteMonths(string sCustCode)
        {
            try
            {
                return ConvertToSalesGraphDataDTO(oBrSales.GetSalesDataForCompleteMonths(sCustCode));
            }
            catch (Exception)
            {

                throw;
            }
        }
        
        #endregion
         * */

        public int UploadSalesTargets(string FilePath, string Extension, string UserName)
        {
            try
            {
                return SalesBR.Instance.UploadSalesTargets(FilePath, Extension, UserName);

            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}
