﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.Entities;
using Peercore.CRM.Service.DTO;


namespace Peercore.CRM.Service.Common.Adapter
{
    public class LeadAddressAdapter
    {
        #region - Converters -

        public static LeadAddressEntity ConvertToLeadAddress(LeadAddressDTO leadAddressDto)
        {
            LeadAddressEntity oLeadAddress = LeadAddressEntity.CreateObject();    
            try
            {
                oLeadAddress.LeadAddressID = leadAddressDto.LeadAddressID;
                oLeadAddress.Address1 = leadAddressDto.Address1;
                oLeadAddress.Address2 = leadAddressDto.Address2;
                oLeadAddress.City = leadAddressDto.City;
                oLeadAddress.State = leadAddressDto.State;
                oLeadAddress.PostCode = leadAddressDto.PostCode;
                oLeadAddress.Country = leadAddressDto.Country;
                oLeadAddress.LeadID = leadAddressDto.LeadID;
                oLeadAddress.CreatedBy = leadAddressDto.CreatedBy;
                oLeadAddress.CreatedDate = leadAddressDto.CreatedDate;
                oLeadAddress.LastModifiedBy = leadAddressDto.LastModifiedBy;
                oLeadAddress.LastModifiedDate = leadAddressDto.LastModifiedDate;

                oLeadAddress.AssignedTo = leadAddressDto.AssignedTo;
                oLeadAddress.AssigneeNo = leadAddressDto.AssigneeNo;
                oLeadAddress.AddressType = leadAddressDto.AddressType;
                oLeadAddress.Name = leadAddressDto.Name;
                oLeadAddress.AreaCode = leadAddressDto.AreaCode;
                oLeadAddress.Telephone = leadAddressDto.Telephone;
                oLeadAddress.Fax = leadAddressDto.Fax;
                oLeadAddress.Contact = leadAddressDto.Contact;
                oLeadAddress.CustCode = leadAddressDto.CustCode;
            }
            catch (Exception)
            {

                throw;
            }
            return oLeadAddress;
        }

        public static LeadAddressDTO ConvertToLeadAddressDTO(LeadAddressEntity leadAddress)
        {
            LeadAddressDTO oLeadAddressDTO = new LeadAddressDTO();   
            try
            {
                oLeadAddressDTO.LeadAddressID = leadAddress.LeadAddressID;
                oLeadAddressDTO.Address1 = leadAddress.Address1;
                oLeadAddressDTO.Address2 = leadAddress.Address2;
                oLeadAddressDTO.City = leadAddress.City;
                oLeadAddressDTO.State = leadAddress.State;
                oLeadAddressDTO.PostCode = leadAddress.PostCode;
                oLeadAddressDTO.Country = leadAddress.Country;
                oLeadAddressDTO.LeadID = leadAddress.LeadID;
                oLeadAddressDTO.CreatedBy = leadAddress.CreatedBy;
                oLeadAddressDTO.CreatedDate = leadAddress.CreatedDate;
                oLeadAddressDTO.LastModifiedBy = leadAddress.LastModifiedBy;
                oLeadAddressDTO.LastModifiedDate = leadAddress.LastModifiedDate;

                oLeadAddressDTO.AssignedTo = leadAddress.AssignedTo;
                oLeadAddressDTO.AssigneeNo = leadAddress.AssigneeNo;
                oLeadAddressDTO.AddressTypeDescription = leadAddress.AddressTypeDescription;
                oLeadAddressDTO.AddressType = leadAddress.AddressType;
                //oLeadAddressDTO.AddressTypeDescription = leadAddress.AddressTypeDescription;
                oLeadAddressDTO.Name = leadAddress.Name;
                oLeadAddressDTO.AreaCode = leadAddress.AreaCode;
                oLeadAddressDTO.Telephone = leadAddress.Telephone;
                oLeadAddressDTO.Fax = leadAddress.Fax;
                oLeadAddressDTO.Contact = leadAddress.Contact;
                oLeadAddressDTO.CustCode = leadAddress.CustCode;
            }
            catch (Exception)
            {

                throw;
            }
            return oLeadAddressDTO;
        } 

        #endregion
        
        #region - Properties -
        
        //private brLeadAddress oBrLeadAddress = null;  

        #endregion

        #region - Constructor -

        public LeadAddressAdapter()
        {
            //if (oBrLeadAddress == null)
            //    oBrLeadAddress = new brLeadAddress();
        } 

        #endregion
        
        #region - Methods -

        public List<LeadAddressDTO> GetLeadAddresses(int leadId)
        {
            try
            {
                List<LeadAddressEntity> leadAddresses = LeadAddressBR.Instance.GetCustomerAddresses(leadId);
                List<LeadAddressDTO> leadAddressesDto = new List<LeadAddressDTO>();

                foreach (LeadAddressEntity item in leadAddresses)
                {
                    leadAddressesDto.Add(ConvertToLeadAddressDTO(item));
                }

                return leadAddressesDto;
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        public LeadAddressDTO GetLeadTableAddress(int leadId)
        {
            try
            {
                return ConvertToLeadAddressDTO(LeadAddressBR.Instance.GetLeadTableAddress(leadId));
            }
            catch
            {
                throw;
            }
        }

        public bool SaveLeadAddress(LeadAddressDTO leadAddressDto, ref int leadAddressId)
        {
            bool save = false;

            try
            {
                save = LeadAddressBR.Instance.Save(ConvertToLeadAddress(leadAddressDto), ref leadAddressId);
            }
            catch (Exception)
            {
                
                throw;
            }
            return save;
        }

        //public bool SaveLeadAndLeadAddress(LeadAddressDTO leadAddressDto,LeadDTO leadDto)
        //{
        //    bool save = false;

        //    try
        //    {
        //        if (oBrLeadAddress.Save(ConvertToLeadAddress(leadAddressDto), LeadAdapter.ConvertToLead(leadDto) ) > 0)
        //            save = true;
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //    return save;
        //}

        public bool TransferAddressToCustomer(List<LeadAddressDTO> customerAddressList)
        {
            try
            {
                List<LeadAddressEntity> leadAddressList = new List<LeadAddressEntity>();
                foreach (LeadAddressDTO item in customerAddressList)
                {
                    leadAddressList.Add(ConvertToLeadAddress(item));
                }

                return LeadAddressBR.Instance.TransferAddressToCustomer(leadAddressList);
            }
            catch
            {
                throw;
            }
        }



        //public bool DeleteLeadAddress(int leadAddressId)
        //{
        //    bool status = false;
        //    try
        //    {
        //        if (oBrLeadAddress.DeleteLeadAddress(leadAddressId) > 0)
        //            status = true;
        //    }
        //    catch (Exception)
        //    {
                
        //        throw;
        //    }
        //    return status;
        //}
        #endregion
    }
}
