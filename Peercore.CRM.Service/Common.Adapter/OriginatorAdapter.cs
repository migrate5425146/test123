﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.Entities;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class OriginatorAdapter
    {    

        #region - Converters -

        public static OriginatorDTO ConvertToOriginatorDTO(OriginatorEntity originator)
        {
            OriginatorDTO originatorDTO = new OriginatorDTO();

            try
            {
                try
                {
                    originatorDTO.ClientType = originator.ClientType;
                    originatorDTO.Email = originator.Email;
                    originatorDTO.Name = originator.Name;
                    originatorDTO.OriginatorId = originator.OriginatorId;
                    originatorDTO.UserId = originator.UserId;
                    originatorDTO.UserName = originator.UserName;
                    originatorDTO.CRMAuthLevel = originator.CRMAuthLevel;
                    originatorDTO.AuthorizationGroupId = originator.AuthorizationGroupId;
                    originatorDTO.CreatedBy = originator.CreatedBy;
                    originatorDTO.CreatedDate = originator.CreatedDate;
                    originatorDTO.HasChildReps = originator.HasChildReps;
                    originatorDTO.LastModifiedBy = originator.LastModifiedBy;
                    originatorDTO.LastModifiedDate = originator.LastModifiedDate;
                    originatorDTO.ManagerMode = originator.ManagerMode;
                    originatorDTO.RepCode = originator.RepCode;
                    originatorDTO.RepType = originator.RepType;
                    originatorDTO.DefaultDepartmentId = originator.DefaultDepartmentId;
                    originatorDTO.DeptString = originator.DeptString;
                    originatorDTO.AccessToken = originator.AccessToken;

                    originatorDTO.TFAPin = originator.TFAPin;
                    originatorDTO.TFAIsAuthenticated = originator.TFAIsAuthenticated;
                    originatorDTO.IsTFAUser = originator.IsTFAUser;

                    originatorDTO.RowCount = originator.TotalCount;
                }
                catch { }
            }
            catch (Exception)
            {
                
                throw;
            }
            return originatorDTO;
        }

        public static OriginatorEMEIDTO ConvertToOriginatorEMEIDTO(OriginatorEMEIEntity originator)
        {
            OriginatorEMEIDTO originatorDTO = new OriginatorEMEIDTO();

            try
            {
                originatorDTO.id = originator.id;
                originatorDTO.originator = originator.originator;
                originatorDTO.mobile_no  = originator.mobile_no;
                originatorDTO.emei_no = originator.emei_no;
            }
            catch (Exception)
            {

                throw;
            }
            return originatorDTO;
        }



        public static OriginatorEntity ConvertToDocument(OriginatorDTO originatoDto)
        {
            OriginatorEntity Originator = OriginatorEntity.CreateObject();

            try
            {
                Originator.AuthorizationGroupId = originatoDto.AuthorizationGroupId;
                Originator.ClientType = originatoDto.ClientType;
                Originator.CreatedBy = originatoDto.CreatedBy;
                Originator.CreatedDate = originatoDto.CreatedDate;
                Originator.CRMAuthLevel = originatoDto.CRMAuthLevel;
                Originator.Email = originatoDto.Email;
                Originator.HasChildReps = originatoDto.HasChildReps;
                Originator.LastModifiedBy = originatoDto.LastModifiedBy;
                Originator.LastModifiedDate = originatoDto.LastModifiedDate;
                Originator.ManagerMode = originatoDto.ManagerMode;
                Originator.Name = originatoDto.Name;
                Originator.OriginatorId = originatoDto.OriginatorId;
                Originator.RepCode = originatoDto.RepCode;
                Originator.RepType = originatoDto.RepType;
                Originator.UserId = originatoDto.UserId;
                Originator.UserName = originatoDto.UserName;
                Originator.DeptString = originatoDto.DeptString;

                Originator.TotalCount = originatoDto.RowCount;
            }
            catch (Exception)
            {

                throw;
            }
            return Originator;
        }

        private List<StateDTO> ConvertToStateDTO(List<StateEntity> list)
        {
            List<StateDTO> stateList = new List<StateDTO>();
            foreach (StateEntity item in list)
            {
                stateList.Add(new StateDTO()
                {
                    StateID = item.StateID,
                    StateName = item.StateName
                });
            }
            return stateList;
        }

        public static RepDTO ConvertToRepDTO(RepEntity rep)
        {
            RepDTO repDTO = new RepDTO();

            try
            {
                repDTO.RepCode = rep.RepCode;
                repDTO.RepName = rep.Name;
                repDTO.StickTarget = rep.StickTarget;
                repDTO.TargetYear = rep.TargetYear;
                repDTO.TargetMonth = rep.TargetMonth;
                repDTO.TargetMonthText = rep.TargetMonthText;

            }
            catch (Exception)
            {

                throw;
            }
            return repDTO;
        }

        #endregion

        #region - Properties -

        //private brOriginator oBrOriginator = null;

        #endregion

        #region - Constructor -

        public OriginatorAdapter()
        {
            //if (oBrOriginator == null)
            //{
            //    oBrOriginator = new brOriginator();
            //}

        }

        #endregion

        #region New Methods

        public List<OriginatorDTO> GetReps()
        {
            List<OriginatorDTO> lstOriginatorDTO = new List<OriginatorDTO>();
            /*
            try
            {
                List<OriginatorEntity> lstOriginator = originator.GetChildOriginatorsList(args.Originator, args.ManagerMode);
                foreach (Originator originator in lstOriginator)
                {
                    lstOriginatorDTO.Add(ConvertToOriginatorDTO(originator));
                }
            }
            catch (Exception)
            {

                throw;
            }*/
            return lstOriginatorDTO;
        }

        public string GetChildOriginators(string sOriginator, bool managerMode = false)
        {
            try
            {
                return OriginatorBR.Instance.GetChildOriginators(sOriginator, managerMode);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<string> GetChildReps(string originator, bool managerMood, ref string sWhereCls, string childOriginators = null)
        {
            try
            {
                return OriginatorBR.Instance.GetChildReps(originator, managerMood, ref sWhereCls, childOriginators);
            }
            catch
            {
                throw;
            }
        }

        public OriginatorDTO GetLoginDetails(string originator, string password)
        {
            OriginatorDTO OriginatorDTO = new OriginatorDTO();

            try
            {
                return ConvertToOriginatorDTO(OriginatorBR.Instance.GetLoginDetails(originator, password));               
            }
            catch (Exception)
            {

                throw;
            }
        }

        public OriginatorDTO GetOriginator(string originator)
        {
            try
            {
                OriginatorEntity ori = OriginatorBR.Instance.GetOriginator(originator);
                if(ori != null)
                    return ConvertToOriginatorDTO(ori);
                else
                    return null;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetRepCode(string originator)
        {
            try
            {
                return OriginatorBR.Instance.GetRepCode(originator);
            }
            catch
            {
                throw;
            }
        }

        public string GetBDMString()
        {
            try
            {
                return OriginatorBR.Instance.GetBDMString();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<KeyValuePair<string, string>> GetChildRepsList(string childOriginators)
        {
            try
            {
                return OriginatorBR.Instance.GetChildRepsList(childOriginators);
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorDTO> GetAllRepCodes(string sOriginator)
        {
            try
            {
                List<OriginatorDTO> originatorDtoList = new List<OriginatorDTO>();

                List<OriginatorEntity> originatorList = OriginatorBR.Instance.GetAllRepCodes(sOriginator);

                foreach (OriginatorEntity item in originatorList)
                {
                    originatorDtoList.Add(ConvertToOriginatorDTO(item));
                }

                return originatorDtoList;
            }
            catch
            {
                throw;
            }
        }

        public List<StateDTO> GetDistinctRepStates()
        {
            try
            {
                return ConvertToStateDTO(OriginatorBR.Instance.GetDistinctRepStates());
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorDTO> GetRepGroupOriginators(string originator)
        {
            try
            {
                List<OriginatorDTO> originatorDTOList = new List<OriginatorDTO>();
                List<OriginatorEntity> originatorList = OriginatorBR.Instance.GetRepGroupOriginators(originator);
                foreach (OriginatorEntity item in originatorList)
                {
                    originatorDTOList.Add(ConvertToOriginatorDTO(item));
                }
                return originatorDTOList;
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorEMEIDTO> GetAllOriginatorEMEINos(string originator)
        {
            try
            {
                List<OriginatorEMEIDTO> originatorDTOList = new List<OriginatorEMEIDTO>();
                List<OriginatorEMEIEntity> originatorList = OriginatorBR.Instance.GetAllOriginatorEMEINos(originator);
                foreach (OriginatorEMEIEntity item in originatorList)
                {
                    originatorDTOList.Add(ConvertToOriginatorEMEIDTO(item));
                }
                return originatorDTOList;
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorDTO> GetCRMReps()
        {
            try
            {
                List<OriginatorDTO> originatorDtoList = new List<OriginatorDTO>();
                List<OriginatorEntity> originatorList = OriginatorBR.Instance.GetCRMReps();
                foreach (OriginatorEntity item in originatorList)
                {
                    originatorDtoList.Add(ConvertToOriginatorDTO(item));
                }
                return originatorDtoList;
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorDTO> GetChildOriginatorsList(ArgsDTO args)
        {
            List<OriginatorDTO> lstOriginatorDTO = new List<OriginatorDTO>();

            try
            {
                List<OriginatorEntity> lstOriginator = OriginatorBR.Instance.GetChildOriginatorsList(ActivityAdapter.ConvertToArgsEntity(args));
                foreach (OriginatorEntity originator in lstOriginator)
                {
                    lstOriginatorDTO.Add(ConvertToOriginatorDTO(originator));
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstOriginatorDTO;
        }

        public List<OriginatorDTO> GetRepForKeyAccounts(string distributor)
        {
            List<OriginatorDTO> lstOriginatorDTO = new List<OriginatorDTO>();

            try
            {
                List<OriginatorEntity> lstOriginator = OriginatorBR.Instance.GetRepForKeyAccounts(distributor);
                foreach (OriginatorEntity originator in lstOriginator)
                {
                    lstOriginatorDTO.Add(ConvertToOriginatorDTO(originator));
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstOriginatorDTO;
        }
        #endregion

        #region - Methods -
        /*

        public List<OriginatorDTO> GetFilterChildOriginatorsList(string filterByUserName, ArgsDTO args)
        {
            List<OriginatorDTO> lstOriginatorDTO = new List<OriginatorDTO>();

            try
            {
                List<Originator> lstOriginator = oBrOriginator.GetChildOriginatorsList(filterByUserName, args.ManagerMode);
                foreach (Originator originator in lstOriginator)
                {
                    lstOriginatorDTO.Add(ConvertToOriginatorDTO(originator));
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstOriginatorDTO;
        }
        */
        #endregion


        //Added on 24/01/2014
        public List<OriginatorDTO> GetOriginatorsByCatergory(ArgsDTO args)
        {
            List<OriginatorDTO> lstOriginatorDTO = new List<OriginatorDTO>();

            try
            {
                List<OriginatorEntity> lstOriginator = OriginatorBR.Instance.GetOriginatorsByCatergory(ActivityAdapter.ConvertToArgsEntity(args));
                foreach (OriginatorEntity originator in lstOriginator)
                {
                    lstOriginatorDTO.Add(ConvertToOriginatorDTO(originator));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstOriginatorDTO;
        }
        
        public List<OriginatorDTO> GetDistributersByASMOriginator(ArgsDTO args)
        {
            List<OriginatorDTO> lstOriginatorDTO = new List<OriginatorDTO>();

            try
            {
                List<OriginatorEntity> lstOriginator = OriginatorBR.Instance.GetDistributersByASMOriginator(ActivityAdapter.ConvertToArgsEntity(args));
                foreach (OriginatorEntity originator in lstOriginator)
                {
                    lstOriginatorDTO.Add(ConvertToOriginatorDTO(originator));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstOriginatorDTO;
        }
        
        public List<OriginatorDTO> GetAllSRByOriginator(ArgsDTO args)
        {
            List<OriginatorDTO> lstOriginatorDTO = new List<OriginatorDTO>();

            try
            {
                List<OriginatorEntity> lstOriginator = OriginatorBR.Instance.GetAllSRByOriginator(ActivityAdapter.ConvertToArgsEntity(args));
                foreach (OriginatorEntity originator in lstOriginator)
                {
                    lstOriginatorDTO.Add(ConvertToOriginatorDTO(originator));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstOriginatorDTO;
        }

        public List<OriginatorDTO> GetAllDistributorList(ArgsDTO args)
        {
            List<OriginatorDTO> lstOriginatorDTO = new List<OriginatorDTO>();

            try
            {
                List<OriginatorEntity> lstOriginator = OriginatorBR.Instance.GetAllDistributorList(ActivityAdapter.ConvertToArgsEntity(args));
                foreach (OriginatorEntity originator in lstOriginator)
                {
                    lstOriginatorDTO.Add(ConvertToOriginatorDTO(originator));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstOriginatorDTO;
        }
        
        public List<OriginatorDTO> GetAllOriginatorsByCatergory(ArgsDTO args)
        {
            List<OriginatorDTO> lstOriginatorDTO = new List<OriginatorDTO>();

            try
            {
                List<OriginatorEntity> lstOriginator = OriginatorBR.Instance.GetAllOriginatorsByCatergory(ActivityAdapter.ConvertToArgsEntity(args));
                foreach (OriginatorEntity originator in lstOriginator)
                {
                    lstOriginatorDTO.Add(ConvertToOriginatorDTO(originator));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstOriginatorDTO;
        }

        public List<OriginatorDTO> GetASEsByDateForReport(ArgsDTO args, DateTime startDate, DateTime endDate)
        {
            List<OriginatorDTO> lstOriginatorDTO = new List<OriginatorDTO>();

            try
            {
                List<OriginatorEntity> lstOriginator = OriginatorBR.Instance.GetASEsByDateForReport(ActivityAdapter.ConvertToArgsEntity(args), startDate, endDate);

                foreach (OriginatorEntity originator in lstOriginator)
                {
                    lstOriginatorDTO.Add(ConvertToOriginatorDTO(originator));
                }
            }
            catch (Exception)
            {
                throw;
            }

            return lstOriginatorDTO;
        }

        public List<OriginatorDTO> GetDistributorsByCatergoryWithNameAndCode(ArgsDTO args)
        {
            List<OriginatorDTO> lstOriginatorDTO = new List<OriginatorDTO>();

            try
            {
                List<OriginatorEntity> lstOriginator = OriginatorBR.Instance.GetDistributorsByCatergoryWithNameAndCode(ActivityAdapter.ConvertToArgsEntity(args));
                foreach (OriginatorEntity originator in lstOriginator)
                {
                    lstOriginatorDTO.Add(ConvertToOriginatorDTO(originator));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstOriginatorDTO;
        }

        public List<OriginatorDTO> GetRepsByOriginator_For_Route_Assign(ArgsDTO args)
        {
            List<OriginatorDTO> lstOriginatorDTO = new List<OriginatorDTO>();

            try
            {
                List<OriginatorEntity> lstOriginator = OriginatorBR.Instance.GetRepsByOriginator_For_Route_Assign(ActivityAdapter.ConvertToArgsEntity(args));
                foreach (OriginatorEntity originator in lstOriginator)
                {
                    lstOriginatorDTO.Add(ConvertToOriginatorDTO(originator));
                }
            }
            catch (Exception)
            {
                throw;
            }

            return lstOriginatorDTO;
        }

        public List<OriginatorDTO> GetRepsByOriginator(string rep, string username)
        {
            List<OriginatorDTO> lstOriginatorDTO = new List<OriginatorDTO>();

            try
            {
                List<OriginatorEntity> lstOriginator = OriginatorBR.Instance.GetRepsByOriginator(rep, username);
                foreach (OriginatorEntity ooriginator in lstOriginator)
                {
                    lstOriginatorDTO.Add(ConvertToOriginatorDTO(ooriginator));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstOriginatorDTO;
        }

        public List<OriginatorDTO> GetRepsByASEOriginator(string ASE)
        {
            List<OriginatorDTO> lstOriginatorDTO = new List<OriginatorDTO>();

            try
            {
                List<OriginatorEntity> lstOriginator = OriginatorBR.Instance.GetRepsByASEOriginator(ASE);
                foreach (OriginatorEntity ooriginator in lstOriginator)
                {
                    lstOriginatorDTO.Add(ConvertToOriginatorDTO(ooriginator));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstOriginatorDTO;
        }

        public List<OriginatorDTO> GetDistributorsByLikeName(string name)
        {
            List<OriginatorDTO> lstOriginatorDTO = new List<OriginatorDTO>();

            try
            {
                List<OriginatorEntity> lstOriginator = OriginatorBR.Instance.GetDistributorsByLikeName(name);
                foreach (OriginatorEntity ooriginator in lstOriginator)
                {
                    lstOriginatorDTO.Add(ConvertToOriginatorDTO(ooriginator));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstOriginatorDTO;
        }

        //public List<RepDTO> GetRepTargetsByDistributor(string username)
        //{
        //    List<RepDTO> reprDTOlist = new List<RepDTO>();

        //    try
        //    {
        //        List<RepEntity> lstrep = OriginatorBR.Instance.GetRepTargetsByDistributor(username);
        //        foreach (RepEntity rep in lstrep)
        //        {
        //            reprDTOlist.Add(ConvertToRepDTO(rep));
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    return reprDTOlist;
        //}        

        public SalesInfoDetailViewStateDTO GetRepMonthleyTargetsByDistributor(SalesInfoDetailSearchCriteriaDTO busSalesEnqSrc, ArgsDTO args)
        {
            List<TargetInfoDTO> reprDTOlist = new List<TargetInfoDTO>();
            SalesInfoDetailViewStateDTO salesInfo = new SalesInfoDetailViewStateDTO();
            try
            {
                SalesInfoDetailViewStateEntity lstrep = OriginatorBR.Instance.GetRepMonthleyTargetsByDistributor(
                    SalesAdapter.ConvertToSalesInfoDetailSearchCriteria(busSalesEnqSrc), ActivityAdapter.ConvertToArgsEntity(args));


                salesInfo = SalesAdapter.ConvertToSalesInfoDetailViewStateDTO(lstrep);

                salesInfo.TargetInfoEntityList = new List<TargetInfoDTO>();

                foreach (TargetInfoEntity rep in lstrep.TargetInfoEntityList)
                {
                    salesInfo.TargetInfoEntityList.Add(CustomerAdapter.ConvertToTargetInfoDTO(rep));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return salesInfo;
        }

        public SalesInfoDetailViewStateDTO GetTodaySalesDrilldown(SalesInfoDetailSearchCriteriaDTO busSalesEnqSrc, ArgsDTO args)
        {
            List<TargetInfoDTO> reprDTOlist = new List<TargetInfoDTO>();
            SalesInfoDetailViewStateDTO salesInfo = new SalesInfoDetailViewStateDTO();
            try
            {
                SalesInfoDetailViewStateEntity lstrep = OriginatorBR.Instance.GetTodaySalesDrilldown(
                    SalesAdapter.ConvertToSalesInfoDetailSearchCriteria(busSalesEnqSrc), ActivityAdapter.ConvertToArgsEntity(args));


                salesInfo = SalesAdapter.ConvertToSalesInfoDetailViewStateDTO(lstrep);

                salesInfo.TargetInfoEntityList = new List<TargetInfoDTO>();

                foreach (TargetInfoEntity rep in lstrep.TargetInfoEntityList)
                {
                    salesInfo.TargetInfoEntityList.Add(CustomerAdapter.ConvertToTargetInfoDTO(rep));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return salesInfo;
        }

        public List<OriginatorDTO> GetParentOriginators(string sOriginator)
        {
            try
            {
                List<OriginatorEntity> entityList = new List<OriginatorEntity>();
                List<OriginatorDTO> dtoList = new List<OriginatorDTO>();
                entityList = OriginatorBR.Instance.GetParentOriginatorsList(sOriginator);
                if (entityList.Count > 0)
                {
                    foreach (OriginatorEntity item in entityList)
                    {
                        dtoList.Add(ConvertToOriginatorDTO(item));
                    }
                }

                return dtoList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<OriginatorDTO> GetLevelThreeParentOriginators(string sOriginator)
        {
            try
            {
                List<OriginatorEntity> entityList = new List<OriginatorEntity>();
                List<OriginatorDTO> dtoList = new List<OriginatorDTO>();
                entityList = OriginatorBR.Instance.GetLevelThreeParentOriginators(sOriginator);
                if (entityList.Count > 0)
                {
                    foreach (OriginatorEntity item in entityList)
                    {
                        dtoList.Add(ConvertToOriginatorDTO(item));
                    }
                }

                return dtoList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetUserNameByRepCode(string sRepCode)
        {
            try
            {
                return OriginatorBR.Instance.GetUserName(sRepCode);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdatePassword(string originator, string password)
        {
            try
            {
                return OriginatorBR.Instance.UpdatePassword(originator, password);
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateMobilePin()
        {
            try
            {
                return OriginatorBR.Instance.UpdateMobilePin();
            }
            catch
            {
                throw;
            }
        }
    }
}
