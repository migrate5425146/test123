﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO.CompositeEntities;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Entities;
using Peercore.CRM.BusinessRules;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class LeadAdapter
    {
        #region - Converters -

        public static LeadEntity ConvertToLead(LeadDTO leadDto)
        {
            LeadEntity oLead = LeadEntity.CreateObject();
            try
            {
                oLead.Address               = leadDto.Address;
                oLead.AnnualRevenue         = leadDto.AnnualRevenue;
                oLead.Business              = leadDto.Business;
                oLead.BusinessPotential     = leadDto.BusinessPotential;
                oLead.City                  = leadDto.City;
                oLead.Company               = leadDto.Company;
                oLead.Country               = leadDto.Country;
                oLead.CreatedBy             = leadDto.CreatedBy;           
                oLead.CreatedDate           = leadDto.CreatedDate;         
                oLead.CustCode              = leadDto.CustCode;            
                oLead.DelFalg               = leadDto.DelFalg;             
                oLead.Description           = leadDto.Description;         
                oLead.EmailAddress          = leadDto.EmailAddress;        
                oLead.Fax                   = leadDto.Fax;                 
                oLead.Industry              = leadDto.Industry;            
                oLead.LastCalledDate        = leadDto.LastCalledDate;      
                oLead.LastModifiedBy        = leadDto.LastModifiedBy;      
                oLead.LastModifiedDate      = leadDto.LastModifiedDate;    
                oLead.LeadID                = leadDto.LeadID;              
                oLead.LeadName              = leadDto.LeadName;            
                oLead.LeadSource            = leadDto.LeadSource;          
                oLead.LeadStageID           = leadDto.LeadStageID;         
                oLead.LeadStatus            = leadDto.LeadStatus;          
                oLead.LitersBy              = leadDto.LitersBy;            
                oLead.Mobile                = leadDto.Mobile;              
                oLead.NoOfEmployees         = leadDto.NoOfEmployees;       
                oLead.Originator            = leadDto.Originator;          
                oLead.PostCode              = leadDto.PostCode;            
                oLead.PotentialLiters       = leadDto.PotentialLiters;     
                oLead.PreferredContact      = leadDto.PreferredContact;    
                //oLead.PreviousCustomerCode  = leadDto.PreviousCustomerCode;
                oLead.Probability           = leadDto.Probability;         
                oLead.Rating                = leadDto.Rating;              
                oLead.ReferredBy            = leadDto.ReferredBy;          
                oLead.RepGroupID            = leadDto.RepGroupID;          
                oLead.RepGroupName          = leadDto.RepGroupName;        
                //oLead.StartDate             = leadDto.StartDate;           
                oLead.State                 = leadDto.State;               
                oLead.Telephone             = leadDto.Telephone;
                oLead.Website               = leadDto.Website;             
            }
            catch (Exception)
            {

                throw;
            }
            return oLead;
        }

        public static LeadDTO ConvertToLeadDTO(LeadEntity lead)
        {
            LeadDTO oLeadDTO = new LeadDTO();
            try
            {
                oLeadDTO.Address = lead.Address;
                oLeadDTO.AnnualRevenue = lead.AnnualRevenue;
                oLeadDTO.Business = lead.Business;
                oLeadDTO.BusinessPotential = lead.BusinessPotential;
                oLeadDTO.City = lead.City;
                oLeadDTO.Company = lead.Company;
                oLeadDTO.Country = lead.Country;
                oLeadDTO.CreatedBy = lead.CreatedBy;
                oLeadDTO.CreatedDate = lead.CreatedDate.Value;
                oLeadDTO.CustCode = lead.CustCode;
                oLeadDTO.DelFalg = lead.DelFalg;
                oLeadDTO.Description = lead.Description;
                oLeadDTO.EmailAddress = lead.EmailAddress;
                oLeadDTO.Fax = lead.Fax;
                oLeadDTO.Industry = lead.Industry;
                oLeadDTO.LastCalledDate = lead.LastCalledDate;
                oLeadDTO.LastModifiedBy = lead.LastModifiedBy;
                oLeadDTO.LastModifiedDate = lead.LastModifiedDate.Value;
                oLeadDTO.LeadID = lead.LeadID;
                oLeadDTO.LeadName = lead.LeadName;
                oLeadDTO.LeadSource = lead.LeadSource;
                oLeadDTO.LeadStageID = lead.LeadStageID;
                oLeadDTO.LeadStatus = lead.LeadStatus;
                oLeadDTO.LitersBy = lead.LitersBy;
                oLeadDTO.Mobile = lead.Mobile;
                oLeadDTO.NoOfEmployees = lead.NoOfEmployees;
                oLeadDTO.Originator = lead.Originator;
                oLeadDTO.PostCode = lead.PostCode;
                oLeadDTO.PotentialLiters = lead.PotentialLiters;
                oLeadDTO.PreferredContact = lead.PreferredContact;
                //oLeadDTO.PreviousCustomerCode = lead.PreviousCustomerCode;
                oLeadDTO.Probability = lead.Probability;
                oLeadDTO.Rating = lead.Rating;
                oLeadDTO.ReferredBy = lead.ReferredBy;
                oLeadDTO.RepGroupID = lead.RepGroupID;
                oLeadDTO.RepGroupName = lead.RepGroupName;
                //oLeadDTO.StartDate = lead.StartDate;
                oLeadDTO.State = lead.State;
                oLeadDTO.Telephone = lead.Telephone;
                oLeadDTO.Website = lead.Website;             
            }
            catch (Exception)
            {

                throw;
            }
            return oLeadDTO;
        }

        public static CheckListDTO ConvertToCheckListDTO(ChecklistEntity checklist)
        {
            CheckListDTO oCheckListDTO = new CheckListDTO();
            try
            {
                oCheckListDTO.ChecklistID = checklist.ChecklistID;
                oCheckListDTO.ChecklistName = checklist.ChecklistName;
                //oCheckListDTO.IsChecked = checklist.IsChecked;
                //oCheckListDTO.Mandatory = checklist.Mandatory;
                oCheckListDTO.PipelineStageID = checklist.PipelineStageID;
                oCheckListDTO.LeadId = checklist.LeadID;
            }
            catch (Exception)
            {

                throw;
            }
            return oCheckListDTO;
        }

        public static ChecklistImageDTO ConvertToCheckListImageDTO(ChecklistImageEntity checklist)
        {
            ChecklistImageDTO oCheckListDTO = new ChecklistImageDTO();
            try
            {
                oCheckListDTO.ChecklistId = checklist.ChecklistId;
                oCheckListDTO.ImageContent = checklist.ImageContent;
                oCheckListDTO.ImageName = checklist.ImageName;
            }
            catch (Exception)
            {

                throw;
            }
            return oCheckListDTO;
        }

        public static List<ChecklistEntity> ConvertToCheckListDTOToEntityList(List<CheckListDTO> checkdtolist)
        {
            List<ChecklistEntity> lstChecklistEntity = new List<ChecklistEntity>();
            ChecklistEntity checklistEntity = null;
            try
            {
                foreach (CheckListDTO obj in checkdtolist)
                {
                    checklistEntity = ChecklistBR.Instance.CreateObject();
                    checklistEntity.LeadID = obj.LeadId;
                    checklistEntity.PipelineStageID = obj.PipelineStageID;

                    lstChecklistEntity.Add(checklistEntity);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstChecklistEntity;
        }

        #endregion

        #region - Properties -
        
        //private brLead oBrLead = null;
        //private brChecklist brChecklist = null;

        #endregion

        #region - Constructor -
        
        public LeadAdapter()
        {
            //if (oBrLead == null)
            //    oBrLead = new brLead();

            //if (brChecklist == null)
            //    brChecklist = new brChecklist();
        } 

        #endregion

        #region Lead

        public bool Save(LeadDTO leadDto, ArgsDTO args, out int leadid)
        {
            try
            {
                return LeadBR.Instance.Save(ConvertToLead(leadDto), ActivityAdapter.ConvertToArgsEntity(args), out leadid);
            }
            catch
            {
                throw;
            }
        }

        public LeadDTO GetLead(string sLeadID)
        {
            try
            {
                return ConvertToLeadDTO(LeadBR.Instance.GetLead(sLeadID));
            }
            catch
            {
                throw;
            }
        }

        public string UpdatePipelineStageQuery(int iLeadID, int iPipelineStageID)
        {
            try
            {
                return LeadBR.Instance.UpdatePipelineStageQuery(iLeadID, iPipelineStageID);
            }
            catch
            {
                throw;
            }

        }

        public int GetTotalCustomerCount(ArgsDTO args)
        {
            try
            {
                return LeadBR.Instance.GetTotalCustomerCount(ActivityAdapter.ConvertToArgsEntity(args));
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteLead(LeadDTO leadDto, ActivityDTO activityDto)
        {
            bool status = false;

            try
            {
                status = LeadBR.Instance.Delete(ConvertToLead(leadDto), ActivityAdapter.ConvertToActivity(activityDto));
            }
            catch
            {
                throw;
            }

            return status;
        }

        public bool ConvertLeadToCustomer(List<string> sSqls)
        {
            try
            {
                return LeadBR.Instance.ConvertLeadToCustomer(sSqls);
            }
            catch
            {
                throw;
            }
        }

        public bool ActivateLead(int leadId, ActivityDTO activity)
        {
            bool status = false;
            try
            {
                status = LeadBR.Instance.Activate(leadId, ActivityAdapter.ConvertToActivity(activity));
            }
            catch
            {
                throw;
            }
            return status;
        }

        public List<LeadDTO> GetLeadMiniViewCollection(ArgsDTO args)
        {
            List<LeadDTO> leadList = new List<DTO.LeadDTO>();
            List<LeadEntity> leadEntityList = LeadBR.Instance.GetLeadMiniViewCollection(ActivityAdapter.ConvertToArgsEntity(args));
            foreach (LeadEntity item in leadEntityList)
            {
                leadList.Add(ConvertToLeadDTO(item));
            }

            return leadList;
        }

        #endregion

        #region - Methods -
        //public int Convert(LeadDTO lead)
        //{
        //    try
        //    {
        //        return oBrLead.Convert(ConvertToLead(lead));
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //public int UpdatePipelineStage(int iLeadID, int iPipelineStageID)
        //{
        //    try
        //    {
        //        return oBrLead.UpdatePipelineStage(iLeadID, iPipelineStageID);
        //    }
        //    catch
        //    {
        //        throw;
        //    }

        //}

        #region - Contact Person Image -
        public ContactPersonImageDTO GetContactPersonImage(int contactPersonId, int leadId, string directoryPath, string sessionId)
        {
            try
            {
                return CommonAdapter.ConvertToContactPersonDTO(ContactPersonImageBR.Instance.GetContactPersonImage(contactPersonId, leadId), directoryPath, sessionId);
            }

            catch (Exception)
            {

                throw;
            }
        }
        #endregion
        #endregion

        //#region CheckList
        //public bool Save(List<CheckListDTO> checklist)
        //{
        //    try
        //    {
        //        return ChecklistBR.Instance.Save(ConvertToCheckListDTOToEntityList(checklist));
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //}

        //public List<CheckListDTO> GetChecklist(int iLeadStageID)
        //{
        //    List<CheckListDTO> lstCheckListDTO = new List<CheckListDTO>();

        //    try
        //    {
        //        List<ChecklistEntity> checklistEntityList = ChecklistBR.Instance.GetChecklist(iLeadStageID);
        //        foreach (ChecklistEntity obj in checklistEntityList)
        //        {
        //            lstCheckListDTO.Add(ConvertToCheckListDTO(obj));
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    return lstCheckListDTO;
        //}
        //#endregion
    }
}
