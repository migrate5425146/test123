﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.DTO.CompositeEntities;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.Entities;
using Peercore.CRM.Model;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class ActivityAdapter
    {
        #region - Converters -

        public static ActivityEntity ConvertToActivity(ActivityDTO activityDto)
        {
            ActivityEntity oActivity = ActivityEntity.CreateObject();
            try
            {
                //oActivity.ActivityCount = activityDto.ActivityCount;
                oActivity.ActivityID = activityDto.ActivityID;
                oActivity.ActivityType = activityDto.ActivityType;
                oActivity.ActivityTypeDescription = activityDto.ActivityTypeDescription;
                oActivity.AppointmentID = activityDto.AppointmentID;
                oActivity.AssignedTo = activityDto.AssignedTo;
                oActivity.ChecklistID = activityDto.ChecklistID;
                oActivity.Comments = activityDto.Comments;
                oActivity.CreatedBy = activityDto.CreatedBy;
                oActivity.CreatedDate = activityDto.CreatedDate;
                oActivity.CustomerCode = activityDto.CustomerCode;
                //oActivity.EndDate = activityDto.EndDate.ToLocalTime();
                oActivity.EndDate = activityDto.EndDate;
                oActivity.HasChecklist = activityDto.HasChecklist;
                oActivity.IsServiceCall = activityDto.IsServiceCall;
                oActivity.LastModifiedBy = activityDto.LastModifiedBy;
                oActivity.LastModifiedDate = activityDto.LastModifiedDate;
                oActivity.LeadID = activityDto.LeadID;
                oActivity.LeadStageID = activityDto.LeadStageID;
                oActivity.ParentActivityID = activityDto.ParentActivityID;
                oActivity.Priority = activityDto.Priority;
                oActivity.PriorityDescription = activityDto.PriorityDescription;
                oActivity.ReminderCreated = activityDto.ReminderCreated;
                oActivity.ReminderDate = activityDto.ReminderDate==null ? DateTime.Now : activityDto.ReminderDate.Value;
                oActivity.RepGroupID = activityDto.RepGroupID;
                oActivity.RepGroupName = activityDto.RepGroupName;
                oActivity.SendReminder = activityDto.SendReminder;
                oActivity.SentMail = activityDto.SentMail;
                //oActivity.StartDate = activityDto.StartDate.ToLocalTime();
                oActivity.StartDate = activityDto.StartDate;
                oActivity.Status = activityDto.Status;
                oActivity.StatusDescription = activityDto.StatusDescription;
                oActivity.Subject = activityDto.Subject;
                oActivity.TotalCount = activityDto.RowCount;
                oActivity.EndUserCode = activityDto.EndUserCode;
                oActivity.CallCycleID = activityDto.CallCycleID;
                oActivity.RepCode = activityDto.RepCode;
                oActivity.RouteID = activityDto.RouteID;
            }
            catch (Exception)
            {

                throw;
            }
            return oActivity;
        }

        public static ActivityDTO ConvertToActivityDTO(ActivityEntity activity)
        {
            ActivityDTO oActivityDto = new ActivityDTO();

            if (activity != null)
            {

                try
                {
                    //oActivityDto.ActivityCount = activity.ActivityCount;
                    oActivityDto.ActivityID = activity.ActivityID;
                    oActivityDto.ActivityType = activity.ActivityType;
                    oActivityDto.ActivityTypeDescription = activity.ActivityTypeDescription;
                    oActivityDto.AppointmentID = activity.AppointmentID;
                    oActivityDto.AssignedTo = activity.AssignedTo;
                    oActivityDto.ChecklistID = activity.ChecklistID;
                    oActivityDto.Comments = activity.Comments;
                    oActivityDto.CreatedBy = activity.CreatedBy;
                    oActivityDto.CreatedDate = activity.CreatedDate;
                    oActivityDto.CustomerCode = activity.CustomerCode;
                    oActivityDto.EndDate = activity.EndDate.ToLocalTime();
                    oActivityDto.HasChecklist = activity.HasChecklist;
                    oActivityDto.IsServiceCall = activity.IsServiceCall;
                    oActivityDto.LastModifiedBy = activity.LastModifiedBy;
                    oActivityDto.LastModifiedDate = activity.LastModifiedDate;
                    oActivityDto.LeadID = activity.LeadID;
                    oActivityDto.LeadStageID = activity.LeadStageID;
                    oActivityDto.ParentActivityID = activity.ParentActivityID;
                    oActivityDto.Priority = activity.Priority;
                    oActivityDto.PriorityDescription = activity.PriorityDescription;
                    oActivityDto.ReminderCreated = activity.ReminderCreated;
                    oActivityDto.ReminderDate = activity.ReminderDate.ToLocalTime();
                    oActivityDto.RepGroupID = activity.RepGroupID;
                    oActivityDto.RepGroupName = activity.RepGroupName;
                    oActivityDto.SendReminder = activity.SendReminder;
                    oActivityDto.SentMail = activity.SentMail;
                    oActivityDto.StartDate = activity.StartDate.ToLocalTime();
                    oActivityDto.Status = activity.Status;
                    oActivityDto.StatusDescription = activity.StatusDescription;
                    oActivityDto.Subject = activity.Subject;
                    oActivityDto.RowCount = activity.TotalCount;
                    oActivityDto.CallCycleID = activity.CallCycleID;
                    oActivityDto.RepCode = activity.RepCode;
                    oActivityDto.RouteID = activity.RouteID;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return oActivityDto;
        }        

        public static ActivityDTO ConvertToActivityTypeDTO(ActivityTypeEntity activityType)
        {
            ActivityDTO ActivityDTO = new ActivityDTO();

            try
            {
                ActivityDTO.Code = activityType.Code;
                ActivityDTO.Description = activityType.Description;
                ActivityDTO.EmailAddresses = activityType.EmailAddresses;
                ActivityDTO.EmailTemplate = activityType.EmailTemplate;
                ActivityDTO.ShowInPlanner = activityType.ShowInPlanner;
                ActivityDTO.ActivityTypeColour = activityType.ActivityTypeColour;
                ActivityDTO.ShowInPlannerChecked = activityType.ShowInPlannerChecked;
            }
            catch (Exception)
            {

                throw;
            }
            return ActivityDTO;
        }

        public static List<CustomerActivitiesDTO> ConvertToActivities(List<CustomerActivityEntity> cookersCustomerActivities)
        {
            List<CustomerActivitiesDTO> lstCustomerActivities = new List<CustomerActivitiesDTO>();
            CustomerActivitiesDTO CustomerActivities = null;

            try
            {
                foreach (CustomerActivityEntity obj in cookersCustomerActivities)
                {
                    CustomerActivities = new CustomerActivitiesDTO();
                    CustomerActivities.ActivityID = obj.ActivityID;
                    CustomerActivities.ActivityType = obj.ActivityType;
                    CustomerActivities.AppointmentId = obj.AppointmentId;
                    CustomerActivities.AssignedTo = obj.AssignedTo;
                    CustomerActivities.Comments = obj.Comments;
                    CustomerActivities.CreatedBy = obj.CreatedBy;
                    CustomerActivities.CreatedDate = obj.CreatedDate.Value;
                    CustomerActivities.CustCode = obj.CustCode;
                    //CustomerActivities.CustomerCity = obj.CustomerCity;
                    //CustomerActivities.CustomerPostCode = obj.CustomerPostCode;
                    CustomerActivities.DelFlag = obj.DelFlag;
                    CustomerActivities.EndDate = obj.EndDate;
                    CustomerActivities.LastModifiedBy = obj.LastModifiedBy;
                    CustomerActivities.LastModifiedDate = obj.LastModifiedDate.Value;
                    CustomerActivities.LeadId = obj.LeadId;
                    CustomerActivities.LeadName = obj.LeadName;
                    CustomerActivities.LeadStage = obj.LeadStage;
                    CustomerActivities.NoOfActivities = obj.NoOfActivities;
                    //CustomerActivities.OriginatorName = obj.OriginatorName;
                    CustomerActivities.PipelineStageID = obj.PipelineStageID;
                    CustomerActivities.Priority = obj.Priority;
                    CustomerActivities.ReminderDate = obj.ReminderDate;
                    CustomerActivities.SendReminder = obj.SendReminder;
                    CustomerActivities.SentMail = obj.SentMail;
                    CustomerActivities.StartDate = obj.StartDate;
                    CustomerActivities.Status = obj.Status;
                    CustomerActivities.StatusDesc = obj.StatusDesc;
                    CustomerActivities.Subject = obj.Subject;
                    //CustomerActivities.TypeColour = obj.TypeColour;
                    CustomerActivities.TypeDesc = obj.TypeDesc;
                    lstCustomerActivities.Add(CustomerActivities);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstCustomerActivities;
        }

        public static CustomerActivitiesDTO ConvertToCustomerActivity(CustomerActivityEntity obj)
        {
            CustomerActivitiesDTO CustomerActivities = new CustomerActivitiesDTO();

            try
            {
                CustomerActivities.ActivityID = obj.ActivityID;
                CustomerActivities.ActivityType = obj.ActivityType;
                CustomerActivities.AppointmentId = obj.AppointmentId;
                CustomerActivities.AssignedTo = obj.AssignedTo;
                CustomerActivities.Comments = obj.Comments;
                CustomerActivities.CreatedBy = obj.CreatedBy;
                CustomerActivities.CreatedDate = obj.CreatedDate.Value;
                CustomerActivities.CustCode = obj.CustCode;
                //CustomerActivities.CustomerCity = obj.CustomerCity;
                //CustomerActivities.CustomerPostCode = obj.CustomerPostCode;
                CustomerActivities.DelFlag = obj.DelFlag;
                CustomerActivities.EndDate = obj.EndDate.ToLocalTime();
                CustomerActivities.LastModifiedBy = obj.LastModifiedBy;
                CustomerActivities.LastModifiedDate = obj.LastModifiedDate.Value;
                CustomerActivities.LeadId = obj.LeadId;
                CustomerActivities.LeadName = obj.LeadName;
                CustomerActivities.LeadStage = obj.LeadStage;
                CustomerActivities.NoOfActivities = obj.NoOfActivities;
                //CustomerActivities.OriginatorName = obj.OriginatorName;
                CustomerActivities.PipelineStageID = obj.PipelineStageID;
                CustomerActivities.Priority = obj.Priority;
                CustomerActivities.ReminderDate = obj.ReminderDate.ToLocalTime();
                CustomerActivities.SendReminder = obj.SendReminder;
                CustomerActivities.SentMail = obj.SentMail;
                CustomerActivities.StartDate = obj.StartDate.ToLocalTime();
                CustomerActivities.Status = obj.Status;
                CustomerActivities.StatusDesc = obj.StatusDesc;
                CustomerActivities.Subject = obj.Subject;
                //CustomerActivities.TypeColour = obj.TypeColour;
                CustomerActivities.TypeDesc = obj.TypeDesc;
                CustomerActivities.EndUserCode = obj.EndUserCode;
                CustomerActivities.RowCount = obj.TotalCount;
                CustomerActivities.RepName = obj.RepName;
                CustomerActivities.RepCode = obj.RepCode;
                CustomerActivities.routeId = obj.routeId;

            }
            catch (Exception ex)
            {

                throw;
            }

            return CustomerActivities;
        }


        public static ArgsEntity ConvertToArgsEntity(ArgsDTO argsDto)
        {
            ArgsEntity args = new ArgsEntity();
            try
            {
                args = new ArgsEntity()
                {
                    ActiveInactiveChecked = argsDto.ActiveInactiveChecked,
                    ActivityStatus= argsDto.ActivityStatus,
                    AdditionalParams = argsDto.AdditionalParams,
                    AddressType =argsDto.AddressType,
                    ChildOriginators = argsDto.ChildOriginators,
                    CustomerCode =argsDto.CustomerCode,
                    DefaultDepartmentId = argsDto.DefaultDepartmentId,
                    DtEndDate =argsDto.DtEndDate,
                    DtStartDate = argsDto.DtStartDate,
                    EnduserCode = argsDto.EnduserCode,
                    Floor = argsDto.Floor,
                    IsIncludeContact =argsDto.IsIncludeContact,
                    IsIncludeCustomer =argsDto.IsIncludeCustomer,
                    IsIncludeLead = argsDto.IsIncludeLead,
                    IsOpportunityAutoGenerated = argsDto.IsOpportunityAutoGenerated,
                    LeadId = argsDto.LeadId,
                    LeadStage= argsDto.LeadStage,
                    LeadStageId = argsDto.LeadStageId,
                    LeadStageName =argsDto.LeadStageName,
                    ManagerMode = argsDto.ManagerMode,
                    OrderBy = argsDto.OrderBy,
                    Originator =argsDto.Originator,
                    OriginatorType = argsDto.OriginatorType,
                    OriginatorId =argsDto.OriginatorId,
                    RepCode =argsDto.RepCode,
                    RepType =argsDto.RepType,
                    ReqSentIsChecked= argsDto.ReqSentIsChecked,
                    ROriginator = argsDto.ROriginator,
                    RowCount =argsDto.RowCount,
                    ShowCount = argsDto.ShowCount,
                    Sector =argsDto.Sector,
                    SEndDate =argsDto.SEndDate,
                    SFilter = argsDto.SFilter,
                    SMonth = argsDto.SMonth,
                    SNoofMonths =argsDto.SNoofMonths,
                    SSource = argsDto.SSource,
                    SStartDate = argsDto.SStartDate,
                    StartIndex =argsDto.StartIndex,
                    Status =argsDto.Status,
                    SToday =argsDto.SToday,
                    SYear=argsDto.SYear,
                    ClientType=argsDto.ClientType,
                    ChildReps=argsDto.ChildReps,
                    Market = argsDto.Market,
                    State = argsDto.State,
                    Brand = args.Brand,
                    SQuantitytype = argsDto.SQuantitytype

                };
            }
            catch (Exception)
            {

                throw;
            }
            return args;
        }

        public static AppointmentCountViewDTO ConvertToAppointmentCountView(AppointmentCountViewEntity obj)
        {
            AppointmentCountViewDTO appointmentViewDTO = new AppointmentCountViewDTO();

            try
            {
                appointmentViewDTO.Category = obj.Category;
                appointmentViewDTO.Color = obj.Color;
                appointmentViewDTO.Week1 = obj.Week1;
                appointmentViewDTO.Week2 = obj.Week2;
                appointmentViewDTO.Week3 = obj.Week3;
                appointmentViewDTO.Week4 = obj.Week4;
                appointmentViewDTO.Week5 = obj.Week5;

                appointmentViewDTO.Month1 = obj.Month1;
                appointmentViewDTO.Month2 = obj.Month2;
                appointmentViewDTO.Month3 = obj.Month3;
            }
            catch (Exception)
            {

                throw;
            }

            return appointmentViewDTO;
        }

        public static CallCycleDTO ConvertToCallCycleDTO(CallCycleEntity callCycle)
        {
            CallCycleDTO callCycleDto = new CallCycleDTO();

            if (callCycle != null)
            {

                try
                {
                    callCycleDto.CallCycleID = callCycle.CallCycleID;
                    callCycleDto.CCType = callCycle.CCType;
                    callCycleDto.Comments = callCycle.Comments;
                    callCycleDto.CreatedBy = callCycle.CreatedBy;
                    callCycleDto.CreatedDate = callCycle.CreatedDate;
                    callCycleDto.DelFlag = callCycle.DelFlag;

                    callCycleDto.Description = callCycle.Description;
                    callCycleDto.DueOn = callCycle.DueOn;
                    callCycleDto.NoOfLeadCustomers = callCycle.NoOfLeadCustomers;
                    callCycleDto.PrefixCode = callCycle.PrefixCode;
                    callCycleDto.RowCount = callCycle.TotalCount;

                }
                catch (Exception)
                {

                    throw;
                }
            }
            return callCycleDto;
        }

        public static CallCycleContactDTO ConvertToCallCycleContactDTO(CallCycleContactEntity callCycle)
        {
            CallCycleContactDTO callCycleDto = new CallCycleContactDTO();

            if (callCycle != null)
            {

                try
                {
                    callCycleDto.CallCycle = ConvertToCallCycleDTO(callCycle.CallCycle);
                    callCycleDto.Contact = LeadCustomerAdapter.ConvertToLeadCustomer(callCycle.Contact);
                    callCycleDto.CreateActivity = callCycle.CreateActivity;
                    callCycleDto.DayOrder = callCycle.DayOrder;
                    callCycleDto.DueOn = callCycle.DueOn;
                    callCycleDto.Originator = callCycle.Originator;
                    callCycleDto.PrefixCode = callCycle.PrefixCode;

                    callCycleDto.RowCount = callCycle.TotalCount;
                    callCycleDto.StartTime = callCycle.StartTime;
                    callCycleDto.WeekDayId = callCycle.WeekDayId;

                }
                catch (Exception)
                {

                    throw;
                }
            }
            return callCycleDto;
        }

        public static CallCycleViewDTO ConvertToCallCycleViewDTO(CallCycleContactEntity callCycle)
        {
            CallCycleViewDTO callCycleDto = new CallCycleViewDTO();

            if (callCycle != null)
            {

                try
                {
                    callCycleDto.Address = callCycle.Contact.Address;
                    callCycleDto.CallCycleID = callCycle.CallCycle.CallCycleID;
                    callCycleDto.City = callCycle.Contact.City;
                    callCycleDto.Description = callCycle.CallCycle.Description;
                    callCycleDto.DueOn = callCycle.CallCycle.DueOn;
                    callCycleDto.Originator = callCycle.Originator;
                    callCycleDto.LeadStage = callCycle.Contact.LeadStage;
                    callCycleDto.Name = callCycle.Contact.Name;
                    callCycleDto.PostalCode = callCycle.Contact.PostalCode;
                    callCycleDto.SourceId = callCycle.Contact.SourceId;
                    callCycleDto.State = callCycle.Contact.State;

                    callCycleDto.RowCount = callCycle.TotalCount;
                    

                }
                catch (Exception)
                {

                    throw;
                }
            }
            return callCycleDto;
        }
        #endregion

        #region - Properties -

        //private ActivityBR oBrActivity = null;
        //private brActivityType oBrActivityType = null;

        #endregion

        #region - Constructor -

        public ActivityAdapter()
        {
            //if (oBrActivity == null || oBrActivityType == null)
            //{
            //    oBrActivity = new brActivity();
            //    oBrActivityType = new brActivityType();
            //}
        }

        #endregion

        #region - New Methods -

        #region - Activity -
        public bool Save(ActivityDTO activityDto, ref int iReminderActivityID)
        {
            bool save = false;

            try
            {
                save = ActivityBR.Instance.Save(ConvertToActivity(activityDto), ref iReminderActivityID);
            }
            catch (Exception)
            {

                throw;
            }
            return save;
        }

        public string InsertConversionQuery(ActivityDTO activity)
        {
            try
            {
                return ActivityBR.Instance.InsertConversionQuery(ConvertToActivity(activity));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<ActivityDTO> GetRelatedActivity(ArgsDTO args)
        {
            List<ActivityDTO> activityDtoList = new List<ActivityDTO>();
            try
            {
                List<ActivityEntity> activityList = ActivityBR.Instance.GetRelatedActivity(ConvertToArgsEntity(args));

                foreach (ActivityEntity item in activityList)
                {
                    activityDtoList.Add(ConvertToActivityDTO(item));
                }
            }
            catch (Exception)
            {
                throw;
            }

            return activityDtoList;
        }

        public ActivityDTO GetActivity(int iActivityID)
        {
            try
            {
                return ConvertToActivityDTO(ActivityBR.Instance.GetActivity(iActivityID));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<ActivityDTO> GetActivitiesForCustomer(ArgsDTO args)
        {
            List<ActivityDTO> activityDtoList = new List<ActivityDTO>();

            try
            {
                List<ActivityEntity> activityList = ActivityBR.Instance.GetActivitiesForCustomer(ConvertToArgsEntity(args));

                foreach (ActivityEntity item in activityList)
                {
                    activityDtoList.Add(ConvertToActivityDTO(item));
                }
            }
            catch (Exception)
            {

                throw;
            }
            return activityDtoList;
        }

        public List<CustomerActivitiesDTO> GetAllActivities(ArgsDTO args, string repTerritory = "")
        {
            List<CustomerActivitiesDTO> customerActivitiesDTOList = new List<CustomerActivitiesDTO>();
            try
            {
                List<CustomerActivityEntity> customerActivityList = ActivityBR.Instance.GetAllActivities(ConvertToArgsEntity(args), repTerritory);
                foreach (CustomerActivityEntity item in customerActivityList)
                {
                    customerActivitiesDTOList.Add(ConvertToCustomerActivity(item));
                }
                 
            }
            catch
            {
                throw;
            }
            return customerActivitiesDTOList;
        }

        public List<CustomerActivitiesDTO> GetActivityCountByType(ArgsDTO args)
        {
            try
            {
                List<CustomerActivitiesDTO> customerActivityDtos = new List<CustomerActivitiesDTO>();
                List<CustomerActivityEntity> CustomerActivities = ActivityBR.Instance.GetActivityCountByType(ConvertToArgsEntity(args));

                foreach (CustomerActivityEntity item in CustomerActivities)
                {
                    customerActivityDtos.Add(CustomerActivitiesAdapter.ConvertToCustomerActivitiesDTO(item));
                }

                return customerActivityDtos;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CustomerActivitiesDTO> GetAllActivitiesByType(ArgsDTO args)
        {
            try
            {
                List<CustomerActivitiesDTO> customerActivityDtos = new List<CustomerActivitiesDTO>();
                List<CustomerActivityEntity> CustomerActivities = ActivityBR.Instance.GetAllActivitiesByType(ConvertToArgsEntity(args));

                foreach (CustomerActivityEntity item in CustomerActivities)
                {
                    customerActivityDtos.Add(CustomerActivitiesAdapter.ConvertToCustomerActivitiesDTO(item));
                }

                return customerActivityDtos;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool ChangeActivityDate(ActivityDTO activity, bool UpdateFromCalendar = false)
        {
            bool ChangeActivity = false;
            try
            {
                ChangeActivity = ActivityBR.Instance.ChangeActivityDate(ConvertToActivity(activity), UpdateFromCalendar) > 0;
            }
            catch (Exception)
            {

                throw;
            }
            return ChangeActivity;
        }

        public CustomerActivitiesDTO GetActivityAppoitmentId(int iActivityId)
        {
            try
            {
                return CustomerActivitiesAdapter.ConvertToCustomerActivitiesDTO(ActivityBR.Instance.GetActivityAppoitmentId(iActivityId)) ;
            }
            catch
            {
                throw;
            }
        }

        public ActivityDTO GetActivityByAppoitmentId(int iAppointmentId)
        {
            ActivityDTO ActivityDTO = new ActivityDTO();

            try
            {
                return ConvertToActivityDTO(ActivityBR.Instance.GetActivityByAppoitmentId(iAppointmentId));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool ResetAppointment(ActivityDTO activity)
        {
            bool reset = false;
            try
            {
                reset = ActivityBR.Instance.ResetAppointment(ConvertToActivity(activity)) > 0;
            }
            catch (Exception)
            {
                throw;
            }
            return reset;
        }

        public bool DeleteActivity(ActivityDTO activity)
        {
            bool delete = false;
            try
            {
                delete = ActivityBR.Instance.Delete(ConvertToActivity(activity)) > 0;
            }
            catch (Exception)
            {
                throw;
            }
            return delete;
        }

        public List<ActivityDTO> GetActivitiesForEndUser(ArgsDTO args)
        {
            List<ActivityDTO> activityDtoList = new List<ActivityDTO>();

            try
            {
                List<ActivityEntity> activityList = ActivityBR.Instance.GetActivitiesForEndUser(ConvertToArgsEntity(args));

                foreach (ActivityEntity item in activityList)
                {
                    activityDtoList.Add(ConvertToActivityDTO(item));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return activityDtoList;
        }

        public List<CustomerActivitiesDTO> GetOutstandingActivities(ArgsDTO args)
        {
            List<CustomerActivitiesDTO> customerActivitiesDtoList = new List<CustomerActivitiesDTO>();

            try
            {
                List<CustomerActivityEntity> activityList = ActivityBR.Instance.GetOutstandingActivities(ConvertToArgsEntity(args));

                foreach (CustomerActivityEntity item in activityList)
                {
                    customerActivitiesDtoList.Add(ConvertToCustomerActivity(item));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return customerActivitiesDtoList;
        }

        public bool UpdateActivityFromPlanner(ActivityDTO activity, string OriginatorType)
        {
            bool update = false;
            try
            {
                update = ActivityBR.Instance.UpdateActivityFromPlanner(ConvertToActivity(activity),  OriginatorType) > 0;
            }
            catch (Exception)
            {
                throw;
            }
            return update;
        }

        public int GetAllHomeActivitiesByCategoryCount(ArgsDTO argsDTO)
        {
            int customerActivities = 0;
            try
            {
                customerActivities= ActivityBR.Instance.GetScheduledActivitiesForHomeCount(ConvertToArgsEntity(argsDTO));
            }
            catch (Exception)
            {

                throw;
            }
            return customerActivities;
        }

        public int GetOutstandingActivitiesCount(ArgsDTO argsDTO)
        {
            int customerActivities = 0;
            try
            {
                customerActivities = ActivityBR.Instance.GetOutstandingActivitiesCount(ConvertToArgsEntity(argsDTO));
            }
            catch (Exception)
            {

                throw;
            }
            return customerActivities;
        }

        public AppointmentCountViewDTO GetAllActivitiesByTypeCount(ArgsDTO args)
        {
            try
            {
                return ConvertToAppointmentCountView(ActivityBR.Instance.GetAllActivitiesByTypeCount(ConvertToArgsEntity(args)));

            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CustomerActivitiesDTO> GetAllActivitiesForHome(ArgsDTO args)
        {
            List<CustomerActivitiesDTO> customerActivitiesDtoList = new List<CustomerActivitiesDTO>();

            try
            {
                List<CustomerActivityEntity> activityList = ActivityBR.Instance.GetAllActivitiesForHome(ConvertToArgsEntity(args));

                foreach (CustomerActivityEntity item in activityList)
                {
                    customerActivitiesDtoList.Add(ConvertToCustomerActivity(item));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return customerActivitiesDtoList;
        }

        public int GetRemindersForHomeCount(ArgsDTO args)
        {
            try
            {
                return ActivityBR.Instance.GetRemindersForHomeCount(ConvertToArgsEntity(args));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CallCycleContactDTO> GetAllCallCyclesCountById(ArgsDTO args)
        {
            List<CallCycleContactDTO> activityDtoList = new List<CallCycleContactDTO>();
            try
            {
                List<CallCycleContactEntity> activityList = ActivityBR.Instance.GetAllCallCyclesCountById(ConvertToArgsEntity(args));

                foreach (CallCycleContactEntity item in activityList)
                {
                    activityDtoList.Add(ConvertToCallCycleContactDTO(item));
                }
            }
            catch (Exception)
            {
                throw;
            }

            return activityDtoList;
        }

        public List<CallCycleViewDTO> GetAllCallCycles(ArgsDTO args)
        {
            List<CallCycleViewDTO> activityDtoList = new List<CallCycleViewDTO>();
            try
            {
                List<CallCycleContactEntity> activityList = ActivityBR.Instance.GetAllCallCycles(ConvertToArgsEntity(args));

                foreach (CallCycleContactEntity item in activityList)
                {
                    activityDtoList.Add(ConvertToCallCycleViewDTO(item));
                }
            }
            catch (Exception)
            {
                throw;
            }

            return activityDtoList;
        }

        public int UpdateActivityStatusForTME(List<ActivityDTO> activityList)
        {
            int save = 0;

            try
            {
                List<ActivityEntity> activityEntityList = new List<ActivityEntity>();
                foreach (ActivityDTO item in activityList)
                    activityEntityList.Add(ConvertToActivity(item));

                save = ActivityBR.Instance.UpdateActivityStatusForTME(activityEntityList);
            }
            catch (Exception)
            {
                throw;
            }
            return save;
        }
        #endregion

        #region Activity Types
        public List<ActivityDTO> GetAllActivityTypes(string defaultDeptID, bool IsSchedular = false)
        {
            List<ActivityDTO> lstActivity = new List<ActivityDTO>();
            try
            {
                List<ActivityTypeEntity> lstActivityType = ActivityTypeBR.Instance.GetAllActivityTypes(defaultDeptID, IsSchedular);
                foreach (ActivityTypeEntity obj in lstActivityType)
                {
                    lstActivity.Add(ConvertToActivityTypeDTO(obj));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstActivity;
        }

        public ActivityDTO GetActivityType(string activityCode, ArgsDTO args)
        {
            ActivityDTO obrActivity = new ActivityDTO();

            try
            {
                obrActivity = ConvertToActivityTypeDTO(ActivityTypeBR.Instance.GetActivityType(activityCode, ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {

                throw;
            }
            return obrActivity;
        }

        public List<ActivityDTO> GetAllFixedActivityTypes(bool bShownInPlanner = false)
        {
            List<ActivityDTO> lstActivityDTO = new List<ActivityDTO>();

            try
            {
                List<ActivityTypeEntity> ActivityType = ActivityTypeBR.Instance.GetAllFixedActivityTypes(bShownInPlanner);
                foreach (ActivityTypeEntity obj in ActivityType)
                {
                    lstActivityDTO.Add(ConvertToActivityTypeDTO(obj));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstActivityDTO;
        }

        public List<ActivityDTO> GetColdCallActyvityTypes()
        {
            List<ActivityDTO> lstActivity = new List<ActivityDTO>();
            try
            {
                List<ActivityTypeEntity> lstActivityType = ActivityTypeBR.Instance.GetColdCallActyvityTypes();
                foreach (ActivityTypeEntity obj in lstActivityType)
                {
                    lstActivity.Add(ConvertToActivityTypeDTO(obj));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstActivity;
        }
        #endregion

        #endregion

        #region Old Methods
        
        /*
        public List<CustomerActivitiesDTO> GetScheduledActivitiesForHome(ArgsDTO argsDTO)
        {
            try
            {
                return ConvertToActivities(oBrActivity.GetScheduledActivitiesForHome(argsDTO.DefaultDepartmentId, argsDTO.ManagerMode, argsDTO.Originator, argsDTO.SSource, argsDTO.Floor, argsDTO.SToday, argsDTO.SStartDate, argsDTO.SEndDate, argsDTO.Status));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CustomerActivitiesDTO> GetAllActivitiesExpiredForHome(ArgsDTO argsDTO)
        {
            try
            {
                return ConvertToActivities(oBrActivity.GetAllActivitiesExpiredForHome(argsDTO.DefaultDepartmentId, argsDTO.ManagerMode, argsDTO.Originator, argsDTO.SSource, argsDTO.Floor, argsDTO.SToday, argsDTO.SStartDate, argsDTO.SEndDate, argsDTO.Status));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CustomerActivitiesDTO> GetAllHomeActivitiesByCategory(ArgsDTO argsDTO, string category)
        {
            List<CustomerActivitiesDTO> customerActivities = new List<CustomerActivitiesDTO>();
            try
            {
                switch (category)
                {
                    case "past":
                        argsDTO.SToday = "Past";
                        customerActivities = ConvertToActivities(oBrActivity.GetAllActivitiesExpiredForHome(argsDTO.DefaultDepartmentId, argsDTO.ManagerMode, argsDTO.Originator, argsDTO.SSource, argsDTO.Floor, argsDTO.SToday, argsDTO.SStartDate, argsDTO.SEndDate, argsDTO.Status));
                        break;
                    case "future":
                        argsDTO.SToday = "Future";
                        customerActivities = ConvertToActivities(oBrActivity.GetScheduledActivitiesForHome(argsDTO.DefaultDepartmentId, argsDTO.ManagerMode, argsDTO.Originator, argsDTO.SSource, argsDTO.Floor, argsDTO.SToday, argsDTO.SStartDate, argsDTO.SEndDate, argsDTO.Status));
                        break;
                    case "today":
                        argsDTO.SToday = "Today";
                        customerActivities = ConvertToActivities(oBrActivity.GetScheduledActivitiesForHome(argsDTO.DefaultDepartmentId, argsDTO.ManagerMode, argsDTO.Originator, argsDTO.SSource, argsDTO.Floor, argsDTO.SToday, argsDTO.SStartDate, argsDTO.SEndDate, argsDTO.Status));
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {

                throw;
            }

            return customerActivities;
        }

        public int GetAllHomeActivitiesByCategoryCount(ArgsDTO argsDTO, string category)
        {
            int customerActivities = 0;
            try
            {
                switch (category)
                {
                    case "past":
                        argsDTO.SToday = "Past";
                        customerActivities = oBrActivity.GetAllActivitiesExpiredForHomeCount(argsDTO.DefaultDepartmentId, argsDTO.ManagerMode, argsDTO.Originator, argsDTO.SSource, argsDTO.Floor, argsDTO.SToday, argsDTO.SStartDate, argsDTO.SEndDate, argsDTO.Status);
                        break;
                    case "future":
                        argsDTO.SToday = "Future";
                        customerActivities = oBrActivity.GetScheduledActivitiesForHomeCount(argsDTO.DefaultDepartmentId, argsDTO.ManagerMode, argsDTO.Originator, argsDTO.SSource, argsDTO.Floor, argsDTO.SToday, argsDTO.SStartDate, argsDTO.SEndDate, argsDTO.Status);
                        break;
                    case "today":
                        argsDTO.SToday = "Today";
                        customerActivities = oBrActivity.GetScheduledActivitiesForHomeCount(argsDTO.DefaultDepartmentId, argsDTO.ManagerMode, argsDTO.Originator, argsDTO.SSource, argsDTO.Floor, argsDTO.SToday, argsDTO.SStartDate, argsDTO.SEndDate, argsDTO.Status);
                        break;
                    default:
                        break;
                }
            }
            catch (Exception)
            {

                throw;
            }

            return customerActivities;
        }

        public List<ActivityDTO> GetAllFixedActivityTypes(bool bShownInPlanner = false)
        {
            brActivityType oBrActivityType = new brActivityType();
            List<ActivityDTO> lstActivityDTO = new List<ActivityDTO>();

            try
            {
                List<ActivityType> ActivityType = oBrActivityType.GetAllFixedActivityTypes(bShownInPlanner);
                foreach (ActivityType obj in ActivityType)
                {
                    lstActivityDTO.Add(ConvertToActivityTypeDTO(obj));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstActivityDTO;
        }

        public ActivityDTO GetLastServiceCall(string custCode)
        {
            try
            {
                return ConvertToActivityDTO(oBrActivity.GetLastServiceCall(custCode));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CustomerActivitiesDTO> TestMethod(ArgsDTO argsDTO)
        {
            try
            {
                return ConvertToActivities(oBrActivity.TestMethod(argsDTO.DefaultDepartmentId, argsDTO.ManagerMode, argsDTO.Originator, argsDTO.SSource, argsDTO.Floor, argsDTO.SToday, argsDTO.SStartDate, argsDTO.SEndDate, argsDTO.Status));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CustomerActivitiesDTO> GetActivitiesByCategory(ArgsDTO argsDTO)
        {
            List<CustomerActivitiesDTO> customerActivities = new List<CustomerActivitiesDTO>();

            try
            {
                customerActivities = ConvertToActivities(oBrActivity.GetAllActivitiesByCategory(argsDTO.DefaultDepartmentId, argsDTO.ManagerMode, argsDTO.Originator, argsDTO.SSource, argsDTO.Floor, argsDTO.SToday, argsDTO.SStartDate, argsDTO.SEndDate, argsDTO.Status));
            }
            catch (Exception)
            {

                throw;
            }
            return customerActivities;
        }
        */

        //public List<CustomerActivitiesDTO> GetScheduledActivities(ArgsDTO argsDTO)
        //{
        //    try
        //    {
        //        return ConvertToActivities(oBrActivity.GetScheduledActivities(argsDTO.Originator, argsDTO.SSource, argsDTO.Floor, argsDTO.SToday, argsDTO.SStartDate, argsDTO.SEndDate, argsDTO.Status, argsDTO.RepTerritory
        //                        , argsDTO.DefaultDepartmentId, argsDTO.ChiildOriginators));
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        //public CustomerActivitiesDTO GetActivityAppointmentId(int activityId)
        //{
        //    CustomerActivitiesDTO customerActivities = new CustomerActivitiesDTO();

        //    try
        //    {
        //        customerActivities = ConvertToCustomerActivity(oBrActivity.GetActivityAppointmentId(activityId));
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }

        //    return customerActivities;
        //}

        //public List<ActivityDTO> GetActivityTypeEntries(ArgsDTO args)
        //{
        //    List<ActivityDTO> lstActivity = new List<ActivityDTO>();
        //    try
        //    {
        //        List<ActivityType> lstActivityType = oBrActivityType.GetActivityTypeEntries(args.DefaultDepartmentId);
        //        foreach (ActivityType obj in lstActivityType)
        //        {
        //            lstActivity.Add(ConvertToActivityTypeDTO(obj));
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    return lstActivity;
        //}

        #endregion
    }
}
