﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Entities;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.BusinessRules;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class DiscountSchemeAdapter
    {
        #region Converters

        #region SchemeHeader

        public static SchemeHeaderDTO ConvertToSchemeHeaderDTO(SchemeHeaderEntity schemeHeaderEntity)
        {
            SchemeHeaderDTO schemeHeaderDTO = new SchemeHeaderDTO();

            try
            {
                if (schemeHeaderDTO != null)
                {
                    schemeHeaderDTO.SchemeHeaderId = schemeHeaderEntity.SchemeHeaderId;
                    schemeHeaderDTO.SchemeName = schemeHeaderEntity.SchemeName;
                    schemeHeaderDTO.IsActive = schemeHeaderEntity.IsActive;
                    schemeHeaderDTO.StartDate = schemeHeaderEntity.StartDate;
                    schemeHeaderDTO.EndDate = schemeHeaderEntity.EndDate;
                    schemeHeaderDTO.DivisionId = schemeHeaderEntity.DivisionId;
                    schemeHeaderDTO.DivisionName = schemeHeaderEntity.DivisionName;
                    schemeHeaderDTO.RowCount = schemeHeaderEntity.TotalCount;
                    if (schemeHeaderEntity.SchemeDetailsList != null)
                        schemeHeaderDTO.SchemeDetailsList = ConvertToSchemeDetailsDTOList(schemeHeaderEntity.SchemeDetailsList);

                    schemeHeaderDTO.CreatedBy = schemeHeaderEntity.CreatedBy;
                    schemeHeaderDTO.CreatedDate = schemeHeaderEntity.CreatedDate;
                    schemeHeaderDTO.LastModifiedBy = schemeHeaderEntity.LastModifiedBy;
                    schemeHeaderDTO.LastModifiedDate = schemeHeaderEntity.LastModifiedDate;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return schemeHeaderDTO;
        }

        public static SchemeHeaderEntity ConvertToSchemeHeaderEntity(SchemeHeaderDTO schemeHeaderDTO)
        {
            SchemeHeaderEntity schemeHeaderEntity = new SchemeHeaderEntity();

            try
            {
                if (schemeHeaderEntity != null)
                {
                    schemeHeaderEntity.SchemeHeaderId = schemeHeaderDTO.SchemeHeaderId;
                    schemeHeaderEntity.SchemeName = schemeHeaderDTO.SchemeName;
                    schemeHeaderEntity.IsActive = schemeHeaderDTO.IsActive;
                    schemeHeaderEntity.StartDate = schemeHeaderDTO.StartDate;
                    schemeHeaderEntity.EndDate = schemeHeaderDTO.EndDate;
                    schemeHeaderEntity.DivisionId = schemeHeaderDTO.DivisionId;
                    schemeHeaderEntity.DivisionName = schemeHeaderDTO.DivisionName;
                    schemeHeaderEntity.TotalCount = schemeHeaderDTO.RowCount;
                    if (schemeHeaderDTO.SchemeDetailsList != null)
                        schemeHeaderEntity.SchemeDetailsList = ConvertToSchemeDetailsEntityList(schemeHeaderDTO.SchemeDetailsList);

                    schemeHeaderEntity.CreatedBy = schemeHeaderDTO.CreatedBy;
                    schemeHeaderEntity.CreatedDate = schemeHeaderDTO.CreatedDate;
                    schemeHeaderEntity.LastModifiedBy = schemeHeaderDTO.LastModifiedBy;
                    schemeHeaderEntity.LastModifiedDate = schemeHeaderDTO.LastModifiedDate;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return schemeHeaderEntity;
        }

        public static List<SchemeHeaderDTO> ConvertToSchemeHeaderDTOList(List<SchemeHeaderEntity> lstSchemeHeaderEntity)
        {
            SchemeHeaderDTO schemeHeaderDTO = null;
            List<SchemeHeaderDTO> lstSchemeHeaderDTO = new List<SchemeHeaderDTO>();
            foreach (SchemeHeaderEntity item in lstSchemeHeaderEntity)
            {
                schemeHeaderDTO = new SchemeHeaderDTO();
                schemeHeaderDTO = ConvertToSchemeHeaderDTO(item);
                lstSchemeHeaderDTO.Add(schemeHeaderDTO);
            }
            return lstSchemeHeaderDTO;
        }

        public static List<SchemeHeaderEntity> ConvertToSchemeHeaderEntityList(List<SchemeHeaderDTO> lstSchemeHeaderDTO)
        {
            SchemeHeaderEntity schemeHeaderEntity = null;
            List<SchemeHeaderEntity> lstBankAccountEntity = new List<SchemeHeaderEntity>();
            foreach (SchemeHeaderDTO item in lstSchemeHeaderDTO)
            {
                schemeHeaderEntity = new SchemeHeaderEntity();

                schemeHeaderEntity = ConvertToSchemeHeaderEntity(item);

                lstBankAccountEntity.Add(schemeHeaderEntity);
            }
            return lstBankAccountEntity;
        }

        #endregion

        #region FreeProduct

        public static FreeProductDTO ConvertToFreeProductDTO(FreeProductEntity objectIn)
        {
            FreeProductDTO objectOut = new FreeProductDTO();
            try
            {
                if (objectIn != null)
                {
                    objectOut.FreeProductId = objectIn.FreeProductId;
                    objectOut.ProductId = objectIn.ProductId;
                    objectOut.Qty = objectIn.Qty;
                    objectOut.ProductPackingId = objectIn.ProductPackingId;
                    objectOut.SchemeDetailsId = objectIn.SchemeDetailsId;
                    objectOut.CreatedBy = objectIn.CreatedBy;
                    objectOut.CreatedDate = objectIn.CreatedDate;
                    objectOut.LastModifiedBy = objectIn.LastModifiedBy;
                    objectOut.LastModifiedDate = objectIn.LastModifiedDate;
                    objectOut.RowCount = objectIn.TotalCount;

                    objectOut.ProductPackingName = objectIn.ProductPackingName;
                    objectOut.ProductName = objectIn.ProductName;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return objectOut;
        }

        public static FreeProductEntity ConvertToFreeProductEntity(FreeProductDTO objectIn)
        {
            FreeProductEntity objectOut = FreeProductEntity.CreateObject();
            try
            {
                if (objectIn != null)
                {
                    objectOut.FreeProductId = objectIn.FreeProductId;
                    objectOut.ProductId = objectIn.ProductId;
                    objectOut.Qty = objectIn.Qty;
                    objectOut.ProductPackingId = objectIn.ProductPackingId;
                    objectOut.SchemeDetailsId = objectIn.SchemeDetailsId;
                    objectOut.CreatedBy = objectIn.CreatedBy;
                    objectOut.CreatedDate = objectIn.CreatedDate;
                    objectOut.LastModifiedBy = objectIn.LastModifiedBy;
                    objectOut.LastModifiedDate = objectIn.LastModifiedDate;
                    objectOut.TotalCount = objectIn.RowCount;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return objectOut;
        }

        public static List<FreeProductDTO> ConvertToFreeProductDTOList(List<FreeProductEntity> listIn)
        {
            List<FreeProductDTO> listOut = new List<FreeProductDTO>();
            try
            {
                foreach (FreeProductEntity itemIn in listIn)
                {
                    listOut.Add(ConvertToFreeProductDTO(itemIn));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return listOut;
        }

        public static List<FreeProductEntity> ConvertToFreeProductEntityList(List<FreeProductDTO> listIn)
        {
            List<FreeProductEntity> listOut = new List<FreeProductEntity>();
            try
            {
                foreach (FreeProductDTO itemIn in listIn)
                {
                    listOut.Add(ConvertToFreeProductEntity(itemIn));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return listOut;
        }

        #endregion

        #region Division

        public static DivisionDTO ConvertToDivisionDTO(DivisionEntity objectIn)
        {
            DivisionDTO objectOut = new DivisionDTO();
            try
            {
                if (objectIn != null)
                {
                    objectOut.DivisionId = objectIn.DivisionId;
                    objectOut.DivisionCode = objectIn.DivisionCode;
                    objectOut.DivisionName = objectIn.DivisionName;
                    objectOut.IsActive = objectIn.IsActive;
                    objectOut.CreatedBy = objectIn.CreatedBy;
                    objectOut.CreatedDate = objectIn.CreatedDate;
                    objectOut.LastModifiedBy = objectIn.LastModifiedBy;
                    objectOut.LastModifiedDate = objectIn.LastModifiedDate;
                    objectOut.RowCount = objectIn.TotalCount;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return objectOut;
        }

        public static DivisionEntity ConvertToDivisionEntity(DivisionDTO objectIn)
        {
            DivisionEntity objectOut = DivisionEntity.CreateObject();
            try
            {
                if (objectIn != null)
                {
                    objectOut.DivisionId = objectIn.DivisionId;
                    objectOut.DivisionCode = objectIn.DivisionCode;
                    objectOut.DivisionName = objectIn.DivisionName;
                    objectOut.IsActive = objectIn.IsActive;
                    objectOut.CreatedBy = objectIn.CreatedBy;
                    objectOut.CreatedDate = objectIn.CreatedDate;
                    objectOut.LastModifiedBy = objectIn.LastModifiedBy;
                    objectOut.LastModifiedDate = objectIn.LastModifiedDate;
                    objectOut.TotalCount = objectIn.RowCount;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return objectOut;
        }

        public static List<DivisionDTO> ConvertToDivisionDTOList(List<DivisionEntity> listIn)
        {
            List<DivisionDTO> listOut = new List<DivisionDTO>();
            try
            {
                foreach (DivisionEntity itemIn in listIn)
                {
                    listOut.Add(ConvertToDivisionDTO(itemIn));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return listOut;
        }

        public static List<DivisionEntity> ConvertToDivisionDTOList(List<DivisionDTO> listIn)
        {
            List<DivisionEntity> listOut = new List<DivisionEntity>();
            try
            {
                foreach (DivisionDTO itemIn in listIn)
                {
                    listOut.Add(ConvertToDivisionEntity(itemIn));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return listOut;
        }

        #endregion

        #region SchemeDetails

        public static SchemeDetailsDTO ConvertToSchemeDetailsDTO(SchemeDetailsEntity schemeDetailsEntity)
        {
            SchemeDetailsDTO schemeDetailsDTO = new SchemeDetailsDTO();

            try
            {
                if (schemeDetailsDTO != null)
                {
                    schemeDetailsDTO.SchemeDetailsId = schemeDetailsEntity.SchemeDetailsId;
                    schemeDetailsDTO.SchemeHeaderId = schemeDetailsEntity.SchemeHeaderId;
                    schemeDetailsDTO.SchemeTypeId = schemeDetailsEntity.SchemeTypeId;
                    schemeDetailsDTO.DetailsTypeId = schemeDetailsEntity.DetailsTypeId;
                    schemeDetailsDTO.SchemeDetails = schemeDetailsEntity.SchemeDetails;
                    schemeDetailsDTO.FreeIssueCondition = schemeDetailsEntity.FreeIssueCondition;
                    schemeDetailsDTO.DiscountPercentage = schemeDetailsEntity.DiscountPercentage;
                    schemeDetailsDTO.DiscountValue = schemeDetailsEntity.DiscountValue;
                    schemeDetailsDTO.IsRetail = schemeDetailsEntity.IsRetail;
                    schemeDetailsDTO.IsActive = schemeDetailsEntity.IsActive;
                    schemeDetailsDTO.RowCount = schemeDetailsEntity.TotalCount;
                    if (schemeDetailsEntity.OptionLevelList != null)
                        schemeDetailsDTO.OptionLevelList = ConvertToOptionLevelDTOList(schemeDetailsEntity.OptionLevelList);
                    if (schemeDetailsEntity.FreeProductList != null)
                        schemeDetailsDTO.FreeProductList = ConvertToFreeProductDTOList(schemeDetailsEntity.FreeProductList);
                    schemeDetailsDTO.SchemeHeaderName = schemeDetailsEntity.SchemeHeaderName;
                    schemeDetailsDTO.DetailsTypeName = schemeDetailsEntity.DetailsTypeName;
                    schemeDetailsDTO.SchemeTypeName = schemeDetailsEntity.SchemeTypeName;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return schemeDetailsDTO;
        }

        public static SchemeDetailsEntity ConvertToSchemeDetailsEntity(SchemeDetailsDTO schemeDetailsDTO)
        {
            SchemeDetailsEntity schemeDetailsEntity = new SchemeDetailsEntity();

            try
            {
                if (schemeDetailsEntity != null)
                {
                    schemeDetailsEntity.SchemeDetailsId = schemeDetailsDTO.SchemeDetailsId;
                    schemeDetailsEntity.SchemeHeaderId = schemeDetailsDTO.SchemeHeaderId;
                    schemeDetailsEntity.SchemeTypeId = schemeDetailsDTO.SchemeTypeId;
                    schemeDetailsEntity.DetailsTypeId = schemeDetailsDTO.DetailsTypeId;
                    schemeDetailsEntity.SchemeDetails = schemeDetailsDTO.SchemeDetails;
                    schemeDetailsEntity.DiscountPercentage = schemeDetailsDTO.DiscountPercentage;
                    schemeDetailsEntity.DiscountValue = schemeDetailsDTO.DiscountValue;
                    schemeDetailsEntity.IsRetail = schemeDetailsDTO.IsRetail;
                    schemeDetailsEntity.IsActive = schemeDetailsDTO.IsActive;
                    schemeDetailsEntity.TotalCount = schemeDetailsDTO.RowCount;
                    if (schemeDetailsDTO.OptionLevelList != null)
                        schemeDetailsEntity.OptionLevelList = ConvertToOptionLevelEntityList(schemeDetailsDTO.OptionLevelList);

                    if (schemeDetailsDTO.FreeProductList != null)
                        schemeDetailsEntity.FreeProductList = ConvertToFreeProductEntityList(schemeDetailsDTO.FreeProductList);

                    schemeDetailsEntity.SchemeHeaderName = schemeDetailsDTO.SchemeHeaderName;
                    schemeDetailsEntity.DetailsTypeName = schemeDetailsDTO.DetailsTypeName;
                    schemeDetailsEntity.SchemeTypeName = schemeDetailsDTO.SchemeTypeName;
                    schemeDetailsEntity.FreeIssueCondition = schemeDetailsDTO.FreeIssueCondition;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return schemeDetailsEntity;
        }

        public static List<SchemeDetailsDTO> ConvertToSchemeDetailsDTOList(List<SchemeDetailsEntity> lstSchemeDetailsEntity)
        {
            SchemeDetailsDTO schemeDetailsDTO = null;
            List<SchemeDetailsDTO> lstSchemeDetailsDTO = new List<SchemeDetailsDTO>();
            int index = 1;
            foreach (SchemeDetailsEntity item in lstSchemeDetailsEntity)
            {
                schemeDetailsDTO = new SchemeDetailsDTO();
                schemeDetailsDTO = ConvertToSchemeDetailsDTO(item);
                schemeDetailsDTO.IndexId = index;
                lstSchemeDetailsDTO.Add(schemeDetailsDTO);
                index++;
            }
            return lstSchemeDetailsDTO;
        }

        public static List<SchemeDetailsEntity> ConvertToSchemeDetailsEntityList(List<SchemeDetailsDTO> lstSchemeDetailsDTO)
        {
            SchemeDetailsEntity schemeDetailsEntity = null;
            List<SchemeDetailsEntity> lstSchemeDetailsEntity = new List<SchemeDetailsEntity>();
            foreach (SchemeDetailsDTO item in lstSchemeDetailsDTO)
            {
                schemeDetailsEntity = new SchemeDetailsEntity();
                schemeDetailsEntity = ConvertToSchemeDetailsEntity(item);
                lstSchemeDetailsEntity.Add(schemeDetailsEntity);
            }
            return lstSchemeDetailsEntity;
        }

        #endregion

        #region OptionLevel

        public static OptionLevelDTO ConvertToOptionLevelDTO(OptionLevelEntity optionLevelEntity)
        {
            OptionLevelDTO optionLevelDTO = new OptionLevelDTO();

            try
            {
                if (optionLevelDTO != null)
                {
                    optionLevelDTO.SchemeDetailId = optionLevelEntity.SchemeDetailId;
                    optionLevelDTO.OptionLevelId = optionLevelEntity.OptionLevelId;
                    optionLevelDTO.ProductCategoryId = optionLevelEntity.ProductCategoryId;
                    optionLevelDTO.ProductPackingId = optionLevelEntity.ProductPackingId;
                    optionLevelDTO.ProductId = optionLevelEntity.ProductId;
                    optionLevelDTO.BrandId = optionLevelEntity.BrandId;                    
                    optionLevelDTO.MinQuantity = optionLevelEntity.MinQuantity;
                    optionLevelDTO.MinValue = optionLevelEntity.MinValue;
                    optionLevelDTO.MaxQuantity = optionLevelEntity.MaxQuantity;
                    optionLevelDTO.MaxValue = optionLevelEntity.MaxValue;
                    optionLevelDTO.RowCount = optionLevelEntity.TotalCount;
                    optionLevelDTO.IsActive = optionLevelEntity.IsActive;
                    optionLevelDTO.CatlogId = optionLevelEntity.CatalogId;
                    optionLevelDTO.CatlogName = optionLevelEntity.CatlogName;
                    optionLevelDTO.IsHvp = optionLevelEntity.IsHvp;
                    optionLevelDTO.IsBill = optionLevelEntity.IsBill;
                    optionLevelDTO.OperatorId = optionLevelEntity.OperatorId;

                    optionLevelDTO.ProductCatagoryName = optionLevelEntity.ProductCatagoryName;
                    optionLevelDTO.ProductPackingName = optionLevelEntity.ProductPackingName;
                    optionLevelDTO.ProductName = optionLevelEntity.ProductName;
                    optionLevelDTO.BrandName = optionLevelEntity.BrandName;

                    optionLevelDTO.IsForEvery = optionLevelEntity.IsForEvery;
                    
                    optionLevelDTO.LineCount = optionLevelEntity.LineCount;

                    if (optionLevelEntity.OptionLevelCombinationList != null)
                        optionLevelDTO.OptionLevelCombinationList = ConvertToOptionLevelCombinationDTOList(optionLevelEntity.OptionLevelCombinationList);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return optionLevelDTO;
        }

        public static OptionLevelEntity ConvertToOptionLevelEntity(OptionLevelDTO optionLevelDTO)
        {
            OptionLevelEntity optionLevelEntity = new OptionLevelEntity();

            try
            {
                if (optionLevelEntity != null)
                {
                    optionLevelEntity.SchemeDetailId = optionLevelDTO.SchemeDetailId;
                    optionLevelEntity.OptionLevelId = optionLevelDTO.OptionLevelId;
                    optionLevelEntity.ProductCategoryId = optionLevelDTO.ProductCategoryId;
                    optionLevelEntity.ProductPackingId = optionLevelDTO.ProductPackingId;
                    optionLevelEntity.ProductId = optionLevelDTO.ProductId;
                    optionLevelEntity.BrandId = optionLevelDTO.BrandId;
                    //optionLevelEntity.ProductId = optionLevelDTO.ItemTypeId;
                    optionLevelEntity.MinQuantity = optionLevelDTO.MinQuantity;
                    optionLevelEntity.MinValue = optionLevelDTO.MinValue;
                    optionLevelEntity.MaxQuantity = optionLevelDTO.MaxQuantity;
                    optionLevelEntity.MaxValue = optionLevelDTO.MaxValue;
                    optionLevelEntity.TotalCount = optionLevelDTO.RowCount;
                    optionLevelEntity.IsActive = optionLevelDTO.IsActive;
                    optionLevelEntity.CatalogId = optionLevelDTO.CatlogId;
                    optionLevelEntity.CatlogName = optionLevelDTO.CatlogName;
                    optionLevelEntity.IsHvp = optionLevelDTO.IsHvp;
                    optionLevelEntity.IsBill = optionLevelDTO.IsBill;
                    optionLevelEntity.OperatorId = optionLevelDTO.OperatorId;

                    optionLevelEntity.IsForEvery = optionLevelDTO.IsForEvery;

                    optionLevelEntity.LineCount = optionLevelDTO.LineCount;

                    if (optionLevelDTO.OptionLevelCombinationList != null)
                        optionLevelEntity.OptionLevelCombinationList = ConvertToOptionLevelCombinationEntityList(optionLevelDTO.OptionLevelCombinationList);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return optionLevelEntity;
        }

        public static List<OptionLevelDTO> ConvertToOptionLevelDTOList(List<OptionLevelEntity> lstOptionLevelEntity)
        {
            OptionLevelDTO optionLevelDTO = null;
            List<OptionLevelDTO> lstOptionLevelDTO = new List<OptionLevelDTO>();
            int index = 1;
            if (lstOptionLevelEntity != null)
            {                
                foreach (OptionLevelEntity item in lstOptionLevelEntity)
                {
                    optionLevelDTO = new OptionLevelDTO();
                    optionLevelDTO = ConvertToOptionLevelDTO(item);

                    optionLevelDTO.IndexId = index;
                    lstOptionLevelDTO.Add(optionLevelDTO);
                    index++;
                }
            }
            return lstOptionLevelDTO;
        }

        public static List<OptionLevelEntity> ConvertToOptionLevelEntityList(List<OptionLevelDTO> lstOptionLevelDTO)
        {
            OptionLevelEntity optionLevelEntity = null;
            List<OptionLevelEntity> lstOptionLevelEntity = new List<OptionLevelEntity>();
            foreach (OptionLevelDTO item in lstOptionLevelDTO)
            {
                optionLevelEntity = new OptionLevelEntity();
                optionLevelEntity = ConvertToOptionLevelEntity(item);
                lstOptionLevelEntity.Add(optionLevelEntity);
            }
            return lstOptionLevelEntity;
        }

        #endregion

        #region OptionLevelCombination

        public static OptionLevelCombinationDTO ConvertToOptionLevelCombinationDTO(OptionLevelCombinationEntity optionLevelCombinationEntity)
        {
            OptionLevelCombinationDTO optionLevelCombinationDTO = new OptionLevelCombinationDTO();

            try
            {
                if (optionLevelCombinationDTO != null)
                {
                    optionLevelCombinationDTO.OptionLevelCombinationId = optionLevelCombinationEntity.OptionLevelCombinationId;
                    optionLevelCombinationDTO.OptionLevelId = optionLevelCombinationEntity.OptionLevelId;
                    optionLevelCombinationDTO.ProductId = optionLevelCombinationEntity.ProductId;
                    optionLevelCombinationDTO.ProductCategoryId = optionLevelCombinationEntity.ProductCategoryId;
                    optionLevelCombinationDTO.ProductPackingId = optionLevelCombinationEntity.ProductPackingId;
                    optionLevelCombinationDTO.BrandId = optionLevelCombinationEntity.BrandId;
                    optionLevelCombinationDTO.IsExceptProduct = optionLevelCombinationEntity.IsExceptProduct;

                    string isexception_desc = "Default";

                    switch (optionLevelCombinationEntity.conditionType)
                    {
                        case "C":
                            isexception_desc = "Compulsory";
                            break;
                        case "D":
                            isexception_desc = "Default";
                            break;
                        case "E":
                            isexception_desc = "Exception";
                            break;
                        default:
                            isexception_desc = "Default";
                            break;
                    }

                    optionLevelCombinationDTO.IsExcept = isexception_desc;
                    optionLevelCombinationDTO.RowCount = optionLevelCombinationEntity.TotalCount;

                    optionLevelCombinationDTO.IsHvp = optionLevelCombinationEntity.IsHvp;
                    optionLevelCombinationDTO.FlavorId = optionLevelCombinationEntity.FlavorId;
                    optionLevelCombinationDTO.FlavorName = optionLevelCombinationEntity.FlavorName;
                    optionLevelCombinationDTO.ProductCatagoryName = optionLevelCombinationEntity.ProductCatagoryName;
                    optionLevelCombinationDTO.ProductPackingName = optionLevelCombinationEntity.ProductPackingName;
                    optionLevelCombinationDTO.ProductName = optionLevelCombinationEntity.ProductName;
                    optionLevelCombinationDTO.BrandName = optionLevelCombinationEntity.BrandName;

                }
            }
            catch (Exception)
            {
                throw;
            }
            return optionLevelCombinationDTO;
        }

        public static OptionLevelCombinationEntity ConvertToOptionLevelCombinationEntity(OptionLevelCombinationDTO optionLevelCombinationDTO)
        {
            OptionLevelCombinationEntity optionLevelCombinationEntity = new OptionLevelCombinationEntity();

            try
            {
                if (optionLevelCombinationEntity != null)
                {
                    optionLevelCombinationEntity.OptionLevelCombinationId = optionLevelCombinationDTO.OptionLevelCombinationId;
                    optionLevelCombinationEntity.OptionLevelId = optionLevelCombinationDTO.OptionLevelId;
                    optionLevelCombinationEntity.ProductId = optionLevelCombinationDTO.ProductId;
                    optionLevelCombinationEntity.ProductId = optionLevelCombinationDTO.ProductId;
                    optionLevelCombinationEntity.ProductCategoryId = optionLevelCombinationDTO.ProductCategoryId;
                    optionLevelCombinationEntity.ProductPackingId = optionLevelCombinationDTO.ProductPackingId;
                    optionLevelCombinationEntity.BrandId = optionLevelCombinationDTO.BrandId;
                    optionLevelCombinationEntity.IsExceptProduct = optionLevelCombinationDTO.IsExceptProduct;
                    optionLevelCombinationEntity.TotalCount = optionLevelCombinationDTO.RowCount;
                    optionLevelCombinationEntity.IsHvp = optionLevelCombinationDTO.IsHvp;
                    optionLevelCombinationEntity.FlavorId = optionLevelCombinationDTO.FlavorId;
                    optionLevelCombinationEntity.conditionType = optionLevelCombinationDTO.IsExcept.Substring(0, 1).ToUpper();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return optionLevelCombinationEntity;
        }

        public static List<OptionLevelCombinationDTO> ConvertToOptionLevelCombinationDTOList(List<OptionLevelCombinationEntity> lstOptionLevelCombinationEntity)
        {
            OptionLevelCombinationDTO optionLevelCombinationDTO = null;
            List<OptionLevelCombinationDTO> lstOptionLevelCombinationDTO = new List<OptionLevelCombinationDTO>();
            int index = 1;
            foreach (OptionLevelCombinationEntity item in lstOptionLevelCombinationEntity)
            {
                optionLevelCombinationDTO = new OptionLevelCombinationDTO();
                optionLevelCombinationDTO = ConvertToOptionLevelCombinationDTO(item);
                optionLevelCombinationDTO.IndexId = index;
                lstOptionLevelCombinationDTO.Add(optionLevelCombinationDTO);
                index++;
            }
            return lstOptionLevelCombinationDTO;
        }

        public static List<OptionLevelCombinationEntity> ConvertToOptionLevelCombinationEntityList(List<OptionLevelCombinationDTO> lstOptionLevelCombinationDTO)
        {
            OptionLevelCombinationEntity optionLevelCombinationEntity = null;
            List<OptionLevelCombinationEntity> lstOptionLevelCombinationEntity = new List<OptionLevelCombinationEntity>();
            foreach (OptionLevelCombinationDTO item in lstOptionLevelCombinationDTO)
            {
                optionLevelCombinationEntity = new OptionLevelCombinationEntity();
                optionLevelCombinationEntity = ConvertToOptionLevelCombinationEntity(item);
                lstOptionLevelCombinationEntity.Add(optionLevelCombinationEntity);
            }
            return lstOptionLevelCombinationEntity;
        }

        #endregion

        #endregion

        public List<SchemeHeaderDTO> GetAllSchemeHeader(ArgsDTO args)
        {
            try
            {
                List<SchemeHeaderEntity> schemeHeaderList = SchemeHeaderBR.Instance.GetAllSchemeHeader(ActivityAdapter.ConvertToArgsEntity(args));

                return ConvertToSchemeHeaderDTOList(schemeHeaderList);
            }
            catch (Exception)
            {
                throw;
            }
        }
        
        public List<SchemeHeaderDTO> GetAllSchemeHeader()
        {
            try
            {
                List<SchemeHeaderEntity> schemeHeaderList = SchemeHeaderBR.Instance.GetAllSchemeHeader();
                return ConvertToSchemeHeaderDTOList(schemeHeaderList);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public SchemeHeaderDTO GetSchemeHeaderById(ArgsDTO args,int schemeHeaderId)
        {
            List<SchemeHeaderDTO> lstAccounts = new List<SchemeHeaderDTO>();
            try
            {
                //SchemeHeaderEntity schemeHeader = SchemeHeaderBR.Instance.GetSchemeHeaderById(CommonAdapter.ConvertToArgsEntity(args),schemeHeaderId);
                SchemeHeaderEntity schemeHeader = SchemeHeaderBR.Instance.GetSchemeHeaderById(schemeHeaderId);
                return ConvertToSchemeHeaderDTO(schemeHeader);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SchemeHeaderDTO> GetSchemeHeaderByName(string name, bool isActive)
        {
            try
            {
                List<SchemeHeaderEntity> schemeHeader = SchemeHeaderBR.Instance.GetSchemeHeaderByName(name, isActive);
                return ConvertToSchemeHeaderDTOList(schemeHeader);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateSchemeHeader(SchemeHeaderDTO schemeHeader)
        {
            try
            {
                return SchemeHeaderBR.Instance.UpdateSchemeHeader(ConvertToSchemeHeaderEntity(schemeHeader));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool InsertSchemeHeader(out int schemeHeaderId, SchemeHeaderDTO schemeHeader)
        {
            try
            {
                return SchemeHeaderBR.Instance.InsertSchemeHeader(out schemeHeaderId, ConvertToSchemeHeaderEntity(schemeHeader));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SchemeHeaderSave(out int schemeHeaderId, SchemeHeaderDTO schemeHeader)
        {
            try
            {
                return SchemeHeaderBR.Instance.SchemeHeaderSave(out schemeHeaderId, ConvertToSchemeHeaderEntity(schemeHeader) );
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateSchemeDetails(SchemeDetailsDTO schemeDetails)
        {
            try
            {
                return SchemeDetailsBR.Instance.UpdateSchemeDetails(ConvertToSchemeDetailsEntity(schemeDetails));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool InsertSchemeDetails(out int schemeDetailsId, SchemeDetailsDTO schemeDetails)
        {
            try
            {
                return SchemeDetailsBR.Instance.InsertSchemeDetails(out schemeDetailsId, ConvertToSchemeDetailsEntity(schemeDetails));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<SchemeDetailsDTO> GetSchemeDetailsBySchemeHeaderId(ArgsDTO args, int schemeHeaderId)
        {
            List<SchemeDetailsDTO> lstSchemeDetailsDTO = new List<SchemeDetailsDTO>();
            try
            {
                List<SchemeDetailsEntity> schemeHeaderList = SchemeDetailsBR.Instance.GetSchemeDetailsBySchemeHeaderId(CommonAdapter.ConvertToArgsEntity(args), schemeHeaderId);
                return ConvertToSchemeDetailsDTOList(schemeHeaderList);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OptionLevelDTO> GetOptionLevelByDetailsId(int detailsId)
        {
            try
            {
                List<OptionLevelEntity> optionLevelList = OptionLevelBR.Instance.GetOptionLevelByDetailsId(detailsId);
                return ConvertToOptionLevelDTOList(optionLevelList);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OptionLevelCombinationDTO> GetOptionLevelCombinationByOptionLevelId(int optionLevelId)
        {
            try
            {
                List<OptionLevelCombinationEntity> optionLevelList =
                    OptionLevelCombinationBR.Instance.GetOptionLevelCombinationByOptionLevelId(optionLevelId);
                return ConvertToOptionLevelCombinationDTOList(optionLevelList);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<FreeProductDTO> GetAllFreeProducts(ArgsDTO args)
        {
            try
            {
                return ConvertToFreeProductDTOList(FreeProductBR.Instance.GetAllFreeProducts(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {
                throw;
            }
        }

        #region - Division -

        public List<DivisionDTO> GetAllDivisions(ArgsDTO args)
        {
            try
            {
                return ConvertToDivisionDTOList(DivisionBR.Instance.GetAllDivisions(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveDivision(out int divisionId, DivisionDTO divisionEntity)
        {
            try
            {
                return DivisionBR.Instance.SaveDivision(out divisionId, ConvertToDivisionEntity(divisionEntity));
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        public bool IsSchemeHeaderNameExists(SchemeHeaderDTO schemeHeaderDTO)
        {
            try
            {
                return SchemeHeaderBR.Instance.IsSchemeHeaderNameExists(ConvertToSchemeHeaderEntity(schemeHeaderDTO));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool UpdateSchemeHeaderIsActive(SchemeHeaderDTO schemeHeader)
        {
            try
            {
                return SchemeHeaderBR.Instance.UpdateSchemeHeaderIsActive(ConvertToSchemeHeaderEntity(schemeHeader));
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
