﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.DTO.CompositeEntities;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.Entities;
using System.Data;

namespace Peercore.CRM.Service.Common.Adapter
{
    class CustomerAdapter
    {
        #region - Converters -

        public static CustomerEntity ConvertToCustomer(CustomerDTO activityDto)
        {
            CustomerEntity Customer = new CustomerEntity();

            try
            {
                Customer.Address1 = activityDto.Address;
                Customer.AnnualRevenue = activityDto.AnnualRevenue;
                Customer.Business = activityDto.Business;
                Customer.BusinessPotential = activityDto.BusinessPotential;
                Customer.City = activityDto.City;
                Customer.Company = activityDto.Company;
                Customer.Country = activityDto.Country;
                Customer.CreatedBy = activityDto.CreatedBy;
                Customer.CreatedDate = activityDto.CreatedDate;
                Customer.CreditStatus = activityDto.CreditStatus;
                Customer.CustomerCode = activityDto.CustomerCode;
                //Customer.CustomerGroup = activityDto.CustomerGroup;
                Customer.Description = activityDto.Description;
                Customer.Email = activityDto.Email;
                Customer.Fax = activityDto.Fax;
                Customer.Industry = activityDto.Industry;
                Customer.LastCalledDate = activityDto.LastCalledDate;
                Customer.LeadSource = activityDto.LeadSource;
                Customer.LeadStatus = activityDto.LeadStatus;
                Customer.LitersBy = activityDto.LitersBy;
                Customer.Mobile = activityDto.Mobile;
                Customer.ModifiedBy = activityDto.ModifiedBy;
                Customer.ModifiedDate = activityDto.ModifiedDate;
                Customer.Name = activityDto.Name;
                Customer.NoOfEmployees = activityDto.NoOfEmployees;
                Customer.PostalCode = activityDto.PostalCode;
                Customer.PotentialLiters = activityDto.PotentialLiters;
                Customer.PreferredContact = activityDto.PreferredContact;
                //Customer.PreviousCustomerCode = activityDto.PreviousCustomerCode;
                //Customer.PreviousCustomerName = activityDto.PreviousCustomerName;
                Customer.Probability = activityDto.Probability;
                Customer.Rating = activityDto.Rating;
                Customer.ReferredBy = activityDto.ReferredBy;
                Customer.RepCode = activityDto.RepCode;
                Customer.ShortName = activityDto.ShortName;
                //Customer.StartDate = activityDto.StartDate;
                Customer.State = activityDto.State;
                Customer.Status = activityDto.Status;
                Customer.Telephone = activityDto.Telephone;
                Customer.Website = activityDto.Website;


                Customer.Periphery = activityDto.Periphery;
                Customer.Volume = activityDto.Volume;
                Customer.OutletType = activityDto.OutletType;
                Customer.Category = activityDto.Category;
                Customer.MarketName = activityDto.MarketName;
                Customer.isTLP = activityDto.isTLP;
                Customer.Longitude = activityDto.Longitude;
                Customer.Latitiude = activityDto.Latitiude;


                Customer.PeripheryId = activityDto.PeripheryId;
                Customer.VolumeId = activityDto.VolumeId;
                Customer.OutletTypeId = activityDto.OutletTypeId;
                Customer.CategoryId = activityDto.CategoryId;
                Customer.MarketId = activityDto.MarketId;
                Customer.CustomerTypeOfApprove = activityDto.CustomerTypeOfApprove;
                Customer.Province = activityDto.Province;
                Customer.GeoLocation = activityDto.GeoLocation;
                Customer.ProductsHandled = activityDto.ProductsHandled;
                Customer.CanvassingFrequency = activityDto.CanvassingFrequency;
                Customer.AsuShare = activityDto.AsuShare;
                Customer.AuthorityAssessment = activityDto.AuthorityAssessment;

                Customer.OutstandingBalance = activityDto.OutstandingBalance;
                Customer.CreditLimit = activityDto.CreditLimit;
                Customer.CashLimit = activityDto.CashLimit;

                Customer.StickTarget = activityDto.StickTarget * 1000;
                Customer.TargetId = activityDto.TargetId;
                Customer.EffStartDate = activityDto.EffStartDate;
                Customer.EffEndDate = activityDto.EffEndDate;

                Customer.TotalCount = activityDto.RowCount;
                Customer.RouteAssignId = activityDto.RouteAssignId;
                Customer.AssignedRouteName = activityDto.AssignedRouteName;

                Customer.ImageContent = activityDto.ImageContent;
                Customer.IsRetail = activityDto.IsRetail;
                Customer.OutletUserCode = activityDto.OutletUserCode;
            }
            catch (Exception)
            {

                throw;
            }
            return Customer;
        }

        public static CustomerDTO ConvertToCustomerDTO(CustomerEntity customer)
        {
            CustomerDTO customerDto = new CustomerDTO();

            try
            {
                if (customer != null)
                {
                    customerDto.Address = customer.Address1;
                    customerDto.Address2 = customer.Address2;
                    customerDto.AnnualRevenue = customer.AnnualRevenue;
                    customerDto.Business = customer.Business;
                    customerDto.BusinessPotential = customer.BusinessPotential;
                    customerDto.City = customer.City;
                    customerDto.Company = customer.Company;
                    customerDto.Country = customer.Country;
                    customerDto.CreatedBy = customer.CreatedBy;
                    customerDto.CreatedDate = customer.CreatedDate;
                    customerDto.CreditStatus = customer.CreditStatus;
                    customerDto.CustomerCode = customer.CustomerCode;
                    customerDto.EndUserCode = customer.EndUserCode;
                    //customerDto.CustomerGroup = customer.CustomerGroup;
                    customerDto.Description = customer.Description;
                    customerDto.Email = customer.Email;
                    customerDto.Fax = customer.Fax;
                    customerDto.Industry = customer.Industry;
                    customerDto.LastCalledDate = customer.LastCalledDate;
                    customerDto.LeadSource = customer.LeadSource;
                    customerDto.LeadStatus = customer.LeadStatus;
                    customerDto.LitersBy = customer.LitersBy;
                    customerDto.Mobile = customer.Mobile;
                    customerDto.ModifiedBy = customer.ModifiedBy;
                    customerDto.ModifiedDate = customer.ModifiedDate;
                    customerDto.Name = customer.Name;
                    customerDto.NoOfEmployees = customer.NoOfEmployees;
                    customerDto.PostalCode = customer.PostalCode;
                    customerDto.PotentialLiters = customer.PotentialLiters;
                    customerDto.PreferredContact = customer.PreferredContact;
                    //customerDto.PreviousCustomerCode = customer.PreviousCustomerCode;
                    //customerDto.PreviousCustomerName = customer.PreviousCustomerName;
                    customerDto.Probability = customer.Probability;
                    customerDto.Rating = customer.Rating;
                    customerDto.ReferredBy = customer.ReferredBy;
                    customerDto.RepCode = customer.RepCode;
                    customerDto.ShortName = customer.ShortName;
                    //customerDto.StartDate = customer.StartDate;
                    customerDto.State = customer.State;
                    customerDto.Status = customer.Status;
                    customerDto.Telephone = customer.Telephone;
                    customerDto.Website = customer.Website;
                    customerDto.PrimaryRepName = customer.PrimaryRepName;
                    customerDto.SecondaryRepName = customer.SecondaryRepName;
                    customerDto.RowCount = customer.TotalCount;
                    customerDto.IsPrimaryDist = ((!string.IsNullOrWhiteSpace(customer.PrimaryDist) && (customer.PrimaryDist == "Y")) ? true : false);
                    customerDto.Periphery = customer.Periphery;
                    customerDto.Volume = customer.Volume;
                    customerDto.OutletType = customer.OutletType;
                    customerDto.Category = customer.Category;
                    customerDto.MarketName = customer.MarketName;
                    customerDto.isTLP = customer.isTLP;
                    customerDto.Longitude = customer.Longitude;
                    customerDto.Latitiude = customer.Latitiude;

                    customerDto.PeripheryId = customer.PeripheryId;
                    customerDto.VolumeId = customer.VolumeId;
                    customerDto.OutletTypeId = customer.OutletTypeId;
                    customerDto.CategoryId = customer.CategoryId;
                    customerDto.MarketId = customer.MarketId;
                    customerDto.CustomerTypeOfApprove = customer.CustomerTypeOfApprove;

                    customerDto.OutstandingBalance = customer.OutstandingBalance;
                    customerDto.CreditLimit = customer.CreditLimit;
                    customerDto.CashLimit = customer.CashLimit;

                    customerDto.StickTarget = customer.StickTarget != 0 ? customer.StickTarget / 1000 : 0;
                    customerDto.TargetId = customer.TargetId;
                    customerDto.EffStartDate = customer.EffStartDate;
                    customerDto.EffEndDate = customer.EffEndDate;

                    customerDto.RowCount = customer.TotalCount;
                    customerDto.AssignedRouteName = customer.AssignedRouteName;
                    customerDto.RouteAssignId = customer.RouteAssignId;
                    customerDto.ImageContent = customer.ImageContent;
                    customerDto.IsRetail = customer.IsRetail;

                    customerDto.OutletUserCode = customer.OutletUserCode;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return customerDto;
        }

        public static CatalogDTO ConvertToCatalogDTO(CatalogEntity catalog)
        {
            CatalogDTO oCatalogDTO = new CatalogDTO();

            try
            {
                if (catalog != null)
                {
                    oCatalogDTO.CatlogId = catalog.CatlogId;
                    oCatalogDTO.CatlogCode = catalog.CatlogCode;
                    oCatalogDTO.Description = catalog.Description;
                    oCatalogDTO.Price = catalog.Price;
                    oCatalogDTO.NextDue = catalog.NextDue;
                    oCatalogDTO.DeliveryFrequency = catalog.DeliveryFrequency;
                    oCatalogDTO.RowCount = catalog.TotalCount;
                    oCatalogDTO.Conversion = catalog.Conversion;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return oCatalogDTO;
        }

        public static CatalogEntity ConvertToCatalog(CatalogDTO catalogDto)
        {
            CatalogEntity oCatalog = CatalogEntity.CreateObject();

            try
            {
                if (catalogDto != null)
                {
                    oCatalog.CatlogCode = catalogDto.CatlogCode;
                    oCatalog.Description = catalogDto.Description;
                    oCatalog.Price = catalogDto.Price;
                    oCatalog.NextDue = catalogDto.NextDue;
                    oCatalog.DeliveryFrequency = catalogDto.DeliveryFrequency;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return oCatalog;
        }

        public static RepCustomerDTO CovertToRepCustomerDTO(RepCustomerViewEntity repCustomer)
        {
            RepCustomerDTO repCustomerDTO = new RepCustomerDTO();
            try
            {
                repCustomerDTO.State = repCustomer.State;
                repCustomerDTO.Originator = repCustomer.Originator;
                repCustomerDTO.Name = repCustomer.Name;
                repCustomerDTO.NewCustomers = repCustomer.NewCustomers;
                repCustomerDTO.CustomersInState = repCustomer.CustomersInState;
                //repCustomerDTO.Target = repCustomer.Target;

                //List<Lead> CustomerList = repCustomer.CustomerList;
                //List<LeadDTO> customerDtoList = new List<LeadDTO>();

                //foreach (Lead item in CustomerList)
                //{
                //    customerDtoList.Add(LeadAdapter.ConvertToLeadDTO(item));
                //}

                //repCustomerDTO.CustomerList = customerDtoList;

                return repCustomerDTO;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static SalesOrderDTO ConvertToSalesOrderDTO(SalesOrderEntity salesOrder)
        {
            SalesOrderDTO oSalesOrderDTO = new SalesOrderDTO();
            try
            {
                if (salesOrder != null)
                {
                    oSalesOrderDTO.SOrderNo = salesOrder.SOrderNo;
                    oSalesOrderDTO.PListNo = salesOrder.PListNo;
                    oSalesOrderDTO.DateRequired = salesOrder.DateRequired;
                    oSalesOrderDTO.CatlogCode = salesOrder.CatlogCode;
                    oSalesOrderDTO.Description = salesOrder.Description;
                    oSalesOrderDTO.OrderDate = salesOrder.OrderDate;
                    oSalesOrderDTO.RequiredQty = salesOrder.RequiredQty;
                    oSalesOrderDTO.Price = salesOrder.Price;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return oSalesOrderDTO;
        }

        public static TargetInfoDTO ConvertToTargetInfoDTO(TargetInfoEntity targetinfo)
        {
            TargetInfoDTO targetinfoDto = new TargetInfoDTO();

            try
            {
                if (targetinfo != null)
                {
                    targetinfoDto.Code = targetinfo.Code;
                    targetinfoDto.Name = targetinfo.Name;
                    targetinfoDto.CustCode = targetinfo.CustCode;
                    targetinfoDto.CustCode = targetinfo.CustCode;
                    targetinfoDto.RepCode = targetinfo.RepCode;
                    targetinfoDto.RepName = targetinfo.RepName;
                    targetinfoDto.TargetMonth1 = targetinfo.TargetMonth1;
                    targetinfoDto.TargetMonth2 = targetinfo.TargetMonth2;
                    targetinfoDto.TargetMonth3 = targetinfo.TargetMonth3;
                    targetinfoDto.TargetMonth4 = targetinfo.TargetMonth4;
                    targetinfoDto.TargetMonth5 = targetinfo.TargetMonth5;
                    targetinfoDto.TargetMonth6 = targetinfo.TargetMonth6;
                    targetinfoDto.TargetMonth7 = targetinfo.TargetMonth7;
                    targetinfoDto.TargetMonth8 = targetinfo.TargetMonth8;
                    targetinfoDto.TargetMonth9 = targetinfo.TargetMonth9;
                    targetinfoDto.TargetMonth10 = targetinfo.TargetMonth10;
                    targetinfoDto.TargetMonth11 = targetinfo.TargetMonth11;
                    targetinfoDto.TargetMonth12 = targetinfo.TargetMonth12;

                    targetinfoDto.TargetYear1 = targetinfo.TargetYear1;
                    targetinfoDto.TargetYear2 = targetinfo.TargetYear2;
                    targetinfoDto.TargetTYTD = targetinfo.TargetTYTD;
                    targetinfoDto.TargetLYTD = targetinfo.TargetLYTD;                    
                    targetinfoDto.Target1 = targetinfo.Target1;
                    targetinfoDto.Target2 = targetinfo.Target2;
                    targetinfoDto.Target3 = targetinfo.Target3;
                    targetinfoDto.Target1Tot = targetinfo.Target1Tot;
                    targetinfoDto.Target2Tot = targetinfo.Target2Tot;
                    targetinfoDto.Target3Tot = targetinfo.Target3Tot;
                    targetinfoDto.TargetTotal = targetinfo.TargetTotal;

                    targetinfoDto.MDTot = targetinfo.MDTot;
                    targetinfoDto.TargetTot = targetinfo.TargetTot;
                    targetinfoDto.TargetYear1Tot = targetinfo.TargetYear1Tot;
                    targetinfoDto.TargetYear2Tot = targetinfo.TargetYear2Tot;
                    targetinfoDto.TargetTYTDTot = targetinfo.TargetTYTDTot;
                    targetinfoDto.TargetLYTDTot = targetinfo.TargetLYTDTot;


                    targetinfoDto.SelectedMonth = targetinfo.SelectedMonth;


                    targetinfoDto.Actual1 = targetinfo.Actual1;
                    targetinfoDto.Actual2 = targetinfo.Actual2;
                    targetinfoDto.Actual3 = targetinfo.Actual3;

                    targetinfoDto.ActualYear1 = targetinfo.ActualYear1;
                    targetinfoDto.ActualYear2 = targetinfo.ActualYear2;
                    targetinfoDto.ActualTYTD = targetinfo.ActualTYTD;
                    targetinfoDto.ActualLYTD = targetinfo.ActualLYTD;   

                    targetinfoDto.Actual1Tot = targetinfo.Actual1Tot;
                    targetinfoDto.Actual2Tot = targetinfo.Actual2Tot;
                    targetinfoDto.Actual3Tot = targetinfo.Actual3Tot;
                    targetinfoDto.ActualTotal = targetinfo.ActualTotal;

                    targetinfoDto.MDTot = targetinfo.MDTot;
                    targetinfoDto.ActualTot = targetinfo.ActualTot;
                    targetinfoDto.ActualYear1Tot = targetinfo.ActualYear1Tot;
                    targetinfoDto.ActualYear2Tot = targetinfo.ActualYear2Tot;
                    targetinfoDto.ActualTYTDTot = targetinfo.ActualTYTDTot;
                    targetinfoDto.ActualLYTDTot = targetinfo.ActualLYTDTot;
                    targetinfoDto.TargetPercentage = targetinfo.TargetPercentage;
                    targetinfoDto.ActualPercentage = targetinfo.ActualPercentage;

                    targetinfoDto.RowCount = targetinfo.RowCount;


                    targetinfoDto.ActualMonth1 = targetinfo.ActualMonth1;
                    targetinfoDto.ActualMonth2 = targetinfo.ActualMonth2;
                    targetinfoDto.ActualMonth3 = targetinfo.ActualMonth3;
                    targetinfoDto.ActualMonth4 = targetinfo.ActualMonth4;
                    targetinfoDto.ActualMonth5 = targetinfo.ActualMonth5;
                    targetinfoDto.ActualMonth6 = targetinfo.ActualMonth6;
                    targetinfoDto.ActualMonth7 = targetinfo.ActualMonth7;
                    targetinfoDto.ActualMonth8 = targetinfo.ActualMonth8;
                    targetinfoDto.ActualMonth9 = targetinfo.ActualMonth9;
                    targetinfoDto.ActualMonth10 = targetinfo.ActualMonth10;
                    targetinfoDto.ActualMonth11 = targetinfo.ActualMonth11;
                    targetinfoDto.ActualMonth12 = targetinfo.ActualMonth12;

                    targetinfoDto.TyTodayActual = targetinfo.TyTodayActual;
                    targetinfoDto.TyTtmtdActual = targetinfo.TyTtmtdActual;
                    targetinfoDto.TyLmtdActual = targetinfo.TyLmtdActual;
                    targetinfoDto.LyTmtdActual = targetinfo.LyTmtdActual;
                    targetinfoDto.TyTodayActualTot = targetinfo.TyTodayActualTot;
                    targetinfoDto.TyTtmtdActualTot = targetinfo.TyTtmtdActualTot;
                    targetinfoDto.TyLmtdActualTot = targetinfo.TyLmtdActualTot;
                    targetinfoDto.LyTmtdActualTot = targetinfo.LyTmtdActualTot;
                    
                }
            }
            catch (Exception)
            {

                throw;
            }
            return targetinfoDto;
        }

        #endregion

        #region - Properties -

        //private brCustomer oBrCustomer = null;

        #endregion

        #region - Constructor -

        public CustomerAdapter()
        {
            //if (oBrCustomer == null)
            //    oBrCustomer = new brCustomer();
        }

        #endregion

        #region - Contact Person Image -
        public ContactPersonImageDTO GetContactPersonImage(int contactPersonId, string custCode, string imgLocation, string sessionId)
        {
            try
            {
                return CommonAdapter.ConvertToContactPersonDTO(ContactPersonImageBR.Instance.GetContactPersonImage(contactPersonId, custCode), imgLocation, sessionId);
            }

            catch (Exception)
            {

                throw;
            }
        }
        #endregion

        #region - Customer -

        public bool SaveCustomerAddress(LeadAddressDTO customerAddressDto)
        {
            try
            {
                return CustomerBR.Instance.SaveCustomerAddress(LeadAddressAdapter.ConvertToLeadAddress(customerAddressDto));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public CustomerDTO GetCustomer(string customerCode)
        {
            try
            {
                return ConvertToCustomerDTO(CustomerBR.Instance.GetCustomer(customerCode));
            }
            catch (Exception)
            {

                throw;
            }

        }

        public CustomerDTO GetCustomerPending(string customerCode)
        {
            try
            {
                return ConvertToCustomerDTO(CustomerBR.Instance.GetCustomerPending(customerCode));
            }
            catch (Exception)
            {

                throw;
            }

        }

        public List<LeadAddressDTO> GetCustomerAddresses(string customerCode, ArgsDTO args)
        {
            List<LeadAddressDTO> leadAddressDtoList = new List<LeadAddressDTO>();

            try
            {
                List<LeadAddressEntity> leadAddresses = CustomerBR.Instance.GetCustomerAddresses(customerCode,ActivityAdapter.ConvertToArgsEntity(args));

                foreach (LeadAddressEntity item in leadAddresses)
                {
                    leadAddressDtoList.Add(LeadAddressAdapter.ConvertToLeadAddressDTO(item));
                }
            }
            catch (Exception)
            {

                throw;
            }

            return leadAddressDtoList;
        }

        public List<RepCustomerDTO> GetNewCustomers(ArgsDTO args)
        {
            try
            {
                List<RepCustomerViewEntity> repCustomerList = CustomerBR.Instance.GetNewCustomerCount(ActivityAdapter.ConvertToArgsEntity(args));
                List<RepCustomerDTO> repCustomerDtoList = new List<RepCustomerDTO>();

                foreach (RepCustomerViewEntity item in repCustomerList)
                {
                    repCustomerDtoList.Add(CovertToRepCustomerDTO(item));
                }
                return repCustomerDtoList;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public LeadCustomerDTO GetCustomerView(ArgsDTO args)
        {
            try
            {
                return LeadCustomerAdapter.ConvertToLeadCustomer(CustomerBR.Instance.GetCustomerView(args.CustomerCode, args.DefaultDepartmentId));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public LeadAddressDTO GetCustomerTableAddresses(string custCode)
        {
            try
            {
                return LeadAddressAdapter.ConvertToLeadAddressDTO(CustomerBR.Instance.GetCustomerTableAddresses(custCode));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<SalesOrderDTO> GetOutstandingDeliveries(string custCode)
        {
            try
            {
                List<SalesOrderDTO> salesOrderDTOList = new List<SalesOrderDTO>();
                List<SalesOrderEntity> salesOrderList = CustomerBR.Instance.GetOutstandingDeliveries(custCode);
                foreach (SalesOrderEntity item in salesOrderList)
                    salesOrderDTOList.Add(ConvertToSalesOrderDTO(item));

                return salesOrderDTOList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int GetOutstandingDeliveriesCount(string custCode)
        {
            try
            {
                return CustomerBR.Instance.GetOutstandingDeliveriesCount(custCode);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public DateTime GetLastDelivery(string custCode)
        {
            try
            {
                return CustomerBR.Instance.GetLastDelivery(custCode);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<TargetInfoDTO> GetCustomerMonthleyTargetsByDistributor(string username)
        {
            try
            {
                List<TargetInfoDTO> tarInfoDTOList = new List<TargetInfoDTO>();
                List<TargetInfoEntity> targetInfoList = CustomerBR.Instance.GetCustomerMonthleyTargetsByDistributor(username);
                foreach (TargetInfoEntity item in targetInfoList)
                    tarInfoDTOList.Add(ConvertToTargetInfoDTO(item));

                return tarInfoDTOList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        #region - Methods -
   
        public bool SaveCustomer(CustomerDTO customerDto)
        {
            try
            {
                return CustomerBR.Instance.SaveCustomer(ConvertToCustomer(customerDto));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<KeyValuePair<int, string>> GetAllOutletTypes()
        {
            try
            {
                return CustomerBR.Instance.GetAllOutletTypes();
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<KeyValuePair<string, string>> GetAllTowns()
        {
            try
            {
                return CustomerBR.Instance.GetAllTowns();
            }
            catch (Exception)
            {

                throw;
            }
        }

        //public bool UploadExcelfile(string FilePath, string Extension, string UserName, string ASEOriginator, string DistOriginator, string RepOriginator)
        //{
        //    try
        //    {
        //        return CustomerBR.Instance.UploadExcelfile(FilePath, Extension, UserName, ASEOriginator, DistOriginator, RepOriginator);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}
        
        public bool UploadExcelfile(string FilePath, string Extension, string UserName, string TerritoryId)
        {
            try
            {
                return CustomerBR.Instance.UploadExcelfile(FilePath, Extension, UserName, TerritoryId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteCustomerByCustomerCode(CustomerDTO delCustomer)
        {
            try
            {
                return CustomerBR.Instance.DeleteCustomerFromSystem(delCustomer.CustomerCode);
            }
            catch
            {
                throw;
            }
        }

        //public DataTable ReviewExcelfile(string FilePath, string Extension, string UserName)
        //{
        //    try
        //    {
        //        return CustomerBR.Instance.ReviewExcelfile(FilePath, Extension, UserName);
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //}

        //public List<PromotionDTO> GetPromotionsForCustomer(string custcode)
        //{
        //    try
        //    {
        //        return CustomerBR.Instance.GetPromotionsForCustomer(ConvertToCustomerDTO(custcode));
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}
        /*
   public List<CatalogDTO> GetRecentProducts(string customerCode)
   {
       List<CatalogDTO> catalogDTOList = new List<CatalogDTO>();

       try
       {
           List<Catalog> catalog = oBrCustomer.GetRecentProducts(customerCode);

           foreach (Catalog item in catalog)
           {
               catalogDTOList.Add(ConvertToCatalogDTO(item));
           }
       }
       catch (Exception)
       {

           throw;
       }

       return catalogDTOList;
   }

   public bool IsWasteCustomer(string custCode)
   {
       try
       {
           return oBrCustomer.IsWasteCustomer(custCode);
       }
       catch (Exception)
       {

           throw;
       }
   }
   */

        public bool DeleteProductFlavor(int flavorId)
        {
            try
            {

                return FlavorBR.Instance.DeleteProductFlavor(flavorId);
            }
            catch (Exception)
            {
                throw;
            }
        }


        #endregion
    }
}
