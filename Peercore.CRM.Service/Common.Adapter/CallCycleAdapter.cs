﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Service.DTO.CompositeEntities;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.Entities;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class CallCycleAdapter
    {
        #region - Converters -

        public static CallCycleEntity ConvertToCallCycleEntity(CallCycleDTO callCycleDto)
        {
            CallCycleEntity oCallCycle = CallCycleEntity.CreateObject();
            try
            {
                oCallCycle.CallCycleID = callCycleDto.CallCycleID;
                oCallCycle.CCType= callCycleDto.CCType;
                oCallCycle.Comments = callCycleDto.Comments;
                oCallCycle.CreatedBy = callCycleDto.CreatedBy;
                oCallCycle.CreatedDate = callCycleDto.CreatedDate;
                oCallCycle.DelFlag = callCycleDto.DelFlag;
                oCallCycle.Description = callCycleDto.Description;
                oCallCycle.DueOn = callCycleDto.DueOn;
                //oCallCycle.IsDirty = callCycleDto.IsDirty;
                //oCallCycle.IsNew = callCycleDto.IsNew;
                oCallCycle.LastModifiedBy = callCycleDto.LastModifiedBy;
                oCallCycle.LastModifiedDate = callCycleDto.LastModifiedDate;
                oCallCycle.NoOfLeadCustomers = callCycleDto.NoOfLeadCustomers;
                oCallCycle.PrefixCode = callCycleDto.PrefixCode;
                oCallCycle.TotalCount = callCycleDto.RowCount;
                oCallCycle.RepName = callCycleDto.RepName;
            }
            catch (Exception)
            {

                throw;
            }
            return oCallCycle;
        }

        public static CallCycleDTO ConvertToCallCycleDTO(CallCycleEntity callCycleEntity)
        {
            CallCycleDTO oCallCycleDTO = new CallCycleDTO();

            if (callCycleEntity != null)
            {
                try
                {
                    oCallCycleDTO.CallCycleID = callCycleEntity.CallCycleID;
                    oCallCycleDTO.CCType = callCycleEntity.CCType;
                    oCallCycleDTO.Comments = callCycleEntity.Comments;
                    oCallCycleDTO.CreatedBy = callCycleEntity.CreatedBy;
                    oCallCycleDTO.CreatedDate = callCycleEntity.CreatedDate;
                    oCallCycleDTO.DelFlag = callCycleEntity.DelFlag;
                    oCallCycleDTO.Description = callCycleEntity.Description;
                    oCallCycleDTO.DueOn = callCycleEntity.DueOn;
                    oCallCycleDTO.LastModifiedBy = callCycleEntity.LastModifiedBy;
                    oCallCycleDTO.LastModifiedDate = callCycleEntity.LastModifiedDate;
                    oCallCycleDTO.NoOfLeadCustomers = callCycleEntity.NoOfLeadCustomers;
                    oCallCycleDTO.PrefixCode = callCycleEntity.PrefixCode;
                    oCallCycleDTO.RowCount = callCycleEntity.TotalCount;
                    oCallCycleDTO.RepName = callCycleEntity.RepName;
                }
                catch (Exception)
                {

                    throw;
                }
            }
            return oCallCycleDTO;
        }

        public static List<CallCycleDTO> ConvertToActivities(List<CallCycleEntity> callCycleActivities)
        {
            List<CallCycleDTO> lstCallCycle = new List<CallCycleDTO>();
            CallCycleDTO callCycleDto = null;

            try
            {
                foreach (CallCycleEntity obj in callCycleActivities)
                {
                    callCycleDto = new CallCycleDTO();
                    callCycleDto.CallCycleID = obj.CallCycleID;
                    callCycleDto.CCType = obj.CCType;
                    callCycleDto.Comments = obj.Comments;
                    callCycleDto.CreatedBy = obj.CreatedBy;
                    callCycleDto.CreatedDate = obj.CreatedDate;
                    callCycleDto.DelFlag = obj.DelFlag;
                    callCycleDto.Description = obj.Description;
                    callCycleDto.DueOn = obj.DueOn;
                    callCycleDto.LastModifiedBy = obj.LastModifiedBy;
                    callCycleDto.LastModifiedDate = obj.LastModifiedDate;
                    callCycleDto.NoOfLeadCustomers = obj.NoOfLeadCustomers;
                    callCycleDto.PrefixCode = obj.PrefixCode;
                    callCycleDto.RowCount = obj.TotalCount;
                    callCycleDto.RepName = obj.RepName;

                    lstCallCycle.Add(callCycleDto);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstCallCycle;
        }

        public static List<CallCycleContactDTO> ConvertToCallCycleContactList(List<CallCycleContactEntity> callCycleContact)
        {
            List<CallCycleContactDTO> lstCallCycleContact = new List<CallCycleContactDTO>();
            CallCycleContactDTO callCycleContactDto = null;

            try
            {
                foreach (CallCycleContactEntity obj in callCycleContact)
                {
                    callCycleContactDto = new CallCycleContactDTO();
                    callCycleContactDto.CallCycle = ConvertToCallCycleDTO(obj.CallCycle);
                    callCycleContactDto.Contact = LeadCustomerAdapter.ConvertToLeadCustomer(obj.Contact);
                    callCycleContactDto.CreateActivity = obj.CreateActivity;
                    callCycleContactDto.DayOrder = obj.DayOrder;
                    callCycleContactDto.DueOn = obj.DueOn;
                    callCycleContactDto.Originator = obj.Originator;
                    callCycleContactDto.PrefixCode = obj.PrefixCode;
                    callCycleContactDto.StartTime = obj.StartTime;
                    callCycleContactDto.WeekDayId = obj.WeekDayId;
                    callCycleContactDto.RowCount = obj.TotalCount;
                    callCycleContactDto.ItemIndex = obj.ItemIndex;
                    callCycleContactDto.LeadStage = LeadStageAdapter.ConvertToLeadStageDTO(obj.LeadStage);
                    callCycleContactDto.CreateActivity = false;
                    callCycleContactDto.RouteId = obj.RouteId;
                    lstCallCycleContact.Add(callCycleContactDto);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstCallCycleContact;
        }

        public static List<CallCycleContactEntity> ConvertToCallCycleContactDtoList(List<CallCycleContactDTO> callCycleContact)
        {
            List<CallCycleContactEntity> lstCallCycleContact = new List<CallCycleContactEntity>();
            CallCycleContactEntity callCycleContactDto = null;

            try
            {
                if (callCycleContact != null)
                {
                    foreach (CallCycleContactDTO obj in callCycleContact)
                    {
                        callCycleContactDto = CallCycleContactEntity.CreateObject();

                        callCycleContactDto.CallCycle = ConvertToCallCycleEntity(obj.CallCycle);
                        callCycleContactDto.Contact = LeadCustomerAdapter.ConvertToLeadCustomerView(obj.Contact);
                        callCycleContactDto.CreateActivity = obj.CreateActivity;
                        callCycleContactDto.DayOrder = obj.DayOrder;
                        callCycleContactDto.DueOn = obj.DueOn;
                        callCycleContactDto.Originator = obj.Originator;
                        callCycleContactDto.PrefixCode = obj.PrefixCode;
                        callCycleContactDto.StartTime = obj.StartTime;
                        callCycleContactDto.WeekDayId = obj.WeekDayId;
                        callCycleContactDto.TotalCount = obj.RowCount;
                        callCycleContactDto.ItemIndex = obj.ItemIndex;
                        callCycleContactDto.RouteId = obj.RouteId;
                        if (obj.LeadStage != null)
                            callCycleContactDto.LeadStage = LeadStageAdapter.ConvertToLeadStage(obj.LeadStage);
                        else
                            callCycleContactDto.LeadStage = LeadStageEntity.CreateObject();
                        lstCallCycleContact.Add(callCycleContactDto);
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
            return lstCallCycleContact;
        }


        public static RouteDTO ConvertToPRouteDto(RouteEntity oRoute)
        {
            RouteDTO routeDto = new RouteDTO();

            routeDto.RouteMasterId = oRoute.RouteMasterId;
            routeDto.RouteName = oRoute.RouteName;
            routeDto.RepCode = oRoute.RepCode;
            routeDto.RepName = oRoute.RepName;
            routeDto.CreatedBy = oRoute.CreatedBy;
            routeDto.RowCount = oRoute.TotalCount;
            routeDto.IsTempRep = oRoute.IsTempRep;
            routeDto.TempRepAssignDate = oRoute.TempRepAssignDate;
            routeDto.routeSequence = oRoute.routeSequence;
            routeDto.OriginatorType = oRoute.OriginatorType;
            routeDto.TMECode = oRoute.TMECode;

            return routeDto;

        }

        public static RouteEntity ConvertToRouteDTO(RouteDTO routeDto)
        {
            RouteEntity oRoute = RouteEntity.CreateObject();

            oRoute.RouteMasterId = routeDto.RouteMasterId;
            oRoute.RouteName = routeDto.RouteName;
            oRoute.RepCode = routeDto.RepCode;
            oRoute.CreatedBy = routeDto.CreatedBy;
            oRoute.TotalCount = routeDto.RowCount;
            oRoute.IsTempRep = routeDto.IsTempRep;
            oRoute.TempRepAssignDate = routeDto.TempRepAssignDate;
            oRoute.routeSequence = routeDto.routeSequence;
            oRoute.OriginatorType = routeDto.OriginatorType;
            oRoute.TMECode = routeDto.TMECode;

            return oRoute;
        }

        public static List<RouteEntity> ConvertToRouteEntityList(List<RouteDTO> routeDTOList)
        {
            List<RouteEntity> routeEntityList = new List<RouteEntity>();
            foreach (RouteDTO routeDTO in routeDTOList)
            {
                routeEntityList.Add(ConvertToRouteDTO(routeDTO));
            }
            return routeEntityList;
        }

        public static List<RouteDTO> ConvertToRouteDTOList(List<RouteEntity> routeEntityList)
        {
            List<RouteDTO> routeDTOList = new List<RouteDTO>();
            foreach (RouteEntity routeEntity in routeEntityList)
            {
                routeDTOList.Add(ConvertToPRouteDto(routeEntity));
            }
            return routeDTOList;
        }

        #region RouteMaster
        public static RouteMasterDTO ConvertToRouteMasterDTO(RouteMasterEntity objectEntity)
        {
            RouteMasterDTO objectDTO = new RouteMasterDTO();

            objectDTO.RouteMasterId = objectEntity.RouteMasterId;
            objectDTO.RouteName = objectEntity.RouteMasterName;
            objectDTO.Status = objectEntity.Status;
            objectDTO.CreatedBy = objectEntity.CreatedBy;
            objectDTO.CreatedDate = objectEntity.CreatedDate.Value;
            objectDTO.LastModifiedBy = objectEntity.LastModifiedBy;
            objectDTO.LastModifiedDate = objectEntity.LastModifiedDate.Value;
            objectDTO.RowCount = objectEntity.TotalCount;
            objectDTO.Target = Math.Round(objectEntity.Target != 0 ? objectEntity.Target / 1000 : 0, 3);
            objectDTO.TargetId = objectEntity.TargetId;

            objectDTO.EffStartDate = objectEntity.EffStartDate;
            objectDTO.EffEndDate = objectEntity.EffEndDate;
            objectDTO.RouteType = objectEntity.RouteType;
            objectDTO.RepCode = objectEntity.RepCode;
            objectDTO.RepName = objectEntity.RepName;
            objectDTO.CreatedByName = objectEntity.CreatedByName;
            objectDTO.CallCycleId = objectEntity.CallCycleId;
            objectDTO.HasSelect = objectEntity.HasSelect;

            return objectDTO;
        }

        public static RouteMasterEntity ConvertToRouteMasterEntity(RouteMasterDTO objectDTO)
        {
            RouteMasterEntity objectEntity = RouteMasterEntity.CreateObject();

            objectEntity.RouteMasterId = objectDTO.RouteMasterId;
            objectEntity.RouteMasterName = objectDTO.RouteName;
            objectEntity.Status = objectDTO.Status;
            objectEntity.CreatedBy = objectDTO.CreatedBy;
            objectEntity.CreatedDate = objectDTO.CreatedDate;
            objectEntity.LastModifiedBy = objectDTO.LastModifiedBy;
            objectEntity.LastModifiedDate = objectDTO.LastModifiedDate;
            objectEntity.TotalCount = objectDTO.RowCount;
            objectEntity.Target = objectDTO.Target * 1000;
            objectEntity.TargetId = objectDTO.TargetId;

            objectEntity.EffStartDate = objectDTO.EffStartDate;
            objectEntity.EffEndDate = objectDTO.EffEndDate;
            objectEntity.RouteType = objectDTO.RouteType;

            objectEntity.RepCode = objectDTO.RepCode;
            objectEntity.RepName = objectDTO.RepName;
            objectEntity.CreatedByName = objectDTO.CreatedByName;
            objectEntity.CallCycleId = objectDTO.CallCycleId;

            return objectEntity;
        }

        public static List<RouteMasterDTO> ConvertToRouteMasterDTOList(List<RouteMasterEntity> entityList)
        {
            List<RouteMasterDTO> dtoList = new List<RouteMasterDTO>();
            foreach (RouteMasterEntity routeEntity in entityList)
            {
                dtoList.Add(ConvertToRouteMasterDTO(routeEntity));
            }
            return dtoList;
        }

        public static List<RouteMasterEntity> ConvertToRouteMasterEntityList(List<RouteMasterDTO> dtoList)
        {
            List<RouteMasterEntity> entityList = new List<RouteMasterEntity>();
            foreach (RouteMasterDTO routeDTO in dtoList)
            {
                entityList.Add(ConvertToRouteMasterEntity(routeDTO));
            }
            return entityList;
        }
        #endregion

        #region OriginatorRouteOutlets

        public static OriginatorRouteOutletsDTO ConvertToOriginatorRouteOutletsDTO(OriginatorRouteOutletsEntity objectIn)
        {
            OriginatorRouteOutletsDTO objectOut = new OriginatorRouteOutletsDTO();
            try
            {
                if (objectIn != null)
                {
                    objectOut.Originator = CommonAdapter.ConvertToOriginatorDTO(objectIn.Originator);
                    objectOut.Route = ConvertToPRouteDto(objectIn.Route);
                    if (objectIn.Outlets != null)
                        objectOut.Outlets = ConvertToCallCycleContactList(objectIn.Outlets);
                    objectOut.DelFlag = objectIn.DelFlag;
                    objectOut.CcType = objectIn.CcType;

                    objectOut.CreatedBy = objectIn.CreatedBy;
                    objectOut.CreatedDate = objectIn.CreatedDate;
                    objectOut.LastModifiedBy = objectIn.LastModifiedBy;
                    objectOut.LastModifiedDate = objectIn.LastModifiedDate;

                    objectOut.RowCount = objectIn.TotalCount;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objectOut;
        }

        public static OriginatorRouteOutletsEntity ConvertToOriginatorRouteOutletsEntity(OriginatorRouteOutletsDTO objectIn)
        {
            OriginatorRouteOutletsEntity objectOut = OriginatorRouteOutletsEntity.CreateObject();
            try
            {
                if (objectIn != null)
                {
                    objectOut.Originator = CommonAdapter.ConvertToOriginator(objectIn.Originator);
                    objectOut.Route = ConvertToRouteDTO(objectIn.Route);
                    objectOut.Outlets = ConvertToCallCycleContactDtoList(objectIn.Outlets);
                    objectOut.DelFlag = objectIn.DelFlag;
                    objectOut.CcType = objectIn.CcType;

                    objectOut.CreatedBy = objectIn.CreatedBy;
                    objectOut.CreatedDate = objectIn.CreatedDate;
                    objectOut.LastModifiedBy = objectIn.LastModifiedBy;
                    objectOut.LastModifiedDate = objectIn.LastModifiedDate;

                    objectOut.TotalCount = objectIn.RowCount;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objectOut;
        }

        public static List<OriginatorRouteOutletsDTO> ConvertToOriginatorRouteOutletsDTOList(List<OriginatorRouteOutletsEntity> listIn)
        {
            List<OriginatorRouteOutletsDTO> listOut = new List<OriginatorRouteOutletsDTO>();
            foreach (OriginatorRouteOutletsEntity item in listIn)
            {
                listOut.Add(ConvertToOriginatorRouteOutletsDTO(item));
            }
            return listOut;
        }

        public static List<OriginatorRouteOutletsEntity> ConvertToOriginatorRouteOutletsEntityList(List<OriginatorRouteOutletsDTO> listIn)
        {
            List<OriginatorRouteOutletsEntity> listOut = new List<OriginatorRouteOutletsEntity>();
            foreach (OriginatorRouteOutletsDTO item in listIn)
            {
                listOut.Add(ConvertToOriginatorRouteOutletsEntity(item));
            }
            return listOut;
        }

        #endregion

        #region Itinerary

        public static ItineraryDetailDTO ConvertToItineraryDTO(ItineraryDetailEntity oItinerary)
        {
            ItineraryDetailDTO itineraryDto = new ItineraryDetailDTO();


            itineraryDto.Date = oItinerary.Date;
            itineraryDto.TownsToVisit = oItinerary.TownsToVisit;
            itineraryDto.Sequence = oItinerary.Sequence;
            itineraryDto.DirectMiles = oItinerary.DirectMiles;
            itineraryDto.LocalMiles = oItinerary.LocalMiles;
            itineraryDto.TotalMiles = oItinerary.TotalMiles;
            itineraryDto.WorkProgram = oItinerary.WorkProgram;
            itineraryDto.NightAt = oItinerary.NightAt;
            itineraryDto.ActivityId = oItinerary.ActivityId;
            itineraryDto.RouteId = oItinerary.RouteId;
            itineraryDto.ItineraryDetailId = oItinerary.ItineraryDetailId;
            itineraryDto.ItineraryHeaderId = oItinerary.ItineraryHeaderId;
            itineraryDto.CreatedBy = oItinerary.CreatedBy;
            itineraryDto.CreatedDate = oItinerary.CreatedDate;
            itineraryDto.LastModifiedBy = oItinerary.LastModifiedBy;
            itineraryDto.LastModifiedDate = oItinerary.LastModifiedDate;
            itineraryDto.SequenceRoute = oItinerary.SequenceRoute;
            itineraryDto.DayTypeColor = oItinerary.DayTypeColor;

            return itineraryDto;

        }

        public static ItineraryDetailEntity ConvertToItineraryEntity(ItineraryDetailDTO itineraryDto)
        {
            ItineraryDetailEntity oItinerary = ItineraryDetailEntity.CreateObject();

            oItinerary.Date = itineraryDto.Date;
            oItinerary.TownsToVisit = itineraryDto.TownsToVisit;
            oItinerary.Sequence = itineraryDto.Sequence;
            oItinerary.DirectMiles = itineraryDto.DirectMiles;
            oItinerary.LocalMiles = itineraryDto.LocalMiles;
            oItinerary.TotalMiles = itineraryDto.TotalMiles;
            oItinerary.WorkProgram = itineraryDto.WorkProgram;
            oItinerary.NightAt = itineraryDto.NightAt;
            oItinerary.ActivityId = itineraryDto.ActivityId;
            oItinerary.RouteId = itineraryDto.RouteId;
            oItinerary.ItineraryDetailId = itineraryDto.ItineraryDetailId;
            oItinerary.ItineraryHeaderId = itineraryDto.ItineraryHeaderId;
            oItinerary.CreatedBy = itineraryDto.CreatedBy;
            oItinerary.CreatedDate = itineraryDto.CreatedDate;
            oItinerary.LastModifiedBy = itineraryDto.LastModifiedBy;
            oItinerary.LastModifiedDate = itineraryDto.LastModifiedDate;
            oItinerary.SequenceRoute = itineraryDto.SequenceRoute;
            oItinerary.DayTypeColor = itineraryDto.DayTypeColor;

            return oItinerary;
        }

        public static List<ItineraryDetailEntity> ConvertToItineraryEntityList(List<ItineraryDetailDTO> itineraryDTOList)
        {
            List<ItineraryDetailEntity> itineraryEntityList = new List<ItineraryDetailEntity>();
            foreach (ItineraryDetailDTO itineraryeDTO in itineraryDTOList)
            {
                itineraryEntityList.Add(ConvertToItineraryEntity(itineraryeDTO));
            }
            return itineraryEntityList;
        }

        public static List<ItineraryDetailDTO> ConvertToItineraryDTOList(List<ItineraryDetailEntity> itineraryEntityList)
        {
            List<ItineraryDetailDTO> itineraryDTOList = new List<ItineraryDetailDTO>();
            foreach (ItineraryDetailEntity itineraryEntity in itineraryEntityList)
            {
                itineraryDTOList.Add(ConvertToItineraryDTO(itineraryEntity));
            }
            return itineraryDTOList;
        }

        public static ItineraryHeaderDTO ConvertToItineraryHeaderDTO(ItineraryHeaderEntity objectIn)
        {
            ItineraryHeaderDTO objectOut = new ItineraryHeaderDTO();

            objectOut.ItineraryHeaderId = objectIn.ItineraryHeaderId;
            objectOut.CallCycleId = objectIn.CallCycleId;
            objectOut.AssignedTo = objectIn.AssignedTo;
            objectOut.BaseTown = objectIn.BaseTown;
            objectOut.ItineraryName = objectIn.ItineraryName;
            objectOut.Status = objectIn.Status;
            objectOut.HasSentMail = objectIn.HasSentMail;
            objectOut.Comments = objectIn.Comments;
            objectOut.Month = objectIn.Month;
            objectOut.ItineraryLogEntryId = objectIn.ItineraryLogEntryId;
            objectOut.RowCount = objectIn.TotalCount;
            objectOut.CreatedBy = objectIn.CreatedBy;
            objectOut.CreatedDate = objectIn.CreatedDate;
            objectOut.LastModifiedBy = objectIn.LastModifiedBy;
            objectOut.LastModifiedDate = objectIn.LastModifiedDate;
            if (objectIn.ItineraryDetailList != null)
                objectOut.ItineraryDetailList = ConvertToItineraryDTOList(objectIn.ItineraryDetailList);
            objectOut.EffDate = objectIn.EffDate;
            objectOut.PrivateField = objectIn.PrivateField;
            objectOut.Contigency = objectIn.Contigency;


            return objectOut;

        }

        public static ItineraryHeaderEntity ConvertToItineraryHeaderEntity(ItineraryHeaderDTO objectIn)
        {
            ItineraryHeaderEntity objectOut = new ItineraryHeaderEntity();

            objectOut.ItineraryHeaderId = objectIn.ItineraryHeaderId;
            objectOut.CallCycleId = objectIn.CallCycleId;
            objectOut.AssignedTo = objectIn.AssignedTo;
            objectOut.BaseTown = objectIn.BaseTown;
            objectOut.ItineraryName = objectIn.ItineraryName;
            objectOut.Status = objectIn.Status;
            objectOut.HasSentMail = objectIn.HasSentMail;
            objectOut.Comments = objectIn.Comments;
            objectOut.Month = objectIn.Month;
            objectOut.ItineraryLogEntryId = objectIn.ItineraryLogEntryId;
            objectOut.TotalCount = objectIn.RowCount;
            objectOut.CreatedBy = objectIn.CreatedBy;
            objectOut.CreatedDate = objectIn.CreatedDate;
            objectOut.LastModifiedBy = objectIn.LastModifiedBy;
            objectOut.LastModifiedDate = objectIn.LastModifiedDate;
            if (objectIn.ItineraryDetailList != null)
                objectOut.ItineraryDetailList = ConvertToItineraryEntityList(objectIn.ItineraryDetailList);
            objectOut.EffDate = objectIn.EffDate;
            objectOut.PrivateField = objectIn.PrivateField;
            objectOut.Contigency = objectIn.Contigency;

            return objectOut;

        }
        

        #endregion

        #region VisitCheckList

        public static VisitChecklistMasterDTO ConvertToVisitChecklistMasterDTO(VisitChecklistMasterEntity objectIn)
        {
            VisitChecklistMasterDTO objectOut = new VisitChecklistMasterDTO();

            objectOut.VisitChecklistMasterId = objectIn.VisitChecklistMasterId;
            objectOut.Name = objectIn.Name;
            objectOut.Description = objectIn.Description;
            objectOut.EffectiveStartDate = objectIn.EffectiveStartDate;
            objectOut.EffectiveEndDate = objectIn.EffectiveEndDate;
            objectOut.Status = objectIn.Status;
            objectOut.CreatedBy = objectIn.CreatedBy;
            objectOut.CreatedDate = objectIn.CreatedDate;
            objectOut.LastModifiedBy = objectIn.LastModifiedBy;
            objectOut.LastModifiedDate = objectIn.LastModifiedDate;
            objectOut.RowCount = objectIn.TotalCount;

            return objectOut;
        }

        public static VisitChecklistMasterEntity ConvertToVisitChecklistMasterEntity(VisitChecklistMasterDTO objectIn)
        {
            VisitChecklistMasterEntity objectOut = new VisitChecklistMasterEntity();

            objectOut.VisitChecklistMasterId = objectIn.VisitChecklistMasterId;
            objectOut.Name = objectIn.Name;
            objectOut.Description = objectIn.Description;
            objectOut.EffectiveStartDate = objectIn.EffectiveStartDate;
            objectOut.EffectiveEndDate = objectIn.EffectiveEndDate;
            objectOut.Status = objectIn.Status;
            objectOut.CreatedBy = objectIn.CreatedBy;
            objectOut.CreatedDate = objectIn.CreatedDate;
            objectOut.LastModifiedBy = objectIn.LastModifiedBy;
            objectOut.LastModifiedDate = objectIn.LastModifiedDate;
            objectOut.TotalCount = objectIn.RowCount;

            return objectOut;
        }

        public static List<VisitChecklistMasterDTO> ConvertToVisitChecklistMasterDTOList(List<VisitChecklistMasterEntity> listIn)
        {
            List<VisitChecklistMasterDTO> listOut = new List<VisitChecklistMasterDTO>();
            foreach (VisitChecklistMasterEntity item in listIn)
            {
                listOut.Add(ConvertToVisitChecklistMasterDTO(item));
            }
            return listOut;
        }

        public static List<VisitChecklistMasterEntity> ConvertToVisitChecklistMasterEntityList(List<VisitChecklistMasterDTO> listIn)
        {
            List<VisitChecklistMasterEntity> listOut = new List<VisitChecklistMasterEntity>();
            foreach (VisitChecklistMasterDTO item in listIn)
            {
                listOut.Add(ConvertToVisitChecklistMasterEntity(item));
            }
            return listOut;
        }

        #endregion

        #endregion

        #region Methods
        public List<CallCycleContactDTO> GetCallCycleContacts(ArgsDTO args, int iCallCycleID = 0, bool bInActive = false, string CallCycleName = "")
        {
            try
            {
                return ConvertToCallCycleContactList(CallCycleBR.Instance.GetCallCycleContacts(ActivityAdapter.ConvertToArgsEntity(args),iCallCycleID,bInActive,CallCycleName));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool GetCallCycleState(int callCycleId)
        {
            try
            {
                return CallCycleBR.Instance.GetCallCycleState(callCycleId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<CallCycleDTO> GetCallCycle(ArgsDTO args, bool bInActive = false, int iCallCycleID = 0, string CallCycleName = "")
        {
            try
            {
                return ConvertToActivities(CallCycleBR.Instance.GetCallCycle(ActivityAdapter.ConvertToArgsEntity(args), bInActive,CallCycleName, iCallCycleID));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool CallCycleSave(CallCycleDTO callCycle, List<CallCycleContactDTO> oCustomers, string originator, TimeSpan tsStartTime, out int callCycleId)
        {
            try
            {
                return CallCycleBR.Instance.Save(ConvertToCallCycleEntity(callCycle), ConvertToCallCycleContactDtoList(oCustomers), originator, tsStartTime, out callCycleId);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeactivateCallCycle(int callCycleId, string isDelete)
        {
            try
            {
                return CallCycleBR.Instance.DeactivateCallCycle(callCycleId, isDelete);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool CallCycleContactDelete(CallCycleContactDTO callCycleContact, int callCycleId)
        {
            try
            {
                List<CallCycleContactDTO> tempList = new List<CallCycleContactDTO>();
                tempList.Add(callCycleContact);
                List<CallCycleContactEntity> callCycleContactDTO = ConvertToCallCycleContactDtoList(tempList);
                return CallCycleBR.Instance.Delete(callCycleContactDTO[0], callCycleId);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool SaveSchedule(List<CallCycleContactDTO> callCycleContactList)
        {
            try
            {
                return CallCycleBR.Instance.SaveSchedule(ConvertToCallCycleContactDtoList(callCycleContactList));
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public bool CreateCallCycleActivitiesFromTemplate(CallCycleDTO callCycle, List<CallCycleContactDTO> callCycleContact, ArgsDTO args)
        {
            try
            {
                return CallCycleBR.Instance.CreateCallCycleActivitiesFromTemplate(ConvertToCallCycleEntity(callCycle), ConvertToCallCycleContactDtoList(callCycleContact),
                               ActivityAdapter.ConvertToArgsEntity(args));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<LookupDTO> GetAllCallCyclesList(ArgsDTO args)
        {
            try
            {
                List<LookupEntity> lookupValues = new List<LookupEntity>();
                List<LookupDTO> lookupDtoValues = new List<LookupDTO>();

                lookupValues = CallCycleBR.Instance.GetAllCallCyclesList(ActivityAdapter.ConvertToArgsEntity(args));

                foreach (LookupEntity item in lookupValues)
                {
                    lookupDtoValues.Add(CommonAdapter.ConvertToLookupDTO(item));
                }

                return lookupDtoValues;

            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveRoute(RouteDTO routedto)
        {
            try
            {
                return CallCycleBR.Instance.SaveRoute(ConvertToRouteDTO(routedto));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteRoutesByRepCode(RouteDTO routedto)
        {
            try
            {
                return CallCycleBR.Instance.DeleteRoutesByRepCode(ConvertToRouteDTO(routedto));
            }
            catch (Exception)
            {
                throw;
            }
        }


        public bool UpdateRouteSequence(List<RouteDTO> routedtolist)
        {
            try
            {
                return CallCycleBR.Instance.UpdateRouteSequence(ConvertToRouteEntityList(routedtolist));
            }
            catch (Exception)
            {
                throw;
            }
        }




        public List<RouteDTO> GetRoutesByRepCode(ArgsDTO args)
        {
            try
            {
                List<RouteEntity> routeValues = new List<RouteEntity>();
                List<RouteDTO> routeDtoValues = new List<RouteDTO>();

                routeValues = CallCycleBR.Instance.GetRoutesByRepCode(CommonAdapter.ConvertToArgsEntity(args));

                foreach (RouteEntity item in routeValues)
                {
                    routeDtoValues.Add(ConvertToPRouteDto(item));
                }

                return routeDtoValues;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<RouteDTO> GetRoutesForTME(ArgsDTO args)
        {
            try
            {
                List<RouteEntity> routeValues = new List<RouteEntity>();
                List<RouteDTO> routeDtoValues = new List<RouteDTO>();

                routeValues = CallCycleBR.Instance.GetRoutesForTME(CommonAdapter.ConvertToArgsEntity(args));

                foreach (RouteEntity item in routeValues)
                {
                    routeDtoValues.Add(ConvertToPRouteDto(item));
                }

                return routeDtoValues;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SchedulePlanedRoutesForDR(ActivityDTO activitydto ,string originatorType)
        {
            try
            {
                return CallCycleBR.Instance.SchedulePlanedRoutesForDR(ActivityAdapter.ConvertToActivity(activitydto),originatorType);
            }
            catch (Exception)
            {
                throw;
            }
        }
        

        public List<RouteDTO> GetAssignedRoutes(string parentOriginator, ArgsDTO args)
        {
            try
            {
                return ConvertToRouteDTOList(CallCycleBR.Instance.GetAssignedRoutes(parentOriginator, ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<RouteMasterDTO> GetAllRoutes(string routeNameLike)
        {
            try
            {
                return ConvertToRouteMasterDTOList(RouteMasterBR.Instance.GetAllRoutesMaster(routeNameLike));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<RouteMasterDTO> GetAllRouteMaster(string parentOriginator, ArgsDTO args)
        {
            try
            {
                return ConvertToRouteMasterDTOList(RouteMasterBR.Instance.GetAllRoutesMaster(parentOriginator, ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<RouteMasterDTO> GetAllRouteMasterForCheckList(int CheckListId, string parentOriginator, ArgsDTO args)
        {
            try
            {
                return ConvertToRouteMasterDTOList(RouteMasterBR.Instance.GetAllRoutesMasterForCheckList(CheckListId, parentOriginator, ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<RouteMasterDTO> GetAllTMERoutesMaster(string tmeCode, ArgsDTO args)
        {
            try
            {
                return ConvertToRouteMasterDTOList(RouteMasterBR.Instance.GetAllTMERoutesMaster(tmeCode, ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveRouteMaster(RouteMasterDTO routeMasterDTO)
        {
            try
            {
                List<RouteMasterDTO> routeMasterList = new List<RouteMasterDTO>();
                routeMasterList.Add(routeMasterDTO);
                return RouteMasterBR.Instance.SaveRoutesMaster(ConvertToRouteMasterEntityList(routeMasterList));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<RouteMasterDTO> GetRouteTargetsByDistributorId(ArgsDTO args)
        {
            try
            {
                return ConvertToRouteMasterDTOList(RouteMasterBR.Instance.GetRouteTargetsByDistributorId(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CallCycleDTO> GetCallCyclesForRep(string repcode)
        {
            try
            {
                List<CallCycleEntity> callcycleEntLis = CallCycleBR.Instance.GetCallCyclesForRep(repcode);
                List<CallCycleDTO> callCycDTOList = new List<CallCycleDTO>();

                foreach (CallCycleEntity callnt in callcycleEntLis)
                {
                    callCycDTOList.Add(ConvertToCallCycleDTO(callnt));

                }

                return callCycDTOList;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool IsRouteMasterExists(RouteMasterDTO routeMasterDTO)
        {
            try
            {
                return RouteMasterBR.Instance.IsRouteMasterExists(ConvertToRouteMasterEntity(routeMasterDTO));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public RouteMasterDTO GetRouteTargetFromRouteId(int routeMasterId)
        {
            try
            {
                return ConvertToRouteMasterDTO(RouteMasterBR.Instance.GetRouteTargetFromRouteId(routeMasterId));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CallCycleContactDTO> GetCallCycleContactsForNewEntry(ArgsDTO args, string repCode)
        {
            try
            {
                return ConvertToCallCycleContactList(CallCycleBR.Instance.GetCallCycleContactsForNewEntry(ActivityAdapter.ConvertToArgsEntity(args), repCode));
            }
            catch (Exception)
            {
                throw;
            }
        }


        public bool IsCallCycleContactInAnyOtherSchedule(string customerCode)
        {
            try
            {
                return CallCycleBR.Instance.IsCallCycleContactInAnyOtherSchedule(customerCode);
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string GetRepCodeForCallCycleId(int callCycleId)
        {
            try
            {
                return CallCycleBR.Instance.GetRepCodeForCallCycleId(callCycleId);
            }
            catch (Exception)
            {
                throw;
            }
        }



        public bool SaveOriginatorRouteOutlets(OriginatorRouteOutletsDTO originatorRouteOutletsDTO)
        {
            try
            {
                return CallCycleBR.Instance.SaveOriginatorRouteOutlets(ConvertToOriginatorRouteOutletsEntity(originatorRouteOutletsDTO));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ItineraryHeaderDTO GetItineraryDetailsForUser(string tme, DateTime date)
        {
            try
            {
                return ConvertToItineraryHeaderDTO(ItineraryBR.Instance.GetItineraryDetailsForUser(tme, date));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool SaveItinerary(ItineraryHeaderDTO itineraryHeaderdto)
        {
            try
            {
                return ItineraryBR.Instance.SaveItinerary(ConvertToItineraryHeaderEntity(itineraryHeaderdto));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public int InsertItineraryLogEntry(ItineraryHeaderDTO itineraryHeaderDTO)
        {
            try
            {
                return ItineraryBR.Instance.InsertItineraryLogEntry(ConvertToItineraryHeaderEntity(itineraryHeaderDTO));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateItineraryHeaderStatus(ItineraryHeaderDTO itineraryHeaderDTO)
        {
            try
            {
                return ItineraryBR.Instance.UpdateItineraryHeaderStatus(ConvertToItineraryHeaderEntity(itineraryHeaderDTO));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ItineraryHeaderDTO GetItineraryLogEntryById(int itineraryLogEntryId)
        {
            try
            {
                return ConvertToItineraryHeaderDTO(ItineraryBR.Instance.GetItineraryLogEntryById(itineraryLogEntryId));
            }
            catch
            {
                throw;
            }
        }

        public OriginatorDTO GetItineraryAssignedToByHeaderId(int itineraryHeaderId)
        {
            try
            {
                return OriginatorAdapter.ConvertToOriginatorDTO(ItineraryBR.Instance.GetItineraryAssignedToByHeaderId(itineraryHeaderId));
            }
            catch
            {
                throw;
            }
        }

        public List<VisitChecklistMasterDTO> GetAllVisitChecklistMaster(ArgsDTO args)
        {
            try
            {
                return ConvertToVisitChecklistMasterDTOList(VisitChecklistBR.Instance.GetAllVisitChecklistMaster(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveVisitChecklistMaster(VisitChecklistMasterDTO visitChecklistMasterDTO)
        {
            try
            {
                List<VisitChecklistMasterDTO> VisitChecklistMasterDTOList = new List<VisitChecklistMasterDTO>();
                VisitChecklistMasterDTOList.Add(visitChecklistMasterDTO);
                return VisitChecklistBR.Instance.SaveVisitChecklistMaster(ConvertToVisitChecklistMasterEntityList(VisitChecklistMasterDTOList));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool DeleteVisitChecklistMaster(VisitChecklistMasterDTO visitChecklistMasterDTO)
        {
            try
            {
                return VisitChecklistBR.Instance.DeleteVisitChecklistMaster(ConvertToVisitChecklistMasterEntity(visitChecklistMasterDTO));
            }
            catch
            {
                throw;
            }
        }

        public bool DeleteRouteMaster(RouteMasterDTO routeMasterDTO)
        {
            try
            {
                return RouteMasterBR.Instance.DeleteRouteMaster(ConvertToRouteMasterEntity(routeMasterDTO));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public ItineraryHeaderDTO GetItineraryHeaderByUser(string tme, DateTime date)
        {
            try
            {
                return ConvertToItineraryHeaderDTO(ItineraryBR.Instance.GetItineraryHeaderByUser(tme, date));
            }
            catch
            {
                throw;
            }
        }

        public ItineraryHeaderDTO GetItineraryHeaderByHeaderId(int itineraryHeaderId)
        {
            try
            {
                return ConvertToItineraryHeaderDTO(ItineraryBR.Instance.GetItineraryHeaderByHeaderId(itineraryHeaderId));
            }
            catch
            {
                throw;
            }
        }
        #endregion
    }
}
