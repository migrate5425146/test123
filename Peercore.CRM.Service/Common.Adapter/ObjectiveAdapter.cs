﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.Entities;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class ObjectiveAdapter
    {
        #region Converters

        public static ObjectiveDTO ConvertToObjectiveDTO(ObjectiveEntity objectIn)
        {
            ObjectiveDTO objectOut = new ObjectiveDTO();
            try
            {
                if (objectIn != null)
                {
                    objectOut.ObjectiveId = objectIn.ObjectiveId;
                    objectOut.Code = objectIn.Code;
                    objectOut.Description = objectIn.Description;
                    objectOut.Status = objectIn.Status;
                    objectOut.IsAchieved = objectIn.IsAchieved;
                    objectOut.CustomerCode = objectIn.CustomerCode;
                    objectOut.Status = objectIn.Status;
                    objectOut.Type = objectIn.Type;

                    objectOut.CreatedBy = objectIn.CreatedBy;
                    objectOut.CreatedDate = objectIn.CreatedDate.Value;
                    objectOut.LastModifiedBy = objectIn.LastModifiedBy;
                    objectOut.LastModifiedDate = objectIn.LastModifiedDate.Value;

                    objectOut.RowCount = objectIn.TotalCount;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objectOut;
        }

        public static ObjectiveEntity ConvertToObjectiveEntity(ObjectiveDTO objectIn)
        {
            ObjectiveEntity objectOut = new ObjectiveEntity();
            try
            {
                if (objectIn != null)
                {
                    objectOut.ObjectiveId = objectIn.ObjectiveId;
                    objectOut.Code = objectIn.Code;
                    objectOut.Description = objectIn.Description;
                    objectOut.Status = objectIn.Status;
                    objectOut.IsAchieved = objectIn.IsAchieved;
                    objectOut.CustomerCode = objectIn.CustomerCode;
                    objectOut.Status = objectIn.Status;
                    objectOut.Type = objectIn.Type;

                    objectOut.CreatedBy = objectIn.CreatedBy;
                    objectOut.CreatedDate = objectIn.CreatedDate;
                    objectOut.LastModifiedBy = objectIn.LastModifiedBy;
                    objectOut.LastModifiedDate = objectIn.LastModifiedDate;

                    objectOut.TotalCount = objectIn.RowCount;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objectOut;
        }

        public static List<ObjectiveDTO> ConvertToObjectiveDTOList(List<ObjectiveEntity> listIn)
        {
            List<ObjectiveDTO> listOut = new List<ObjectiveDTO>();
            try
            {
                foreach (ObjectiveEntity itemIn in listIn)
                {
                    listOut.Add(ConvertToObjectiveDTO(itemIn));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return listOut;
        }

        public static List<ObjectiveEntity> ConvertToObjectiveEntityList(List<ObjectiveDTO> listIn)
        {
            List<ObjectiveEntity> listOut = new List<ObjectiveEntity>();
            try
            {
                foreach (ObjectiveDTO itemIn in listIn)
                {
                    listOut.Add(ConvertToObjectiveEntity(itemIn));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return listOut;
        }

        public static MarketObjectiveDTO ConvertToMarketObjectiveDTO(MarketObjectiveEntity objectIn)
        {
            MarketObjectiveDTO objectOut = new MarketObjectiveDTO();
            try
            {
                if (objectIn != null)
                {
                    objectOut.MarketObjectiveId = objectIn.MarketObjectiveId;
                    objectOut.Market = CommonAdapter.ConvertToMarketDTO(objectIn.Market);
                    objectOut.Objective = ConvertToObjectiveDTO(objectIn.Objective);
                    objectOut.Status = objectIn.Status;

                    objectOut.CreatedBy = objectIn.CreatedBy;
                    objectOut.CreatedDate = objectIn.CreatedDate;
                    objectOut.LastModifiedBy = objectIn.LastModifiedBy;
                    objectOut.LastModifiedDate = objectIn.LastModifiedDate;

                    objectOut.RowCount = objectIn.TotalCount;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objectOut;
        }

        public static MarketObjectiveEntity ConvertToMarketObjectiveEntity(MarketObjectiveDTO objectIn)
        {
            MarketObjectiveEntity objectOut = new MarketObjectiveEntity();
            try
            {
                if (objectIn != null)
                {
                    objectOut.MarketObjectiveId = objectIn.MarketObjectiveId;
                    objectOut.Market = CommonAdapter.ConvertToMarket(objectIn.Market);
                    objectOut.Objective = ConvertToObjectiveEntity(objectIn.Objective);
                    objectOut.Status = objectIn.Status;

                    objectOut.CreatedBy = objectIn.CreatedBy;
                    objectOut.CreatedDate = objectIn.CreatedDate;
                    objectOut.LastModifiedBy = objectIn.LastModifiedBy;
                    objectOut.LastModifiedDate = objectIn.LastModifiedDate;

                    objectOut.TotalCount = objectIn.RowCount;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return objectOut;
        }

        public static List<MarketObjectiveEntity> ConvertToMarketObjectiveEntityList(List<MarketObjectiveDTO> listIn)
        {
            List<MarketObjectiveEntity> listOut = new List<MarketObjectiveEntity>();
            try
            {
                foreach (MarketObjectiveDTO itemIn in listIn)
                {
                    listOut.Add(ConvertToMarketObjectiveEntity(itemIn));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return listOut;
        }

        public static List<MarketObjectiveDTO> ConvertToMarketObjectiveDTOList(List<MarketObjectiveEntity> listIn)
        {
            List<MarketObjectiveDTO> listOut = new List<MarketObjectiveDTO>();
            try
            {
                foreach (MarketObjectiveEntity itemIn in listIn)
                {
                    listOut.Add(ConvertToMarketObjectiveDTO(itemIn));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return listOut;
        }

        #endregion

        #region Methods

        public bool SaveMarketObjective(MarketObjectiveDTO marketObjectiveDTO)
        {
            try
            {
                List<MarketObjectiveDTO> marketObjectiveDTOList = new List<MarketObjectiveDTO>();
                marketObjectiveDTOList.Add(marketObjectiveDTO);
                return MarketObjectiveBR.Instance.SaveMarketObjectives(ConvertToMarketObjectiveEntityList(marketObjectiveDTOList));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<ObjectiveDTO> GetAllMarketObjectives(int marketId, ArgsDTO args)
        {
            try
            {
                return ConvertToObjectiveDTOList(MarketObjectiveBR.Instance.GetAllMarketObjectives(marketId, ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool IsObjectiveCodeExists(ObjectiveDTO objectiveDTO)
        {
            try
            {
                return ObjectiveBR.Instance.IsObjectiveCodeExists(ConvertToObjectiveEntity(objectiveDTO));
            }
            catch
            {
                throw;
            }
        }

        #endregion
    }
}
