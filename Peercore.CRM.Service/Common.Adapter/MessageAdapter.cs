﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.Entities;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class MessageAdapter
    {
        #region Converters
        private MessagesDTO MessagesDTOConverters(MessagesEntity messages)
        {
            MessagesDTO MessagesDTO = new MessagesDTO();

            try
            {
                if (messages != null)
                {
                    MessagesDTO.Message = messages.Message;
                    MessagesDTO.MessageDate = messages.MessageDate;
                    MessagesDTO.MessageID = messages.MessageID;
                    MessagesDTO.Originator = messages.Originator;
                    MessagesDTO.RowCount = messages.TotalCount;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return MessagesDTO;
        }

        private MessagesEntity MessagesConverters(MessagesDTO messagesDTO)
        {
            MessagesEntity Messages = MessagesEntity.CreateObject();

            try
            {
                if (messagesDTO != null)
                {
                    Messages.Message = messagesDTO.Message;
                    Messages.MessageDate = messagesDTO.MessageDate;
                    Messages.MessageID = messagesDTO.MessageID;
                    Messages.Originator = messagesDTO.Originator;
                    messagesDTO.RowCount = Messages.TotalCount;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return Messages;
        }
        #endregion

        public bool SaveMessages(MessagesDTO message, List<OriginatorDTO> chldOriginatorList, out int newrecordid)
        {
            try
            {
                newrecordid = 0;
                List<OriginatorEntity> objOriginator = new List<OriginatorEntity>();
                foreach (OriginatorDTO obj in chldOriginatorList)
                {
                    objOriginator.Add(CommonAdapter.ConvertToOriginator(obj));
                }
                return MessageEntryBR.Instance.Save(MessagesConverters(message), objOriginator, out newrecordid);

            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<MessagesDTO> GetMessageByOriginator(string originator)
        {
            List<MessagesDTO> lstMessagesDTO = new List<MessagesDTO>();
            try
            {
                List<MessagesEntity> lstMessages = MessageEntryBR.Instance.GetMessageByOriginator(originator);
                foreach (MessagesEntity obj in lstMessages)
                {
                    lstMessagesDTO.Add(MessagesDTOConverters(obj));
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstMessagesDTO;
        }

        public MessagesDTO GetMessageCount(string originator, string datestring)
        {
            MessagesDTO messagesDTO = new MessagesDTO();
            try
            {
                return MessagesDTOConverters(MessageEntryBR.Instance.GetMessageCount(originator, datestring));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public MessagesDTO GetMessage(int messageId)
        {
            try
            {
                return MessagesDTOConverters(MessageEntryBR.Instance.GetMessage(messageId));

            }
            catch (Exception)
            {

                throw;
            }
        }

        public bool DeleteMessage(int messageId)
        {
            bool delete = false;
            try
            {
                delete = MessageEntryBR.Instance.DeleteMessage(messageId);
            }
            catch (Exception)
            {

                throw;
            }
            return delete;
        }

        public bool UpdateMessageRep(string username)
        {
            try
            {
                return MessageEntryBR.Instance.UpdateMessageRep(username);
            }
            catch (Exception)
            {

                throw;
            }
        }
    }
}
