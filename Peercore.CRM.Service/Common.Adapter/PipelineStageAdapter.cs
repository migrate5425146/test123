﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.Entities;
using Peercore.CRM.BusinessRules;
using System.Data;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class PipelineStageAdapter
    {
        #region - Converters -
        public static PipelineStageEntity ConvertToPipelineStage(PipelineStageDTO pipelineStageDto)
        {
            PipelineStageEntity PipelineStage = PipelineStageEntity.CreateObject();

            try
            {
                PipelineStage.PipelineStageID = pipelineStageDto.PipelineStageID;
                PipelineStage.PipelineStageName = pipelineStageDto.PipelineStageName;
                PipelineStage.Order = pipelineStageDto.Order;
            }
            catch (Exception)
            {
                throw;
            }
            return PipelineStage;
        }

        public static PipelineStageDTO ConvertToPipelineStageDTO(PipelineStageEntity pipelineStage)
        {
            PipelineStageDTO pipelineStageDtO = new PipelineStageDTO();

            try
            {
                pipelineStageDtO.PipelineStageID = pipelineStage.PipelineStageID;
                pipelineStageDtO.PipelineStageName = pipelineStage.PipelineStageName;
                pipelineStageDtO.Order = pipelineStage.Order;
            }
            catch (Exception)
            {

                throw;
            }
            return pipelineStageDtO;
        }


        public static List<PipelineStageDTO> ConvertToPipelineStageDTOList(List<PipelineStageEntity> pipelineStage)
        {
            List<PipelineStageDTO> lstPipelineStage = new List<PipelineStageDTO>();
            PipelineStageDTO pipelineStageDTO = null;

            try
            {
                foreach (PipelineStageEntity lstboj in pipelineStage)
                {
                    pipelineStageDTO = new DTO.PipelineStageDTO();
                    pipelineStageDTO.PipelineStageID = lstboj.PipelineStageID;
                    pipelineStageDTO.PipelineStageName = lstboj.PipelineStageName;
                    pipelineStageDTO.Order = lstboj.Order;
                    pipelineStageDTO.TotalAmount = lstboj.TotalAmount;
                    pipelineStageDTO.TotalUnits = lstboj.TotalUnits;
                    lstPipelineStage.Add(pipelineStageDTO);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstPipelineStage;
        }


        public static List<PipelineStageEntity> ConvertToPipelineStageList(List<PipelineStageDTO> pipelineStage)
        {
            List<PipelineStageEntity> lstPipelineStage = new List<PipelineStageEntity>();
            PipelineStageEntity PipelineStageDTO = null;

            try
            {
                foreach (PipelineStageDTO lstboj in pipelineStage)
                {
                    PipelineStageDTO.PipelineStageID = lstboj.PipelineStageID;
                    PipelineStageDTO.PipelineStageName = lstboj.PipelineStageName;
                    PipelineStageDTO.Order = lstboj.Order;
                    lstPipelineStage.Add(PipelineStageDTO);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstPipelineStage;
        }

        /*
        public static List<PipelineStageGraphDTO> ConvertToPipelineStageGraphDTOList(List<PipelineStageGraph> pipelineStageGraphList)
        {
            List<PipelineStageGraphDTO> pipelineStageGraphDTOList = new List<PipelineStageGraphDTO>();
            PipelineStageGraphDTO pipelineStageGraphDTO = null;

            try
            {
                foreach (PipelineStageGraph lstboj in pipelineStageGraphList)
                {
                    pipelineStageGraphDTO = new PipelineStageGraphDTO();
                    pipelineStageGraphDTO.PipelineStageID = lstboj.PipelineStageID;
                    pipelineStageGraphDTO.PipelineStage = lstboj.PipelineStage;
                    pipelineStageGraphDTO.StageOrder = lstboj.StageOrder;
                    pipelineStageGraphDTO.TotalAmount = lstboj.TotalAmount;
                    pipelineStageGraphDTO.TotalUnits = lstboj.TotalUnits;
                    pipelineStageGraphDTO.Code = lstboj.Code;
                    pipelineStageGraphDTOList.Add(pipelineStageGraphDTO);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return pipelineStageGraphDTOList;
        }
        */
        #endregion

        #region - Properties -

        //private brPipelineStage oBrPipelineStage = null;

        #endregion

        #region - Constructor -

        public PipelineStageAdapter()
        {
            //if (oBrPipelineStage == null)
            //    oBrPipelineStage = new brPipelineStage();
        }

        #endregion

        #region - Methods -

        public List<PipelineStageDTO> GetPipelineStage(ArgsDTO args)
        {
            try
            {
                return ConvertToPipelineStageDTOList(PipelineStageBR.Instance.GetPipelineStages(args.DefaultDepartmentId));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<PipelineStageDTO> GetPipelineChart(ArgsDTO args)
        {
            try
            {
                return ConvertToPipelineStageDTOList(PipelineStageBR.Instance.GetPipelineChart(ActivityAdapter.ConvertToArgsEntity(args)));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool SavePipelineStage(List<PipelineStageDTO> pipelineStageList, List<int> deletedStageList, ArgsDTO args)
        {
            bool save = false;
            try
            {
                save = PipelineStageBR.Instance.Save(ConvertToPipelineStageList(pipelineStageList), deletedStageList, ActivityAdapter.ConvertToArgsEntity(args));
            }
            catch (Exception)
            {

                throw;
            }
            return save;
        }

        public bool PipelineStageCanDelete(int pipelineStageId)
        {
            try
            {
                return PipelineStageBR.Instance.CanDelete(pipelineStageId);
            }
            catch
            {
                throw;
            }
        }

        /*
        public List<PipelineStageGraphDTO> GetPipelineStageGraph(string sOriginator, bool IsCustomerPipeline, string childOriginators, string defaultDeptID)
        {
            try
            {
                return ConvertToPipelineStageGraphDTOList(oBrPipelineStage.GetPipelineStageGraph(sOriginator, IsCustomerPipeline, childOriginators, defaultDeptID));
            }
            catch (Exception)
            {

                throw;
            }
        }
        */
        #endregion
    }
}
