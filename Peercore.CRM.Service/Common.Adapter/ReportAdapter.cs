﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Service.DTO;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.Service.DTO.CompositeEntities;
using System.Collections.ObjectModel;
using Peercore.CRM.Entities;

namespace Peercore.CRM.Service.Common.Adapter
{
    public class ReportAdapter
    {
        #region - Converters -
        /*
        public static CallCycleGraphDTO ConvertToCallCycleGraphDTO(CallCycleGraph callCycleGraph)
        {
            CallCycleGraphDTO oCallCycleGraphDTO = new CallCycleGraphDTO();
            try
            {
                oCallCycleGraphDTO.ActivityId = callCycleGraph.ActivityId;
                oCallCycleGraphDTO.ActivityType = callCycleGraph.ActivityType;
                oCallCycleGraphDTO.ActivityTypeCount = callCycleGraph.ActivityTypeCount;
                oCallCycleGraphDTO.ActivityTypeDesc = callCycleGraph.ActivityTypeDesc;
                oCallCycleGraphDTO.AssignTo = callCycleGraph.AssignTo;
                oCallCycleGraphDTO.CallCycleId = callCycleGraph.CallCycleId;
                oCallCycleGraphDTO.CallCycleName = callCycleGraph.CallCycleName;
                oCallCycleGraphDTO.Comments = callCycleGraph.Comments;
                oCallCycleGraphDTO.CreatedBy = callCycleGraph.CreatedBy;
                oCallCycleGraphDTO.CreatedDate = callCycleGraph.CreatedDate;
                oCallCycleGraphDTO.CustomerCode = callCycleGraph.CustomerCode;
                oCallCycleGraphDTO.DeleteFlag = callCycleGraph.DeleteFlag;
                oCallCycleGraphDTO.EndDate = callCycleGraph.EndDate;
                oCallCycleGraphDTO.LeadId = callCycleGraph.LeadId;
                oCallCycleGraphDTO.LeadStage = callCycleGraph.LeadStage;
                oCallCycleGraphDTO.LeadStageId = callCycleGraph.LeadStageId;
                oCallCycleGraphDTO.ModifiedBy = callCycleGraph.ModifiedBy;
                oCallCycleGraphDTO.ModifiedDate = callCycleGraph.ModifiedDate;
                oCallCycleGraphDTO.ParentActivityId = callCycleGraph.ParentActivityId;
                oCallCycleGraphDTO.Priority = callCycleGraph.Priority;
                oCallCycleGraphDTO.ReminderDate = callCycleGraph.ReminderDate;
                oCallCycleGraphDTO.RepGroupId = callCycleGraph.RepGroupId;
                oCallCycleGraphDTO.RowCount = callCycleGraph.RowCount;
                oCallCycleGraphDTO.SendReminder = callCycleGraph.SendReminder;
                oCallCycleGraphDTO.SentMail = callCycleGraph.SentMail;
                oCallCycleGraphDTO.SourceName = callCycleGraph.SourceName;
                oCallCycleGraphDTO.StartDate = callCycleGraph.StartDate;
                oCallCycleGraphDTO.Status = callCycleGraph.Status;
                oCallCycleGraphDTO.StatusDescription = callCycleGraph.StatusDescription;
                oCallCycleGraphDTO.Subject = callCycleGraph.Subject;

            }
            catch (Exception)
            {

                throw;
            }

            return oCallCycleGraphDTO;
        }
        
        
*/
        public static RepWizeActivityAnalysisDTO ConvertToRepwizeActivityAnalsysisDTO(RepWizeActivityAnalysisEntity repWizeActivityAnalysis)
        {
            RepWizeActivityAnalysisDTO repWizeActivityDTO = new RepWizeActivityAnalysisDTO();

            try
            {
                repWizeActivityDTO.RepName = repWizeActivityAnalysis.RepName;
                repWizeActivityDTO.LeadCount = repWizeActivityAnalysis.LeadCount;
                repWizeActivityDTO.ProspectCount = repWizeActivityAnalysis.ProspectCount;
                repWizeActivityDTO.OppotunityCount = repWizeActivityAnalysis.OppotunityCount;
                repWizeActivityDTO.ActivityCount = repWizeActivityAnalysis.ActivityCount;
                repWizeActivityDTO.ActivityCountNotCompleted = repWizeActivityAnalysis.ActivityCountNotCompleted;
                repWizeActivityDTO.ActivityStatus = repWizeActivityAnalysis.ActivityStatus;

                List<StageDTO> stageList = new List<StageDTO>();
                foreach (Stages item in repWizeActivityAnalysis.StagesList)
                {
                    stageList.Add(ConvertToStageDTO(item));
                }
                repWizeActivityDTO.StagesList = stageList;
                //repWizeActivityDTO.ConversionRatio = repWizeActivityAnalysis.ConversionRatio;
                //repWizeActivityDTO.RowCount = repWizeActivityAnalysis.RowCount;
            }
            catch (Exception)
            {

                throw;
            }
            return repWizeActivityDTO;
        }
        public static StageDTO ConvertToStageDTO(Stages item)
        {
            StageDTO StageDTO = new StageDTO();
            try
            {
                StageDTO.Count = item.Count;
                StageDTO.RepName = item.RepName;
                StageDTO.StageName = item.StageName;
            }
            catch (Exception)
            {

                throw;
            }
            return StageDTO;
        }

       /* public static BDMWideSalesAnalysisDTO ConvertToBDMWideSalesAnalysisDTO(BDMWideSalesAnalysis item)
        {
            BDMWideSalesAnalysisDTO BDMWideSalesAnalysisDTO = new BDMWideSalesAnalysisDTO();

            try
            {
                BDMWideSalesAnalysisDTO.AcLtr = item.AcLtr == null ? 0 : item.AcLtr;
                BDMWideSalesAnalysisDTO.ActualLiters = item.ActualLiters;
                BDMWideSalesAnalysisDTO.ActualValue = item.ActualValue;
                BDMWideSalesAnalysisDTO.BDM = item.BDM;
                BDMWideSalesAnalysisDTO.CustomerCode = item.CustomerCode;
                BDMWideSalesAnalysisDTO.CustomerName = item.CustomerName;
                BDMWideSalesAnalysisDTO.NoOfWeeks = item.NoOfWeeks;
                BDMWideSalesAnalysisDTO.PotentialLiters = item.PotentialLiters;
                BDMWideSalesAnalysisDTO.ReqSentDate = item.ReqSentDate;
                BDMWideSalesAnalysisDTO.StartDate = item.StartDate;
                BDMWideSalesAnalysisDTO.T1 = item.T1;
                BDMWideSalesAnalysisDTO.T2 = item.T2;
                BDMWideSalesAnalysisDTO.T3 = item.T3;
                BDMWideSalesAnalysisDTO.TotalLiters = item.TotalLiters;
                BDMWideSalesAnalysisDTO.Units = item.Units;
                BDMWideSalesAnalysisDTO.RowCount = item.RowCount;
            }
            catch (Exception)
            {

                throw;
            }

            return BDMWideSalesAnalysisDTO;
        }
        */

       /* public static BDMWideServiceCallAnalysisDTO ConvertToBDMWideServiceCallAnalysisDTO(BDMWideServiceCallAnalysis item)
        {
            BDMWideServiceCallAnalysisDTO BDMWideServiceCallAnalysisDTO = new BDMWideServiceCallAnalysisDTO();

            try
            {
                BDMWideServiceCallAnalysisDTO.ActivityId = item.ActivityId;
                BDMWideServiceCallAnalysisDTO.AssignTo = item.AssignTo;
                BDMWideServiceCallAnalysisDTO.City = item.City;
                BDMWideServiceCallAnalysisDTO.CustomerCode = item.CustomerCode;
                BDMWideServiceCallAnalysisDTO.CustomerName = item.CustomerName;
                BDMWideServiceCallAnalysisDTO.PostCode = item.PostCode;
                int count = 0;
                foreach (BDMWideServiceCallAnalysisStages it in item.ServicaCallStageList)
                {
                    switch (count)
                    {
                        case 0:
                            BDMWideServiceCallAnalysisDTO.StageCount1 = it.StageCount;
                            break;
                        case 1:
                            BDMWideServiceCallAnalysisDTO.StageCount2 = it.StageCount;
                            break;
                        case 2:
                            BDMWideServiceCallAnalysisDTO.StageCount3 = it.StageCount;
                            break;
                        case 3:
                            BDMWideServiceCallAnalysisDTO.StageCount4 = it.StageCount;
                            break;
                        case 4:
                            BDMWideServiceCallAnalysisDTO.StageCount5 = it.StageCount;
                            break;
                        case 5:
                            BDMWideServiceCallAnalysisDTO.StageCount6 = it.StageCount;
                            break;
                        case 6:
                            BDMWideServiceCallAnalysisDTO.StageCount7 = it.StageCount;
                            break;
                        case 7:
                            BDMWideServiceCallAnalysisDTO.StageCount8 = it.StageCount;
                            break;

                        case 8:
                            BDMWideServiceCallAnalysisDTO.StageCount9 = it.StageCount;
                            break;
                        case 9:
                            BDMWideServiceCallAnalysisDTO.StageCount10 = it.StageCount;
                            break;
                        case 10:
                            BDMWideServiceCallAnalysisDTO.StageCount11 = it.StageCount;
                            break;
                        case 11:
                            BDMWideServiceCallAnalysisDTO.StageCount12 = it.StageCount;
                            break;
                        case 12:
                            BDMWideServiceCallAnalysisDTO.StageCount13 = it.StageCount;
                            break;
                        case 13:
                            BDMWideServiceCallAnalysisDTO.StageCount14 = it.StageCount;
                            break;
                        case 14:
                            BDMWideServiceCallAnalysisDTO.StageCount15 = it.StageCount;
                            break;
                        case 15:
                            BDMWideServiceCallAnalysisDTO.StageCount16 = it.StageCount;
                            break;

                        case 16:
                            BDMWideServiceCallAnalysisDTO.StageCount17 = it.StageCount;
                            break;
                        case 17:
                            BDMWideServiceCallAnalysisDTO.StageCount18 = it.StageCount;
                            break;
                        case 18:
                            BDMWideServiceCallAnalysisDTO.StageCount19 = it.StageCount;
                            break;
                        case 19:
                            BDMWideServiceCallAnalysisDTO.StageCount20 = it.StageCount;
                            break;
                        case 20:
                            BDMWideServiceCallAnalysisDTO.StageCount21 = it.StageCount;
                            break;
                        case 21:
                            BDMWideServiceCallAnalysisDTO.StageCount22 = it.StageCount;
                            break;
                        case 22:
                            BDMWideServiceCallAnalysisDTO.StageCount23 = it.StageCount;
                            break;
                        case 23:
                            BDMWideServiceCallAnalysisDTO.StageCount24 = it.StageCount;
                            break;
                    }
                    count++;
                }
                //BDMWideServiceCallAnalysisDTO.ServicaCallStageList = ReportAdapter.ConvertToBDMWideServiceCallAnalysisStagesDTO(item.ServicaCallStageList);
                BDMWideServiceCallAnalysisDTO.StageCountTotal = item.StageCountTotal;
                BDMWideServiceCallAnalysisDTO.RowCount = item.RowCount;

            }
            catch (Exception)
            {

                throw;
            }

            return BDMWideServiceCallAnalysisDTO;
        }*/
        /*
        public static List<BDMWideServiceCallAnalysisStagesDTO> ConvertToBDMWideServiceCallAnalysisStagesDTO(List<BDMWideServiceCallAnalysisStages> items)
        {
            List<BDMWideServiceCallAnalysisStagesDTO> WideServiceCallAnalysisStagesList = new List<BDMWideServiceCallAnalysisStagesDTO>();
            try
            {

                foreach (BDMWideServiceCallAnalysisStages item in items)
                {
                    BDMWideServiceCallAnalysisStagesDTO BDMWideServiceCallAnalysisStagesDTO = new BDMWideServiceCallAnalysisStagesDTO();


                    BDMWideServiceCallAnalysisStagesDTO.FromDate = item.FromDate;
                    BDMWideServiceCallAnalysisStagesDTO.StageCount = item.StageCount;
                    BDMWideServiceCallAnalysisStagesDTO.StageName = item.StageName;
                    BDMWideServiceCallAnalysisStagesDTO.StageOrder = item.StageOrder;
                    BDMWideServiceCallAnalysisStagesDTO.ToDate = item.ToDate;
                    WideServiceCallAnalysisStagesList.Add(BDMWideServiceCallAnalysisStagesDTO);
                }

            }
            catch (Exception)
            {

                throw;
            }

            return WideServiceCallAnalysisStagesList;
        }
         * */
        #endregion

        #region - Properties -

       // brLead oBrLead = null;
        //brActivity oBrActivity = null;
        //brCallCycleGraph oBrCallCycleGraph = null;
        #endregion

        #region - Constructor -

        public ReportAdapter()
        {
            //if (oBrLead == null)
            //    oBrLead = new brLead();

            //if (oBrActivity == null)
            //    oBrActivity = new brActivity();

            /*if (oBrCallCycleGraph == null)
                oBrCallCycleGraph = new brCallCycleGraph();
             */
        }

        #endregion

        #region - Methods -
        public List<RepWizeActivityAnalysisDTO> GetRepWideActivity(DateTime FromDate,
            DateTime ToDate,
            List<LeadStageDTO> leadStageList,
            ArgsDTO args,
            List<OriginatorDTO> OriginatorList)
        {
            try
            {
                List<LeadStageEntity> leadStages = new List<LeadStageEntity>();
                foreach (LeadStageDTO item in leadStageList)
                {
                    leadStages.Add(LeadStageAdapter.ConvertToLeadStage(item));
                }

                List<OriginatorEntity> originators =new List<OriginatorEntity>();
                foreach (OriginatorDTO item in OriginatorList)
                {
                    originators.Add(CommonAdapter.ConvertToOriginator(item));
                }

                List<RepWizeActivityAnalysisDTO> repWizeActivityAnalysisDtos = new List<RepWizeActivityAnalysisDTO>();
                List<RepWizeActivityAnalysisEntity> repWizeActivityAnalysisList = RepWizeActivityAnalysisBR.Instance.GetRepWideActivity(FromDate,
                    ToDate,
                    leadStages,
                    ActivityAdapter.ConvertToArgsEntity(args),
                    originators);

                foreach (RepWizeActivityAnalysisEntity item in repWizeActivityAnalysisList)
                {
                    repWizeActivityAnalysisDtos.Add(ConvertToRepwizeActivityAnalsysisDTO(item));
                }

                return repWizeActivityAnalysisDtos;
            }
            catch (Exception)
            {

                throw;
            }
        }


        
        /*

        
        


        public List<LeadCustomerDTO> GetCombinedCollectionCountByState(int iLeadStageId,
            string sFromDate,
            string sEndDate,
            ArgsDTO args,
            string sLeadStage = "")
        {
            try
            {
                return LeadCustomerAdapter.ConvertToLeadCustomerList((oBrLead.GetCombinedCollectionCountByState(args.Originator,
                    iLeadStageId,
                    sFromDate,
                    sEndDate,
                    args.ChiildOriginators,
                    sLeadStage)));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<LeadCustomerDTO> GetCombinedCollectionByState(int iLeadStageId,
            string sFromDate,
            string sEndDate,
            ArgsDTO args,
            string sLeadStage = "")
        {
            try
            {

                return LeadCustomerAdapter.ConvertToLeadCustomerList(oBrLead.GetCombinedCollectionByState(args.Originator,
                    iLeadStageId,
                    sFromDate,
                    sEndDate,
                    args.ChiildOriginators,
                    args.AddressType,
                    args.AdditionalParams,
                    args.OrderBy,
                    args.StartIndex,
                    args.RowCount,
                    sLeadStage
                    ));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<LeadCustomerDTO> GetLeadCustomerCountByRep(int iLeadStageId,
            string sFromDate,
            string sEndDate,
            ArgsDTO args,
            string sLeadStage = "")
        {
            try
            {

                return LeadCustomerAdapter.ConvertToLeadCustomerList(oBrLead.GetLeadCustomerCountByRep(sFromDate,
                    sEndDate,
                    args.Originator,
                    iLeadStageId,
                    sLeadStage,
                    args.ChiildOriginators));
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<LeadCustomerDTO> GetLeadCustomerCollectionByRep(int iLeadStageId,
            string sFromDate,
            string sEndDate,
            ArgsDTO args,
            string sLeadStage = "")
        {
            try
            {

                return LeadCustomerAdapter.ConvertToLeadCustomerList(oBrLead.GetLeadCustomerCollectionByRep(
                    sFromDate,
                    sEndDate,
                    args.Originator,
                    iLeadStageId,
                    sLeadStage,
                    args.AdditionalParams,
                    args.StartIndex,
                    args.ChiildOriginators,
                    args.AddressType,
                    args.RowCount,
                    args.OrderBy));
            }
            catch (Exception)
            {

                throw;
            }
        }



        

        public List<CallCycleGraphDTO> GetCallCycleList(ArgsDTO args,
            string sFromDate = "",
            string sToDate = "",
            string sFilter = "All",
            string activityStatus = "")
        {
            try
            {
                List<CallCycleGraphDTO> callCycleGraphDtos = new List<CallCycleGraphDTO>();
                List<CallCycleGraph> callCycleGraphs = oBrCallCycleGraph.GetCallCycleList(args.StartIndex,
                    args.RowCount,
                    sFromDate,
                    sToDate,
                    args.Originator,
                    sFilter,
                    activityStatus,
                    args.ManagerMode,
                    args.AdditionalParams,
                    args.OrderBy);

                foreach (CallCycleGraph item in callCycleGraphs)
                {
                    callCycleGraphDtos.Add(ConvertToCallCycleGraphDTO(item));
                }

                return callCycleGraphDtos;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<CallCycleGraphDTO> GetCallCycleCountList(ArgsDTO args,
            string sFromDate = "",
            string sToDate = "",
            string sFilter = "All",
            string activityStatus = "")
        {
            try
            {
                List<CallCycleGraphDTO> callCycleGraphDtos = new List<CallCycleGraphDTO>();
                List<CallCycleGraph> callCycleGraphs = oBrCallCycleGraph.GetCallCycleCountList(sFromDate,
                    sToDate,
                    args.Originator,
                    sFilter,
                    activityStatus,
                    args.ManagerMode);

                foreach (CallCycleGraph item in callCycleGraphs)
                {
                    callCycleGraphDtos.Add(ConvertToCallCycleGraphDTO(item));
                }

                return callCycleGraphDtos;
            }
            catch (Exception)
            {

                throw;
            }
        }

        //No changes done - Non required
        
        //should check while debugging
        public List<BDMWideServiceCallAnalysisDTO> GetCustomerServiceCallActivities(ArgsDTO args,
            DateTime? FromDate,
            DateTime? ToDate,
            bool IsAllCustomers,
            bool IsNotServiceCalledCustomers,
            string activityStatus = "",
            string filterText = "")
        {
            try
            {
                brBDMWideServiceCallAnalysis oBrBDMWideServiceCallAnalysis = new brBDMWideServiceCallAnalysis();
                List<BDMWideServiceCallAnalysisDTO> customerActivityDtos = new List<BDMWideServiceCallAnalysisDTO>();
                List<BDMWideServiceCallAnalysis> CustomerActivities = oBrBDMWideServiceCallAnalysis.GetCustomerServiceCallActivities(args.ChiildOriginators,
                    FromDate,
                    ToDate,
                    args.DefaultDepartmentId,
                    args.AddressType,
                    args.StartIndex,
                    args.RowCount,
                    args.SNoofMonths,
                    args.SFilter,
                    args.SMonth,
                    args.LeadStageId,
                    args.LeadStageName,
                    args.SYear,
                    args.WeekInfo,
                    args.OrderBy,
                    args.AdditionalParams,
                    filterText
                    );

                foreach (BDMWideServiceCallAnalysis item in CustomerActivities)
                {
                    customerActivityDtos.Add(ReportAdapter.ConvertToBDMWideServiceCallAnalysisDTO(item));
                }

                return customerActivityDtos;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<LeadDTO> GetNewCustomersForSales(ArgsDTO args,
            int year,
            int month)
        {
            List<LeadDTO> leadDtos = new List<LeadDTO>();
            try
            {
                List<Lead> leads = new List<Lead>();
                leads = new brBDMWideSalesAnalysis().GetNewCustomersForSales(args.Originator, year, month, args.DefaultDepartmentId, args.StartIndex, args.RowCount, args.AdditionalParams);

                foreach (Lead item in leads)
                {
                    leadDtos.Add(LeadAdapter.ConvertToLeadDTO(item));
                }
            }
            catch (Exception)
            {

                throw;
            }
            return leadDtos;
        }

        public List<BDMWideSalesAnalysisDTO> GeActualLiters(string sTotalSql,
            string sWhereColumns,
            string CustCodes,
            ArgsDTO args)
        {
            List<BDMWideSalesAnalysisDTO> actualLitersSalesDto = new List<BDMWideSalesAnalysisDTO>();

            try
            {
                List<BDMWideSalesAnalysis> actualLitersSales = new brBDMWideSalesAnalysis().GeActualLiters(sTotalSql, sWhereColumns, CustCodes, args.StartIndex, args.RowCount);

                foreach (BDMWideSalesAnalysis item in actualLitersSales)
                {
                    actualLitersSalesDto.Add(ConvertToBDMWideSalesAnalysisDTO(item));
                }
            }
            catch (Exception)
            {

                throw;
            }

            return actualLitersSalesDto;
        }

        public List<BDMWideSalesAnalysisDTO> GeActualLiters(ArgsDTO args)
        {
            List<BDMWideSalesAnalysisDTO> actualLitersSalesDto = new List<BDMWideSalesAnalysisDTO>();

            try
            {
                List<BDMWideSalesAnalysis> actualLitersSales = new brBDMWideSalesAnalysis().GeActualLiters(args.Originator, args.SYear, args.SMonth, args.DefaultDepartmentId,
                                args.ChiildOriginators, args.AdditionalParams, args.SNoofMonths, args.DtStartDate, args.StartIndex, args.RowCount, args.OrderBy);

                foreach (BDMWideSalesAnalysis item in actualLitersSales)
                {
                    actualLitersSalesDto.Add(ConvertToBDMWideSalesAnalysisDTO(item));
                }
            }
            catch (Exception)
            {

                throw;
            }

            return actualLitersSalesDto;
        }

        public List<CustomerOpportunitiesDTO> GetCustomerOpportunities(ArgsDTO args)
        {
            List<CustomerOpportunitiesDTO> opportunitiesDto = new List<CustomerOpportunitiesDTO>();

            try
            {
                List<CustomerOpportunities> opportunities = new brOpportunity().GetCustomerOpportunities(args.SFilter, args.DefaultDepartmentId, args.StartIndex, args.RowCount, args.AdditionalParams, args.OrderBy, args.ManagerMode).ToList();

                foreach (CustomerOpportunities item in opportunities)
                {
                    opportunitiesDto.Add(OpportunityAdapter.ConvertToCustomerOpportunityDTO(item));
                }
            }
            catch (Exception)
            {

                throw;
            }

            return opportunitiesDto;
        }

        public List<CustomerOpportunitiesDTO> GetCustomerOpportunitiesCount(ArgsDTO args)
        {
            List<CustomerOpportunitiesDTO> opportunitiesDto = new List<CustomerOpportunitiesDTO>();

            try
            {
                List<CustomerOpportunities> opportunities = new brOpportunity().GetCustomerOpportunitiesCount(args.SFilter, args.DefaultDepartmentId, args.ManagerMode).ToList();

                foreach (CustomerOpportunities item in opportunities)
                {
                    opportunitiesDto.Add(OpportunityAdapter.ConvertToCustomerOpportunityDTO(item));
                }
            }
            catch (Exception)
            {

                throw;
            }

            return opportunitiesDto;
        }

        public List<CustomerActivitiesDTO> GetA20Customers(DateTime dtmStartDate,
            DateTime dtmEndDate,
            ArgsDTO args)
        {
            try
            {
                List<CustomerActivities> customerActivities = new brActivity().GetA20Customers(args.ROriginator, dtmStartDate, dtmEndDate, args.ChiildOriginators, args.AddressType, args.DefaultDepartmentId, args.StartIndex, args.RowCount, args.AdditionalParams, args.OrderBy);
                List<CustomerActivitiesDTO> customerActivitiesList = new List<CustomerActivitiesDTO>();
                foreach (CustomerActivities item in customerActivities)
                {
                    customerActivitiesList.Add(CustomerActivitiesAdapter.ConvertToCustomerActivitiesDTO(item));
                }

                return customerActivitiesList;
            }
            catch (Exception)
            {

                throw;
            }
        }
        public List<CustomerActivitiesDTO> GetA20CustomersCount(DateTime dtmStartDate,
            DateTime dtmEndDate,
            ArgsDTO args)
        {
            try
            {
                List<CustomerActivities> customerActivities = new brActivity().GetA20CustomersCount(args.ROriginator, dtmStartDate, dtmEndDate, args.ChiildOriginators, args.AddressType, args.DefaultDepartmentId, args.StartIndex, args.RowCount, args.AdditionalParams, args.OrderBy);
                List<CustomerActivitiesDTO> customerActivitiesList = new List<CustomerActivitiesDTO>();
                foreach (CustomerActivities item in customerActivities)
                {
                    customerActivitiesList.Add(CustomerActivitiesAdapter.ConvertToCustomerActivitiesDTO(item));
                }

                return customerActivitiesList;
            }
            catch (Exception)
            {

                throw;
            }
        }


        public string GetCustomerServiceCallActivitiesExport(ArgsDTO args,
            DateTime? FromDate,
            DateTime? ToDate,
            bool IsAllCustomers,
            bool IsNotServiceCalledCustomers,
            string activityStatus = "",
            string filterText = "",
            string path = "")
        {
            try
            {
                brBDMWideServiceCallAnalysis oBrBDMWideServiceCallAnalysis = new brBDMWideServiceCallAnalysis();
                // List<BDMWideServiceCallAnalysisDTO> customerActivityDtos = new List<BDMWideServiceCallAnalysisDTO>();
                string CustomerActivities = oBrBDMWideServiceCallAnalysis.GetCustomerServiceCallActivitiesExport(args.ChiildOriginators,
                    FromDate,
                    ToDate,
                    args.DefaultDepartmentId,
                    args.AddressType,
                    args.StartIndex,
                    args.RowCount,
                    args.SNoofMonths,
                    args.SFilter,
                    args.SMonth,
                    args.LeadStageId,
                    args.LeadStageName,
                    args.SYear,
                    args.WeekInfo,
                    args.OrderBy,
                    args.AdditionalParams,
                    filterText, path
                    );



                return CustomerActivities;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetCombinedCollectionByStateExport(int iLeadStageId,
           string sFromDate,
           string sEndDate,
           ArgsDTO args,
           string sLeadStage = "",
           string path = "")
        {
            try
            {

                return oBrLead.GetCombinedCollectionByStateExport(args.Originator,
                    iLeadStageId,
                    sFromDate,
                    sEndDate,
                    args.ChiildOriginators,
                    args.AddressType,
                    args.AdditionalParams,
                    args.OrderBy,
                    args.StartIndex,
                    args.RowCount,
                    sLeadStage, path
                    );
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetLeadCustomerCollectionByRepExport(int iLeadStageId,
            string sFromDate,
            string sEndDate,
            ArgsDTO args,
            string sLeadStage = "",
           string path = "")
        {
            try
            {

                return oBrLead.GetLeadCustomerCollectionByRepExport(
                    sFromDate,
                    sEndDate,
                    args.Originator,
                    iLeadStageId,
                    sLeadStage,
                    args.AdditionalParams,
                    args.StartIndex,
                    args.ChiildOriginators,
                    args.AddressType,
                    args.RowCount,
                    args.OrderBy,
                    path);
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetCallCycleListExport(ArgsDTO args,
            string sFromDate = "",
            string sToDate = "",
            string sFilter = "All",
            string activityStatus = "",
            string path = "")
        {
            try
            {
                string callCycleGraphs = oBrCallCycleGraph.GetCallCycleListExport(args.StartIndex,
                    args.RowCount,
                    sFromDate,
                    sToDate,
                    args.Originator,
                    sFilter,
                    activityStatus,
                    args.ManagerMode,
                    args.AdditionalParams,
                    args.OrderBy,
                    path);

                return callCycleGraphs;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GetAllActivitiesByTypeExport(ArgsDTO args,
            string originator,
            string sFromDate = "",
            string sEndDate = "",
            string activityStatus = "",
            string path = "")
        {
            try
            {
                List<CustomerActivitiesDTO> customerActivityDtos = new List<CustomerActivitiesDTO>();
                string CustomerActivities = oBrActivity.GetAllActivitiesByTypeExport(originator,
                    args.DefaultDepartmentId,
                    args.StartIndex,
                    args.RowCount,
                    args.OrderBy,
                    "",
                    sFromDate,
                    sEndDate,
                    activityStatus,
                    args.ManagerMode,
                    args.AdditionalParams,
                    path);

                return CustomerActivities;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public string GeActualLitersExport(ArgsDTO args, string path)
        {
            string actualLitersSales = new brBDMWideSalesAnalysis().GeActualLitersExport(args.Originator, args.SYear, args.SMonth, args.DefaultDepartmentId,
                                args.ChiildOriginators, args.AdditionalParams, args.SNoofMonths, args.DtStartDate, args.StartIndex, args.RowCount, args.OrderBy, path);



            return actualLitersSales;
        }
        public string GetA20CustomersExport(DateTime dtmStartDate,
            DateTime dtmEndDate,
            ArgsDTO args,
            string path)
        {
            try
            {
                string customerActivities = new brActivity().GetA20CustomersExport(args.ROriginator, dtmStartDate, dtmEndDate, args.ChiildOriginators, args.AddressType,
                    args.DefaultDepartmentId, args.StartIndex, args.RowCount, args.AdditionalParams, args.OrderBy, path);
                return customerActivities;
            }
            catch (Exception)
            {

                throw;
            }
        }
        */
        #endregion
    }  
}
