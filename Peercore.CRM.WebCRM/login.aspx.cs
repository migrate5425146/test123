﻿using CRMServiceReference;
using Peercore.CRM.Shared;
using RestSharp;
using System;
using System.Configuration;
using System.IO;

public partial class login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }   
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        div_loader.Visible = true;

        OriginatorClient OriginatorClient = new OriginatorClient();
        OriginatorDTO OriginatorDTO = new OriginatorDTO();

        string uid = username.Text;
        string pwd = password.Text;

        try
        {
            if (uid == "" || pwd == "")
            {
                divError.Text = "User name or password cannot be blank.";
                div_loader.Visible = false;
                return;
            }

            string logId = DateTime.Now.ToString("yyyyMMddHHmmssfff");
            CreateLog(uid, pwd, logId, "Try Login");

            OriginatorDTO = OriginatorClient.GetLoginDetails(uid, pwd);

            if (OriginatorDTO.OriginatorId > 0)
            {
                CreateLog(uid, pwd, logId, "Done");

                UserSession.Instance.OriginatorId = OriginatorDTO.OriginatorId;
                UserSession.Instance.Originator = OriginatorDTO.Name;
                UserSession.Instance.OriginatorCookers = OriginatorDTO.Name;
                //UserSession.Instance.UserName = uid;
                UserSession.Instance.OriginalUserName = uid;
                UserSession.Instance.DefaultDepartmentId = OriginatorDTO.DefaultDepartmentId;
                UserSession.Instance.OriginatorEmail = OriginatorDTO.Email;

                UserSession.Instance.ManagerMode = true;
                UserSession.Instance.HasChildReps = OriginatorDTO.HasChildReps;
                UserSession.Instance.ChildOriginators = OriginatorClient.GetChildOriginators(Request.QueryString["uname"], true);
                string rep = "";
                string childOriginatorsForRep = UserSession.Instance.ChildOriginators.Replace("originator", "r.originator");
                UserSession.Instance.RepCodeList = OriginatorClient.GetChildReps(Request.QueryString["uname"], UserSession.Instance.ManagerMode, ref rep, childOriginatorsForRep);
                UserSession.Instance.ChildReps = rep;

                UserSession.Instance.BDMList = OriginatorClient.GetBDMString();
                UserSession.Instance.CRMAuthLevel = OriginatorDTO.CRMAuthLevel;

                UserSession.Instance.ClientType = OriginatorDTO.ClientType;
                UserSession.Instance.RepCode = OriginatorDTO.RepCode;
                UserSession.Instance.RepType = OriginatorDTO.RepType;
                UserSession.Instance.OriginatorString = OriginatorDTO.DeptString;
                UserSession.Instance.AccessToken = OriginatorDTO.AccessToken;

                UserSession.Instance.TFAPin = OriginatorDTO.TFAPin;
                UserSession.Instance.TFAIsAuthenticated = OriginatorDTO.TFAIsAuthenticated;
                UserSession.Instance.IsTFAUser = OriginatorDTO.IsTFAUser;
                
                try
                {
                    CommonClient cClient = new CommonClient();
                    cClient.CreateTransactionLog(UserSession.Instance.OriginalUserName,
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        TransactionTypeModules.Login,
                        "Backend-Login",
                        OriginatorDTO.Name + " is logged in to the system");
                }
                catch { }

                if (OriginatorDTO.IsTFAUser)
                {
                    //if (OriginatorDTO.TFAIsAuthenticated)
                    //{
                    //    Response.Redirect("2fauth/gauth/verify.aspx");
                    //}
                    //else
                    //{
                    //    Response.Redirect("2fauth/gauth/auth.aspx");
                    //}

                    OtpUser otpUser = new OtpUser();
                    otpUser.userName = UserSession.Instance.OriginalUserName;

                    var client = new RestClient("https://lkamsales.sl.pvmgrp.com/DialogSMS/smsgateway/sendotp");
                    client.Timeout = -1;
                    var request = new RestRequest(Method.POST);
                    request.AddJsonBody(otpUser);

                    //var body = @"{
                    //        " + "\n" +
                    //        @"    ""userName"": """+ otpUser + "" + "\n " +
                    //        @"}";

                    //string otpname = "udara";
                    //var body = @"{""userName"":""" + otpname + "" +
                    //    "}";

                    //request.AddParameter("application/json", body, ParameterType.RequestBody);
                    IRestResponse response = client.Execute(request);
                    Console.WriteLine(response.Content);

                    if (response.Content.Contains("OK"))
                    {
                        divloginform.Visible = false;
                        divotpverifyform.Visible = true;
                    }
                }
                else
                {
                    UserSession.Instance.UserName = UserSession.Instance.OriginalUserName;

                    Response.Redirect("default.aspx");
                }

                div_loader.Visible = false;
            }
            else
            {
                CreateLog(uid, pwd, logId, "Faild");

                divError.Text = "Invalid login. Please try again.";
                div_loader.Visible = false;
                return;
            }

            OriginatorClient.Close();
        }
        catch (Exception ex)
        {
            div_loader.Visible = false;
            divError.Text = "Invalid login. Please try again.";
        }
        //return txt.ToString();
    }

    protected void btnVerify_Click(object sender, EventArgs e)
    {
        OtpUser otpUser = new OtpUser();
        otpUser.userName = UserSession.Instance.OriginalUserName;
        otpUser.otpNumber = txtOtpNumber.Text;

        var client = new RestClient("https://lkamsales.sl.pvmgrp.com/DialogSMS/smsgateway/verifyotp");
        client.Timeout = -1;
        var request = new RestRequest(Method.POST);
        request.AddJsonBody(otpUser);

        IRestResponse response = client.Execute(request);
        Console.WriteLine(response.Content);

        if (response.Content.Contains("OK"))
        {
            UserSession.Instance.UserName = UserSession.Instance.OriginalUserName;
            Response.Redirect("default.aspx");
        }
        else
        {
            divOtpError.Text = "Invalid OTP number.";
            div_loader.Visible = false;
            return;
        }
    } 
    
    protected void btnResendOtp_Click(object sender, EventArgs e)
    {
        OtpUser otpUser = new OtpUser();
        otpUser.userName = UserSession.Instance.OriginalUserName;

        var client = new RestClient("https://lkamsales.sl.pvmgrp.com/DialogSMS/smsgateway/sendotp");
        client.Timeout = -1;
        var request = new RestRequest(Method.POST);
        request.AddJsonBody(otpUser);

        IRestResponse response = client.Execute(request);
        Console.WriteLine(response.Content);

        if (response.Content.Contains("OK"))
        {
            divloginform.Visible = false;
            divotpverifyform.Visible = true;
        }
    }

    public void CreateLog(string originator, string password, string logId, string status)
    {
        string filename = "WebLogin_" + DateTime.Now.ToString("yyyyMMdd");
        var path = System.IO.Path.GetDirectoryName(
            System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
        string filePath = ConfigurationManager.AppSettings["CreateLoginLogPath"].ToString() + filename + ".txt";

        try
        {
            // Check if file already exists. If yes, delete it. 
            using (StreamWriter sw = new StreamWriter(filePath, true))
            {
                string sr = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss:fff tt") + "  " + originator + "  |  " + password + "  |  " + logId + "  |  " + status + Environment.NewLine;
                sw.Write(sr);
            }
        }
        catch (Exception Ex)
        {
            Console.WriteLine(Ex.ToString());
        }
    }
}

public class OtpUser
{
    public string userName { get; set; }
    public string otpNumber { get; set; }
}