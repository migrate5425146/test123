﻿using System;
using System.Web;
using System.Web.Services;
using System.Web.Services.Protocols;
using System.Web.Script.Services;
using System.Collections.Generic;
using System.Reflection;
using System.ComponentModel;
using Peercore.CRM.Shared;
using System.ComponentModel;
using CRMServiceReference;
using Peercore.CRM.Common;
using System.Web.UI;

using System.Collections.Generic;
using System.Data;
using System.Linq;

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class lead_contacts_Service : System.Web.Services.WebService
{

 
    LeadCustomerClient cookerService = new LeadCustomerClient();
    EndUserClient endUserService = new EndUserClient();
    int totalRows = 10000;

    public List<string> BackButtonForAll
    {
        get
        {
            if (Session[CommonUtility.BACK_BUTTON_FOR_ALL] != null)
                return ((List<string>)(Session[CommonUtility.BACK_BUTTON_FOR_ALL]));

            return null;
        }
        set
        {
            if (value != null)
                Session[CommonUtility.BACK_BUTTON_FOR_ALL] = value;
            else
                Session.Remove(CommonUtility.BACK_BUTTON_FOR_ALL);
        }
    }
 
    [WebMethod(EnableSession=true)]
    public DataSourceResult GetDataAndCount(string activeInactiveChecked, string reqSentIsChecked, string repType, string gridName, int take, int skip, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<LeadCustomerDTO> data = null;
        List<EndUserDTO> endUserList = null;
        int rowCount = 0;
        ArgsDTO args = new ArgsDTO();
        ColumnSettingDTO ColumnSettingDTO = new ColumnSettingDTO();
        CommonClient serviceCommon = new CommonClient();

        const string CLIENT_SORT_ORDER = "CLIENT_SORT_ORDER";
        const string CLIENT_SORT_NAME = "CLIENT_SORT_NAME";
        const string CLIENT_SORT_FLAG = "CLIENT_SORT_FLAG";
        const string CLEAR_FLAG = "CLEAR_FLAG";
        const string ORDER_BY = "ORDER_BY";
        List<Sort> sortExpression = (List<Sort>)sort;

        if (Session != null && Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            if (Session[CLIENT_SORT_ORDER] != null && Session[CLIENT_SORT_NAME] != null && Session[CLIENT_SORT_ORDER] != "" && Session[CLIENT_SORT_NAME] != "" && Convert.ToBoolean(Session[CLIENT_SORT_FLAG]) == true)
            {
                ColumnSettingDTO.sortName = Convert.ToString(Session[CLIENT_SORT_NAME]);
                ColumnSettingDTO.sortType = Convert.ToString(Session[CLIENT_SORT_ORDER]);
                Session[CLIENT_SORT_FLAG] = false;
            }
            else
                ColumnSettingDTO = serviceCommon.GetSortItems(UserSession.Instance.UserName, gridName);

            args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
            args.ActiveInactiveChecked = activeInactiveChecked == "1";
            args.ReqSentIsChecked = reqSentIsChecked == "1";
            args.RepType = repType;
            args.RowCount = take;
            args.AdditionalParams = "";

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter,"leadcontact", args.LeadStage);
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            args.Originator = UserSession.Instance.UserName;
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.ChildReps = UserSession.Instance.ChildReps;
            args.CRMAuthLevel = (!string.IsNullOrWhiteSpace(UserSession.Instance.FilterByUserName)) ? 0 : UserSession.Instance.CRMAuthLevel;

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<string> back = new List<string>();
            back.Add(args.StartIndex.ToString());
            BackButtonForAll = back;

            if (Convert.ToBoolean(Session[CLEAR_FLAG]) == true)
            {
                args.OrderBy = Convert.ToString(Session[ORDER_BY]);
            }
            else if (ColumnSettingDTO.sortType != null && ColumnSettingDTO.sortName != null)
            {
                if (sortExpression != null &&  sortExpression.Count > 0)
                {
                    args.OrderBy = "";
                    foreach (Sort item in sortExpression)
                    {
                        item.Field = GetSortType(item.Field, args.LeadStage);
                        if (string.IsNullOrEmpty(args.OrderBy))
                            args.OrderBy =  item.ToExpression();
                        else
                            args.OrderBy = ", "+args.OrderBy + item.ToExpression();
                    }

                    Session[ORDER_BY] = args.OrderBy;
                }
                else
                {
                    args.OrderBy = GetSortType(ColumnSettingDTO.sortName, args.LeadStage) + (ColumnSettingDTO.sortType == "A" ? " ASC" : "DESC");
                    Session[ORDER_BY] = args.OrderBy;
                }
            }
            else if (sortExpression != null && sortExpression.Count > 0)
            {
                args.OrderBy = "";
                foreach (Sort item in sortExpression)
                {
                    item.Field = GetSortType(item.Field, args.LeadStage);
                    if (string.IsNullOrEmpty(args.OrderBy))
                        args.OrderBy = item.ToExpression();
                    else
                        args.OrderBy = ", " + args.OrderBy + item.ToExpression();
                }

                Session[ORDER_BY] = args.OrderBy;
            }
            else
            {
                args.OrderBy = "Name ASC";
                Session[ORDER_BY] = "Name ASC";
            }

            if (ColumnSettingDTO != null )
            {
                if (ColumnSettingDTO.sortType == null)
                    ColumnSettingDTO.sortType = "A";
                if (ColumnSettingDTO.sortName == null)
                    ColumnSettingDTO.sortName = "Name";
            }

            SetSortType(ColumnSettingDTO.sortType, ColumnSettingDTO.sortName);
        }
        else
        {
            args = new ArgsDTO();
        }


        if (args.LeadStage.ToLower() == "pendingcustomer")
            data = cookerService.GetCombinedCollection(args);
        else if (args.LeadStage.ToLower() != "enduser")
            data = cookerService.GetCombinedCollection(args);
        else        
            endUserList = endUserService.GetAllEndUsers(args);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (args.LeadStage.ToLower() != "enduser" && data != null && data.Count > 0)
        {
            CallCycleContactDTO contact = new CallCycleContactDTO();

            List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
            listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
            if (listCallCycleContact == null)
                listCallCycleContact = new List<CallCycleContactDTO>();

            foreach (LeadCustomerDTO item in data)
            {
                if (item.LeadStage == "Customer")
                {
                    contact = listCallCycleContact.Find(c => c.Contact.CustomerCode == item.CustomerCode);
                    if (contact != null)
                    {
                        item.SelectedToCall = "checked";
                        contact.rowCount = data.Count(c => c.CustomerCode == item.CustomerCode);
                    }
                    if (item.Address.Length > 38)
                        item.Address = item.Address.Substring(0, 35) + "...";
                }
                else
                {
                    contact = listCallCycleContact.Find(c => c.Contact.SourceId == item.SourceId);
                    if (contact != null)
                    {
                        item.SelectedToCall = "checked";
                        contact.rowCount = data.Count(c => c.SourceId == item.SourceId);
                    }
                    
                }
            }

            dataSourceResult.Data = data;
            dataSourceResult.Total = data[0].rowCount;
        }
        else if (endUserList != null && endUserList.Count > 0)
        {
            CallCycleContactDTO contact = new CallCycleContactDTO();

            List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
            listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
            if (listCallCycleContact == null)
                listCallCycleContact = new List<CallCycleContactDTO>();

            foreach (EndUserDTO item in endUserList)
            {
                contact = listCallCycleContact.Find(c => c.Contact.CustomerCode == item.CustomerCode && c.Contact.EndUserCode == item.EndUserCode);

                if (contact != null)
                {
                    item.SelectedToCall = "checked";
                }

                if (item.Address1.Length > 18)
                    item.Address1 = item.Address1.Substring(0, 15) + "...";
                if (item.CustomerName.Length > 23)
                    item.CustomerName = item.CustomerName.Substring(0, 20) + "...";
            }

            dataSourceResult.Data = endUserList;
            dataSourceResult.Total = endUserList[0].rowCount;
        }

       return dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetDataAndCountForCheckList(int CheckListId, string activeInactiveChecked, string reqSentIsChecked, string repType, string gridName, int take, int skip, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<LeadCustomerDTO> data = null;
        List<EndUserDTO> endUserList = null;
        int rowCount = 0;
        ArgsDTO args = new ArgsDTO();
        ColumnSettingDTO ColumnSettingDTO = new ColumnSettingDTO();
        CommonClient serviceCommon = new CommonClient();

        const string CLIENT_SORT_ORDER = "CLIENT_SORT_ORDER";
        const string CLIENT_SORT_NAME = "CLIENT_SORT_NAME";
        const string CLIENT_SORT_FLAG = "CLIENT_SORT_FLAG";
        const string CLEAR_FLAG = "CLEAR_FLAG";
        const string ORDER_BY = "ORDER_BY";
        List<Sort> sortExpression = (List<Sort>)sort;

        if (Session != null && Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            if (Session[CLIENT_SORT_ORDER] != null && Session[CLIENT_SORT_NAME] != null && Session[CLIENT_SORT_ORDER] != "" && Session[CLIENT_SORT_NAME] != "" && Convert.ToBoolean(Session[CLIENT_SORT_FLAG]) == true)
            {
                ColumnSettingDTO.sortName = Convert.ToString(Session[CLIENT_SORT_NAME]);
                ColumnSettingDTO.sortType = Convert.ToString(Session[CLIENT_SORT_ORDER]);
                Session[CLIENT_SORT_FLAG] = false;
            }
            else
                ColumnSettingDTO = serviceCommon.GetSortItems(UserSession.Instance.UserName, gridName);

            args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
            args.ActiveInactiveChecked = activeInactiveChecked == "1";
            args.ReqSentIsChecked = reqSentIsChecked == "1";
            args.RepType = repType;
            args.RowCount = take;
            args.AdditionalParams = "";

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "leadcontact", args.LeadStage);
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }
            args.Originator = UserSession.Instance.UserName;

            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.ChildReps = UserSession.Instance.ChildReps;
            args.CRMAuthLevel = (!string.IsNullOrWhiteSpace(UserSession.Instance.FilterByUserName)) ? 0 : UserSession.Instance.CRMAuthLevel;

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<string> back = new List<string>();
            back.Add(args.StartIndex.ToString());
            BackButtonForAll = back;


            if (Convert.ToBoolean(Session[CLEAR_FLAG]) == true)
            {
                args.OrderBy = Convert.ToString(Session[ORDER_BY]);
            }
            else if (ColumnSettingDTO.sortType != null && ColumnSettingDTO.sortName != null)
            {
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = "";
                    foreach (Sort item in sortExpression)
                    {
                        item.Field = GetSortType(item.Field, args.LeadStage);
                        if (string.IsNullOrEmpty(args.OrderBy))
                            args.OrderBy = item.ToExpression();
                        else
                            args.OrderBy = ", " + args.OrderBy + item.ToExpression();
                    }

                    Session[ORDER_BY] = args.OrderBy;
                }
                else
                {
                    args.OrderBy = GetSortType(ColumnSettingDTO.sortName, args.LeadStage) + (ColumnSettingDTO.sortType == "A" ? " ASC" : "DESC");
                    Session[ORDER_BY] = args.OrderBy;
                }
            }
            else if (sortExpression != null && sortExpression.Count > 0)
            {
                args.OrderBy = "";
                foreach (Sort item in sortExpression)
                {
                    item.Field = GetSortType(item.Field, args.LeadStage);
                    if (string.IsNullOrEmpty(args.OrderBy))
                        args.OrderBy = item.ToExpression();
                    else
                        args.OrderBy = ", " + args.OrderBy + item.ToExpression();
                }

                Session[ORDER_BY] = args.OrderBy;
            }
            else
            {
                args.OrderBy = "Name ASC";
                Session[ORDER_BY] = "Name ASC";
            }

            if (ColumnSettingDTO != null)
            {
                if (ColumnSettingDTO.sortType == null)
                    ColumnSettingDTO.sortType = "A";
                if (ColumnSettingDTO.sortName == null)
                    ColumnSettingDTO.sortName = "Name";
            }
            SetSortType(ColumnSettingDTO.sortType, ColumnSettingDTO.sortName);
        }
        else
        {
            args = new ArgsDTO();
        }

        data = cookerService.GetLeadCollectionForCheckList(CheckListId, args);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (args.LeadStage.ToLower() != "enduser" && data != null && data.Count > 0)
        {
            CallCycleContactDTO contact = new CallCycleContactDTO();

            List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
            listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
            if (listCallCycleContact == null)
                listCallCycleContact = new List<CallCycleContactDTO>();

            foreach (LeadCustomerDTO item in data)
            {
                if (item.LeadStage == "Customer")
                {
                    contact = listCallCycleContact.Find(c => c.Contact.CustomerCode == item.CustomerCode);
                    if (contact != null)
                    {
                        item.SelectedToCall = "checked";
                        contact.rowCount = data.Count(c => c.CustomerCode == item.CustomerCode);
                    }
                    if (item.Address.Length > 38)
                        item.Address = item.Address.Substring(0, 35) + "...";
                }
                else
                {
                    contact = listCallCycleContact.Find(c => c.Contact.SourceId == item.SourceId);
                    if (contact != null)
                    {
                        item.SelectedToCall = "checked";
                        contact.rowCount = data.Count(c => c.SourceId == item.SourceId);
                    }

                }
            }

            dataSourceResult.Data = data;
            dataSourceResult.Total = data[0].rowCount;
        }
        else if (endUserList != null && endUserList.Count > 0)
        {
            CallCycleContactDTO contact = new CallCycleContactDTO();

            List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
            listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
            if (listCallCycleContact == null)
                listCallCycleContact = new List<CallCycleContactDTO>();

            foreach (EndUserDTO item in endUserList)
            {
                contact = listCallCycleContact.Find(c => c.Contact.CustomerCode == item.CustomerCode && c.Contact.EndUserCode == item.EndUserCode);

                if (contact != null)
                {
                    item.SelectedToCall = "checked";
                }

                if (item.Address1.Length > 18)
                    item.Address1 = item.Address1.Substring(0, 15) + "...";
                if (item.CustomerName.Length > 23)
                    item.CustomerName = item.CustomerName.Substring(0, 20) + "...";
            }

            dataSourceResult.Data = endUserList;
            dataSourceResult.Total = endUserList[0].rowCount;
        }

        return dataSourceResult;
    }

    private string GetSortType(string sortName,string leadStage)
    {
        string sortingType = "";

        try
        {
            if (leadStage.ToLower() != "enduser")
            {
                if (sortName == "Name")
                {
                    sortingType = "Name";
                }
                else if (sortName == "BDM")
                {
                    sortingType = "Originator";
                }
                else if (sortName == "LeadStage")
                {
                    sortingType = "lead_stage";
                }
                else if (sortName == "CustomerCode")
                {
                    sortingType = "cust_code";
                }
                else if (sortName == "State")
                {
                    sortingType = "state";
                }
                else if (sortName == "StartDate")
                {
                    sortingType = "StartDate";
                }
                else if (sortName == "City")
                {
                    sortingType = "City";
                }
                else if (sortName == "Channel")
                {
                    sortingType = "ChannelDescription";
                }
                else if (sortName == "Address")
                {
                    sortingType = "address_1";
                }
                else if (sortName == "SourceId")
                {
                    sortingType = "lead_id";
                }
                else if (sortName == "DistributorCode")
                {
                    sortingType = "dis_code";
                }
                else if (sortName == "DistributorName")
                {
                    sortingType = "dis_name";
                }
                else if (sortName == "LastInvoiceDate")
                {
                    sortingType = "Last_invoice_date";
                }
                else if (sortName == "RouteName")
                {
                    sortingType = "route_name";
                }
                else if (sortName == "RepCode")
                {
                    sortingType = "originator";
                }
                else if (sortName == "RepName")
                {
                    sortingType = "rep_name";
                }
            }
            else
            {
                if (sortName == "CustomerCode")
                {
                    sortingType = "cust_code";
                }
                else if (sortName == "EndUserCode")
                {
                    sortingType = "enduser_code";
                }
                else if (sortName == "Name")
                {
                    sortingType = "name";
                }
                else if (sortName == "Address1")
                {
                    sortingType = "address_1";
                }
                else if (sortName == "Address2")
                {
                    sortingType = "address_2";
                }

                else if (sortName == "City")
                {
                    sortingType = "city";
                }
                else if (sortName == "PostCode")
                {
                    sortingType = "postcode";
                }
                else if (sortName == "State")
                {
                    sortingType = "state";
                }
                else if (sortName == "Telephone")
                {
                    sortingType = "telephone";
                }
                else if (sortName == "Contact")
                {
                    sortingType = "contact";
                }

                else if (sortName == "RepCode")
                {
                    sortingType = "rep_code";
                }
                else if (sortName == "EmailAddress")
                {
                    sortingType = "email_address";
                }
                else if (sortName == "Version")
                {
                    sortingType = "version";
                }

                else if (sortName == "CustomerName")
                {
                    sortingType = "cust_name";
                }
                else if (sortName == "LastInvoiceDate")
                {
                    sortingType = "Last_invoice_date";
                }
            }
            return sortingType;
        }
        catch (Exception)
        {

            throw;
        }
    }

    private void SetSortType(string sortType, string sortName)
    {
        const string SORT_ORDER = "SORT_ORDER";
        const string SORT_NAME = "SORT_NAME";

        try
        {
            switch (sortType)
            {
                case "A":
                    if (sortName == "Name")
                    {
                        Session[SORT_ORDER] = "A";
                        Session[SORT_NAME] = "Name";
                    }
                    if (sortName == "Originator")
                    {
                        Session[SORT_ORDER] = "A";
                        Session[SORT_NAME] = "BDM";
                    }
                    if (sortName == "lead_stage")
                    {
                        Session[SORT_ORDER] = "A";
                        Session[SORT_NAME] = "Lead Stage";
                    }
                    if (sortName == "cust_code")
                    {
                        Session[SORT_ORDER] = "A";
                        Session[SORT_NAME] = "Cust Code";
                    }
                    if (sortName == "state")
                    {
                        Session[SORT_ORDER] = "A";
                        Session[SORT_NAME] = "State";
                    }
                    if (sortName == "StartDate")
                    {
                        Session[SORT_ORDER] = "A";
                        Session[SORT_NAME] = "Start Date";
                    }
                    if (sortName == "City")
                    {
                        Session[SORT_ORDER] = "A";
                        Session[SORT_NAME] = "Suburb";
                    }
                    if (sortName == "ChannelDescription")
                    {
                        Session[SORT_ORDER] = "A";
                        Session[SORT_NAME] = "Channel";
                    }
                    if (sortName == "SourceId")
                    {
                        Session[SORT_ORDER] = "A";
                        Session[SORT_NAME] = "CRM Ref. No.";
                    }
                    break;
                case "D":
                    if (sortName == "Name")
                    {
                        Session[SORT_ORDER] = "D";
                        Session[SORT_NAME] = "Name";
                    }
                    if (sortName == "Originator")
                    {
                        Session[SORT_ORDER] = "D";
                        Session[SORT_NAME] = "BDM";
                    }
                    if (sortName == "lead_stage")
                    {
                        Session[SORT_ORDER] = "D";
                        Session[SORT_NAME] = "Lead Stage";
                    }
                    if (sortName == "cust_code")
                    {
                        Session[SORT_ORDER] = "D";
                        Session[SORT_NAME] = "Cust Code";
                    }
                    if (sortName == "state")
                    {
                        Session[SORT_ORDER] = "D";
                        Session[SORT_NAME] = "State";
                    }
                    if (sortName == "StartDate")
                    {
                        Session[SORT_ORDER] = "D";
                        Session[SORT_NAME] = "Start Date";
                    }
                    if (sortName == "City")
                    {
                        Session[SORT_ORDER] = "D";
                        Session[SORT_NAME] = "Suburb";
                    }
                    if (sortName == "ChannelDescription")
                    {
                        Session[SORT_ORDER] = "D";
                        Session[SORT_NAME] = "Channel";
                    }
                    if (sortName == "SourceId")
                    {
                        Session[SORT_ORDER] = "D";
                        Session[SORT_NAME] = "CRM Ref. No.";
                    }
                    break;
            }

        }
        catch (Exception)
        {

            throw;
        }
    }

    //public void SetFilterField(Filter filter, string leadStage)
    //{
    //    if (filter.Filters != null)
    //    {
    //        foreach (Filter item in filter.Filters)
    //        {
    //            item.Field = GetSortType(item.Field, leadStage);
    //            if (item.Filters != null)
    //            {
    //                foreach (Filter itemTo in item.Filters)
    //                {
    //                    SetFilterField(itemTo, leadStage);
    //                }
    //            }
    //        }
    //    }
    //}



    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllProgramCustomer(string activeInactiveChecked, string reqSentIsChecked, string repType, string gridName, int take, int skip, IEnumerable<Sort> sort, Filter filter, string pgindex, int ProgramId)
    {

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<LeadCustomerDTO> data = null;
        List<EndUserDTO> endUserList = null;
       
        int rowCount = 0;
        ArgsDTO args = new ArgsDTO();
        ColumnSettingDTO ColumnSettingDTO = new ColumnSettingDTO();
        CommonClient serviceCommon = new CommonClient(); 
        ProgramDTO oProgram = new ProgramDTO();

        const string CLIENT_SORT_ORDER = "CLIENT_SORT_ORDER";
        const string CLIENT_SORT_NAME = "CLIENT_SORT_NAME";
        const string CLIENT_SORT_FLAG = "CLIENT_SORT_FLAG";
        const string CLEAR_FLAG = "CLEAR_FLAG";
        const string ORDER_BY = "ORDER_BY";
        List<Sort> sortExpression = (List<Sort>)sort;
        
        if (Session != null && Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            if (Session[CLIENT_SORT_ORDER] != null && Session[CLIENT_SORT_NAME] != null && Session[CLIENT_SORT_ORDER] != "" && Session[CLIENT_SORT_NAME] != "" && Convert.ToBoolean(Session[CLIENT_SORT_FLAG]) == true)
            {
                ColumnSettingDTO.sortName = Convert.ToString(Session[CLIENT_SORT_NAME]);
                ColumnSettingDTO.sortType = Convert.ToString(Session[CLIENT_SORT_ORDER]);
                Session[CLIENT_SORT_FLAG] = false;
            }
            else
                ColumnSettingDTO = serviceCommon.GetSortItems(UserSession.Instance.UserName, gridName);

            args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
            args.ActiveInactiveChecked = activeInactiveChecked == "1";
            args.ReqSentIsChecked = reqSentIsChecked == "1";
            args.RepType = repType;
            args.RowCount = take;
            args.AdditionalParams = "";

            //args.AddressType = "A";
            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "leadcontact", args.LeadStage);
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }
            args.Originator = UserSession.Instance.UserName;

            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.ChildReps = UserSession.Instance.ChildReps;
            args.CRMAuthLevel = (!string.IsNullOrWhiteSpace(UserSession.Instance.FilterByUserName)) ? 0 : UserSession.Instance.CRMAuthLevel;

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<string> back = new List<string>();
            back.Add(args.StartIndex.ToString());
            BackButtonForAll = back;


            if (Convert.ToBoolean(Session[CLEAR_FLAG]) == true)
            {
                args.OrderBy = Convert.ToString(Session[ORDER_BY]);
            }
            else if (ColumnSettingDTO.sortType != null && ColumnSettingDTO.sortName != null)
            {


                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = "";
                    foreach (Sort item in sortExpression)
                    {
                        item.Field = GetSortType(item.Field, args.LeadStage);
                        if (string.IsNullOrEmpty(args.OrderBy))
                            args.OrderBy = item.ToExpression();
                        else
                            args.OrderBy = ", " + args.OrderBy + item.ToExpression();
                    }
                    Session[ORDER_BY] = args.OrderBy;
                }
                else
                {
                    args.OrderBy = GetSortType(ColumnSettingDTO.sortName, args.LeadStage) + (ColumnSettingDTO.sortType == "A" ? " ASC" : "DESC");
                    Session[ORDER_BY] = args.OrderBy;
                }
            }
            else if (sortExpression != null && sortExpression.Count > 0)
            {
                args.OrderBy = "";
                foreach (Sort item in sortExpression)
                {
                    item.Field = GetSortType(item.Field, args.LeadStage);
                    if (string.IsNullOrEmpty(args.OrderBy))
                        args.OrderBy = item.ToExpression();
                    else
                        args.OrderBy = ", " + args.OrderBy + item.ToExpression();
                }
                Session[ORDER_BY] = args.OrderBy;
            }
            else
            {
                args.OrderBy = "Name ASC";
                Session[ORDER_BY] = "Name ASC";
            }

            if (ColumnSettingDTO != null)
            {
                if (ColumnSettingDTO.sortType == null)
                    ColumnSettingDTO.sortType = "A";
                if (ColumnSettingDTO.sortName == null)
                    ColumnSettingDTO.sortName = "Name";
            }
            SetSortType(ColumnSettingDTO.sortType, ColumnSettingDTO.sortName);
        }
        else
        {
            args = new ArgsDTO();
        }

        data = cookerService.GetCombinedCollection(args);        

        DataSourceResult dataSourceResult = new DataSourceResult();

        if ( data != null && data.Count > 0)
        {
            List<string> listCallCycleContact = new List<string>();
            listCallCycleContact = (List<string>)Session[CommonUtility.CUSTOMER_PROGRAM_CONTACTS];
            if (listCallCycleContact == null)
                listCallCycleContact = new List<string>();

            foreach (LeadCustomerDTO item in data)
            {
                if (item.LeadStage == "Customer")
                {
                    if (listCallCycleContact.Contains(item.CustomerCode))
                    {
                        item.SelectedToCall = "checked";
                    }
                    if (item.Address.Length > 38)
                        item.Address = item.Address.Substring(0, 35) + "...";
                }
            }

            dataSourceResult.Data = data;
            dataSourceResult.Total = data[0].rowCount;
        }

        return dataSourceResult;
    }


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GetPendingCustomersCount()
    {
        LeadCustomerClient cookersService = new LeadCustomerClient();
        ArgsDTO args = new ArgsDTO();
        args.ActiveInactiveChecked = true;
        args.RepType = "B";
        args.AdditionalParams = "";
        args.Originator = UserSession.Instance.UserName;
        args.ChildOriginators = UserSession.Instance.ChildOriginators;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.OrderBy = "Name ASC";
        
        return cookerService.GetPendingCustomersCount(args).ToString();
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetOutletsForLookupInScheduleEntry(string activeInactiveChecked, string reqSentIsChecked, string repType, string gridName, int take, int skip, IEnumerable<Sort> sort, Filter filter, string pgindex, string dist, string marketId, string volume, string outletTypeId, string town)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<LeadCustomerDTO> data = null;
        List<EndUserDTO> endUserList = null;
        int rowCount = 0;
        ArgsDTO args = new ArgsDTO();
        ColumnSettingDTO ColumnSettingDTO = new ColumnSettingDTO();
        CommonClient serviceCommon = new CommonClient();

        const string CLIENT_SORT_ORDER = "CLIENT_SORT_ORDER";
        const string CLIENT_SORT_NAME = "CLIENT_SORT_NAME";
        const string CLIENT_SORT_FLAG = "CLIENT_SORT_FLAG";
        const string CLEAR_FLAG = "CLEAR_FLAG";
        const string ORDER_BY = "ORDER_BY";
        List<Sort> sortExpression = (List<Sort>)sort;

        if (Session != null && Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            if (Session[CLIENT_SORT_ORDER] != null && Session[CLIENT_SORT_NAME] != null && Session[CLIENT_SORT_ORDER] != "" && Session[CLIENT_SORT_NAME] != "" && Convert.ToBoolean(Session[CLIENT_SORT_FLAG]) == true)
            {
                ColumnSettingDTO.sortName = Convert.ToString(Session[CLIENT_SORT_NAME]);
                ColumnSettingDTO.sortType = Convert.ToString(Session[CLIENT_SORT_ORDER]);
                Session[CLIENT_SORT_FLAG] = false;
            }
            else
                ColumnSettingDTO = serviceCommon.GetSortItems(UserSession.Instance.UserName, gridName);

            args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
            args.ActiveInactiveChecked = activeInactiveChecked == "1";
            args.ReqSentIsChecked = reqSentIsChecked == "1";
            args.RepType = repType;
            args.RowCount = take;
            args.AdditionalParams = "";
            args.SSource = UserSession.Instance.OriginatorString;

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "outletsforlookup", args.LeadStage);
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            //Setting Filter Fields
            //if (!dist.Equals("0") && !dist.Equals(""))
            //{
            //    if (args.AdditionalParams.Equals(""))
            //        args.AdditionalParams = args.AdditionalParams + " c.dist = " + dist;
            //    else
            //        args.AdditionalParams = args.AdditionalParams + " AND c.dist = " + dist;
            //}

            if (!marketId.Equals("0") && !marketId.Equals(""))
            {
                if (args.AdditionalParams.Equals(""))
                    args.AdditionalParams = args.AdditionalParams + " c.market_id = " + marketId;
                else
                    args.AdditionalParams = args.AdditionalParams + " AND c.market_id = " + marketId;
            }

            if (!volume.Equals("0") && !volume.Equals(""))
            {
                if (args.AdditionalParams.Equals(""))
                    args.AdditionalParams = args.AdditionalParams + " c.volume = " + volume;
                else
                    args.AdditionalParams = args.AdditionalParams + " AND c.volume = " + volume;
            }

            if (!outletTypeId.Equals("0") && !outletTypeId.Equals(""))
            {
                if (args.AdditionalParams.Equals(""))
                    args.AdditionalParams = args.AdditionalParams + " c.outlet_typeid = " + outletTypeId;
                else
                    args.AdditionalParams = args.AdditionalParams + " AND c.outlet_typeid = " + outletTypeId;
            }

            if (!town.Equals("0") && !town.Equals(""))
            {
                if (args.AdditionalParams.Equals(""))
                    args.AdditionalParams = args.AdditionalParams + " c.city = '" + town + "'";
                else
                    args.AdditionalParams = args.AdditionalParams + " AND c.city = '" + town + "'";
            }


            args.Originator = UserSession.Instance.UserName;

            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = "PS";
            args.ChildReps = UserSession.Instance.ChildReps;
            args.CRMAuthLevel = (!string.IsNullOrWhiteSpace(UserSession.Instance.FilterByUserName)) ? 0 : UserSession.Instance.CRMAuthLevel;

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<string> back = new List<string>();
            back.Add(args.StartIndex.ToString());
            BackButtonForAll = back;

            if (Convert.ToBoolean(Session[CLEAR_FLAG]) == true)
            {
                args.OrderBy = Convert.ToString(Session[ORDER_BY]);
            }
            else if (ColumnSettingDTO.sortType != null && ColumnSettingDTO.sortName != null)
            {
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = "";
                    foreach (Sort item in sortExpression)
                    {
                        item.Field = GetSortType(item.Field, args.LeadStage);
                        if (string.IsNullOrEmpty(args.OrderBy))
                            args.OrderBy = item.ToExpression();
                        else
                            args.OrderBy = ", " + args.OrderBy + item.ToExpression();
                    }
                    Session[ORDER_BY] = args.OrderBy;
                }
                else
                {
                    args.OrderBy = GetSortType(ColumnSettingDTO.sortName, args.LeadStage) + (ColumnSettingDTO.sortType == "A" ? " ASC" : "DESC");
                    Session[ORDER_BY] = args.OrderBy;
                }
            }
            else if (sortExpression != null && sortExpression.Count > 0)
            {
                args.OrderBy = "";
                foreach (Sort item in sortExpression)
                {
                    item.Field = GetSortType(item.Field, args.LeadStage);
                    if (string.IsNullOrEmpty(args.OrderBy))
                        args.OrderBy = item.ToExpression();
                    else
                        args.OrderBy = ", " + args.OrderBy + item.ToExpression();
                }
                Session[ORDER_BY] = args.OrderBy;
            }
            else
            {
                args.OrderBy = "Name ASC";
                Session[ORDER_BY] = "Name ASC";
            }

            if (ColumnSettingDTO != null)
            {
                if (ColumnSettingDTO.sortType == null)
                    ColumnSettingDTO.sortType = "A";
                if (ColumnSettingDTO.sortName == null)
                    ColumnSettingDTO.sortName = "Name";
            }
            SetSortType(ColumnSettingDTO.sortType, ColumnSettingDTO.sortName);
        }
        else
        {
            args = new ArgsDTO();
        }


        if (args.LeadStage.ToLower() == "pendingcustomer")
            data = cookerService.GetCombinedCollection(args);
        else if (args.LeadStage.ToLower() != "enduser")
            data = cookerService.GetOutletsForLookupInScheduleEntry(args);
        //data = cookerService.GetCombinedCollection(args);
        else
            endUserList = endUserService.GetAllEndUsers(args);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (args.LeadStage.ToLower() != "enduser" && data != null && data.Count > 0)
        {
            CallCycleContactDTO contact = new CallCycleContactDTO();

            List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
            listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
            if (listCallCycleContact == null)
                listCallCycleContact = new List<CallCycleContactDTO>();

            foreach (LeadCustomerDTO item in data)
            {
                if (item.LeadStage == "Customer")
                {
                    contact = listCallCycleContact.Find(c => c.Contact.CustomerCode == item.CustomerCode);
                    if (contact != null)
                    {
                        item.SelectedToCall = "checked";
                        contact.rowCount = data.Count(c => c.CustomerCode == item.CustomerCode);
                    }
                    if (item.Address.Length > 38)
                        item.Address = item.Address.Substring(0, 35) + "...";
                }
                else
                {
                    contact = listCallCycleContact.Find(c => c.Contact.SourceId == item.SourceId);
                    if (contact != null)
                    {
                        item.SelectedToCall = "checked";
                        contact.rowCount = data.Count(c => c.SourceId == item.SourceId);
                    }

                }
            }

            dataSourceResult.Data = data;
            dataSourceResult.Total = data[0].rowCount;
        }
        else if (endUserList != null && endUserList.Count > 0)
        {
            CallCycleContactDTO contact = new CallCycleContactDTO();

            List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
            listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
            if (listCallCycleContact == null)
                listCallCycleContact = new List<CallCycleContactDTO>();

            foreach (EndUserDTO item in endUserList)
            {
                contact = listCallCycleContact.Find(c => c.Contact.CustomerCode == item.CustomerCode && c.Contact.EndUserCode == item.EndUserCode);
                if (contact != null)
                {
                    item.SelectedToCall = "checked";
                }

                if (item.Address1.Length > 18)
                    item.Address1 = item.Address1.Substring(0, 15) + "...";
                if (item.CustomerName.Length > 23)
                    item.CustomerName = item.CustomerName.Substring(0, 20) + "...";


            }

            dataSourceResult.Data = endUserList;
            dataSourceResult.Total = endUserList[0].rowCount;
        }

        return dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string DeleteSelectedCustomerListformGrid(List<int> DeleteCustomerListDTO)
    {
        DataSourceResult dataSourceResult = new DataSourceResult();
        bool status = false;
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        string jsonString = string.Empty;
        CustomerClient custClient = new CustomerClient();
        CustomerDTO delCustomer = new CustomerDTO();

        foreach (int item in DeleteCustomerListDTO)
        {

            delCustomer.CustomerCode = item.ToString();
            delCustomer.CreatedBy = UserSession.Instance.UserName;
            status = custClient.DeleteCustomerByCustomerCode(delCustomer);
        }
        string returnvalue = string.Empty;
        if (status)
            returnvalue = "Deleted";

        return returnvalue;
    }
    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string DeleteEMEIFromOriginator(string originator, string emei_no)
    {
        DataSourceResult dataSourceResult = new DataSourceResult();
        bool status = false;
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        string jsonString = string.Empty;
        OriginatorClient originatorClient = new OriginatorClient();
        OriginatorDTO delOriginator = new OriginatorDTO();

        status = originatorClient.DeleteEMEIFromOriginator(originator, emei_no);
        
        string returnvalue = string.Empty;
        if (status)
            returnvalue = "Deleted";

        return returnvalue;
    }
    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string ClearDeviceEMEIFromOriginator(string originator, string emei_no)
    {
        DataSourceResult dataSourceResult = new DataSourceResult();
        bool status = false;
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        string jsonString = string.Empty;
        OriginatorClient originatorClient = new OriginatorClient();
        OriginatorDTO delOriginator = new OriginatorDTO();

        status = originatorClient.ClearDeviceEMEIFromOriginator(originator, emei_no);
        
        string returnvalue = string.Empty;
        if (status)
            returnvalue = "Deleted";

        return returnvalue;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public DataSourceResult GetIdleCustomers(DateTime date, string selectType, string gridName, int take, int skip, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<LeadCustomerDTO> data = null;
        List<EndUserDTO> endUserList = null;
        int rowCount = 0;
        ArgsDTO args = new ArgsDTO();
        ColumnSettingDTO ColumnSettingDTO = new ColumnSettingDTO();
        CommonClient serviceCommon = new CommonClient();

        const string CLIENT_SORT_ORDER = "CLIENT_SORT_ORDER";
        const string CLIENT_SORT_NAME = "CLIENT_SORT_NAME";
        const string CLIENT_SORT_FLAG = "CLIENT_SORT_FLAG";
        const string CLEAR_FLAG = "CLEAR_FLAG";
        const string ORDER_BY = "ORDER_BY";
        List<Sort> sortExpression = (List<Sort>)sort;

        if (Session != null && Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            if (Session[CLIENT_SORT_ORDER] != null && Session[CLIENT_SORT_NAME] != null && Session[CLIENT_SORT_ORDER] != "" && Session[CLIENT_SORT_NAME] != "" && Convert.ToBoolean(Session[CLIENT_SORT_FLAG]) == true)
            {
                ColumnSettingDTO.sortName = Convert.ToString(Session[CLIENT_SORT_NAME]);
                ColumnSettingDTO.sortType = Convert.ToString(Session[CLIENT_SORT_ORDER]);
                Session[CLIENT_SORT_FLAG] = false;
            }
            else
                ColumnSettingDTO = serviceCommon.GetSortItems(UserSession.Instance.UserName, gridName);

            args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
            args.ActiveInactiveChecked = true;
            args.ReqSentIsChecked = true;
            args.RepType = "B";
            args.RowCount = take;
            args.AdditionalParams = "";

            //args.AddressType = "A";
            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "leadcontact", args.LeadStage);
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }
            args.Originator = UserSession.Instance.UserName;

            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.ChildReps = UserSession.Instance.ChildReps;
            args.CRMAuthLevel = (!string.IsNullOrWhiteSpace(UserSession.Instance.FilterByUserName)) ? 0 : UserSession.Instance.CRMAuthLevel;

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<string> back = new List<string>();
            back.Add(args.StartIndex.ToString());
            BackButtonForAll = back;


            if (Convert.ToBoolean(Session[CLEAR_FLAG]) == true)
            {

                args.OrderBy = Convert.ToString(Session[ORDER_BY]);
            }
            else if (ColumnSettingDTO.sortType != null && ColumnSettingDTO.sortName != null)
            {


                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = "";
                    foreach (Sort item in sortExpression)
                    {
                        item.Field = GetSortType(item.Field, args.LeadStage);
                        if (string.IsNullOrEmpty(args.OrderBy))
                            args.OrderBy = item.ToExpression();
                        else
                            args.OrderBy = ", " + args.OrderBy + item.ToExpression();
                    }
                    Session[ORDER_BY] = args.OrderBy;
                }
                else
                {
                    args.OrderBy = GetSortType(ColumnSettingDTO.sortName, args.LeadStage) + (ColumnSettingDTO.sortType == "A" ? " ASC" : "DESC");
                    Session[ORDER_BY] = args.OrderBy;
                }
            }
            else if (sortExpression != null && sortExpression.Count > 0)
            {
                args.OrderBy = "";
                foreach (Sort item in sortExpression)
                {
                    item.Field = GetSortType(item.Field, args.LeadStage);
                    if (string.IsNullOrEmpty(args.OrderBy))
                        args.OrderBy = item.ToExpression();
                    else
                        args.OrderBy = ", " + args.OrderBy + item.ToExpression();
                }
                Session[ORDER_BY] = args.OrderBy;
            }
            else
            {
                args.OrderBy = "Name ASC";
                Session[ORDER_BY] = "Name ASC";
            }

            if (ColumnSettingDTO != null)
            {
                if (ColumnSettingDTO.sortType == null)
                    ColumnSettingDTO.sortType = "A";
                if (ColumnSettingDTO.sortName == null)
                    ColumnSettingDTO.sortName = "Name";
            }
            SetSortType(ColumnSettingDTO.sortType, ColumnSettingDTO.sortName);
        }
        else
        {
            args = new ArgsDTO();
        }


        //if (args.LeadStage.ToLower() == "pendingcustomer")
        //    // data = cookerService.GetPendingCustomers(args);
        //data = cookerService.GetCombinedCollection(args);
        //else if (args.LeadStage.ToLower() != "enduser")
        //    data = cookerService.GetCombinedCollection(args);
        //else

        if (selectType == "notInvoiced")
        {
            args.AdditionalParams = "notInvoiced";
            args.DtStartDate = date;
            data = cookerService.GetAllNotInvoicedUsers(args);
        }
        else if (selectType == "idle")
        {
            args.AdditionalParams = "idle";
            args.DtStartDate = date;
            data = cookerService.GetAllNotInvoicedUsers(args);
        }

        //if (selectType == "notInvoiced")
        //{
        //    args.AdditionalParams = "notInvoiced";
        //    data = cookerService.GetAllNotInvoicedUsers(args);
        //}
        //else if (selectType == "idle")
        //{
        //    args.AdditionalParams = "idle";
        //    data = cookerService.GetAllNotInvoicedUsers(args);
        //}

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (args.LeadStage.ToLower() != "enduser" && data != null && data.Count > 0)
        {
            CallCycleContactDTO contact = new CallCycleContactDTO();

            List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
            listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
            if (listCallCycleContact == null)
                listCallCycleContact = new List<CallCycleContactDTO>();

            foreach (LeadCustomerDTO item in data)
            {
                if (item.LeadStage == "Customer")
                {
                    contact = listCallCycleContact.Find(c => c.Contact.CustomerCode == item.CustomerCode);
                    if (contact != null)
                    {
                        item.SelectedToCall = "checked";
                        contact.rowCount = data.Count(c => c.CustomerCode == item.CustomerCode);
                    }
                    if (item.Address.Length > 38)
                        item.Address = item.Address.Substring(0, 35) + "...";
                }
                else
                {
                    contact = listCallCycleContact.Find(c => c.Contact.SourceId == item.SourceId);
                    if (contact != null)
                    {
                        item.SelectedToCall = "checked";
                        contact.rowCount = data.Count(c => c.SourceId == item.SourceId);
                    }

                }
            }

            dataSourceResult.Data = data;
            dataSourceResult.Total = data[0].rowCount;
        }
        else if (endUserList != null && endUserList.Count > 0)
        {
            CallCycleContactDTO contact = new CallCycleContactDTO();

            List<CallCycleContactDTO> listCallCycleContact = new List<CallCycleContactDTO>();
            listCallCycleContact = (List<CallCycleContactDTO>)Session[CommonUtility.CALL_CYCLE_CONTACTS];
            if (listCallCycleContact == null)
                listCallCycleContact = new List<CallCycleContactDTO>();

            foreach (EndUserDTO item in endUserList)
            {
                contact = listCallCycleContact.Find(c => c.Contact.CustomerCode == item.CustomerCode && c.Contact.EndUserCode == item.EndUserCode);

                if (contact != null)
                {
                    item.SelectedToCall = "checked";
                }

                if (item.Address1.Length > 18)
                    item.Address1 = item.Address1.Substring(0, 15) + "...";
                if (item.CustomerName.Length > 23)
                    item.CustomerName = item.CustomerName.Substring(0, 20) + "...";
            }

            dataSourceResult.Data = endUserList;
            dataSourceResult.Total = endUserList[0].rowCount;
        }

        return dataSourceResult;
    }
}


