﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.ServiceModel.Web;
using CRMServiceReference;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using System.Web.Script.Serialization;

/// <summary>
/// Summary description for distributor_Service
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class distributor_Service : System.Web.Services.WebService {

    public distributor_Service () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    #region Properties

    private KeyValuePair<string, string> DistributorEntry
    {
        get
        {
            if (Session[CommonUtility.DISTRIBUTOR_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.DISTRIBUTOR_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.DISTRIBUTOR_DATA] = value;
        }
    }

    #endregion Properties

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetDistributorBankDataAndCount(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        CustomerClient customerClient = new CustomerClient();
        DistributorClient distributorClient = new DistributorClient();
        List<LeadAddressDTO> leadAddressList = null;
        List<BankAccountDTO> bankAccountList = null;

        if (DistributorEntry.Key != null)
        {
            string sortby = string.Empty;

            List<Sort> sortExpression = (List<Sort>)sort;

            #region Args Setting
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            #endregion

            KeyValuePair<string, string> lead_value = DistributorEntry;

            //if (UserSession.Instance != null)
            //{
            //    if (sortExpression != null && sortExpression.Count > 0)
            //    {
            //       // args.OrderBy = GetCustomerAddressSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            //    }
            //    else
            //    {
            //     //   args.OrderBy = "assignee_no asc";
            //    }

            //    if (filter != null)
            //    {
            //        new CommonUtility().SetFilterField(ref filter, "customeraddress", "");
            //        args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            //    }
            //}
            //else
            //{
            //    args = new ArgsDTO();
            //}

            bankAccountList = distributorClient.GetBankAccountsForUser(Server.UrlDecode(DistributorEntry.Value), Server.UrlDecode(DistributorEntry.Key));
        }
        else
        {
            bankAccountList = new List<BankAccountDTO>();
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (bankAccountList != null)
        {
            if (bankAccountList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = bankAccountList;
                dataSourceResult.Total = bankAccountList.Count;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = bankAccountList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = bankAccountList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GetDistributorDetails(string originator)
    {
        Session[CommonUtility.DISTRIBUTOR_DATA] = "";
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        DistributorClient distributorClient = new DistributorClient();
        DistributerModel distributorDTO = new DistributerModel();

        string returnvalue = string.Empty;

        try
        {
            distributorDTO = distributorClient.GetDistributorDetailsByOriginator(originator);
            Session[CommonUtility.DISTRIBUTOR_DATA] = new KeyValuePair<string, string>(distributorDTO.Originator, distributorDTO.DeptString);
        }
        catch (Exception)
        {
            returnvalue = "error";
        }

        return serializer.Serialize(distributorDTO);
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string SaveDistributorTerritory(string territory_id, string distributor_id, string created_by)
    {
        string returnvalue = "false";
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        DistributorClient distributorClient = new DistributorClient();

        try
        {
            bool status = false;
            status = distributorClient.SaveDistributorTerritory(territory_id, distributor_id, created_by);

            if (status)
            {
                returnvalue = "true";
            }
            else
            {
                returnvalue = "false";
            }
        }
        catch (Exception)
        {
            returnvalue = "error";
        }
        return returnvalue;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllTradeDiscountByDistributor(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string dbCode)
    {
        DistributorClient dbClient = new DistributorClient();

        string parentOriginator = "";
        parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                //args.OrderBy = GetAllAssetSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " date asc";
            }
        }
        else
        {
            args.OrderBy = " date asc";
        }

        if (filter != null)
        {
            if (filter.Filters != null)
            {
                foreach (Filter item in filter.Filters)
                {
                    item.Field = GetFilterTradeDiscountsField(item.Field);
                }
            }

            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        List<TradeDiscountUploadModel> data = new List<TradeDiscountUploadModel>();
        DataSourceResult obj_dataSourceResult = null;

        try
        {
            data = dbClient.GetAllTradeDiscounts(args, dbCode);
            obj_dataSourceResult = new DataSourceResult();

            if (data != null && data.Count > 0)
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = data[0].TotalCount;
            }
            else
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = 0;
            }
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
        }

        return obj_dataSourceResult;
    }

    private string GetFilterTradeDiscountsField(string fieldName)
    {
        string realfield = "";

        switch (fieldName)
        {
            case "date":
                realfield = "date";
                break;
            case "vouNo":
                realfield = "vou_no";
                break;
        }

        return realfield;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public Peercore.CRM.Model.ResponseModel UpdateTradeDiscount(string id,
        string date, 
        string dbCode, 
        string vouType, 
        string vouNo,
        string saleVal,
        string discountVal,
        string tradeDiscountVal)
    {
        ArgsModel args = new ArgsModel();
        DistributorClient dbClient = new DistributorClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        
        TradeDiscountUploadModel model = new TradeDiscountUploadModel();
        model.id = decimal.Parse(id);
        model.saleVal = double.Parse(saleVal);
        model.discountVal = double.Parse(discountVal);
        model.tradeDiscountVal = double.Parse(tradeDiscountVal);

        bool delstatus = false;

        if (!String.IsNullOrEmpty(id))
        {
            delstatus = dbClient.UpdateTradeDiscount(model);

            if (delstatus)
            {
                response.Status = true;
                response.StatusCode = "Success";
            }
            else
            {
                response.Status = false;
                response.StatusCode = "Error";
            }
        }
        
        return response;
    }
    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public Peercore.CRM.Model.ResponseModel UpdateTradeDiscountApprovedProcess(string dtpMonth)
    {
        string dateVal = DateTime.Now.ToString("yyyy/MM/dd");

        if (!string.IsNullOrEmpty(dtpMonth))
        {
            dateVal = DateTime.Parse("20/" + dtpMonth.Trim().Replace(" ", "/")).ToString("yyyy/MM/dd");
        }
        
        ArgsModel args = new ArgsModel();
        DistributorClient dbClient = new DistributorClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        bool delstatus = false;

        if (!String.IsNullOrEmpty(dateVal))
        {
            delstatus = dbClient.UpdateTradeDiscountApprovedProcess(dateVal);

            if (delstatus)
            {
                response.Status = true;
                response.StatusCode = "Success";
            }
            else
            {
                response.Status = false;
                response.StatusCode = "Error";
            }
        }
        
        return response;
    }
}
