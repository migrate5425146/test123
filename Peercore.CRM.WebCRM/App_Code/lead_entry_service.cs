﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using Peercore.CRM.Entities;
using Telerik.Web.UI;
using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;

/// <summary>
/// Summary description for lead_entry_service
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
 [System.Web.Script.Services.ScriptService]
public class lead_entry_service : System.Web.Services.WebService {

    public lead_entry_service () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    #region Properties
    private KeyValuePair<string, string> LeadEntry
    {
        get
        {
            if (Session[CommonUtility.LEAD_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.LEAD_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.LEAD_DATA] = value;
        }
    }

    private KeyValuePair<string, string> CustomerEntry
    {
        get
        {
            if (Session[CommonUtility.CUSTOMER_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.CUSTOMER_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.CUSTOMER_DATA] = value;
        }
    }

    private KeyValuePair<string, string> EndUserEntry
    {
        get
        {
            if (Session[CommonUtility.ENDUSER_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.ENDUSER_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.ENDUSER_DATA] = value;
        }
    }
    #endregion

    #region Customer Address

    //[WebMethod(EnableSession = true)]
    //public List<LeadAddressDTO> GetContactAddressWithCount(int startIndex, int maximumRows, List<GridSortExpression> sortExpression, List<GridFilterExpression> filterExpression)
    //{

    [WebMethod(EnableSession=true)]
    public DataSourceResult GetContactAddressDataAndCount(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        LeadClient leadAddress = new LeadClient();
        List<LeadAddressDTO> leadAddressList = null;

        if (LeadEntry.Key != null)
        {
            string sortby = string.Empty;
            ArgsDTO args;
            KeyValuePair<string, string> lead_value = LeadEntry;

            if (UserSession.Instance != null)
            {
                args = new ArgsDTO();
                args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
                args.ChildOriginators = UserSession.Instance.ChildOriginators;

                //if (sortExpression.Count != 0)
                //{
                //    args.OrderBy = GetContactAddressSortString(sortExpression[(sortExpression.Count - 1)].FieldName, sortExpression[(sortExpression.Count - 1)].SortOrder);
                //}
            }
            else
            {
                args = new ArgsDTO();
            }

            leadAddressList = leadAddress.GetLeadAddreses(int.Parse(lead_value.Key));

            leadAddress.Close();
        }
        else
        {
            leadAddressList = new List<LeadAddressDTO>();
        }

        DataSourceResult dataSourceResult = new DataSourceResult();


        dataSourceResult.Data = leadAddressList;
        dataSourceResult.Total = leadAddressList.Count;
        

        return dataSourceResult;
    }

    
    #endregion
    
    #region Customer Contact
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetContactPersonDataAndCount(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        List<Sort> sortExpression = (List<Sort>)sort;
        LeadClient contactPersonClient = new LeadClient();
        List<ContactPersonDTO> contactPersList = null;
        if (LeadEntry.Key != null)
        {
            string sortby = string.Empty;
            ArgsDTO args;
            KeyValuePair<string, string> lead_value = LeadEntry;

            if (Session[CommonUtility.GLOBAL_SETTING] != null)
            {
                args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
                args.StartIndex = (skip / take) + 1;
                args.RowCount = take;
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = "";
                    foreach (Sort item in sortExpression)
                    {
                        item.Field = GetLeadContactPersonSortString(item.Field);
                        if (string.IsNullOrEmpty(args.OrderBy))
                            args.OrderBy = item.ToExpression();
                        else
                            args.OrderBy = args.OrderBy + ", " + item.ToExpression();
                    }
                }
                else
                {
                    args.OrderBy = "first_name ASC";
                }
            }
            else
            {
                args = new ArgsDTO();
            }

            args.LeadId = int.Parse(lead_value.Key.Trim());
            args.Originator = UserSession.Instance.UserName;
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.ClientType = UserSession.Instance.ClientType;
            //args.RepType = "";
            contactPersList = contactPersonClient.GetContactsByOrigin( args,true);
            contactPersonClient.Close();
        }

        

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (contactPersList != null && contactPersList.Count > 0)
        {
            dataSourceResult.Data = contactPersList;
            dataSourceResult.Total = contactPersList[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = contactPersList;
            dataSourceResult.Total = 0;

        }

        return dataSourceResult;
    }

    private string GetLeadContactPersonSortString(string sortby)
    {
        string sortStr = "";


        switch (sortby)
        {
            case "Title":
                sortStr = "title";
                break;
            case "FirstName":
                sortStr = "first_name";
                break;
            case "LastName":
                sortStr = "last_name";
                break;
            case "Position":
                sortStr = "position";
                break;
            case "Telephone":
                sortStr = "telephone" ;
                break;
            case "Mobile":
                sortStr = "mobile";
                break;
            case "EmailAddress":
                sortStr = "email_address";
                break;
            default:
                sortStr = "";
                break;
        }

        return sortStr;
    }

    #endregion

    #region Customer Activity
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetActivityDataAndCount(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        List<Sort> sortExpression = (List<Sort>)sort;
        ActivityClient activity = new ActivityClient();
        List<ActivityDTO> activityList = null;

        if (LeadEntry.Key != null)
        {
            string sortby = string.Empty;
            ArgsDTO args;
            KeyValuePair<string, string> lead_value = LeadEntry;

            if (UserSession.Instance != null)
            {
                args = new ArgsDTO();
                args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
                args.ChildOriginators = UserSession.Instance.ChildOriginators;

                args.StartIndex = (skip / take) + 1;
                args.RowCount = take;
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = "";
                    foreach (Sort item in sortExpression)
                    {
                        item.Field = GetActivitySortString(item.Field);
                        if (string.IsNullOrEmpty(args.OrderBy))
                            args.OrderBy = item.ToExpression();
                        else
                            args.OrderBy = args.OrderBy + ", " + item.ToExpression();
                    }
                }
                else
                {
                    args.OrderBy = " start_date desc";
                }
                

            }
            else
            {
                args = new ArgsDTO();
            }
            args.LeadId = int.Parse(lead_value.Key);
            activityList = activity.GetRelatedActivity(args);
            activity.Close();
        }


        DataSourceResult dataSourceResult = new DataSourceResult();

        if (activityList != null && activityList.Count > 0)
        {
            dataSourceResult.Data = activityList;
            dataSourceResult.Total = activityList[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = activityList;
            dataSourceResult.Total = 0;

        }
        return dataSourceResult;
    }

    private static string GetActivitySortString(string sortby)
    {
        string sortStr = "";


        switch (sortby)
        {
            case "Subject":
                sortStr = "subject";
                break;
            case "StartDate":
                sortStr = "start_date";
                break;
            case "EndDate":
                sortStr = "end_date";
                break;
            case "Status":
                sortStr = "subject";
                break;
            case "AssignedTo":
                sortStr = "assigned_to";
                break;
            case "SentMail":
                sortStr = "sent_mail";
                break;
            case "Priority":
                sortStr = "priority";
                break;
            case "ActivityType":
                sortStr = "activity_type";
                break;
            default:
                sortStr = "start_date";
                break;
        }

        return sortStr;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetActivityHistoryDataAndCount(string leadId,string custCode,string enduserCode, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        List<Sort> sortExpression = (List<Sort>)sort;
        ActivityClient activity = new ActivityClient();
        List<ActivityDTO> activityList = null;

        //if (LeadEntry.Key != null)
        //{
            string sortby = string.Empty;
            ArgsDTO args;
            //KeyValuePair<string, string> lead_value = LeadEntry;

            if (UserSession.Instance != null)
            {
                args = new ArgsDTO();
                args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
                args.ChildOriginators = UserSession.Instance.ChildOriginators;

                args.StartIndex = (skip / take) + 1;
                args.RowCount = take;
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = "";
                    foreach (Sort item in sortExpression)
                    {
                        item.Field = GetActivitySortString(item.Field);
                        if (string.IsNullOrEmpty(args.OrderBy))
                            args.OrderBy = item.ToExpression();
                        else
                            args.OrderBy = args.OrderBy + ", " + item.ToExpression();
                    }
                }
                else
                {
                    args.OrderBy = " start_date desc";
                }


            }
            else
            {
                args = new ArgsDTO();
            }
            args.LeadId = string.IsNullOrEmpty(leadId) ? 0 : int.Parse(leadId);
            args.CustomerCode = custCode;
            args.EnduserCode = enduserCode;

            if (!string.IsNullOrEmpty(args.EnduserCode))
            {
                activityList = activity.GetActivitiesForEndUser(args);
            }
            else if (!string.IsNullOrEmpty(args.CustomerCode))
            {
                activityList = activity.GetActivitiesForCustomer(args);
            }
            else if (args.LeadId > 0)
            {
                activityList = activity.GetRelatedActivity(args);
            }
            activity.Close();
        //}


        DataSourceResult dataSourceResult = new DataSourceResult();

        if (activityList != null && activityList.Count > 0)
        {
            dataSourceResult.Data = activityList;
            dataSourceResult.Total = activityList[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = activityList;
            dataSourceResult.Total = 0;

        }
        return dataSourceResult;
    }

    #endregion

    #region Customer Opportunities
   
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetLeadOpportunitiesWithCount(int take, int skip, string enduserid, string cust_id, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        List<Sort> sortExpression = (List<Sort>)sort;
        OpportunityClient opportunity = new OpportunityClient();
        List<OpportunityDTO> opportunityList = null;
        List<CustomerOpportunitiesDTO> customeropportunityList = null;

        if (LeadEntry.Key != null)
        {
            string sortby = string.Empty;
            ArgsDTO args = new ArgsDTO(); ;
            KeyValuePair<string, string> lead_value = LeadEntry;

            args.AdditionalParams = "";
            args.ChildOriginators = UserSession.Instance.ChildOriginators;

            args.StartIndex = (skip / take) + 1;
            args.RowCount = take;
            if (sortExpression != null && sortExpression.Count > 0)
            {
                args.OrderBy = GetOpportunitySortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = "opportunity_id asc";
            }

            opportunityList = opportunity.GetOpportunities(args, int.Parse(lead_value.Key));
        }
        else if (CustomerEntry.Key != null)
        {
            string sortby = string.Empty;
            ArgsDTO args = new ArgsDTO(); ;
            KeyValuePair<string, string> lead_value = CustomerEntry;

            args.AdditionalParams = "";
            args.ChildOriginators = UserSession.Instance.ChildOriginators;

            args.StartIndex = (skip / take) + 1;
            args.RowCount = take;
            if (sortExpression != null && sortExpression.Count > 0)
            {
                args.OrderBy = GetOpportunitySortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = "opportunity_id asc";
            }

            opportunityList = opportunity.GetOpportunitiesForCustomer(HttpContext.Current.Server.UrlDecode(CustomerEntry.Key),args);
        }
        else if (EndUserEntry.Key != null)
        {
            string sortby = string.Empty;
            ArgsDTO args = new ArgsDTO();
            KeyValuePair<string, string> lead_value = EndUserEntry;

            args.AdditionalParams = "";
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.CustomerCode = HttpContext.Current.Server.UrlDecode(cust_id);
            args.EnduserCode = lead_value.Key;
            args.StartIndex = (skip / take) + 1;
            args.RowCount = take;
            if (sortExpression != null && sortExpression.Count > 0)
            {
                args.OrderBy = GetOpportunitySortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = "opportunity_id asc";
            }

            customeropportunityList = opportunity.GetOpportunitiesForEndUser(args);
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (opportunityList != null && opportunityList.Count > 0)
        {
            dataSourceResult.Data = opportunityList;
            dataSourceResult.Total = opportunityList[0].rowCount;
        }
        else if (customeropportunityList != null && customeropportunityList.Count > 0)
        {
            dataSourceResult.Data = customeropportunityList;
            dataSourceResult.Total = customeropportunityList[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = opportunityList;
            dataSourceResult.Total = 0;

        }
        return dataSourceResult;
    }

    private string GetOpportunitySortString(string sortby, string sorttype)
    {
        string sortStr = "name";
        string sortoption = "asc";

        if (!string.IsNullOrWhiteSpace(sorttype))
            sortoption = sorttype;

        switch (sortby)
        {
            case "Name":
                sortStr = "name" + sortoption;
                break;
            case "CloseDate":
                sortStr = "close_date" + sortoption;
                break;
            case "Stage":
                sortStr = "pipeline_stage_id" + sortoption;
                break;
            case "Probability":
                sortStr = "probability" + sortoption;
                break;
            case "Amount":
                sortStr = "amount" + sortoption;
                break;
            case "Description":
                sortStr = "description" + sortoption;
                break;
            default:
                sortStr = "name" + sortoption;
                break;
        }

        return sortStr;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetCustomerCatalogDataAndCount(string repType, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        ActivityClient activityClient = new ActivityClient();
        CommonClient commonClient = new CommonClient(); 
        List<CatalogDTO> catalogList = null;

        if (CustomerEntry.Key != null)
        {
            string sortby = string.Empty;

            List<Sort> sortExpression = (List<Sort>)sort;

            #region Args Setting
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            args.ManagerMode = true;
            args.StartIndex = (skip / take) + 1;
            args.RowCount = take;
            args.AdditionalParams = "(product_type like lower('%" + repType + "%') )";
            #endregion

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "catalog", "");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            if (UserSession.Instance != null)
            {
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = GetCatalogSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " description asc";
                }
            }
            else
            {
                args = new ArgsDTO();
            }

            catalogList = commonClient.GetAllCatalog(args);
        }

        else if (EndUserEntry.Key != null)
        {
            string sortby = string.Empty;

            List<Sort> sortExpression = (List<Sort>)sort;

            #region Args Setting
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            args.ManagerMode = true;
            args.StartIndex = (skip / take) + 1;
            args.RowCount = take;
            args.AdditionalParams = "(product_type like lower('%" + repType + "%') )";
            #endregion

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "catalog", "");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            if (UserSession.Instance != null)
            {
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = GetCatalogSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " description asc";
                }
            }
            else
            {
                args = new ArgsDTO();
            }

            catalogList = commonClient.GetAllCatalog(args);
        }
        else
        {
            catalogList = new List<CatalogDTO>();
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (catalogList != null)
        {
            if (catalogList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = catalogList;
                dataSourceResult.Total = catalogList[0].rowCount;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = catalogList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = catalogList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    private string GetCatalogSortString(string sortby, string sorttype)
    {
        string sortStr = "description";
        string sortoption = " asc";

        if (!string.IsNullOrWhiteSpace(sorttype))
            sortoption = " " + sorttype;

        switch (sortby)
        {
            case "Description":
                sortStr = "description" + sortoption;
                break;
            case "CatlogCode":
                sortStr = "catlog_code" + sortoption;
                break;
            case "DeliveryFrequency":
                sortStr = "product_type" + sortoption;
                break;
            
        }

        return sortStr;
    }
    #endregion

    #region Customer Email
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetEmailDataAndCount(int take, int skip,string cust_id, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        List<EmailDTO> emailList = null;
        List<Sort> sortExpression = (List<Sort>)sort;
        if (LeadEntry.Key != null)
        {
            LeadClient emailClient = new LeadClient();

            string sortby = string.Empty;
            ArgsDTO args;
            KeyValuePair<string, string> lead_value = LeadEntry;

            if (Session[CommonUtility.GLOBAL_SETTING] != null)
            {
                args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
                args.StartIndex = (skip / take) + 1;
                args.RowCount = take;
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = GetEmailSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = "mail_id asc";
                }
            }
            else
            {
                args = new ArgsDTO();
            }
            args.LeadId = int.Parse(lead_value.Key);
            string path = HttpContext.Current.Request.PhysicalApplicationPath + "\\docs\\";
            emailList = emailClient.GetLeadEmails(args, path, Session.SessionID.ToString());
        }
        else if (CustomerEntry.Key != null)
        {
            CustomerClient customerClient = new CustomerClient();

            string sortby = string.Empty;

            #region Args Setting
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            args.CustomerCode = HttpContext.Current.Server.UrlDecode(CustomerEntry.Key);
            args.ManagerMode = true;
            args.StartIndex = (skip / take) + 1;
            args.RowCount = take;
            #endregion

            KeyValuePair<string, string> customer_value = CustomerEntry;

            if (UserSession.Instance != null)
            {
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = GetEmailSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " mail_id asc";
                }
            }
            else
            {
                args = new ArgsDTO();
            }
            string path = HttpContext.Current.Request.PhysicalApplicationPath + "\\docs\\";
            emailList = customerClient.GetCustomerEmails(HttpContext.Current.Server.UrlDecode(customer_value.Key), args, path, Session.SessionID.ToString());
        }
        //else if (EndUserEntry.Key != null)
        //{
        //    CustomerClient customerClient = new CustomerClient();

        //    string sortby = string.Empty;

        //    #region Args Setting
        //    ArgsDTO args = new ArgsDTO();
        //    args.ChildOriginators = UserSession.Instance.ChildOriginators;
        //    args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        //    args.Originator = UserSession.Instance.UserName;
        //    args.CustomerCode = HttpContext.Current.Server.UrlDecode(cust_id);
        //    args.EnduserCode = EndUserEntry.Key;
        //    args.ManagerMode = true;
        //    args.StartIndex = (skip / take) + 1;
        //    args.RowCount = take;
        //    #endregion

        //    if (UserSession.Instance != null)
        //    {
        //        if (sortExpression != null && sortExpression.Count > 0)
        //        {
        //            args.OrderBy = GetEmailSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
        //        }
        //        else
        //        {
        //            args.OrderBy = " mail_id asc";
        //        }
        //    }
        //    else
        //    {
        //        args = new ArgsDTO();
        //    }
        //    string path = HttpContext.Current.Request.PhysicalApplicationPath + "\\docs\\";
        //    emailList = customerClient.GetCustomerEmails(args.CustomerCode, args, path, Session.SessionID.ToString());
        //}
        DataSourceResult dataSourceResult = new DataSourceResult();

        if (emailList != null && emailList.Count > 0)
        {
            dataSourceResult.Data = emailList;
            dataSourceResult.Total = emailList[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = emailList;
            dataSourceResult.Total = 0;

        }
        return dataSourceResult;

    }

    private string GetEmailSortString(string sortby, string sorttype)
    {
        string sortStr = "name";
        string sortoption = "asc";

        if (!string.IsNullOrWhiteSpace(sorttype))
            sortoption = sorttype;

        switch (sortby)
        {
            case "Name":
                sortStr = "name" + sortoption;
                break;
            case "CloseDate":
                sortStr = "close_date" + sortoption;
                break;
            case "Stage":
                sortStr = "pipeline_stage_id" + sortoption;
                break;
            case "Probability":
                sortStr = "probability" + sortoption;
                break;
            case "Amount":
                sortStr = "amount" + sortoption;
                break;
            case "Description":
                sortStr = "description" + sortoption;
                break;
            default:
                sortStr = "name" + sortoption;
                break;
        }

        return sortStr;
    }
    #endregion

    #region Customer Documents
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetDocumenDataAndCount(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        List<Sort> sortExpression = (List<Sort>)sort;
        CustomerClient documentClient = new CustomerClient();
        List<DocumentDTO> documentList = null;

        if (LeadEntry.Key != null)
        {
            string sortby = string.Empty;
            ArgsDTO args;
            KeyValuePair<string, string> lead_value = LeadEntry;

            if (Session[CommonUtility.GLOBAL_SETTING] != null)
            {
                args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;

                args.StartIndex = (skip / take) + 1;
                args.RowCount = take;
                args.CustomerType = "Lead";
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = "";
                    foreach (Sort item in sortExpression)
                    {
                        item.Field = GetDocumentSortString(item.Field);
                        if (string.IsNullOrEmpty(args.OrderBy))
                            args.OrderBy = item.ToExpression();
                        else
                            args.OrderBy = args.OrderBy + ", " + item.ToExpression();
                    }
                }
                else
                {
                    args.OrderBy = " attached_date desc";
                }

            }
            else
            {
                args = new ArgsDTO();
            }

            //HttpContext.Current.Request.PhysicalApplicationPath
            string path = HttpContext.Current.Request.PhysicalApplicationPath + "\\docs\\";
            args.LeadId = int.Parse(lead_value.Key);
            documentList = documentClient.GetDocumentsForEntryPages(args, path, Session.SessionID.Trim(), true);

            documentClient.Close();
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (documentList != null && documentList.Count > 0)
        {
            dataSourceResult.Data = documentList;
            dataSourceResult.Total = documentList[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = documentList;
            dataSourceResult.Total = 0;

        }
        return dataSourceResult;
    }

    private static string GetDocumentSortString(string sortby)
    {
        string sortStr = "attached_date";

        switch (sortby)
        {
            case "DocumentName":
                sortStr = "document_name";
                break;
            case "AttachedBy":
                sortStr = "attached_by";
                break;
            case "AttachedDate":
                sortStr = "attached_date";
                break;
            default:
                sortStr = "document_name";
                break;
        }

        return sortStr;
    }
    #endregion

}
