﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for FilterUtility
/// </summary>
public class FilterUtility
{
    public FilterUtility()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public void SetFilterField(ref Filter filter, string page, string leadstage = "")
    {
        if (filter.Filters != null)
        {
            foreach (Filter item in filter.Filters)
            {
                item.Field = SplitMethodForGetFieldName(item.Field, page, leadstage);
            }
        }
    }

    private string SplitMethodForGetFieldName(string fieldName, string page, string leadstage = "")
    {
        StringBuilder sb = new StringBuilder();

        switch (page)
        {
            //case "leadcontact":
            //    sb.Append(GetLeadContactField(fieldName, leadstage));
            //    break;
            case "regionmaster":
                sb.Append(GetRegionField(fieldName));
                break;
            
            case "areamaster":
                sb.Append(GetAreaField(fieldName));
                break;
            
            case "territorymaster":
                sb.Append(GetTerritoryField(fieldName));
                break;
            
            case "rsmmaster":
                sb.Append(GetRSMField(fieldName));
                break;

            case "asmmaster":
                sb.Append(GetASMField(fieldName));
                break;
        }

        return sb.ToString();
    }

    #region "Master Filters"
    private string GetRegionField(string fieldName)
    {
        string realfield = "";

        switch (fieldName)
        {
            case "RegionCode":
                realfield = "region_code";
                break;
            case "RegionName":
                realfield = "region_name";
                break;
        }

        return realfield;
    }
    
    private string GetAreaField(string fieldName)
    {
        string realfield = "";

        switch (fieldName)
        {
            case "AreaCode":
                realfield = "crm_mast_area.area_code";
                break;
            case "AreaName":
                realfield = "crm_mast_area.area_name";
                break; 
            case "RegionName":
                realfield = "crm_mast_region.region_name";
                break;
            case "RegionId":
                realfield = "crm_mast_area.region_id";
                break;
        }

        return realfield;
    }
    
    private string GetTerritoryField(string fieldName)
    {
        string realfield = "";

        switch (fieldName)
        {
            case "TerritoryCode":
                realfield = "territory_code";
                break;
            case "TerritoryName":
                realfield = "territory_name";
                break;
            case "AreaName":
                realfield = "area_name";
                break;
            case "AreaId":
                realfield = "area_id";
                break;
        }

        return realfield;
    }   
    
    private string GetRSMField(string fieldName)
    {
        string realfield = "";

        switch (fieldName)
        {
            case "RsmCode":
                realfield = "crm_mast_rsm.rsm_code";
                break;
            case "RsmName":
                realfield = "crm_mast_rsm.rsm_name";
                break;
            case "RsmUserName":
                realfield = "crm_mast_rsm.originator";
                break;
            case "RegionName":
                realfield = "crm_mast_region.region_code";
                break;
            case "RsmTel1":
                realfield = "crm_mast_rsm.rsm_tel1";
                break;
            case "RsmEmail":
                realfield = "crm_mast_rsm.rsm_email";
                break;
        }

        return realfield;
    }
    
    private string GetASMField(string fieldName)
    {
        string realfield = "";

        switch (fieldName)
        {
            case "AsmCode":
                realfield = "crm_mast_asm.asm_code";
                break;
            case "AsmName":
                realfield = "crm_mast_asm.asm_name";
                break;
            case "AsmUserName":
                realfield = "crm_mast_asm.originator";
                break;
            case "AreaName":
                realfield = "crm_mast_area.area_name";
                break;
        }

        return realfield;
    }

    #endregion
}