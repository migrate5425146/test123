﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using System.ServiceModel.Web;
using CRMServiceReference;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using System.Web.Script.Serialization;
using System.IO;

/// <summary>
/// Summary description for common
/// </summary>

[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class common : System.Web.Services.WebService
{

    CommonClient commonClient = new CommonClient();
    CustomerClient customerClient = new CustomerClient();

    public common()
    {
        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    #region Home Page Sales Details
    [WebMethod]
    public DataSourceResult GetSalesLeaderBoard(string businesscode, string period, int skip, int take, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        SalesLeaderDAO salesDetails = null;
        List<SalesLeaderDAO> data = new List<SalesLeaderDAO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = 5;
            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetSalesLeaderBoardSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " ytdvalue desc";
                }
            }
            else
            {
                args.OrderBy = " ytdvalue desc";
            }

           // data = commonClient.GetSalesLeaderBoard(businesscode, period, args);
          //  data.ro


            salesDetails = new SalesLeaderDAO();
            salesDetails.OwnerName = "Target";
            salesDetails.Data = "Rs.200,000.00";
            data.Add(salesDetails);

            salesDetails = new SalesLeaderDAO();
            salesDetails.OwnerName = "Percentage achived ";
            salesDetails.Data = "37%";
            data.Add(salesDetails);

            salesDetails = new SalesLeaderDAO();
            salesDetails.OwnerName = "Remaining to achive";
            salesDetails.Data = "Rs.126,000.00";
            data.Add(salesDetails);

            salesDetails = new SalesLeaderDAO();
            salesDetails.OwnerName = "Current Strike Rate";
            salesDetails.Data = "33%";
            data.Add(salesDetails);

            salesDetails = new SalesLeaderDAO();
            salesDetails.OwnerName = "Required Strike Rate";
            salesDetails.Data = "47%";
            data.Add(salesDetails);


            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult GetOpportunity(string businesscode, string period, int skip, int take, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        OpportunityOwnerDTO odatadto = null;
        List<OpportunityOwnerDTO> data = new List<OpportunityOwnerDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = 5;

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetOpportunitySortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " amount desc";
                }
            }
            else
            {
                args.OrderBy = " amount desc";
            }

            string cutofdate_string = null;
            string closedate_string = null;

            DateTime cutoffDate = DateTime.Today;

            if (period == "YTD")
            {
                if (DateTime.Now.Date >= new DateTime(DateTime.Now.Date.Year, 7, 1))
                    cutoffDate = new DateTime(DateTime.Now.Date.Year, 7, 1);
                else
                    cutoffDate = new DateTime(DateTime.Now.Date.Year - 1, 7, 1);
            }
            else if (period == "MTD")
                cutoffDate = new DateTime(DateTime.Now.Date.Year, DateTime.Now.Date.Month, 1);

            //string cutofdate_string = DateTime.Parse(cutoffDate.ToString(ConfigUtil.DateTimePatternForSql + "00:00:00")).ToUniversalTime().ToString(ConfigUtil.DateTimePatternForSql + "HH:mm:ss");
            //string closedate_string = DateTime.Parse(cutoffDate.ToString(ConfigUtil.DateTimePatternForSql + "23:59:59")).ToUniversalTime().ToString(ConfigUtil.DateTimePatternForSql + "HH:mm:ss");

            cutofdate_string = DateTime.Parse(cutoffDate.ToString("dd-MMM-yyyy 00:00:00")).ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss");
            if (period == "MTD")
                closedate_string = DateTime.Parse(DateTime.Now.ToString("dd-MMM-yyyy 23:59:59")).ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss");

           //data = commonClient.GetLeaderBoard(cutofdate_string, closedate_string, businesscode, period, args);
            odatadto = new OpportunityOwnerDTO();
            odatadto.OwnerName = "Bombay Sweet";
            odatadto.Originator = "Nugegoda";
            data.Add(odatadto);

            odatadto = new OpportunityOwnerDTO();
            odatadto.OwnerName = "Sweetha Hotel";
            odatadto.Originator = "Nugegoda";
            data.Add(odatadto);

            odatadto = new OpportunityOwnerDTO();
            odatadto.OwnerName = "LANLIB";
            odatadto.Originator = "Nugegoda";
            data.Add(odatadto);

            odatadto = new OpportunityOwnerDTO();
            odatadto.OwnerName = "Imaco Grocery";
            odatadto.Originator = "Nugegoda";
            data.Add(odatadto);


            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;

        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult GetDeals(string businesscode, int skip, int take, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        List<OpenDealDTO> data = new List<OpenDealDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = 5;

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetDealsSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " amount desc";
                }
            }
            else
            {
                args.OrderBy = " amount desc";
            }

            DateTime cutoffDate = DateTime.Today;

            data = commonClient.GetTopFiveOpportunities(businesscode, args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetCustomers(string repcode, int skip, int take, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        List<KeyAccountDAO> data = new List<KeyAccountDAO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = 5;

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetCustomerSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " price DESC";
                }
            }
            else
            {
                args.OrderBy = " price DESC";
            }
            args.Originator = UserSession.Instance.UserName;
            DateTime cutoffDate = DateTime.Today;

            data = commonClient.GetSalesKeyAccounts(repcode, args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    private static string GetSalesLeaderBoardSortString(string sortby, string gridSortOrder)
    {
        string sortStr = "ytdvalue desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "OwnerName":
                sortStr = "name " + sortoption;
                break;
            case "Amount":
                sortStr = "ytdvalue " + sortoption;
                break;
            case "EndDate":
                sortStr = "end_date " + sortoption;
                break;
            case "Units":
                sortStr = "subject " + sortoption;
                break;
            default:
                sortStr = "ytdvalue desc ";
                break;
        }

        return sortStr;
    }

    private static string GetOpportunitySortString(string sortby, string gridSortOrder)
    {
        string sortStr = "amount desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "OwnerName":
                sortStr = " name " + sortoption;
                break;
            case "Amount":
                sortStr = " amount " + sortoption;
                break;
            case "Units":
                sortStr = " tonnes " + sortoption;
                break;
            case "Count":
                sortStr = " number " + sortoption;
                break;
            case "Ratio":
                sortStr = " number " + sortoption;
                break;
            default:
                sortStr = "amount desc ";
                break;
        }

        return sortStr;
    }

    private static string GetDealsSortString(string sortby, string gridSortOrder)
    {
        string sortStr = "amount desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "OpportunityName":
                sortStr = " name " + sortoption;
                break;
            case "Rep":
                sortStr = " originator " + sortoption;
                break;
            case "Customer":
                sortStr = " customer " + sortoption;
                break;
            case "Amount":
                sortStr = " amount " + sortoption;
                break;
            default:
                sortStr = "amount desc ";
                break;
        }

        return sortStr;
    }

    private static string GetCustomerSortString(string sortby, string gridSortOrder)
    {
        string sortStr = "price desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "AccountName":
                sortStr = " name " + sortoption;
                break;
            case "Amount":
                sortStr = " price " + sortoption;
                break;
            default:
                sortStr = "price desc ";
                break;
        }

        return sortStr;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetSalesDetails(string repcode, string type, int skip, int take, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        List<KeyAccountDAO> data = new List<KeyAccountDAO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = 10;

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetCustomerSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " price DESC";
                }
            }
            else
            {
                args.OrderBy = " price DESC";
            }
            args.Originator = UserSession.Instance.UserName;
            DateTime cutoffDate = DateTime.Today;

            if (type == "customer")
            {
                data = commonClient.GetSalesKeyAccounts(repcode, args);
            }
            else if (type == "rep")
            {
                data = commonClient.GetTopSalesReps(args);
            }
            else if (type == "distributor")
            {
                data = commonClient.GetTopSalesDistributors(args);
            }
            else if (type == "product")
            {
                data = commonClient.GetTopSalesProducts(args);
            }
            else if (type == "market")
            {
                data = commonClient.GetTopSalesMarkets(args);
            }
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    #endregion

    #region Home Page CRM Details
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetGetNewCustomer()
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        List<RepCustomerDTO> data = new List<RepCustomerDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            args.SMonth = DateTime.Today.Month;
            args.SYear = DateTime.Today.Year;

            data = customerClient.GetNewCustomers(args);

            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetTodayAppointmentsForHome(int take, int skip)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        AppointmentClient appointmentClient = new AppointmentClient();

        #region Args Setting
        ArgsDTO args = new ArgsDTO();
        args.ChildOriginators = UserSession.Instance.ChildOriginators;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.Originator = UserSession.Instance.UserName;
        args.SStartDate = DateTime.Today.ToString(ConfigUtil.DateTimePatternForSql);
        args.SEndDate = DateTime.Today.AddDays(1).ToString(ConfigUtil.DateTimePatternForSql);
        #endregion

        List<AppointmentViewDTO> data = new List<AppointmentViewDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {

            args.StartIndex = skip / take + 1;
            args.RowCount = take;

            args.OrderBy = " close_date desc";

            data = appointmentClient.GetTodaysAppointments(args);

            if (data != null)
            {
                if (data.Count != 0)
                {
                    obj_dataSourceResult = new DataSourceResult();
                    obj_dataSourceResult.Data = data;
                    obj_dataSourceResult.Total = data[0].rowCount;
                }
                else
                {
                    obj_dataSourceResult = new DataSourceResult();
                    obj_dataSourceResult.Data = data;
                    obj_dataSourceResult.Total = 0;
                }
            }
            else
            {
                obj_dataSourceResult = new DataSourceResult();
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = 0;
            }
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetPendingAppointmentsForHome(int take, int skip)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        AppointmentClient appointmentClient = new AppointmentClient();

        #region Args Setting
        ArgsDTO args = new ArgsDTO();
        args.ChildOriginators = UserSession.Instance.ChildOriginators;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.Originator = UserSession.Instance.UserName;
        args.SStartDate = DateTime.Today.ToString(ConfigUtil.DateTimePatternForSql);
        args.SEndDate = DateTime.Today.AddDays(1).ToString(ConfigUtil.DateTimePatternForSql);
        #endregion

        List<AppointmentViewDTO> data = new List<AppointmentViewDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {

            args.StartIndex = skip / take + 1;
            args.RowCount = take;

            args.OrderBy = " close_date desc";

            data = appointmentClient.GetPendingAppointments(args);

            if (data != null)
            {
                if (data.Count != 0)
                {
                    obj_dataSourceResult = new DataSourceResult();
                    obj_dataSourceResult.Data = data;
                    obj_dataSourceResult.Total = data[0].rowCount;
                }
                else
                {
                    obj_dataSourceResult = new DataSourceResult();
                    obj_dataSourceResult.Data = data;
                    obj_dataSourceResult.Total = 0;
                }
            }
            else
            {
                obj_dataSourceResult = new DataSourceResult();
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = 0;
            }
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetOutstandingActiesForHome(int take, int skip)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        ActivityClient activityClient = new ActivityClient();

        #region Args Setting
        ArgsDTO args = new ArgsDTO();
        args.ChildOriginators = UserSession.Instance.ChildOriginators;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.Originator = UserSession.Instance.UserName;
        args.SStartDate = DateTime.Today.ToString(ConfigUtil.DateTimePatternForSql);
        args.SEndDate = DateTime.Today.AddDays(1).ToString(ConfigUtil.DateTimePatternForSql);
        #endregion

        List<CustomerActivitiesDTO> data = new List<CustomerActivitiesDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {

            args.StartIndex = skip / take + 1;
            args.RowCount = take;

            args.OrderBy = " subject desc";

            data = activityClient.GetOutstandingActivities(args);

            if (data != null)
            {
                if (data.Count != 0)
                {
                    obj_dataSourceResult = new DataSourceResult();
                    obj_dataSourceResult.Data = data;
                    obj_dataSourceResult.Total = data[0].rowCount;
                }
                else
                {
                    obj_dataSourceResult = new DataSourceResult();
                    obj_dataSourceResult.Data = data;
                    obj_dataSourceResult.Total = 0;
                }
            }
            else
            {
                obj_dataSourceResult = new DataSourceResult();
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = 0;
            }
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetRemindersForHome(int take, int skip, string type)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        ActivityClient activityClient = new ActivityClient();

        #region Args Setting
        ArgsDTO args = new ArgsDTO();
        args.ChildOriginators = UserSession.Instance.ChildOriginators;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.Originator = UserSession.Instance.UserName;
        args.SStartDate = "";// DateTime.Today.ToString(ConfigUtil.DateTimePatternForSql);
        args.SEndDate = "";// DateTime.Today.AddDays(1).ToString(ConfigUtil.DateTimePatternForSql);


        args.SToday = type;
        args.SSource = "Home";
        args.Floor = "";
        args.Status = "";
        #endregion

        List<CustomerActivitiesDTO> data = new List<CustomerActivitiesDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {

            args.StartIndex = skip / take + 1;
            args.RowCount = take;

            args.OrderBy = " subject desc";

            data = activityClient.GetAllActivitiesForHome(args);

            if (data != null)
            {
                if (data.Count != 0)
                {
                    obj_dataSourceResult = new DataSourceResult();
                    obj_dataSourceResult.Data = data;
                    obj_dataSourceResult.Total = data[0].rowCount;
                }
                else
                {
                    obj_dataSourceResult = new DataSourceResult();
                    obj_dataSourceResult.Data = data;
                    obj_dataSourceResult.Total = 0;
                }
            }
            else
            {
                obj_dataSourceResult = new DataSourceResult();
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = 0;
            }
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }
    #endregion

    #region Dashboard

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAppointmentsByType(string id, string type, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        List<CustomerOpportunitiesDTO> lstOpportunity = null;
        List<ClientOpportunityDTO> lstClientOpportunity = null;
        ArgsDTO argsDTO = new ArgsDTO();
        //CommonUtility commonUtility = new CommonUtility();
        //KeyValuePair<string, string> pipelineChart = PipelineChart;

        //if (filter != null)
        //{
        //    SetFilterField(filter);
        //    argsDTO.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
        //}
        if (Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            argsDTO = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;

            DateTime dtmStartDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1, 0, 0, 0);
            DateTime dtmLastDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddDays(-1);
            DateTime dtmEndDate = new DateTime();
            switch (id)
            {
                case "0":
                    dtmEndDate = dtmStartDate.AddDays(6);
                    break;
                case "1":
                    dtmStartDate = dtmStartDate.AddDays(7);
                    dtmEndDate = dtmStartDate.AddDays(6);
                    break;
                case "2":
                    dtmStartDate = dtmStartDate.AddDays(14);
                    dtmEndDate = dtmStartDate.AddDays(6);
                    break;
                case "3":
                    dtmStartDate = dtmStartDate.AddDays(21);
                    dtmEndDate = dtmStartDate.AddDays(6);
                    break;
                case "4":
                    dtmStartDate = dtmStartDate.AddDays(21);
                    dtmEndDate = dtmStartDate.AddDays(6);
                    break;

                case "5":
                    dtmEndDate = dtmStartDate.AddDays(-1);
                    dtmStartDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1);
                    break;

                case "6":
                    dtmStartDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2);
                    dtmEndDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).AddDays(-1);
                    break;

                case "7":
                    dtmStartDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-3);
                    dtmEndDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).AddDays(-1);
                    break;
            }

            argsDTO.SStartDate = dtmStartDate.ToString("dd-MMM-yyyy") + " 00:00:00";
            argsDTO.SEndDate = dtmEndDate.ToString("dd-MMM-yyyy") + " 23:59:59";
            argsDTO.Originator = string.IsNullOrEmpty(argsDTO.Originator) ? "" : argsDTO.Originator;
            //argsDTO.ChildOriginators = UserSession.Instance.ChildOriginators;
            //argsDTO.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            //argsDTO.Originator = UserSession.Instance.UserName;
            if (sortExpression != null && sortExpression.Count > 0)
            {
                argsDTO.OrderBy = "";
                foreach (Sort item in sortExpression)
                {
                    item.Field = GetSortType(item.Field);
                    if (string.IsNullOrEmpty(argsDTO.OrderBy))
                        argsDTO.OrderBy = item.ToExpression();
                    else
                        argsDTO.OrderBy = argsDTO.OrderBy + ", " + item.ToExpression();
                }
            }
            else
            {
                argsDTO.OrderBy = "lead_name ASC";
            }
            argsDTO.StartIndex = (skip / take) + 1;
            argsDTO.RowCount = take;
        }
        argsDTO.AdditionalParams = " type_description = '" + type + "' ";
        //if (status == "0" && !string.IsNullOrEmpty(clientType))
        //{
        //    if (clientType == "Lead")
        //        clientType = "lead_stage != 'Customer' AND lead_stage != 'End User'";
        //    else if (clientType == "Customer")
        //        clientType = "lead_stage = 'Customer'";
        //    else if (clientType == "End User")
        //        clientType = "lead_stage = 'End User'";
        //    else
        //        clientType = " 0=0 ";
        //    if (string.IsNullOrEmpty(argsDTO.AdditionalParams))
        //    {
        //        argsDTO.AdditionalParams = clientType;
        //    }
        //    else
        //    {
        //        argsDTO.AdditionalParams = argsDTO.AdditionalParams + " AND " + clientType;
        //    }
        //}

        //List<string> back = new List<string>();
        //back.Add(argsDTO.StartIndex.ToString());
        //BackButtonForAll = back;

        // int isCustomerPipeline = int.Parse(Session[CommonUtility.SELECTED_CUSTOMER].ToString());

        //if (status == "0")
        //{
        //    lstOpportunity = OpportunityService.GetAllOpportunities(int.Parse(pipelineStageId), argsDTO);
        //}
        //else
        //{
        //    lstClientOpportunity = OpportunityService.GetAllClientOpportunity(int.Parse(pipelineStageId), argsDTO);
        //}
        AppointmentClient oAppointment = new AppointmentClient();
        List<AppointmentViewDTO> listAppointments = new List<AppointmentViewDTO>();
        listAppointments = oAppointment.GetAllAppointmentsByType(argsDTO);
        DataSourceResult dataSourceResult = new DataSourceResult();

        if (listAppointments != null && listAppointments.Count > 0)
        {
            dataSourceResult.Data = listAppointments;
            dataSourceResult.Total = listAppointments[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = listAppointments;
            dataSourceResult.Total = 0;

        }
        //OpportunityService.Close();

        return dataSourceResult;

    }

    private string GetSortType(string sortName)
    {
        string sortingType = "";

        try
        {
            if (sortName == "LeadName")
            {
                sortingType = "lead_name";
            }
            else if (sortName == "Category")
            {
                sortingType = "category";
            }
            else if (sortName == "StartTime")
            {
                sortingType = "start_time";
            }
            else if (sortName == "EndTime")
            {
                sortingType = "end_time";
            }
            else if (sortName == "Body")
            {
                sortingType = "body";
            }
            else if (sortName == "Subject")
            {
                sortingType = "subject";
            }
            else if (sortName == "LeadStage")
            {
                sortingType = "lead_stage";
            }

            return sortingType;
        }
        catch (Exception)
        {

            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetActivityByType(string id, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        ArgsDTO argsDTO = new ArgsDTO();

        if (Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            argsDTO = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;

            DateTime dtmStartDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1, 0, 0, 0);
            DateTime dtmLastDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddDays(-1);
            DateTime dtmEndDate = new DateTime();
            switch (id)
            {
                case "0":
                    dtmEndDate = dtmStartDate.AddDays(6);
                    break;
                case "1":
                    dtmStartDate = dtmStartDate.AddDays(7);
                    dtmEndDate = dtmStartDate.AddDays(6);
                    break;
                case "2":
                    dtmStartDate = dtmStartDate.AddDays(14);
                    dtmEndDate = dtmStartDate.AddDays(6);
                    break;
                case "3":
                    dtmStartDate = dtmStartDate.AddDays(21);
                    dtmEndDate = dtmStartDate.AddDays(6);
                    break;
                case "4":
                    dtmStartDate = dtmStartDate.AddDays(21);
                    dtmEndDate = dtmStartDate.AddDays(6);
                    break;

                case "5":
                    dtmEndDate = dtmStartDate.AddDays(-1);
                    dtmStartDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1);
                    break;

                case "6":
                    dtmStartDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2);
                    dtmEndDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).AddDays(-1);
                    break;

                case "7":
                    dtmStartDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-3);
                    dtmEndDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-2).AddDays(-1);
                    break;
                case "8":
                    dtmStartDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-3);
                    dtmEndDate = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1).AddDays(-1);
                    break;
            }

            argsDTO.SStartDate = dtmStartDate.ToString("dd-MMM-yyyy") + " 00:00:00";
            argsDTO.SEndDate = dtmEndDate.ToString("dd-MMM-yyyy") + " 23:59:59";
            argsDTO.Originator = string.IsNullOrEmpty(argsDTO.Originator) ? "" : argsDTO.Originator;

            if (sortExpression != null && sortExpression.Count > 0)
            {
                argsDTO.OrderBy = "";
                foreach (Sort item in sortExpression)
                {
                    item.Field = GetSortType(item.Field);
                    if (string.IsNullOrEmpty(argsDTO.OrderBy))
                        argsDTO.OrderBy = item.ToExpression();
                    else
                        argsDTO.OrderBy = argsDTO.OrderBy + ", " + item.ToExpression();
                }
            }
            else
            {
                argsDTO.OrderBy = "lead_name ASC";
            }
            argsDTO.StartIndex = (skip / take) + 1;
            argsDTO.RowCount = take;
        }
        //argsDTO.AdditionalParams = " category = '" + type + "' ";

        //AppointmentClient oAppointment = new AppointmentClient();
        //List<AppointmentViewDTO> listAppointments = new List<AppointmentViewDTO>();
        //listAppointments = oAppointment.GetAllAppointmentsByType(argsDTO);

        List<CustomerActivitiesDTO> listActivity = new List<CustomerActivitiesDTO>();
        ActivityClient oActivity = new ActivityClient();

        listActivity = oActivity.GetAllActivitiesByType(argsDTO);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (listActivity != null && listActivity.Count > 0)
        {
            dataSourceResult.Data = listActivity;
            dataSourceResult.Total = listActivity[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = listActivity;
            dataSourceResult.Total = 0;

        }
        //OpportunityService.Close();

        return dataSourceResult;

    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllCallCycles(string id, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        ArgsDTO argsDTO = new ArgsDTO();

        if (Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            argsDTO = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
            argsDTO.LeadId = int.Parse(id);


            if (sortExpression != null && sortExpression.Count > 0)
            {
                argsDTO.OrderBy = "";
                foreach (Sort item in sortExpression)
                {
                    item.Field = GetSortTypeCallCycles(item.Field);
                    if (string.IsNullOrEmpty(argsDTO.OrderBy))
                        argsDTO.OrderBy = item.ToExpression();
                    else
                        argsDTO.OrderBy = argsDTO.OrderBy + ", " + item.ToExpression();
                }
            }
            else
            {
                argsDTO.OrderBy = "lead_name ASC";
            }
            argsDTO.StartIndex = (skip / take) + 1;
            argsDTO.RowCount = take;
        }

        List<CallCycleViewDTO> listCallCycleContact = new List<CallCycleViewDTO>();
        ActivityClient oActivity = new ActivityClient();

        listCallCycleContact = oActivity.GetAllCallCycles(argsDTO);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (listCallCycleContact != null && listCallCycleContact.Count > 0)
        {
            dataSourceResult.Data = listCallCycleContact;
            dataSourceResult.Total = listCallCycleContact[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = listCallCycleContact;
            dataSourceResult.Total = 0;

        }
        //OpportunityService.Close();

        return dataSourceResult;

    }

    private string GetSortTypeCallCycles(string sortName)
    {
        string sortingType = "";

        try
        {
            if (sortName == "CallCycleID")
            {
                sortingType = "callcycle_id";
            }
            else if (sortName == "SourceId")
            {
                sortingType = "lead_id";
            }
            else if (sortName == "Description")
            {
                sortingType = "description";
            }
            else if (sortName == "DueOn")
            {
                sortingType = "dueon";
            }
            else if (sortName == "Name")
            {
                sortingType = "lead_name";
            }
            else if (sortName == "Originator")
            {
                sortingType = "created_by";
            }
            else if (sortName == "LeadStage")
            {
                sortingType = "lead_stage";
            }
            else if (sortName == "Address")
            {
                sortingType = "address";
            }
            else if (sortName == "City")
            {
                sortingType = "city";
            }
            else if (sortName == "State")
            {
                sortingType = "state";
            }
            else if (sortName == "PostalCode")
            {
                sortingType = "postcode";
            }


            return sortingType;
        }
        catch (Exception)
        {

            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetLeaderCustomerOpportunityCount(string rep, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        ArgsDTO argsDTO = new ArgsDTO();

        if (Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            argsDTO = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
            if (!string.IsNullOrEmpty(rep))
                argsDTO.Originator = rep;
            else
                argsDTO.Originator = "";

            if (sortExpression != null && sortExpression.Count > 0)
            {
                argsDTO.OrderBy = "";
                foreach (Sort item in sortExpression)
                {
                    //item.Field = GetSortTypeCallCycles(item.Field);
                    if (string.IsNullOrEmpty(argsDTO.OrderBy))
                        argsDTO.OrderBy = item.ToExpression();
                    else
                        argsDTO.OrderBy = argsDTO.OrderBy + ", " + item.ToExpression();
                }
            }
            else
            {
                argsDTO.OrderBy = "name ASC";
            }
            argsDTO.StartIndex = (skip / take) + 1;
            argsDTO.RowCount = take;
        }

        List<LeadCustomerOpportunityDAO> leadCustOppList = commonClient.GetLeaderCustomerOpportunityCount(argsDTO);
        //List<CallCycleViewDTO> listCallCycleContact = new List<CallCycleViewDTO>();
        //ActivityClient oActivity = new ActivityClient();

        //listCallCycleContact = oActivity.GetAllCallCycles(argsDTO);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (leadCustOppList != null && leadCustOppList.Count > 0)
        {
            dataSourceResult.Data = leadCustOppList;
            dataSourceResult.Total = leadCustOppList.Count;
        }
        else
        {
            dataSourceResult.Data = leadCustOppList;
            dataSourceResult.Total = 0;

        }
        //OpportunityService.Close();

        return dataSourceResult;

    }

    private string GetSortTypeOpportunity(string sortName)
    {
        string sortingType = "";

        try
        {
            if (sortName == "Name")
            {
                sortingType = "name1";
            }
            else if (sortName == "Originator")
            {
                sortingType = "originator";
            }
            else if (sortName == "Opportunity")
            {
                sortingType = "opportunity";
            }
            else if (sortName == "RepName")
            {
                sortingType = "repname";
            }

            else if (sortName == "Stage")
            {
                sortingType = "pipeline_stage";
            }
            else if (sortName == "CreatedDate")
            {
                sortingType = "created_date";
            }

            else if (sortName == "Amount")
            {
                sortingType = "amount";
            }
            else if (sortName == "Tonnes")
            {
                sortingType = "tonnes";
            }
            else if (sortName == "State")
            {
                sortingType = "state";
            }

            else if (sortName == "MarketDescription")
            {
                sortingType = "market_description";
            }


            return sortingType;
        }
        catch (Exception)
        {

            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetLeaderCustomerOpportunity(string rep, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        ArgsDTO argsDTO = new ArgsDTO();

        if (Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            argsDTO = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
            if (!string.IsNullOrEmpty(rep))
                argsDTO.Originator = rep;

            if (sortExpression != null && sortExpression.Count > 0)
            {
                argsDTO.OrderBy = "";
                foreach (Sort item in sortExpression)
                {
                    item.Field = GetSortTypeOpportunity(item.Field);
                    if (string.IsNullOrEmpty(argsDTO.OrderBy))
                        argsDTO.OrderBy = item.ToExpression();
                    else
                        argsDTO.OrderBy = argsDTO.OrderBy + ", " + item.ToExpression();
                }
            }
            else
            {
                argsDTO.OrderBy = "Originator ASC";
            }
            argsDTO.StartIndex = (skip / take) + 1;
            argsDTO.RowCount = take;
        }

        List<LeadCustomerOpportunityDAO> leadCustOppList = commonClient.GetLeaderCustomerOpportunity(argsDTO);
        //List<CallCycleViewDTO> listCallCycleContact = new List<CallCycleViewDTO>();
        //ActivityClient oActivity = new ActivityClient();

        //listCallCycleContact = oActivity.GetAllCallCycles(argsDTO);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (leadCustOppList != null && leadCustOppList.Count > 0)
        {
            dataSourceResult.Data = leadCustOppList;
            dataSourceResult.Total = leadCustOppList[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = leadCustOppList;
            dataSourceResult.Total = 0;

        }
        //OpportunityService.Close();

        return dataSourceResult;

    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetEnduserSalesWithAllProducts(int prodWithNoSales, int activeOnly, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        ArgsDTO argsDTO = new ArgsDTO();
        //Session[CommonUtility.GLOBAL_SETTING] = argsDTO;
        if (Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            argsDTO = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
            //if (!string.IsNullOrEmpty(rep))
            //    argsDTO.Originator = rep;
            //argsDTO.RepCode = "Q2";

            //argsDTO.CustomerCode = "";
            //argsDTO.EnduserCode = "";
            //argsDTO.SMonth =10;
            //argsDTO.SYear = 2012;

            ////if (sortExpression != null && sortExpression.Count > 0)
            ////{
            ////    argsDTO.OrderBy = "";
            ////    foreach (Sort item in sortExpression)
            ////    {
            ////        item.Field = GetSortTypeOpportunity(item.Field);
            ////        if (string.IsNullOrEmpty(argsDTO.OrderBy))
            ////            argsDTO.OrderBy = item.ToExpression();
            ////        else
            ////            argsDTO.OrderBy = argsDTO.OrderBy + ", " + item.ToExpression();
            ////    }
            ////}
            ////else
            ////{
            ////    argsDTO.OrderBy = "Originator ASC";
            ////}
            argsDTO.StartIndex = (skip / take) + 1;
            argsDTO.RowCount = take;
        }
        EndUserClient endUserClient = new EndUserClient();
        List<EnduserEnquiryDTO> leadCustOppList = endUserClient.GetEnduserSalesWithAllProducts(argsDTO, prodWithNoSales == 1, activeOnly == 1);
        //List<CallCycleViewDTO> listCallCycleContact = new List<CallCycleViewDTO>();
        //ActivityClient oActivity = new ActivityClient();

        //listCallCycleContact = oActivity.GetAllCallCycles(argsDTO);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (leadCustOppList != null && leadCustOppList.Count > 0)
        {
            dataSourceResult.Data = leadCustOppList;
            dataSourceResult.Total = leadCustOppList[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = leadCustOppList;
            dataSourceResult.Total = 0;

        }
        //OpportunityService.Close();

        return dataSourceResult;

    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetEOISales(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        ArgsDTO argsDTO = new ArgsDTO();
        //Session[CommonUtility.GLOBAL_SETTING] = argsDTO;
        if (Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            argsDTO = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
            //if (!string.IsNullOrEmpty(rep))
            //    argsDTO.Originator = rep;
            //argsDTO.RepCode = "Q2";

            //argsDTO.CustomerCode = "";// "5WADANV0";
            //argsDTO.RepCode = "S2";
            //argsDTO.RepType = "F";

            ////if (sortExpression != null && sortExpression.Count > 0)
            ////{
            ////    argsDTO.OrderBy = "";
            ////    foreach (Sort item in sortExpression)
            ////    {
            ////        item.Field = GetSortTypeOpportunity(item.Field);
            ////        if (string.IsNullOrEmpty(argsDTO.OrderBy))
            ////            argsDTO.OrderBy = item.ToExpression();
            ////        else
            ////            argsDTO.OrderBy = argsDTO.OrderBy + ", " + item.ToExpression();
            ////    }
            ////}
            ////else
            ////{
            ////    argsDTO.OrderBy = "Originator ASC";
            ////}
            argsDTO.StartIndex = (skip / take) + 1;
            argsDTO.RowCount = take;
        }
        SalesClient salesClient = new SalesClient();
        List<EOISalesDTO> leadCustOppList = salesClient.GetEOISales(argsDTO);
        //List<CallCycleViewDTO> listCallCycleContact = new List<CallCycleViewDTO>();
        //ActivityClient oActivity = new ActivityClient();

        //listCallCycleContact = oActivity.GetAllCallCycles(argsDTO);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (leadCustOppList != null && leadCustOppList.Count > 0)
        {
            dataSourceResult.Data = leadCustOppList;
            dataSourceResult.Total = leadCustOppList[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = leadCustOppList;
            dataSourceResult.Total = 0;

        }
        //OpportunityService.Close();

        return dataSourceResult;

    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetDistributorCodes(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        CommonClient commonClient = new CommonClient();
        OriginatorClient originatorClient = new OriginatorClient();
        List<LookupDTO> lookupList = null;

        string sortby = string.Empty;
        string sWhereCls = "";

        List<Sort> sortExpression = (List<Sort>)sort;

        #region Args Setting
        ArgsDTO args = new ArgsDTO();
        args.ChildOriginators = UserSession.Instance.ChildOriginators;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.Originator = UserSession.Instance.UserName;
        args.StartIndex = (skip / take) + 1;
        args.RowCount = take;
        #endregion


        sWhereCls = " AND " + UserSession.Instance.ChildOriginators;

        if (sortExpression != null && sortExpression.Count > 0)
        {
            args.OrderBy = "";
            foreach (Sort item in sortExpression)
            {
                item.Field = GetDistributorSortString(item.Field);
                if (string.IsNullOrEmpty(args.OrderBy))
                    args.OrderBy = item.ToExpression();
                else
                    args.OrderBy = args.OrderBy + ", " + item.ToExpression();
            }
        }
        else
        {
            args.OrderBy = " name asc";
        }

        if (filter != null)
        {
            new CommonUtility().SetFilterField(ref filter, "distributor", "");
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
        }


        if (!string.IsNullOrEmpty(args.AdditionalParams))
        {
            sWhereCls = sWhereCls + " AND " + args.AdditionalParams;
        }
        sWhereCls = sWhereCls + " Order By " + args.OrderBy;


        lookupList = commonClient.GetLookups("CustomerLookupByOrder", sWhereCls, null, args, true);


        DataSourceResult dataSourceResult = new DataSourceResult();

        if (lookupList != null)
        {
            if (lookupList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = lookupList;
                dataSourceResult.Total = lookupList[0].rowCount;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = lookupList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = lookupList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    //-- Rep Code ------

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetCustomerRepCodes(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        CommonClient commonClient = new CommonClient();
        OriginatorClient originatorClient = new OriginatorClient();
        List<LookupDTO> lookupList = null;

        string sortby = string.Empty;
        string sWhereCls = "";

        List<Sort> sortExpression = (List<Sort>)sort;

        #region Args Setting
        ArgsDTO args = new ArgsDTO();
        args.ChildOriginators = UserSession.Instance.ChildOriginators;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.Originator = UserSession.Instance.UserName;
        args.StartIndex = (skip / take) + 1;
        args.RowCount = take;
        #endregion


        //sWhereCls = " AND " + UserSession.Instance.ChildOriginators;
        sWhereCls = " " + UserSession.Instance.ChildOriginators;

        sWhereCls = sWhereCls.Replace("originator", "r.originator");

        if (sortExpression != null && sortExpression.Count > 0)
        {
            args.OrderBy = "";
            foreach (Sort item in sortExpression)
            {
                item.Field = GetDistributorSortString(item.Field);
                if (string.IsNullOrEmpty(args.OrderBy))
                    args.OrderBy = item.ToExpression();
                else
                    args.OrderBy = args.OrderBy + ", " + item.ToExpression();
            }
        }
        else
        {
            args.OrderBy = " name asc";
        }

        if (filter != null)
        {
            new CommonUtility().SetFilterField(ref filter, "distributor", "");
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
        }


        if (!string.IsNullOrEmpty(args.AdditionalParams))
        {
            sWhereCls = sWhereCls + " AND " + args.AdditionalParams;
        }
        //    sWhereCls = sWhereCls + " Order By " + args.OrderBy;


        lookupList = commonClient.GetLookups("RepLookup", sWhereCls, null, args, true);


        DataSourceResult dataSourceResult = new DataSourceResult();

        if (lookupList != null)
        {
            if (lookupList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = lookupList;
                dataSourceResult.Total = lookupList[0].rowCount;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = lookupList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = lookupList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }


    //---end of rep code ---- 


    private string GetDistributorSortString(string sorttype)
    {
        string sortStr = "name";

        switch (sorttype)
        {
            case "Code":
                sortStr = "cust_code";
                break;
            default:
                sortStr = "name";
                break;
        }

        return sortStr;
    }
    private string GetEndUserSortString(string sorttype)
    {
        string sortStr = "name";

        switch (sorttype)
        {
            case "Code":
                sortStr = "enduser_code";
                break;
            default:
                sortStr = "name";
                break;
        }

        return sortStr;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetEndUserLookup(string custCode, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        CommonClient commonClient = new CommonClient();
        OriginatorClient originatorClient = new OriginatorClient();
        List<LookupDTO> lookupList = null;

        string sortby = string.Empty;
        string sWhereCls = "";

        List<Sort> sortExpression = (List<Sort>)sort;

        #region Args Setting
        ArgsDTO args = new ArgsDTO();
        args.ChildOriginators = UserSession.Instance.ChildOriginators;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.Originator = UserSession.Instance.UserName;
        args.StartIndex = (skip / take) + 1;
        args.RowCount = take;
        #endregion

        sWhereCls = "WHERE de.del_flag != 'Y'" +
            (!string.IsNullOrWhiteSpace(custCode) ? " AND de.cust_code = '" + custCode + "'" : string.Empty);

        sWhereCls = sWhereCls + " AND " + UserSession.Instance.ChildOriginators;

        if (sortExpression != null && sortExpression.Count > 0)
        {
            args.OrderBy = "";
            foreach (Sort item in sortExpression)
            {
                item.Field = GetEndUserSortString(item.Field);
                if (string.IsNullOrEmpty(args.OrderBy))
                    args.OrderBy = item.ToExpression();
                else
                    args.OrderBy = args.OrderBy + ", " + item.ToExpression();
            }
        }
        else
        {
            args.OrderBy = " name desc";
        }

        if (filter != null)
        {
            new CommonUtility().SetFilterField(ref filter, "enduser", "");
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
        }

        if (!string.IsNullOrEmpty(args.AdditionalParams))
        {
            sWhereCls = sWhereCls + " AND " + args.AdditionalParams;
        }
        sWhereCls = sWhereCls + " Order By " + args.OrderBy;



        lookupList = commonClient.GetLookups("EndUserLookup", sWhereCls, null, args, true);


        DataSourceResult dataSourceResult = new DataSourceResult();

        if (lookupList != null)
        {
            if (lookupList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = lookupList;
                dataSourceResult.Total = lookupList[0].rowCount;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = lookupList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = lookupList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }
    #endregion

    #region BusinessSales

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetBusinessSales(string sortText, string detailType,string type, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;
        //List<TargetInfoDTO> targetlist = new List<TargetInfoDTO>();
        ArgsDTO argsDTO = new ArgsDTO();
        //Session[CommonUtility.GLOBAL_SETTING] = argsDTO;
        if (Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            argsDTO = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
        }
        //if (!string.IsNullOrEmpty(rep))
        //    argsDTO.Originator = rep;
        //argsDTO.RepCode = "Q2";

        //argsDTO.CustomerCode = "";// "5WADANV0";
        //argsDTO.RepCode = "S2";
        //argsDTO.RepType = "F";

        if (sortText == "")
        {

            if (sortExpression != null && sortExpression.Count > 0)
            {
                argsDTO.OrderBy = "";
                foreach (Sort item in sortExpression)
                {
                    item.Field = GetSortTypeBusinessSales(item.Field, detailType);
                    if (string.IsNullOrEmpty(argsDTO.OrderBy))
                        argsDTO.OrderBy = item.ToExpression();
                    else
                        argsDTO.OrderBy = argsDTO.OrderBy + ", " + item.ToExpression();
                }
            }
            else
            {
                argsDTO.OrderBy = " name asc" ;
            }
        }
        else
        {
            argsDTO.OrderBy = GetSortTypeBusinessSales(sortText, detailType);
            argsDTO.OrderBy += " ASC";
            //if (sortText == "MC")
            //{
            //    argsDTO.OrderBy += " ASC";
            //}
            //else
            //{
            //    argsDTO.OrderBy += " DESC";
            //}
        }

        argsDTO.AdditionalParams = "";

        if (filter != null)
        {
            if (filter.Filters != null)
            {
                foreach (Filter item in filter.Filters)
                {
                    item.Field = GetSortTypeBusinessSales(item.Field, detailType);
                }
            }

            argsDTO.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        argsDTO.StartIndex = (skip / take) + 1;
        argsDTO.RowCount = take;

        Session[CommonUtility.GLOBAL_SETTING] = argsDTO;

        SalesInfoDetailSearchCriteriaDTO salesInfoDetSrcCritaria = (SalesInfoDetailSearchCriteriaDTO)Session[CommonUtility.SALES_INFO_DETAIL];
        OriginatorClient originatorService = new OriginatorClient(); 
        SalesClient salesClient = new SalesClient();
        SalesInfoDetailViewStateDTO oSalesInfoDetailViewState = new SalesInfoDetailViewStateDTO();
        //oSalesInfoDetailViewState = salesClient.GetSalesInfoDetailViewState(salesInfoDetSrcCritaria, argsDTO, "grid");

        argsDTO.Originator = UserSession.Instance.UserName;

        DataSourceResult dataSourceResult = new DataSourceResult();
        try
        {
            salesInfoDetSrcCritaria.cDisplayOption = type.ToCharArray()[0];


            oSalesInfoDetailViewState = originatorService.GetRepMonthleyTargetsByDistributor(salesInfoDetSrcCritaria, argsDTO);
            string sFlowContent = LoadViewState(oSalesInfoDetailViewState);

            //targetlist = HttpContext.Current.Session[CommonUtility.REP_TARGETS] as List<TargetInfoDTO>;

            //argsDTO.RepCode = salesInfoDetSrcCritaria.RepCode;
            //argsDTO.CustomerCode = salesInfoDetSrcCritaria.CustomerCode;
            //argsDTO.Brand = salesInfoDetSrcCritaria.Brand;

            //string sFlowContent = LoadViewState(argsDTO);

            //targetlist = originatorService.GetRepMonthleyTargetsByDistributor(UserSession.Instance.UserName, detailType,argsDTO);

            if (oSalesInfoDetailViewState.TargetInfoEntityList != null && oSalesInfoDetailViewState.TargetInfoEntityList.Count > 0)
            {
                oSalesInfoDetailViewState.TargetInfoEntityList.ForEach(x => x.FlowContent = sFlowContent);
                dataSourceResult.Data = oSalesInfoDetailViewState.TargetInfoEntityList;
                dataSourceResult.Total = oSalesInfoDetailViewState.TargetInfoEntityList[0].RowCount;
            }
            else
            {
                dataSourceResult.Data = oSalesInfoDetailViewState.TargetInfoEntityList;
                dataSourceResult.Total = 0;

            }
        }
        catch { }
        //OpportunityService.Close();

        return dataSourceResult;

    }

    private string GetSortTypeBusinessSalesEndUser(string sortName, string detailType)
    {
        string sortingType = "";

        try
        {
            if (sortName == "MC")
            {
                sortingType = "code";
            }
            else if (sortName == "M1D")
            {
                sortingType = "Sum1";
            }
            else if (sortName == "M1T")
            {
                sortingType = "Sum2";
            }
            else if (sortName == "M1U")
            {
                sortingType = "Sum2";
            }

            else if (sortName == "M2D")
            {
                sortingType = "Sum3";
            }
            else if (sortName == "M2T")
            {
                sortingType = "Sum4";
            }
            else if (sortName == "M2U")
            {
                sortingType = "Sum4";
            }

            else if (sortName == "M3D")
            {
                sortingType = "Sum5";
            }
            else if (sortName == "M3T")
            {
                sortingType = "Sum6";
            }
            else if (sortName == "M3U")
            {
                sortingType = "Sum6";
            }

            else if (sortName == "M4D")
            {
                sortingType = "Sum7";
            }
            else if (sortName == "M4T")
            {
                sortingType = "Sum8";
            }
            else if (sortName == "M4U")
            {
                sortingType = "Sum8";
            }

            else if (sortName == "M5D")
            {
                sortingType = "Sum9";
            }
            else if (sortName == "M5T")
            {
                sortingType = "Sum10";
            }
            else if (sortName == "M5U")
            {
                sortingType = "Sum10";
            }

            else if (sortName == "M6D")
            {
                sortingType = "Sum11";
            }
            else if (sortName == "M6T")
            {
                sortingType = "Sum12";
            }
            else if (sortName == "M6U")
            {
                sortingType = "Sum12";
            }

            else if (sortName == "MTD")
            {
                sortingType = "(Sum1 + Sum3 + Sum5 + Sum7 + Sum9 + Sum11)";
            }
            else if (sortName == "MTT")
            {
                sortingType = "(Sum2 + Sum4 + Sum6 + Sum8 + Sum10 + Sum12)";
            }
            else if (sortName == "MTU")
            {
                sortingType = "(Sum2 + Sum4 + Sum6 + Sum8 + Sum10 + Sum12)";
            }

            else if (sortName == "MD")
            {
                sortingType = "description";
            }

            else if (sortName == "MTYTDD")
            {
                sortingType = "";
            }
            else if (sortName == "MTYTDT")
            {
                sortingType = "";
            }
            else if (sortName == "MTYTDU")
            {
                sortingType = "";
            }

            else if (sortName == "MLYTDD")
            {
                sortingType = "";
            }
            else if (sortName == "MLYTDT")
            {
                sortingType = "";
            }
            else if (sortName == "MLYTDU")
            {
                sortingType = "";
            }

            else if (sortName == "MPD")
            {
                sortingType = "";
            }
            else if (sortName == "MPT")
            {
                sortingType = "";
            }
            else if (sortName == "MPU")
            {
                sortingType = "";
            }


            else if (sortName == "MY1D")
            {
                sortingType = "";
            }
            else if (sortName == "MY1T")
            {
                sortingType = "";
            }
            else if (sortName == "MY1U")
            {
                sortingType = "";
            }

            else if (sortName == "MY2D")
            {
                sortingType = "";
            }
            else if (sortName == "MY2T")
            {
                sortingType = "";
            }
            else if (sortName == "MY2U")
            {
                sortingType = "";
            }

            else if (sortName == "MY3D")
            {
                sortingType = "";
            }
            else if (sortName == "MY3T")
            {
                sortingType = "";
            }
            else if (sortName == "MY3U")
            {
                sortingType = "";
            }

            return sortingType;
        }
        catch (Exception)
        {

            throw;
        }
    }

    private string GetSortTypeBusinessSales(string sortName, string detailType)
    {
        string sortingType = "";

        try
        {
            if (sortName == "Name")
            {
                sortingType = "name";
                
            }
            else if (sortName == "Code")
            {
                sortingType = "code";
            }

            return sortingType;
        }
        catch (Exception)
        {

            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetBusinessSalesEndUser(string sortText, string detailType, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        ArgsDTO argsDTO = new ArgsDTO();
        //Session[CommonUtility.GLOBAL_SETTING] = argsDTO;
        if (Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            argsDTO = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
        }
        //if (!string.IsNullOrEmpty(rep))
        //    argsDTO.Originator = rep;
        //argsDTO.RepCode = "Q2";

        //argsDTO.CustomerCode = "";// "5WADANV0";
        //argsDTO.RepCode = "S2";
        //argsDTO.RepType = "F";

        if (sortText == "")
        {

            if (sortExpression != null && sortExpression.Count > 0)
            {
                argsDTO.OrderBy = "";
                foreach (Sort item in sortExpression)
                {
                    item.Field = GetSortTypeBusinessSalesEndUser(item.Field, detailType);
                    if (!string.IsNullOrEmpty(item.Field))
                    {
                        if (string.IsNullOrEmpty(argsDTO.OrderBy))
                            argsDTO.OrderBy = item.ToExpression();
                        else
                            argsDTO.OrderBy = argsDTO.OrderBy + ", " + item.ToExpression();
                    }
                }
            }
            else
            {
                argsDTO.OrderBy = null;
            }
        }
        else
        {
            argsDTO.OrderBy = GetSortTypeBusinessSalesEndUser(sortText, detailType);
            if (!string.IsNullOrEmpty(argsDTO.OrderBy))
            {
                if (sortText == "MC")
                {
                    argsDTO.OrderBy += " ASC";
                }
                else
                {
                    argsDTO.OrderBy += " DESC";
                }
            }
            else
            {
                argsDTO.OrderBy = null;
            }
        }

        argsDTO.AdditionalParams = "";
        if (filter != null)
        {
            if (filter.Filters != null)
            {
                foreach (Filter item in filter.Filters)
                {
                    item.Field = GetSortTypeBusinessSales(item.Field, detailType);
                    item.Logic = "";
                    item.Operator = "";
                    item.Value = "";
                }
            }

            argsDTO.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        argsDTO.StartIndex = (skip / take) + 1;
        argsDTO.RowCount = take;

        Session[CommonUtility.GLOBAL_SETTING] = argsDTO;

        SalesInfoDetailSearchCriteriaDTO salesInfoDetSrcCritaria = (SalesInfoDetailSearchCriteriaDTO)Session[CommonUtility.SALES_INFO_DETAIL_ENDUSER];
        salesInfoDetSrcCritaria.RepType = "";
        SalesClient salesClient = new SalesClient();
        SalesInfoDetailViewStateDTO oSalesInfoDetailViewState = new SalesInfoDetailViewStateDTO();
        oSalesInfoDetailViewState = salesClient.GetEndUserSalesInfoDetails(salesInfoDetSrcCritaria, argsDTO, "grid");

        string sFlowContent = LoadViewState(oSalesInfoDetailViewState);

        DataSourceResult dataSourceResult = new DataSourceResult();

        //if (oSalesInfoDetailViewState.LstCustomizedTableGrid != null && oSalesInfoDetailViewState.LstCustomizedTableGrid.Count > 0)
        //{
        //    oSalesInfoDetailViewState.LstCustomizedTableGrid.ForEach(x => x.FlowContent = sFlowContent);
        //    dataSourceResult.Data = oSalesInfoDetailViewState.LstCustomizedTableGrid;
        //    dataSourceResult.Total = oSalesInfoDetailViewState.LstCustomizedTableGrid[0].rowCount;
        //}
        //else
        //{
        //    dataSourceResult.Data = oSalesInfoDetailViewState.LstCustomizedTableGrid;
        //    dataSourceResult.Total = 0;

        //}
        //OpportunityService.Close();

        return dataSourceResult;

    }

    private string LoadViewState(SalesInfoDetailViewStateDTO oSalesInfoDetailViewState)
    {
        try
        {
            string sFlowContent = "";

            //if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sMonth))
            //    sFlowContent += "<span class=\"tlbcolor\">Month : </span>" + oSalesInfoDetailViewState.sMonth + " <span class=\"rnb\">&raquo; </span>";
            sFlowContent += "<span class=\"tlbcolor\">Month : </span>" + DateTime.Now.ToString("MMM") + " <span class=\"rnb\">&raquo; </span>";
            if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sProductType))
                sFlowContent += "<span class=\"tlbcolor\"> Product Type : </span>" + oSalesInfoDetailViewState.sProductType + " <span class=\"rnb\"> &raquo; </span>";
            
            if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sRep))
            {
                if (oSalesInfoDetailViewState.sRep != UserSession.Instance.UserName)
                    sFlowContent += "<span class=\"tlbcolor\"> DR : </span>" + oSalesInfoDetailViewState.sRep + " <span class=\"rnb\"> &raquo; </span>";
                else if (UserSession.Instance.OriginatorString == "DIST")
                    sFlowContent += "<span class=\"tlbcolor\"> Distributor : </span>" + UserSession.Instance.Originator + " <span class=\"rnb\"> &raquo; </span>";
                else if (UserSession.Instance.OriginatorString == "DR")
                    sFlowContent += "<span class=\"tlbcolor\"> DR : </span>" + UserSession.Instance.Originator + " <span class=\"rnb\"> &raquo; </span>";
                else if (UserSession.Instance.OriginatorString == "ASE")
                    sFlowContent += "<span class=\"tlbcolor\"> ASM : </span>" + UserSession.Instance.Originator + " <span class=\"rnb\"> &raquo; </span>";
            }
            
            if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sCustomer))
                sFlowContent += "<span class=\"tlbcolor\"> Customer : </span>" + oSalesInfoDetailViewState.sCustomer + " <span class=\"rnb\"> &raquo; </span>";
            if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sProduct))
                sFlowContent += "<span class=\"tlbcolor\"> Product : </span>" + oSalesInfoDetailViewState.sProduct + " <span class=\"rnb\"> &raquo; </span>";
            if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sBrand))
                sFlowContent += "<span class=\"tlbcolor\"> Brand : </span>" + oSalesInfoDetailViewState.sBrand + " <span class=\"rnb\"> &raquo; </span>";
            if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sMarket))
                sFlowContent += "<span class=\"tlbcolor\"> Market : </span>" + oSalesInfoDetailViewState.sMarket + " <span class=\"rnb\"> &raquo; </span>";

            return sFlowContent;



        }
        catch (Exception)
        {
            throw;
        }
    }


    //private string LoadViewState(ArgsDTO args)
    //{
    //    try
    //    {
    //        /*
             
    //                <span class="tlbcolor"> Product Type : </span>Refinery Products
    //        */
    //        string sFlowContent = "";

    //        //if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sMonth))
    //        //    sFlowContent += "<span class=\"tlbcolor\">Month : </span>" + oSalesInfoDetailViewState.sMonth + " <span class=\"rnb\">&raquo; </span>";
    //        if (!string.IsNullOrEmpty(args.RepCode) && args.RepCode != UserSession.Instance.UserName)
    //            sFlowContent += "<span class=\"tlbcolor\"> Rep : </span>" + args.RepCode + " <span class=\"rnb\"> &raquo; </span>";
    //        else 
    //            sFlowContent += "<span class=\"tlbcolor\"> Rep : </span>" + UserSession.Instance.Originator + " <span class=\"rnb\"> &raquo; </span>";

    //        if (!string.IsNullOrEmpty(args.CustomerCode))
    //            sFlowContent += "<span class=\"tlbcolor\"> Customer : </span>" + args.CustomerCode + " <span class=\"rnb\"> &raquo; </span>";
    //        //if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sProduct))
    //        //    sFlowContent += "<span class=\"tlbcolor\"> Product : </span>" + oSalesInfoDetailViewState.sProduct + " <span class=\"rnb\"> &raquo; </span>";
    //        if (!string.IsNullOrEmpty(args.Brand))
    //            sFlowContent += "<span class=\"tlbcolor\"> Brand : </span>" + args.Brand + " <span class=\"rnb\"> &raquo; </span>";
    //        //if (!string.IsNullOrEmpty(oSalesInfoDetailViewState.sMarket))
    //        //    sFlowContent += "<span class=\"tlbcolor\"> Market : </span>" + oSalesInfoDetailViewState.sMarket + " <span class=\"rnb\"> &raquo; </span>";
            
    //        return sFlowContent;



    //    }
    //    catch (Exception)
    //    {
    //        throw;
    //    }
    //}


    [WebMethod(EnableSession = true)]
    public DataSourceResult GetSalesHistoryMonth(string code, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        ArgsDTO argsDTO = new ArgsDTO();
        //Session[CommonUtility.GLOBAL_SETTING] = argsDTO;
        if (Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            argsDTO = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
        }
        //if (!string.IsNullOrEmpty(rep))
        //    argsDTO.Originator = rep;
        //argsDTO.RepCode = "Q2";

        //argsDTO.CustomerCode = "";// "5WADANV0";
        //argsDTO.RepCode = "S2";
        //argsDTO.RepType = "F";

        ////if (sortText == "")
        ////{

        string orderBy = "";
        string ordertype = "";
        if (sortExpression != null && sortExpression.Count > 0)
        {
            foreach (Sort item in sortExpression)
            {
                if (!string.IsNullOrEmpty(item.Field))
                {
                    orderBy = item.Field;
                    ordertype = item.Dir;
                }
            }
        }

        argsDTO.AdditionalParams = "";
        //if (filter != null)
        //{
        //    if (filter.Filters != null)
        //    {
        //        foreach (Filter item in filter.Filters)
        //        {
        //            item.Field = GetSortTypeBusinessSales(item.Field, detailType);
        //            item.Logic = "";
        //            item.Operator = "";
        //            item.Value = "";
        //        }
        //    }

        //    argsDTO.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        //}

        argsDTO.StartIndex = (skip / take) + 1;
        argsDTO.RowCount = take;

        Session[CommonUtility.GLOBAL_SETTING] = argsDTO;

        SalesInfoDetailSearchCriteriaDTO salesInfoDetSrcCritaria = (SalesInfoDetailSearchCriteriaDTO)Session[CommonUtility.SALES_INFO_DETAIL];

        salesInfoDetSrcCritaria = GetSalesInfoDetailSearchCriteria(salesInfoDetSrcCritaria, code, "", salesInfoDetSrcCritaria.DetailType, salesInfoDetSrcCritaria.cDisplayOption);

        SalesClient salesClient = new SalesClient();
        List<BusAllDTO> lstBusAllDTO = new List<BusAllDTO>();
        lstBusAllDTO = salesClient.GetBusAllDetails(salesInfoDetSrcCritaria, "grid");

        System.Reflection.PropertyInfo prop = typeof(BusAllDTO).GetProperty(orderBy);

        if (ordertype == "asc")
        {
            lstBusAllDTO = lstBusAllDTO.OrderBy(x => prop.GetValue(x, null)).ToList();
        }
        else if (ordertype == "desc")
        {
            lstBusAllDTO = lstBusAllDTO.OrderByDescending(x => prop.GetValue(x, null)).ToList();
        }

        //if (orderBy != "")
        //    lstBusAllDTO.OrderBy(c => orderBy);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (lstBusAllDTO != null && lstBusAllDTO.Count > 0)
        {
            dataSourceResult.Data = lstBusAllDTO;
            dataSourceResult.Total = lstBusAllDTO.Count;
        }
        else
        {
            dataSourceResult.Data = lstBusAllDTO;
            dataSourceResult.Total = 0;

        }
        //OpportunityService.Close();

        return dataSourceResult;

    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetSalesHistoryYear(string code, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        ArgsDTO argsDTO = new ArgsDTO();
        //Session[CommonUtility.GLOBAL_SETTING] = argsDTO;
        if (Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            argsDTO = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
        }
        //if (!string.IsNullOrEmpty(rep))
        //    argsDTO.Originator = rep;
        //argsDTO.RepCode = "Q2";

        //argsDTO.CustomerCode = "";// "5WADANV0";
        //argsDTO.RepCode = "S2";
        //argsDTO.RepType = "F";

        ////if (sortText == "")
        ////{

        string orderBy = "";
        string ordertype = "";
        if (sortExpression != null && sortExpression.Count > 0)
        {
            foreach (Sort item in sortExpression)
            {
                if (!string.IsNullOrEmpty(item.Field))
                {
                    orderBy = item.Field;
                    ordertype = item.Dir;
                }
            }
        }

        argsDTO.AdditionalParams = "";
        //if (filter != null)
        //{
        //    if (filter.Filters != null)
        //    {
        //        foreach (Filter item in filter.Filters)
        //        {
        //            item.Field = GetSortTypeBusinessSales(item.Field, detailType);
        //            item.Logic = "";
        //            item.Operator = "";
        //            item.Value = "";
        //        }
        //    }

        //    argsDTO.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        //}

        argsDTO.StartIndex = (skip / take) + 1;
        argsDTO.RowCount = take;

        Session[CommonUtility.GLOBAL_SETTING] = argsDTO;

        SalesInfoDetailSearchCriteriaDTO salesInfoDetSrcCritaria = (SalesInfoDetailSearchCriteriaDTO)Session[CommonUtility.SALES_INFO_DETAIL];

        salesInfoDetSrcCritaria = GetSalesInfoDetailSearchCriteria(salesInfoDetSrcCritaria, code, "", salesInfoDetSrcCritaria.DetailType, salesInfoDetSrcCritaria.cDisplayOption);

        SalesClient salesClient = new SalesClient();
        List<BusAllYearDTO> lstBusAllDTO = new List<BusAllYearDTO>();
        lstBusAllDTO = salesClient.GetBusAllYearDetails(salesInfoDetSrcCritaria, "grid");

        System.Reflection.PropertyInfo prop = typeof(BusAllDTO).GetProperty(orderBy);

        if (ordertype == "asc")
        {
            lstBusAllDTO = lstBusAllDTO.OrderBy(x => prop.GetValue(x, null)).ToList();
        }
        else if (orderBy == "desc")
        {
            lstBusAllDTO = lstBusAllDTO.OrderByDescending(x => prop.GetValue(x, null)).ToList();
        }

        //if (orderBy != "")
        //    lstBusAllDTO.OrderBy(c => orderBy);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (lstBusAllDTO != null && lstBusAllDTO.Count > 0)
        {
            dataSourceResult.Data = lstBusAllDTO;
            dataSourceResult.Total = lstBusAllDTO.Count;
        }
        else
        {
            dataSourceResult.Data = lstBusAllDTO;
            dataSourceResult.Total = 0;

        }
        //OpportunityService.Close();

        return dataSourceResult;

    }

    private SalesInfoDetailSearchCriteriaDTO GetSalesInfoDetailSearchCriteria(SalesInfoDetailSearchCriteriaDTO oOrgSrcCriteria,
        string commenCode, string commenDescription, string detailType, char displayOption)
    {
        try
        {
            //string commenCode = Request.QueryString["code"];
            //string commenDescription = Request.QueryString["desc"];
            //string detailType = Request.QueryString["dtype"];
            //char displayOption = char.Parse(Request.QueryString["option"]);

            SalesInfoDetailSearchCriteriaDTO sidSrcCriteria = new SalesInfoDetailSearchCriteriaDTO();
            //sidSrcCriteria.Market = oOrgSrcCriteria.Market;

            //sidSrcCriteria.BusinessArea = oOrgSrcCriteria.BusinessArea;

            // sidSrcCriteria.Brand = oOrgSrcCriteria.Brand;

            // sidSrcCriteria.CategoryGroup = oOrgSrcCriteria.CategoryGroup;

            //sidSrcCriteria.CategorySubGroup = oOrgSrcCriteria.CategorySubGroup;

            // sidSrcCriteria.CatalogCode = oOrgSrcCriteria.CatalogCode;
            //sidSrcCriteria.CatalogDescripion = oOrgSrcCriteria.CatalogDescripion;
            //sidSrcCriteria.CustomerCode = oOrgSrcCriteria.CustomerCode;
            //sidSrcCriteria.CustomerDescription = oOrgSrcCriteria.CustomerDescription;
            //sidSrcCriteria.RepCode = oOrgSrcCriteria.RepCode;
            //sidSrcCriteria.RepDescription = oOrgSrcCriteria.RepDescription;

            // sidSrcCriteria.State = oOrgSrcCriteria.State;
            //sidSrcCriteria.ParentCustomerCode = oOrgSrcCriteria.ParentCustomerCode;
            //sidSrcCriteria.parentCustomerDescription = oOrgSrcCriteria.parentCustomerDescription;

            sidSrcCriteria.sLastFlag = oOrgSrcCriteria.sLastFlag;
            sidSrcCriteria.Month = oOrgSrcCriteria.Month;
            sidSrcCriteria.iCostPeriod = oOrgSrcCriteria.iCostPeriod;
            sidSrcCriteria.cDisplayOption = displayOption;
            sidSrcCriteria.SortField = oOrgSrcCriteria.SortField;
            //sidSrcCriteria.CatalogType = oOrgSrcCriteria.CatalogType;
            //sidSrcCriteria.sLcSumTable = oOrgSrcCriteria.sLcSumTable;
            //sidSrcCriteria.BackeryOption = oOrgSrcCriteria.BackeryOption;
            sidSrcCriteria.DisplayFullName = oOrgSrcCriteria.DisplayFullName;
            //sidSrcCriteria.BusinessAreaList = oOrgSrcCriteria.BusinessAreaList;
            //sidSrcCriteria.RepCode = "";
            //sidSrcCriteria.RepDescription = "";
            //sidSrcCriteria.BusinessAreaList = new List<string>();

            sidSrcCriteria.DetailType = detailType;


            if (oOrgSrcCriteria.DetailType.Equals("bus area"))
            {
                //sidSrcCriteria.BusinessArea = commenCode;
            }
            else if (oOrgSrcCriteria.DetailType.Equals("market"))
            {
                sidSrcCriteria.Market = commenCode;
            }
            else if (oOrgSrcCriteria.DetailType.Equals("brand"))
            {
                sidSrcCriteria.Brand = commenCode;
            }
            //else if (oOrgSrcCriteria.DetailType.Equals("cat_group"))
            //{
            //    sidSrcCriteria.CategoryGroup = commenCode;
            //}
            //else if (oOrgSrcCriteria.DetailType.Equals("cat_sub_group"))
            //{
            //    sidSrcCriteria.CategorySubGroup = commenCode;
            //}
            //else if (oOrgSrcCriteria.DetailType.Equals("product"))
            //{
            //    sidSrcCriteria.CatalogCode = commenCode;
            //    sidSrcCriteria.CatalogDescripion = commenDescription;
            //}
            else if (oOrgSrcCriteria.DetailType.Equals("customer"))
            {
                sidSrcCriteria.CustomerCode = commenCode;
                sidSrcCriteria.CustomerDescription = commenDescription;
            }
            //else if (oOrgSrcCriteria.DetailType.Equals("custgroup"))
            //{
            //    if (string.IsNullOrEmpty(commenDescription))
            //    {
            //        sidSrcCriteria.ParentCustomerCode = "";
            //        sidSrcCriteria.parentCustomerDescription = "Customers With No Group";
            //    }
            //    else
            //    {
            //        sidSrcCriteria.ParentCustomerCode = commenCode;
            //        sidSrcCriteria.parentCustomerDescription = commenDescription;
            //    }
            //}
            else if (oOrgSrcCriteria.DetailType.Equals("repgroup"))
            {
                sidSrcCriteria.RepCode = commenCode;
                sidSrcCriteria.RepDescription = commenDescription;
            }
            else if (oOrgSrcCriteria.DetailType.Equals("rep"))
            {
                sidSrcCriteria.RepCode = commenCode;
                sidSrcCriteria.RepDescription = commenDescription;
            }
            //else if (oOrgSrcCriteria.DetailType.Equals("state"))
            //{
            //    sidSrcCriteria.State = commenCode;
            //}

            return sidSrcCriteria;
        }
        catch (Exception)
        {
            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetTodaySalesDrilldown(string sortText, string detailType, string type, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;
        //List<TargetInfoDTO> targetlist = new List<TargetInfoDTO>();
        ArgsDTO argsDTO = new ArgsDTO();
        //Session[CommonUtility.GLOBAL_SETTING] = argsDTO;
        if (Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            argsDTO = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
        }
        //if (!string.IsNullOrEmpty(rep))
        //    argsDTO.Originator = rep;
        //argsDTO.RepCode = "Q2";

        //argsDTO.CustomerCode = "";// "5WADANV0";
        //argsDTO.RepCode = "S2";
        //argsDTO.RepType = "F";

        if (sortText == "")
        {

            if (sortExpression != null && sortExpression.Count > 0)
            {
                argsDTO.OrderBy = "";
                foreach (Sort item in sortExpression)
                {
                    item.Field = GetSortTypeBusinessSales(item.Field, detailType);
                    if (string.IsNullOrEmpty(argsDTO.OrderBy))
                        argsDTO.OrderBy = item.ToExpression();
                    else
                        argsDTO.OrderBy = argsDTO.OrderBy + ", " + item.ToExpression();
                }
            }
            else
            {
                argsDTO.OrderBy = " name asc";
            }
        }
        else
        {
            argsDTO.OrderBy = GetSortTypeBusinessSales(sortText, detailType);
            argsDTO.OrderBy += " ASC";
            //if (sortText == "MC")
            //{
            //    argsDTO.OrderBy += " ASC";
            //}
            //else
            //{
            //    argsDTO.OrderBy += " DESC";
            //}
        }

        argsDTO.AdditionalParams = "";

        if (filter != null)
        {
            if (filter.Filters != null)
            {
                foreach (Filter item in filter.Filters)
                {
                    item.Field = GetSortTypeBusinessSales(item.Field, detailType);
                }
            }

            argsDTO.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        argsDTO.StartIndex = (skip / take) + 1;
        argsDTO.RowCount = take;

        Session[CommonUtility.GLOBAL_SETTING] = argsDTO;

        SalesInfoDetailSearchCriteriaDTO salesInfoDetSrcCritaria = (SalesInfoDetailSearchCriteriaDTO)Session[CommonUtility.SALES_INFO_DETAIL];
        OriginatorClient originatorService = new OriginatorClient();
        SalesClient salesClient = new SalesClient();
        SalesInfoDetailViewStateDTO oSalesInfoDetailViewState = new SalesInfoDetailViewStateDTO();
        //oSalesInfoDetailViewState = salesClient.GetSalesInfoDetailViewState(salesInfoDetSrcCritaria, argsDTO, "grid");

        argsDTO.Originator = UserSession.Instance.UserName;

        
        salesInfoDetSrcCritaria.cDisplayOption = type.ToCharArray()[0];
        

        try
        {
            oSalesInfoDetailViewState = originatorService.GetTodaySalesDrilldown(salesInfoDetSrcCritaria, argsDTO);
        }
        catch { }
        string sFlowContent = LoadViewState(oSalesInfoDetailViewState);

        //targetlist = HttpContext.Current.Session[CommonUtility.REP_TARGETS] as List<TargetInfoDTO>;

        //argsDTO.RepCode = salesInfoDetSrcCritaria.RepCode;
        //argsDTO.CustomerCode = salesInfoDetSrcCritaria.CustomerCode;
        //argsDTO.Brand = salesInfoDetSrcCritaria.Brand;

        //string sFlowContent = LoadViewState(argsDTO);

        //targetlist = originatorService.GetRepMonthleyTargetsByDistributor(UserSession.Instance.UserName, detailType,argsDTO);
        DataSourceResult dataSourceResult = new DataSourceResult();

        if (oSalesInfoDetailViewState.TargetInfoEntityList != null && oSalesInfoDetailViewState.TargetInfoEntityList.Count > 0)
        {
            oSalesInfoDetailViewState.TargetInfoEntityList.ForEach(x => x.FlowContent = sFlowContent);
            dataSourceResult.Data = oSalesInfoDetailViewState.TargetInfoEntityList;
            dataSourceResult.Total = oSalesInfoDetailViewState.TargetInfoEntityList[0].RowCount;
        }
        else
        {
            dataSourceResult.Data = oSalesInfoDetailViewState.TargetInfoEntityList;
            dataSourceResult.Total = 0;

        }
        //OpportunityService.Close();

        return dataSourceResult;

    }
    #endregion

    #region Contact Person

    //  [WebMethod(EnableSession = true)]
    //  [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    ////  public DataSourceResult LoadLeadAddressGrid(string custCode, int leadid)
    ////  public DataSourceResult LoadLeadAddressGrid()
    //  public String LoadLeadAddressGrid()
    //  {
    //      string custCode = "5WADANV0";
    //      int leadid = 0;

    //      LeadClient leadAddress = new LeadClient();
    //      CustomerClient oCustomer = new CustomerClient();
    //      List<LeadAddressDTO> addressList;
    //      //testing
    //      LeadAddressDTO leadaddress = new LeadAddressDTO();
    //      //testing
    //      List<LeadAddressDTO> addressList2 = new List<LeadAddressDTO> ();
    //      //testing
    //      DataSourceResult obj_dataSourceResult2 = null;
    //      //testing
    //      KeyValuePair<string, string> add = new KeyValuePair<string, string>("Address1", "7 HINKLER ROAD");

    //      DataSourceResult obj_dataSourceResult = null;
    //      try
    //      {
    //          if (leadid != null && int.Parse(leadid.ToString()) > 0)
    //          {
    //               addressList = leadAddress.GetLeadAddreses(int.Parse(leadid.ToString()));
    //               obj_dataSourceResult = new DataSourceResult();
    //               obj_dataSourceResult.Data = addressList;
    //     //         LoadAddress(addressList);
    //          }
    //          else if (custCode != null && !string.IsNullOrWhiteSpace(custCode.ToString()))
    //          {
    //               addressList = oCustomer.GetCustomerAddresses(custCode.ToString(), new ArgsDTO());
    //               obj_dataSourceResult = new DataSourceResult();
    //               obj_dataSourceResult.Data = addressList;
    //       //       LoadAddress(addressList);
    //          }

    //          //obj_dataSourceResult.Total = addressList[0].row;

    //      }
    //      catch (Exception oException)
    //      {
    //          obj_dataSourceResult = new DataSourceResult();
    //          obj_dataSourceResult.Data = new List<DataSourceResult>();
    //         /// obj_dataSourceResult.Total = 0;
    //      }
    //      //leadaddress.Address1 = "7 HINKLER ROAD";
    //     // addressList2.Add(leadaddress);
    //    //  string a = "{ text: \"Black\", value: \"1\"}";

    ////      obj_dataSourceResult2 = new DataSourceResult();
    //    //  obj_dataSourceResult2.Data = add;
    //  //    JavaScriptSerializer serializer = new JavaScriptSerializer();
    //   //  string jsonData = serializer.Serialize(a);
    //     // return a;

    //    //  return obj_dataSourceResult;
    //      JavaScriptSerializer serializer = new JavaScriptSerializer();
    //      string jsonData = serializer.Serialize(obj_dataSourceResult.Data);
    //      return jsonData;
    //  }

    [WebMethod]
    public List<LeadAddressDTO> GetAddressDropdownData(string custCode, string leadId)
    {
        LeadClient leadAddress = new LeadClient();
        CustomerClient oCustomer = new CustomerClient();

        List<LeadAddressDTO> addressList = null;


        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CustomerClient customerClient = new CustomerClient();
        // List<LeadAddressDTO> equipmentList = null;

        if (!string.IsNullOrEmpty(custCode) || !string.IsNullOrEmpty(leadId))
        {
            string sortby = string.Empty;
            ArgsDTO args;

            args = new ArgsDTO();

            args.StartIndex = 0;
            args.RowCount = 10;
            args.CustomerCode = custCode;
            args.LeadId = !string.IsNullOrEmpty(leadId) ? int.Parse(leadId) : 0;

            if (!string.IsNullOrEmpty(custCode))
                addressList = oCustomer.GetCustomerAddresses(custCode.ToString(), new ArgsDTO());
            else if (args.LeadId != 0)
                addressList = leadAddress.GetLeadAddreses(args.LeadId);

            else
            {

            }

        }
        return addressList;
    }

    #endregion


    #region Market Entry


    private string GetMarketSortString(string sorttype)
    {
        string sortStr = "market_id";

        switch (sorttype)
        {
            case "MarketId":
                sortStr = "market_id";
                break;
            case "MarketName":
                sortStr = "market_name";
                break;
            default:
                sortStr = "name";
                break;
        }

        return sortStr;
    }


    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllMarket(string type, string param, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        List<MarketDTO> marketList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        ArgsDTO argsDTO = new ArgsDTO();

        if (Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            argsDTO = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;

            if (sortExpression != null && sortExpression.Count > 0)
            {
                argsDTO.OrderBy = "";
                foreach (Sort item in sortExpression)
                {
                    item.Field = GetMarketSortString(item.Field);
                    if (string.IsNullOrEmpty(argsDTO.OrderBy))
                        argsDTO.OrderBy = item.ToExpression();
                    else
                        argsDTO.OrderBy = argsDTO.OrderBy + ", " + item.ToExpression();
                }
            }
            else
            {
                argsDTO.OrderBy = " market_id ASC ";
            }

            argsDTO.StartIndex = (skip / take) + 1;
            argsDTO.RowCount = take;
            argsDTO.AdditionalParams = string.Empty;

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "market", "");
                argsDTO.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            if (!string.IsNullOrEmpty(type))
            {
                switch (type.ToUpper())
                {
                    case "UNASSIGN_MARKETS":
                        {
                            string para = (filter != null) ? " AND originator IS NULL " : " originator IS NULL ";
                            argsDTO.AdditionalParams = argsDTO.AdditionalParams + para;
                            break;
                        }

                    default:
                        break;
                }
            }
        }

        CommonClient commonClient = new CommonClient();
        marketList = commonClient.GetAllMarket(argsDTO);

        DataSourceResult dataSourceResult = new DataSourceResult();

         if (marketList != null && marketList.Count > 0)
        {
            dataSourceResult.Data = marketList;
            dataSourceResult.Total = marketList[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = marketList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;

    }


    [WebMethod(EnableSession = true)]
    public DataSourceResult GetOriginatorMarkets(string type, string param, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        List<MarketDTO> marketList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        ArgsDTO argsDTO = new ArgsDTO();

        if (Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            argsDTO = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;

            if (sortExpression != null && sortExpression.Count > 0)
            {
                argsDTO.OrderBy = "";
                foreach (Sort item in sortExpression)
                {
                    item.Field = GetMarketSortString(item.Field);
                    if (string.IsNullOrEmpty(argsDTO.OrderBy))
                        argsDTO.OrderBy = item.ToExpression();
                    else
                        argsDTO.OrderBy = argsDTO.OrderBy + ", " + item.ToExpression();
                }
            }
            else
            {
                argsDTO.OrderBy = " market_id ASC ";
            }

            argsDTO.StartIndex = (skip / take) + 1;
            argsDTO.RowCount = take;
            argsDTO.AdditionalParams = string.Empty;

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "market", "");
                argsDTO.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            CommonClient commonClient = new CommonClient();

            if (!string.IsNullOrEmpty(type))
            {
                switch (type.ToUpper())
                {
                    case "ORIGINATOR_MARKETS":
                        {
                            if (!string.IsNullOrEmpty(param))
                            {
                                string originatorId = param.Trim();
                                string para = (filter != null) ? " AND originator = '" + originatorId + "'" : " originator = '" + originatorId + "'";
                                argsDTO.AdditionalParams = argsDTO.AdditionalParams + para;
                                marketList = commonClient.GetAllMarket(argsDTO);
                                Session[CommonUtility.ORIGINATOR_MARKETS] = marketList;
                            }
                            else
                            {
                                if (Session[CommonUtility.ORIGINATOR_MARKETS] != null)
                                {
                                    marketList = (List<MarketDTO>)Session[CommonUtility.ORIGINATOR_MARKETS];
                                }
                            }

                            break;
                        }

                    default:
                        break;
                }
            }
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (marketList != null && marketList.Count > 0)
        {
            dataSourceResult.Data = marketList;
            dataSourceResult.Total = marketList[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = marketList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;

    }

    



    #endregion


    #region Promotions

    [WebMethod]
    public DataSourceResult GetAllPromotions()
    {
        CommonClient commonclient = new CommonClient();


        List<PromotionDTO> promotionList = null;


        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        // List<LeadAddressDTO> equipmentList = null;
        promotionList = commonClient.GetAllPromotions();




        DataSourceResult dataSourceResult = new DataSourceResult();

        if (promotionList != null)
        {
            if (promotionList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = promotionList;
                //   dataSourceResult.Total = promotionList[0].rowCount;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = promotionList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = promotionList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;


    }
    #endregion

    #region Cheques

    /// <summary>
    /// added By: Milinda
    /// date: 24.04.2014
    /// Returns a list of payment settlements of type cheque and cheque status returned
    /// </summary>
    /// <param name="createdDate">payment settlement created Date</param>
    /// <param name="distributorAccountId">reference ID for distributor's account</param>
    /// <param name="skip"></param>
    /// <param name="take"></param>
    /// <param name="sort"></param>
    /// <param name="filter"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetChequesDataAndCount(DateTime createdDate, DateTime toDate, int distributorAccountId, string selectedChequeStatus, int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        string createdBy = "";
        createdBy = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        SalesClient salesClient =new CRMServiceReference.SalesClient();
        List<PaymentSettlementDTO> data = new List<PaymentSettlementDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            //args.RowCount = 10;
            args.RowCount = take;

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "created_by"));
            //args.ChildOriginators = UserSession.Instance.ChildOriginators;

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetReturnedChequesSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " cheque_number desc";
                }
            }
            else
            {
                args.OrderBy = " cheque_number desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "paymentsettlements");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }


            data = salesClient.GetCheques(createdBy, createdDate, toDate, distributorAccountId, selectedChequeStatus, args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    } 

    private static string GetReturnedChequesSortString(string sortby, string gridSortOrder)
    {
        string sortStr = "cheque_number desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "ChequeNumber":
                sortStr = " cheque_number " + sortoption;
                break;
            case "OutletBank":
                sortStr = " outlet_bank " + sortoption;
                break;
            case "Amount":
                sortStr = " amount " + sortoption;
                break;
            case "ChequeDate":
                sortStr = " cheque_date " + sortoption;
                break;
            default:
                sortStr = "cheque_number desc ";
                break;
        }

        return sortStr;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetDatedChequesDataAndCount(DateTime createdDate, DateTime toDate, int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        string createdBy = "";
        createdBy = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        SalesClient salesClient = new CRMServiceReference.SalesClient();
        List<PaymentSettlementDTO> data = new List<PaymentSettlementDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "created_by"));

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetReturnedChequesSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " cheque_number desc";
                }
            }
            else
            {
                args.OrderBy = " cheque_number desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "paymentsettlements");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }


            data = salesClient.GetDatedCheques(createdBy, createdDate, toDate, args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }    
    
    #endregion


    [WebMethod(EnableSession = true)]
    public List<OriginatorDTO> GetDistributorsByLikeName(string name)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> lstRouteDTO = new List<OriginatorDTO>();
        try
        {
            lstRouteDTO = originatorClient.GetDistributorsByLikeName(name);
            return lstRouteDTO;
        }
        catch (Exception)
        {
            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetDistributorTargetById(string DistributorId, string selectedMonth)
    {


        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        DistributorDTO targetDetail = new DistributorDTO();
        List<DistributorDTO> targetlist = new List<DistributorDTO>();
        ArgsDTO args = new ArgsDTO();

        args.OriginatorId = Convert.ToInt32(DistributorId);
        args.DtStartDate = Convert.ToDateTime(new DateTime(DateTime.Today.Year, Convert.ToInt32(selectedMonth) + 1, 1));
        //args.DtEndDate = Convert.ToDateTime(enddate);

        DataSourceResult dataSourceResult = new DataSourceResult();
        DistributorClient distributorClient = new DistributorClient();

        targetDetail = distributorClient.GetDistributorTargetById(args);
        targetlist.Add(targetDetail);

        dataSourceResult.Data = targetlist;
        dataSourceResult.Total = targetlist.Count;
        return dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetMarketTargetByDistributorId(string DistributorId, string selectedMonth)
    {

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        List<MarketDTO> targetlist = new List<MarketDTO>();
        ArgsDTO args = new ArgsDTO();

        args.OriginatorId = Convert.ToInt32(DistributorId);
        args.DtStartDate = Convert.ToDateTime(new DateTime(DateTime.Today.Year, Convert.ToInt32(selectedMonth) + 1, 1));
        //args.DtEndDate = Convert.ToDateTime(enddate);

        DataSourceResult dataSourceResult = new DataSourceResult();
        CommonClient commonClient = new CommonClient();

        targetlist = commonClient.GetMarketTargetsByDistributorId(args);

        dataSourceResult.Data = targetlist;
        dataSourceResult.Total = targetlist.Count;
        return dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetRouteTargetByDistributorId(string DistributorId, string selectedMonth)
    {

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        List<RouteMasterDTO> targetlist = new List<RouteMasterDTO>();
        ArgsDTO args = new ArgsDTO();

        args.OriginatorId = Convert.ToInt32(DistributorId);
        args.DtStartDate = Convert.ToDateTime(new DateTime(DateTime.Today.Year, Convert.ToInt32(selectedMonth) + 1, 1));
        //args.DtEndDate = Convert.ToDateTime(enddate);

        DataSourceResult dataSourceResult = new DataSourceResult();
        CallCycleClient callCycleClient = new CallCycleClient();

        targetlist = callCycleClient.GetRouteTargetsByDistributorId(args);

        dataSourceResult.Data = targetlist;
        dataSourceResult.Total = targetlist.Count;
        return dataSourceResult;
    }

    #region Market Objectives

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllMarketObjectives(string marketId, int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        string parentOriginator = "";
        parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        LeadCustomerClient leadCustomerClient = new LeadCustomerClient();

        List<ObjectiveDTO> data = new List<ObjectiveDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;
            args.ROriginator = UserSession.Instance.OriginatorString;
            args.Originator = UserSession.Instance.OriginalUserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "created_by"));

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllMarketObjectivesSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " description desc";
                }
            }
            else
            {
                args.OrderBy = " description desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "allmarketobjectives");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
            }

            data = leadCustomerClient.GetAllMarketObjectives(Convert.ToInt32(marketId), args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    private static string GetAllMarketObjectivesSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " description desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "Code":
                sortStr = " code " + sortoption;
                break;
            case "Description":
                sortStr = " description " + sortoption;
                break;
            default:
                sortStr = " description desc ";
                break;
        }

        return sortStr;
    }

    #endregion

    //[WebMethod(EnableSession = true)]
    //public DataSourceResult GetRepTargetsByDistributor()
    //{

    //    HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
    //    HttpContext.Current.Response.CacheControl = "no-cache";
    //    List<RepDTO> replist = new List<RepDTO>();

    //    DataSourceResult dataSourceResult = new DataSourceResult();
    //    OriginatorClient originatorClient = new OriginatorClient();

    //    replist = originatorClient.GetRepTargetsByDistributor(UserSession.Instance.UserName);

    //    dataSourceResult.Data = replist;
    //    dataSourceResult.Total = replist.Count;
    //    return dataSourceResult;
    //}

    #region Customer Targets
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetCustomerTargets(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        ArgsDTO argsDTO = new ArgsDTO();
        List<TargetInfoDTO> targetInfoDto = new List<TargetInfoDTO>();
        if (Session[CommonUtility.CUSTOMER_TARGETS] != null)
        {
            targetInfoDto = Session[CommonUtility.CUSTOMER_TARGETS] as List<TargetInfoDTO>;
        }

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (targetInfoDto != null && targetInfoDto.Count > 0)
        {

            dataSourceResult.Data = targetInfoDto;
            dataSourceResult.Total = targetInfoDto.Count;
        }
        else
        {
            dataSourceResult.Data = targetInfoDto;
            dataSourceResult.Total = 0;
        }
        //OpportunityService.Close();

        return dataSourceResult;

    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetCustomerTargetsDataAndCount(string routeMasterId, int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        string createdBy = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CustomerClient customerClient = new CRMServiceReference.CustomerClient();
        //SalesClient salesClient = new CRMServiceReference.SalesClient();
        List<CustomerDTO> data = new List<CustomerDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            //args.RowCount = 1;
            args.RowCount = take;

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetCustomerTargetsSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " name desc";
                }
            }
            else
            {
                args.OrderBy = " name desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "customertargets");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            data = customerClient.GetCustomerTargetsByRouteId(Convert.ToInt32(routeMasterId), args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    private static string GetCustomerTargetsSortString(string sortby, string gridSortOrder)
    {
        string sortStr = "name desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "Name":
                sortStr = " name " + sortoption;
                break;
            case "StickTarget":
                sortStr = " stick_target " + sortoption;
                break;
            default:
                sortStr = "name desc ";
                break;
        }

        return sortStr;
    }

    #endregion

    #region - Program -

    [WebMethod(EnableSession = true)]
    public List<ProgramTargetsDTO> GetProgramTargetsByProgram(string name)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CommonClient commonClient = new CommonClient();
        List<ProgramTargetsDTO> lstProgramTargetsDTO = new List<ProgramTargetsDTO>();

        try
        {
            lstProgramTargetsDTO = commonClient.GetProgramTargetsByProgram(name, 0, 0);
            return lstProgramTargetsDTO;
        }
        catch (Exception)
        {

            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllPrograms(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        ArgsDTO argsDTO = new ArgsDTO();

        argsDTO.Originator = UserSession.Instance.UserName;
        argsDTO.ChildOriginators = UserSession.Instance.ChildOriginators;
        argsDTO.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        //if (!String.IsNullOrEmpty(UserSession.Instance.RepCode))
        //    argsDTO.ChildOriginators = "(" + argsDTO.ChildOriginators + " OR c.rep_code =\'" + UserSession.Instance.RepCode + "\')";
        argsDTO.AdditionalParams = "";

        if (sortExpression != null && sortExpression.Count > 0)
        {
            argsDTO.OrderBy = "";
            foreach (Sort item in sortExpression)
            {
                item.Field = GetSortTypeProgram(item.Field);
                if (string.IsNullOrEmpty(argsDTO.OrderBy))
                    argsDTO.OrderBy = item.ToExpression();
                else
                    argsDTO.OrderBy = argsDTO.OrderBy + ", " + item.ToExpression();
            }
        }
        else
        {
            argsDTO.OrderBy = "name ASC";
        }
        argsDTO.StartIndex = (skip / take) + 1;
        argsDTO.RowCount = take;

        List<ProgramDTO> listProgram = new List<ProgramDTO>();


        listProgram = commonClient.GetAllProgram(argsDTO);

        

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (listProgram != null && listProgram.Count > 0)
        {
            dataSourceResult.Data = listProgram;
            dataSourceResult.Total = listProgram[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = listProgram;
            dataSourceResult.Total = 0;

        }
        //OpportunityService.Close();

        return dataSourceResult;

    }
    
    private string GetSortTypeProgram(string sortName)
    {
        string sortingType = "";

        try
        {
            if (sortName == "ProgramId")
            {
                sortingType = "program_id";
            }
            else if (sortName == "ProgramName")
            {
                sortingType = "name";
            }
            


            return sortingType;
        }
        catch (Exception)
        {

            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public List<ProductDTO> GetProductsByName(string name)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CommonClient commonClient = new CommonClient();
        List<ProductDTO> lstProductsList = new List<ProductDTO>();

        try
        {
            lstProductsList = commonClient.GetProductsByName(name);
            return lstProductsList;
        }
        catch (Exception)
        {

            return null;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string LoadSelectedProgramTargetDetails(string programTargetId)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        CommonClient commonClient = new CommonClient();

        List<ProgramTargetsDTO> programTargetsDTOList = new List<ProgramTargetsDTO>();
        ProgramTargetsDTO programTargetsDTO = new ProgramTargetsDTO();

        string returnvalue = string.Empty;

        try
        {
            programTargetsDTOList = commonClient.GetProgramTargetsByProgram(null, Convert.ToInt32(programTargetId),0);
            if (programTargetsDTOList.Count > 0)
                programTargetsDTO = programTargetsDTOList[0];
        }
        catch (Exception)
        {
            returnvalue = "error";
        }
        return serializer.Serialize(programTargetsDTO);
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string LoadSelectedProgramTargetDetailsByProgramId(string programId)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        CommonClient commonClient = new CommonClient();

        List<ProgramTargetsDTO> programTargetsDTOList = new List<ProgramTargetsDTO>();
        ProgramTargetsDTO programTargetsDTO = new ProgramTargetsDTO();

        string returnvalue = string.Empty;

        try
        {
            programTargetsDTOList = commonClient.GetProgramTargetsByProgram(null, 0, Convert.ToInt32(programId));
            if (programTargetsDTOList.Count > 0)
                programTargetsDTO = programTargetsDTOList[0];
        }
        catch (Exception)
        {
            returnvalue = "error";
        }
        return serializer.Serialize(programTargetsDTO);
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetProgramTargetsDateAndCount(int programId, DateTime fromDate, DateTime toDate, int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        string createdBy = "";
        createdBy = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CommonClient commonClient = new CommonClient();
        List<ProgramTargetsDTO> data = new List<ProgramTargetsDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "p.created_by"));

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetProgramTargetsSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " name desc";
                }
            }
            else
            {
                args.OrderBy = " name desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "programtargets");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
            }


            data = commonClient.GetProgramTargets(programId, fromDate, toDate, args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    private static string GetProgramTargetsSortString(string sortby, string gridSortOrder)
    {
        string sortStr = "name desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "Program.Name":
                sortStr = " name " + sortoption;
                break;
            case "EffStartDate":
                sortStr = " eff_start_date " + sortoption;
                break;
            case "EffEndDate":
                sortStr = " eff_end_date " + sortoption;
                break;
            default:
                sortStr = "name desc ";
                break;
        }

        return sortStr;
    }

    #endregion

    [WebMethod(EnableSession = true)]
    public List<RouteDTO> GetRouteRepLikeRouteNameForCustomer(string name, string isTemp)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CommonClient commonClient = new CommonClient();
        List<RouteDTO> lstRouteDTO = new List<RouteDTO>();
        List<RouteDTO> returnList = new List<RouteDTO>();
        string originator = UserSession.Instance.UserName;
        string chlidOriginators = UserSession.Instance.ChildOriginators;


        try
        {
            int iIsTemp = Convert.ToInt32(isTemp);

            lstRouteDTO = commonClient.GetRouteRepLikeRouteNameForCustomer(name, iIsTemp, originator, chlidOriginators);
            foreach (RouteDTO item in lstRouteDTO)
            {
                returnList.Add(item);
            }
            return returnList;
        }
        catch (Exception)
        {

            return null;
        }
    }


    #region Invoice Header

    private string GetRepsString(string strASE)
    {
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> repList = new List<OriginatorDTO>();
        string repString = "";

        if (strASE == "ALL")
        {
            repString = "";
        }
        else
        {
            repString = " ih.rep_code IN (";

            repList = originatorClient.GetRepsByASEOriginator(strASE);

            foreach (var item in repList)
            {
                repString = repString + "'" + item.RepCode.ToString() +"',";
            }

            repString = repString.Remove(repString.Length - 1, 1);//Removing Last Comma
            repString = repString + ") ";
        }

        //if (repsList != null)
        //{
        //    if (repsList.Count > 0)
        //    {
        //        repString = " originator IN (";
        //        foreach (ListItem item in repsList)
        //        {
        //            repString = repString + "'" + item.Value + "',";
        //        }
        //        repString = repString.Replace("'ALL',", "'',");
        //        repString = repString.Remove(repString.Length - 1, 1);//Removing Last Comma
        //        repString = repString + ") ";
        //    }
        //    else
        //    {
        //        repString = " originator IN ( ' ' ) ";
        //    }
        //}
        //else
        //{
        //    repString = " originator IN ( ' ' ) ";
        //}

        return repString;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllInvoicesDataAndCount(string startDate, string endDate, string aseVal, string quantityType, int skip, int take, IEnumerable<Sort> sort, Filter filter)
    {
        string parentOriginator = "";
        parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        //LeadCustomerClient leadCustomerClient = new LeadCustomerClient();

        List<InvoiceHeaderDTO> data = new List<InvoiceHeaderDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            //args.StartIndex = 0;
            args.RowCount = take;
            args.ROriginator = UserSession.Instance.OriginatorString;
            args.Originator = UserSession.Instance.OriginalUserName;

            args.AdditionalParams = GetRepsString(aseVal);
            args.SStartDate = startDate;
            args.SEndDate = endDate;
            args.SQuantitytype = quantityType;

            args.StartIndex = (skip / take) + 1;
            

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetInvoicesSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " invoice_no desc";
                }
            }
            else
            {
                args.OrderBy = " invoice_no desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "invoices");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
            }

            data = commonClient.GetAllInvoicesDataAndCount(args);
            obj_dataSourceResult = new DataSourceResult();

            if (data != null && data.Count > 0)
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = data[0].rowCount;
            }
            else
            {
                obj_dataSourceResult.Data = new List<InvoiceHeaderDTO>();
                obj_dataSourceResult.Total = 0;
            }

            
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<InvoiceHeaderDTO>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllOrderDataAndCount(string startDate, string endDate, string aseVal, string repVal, string status, string quantityType, int skip, int take, IEnumerable<Sort> sort, Filter filter)
    {
        OrderClient orderClient = new OrderClient();

        string parentOriginator = "";
        parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<OrderModel> data = new List<OrderModel>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsModel args = new ArgsModel();
            args.RowCount = take;
            args.AdditionalParams = GetRepsStringForOrder(aseVal, repVal);
            args.SStartDate = startDate;
            args.SEndDate = endDate;
            args.SQuantitytype = quantityType;
            args.StartIndex = (skip / take) + 1;

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetorderSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " order_no desc";
                }
            }
            else
            {
                args.OrderBy = " order_no desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "ordersview");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
            }

            data = orderClient.GetAllOrders(args, UserSession.Instance.OriginalUserName, status);
            obj_dataSourceResult = new DataSourceResult();

            if (data != null && data.Count > 0)
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = data[0].RecordCount;
            }
            else
            {
                obj_dataSourceResult.Data = new List<InvoiceHeaderDTO>();
                obj_dataSourceResult.Total = 0;
            }
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<InvoiceHeaderDTO>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    private static string GetInvoicesSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " invoice_no desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "InvoiceNo":
                sortStr = " invoice_no " + sortoption;
                break;
            case "CustCode":
                sortStr = " cust_code " + sortoption;
                break;
            case "InvoiceDate":
                sortStr = " invoice_date " + sortoption;
                break;
            case "GrossTotal":
                sortStr = " gross_total " + sortoption;
                break;

            case "Discount":
                sortStr = " discount_total " + sortoption;
                break;
            case "ReturnsTotal":
                sortStr = " returns_total " + sortoption;
                break;
            case "GrandTotal":
                sortStr = " grand_total " + sortoption;
                break;
            case "RepCode":
                sortStr = " rep_code " + sortoption;
                break;
            case "CustomerName":
                sortStr = " name " + sortoption;
                break;
            case "Status":
                sortStr = " status " + sortoption;
                break;
            default:
                sortStr = " invoice_no desc ";
                break;
        }

        return sortStr;
    }

    private string GetRepsStringForOrder(string strASE, string strRep)
    {
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> repList = new List<OriginatorDTO>();
        string repString = "";

        if (strRep == "ALL")
        {
            if (strASE == "ALL")
            {
                repString = "";
            }
            else
            {
                repString = " oh.rep_code IN (";

                repList = originatorClient.GetRepsByASEOriginator(strASE);

                foreach (var item in repList)
                {
                    repString = repString + "'" + item.RepCode.ToString() + "',";
                }

                repString = repString.Remove(repString.Length - 1, 1);//Removing Last Comma
                repString = repString + ") ";
            }
        }
        else
        {
            repString = " oh.rep_code IN ('" + strRep + "') ";
        }

        return repString;
    }

    private static string GetorderSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " order_no desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "OrderNo":
                sortStr = " order_no " + sortoption;
                break;
            case "CustCode":
                sortStr = " cust_code " + sortoption;
                break;
            case "OrderDate":
                sortStr = " order_date " + sortoption;
                break;
            case "GrossTotal":
                sortStr = " gross_total " + sortoption;
                break;
            case "DayCount":
                sortStr = " day_count " + sortoption;
                break;
            case "Discount":
                sortStr = " discount_total " + sortoption;
                break;
            case "ReturnsTotal":
                sortStr = " returns_total " + sortoption;
                break;
            case "GrandTotal":
                sortStr = " grand_total " + sortoption;
                break;
            case "RepCode":
                sortStr = " rep_code " + sortoption;
                break;
            case "CustomerName":
                sortStr = " name " + sortoption;
                break;
            case "Status":
                sortStr = " status " + sortoption;
                break;
            default:
                sortStr = " order_no desc ";
                break;
        }

        return sortStr;
    }

    #endregion


    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string SaveProductImageAndGetImageName(string productId)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        CommonClient commonClient = new CommonClient();
        ProductDTO productDTO = new ProductDTO();

        string returnvalue = string.Empty;

        try
        {
            productDTO = commonClient.GetProductById(Convert.ToInt32(productId));
            if (productDTO.ImageContent != null)
            {
                string new_tempFileName = productDTO.Code;
                string new_path = ConfigUtil.ApplicationPath + "docs/product_images/" + new_tempFileName + ".jpg";

                //Create Document
                if (!isFileOpen(Server.MapPath(new_path)))
                {
                    FileStream fs = new FileStream(Server.MapPath(new_path), FileMode.Create, FileAccess.ReadWrite);
                    BinaryWriter bw = new BinaryWriter(fs);
                    bw.Write(productDTO.ImageContent);
                    bw.Close();
                    fs.Close();
                }

                //To avoid Unnessesary Serialization of a Lengthy String
                productDTO.ImageContent = null;
                productDTO.ImageName = new_tempFileName + ".jpg";
                productDTO.ImagePath = new_path;
            }
        }
        catch (Exception)
        {
            returnvalue = "error";
        }
        return serializer.Serialize(productDTO);
    }

    private bool isFileOpen(string file)
    {
        try
        {
            //first we open the file with a FileStream
            using (FileStream stream = new FileStream(file, FileMode.OpenOrCreate, FileAccess.Read, FileShare.None))
            {
                try
                {
                    stream.ReadByte();
                    return false;
                }
                catch (IOException)
                {
                    return true;
                }
                finally
                {
                    stream.Close();
                    stream.Dispose();
                }
            }

        }
        catch (IOException)
        {
            return true;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string SaveCheckListImageAndGetImageName(string CheckListId)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        CommonClient commonClient = new CommonClient();
        List<ChecklistImageDTO> checklistDTO = new List<ChecklistImageDTO>();

        string returnvalue = string.Empty;

        try
        {
            checklistDTO = commonClient.GetChecklistImageById(Convert.ToInt32(CheckListId));
            if (checklistDTO[0].ImageContent != null)
            {
                string new_tempFileName = checklistDTO[0].ImageName;
                string new_path = ConfigUtil.ApplicationPath + "docs/checklist_images/" + new_tempFileName + ".jpg";

                //Create Document
                if (!isFileOpen(Server.MapPath(new_path)))
                {
                    FileStream fs = new FileStream(Server.MapPath(new_path), FileMode.Create, FileAccess.ReadWrite);
                    BinaryWriter bw = new BinaryWriter(fs);
                    bw.Write(checklistDTO[0].ImageContent);
                    bw.Close();
                    fs.Close();
                }
                //To avoid Unnessesary Serialization of a Lengthy String
                checklistDTO[0].ImageContent = null;

                checklistDTO[0].ImageName = new_tempFileName + ".jpg";
                checklistDTO[0].ImagePath = new_path;
            }
            
            return serializer.Serialize(checklistDTO[0]);
        }
        catch (Exception)
        {
            return returnvalue = "error";
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string SaveTargetAndIncentiveNew(string TargetBreakDay, string First14Days, string Last14Days)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        CommonClient commonClient = new CommonClient();

        string returnvalue = string.Empty;

        try
        {
            if (!commonClient.SaveTargetAndIncentiveNew(TargetBreakDay, First14Days, Last14Days))
            {
                return "error";
            }

            return "success";
        }
        catch (Exception)
        {
            return "error";
        }
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllUsersByDeptString(string deptString, int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        OriginatorClient originatorClient = new OriginatorClient();
        ArgsDTO args = new ArgsDTO();
        List<OriginatorDTO> data = new List<OriginatorDTO>();
        DataSourceResult obj_dataSourceResult = null;

        try
        {
            args.StartIndex = 0;
            args.RowCount = take;

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            if (UserSession.Instance != null)
                args.ChildOriginators = UserSession.Instance.ChildOriginators;            

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllUsersSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " name desc";
                }
            }
            else
            {
                args.OrderBy = " name desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "getallusersbydeptstring");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            if (String.IsNullOrEmpty(args.AdditionalParams))
            {
                args.AdditionalParams = " dept_string = '" + deptString + "'";
            }
            else
            {
                args.AdditionalParams = args.AdditionalParams + " AND dept_string = '" + deptString + "'";
            }

            data = originatorClient.GetOriginatorsByCatergory(args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }

        return obj_dataSourceResult;
    }
    
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllEMEIByOriginator(string originator, string deptString, int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        OriginatorClient originatorClient = new OriginatorClient();
        ArgsDTO args = new ArgsDTO();
        List<OriginatorEMEIDTO> data = new List<OriginatorEMEIDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            args.StartIndex = 0;
            args.RowCount = take;

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            if (UserSession.Instance != null)
                args.ChildOriginators = UserSession.Instance.ChildOriginators;            

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllUsersSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " name desc";
                }
            }
            else
            {
                args.OrderBy = " name desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "getallusersbydeptstring");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            if (String.IsNullOrEmpty(args.AdditionalParams))
            {
                args.AdditionalParams = " dept_string = '" + deptString + "'";
            }
            else
            {
                args.AdditionalParams = args.AdditionalParams + " AND dept_string = '" + deptString + "'";
            }

            data = originatorClient.GetAllOriginatorEMEINos(originator);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data.Count;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    private static string GetAllUsersSortString(string sortby, string gridSortOrder)
    {
        string sortStr = "name desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "Distributor":
                sortStr = " name " + sortoption;
                break;
            default:
                sortStr = "name desc ";
                break;
        }

        return sortStr;
    }

    #region - Rep Target -

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllRepTargetsDataAndCount(string Originator, string OriginatorType, int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CommonClient commonClient = new CommonClient();
        List<RepTargetDTO> data = new List<RepTargetDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "r.originator"));
            //args.ChildOriginators = UserSession.Instance.ChildOriginators;

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllRepTargetsSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " name desc";
                }
            }
            else
            {
                args.OrderBy = " name desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "reptargets");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            //int divisId = String.IsNullOrEmpty(divisionId) ? 0 : Convert.ToInt32(divisionId);

            data = commonClient.GetAllRepTargets(Originator, OriginatorType, args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    private static string GetAllRepTargetsSortString(string sortby, string gridSortOrder)
    {
        string sortStr = "name desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "RepCode":
                sortStr = " rep_code " + sortoption;
                break;
            case "RepName":
                sortStr = " name " + sortoption;
                break;
            case "PrimaryTarAmt":
                sortStr = " primary_tar_amt " + sortoption;
                break;
            case "PrimaryIncAmt":
                sortStr = " primary_inc_amt " + sortoption;
                break;
            default:
                sortStr = "name desc ";
                break;
        }

        return sortStr;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string SaveRepTarget(string repTargetId, string repCode, string target, string incentive)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CommonClient commonClient = new CommonClient();

        RepTargetDTO repTargetDTO = new RepTargetDTO();

        repTargetDTO.RepTargetId = String.IsNullOrEmpty(repTargetId)? 0 : Convert.ToInt32(repTargetId);
        repTargetDTO.RepCode = repCode;
        repTargetDTO.PrimaryTarAmt = String.IsNullOrEmpty(target) ? 0.0 : Convert.ToDouble(target);
        repTargetDTO.PrimaryIncAmt = String.IsNullOrEmpty(incentive) ? 0.0 : Convert.ToDouble(incentive);

        repTargetDTO.CreatedBy = UserSession.Instance.UserName;
        repTargetDTO.LastModifiedBy = UserSession.Instance.UserName;

        string returnvalue = string.Empty;
        try
        {
            bool status = false;
            List<RepTargetDTO> repTargetList = new List<RepTargetDTO>();
            repTargetList.Add(repTargetDTO);
            status = commonClient.SaveRepTarget(repTargetList);

            if (status)
            {
                returnvalue = "true";
            }
            else
            {
                returnvalue = "false";
            }
        }
        catch (Exception)
        {
            returnvalue = "error";
        }
        return returnvalue;
    }


    #endregion

    #region - Incentive Plan -

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllIncentivePlansForLookup(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CommonClient commonClient = new CommonClient();
        ArgsDTO args = new ArgsDTO();
        List<IncentivePlanHeaderDTO> data = new List<IncentivePlanHeaderDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            args.StartIndex = 0;
            args.RowCount = take;

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            if (UserSession.Instance != null)
                args.ChildOriginators = UserSession.Instance.ChildOriginators;

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllIncentivePlanSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " start_date ASC";
                }
            }
            else
            {
                args.OrderBy = " start_date ASC";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "getallincentiveplan");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            data = commonClient.GetAllIncentivePlans(args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    private static string GetAllIncentivePlanSortString(string sortby, string gridSortOrder)
    {
        string sortStr = "plan_name desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "PlanName":
                sortStr = " plan_name " + sortoption;
                break;
            case "Description":
                sortStr = " description " + sortoption;
                break;
            case "DivisionName":
                sortStr = " division_name " + sortoption;
                break;
            default:
                sortStr = "plan_name desc ";
                break;
        }

        return sortStr;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetWeeklyTargetIncentives(string selectedHeaderId, int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CommonClient commonClient = new CommonClient();
        ArgsDTO args = new ArgsDTO();
        List<IncentiveSecondarySalesWeeklyDTO> data = new List<IncentiveSecondarySalesWeeklyDTO>();
        List<IncentivePlanHeaderDTO> headerList = new List<IncentivePlanHeaderDTO>();
        List<IncentivePlanDetailsDTO> detailList = new List<IncentivePlanDetailsDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            args.StartIndex = 0;
            args.RowCount = take;

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            if (UserSession.Instance != null)
                args.ChildOriginators = UserSession.Instance.ChildOriginators;

            List<Sort> sortExpression = (List<Sort>)sort;

            args.OrderBy = " plan_name ASC";

            args.AdditionalParams = "";

            headerList = commonClient.GetAllIncentivePlans(args);
            int sHeaderId = String.IsNullOrEmpty(selectedHeaderId) ? 0 : Convert.ToInt32(selectedHeaderId);
            detailList = (headerList.FirstOrDefault(a => a.IncentivePlanHeaderId == sHeaderId)).IncentivePlanDetailsList;
            if(detailList != null)
            {
                if (detailList.Count > 0)
                    data = detailList[0].IncentiveSecondarySalesWeeklyList;
                else
                    data = new List<IncentiveSecondarySalesWeeklyDTO>();
            }
            else
            {
                data = new List<IncentiveSecondarySalesWeeklyDTO>();
            }

            Session["SELECTED_DEATIL_ID"] = detailList.Count > 0 ? detailList[0].IncentivePlanDetailsId : 0;
                        
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data.OrderBy(a=>a.CumTarPerc);
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetPartialTargetIncentives(string selectedHeaderId, int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CommonClient commonClient = new CommonClient();
        ArgsDTO args = new ArgsDTO();
        List<IncentiveSecondarySalesPartialDTO> data = new List<IncentiveSecondarySalesPartialDTO>();
        List<IncentivePlanHeaderDTO> headerList = new List<IncentivePlanHeaderDTO>();
        List<IncentivePlanDetailsDTO> detailList = new List<IncentivePlanDetailsDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            args.StartIndex = 0;
            args.RowCount = take;

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            if (UserSession.Instance != null)
                args.ChildOriginators = UserSession.Instance.ChildOriginators;

            List<Sort> sortExpression = (List<Sort>)sort;

            args.OrderBy = " plan_name ASC";

            args.AdditionalParams = "";

            headerList = commonClient.GetAllIncentivePlans(args);
            int sHeaderId = String.IsNullOrEmpty(selectedHeaderId) ? 0 : Convert.ToInt32(selectedHeaderId);
            detailList = (headerList.FirstOrDefault(a => a.IncentivePlanHeaderId == sHeaderId)).IncentivePlanDetailsList;
            if (detailList != null)
            {
                if (detailList.Count > 0)
                    data = detailList[0].IncentiveSecondarySalesPartialList;
                else
                    data = new List<IncentiveSecondarySalesPartialDTO>();
            }
            else
            {
                data = new List<IncentiveSecondarySalesPartialDTO>();
            }

            Session["SELECTED_DEATIL_ID"] = detailList.Count > 0 ? detailList[0].IncentivePlanDetailsId : 0;

            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data.OrderBy(a => a.AchievementPerc);
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string DeactivateSecondarySalesWeeklyRecord(string weeklyId)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CommonClient commonClient = new CommonClient();

        IncentiveSecondarySalesWeeklyDTO weeklyDTO = new IncentiveSecondarySalesWeeklyDTO();

        weeklyDTO.SecondarySalesWeeklyId = String.IsNullOrEmpty(weeklyId) ? 0 : Convert.ToInt32(weeklyId);
        weeklyDTO.CreatedBy = UserSession.Instance.UserName;
        weeklyDTO.LastModifiedBy = UserSession.Instance.UserName;

        string returnvalue = string.Empty;
        try
        {
            bool status = false;
            status = commonClient.DeactivateSecondarySalesWeekly(weeklyDTO);

            if (status)
            {
                returnvalue = "true";
            }
            else
            {
                returnvalue = "false";
            }
        }
        catch (Exception)
        {
            returnvalue = "error";
        }
        return returnvalue;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string DeactivateSecondarySalesPartialRecord(string partialId)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CommonClient commonClient = new CommonClient();

        IncentiveSecondarySalesPartialDTO partialDTO = new IncentiveSecondarySalesPartialDTO();

        partialDTO.SecondarySalesPartialId = String.IsNullOrEmpty(partialId) ? 0 : Convert.ToInt32(partialId);
        partialDTO.CreatedBy = UserSession.Instance.UserName;
        partialDTO.LastModifiedBy = UserSession.Instance.UserName;

        string returnvalue = string.Empty;
        try
        {
            bool status = false;
            status = commonClient.DeactivateSecondarySalesPartial(partialDTO);

            if (status)
            {
                returnvalue = "true";
            }
            else
            {
                returnvalue = "false";
            }
        }
        catch (Exception)
        {
            returnvalue = "error";
        }
        return returnvalue;
    }

    #endregion

    [WebMethod]
    public DataSourceResult GetAllArea(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        CommonClient commonclient = new CommonClient();
        List<CityAreaDTO> areaList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsDTO args = new ArgsDTO();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetAllAreaSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " area_id asc";
            }
        }
        else
        {
            args.OrderBy = " area_id asc";
        }


        // List<LeadAddressDTO> equipmentList = null;
        areaList = commonClient.GetAllCityAreas(args);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (areaList != null)
        {
            if (areaList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = areaList;
                dataSourceResult.Total = areaList[0].rowCount;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = areaList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = areaList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    private static string GetAllAreaSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " area_id asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "AreaID":
                sortStr = " area_id " + sortoption;
                break;
            case "AreaCode":
                sortStr = " area_name " + sortoption;
                break;
            default:
                sortStr = " area_id asc ";
                break;
        }

        return sortStr;
    }

    [WebMethod]
    public DataSourceResult GetAllOutletTypes()
    {
        CommonClient commonclient = new CommonClient();
        List<OutletTypeDTO> outlettypeList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        // List<LeadAddressDTO> equipmentList = null;
        outlettypeList = commonClient.GetAllOutletType();
        DataSourceResult dataSourceResult = new DataSourceResult();

        if (outlettypeList != null)
        {
            if (outlettypeList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = outlettypeList;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = outlettypeList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = outlettypeList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GetAdminPassword(string password)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        CommonClient commonClient = new CommonClient();

        bool status = false;
        string returnvalue = string.Empty;

        try
        {
            status = commonClient.GetAdminPassword(password);
        }
        catch (Exception)
        {
            returnvalue = "error";
        }

        return serializer.Serialize(status);
    }

    [WebMethod(EnableSession = true)]
    public List<SalesDataEntity> getSalesData(string UserName, string duration, string IsASMOrProduct, int ProductId)
    {

        CommonClient commonClient = new CommonClient();
        SalesTrendDAO productDTO = new SalesTrendDAO();

        ArgsDTO args = new ArgsDTO();
        args.AdditionalParams = duration;
        if (UserName == "")
            args.Originator = UserSession.Instance.UserName;
        else
            args.Originator = UserName;

        List<SalesDataEntity> List = new List<SalesDataEntity>();

        List = commonClient.GetAllSales(args, IsASMOrProduct, ProductId);

        return List;
    }


    [WebMethod(EnableSession = true)]
    public List<SalesDataEntity> getASMSalesData(string UserName, string duration)
    {

        CommonClient commonClient = new CommonClient();
        SalesTrendDAO productDTO = new SalesTrendDAO();

        ArgsDTO args = new ArgsDTO();
        args.AdditionalParams = duration;
        if (UserName == "")
            args.Originator = UserSession.Instance.UserName;
        else
            args.Originator = UserName;

        List<SalesDataEntity> List = new List<SalesDataEntity>();

        List = commonClient.getASMSalesData(args);

        return List;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllRepSales(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string date, string ASM)
    {
        //string parentOriginator = "";
        //parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        //SalesClient salesClient = new CRMServiceReference.SalesClient();
        CommonClient commonClient = new CommonClient();

        List<SalesDataEntity> data = new List<SalesDataEntity>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;
            args.ROriginator = UserSession.Instance.OriginatorString;
            if (ASM == null || ASM == "")
                args.Originator = UserSession.Instance.OriginalUserName;
            else
                args.Originator = ASM;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.SStartDate = date;
            //args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "created_by"));

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllRepSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " description desc";
                }
            }
            else
            {
                args.OrderBy = " description desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "allproductcategorydata");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            data = commonClient.GetRepSales(args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            //obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    private static string GetAllRepSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " description desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "CategoryCode":
                sortStr = " category_code " + sortoption;
                break;
            case "CategoryName":
                sortStr = " description " + sortoption;
                break;
            default:
                sortStr = " description desc ";
                break;
        }

        return sortStr;
    }

    private static string GetAllUserAccountSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " originator asc";
        string sortoption = "asc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "Originator":
                sortStr = " originator " + sortoption;
                break;
            case "UserName":
                sortStr = " name " + sortoption;
                break;
            case "designation":
                sortStr = " designation " + sortoption;
                break;
            case "type_des":
                sortStr = " type_des " + sortoption;
                break;
            case "Mobile":
                sortStr = " otp_mobile " + sortoption;
                break;
            default:
                sortStr = " originator asc ";
                break;
        }

        return sortStr;
    }

    //Load all Check List Details
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllCheckListsDataAndCount(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        CommonClient commonClient = new CommonClient();
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<CheckListDTO> data = new List<CheckListDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;
            args.ROriginator = UserSession.Instance.OriginatorString;
            args.Originator = UserSession.Instance.OriginalUserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.AdditionalParams = "";
            //args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "created_by"));

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllCheckListSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " task asc";
                }
            }
            else
            {
                args.OrderBy = " task asc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "allchecklistdata");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            data = commonClient.GetAllCheckListDataAndCount(args);

            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].RowCount1;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    private static string GetAllCheckListSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " name desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "Code":
                sortStr = " code " + sortoption;
                break;
            case "Name":
                sortStr = " name " + sortoption;
                break;
            case "FgCode":
                sortStr = " fg_code " + sortoption;
                break;
            case "CategoryName":
                sortStr = " category " + sortoption;
                break;
            case "BrandName":
                sortStr = " brand_name " + sortoption;
                break;
            default:
                sortStr = " name desc ";
                break;
        }

        return sortStr;
    }

    public bool UpdateCheckListCustomer(string CheckListId, string CustomerCode)
    {
        return true;
    }

    //for originator details
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetOriginators(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";


        CommonClient commonClient = new CommonClient();

        List<OriginatorEntity> data = new List<OriginatorEntity>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;
            args.ROriginator = UserSession.Instance.OriginatorString;
            args.Originator = UserSession.Instance.OriginalUserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllUserAccountSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " originator desc";
                }
            }
            else
            {
                args.OrderBy = " originator desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "getallusersbydeptstring");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            data = commonClient.GetOriginators(args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;

            obj_dataSourceResult.Total = data[0].totalCount;

        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    //for originator details
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetOriginatorsfor2FA(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";


        CommonClient commonClient = new CommonClient();

        List<OriginatorEntity> data = new List<OriginatorEntity>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;
            args.ROriginator = UserSession.Instance.OriginatorString;
            args.Originator = UserSession.Instance.OriginalUserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;



            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }



            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "getallusersbydeptstring");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            data = commonClient.GetOriginatorsFor2FA(args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;

            obj_dataSourceResult.Total = data[0].totalCount;

        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel UpdateIsTFAUser(string originator, string tfa_user, string is_checked)
    {
        ArgsModel args = new ArgsModel();
        CommonClient cClient = new CommonClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        bool statusOut = cClient.UpdateIsTFAUser(tfa_user, Convert.ToBoolean(is_checked));
        if (statusOut)
        {
            response.Status = true;
            response.StatusCode = "Success";

            try
            {
                if (is_checked == "true")
                {
                    cClient.CreateTransactionLog(originator,
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        TransactionTypeModules.Assign,
                        TransactionModules.UserTFA,
                        originator + " assign TFA to user " + originator);
                }
                else if (is_checked == "false")
                {
                    cClient.CreateTransactionLog(originator,
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        TransactionTypeModules.Assign,
                        TransactionModules.UserTFA,
                        originator + " unassign TFA from user " + originator);
                }
            }
            catch (Exception ex) { }
        }
        else
        {
            response.Status = false;
            response.StatusCode = "Error";
        }

        return response;
    }

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel UpdateIsTFAAuth(string originator, string tfa_user, string is_checked)
    {
        ArgsModel args = new ArgsModel();
        CommonClient cClient = new CommonClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        bool statusOut = cClient.UpdateIsTFAAuth(tfa_user, Convert.ToBoolean(is_checked));

        if (statusOut)
        {
            response.Status = true;
            response.StatusCode = "Success";

            try
            {
                if (is_checked == "true")
                {
                    cClient.CreateTransactionLog(originator,
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        TransactionTypeModules.Assign,
                        TransactionModules.UserTFA,
                        originator + " authenticate TFA user " + originator);
                }
                else if (is_checked == "false")
                {
                    cClient.CreateTransactionLog(originator,
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        TransactionTypeModules.Assign,
                        TransactionModules.UserTFA,
                        originator + " un-authenticate TFA user " + originator);
                }
            }
            catch (Exception ex) { }
        }
        else
        {
            response.Status = false;
            response.StatusCode = "Error";
        }

        return response;
    }

    //for adding new user
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string addNewUser(string originator, string userName, string designation, string password, string userType, string mobile)
    {
        string status = null;
        CommonClient commonClient = new CommonClient();

        status = commonClient.addNewUser(originator, userName, designation, password, userType, mobile);

        return status;
    }

    //for updating user
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string updateUser(string originator, string userName, string designation, string password, string userType, int UserID, string mobile)
    {
        string status = null;
        CommonClient commonClient = new CommonClient();

        status = commonClient.updateUser(originator, userName, designation, password, userType, UserID, mobile);

        return status;
    }

    //for deleting user
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string deleteUser(int UserId)
    {
        string status = null;
        CommonClient commonClient = new CommonClient();

        status = commonClient.deleteUser(UserId);

        try
        {
            string originator = UserSession.Instance.OriginalUserName;

            commonClient.CreateTransactionLog(originator,
                DateTime.Now.ToString(),
                TransactionTypeModules.Delete,
                TransactionModules.User,
                "Delete user id " + UserId.ToString());
        }
        catch { }

        return status;
    }

    [WebMethod(EnableSession = true)]
    public List<SalesDataEntity> getRepsLiveSales(string Originator)
    {
        CommonClient commonClient = new CommonClient();
        List<SalesDataEntity> List = new List<SalesDataEntity>();

        List = commonClient.getRepsLiveSales(Originator);

        return List;
    }

    #region "3dr Phase"

    [WebMethod(EnableSession = true)]
    public List<SalesRepAttendanceHoursModel> getRepsAttendanceData(string Originator, string SelectDate)
    {
        CommonClient commonClient = new CommonClient();

        return commonClient.GetRepsAttendanceHours(Originator, SelectDate);
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult getAttendanceReasons(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";


        CommonClient commonClient = new CommonClient();

        List<AttendanceReasons> data = new List<AttendanceReasons>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;
            args.ROriginator = UserSession.Instance.OriginatorString;
            args.Originator = UserSession.Instance.OriginalUserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }



            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "getallusersbydeptstring");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            data = commonClient.GetAttendanceReasons();
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;

            obj_dataSourceResult.Total = 0;// data[0].rowCount;

        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public bool deleteAttendanceReason(int id)
    {
        bool status = false;
        CommonClient commonClient = new CommonClient();

        status = commonClient.deleteAttendanceReason(UserSession.Instance.OriginalUserName, id);

        try
        {
            string originator = UserSession.Instance.OriginalUserName;

            commonClient.CreateTransactionLog(originator, 
                DateTime.Now.ToString(), 
                TransactionTypeModules.Delete, 
                TransactionModules.AttendanceReason, 
                "Delete reason id " + id.ToString());
        }
        catch { }

        return status;
    }
    
    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public bool addAttendanceReason(int id, string reason, string description)
    {
        bool status = false;
        CommonClient commonClient = new CommonClient();

        AttendanceReasons obj = new AttendanceReasons();

        obj.id = id;
        obj.reason = reason;
        obj.description = description;

        status = commonClient.InsertAttendanceReason(obj);

        try
        {
            string originator = UserSession.Instance.OriginalUserName;

            if (id > 0)
            {
                commonClient.CreateTransactionLog(originator,
                    DateTime.Now.ToString(),
                    TransactionTypeModules.Update,
                    TransactionModules.AttendanceReason,
                    "Update reason id " + id.ToString());
            }
            else
            {
                commonClient.CreateTransactionLog(originator,
                    DateTime.Now.ToString(),
                    TransactionTypeModules.Insert,
                    TransactionModules.AttendanceReason,
                    "Insert reason id " + id.ToString());
            }
        }
        catch { }

        return status;
    }

    [WebMethod(EnableSession = true)]
    public List<DashboardAttendanceByRepType> getDashboardAttendanceByRepType(string Originator, string SelectDate)
    {
        CommonClient commonClient = new CommonClient();

        return commonClient.GetDashboardAttendanceByRepType(Originator, SelectDate);
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetDashboardAttendanceDetails(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string originator, string date, string salesType, string attendType)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CommonClient commonClient = new CommonClient();

        List<DashboardAttendanceDetails> data = new List<DashboardAttendanceDetails>();
        DataSourceResult obj_dataSourceResult = null;

        try
        {
            ArgsModel args = new ArgsModel();
            args.StartIndex = 0;
            args.RowCount = take;
            args.Originator = UserSession.Instance.OriginalUserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "getallusersbydeptstring");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            data = commonClient.GetDashboardAttendanceDetails(args, originator, date, salesType, attendType);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;

            obj_dataSourceResult.Total = 0;// data[0].rowCount;

        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public bool SaveMobileRepDashboard(string BillVal1300)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        CommonClient commonClient = new CommonClient();

        string returnvalue = string.Empty;

        try
        {
            MobileRepsDashboardModel mobileDashboard = new MobileRepsDashboardModel();
            mobileDashboard.originator = UserSession.Instance.OriginalUserName;
            mobileDashboard.BillVal1300 = BillVal1300;

            if (!commonClient.SaveMobileRepDashboard(mobileDashboard))
            {
                return false;
            }

            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllCustomerCategory(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        CommonClient commonClient = new CommonClient();

        List<CustomerCategory> data = new List<CustomerCategory>();
        DataSourceResult obj_dataSourceResult = null;

        try
        {
            //ArgsModel args = new ArgsModel();
            //args.StartIndex = 0;
            //args.RowCount = take;
            //args.Originator = UserSession.Instance.OriginalUserName;
            //args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;

            //if (!string.IsNullOrEmpty(pgindex))
            //{
            //    if (((int.Parse(pgindex) - 2) * take) != skip)
            //    {
            //        args.StartIndex = (skip / take) + 1;
            //    }
            //    else
            //    {
            //        args.StartIndex = int.Parse(pgindex) - 1;
            //    }
            //}
            //else
            //{
            //    args.StartIndex = (skip / take) + 1;
            //}

            //if (filter != null)
            //{
            //    new CommonUtility().SetFilterField(ref filter, "getallusersbydeptstring");
            //    args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            //}

            data = commonClient.GetAllCustomerCategory();
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;

            obj_dataSourceResult.Total = 0;// data[0].rowCount;

        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public bool InsertCustomerCategory(int categoryId, string categoryName)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        CustomerClient custClient = new CustomerClient();

        string returnvalue = string.Empty;

        try
        {
            CustomerCategory cCategory = new CustomerCategory();
            cCategory.CategoryId = categoryId;
            cCategory.CategoryName = categoryName;
            cCategory.LastModifiedBy = UserSession.Instance.OriginalUserName;

            if (!custClient.InsertCustomerCategory(cCategory))
            {
                return false;
            }

            try
            {
                string originator = UserSession.Instance.OriginalUserName;

                if (categoryId > 0)
                {
                    commonClient.CreateTransactionLog(originator,
                        DateTime.Now.ToString(),
                        TransactionTypeModules.Update,
                        "CustomerMTCategory",
                        "Update category name " + categoryName);
                }
                else
                {
                    commonClient.CreateTransactionLog(originator,
                        DateTime.Now.ToString(),
                        TransactionTypeModules.Insert,
                        "CustomerMTCategory",
                        "Insert category name " + categoryName);
                }
            }
            catch { }

            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public bool DeleteCustomerCategoryByCategoryId(int categoryId, string categoryName)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        CustomerClient custClient = new CustomerClient();

        string returnvalue = string.Empty;

        try
        {
            CustomerCategory cCategory = new CustomerCategory();
            cCategory.CategoryId = categoryId;
            cCategory.LastModifiedBy = UserSession.Instance.OriginalUserName;

            if (!custClient.DeleteCustomerCategory(cCategory))
            {
                return false;
            }

            try
            {
                string originator = UserSession.Instance.OriginalUserName;

                commonClient.CreateTransactionLog(originator,
                    DateTime.Now.ToString(),
                    TransactionTypeModules.Delete,
                    "CustomerMTCategory",
                    "Delete category name " + categoryName);
            }
            catch { }

            return true;
        }
        catch (Exception)
        {
            return false;
        }
    }

    #endregion

}

public class CheckListCustomer
{
    public string CheckListId { get; set; }
    public string CustomerCode { get; set; }
}
