﻿using CRMServiceReference;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Summary description for KPIReports_service
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class KPIReports_service : System.Web.Services.WebService
{

    public KPIReports_service()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    #region KPIReports - Brands

    [WebMethod]
    public DataSourceResult UpdateKPIBrands(string brand1, 
        string brand2, 
        string brand3, 
        string brand4, 
        string brand5, 
        string Originator)
    {
        ArgsModel args = new ArgsModel();
        KPIReportClient reportService = new KPIReportClient();
        DataSourceResult dataSourceResult = new DataSourceResult();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        if (reportService.GetKPIReportBrands(args).Count > 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = false;
        }
        else
        {
            KPIReportSettingsModel model = new KPIReportSettingsModel();
            model.Brand1 = 0;
            model.Brand2 = 0;
            model.Brand3 = 0;
            model.Brand4 = 0;
            model.Brand5 = 0;
            model.CreatedBy = Originator;


            bool delstatus = false;

            delstatus = reportService.UpdateKPIReportBrands(model);

            if (delstatus)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Status = true;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Status = false;
            }
        }

        return dataSourceResult;
    }


    #endregion

    #region KPIReports - Email Recipients

   


    [WebMethod]
    public DataSourceResult UpdateKPIEmailRecipients(string emailRecipints)
    {
        ArgsModel args = new ArgsModel();
        KPIReportClient reportService = new KPIReportClient();
        DataSourceResult dataSourceResult = new DataSourceResult();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        bool delstatus = false;

        delstatus = reportService.UpdateKPIEmailRecipients(emailRecipints);

        if (delstatus)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = true;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = false;
        }

        return dataSourceResult;
    }


    #endregion



}
