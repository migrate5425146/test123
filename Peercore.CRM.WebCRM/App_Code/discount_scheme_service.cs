﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Script.Services;
using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;

/// <summary>
/// Summary description for discount_scheme_service
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
[ScriptService]
public class discount_scheme_service : System.Web.Services.WebService {

    public discount_scheme_service () {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }
    private SchemeHeaderDTO SchemeHeader
    {
        get
        {
            if (Session[CommonUtility.SCHEME_HEADER] != null)
            {
                return (SchemeHeaderDTO)Session[CommonUtility.SCHEME_HEADER];
            }
            return new SchemeHeaderDTO();
        }
        set
        {
            Session[CommonUtility.SCHEME_HEADER] = value;
        }
    }

    private int SchemeDetailId
    {
        get
        {
            if (Session[CommonUtility.SCHEME_DETAIL_ID] != null)
            {
                return (int)Session[CommonUtility.SCHEME_DETAIL_ID];
            }
            return 0;
        }
        set
        {
            Session[CommonUtility.SCHEME_DETAIL_ID] = value;
        }
    }

    //private List<FreeProductDTO> FreeProductList
    //{
    //    get
    //    {
    //        if (Session[CommonUtility.SCHEME_FREE_PRODUCT] != null)
    //        {
    //            return (List<FreeProductDTO>)Session[CommonUtility.SCHEME_FREE_PRODUCT];
    //        }
    //        return new List<FreeProductDTO>();
    //    }
    //    set
    //    {
    //        Session[CommonUtility.SCHEME_FREE_PRODUCT] = value;
    //    }
    //}

    private SchemeDetailsDTO SchemeDetail
    {
        get
        {
            if (Session[CommonUtility.SCHEME_DETAILS] != null)
            {
                return (SchemeDetailsDTO)Session[CommonUtility.SCHEME_DETAILS];
            }
            return new SchemeDetailsDTO();
        }
        set
        {
            Session[CommonUtility.SCHEME_DETAILS] = value;
        }
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetFreeProductList(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        SchemeDetailsDTO schemeDetails = SchemeDetail;

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (schemeDetails.FreeProductList == null)
            schemeDetails.FreeProductList = new List<FreeProductDTO>();

        dataSourceResult.Data = schemeDetails.FreeProductList;
        dataSourceResult.Total = schemeDetails.FreeProductList.Count;


        return dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetOptionLevelList(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        SchemeDetailsDTO schemeDetails = SchemeDetail;

        //if (SchemeHeader.SchemeDetailsList != null)
        //    schemeDetails = SchemeHeader.SchemeDetailsList.FirstOrDefault(c => c.SchemeDetailsId == SchemeDetailId);
        //List<OptionLevelDTO> optionLevelList = new List<OptionLevelDTO>();
        //if (schemeDetails != null && schemeDetails.OptionLevelList != null)
        //{
        //    optionLevelList = schemeDetails.OptionLevelList;
        //}
        //else
        //{
        //    optionLevelList = new List<OptionLevelDTO>();
        //}

        if (schemeDetails.OptionLevelList == null)
            schemeDetails.OptionLevelList = new List<OptionLevelDTO>();

        DataSourceResult dataSourceResult = new DataSourceResult();


        dataSourceResult.Data = schemeDetails.OptionLevelList.OrderBy(c=>c.IndexId);
        dataSourceResult.Total = schemeDetails.OptionLevelList.Count;


        return dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetCombinationList(int indexId ,int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        SchemeDetailsDTO schemeDetails = SchemeDetail;

        if (schemeDetails.OptionLevelList == null)
            schemeDetails.OptionLevelList = new List<OptionLevelDTO>();

        List<OptionLevelDTO> oLevelList = schemeDetails.OptionLevelList;

        OptionLevelDTO optionLevel = oLevelList.FirstOrDefault(c => c.IndexId == indexId);
        if (optionLevel == null)
            optionLevel = new OptionLevelDTO();
        if (optionLevel.OptionLevelCombinationList == null)
            optionLevel.OptionLevelCombinationList = new List<OptionLevelCombinationDTO>();

        DataSourceResult dataSourceResult = new DataSourceResult();


        dataSourceResult.Data = optionLevel.OptionLevelCombinationList.OrderBy(c => c.IndexId);
        dataSourceResult.Total = optionLevel.OptionLevelCombinationList.Count;


        return dataSourceResult;
    }

    
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllPackingTypeDataAndCount(int skip, int take, IEnumerable<Sort> sort, Filter filter)
    {
        //string parentOriginator = "";
        //parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        //SalesClient salesClient = new CRMServiceReference.SalesClient();
        CommonClient commonClient = new CommonClient();

        List<BrandDTO> data = new List<BrandDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;
            args.ROriginator = UserSession.Instance.OriginatorString;
            args.Originator = UserSession.Instance.OriginalUserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            //args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "created_by"));

            
            args.StartIndex = (skip / take) + 1;
            

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllPackingTypeSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " packing_name desc";
                }
            }
            else
            {
                args.OrderBy = " packing_name desc";
            }
            args.OrderBy = " ORDER BY " + args.OrderBy; 
            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "allPackingTypedata");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            if (!string.IsNullOrEmpty(args.AdditionalParams))
            {
                args.AdditionalParams = " AND " + args.AdditionalParams + args.OrderBy;
            }
            else
            {
                args.AdditionalParams =  args.OrderBy;
            }

            //CommonClient commonClient = new CommonClient();
            List<object> objList = new List<object>();
            objList.Add(args.AdditionalParams);
            List<LookupDTO> lookupList = commonClient.GetLookups("PackingNameLookup", "", objList, null, false);

            //data = commonClient.GetAllBrandsDataAndCount(args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = lookupList;
            obj_dataSourceResult.Total = lookupList.Count;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    private string GetAllPackingTypeSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " packing_name desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "Code":
                sortStr = " product_packing_id " + sortoption;
                break;
            case "Description":
                sortStr = " packing_name " + sortoption;
                break;
            default:
                sortStr = " packing_name desc ";
                break;
        }

        return sortStr;
    }


    [WebMethod(EnableSession = true)]
    public DataSourceResult GetCatalogDataAndCount(int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        ActivityClient activityClient = new ActivityClient();
        CommonClient commonClient = new CommonClient();
        List<CatalogDTO> catalogList = null;

            string sortby = string.Empty;

            List<Sort> sortExpression = (List<Sort>)sort;

            #region Args Setting
            ArgsDTO args = new ArgsDTO();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.Originator = UserSession.Instance.UserName;
            args.ManagerMode = true;
            args.StartIndex = (skip / take) + 1;
            args.RowCount = take;
            args.AdditionalParams = "";
            #endregion

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "catalog", "");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            if (UserSession.Instance != null)
            {
                if (sortExpression != null && sortExpression.Count > 0)
                {
                    args.OrderBy = GetCatalogSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " description asc";
                }
            }
            else
            {
                args = new ArgsDTO();
            }

            catalogList = commonClient.GetCatalogLookup(args);
        
  

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (catalogList != null)
        {
            if (catalogList.Count != 0)
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = catalogList;
                dataSourceResult.Total = catalogList[0].rowCount;
            }
            else
            {
                dataSourceResult = new DataSourceResult();
                dataSourceResult.Data = catalogList;
                dataSourceResult.Total = 0;
            }
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = catalogList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    private string GetCatalogSortString(string sortby, string sorttype)
    {
        string sortStr = "description";
        string sortoption = " asc";

        if (!string.IsNullOrWhiteSpace(sorttype))
            sortoption = " " + sorttype;

        switch (sortby)
        {
            case "CatlogId":
                sortStr = "catlog_id" + sortoption;
                break;
            case "Description":
                sortStr = "description" + sortoption;
                break;
            case "CatlogCode":
                sortStr = "catlog_code" + sortoption;
                break;
            case "DeliveryFrequency":
                sortStr = "product_type" + sortoption;
                break;

        }

        return sortStr;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllProductCategoryDataAndCount(int skip, int take, IEnumerable<Sort> sort, Filter filter)
    {
        //string parentOriginator = "";
        //parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        //SalesClient salesClient = new CRMServiceReference.SalesClient();
        CommonClient commonClient = new CommonClient();

        List<BrandDTO> data = new List<BrandDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;
            args.ROriginator = UserSession.Instance.OriginatorString;
            args.Originator = UserSession.Instance.OriginalUserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            //args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "created_by"));


            args.StartIndex = (skip / take) + 1;


            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllProductCategorySortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " description desc";
                }
            }
            else
            {
                args.OrderBy = " description desc";
            }
            args.OrderBy = " ORDER BY " + args.OrderBy;
            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "allProductCategorydata");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            if (!string.IsNullOrEmpty(args.AdditionalParams))
            {
                args.AdditionalParams = " WHERE " + args.AdditionalParams + args.OrderBy;
            }
            else
            {
                args.AdditionalParams = args.OrderBy;
            }

            //CommonClient commonClient = new CommonClient();
            List<object> objList = new List<object>();
            objList.Add(args.AdditionalParams);
            List<LookupDTO> lookupList = commonClient.GetLookups("ProductCategoryLookup", "", objList, null, false);

            //data = commonClient.GetAllBrandsDataAndCount(args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = lookupList;
            obj_dataSourceResult.Total = lookupList.Count;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    private string GetAllProductCategorySortString(string sortby, string gridSortOrder)
    {
        string sortStr = " description desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "Code":
                sortStr = " id " + sortoption;
                break;
            case "Description":
                sortStr = " description " + sortoption;
                break;
            default:
                sortStr = " description desc ";
                break;
        }

        return sortStr;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetSchemeHeaderDataAndCount(DateTime fromDate, DateTime toDate, int isInactive, int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        string createdBy = "";
        createdBy = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        DiscountSchemeClient discountSchemeClient = new DiscountSchemeClient();
        List<SchemeHeaderDTO> data = new List<SchemeHeaderDTO>();

        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            //args.RowCount = 10;
            args.RowCount = take;

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "created_by"));
            args.SStartDate = fromDate.ToString("yyyy-MM-dd");
            args.SEndDate = toDate.ToString("yyyy-MM-dd");
            if(isInactive == 1)
                args.ActiveInactiveChecked = false; 
            else
                args.ActiveInactiveChecked = true; 

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetSchemeHeaderSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " scheme_name desc";
                }
            }
            else
            {
                args.OrderBy = " scheme_name desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "schemeheaders");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }


            data = discountSchemeClient.GetAllSchemeHeader(args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public List<SchemeHeaderDTO> GetDiscountSchemeName(string name,string isActive)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        bool status=true;

        DiscountSchemeClient discountSchemeClient = new DiscountSchemeClient();
       // List<RouteMasterDTO> lstRouteDTO = new List<RouteMasterDTO>();
        List<SchemeHeaderDTO> returnList = new List<SchemeHeaderDTO>();
        if(isActive=="0")
          status= false;
        try
        {
            returnList = discountSchemeClient.GetSchemeHeaderByName(name, status);
            return returnList;
        }
        catch (Exception)
        {

            return null;
        }
    }


    [WebMethod(EnableSession = true)]
    public DataSourceResult GetSchemeDetailsDataAndCount(string headerid, int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, int isSession)
    {
        string createdBy = "";
        createdBy = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        DiscountSchemeClient discountSchemeClient = new DiscountSchemeClient();
        List<SchemeDetailsDTO> data = new List<SchemeDetailsDTO>();
        SchemeHeaderDTO allheaders = new SchemeHeaderDTO();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            //args.RowCount = 10;
            args.RowCount = take;

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "created_by"));
            //args.SStartDate = fromDate.ToString("yyyy-MM-dd");
            //args.SEndDate = toDate.ToString("yyyy-MM-dd");
            //if (isInactive == 1)
            //    args.ActiveInactiveChecked = false;
            //else
            //    args.ActiveInactiveChecked = true;

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetSchemeHeaderSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " scheme_name desc";
                }
            }
            else
            {
                args.OrderBy = " scheme_name desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "schemeheaders");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }


            //data = discountSchemeClient.GetSchemeDetailsBySchemeHeaderId(args,Convert.ToInt32(headerid));

            //if (Session[CommonUtility.SCHEME_HEADER] == null)
            if(isSession == 0)
            {
                args.RowCount = 10000;
                allheaders = discountSchemeClient.GetSchemeHeaderById(args, Convert.ToInt32(headerid));
                Session[CommonUtility.SCHEME_HEADER] = allheaders;
            }
            else
            {
                allheaders = (SchemeHeaderDTO)Session[CommonUtility.SCHEME_HEADER];
            }
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = allheaders.SchemeDetailsList.OrderBy(c=>c.IndexId);
            obj_dataSourceResult.Total = allheaders.SchemeDetailsList.Count;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }


    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllDivisionsDataAndCount(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        //string parentOriginator = "";
        //parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        //SalesClient salesClient = new CRMServiceReference.SalesClient();
        DiscountSchemeClient commonClient = new DiscountSchemeClient();

        List<DivisionDTO> data = new List<DivisionDTO>();
        DataSourceResult obj_dataSourceResult = null;
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;
            args.ROriginator = UserSession.Instance.OriginatorString;
            args.Originator = UserSession.Instance.OriginalUserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            //args.ChildOriginators = ((UserSession.Instance.ChildOriginators).Replace("originator", "created_by"));

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<Sort> sortExpression = (List<Sort>)sort;

            if (sortExpression != null)
            {
                if (sortExpression.Count != 0)
                {
                    args.OrderBy = GetAllDivisionSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
                }
                else
                {
                    args.OrderBy = " division_name desc";
                }
            }
            else
            {
                args.OrderBy = " division_name desc";
            }

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "allbrandsdata");
                args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }

            data = commonClient.GetAllDivisions(args);
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data[0].rowCount;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }
        return obj_dataSourceResult;
    }

    
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetProductsByDivisionId(int divisionId,int status, int take, int skip, IEnumerable<Sort> sort, Filter filter)
    {
        List<ProductDTO> poroductList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<Sort> sortExpression = (List<Sort>)sort;

        ArgsDTO argsDTO = new ArgsDTO();

        if (Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            argsDTO = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;

            if (sortExpression != null && sortExpression.Count > 0)
            {
                argsDTO.OrderBy = "";
                foreach (Sort item in sortExpression)
                {
                    item.Field = GetDivisionProductSortString(item.Field);
                    if (string.IsNullOrEmpty(argsDTO.OrderBy))
                        argsDTO.OrderBy = item.ToExpression();
                    else
                        argsDTO.OrderBy = argsDTO.OrderBy + ", " + item.ToExpression();
                }
            }
            else
            {
                argsDTO.OrderBy = " product_id ASC ";
            }

            argsDTO.StartIndex = (skip / take) + 1;
            argsDTO.RowCount = take;
            argsDTO.AdditionalParams = string.Empty;

            if (filter != null)
            {
                new CommonUtility().SetFilterField(ref filter, "DivisionProduct", "");
                argsDTO.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            }
        }
        else
        {
            argsDTO = new ArgsDTO();
        }
        CommonClient commonClient = new CommonClient();
        bool isDataFromSession = false;
        //if (Session[CommonUtility.DIVISION_PRODUCTS] != null && status != 1)
        //{
        //    poroductList = (List<ProductDTO>)Session[CommonUtility.DIVISION_PRODUCTS];
        //    isDataFromSession = true;
        //}
        //else
        //{
            poroductList = commonClient.GetProductsByDivisionId(divisionId, argsDTO);
            Session[CommonUtility.DIVISION_PRODUCTS] = poroductList;
        //}

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (poroductList != null && poroductList.Count > 0)
        {
            dataSourceResult.Data = poroductList;
            dataSourceResult.Total = poroductList[0].rowCount;
        }
        else
        {
            dataSourceResult.Data = poroductList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;

    }

    [WebMethod]
    public DataSourceResult GetAllTerritoriesforDiscountScheme(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string SchemeHeaderId)
    {
        MasterClient schemeclient = new MasterClient();
        List<TerritoryModel> territoryList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetProductTerritorySortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " territory_name asc";
            }
        }
        else
        {
            args.OrderBy = " territory_name asc";
        }

        if (filter != null)
        {
            new CommonUtility().SetFilterField(ref filter, "ProductTerritoryDiscount", "");
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
        }

        territoryList = schemeclient.GetAllTerritoryByDiscountScheme(args, SchemeHeaderId);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (territoryList != null && territoryList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = territoryList;
            dataSourceResult.Total = territoryList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = territoryList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel SaveUpdateDiscountTerritory(string originator,
                                                       string scheme_header_id,
                                                       string territory_id,
                                                       string is_checked)
    {
        ArgsModel args = new ArgsModel();
        DiscountSchemeClient schemeclient = new DiscountSchemeClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        bool statusOut = schemeclient.SaveUpdateDiscountTerritory(originator,
                                                    scheme_header_id,
                                                   territory_id,
                                                   is_checked);
        if (statusOut)
        {
            response.Status = true;
            response.StatusCode = "Success";

            try
            {
                CommonClient cClient = new CommonClient();

                if (is_checked == "true")
                {
                    cClient.CreateTransactionLog(originator,
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        TransactionTypeModules.Assign,
                        TransactionModules.DiscountSchemeTerritory,
                        originator + " assign territory " + territory_id.ToString() + " to scheme " + scheme_header_id.ToString());
                }
                else if (is_checked == "false")
                {
                    cClient.CreateTransactionLog(originator,
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        TransactionTypeModules.Assign,
                        TransactionModules.DiscountSchemeTerritory,
                        originator + " unassign territory " + territory_id.ToString() + " to scheme " + scheme_header_id.ToString());
                }
            }
            catch (Exception ex) { }
        }
        else
        {
            response.Status = false;
            response.StatusCode = "Error";
        }

        return response;
    }

    private static string GetAllDivisionSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " division_name desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "DivisionCode":
                sortStr = " division_code " + sortoption;
                break;
            case "DivisionName":
                sortStr = " division_name " + sortoption;
                break;
            default:
                sortStr = " division_name desc ";
                break;
        }

        return sortStr;
    }

    private string GetDivisionProductSortString(string sorttype)
    {
        string sortStr = "market_id";

        switch (sorttype)
        {
            case "DivisionName":
                sortStr = "division_name";
                break;
            case "Name":
                sortStr = "name";
                break;
            default:
                sortStr = "name";
                break;
        }

        return sortStr;
    }
    
    private static string GetSchemeHeaderSortString(string sortby, string gridSortOrder)
    {
        string sortStr = "scheme_name desc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "SchemeName":
                sortStr = " scheme_name " + sortoption;
                break;
            case "StartDate":
                sortStr = " start_date " + sortoption;
                break;
            case "EndDate":
                sortStr = " end_date " + sortoption;
                break;
            case "DivisionName":
                sortStr = " division_name " + sortoption;
                break;
            default:
                sortStr = "scheme_name desc ";
                break;
        }

        return sortStr;
    }
    
    private static string GetProductTerritorySortString(string sortby, string gridSortOrder)
    {
        string sortStr = "territory_name asc";
        string sortoption = "asc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "TerritoryName":
                sortStr = " territory_name " + sortoption;
                break;
            case "TerritoryCode":
                sortStr = " territory_code " + sortoption;
                break;
            case "AreaName":
                sortStr = " area_name " + sortoption;
                break;
            default:
                sortStr = "territory_name asc ";
                break;
        }

        return sortStr;
    }

}
