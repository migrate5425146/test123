﻿using CRMServiceReference;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Summary description for product
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]

[ScriptService]

public class product : System.Web.Services.WebService
{
    #region "Product Territory"

    [WebMethod]
    public DataSourceResult GetAllProductsByTerritoryId(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string TerritoryId)
    {
        ProductClient productclient = new ProductClient();
        List<ProductModel> procutList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetAllProductSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " id asc";
            }
        }
        else
        {
            args.OrderBy = " id asc";
        }

        procutList = productclient.GetAllProductsByTerritoryId(args, TerritoryId);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (procutList != null && procutList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = procutList;
            dataSourceResult.Total = procutList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = procutList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult GetAllTerritoriesByProductId(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, string ProductId)
    {
        ProductClient productclient = new ProductClient();
        List<TerritoryModel> territoryList = null;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetAllProductSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " territory_name asc";
            }
        }
        else
        {
            args.OrderBy = " territory_name asc";
        }

        territoryList = productclient.GetAllTerritoriesByProductId(args, ProductId);

        DataSourceResult dataSourceResult = new DataSourceResult();

        if (territoryList != null && territoryList.Count != 0)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = territoryList;
            dataSourceResult.Total = territoryList[0].TotalCount;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Data = territoryList;
            dataSourceResult.Total = 0;
        }

        return dataSourceResult;
    }

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel SaveUpdateProductTerritory(string originator,
                                                       string territory_id,
                                                       string product_id,
                                                       string is_checked)
    {
        ArgsModel args = new ArgsModel();
        ProductClient productlient = new ProductClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        bool statusOut = productlient.SaveUpdateTerritoryProducts(originator,
                                                   territory_id,
                                                   product_id,
                                                   is_checked);
        if (statusOut)
        {
            response.Status = true;
            response.StatusCode = "Success";
        }
        else
        {
            response.Status = false;
            response.StatusCode = "Error";
        }

        return response;
    }

    #endregion

    private static string GetAllProductSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " id asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "ProductId":
                sortStr = " id " + sortoption;
                break;
            case "Code":
                sortStr = " code " + sortoption;
                break;
            case "Name":
                sortStr = " name " + sortoption;
                break;
            default:
                sortStr = " id asc ";
                break;
        }

        return sortStr;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string GetProductByProductCode(string productCode)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";
        JavaScriptSerializer serializer = new JavaScriptSerializer();
        ProductClient productClient = new ProductClient();
        ProductModel productModel = new ProductModel();

        string returnvalue = string.Empty;

        try
        {
            productModel = productClient.GetProductByProductCode(productCode);
        }
        catch (Exception)
        {
            returnvalue = "error";
        }

        return serializer.Serialize(productModel);
    }
}
