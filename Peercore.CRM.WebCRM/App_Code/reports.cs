﻿using CRMServiceReference;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Summary description for reports
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
public class reports : System.Web.Services.WebService
{
    CommonClient commonClient = new CommonClient();

    public reports()
    {

        //Uncomment the following line if using designed components 
        //InitializeComponent(); 
    }

    //Load all Check List Details
    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllKPIReportUsersByReportId(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, int ReportId)
    {
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        List<KPIReportUser> data = new List<KPIReportUser>();
        DataSourceResult obj_dataSourceResult = null;

        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.RowCount = take;
            args.ROriginator = UserSession.Instance.OriginatorString;
            args.Originator = UserSession.Instance.OriginalUserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.AdditionalParams = "";

            if (!string.IsNullOrEmpty(pgindex))
            {
                if (((int.Parse(pgindex) - 2) * take) != skip)
                {
                    args.StartIndex = (skip / take) + 1;
                }
                else
                {
                    args.StartIndex = int.Parse(pgindex) - 1;
                }
            }
            else
            {
                args.StartIndex = (skip / take) + 1;
            }

            List<Sort> sortExpression = (List<Sort>)sort;

            args.OrderBy = " task asc";
            //if (sortExpression != null)
            //{
            //    if (sortExpression.Count != 0)
            //    {
            //        args.OrderBy = GetAllCheckListSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            //    }
            //    else
            //    {
            //        args.OrderBy = " task asc";
            //    }
            //}

            //if (filter != null)
            //{
            //    new CommonUtility().SetFilterField(ref filter, "allchecklistdata");
            //    args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);// new CommonUtility().GridFilterString(filterExpression);
            //}

            data = commonClient.GetAllKPIReportUsersByReportId(ReportId);

            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = data;
            obj_dataSourceResult.Total = data.Count;
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
            obj_dataSourceResult.Data = new List<DataSourceResult>();
            obj_dataSourceResult.Total = 0;
        }

        return obj_dataSourceResult;
    }

}
