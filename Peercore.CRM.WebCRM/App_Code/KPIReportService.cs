﻿using CRMServiceReference;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;

/// <summary>
/// Summary description for KPIReportService
/// </summary>
[WebService(Namespace = "http://tempuri.org/")]
[WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
[ScriptService]
// To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
// [System.Web.Script.Services.ScriptService]
public class KPIReportService : System.Web.Services.WebService
{

	public KPIReportService()
	{
		//Uncomment the following line if using designed components 
		//InitializeComponent(); 
	}

    #region - KPIReports Week - 

    private static string GetAllKPIWeekSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " BatchId asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "AreaId":
                sortStr = " area_id " + sortoption;
                break;
            case "TotReadyStockVal":
                sortStr = " ta_ready_stock_val " + sortoption;
                break;
            case "TotPreInvoiceVal":
                sortStr = " ta_pre_invoice_val " + sortoption;
                break;
            default:
                sortStr = " batch_id asc ";
                break;
        }

        return sortStr;
    }

    private string GetFilterKPIWeekField(string fieldName)
    {
        string realfield = "";

        switch (fieldName)
        {
            case "BatchId":
                realfield = "batch_id";
                break;
            case "AreaId":
                realfield = "area_id";
                break;
            case "WeekId":
                realfield = "week_id";
                break;
            case "TargetDate":
                realfield = "target_date";
                break;
        }

        return realfield;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllKPIReportsWeeklyTargetByMonth(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex, int month, int year)
    {
        KPIReportClient reportClient = new KPIReportClient();

        string parentOriginator = "";
        parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetAllKPIWeekSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " week_id asc";
            }
        }
        else
        {
            args.OrderBy = " week_id asc";
        }

        if (filter != null)
        {
            if (filter.Filters != null)
            {
                foreach (Filter item in filter.Filters)
                {
                    item.Field = GetFilterKPIWeekField(item.Field);
                }
            }
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        List<KPIReportModel> data = new List<KPIReportModel>();
        DataSourceResult obj_dataSourceResult = null;

        try
        {
            data = reportClient.GetAllKPIReportWeeklyTargetsByMonth(args, month, year);
            obj_dataSourceResult = new DataSourceResult();

            if (data != null && data.Count > 0)
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = data[0].TotalCount;
            }
            else
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = 0;
            }
        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
        }

        return obj_dataSourceResult;
    }

    #endregion


    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllKPIReportsByMonth(int month, int year, int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        KPIReportClient reportClient = new KPIReportClient();

        string parentOriginator = "";
        parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GetAllReportSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " area_id asc";
            }
        }
        else
        {
            args.OrderBy = " area_id desc";
        }

        if (filter != null)
        {
            if (filter.Filters != null)
            {
                foreach (Filter item in filter.Filters)
                {
                    item.Field = GetFilterReportField(item.Field);
                }
            }
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        List<KPIReportModel> data = new List<KPIReportModel>();
        DataSourceResult obj_dataSourceResult = null;

        try
        {
            data = reportClient.GetAllKPIs(args, month, year);
            obj_dataSourceResult = new DataSourceResult();

            if (data != null && data.Count > 0)
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = data[0].TotalCount;
            }
            else
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = 0;
            }

        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
        }

        return obj_dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public string DeleteEmailRecipients(string email)
    {
        DataSourceResult dataSourceResult = new DataSourceResult();
        bool status = false;
        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        string jsonString = string.Empty;
        KPIReportClient reportService = new KPIReportClient();
        OriginatorDTO delOriginator = new OriginatorDTO();

        status = reportService.DeleteKPIEmailRecipient(email);

        string returnvalue = string.Empty;
        if (status)
            returnvalue = "Deleted";

        return returnvalue;
    }

    private static string GetAllReportSortString(string sortby, string gridSortOrder)
    {
        string sortStr = " BatchId asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "AreaId":
                sortStr = " area_id " + sortoption;
                break;
            case "TotReadyStockVal":
                sortStr = " ta_ready_stock_val " + sortoption;
                break;
            case "TotPreInvoiceVal":
                sortStr = " ta_pre_invoice_val " + sortoption;
                break;
            default:
                sortStr = " batch_id asc ";
                break;
        }

        return sortStr;
    }
    
    private static string GridSortingBrandItems(string sortby, string gridSortOrder)
    {
        string sortStr = " ProductName asc";
        string sortoption = "desc";

        if (gridSortOrder == "asc")
            sortoption = "asc";
        else
            sortoption = "desc";

        switch (sortby)
        {
            case "ProductFgsCode":
                sortStr = " fg_code " + sortoption;
                break;
            case "ProductName":
                sortStr = " product_name " + sortoption;
                break;
            case "BrandName":
                sortStr = " brand_name " + sortoption;
                break;
            case "CategoryName":
                sortStr = " category_name " + sortoption;
                break;
            case "FlavourName":
                sortStr = " flavor_name " + sortoption;
                break;
            case "PackingName":
                sortStr = " packing_name " + sortoption;
                break;
            case "IsHighValue":
                sortStr = " is_high_value " + sortoption;
                break;
            default:
                sortStr = " product_name asc ";
                break;
        }

        return sortStr;
    }

    private string GetFilterReportField(string fieldName)
    {
        string realfield = "";

        switch (fieldName)
        {
            case "BatchId":
                realfield = "batch_id";
                break;
            case "AreaId":
                realfield = "area_id";
                break;
            case "WeekId":
                realfield = "week_id";
                break;
            case "TargetDate":
                realfield = "target_date";
                break;

        }

        return realfield;
    }

    private string GetFilterKPIBrandItemField(string fieldName)
    {
        string realfield = "";

        switch (fieldName)
        {
            case "ProductFgsCode":
                realfield = "crm_product.fg_code";
                break;
            case "ProductName":
                realfield = "crm_product.name";
                break;
            case "BrandName":
                realfield = "crm_brands.name";
                break;
            case "CategoryName":
                realfield = "crm_product_category.description";
                break;
            case "FlavourName":
                realfield = "crm_ds_flavor.flavor_name";
                break;
            case "PackingName":
                realfield = "crm_product_packing.packing_name";
                break;
            case "IsHighValue":
                realfield = "crm_product.is_high_value";
                break;
        }

        return realfield;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetAllEmailRecipients(int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        KPIReportClient reportClient = new KPIReportClient();

        string parentOriginator = "";
        parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        //if (sortExpression != null)
        //{
        //    if (sortExpression.Count != 0)
        //    {
        //        args.OrderBy = GetAllReportSortString(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
        //    }
        //    else
        //    {
        args.OrderBy = " Email asc";
        //    }
        //}
        //else
        //{
        //    args.OrderBy = " Email desc";
        //}

        if (filter != null)
        {
            //if (filter.Filters != null)
            //{
            //    foreach (Filter item in filter.Filters)
            //    {
            //        item.Field = GetFilterReportField(item.Field);
            //    }
            //}
            //args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        List<KPIReportEmailRecipientsModel> data = new List<KPIReportEmailRecipientsModel>();
        DataSourceResult obj_dataSourceResult = null;

        try
        {
            data = reportClient.GetAllEmailRecipients(args);
            obj_dataSourceResult = new DataSourceResult();

            if (data != null && data.Count > 0)
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = data.Count;
            }
            else
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = 0;
            }

        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
        }

        return obj_dataSourceResult;
    }

    [WebMethod]
    public DataSourceResult UpdateKPIEmailRecipients(string emailRecipints)
    {
        ArgsModel args = new ArgsModel();
        KPIReportClient reportService = new KPIReportClient();
        DataSourceResult dataSourceResult = new DataSourceResult();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        bool delstatus = false;

        delstatus = reportService.UpdateKPIEmailRecipients(emailRecipints);

        if (delstatus)
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = true;
        }
        else
        {
            dataSourceResult = new DataSourceResult();
            dataSourceResult.Status = false;
        }

        return dataSourceResult;
    }

    [WebMethod(EnableSession = true)]
    public DataSourceResult GetKPIBrandItemsByBrandId(int kpiBrandId, int skip, int take, IEnumerable<Sort> sort, Filter filter, string pgindex)
    {
        KPIReportClient reportClient = new KPIReportClient();

        string parentOriginator = "";
        parentOriginator = UserSession.Instance.UserName;

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        ArgsModel args = new ArgsModel();
        args.StartIndex = 0;
        args.RowCount = take;
        if (!string.IsNullOrEmpty(pgindex))
        {
            if (((int.Parse(pgindex) - 2) * take) != skip)
            {
                args.StartIndex = (skip / take) + 1;
            }
            else
            {
                args.StartIndex = int.Parse(pgindex) - 1;
            }
        }
        else
        {
            args.StartIndex = (skip / take) + 1;
        }

        List<Sort> sortExpression = (List<Sort>)sort;

        if (sortExpression != null)
        {
            if (sortExpression.Count != 0)
            {
                args.OrderBy = GridSortingBrandItems(sortExpression[(sortExpression.Count - 1)].Field, sortExpression[(sortExpression.Count - 1)].Dir);
            }
            else
            {
                args.OrderBy = " product_name asc";
            }
        }
        else
        {
            args.OrderBy = " product_name desc";
        }

        if (filter != null)
        {
            if (filter.Filters != null)
            {
                foreach (Filter item in filter.Filters)
                {
                    item.Field = GetFilterKPIBrandItemField(item.Field);
                }
            }
            args.AdditionalParams = filter.ToExpression((List<Filter>)filter.Filters);
        }

        List<KPIReportBrandItemModel> data = new List<KPIReportBrandItemModel>();
        DataSourceResult obj_dataSourceResult = null;

        try
        {
            data = reportClient.GetKPIBrandItemsByBrandId(args, kpiBrandId);
            obj_dataSourceResult = new DataSourceResult();

            if (data != null && data.Count > 0)
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = data[0].TotalCount;
            }
            else
            {
                obj_dataSourceResult.Data = data;
                obj_dataSourceResult.Total = 0;
            }

        }
        catch (Exception)
        {
            obj_dataSourceResult = new DataSourceResult();
        }

        return obj_dataSourceResult;
    }

    [WebMethod]
    public Peercore.CRM.Model.ResponseModel AssignKPIBrandItems(string originator,
                                                       string kpi_brand_id,
                                                       string product_id,
                                                       string is_checked)
    {
        ArgsModel args = new ArgsModel();
        KPIReportClient reportClient = new KPIReportClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        bool statusOut =  reportClient.AssignKPIBrandItems(originator,
                                                   kpi_brand_id,
												   product_id,
												   is_checked);
		if (statusOut)
        {
            response.Status = true;
            response.StatusCode = "Success";
        }
        else
        {
            response.Status = false;
            response.StatusCode = "Error";
        }

        return response;
    }
    
    [WebMethod]
    public Peercore.CRM.Model.ResponseModel UpdateKPIBrandDisplayName(string kpi_brand_id,
                                                       string displayName)
    {
        ArgsModel args = new ArgsModel();
        KPIReportClient reportClient = new KPIReportClient();
        Peercore.CRM.Model.ResponseModel response = new Peercore.CRM.Model.ResponseModel();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        bool statusOut = reportClient.UpdateKPIBrandDisplayName(kpi_brand_id, displayName);
		if (statusOut)
        {
            response.Status = true;
            response.StatusCode = "Success";
        }
        else
        {
            response.Status = false;
            response.StatusCode = "Error";
        }

        return response;
    } 
    
    [WebMethod]
    public KPIReportBrandsModel GetKPIReportBrandByBrandId(string kpi_brand_id)
    {
        ArgsModel args = new ArgsModel();
        KPIReportClient reportClient = new KPIReportClient();

        HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
        HttpContext.Current.Response.CacheControl = "no-cache";

        KPIReportBrandsModel response = reportClient.GetKPIReportBrandByBrandId(kpi_brand_id);
		
        return response;
    }
}
