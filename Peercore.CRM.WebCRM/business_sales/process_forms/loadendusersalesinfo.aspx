﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="loadendusersalesinfo.aspx.cs" Inherits="business_sales_process_forms_loadendusersalesinfo" %>

<%@ Register Src="~/usercontrols/endusersalesinfo.ascx" TagPrefix="ucl1" TagName="endusersalesinfo" %>
<%@ Register Src="~/usercontrols/sales_history_details_enduser.ascx" TagPrefix="ucl1" TagName="SalesHistoryDetailsEnduser" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="div_sales_enduser_window" style="display:none">
        <ucl1:SalesHistoryDetailsEnduser ID="salesHistoryDetailsEnduser" runat="server" />
    </div>
    <asp:HiddenField ID="HiddenFieldDetailType" runat="server" />
    <div class="toolbar2">
        <a id="href_reports" class="sprite_button" style="float: right; margin-right: 10px; display:none;"><span class="copy icon"></span>Reports</a>
        <a id="href_print_end" class="sprite_button" style="float: right; margin-right: 10px;"><span class="print icon"></span>Print</a>
        <a id="href_close_enduser" class="sprite_button" style="float: right; margin-right: 10px;"><span
            class="leftarrow icon"></span>Back</a> 
            
            <span id="div_row_count" class="rowcounttext">
        </span>
        <h4 id="div_report_header1" <%--style="padding-top:47px;--%> padding-left:5px;"></h4>
    </div>
    <div class="toolbar3" id="lblCurrentFlowEnduser">
        Month : FEB | text1 | text2
    </div>
    <div class="toolbar4">
        <asp:DropDownList ID="DropDownDisplayOption" runat="server" CssClass="dropdownwidth" Width="120px">
                    
            <asp:ListItem Value="D" Text="Sales In Dollars"></asp:ListItem>
            <asp:ListItem Value="T" Text="Sales In Tonnes"  Selected="True" ></asp:ListItem>
            <asp:ListItem Value="U" Text="Sales In Units" ></asp:ListItem>
            <%--<asp:ListItem Value="Tonnes" Text="Tonnes" Selected="True"></asp:ListItem>
            <asp:ListItem Value="Sales/Tonne" Text="Sales/Tonne" ></asp:ListItem>
            <asp:ListItem Value="Sales vs Budget ($)" Text="Sales vs Budget ($)" ></asp:ListItem>
            <asp:ListItem Value="Sales vs Budget (T)" Text="Sales vs Budget (T)" ></asp:ListItem>
            <asp:ListItem Value="Last Sales vs Budget ($)" Text="Last Sales vs Budget ($)" ></asp:ListItem>
            <asp:ListItem Value="Last Sales vs Budget (T)" Text="Last Sales vs Budget (T)" ></asp:ListItem>--%>
        </asp:DropDownList>
        <span>Sort by </span>
        <asp:DropDownList ID="DropDownListSort" runat="server" CssClass="dropdownwidth">
            <asp:ListItem Value="6" Text="MTD (U)" Selected="True"></asp:ListItem>
            <asp:ListItem Value="3" Text="MTD ($)"></asp:ListItem>
            <asp:ListItem Value="9" Text="YTD (U)" ></asp:ListItem>
            <asp:ListItem Value="10" Text="YTD ($)" ></asp:ListItem>
            <asp:ListItem Value="11" Text="Last YTD (U)" ></asp:ListItem>
            <asp:ListItem Value="12" Text="Last YTD ($)" ></asp:ListItem>
            <asp:ListItem Value="1" Text="Code" ></asp:ListItem>
        </asp:DropDownList>
        <span>Group by </span>
        <asp:DropDownList ID="DropDownGroupBy" runat="server" CssClass="dropdownwidth">
            <asp:ListItem Value="brand" Text="Brand"></asp:ListItem>
            <asp:ListItem Value="product" Text="Product"></asp:ListItem>
            <asp:ListItem Value="customer" Text="Customer"></asp:ListItem>            
            <asp:ListItem Value="rep" Text="Rep"></asp:ListItem>
            <asp:ListItem Value="state" Text="State"></asp:ListItem>
        </asp:DropDownList>
        <div style="display: inline-block;">
            <span>Selection</span>
            <asp:TextBox ID="txtSelection" runat="server" ReadOnly="true" Width="100px"></asp:TextBox></div>
    </div>
    <div class="toolbar4">
        <a id="a_end_state" href="javascript:SetSalesInfo('state','gridEndUserSales','GetBusinessSalesEndUser','1')">State</a> | <a id="a_end_rep" href="javascript:SetSalesInfo('rep','gridEndUserSales','GetBusinessSalesEndUser','1')">
            Rep</a> | <a id="a_end_brand" href="javascript:SetSalesInfo('brand','gridEndUserSales','GetBusinessSalesEndUser','1')">Brand</a> | <a id="a_end_product" href="javascript:SetSalesInfo('product','gridEndUserSales','GetBusinessSalesEndUser','1')">
            Product</a> | <a id="a_end_customer" href="javascript:SetSalesInfo('customer','gridEndUserSales','GetBusinessSalesEndUser','1')">
            Customer</a>
        <asp:Button ID="btnAllRepsEnduser" Text="All Reps"  CssClass="button"  />
    </div>
    <div id="div_salesinfo" runat="server">
       <ucl1:endusersalesinfo ID="enduserinfo" runat="server" />
    </div>
    </form>
    <script type="text/javascript">
        $(document).ready(function () {
            //Load Data.
            loadEndUserSalesInfo();
        });
    </script>
     
     <script type="text/javascript">
         hideStatusDiv("MainContent_div_message");

         $("#DropDownDisplayOption").change(function () {
             LoadTitle($("#HiddenFieldDetailType").val(), "n", "0", $("#DropDownDisplayOption").val(), "", "gridEndUserSales", "GetBusinessSalesEndUser")
             LoadSalesInfoChart();
         });

         $("#DropDownGroupBy").change(function () {
             //alert($("#MainContent_DropDownGroupBy").val());
             SetSalesInfo($("#DropDownGroupBy").val(), "gridEndUserSales", "GetBusinessSalesEndUser");
             //LoadTitle($("#MainContent_DropDownGroupBy").val(), "n", "0", $("#MainContent_DropDownDisplayOption").val(), "")
         });


         $("#DropDownListSort").change(function () {
             loadSortSalesInfoGrid("gridEndUserSales", "GetBusinessSalesEndUser", "enduser");
         });

         $(document).ready(function () {
            

             LoadTitle("rep", "n", "0", "T", "", "gridEndUserSales", "GetBusinessSalesEndUser");
             SetSalesInfoCss('rep', 'enduser');
             LoadSalesInfoChart($("#DropDownDisplayOption").val(), "enduser");

             $("#panelbarcontent1").kendoPanelBar({
                 expandMode: "single",
                 select: onSelect
             });

             function onSelect(e) {
                 var item = $(e.item)[0].id;
                 //alert(item);
                 if (item != undefined && item != '') {
                     switch (item) {
                         case "4":
                             //loadSortSalesInfoGrid("gridCustomerSales", "GetBusinessSales", "enduser");
                             break;
                         case "5":
                             //alert($("#DropDownDisplayOption").val());
                             //LoadSalesInfoChart($("#DropDownDisplayOption").val(), "enduser");
                             break;
                         case "6":
                             LoadSalesInfoTrendChart($("#txtSelection").val(), "enduser");
                             break;
                     }
                 }
             }

             var div_sales_enduser_window = $("#div_sales_enduser_window"),
                        newcust_undo = $("#undo")
                                .bind("click", function () {
                                    div_sales_enduser_window.data("kendoWindow").open();
                                    newcust_undo.hide();
                                });

             var onClose = function () {
                 newcust_undo.show();
             }

             if (!div_sales_enduser_window.data("kendoWindow")) {
                 div_sales_enduser_window.kendoWindow({
                     width: "800px",
                     height: "500px",
                     title: "Perfetti | Business Sales History Details",
                     close: onClose
                 });
             }

             div_sales_enduser_window.data("kendoWindow").close();

         });

//         $("#href_print_end").click(function () {
//             $("div#div_loader").show();
//             $("#div_sales_enduser_window").data("kendoWindow").open();
//             $("div#div_loader").hide();

//         });

         $("#enduserinfo_DropDownGraphTypeenduser").change(function () {
             LoadSalesInfoTrendChart($("#txtSelection").val(), "enduser");
         });

         $("#enduserinfo_rbTrendGraphAccumulateenduser").change(function () {
             LoadSalesInfoTrendChart($("#txtSelection").val(), "enduser");
         });

         $("#enduserinfo_rbTrendGraphMonthlyenduser").change(function () {
             LoadSalesInfoTrendChart($("#txtSelection").val(), "enduser");
         });

         $("#href_close_enduser").click(function () {
             LoadBackSalesInfo("enduser");
         });
        </script>

</body>
</html>
