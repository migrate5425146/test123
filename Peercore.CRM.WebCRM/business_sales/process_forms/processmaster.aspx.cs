﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using Peercore.CRM.Common;
using System.Text;
using Peercore.CRM.Shared;
using System.Web.Script.Services;

public partial class business_sales_process_forms_processmaster : PageBase
{
    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != "")
        {
            if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
            {
                if (Request.QueryString["type"] == "Update")
                {
                    
                }
                else if (Request.QueryString["type"] == "Delete")
                {
                    
                }
                else if (Request.QueryString["type"] == "select")
                {
                    if (Request.QueryString["fm"].ToLower() == "alldbclaiminvoices")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = GetAllDBClaimInvoices(Request.QueryString["disVal"]);
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                }
                else if (Request.QueryString["type"] == "query")
                {
                    if (Request.QueryString["fm"] == "SalesInfoChart")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string loadScheduleTemplate = SetChartDisplay(Request.QueryString["dispOp"]);
                        Response.ContentType = "text/html";
                        Response.Write(loadScheduleTemplate);
                        Response.End();
                    }
                    if (Request.QueryString["fm"] == "TrendChart")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string loadScheduleTemplate = PopulateTrendGraph();
                        Response.ContentType = "text/html";
                        Response.Write(loadScheduleTemplate);
                        Response.End();
                    }
                   
                }
                else if (Request.QueryString["type"] == "Set")
                {
                    if (Request.QueryString["fm"] == "SalesInfo")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string loadScheduleTemplate = SetSalesInfo();
                        Response.ContentType = "text/html";
                        Response.Write(loadScheduleTemplate);
                        Response.End();
                    }
                    if (Request.QueryString["fm"] == "DailySalesInfo")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string loadScheduleTemplate = DailySalesInfo();
                        Response.ContentType = "text/html";
                        Response.Write(loadScheduleTemplate);
                        Response.End();
                    }
                    if (Request.QueryString["fm"] == "LoadBackSalesInfo")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string loadScheduleTemplate = LoadBackSalesInfo();
                        Response.ContentType = "text/html";
                        Response.Write(loadScheduleTemplate);
                        Response.End();
                    }
                }
            }
        }
    }
    #endregion
    
    #region Methods
    private string SetSalesInfo()
    {
        try
        {
            SalesInfoDetailSearchCriteriaDTO salesInfoDetSrcCritaria = null;

            string user = Request.QueryString["user"];

            string commenCode = Server.UrlDecode(Request.QueryString["code"]);
            string commenDescription = Request.QueryString["desc"];
            string detailType = Request.QueryString["dtype"];
            char displayOption = char.Parse(Request.QueryString["option"]);
            string pgindex = Request.QueryString["pgindex"];

            string market = Server.UrlDecode(Request.QueryString["market"]);
            string isCmp = Server.UrlDecode(Request.QueryString["isCmp"]);
            

            List<SalesInfoDetailBackDTO> backList = (List<SalesInfoDetailBackDTO>)Session[CommonUtility.SALES_INFO_DETAIL_BACK];

            SalesInfoDetailBackDTO salesInfoDetailBack = new SalesInfoDetailBackDTO();


            if (!string.IsNullOrEmpty(user) && user != "GetBusinessSales")
            {
                salesInfoDetSrcCritaria = (SalesInfoDetailSearchCriteriaDTO)Session[CommonUtility.SALES_INFO_DETAIL_ENDUSER];
                Session[CommonUtility.SALES_INFO_DETAIL_ENDUSER] = GetSalesInfoDetailSearchCriteria(salesInfoDetSrcCritaria, commenCode, commenDescription, detailType, displayOption, market, isCmp);
                salesInfoDetailBack.Type = "E";
                salesInfoDetailBack.Id = backList.Count == 0 ? 1 : backList.Where(x => x.Type == "E").Max(c => c.Id) + 1;
            }
            else
            {
                //if (pgindex == "2")
                //{
                //    commenCode = "";
                //    commenDescription = "";
                //}

                salesInfoDetSrcCritaria = (SalesInfoDetailSearchCriteriaDTO)Session[CommonUtility.SALES_INFO_DETAIL];

                if (pgindex == "2")
                {
                    commenCode = "";
                    commenDescription = "";
                    salesInfoDetSrcCritaria.MarketId = "";
                    salesInfoDetSrcCritaria.Brand = "";
                    salesInfoDetSrcCritaria.BrandDescription = "";
                    salesInfoDetSrcCritaria.CustomerCode = "";
                    salesInfoDetSrcCritaria.CustomerDescription = "";
                    salesInfoDetSrcCritaria.RepCode = "";
                    salesInfoDetSrcCritaria.RepDescription = "";
                }

                Session[CommonUtility.SALES_INFO_DETAIL] = GetSalesInfoDetailSearchCriteria(salesInfoDetSrcCritaria, commenCode, commenDescription, detailType, displayOption, market);
                salesInfoDetailBack.Type = "C";
                //salesInfoDetailBack.Id = backList.Where(x => x.Type == "C").Max(c => c.Id) + 1;
                salesInfoDetailBack.Id = backList.Count == 0 ? 1 : backList.Where(x => x.Type == "C").Max(c => c.Id) + 1;
            }
            if (pgindex == "1" || pgindex == "2")
            {
                salesInfoDetailBack.Code = commenCode;
                salesInfoDetailBack.Description = commenDescription;
                salesInfoDetailBack.DetailType = detailType;// salesInfoDetSrcCritaria.DetailType;
                salesInfoDetailBack.MarketId = market;
                backList.Add(salesInfoDetailBack);
                Session[CommonUtility.SALES_INFO_DETAIL_BACK] = backList;
            }
        }
        catch (Exception ex)
        {
        }
        return "true";
    }

    private string DailySalesInfo()
    {
        try
        {
            SalesInfoDetailSearchCriteriaDTO salesInfoDetSrcCritaria = null;
            string isCmp = Server.UrlDecode(Request.QueryString["isCmp"]);
            salesInfoDetSrcCritaria = (SalesInfoDetailSearchCriteriaDTO)Session[CommonUtility.SALES_INFO_DETAIL];
            salesInfoDetSrcCritaria.IsCMP = isCmp=="1";
            Session[CommonUtility.SALES_INFO_DETAIL] = salesInfoDetSrcCritaria;
        }
        catch (Exception ex)
        {
        }
        return "true";
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static List<string> SetCustomerTargetInfo(string detailTypeparam, string sastFlag, string monthparam)
    {
        List<TargetInfoDTO> targetlist = new List<TargetInfoDTO>();
        List<string> titleList = new List<string>();

        try
        {
            SalesInfoDetailSearchCriteriaDTO salesInfoDetSrcCritaria = null;

            string user = HttpContext.Current.Request.QueryString["user"];
            string detailType = HttpContext.Current.Request.QueryString["tartype"];
            string pgindex = HttpContext.Current.Request.QueryString["pgindex"];

           // List<SalesInfoDetailBackDTO> backList = (List<SalesInfoDetailBackDTO>)Session[CommonUtility.SALES_INFO_DETAIL_BACK];

            CustomerClient customerService = new CustomerClient();

            targetlist = HttpContext.Current.Session[CommonUtility.CUSTOMER_TARGETS] as List<TargetInfoDTO>;

            if (targetlist == null) {
                targetlist = customerService.GetCustomerMonthleyTargetsByDistributor(UserSession.Instance.UserName);
            }

            //SalesClient salesClient = new SalesClient();
            //int iCostYear = 0;
            //if (sastFlag.Equals("n"))
            //{
            //    iCostYear = salesClient.GetCurrentCostPeriod().Year;
            //}
            //else
            //{
            //    iCostYear = salesClient.GetCurrentCostPeriod().Year - 1;
            //}

            //List<string> lstAllColumnHeaders = new List<string>();
            //List<string> lstColumnHeaders = new List<string>();

            //lstAllColumnHeaders = salesClient.LoadCostPeriods(iCostYear);

            ////int i = lstAllColumnHeaders.IndexOf(month) - 5;
            //int i = lstAllColumnHeaders.IndexOf(DateTime.Now.ToString("MMM").ToUpper()) - 5;

            //if (i < 0) i += 12;

            //for (int x = 0; x < 6; x++)
            //{
            //    lstColumnHeaders.Add(lstAllColumnHeaders[i]);
            //    //lstColumnHeaders.Add(lstAllColumnHeaders[i]);
            //    //lstColumnHeaders.Add(lstAllColumnHeaders[i]);

            //    i++;
            //    if (i > 11) i = 0;
            //}

            if (targetlist != null)
            {
                foreach (TargetInfoDTO targetInfo in targetlist)
                {
                    if (targetInfo.TargetMonth1 != null || targetInfo.TargetMonth1 != 0) {
                        string month = "Jan";
                        foreach (string title in titleList)
                        {
                            if (!titleList.Contains(month))
                                titleList.Add(month);
                        }
                    }

                    if (targetInfo.TargetMonth2 != null || targetInfo.TargetMonth2 != 0)
                    {
                        string month = "Feb";
                        foreach (string title in titleList)
                        {
                            if (!titleList.Contains(month))
                                titleList.Add(month);
                        }
                    }

                    if (targetInfo.TargetMonth3 != null || targetInfo.TargetMonth3 != 0)
                    {
                        string month = "Mar";
                        foreach (string title in titleList)
                        {
                            if (!titleList.Contains(month))
                                titleList.Add(month);
                        }
                    }

                    if (targetInfo.TargetMonth4 != null || targetInfo.TargetMonth4 != 0)
                    {
                        string month = "Apr";
                        foreach (string title in titleList)
                        {
                            if (!titleList.Contains(month))
                                titleList.Add(month);
                        }
                    }

                    if (targetInfo.TargetMonth5 != null || targetInfo.TargetMonth5 != 0)
                    {
                        string month = "May";
                        foreach (string title in titleList)
                        {
                            if (!titleList.Contains(month))
                                titleList.Add(month);
                        }
                    }

                    if (targetInfo.TargetMonth6 != null || targetInfo.TargetMonth6 != 0)
                    {
                        string month = "Jun";
                        foreach (string title in titleList)
                        {
                            if (!titleList.Contains(month))
                                titleList.Add(month);
                        }
                    }

                    if (targetInfo.TargetMonth7 != null || targetInfo.TargetMonth7 != 0)
                    {
                        string month = "Jul";
                        foreach (string title in titleList)
                        {
                            if (!titleList.Contains(month))
                                titleList.Add(month);
                        }
                    }


                    if (targetInfo.TargetMonth8 != null || targetInfo.TargetMonth8 != 0)
                    {
                        string month = "Aug";
                        foreach (string title in titleList)
                        {
                            if (!titleList.Contains(month))
                                titleList.Add(month);
                        }
                    }

                    if (targetInfo.TargetMonth9 != null || targetInfo.TargetMonth9 != 0)
                    {
                        string month = "Sep";
                        foreach (string title in titleList)
                        {
                            if (!titleList.Contains(month))
                                titleList.Add(month);
                        }
                    }

                    if (targetInfo.TargetMonth10 != null || targetInfo.TargetMonth10 != 0)
                    {
                        string month = "Oct";
                        foreach (string title in titleList)
                        {
                            if (!titleList.Contains(month))
                                titleList.Add(month);
                        }
                    }

                    if (targetInfo.TargetMonth11 != null || targetInfo.TargetMonth11 != 0)
                    {
                        string month = "Nov";
                        foreach (string title in titleList)
                        {
                            if (!titleList.Contains(month))
                                titleList.Add(month);
                        }
                    }

                    if (targetInfo.TargetMonth12 != null || targetInfo.TargetMonth12 != 0)
                    {
                        string month = "Dec";
                        foreach (string title in titleList)
                        {
                            if (!titleList.Contains(month))
                                titleList.Add(month);
                        }
                    }

                    titleList.Add("This YTD");
                    titleList.Add("Last YTD");
                    titleList.Add(DateTime.Now.AddYears(-1).ToString());
                    titleList.Add(DateTime.Now.AddYears(-2).ToString());
                }
            }else{
                //Show last 6 month.
                
            }


            if (pgindex == "1")
            {
                //salesInfoDetailBack.Code = commenCode;
                //salesInfoDetailBack.Description = commenDescription;
                //salesInfoDetailBack.DetailType = detailType;// salesInfoDetSrcCritaria.DetailType;

                //backList.Add(salesInfoDetailBack);
                //Session[CommonUtility.SALES_INFO_DETAIL_BACK] = backList;
            }
        }
        catch (Exception ex)
        {
        }
        return titleList;
    }

    [System.Web.Services.WebMethod(EnableSession = true)]
    [ScriptMethod(UseHttpGet = false, ResponseFormat = ResponseFormat.Json)]
    public static List<String> SetRepTargetInfo(string detailTypeparam, string sastFlag, string monthparam)
    {
        //List<TargetInfoDTO> targetlist = new List<TargetInfoDTO>();
        List<String> titleList = new List<string>();

        try
        {

            //// List<SalesInfoDetailBackDTO> backList = (List<SalesInfoDetailBackDTO>)Session[CommonUtility.SALES_INFO_DETAIL_BACK];

            //OriginatorClient originatorService = new OriginatorClient();

            //targetlist = HttpContext.Current.Session[CommonUtility.REP_TARGETS] as List<TargetInfoDTO>;

            //if (targetlist == null)
            //{
            //    targetlist = originatorService.GetRepMonthleyTargetsByDistributor(UserSession.Instance.UserName);
            //    HttpContext.Current.Session[CommonUtility.REP_TARGETS] = targetlist;
            //}

            //int monthNumber = DateTime.Now.Month;


            string sEnquiryTitle = "";
            sEnquiryTitle = SetDisplayName(detailTypeparam, sEnquiryTitle);


            List<int> selectedMonth = new List<int>();

            int currentMonth = DateTime.Today.Month;
            currentMonth = currentMonth - 2;
            if (currentMonth <= 0) currentMonth += 12;
            for (int x = 0; x < 3; x++)
            {
                selectedMonth.Add(currentMonth + x);

                if (currentMonth > 11) currentMonth = 0;
            }


            //if (targetlist != null && targetlist.Count > 0)
            //{
            foreach (int item in selectedMonth)
            {
                if (item == 1)
                {
                    titleList.Add("Jan(T/A)");
                }
                else if (item == 2)
                {
                    titleList.Add("Feb(T/A)");
                }
                else if (item == 3)
                {
                    titleList.Add("Mar(T/A)");
                }
                else if (item == 4)
                {
                    titleList.Add("Apr(T/A)");
                }
                else if (item == 5)
                {
                    titleList.Add("May(T/A)");
                }
                else if (item == 6)
                {
                    titleList.Add("Jun(T/A)");
                }
                else if (item == 7)
                {
                    titleList.Add("Jul(T/A)");
                }

                else if (item == 8)
                {
                    titleList.Add("Aug(T/A)");
                }
                else if (item == 9)
                {
                    titleList.Add("Sep(T/A)");
                }
                else if (item == 10)
                {
                    titleList.Add("Oct(T/A)");
                }
                else if (item == 11)
                {
                    titleList.Add("Nov(T/A)");
                }
                else if (item == 12)
                {
                    titleList.Add("Dec(T/A)");
                }
            }

            titleList.Add("Total(T/A)");
            titleList.Add(sEnquiryTitle);
            titleList.Add("This YTD(T/A)");
            titleList.Add("Last YTD(T/A)");
            titleList.Add("%(T/A)");
            titleList.Add(DateTime.Now.AddYears(-1).Year.ToString() + "(T/A)");
            titleList.Add(DateTime.Now.AddYears(-2).Year.ToString() + "(T/A)");

            //}
        }
        catch (Exception ex)
        {
        }
        return titleList;
    }

    private static string SetDisplayName(string detailType, string sEnquiryTitle)
    {
        switch (detailType)
        {
            case "market":
                sEnquiryTitle = "Market";
                break;
            case "brand":
                sEnquiryTitle = "Brand";
                break;
            case "product":
                sEnquiryTitle = "Product";
                break;
            case "customer":
                sEnquiryTitle = "Customer";
                //sEnquiryTitle = "Distributor";
                break;
            case "rep":
                sEnquiryTitle = "DR";
                break;
            case "route":
                sEnquiryTitle = "Route";
                break;
            default:
                break;
        }
        return sEnquiryTitle;
    }



    private string LoadBackSalesInfo()
    {
        SalesInfoDetailBackDTO salesInfoDetailBack = new SalesInfoDetailBackDTO();
        try
        {
            SalesInfoDetailSearchCriteriaDTO salesInfoDetSrcCritaria = null;

            string user = Request.QueryString["user"];

            List<SalesInfoDetailBackDTO> backList = (List<SalesInfoDetailBackDTO>)Session[CommonUtility.SALES_INFO_DETAIL_BACK];

            if (!string.IsNullOrEmpty(user) && user != "cust")
            {

                List<SalesInfoDetailBackDTO> tempList = backList.Where(x => x.Type == "E").OrderBy(c => c.Id).ToList();
                if (tempList.Count == 0)
                    salesInfoDetSrcCritaria = (SalesInfoDetailSearchCriteriaDTO)Session[CommonUtility.SALES_INFO_DETAIL_ENDUSER];
                else
                    salesInfoDetSrcCritaria = LoadSalesDetailsPage();

                for (int i = 0; i < tempList.Count - 1; i++)
                {
                    salesInfoDetSrcCritaria = GetSalesInfoDetailSearchCriteria(salesInfoDetSrcCritaria,
                                                                                        tempList[i].Code, tempList[i].Description,
                                                                                            tempList[i].DetailType, 'T', tempList[i].MarketId);
                }
                salesInfoDetailBack = tempList[tempList.Count - 2];
                backList.Remove(tempList[tempList.Count - 1]);
                Session[CommonUtility.SALES_INFO_DETAIL_ENDUSER] = salesInfoDetSrcCritaria;


                //salesInfoDetSrcCritaria = (SalesInfoDetailSearchCriteriaDTO)Session[CommonUtility.SALES_INFO_DETAIL_ENDUSER];                

                //List<SalesInfoDetailBackDTO> tempList = backList.Where(x => x.Type == "E").OrderBy(c=>c.Id).ToList();
                //if (tempList.Count != 1)
                //{
                //    salesInfoDetailBack = tempList[tempList.Count - 2];
                //    backList.Remove(tempList[tempList.Count - 1]);
                //}
                //else
                //{
                //    salesInfoDetailBack = tempList[0];
                //    salesInfoDetSrcCritaria.DetailType = salesInfoDetailBack.DetailType;
                //}

                //Session[CommonUtility.SALES_INFO_DETAIL_ENDUSER] = GetSalesInfoDetailSearchCriteria(salesInfoDetSrcCritaria,
                //                                                                    salesInfoDetailBack.Code, salesInfoDetailBack.Description, 
                //                                                                        salesInfoDetailBack.DetailType,'T');
            }
            else
            {
                //salesInfoDetSrcCritaria = LoadSalesDetailsPage();// (SalesInfoDetailSearchCriteriaDTO)Session[CommonUtility.SALES_INFO_DETAIL];

                List<SalesInfoDetailBackDTO> tempList = backList.Where(x => x.Type == "C").OrderBy(c => c.Id).ToList();
                if (tempList.Count <= 1)
                    salesInfoDetSrcCritaria = (SalesInfoDetailSearchCriteriaDTO)Session[CommonUtility.SALES_INFO_DETAIL];
                else
                    salesInfoDetSrcCritaria = LoadSalesDetailsPage();

                

                for (int i = 0; i < tempList.Count-1; i++)
                {

                    salesInfoDetSrcCritaria = GetSalesInfoDetailSearchCriteria(salesInfoDetSrcCritaria,
                                                                                        tempList[i].Code, tempList[i].Description,
                                                                                            tempList[i].DetailType, 'T', tempList[i].MarketId);
                    
                }
                salesInfoDetailBack = tempList[tempList.Count - 2];
                if (tempList.Count > 1)
                    backList.Remove(tempList[tempList.Count - 1]);

                if (tempList.Count == 2)
                {
                    //salesInfoDetSrcCritaria.ChildReps = UserSession.Instance.UserName == "cpereira" ? "" : UserSession.Instance.ChildReps;
                    salesInfoDetSrcCritaria.RepType = UserSession.Instance.RepType;
                }

                Session[CommonUtility.SALES_INFO_DETAIL] = salesInfoDetSrcCritaria;
                //if (tempList.Count > 1)
                //{
                //    salesInfoDetailBack = tempList[tempList.Count - 1];
                    
                //    salesInfoDetSrcCritaria.DetailType = tempList[tempList.Count - 2].DetailType;
                //    //salesInfoDetailBack.DetailType = tempList[tempList.Count - 1].DetailType;
                //    backList.Remove(tempList[tempList.Count - 1]);
                //}
                //else
                //{
                //    salesInfoDetailBack = tempList[tempList.Count - 1];
                //    salesInfoDetSrcCritaria.DetailType = tempList[tempList.Count - 0].DetailType;
                //    salesInfoDetailBack.Code = "";
                //    salesInfoDetailBack.Description = "";
                //    //backList.Remove(tempList[tempList.Count - 1]);
                //}
                
                ////if (tempList.Count > 2)
                ////{
                ////    salesInfoDetailBack = tempList[tempList.Count - 2];
                ////    salesInfoDetSrcCritaria.DetailType = tempList[tempList.Count - 3].DetailType;
                ////    backList.Remove(tempList[tempList.Count - 1]);
                ////}
                ////else if (tempList.Count == 2)
                ////{
                ////    salesInfoDetailBack = tempList[tempList.Count - 2];
                ////    salesInfoDetSrcCritaria.DetailType = tempList[tempList.Count - 2].DetailType;
                ////    backList.Remove(tempList[tempList.Count - 1]);
                ////}
                ////else
                ////{
                ////    salesInfoDetailBack = tempList[0];
                ////    salesInfoDetSrcCritaria.DetailType = salesInfoDetailBack.DetailType;
                ////}
                //Session[CommonUtility.SALES_INFO_DETAIL] = GetSalesInfoDetailSearchCriteria(salesInfoDetSrcCritaria,
                //                                                                    salesInfoDetailBack.Code, salesInfoDetailBack.Description,
                //                                                                        salesInfoDetailBack.DetailType, 'T');
            }
            
            Session[CommonUtility.SALES_INFO_DETAIL_BACK] = backList;
            
        }
        catch (Exception ex)
        {
        }
        return salesInfoDetailBack.DetailType + "|" + salesInfoDetailBack.MarketId;
    }

    private SalesInfoDetailSearchCriteriaDTO LoadSalesDetailsPage()
    {
        int iCostYear = 0;
        string sMonthAbbr = "";
        bool bShowAllReps = false;
        SalesClient salesClient = new SalesClient();
        //brSales oSales = new brSales();

        //string repCode = (string)new brOriginator().GetRepCode(GlobalValues.GetInstance().FilterByUserName == "" ? GlobalValues.GetInstance().UserName : GlobalValues.GetInstance().FilterByUserName);
        string repCode = "";//UserSession.Instance.RepCode;// " WHERE originator = '" + UserSession.Instance.RepCode + "'";
        //int costPeriod = Convert.ToInt32(oSales.GetCurrentCostPeriod(ref iCostYear));

        EndUserSalesDTO endUserSalesEntity = salesClient.GetCurrentCostPeriod();
        int costPeriod = endUserSalesEntity.Period;
        iCostYear = endUserSalesEntity.Year;

        sMonthAbbr = salesClient.GetMonthForCostPeriod(iCostYear, costPeriod);
        // Quinn - 16-Nov-2012
        if (UserSession.Instance.UserName == "cpereira")
            bShowAllReps = true;

        SalesInfoDetailSearchCriteriaDTO dto = new SalesInfoDetailSearchCriteriaDTO()
        {
            
            Brand = string.Empty,
            cDisplayOption = 'D',
            CustomerCode = string.Empty,
            CustomerDescription = string.Empty,
            DetailType = "customer",
            DisplayFullName = true,
            iCostPeriod = costPeriod,
            Market = string.Empty,
            Month = sMonthAbbr,
            RepCode = repCode == null ? string.Empty : repCode,
            RepDescription = UserSession.Instance.OriginalUserName,
            sLastFlag = "n",
            SortField = "6",
            showAllReps = bShowAllReps,
            RepType = UserSession.Instance.RepType
        };

        return dto;
        //ucSalesInfoDetail salesInfoView = ViewAdapter.Instance.GetSalesInfoDetail(dto, GlobalValues.GetInstance().UserName);
        //saleDetailsContent.Content = salesInfoView;
    }

    private SalesInfoDetailSearchCriteriaDTO GetSalesInfoDetailSearchCriteria(SalesInfoDetailSearchCriteriaDTO oOrgSrcCriteria,
        string commenCode, string commenDescription, string detailType, char displayOption, string market,string isCmp ="0")
    {
        try
        {
            //string commenCode = Request.QueryString["code"];
            //string commenDescription = Request.QueryString["desc"];
            //string detailType = Request.QueryString["dtype"];
            //char displayOption = char.Parse(Request.QueryString["option"]);

            SalesInfoDetailSearchCriteriaDTO sidSrcCriteria = new SalesInfoDetailSearchCriteriaDTO();
            sidSrcCriteria.MarketId = oOrgSrcCriteria.MarketId;
                               
            sidSrcCriteria.Brand = oOrgSrcCriteria.Brand;
            sidSrcCriteria.BrandDescription = oOrgSrcCriteria.BrandDescription;

            sidSrcCriteria.CustomerCode = oOrgSrcCriteria.CustomerCode;
            sidSrcCriteria.CustomerDescription = oOrgSrcCriteria.CustomerDescription;
            sidSrcCriteria.RepCode = oOrgSrcCriteria.RepCode;
            sidSrcCriteria.RepDescription = oOrgSrcCriteria.RepDescription;
            
            sidSrcCriteria.sLastFlag = oOrgSrcCriteria.sLastFlag;
            sidSrcCriteria.Month = oOrgSrcCriteria.Month;
            sidSrcCriteria.iCostPeriod = oOrgSrcCriteria.iCostPeriod;
            sidSrcCriteria.cDisplayOption = displayOption;
            sidSrcCriteria.SortField = oOrgSrcCriteria.SortField;

            sidSrcCriteria.IsCMP = (isCmp == "1"); 

            sidSrcCriteria.DisplayFullName = oOrgSrcCriteria.DisplayFullName;

            if (market == "" || market == "0")
            {
                sidSrcCriteria.MarketId = "";
            }
            else
            {
                sidSrcCriteria.MarketId = market;
            }

            sidSrcCriteria.DetailType = detailType;
            
            if (oOrgSrcCriteria.DetailType.Equals("market"))
            {
                sidSrcCriteria.MarketId = commenCode;
                sidSrcCriteria.Market = commenDescription;
            }
            else if (oOrgSrcCriteria.DetailType.Equals("brand"))
            {
                sidSrcCriteria.Brand = commenCode;
                sidSrcCriteria.BrandDescription = commenDescription;
            }
            
            else if (oOrgSrcCriteria.DetailType.Equals("customer"))
            {
                sidSrcCriteria.CustomerCode = commenCode;
                sidSrcCriteria.CustomerDescription = commenDescription;
            }
            
            else if (oOrgSrcCriteria.DetailType.Equals("repgroup"))
            {
                sidSrcCriteria.RepCode = commenCode;
                sidSrcCriteria.RepDescription = commenDescription;
            }
            else if (oOrgSrcCriteria.DetailType.Equals("rep"))
            {
                sidSrcCriteria.RepCode = commenCode;
                sidSrcCriteria.RepDescription = commenDescription;
            }

            else if (oOrgSrcCriteria.DetailType.Equals("route"))
            {
                sidSrcCriteria.Route = commenCode;
                sidSrcCriteria.RouteDescription = commenDescription;
            }
            

            return sidSrcCriteria;
        }
        catch (Exception)
        {
            throw;
        }
    }

    private string SetChartDisplay(string displayOption)
    {
//        try
//        {
//            List<BePieChartDTO> lstPieChartValues = new List<BePieChartDTO>();

//            SalesInfoDetailSearchCriteriaDTO salesInfoDetSrcCritaria = null;
            
//            string user = Request.QueryString["user"];
//            if (!string.IsNullOrEmpty(user) && user != "cust")
//            {
//                if (Session[CommonUtility.SALES_INFO_DETAIL_ENDUSER] != null)
//                {
//                    salesInfoDetSrcCritaria = (SalesInfoDetailSearchCriteriaDTO)Session[CommonUtility.SALES_INFO_DETAIL_ENDUSER];
//                    salesInfoDetSrcCritaria.cDisplayOption = displayOption.ToCharArray()[0];
//                    //salesInfoDetSrcCritaria.RepCodes = new List<object>();
//                    //salesInfoDetSrcCritaria.RepCodes.Add("Q1");
//                }
//            }
//            else
//            {
//                if (Session[CommonUtility.SALES_INFO_DETAIL] != null)
//                {
//                    salesInfoDetSrcCritaria = (SalesInfoDetailSearchCriteriaDTO)Session[CommonUtility.SALES_INFO_DETAIL];
//                    salesInfoDetSrcCritaria.cDisplayOption = displayOption.ToCharArray()[0];
//                    //salesInfoDetSrcCritaria.RepCodes = new List<object>();
//                    //salesInfoDetSrcCritaria.RepCodes.Add("Q1");
//                }
//            }

//             ArgsDTO argsDTO = new ArgsDTO();
//            if (Session[CommonUtility.GLOBAL_SETTING] != null)
//            {
//                argsDTO = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
//                argsDTO.RowCount = 9;
//                if (displayOption == "D")
//                {
//                    argsDTO.OrderBy = "Sum1 DESC";
//                }
//                else
//                {
//                    argsDTO.OrderBy = "Sum2 DESC";
//                }
//            }
//            SalesInfoDetailViewStateDTO oSalesInfoDetailViewState = new SalesInfoDetailViewStateDTO();
//            List<TargetInfoDTO> repList = new List<TargetInfoDTO>();
//            SalesClient salesClient = new SalesClient();
//            OriginatorClient originatorClient = new OriginatorClient();
            
            
            
//            //if (!string.IsNullOrEmpty(user) && user != "cust")
//            //{
//            //    oSalesInfoDetailViewState = salesClient.GetEndUserSalesInfoDetails(salesInfoDetSrcCritaria, argsDTO, "chart");
//            //}
//            //else
//            //{
//            //    oSalesInfoDetailViewState = salesClient.GetSalesInfoDetailViewState(salesInfoDetSrcCritaria, argsDTO, "chart");
//            //}

//            if (!string.IsNullOrEmpty(user) && user != "cust")
//            {
//                repList = originatorClient.GetRepMonthleyTargetsByDistributor(UserSession.Instance.UserName);
//            }
//            else
//            {
//                repList = originatorClient.GetRepMonthleyTargetsByDistributor(UserSession.Instance.UserName);
//            }



//            var targetSummaryList = repList.GroupBy(i => i.RepName)
//                                                .Select(g => new
//                                                {
//                                                    RepName = g.Key,
//                                                    PeriodTarget = g.Sum(i => i.TargetMonth1 + i.TargetMonth2 + i.TargetMonth3 + i.TargetMonth4 + i.TargetMonth5
//                                                    + i.TargetMonth6 + i.TargetMonth7 + i.TargetMonth8 + i.TargetMonth9 + i.TargetMonth10 + i.TargetMonth11 + i.TargetMonth12)

//                                                });


//            List<TargetInfoDTO> repslist = new List<TargetInfoDTO>();
//            TargetInfoDTO repdto = null;

//            foreach (var item in targetSummaryList)
//            {
//                repdto = new TargetInfoDTO();

//                repdto.RepName = item.RepName;
//<<<<<<< .mine
//                repdto.TotalTarget =Convert.ToDouble(item.PeriodTarget.Value);
//=======
//    //            repdto.TotalTarget = item.PeriodTarget;
//>>>>>>> .r921

//                repslist.Add(repdto);
//            }

//     //  return SalesInfoGraph(oSalesInfoDetailViewState.lstPieChartValues);

//           return SalesInfoGraphCTC(repslist);

            //LoadToPieChart(lstPieChartValues);

        return "";
    }
        //catch (Exception)
        //{
        //    throw;
        //}

    //public string SetSeriesStateData(List<BePieChartDTO> bePieChartList)
    //{
    //    string series = "";
    //    int count = 0;
    //    foreach (BePieChartDTO lead in bePieChartList)
    //    {
    //        count++;
    //        if (string.IsNullOrEmpty(series))
    //        {
    //            series = "{ name: '" + lead.Name.Replace("'","`") + "', y: " + lead.Value + " }";
    //        }
    //        else
    //        {
    //            series += ",{ name: '" + lead.Name.Replace("'", "`") + "', y: " + lead.Value + " }";
    //        }
    //    }
    //    return series;
    //}

    public string SetSeriesStateDataCTC(List<TargetInfoDTO> bePieChartList)
    {
        string series = "";
        int count = 0;
        foreach (TargetInfoDTO lead in bePieChartList)
        {
            count++;
            if (string.IsNullOrEmpty(series))
            {
                series = "{ name: '" + lead.RepName.Replace("'", "`") + "', y: " + lead.TotalTarget +" }";
            }
            else
            {
                series += ",{ name: '" + lead.RepName.Replace("'", "`") + "', y: " + lead.TotalTarget + " }";
            }
        }
        return series;
    }

    //public string SalesInfoGraph(List<BePieChartDTO> bePieChartList)
    //{
    //    string reportDate = "";

    //    BePieChartDTO bePieChart = bePieChartList.FirstOrDefault(c => c.name == "other");

    //    if(bePieChart != null)
    //    {
    //        if (bePieChart.Value == 0)
    //        {
    //            bePieChartList.Remove(bePieChart);
    //        }
    //    }

    //    reportDate = SetSeriesStateData(bePieChartList);

    //    string user = Request.QueryString["user"];
    //    if (!string.IsNullOrEmpty(user) && user != "cust")
    //    {
    //        user = "div_trend1";
    //    }
    //    else
    //    {
    //        user = "div_trend";
    //    }

    //    StringBuilder sb = new StringBuilder();

    //    sb.Append("<div>");
    //    sb.Append("<script type=\"text/javascript\">");
    //    sb.Append("$(function () {");
    //    sb.Append("    var chart;");
    //    sb.Append("    $(document).ready(function () {");
    //    sb.Append("        chart = new Highcharts.Chart({");
    //    sb.Append("            chart: {");
    //    sb.Append("                renderTo: '" + user + "',");
    //    sb.Append("                plotBackgroundColor: null,");
    //    sb.Append("                plotBorderWidth: null,");
    //    sb.Append("                plotShadow: false");
    //    sb.Append("            },");
    //    sb.Append("            title: {");
    //    sb.Append("                text: ''");
    //    sb.Append("            },");
    //    sb.Append("            tooltip: {");
    //    sb.Append("                formatter: function () {");
    //    sb.Append("                    return '<b>' + this.point.name + ' - ' + Math.round(this.percentage*100)/100  + ' %</b>';");
    //    sb.Append("                }");
    //    sb.Append("            },");

    //    sb.Append("            plotOptions: {");
    //    sb.Append("                pie: {");
    //    sb.Append("                    allowPointSelect: true,");
    //    sb.Append("                    cursor: 'pointer',");
    //    sb.Append("                    point: {");
    //    sb.Append("                        events: {");
    //    sb.Append("                            click: function (e) {");
    //    sb.Append("                                 GridCallCyclesGraph(this.des);");
    //    sb.Append("                            }");
    //    sb.Append("                        }");
    //    sb.Append("                    },");
    //    sb.Append("                    dataLabels: {");
    //    sb.Append("                        enabled: true,");
    //    //sb.Append("                        color: '#000000',");
    //    //sb.Append("                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',");
    //    sb.Append("                        connectorColor: '#000000',");
    //    sb.Append("                        formatter: function () {");
    //    sb.Append("                           if(this.point.name == ''){");
    //    sb.Append("                             return '<b>' + this.point.name + ' - ' + Math.round(this.percentage*100)/100  + ' %</b>';");
    //    sb.Append("                           }else {");
    //    sb.Append("                             return '<b>' + this.point.name + ' - ' + Math.round(this.percentage*100)/100  + ' %</b>';");
    //    sb.Append("                           }");
    //    sb.Append("                        }");
    //    sb.Append("                    },");
    //    sb.Append("                    showInLegend: false");
    //    sb.Append("                }");
    //    sb.Append("            },");
    //    sb.Append("            series: [{");
    //    sb.Append("                type: 'pie',");
    //    sb.Append("                name: 'state',");
    //    //sb.Append("                rowcount:'rowcount',");
    //    //sb.Append("                des:'des',");
    //    sb.Append("                data: [");
    //    sb.Append(reportDate);
    //    sb.Append("        ]");
    //    sb.Append("            }]");
    //    sb.Append("        });");
    //    sb.Append("    });");

    //    sb.Append("});");
    //    sb.Append("</script>");
    //    sb.Append("</div>");

    //    return sb.ToString();
    //}

    public string SalesInfoGraphCTC(List<TargetInfoDTO> bePieChartList)
    {
        string reportDate = "";

        TargetInfoDTO bePieChart = bePieChartList.FirstOrDefault(c => c.RepName == "other");

        if (bePieChart != null)
        {
            if (bePieChart.TotalTarget == 0)
            {
                bePieChartList.Remove(bePieChart);
            }
        }

        reportDate = SetSeriesStateDataCTC(bePieChartList);

        string user = Request.QueryString["user"];
        if (!string.IsNullOrEmpty(user) && user != "cust")
        {
            user = "div_trend1";
        }
        else
        {
            user = "div_trend";
        }

        StringBuilder sb = new StringBuilder();

        sb.Append("<div>");
        sb.Append("<script type=\"text/javascript\">");
        sb.Append("$(function () {");
        sb.Append("    var chart;");
        sb.Append("    $(document).ready(function () {");
        sb.Append("        chart = new Highcharts.Chart({");
        sb.Append("            chart: {");
        sb.Append("                renderTo: '" + user + "',");
        sb.Append("                plotBackgroundColor: null,");
        sb.Append("                plotBorderWidth: null,");
        sb.Append("                plotShadow: false");
        sb.Append("            },");
        sb.Append("            title: {");
        sb.Append("                text: ''");
        sb.Append("            },");
        sb.Append("            tooltip: {");
        sb.Append("                formatter: function () {");
        sb.Append("                    return '<b>' + this.point.name + ' - ' + Math.round(this.percentage*100)/100  + ' %</b>';");
        sb.Append("                }");
        sb.Append("            },");

        sb.Append("            plotOptions: {");
        sb.Append("                pie: {");
        sb.Append("                    allowPointSelect: true,");
        sb.Append("                    cursor: 'pointer',");
        sb.Append("                    point: {");
        sb.Append("                        events: {");
        sb.Append("                            click: function (e) {");
        sb.Append("                                 GridCallCyclesGraph(this.des);");
        sb.Append("                            }");
        sb.Append("                        }");
        sb.Append("                    },");
        sb.Append("                    dataLabels: {");
        sb.Append("                        enabled: true,");
        //sb.Append("                        color: '#000000',");
        //sb.Append("                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white',");
        sb.Append("                        connectorColor: '#000000',");
        sb.Append("                        formatter: function () {");
        sb.Append("                           if(this.point.name == ''){");
        sb.Append("                             return '<b>' + this.point.name + ' - ' + Math.round(this.percentage*100)/100  + ' %</b>';");
        sb.Append("                           }else {");
        sb.Append("                             return '<b>' + this.point.name + ' - ' + Math.round(this.percentage*100)/100  + ' %</b>';");
        sb.Append("                           }");
        sb.Append("                        }");
        sb.Append("                    },");
        sb.Append("                    showInLegend: false");
        sb.Append("                }");
        sb.Append("            },");
        sb.Append("            series: [{");
        sb.Append("                type: 'pie',");
        sb.Append("                name: 'state',");
        //sb.Append("                rowcount:'rowcount',");
        //sb.Append("                des:'des',");
        sb.Append("                data: [");
        sb.Append(reportDate);
        sb.Append("        ]");
        sb.Append("            }]");
        sb.Append("        });");
        sb.Append("    });");

        sb.Append("});");
        sb.Append("</script>");
        sb.Append("</div>");

        return sb.ToString();
    }



    public string TrendDataGraph(string series ,string title)
    {
        if (string.IsNullOrEmpty(series))
        {
            CommonUtility commonUtility = new CommonUtility();
            return commonUtility.GetWarningMessage("No records.");
        }
        StringBuilder sb = new StringBuilder();

        string user = Request.QueryString["user"];
        if (!string.IsNullOrEmpty(user) && user == "cust")
        {
            user = "div_chart"; 
        }
        else if (!string.IsNullOrEmpty(user) && user == "cust_sh")
        {
            user = "div_chart_SalesHistory";
        }
        else if (!string.IsNullOrEmpty(user) && user == "enduser_sh")
        {
            user = "div_chart_SalesHistoryEndUser";
        }    
        else
        {
            user = "div_chart_enduser";
        }

        sb.Append("<div>");
        sb.Append("<script type=\"text/javascript\">");
        sb.Append("$(function () {");
        sb.Append("    var chart;");
        sb.Append("    $(document).ready(function() {");
        sb.Append("        chart = new Highcharts.Chart({");
        sb.Append("            chart: {");
        sb.Append("                renderTo: '" + user + "',");
        sb.Append("                type: 'column'");
        sb.Append("            },");
        sb.Append("            title: {");
        sb.Append("                text: '" + title + "'");
        sb.Append("            },");
        sb.Append("            subtitle: {");
        sb.Append("                text: ''");
        sb.Append("            },");
        sb.Append("            xAxis: {");
        sb.Append("                categories: ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec' ]");
        sb.Append("            },");
        sb.Append("            yAxis: {");
        sb.Append("                min: 0,");
        sb.Append("                title: {");
        sb.Append("                    text: ''");
        sb.Append("                }");
        sb.Append("            },");
        sb.Append("            legend: {");
        sb.Append("                layout: 'vertical',");
        sb.Append("                backgroundColor: '#FFFFFF',");
        sb.Append("                align: 'left',");
        sb.Append("                verticalAlign: 'top',");
        sb.Append("                x: 100,");
        sb.Append("                y: 70,");
        sb.Append("                floating: true,");
        sb.Append("                shadow: true");
        sb.Append("            },");
        sb.Append("            tooltip: {");
        sb.Append("                formatter: function() {");
        sb.Append("                    return ");
        sb.Append("                        this.y ;");
        sb.Append("                }");
        sb.Append("            },");
        sb.Append("            plotOptions: {");
        sb.Append("                column: {");
        sb.Append("                    pointPadding: 0.2,");
        sb.Append("                    borderWidth: 0");
        sb.Append("                }");
        sb.Append("            },");
        sb.Append("                series: [" + series);
        //sb.Append("                    {  name: 'Tokyo',");
        //sb.Append("                       data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]");
        //sb.Append("                    }, ");
        //sb.Append("                    {  name: 'Berlin',");
        //sb.Append("                       data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]   ");
        //sb.Append("                    }");
        sb.Append("                ]");
        sb.Append("        });");
        sb.Append("    });");
    
        sb.Append("});");
        sb.Append("		</script>");
        sb.Append("</div>");

        return sb.ToString();
    }

    private string PopulateTrendGraph()
    {
        string graph_type = Request.QueryString["graph"];// (cmbGraphType.SelectedItem as BSEComboBoxItem<string>).Value;
        bool bIsAccumulated = Request.QueryString["Accum"] == "1"; //rbTrendGraphAccumulate.IsChecked ?? false;
        string code = Request.QueryString["code"];

        string title = "";
        


        bool accumulate = true;

        if (graph_type.Equals("Actual"))
        {
            title = "Actual over 12 months";
        }
        else if (graph_type.Equals("Target"))
        {
            //graphtype = "money";
            title = "Target over 12 months";
        }
        else if (graph_type.Equals("Target/Actual"))
        {
            title = "Target vs Actual over 12 months";
        }

      

        
        List<string> lst = new List<string>();
        SalesClient salesClient = new SalesClient();
        
        OriginatorClient originatorClient = new OriginatorClient();

        ArgsDTO argsDTO = new ArgsDTO();
        if (Session[CommonUtility.GLOBAL_SETTING] != null)
        {
            argsDTO = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
        }

        SalesInfoDetailSearchCriteriaDTO salesInfoDetSrcCritaria = (SalesInfoDetailSearchCriteriaDTO)Session[CommonUtility.SALES_INFO_DETAIL];

        switch (salesInfoDetSrcCritaria.DetailType)
        {
            case "market":
                salesInfoDetSrcCritaria.Market = code;
                break;
            case "brand":
                salesInfoDetSrcCritaria.Brand = code;
                break;
            case "product":
                
                break;
            case "customer":
                salesInfoDetSrcCritaria.CustomerCode = code;
                //sEnquiryTitle = "Distributor";
                break;
            case "rep":
                salesInfoDetSrcCritaria.RepCode = code;
                break;
            case "route":
                salesInfoDetSrcCritaria.Route = code;
                break;
            default:
                break;
        }


        //SalesInfoDetailSearchCriteriaDTO salesInfoDetSrcCritaria = (SalesInfoDetailSearchCriteriaDTO)Session[CommonUtility.SALES_INFO_DETAIL];
        OriginatorClient originatorService = new OriginatorClient();
        SalesInfoDetailViewStateDTO oSalesInfoDetailViewState = new SalesInfoDetailViewStateDTO();
        //oSalesInfoDetailViewState = salesClient.GetSalesInfoDetailViewState(salesInfoDetSrcCritaria, argsDTO, "grid");

        argsDTO.Originator = UserSession.Instance.UserName;
        oSalesInfoDetailViewState = originatorService.GetRepMonthleyTargetsByDistributor(salesInfoDetSrcCritaria, argsDTO);


        switch (salesInfoDetSrcCritaria.DetailType)
        {
            case "market":
                salesInfoDetSrcCritaria.Market = "";
                break;
            case "brand":
                salesInfoDetSrcCritaria.Brand = "";
                break;
            case "product":

                break;
            case "customer":
                salesInfoDetSrcCritaria.CustomerCode = "";
                //sEnquiryTitle = "Distributor";
                break;
            case "rep":
                salesInfoDetSrcCritaria.RepCode = "";
                break;
            case "route":
                salesInfoDetSrcCritaria.Route = "";
                break;
            default:
                break;
        }



        List<TargetInfoDTO> targetInfList = oSalesInfoDetailViewState.TargetInfoEntityList;//originatorClient.GetRepMonthleyTargetsByDistributor(UserSession.Instance.UserName, salesInfoDetSrcCritaria.DetailType, argsDTO);
        List<double> targetList = new List<double>();
        List<double> actualList = new List<double>();

        if (targetInfList != null && targetInfList.Count > 0)
        {
            TargetInfoDTO targetInfoDTO = targetInfList[0];
            if (bIsAccumulated)
            {
                double accumulatedValue = 0;
                accumulatedValue = targetInfoDTO.TargetMonth1;
                targetList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.TargetMonth2;
                targetList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.TargetMonth3;
                targetList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.TargetMonth4;
                targetList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.TargetMonth5;
                targetList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.TargetMonth6;
                targetList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.TargetMonth7;
                targetList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.TargetMonth8;
                targetList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.TargetMonth9;
                targetList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.TargetMonth10;
                targetList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.TargetMonth11;
                targetList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.TargetMonth12;
                targetList.Add(accumulatedValue);

                accumulatedValue = 0;
                accumulatedValue = targetInfoDTO.ActualMonth1;
                actualList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.ActualMonth2;
                actualList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.ActualMonth3;
                actualList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.ActualMonth4;
                actualList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.ActualMonth5;
                actualList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.ActualMonth6;
                actualList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.ActualMonth7;
                actualList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.ActualMonth8;
                actualList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.ActualMonth9;
                actualList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.ActualMonth10;
                actualList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.ActualMonth11;
                actualList.Add(accumulatedValue);
                accumulatedValue += targetInfoDTO.ActualMonth12;
                actualList.Add(accumulatedValue);
            }
            else
            {
                targetList.Add(targetInfoDTO.TargetMonth1);
                targetList.Add(targetInfoDTO.TargetMonth2);
                targetList.Add(targetInfoDTO.TargetMonth3);
                targetList.Add(targetInfoDTO.TargetMonth4);
                targetList.Add(targetInfoDTO.TargetMonth5);
                targetList.Add(targetInfoDTO.TargetMonth6);
                targetList.Add(targetInfoDTO.TargetMonth7);
                targetList.Add(targetInfoDTO.TargetMonth8);
                targetList.Add(targetInfoDTO.TargetMonth9);
                targetList.Add(targetInfoDTO.TargetMonth10);
                targetList.Add(targetInfoDTO.TargetMonth11);
                targetList.Add(targetInfoDTO.TargetMonth12);

                actualList.Add(targetInfoDTO.ActualMonth1);
                actualList.Add(targetInfoDTO.ActualMonth2);
                actualList.Add(targetInfoDTO.ActualMonth3);
                actualList.Add(targetInfoDTO.ActualMonth4);
                actualList.Add(targetInfoDTO.ActualMonth5);
                actualList.Add(targetInfoDTO.ActualMonth6);
                actualList.Add(targetInfoDTO.ActualMonth7);
                actualList.Add(targetInfoDTO.ActualMonth8);
                actualList.Add(targetInfoDTO.ActualMonth9);
                actualList.Add(targetInfoDTO.ActualMonth10);
                actualList.Add(targetInfoDTO.ActualMonth11);
                actualList.Add(targetInfoDTO.ActualMonth12);
            }
            

            
        }

        double maxTargetVal = targetList.Max();
        double maxActualVal = actualList.Max();
        double maxVal = (maxTargetVal > maxActualVal ? maxTargetVal : maxActualVal);
        string series1 = "";
        string series2 = "";

        for (int i = 0; i < 12; i++)
        {
            double aPercentage = Math.Round(actualList[i] * 100 / maxVal, 2);
            double tPercentage = Math.Round(targetList[i] * 100 / maxVal, 2);
            if (i == 0)
            {
                series1 += aPercentage;
                series2 += tPercentage;
            }
            else
            {
                series1 += "," + aPercentage;
                series2 += "," + tPercentage;
            }
        }

        string series="";
        if (graph_type.Equals("Actual"))
        {
            series = "{ color :'#077695', name: 'Actual', data: [ ";
            series += series1;
            series += "]},";
        }
        else if (graph_type.Equals("Target"))
        {
            series += "{color :'#93c207', name: 'Target', data: [ ";
            series += series2;
            series += "]}";
        }
        else if (graph_type.Equals("Target/Actual"))
        {
            series = "{ color :'#077695', name: 'Actual', data: [ ";
            series += series1;
            series += "]},";
            series += "{color :'#93c207', name: 'Target', data: [ ";
            series += series2;
            series += "]}";
        }

       
        title = string.Format("{0} {1}, max value is {2}{3}", (bIsAccumulated ? "Accumulated" : ""), title, "",  maxVal.ToString());
        
        return TrendDataGraph(series, title);

    }
    /*
    private void LoadToPieChart(List<BSEPieChartValue> lstPieChartValues)
    {
        try
        {
            this.radChart1.SeriesMappings.Clear();
            radChart1.DefaultView.ChartArea.DataSeries.Clear();

            SeriesMapping seriesMapping = new SeriesMapping();
            seriesMapping.SeriesDefinition = new Bar3DSeriesDefinition();
            seriesMapping.ItemMappings.Add(new ItemMapping("Sales Info Details", DataPointMember.YValue));

            this.radChart1.SeriesMappings.Add(seriesMapping);

            DataSeries dSeries = new DataSeries();

            PieSeriesDefinition pie3DDefinition = new PieSeriesDefinition();
            //Pie3DSeriesDefinition pie3DDefinition = new Pie3DSeriesDefinition();

            pie3DDefinition.InteractivitySettings.HoverScope = InteractivityScope.Item;
            pie3DDefinition.InteractivitySettings.SelectionMode = ChartSelectionMode.Single;
            pie3DDefinition.InteractivitySettings.SelectionScope = InteractivityScope.Item;

            pie3DDefinition.LabelSettings.SpiderModeEnabled = true;
            pie3DDefinition.ShowItemLabels = true;
            pie3DDefinition.ShowItemToolTips = true;
            pie3DDefinition.LabelSettings.ShowConnectors = true;


            dSeries.Definition = pie3DDefinition;

            List<BSEPieChartValue> sortedValues = lstPieChartValues.OrderByDescending(V => V.Value).ToList<BSEPieChartValue>();

            List<BSEPieChartValue> selectedValues = sortedValues.Take(9).ToList();
            selectedValues.Add(new BSEPieChartValue() { Name = "Other", Value = sortedValues.Skip(9).Sum(V => V.Value) });

            double dValuesSum = selectedValues.Sum(A => A.Value);

            foreach (BSEPieChartValue bsePieChartVal in selectedValues.OrderByDescending(V => V.Value).ToList<BSEPieChartValue>())
            {
                double dPercentage = Math.Round((bsePieChartVal.Value / dValuesSum) * 100, 2);
                string sItemLabel = string.Format("{0} - {1}%", bsePieChartVal.Name, dPercentage);

                if (dPercentage > 0)
                {
                    DataPoint dPoint = new DataPoint(sItemLabel, dPercentage);
                    dPoint.Label = sItemLabel;
                    dPoint.LegendLabel = sItemLabel;
                    dPoint.Tooltip = sItemLabel;
                    dSeries.Add(dPoint);
                }
            }

            radChart1.DefaultView.ChartArea.DataSeries.Add(dSeries);
            radChart1.Rebind();



        }
        catch (Exception)
        {
            throw;
        }
    }
    */

    private string GetAllDBClaimInvoices(string disVal)
    {
        StringBuilder txt = new StringBuilder();
        DistributorClient distributerClient = new DistributorClient();
        ArgsDTO args = new ArgsDTO();

        if (UserSession.Instance != null)
            args.ChildOriginators = UserSession.Instance.ChildOriginators;

        args.AdditionalParams = " dept_string = 'DR'";

        args.StartIndex = ConfigUtil.StartIndex;
        args.RowCount = 1000;// ConfigUtil.MaxRowCount;
        args.OrderBy = " name asc ";

        List<DBClaimInvoiceHeaderModel> claimInvoiceList = new List<DBClaimInvoiceHeaderModel>();
        claimInvoiceList = distributerClient.GetDBClaimInvoicesByDistributerId(disVal);

        if (claimInvoiceList != null && claimInvoiceList.Count != 0)
        {
            txt.Append("<div style=\"height: 300px; max-width:700px; overflow-y: scroll;\">");
            txt.Append("<table border=\"0\" id=\"claimInvoiceTable\" width=\"860px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
            txt.Append("<tr class=\"detail-popsheader\">");
            txt.Append("<th></th>");
            txt.Append("<th>Invoice No</th><th>Invoice Date</th>");
            txt.Append("</tr>");

            for (int i = 0; i < claimInvoiceList.Count; i++)
            {
                txt.Append("<tr>");
                if (i == 0)
                {
                    txt.Append("<td style=\"padding-left:5px; width: 40px;\" class=\"tblborder\"> <input type=\"radio\" checked name=\"group1\" value=\"" + claimInvoiceList[i].DBClaimInvoiceNo + "\" id=\"a\"/> <input type=\"hidden\" name=\"group1\" value=\"" + claimInvoiceList[i].DBClaimInvoiceId + "\" id=\"b\"/>   </td>");
                }
                else
                {
                    txt.Append("<td style=\"padding-left:5px; width: 40px;\" class=\"tblborder\"> <input type=\"radio\" name=\"group1\" value=\"" + claimInvoiceList[i].DBClaimInvoiceNo + "\" id=\"a\"/> <input type=\"hidden\" name=\"group1\" value=\"" + claimInvoiceList[i].DBClaimInvoiceId + "\" id=\"b\"/>   </td>");

                }

                txt.Append("<td align=\"left\" class=\"tblborder \" style=\" width: 100px;\">" + claimInvoiceList[i].DBClaimInvoiceNo + "</td>");
                txt.Append("<td align=\"left\" class=\"tblborder \" style=\" width: 200px;\">" + claimInvoiceList[i].DBClaimInvoiceDate.ToString("dd-MMM-yyyy") + "</td>");
                txt.Append("</tr>");
            }

            txt.Append("</table>");
            txt.Append("</div>");
            txt.Append("<button id=\"jqi_state0_buttonOk\" class=\"k-button\">Ok</button>");
            txt.Append("<button id=\"jqi_state0_buttonCancel\" class=\"k-button\">Cancel</button>");
        }
        else
        {
            txt.Append("<table border=\"0\" id=\"originatorTable\" width=\"700px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
            txt.Append("<tr class=\"detail-popsheader\">");
            txt.Append("<th></th>");
            txt.Append("<th>Originator</th>");
            txt.Append("<th>Name</th>");
            txt.Append("</tr>");
            txt.Append("</table>");
        }

        return txt.ToString();
    }

    #endregion
}