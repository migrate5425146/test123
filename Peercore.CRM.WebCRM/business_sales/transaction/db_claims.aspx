﻿<%@ Page Title="Distributer Claims" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="db_claims.aspx.cs" Inherits="business_sales_transaction_db_claims" %>

<%@ MasterType TypeName="SiteMaster" %>
<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ Register Src="~/usercontrols/entrypage_tabs.ascx" TagPrefix="ucl2" TagName="tabentry" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />

    <div class="divcontectmainforms" style="color: black;">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content" style="padding-bottom: 0px;">
                <div class="hoributton">
                    <ucl:buttonbar ID="buttonbar" runat="server" />
                </div>
            </div>
            <div class="toolbar_right" id="div1">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 35%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/lead_customer/transaction/lead_contacts.aspx?cht=lead&ty=Customer">
            <div class="back"></div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div id="div_message" runat="server" style="display: none">
        </div>
        <div id="modalWindowInvoices" style="display: none">
        </div>
        <div>
            <div>
                <fieldset>
                    <legend>Search</legend>
                    <div class="formtextdiv">
                        Claim :
                    </div>
                    <div class="formdetaildiv_right">
                        <a href="#" id="id_search_claims" runat="server">
                            <img id="img1" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a>
                    </div>
                </fieldset>
            </div>
            <div>
                <fieldset>
                    <legend>Claim Details</legend>
                    <div class="formleft">
                        <div class="formtextdiv">
                            Claim No
                        </div>
                        <div class="formdetaildiv_right">
                            <span class="wosub mand">:
                                <asp:TextBox ID="txtClaimNo" runat="server" CssClass="input-large1" MaxLength="8" Text="<<Auto>>" ReadOnly="true"></asp:TextBox><span
                                    style="color: Red">*</span></span>
                        </div>
                        <div class="clearall">
                        </div>

                        <div class="formtextdiv">
                            DB Ref. No
                        </div>
                        <div class="formdetaildiv_right">
                            <span class="wosub mand">:
                                <asp:TextBox ID="txtDBRefNo" runat="server" CssClass="input-large1" TextMode="Password" MaxLength="16"></asp:TextBox><span
                                    style="color: Red">*</span></span>
                        </div>
                        <div class="clearall">
                        </div>

                    </div>
                    <div class="formright">
                        <div class="formtextdiv">
                            Claim Date
                        </div>
                        <div class="formdetaildiv_right">
                            <span class="wosub mand">:
                                <asp:TextBox ID="txtClaimDate" runat="server" CssClass="input-large1" MaxLength="100"></asp:TextBox><span
                                    style="color: Red">*</span></span>
                        </div>
                        <div class="clearall">
                        </div>

                        <div class="formtextdiv">
                            Invoice No
                        </div>
                        <div class="formdetaildiv_right">
                            <span class="wosub mand">:
                                <asp:TextBox ID="txtInvoiceNo" runat="server" CssClass="input-large1" ReadOnly="true"></asp:TextBox><span
                                    style="color: Red">*</span></span>
                            <a href="#" id="id_invoice_no" runat="server">
                                <img id="img2" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a>
                        </div>
                        <div class="clearall">
                        </div>

                    </div>
                    <div class="clearall">
                    </div>
                </fieldset>
            </div>
            <div>
                <fieldset>
                    <legend>Distributer Details</legend>
                    <div class="formleft">
                        <div class="formtextdiv">
                            DB Code
                        </div>
                        <div class="formdetaildiv_right">
                            :
                            <asp:TextBox ID="txtDBCode" runat="server" CssClass="input-large1" MaxLength="250" Text="501020" ReadOnly="true"></asp:TextBox>
                        </div>
                        <div class="clearall">
                        </div>

                        <div class="formtextdiv">
                            DB Name
                        </div>
                        <div class="formdetaildiv_right">
                            <span class="wosub mand">:
                                <asp:TextBox ID="txtDBName" runat="server" CssClass="input-large1" MaxLength="100" Text="Subhash Banerjee" ReadOnly="true"></asp:TextBox>
                            </span>
                        </div>
                    </div>
                    <div class="formright">
                        <div class="formtextdiv">
                            DB Mobile
                        </div>
                        <div class="formdetaildiv_right">
                            <span class="wosub mand">:
                                <asp:TextBox ID="txtDBMobile" runat="server" MaxLength="10" CssClass="input-large1-mobile" Text="081-64981" ReadOnly="true"></asp:TextBox>
                            </span>
                        </div>
                        <div class="clearall">
                        </div>

                        <div class="formtextdiv">
                            DB Email
                        </div>
                        <div class="formdetaildiv_right">
                            <span class="wosub mand">:
                                <asp:TextBox ID="txtDBEmail" runat="server" CssClass="input-large1" MaxLength="100" Text="subhashbanerjee@gmail.com" ReadOnly="true"></asp:TextBox>
                            </span>
                        </div>
                        <div class="clearall">
                        </div>

                        <div class="formtextdiv">
                            DB Address
                        </div>
                        <div class="formdetaildiv_right">
                            <span class="wosub mand">:
                                <asp:TextBox ID="txtDBAddress" runat="server" CssClass="input-large1" MaxLength="150" Text="Block-B, Pallabi, Mirpur-12, P.O. Box: 1216"></asp:TextBox>
                            </span>
                        </div>
                        <div class="clearall">
                        </div>
                    </div>
                </fieldset>
            </div>

            <div>
                <fieldset>
                    <legend>Product Details</legend>

                    <div class="grid_container" style="width: 100%">
                        <div id="StockGrid">
                        </div>
                        <div class="clearall">
                        </div>

                        <div>
                                <div class="formleft">
                                    <div class="formtextdiv">
                                        Inv. Total Qty
                                    </div>
                                    <div class="formdetaildiv_right">
                                        :
                                        <asp:TextBox ID="txtInvTotalQty" runat="server" CssClass="input-large1" MaxLength="250" ReadOnly="true"></asp:TextBox>
                                    </div>
                                    <div class="clearall">
                                    </div>

                                </div>
                                <div class="formright">
                                    <div class="formtextdiv">
                                        Claim Total Qty
                                    </div>
                                    <div class="formdetaildiv_right">
                                        <span class="wosub mand">:
                                        <asp:TextBox ID="txtClaimTotalQty" runat="server" MaxLength="10" CssClass="input-large1" ReadOnly="true"></asp:TextBox>
                                        </span>
                                    </div>
                                    <div class="clearall">
                                    </div>
                                </div>
                        </div>
                    </div>
                </fieldset>
            </div>

            <div>
                <fieldset>
                    <legend>Attached Images</legend>
                    <div class="formleft">
                        <input type="file" multiple id="gallery-photo-add">
                    </div>
                    <div class="formright">
                        <div class="gallery"></div>
                    </div>
                </fieldset>
            </div>
        </div>
    </div>

    <script src="../../assets/scripts/moment.js" type="text/javascript"></script>
    <script src="../../assets/scripts/moment-with-locales.js" type="text/javascript"></script>

    <script type="text/javascript">
        hideStatusDiv("MainContent_div_message");

        $(document).ready(function () {
            $("#MainContent_txtClaimDate").kendoDatePicker({
                format: "dd-MMM-yyyy"
            });

            var joinedDate = moment(new Date()).format('DD-MMM-YYYY');;
            $('#MainContent_txtClaimDate').val(joinedDate);

            GetClaimInvoiceDetails(-1);

        });

        $("#MainContent_id_invoice_no").click(function () {
            get_invoices_lookup('business_sales/process_forms/processmaster.aspx?fm=alldbclaiminvoices&type=select&disVal=1');
        });

        function get_invoices_lookup(targeturl) {
            $("#div_loader").show();
            var url = ROOT_PATH + targeturl;
            $.ajax({
                url: url,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    $("#div_loader").hide();
                    if (msg != "") {
                        var wnd = $("#modalWindowInvoices").kendoWindow({
                            title: "Invoice Details",
                            modal: true,
                            visible: false,
                            resizable: false,
                            width: 700
                        }).data("kendoWindow");

                        $("#modalWindowInvoices").html(msg);
                        wnd.center().open();

                        $("#jqi_state0_buttonOk").click(function () {

                            var invoiceId = 0;
                            var invoiceNo = '';

                            $('#claimInvoiceTable tr').each(function () {
                                if ($(this).children(':eq(0)').find('#a').is(':checked')) {
                                    invoiceNo = $(this).children(':eq(0)').find('#a').val();
                                    invoiceId = $(this).children(':eq(0)').find('#b').val();
                                }
                            });

                            $('#MainContent_txtInvoiceNo').val(invoiceNo);

                            wnd.close();

                            GetClaimInvoiceDetails(invoiceId);
                        });

                        $("#jqi_state0_buttonCancel").click(function () {
                            closeUsersPopup();
                        });
                    }
                },
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                        $("#div_loader").hide();
                    }
                }
            });
        }

        function closeUsersPopup() {
            var wnd = $("#modalWindowInvoices").kendoWindow({
                title: "Reps",
                modal: true,
                visible: false,
                resizable: false,
                width: 300
            }).data("kendoWindow");
            wnd.center().close();
        }

        function GetClaimInvoiceDetails(invoiceId) {
            var take_grid = $("#MainContent_hfPageIndex").val();
            $("#div_loader").show();

            $("#StockGrid").kendoGrid({
                height: 350,
                selectable: true,
                columns: [
                    { field: "DBClaimInvoiceId", title: "DBClaimInvoiceId", width: "35px", editable: false, hidden: true },
                    { field: "ProductId", title: "ProductId", width: "35px", editable: false, hidden: true },
                    { field: "ProductCode", title: "Product Code", width: "150px", editable: false, hidden: false },
                    { field: "ProductName", title: "Product Name", width: "300px", editable: false },
                    { field: "ProductPrice", title: "Product Price", width: "130px", format: "{0:n1}", editable: false, hidden: false },
                    { field: "ProductInvQty", title: "Inv. Qty.", width: "100px", format: "{0:n1}", editable: false },
                    { field: "ProductInvValue", title: "Inv. Value", width: "100px", format: "{0:n1}", editable: false },
                    { field: "ProductClaimQty", title: "Claim. Qty.", width: "120px", format: "{0:n1}", editable: true, editor: ClaimQtyEditor },
                    { field: "ProductClaimValue", title: "Claim. Value", width: "120px", format: "{0:n1}", editable: false },
                    { field: "Reason", title: "Reason", width: "250px", editable: false }
                ],
                editable: true,
                save: function (e) {
                    var claimTotQty = e.sender.dataSource.aggregates().ProductClaimQty.sum;
                    var invTotQty = e.sender.dataSource.aggregates().ProductInvQty.sum;

                    $('#MainContent_txtClaimTotalQty').val(claimTotQty);
                    $('#MainContent_txtInvTotalQty').val(invTotQty);
                },
                pageable: true,
                height: 400,
                sortable: true,
                filterable: true,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 100,
                    schema: {
                        data: "d.Data",
                        total: "d.Total",
                        model: {
                            fields: {
                                DBClaimInvoiceId: { type: "number", validation: { required: true } },
                                ProductId: { type: "number", validation: { required: true } },
                                ProductCode: { editable: false, validation: { required: true } },
                                ProductName: { editable: false, validation: { required: true } },
                                ProductPrice: { editable: false, type: "number", validation: { required: true } },
                                ProductInvQty: { editable: false, type: "number", validation: { required: false } },
                                ProductInvValue: { editable: false, type: "number", validation: { required: true } },
                                ProductClaimQty: { type: "number", validation: { required: true }, editable: true },
                                ProductClaimValue: { type: "number", validation: { required: true }, editable: false },
                                Reason: { editable: true, validation: { required: false } },
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/load_stock/load_stock.asmx/GetAllClaimInvoiceDetailsByInvoiceId",
                            contentType: "application/json; charset=utf-8",
                            data: { invoiceId: invoiceId, pgindex: take_grid },
                            type: "POST",
                            complete: function (jqXHR, textStatus) {
                                $("#div_loader").hide();
                                if (textStatus == "success") {
                                    var entityGrid = $("#StockGrid").data("kendoGrid");
                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));
                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    },
                    aggregate: [{ field: "ProductClaimQty", aggregate: "sum" }, { field: "ProductInvQty", aggregate: "sum" }]
                },
                dataBound: function (e) {
                    var claimTotQty = e.sender.dataSource.aggregates().ProductClaimQty.sum;
                    var invTotQty = e.sender.dataSource.aggregates().ProductInvQty.sum;

                    $('#MainContent_txtClaimTotalQty').val(claimTotQty);
                    $('#MainContent_txtInvTotalQty').val(invTotQty);
                },
            });
        }

        function ClaimQtyEditor(container, options) {
            $('<input type="text" name="ProductClaimQty" data-text-field="ProductClaimQty" data-value-field="ProductClaimQty" data-bind="value:' + options.field + '"/>')
                .appendTo(container)
                .kendoNumericTextBox({
                    format: "{0:n2}",
                    change: function changeallocated() {
                        settingallocationgridforClaimQty(options.model.uid, options.field);
                        //break;
                    }
                });
        }

        function settingallocationgridforClaimQty(uid, field) {
            var model = $("#StockGrid").data("kendoGrid").dataSource.getByUid(uid);

            if (model != undefined) {
                model.fields.ProductClaimValue.editable = true;
                if (model.ProductClaimQty <= model.ProductInvQty) {
                    var claimValue = model.ProductPrice * model.ProductClaimQty;
                    model.set("ProductClaimValue", claimValue);
                }
                else {
                    model.set("ProductClaimValue", 0);
                    alert('Claim quantity cannot be greater than invoice quantity.');
                }
                model.fields.ProductClaimValue.editable = false;
            }
        }

        $(function () {
            // Multiple images preview in browser
            var imagesPreview = function (input, placeToInsertImagePreview) {

                if (input.files) {
                    var filesAmount = input.files.length;

                    for (i = 0; i < filesAmount; i++) {
                        var reader = new FileReader();

                        reader.onload = function (event) {
                            $($.parseHTML('<img width="300">')).attr('src', event.target.result).appendTo(placeToInsertImagePreview);
                        }

                        reader.readAsDataURL(input.files[i]);
                    }
                }

            };

            $('#gallery-photo-add').on('change', function () {
                imagesPreview(this, 'div.gallery');
            });
        });
    </script>
</asp:Content>

