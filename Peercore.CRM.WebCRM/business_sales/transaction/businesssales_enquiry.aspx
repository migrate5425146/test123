﻿<%@ Page Title="mSales" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="businesssales_enquiry.aspx.cs" Inherits="business_sales_transaction_businesssales_enquiry" %>
<%@ Register Src="~/usercontrols/CustomerSalesinfo.ascx" TagPrefix="ucl1" TagName="Customersalesinfo" %>
<%@ MasterType TypeName="SiteMaster" %>

<asp:Content ID="HeaderContent" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" Runat="Server">

    <asp:HiddenField ID="HiddenFieldRepCode" runat="server" />
    <asp:HiddenField ID="HiddenFieldDetailType" runat="server" />


    <script type="text/javascript">
        $(document).ready(function () {
            loadSalesInfoTabs();
        });
    </script>
    <style type="text/css">
        #tabstrip
        {
            height: 100%;
            border-width: 0;
        }
        
    </style>
    <div>
        <div id="tabstrip">
            <ul id="ul_tabstrip">
                <li class="k-state-active">Customer</li>
                <li>End Users </li>
            </ul>
            <div>
                <div class="salestabs" style="display: none;">
                    <a href="#" class="tabslinks">Customer</a> <a href="#" class="tabslinks">End User</a>
                </div>
                <div class="toolbar2">
                    <a id="href_reports" class="sprite_button"><span class="copy icon"></span>Reports</a>
                    <a id="href_print" class="sprite_button"><span class="print icon"></span>Print</a>
                    <a id="href_close" class="sprite_button" style="float: right; margin-right: 10px;display:block;  border-top-left-radius: 3px; border-top-right-radius: 3px;"><span
                        class="leftarrow icon"></span>Back</a> <span id="div_row_count" class="rowcounttext">
                    </span>
                </div>
                <div class="toolbar3" runat="server" id="lblCurrentFlow">
                    Month : FEB | text1 | text2
                </div>
                <div class="toolbar4">
                    <asp:DropDownList ID="DropDownDisplayOption" runat="server" CssClass="dropdownwidth" Width="120px">
                    
                        <asp:ListItem Value="D" Text="Sales In Dollars" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="T" Text="Sales In Tonnes" ></asp:ListItem>
                        <asp:ListItem Value="U" Text="Sales In Units" ></asp:ListItem>
                        <%--<asp:ListItem Value="Tonnes" Text="Tonnes" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="Sales/Tonne" Text="Sales/Tonne" ></asp:ListItem>
                        <asp:ListItem Value="Sales vs Budget ($)" Text="Sales vs Budget ($)" ></asp:ListItem>
                        <asp:ListItem Value="Sales vs Budget (T)" Text="Sales vs Budget (T)" ></asp:ListItem>
                        <asp:ListItem Value="Last Sales vs Budget ($)" Text="Last Sales vs Budget ($)" ></asp:ListItem>
                        <asp:ListItem Value="Last Sales vs Budget (T)" Text="Last Sales vs Budget (T)" ></asp:ListItem>--%>
                    </asp:DropDownList>
                    <span>Sort by </span>
                    <asp:DropDownList ID="DropDownListSort" runat="server" CssClass="dropdownwidth">
                        <asp:ListItem Value="6" Text="MTD (U)" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="3" Text="MTD ($)"></asp:ListItem>
                        <asp:ListItem Value="9" Text="YTD (U)" ></asp:ListItem>
                        <asp:ListItem Value="10" Text="YTD ($)" ></asp:ListItem>
                        <asp:ListItem Value="11" Text="Last YTD (U)" ></asp:ListItem>
                        <asp:ListItem Value="12" Text="Last YTD ($)" ></asp:ListItem>
                        <asp:ListItem Value="1" Text="Code" ></asp:ListItem>
                    </asp:DropDownList>
                    <span>Group by </span>
                    <asp:DropDownList ID="DropDownGroupBy" runat="server" CssClass="dropdownwidth">
                        <asp:ListItem Value="bus area" Text="Bus. Area"></asp:ListItem>
                        <asp:ListItem Value="market" Text="Market"></asp:ListItem>
                        <asp:ListItem Value="brand" Text="Brand"></asp:ListItem>
                        <asp:ListItem Value="cat_group" Text="Cat Group"></asp:ListItem>
                        <asp:ListItem Value="cat_sub_group" Text="Sub Group"></asp:ListItem>
                        <asp:ListItem Value="product" Text="Product"></asp:ListItem>
                        <asp:ListItem Value="customer" Text="Customer"></asp:ListItem>
                        <asp:ListItem Value="custgroup" Text="Cust. Group"></asp:ListItem>
                        <asp:ListItem Value="repgroup" Text="Rep. Group"></asp:ListItem>
                        <asp:ListItem Value="rep" Text="Rep"></asp:ListItem>
                        <asp:ListItem Value="state" Text="State"></asp:ListItem>
                    </asp:DropDownList>
                    <div style="display: inline-block;">
                        <span>Selection</span>
                        <asp:TextBox ID="txtSelection" runat="server" ReadOnly="true" Width="100px"></asp:TextBox></div>
                </div>
                <div class="toolbar4">
                    <a href="javascript:SetSalesInfo('bus area','gridCustomerSales','GetBusinessSales','1')">Bus</a> | <a href="javascript:SetSalesInfo('market','gridCustomerSales','GetBusinessSales','1')">
                        Market</a> | <a href="javascript:SetSalesInfo('brand','gridCustomerSales','GetBusinessSales','1')">Brand</a> | <a href="javascript:SetSalesInfo('cat_group','gridCustomerSales','GetBusinessSales','1')">
                            Cat Group</a> | <a href="javascript:SetSalesInfo('cat_sub_group','gridCustomerSales','GetBusinessSales','1')">Sub Group</a>
                    | <a href="javascript:SetSalesInfo('product','gridCustomerSales','GetBusinessSales','1')">Product</a> | <a href="javascript:SetSalesInfo('customer','gridCustomerSales','GetBusinessSales','1')">
                        Customer</a> | <a href="javascript:SetSalesInfo('rep','gridCustomerSales','GetBusinessSales','1')">Rep</a>
                    <asp:Button ID="Button2" Text="All Reps" runat="server" CssClass="button" />
                </div>
                <div id="div_salesinfo" runat="server">
                    <ucl1:Customersalesinfo ID="custsalesinfo" runat="server" />
                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        hideStatusDiv("MainContent_div_message");

        $("#MainContent_DropDownDisplayOption").change(function () {
            LoadTitle($("#MainContent_HiddenFieldDetailType").val(), "n", "0", $("#MainContent_DropDownDisplayOption").val(), "", "gridCustomerSales", "GetBusinessSales")
            LoadSalesInfoChart();
        });

        $("#MainContent_DropDownGroupBy").change(function () {
            //alert($("#MainContent_DropDownGroupBy").val());
            SetSalesInfo($("#MainContent_DropDownGroupBy").val(), "gridCustomerSales", "GetBusinessSales");
            //LoadTitle($("#MainContent_DropDownGroupBy").val(), "n", "0", $("#MainContent_DropDownDisplayOption").val(), "")
        });

        $("#MainContent_DropDownListSort").change(function () {
            loadSortSalesInfoGrid( "gridCustomerSales", "GetBusinessSales","cust");
        });

        $(document).ready(function () {

            LoadTitle("rep", "n", "0", "T", "", "gridCustomerSales", "GetBusinessSales");

            $("#panelbarcontent").kendoPanelBar({
                expandMode: "single",
                select: onSelect
            });

            function onSelect(e) {
                var item = $(e.item)[0].id;
                //alert(item);

                if (item != undefined && item != '') {
                    switch (item) {
                        case "1":
                            loadSortSalesInfoGrid("gridCustomerSales", "GetBusinessSales", "cust");
                            break;
                        case "2":
                            LoadSalesInfoChart($("#MainContent_DropDownDisplayOption").val(), "cust");
                            break;
                        case "3":
                            LoadSalesInfoTrendChart($("#MainContent_txtSelection").val(), "cust");
                            break;
                    }
                }
            }

        });

        $("#MainContent_custsalesinfo_DropDownGraphType").change(function () {
            LoadSalesInfoTrendChart($("#MainContent_txtSelection").val(), "cust");
        });

        $("#MainContent_custsalesinfo_rbTrendGraphAccumulate").change(function () {
            LoadSalesInfoTrendChart($("#MainContent_txtSelection").val(), "cust");
        });

        $("#MainContent_custsalesinfo_rbTrendGraphMonthly").change(function () {
            LoadSalesInfoTrendChart($("#MainContent_txtSelection").val(), "cust");
        });

        $("#href_close").click(function () {
            LoadBackSalesInfo( "cust");
        });

        
        </script>

</asp:Content>

