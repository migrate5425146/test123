﻿<%@ Page Title="mSales - Territory" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="territoryentry.aspx.cs" Inherits="common_templates_master_territoryentry" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server" />
    <asp:HiddenField ID="saveDeleteOption" runat="server" />

    <asp:HiddenField ID="hfOriginator" runat="server" />
    <asp:HiddenField ID="hfOriginatorType" runat="server" />

    <asp:HiddenField ID="hdnSelectedTerritoryId" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSelectedRouteId" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSelectedTerritoryCode" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSelectedTerritoryName" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdnSelectedRouteCode" runat="server" ClientIDMode="Static" />

    <div id="div_main" class="divcontectmainforms">
        <div id="windowTerritory" style="display: none; width: 500px;">
            <div style="width: 100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td colspan="2" class="textalignbottom">Territory Code
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtTerritoryCode" maxlength="10" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Territory Name
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtTerritoryName" maxlength="70" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Area
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;
                                        <asp:DropDownList Height="20px" ID="ddlArea"
                                            CssClass="input-large1" runat="server">
                                        </asp:DropDownList>
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonSave" type="button">Save</button>
                                    <button class="k-button" id="buttonClear">Clear</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div id="windowRoute" style="display: none; width: 500px;">
            <div style="width: 100%" id="TerritoryEntry">
                <div id="div_text_area">
                    <div id="div1" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td colspan="2" class="textalignbottom">Route Code
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtRouteCode" maxlength="10" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Route Name
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtRouteName" maxlength="70" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Territory
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;
                                        <asp:DropDownList Height="20px" ID="ddlTerritory"
                                            CssClass="input-large1" runat="server">
                                        </asp:DropDownList>
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonSaveRoute" type="button">Save</button>
                                    <button class="k-button" id="buttonClearRoute">Clear</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div class="addlinkdiv" style="float: left" id="div_address">
                        <a id="id_add_territory" clientidmode="Static" runat="server" href="#">Add Territory</a>
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
            <div class="back"></div></asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div class="clearall">
        </div>
        <div style="min-width: 200px; overflow: auto">
            <div style="float: left; width: 100%; overflow: auto">
                <div id="gridAllTerritory"></div>

                <script type="text/x-kendo-template" id="templateTerritory">
                    <div class="tabstrip">
                        <ul>
                            <li class="k-state-active">
                                Route
                            </li>
                            <li>
                                SR Info
                            </li>
                        </ul>
                        <div>
                            <div class="territory-route"></div>
                        </div>
                        <div>
                            <div class='territory-sr'>
                                <%--<ul>
                                   <li><label>RSM Code:</label>#= RsmCode #</li>
                                    <li><label>RSM Name:</label>#= RsmName #</li>
                                   <li><label>User Name:</label>#= RsmUserName #</li>
                                    <li><label>Email:</label>#= RsmEmail #</li>
                                </ul>--%>
                            </div>
                        </div>
                    </div>
                </script>
            </div>
        </div>
    </div>

    <div id="popup">
        <div>
            <b>Enter Administrator Password:</b>
        </div>
        <input style="width: 100%;" id="adminPass" type="password" />
        <button type="button" onclick="confirmAdmin()">
            Confirm</button>
        <button type="button" onclick="exitAdmin()">
            Cancel</button>
    </div>


    <script type="text/javascript">

        var area_id;

        hideStatusDiv("MainContent_div_message");

        $(document).ready(function () {
            LoadAllTerritories();
            $("#txtTerritoryCode").focus();
        });

        //Load popup for Add new Brand
        $("#id_add_territory").click(function () {
            $("#windowTerritory").css("display", "block");

            ClearControlsTerritory()

            var wnd = $("#windowTerritory").kendoWindow({
                title: "Add New Territory",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            $("#txtTerritoryCode").focus();

            wnd.center().open();
        });

        //Save button click
        $("#buttonSave").click(function () {
            showPopupTerritory(0, 1);
        });

        $("#buttonSaveRoute").click(function () {
            showPopupRoute(0, 1);
        });

        //#region Admin Confirmation
        function showPopupTerritory(aID, del) {
            $("#<%= saveDeleteOption.ClientID %>").val(del);
            area_id = aID;
            $("#adminPass").val("");
            var OriginatorString = $("#<%= OriginatorString.ClientID %>").val();
            if (OriginatorString != "ADMIN") {
                document.getElementById("popup").style.display = "block";
            }
            else {
                if ($("#<%= saveDeleteOption.ClientID %>").val() == '0')
                    delete_selected_area(aID);
                if ($("#<%= saveDeleteOption.ClientID %>").val() == '1')
                    SaveTerritory();
            }
        }

        function showPopupRoute(aID, del) {
            $("#<%= saveDeleteOption.ClientID %>").val(del);
            area_id = aID;
            $("#adminPass").val("");
            var OriginatorString = $("#<%= OriginatorString.ClientID %>").val();
            if (OriginatorString != "ADMIN") {
                document.getElementById("popup").style.display = "block";
            }
            else {
                <%--if ($("#<%= saveDeleteOption.ClientID %>").val() == '0')
                    delete_selected_area(aID);--%>
                if ($("#<%= saveDeleteOption.ClientID %>").val() == '1')
                    SaveRouteFromTerritory();
            }
        }

        function confirmAdmin() {
            var password = document.getElementById("adminPass").value;
            var param = { "password": password };
            if (password == "") {
                var errorMsg = GetErrorMessageDiv("Please enter admin password", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);
                hideStatusDiv("MainContent_div_message");
                return;
            }

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/GetAdminPassword",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.hasOwnProperty('d')) {
                        msg = response.d;
                        if (msg == 'true') {
                            if ($("#<%= saveDeleteOption.ClientID %>").val() == '0')
                                delete_selected_area(area_id);
                            if ($("#<%= saveDeleteOption.ClientID %>").val() == '1')
                                SaveTerritory();
                            document.getElementById("popup").style.display = "none";
                        }
                        else {
                            var errorMsg = GetErrorMessageDiv("Invalid admin password", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(errorMsg);
                            hideStatusDiv("MainContent_div_message");
                            $("#adminPass").val("");
                        }
                    } else {
                        msg = response;

                        var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        hideStatusDiv("MainContent_div_message");
                        $("#adminPass").val("");
                    }
                },
                error: function (response) {
                }
            });
        }

        function exitAdmin() {
            document.getElementById("popup").style.display = "none";
            return false;
        }

        //#endregion

        $("#buttonClear").click(function () {
            ClearControlsTerritory();
        });

        $("#buttonClearRoute").click(function () {
            ClearControlsRoute();
        });

        function closepopupTerritory() {
            var wnd = $("#windowTerritory").kendoWindow({
                title: "Add/Edit Territory",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#windowTerritory").css("display", "none");
        }

        function closepopupRoute() {
            var wnd = $("#windowRoute").kendoWindow({
                title: "Add/Edit Route",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#windowRoute").css("display", "none");
        }

        function ClearControlsTerritory() {
            $("#hdnSelectedTerritoryId").val('');
            $("#txtTerritoryCode").val('');
            $("#txtTerritoryName").val('');
            $("#hdnSelectedTerritoryCode").val('');
            $("#hdnSelectedTerritoryName").val('');
        }

        function ClearControlsRoute() {
            $("#hdnSelectedCompId").val('1');
            $("#hdnSelectedTerritoryId").val('');
            $("#hdnSelectedRouteId").val('');
            $("#txtRouteCode").val('');
            $("#txtRouteName").val('');
        }

    </script>
</asp:Content>

