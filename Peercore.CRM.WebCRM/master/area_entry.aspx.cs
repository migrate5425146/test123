﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;

public partial class common_templates_master_area_entry : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
            }

            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Area Entry ", "#", "");
            LoadOptions();
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        OriginatorString.Value = UserSession.Instance.OriginatorString;
    }

    private void LoadOptions()
    {
        MasterClient masterlient = new MasterClient();

        ArgsModel args = new ArgsModel();
        args.StartIndex = 1;
        args.RowCount = 10000;
        args.OrderBy = " region_id asc";

        List<RegionModel> regionList = new List<RegionModel>();
        regionList = masterlient.GetAllRegions(args);
        if (regionList.Count != 0)
        {
            ddlRegion.Items.Clear();
            ddlRegion.Items.Add(new ListItem("- Select Region -", "0"));
            foreach (RegionModel item in regionList)
            {
                var strLength = item.RegionCode.Length;
                string result = item.RegionCode.PadRight(strLength + 20).Substring(0, strLength + 20);

                ddlRegion.Items.Add(new ListItem(result + "  |  " + item.RegionName, item.RegionId.ToString()));
            }
            ddlRegion.Enabled = true;
        }

        args = new ArgsModel();
        args.StartIndex = 1;
        args.RowCount = 10000;
        args.OrderBy = " area_id asc";

        List<AreaModel> objList = new List<AreaModel>();
        objList = masterlient.GetAllAreaNew(args);

        if (objList.Count != 0)
        {
            ddlArea.Items.Clear();
            ddlArea.Items.Add(new ListItem("- Select Area -", "0"));
            foreach (AreaModel item in objList)
            {
                var strLength = (item.AreaCode == null) ? 0 : item.AreaCode.Length;
                string result = (item.AreaCode == null) ? "" : item.AreaCode.PadRight(strLength + 20).Substring(0, strLength + 20);

                ddlArea.Items.Add(new ListItem(result + "  |  " + item.AreaName, item.AreaId.ToString()));
            }

            ddlArea.Enabled = true;
        }
    }
}