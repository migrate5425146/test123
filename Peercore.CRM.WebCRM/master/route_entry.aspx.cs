﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using CRMServiceReference;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using System.Web.UI.HtmlControls;

public partial class call_cycle_transaction_route_entry : PageBase
{
    CommonUtility commonUtility = new CommonUtility();

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (UserSession.Instance.OriginatorString == "ASE")
        //{
        //    buttonbar.onButtonSendMail = new usercontrols_buttonbar.ButtonSendMail(ButtonSendMail_Click);
        //    buttonbar.VisibleSendMail(true);
        //}

        hfOriginator.Value = UserSession.Instance.UserName;

        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            buttonbarNavi.onButtonNext = new usercontrols_buttonbar_navigation.ButtonNext(buttonNext_Click);
            buttonbarNavi.onButtonPrevious = new usercontrols_buttonbar_navigation.ButtonPrevious(buttonPrevious_Click);

            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Route Entry ", "#", "");

            if (!IsPostBack)
            {
                //div_autocomplete.InnerHtml = SetAutocomplete_Temp();
                //if (UserSession.Instance.OriginatorString.Equals("ASE"))
                //    div_search_panel_tme.Visible = true;
                //else
                //    div_search_panel_tme.Visible = false;

                //
            }
            
            LoadOptions();

            hfOriginatorType.Value = UserSession.Instance.OriginatorString;
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    protected void ButtonSendMail_Click(object sender)
    {
        CallCycleClient callcycleClient = new CallCycleClient();
        bool status = false;
        CommonUtility commmonUtil = new CommonUtility();
        StringBuilder builder = new StringBuilder();

        List<string> tolist = new List<string>();
        tolist.Add(UserSession.Instance.OriginatorEmail);
        List<string> cclist = new List<string>();
        cclist.Add("iroshf@peercore.com.au");

        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name";
        args.StartIndex = 1;
        args.RowCount = 1000;

        List<RouteMasterDTO> routeList = callcycleClient.GetAllTMERoutesMaster(UserSession.Instance.OriginalUserName, args);

        string MessageBody = "";

        foreach (RouteMasterDTO list in routeList)
        {
            builder.Append(list.RouteName);
            builder.AppendLine("</br>");
        }

        builder.AppendLine("</br>");
        builder.Append("<div style=\"Color:#b7b7b7\">* This is a system generated mail, Please do not reply.</div>");

        MessageBody = builder.ToString();

        List<string> attachementPathList = new List<string>();
        status = commmonUtil.SendEmailOut(tolist, cclist, UserSession.Instance.Originator + " - ASM Route List  ", MessageBody, attachementPathList);

        if (status == true)
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetSucessfullMessage("Mail Sent.");
        }
        else
        {
            div_message.Attributes.Add("style", "display:block;padding:2px");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Error Occured. Mail did not delivered.");
            return;
        }
    }

    private string SetAutocomplete_Temp()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script type=\"text/javascript\">");
        sb.Append("     $(function () {");
        sb.Append("         $(\".tb\").autocomplete({");
        sb.Append("             source: function (request, response) {");
        sb.Append("                 $.ajax({");
        sb.Append("                     type: \"POST\",");
        sb.Append("                     contentType: \"application/json; charset=utf-8\",");
        sb.Append("                     url: \"" + ConfigUtil.ApplicationPath + "service/call_cycle/call_cycle_service.asmx/GetRepsByOriginator\",");
        sb.Append("                     data: \"{'name':'\" + request.term + \"'}\",");
        sb.Append("                     dataType: \"json\",");
        sb.Append("                     async: true,");
        sb.Append("                     dataFilter: function(data) {");
        sb.Append("                           return data;");
        sb.Append("                     },");
        sb.Append("                     success: function (data) {");
        sb.Append("                     response($.map(data.d, function(item) {");
        sb.Append("                     return {");
        sb.Append("                         label: item.Name,");
        sb.Append("                         desc: item.RepCode");
        sb.Append("                     };");
        sb.Append("                     }));");
        sb.Append("                     },");
        sb.Append("                     error: function (result) {");
        sb.Append("                     }");
        sb.Append("                 });");
        sb.Append("             },");
        sb.Append("             minLength: 2,");
        sb.Append("            select: function( event, ui ) {");
        sb.Append("                  var selectedObj = ui.item;");
        //sb.Append("                 $('#MainContent_HiddenFieldCustomerIndustry').val(selectedObj.industry);");
        //sb.Append("                 $('#MainContent_HiddenFieldCustomerCode').val(selectedObj.label);");
        //sb.Append("                 $('#MainContent_HiddenFieldCustCode').val(selectedObj.label);");
        sb.Append("                 $('#MainContent_txtDRName').val(selectedObj.label);");
        sb.Append("                 setRepCode(selectedObj.desc);");
        //sb.Append("                 setcuscode(selectedObj.label,selectedObj.desc,'0');");
        //k   sb.Append("                 loadproductlist(selectedObj.label);");
        //sb.Append("                 $('ul.ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all').css('display','none');");
        sb.Append("            }");
        sb.Append("         });");
        sb.Append("     });");
        sb.Append("</script>");
        sb.Append("");

        return sb.ToString();
    }

    private void LoadOptions()
    {
        MasterClient masterlient = new MasterClient();

        ArgsModel args = new ArgsModel();
        args.StartIndex = 1;
        args.RowCount = 10000;
        args.OrderBy = " area_id asc";

        List<TerritoryModel> objTrrList = new List<TerritoryModel>();
        objTrrList = masterlient.GetAllTerritoryByOriginator(args, UserSession.Instance.OriginatorString,
                                                                                UserSession.Instance.UserName);
        if (objTrrList.Count != 0)
        {
            ddlTerritory.Items.Clear();
            ddlTerritory.Items.Add(new ListItem("- Select Territory -", "0"));
            foreach (TerritoryModel item in objTrrList)
            {
                var strLength = (item.TerritoryCode == null) ? 0 : item.TerritoryCode.Length;
                string result = (item.TerritoryCode == null) ? "" : item.TerritoryCode.PadRight(strLength + 20).Substring(0, strLength + 20);

                ddlTerritory.Items.Add(new ListItem(result + "  |  " + item.TerritoryName, item.TerritoryId.ToString()));
            }

            ddlTerritory.Enabled = true;
        }

        args.OrderBy = " route_master_name asc";

        List<RouteMasterModel> routeList = new List<RouteMasterModel>();
        routeList = masterlient.GetAllRoutesMaster(args);
        if (routeList.Count != 0)
        {
            ddlRoute.Items.Clear();
            ddlRoute.Items.Add(new ListItem("- Select Route -", "0"));
            foreach (RouteMasterModel item in routeList)
            {
                if (item.RouteMasterCode == null)
                {
                    item.RouteMasterCode = "";
                }

                var strLength = item.RouteMasterCode.Length;
                string result = item.RouteMasterCode.PadRight(strLength + 20).Substring(0, strLength + 20);

                ddlRoute.Items.Add(new ListItem(result + "  |  " + item.RouteMasterName, item.RouteMasterId.ToString()));
            }
            ddlRoute.Enabled = true;
        }
    }

    //private void LoadOptions_old()
    //{
    //    //markets
    //    CommonClient commonClient = new CommonClient();
    //    CustomerClient customerClient = new CustomerClient();
    //    OriginatorClient originatorClient = new OriginatorClient();
    //    ArgsDTO args2 = new ArgsDTO();
    //    args2.OrderBy = " market_name asc ";
    //    args2.AdditionalParams = " originator = '" + UserSession.Instance.UserName + "'";
    //    args2.StartIndex = 1;
    //    args2.RowCount = 300;

    //    List<MarketDTO> marketList = commonClient.GetAllMarket(args2);
    //    marketList.Insert(0, new MarketDTO() { MarketName = "All", MarketId = 0 });

    //    DropDownListMarkets.DataSource = marketList;
    //    DropDownListMarkets.DataTextField = "MarketName";
    //    DropDownListMarkets.DataValueField = "MarketId";
    //    DropDownListMarkets.DataBind();

    //    //distributors
    //    //args2 = new ArgsDTO();
    //    //args2.OrderBy = " name asc ";
    //    //args2.AdditionalParams = "";
    //    //args2.StartIndex = 1;
    //    //args2.RowCount = 300;

    //    List<OriginatorDTO> distributors = originatorClient.GetParentOriginators(UserSession.Instance.UserName);
    //    distributors.Insert(0, new OriginatorDTO() { Name = "All", UserName = "0" });

    //    DropDownListDistributors.DataSource = distributors;
    //    DropDownListDistributors.DataTextField = "Name";
    //    DropDownListDistributors.DataValueField = "Username";
    //    DropDownListDistributors.DataBind();

    //    //retail type
    //    //args2 = new ArgsDTO();
    //    //args2.OrderBy = " name asc ";
    //    //args2.AdditionalParams = "";
    //    //args2.StartIndex = 1;
    //    //args2.RowCount = 300;

    //    List<KeyValuePairOfintstring> retailTypes = customerClient.GetAllOutletTypes();
    //    KeyValuePairOfintstring kv = new KeyValuePairOfintstring();
    //    kv.key = 0;
    //    kv.value = "All";
    //    retailTypes.Insert(0, kv);
    //    //DropDownListRetailType.Items.Clear();
    //    //foreach (KeyValuePair<int, string> item in retailTypes)
    //    //{
    //    //    DropDownListRetailType.Items.Add(new ListItem(item, item));
    //    //    DropDownListRetailType.SelectedIndex = 0;
    //    //}
    //    DropDownListRetailType.DataSource = retailTypes;
    //    DropDownListRetailType.DataTextField = "Value";
    //    DropDownListRetailType.DataValueField = "Key";
    //    DropDownListRetailType.DataBind();

    //    //town
    //    //args2 = new ArgsDTO();
    //    //args2.OrderBy = " name asc ";
    //    //args2.AdditionalParams = "";
    //    //args2.StartIndex = 1;
    //    //args2.RowCount = 300;

    //    List<KeyValuePairOfstringstring> towns = customerClient.GetAllTowns();
    //    KeyValuePairOfstringstring kv1 = new KeyValuePairOfstringstring();
    //    kv1.key = "0";
    //    kv1.value = "All";
    //    towns.Insert(0, kv1);
    //    //DropDownListTown.Items.Clear();
    //    //foreach (string item in towns)
    //    //{
    //    //    DropDownListTown.Items.Add(new ListItem(item, item));
    //    //    DropDownListTown.SelectedIndex = 0;
    //    //}
    //    DropDownListTown.DataSource = towns;
    //    DropDownListTown.DataTextField = "Value";
    //    DropDownListTown.DataValueField = "Key";
    //    DropDownListTown.DataBind();
    //}

    protected void buttonNext_Click(object sender)
    {
        Response.Redirect(ConfigUtil.ApplicationPath + "call_cycle/transaction/route_seqence.aspx");
    }

    protected void buttonPrevious_Click(object sender)
    {
        Response.Redirect(ConfigUtil.ApplicationPath + "calendar/transaction/holiday_entry.aspx");
    }
}