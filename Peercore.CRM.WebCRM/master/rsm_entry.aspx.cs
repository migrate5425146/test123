﻿using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class common_templates_master_rsm_entry : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
            }

            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("RSM Entry ", "#", "");
            LoadOptions();
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        OriginatorString.Value = UserSession.Instance.OriginatorString;
        hfRSMOriginator.Value = UserSession.Instance.UserName;
    }

    private void LoadOptions()
    {
        MasterClient masterlient = new MasterClient();

        ArgsModel args = new ArgsModel();
        args.StartIndex = 1;
        args.RowCount = 10000;
        args.OrderBy = " region_id asc";

        List<RegionModel> regionList = new List<RegionModel>();
        regionList = masterlient.GetAllRegions(args);
        if (regionList.Count != 0)
        {
            ddlRegion.Items.Clear();
            ddlRegion.Items.Add(new ListItem("- Select Region -", "0"));
            foreach (RegionModel item in regionList)
            {
                var strLength = item.RegionCode.Length;
                string result = item.RegionCode.PadRight(strLength + 20).Substring(0, strLength + 20);

                ddlRegion.Items.Add(new ListItem(result + "  |  " + item.RegionName, item.RegionId.ToString()));
            }
            ddlRegion.Enabled = true;
        }
    }
}