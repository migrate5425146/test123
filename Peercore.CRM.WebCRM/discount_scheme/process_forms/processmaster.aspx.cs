﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Web.Services;
using System.Xml.Serialization;
using System.Configuration;
using CRMServiceReference;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class discount_scheme_process_forms_processmaster : System.Web.UI.Page
{
    private SchemeHeaderDTO SchemeHeader
    {
        get
        {
            if (Session[CommonUtility.SCHEME_HEADER] != null)
            {
                return (SchemeHeaderDTO)Session[CommonUtility.SCHEME_HEADER];
            }
            return new SchemeHeaderDTO();
        }
        set
        {
            Session[CommonUtility.SCHEME_HEADER] = value;
        }
    }

    private int SchemeDetailId
    {
        get
        {
            if (Session[CommonUtility.SCHEME_DETAIL_ID] != null)
            {
                return (int)Session[CommonUtility.SCHEME_DETAIL_ID];
            }
            return 0;
        }
        set
        {
            Session[CommonUtility.SCHEME_DETAIL_ID] = value;
        }
    }

    //private List<FreeProductDTO> FreeProductList
    //{
    //    get
    //    {
    //        if (Session[CommonUtility.SCHEME_FREE_PRODUCT] != null)
    //        {
    //            return (List<FreeProductDTO>)Session[CommonUtility.SCHEME_FREE_PRODUCT];
    //        }
    //        return new List<FreeProductDTO>();
    //    }
    //    set
    //    {
    //        Session[CommonUtility.SCHEME_FREE_PRODUCT] = value;
    //    }
    //}
    private SchemeDetailsDTO SchemeDetail
    {
        get
        {
            if (Session[CommonUtility.SCHEME_DETAILS] != null)
            {
                return (SchemeDetailsDTO)Session[CommonUtility.SCHEME_DETAILS];
            }
            return new SchemeDetailsDTO();
        }
        set
        {
            Session[CommonUtility.SCHEME_DETAILS] = value;
        }
    }

    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != "")
        {
            if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
            {
                if (Request.QueryString["type"] == "Update")
                {
                    if (Request.QueryString["fm"] == "leadCustomer")
                    {
                        //Response.Expires = -1;
                        //Response.Clear();
                        //string saveHeaderText = UpdateHeaderText();
                        //Response.ContentType = "text/html";
                        ////Response.Write(saveHeaderText);
                        //Response.End();
                    }
                    else if (Request.QueryString["fm"] == "CheckSchemeActiveInactive")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = UpdateSchemeHeaderActiveInactive();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }

                }
                else if (Request.QueryString["type"] == "Delete")
                {

                }
                else if (Request.QueryString["type"] == "query")
                {
                    if (Request.QueryString["fm"] == "addfreeproduct")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = AddFreeProduct();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }
                    if (Request.QueryString["fm"] == "removefreeproduct")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = RemoveFreeProduct();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }
                    if (Request.QueryString["fm"] == "addoptionlevel")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = AddOptionLevel();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }
                    if (Request.QueryString["fm"] == "removeoptionlevel")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = RemoveOptionLevel();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }
                    if (Request.QueryString["fm"] == "addschemedetail")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = AddSchemeDetail();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }
                    if (Request.QueryString["fm"] == "addcombination")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = AddCombination();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }
                    if (Request.QueryString["fm"] == "removecombination")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = RemoveCombination();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }
                    if (Request.QueryString["fm"] == "loadschemedetail")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = LoadSchemeDetail();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    } if (Request.QueryString["fm"] == "newschemedetail")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = NewSchemeDetail();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }if (Request.QueryString["fm"] == "removeschemedetail")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = RemoveSchemedetail();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }
                    if (Request.QueryString["fm"] == "printdiscountscheme")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string InsertLeadstageQuery = PrintDiscountScheme();
                        Response.ContentType = "text/html";
                        Response.Write(InsertLeadstageQuery);
                        Response.End();
                    }

                    
                }
                else if (Request.QueryString["type"] == "get")
                {

                }
                else if (Request.QueryString["type"] == "change")
                {

                }
                else if (Request.QueryString["type"] == "insert")
                {

                }
            }
        }
    }
    #endregion

    #region Methods

    public string AddFreeProduct()
    {
        try
        {
            int productId = string.IsNullOrEmpty(Request.QueryString["id"]) ? 0 : int.Parse(Request.QueryString["id"]);
            string pname = Request.QueryString["name"];
            int qty = string.IsNullOrEmpty(Request.QueryString["qty"]) ? 0 : int.Parse(Request.QueryString["qty"]);
            int ptypeid = string.IsNullOrEmpty(Request.QueryString["ptypeid"]) ? 0 : int.Parse(Request.QueryString["ptypeid"]);
            string ptype = Request.QueryString["ptype"];


            List<FreeProductDTO> productList = SchemeDetail.FreeProductList;
            if (productList == null)
                productList = new List<FreeProductDTO>();
            if (!productList.Any(c => c.ProductId == productId))
            {
                productList.Add(new FreeProductDTO()
                {
                    ProductId = productId,
                    ProductName = pname,
                    Qty = qty,
                    ProductPackingId = ptypeid,
                    ProductPackingName = ptype
                });
            }
            else
            {
                FreeProductDTO item = productList.FirstOrDefault(c => c.ProductId == productId);
                item.ProductId = productId;
                item.ProductName = pname;
                item.Qty = qty;
                item.ProductPackingId = ptypeid;
                item.ProductPackingName = ptype;
            }
            SchemeDetailsDTO schemeDetail = SchemeDetail;
            schemeDetail.FreeProductList = productList;
            SchemeDetail = schemeDetail;
        }
        catch (Exception ex)
        {
        }
        return "1";
    }

    public string RemoveFreeProduct()
    {
        try
        {
            int productId = string.IsNullOrEmpty(Request.QueryString["id"]) ? 0 : int.Parse(Request.QueryString["id"]);

            //List<FreeProductDTO> productList = FreeProductList;
            List<FreeProductDTO> productList = SchemeDetail.FreeProductList;
            if (productList == null)
                productList = new List<FreeProductDTO>();

            if (productList.Any(c => c.ProductId == productId))
            {
                productList.RemoveAll(c => c.ProductId == productId);
            }
            //FreeProductList = productList;

            SchemeDetailsDTO schemeDetail = SchemeDetail;
            schemeDetail.FreeProductList = productList;
            SchemeDetail = schemeDetail;
        }
        catch (Exception ex)
        {
        }
        return "1";
    }

    public string AddOptionLevel()
    {
        try
        {
            int id = string.IsNullOrEmpty(Request.QueryString["id"]) ? 0 : int.Parse(Request.QueryString["id"]);
            string brand = Request.QueryString["brand"];
            string packt = Request.QueryString["packt"];
            string fla = Request.QueryString["fla"];
            string itemg = Request.QueryString["itemg"];

            int brandid = string.IsNullOrEmpty(Request.QueryString["brandid"]) ? 0 : int.Parse(Request.QueryString["brandid"]);
            int packtid = string.IsNullOrEmpty(Request.QueryString["packtid"]) ? 0 : int.Parse(Request.QueryString["packtid"]);
            int flaid = string.IsNullOrEmpty(Request.QueryString["flaid"]) ? 0 : int.Parse(Request.QueryString["flaid"]);
            int itemgid = string.IsNullOrEmpty(Request.QueryString["itemgid"]) ? 0 : int.Parse(Request.QueryString["itemgid"]);

            double linecount = string.IsNullOrEmpty(Request.QueryString["linecount"]) ? 0 : double.Parse(Request.QueryString["linecount"]);
            double minq = string.IsNullOrEmpty(Request.QueryString["minq"]) ? 0 : double.Parse(Request.QueryString["minq"]);
            double maxq = string.IsNullOrEmpty(Request.QueryString["maxq"]) ? 0 : double.Parse(Request.QueryString["maxq"]);
            double minv = string.IsNullOrEmpty(Request.QueryString["minv"]) ? 0 : double.Parse(Request.QueryString["minv"]);
            double maxv = string.IsNullOrEmpty(Request.QueryString["maxv"]) ? 0 : double.Parse(Request.QueryString["maxv"]);

            int ishvp = string.IsNullOrEmpty(Request.QueryString["ishvp"]) ? 0 : int.Parse(Request.QueryString["ishvp"]);
            int isbill = string.IsNullOrEmpty(Request.QueryString["isbill"]) ? 0 : int.Parse(Request.QueryString["isbill"]);

            int opeid = string.IsNullOrEmpty(Request.QueryString["opeid"]) ? 0 : int.Parse(Request.QueryString["opeid"]);
            int isfor = string.IsNullOrEmpty(Request.QueryString["isfor"]) ? 0 : int.Parse(Request.QueryString["isfor"]);

            SchemeDetailsDTO schemeDetails = SchemeDetail;
            List<OptionLevelDTO> optionLevelList = new List<OptionLevelDTO>();
            if (schemeDetails != null && schemeDetails.OptionLevelList != null)
            {
                optionLevelList = schemeDetails.OptionLevelList;
            }

            if (!optionLevelList.Any(c => c.IndexId == id))
            {
                int maxIndex = 0;
                if (optionLevelList.Count > 0)
                    maxIndex = optionLevelList.Max(c => c.IndexId);
                optionLevelList.Add(new OptionLevelDTO()
                {
                    ProductId = 1,
                    ProductName = "",
                    BrandId = brandid,
                    BrandName = brand,
                    ProductPackingName = packt,
                    ProductPackingId = packtid,
                    ProductCatagoryName = itemg,
                    ProductCategoryId = itemgid,
                    CatlogName = fla,
                    CatlogId = flaid,
                    MaxQuantity = maxq,
                    MaxValue = maxv,
                    MinQuantity = minq,
                    MinValue = minv,
                    LineCount = linecount,
                    IsBill = isbill == 1,
                    IsHvp = ishvp == 1,
                    OperatorId = opeid,
                    IndexId = maxIndex + 1,
                    IsForEvery = isfor == 1
                });
            }
            else
            {
                OptionLevelDTO item = optionLevelList.FirstOrDefault(c => c.IndexId == id);
                item.BrandId = brandid;
                item.BrandName = brand;
                item.ProductPackingName = packt;
                item.ProductPackingId = packtid;
                item.ProductCatagoryName = itemg;
                item.ProductCategoryId = itemgid;
                item.CatlogName = fla;
                item.CatlogId = flaid;
                item.MaxQuantity = maxq;
                item.MaxValue = maxv;
                item.MinQuantity = minq;
                item.MinValue = minv;
                item.LineCount = linecount;
                item.IsBill = isbill == 1;
                item.IsHvp = ishvp == 1;
                item.OperatorId = opeid;
                item.IsForEvery = isfor == 1;
            }

            schemeDetails.OptionLevelList = optionLevelList;
            SchemeDetail = schemeDetails;
        }
        catch (Exception)
        {
        }
        return "1";
    }

    public string RemoveOptionLevel()
    {
        try
        {
            int id = string.IsNullOrEmpty(Request.QueryString["id"]) ? 0 : int.Parse(Request.QueryString["id"]);

            SchemeDetailsDTO schemeDetails = new SchemeDetailsDTO();

            schemeDetails = SchemeDetail;
            List<OptionLevelDTO> optionLevelList = new List<OptionLevelDTO>();
            if (schemeDetails != null && schemeDetails.OptionLevelList != null)
            {
                optionLevelList = schemeDetails.OptionLevelList;
            }

            if (optionLevelList.Any(c => c.IndexId == id))
            {
                optionLevelList.RemoveAll(c => c.IndexId == id);
            }
            schemeDetails.OptionLevelList = optionLevelList;
            SchemeDetail = schemeDetails;
        }
        catch (Exception ex)
        {
        }
        return "1";
    }

    public string AddSchemeDetail()
    {
        try
        {
            int id = string.IsNullOrEmpty(Request.QueryString["id"]) ? 0 : int.Parse(Request.QueryString["id"]);

            double per = string.IsNullOrEmpty(Request.QueryString["per"]) ? 0 : double.Parse(Request.QueryString["per"]);
            double val = string.IsNullOrEmpty(Request.QueryString["val"]) ? 0 : double.Parse(Request.QueryString["val"]);

            int stypeid = string.IsNullOrEmpty(Request.QueryString["stypeid"]) ? 0 : int.Parse(Request.QueryString["stypeid"]);
            string stype = Request.QueryString["stype"];
            string freeprodcond = Request.QueryString["freeprodcond"];
            int dtypeid = string.IsNullOrEmpty(Request.QueryString["dtypeid"]) ? 0 : int.Parse(Request.QueryString["dtypeid"]);
            string dtype = Request.QueryString["dtype"];
            int isret = string.IsNullOrEmpty(Request.QueryString["isret"]) ? 0 : int.Parse(Request.QueryString["isret"]);
            string stext = Request.QueryString["stext"];
            string freeisshuecondition = Request.QueryString["freeisshuecondition"];

            SchemeDetailsDTO schemeDetails = SchemeDetail;

            schemeDetails.SchemeTypeId = stypeid;
            schemeDetails.DiscountPercentage = per;
            schemeDetails.DiscountValue = val;
            schemeDetails.DetailsTypeId = dtypeid;
            schemeDetails.FreeIssueCondition = freeisshuecondition;

            if (isret != 2)
            {
                schemeDetails.IsRetail = isret == 1;
            }
            else
            {
                schemeDetails.IsRetail = null;
            }
            schemeDetails.SchemeDetails = stext;
            schemeDetails.SchemeTypeName = stype;
            schemeDetails.DetailsTypeName = dtype;

            SchemeHeaderDTO scheme = SchemeHeader;
            if (scheme.SchemeDetailsList == null)
            {
                scheme.SchemeDetailsList = new List<SchemeDetailsDTO>();
            }

            int maxIndex = 0;
            if (scheme.SchemeDetailsList.Count > 0)
                maxIndex = scheme.SchemeDetailsList.Max(c => c.IndexId);

            if (schemeDetails.IndexId == 0)
            {
                //if (scheme.SchemeDetailsList.Count > 0)
                //    schemeDetails.SchemeDetailsId = scheme.SchemeDetailsList.Max(c => c.IndexId);
                //else
                schemeDetails.IndexId = maxIndex + 1;
            }
            else
            {
                scheme.SchemeDetailsList.RemoveAll(c => c.IndexId == schemeDetails.IndexId);
            }
            schemeDetails.SchemeHeaderId = scheme.SchemeHeaderId;
            scheme.SchemeDetailsList.Add(schemeDetails);

            SchemeHeader = scheme;
            SchemeDetail = null;
        }
        catch (Exception ex)
        {
        }
        return "1";
    }

    //public string AddCombination()
    //{
    //    try
    //    {
    //        int cid = string.IsNullOrEmpty(Request.QueryString["cid"]) ? 0 : int.Parse(Request.QueryString["cid"]);
    //        int oid = string.IsNullOrEmpty(Request.QueryString["oid"]) ? 0 : int.Parse(Request.QueryString["oid"]);
    //        string brand = Request.QueryString["brand"];
    //        string packt = Request.QueryString["packt"];
    //        string fla = Request.QueryString["fla"];
    //        string itemg = Request.QueryString["itemg"];
    //        string pro = Request.QueryString["pro"];


    //        int brandid = string.IsNullOrEmpty(Request.QueryString["brandid"]) ? 0 : int.Parse(Request.QueryString["brandid"]);
    //        int packtid = string.IsNullOrEmpty(Request.QueryString["packtid"]) ? 0 : int.Parse(Request.QueryString["packtid"]);
    //        int flaid = string.IsNullOrEmpty(Request.QueryString["flaid"]) ? 0 : int.Parse(Request.QueryString["flaid"]);
    //        int itemgid = string.IsNullOrEmpty(Request.QueryString["itemgid"]) ? 0 : int.Parse(Request.QueryString["itemgid"]);
    //        int proid = string.IsNullOrEmpty(Request.QueryString["proid"]) ? 0 : int.Parse(Request.QueryString["proid"]);

    //        int ishvp = string.IsNullOrEmpty(Request.QueryString["ishvp"]) ? 0 : int.Parse(Request.QueryString["ishvp"]);
    //        //int isexc = string.IsNullOrEmpty(Request.QueryString["isexc"]) ? 0 : int.Parse(Request.QueryString["isexc"]);
    //        int isexc = 0; // string.IsNullOrEmpty(Request.QueryString["isexc"]) ? 0 : int.Parse(Request.QueryString["isexc"]);
    //        string isexception = Request.QueryString["isexc"];
    //        string isexception_desc = "Default";

    //        switch (isexception)
    //        {
    //            case "C":
    //                isexception_desc = "Compulsory";
    //                break;
    //            case "D":
    //                isexception_desc = "Default";
    //                break;
    //            case "E":
    //                isexception_desc = "Exception";
    //                break;
    //            default:
    //                isexception_desc = "Default";
    //                break;
    //        }

    //        SchemeDetailsDTO schemeDetails = SchemeDetail;
    //        List<OptionLevelDTO> optionLevelList = new List<OptionLevelDTO>();
    //        if (schemeDetails != null && schemeDetails.OptionLevelList != null)
    //        {
    //            optionLevelList = schemeDetails.OptionLevelList;
    //        }

    //        OptionLevelDTO optionLevelSelected = optionLevelList.FirstOrDefault(c => c.IndexId == oid);

    //        if (optionLevelSelected == null)
    //        {
    //            optionLevelSelected = new OptionLevelDTO();
    //            optionLevelSelected.OptionLevelCombinationList = new List<OptionLevelCombinationDTO>();
    //        }
    //        if (optionLevelSelected.OptionLevelCombinationList == null)
    //        {
    //            optionLevelSelected.OptionLevelCombinationList = new List<OptionLevelCombinationDTO>();
    //        }

    //        if (!optionLevelSelected.OptionLevelCombinationList.Any(c => c.IndexId == cid))
    //        {
    //            int maxIndex = 0;
    //            if (optionLevelSelected.OptionLevelCombinationList.Count > 0)
    //                maxIndex = optionLevelSelected.OptionLevelCombinationList.Max(c => c.IndexId);

    //            var newOptionLevelCombination = new OptionLevelCombinationDTO()
    //            {
    //                ProductId = proid,
    //                ProductName = pro,
    //                BrandId = brandid,
    //                BrandName = brand,
    //                ProductPackingName = packt,
    //                ProductPackingId = packtid,
    //                ProductCatagoryName = itemg,
    //                ProductCategoryId = itemgid,
    //                FlavorName = fla,
    //                FlavorId = flaid,
    //                IsHvp = ishvp == 1,
    //                IsExceptProduct = isexc == 1,
    //                IndexId = maxIndex + 1,
    //                IsExcept = isexception_desc

    //            };
    //            if (ishvp == 3)
    //            {
    //                newOptionLevelCombination.IsHvp = null;
    //            }

    //            optionLevelSelected.OptionLevelCombinationList.Add(newOptionLevelCombination);
    //        }
    //        else
    //        {
    //            OptionLevelCombinationDTO item = optionLevelSelected.OptionLevelCombinationList.FirstOrDefault(c => c.IndexId == cid);
    //            item.ProductId = proid;
    //            item.ProductName = pro;
    //            item.BrandId = brandid;
    //            item.BrandName = brand;
    //            item.ProductPackingName = packt;
    //            item.ProductPackingId = packtid;
    //            item.ProductCatagoryName = itemg;
    //            item.ProductCategoryId = itemgid;
    //            item.FlavorName = fla;
    //            item.FlavorId = flaid;
    //            item.IsHvp = ishvp == 1;
    //            item.IsExceptProduct = isexc == 1;
    //            item.IsExcept = isexception_desc;

    //            if (ishvp == 3)
    //            {
    //                item.IsHvp = null;
    //            }
    //        }
            
    //        optionLevelList.RemoveAll(c => c.IndexId == oid);
    //        optionLevelList.Add(optionLevelSelected);
    //        schemeDetails.OptionLevelList = optionLevelList;
    //        SchemeDetail = schemeDetails;
    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //    return "1";
    //}


    public string AddCombination()
    {
        try
        {
            int cid = string.IsNullOrEmpty(Request.QueryString["cid"]) ? 0 : int.Parse(Request.QueryString["cid"]);
            int oid = string.IsNullOrEmpty(Request.QueryString["oid"]) ? 0 : int.Parse(Request.QueryString["oid"]);
            string brand = Request.QueryString["brand"];
            string packt = Request.QueryString["packt"];
            string fla = Request.QueryString["fla"];
            string itemg = Request.QueryString["itemg"];
            string pro = Request.QueryString["pro"];


            int brandid = string.IsNullOrEmpty(Request.QueryString["brandid"]) ? 0 : int.Parse(Request.QueryString["brandid"]);
            int packtid = string.IsNullOrEmpty(Request.QueryString["packtid"]) ? 0 : int.Parse(Request.QueryString["packtid"]);
            int flaid = string.IsNullOrEmpty(Request.QueryString["flaid"]) ? 0 : int.Parse(Request.QueryString["flaid"]);
            int itemgid = string.IsNullOrEmpty(Request.QueryString["itemgid"]) ? 0 : int.Parse(Request.QueryString["itemgid"]);
            int proid = string.IsNullOrEmpty(Request.QueryString["proid"]) ? 0 : int.Parse(Request.QueryString["proid"]);

            int ishvp = string.IsNullOrEmpty(Request.QueryString["ishvp"]) ? 0 : int.Parse(Request.QueryString["ishvp"]);
            //int isexc = string.IsNullOrEmpty(Request.QueryString["isexc"]) ? 0 : int.Parse(Request.QueryString["isexc"]);
            int isexc = 0; // string.IsNullOrEmpty(Request.QueryString["isexc"]) ? 0 : int.Parse(Request.QueryString["isexc"]);
            string isexception = Request.QueryString["isexc"];
            string isexception_desc = "Default";

            switch (isexception)
            {
                case "C":
                    isexception_desc = "Compulsory";
                    break;
                case "D":
                    isexception_desc = "Default";
                    break;
                case "E":
                    isexception_desc = "Exception";
                    break;
                default:
                    isexception_desc = "Default";
                    break;
            }

            SchemeDetailsDTO schemeDetails = SchemeDetail;
            List<OptionLevelDTO> optionLevelList = new List<OptionLevelDTO>();
            if (schemeDetails != null && schemeDetails.OptionLevelList != null)
            {
                optionLevelList = schemeDetails.OptionLevelList;
            }

            OptionLevelDTO optionLevelSelected = optionLevelList.FirstOrDefault(c => c.IndexId == oid);

            if (optionLevelSelected == null)
            {
                optionLevelSelected = new OptionLevelDTO();
                optionLevelSelected.OptionLevelCombinationList = new List<OptionLevelCombinationDTO>();
            }
            if (optionLevelSelected.OptionLevelCombinationList == null)
            {
                optionLevelSelected.OptionLevelCombinationList = new List<OptionLevelCombinationDTO>();
            }

            string strHVP = "Any";
            if (ishvp == 1)
                strHVP = "Yes";
            else if (ishvp == 0)
                strHVP = "No";

            if (!optionLevelSelected.OptionLevelCombinationList.Any(c => c.IndexId == cid))
            {
                int maxIndex = 0;
                if (optionLevelSelected.OptionLevelCombinationList.Count > 0)
                    maxIndex = optionLevelSelected.OptionLevelCombinationList.Max(c => c.IndexId);

                var newOptionLevelCombination = new OptionLevelCombinationDTO()
                {
                    ProductId = proid,
                    ProductName = pro,
                    BrandId = brandid,
                    BrandName = brand,
                    ProductPackingName = packt,
                    ProductPackingId = packtid,
                    ProductCatagoryName = itemg,
                    ProductCategoryId = itemgid,
                    FlavorName = fla,
                    FlavorId = flaid,
                    IsHvp = ishvp == 1,
                    IsExceptProduct = isexc == 1,
                    IndexId = maxIndex + 1,
                    IsExcept = isexception_desc,
                    Hvp = strHVP
                };
                if (ishvp == 3)
                {
                    newOptionLevelCombination.IsHvp = null;
                }

                optionLevelSelected.OptionLevelCombinationList.Add(newOptionLevelCombination);
            }
            else
            {
                OptionLevelCombinationDTO item = optionLevelSelected.OptionLevelCombinationList.FirstOrDefault(c => c.IndexId == cid);
                item.ProductId = proid;
                item.ProductName = pro;
                item.BrandId = brandid;
                item.BrandName = brand;
                item.ProductPackingName = packt;
                item.ProductPackingId = packtid;
                item.ProductCatagoryName = itemg;
                item.ProductCategoryId = itemgid;
                item.FlavorName = fla;
                item.FlavorId = flaid;
                item.IsHvp = ishvp == 1;
                item.IsExceptProduct = isexc == 1;
                item.IsExcept = isexception_desc;
                item.Hvp = strHVP;
                if (ishvp == 3)
                {
                    item.IsHvp = null;
                }
            }

            optionLevelList.RemoveAll(c => c.IndexId == oid);
            optionLevelList.Add(optionLevelSelected);
            schemeDetails.OptionLevelList = optionLevelList;
            SchemeDetail = schemeDetails;
        }
        catch (Exception ex)
        {
        }
        return "1";
    }
    public string RemoveCombination()
    {
        try
        {
            int cid = string.IsNullOrEmpty(Request.QueryString["cid"]) ? 0 : int.Parse(Request.QueryString["cid"]);
            int oid = string.IsNullOrEmpty(Request.QueryString["oid"]) ? 0 : int.Parse(Request.QueryString["oid"]);

            SchemeDetailsDTO schemeDetails = new SchemeDetailsDTO();


            schemeDetails = SchemeDetail;
            List<OptionLevelDTO> optionLevelList = new List<OptionLevelDTO>();
            if (schemeDetails != null && schemeDetails.OptionLevelList != null)
            {
                optionLevelList = schemeDetails.OptionLevelList;
            }

            OptionLevelDTO optionLevelSelected = optionLevelList.FirstOrDefault(c => c.IndexId == oid);

            if (optionLevelSelected == null)
            {
                optionLevelSelected = new OptionLevelDTO();
                optionLevelSelected.OptionLevelCombinationList = new List<OptionLevelCombinationDTO>();
            }

            if (optionLevelSelected.OptionLevelCombinationList.Any(c => c.IndexId == cid))
            {
                optionLevelSelected.OptionLevelCombinationList.RemoveAll(c => c.IndexId == cid);
            }
            schemeDetails.OptionLevelList = optionLevelList;
            SchemeDetail = schemeDetails;
        }
        catch (Exception ex)
        {
        }
        return "1";
    }

    private string UpdateSchemeHeaderActiveInactive()
    {
        StringBuilder sb = new StringBuilder();
        DiscountSchemeClient discountSchemeClient = new DiscountSchemeClient();
        string str = string.Empty;

        try
        {
            string schemeHeaderId = Request.QueryString["schemeHeaderId"];
            bool isActive = false;
            if ((Request.QueryString["isActive"]).Equals("true"))
                isActive = true;

            SchemeHeaderDTO schemeHeaderDTO = new SchemeHeaderDTO();
            schemeHeaderDTO.SchemeHeaderId = string.IsNullOrEmpty(schemeHeaderId) ? 0 : Convert.ToInt32(schemeHeaderId);
            schemeHeaderDTO.IsActive = isActive;
            schemeHeaderDTO.LastModifiedBy = UserSession.Instance.UserName;

            bool status = false;
            status = discountSchemeClient.UpdateSchemeHeaderIsActive(schemeHeaderDTO);
            str = (status == true) ? "true" : "false";
            sb.Append(str);
        }
        catch (Exception)
        {
            throw;
        }
        return sb.ToString();
    }

    public string LoadSchemeDetail()
    {
        try
        {
            int id = string.IsNullOrEmpty(Request.QueryString["id"]) ? 0 : int.Parse(Request.QueryString["id"]);

            SchemeHeaderDTO scheme = SchemeHeader;

            SchemeDetailsDTO schemeDetails = SchemeHeader.SchemeDetailsList.FirstOrDefault(c => c.IndexId == id);

            SchemeDetail = schemeDetails;
        }
        catch (Exception ex)
        {
        }
        return "1";
    }

    public string NewSchemeDetail()
    {
        SchemeDetail = new SchemeDetailsDTO();
        return "1";
    }

    public string RemoveSchemedetail()
    {
        try
        {
            int id = string.IsNullOrEmpty(Request.QueryString["id"]) ? 0 : int.Parse(Request.QueryString["id"]);

            SchemeDetailsDTO schemeDetails = new SchemeDetailsDTO();

            schemeDetails = SchemeDetail;
            SchemeHeaderDTO scheme = SchemeHeader;

            SchemeDetailsDTO schemeDetails_selected = SchemeHeader.SchemeDetailsList.FirstOrDefault(c => c.IndexId == id);


            List<SchemeDetailsDTO> schemeDetailsList = new List<SchemeDetailsDTO>();
            if (scheme != null && scheme.SchemeDetailsList != null)
            {
                schemeDetailsList = scheme.SchemeDetailsList;
            }

            if (schemeDetailsList.Any(c => c.IndexId == id))
            {
                schemeDetailsList.RemoveAll(c => c.IndexId == id);
            }
            scheme.SchemeDetailsList = schemeDetailsList;
            SchemeHeader = scheme;
        }
        catch (Exception ex)
        {
        }
        return "1";
    }

    public string PrintDiscountScheme()
    {
        ReportDocument rptDoc = GetReportDocument();
        string FilePath = SavePDF(Server.MapPath(GenerateFileName("invoice_summary", Session.SessionID.ToString())) + ".pdf", GenerateFileName("invoice_summary", Session.SessionID.ToString()) + ".pdf", rptDoc);
        return FilePath;
    }

    #region report methods

    private string GenerateFileName(string ori_filename, string sessionid)
    {
        /// TODO:
        /// Generate file name according to system path.
        string fn = "";
        DateTime d = DateTime.Now;
        fn += "../../docs/tmp_" + sessionid.Trim() + "_" + ori_filename.Trim() + "_";
        return fn;
    }

    private string SavePDF(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.PortableDocFormat, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);
        return generatefilename;
        //ScriptManager.RegisterStartupScript(this, GetType(), "modalscript1", "loadpdf('" + generatefilename + "');", true);
    }

    private void SaveExcel(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.Excel, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);
        System.IO.FileInfo file = new System.IO.FileInfo(FilePath);
        if (file.Exists)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.WriteFile(file.FullName);
            HttpContext.Current.Response.End();
        }
    }

    private ReportDocument GetReportDocument()
    {
        int id = string.IsNullOrEmpty(Request.QueryString["id"]) ? 0 : int.Parse(Request.QueryString["id"]);

        ReportDocument rptDoc = new ReportDocument();

        rptDoc.Load(Server.MapPath("../reports/discount_scheme.rpt"));

        rptDoc.SetParameterValue("@SchemeHeaderId", id);


        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        Tables CrTables;

        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());


        CrTables = rptDoc.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }
        return rptDoc;
    }

    #endregion
    
    #endregion
}