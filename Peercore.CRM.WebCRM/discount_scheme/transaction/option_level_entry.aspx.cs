﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Entities;
//using Peercore.CRM.BusinessRules;
using Peercore.CRM.Shared;
using CRMServiceReference;
using Peercore.CRM.Common;
using Telerik.Web.UI;
using System.Drawing;
using System.Data;

public partial class discount_scheme_transaction_option_level_entry : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {

            if (!IsPostBack)
            {
                LoadData();
            }

            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Discount Scheme ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    private void LoadData()
    {
        CommonClient commonClient = new CommonClient();

        List<LookupDTO> lookupList = commonClient.GetLookups("DetailsTypeLookup", "", null, null, false);
        lookupList = lookupList.OrderBy(c => c.Code).ToList();
        DropDownListDetailType.DataSource = lookupList;
        DropDownListDetailType.DataTextField = "Description";
        DropDownListDetailType.DataValueField = "Code";
        DropDownListDetailType.DataBind();

        lookupList = commonClient.GetLookups("SchemeTypeLookup", "", null, null, false);

        DropDownListSchemeType.DataSource = lookupList;
        DropDownListSchemeType.DataTextField = "Description";
        DropDownListSchemeType.DataValueField = "Code";
        DropDownListSchemeType.DataBind();
    }
}