﻿using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class discount_scheme_transaction_territory_discounts : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        hfOriginator.Value = UserSession.Instance.UserName;

        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Territory Discount Scheme ", "#", "");

            if (!IsPostBack)
            {
                LoadOptions();
            }

            hfOriginatorType.Value = UserSession.Instance.OriginatorString;
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    private void LoadOptions()
    {
        DiscountSchemeClient dsclient = new DiscountSchemeClient();

        ArgsDTO args = new ArgsDTO();
        args.StartIndex = 1;
        args.RowCount = 10000;
        args.OrderBy = " scheme_name asc";

        List<SchemeHeaderDTO> objList = new List<SchemeHeaderDTO>();
        objList = dsclient.GetAllSchemeHeader(args);

        if (objList.Count != 0)
        {
            ddlScheme.Items.Clear();
            ddlScheme.Items.Add(new ListItem("- Select Scheme -", "0"));
            foreach (SchemeHeaderDTO item in objList)
            {
                var strLength = (item.SchemeName == null) ? 0 : item.SchemeName.Length;
                string result = (item.SchemeName == null) ? "" : item.SchemeName.PadRight(strLength + 20).Substring(0, strLength + 20);

                ddlScheme.Items.Add(new ListItem(result, item.SchemeHeaderId.ToString()));
            }

            ddlScheme.Enabled = true;
        }
    }
}