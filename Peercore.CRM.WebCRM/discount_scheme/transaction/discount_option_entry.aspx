﻿<%@ Page Title="mSales - Discount Promotions" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="discount_option_entry.aspx.cs" Inherits="discount_scheme_transaction_discount_option_entry" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ Register Src="~/usercontrols/option_level.ascx" TagPrefix="ucl" TagName="OptionLevel" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnablePageMethods="true">
    </telerik:RadScriptManager>
    <asp:HiddenField ID="HiddenFieldDiscountSchemeId" runat="server" Value="0" />
    <asp:HiddenField ID="HiddenFieldIsActive" runat="server" />
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <div id="customermodalWindow" style="display: none">
        <div id="div_customerconfirm_message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>
    <div class="divcontectmainforms" id="maincont">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <ucl:buttonbar ID="buttonbar" runat="server" />
                </div>
            </div>
            <div class="toolbar_right" id="div1">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 35%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/discount_scheme/transaction/discount_scheme_entry.aspx">
            <div class="back"></div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none">
        </div>
        <div id="div_info" runat="server" style="display: none">
        </div>
        <div id="div_promt" style="display: none">
        </div>
        <div id="div_scheme_detail">
            <div>
                <%--layout new format--%>
                <div class="formleft">
                    <div class="formtextdiv">
                        Scheme name</div>
                    <div class="formdetaildiv_right">
                        :<span class="wosub mand">
                            <asp:TextBox ID="txtSchemeName" runat="server" CssClass="tb" MaxLength="50" Style="width: 200px;
                                text-transform: uppercase"></asp:TextBox>
                            <div runat="server" id="div_autocomplete">
                            </div>
                        </span>
                        <%-- <asp:CheckBox ID="radioActive" GroupName="grpDiscountOption" runat="server" Text=" Active " Checked="true"
                        CssClass="radibutton_width" />--%>
                        <asp:CheckBox ID="chkInactive" GroupName="grpDiscountOption" runat="server" OnCheckedChanged="check_changed"
                            AutoPostBack="true" Text=".  Inactive" CssClass="radibutton_width" />
                    </div>
                    <div class="clearall">
                    </div>
                    <%--                <div class="formdetaildiv_right" id = "divGrpStatus">
                    <asp:RadioButton ID="radioActive" GroupName = "grpDiscountOption"  runat="server" Text=" Active " CssClass="radibutton_width" />
                    <asp:RadioButton ID="radioInactive" GroupName = "grpDiscountOption" runat="server" Text=" Inactive " CssClass="radibutton_width" />
                </div>
                <div class="clearall">
                </div>--%>
                </div>
                <div class="formright">
                    <div class="formtextdiv">
                        Copy From Scheme</div>
                    <div class="formdetaildiv_right">
                        :<span class="wosub mand">
                            <asp:DropDownList ID="ddlCopyFromScheme" runat="server" CssClass="tb" 
                            MaxLength="50" Style="width: 200px;" AutoPostBack="True" 
                            onselectedindexchanged="ddlCopyFromScheme_SelectedIndexChanged">
                            </asp:DropDownList>
                        </span><asp:Button ID="Button1" runat="server" Text="Load" style="display:none" />
                    </div>
                    <div class="clearall">
                    </div>
                    
                </div>
            </div>
            <div class="clearall">
                
            </div>
            <div class="grid_container">
                <div id="divSchemeHeaderGrid">
                </div>
            </div>
        </div>
    </div>
    <div class="divcontectmainforms" id="itemcontent" style="display: none">
        <ucl:OptionLevel ID="uclOptionLevel" runat="server" />
    </div>
    <asp:HiddenField ID="hfWebsite" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfPreferredMethod" runat="server"></asp:HiddenField>
    <script type="text/javascript">
        jQuery(function ($) {

            $('input.validate-numbers').autoNumeric({ mDec: 0 });
            $('input.validate-numbers-percentage').autoNumeric({ mDec: 1 });
            //            jQuery("#txtSchemeDetails").validate({
            //                expression: "if (VAL) return true; else return false;",
            //                message: "Please Enter Scheme Details"
            //            });
            loadDiscountSchemeDetails($('#MainContent_HiddenFieldDiscountSchemeId').val(), 0);
        });

        $("#a_add_option").click(function () {
            $('#div_scheme_detail').css('display', 'none');
            $('#itemcontent').css('display', 'block');
            $('#a_add_scheme').css('display', 'inline-block');
            $('#a_cancel').css('display', 'inline-block');
            $('#MainContent_buttonbar_buttinSave').css('display', 'none');
            $('#a_add_option').css('display', 'none');
            $('#div_Percentage').css('display', 'none');
            $('#div_value').css('display', 'block');
            $('#div_combination').css('display', 'none');
            $('#div_free_product').css('display', 'none');

            $("#div_typebill").css("display", "block");
            $("#div_qty_val").css("display", "none");
            $("#div_bill").css("display", "block");
            $('#chkIsBill').prop('checked', true);

            var root = ROOT_PATH + "discount_scheme/process_forms/processmaster.aspx?fm=newschemedetail&type=query";

            $.ajax({
                url: root,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg == "1") {
                        //                        

                        $("#txtDiscountValue").val('0');
                        $("#txtPercentage").val('0');
                        $("#DropDownListDetailType").val(0);
                        $("#DropDownListSchemeType").val(0);
                        $("#txtSchemeDetails").val('');
                        $('#chkIsRetailer').prop('checked', false);

                        $("#txtMinQuantity").val("0");
                        $("#txtMaxQuantity").val("0");
                        $("#txtMinValue").val("0");
                        $("#txtMaxValue").val("0");
                        $("#txtMinBValue").val("0");
                        $("#txtMaxBValue").val("0");

                        $("#DropDownListOperator").val(0);

                        //$("#grid_option_level").html('');
                        GetOptionLevelList();
                        // var grid = $("#grid_option_level").data("kendoGrid");
                        //                       grid.dataSource.read();
                    }
                },
                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                    }
                }
            });

        });

        $(document).ready(function () {
            $("#MainContent_HiddenFieldIsActive").val("1");
            $('#a_add_option').css('display', 'inline-block');
        });
        //   
        // hideStatusDiv("MainContent_div_message");
        //        jQuery(function ($) {

        //            $("#MainContent_HiddenFieldIsActive").val("1");
        //        });



        //            hideStatusDiv("MainContent_div_message");
        //            if ($('#MainContent_HiddenFieldMarketName').val() != '' && $('#MainContent_HiddenFieldMarketName').val() != null) {
        //                $('#MainContent_txtMarketName').val($('#MainContent_HiddenFieldMarketName').val());
        //            }
        //            else {
        //                $('#MainContent_txtMarketName').val(' - ');
        //            }

        //            jQuery(function ($) {
        //                $('input.input_numeric_tp').autoNumeric({ aSep: null, aDec: null, mDec: 0 });
        //            });

        //        function isNumber(evt) {
        //            evt = (evt) ? evt : window.event;
        //            var charCode = (evt.which) ? evt.which : evt.keyCode;
        //            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        //                return false;
        //            }
        //            return true;
        //        }

        //                               $('#MainContent_radioActive').change(function () {
        //            if ($(this).is(":checked")) {
        //                alert('1');
        //            }
        //        });
        $('#MainContent_chkInactive').change(function () {

            if ($(this).is(":checked")) {
                $("#MainContent_HiddenFieldIsActive").val("0");

            }
            else {
                $("#MainContent_HiddenFieldIsActive").val("1");
                // alert('3'); 
            }
        });

        function setSchemeId(schemeId) {
            $("#MainContent_HiddenFieldDiscountSchemeId").val(schemeId);
            // alert($("#MainContent_HiddenFieldDiscountSchemeId").val());
            loadDiscountSchemeDetails(schemeId, 0);
        }
        //   }



    </script>
</asp:Content>
