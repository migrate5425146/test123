﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System.Text;
using CRMServiceReference;
using System.ServiceModel;

public partial class discount_scheme_transaction_discount_option_entry : System.Web.UI.Page
{
    #region - Properties -
    private SchemeHeaderDTO SchemeHeaderEntry
    {
        get
        {
            if (Session[CommonUtility.CUSTOMER_DATA] != null)
            {
                return (SchemeHeaderDTO)Session[CommonUtility.SCHEME_HEADER];
            }
            return new SchemeHeaderDTO();
        }
        set
        {
            Session[CommonUtility.SCHEME_HEADER] = value;
        }
    }

    #endregion

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
            buttonbar.onButtonAddOption = new usercontrols_buttonbar.ButtonAddOption(ButtonAddOption_Click);
            // chkInactive.CheckedChanged += new EventHandler(check_changed);
            buttonbar.EnableSave(true);
            buttonbar.VisibleSave(true);
            //buttonbar.VisibleAddOption(true);
            //buttonbar.EnableAddOption(true);
            //itemcontent.Visible = false;

            if (!IsPostBack)
            {
                //div_autocomplete.InnerHtml = SetAutocomplete_Temp(!string.IsNullOrEmpty(HiddenFieldIsActive.Value) ? HiddenFieldIsActive.Value : "1");    
                div_autocomplete.InnerHtml = SetAutocomplete_Temp("1");
                Load();
            }

            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Discount Scheme ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    private void Load()
    {
        int headerid = string.IsNullOrEmpty(Request.QueryString["id"]) ? 0 : int.Parse(Request.QueryString["id"]);
        HiddenFieldDiscountSchemeId.Value = headerid.ToString();
        DiscountSchemeClient discountSchemeClient = new DiscountSchemeClient();
        SchemeHeaderDTO allheaders = new SchemeHeaderDTO();
        ArgsDTO args = new ArgsDTO();
        args.StartIndex = 0;
        args.OrderBy = " scheme_name desc";

        args.RowCount = 1000;
        allheaders = discountSchemeClient.GetSchemeHeaderById(args, Convert.ToInt32(headerid));
        Session[CommonUtility.SCHEME_HEADER] = allheaders;
        txtSchemeName.Text = allheaders.SchemeName;

        //if discount scheme is selected dissable the dropdown
        if (allheaders.SchemeDetailsList.Count != 0)
        {
            ddlCopyFromScheme.Enabled = false;
        }

        List<SchemeHeaderDTO> allSchemes = new List<SchemeHeaderDTO>();
        allSchemes = discountSchemeClient.GetAllSchemeHeaderForCopy();

        ddlCopyFromScheme.DataSource = allSchemes;
        ddlCopyFromScheme.DataTextField = "SchemeName";
        ddlCopyFromScheme.DataValueField = "SchemeHeaderId";
        ddlCopyFromScheme.DataBind();
    }

    private void Load_PastScheme()
    {
        int headerid = string.IsNullOrEmpty(Request.QueryString["id"]) ? 0 : int.Parse(Request.QueryString["id"]);
        int hid = int.Parse(ddlCopyFromScheme.SelectedValue);
        HiddenFieldDiscountSchemeId.Value = hid.ToString();
        DiscountSchemeClient discountSchemeClient = new DiscountSchemeClient();
        SchemeHeaderDTO allheaders = new SchemeHeaderDTO();
        ArgsDTO args = new ArgsDTO();
        args.StartIndex = 0;
        args.OrderBy = " scheme_name desc";

        args.RowCount = 1000;
        allheaders = discountSchemeClient.GetSchemeHeaderById(args, Convert.ToInt32(headerid));
        Session[CommonUtility.SCHEME_HEADER] = allheaders;

        discountSchemeClient.GetSchemeHeaderById(args, Convert.ToInt32(hid));
    }

    protected void ButtonSave_Click(object sender)
    {
        DiscountSchemeClient oDiscount = new DiscountSchemeClient();
        CommonUtility commonUtility = new CommonUtility();
        SchemeHeaderDTO schemeHeader = new SchemeHeaderDTO();
        int schemeHeaderId = 0;

        try
        {
            schemeHeader = Session[CommonUtility.SCHEME_HEADER] as SchemeHeaderDTO;
            schemeHeader.SchemeDetailsList = schemeHeader.SchemeDetailsList.OrderBy(c => c.IndexId).ToList();

            DiscountSchemeClient discountSchemeClient = new DiscountSchemeClient();
            int headerid = string.IsNullOrEmpty(Request.QueryString["id"]) ? 0 : int.Parse(Request.QueryString["id"]);
            SchemeHeaderDTO allheaders_details = new SchemeHeaderDTO();
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 0;
            args.OrderBy = " scheme_name desc";
            args.RowCount = 1000;
            allheaders_details = discountSchemeClient.GetSchemeHeaderById(args, Convert.ToInt32(headerid));

            schemeHeader.SchemeHeaderId = allheaders_details.SchemeHeaderId;
            schemeHeader.SchemeName = allheaders_details.SchemeName;
            schemeHeader.StartDate = allheaders_details.StartDate;
            schemeHeader.EndDate = allheaders_details.EndDate;
            schemeHeader.DivisionName = allheaders_details.DivisionName;
            schemeHeader.DivisionId = allheaders_details.DivisionId;
            schemeHeader.CreatedBy = allheaders_details.CreatedBy;
            schemeHeader.CreatedDate = allheaders_details.CreatedDate;
            schemeHeader.IsActive = allheaders_details.IsActive;
            schemeHeader.isDirty = allheaders_details.isDirty;
            schemeHeader.LastModifiedBy = UserSession.Instance.OriginalUserName;
            schemeHeader.LastModifiedDate = allheaders_details.LastModifiedDate;

            bool iNoRecs = oDiscount.SchemeHeaderSave(schemeHeader, out schemeHeaderId);

            if (iNoRecs)
            {
                div_info.Attributes.Add("style", "display:none");
                div_info.InnerHtml = "";
                div_message.Attributes.Add("style", "display:block");
                div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved.");
                ddlCopyFromScheme.Enabled = false;
            }



            //try
            //{
            //    CommonClient cClient = new CommonClient();
            //    cClient.CreateTransactionLog(UserSession.Instance.OriginalUserName,
            //    DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
            //    TransactionTypeModules.Update,
            //    TransactionModules.DiscountScheme,
            //    UserSession.Instance.OriginalUserName + " update discount scheme " + schemeHeader.SchemeName + " options");
            //}
            //catch { }
        }
        catch (Exception e)
        {
        }
        finally
        {
            if (oDiscount.State != CommunicationState.Closed)
                oDiscount.Close();
        }
    }

    protected void ButtonAddOption_Click(object sender)
    {
        //maincont.Visible = false;
        //itemcontent.Visible = true;
    }

    protected void check_changed(object sender, EventArgs e)
    {
        if (sender != null)
        {
            if (((CheckBox)sender).Checked)
            {
                HiddenFieldIsActive.Value = "0";
                div_autocomplete.InnerHtml = SetAutocomplete_Temp("0");
            }
            else
            {
                HiddenFieldIsActive.Value = "1";
                div_autocomplete.InnerHtml = SetAutocomplete_Temp("1");
            }
        }
    }

    private string SetAutocomplete_Temp(string isActive)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script type=\"text/javascript\">");
        //   $("#MainContent_HiddenField1TemplateName").val($("#MainContent_txtTemplateName").val());

        sb.Append("     $(function () {");
        sb.Append("         var aa= $(\"#MainContent_HiddenFieldIsActive\").val(); ");
        sb.Append("         $(\".tb\").autocomplete({");
        sb.Append("             source: function (request, response) {");
        sb.Append("                 $.ajax({");
        sb.Append("                     type: \"POST\",");
        sb.Append("                     contentType: \"application/json; charset=utf-8\",");
        //sb.Append("                     url: \"" + ConfigUtil.ApplicationPath + "service/discount_scheme/discount_scheme_service.asmx/GetDiscountSchemeName\",");
        //sb.Append("                     data: \"{'name':'\" + request.term + \"','isActive':'" + isActive + "'}\",");
        //sb.Append("                     data: \"{'name':'\" + request.term + \"','isActive':'" + isActive + "'}\",");");

        sb.Append("                     url: \"" + ConfigUtil.ApplicationPath + "service/discount_scheme/discount_scheme_service.asmx/GetDiscountSchemeName\",");
        sb.Append("                     data: \"{'name':'\" + request.term + \"','isActive':'" + isActive + "'}\",");
        sb.Append("                     dataType: \"json\",");
        sb.Append("                     async: true,");
        sb.Append("                     dataFilter: function(data) {");
        sb.Append("                           return data;");
        sb.Append("                     },");
        sb.Append("                     success: function (data) {");
        sb.Append("                     response($.map(data.d, function(item) {");
        sb.Append("                     return {");
        sb.Append("                         label: item.SchemeName,");
        sb.Append("                         desc: item.SchemeHeaderId");
        sb.Append("                     };");
        sb.Append("                     }));");
        sb.Append("                     },");
        sb.Append("                     error: function (result) {");
        sb.Append("                     }");
        sb.Append("                 });");
        sb.Append("             },");
        sb.Append("            minLength: 2,");
        sb.Append("            select: function( event, ui ) {");
        sb.Append("                  var selectedObj = ui.item;");
        sb.Append("                 $('#MainContent_txtSchemeName').val(selectedObj.label);");
        sb.Append("                 setSchemeId(selectedObj.desc);");
        sb.Append("            }");
        sb.Append("         });");
        sb.Append("     });");
        sb.Append("</script>");
        sb.Append("");

        return sb.ToString();
    }

    protected void ddlCopyFromScheme_SelectedIndexChanged(object sender, EventArgs e)
    {
        Load_PastScheme();
    }
}