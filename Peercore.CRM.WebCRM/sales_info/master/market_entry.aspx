﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="market_entry.aspx.cs" Inherits="sales_info_master_market_entry" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
 <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <div id="div_main" class="divcontectmainforms">
        <div id="window" style="display:none; width:500px;">
            <div style="width:100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="textalignbottom">
                                    Name
                                </td>
                                <td colspan="3">
                                <span class="wosub mand">
                                    &nbsp;<input type="text" class="textboxwidth" id="txtName">
                                    <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonSave" type="button">Save</button>
                                    <button class="k-button" id="buttonClear">Clear</button>
                                    <asp:HiddenField ID="hdnSelectedMarketId" runat="server" ClientIDMode="Static" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div class="addlinkdiv" style="float: left" id="div_address">
                        <a id="id_add_market" clientidmode="Static" runat="server" href="#">Add Market</a>
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
            <div class="back"></div></asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
         <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
           <div class="clearall">
        </div>
        <div style="min-width: 200px; overflow: auto">
            <div style="float: left; width: 100%; overflow: auto">
                <div id="MarketGrid">
                </div>
            </div>
        </div>
    </div>
 
    <script type="text/javascript">
        hideStatusDiv("MainContent_div_message");

        $(document).ready(function () {
            loadMarketGrid('1', "MarketGrid");
            $("#txtName").focus();
        });


        //Load popup for Add new market
        $("#id_add_market").click(function () {
            $("#window").css("display", "block");
            $("#hdnSelectedMarketId").val('');
            
            $("#txtName").val('');

            var wnd = $("#window").kendoWindow({
                title: "Add/Edit Market",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            $("#txtName").focus();
            wnd.center().open();            
        });

        //Save button click
        $("#buttonSave").click(function () {            
            MarketEntrySaveMarket();
        });

        $("#buttonClear").click(function () {
            $("#hdnSelectedMarketId").val('');
            $("#txtName").val('');
        });

        function closepopup() {
            var wnd = $("#window").kendoWindow({
                title: "Add/Edit Market",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#window").css("display", "none");
        }

    </script>
</asp:Content>
