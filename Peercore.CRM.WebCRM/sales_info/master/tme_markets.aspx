﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="tme_markets.aspx.cs" Inherits="sales_info_master_tme_markets" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="HiddenFieldId" runat="server" />
    <asp:HiddenField ID="HiddenFieldName" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedOriginatorId" runat="server" />

    <div id="marketmodalWindow" style="display: none">
        <div id="div_marketconfirm_message">
        </div>
        <div class="clearall" style="margin-bottom:15px"></div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>
    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div>
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
            <div class="back"></div></asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none"></div>
          <div class="clearall">
        </div>
        <div id="modalWindowOriginators" style="display: none"></div>
        <div id="modalWindowUnassignMarkets" style="display: none"></div>

        <div class="formtextdiv_extended"> TME </div>
        <div class="formdetaildiv_right">
        :
        <asp:TextBox ID="txtSelectedOriginatorName" runat="server" CssClass="input-large"  ReadOnly="true" Width="235px"></asp:TextBox>
                    <a href="#" id="id_select_tme"><img id="img1" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a>
                   
        </div>
        <div class="clearall"></div>


        <div id="div_address">
            <div class="addnewdiv" style="float: right; margin-right: 10px; margin-bottom:10px;">
                <a id="id_assign_market" clientidmode="Static" runat="server" href="#">Assign Market</a>
            </div>
                        <asp:Label ID="lblmarket" runat="server" Text="Markets" CssClass="grid_title" style="margin-left:10px;"></asp:Label>
        </div>
         <div class="clearall"></div>
                <div id="loadMarketAssignGrid" style="margin:0px 10px">
                </div>

        <div class="clearall">
        </div>
    </div>
    <script type="text/javascript">
        var originator = '';
        $(document).ready(function () {
            hideStatusDiv("MainContent_div_message");
            $("div.divcontectmainforms").height('500px');
            $("#div_address").hide();
            $("#id_select_tme").click(function () {
                get_tmes('sales_info/process_forms/processmaster.aspx?fm=tmemarkets&type=select&catergory=ASE');
            });
        });

        //Load popup for assign new market
        $("#id_assign_market").click(function () {
            load_unassign_markets('modalWindowUnassignMarkets');
        });        
    </script>
</asp:Content>
