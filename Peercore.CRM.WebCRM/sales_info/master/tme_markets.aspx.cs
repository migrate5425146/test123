﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Common;
using Peercore.CRM.Entities;
using System.IO;
using System.Web.Services;
using System.Xml.Serialization;
using System.Configuration;
using CRMServiceReference;
using Peercore.CRM.Shared;


public partial class sales_info_master_tme_markets : PageBase
{
    CommonUtility commonUtility = new CommonUtility();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Remove the session before setting the breadcrumb.
        Session.Remove(CommonUtility.BREADCRUMB);
        Master.SetBreadCrumb("ASM Markets ", "#", "");

        //Set the Toolbar buttons
        buttonbar.VisibleSave(true);
        buttonbar.VisibleDeactivate(true);
        buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
        buttonbar.onButtonClear = new usercontrols_buttonbar.ButtonClear(ButtonClear_Click);

        buttonbar.EnableSave(false);
        buttonbar.EnableDeactivate(true);
    }

    protected void ButtonSave_Click(object sender)
    {
        this.SaveTmeMarkets();
    }

    protected void ButtonClear_Click(object sender)
    {
        // Clear();
        txtSelectedOriginatorName.Text = string.Empty;
        HiddenFieldSelectedOriginatorId.Value = string.Empty;
        div_message.Attributes.Add("style", "display:none;padding:8px");
    }

    private void SaveTmeMarkets()
    {
        try
        {
            if (Session[CommonUtility.ORIGINATOR_MARKETS] == null)
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Please assign markets for selected ASM.");
                return;
            }

            CommonClient commonClient = new CommonClient();
            List<MarketDTO> marketList = (List<MarketDTO>)Session[CommonUtility.ORIGINATOR_MARKETS];
            bool status = commonClient.AssignTMEMarkets(marketList);

            if (status)
            {
                div_message.Attributes.Add("style", "display:block;padding:8px");
                div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved !");
                Session[CommonUtility.ORIGINATOR_MARKETS] = null;
                txtSelectedOriginatorName.Text = string.Empty;
                HiddenFieldSelectedOriginatorId.Value = string.Empty;
            }
        }
        catch (Exception ex)
        {
            div_message.Attributes.Add("style", "display:block;padding:2px");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Error Occured.");
        }
    }

}