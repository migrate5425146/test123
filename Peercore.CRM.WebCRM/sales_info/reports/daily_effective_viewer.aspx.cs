﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CrystalDecisions.CrystalReports.Engine;
using Peercore.CRM.Entities;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.DataAccess.datasets;
using System.Net;
using CrystalDecisions.Shared;
using System.Data;

using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using SpreadsheetLight;
using SpreadsheetLight.Drawing;
using System.IO;


public partial class sales_info_reports_daily_effective_viewer : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
        //GenerataReport();
       this.GenerateExcelReport();
       

    }


    private void GenerateExcelReport()
    {
        if (Request.QueryString["fromDate"] != "" && Request.QueryString["toDate"] != "")
        {
            this.CreateDailyEffectiveReport(Request.QueryString["fromDate"].ToString(), Request.QueryString["toDate"].ToString());
        }
    }


    private void CreateDailyEffectiveReport(string fromDate, string toDate)
    {
        DailyEffectiveDataSet ds = null;
        DataTable reportDT = null;

        try
        {
            ArgsEntity args = new ArgsEntity();
            args.SStartDate = fromDate;
            args.SEndDate = toDate;
            ds = CustomerBR.Instance.GetDailyEffectiveReportData(args);

            reportDT = new DataTable();
            reportDT.Columns.Clear();
            reportDT.Columns.Add("No", typeof(string));
            reportDT.Columns.Add("Customer Name", typeof(string));
            reportDT.Columns.Add("Address/Town", typeof(string));
            reportDT.Columns.Add("TLP", typeof(string));


            //Handle product details
            ArgsEntity paramargs = new ArgsEntity();
            paramargs.OrderBy = " id asc ";
            paramargs.AdditionalParams = string.Empty;
            paramargs.StartIndex = 1;
            paramargs.RowCount = 100;


            //Get Distinct productCode list
            List<ProductEntity> productList = ProductBR.Instance.GetCustomerProducts(paramargs);
            List<string> productCodeList = productList.Select(x => x.Code).Distinct().ToList();
            List<string> productCodes = new List<string>() { "NA", "NA", "NA", "NA", "NA", "NA" };
            for (int i = 0; i < productCodeList.Count; i++)
            {
                productCodes[i] = productCodeList[i].ToString();
                reportDT.Columns.Add(productCodeList[i].ToString(), typeof(string));
            }

            //Get Distinct merchandiseCode list
            List<MerchandiseEntity> merchandiseList = MerchandiseBR.Instance.GetCustomerMerchandise(paramargs);
            List<string> merchandiseCodeList = merchandiseList.Select(x => x.Code).Distinct().ToList();
            List<string> merchandiseCodes = new List<string>() { "NA", "NA", "NA", "NA", "NA", "NA" };
            for (int i = 0; i < merchandiseCodeList.Count; i++)
            {
                merchandiseCodes[i] = merchandiseCodeList[i].ToString();
                reportDT.Columns.Add(merchandiseCodeList[i].ToString(), typeof(string));
            }

            //Get Distinct objectiveCode list
            List<ObjectiveEntity> objectiveList = ObjectiveBR.Instance.GetCustomerObjectives(paramargs);
            List<string> objectiveCodeList = objectiveList.Select(x => x.Code).Distinct().ToList();
            List<string> objectiveCodes = new List<string>() { "NA", "NA", "NA", "NA", "NA", "NA" };
            for (int i = 0; i < objectiveCodeList.Count; i++)
            {
                objectiveCodes[i] = objectiveCodeList[i].ToString();
                reportDT.Columns.Add(objectiveCodeList[i].ToString(), typeof(string));
            }


            reportDT.Columns.Add("RDF", typeof(string));
            reportDT.Columns.Add("Display", typeof(string));
            reportDT.Columns.Add("CDR", typeof(string));
            reportDT.Columns.Add("Remarks", typeof(string));


            foreach (DataRow drow in ds.Tables["DailyEffectiveGeneralDetail"].Rows)
            {
                DataRow dr = reportDT.NewRow();
                dr["No"] = drow["RowNumber"].ToString();
                dr["Customer Name"] = drow["name"].ToString();
                dr["Address/Town"] = drow["address_1"].ToString();
                dr["TLP"] = drow["TLP"].ToString();

                dr["RDF"] = drow["rdf_status"].ToString();
                dr["Display"] = drow["display"].ToString();
                dr["CDR"] = drow["cdr"].ToString();
                dr["Remarks"] = drow["remarks"].ToString();


                string custCode = drow["cust_code"].ToString();

                //Fill products data
                var cusProductList = productList.Where(c => c.CustomerCode.Equals(custCode)).ToList();
                int productColumnCount = 6;
                for (int r = 0; r < productColumnCount; r++)
                {
                    string columnName = "productColumn" + (r + 1).ToString();
                    drow[columnName] = "NA";
                }

                for (int x = 0; x < productCodeList.Count; x++)
                {
                    string columnName = "productColumn" + (x + 1).ToString();
                    drow[columnName] = "N";
                    dr[productCodeList[x].ToString()] = "N";

                    List<ProductEntity> pfilterList = cusProductList.Where(p => p.Code.Equals(productCodeList[x].ToString())).ToList();
                    if (pfilterList.Count > 0)
                    {
                        drow[columnName] = "Y";
                        dr[productCodeList[x].ToString()] = "Y";
                    }
                }


                //Fill Merchandise data
                var cusMerchandiseList = merchandiseList.Where(c => c.CustomerCode.Equals(custCode)).ToList();
                int merchandiseColumnCount = 5;
                for (int a = 0; a < merchandiseColumnCount; a++)
                {
                    string columnName = "merchandiseColumn" + (a + 1).ToString();
                    drow[columnName] = "NA";
                }

                for (int b = 0; b < merchandiseCodeList.Count; b++)
                {
                    string columnName = "merchandiseColumn" + (b + 1).ToString();
                    drow[columnName] = "N";
                    dr[merchandiseCodeList[b].ToString()] = "N";

                    List<MerchandiseEntity> mfilterList = cusMerchandiseList.Where(m => m.Code.Equals(merchandiseCodeList[b].ToString())).ToList();
                    if (mfilterList.Count > 0)
                    {
                        drow[columnName] = "Y";
                        dr[merchandiseCodeList[b].ToString()] = "Y";
                    }
                }


                //Fill Objective data
                var cusObjectiveList = objectiveList.Where(c => c.CustomerCode.Equals(custCode)).ToList();
                int objectiveColumnCount = 5;
                for (int c = 0; c < objectiveColumnCount; c++)
                {
                    string columnName = "objectiveColumn" + (c + 1).ToString();
                    drow[columnName] = "NA";
                }

                for (int d = 0; d < objectiveCodeList.Count; d++)
                {
                    string columnName = "objectiveColumn" + (d + 1).ToString();
                    drow[columnName] = "N";
                    dr[objectiveCodeList[d].ToString()] = "N";

                    List<ObjectiveEntity> ofilterList = cusObjectiveList.Where(m => m.Code.Equals(objectiveCodeList[d].ToString())).ToList();
                    if (ofilterList.Count > 0)
                    {
                        drow[columnName] = "Y";
                        dr[objectiveCodeList[d].ToString()] = "Y";
                    }
                }

                reportDT.Rows.Add(dr);
            }

            bool result = this.CreteExcelReport(reportDT, productCodeList.Count);

            if (result)
            {
                Response.Write("Daily Effective Report Sucessfully created.");
            }

        }
        catch (Exception ex)
        {
            Response.Write("Error Occured : " + ex.Message);
        }
    }

    //------------------- Excel report creation methods ------------------------//

    private bool CreteExcelReport(DataTable reportDT, int productCount)
    {
        bool status = false;

        try
        {
            SLDocument exlDoc = new SLDocument();

            string path = HttpContext.Current.Request.PhysicalApplicationPath + "\\docs\\der_reports";
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            string savepath = Server.MapPath("../../docs/der_reports/");
            string filename = savepath + "tmp_dailyeffectivereport.xlsx";

            int header1RowIndex = 8;
            int header2RowIndex = header1RowIndex + 1;
            int dataRowIndex = header2RowIndex + 1;

            int headerColumnIndex = 1;
            int dataColumnIndex = headerColumnIndex;

            foreach (DataColumn col in reportDT.Columns)
            {
                //Draw the excel data column header cells.
                string header1colName = string.Empty;
                string header2colName = col.ColumnName;
                switch (col.ColumnName)
                {
                    case "No":
                        {
                            header1colName = "No";
                            header2colName = string.Empty;
                            break;
                        }
                    case "Customer Name":
                        {
                            header1colName = "Customer Name";
                            header2colName = string.Empty;
                            break;
                        }
                    case "Address/Town":
                        {
                            header1colName = "Address/Town";
                            header2colName = string.Empty;
                            break;
                        }
                    case "TLP":
                        {
                            header1colName = "TLP";
                            header2colName = "Y/N";
                            break;
                        }

                    case "RDF":
                        {
                            header1colName = "RDF";
                            header2colName = "STATUS";
                            break;
                        }

                    case "Display":
                        {
                            header1colName = "Display";
                            header2colName = string.Empty;
                            break;
                        }

                    case "CDR":
                        {
                            header1colName = "CDR";
                            header2colName = string.Empty;
                            break;
                        }

                    case "Remarks":
                        {
                            header1colName = "Remarks";
                            header2colName = string.Empty;
                            break;
                        }

                    default:
                        break;
                }

                //Draw the excel data column for header1 cells.
                this.DrawExcelHeaderCellWithAutoFit(exlDoc, header1RowIndex, headerColumnIndex, header1colName);

                //Draw the excel data column for header2 cells.
                this.DrawExcelHeaderCellWithAutoFit(exlDoc, header2RowIndex, headerColumnIndex, header2colName);

                //Draw the excel data row cells.
                dataRowIndex = header2RowIndex + 1;
                foreach (DataRow row in reportDT.Rows)
                {
                    this.DrawExcelDataRowCellWithAutoFit(exlDoc, dataRowIndex, headerColumnIndex, row[headerColumnIndex - 1].ToString());
                    dataRowIndex++;
                }

                headerColumnIndex++;
            }

            // merge all cells in the cell 
            exlDoc.MergeWorksheetCells("A8", "A9");
            exlDoc.MergeWorksheetCells("B8", "B9");
            exlDoc.MergeWorksheetCells("C8", "C9");
            exlDoc.MergeWorksheetCells("T8", "T9");
            exlDoc.MergeWorksheetCells("U8", "U9");
            exlDoc.MergeWorksheetCells("V8", "V9");

            exlDoc.MergeWorksheetCells("E8", "I8");
            exlDoc.MergeWorksheetCells("J8", "M8");
            exlDoc.MergeWorksheetCells("N8", "R8");

            exlDoc.SetCellValue("E8", "Availability");
            exlDoc.SetCellValue("J8", "Merchandising");
            exlDoc.SetCellValue("N8", "Cy Obj. Achievement");

            //SLPicture pic = new SLPicture("C:\\PROJECTS\\CTC New_Module - 21012014\\WebCRM\\Peercore.Web.CRM\\Peercore.CRM.WebCRM\\assets\\images\\CTC_logo.png");
            SLPicture pic = new SLPicture(Server.MapPath("../../assets/images/CTC_logo.png"));

            pic.SetPosition(0.5, 1.0);
            exlDoc.InsertPicture(pic);

            exlDoc.SaveAs(filename);
            status = true;

        }
        catch (Exception ex)
        {
            status = false;
            throw ex;
        }

        return status;
    }

    private void DrawExcelHeaderCellWithAutoFit(SLDocument doc, int rowIndex, int columnIndex, string data)
    {
        doc.SetCellValue(rowIndex, columnIndex, data);

        SLStyle style = doc.CreateStyle();
        style.SetTopBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
        style.SetBottomBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
        style.SetRightBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
        style.SetLeftBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);

        style.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
        style.Font.Bold = true;

        doc.SetCellStyle(rowIndex, columnIndex, style);
        doc.AutoFitColumn(columnIndex);
    }

    private void DrawExcelDataRowCellWithAutoFit(SLDocument doc, int rowIndex, int columnIndex, string data)
    {
        doc.SetCellValue(rowIndex, columnIndex, data);

        SLStyle style = doc.CreateStyle();
        style.SetTopBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
        style.SetBottomBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
        style.SetRightBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
        style.SetLeftBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);

        style.SetHorizontalAlignment(HorizontalAlignmentValues.Left);

        doc.SetCellStyle(rowIndex, columnIndex, style);
        doc.AutoFitColumn(columnIndex);
    }

    private void DrawExcelHeaderCell(SLDocument doc, int rowIndex, int columnIndex, string data, double columnWidth)
    {
        doc.SetCellValue(rowIndex, columnIndex, data);

        SLStyle style = doc.CreateStyle();
        style.SetTopBorder(BorderStyleValues.Thick, System.Drawing.Color.Black);
        style.SetBottomBorder(BorderStyleValues.Thick, System.Drawing.Color.Black);
        style.SetRightBorder(BorderStyleValues.Thick, System.Drawing.Color.Black);
        style.SetLeftBorder(BorderStyleValues.Thick, System.Drawing.Color.Black);

        style.SetHorizontalAlignment(HorizontalAlignmentValues.Center);

        doc.SetCellStyle(rowIndex, columnIndex, style);
        //doc.AutoFitColumn((columnIndex + 1));
        doc.SetColumnWidth(columnIndex, columnWidth);
    }




    private DailyEffectiveDataSet GetDailyEffectiveDataSet(string fromDate, string toDate)
    {
        DailyEffectiveDataSet ds = null;

        try
        {
            ArgsEntity args = new ArgsEntity();
            args.SStartDate = fromDate;
            args.SEndDate = toDate;
            ds = CustomerBR.Instance.GetDailyEffectiveReportData(args);

            //Handle product details
            ArgsEntity paramargs = new ArgsEntity();
            paramargs.OrderBy = " id asc ";
            paramargs.AdditionalParams = string.Empty;
            paramargs.StartIndex = 1;
            paramargs.RowCount = 10;


            //Get Distinct productCode list
            List<ProductEntity> productList = ProductBR.Instance.GetCustomerProducts(paramargs);
            List<string> productCodeList = productList.Select(x => x.Code).Distinct().ToList();
            List<string> productCodes = new List<string>() { "NA", "NA", "NA", "NA", "NA", "NA" };
            for (int i = 0; i < productCodeList.Count; i++)
            {
                productCodes[i] = productCodeList[i].ToString();
            }

            //Get Distinct merchandiseCode list
            List<MerchandiseEntity> merchandiseList = MerchandiseBR.Instance.GetCustomerMerchandise(paramargs);
            List<string> merchandiseCodeList = merchandiseList.Select(x => x.Code).Distinct().ToList();
            List<string> merchandiseCodes = new List<string>() { "NA", "NA", "NA", "NA", "NA", "NA" };
            for (int i = 0; i < merchandiseCodeList.Count; i++)
            {
                merchandiseCodes[i] = merchandiseCodeList[i].ToString();
            }

            //Get Distinct objectiveCode list
            List<ObjectiveEntity> objectiveList = ObjectiveBR.Instance.GetCustomerObjectives(paramargs);
            List<string> objectiveCodeList = objectiveList.Select(x => x.Code).Distinct().ToList();
            List<string> objectiveCodes = new List<string>() { "NA", "NA", "NA", "NA", "NA", "NA" };
            for (int i = 0; i < objectiveCodeList.Count; i++)
            {
                objectiveCodes[i] = objectiveCodeList[i].ToString();
            }


            foreach (DataRow drow in ds.Tables["DailyEffectiveGeneralDetail"].Rows)
            {
                string custCode = drow["cust_code"].ToString();

                //Fill products data
                var cusProductList = productList.Where(c => c.CustomerCode.Equals(custCode)).ToList();
                int productColumnCount = 6;
                for (int r = 0; r < productColumnCount; r++)
                {
                    string columnName = "productColumn" + (r + 1).ToString();
                    drow[columnName] = "NA";
                }

                for (int x = 0; x < productCodeList.Count; x++)
                {
                    string columnName = "productColumn" + (x + 1).ToString();
                    drow[columnName] = "N";
                    

                    List<ProductEntity> pfilterList = cusProductList.Where(p => p.Code.Equals(productCodeList[x].ToString())).ToList();
                    if (pfilterList.Count > 0) { drow[columnName] = "Y"; }
                }


                //Fill Merchandise data
                var cusMerchandiseList = merchandiseList.Where(c => c.CustomerCode.Equals(custCode)).ToList();
                int merchandiseColumnCount = 5;
                for (int a = 0; a < merchandiseColumnCount; a++)
                {
                    string columnName = "merchandiseColumn" + (a + 1).ToString();
                    drow[columnName] = "NA";
                }

                for (int b = 0; b < merchandiseCodeList.Count; b++)
                {
                    string columnName = "merchandiseColumn" + (b + 1).ToString();
                    drow[columnName] = "N";

                    List<MerchandiseEntity> mfilterList = cusMerchandiseList.Where(m => m.Code.Equals(merchandiseCodeList[b].ToString())).ToList();
                    if (mfilterList.Count > 0) { drow[columnName] = "Y"; }
                }


                //Fill Objective data
                var cusObjectiveList = objectiveList.Where(c => c.CustomerCode.Equals(custCode)).ToList();
                int objectiveColumnCount = 5;
                for (int c = 0; c < objectiveColumnCount; c++)
                {
                    string columnName = "objectiveColumn" + (c + 1).ToString();
                    drow[columnName] = "NA";
                }

                for (int d = 0; d < objectiveCodeList.Count; d++)
                {
                    string columnName = "objectiveColumn" + (d + 1).ToString();
                    drow[columnName] = "N";

                    List<ObjectiveEntity> ofilterList = cusObjectiveList.Where(m => m.Code.Equals(objectiveCodeList[d].ToString())).ToList();
                    if (ofilterList.Count > 0) { drow[columnName] = "Y"; }
                }

            }  
        }
        catch (Exception)
        {

        }

        return ds;
    }

    private void GenerataReport()
    {
        if (Request.QueryString["fromDate"] != "" && Request.QueryString["toDate"] != "")
        {
            ReportDocument rptDoc = new ReportDocument();
            rptDoc.Load(Server.MapPath("dailyeffectivereport.rpt"));

            ArgsEntity args = new ArgsEntity();
            args.SStartDate = Request.QueryString["fromDate"];
            args.SEndDate = Request.QueryString["toDate"];
            DailyEffectiveDataSet ds = CustomerBR.Instance.GetDailyEffectiveReportData(args);

            //Handle product details
            ArgsEntity paramargs = new ArgsEntity();
            paramargs.OrderBy = " id asc ";
            paramargs.AdditionalParams = string.Empty;
            paramargs.StartIndex = 1;
            paramargs.RowCount = 10;


            List<ProductEntity> productList = ProductBR.Instance.GetCustomerProducts(paramargs);
            List<string> productCodeList = productList.Select(x => x.Code).Distinct().ToList();
            List<string> productCodes = new List<string>() { "NA", "NA", "NA", "NA", "NA", "NA" };
            for (int i = 0; i < productCodeList.Count; i++)
            {
                productCodes[i] = productCodeList[i].ToString();
            }


            List<MerchandiseEntity> merchandiseList = MerchandiseBR.Instance.GetCustomerMerchandise(paramargs);
            List<string> merchandiseCodeList = merchandiseList.Select(x => x.Code).Distinct().ToList();
            List<string> merchandiseCodes = new List<string>() { "NA", "NA", "NA", "NA", "NA", "NA" };
            for (int i = 0; i < merchandiseCodeList.Count; i++)
            {
                merchandiseCodes[i] = merchandiseCodeList[i].ToString();
            }


            List<ObjectiveEntity> objectiveList = ObjectiveBR.Instance.GetCustomerObjectives(paramargs);
            List<string> objectiveCodeList = objectiveList.Select(x => x.Code).Distinct().ToList();
            List<string> objectiveCodes = new List<string>() { "NA", "NA", "NA", "NA", "NA", "NA" };
            for (int i = 0; i < objectiveCodeList.Count; i++)
            {
                objectiveCodes[i] = objectiveCodeList[i].ToString();
            }


            foreach (DataRow drow in ds.Tables["DailyEffectiveGeneralDetail"].Rows)
	        {
                string custCode = drow["cust_code"].ToString();

                //Fill products data
                var cusProductList = productList.Where(c=> c.CustomerCode.Equals(custCode)).ToList();
                int productColumnCount = 6;
                for (int r = 0; r < productColumnCount; r++)
			    {           
                   string columnName = "productColumn" + (r + 1).ToString();
                   drow[columnName] = "NA";
			    }

                for (int x = 0; x < productCodeList.Count; x++)
			    {
                    string columnName = "productColumn" + (x + 1).ToString();
                    drow[columnName] = "N";

                    List<ProductEntity> pfilterList = cusProductList.Where(p => p.Code.Equals(productCodeList[x].ToString())).ToList();
                    if (pfilterList.Count > 0) { drow[columnName] = "Y"; }
			    }


                //Fill Merchandise data
                var cusMerchandiseList = merchandiseList.Where(c => c.CustomerCode.Equals(custCode)).ToList();
                int merchandiseColumnCount = 4;
                for (int a = 0; a < merchandiseColumnCount; a++)
                {
                    string columnName = "merchandiseColumn" + (a + 1).ToString();
                    drow[columnName] = "NA";
                }

                for (int b = 0; b < merchandiseCodeList.Count; b++)
                {
                    string columnName = "merchandiseColumn" + (b + 1).ToString();
                    drow[columnName] = "N";

                    List<MerchandiseEntity> mfilterList = cusMerchandiseList.Where(m => m.Code.Equals(merchandiseCodeList[b].ToString())).ToList();
                    if (mfilterList.Count > 0) { drow[columnName] = "Y"; }
                }


                //Fill Objective data
                var cusObjectiveList = objectiveList.Where(c => c.CustomerCode.Equals(custCode)).ToList();
                int objectiveColumnCount = 5;
                for (int c = 0; c < objectiveColumnCount; c++)
                {
                    string columnName = "objectiveColumn" + (c + 1).ToString();
                    drow[columnName] = "NA";
                }

                for (int d = 0; d < objectiveCodeList.Count; d++)
                {
                    string columnName = "objectiveColumn" + (d + 1).ToString();
                    drow[columnName] = "N";

                    List<ObjectiveEntity> ofilterList = cusObjectiveList.Where(m => m.Code.Equals(objectiveCodeList[d].ToString())).ToList();
                    if (ofilterList.Count > 0) { drow[columnName] = "Y"; }
                }
               
	        }  


            rptDoc.SetDataSource(ds);

            rptDoc.SetParameterValue("productHeader1", productCodes[0].ToString());
            rptDoc.SetParameterValue("productHeader2", productCodes[1].ToString());
            rptDoc.SetParameterValue("productHeader3", productCodes[2].ToString());
            rptDoc.SetParameterValue("productHeader4", productCodes[3].ToString());
            rptDoc.SetParameterValue("productHeader5", productCodes[4].ToString());
            rptDoc.SetParameterValue("productHeader6", productCodes[5].ToString());

            rptDoc.SetParameterValue("merchandiseHeader1", merchandiseCodes[0].ToString());
            rptDoc.SetParameterValue("merchandiseHeader2", merchandiseCodes[1].ToString());
            rptDoc.SetParameterValue("merchandiseHeader3", merchandiseCodes[2].ToString());
            rptDoc.SetParameterValue("merchandiseHeader4", merchandiseCodes[3].ToString());

            rptDoc.SetParameterValue("objectiveHeader1", objectiveCodes[0].ToString());
            rptDoc.SetParameterValue("objectiveHeader2", objectiveCodes[1].ToString());
            rptDoc.SetParameterValue("objectiveHeader3", objectiveCodes[2].ToString());
            rptDoc.SetParameterValue("objectiveHeader4", objectiveCodes[3].ToString());
            rptDoc.SetParameterValue("objectiveHeader5", objectiveCodes[4].ToString());


            //CrystalReportViewer1.ReportSource = rptDoc;
            //Session["DailyEffectiveReportSource"] = rptDoc;
            //CrystalReportViewer1.DataBind();

            //SavePDF(Server.MapPath(GenerateFileName("dailyeffectivereport", Session.SessionID.ToString(), DateTime.Now.Minute.ToString())) + ".pdf", GenerateFileName("dailyeffectivereport", Session.SessionID.ToString(), DateTime.Now.Minute.ToString()) + ".pdf", rptDoc);
            //SaveToEXCEL(Server.MapPath(GenerateFileName("dailyeffectivereport", Session.SessionID.ToString(), DateTime.Now.Minute.ToString())) + ".xls", GenerateFileName("dailyeffectivereport", Session.SessionID.ToString(), DateTime.Now.Minute.ToString()) + ".xls", rptDoc);

            string destinationFileName = Server.MapPath(GenerateFileName("dailyeffectivereport", Session.SessionID.ToString(), DateTime.Now.Minute.ToString())) + ".xls";
            this.ExportReport("EXCEL", destinationFileName, rptDoc);

        }
    }

    //-------------- Common method for report export function -----------------
    public void ExportReport(string fileType, string destinationFileName, ReportDocument rptDoc)
    {
        ExportOptions ex = new ExportOptions();
        DiskFileDestinationOptions destinationURL = new DiskFileDestinationOptions();
        ex.ExportDestinationType = ExportDestinationType.DiskFile;
        ex.ExportDestinationOptions = destinationURL;

        switch (fileType.ToUpper())
        {
            case "PDF":
                {
                    PdfRtfWordFormatOptions formatOptionPDF = new PdfRtfWordFormatOptions();
                    destinationURL.DiskFileName = destinationFileName + ".pdf";
                    ex.ExportFormatType = ExportFormatType.PortableDocFormat;
                    ex.ExportFormatOptions = formatOptionPDF;
                    rptDoc.Export(ex);
                    break;
                }

            case "EXCEL":
                {
                    ExcelFormatOptions formatOptionEXCEL = new ExcelFormatOptions();
                    destinationURL.DiskFileName = destinationFileName + ".xls";
                    ex.ExportFormatType = ExportFormatType.Excel;
                    ex.ExportFormatOptions = formatOptionEXCEL;
                    rptDoc.Export(ex);
                    break;
                }

            case "WORD":
                {
                    PdfRtfWordFormatOptions formatOptionWORD = new PdfRtfWordFormatOptions();
                    destinationURL.DiskFileName = destinationFileName + ".doc";
                    ex.ExportFormatType = ExportFormatType.WordForWindows;
                    ex.ExportFormatOptions = formatOptionWORD;
                    rptDoc.Export(ex);
                    break;
                }

        }
    }

    private void SaveToPDF(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.PortableDocFormat, downloadAsFilename);
        //string FilePath = Server.MapPath(generatefilename);
        string FilePath = Server.MapPath(generatefilename);
        WebClient User = new WebClient();
        Byte[] FileBuffer = User.DownloadData(FilePath);
        if (FileBuffer != null)
        {
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-length", FileBuffer.Length.ToString());
            Response.BinaryWrite(FileBuffer);
        }
    }

    private void SaveToEXCEL(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.Excel, downloadAsFilename);
        //string FilePath = Server.MapPath(generatefilename);
        string FilePath = Server.MapPath(generatefilename);
        WebClient User = new WebClient();
        Byte[] FileBuffer = User.DownloadData(FilePath);
        if (FileBuffer != null)
        {
            Response.ContentType = "application/x-msexcel";
            Response.AddHeader("content-length", FileBuffer.Length.ToString());
            Response.BinaryWrite(FileBuffer);
        }
    }

    private string GenerateFileName(string ori_filename, string sessionid, string jobid)
    {
        /// TODO:
        /// Generate file name according to system path.
        string fn = "";
        DateTime d = DateTime.Now;
        fn += "../../docs/tmp_" + sessionid.Trim() + "_" + ori_filename.Trim() + "_";
        fn += jobid.Trim();   //to generate a unique name
        return fn;
    }


    


 



}