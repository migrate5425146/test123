﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="daily_effective_viewer.aspx.cs" Inherits="sales_info_reports_daily_effective_viewer" %>

<%@ Register Assembly="CrystalDecisions.Web, Version=13.0.4000.0, Culture=neutral, PublicKeyToken=692fbea5521e1304"
    Namespace="CrystalDecisions.Web" TagPrefix="CR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server"> 

  <div class="divcontectmainforms">
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" HasCrystalLogo="False"
        HasToggleGroupTreeButton="False" EnableDatabaseLogonPrompt="False" EnableParameterPrompt="False"
        BorderWidth="1px" Height="300px" ReuseParameterValuesOnRefresh="True" Width="450px" />
    </div>

</asp:Content>
