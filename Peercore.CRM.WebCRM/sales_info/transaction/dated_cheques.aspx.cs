﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using System.Text;

public partial class sales_info_transaction_dated_cheques : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
                dtpDate.Text = DateTime.Today.ToString("dd-MMM-yyyy");
                dtpDateTo.Text = DateTime.Today.ToString("dd-MMM-yyyy");

                SetBackLink();

                //Remove Other Session.
                ClearSession();
                HiddenFieldIsContinueWithSave.Value = string.Empty;
            }

            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Dated Cheques ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    #region Methods

    private void SetBackLink()
    {
        if (Request.QueryString["ty"] != null && Request.QueryString["ty"] != string.Empty)
        {
            HiddenFieldLeadStage.Value = Request.QueryString["ty"].ToString();
        }

        if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty)
        {
            if (Request.QueryString["fm"] == "home")
            {
                hlBack.NavigateUrl = "~/default.aspx?fm=home";
            }
            else if (Request.QueryString["fm"] == "activity")
            {
                hlBack.NavigateUrl = "~/activity_planner/transaction/activity_scheduler.aspx";
            }
            else if (Request.QueryString["fm"] == "act")
            {
                hlBack.NavigateUrl = "~/calendar/transaction/appointment_entry.aspx";
            }

            else if (Request.QueryString["fm"] == "opp")
            {
                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "pipelines_stage/transaction/opportunity.aspx?cht=pipe&fm=lead";//?cht=pipe&fm=lead&pipid=1";
            }
            else if (Request.QueryString["fm"] == "oppent")
            {
                StringBuilder additionalParam = new StringBuilder("?fm=Lead");

                if (Request.QueryString["oleadid"] != null && Request.QueryString["oleadid"] != string.Empty)
                    additionalParam.Append("&leadid=" + Request.QueryString["oleadid"]);

                if (Request.QueryString["oppid"] != null && Request.QueryString["oppid"] != string.Empty)
                    additionalParam.Append("&oppid=" + Request.QueryString["oppid"]);

                if (Request.QueryString["custid"] != null && Request.QueryString["custid"] != string.Empty)
                    additionalParam.Append("&custid=" + Request.QueryString["custid"]);

                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "pipelines_stage/transaction/opportunityentry.aspx" + additionalParam.ToString();
            }
            else if (Request.QueryString["fm"] == "lead")
            {
                if (Request.QueryString["ty"] != null && Request.QueryString["ty"] != string.Empty)
                {
                    hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "lead_customer/transaction/lead_contacts.aspx?ty=" + Request.QueryString["ty"] + "&fm=lead";
                }
            }
            else if (Request.QueryString["fm"] == "search")
            {
                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "lead_customer/transaction/search.aspx";
            }

            else if (Request.QueryString["fm"] == "dsbd1")
            {
                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/ActivityAnalysis.aspx";
            }
            else if (Request.QueryString["fm"] == "dsbd2")
            {
                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/OpportunityAnalysis.aspx";
            }
        }
    }
    private void ClearSession()
    {
        Session[CommonUtility.CUSTOMER_DATA] = null;
    }

    #endregion
}