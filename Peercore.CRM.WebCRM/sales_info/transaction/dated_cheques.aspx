﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="dated_cheques.aspx.cs" Inherits="sales_info_transaction_dated_cheques" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnablePageMethods="true">
    </telerik:RadScriptManager>
    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <%--<asp:HiddenField ID="HiddenFieldCheques" runat="server" />--%>
    <asp:HiddenField ID="HiddenFieldIsContinueWithSave" runat="server" />
    <asp:HiddenField ID="HiddenFieldLeadStage" runat="server" />
    <script type="text/javascript">

    </script>
    <div id="div_promt" style="display: none">
    </div>
    <div id="modalWindow" style="display: none">
        <div id="div_duplicaterecord" runat="server" style="display: block">
        </div>
        <div id="div_confirm_message" class="msg">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
        <button id="save" class="k-button">
            Save</button>
    </div>
    <div style="display: none">
        <div id="div_prevcustcode" runat="server">
        </div>
    </div>
    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <%--<div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div>
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                </div>
            </div>--%>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 35%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
                        <div class="back"></div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div class="clearall">
        </div>
        <div class="clearall">
        </div>
        <div class="right">
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div id="div_info" runat="server" style="display: none">
        </div>
        <div class="formleft">
            <%--            <div class="formtextdiv">
                Bank Account
            </div>
            <div class="formdetaildivlead">
                <asp:DropDownList ID="dropdownBankAccount" runat="server">
                    <asp:ListItem>1</asp:ListItem>
                    <asp:ListItem>2</asp:ListItem>
                    <asp:ListItem>13</asp:ListItem>
                    <asp:ListItem>19</asp:ListItem>
                    <asp:ListItem>22</asp:ListItem>
                    <asp:ListItem>29</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="clearall">
            </div>--%>
            <div class="formtextdiv">
                From Date
            </div>
            <div class="formdetaildiv_right" style="margin-top: -10px;">
                <div class="datepicker" id="div_Date" style="display: none">
                    <%--                    <asp:Label ID="lbDate" runat="server" Text="Start Date" Visible="false"></asp:Label>--%>
                    <asp:TextBox ID="dtpDate" runat="server" ClientIDMode="Static" Style="width: 120px;"></asp:TextBox>
                </div>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                To Date
            </div>
            <div class="formdetaildiv_right" style="margin-top: -10px;">
                <div class="datepicker" id="div_Date_to" style="display: none">
                    <%--                    <asp:Label ID="lbDate" runat="server" Text="Start Date" Visible="false"></asp:Label>--%>
                    <asp:TextBox ID="dtpDateTo" runat="server" ClientIDMode="Static" Style="width: 120px;"></asp:TextBox>
                </div>
            </div>
            <div class="clearall">
            </div>
            <%--            <div class="formtextdiv">
                Cheque Status
            </div>
            <div class="formdetaildivlead">
                <asp:DropDownList ID="dropdownChequeStatus" runat="server">
                </asp:DropDownList>
            </div>
            <div class="clearall">
            </div>--%>
            <div class="formtextdiv">
            </div>
            <div class="formdetaildivlead">
                <input type="button" id="btn_filter" value="Search" class="searchbtn1" />
            </div>
            <div class="clearall">
            </div>
        </div>
        <div class="clearall">
        </div>
        <div class="grid_container" id="div_mainDatedCheques">
            <div class="grid_title">
                Dated Cheques
            </div>
            <div id="div_DatedCheques">
            </div>
        </div>
    </div>
    <div>
        &nbsp;</div>
    <div runat="server" id="div_autocomplete">
    </div>
    <script type="text/javascript">



        $(document).ready(function () {
            hideStatusDiv("MainContent_div_message");
            $("#div_Date").css("display", "inline-block");
            $("#div_Date_to").css("display", "inline-block");
            $("#dtpDate").kendoDatePicker({
                // display month and year in the input
                format: "dd-MMM-yyyy"

            });
            $("#dtpDateTo").kendoDatePicker({
                // display month and year in the input
                format: "dd-MMM-yyyy"

            });

            $("#div_mainDatedCheques").css("display", "block");

            var createdDate = $("#dtpDate").val();
            var toDate = $("#dtpDateTo").val();

            loadDatedCheques(createdDate, toDate);

            $("#btn_filter").click(function () {

                var createdDate = $("#dtpDate").val();
                var toDate = $("#dtpDateTo").val();

                loadDatedCheques(createdDate, toDate);
            });
        });


        //        $("#MainContent_buttonbar_buttinSave").click(function () {
        //            entityGrid = $("#div_Cheques").data("kendoGrid");
        //            $("#MainContent_HiddenFieldCheques").val(JSON.stringify(entityGrid.dataSource.view()));
        //        });
        
    </script>
</asp:Content>
