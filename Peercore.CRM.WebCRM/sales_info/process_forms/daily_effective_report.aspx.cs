﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Common;


public partial class sales_info_process_forms_daily_effective_report : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Remove the session before setting the breadcrumb.
        Session.Remove(CommonUtility.BREADCRUMB);
        Master.SetBreadCrumb("Daily Effectiveness Report ", "#", "");
        buttonbar1.onButtonFilter = new usercontrols_buttonbar_analysisgraph.ButtonFilter(ButtonFilter_Click);

        if (!Page.IsPostBack)
        {
            buttonbar1.SetVisible(2);
        }
    }

    protected void ButtonFilter_Click(object sender)
    {
        GenerateReport();
    }

    private void GenerateReport()
    {
        //string startDateTime = startDate.Value+ " 00:00:00";
        //string endDateTime = endDate.Value + " 23:59:59";
        string fromDateTime = Server.UrlDecode(buttonbar1.GetFromDateText() + " 00:00:00");
        string toDateTime = Server.UrlDecode(buttonbar1.GetToDateText() + " 23:59:59");

        try
        {
            Response.Redirect("../../sales_info/reports/daily_effective_viewer.aspx?fromDate=" + fromDateTime + "&toDate=" + toDateTime);
        }
        catch (Exception)
        {
            throw;
        }
    }

}