﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using System.IO;
using System.Web.Services;
using System.Xml.Serialization;
using System.Configuration;
using CRMServiceReference;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using System.Web.Script.Serialization;


public partial class sales_info_process_forms_processmaster : System.Web.UI.Page
{

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != "")
        {
            if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
            {
                if (Request.QueryString["type"] == "insert")
                {
                    if (Request.QueryString["fm"] == "marketentry")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = SaveMarket();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                }

                else if (Request.QueryString["type"] == "select")
                {
                    if (Request.QueryString["fm"].ToLower() == "tmemarkets")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = GetOriginatorsByCatergory();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                }

                else if (Request.QueryString["type"] == "select")
                {
                    if (Request.QueryString["fm"].ToLower() == "originatormarkets")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = "";
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                }

                else if (Request.QueryString["type"] == "update")
                {
                    if (Request.QueryString["fm"].ToLower() == "updatemarkets")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = UpdateOriginatorMarkets();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                }

                else if (Request.QueryString["type"] == "delete")
                {
                    if (Request.QueryString["fm"].ToLower() == "deletemarkets")
                    {
                       // int marketid = Convert.ToInt32(Request.QueryString["marketid"].ToString());
                        Response.Expires = -1;
                        Response.Clear();
                        string getdocumentContent = DeleteOriginatorMarkets();
                        Response.ContentType = "text/html";
                        Response.Write(getdocumentContent);
                        Response.End();
                    }
                }
                

            }
        }
    }

    #endregion


    private string SaveMarket()
    {
        StringBuilder sb = new StringBuilder();
        CommonClient commonClient = new CommonClient();
        string str = string.Empty;

        try
        {
            string selectedMarketId = Request.QueryString["selectedMarketId"];
            string marketName = Request.QueryString["marketName"];

            MarketDTO marketDTO = new MarketDTO();
            //marketDTO.isNew = true;
            marketDTO.MarketId = string.IsNullOrEmpty(selectedMarketId) ? 0 : Convert.ToInt32(selectedMarketId);
            marketDTO.MarketName = marketName;
            marketDTO.Status = "A";

            if (IsMarketExists(marketDTO))
                return "exists";

            //marketDTO.Originator = UserSession.Instance.UserName; - should be null

            bool status = commonClient.SaveMarket(marketDTO);
            str = (status == true) ? "true" : "false";
            sb.Append(str);
        }
        catch (Exception)
        {
            throw;
        }
        return sb.ToString();
    }

    private string GetOriginatorsByCatergory()
    {
        StringBuilder txt = new StringBuilder();
        OriginatorClient originatorClient = new OriginatorClient();
        ArgsDTO args = new ArgsDTO();

        if (UserSession.Instance != null)
            args.ChildOriginators = UserSession.Instance.ChildOriginators;

        if (Request.QueryString["catergory"] != null && Request.QueryString["catergory"] != "")
        {
             args.AdditionalParams = " dept_string = '" + Request.QueryString["catergory"].ToUpper() + "'";
        }

        args.StartIndex = ConfigUtil.StartIndex;
        args.RowCount = ConfigUtil.MaxRowCount;
        args.OrderBy = " name asc ";

        List<OriginatorDTO> originatorList = originatorClient.GetOriginatorsByCatergory(args);

        if (originatorList != null && originatorList.Count != 0)
        {
            txt.Append("<table border=\"0\" id=\"originatorTable\" width=\"270px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
            txt.Append("<tr class=\"detail-popsheader\">");
            txt.Append("<th></th>");
            txt.Append("<th>Originator</th>");
            txt.Append("</tr>");

            for (int i = 0; i < originatorList.Count; i++)
            {
                txt.Append("<tr>");
                txt.Append("<td style=\"padding-left:5px;\" class=\"tblborder\"> <input type=\"radio\" name=\"group1\" value=\"" + originatorList[i].Name + "\" id=\"a\"/> <input type=\"hidden\" name=\"group1\" value=\"" + originatorList[i].UserName + "\" id=\"b\"/>   </td>");
                txt.Append("<td align=\"left\" class=\"tblborder\">" + originatorList[i].Name + "</td>");
                txt.Append("</tr>");
            }

            txt.Append("</table>");

            txt.Append("<button id=\"jqi_state0_buttonOk\" class=\"k-button\">Ok</button>");
        }
        else
        {
            txt.Append("<table border=\"0\" id=\"originatorTable\" width=\"270px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
            txt.Append("<tr class=\"detail-popsheader\">");
            txt.Append("<th></th>");
            txt.Append("<th>Originator</th>");
            txt.Append("<th>Name</th>");
            txt.Append("</tr>");
            txt.Append("</table>");
        }

        return txt.ToString();
    }


    private string UpdateOriginatorMarkets()
    {
        try
        {
          List<MarketDTO> marketList = null;
          marketList = (Session[CommonUtility.ORIGINATOR_MARKETS] != null) ? (List<MarketDTO>)Session[CommonUtility.ORIGINATOR_MARKETS] : new List<MarketDTO>();
          MarketDTO obj = new MarketDTO();
          int marketId = 0; 
          string marketName = string.Empty;
          string originator = string.Empty;

          if (Request.QueryString["marketid"] != null && Request.QueryString["marketid"] != "")
          {
              marketId = Convert.ToInt32(Request.QueryString["marketid"].ToString());
          }

          if (Request.QueryString["marketname"] != null && Request.QueryString["marketname"] != "")
          {
              marketName = Request.QueryString["marketname"].ToString();
          }

          if (Request.QueryString["originatorId"] != null && Request.QueryString["originatorId"] != "")
          {
              originator = Request.QueryString["originatorId"].ToString();
          }

          var filterList = marketList.Where(x => x.MarketId == marketId).ToList();
          if (filterList.Count > 0)
          {
              return "Market already assigned.Please select another market.";
          }

          obj.MarketId = marketId;
          obj.MarketName = marketName;
          obj.Originator = originator;
          marketList.Add(obj);
          Session[CommonUtility.ORIGINATOR_MARKETS] = marketList;
          return "true";
        }
        catch (Exception ex)
        {
            return "false";
        }
    }


    private string DeleteOriginatorMarkets()
    {
        try
        {
            CommonClient commonService = new CommonClient();
            MarketDTO deletedmarket = new MarketDTO();
            List<MarketDTO> marketList = null;
            marketList = (Session[CommonUtility.ORIGINATOR_MARKETS] != null) ? (List<MarketDTO>)Session[CommonUtility.ORIGINATOR_MARKETS] : new List<MarketDTO>();
            int marketId = 0;

            if (Request.QueryString["marketid"] != null && Request.QueryString["marketid"] != "")
            {
                marketId = Convert.ToInt32(Request.QueryString["marketid"].ToString());
            }

            deletedmarket.MarketId = marketId;
            bool status = commonService.DeleteTMEMarkets(deletedmarket);


            int index = marketList.FindIndex(x => x.MarketId == marketId);
            marketList.RemoveAt(index);
            Session[CommonUtility.ORIGINATOR_MARKETS] = marketList;
            return "true";
        }
        catch (Exception)
        {
            return "false";         
        }
    }

    private bool IsMarketExists(MarketDTO marketDTO)
    {
        CommonClient commonClient = new CommonClient();
        try
        {
            return commonClient.IsMarketExists(marketDTO); ;
        }
        catch (Exception)
        {
            return true;
        }
    }
}