﻿<%@ Page Title="mSales - Invoices" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="InvoiceView.aspx.cs" Inherits="Invoice_transaction_InvoiceView" %>
<%@ Register Src="~/usercontrols/buttonbar_reports.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ Register Src="~/usercontrols/toolbar_reports.ascx" TagPrefix="ucl" TagName="toolbar" %>


<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfSessionId" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server" />
    <asp:HiddenField ID="invId" runat="server" />
    <div id="InvoicemodalWindow" style="display: none">
        <div id="div_InvoiceConfirm_Message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>
    <div class="divcontectmainforms" id="maincont">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">

                    <div class="formtextdiv input-mini">
                        <span>Start Date</span>
                        <div>
                            <asp:TextBox ID="txtStartDate" runat="server" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>

                    <div class="formtextdiv input-mini">
                        <span>End Date</span>
                        <div>
                            <asp:TextBox ID="txtEndDate" runat="server" ClientIDMode="Static"></asp:TextBox>
                        </div>
                    </div>

                    <div class="formtextdiv input-mini">
                        <span>ASM</span>
                        <div>
                            <asp:DropDownList ID="ddlASEList" runat="server" ClientIDMode="Static" Style="width: 180px;">
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div class="formtextdiv input-mini">
    <span>QuantityType</span>
    <div>
        <asp:DropDownList ID="txtQuantityType" runat="server" ClientIDMode="Static" Style="width: 150px;" 
            >
            <asp:ListItem Text="Units" Value="N" />
            <asp:ListItem Text="Casses" Value="C" />
            <asp:ListItem Text="Tonnage" Value="T" />
        </asp:DropDownList>
    </div>
</div>

                   

                   
                    <div class="formtextdiv input-mini">
                        <span></span>
                        <div style="width: 245px">
                            <a class="searchbtn sprite_button" id="a_search" style="margin-left: 10px; margin-top: 19px;">Search</a>
                            <a id="id_bulk_inactive" runat="server" class="sprite_button cd-popup-trigger" style="display: inline-block; margin-left: 10px; margin-top: 19px;"><span class="cross icon"></span>Bulk Delete</a>

                        </div>
                    </div>
                </div>
            </div>

            <%--<div class="toolbar_right" id="div1">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 35%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
            <div class="back"></div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>--%>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none">
        </div>
        <div id="div_info" runat="server" style="display: none">
        </div>
        <div id="div_promt" style="display: none">
        </div>
        <%--<div class="toolbar" id="toolbar" runat="server">
        </div>--%>
        <%-- <div class="clearall">
        </div>--%>

        <div class="formleft" style="width: 95%">
            <asp:HyperLink ID="linkBulkDeleteErrorLog" runat="server" Visible="false" target="_blank"><p style="text-align:end;">Bulk delete error log..</p></asp:HyperLink>
        </div>

        <div class="clearall">
        </div>
        <div id="grid_invoice">
        </div>
        <div style="padding-left: 10px;">
                    <ucl:buttonbar ID="buttonbar1" runat="server" />
                </div>
    </div>
    <div id="popup">
        <div>
            <b>Please enter admin password:</b>
        </div>
        <input style="width: 100%;" id="adminPass" type="password" />
        <button type="button" onclick="confirmAdmin()">
            Confirm</button>
        <button type="button" onclick="exitAdmin()">
            Cancel</button>
    </div>
    <div class="cd-popup" role="alert">
        
        <div class="cd-popup-container">
            <p>
                <textarea id="txtBulkDeleteInvoiceList" rows="10" cols="50"
                    placeholder="Eg: REP001/TR001/200311180650/00000001, 
                    REP001/TR001/200311180614/00000130002, 
                    REP001/TR001/200311180531/00000003, 
                    REP001/TR001/200311180451/00000004"
                    style="display: none;">
               </textarea>
                <asp:FileUpload ID="fileuploadBulkDeleteInvoice"  accept=".txt" runat="server" />
            </p>
            <ul class="cd-buttons">
                <li>
                    <button id="btnBulkDeleteInvoice" runat="server" onserverclick="fileuploadBulkDeleteInvoice_Click">Delete</button></li>
                <li>
                    <button class="cd-popup-close">No</button></li>
            </ul>
        </div>
        <!-- cd-popup-container -->
    </div>
    <script type="text/x-kendo-template" id="template">
                <div class="tabstrip">
                    <ul>                        
                        <li class="k-state-active">
                            Invoice Details
                        </li>
                        <li>
                            Discount Scheme
                        </li>
                    </ul>
                    <div>
                        <div class="invoicedetail"></div>
                    </div>
                    <div>
                        <div class="invoiceschemegroup"></div>
                    </div>
                    
                </div>

    </script>

    <!--Popup Script-->
    <script>
        jQuery(document).ready(function ($) {
            //open popup
            $('.cd-popup-trigger').on('click', function (event) {
                event.preventDefault();
                $('.cd-popup').addClass('is-visible');
            });

            //close popup
            $('.cd-popup').on('click', function (event) {
                if ($(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup')) {
                    event.preventDefault();
                    $(this).removeClass('is-visible');
                }
            });

            //close popup when clicking the esc keyboard button
            $(document).keyup(function (event) {
                if (event.which == '27') {
                    $('.cd-popup').removeClass('is-visible');
                }
            });
        });
    </script>
    <!--Popup Script-->
    <script type="text/javascript">
        var DeleteInvId = 0;
        $(document).ready(function () {
            $("#txtStartDate").kendoDatePicker({
                //value: new Date(),
                format: "dd-MMM-yyyy"
                //change: startDate_onChange
            });

            $("#txtEndDate").kendoDatePicker({
                //value: new Date(),
                format: "dd-MMM-yyyy"
                //change: startDate_onChange
            });

            var fromDate = $("#txtStartDate").val();
            var toDate = $("#txtEndDate").val();

            //loadInvoice(fromDate, toDate);
        });
        $("#a_search").click(function () {
            var fromDate = $("#txtStartDate").val();
            var toDate = $("#txtEndDate").val();
            var ase_val = $("#ddlASEList").val();
            var qty_typ = $("#txtQuantityType").val();

            loadInvoice(fromDate, toDate, ase_val,qty_typ);
        });


        function loadInvoice(startDate, endDate, aseVal, quantityType) {
            $("#grid_invoice").html("");
            $("#grid_invoice").kendoGrid({
                columns: [
                    { template: '<a href="javascript:deleteInvoice(\'#=InvoiceId#\', \'#=InvoiceNo#\', \'#=Status#\');">Delete</a>', field: "", width: "60px" },
                    { field: "InvoiceId", width: "120px", title: "InvoiceId", hidden: true },
                    { field: "InvoiceNo", title: "Invoice No", width: 150 },
                    { field: "CustCode", title: "Outlet No", width: 70 },
                    { field: "CustomerName", title: "Outlet", width: 200 },
                    { field: "RepCode", title: "Rep Code", width: 80 },
                    { field: "InvoiceDate", title: "Invoice Date", width: 80, format: "{0:MM/dd/yyyy}" },
                    //{ field: "GrossTotal", title: "Gross Total", width: 100, format: NUMBER_FORMAT_DECIAML },
                    { field: "Discount", title: "Discount Total", width: 110, format: NUMBER_FORMAT_DECIAML },
                    //{ field: "ReturnsTotal", title: "Return Total", width: 100, format: NUMBER_FORMAT_DECIAML },
                    { field: "GrossTotal", width: 100, title: "Grand Total", format: NUMBER_FORMAT_DECIAML },
                    { field: "Status", title: "Status", width: 100 },
                    { field: "Remark", title: "Remark", width: 70 },
                ],
                //editable: "inline",
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                filterable: true,
                navigatable: true,
                detailTemplate: kendo.template($("#template").html()),
                detailInit: detailInit,
                dataBound: function () {
                    //this.expandRow(this.tbody.find("tr.k-master-row").first());
                    var dataView = this.dataSource.view();
                    for (var i = 0; i < dataView.length; i++) {
                        var uid = dataView[i].uid;
                        if (dataView[i].Status == "D") {
                            $("#grid_invoice tbody").find("tr[data-uid=" + uid + "]").css('background-color', '#d8bfd8');
                        }
                    }
                },
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 15,
                    batch: true,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            //id: "SourceId",
                            fields: {
                                InvoiceId: { type: "number" },
                                InvoiceNo: { type: "string" },
                                CustCode: { type: "string" },
                                InvoiceDate: { type: "date", format: "{0:MM/dd/yyyy}" },

                                GrossTotal: { type: "number" },
                                Discount: { type: "number" },
                                ReturnsTotal: { type: "number" },
                                GrandTotal: { type: "number" },
                                Status: { type: "String" }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/lead_customer/common.asmx/GetAllInvoicesDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { startDate: startDate, endDate: endDate, aseVal: aseVal, quantityType: quantityType },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {

                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }
            });
        }

        /*Delete market Click in Market Assign Page*/
        function deleteInvoice(inv_id, inv_no, status) {
            
            if (status == 'A') {
                $("#InvoicemodalWindow").show()
                showdelete_invoice_confirmation(inv_id, inv_no);
            }
            else if (status == 'D') {
                alert('Adready deleted invoice.');
            }
        }

        function showdelete_invoice_confirmation(inv_id, inv_no) {
            delete_selected_invoice('common_templates/process_forms/processmaster.aspx?fm=deleteproductinvoice&type=delete&invid=' + inv_id);
        }

        /*Show Delete market Confirmation in Market Assign Page*/
        function showdelete_invoice_confirmation1(inv_id, inv_no) {
            $("#save").css("display", "none");
            $("#no").css("display", "inline-block");
            $("#yes").css("display", "inline-block");

            var message = "Do you want to delete '" + inv_no + "' invoice?";

            var wnd = $("#InvoicemodalWindow").kendoWindow({
                title: "Delete Invoice",
                modal: true,
                visible: false,
                resizable: false,
                width: 400
            }).data("kendoWindow");

            $("#div_InvoiceConfirm_Message").text(message);

            wnd.center().open();
            alert(1);
            $("#yes").click(function () {
                alert(2);
                var OriginatorString = $("#<%= OriginatorString.ClientID %>").val();
                if (OriginatorString != "ADMIN") {
                    //document.getElementById('invId').value = invId;
                    DeleteInvId = inv_id;
                    //alert(document.getElementById("invId").value);
                    showPopup();
                    wnd.close();
                }
                else {
                    
                    //delete_selected_invoice('common_templates/process_forms/processmaster.aspx?fm=deleteproductinvoice&type=delete&invid=' + inv_id);
                    wnd.close();
                }
            });

            $("#no").click(function () {
                wnd.close();
            });
        }

        function showPopup() {
            $("#adminPass").val("");
            document.getElementById("popup").style.display = "block";
        }

        function exitAdmin() {
            document.getElementById("popup").style.display = "none";
            return false;
        }

        function confirmAdmin() {

            var password = document.getElementById("adminPass").value;
            if (password == '') {
                var errorMsg = GetErrorMessageDiv("Please Enter admin Password", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);
                hideStatusDiv("MainContent_div_message");
                return;
            }

            var param = { "password": password };
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/GetAdminPassword",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.hasOwnProperty('d')) {

                        msg = response.d;
                        if (msg == 'true') {
                            delete_selected_invoice('common_templates/process_forms/processmaster.aspx?fm=deleteproductinvoice&type=delete&invid=' + DeleteInvId);
                            document.getElementById("popup").style.display = "none";
                        }
                        else {
                            var errorMsg = GetErrorMessageDiv("Invalid admin Password", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(errorMsg);
                            hideStatusDiv("MainContent_div_message");
                            showPopup();
                        }
                    } else {
                        msg = response;

                        var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        hideStatusDiv("MainContent_div_message");
                    }
                },
                error: function (response) {
                    //alert("Oops, something went horribly wrong");                    
                }
            });

        }

        function detailInit(e) {
            var detailRow = e.detailRow;

            detailRow.find(".tabstrip").kendoTabStrip({
                animation: {
                    open: { effects: "fadeIn" }
                }
            });

            detailRow.find(".invoicedetail").kendoGrid({
                dataSource: e.data.InvoiceDetailList,
                scrollable: false,
                sortable: false,
                pageable: false,
                columns: [
                    { field: "InvoiceDetailId", title: "Start Date", width: "90px", hidden: true },
                    { field: "InvoiceId", title: "InvoiceId", width: "90px", hidden: true },
                    { field: "ProductId", title: "ProductId", width: "130px", hidden: true },
                    { field: "ProductName", title: "Product", width: "130px" },
                    { field: "Quantity", title: "Quantity", width: "130px" },
                    { field: "SubTotal", title: "Sub Total", width: "90px", format: NUMBER_FORMAT_DECIAML },
                    { field: "ItemPrice", title: "Item Price", width: "80px", format: NUMBER_FORMAT_DECIAML }
                ]
            });

            detailRow.find(".invoiceschemegroup").kendoGrid({
                dataSource: e.data.InvoiceSchemeGroupList,
                scrollable: false,
                sortable: false,
                pageable: false,
                detailInit: InvoiceSchemeInit,
                columns: [
                    { field: "SchemeGroupId", title: "SchemeGroupId", width: "90px", hidden: true },
                    { field: "InvoiceId", title: "InvoiceId", width: "90px", hidden: true },
                    { field: "SchemeDetailsId", title: "SchemeDetailsId", width: "130px", hidden: true },
                    { field: "SchemeDetailName", title: "Scheme Detail" },
                    { field: "DiscountValue", title: "Discount Value", width: "300px", format: NUMBER_FORMAT_DECIAML }
                ]
            });
        }

        function InvoiceSchemeInit(e) {
            indexId = e.data.IndexId;
            $("<div/>").appendTo(e.detailCell).kendoGrid({
                columns: [
                    { field: "ProductName", title: "Product", hidden: false },
                    { field: "UsedQuantity", title: "Used Quantity", width: "300px", hidden: false }
                ],
                editable: false, // enable editing
                pageable: false,
                sortable: false,
                filterable: false,
                selectable: "single",
                columnMenu: false,
                dataSource: e.data.InvoiceSchemeList
            });
        }

        $("#btnBuldDelete").click(function () {
            var txtDeleteCustList = $("#txtBulkDeleteInvoiceList").val();

            DeleteBulkInvoiceList(txtDeleteCustList);
        });

        function DeleteBulkInvoiceList(val) {
            var confirmResult = confirm("Do you want to delete all these invoices? ");
            if (confirmResult == true) {

                var url = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=deletebulkinvoice&type=delete" + "&val=" + val;

                $.ajax({
                    url: url,
                    dataType: "html",
                    cache: false,
                    success: function (msg) {


                        if (msg == "1") {
                            alert("Successfully Deleted.");
                            $("#MainContent_div_message").show();
                            $("#MainContent_div_message").html("Successfully Deleted.");
                            var grid = $("#grid_invoice").data("kendoGrid");
                            grid.dataSource.read();
                        }
                        else {
                            var msg = msg; // "Sorry there was an error: ";
                            $("#MainContent_div_message").show();
                            $("#MainContent_div_message").html("Can't delete invoices.");
                        }
                    },
                    error: function (msg, textStatus) {
                        //if (textStatus == "error") {
                        //    var msg = msg; // "Sorry there was an error: ";
                        //    $("#MainContent_div_message").show();
                        //    $("#MainContent_div_message").html(msg);
                        //}
                        //else {
                        var msg = msg; // "Sorry there was an error: ";
                        $("#MainContent_div_message").show();
                        $("#MainContent_div_message").html("Can't delete invoices.");
                        //}
                    }
                });
            }
        }

    </script>

        <script type="text/javascript">
            $(document).ready(function () {
                $("#MainContent_toolbar1_a_invoice_summary").css("background-color", "#ff6600");
                $("#btnShowAdvancedOptions").css("display", "none");
                $("#MainContent_buttonbar1_div_VisitNote").css("display", "none");
            });

            function loadpdf(name) {
                $("#target embed").attr("src", name);
            }

        </script>

</asp:Content>
