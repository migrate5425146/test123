﻿using CRMServiceReference;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Media_Management_media_category : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != "")
            {
                if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
                {
                    if (Request.QueryString["type"] == "delete")
                    {
                        if (Request.QueryString["fm"].ToLower() == "deletecategory")
                        {
                            Response.Expires = -1;
                            Response.Clear();
                            string getdocumentContent = DeleteMediaCategory();
                            Response.ContentType = "text/html";
                            Response.Write(getdocumentContent);
                            Response.End();
                        }
                    }
                }
            }
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        Master.SetBreadCrumb("Media Category", "#", "");
        OriginatorString.Value = UserSession.Instance.UserName;
    }


    private string DeleteMediaCategory()
    {
        try
        {
            int Id = 0;
            MediaClient mediaService = new MediaClient();

            if (Request.QueryString["catId"] != null && Request.QueryString["catId"] != "")
            {
                Id = Convert.ToInt32(Request.QueryString["catId"].ToString());
            }

            bool status = mediaService.DeleteMediaCategory(Id, UserSession.Instance.UserName);

            return "true";
        }
        catch (Exception)
        {
            return "false";
        }
    }

}