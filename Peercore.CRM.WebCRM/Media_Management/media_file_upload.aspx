﻿<%@ Page Title="mSlaes - Media Upload" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="media_file_upload.aspx.cs" Inherits="Media_Management_media_file_upload" %>

<%@ MasterType TypeName="SiteMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        .auto-style1 {
            left: 0px;
            top: 0px;
            width: 226px;
            height: 57px;
        }
    </style>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server" />

    <asp:HiddenField ID="OriginatorTypeString" runat="server" />
    <%-- <asp:HiddenField ID="saveDeleteOption" runat="server" />--%>

    <asp:HiddenField ID="fileId" runat="server" />
    <div id="MediamodalWindow" style="display: none">
        <div id="div_MediaConfirm_Message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>

    <div class="divcontectmainforms" id="maincont">

        <div id="window" style="display: none; width: 760px;">
            <%--//Media file display window--%>
        </div>
        <%--<div id="mediaplayer"></div>--%>
        
        <div id="divControlBar" class="toolbar_container">
            <div class="toolbar_right" id="div_content_2" style="width: 18% !important; display: none;">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx"> <div class="back"></div></asp:HyperLink>
                    </div>
                </div>
            </div>
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <asp:Button ID="buttonMediaUpload" runat="server" ToolTip="Upload" Text="Upload" OnClick="buttonMediaUpload_Click" CssClass="savebtn" />
                </div>
            </div>
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div id="div_info" runat="server" style="display: none;">
        </div>
        <div id="div_schemeSearchPanel">

            <div class="formtextdiv input-mini">

                <div style="margin-bottom: 200px">
                    <div id="repdiv" style="display: none;">
                        <span>Sales Rep</span>
                        <asp:DropDownList ID="ddlRepList" runat="server" ClientIDMode="Static" onchange="ddlRepList_SelectedIndexChanged()">
                        </asp:DropDownList>
                    </div>
                    <span>Media Category <span style="color: Red">*</span></span>
                    <asp:DropDownList ID="ddlCategory" runat="server" ClientIDMode="Static" onchange="ddlCatList_SelectedIndexChanged()">
                    </asp:DropDownList>
                    <div id="customerdiv" style="display: none;">
                        <span>Customer Code </span>
                        <span style="display: inline-block;">
                            <input type="text" class="textboxwidth" style="width: 225px;margin-bottom: 0px;" id="txtCustCode" onchange="txtCustCode_Changed(this)" />
                            <input type="text" id="txtCusName" maxlength="200" style="border: hidden;width: 300px;" readonly" /></span> <%----%>
                    </div>
                </div>
            </div>
            <div id="fileUploadDiv">
                <div class="formtextdiv" style="margin-left: 80px">
                    <span>Upload Media : </span>
                    <div class="formdetaildiv">
                        <asp:FileUpload ID="FileUploader" runat="server" class="upload" accept=".mp4,.avi,.flv,.png,.jpg,.jpeg,.gif,.bmp,.jfif" onchange="ValidateSingleInput(this)" /><%--onchange="loadFile(event)"--%>
                        <div class="imgbox" id="imgbox1" style="margin-bottom: 50px">
                            <img id="output" height="100px" width="100px" />
                            <video id="play" height="100px" width="100px">
                                <source id="videoFile" /></video>
                        </div>
                    </div>
                </div>

                <div class="formtextdiv">
                    <span>Description : </span>
                    <div class="formdetaildiv_right">
                        <asp:TextBox ID="txtDescription" runat="server" TextMode="MultiLine" Rows="2" CssClass="auto-style1" Style="width: 400px; height: 90px"></asp:TextBox>
                    </div>
                </div>
            </div>

            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <div class="clearall">
            </div>

            <br />
            <div id="div_promt" style="display: none">
            </div>
            <div class="grid_container">
                <div id="grid_media">
                </div>
            </div>

        </div>
        <br />
    </div>

    <div id="popup">
        <div>
            <b>Please enter admin password:</b>
        </div>
        <input style="width: 100%;" id="adminPass" type="password" />
        <button type="button" onclick="confirmAdmin()">
            Confirm</button>
        <button type="button" onclick="exitAdmin()">
            Cancel</button>
    </div>
    <script type="text/javascript">

        var loadFile = function (event) {
            var output = document.getElementById('output');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function () {
                URL.revokeObjectURL(output.src) // free memory
            }
        };

        var loadFileplay = function (event) {
            var output = document.getElementById('play');
            output.src = URL.createObjectURL(event.target.files[0]);
            output.onload = function () {
                URL.revokeObjectURL(output.src) // free memory
            }
        };

        function ValidateSingleInput(oInput) {
            var _validFileExtensions = [".jpg", ".jpeg", ".bmp", ".gif", ".png", ".jfif"];
            var _validVideoExtensions = [".mp4", ".avi", ".flv"];
            if (oInput.type == "file") {
                var sFileName = oInput.value;
                if (sFileName.length > 0) {
                    var blnValid = false;
                    for (var j = 0; j < _validFileExtensions.length; j++) {
                        var sCurExtension = _validFileExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            document.getElementById("output").style.display = "block";
                            document.getElementById("output").style.visibility = "visible";
                            document.getElementById("play").style.display = "none";
                            console.log("jpg");
                            loadFile(event);
                            break;
                        }
                    }

                    for (var j = 0; j < _validVideoExtensions.length; j++) {
                        var sCurExtension = _validVideoExtensions[j];
                        if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                            blnValid = true;
                            document.getElementById("play").style.display = "block";
                            document.getElementById("play").style.visibility = "visible";
                            document.getElementById("output").style.display = "none";
                            console.log("mp4");
                            loadFileplay(event);
                            break;
                        }
                    }

                    if (!blnValid) {
                        alert("Sorry, " + sFileName + " is invalid, allowed extensions are: " + _validFileExtensions.join(", ") + _validVideoExtensions.join(", "));
                        oInput.value = "";
                        return false;
                    }
                }
            }
            return true;
        }

        var take_grid = $("#MainContent_hfPageIndex").val();

        $(document).ready(function () {
            var rep = document.getElementById('ddlRepList').value;
            var category = document.getElementById('ddlCategory').value;
            console.log(rep, " ", category);
            document.getElementById("output").style.display = "none";
            document.getElementById("play").style.display = "none";

            loadMedia(rep, category);

            ddlCatList_SelectedIndexChanged();
        });

        function ViewMediaFile(id, name, imgPath, ext) {

            $("#hdnSelectedId").val(id);

            if (ext == ".mp4") {
                console.log(imgPath + " ++mp4" + name);
                $("#window").css("display", "block");
                var wnd = $("#window").kendoWindow({
                    title: name,
                    modal: true,
                    visible: false,
                    actions: ["Close"], //actions: ["Pin", "Refresh", "Maximize", "Close"],
                    resizable: false
                }).data("kendoWindow");
                wnd.center().open();
                wnd.content("<video width=\"750\" height=\"300\" controls><source src= \"" + imgPath + "\" type=\"video/mp4\" /></video>");

            }
            else {
                console.log(imgPath + " ++mp4 " + name);
                $("#window").css("display", "block");
                var wnd = $("#window").kendoWindow({
                    title: name,
                    modal: true,
                    visible: false,
                    actions: ["Close"], //actions: ["Pin", "Refresh", "Maximize", "Close"],
                    resizable: false
                }).data("kendoWindow");
                wnd.center().open();
                wnd.content("<img src= \"" + imgPath + "\" />");
            }

        }

        function loadMedia(repCode, category, custCode = '') {

            var url = "javascript:ViewMediaFile('#=MediaFileId#','#=MediaFileName#','#=MediaFilePath#','#=FileExtention#');";

            var urlmediaId = "<a href=\"" + url + "\">#=MediaFileId#</a>";
            var urlmediaName = "<a href=\"" + url + "\">#=MediaFileName#</a>";
            var urlPath = "<a href=\"" + url + "\">#=MediaFilePath#</a>";

            var OriginatorTypeString = $("#<%= OriginatorTypeString.ClientID %>").val();

            $("#grid_media").html("");
            $("#grid_media").kendoGrid({
                columns: [
                    {
                        template: '<a class="sprite_button" href="javascript:deleteMedia(\'#=MediaFileId#\', \'#=MediaFileName#\');">Delete</a>',
                        field: "",
                        width: "60px"
                    },
                    {
                        field: "",
                        title: "",
                        template: "<img style=\"width: 80px; margin-right: 20px;\" src=\"#=ThumbnailPath#\" id=\"#=MediaFileId#\" />",
                        width: "30px",
                        filterable: false,
                        sortable: false
                    },
                    {
                        field: "MediaFileName",
                        title: "Media File Name",
                        template: urlmediaName, //"<div class='product-photo'></div><div class='product-name'>#: MediaFileName #</div>",
                        width: "100px"
                    },
                    { field: "MediaCategory", width: "100px", title: "Media Category" },
                    { field: "MediaFileId", width: "10px", title: "MediaFileId", hidden: true },
                    { field: "Description", title: "Description", width: 210, filterable: false }
                    //{ field: "CreatedDate", width: "10px", title: "CreatedDate", hidden: true }
                ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                filterable: true,
                navigatable: true,
                dataBound: onDataBound,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 15,
                    batch: true,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            //id: "SourceId",
                            fields: {
                                MediaFileId: { type: "number" },
                                RepCode: { type: "string" },
                                MediaFileName: { type: "string" },
                                ThumbnailPath: { type: "string" },
                                Description: { type: "string" },
                                //CreatedDate: { type: "date", format: "{0:MM/dd/yyyy}" }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/Media/media.asmx/GetAllMedia", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { pgindex: take_grid, repCode: repCode, category: category, custCode: custCode },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {

                                    var entityGrid = $("#grid_media").data("kendoGrid");
                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));
                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }
            });
        }

        function deleteMedia(mediaId, mediaName) {
            $("#MediamodalWindow").show()
            showdelete_media_confirmation(mediaId, mediaName);
        }

        function showdelete_media_confirmation(mediaId, mediaName) {
            $("#save").css("display", "none");
            $("#no").css("display", "inline-block");
            $("#yes").css("display", "inline-block");

            var message = "Do you want to delete '" + mediaName + "'?";

            var wnd = $("#MediamodalWindow").kendoWindow({
                title: "Delete Media",
                modal: true,
                visible: false,
                resizable: false,
                width: 400
            }).data("kendoWindow");

            $("#div_MediaConfirm_Message").text(message);
            wnd.center().open();

            $("#yes").click(function () {

                var OriginatorString = $("#<%= OriginatorString.ClientID %>").val();

                delete_selected_media('Media_Management/media_file_upload.aspx?fm=deleteMedia&type=delete&mediaid=' + mediaId);
                wnd.close();
            });

            $("#no").click(function () {
                wnd.close();
            });
        }

        function delete_selected_media(targeturl) {
            $("#div_loader").show();
            var url = ROOT_PATH + targeturl;

            var repCode = $("#ddlRepList").val();
            var category = $("#ddlCategory").val();

            $.ajax({
                url: url,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg == "true") {
                        var sucessMsg = GetSuccesfullMessageDiv("Media file was successfully Deleted.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        loadMedia(repCode, category);
                        $("#div_loader").hide();
                        hideStatusDiv("MainContent_div_message");
                    }
                    else {
                        var existsMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message_popup");
                        $('#MainContent_div_message_popup').css('display', 'block');
                        $("#MainContent_div_message_popup").html(existsMsg);
                        hideStatusDiv("MainContent_div_message_popup");
                        $("#div_loader").hide();
                    }
                },
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                        $("#div_loader").hide();
                    }
                }
            });
        }

        function showPopup() {
            $("#adminPass").val("");
            document.getElementById("popup").style.display = "block";
        }

        function ddlRepList_SelectedIndexChanged() {
            var rep = document.getElementById('ddlRepList').value;
            var category = document.getElementById('ddlCategory').value;
            console.log(rep, " ", category);
            loadMedia(rep, category);
        }

        function ddlCatList_SelectedIndexChanged() {
            var rep = document.getElementById('ddlRepList').value;
            var category = document.getElementById('ddlCategory').value;

            var eleDiv = document.getElementById("customerdiv");
            var uploadDiv = document.getElementById("div_content");

            var OriginatorTypeString = $("#<%= OriginatorTypeString.ClientID %>").val();

            if (OriginatorTypeString == 'ASE') {
                console.log(rep, " ", category);
                eleDiv.style.display = "block";
                uploadDiv.style.display = "none";
                fileUploadDiv.style.display = "none";
                divControlBar.style.display = "none";
                customerdiv.style.display = "none";

                if (category == 'MTIMG') {
                    customerdiv.style.display = "block";
                }
            }
            else {
                if (category == 'MTIMG') {
                    console.log(rep, " ", category);
                    eleDiv.style.display = "block";
                    uploadDiv.style.display = "none";
                    fileUploadDiv.style.display = "none";
                    divControlBar.style.display = "none";
                    customerdiv.style.display = "block";
                }
                else {
                    console.log(category);
                    eleDiv.style.display = "none";
                    uploadDiv.style.display = "block";
                    fileUploadDiv.style.display = "block";
                    divControlBar.style.display = "block";
                }
            }
            $("#txtCusName").val("");
            loadMedia(rep, category);
        }

        function txtCustCode_Changed(txtCustCode) {
            var rep = document.getElementById('ddlRepList').value;
            var category = document.getElementById('ddlCategory').value;
            var custCode = document.getElementById('txtCustCode').value;
            console.log(rep, " ", category, " ", custCode);
            loadMedia(rep, category, custCode);
            GetCustomerName();
        }

        function GetCustomerName() {
            var custCode = document.getElementById('txtCustCode').value;
            //x.value = x.value.toUpperCase();

            var params = "{'custCode':'" + custCode + "'}";

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/Media/media.asmx/LoadCustomerName",
                data: params,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    if (result.d == null) {
                        var sucessMsg = GetErrorMessageDiv("Customer code does not exist.", "MainContent_div_message_popup");
                        $('#MainContent_div_message_popup').css('display', 'block');
                        $("#MainContent_div_message_popup").html(sucessMsg);
                        hideStatusDiv("MainContent_div_message_popup");
                        $("#txtCusCode").val("");
                        $("#txtCusName").val("");
                        return;
                    } else {
                        $("#txtCusName").val(result.d);
                    }

                },
                error: function () {
                    //alert("An error has occurred during processing customer code.");
                    var existsMsg = GetErrorMessageDiv("An error has occurred during processing your request.", "MainContent_div_message_popup");
                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(existsMsg);
                    hideStatusDiv("MainContent_div_message_popup");
                    return;
                }
            });
        }

    </script>

</asp:Content>



