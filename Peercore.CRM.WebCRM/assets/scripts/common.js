﻿//var ROOT_PATH = '/Peercore.CRM.WebCRM/'
var ROOT_PATH = '/'
var DATE_FORMAT = "{0: yyyy-MM-dd}"
var DATE_FORMAT3 = "{0: dd-MM-yyyy}"
var DATE_FORMAT2 = "{0: dd/MM/yyyy}"
var DATETIME_FORMAT = "dd/MMM/yyyy hh:mm tt"
var DATETIME_FORMAT2 = "{0: dd/MMM/yyyy HH:mm}"
var NUMBER_FORMAT = "{0:n0}"
var NUMBER_FORMAT_DECIAML = "{0:n2}"
var NUMBER_FORMAT_THREE_DECIAML = "{0:n3}"

function loadHomePageTabs() {
    var original = $("#tabstrip").clone(true);
    original.find(".k-state-active").removeClass("k-state-active");

    var getEffects = function () {
        return (("expand:vertical ") + ("fadeIn"));
    };

    var initTabStrip = function () {
        /*$("#tabstrip").kendoTabStrip({
        animation: { open: {
        effects: getEffects()
        }
        },
        contentUrls: [, "../../content/web/tabstrip/ajax/ajaxContent1.html"]
        });*/

        $("#tabstrip").kendoTabStrip({
            contentUrls: [, ROOT_PATH + "process_forms/loadcrmdetails.aspx"]
        });
        /*var calculatedHeight = $("#tabstrip").outerHeight() - $("ul_tabstrip").outerHeight();
        $("#tabstrip").children(".t-content").height(calculatedHeight);*/
    };

    initTabStrip();
}

function encodeurl(textvalue) {
    var _encodedUrl = encodeURIComponent(textvalue);

    return _encodedUrl;
    //outputs folder%2Findex.html%3Fparam%3D%2323dd%26noob%3Dyes
}

function deodeurl(decodetextvalue) {
    var _decodedUrl = decodeURIComponent(decodetextvalue);
    return _decodedUrl;
    //outputs  folder/index.html?param=#23dd&amp;noob=yes
}

/*Global Variables*/
var hasCompletedSalesDashboard = '0';
var hasCompletedOpportunity = '0';
var hasCompletedDeals = '0';

function loadSalesDetails(businessCode, yearmoneth,repcode) {
    loadsalesdetails(businessCode, yearmoneth, repcode);
}

function loadsalesdetails(sales_businessCode, sales_yearmoneth, sales_repcode) {
    $("#div_salesleaderboard").html("");

    $("#div_salesleaderboard").kendoGrid({
        columns: [
//                    { field: "OwnerName", title: "Acc.Manager", width: "40px" },
//                    { field: "Amount", title: "Amount($)", width: "25px", attributes:{style:"text-align:right;"} ,format:NUMBER_FORMAT_DECIAML },
//                    { field: "Units", title: "Units", width: "20px", attributes:{style:"text-align:right;"} , format:NUMBER_FORMAT }
                    {field: "OwnerName", title: " ", width: "40px" },
                    { field: "Data", title: " ", width: "25px", attributes: { style: "text-align:left;" } }
                 ],
        sortable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total"
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetSalesLeaderBoard", //specify the URL which data should return the records. This is the Read method of the Products.svc service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { businesscode: sales_businessCode, period: sales_yearmoneth },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for svc
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            loadOpportunitydetails(sales_businessCode, sales_yearmoneth,sales_repcode);
                            hasCompletedSalesDashboard = "1";
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

function loadOpportunitydetails(opp_businessCode, opp_yearmoneth, opp_repcode) {
    $("#div_opportunity").html("");
    $("#div_opportunity").kendoGrid({
        columns: [
                    { field: "OwnerName", title: "Outlet", width: "35px" },
                    { field: "Originator", title: "Town", width: "35px" }
//                    { field: "Amount", title: "Amount($)", width: "25px", attributes:{style:"text-align:right;"}, format:NUMBER_FORMAT_DECIAML},
//                    { field: "Units", title: "Units", width: "10px", attributes:{style:"text-align:right;"}, format:NUMBER_FORMAT },
//                    { field: "Count", title: "Open", width: "10px" , attributes:{style:"text-align:right;"}, format:NUMBER_FORMAT},
//                    { field: "Ratio", title: "Ratio(%)", width: "15px" , attributes:{style:"text-align:right;"} }
                ],
        sortable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total"
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetOpportunity", //specify the URL which data should return the records. This is the Read method of the Products.svc service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { businesscode: opp_businessCode, period: opp_yearmoneth },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for svc
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                          //  loadDealsdetails(opp_businessCode, opp_yearmoneth, opp_repcode);
                            hasCompletedOpportunity = "1";
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

function loadDealsdetails(dea_businessCode,dea_yermnt,dea_repcode) {
    $("#div_deals").html("");
    $("#div_deals").kendoGrid({
        columns: [
                    { field: "OpportunityName", title: "Opp.Name", width: "25px" },
                    { field: "Rep", title: "Rep", width: "11px" },
                    { field: "Customer", title: "Customer", width: "30px" },
                    { field: "Amount", title: "Amount($)", width: "14px" , attributes:{style:"text-align:right;"},format:NUMBER_FORMAT_DECIAML}
                ],
        sortable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total"
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetDeals", //specify the URL which data should return the records. This is the Read method of the Products.svc service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { businesscode: dea_businessCode },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for svc
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            //loadCustomersdetails(dea_businessCode, dea_yermnt, dea_repcode);
                            hasCompletedDeals = "1";
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

var gridindex = 0;
function CheckLeadCustomeHeader(control) {
    $("#div_loader").show();
    var entityGrid = $("#grid").data("kendoGrid").dataSource._data;
    var isChecked = 0;
    if ($(control).attr("checked") == "checked") {
        isChecked = 1;
    }

    var leadstring = "";
    var custstring = "";
    for (var i = 0; i < entityGrid.length; i++) {
        var CustomerCode = entityGrid[i].CustomerCode;
        var LeadStage = entityGrid[i].LeadStage;
        var SourceId = entityGrid[i].SourceId;
        if (LeadStage == "Customer") {
            if (custstring == "") {
                custstring = CustomerCode;
            }
            else {
                custstring = custstring + "," + CustomerCode;
            }
        }
        else {
            if (leadstring == "") {
                leadstring = SourceId;
            }
            else {
                leadstring = leadstring + "," + SourceId;
            }
        }
    }

    var root = ROOT_PATH + "call_cycle/process_forms/processmaster.aspx?fm=checkLeadCustHeader&type=query&leadstring=" + leadstring +
                        "&custstring=" +  encodeurl(custstring) + "&isChecked=" + isChecked;

    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg == "true") {
                $("#MainContent_HiddenFieldCallCycleCount").val("1");
            }
            else {
                $("#MainContent_HiddenFieldCallCycleCount").val("0");
            }
            refreshLeadCustomerGrid();
            setTimeout(function () {
                if (isChecked == 1) {
                    $('.myCheckbox').prop('checked', true);
                }
                $("#div_loader").hide();
            }, 400);
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
}

function loadLeadCustomerGrid(activeInactiveChecked, reqSentIsChecked, repType) {
    var take_grid=$("#MainContent_hfPageIndex").val();
    $("#grid").html("");
    $("#grid").kendoGrid({
       // height: 400,
        columns: [
//            { title: "", headerTemplate: '<input type="checkbox" class="myCheckbox" onchange="CheckLeadCustomeHeader(this)" />', width: "30px", template: '<input type="checkbox" onchange="CheckLeadCustomer(\'#=LeadStage#\',\'#=CustomerCode#\',#=SourceId#,this)" #=SelectedToCall#/>' },
//                    //{ title: "", width: "30px", template: '<input type="checkbox" onchange="CheckLeadCustomer(\'#=LeadStage#\',\'#=CustomerCode#\',#=SourceId#,this)" #=SelectedToCall#/>' },
            { text: "HasSelect", title: "", headerTemplate: '<input id="checkAllcustomers" type="checkbox" class="myCheckbox" />', width: "40px", template: "<input id=\"check_datarow_customers\" alt=\"#= uid# \" type=\"checkbox\" #= HasSelect ? checked='checked' : '' # class=\"check_row\"/>" },
            { template: '<a href="javascript:DeleteCustomer(\'#=CustomerCode#\',\'#=SourceId#\');">Delete</a>', field: "", width: "60px" },
            { template: '<a href="javascript:OpenLeadCustomer(\'#=LeadStage#\',\'#=CustomerCode#\',\'#=SourceId#\')">#=Name#</a>', field: "Name", width: "220px" , footerTemplate: "<div id=\"div_rowCount_sum\" ></div>" },
            { field: "LeadStage", title: "Lead Stage", width: "85px", hidden: true },
            { field: "CustomerCode", title: "Cust Code", width: "95px" },
            { field: "State", title: "State", width: "60px", hidden: true },
            { field: "City", title: "City", width: "130px", hidden: true },
            { field: "SourceId", title: "CRM Ref. No.", width: "98px", hidden: true },
            { field: "Address", title: "Address", width: "250px", template: '<div style="height: 20px; ">#=Address#</div>' },
            { field: "DistributorCode", title: "Dis Code", width: "85px" },
            { field: "DistributorName", title: "Dis Name", width: "130px" },
            { 
                field: "LastInvoiceDate", 
                title: "Last Invoice date", 
                width: "115px"
            },
            { field: "rowCount", title: "rowCount", format: "{0:n2}", hidden: true, footerTemplate: "<div id=\"div_rowCount_max\" > #: max # </div>" }
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: true,
        dataBound: LeadCustomerGrid_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "SourceId",
                    fields: {
                        Name: { validation: { required: true} },
                        LeadStage: { validation: { required: true} },
                        CustomerCode: { editable: true, nullable: true },
                        State: { validation: { required: true} },
                        City: { validation: { required: true} },
                        SourceId: { validation: { required: true} },
                        Address: { validation: { required: true} },
                        DistributorCode: { validation: { required: true} },
                        DistributorName: { validation: { required: true} },
                        rowCount: { type: "number", validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH+ "service/lead_customer/lead_contacts_Service.asmx/GetDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' ,pgindex:take_grid},
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            var entityGrid = $("#grid").data("kendoGrid");
                            if((take_grid!='') && (gridindex =='0')){
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex ='1';
                            }
                            }
                        }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            aggregate: [ { field: "rowCount", aggregate: "max" } ]
        }
    });
}

function OutletsPage_loadLeadCustomerGrid(activeInactiveChecked, reqSentIsChecked, repType, OriginatorString) {
    var take_grid=$("#MainContent_hfPageIndex").val();
    $("#grid").html("");

    if (OriginatorString != "ADMIN") {
        $("#grid").kendoGrid({
           // height: 400,
            columns: [
    //            { title: "", headerTemplate: '<input type="checkbox" class="myCheckbox" onchange="CheckLeadCustomeHeader(this)" />', width: "30px", template: '<input type="checkbox" onchange="CheckLeadCustomer(\'#=LeadStage#\',\'#=CustomerCode#\',#=SourceId#,this)" #=SelectedToCall#/>' },
    //                    //{ title: "", width: "30px", template: '<input type="checkbox" onchange="CheckLeadCustomer(\'#=LeadStage#\',\'#=CustomerCode#\',#=SourceId#,this)" #=SelectedToCall#/>' },
                { text: "HasSelect", title: "", headerTemplate: '<input id="checkAllcustomers" type="checkbox" class="myCheckbox" />', width: "40px", template: "<input id=\"check_datarow_customers\" alt=\"#= uid# \" type=\"checkbox\" #= HasSelect ? checked='checked' : '' # class=\"check_row\"/>" },
                { template: '<a href="javascript:DeleteCustomer(\'#=CustomerCode#\',\'#=SourceId#\');">Delete</a>', field: "", width: "60px", hidden: true },
                { template: '<a href="javascript:OpenLeadCustomer(\'#=LeadStage#\',\'#=CustomerCode#\',\'#=SourceId#\')">#=Name#</a>', field: "Name", width: "220px" , footerTemplate: "<div id=\"div_rowCount_sum\" ></div>" },
                { field: "LeadStage", title: "Lead Stage", width: "85px", hidden: true },
                { field: "CustomerCode", title: "Cust Code", width: "95px" },
                { field: "State", title: "State", width: "60px", hidden: true },
                { field: "City", title: "City", width: "130px", hidden: true },
                { field: "SourceId", title: "CRM Ref. No.", width: "98px", hidden: true },
                { field: "Address", title: "Address", width: "250px", template: '<div style="height: 20px; ">#=Address#</div>' },
                { field: "DistributorCode", title: "Dis Code", width: "85px" },
                { field: "DistributorName", title: "Dis Name", width: "130px" },
                { 
                    field: "LastInvoiceDate", 
                    title: "Last Invoice date", 
                    width: "115px"
                },
                { field: "rowCount", title: "rowCount", format: "{0:n2}", hidden: true, footerTemplate: "<div id=\"div_rowCount_max\" > #: max # </div>" }
            ],
            editable: false, // enable editing
            pageable: true,
            sortable: true,
            filterable: true,
            selectable: "single",
            columnMenu: true,
            dataBound: LeadCustomerGrid_onDataBound,
            dataSource: {
                serverSorting: true,
                serverPaging: true,
                serverFiltering: true,
                pageSize: 20,
                schema: {
                    data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                    total: "d.Total",
                    model: { // define the model of the data source. Required for validation and property types.
                        id: "SourceId",
                        fields: {
                            Name: { validation: { required: true} },
                            LeadStage: { validation: { required: true} },
                            CustomerCode: { editable: true, nullable: true },
                            State: { validation: { required: true} },
                            City: { validation: { required: true} },
                            SourceId: { validation: { required: true} },
                            Address: { validation: { required: true} },
                            DistributorCode: { validation: { required: true} },
                            DistributorName: { validation: { required: true} },
                            rowCount: { type: "number", validation: { required: true} }
                        }
                    }
                },
                transport: {

                    read: {
                        url: ROOT_PATH+ "service/lead_customer/lead_contacts_Service.asmx/GetDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                        contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                        data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' ,pgindex:take_grid},
                        type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                        complete: function (jqXHR, textStatus) {
                            if (textStatus == "success") {
                                var entityGrid = $("#grid").data("kendoGrid");
                                if((take_grid!='') && (gridindex =='0')){
                                    entityGrid.dataSource.page((take_grid - 1));
                                    gridindex ='1';
                                }
                                }
                            }
                    },
                    parameterMap: function (data, operation) {
                        if (operation != "read") {
                            return JSON.stringify({ products: data.models })
                        } else {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                },
                aggregate: [ { field: "rowCount", aggregate: "max" } ]
            }
        });
    }
    else
    {
        $("#grid").kendoGrid({
           // height: 400,
            columns: [
    //            { title: "", headerTemplate: '<input type="checkbox" class="myCheckbox" onchange="CheckLeadCustomeHeader(this)" />', width: "30px", template: '<input type="checkbox" onchange="CheckLeadCustomer(\'#=LeadStage#\',\'#=CustomerCode#\',#=SourceId#,this)" #=SelectedToCall#/>' },
    //                    //{ title: "", width: "30px", template: '<input type="checkbox" onchange="CheckLeadCustomer(\'#=LeadStage#\',\'#=CustomerCode#\',#=SourceId#,this)" #=SelectedToCall#/>' },
                { text: "HasSelect", title: "", headerTemplate: '<input id="checkAllcustomers" type="checkbox" class="myCheckbox" />', width: "40px", template: "<input id=\"check_datarow_customers\" alt=\"#= uid# \" type=\"checkbox\" #= HasSelect ? checked='checked' : '' #  class=\"check_row\"/>" },
                { template: '<a href="javascript:DeleteCustomer(\'#=CustomerCode#\',\'#=SourceId#\');">Delete</a>', field: "", width: "60px" },
                { template: '<a href="javascript:OpenLeadCustomer(\'#=LeadStage#\',\'#=CustomerCode#\',\'#=SourceId#\')">#=Name#</a>', field: "Name", width: "220px" , footerTemplate: "<div id=\"div_rowCount_sum\" ></div>" },
                { field: "LeadStage", title: "Lead Stage", width: "85px", hidden: true },
                { field: "CustomerCode", title: "Cust Code", width: "95px" },
                { field: "State", title: "State", width: "60px", hidden: true },
                { field: "City", title: "City", width: "130px", hidden: true },
                { field: "SourceId", title: "CRM Ref. No.", width: "98px", hidden: true },
                { field: "Address", title: "Address", width: "250px", template: '<div style="height: 20px; ">#=Address#</div>' },
                { field: "DistributorCode", title: "Dis Code", width: "85px" },
                { field: "DistributorName", title: "Dis Name", width: "130px" },
                { 
                    field: "LastInvoiceDate", 
                    title: "Last Invoice date", 
                    width: "115px"
                },
                { field: "rowCount", title: "rowCount", format: "{0:n2}", hidden: true, footerTemplate: "<div id=\"div_rowCount_max\" > #: max # </div>" }
            ],
            editable: false, // enable editing
            pageable: true,
            sortable: true,
            filterable: true,
            selectable: "single",
            columnMenu: true,
            //dataBound: LeadCustomerGrid_onDataBound,
            dataSource: {
                serverSorting: true,
                serverPaging: true,
                serverFiltering: true,
                pageSize: 20,
                schema: {
                    data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                    total: "d.Total",
                    model: { // define the model of the data source. Required for validation and property types.
                        id: "SourceId",
                        fields: {
                            Name: { validation: { required: true} },
                            LeadStage: { validation: { required: true} },
                            CustomerCode: { editable: true, nullable: true },
                            State: { validation: { required: true} },
                            City: { validation: { required: true} },
                            SourceId: { validation: { required: true} },
                            Address: { validation: { required: true} },
                            DistributorCode: { validation: { required: true} },
                            DistributorName: { validation: { required: true} },
                            rowCount: { type: "number", validation: { required: true} }
                        }
                    }
                },
                transport: {

                    read: {
                        url: ROOT_PATH+ "service/lead_customer/lead_contacts_Service.asmx/GetDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                        contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                        data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' ,pgindex:take_grid},
                        type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                        complete: function (jqXHR, textStatus) {
                            if (textStatus == "success") {
                                var entityGrid = $("#grid").data("kendoGrid");
                                if((take_grid!='') && (gridindex =='0')){
                                    entityGrid.dataSource.page((take_grid - 1));
                                    gridindex ='1';
                                }
                                }
                            }
                    },
                    parameterMap: function (data, operation) {
                        if (operation != "read") {
                            return JSON.stringify({ products: data.models })
                        } else {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                },
                aggregate: [ { field: "rowCount", aggregate: "max" } ]
            }
        });
    }
}

function CustomersforConfirm(IsReject, OriginatorString, AsmVal, TerritoryVal) {
    var take_grid = $("#MainContent_hfPageIndex").val();

    $("#grid").html("");

    $("#grid").kendoGrid({
        // height: 400,
        columns: [
            //{ text: "HasSelect", title: "", headerTemplate: '<input id="checkAllcustomers" type="checkbox" class="myCheckbox" />', width: "40px", template: "<input id=\"check_datarow_customers\" alt=\"#= uid# \" type=\"checkbox\" #= HasSelect ? checked='checked' : '' #  class=\"check_row\"/>" },
            {
                template: '<a href="javascript:AcceptCustomer(\'admin_user\', \'#=OutletCode#\');" class="sprite_button" style="padding: 6px; height: auto; line-height: 12px; float: left; font-size: 12px; border: none; box-shadow: none !important; background: rgb(8,194,62) !important""> Accept</a><a href="javascript:RejectCustomer(\'admin_user\', \'#=OutletCode#\');" class= "sprite_button" style="padding: 6px;height: auto;line-height: 12px; float: left; font-size: 12px; border: none; box-shadow: none !important; background: rgb(228,10,26) !important"> Reject</a> ', field: "", width: "150px"
            },
            //{
            //    template: '<a href="javascript:OpenLeadCustomer(\'#=LeadStage#\',\'#=OutletCode#\',\'#=SourceId#\')">#=OutletName#</a>', field: "Name", width: "220px", footerTemplate: "<div id=\"div_rowCount_sum\" ></div>"
            //},
            { field: "OutletName", title: "Name", width: "220px" },
            //{ field: "LeadStage", title: "Lead Stage", width: "85px", hidden: true },
            { field: "OutletCode", title: "Code", width: "95px" },
            { field: "OutletTypeName", title: "Outlet Type", width: "120px" },
            //{ field: "State", title: "State", width: "60px", hidden: true },
            //{ field: "City", title: "City", width: "130px", hidden: true },
            //{ field: "SourceId", title: "CRM Ref. No.", width: "98px", hidden: true },
            { field: "Address", title: "Address", width: "250px", template: '<div style="height: 20px; ">#=Address#</div>' },
            { field: "RouteName", title: "Route", width: "200px", template: '<div style="height: 20px; ">#=RouteName#</div>' },
            { field: "TerritoryName", title: "Territory", width: "150px", template: '<div style="height: 20px; ">#=TerritoryName#</div>' },
            { field: "Telephone", title: "Telephone", width: "130px", sortable: false, filterable: false },
            //{ field: "DistributorName", title: "Dis Name", width: "130px" },
            //{
            //    field: "LastInvoiceDate",
            //    title: "Last Invoice date",
            //    width: "115px"
            //},
            { field: "rowCount", title: "rowCount", format: "{0:n2}", hidden: true, footerTemplate: "<div id=\"div_rowCount_max\" > #: max # </div>" }
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        //dataBound: LeadCustomerGrid_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "OutletCode",
                    fields: {
                        OutletName: { validation: { required: true } },
                        LeadStage: { validation: { required: true } },
                        OutletCode: { editable: true, nullable: true },
                        OutletTypeName: { editable: true, nullable: true },
                        State: { validation: { required: true } },
                        City: { validation: { required: true } },
                        SourceId: { validation: { required: true } },
                        Address: { validation: { required: true } },
                        Telephone: { validation: { required: true } },
                        DisCode: { validation: { required: true } },
                        DisName: { validation: { required: true } },
                        RouteName: { validation: { required: true } },
                        TerritoryName: { validation: { required: true } },
                        AreaName: { validation: { required: true } },
                        Asm: { validation: { required: true } },
                        rowCount: { type: "number", validation: { required: true } }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/lead_customer/customer_entry_Service.asmx/GetAllPendingCustomers", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { IsReject: IsReject, Originator: OriginatorString, Asm: AsmVal, Territory: TerritoryVal, pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            var entityGrid = $("#grid").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            aggregate: [{ field: "rowCount", aggregate: "max" }]
        }
    });
}

function CustomersforConfirmBySales(IsReject, OriginatorString, AsmVal, TerritoryVal) {
    var take_grid = $("#MainContent_hfPageIndex").val();

    $("#grid").html("");

    $("#grid").kendoGrid({
        // height: 400,
        columns: [
            //{ text: "HasSelect", title: "", headerTemplate: '<input id="checkAllcustomers" type="checkbox" class="myCheckbox" />', width: "40px", template: "<input id=\"check_datarow_customers\" alt=\"#= uid# \" type=\"checkbox\" #= HasSelect ? checked='checked' : '' #  class=\"check_row\"/>" },
            {
                template: '<a href="javascript:AcceptSalesCustomer(\'admin_user\', \'#=OutletCode#\');" class="sprite_button" style="padding: 6px; height: auto; line-height: 12px; float: left; font-size: 12px; border: none; box-shadow: none !important; background: rgb(8,194,62) !important""> Accept</a><a href="javascript:RejectCustomer(\'admin_user\', \'#=OutletCode#\');" class= "sprite_button" style="padding: 6px;height: auto;line-height: 12px; float: left; font-size: 12px; border: none; box-shadow: none !important; background: rgb(228,10,26) !important"> Reject</a> ', field: "", width: "150px"
            },
            {
                template: '<a href="javascript:OpenLeadCustomer(\'#=LeadStage#\',\'#=OutletCode#\',\'#=SourceId#\')">#=OutletName#</a>', field: "Name", width: "220px", footerTemplate: "<div id=\"div_rowCount_sum\" ></div>"
            },
            //{ field: "LeadStage", title: "Lead Stage", width: "85px", hidden: true },
            { field: "OutletCode", title: "Code", width: "95px" },
            { field: "OutletTypeName", title: "Outlet Type Name", width: "120px" },
            //{ field: "State", title: "State", width: "60px", hidden: true },
            //{ field: "City", title: "City", width: "130px", hidden: true },
            //{ field: "SourceId", title: "CRM Ref. No.", width: "98px", hidden: true },
            { field: "Address", title: "Address", width: "250px", template: '<div style="height: 20px; ">#=Address#</div>' },
            { field: "RouteName", title: "Route", width: "200px", template: '<div style="height: 20px; ">#=RouteName#</div>' },
            { field: "TerritoryName", title: "Territory", width: "150px", template: '<div style="height: 20px; ">#=TerritoryName#</div>' },
            {
                field: "Telephone", title: "Telephone", width: "130px", sortable: false,
                filterable: false
            },
            //{ field: "DistributorName", title: "Dis Name", width: "130px" },
            //{
            //    field: "LastInvoiceDate",
            //    title: "Last Invoice date",
            //    width: "115px"
            //},
            { field: "rowCount", title: "rowCount", format: "{0:n2}", hidden: true, footerTemplate: "<div id=\"div_rowCount_max\" > #: max # </div>" }
        ],
        editable: false,
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        //dataBound: LeadCustomerGrid_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "SourceId",
                    fields: {
                        OutletName: { validation: { required: true } },
                        LeadStage: { validation: { required: true } },
                        OutletCode: { editable: true, nullable: true },
                        State: { validation: { required: true } },
                        City: { validation: { required: true } },
                        SourceId: { validation: { required: true } },
                        Address: { validation: { required: true } },
                        Telephone: { validation: { required: true } },
                        DisCode: { validation: { required: true } },
                        DisName: { validation: { required: true } },
                        RouteName: { validation: { required: true } },
                        TerritoryName: { validation: { required: true } },
                        AreaName: { validation: { required: true } },
                        Asm: { validation: { required: true } },
                        OutletTypeName: { validation: { required: true } },
                        rowCount: { type: "number", validation: { required: true } }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/lead_customer/customer_entry_Service.asmx/GetAllPendingCustomersForSalesConfirm", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { IsReject: IsReject, Originator: OriginatorString, Asm: AsmVal, Territory: TerritoryVal, pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            var entityGrid = $("#grid").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            aggregate: [{ field: "rowCount", aggregate: "max" }]
        }
    });
}


$('#check_datarow_customers').live('click', function (e) {
    var cb = $(this);
    var checked = cb.is(':checked');
    var selected_uid = cb[0].alt.replace(/\s+/g, '');

    setValueToGridDataCustomers(checked, selected_uid);

    if ($('[id=checkAllcustomers]').is(':checked')) {
        $('[id=checkAllcustomers]')[0].checked = false;
    }
});

function setValueToGridDataCustomers(val, rowuid) {
    var grid = $("#grid").data("kendoGrid");
    var item_grid = grid.dataSource.getByUid(rowuid);
    if (item_grid != undefined && item_grid != null)
        item_grid.set("HasSelect", val);
}

$("#checkAllcustomers").live('click', function (e) {
    var state = $(this).is(':checked');
    var grid = $('#grid').data('kendoGrid');
    $.each(grid.dataSource.view(), function () {
        if (this['HasSelect'] != state)
            this.dirty = true;
        this['HasSelect'] = state;
    });
    grid.refresh();
});

function deletecustomers() {
    var callCycleContactDTOList = GetSelectedCustomerList();
    var param = { 'DeleteCustomerListDTO': callCycleContactDTOList };

    $.ajax({
        type: "POST",
        url: ROOT_PATH + "service/lead_customer/lead_contacts_Service.asmx/DeleteSelectedCustomerListformGrid",
        //data: JSON.stringify(param),
        data: JSON.stringify(param),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.hasOwnProperty('d')) {
                msg = response.d;
            } else {
                msg = response;
            }
            if (msg == "Deleted") {
                var sucessMsg = GetSuccesfullMessageDiv("Successfully Deleted.", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(sucessMsg);
                $("div#div_loader").hide();
                loadLeadCustomerGrid($("#MainContent_HiddenFieldIsChecked").val(), $("#MainContent_HiddenFieldReqSentIsChecked").val(), $("#cmbRepType option:selected").val());
                hideStatusDiv("MainContent_div_message");
            }
            else {
                var errorMsg = GetErrorMessageDiv("Please select item to delete", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);

                $("div#div_loader").hide();
                loadLeadCustomerGrid($("#MainContent_HiddenFieldIsChecked").val(), $("#MainContent_HiddenFieldReqSentIsChecked").val(), $("#cmbRepType option:selected").val());
                hideStatusDiv("MainContent_div_message");
            }
        },
        error: function (response) {
            //alert("Oops, something went horribly wrong");
            //debugger;
        }
    });
}

//To get Selected Customers in Customer Page
function GetSelectedCustomerList() {
    //debugger;
    var callCycleContactDTOList = [];
    var idstr = '';
    var items = $("#grid").data("kendoGrid").dataSource.data();
    var callCycleContactDTO = new Object();
    var leadCustomerDTO = new Object();
    for (var i = 0; i < items.length; i++) {
        callCycleContactDTO = new Object();
        leadCustomerDTO = new Object();
        var item = items[i];
        if (i == (items.length - 1) && (item != undefined && item != null && item != '' && item.HasSelect == true)) {
            idstr += item.ProductId;
            callCycleContactDTOList.push(item.CustomerCode);
        }
        else if (item != undefined && item != null && item != '' && item.HasSelect == true) {
            idstr += item.ProductId + ",";
            callCycleContactDTOList.push(item.CustomerCode);                  
        }
    }

    //debugger; 
    return callCycleContactDTOList;
}


/*Delete market Click in Market Assign Page*/
function DeleteCustomer(cus_code, source_id) {
    $("#CustomermodalWindow").show()
    showdelete_customer_confirmation(cus_code, source_id);
}

/*Show Delete market Confirmation in Market Assign Page*/
function showdelete_customer_confirmation(cus_code, source_id) {
    $("#save").css("display", "none");
    $("#no").css("display", "inline-block");
    $("#yes").css("display", "inline-block");

    var message = "Do you want to unasign '" + cus_code + "' customer from route?";

    var wnd = $("#CustomermodalWindow").kendoWindow({
        title: "Delete Customer",
        modal: true,
        visible: false,
        resizable: false,
        width: 400
    }).data("kendoWindow");

    $("#div_CustomerConfirm_Message").text(message);
    wnd.center().open();

    $("#yes").click(function () {
        delete_selected_customer('lead_customer/process_forms/processmaster.aspx?fm=deletecustomer&type=Delete&cusno=' + cus_code);
        wnd.close();
    });

    $("#no").click(function () {
        wnd.close();
    });
}

    function disabletabs(){
        setTimeout(function () {
                var ts = $("#tabstrip").data("kendoTabStrip");
                var items = ts.items();
                //ts.enable(ts.tabGroup.children("li:eq(tabIndex)"), false); // disable tab 1
                ts.disable(items);
        }, 700);
    }

function enabletabs(){
//    var ts = $("#tabstrip").data("kendoTabStrip");
//    var tabIndex = 1;
//    ts.enable(ts.tabGroup.children("li:eq(tabIndex)"), true); // enable tab 1

        var ts = $("#tabstrip").data("kendoTabStrip");        
        var items = ts.items();
        ts.enable(items);
}

//function OpenLeadCustomer(LeadStage, CustomerCode, SourceId) {
//    var lead_stage = $("#MainContent_HiddenFieldLeadType").val();
//    if (LeadStage.toLowerCase() == 'lead') {
//        window.location = 'leadentry.aspx?leadid=' + SourceId + '&ty=' + lead_stage + '&fm=lead';
//    }
//    else if (LeadStage.toLowerCase() == 'customer') {
//        if (CustomerCode == null || CustomerCode == '') {
//            window.location = 'leadentry.aspx?leadid=' + SourceId + '&ty=' + lead_stage + '&fm=lead';
//        } else {
//            window.location = 'customer_entry.aspx?custid=' + encodeurl(CustomerCode) + '&ty=' + lead_stage + '&fm=lead';
            
//        }
//    }
//    else {
//        window.location = 'leadentry.aspx?leadid=' + SourceId + '&ty=' + lead_stage + '&fm=lead'; // radlink.href = '#';
//    }

//    LoadCustomerImage(CustomerCode);
//}

function OpenLeadCustomer(LeadStage, CustomerCode, SourceId) {
    //var lead_stage = $("#MainContent_HiddenFieldLeadType").val();
    //if (LeadStage.toLowerCase() == 'lead') {
    window.location = 'customer_entry.aspx?custid=' + CustomerCode + '&ty=customer&fm=lead';
    //}
    //else if (LeadStage.toLowerCase() == 'customer') {
    //    if (CustomerCode == null || CustomerCode == '') {
    //        window.location = 'leadentry.aspx?leadid=' + SourceId + '&ty=' + lead_stage + '&fm=lead';
    //    } else {
    //        window.location = 'customer_entry.aspx?custid=' + encodeurl(CustomerCode) + '&ty=' + lead_stage + '&fm=lead';

    //    }
    //}
    //else {
    //    window.location = 'leadentry.aspx?leadid=' + SourceId + '&ty=' + lead_stage + '&fm=lead'; // radlink.href = '#';
    //}

    //LoadCustomerImage(CustomerCode);
}

function refreshLeadCustomerGrid() {
    var grid = $("#grid").data("kendoGrid");
    grid.dataSource.read();
}

function loadLeadCustomerMap() {
    var entityGrid = $("#grid").data("kendoGrid");
    var selectedRows = entityGrid.dataItem(entityGrid.select());

    if (selectedRows == "" ||  selectedRows == undefined) {
        alert('Please Select Customer.');
    }
    else {
        var strAddress = "";
        var state = "";
        switch (selectedRows["State"].replace(/^\s\s*/, '').replace(/\s\s*$/, '')) {
            case "ACT":
                state = "Australian Capital Territory";
                break;
            case "NSW":
                state = "New South Wales";
                break;
            case "NT":
                state = "Northern Territory";
                break;
            case "QLD":
                state = "Queensland";
                break;
            case "VIC":
                state = "Victoria";
                break;
            case "TAS":
                state = "Tasmania";
                break;
            case "WA":
                state = "Western Australia";
                break;
        }

        if (selectedRows["Address"] != '' && selectedRows["Address"] != null)
            strAddress = selectedRows["Address"];

        if (selectedRows["Address"] != '' && selectedRows["Address"] != null && selectedRows["City"] != '' && selectedRows["City"] != null)
            strAddress += ", " + selectedRows["City"];
        else if (selectedRows["City"] != '' && selectedRows["City"] != null)
            strAddress += selectedRows["City"];

        if (selectedRows["Address"] != '' && selectedRows["Address"] != null && selectedRows["City"] != '' && selectedRows["City"] != null &&
                        state != '' && state != null)
            strAddress += ", " + state;
        else if (strAddress != '' && strAddress != null && state != '' && state != null)
            strAddress += ", " + state;
        else if (strAddress == '' && strAddress == null && state != '' && state != null)
            strAddress += state;

        strAddress = strAddress.replace("&", " ");

        var url = "map.aspx?address=" + strAddress;
        window.location.href = url;
    }
}

function LeadCustomerGridCopyRow() {
    var lead_stage = $("#MainContent_HiddenFieldLeadType").val();
    var entityGrid = $("#grid").data("kendoGrid");
    var selectedRows = entityGrid.dataItem(entityGrid.select());
    var count = entityGrid.select().length;
    if (count == 0) {
        alert('Please select the Row');
    }
    else if (lead_stage == 'Enduser') {
        var text = "Copy Hear \n" +
        selectedRows["EndUserCode"] + " " +
        selectedRows["CustomerName"] + " " +
        selectedRows["RepCode"] + " "+
        selectedRows["Contact"] + " " +
        selectedRows["Address1"] + " " +
        selectedRows["Address2"] + " " +
        selectedRows["State"] + " " +
        selectedRows["City"] + " " +
        selectedRows["PostCode"] + " " +
        selectedRows["EmailAddress"] + " " +
        selectedRows["Telephone"];
        alert(text);
    }
    else {

        //var varName = selectedRows["Name"];
        //var varOriginator = selectedRows["Originator"];
        var varLeadStage = selectedRows["LeadStage"];
        var varCustomerCode = selectedRows["CustomerCode"];
        var varState = selectedRows["State"];
        //var varStartDate = selectedRows["StartDate"].toString("dd/MM/yyyy");
        var varCity = selectedRows["City"];
        //var varChannelDescription = selectedRows["ChannelDescription"];
        var varSourceId = selectedRows["SourceId"];
        var varAddress = selectedRows["Address"];

        //var text = "Copy Hear \n" + varName + " " + varOriginator + " " + varLeadStage + " " + varCustomerCode + " " + varState + " " + varCity + " " + varChannelDescription + " " + varSourceId;
        var text = "Copy Hear \n" + varLeadStage + " " + varCustomerCode + " " + varState + " " + varCity + " " + varSourceId + " " + varAddress;
        alert(text);
    }
}

function LoadReqSentLeadCustomerGrid() {
    var activeReqSent = $("#MainContent_HiddenFieldActiveReqSent").val();

    if (activeReqSent == "1") {
        $("#MainContent_HiddenFieldReqSentIsChecked").val("1");
        $("#MainContent_HiddenFieldActiveReqSent").val("0");
        $("#MainContent_HiddenFieldIsChecked").val("1");
    }
    else {
        $("#MainContent_HiddenFieldReqSentIsChecked").val("0");
        $("#MainContent_HiddenFieldActiveReqSent").val("1");
        $("#MainContent_HiddenFieldIsChecked").val("1");
    }
    loadLeadCustomerGrid($("#MainContent_HiddenFieldIsChecked").val(), $("#MainContent_HiddenFieldReqSentIsChecked").val(), $("#cmbRepType option:selected").val());
//    var lead_stage = $("#MainContent_HiddenFieldLeadType").val();
//    if (lead_stage != 'Enduser') {
//        loadLeadCustomerGrid($("#MainContent_HiddenFieldIsChecked").val(), $("#MainContent_HiddenFieldReqSentIsChecked").val(), $("#cmbRepType option:selected").val());
//    } else {
//        loadEndUserGrid($("#MainContent_HiddenFieldIsChecked").val(), $("#MainContent_HiddenFieldReqSentIsChecked").val(), $("#cmbRepType option:selected").val());
//    }
}

function LoadInactiveCustomers(OriginatorString) {
    var text = '<span class="cross icon"></span>';
    var activeInactiveChecked = $("#MainContent_HiddenFieldActiveInactiveChecked").val();
    if (activeInactiveChecked == "1") {
        $("#MainContent_HiddenFieldIsChecked").val("0");
        $("#MainContent_HiddenFieldActiveInactiveChecked").val("0");
        $("#MainContent_HiddenFieldReqSentIsChecked").val("0");
        $("#href_LoadInactiveCustomers").html(text+'Active Outlets');
    }
    else {
        $("#MainContent_HiddenFieldIsChecked").val("1");
        $("#MainContent_HiddenFieldActiveInactiveChecked").val("1");
        $("#MainContent_HiddenFieldReqSentIsChecked").val("0");
        $("#href_LoadInactiveCustomers").html(text+'Inactive Outlets');
    }
    var lead_stage = $("#MainContent_HiddenFieldLeadType").val();
    if (lead_stage.toLowerCase() != 'enduser') {
        OutletsPage_loadLeadCustomerGrid($("#MainContent_HiddenFieldIsChecked").val(), $("#MainContent_HiddenFieldReqSentIsChecked").val(), $("#cmbRepType option:selected").val(), OriginatorString);
    } else {
        loadEndUserGrid($("#MainContent_HiddenFieldIsChecked").val(), $("#MainContent_HiddenFieldReqSentIsChecked").val(), $("#cmbRepType option:selected").val());
    }
    //loadLeadCustomerGrid($("#MainContent_HiddenFieldIsChecked").val(), $("#MainContent_HiddenFieldReqSentIsChecked").val(), $("#cmbRepType option:selected").val());
}

function LeadCustomerGridOrganisation() {
    var lead_stage = $("#MainContent_HiddenFieldLeadType").val();

//    if(lead_stage.toLowerCase() == 'customer'){
//        window.location.href = "customer_entry.aspx";
    //    }

    if (lead_stage.toLowerCase() == 'customer') {
        window.location.href = "customer_entry.aspx";     
    }
    else if (lead_stage.toLowerCase() != 'enduser') {
        window.location.href = "leadentry.aspx"+'?ty='+lead_stage+'&fm=lead';
    } else {
        window.location.href = "enduser_entry.aspx";
    }
    
}

function LeadCustomer_cmbRepType_onChange() {
    var repType = $("#cmbRepType option:selected").val();
    $("#MainContent_HiddenFieldRepType").val(repType);
    loadLeadCustomerGrid($("#MainContent_HiddenFieldIsChecked").val(), $("#MainContent_HiddenFieldReqSentIsChecked").val(), $("#cmbRepType option:selected").val());
}

function LeadCustomerClearSetting() {
    //var datasource = $("#Grid").data("kendoGrid").dataSource;
    var datasource = $("#grid").data("kendoGrid").dataSource;
    var filters = datasource.filter([]);
    //loadgrid();
}

function LoadDefaultSettingLeadCustomerGrid() {
    var status = confirm("Do you Wish to apply Default Filter Settings?");
    if (status == true) {
         window.location  = location.href
        //window.location = "../../lead_customer/transaction/lead_contacts.aspx";
    }
}




function HeaderMenueLeadCustomerGrid(falg) {
    var selectedHeader = "";
    var notSelectedHeader = "";
    var root = "";
    var lead_stage = $("#MainContent_HiddenFieldLeadType").val();
    var staus = confirm("Save to Default filter Settings?");

    if (staus == true) {
        var grid = $("#grid").data("kendoGrid");
        var col = grid.columns;
        length = col.length;
        for (var i = 1; i < length; i++) {
            if (grid.columns[i].hidden == true) {
                notSelectedHeader = notSelectedHeader + grid.columns[i].field + ",";
            } else {
                selectedHeader = selectedHeader + grid.columns[i].field + ",";
            }
        }

        if (selectedHeader == "") {
            selectedHeader = notSelectedHeader;
            notSelectedHeader = "";
        }

        if (falg == "server") {
            root = "../process_forms/processmaster.aspx?fm=leadCustomer&type=Update&selected=" + selectedHeader + "&notselected=" + notSelectedHeader + "&leadstage=" + lead_stage;

            $.ajax({
                url: root,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg != "") {
                        document.getElementById("MainContent_div_message").style.display = "block";
                        document.getElementById("MainContent_div_message").innerHTML = msg;
                        hideStatusDiv("MainContent_div_message");
                    }
                },
                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg; // "Sorry there was an error: ";
                        //$("#" + tagertdiv).html(msg);
                    }
                }
            });
        }
        else if (falg == "client") {
            root = "../process_forms/processmaster.aspx?fm=leadCustomer&type=saveXML&selected=" + selectedHeader + "&notselected=" + notSelectedHeader+"&leadstage=" + lead_stage;
            location.assign(root);
        }

        $("div.rtbSlide").hide();
    }
}


function pageLoadLeadCustomerGrid() {
    var lead_stage = $("#MainContent_HiddenFieldLeadType").val();
    var root = "../process_forms/processmaster.aspx?fm=leadCustomer&type=LoadState&leadstage=" + lead_stage;

    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg != "") {
                var txt = msg.split(",");

                var grid = $("#grid").data("kendoGrid");
                var col = grid.columns;
                length = col.length;
                for (var out = 0; out < txt.length; out++) {
                    for (var i = 1; i < length; i++) {
                        if (txt[out] == col[i].field) {
                            grid.hideColumn(i);
                        }
                    }
                }
            }
            else {
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg; // "Sorry there was an error: ";
                //$("#" + tagertdiv).html(msg);
            }
        }
    });
}

function loadLeadCustomerActivity() {
    var entityGrid = $("#grid").data("kendoGrid");
    var selectedRows = entityGrid.dataItem(entityGrid.select());
    var count = entityGrid.select().length;
    if (count == 0) {
        alert('Please select the Row');
    }
    else {
        var leadStage = $("#MainContent_HiddenFieldLeadType").val();

        if (leadStage == "Customer") {
            var CustomerCode = selectedRows["CustomerCode"];
            window.location = "../../activity_planner/transaction/activityentry.aspx?custid=" + CustomerCode;
        }
        else if (leadStage == "Enduser") {
            var EndUserCode = selectedRows["EndUserCode"];
            window.location = "../../activity_planner/transaction/activityentry.aspx?EndUserCode=" + EndUserCode;
        }
        else {
            alert("Activity can be added to a single Organisation at a time.");
        }
    }
}

function loadLeadCustomerOpportunity() {
    var entityGrid = $("#grid").data("kendoGrid");
    var selectedRows = entityGrid.dataItem(entityGrid.select());
    var count = entityGrid.select().length; 
    if (count == 0) {
        alert('Please select the Row');
    }
    else {
        var leadStage = $("#MainContent_HiddenFieldLeadType").val();

        if (leadStage == "Customer") {
            var CustomerCode = selectedRows["CustomerCode"];
            window.location = "../../pipelines_stage/transaction/opportunityentry.aspx?custid=" + CustomerCode;
        }
        else if (leadStage == "Enduser") {
            var EndUserCode = selectedRows["EndUserCode"];
            window.location = "../../pipelines_stage/transaction/opportunityentry.aspx?EndUserCode=" + EndUserCode;
        }
        else {
            alert("Opportunity can be added to a single Organisation at a time.");
        }
    }
}

function loadLeadCustomerContactPerson() {
    var entityGrid = $("#grid").data("kendoGrid");
    var selectedRows = entityGrid.dataItem(entityGrid.select());
    var count = entityGrid.select().length;
    if (count == 0) {
        alert('Please select the Row');
    }
    else {
        var leadStage = $("#MainContent_HiddenFieldLeadType").val();

        if (leadStage == "Customer") {
            var CustomerCode = selectedRows["CustomerCode"];
            window.location = "../../lead_customer/transaction/contact_person_entry.aspx?custid=" + CustomerCode;
        }
        else if (leadStage == "Enduser") {
            var EndUserCode = selectedRows["EndUserCode"];
            window.location = "../../lead_customer/transaction/contact_person_entry.aspx?EndUserCode=" + EndUserCode;
        }
        else {
            alert("Contact Person can be added to a single Organisation at a time.");
        }
    }
}

function LeadCustomerGrid_onDataBound(arg) {
    
    if ($('.myCheckbox').attr("checked") == "checked") {
        $('.myCheckbox').prop('checked', false);
    }

    var lead_stage = $("#MainContent_HiddenFieldLeadType").val();
    if (lead_stage.toLowerCase() != 'enduser') {
        $('#div_row_count').html('<b># Recs : '+ $('#div_rowCount_max').html()+'</b>'); 
//        $('#div_rowCount_sum').html('Total Contacts : ' + $('#div_rowCount_max').html());
    } else {
        $('#div_row_count').html('<b># Recs : '+ $('#div_ViewRowCount_max').html()+'</b>'); 
        $('#div_rowCount_sum').html('Total End Users : ' + $('#div_rowCount_max').html());
    }

    var my = {};
    $('#testMenu').css('display','block');
    my.contextMenu = function ContextMenu(options) {
        if (!options) { alert('You have not specified any options!'); return; }
        if (!options.trigger) { alert('You must supply a trigger element selector!'); return; }
        if (!options.menu) { alert('You must specify a menu element selector!'); return; }
        //if (!options.callback) { alert('You must specify a callback function!'); return; }
        options.rightButton = options.rightButton;
        options.zindex = options.zindex || 999000;

        var menu = $(options.menu);
        if (!menu.parent().hasClass('k-context-menu')) menu.wrap(document.createElement('div'));
        menu = menu.parent().addClass('k-context-menu').hide();

        var panel = $('#ctxMenuPanel');
        if (!panel[0]) panel = $(document.createElement('div')).attr('id', 'ctxMenuPanel');

        menu.visible = false;
        menu.addClass('k-calendar-container k-popup k-group k-reset');
        menu.css({ position: 'absolute', 'background-color': 'white', display: 'none', 'z-index': options.zindex + 1, padding: 0 });

        menu.close = function () { menu.fadeOut('fast'); panel.hide(); menu.visible = false; };
        menu.show = function () {
            
//            if ($(this).attr("href") != undefined) {
//                window.location = $(this).attr("href");
//            } 
            if ($(this).find("a").attr("href") != undefined) {
                window.location = $(this).find("a").attr("href");
            } 
            else if (menu.visible) {
                    menu.close();
            }
            else {
                    var pos = $(this).offset(); pos.top += 10; pos.left += 15;
                    panel.css({ top: 0, left: 0, width: '100%', height: $(document).height(), 'background-color': 'transparent' }).show();
                    menu.data('trigger', $(this));
                    menu.css(pos).fadeIn('fast');
                    menu.visible = true;
            }
            return false;
        };

        panel.css({ position: 'absolute', display: 'none', 'z-index': options.zindex });
        panel.appendTo('html').click(menu.close);

        var list = menu.find('ul').css({ 'list-style-type': 'none', padding: 0, margin: 0 });
        list.find('li').click(function () {
            menu.close();
            //options.callback($(this), menu.data('trigger'));
        }).mouseenter(function () {
            $(this).addClass('k-state-hover');
        }).mouseleave(function () {
            $(this).removeClass('k-state-hover');
        }).mousedown(function () {
            $(this).addClass('k-state-selected');
        }).mouseup(function () {
            $(this).removeClass('k-state-selected');
        }).css({ margin: 0, cursor: 'pointer', padding: '3px 7px 3px 7px' });

        // bind the triggers
        $(options.trigger).each(function (index) {

            //$(this).attr('id', 'Row' + $(this).index('tr'));
            if ($(this).find("a").attr("href") == undefined && $(this).find("input").attr("type") == undefined) {
                if (options.leftButton) $(this).click(menu.show);
            } 
             //if ($(this).find("input").attr("type") == undefined) {
             //   if (options.leftButton) $(this).click(menu.show);
            //} 
            
            //if (options.rightButton) $(this).bind("contextmenu", menu.show);
            //if (options.rightButton) $(this).bind("handleTouchStart", menu.show);
        });
    };

    my.contextMenu({
        trigger: '.menu, #grid td',
        leftButton: true,
        rightButton: true,
        menu: '#testMenu'//,
        //callback: ctxCallback
    });

    // add a row id to each row so that we can illustrate how to read them in the callback
    $('#grid tr').each(function () {
        $(this).attr('id', 'Row' + $(this).index('tr'));
    });
}



function CheckEndUserHeader(control) {

    $("#div_loader").show();
    var entityGrid = $("#grid").data("kendoGrid").dataSource._data;
    var isChecked = 0;
    if ($(control).attr("checked") == "checked") {
        isChecked = 1;
    }

    var enduserstring = "";
    for (var i = 0; i < entityGrid.length; i++) {
        var CustomerCode = entityGrid[i].CustomerCode;
        var LeadStage = entityGrid[i].LeadStage;
        var EndUserCode = entityGrid[i].EndUserCode;

        if (isChecked == 1) {
            if (enduserstring == "") {
                enduserstring = " (de.enduser_code= '" + EndUserCode + "' AND de.cust_code = '" + CustomerCode + "')";
            }
            else {
                enduserstring = enduserstring + " OR (de.enduser_code= '" + EndUserCode + "' AND de.cust_code = '" + CustomerCode + "')";
            }
        }
        else {
            if (enduserstring == "") {
                enduserstring = EndUserCode;
            }
            else {
                enduserstring = enduserstring + "," + EndUserCode;
            }
        }

    }

    var root = ROOT_PATH + "call_cycle/process_forms/processmaster.aspx?fm=checkEndUserHeader&type=query&enduserstring=" + encodeurl(enduserstring) + "&isChecked=" + isChecked; ;

    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg == "true") {
                $("#MainContent_HiddenFieldCallCycleCount").val("1");
            }
            else {
                $("#MainContent_HiddenFieldCallCycleCount").val("0");
            }
            refreshLeadCustomerGrid();
            setTimeout(function () {
                if (isChecked == 1) {
                    $('.myCheckbox').prop('checked', true);
                }
                $("#div_loader").hide();
            }, 600);
        },
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
}

var gridenduserindex = 0
function loadEndUserGrid(activeInactiveChecked, reqSentIsChecked, repType) {
var take_grid=$("#MainContent_hfPageIndex").val();
    $("#grid").html("");
    $("#grid").kendoGrid({
        //height: 400,
        columns: [
                    { field: "CustomerCode", title: "", hidden: true },
                   // { title: "", width: "30px", template: '<input type="checkbox"/>' },
                    //{ title: "", width: "30px", template: '<input type="checkbox" onchange="CheckEndUser(\'#=EndUserCode#\',\'#=CustomerCode#\',this)" #=SelectedToCall#/>' },
                    { title: "", headerTemplate: '<input type="checkbox" class="myCheckbox" onchange="CheckEndUserHeader(this)" />', width: "30px", template: '<input type="checkbox" onchange="CheckEndUser(\'#=EndUserCode#\',\'#=CustomerCode#\',this)" #=SelectedToCall#/>' },
                    { template: '<a href="javascript:OpenEndUser(\'#=EndUserCode#\',\'#=CustomerCode#\',this)">#=Name#</a>',
                        field: "Name", width: "250px", footerTemplate: "<div id=\"div_rowCount_sum\" ></div>"
                    },
                    { field: "EndUserCode", title: "Code", width: "83px" },
                    { field: "CustomerName", title: "Customer Name", width: "175px", template: '<div style="height: 20px; ">#=CustomerName#</div>' },
                    { field: "RepCode", title: "Assigned To", width: "90px" },
                    { field: "Contact", title: "Contact", width: "90px" },
                    { field: "Address1", title: "Address 1", width: "150px", template: '<div style="height: 20px; ">#=Address1#</div>' },
                    { field: "Address2", title: "Address 2", width: "110px", template: '<div style="height: 20px; ">#=Address2#</div>' },
                    { field: "State", title: "State", width: "60px" },
                    { field: "City", title: "City", width: "100px" },
                    { field: "PostCode", title: "Post Code", width: "70px" },
                    { field: "EmailAddress", title: "Email Address", width: "100px" },
                    { field: "Telephone", title: "Telephone", width: "100px" },
                    { field: "rowCount", title: "rowCount", format: "{0:n2}", hidden: true, footerTemplate: "<div id=\"div_rowCount_max\" >#: max #</div>" },
                    { field: "ViewRowCount", title: "ViewRowCount", format: "{0:n2}", hidden: true, footerTemplate: "<div id=\"div_ViewRowCount_max\" >#: max #</div>" }
                ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: true,
        dataBound: LeadCustomerGrid_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "EndUserCode",
                    fields: {
                        CustomerCode :{ validation: { required: true} },
                        Name: { validation: { required: true} },
                        EndUserCode: { validation: { required: true} },
                        CustomerName: { validation: { required: true} },
                        RepCode: { validation: { required: true} },
                        Contact: { validation: { required: true} },
                        Address1: { validation: { required: true} },
                        Address2: { validation: { required: true} },
                        State: { validation: { required: true} },
                        City: { validation: { required: true} },
                        PostCode: { validation: { required: true} },
                        EmailAddress: { validation: { required: true} },
                        Telephone: { validation: { required: true} },
                        rowCount: { type: "number", validation: { required: true} },
                        ViewRowCount: { type: "number", validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url:ROOT_PATH+ "service/lead_customer/lead_contacts_Service.asmx/GetDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'Enduser',pgindex:take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            var entityGrid = $("#grid").data("kendoGrid");
                            if((take_grid!='') && (gridenduserindex=='0')){
                                entityGrid.dataSource.page((take_grid-1));
                                gridenduserindex='1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            aggregate: [ { field: "rowCount", aggregate: "max" } ,
            { field: "ViewRowCount", aggregate: "max" } ]
        }
    });
}

function OpenEndUser(EndUserCode, CustomerCode, a) {
    var lead_stage = $("#MainContent_HiddenFieldLeadType").val();
    window.location = 'EndUser_entry.aspx?custid=' + encodeurl(CustomerCode) + '&eduid=' + encodeurl(EndUserCode) + '&fm=endUser';
   // window.location = 'EndUser_entry.aspx?custid=' + encodeurl(CustomerCode) + '&eduid=' + EndUserCode + '&fm=endUser';
   // $("#div_grid_enduser").find("tr[data-uid=" + CustomerCode + "]").css('background-color', '#d8bfd8');
}

var gridoppertunityindex = 0
function loadOpportunityGrid(pipelineStageId, clientType) {
var take_grid = $("#MainContent_hfPageIndex").val();
    $("#OpportunityGrid").html("");
    $("#OpportunityGrid").kendoGrid({
        height: 400,
        columns: [
            { template: '<a href="javascript:OpenOpportunityLeadCustomer(\'#=LeadStage#\',\'#=CustCode#\',\'#=EndUserCode#\',\'#=LeadID#\')">#=LeadName#</a>',
                field: "LeadName", width: "250px", title: "Name", footerTemplate: "<div id=\"div_rowCount_sum\" ></div>"
            },
            { field: "OpportunityID", title: "OpportunityID", hidden: true },
            { field: "LeadID", title: "LeadID", hidden: true },
            { field: "CustCode", title: "CustCode", hidden: true },
            { field: "EndUserCode", title: "EndUserCode", hidden: true },
            { field: "Originator", title: "Rep" },
            { field: "Name", title: "Opportunity", template: '<div style="height: 20px; ">#=Name#</div>' },
            { field: "Description", title: "Description", template: '<div style="height: 20px; ">#=Description#</div>' },
            { field: "CloseDate", title: "Close Date", width: "80px", format: DATE_FORMAT3 },
            { field: "Probability", title: " % ", attributes: { style: "text-align: right" }, headerTemplate: "<div style =\"text-align: right;\"> % </div>"},
            { field: "Amount", title: "Amount", width: "130px", format: "{0:n2}", footerTemplate: "<div id=\"div_amount_sum\" ></div>",
                attributes: { style: "text-align: right" }, headerTemplate: "<div style =\"text-align: right;\">Amount</div>"
            },
            { field: "Units", title: "Units", width: "110px", format: "{0:n2}", footerTemplate: "<div id=\"div_units_sum\" ></div>",
                attributes: { style: "text-align: right" }, headerTemplate: "<div style =\"text-align: right;\">Units</div>"
            },
            { field: "Tonnes", title: "Tonnes", width: "110px", format: "{0:n2}", footerTemplate: "<div id=\"div_tonnes_sum\" ></div>",
                attributes: { style: "text-align: right" }, headerTemplate: "<div style =\"text-align: right;\">Tonnes</div>"
            },
            { field: "LeadStage", title: "Lead Stage", width: "80px" },
            { field: "Business", title: "Business" },
            { field: "IndustryDescription", title: "Industry" },
            { field: "Address", title: "Address", template: '<div style="height: 20px; ">#=Address#</div>' },
            { field: "City", title: "City" },
            { field: "State", title: "State" },
            { field: "PostCode", title: "PostCode" },
            { field: "AmountSum", title: "AmountSum", format: "{0:n2}", hidden: true, footerTemplate: "<div id=\"div_amount_max\" > Sum: #: max # </div>" },
            { field: "UnitsSum", title: "UnitsSum", format: "{0:n2}", hidden: true, footerTemplate: "<div id=\"div_units_max\" > Sum: #: max # </div>" },
            { field: "TonnesSum", title: "TonnesSum", format: "{0:n2}", hidden: true, footerTemplate: "<div id=\"div_tonnes_max\" > Sum: #: max # </div>" },
            { field: "rowCount", title: "rowCount", format: "{0:n2}", hidden: true, footerTemplate: "<div id=\"div_rowCount_max\" > Total Opportunities : #: max # </div>" }
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        dataBound: OpportunityGrid_onchange,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    id: "OpportunityID",
                    fields: {
                        LeadName: { validation: { required: true} },
                        OpportunityID: { validation: { required: true} },
                        CustCode: { validation: { required: true} },
                        EndUserCode: { validation: { required: true} },
                        LeadID: { editable: true, nullable: true },
                        Name: { validation: { required: true} },
                        Description: { validation: { required: true} },
                        CloseDate: { type: "Date", validation: { required: true} },
                        Probability: { type: "number", validation: { required: true} },
                        Amount: { type: "number", validation: { required: true} },
                        Units: { type: "number", validation: { required: true} },
                        Tonnes: { type: "number", validation: { required: true} },
                        LeadStage: { validation: { required: true} },
                        Business: { validation: { required: true} },
                        IndustryDescription: { validation: { required: true} },
                        Address: { validation: { required: true} },
                        City: { validation: { required: true} },
                        State: { validation: { required: true} },
                        PostCode: { validation: { required: true} },
                        AmountSum: { type: "number", validation: { required: true} },
                        UnitsSum: { type: "number", validation: { required: true} },
                        TonnesSum: { type: "number", validation: { required: true} },
                        rowCount: { type: "number", validation: { required: true} }
                    }
                }

            },
            transport: {

                read: {
                    url: "../../service/pipelines_stage/pipelines_stage_service.asmx/GetOppDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pipelineStageId: pipelineStageId, status: "0", clientType: clientType, pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            var entityGrid = $("#OpportunityGrid").data("kendoGrid");
                            if ((take_grid != '') && (gridoppertunityindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridoppertunityindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            aggregate: [
                            { field: "AmountSum", aggregate: "max" },
                            { field: "UnitsSum", aggregate: "max" },
                            { field: "TonnesSum", aggregate: "max" },
                            { field: "rowCount", aggregate: "max" }
                        ]

        }
    });
}

function unloadStock(StockHeaderId, CatalogCode, CatalogName, AllocQty, LoadQty, BalanceQty, ReturnQty, LoadQtyOld, VehicleBalanceQty, VehicleReturnQty) {            
            if(LoadQty != 0)
            {     
                if(BalanceQty == 0 && VehicleBalanceQty == 0)
                {   
                    //there should be a balance quantity left either in vehicle or allocated
                    var infoMsg = GetInformationMessageDiv("There is No balance remaining!", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(infoMsg);
                    hideStatusDiv("MainContent_div_message");
                    return;
                }           
                if(LoadQty != LoadQtyOld)
                {
                    //Load quantity should be less than Alloc Qty
                      var infoMsg = GetInformationMessageDiv("Please Save Loaded Quantity before Unloading!", "MainContent_div_message");
                      $('#MainContent_div_message').css('display', 'block');
                      $("#MainContent_div_message").html(infoMsg);
                      hideStatusDiv("MainContent_div_message");
                }
                else{
                    $("#HiddenFieldUnLoadUID").val("");
                    $("#txtUnloadQty").val("");
                    $("#txtCatalogName").val("");
                    $("#hdnSelectedStockHeaderId").val("");
                    $("#hdnSelectedAllocQty").val("");
                    $("#hdnSelectedCatalogCode").val("");
                    $("#hdnSelectedLoadQty").val("");
                    $("#MainContent_lblBalanceQty").html("");
                    $("#txtUnloadQty").val("");
                    $("#hdnSelectedTotalReturnQty").val("");
                    $("#hdnSelectedLoadQtyOld").val("");
                    $("#hdnSelectedVehicleBalanceQty").val("");
                    $("#hdnSelectedVehicleReturnQty").val("");
                    $("#hdnSelectedBalanceQty").val("");
                        
                    $("select#MainContent_dropDownUnloadFrom").prop('selectedIndex', 0);
                    //debugger;

                    var grid=$("#StockGrid").data("kendoGrid");   
                    var row = grid.select();
                    $("#HiddenFieldUnLoadUID").val(row[0].dataset.uid);
                
                    $("#hdnSelectedStockHeaderId").val(StockHeaderId);
                    $("#hdnSelectedCatalogCode").val(encodeurl(CatalogCode));
                    $("#hdnSelectedAllocQty").val(AllocQty);
                    $("#hdnSelectedLoadQty").val(LoadQty);
                    $("#txtCatalogName").val(CatalogName);
                    $("#MainContent_lblBalanceQty").html(BalanceQty);
                    $("#txtUnloadQty").val(BalanceQty);
                    $("#hdnSelectedTotalReturnQty").val(ReturnQty);
                    $("#hdnSelectedLoadQtyOld").val(LoadQtyOld);
                    $("#hdnSelectedVehicleBalanceQty").val(VehicleBalanceQty);
                    $("#hdnSelectedVehicleReturnQty").val(VehicleReturnQty);
                    $("#hdnSelectedBalanceQty").val(BalanceQty);
                    
                    $("#window").css("display", "block");
                    var wnd = $("#window").kendoWindow({
                        title: "Unload Stock",
                        modal: true,
                        visible: false,
                        resizable: false
                    }).data("kendoWindow");

                    wnd.center().open();
                }
            }
            else
            {   
                //there should be a balance quantity left either in vehicle or allocated
                var infoMsg = GetInformationMessageDiv("Please Load Stock!", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(infoMsg);
                hideStatusDiv("MainContent_div_message");
                return;
            }  
}

function loadStock(StockHeaderId, CatalogCode, CatalogName, AllocQty, LoadQty, BalanceQty, Sku) {
    if(AllocQty != 0)
    {
        if(BalanceQty == 0)
        {   
            //there should be a some allocated balance quantity left to load
            var infoMsg = GetInformationMessageDiv("Please Allocate Stock!", "MainContent_div_message");
            $('#MainContent_div_message').css('display', 'block');
            $("#MainContent_div_message").html(infoMsg);
            hideStatusDiv("MainContent_div_message");
            return;
        }  

        $("#txtCatalogName").val("");
        $("#hdnSelectedStockHeaderId").val("");
        $("#hdnSelectedAllocQty").val("");
        $("#hdnSelectedCatalogCode").val("");
        $("#hdnSelectedLoadQty").val("");
        $("#MainContent_lblBalanceQty").html("");
        $("#txtLoadQty_load").val("");
        $("#HiddenFieldLoadUID").val("");
        $("#HiddenFieldSKU").val("");

        $("#hdnSelectedStockHeaderId").val(StockHeaderId);
        $("#hdnSelectedCatalogCode").val(encodeurl(CatalogCode));
        $("#hdnSelectedAllocQty").val(AllocQty);
        $("#MainContent_lblBalanceQty").html(BalanceQty);
        $("#hdnSelectedLoadQty").val(LoadQty);
        $("#MainContent_lblCatalogName_load").html(CatalogName);
        $("#MainContent_lblAllocatedQty_load").html(AllocQty);
        $("#MainContent_lblLoadQty_load").html(BalanceQty);
        $("#HiddenFieldSKU").val(Sku);

        //$("#txtmilesLoadQty_load").val("0");
        $("#txtmilesLoadQty_load").val(BalanceQty);
        $("#txtpackslesLoadQty_load").val("0");
        
        var grid=$("#StockGrid").data("kendoGrid");
   
        var row = grid.select();
        $("#HiddenFieldLoadUID").val(row[0].dataset.uid);

        $("#div_load").css("display", "block");
        var wnd = $("#div_load").kendoWindow({
            title: "Load Stock",
            modal: true,
            visible: false,
            resizable: false
        }).data("kendoWindow");

        wnd.center().open();
    }
    else
    {
        //there should be an Allocated Quantity
        var infoMsg = GetInformationMessageDiv("Please Allocate Stock!", "MainContent_div_message");
        $('#MainContent_div_message').css('display', 'block');
        $("#MainContent_div_message").html(infoMsg);
        hideStatusDiv("MainContent_div_message");
        return;
    }
}

function closepopupStockload() {
    var wnd = $("#div_load").kendoWindow({
        title: "Load Stock",
        modal: true,
        visible: false,
        resizable: false
    }).data("kendoWindow");
    wnd.center().close();
    $("#window").css("display", "none");
}

function insertloadrecord() {

    var mil_val = $("#txtmilesLoadQty_load").val();
    var packs_val = $("#txtpackslesLoadQty_load").val();
    var sticks = 0;
    var load_uid = $("#HiddenFieldLoadUID").val();
    var sku = $("#HiddenFieldSKU").val();

    var load_grid = $("#StockGrid").data("kendoGrid");
    var model = $("#StockGrid").data("kendoGrid").dataSource.getByUid(load_uid);

    if (model != undefined) {
        // debugger;
        //Convert mil into sticks.
        if (mil_val != '0' && mil_val != null && mil_val != undefined) {
            sticks = ConvertMilsIntoSticks(mil_val);
        }
        //Convert packs into sticks.
        if (packs_val != '0' && packs_val != null && packs_val != undefined && packs_val != '') {
            sticks = sticks + ConvertPacksIntoSticks(packs_val, sku);
        }
        //debugger;
        //Convert all sticks into miles.
        var load_mils = ConvertStickIntoMiles(sticks);
        load_mils = load_mils.toFixed(3);

        if (parseFloat(load_mils) == 0 || load_mils == '' || load_mils == null) {
            var sucessMsg = GetInformationMessageDiv("Please Enter a Proper Load Quantity !", "MainContent_div_message_popup_load");
            $('#MainContent_div_message_popup_load').css('display', 'block');
            $("#MainContent_div_message_popup_load").html(sucessMsg);
            hideStatusDiv("MainContent_div_message_popup_load");
            return;
        }

        if (parseFloat(load_mils) <= parseFloat(model.BalanceQty)) {
            model.fields.LoadQty.editable = true;
            model.set("LoadQty", parseFloat(load_mils) + parseFloat(model.LoadQty));
            model.fields.LoadQty.editable = false;
            model.BalanceQty = parseFloat(model.BalanceQty) - parseFloat(load_mils);
            model.set("BalanceQty", model.BalanceQty);
            //model.VehicleBalanceQty=model.AllocQty- model.BalanceQty;
            //model.set("VehicleBalanceQty", model.VehicleBalanceQty);
            model.VehicleBalanceQty = parseFloat(model.VehicleBalanceQty) + parseFloat(load_mils);
            model.set("VehicleBalanceQty", model.VehicleBalanceQty);
            load_grid.refresh();
            $("#MainContent_HiddenFieldGridDataChanged").val(1);
        }
        else {
            //Load quantity should be less than Alloc Qty
            var infoMsg = GetInformationMessageDiv("Load Quantity should be less than Balance Quantity !", "MainContent_div_message_popup_load");
            $('#MainContent_div_message_popup_load').css('display', 'block');
            $("#MainContent_div_message_popup_load").html(infoMsg);
            hideStatusDiv("MainContent_div_message_popup_load");
            return;
        }
    }

    closepopupStockload();
}

function loadproductlist(alloDate, distributorId, divisionId) {
    var take_grid = $("#MainContent_hfPageIndex").val();
    $("#div_loader").show();
    $("#hdnSelectedAllocDate").val(alloDate);

    //check for conditions to enable/disable Unload link
    //check for conditions to enable/disable Load link

    var url = "javascript:unloadStock('#=StockHeaderId#','#=CatalogCode#','#=CatalogName#','#=AllocQty#','#=LoadQty#','#=BalanceQty#','#=ReturnQty#','#=LoadQtyOld#','#=VehicleBalanceQty#','#=VehicleReturnQty#');"
    url = "<a href=\"" + url + "\">Unload</a>";

    var load_url = "javascript:loadStock('#=StockHeaderId#','#=CatalogCode#','#=CatalogName#','#=AllocQty#','#=LoadQty#','#=BalanceQty#','#=Sku#');"
    load_url = "<a href=\"" + load_url + "\">Load</a>";

    $("#StockGrid").kendoGrid({
        height: 350,
        selectable: true,
        columns: [
            { field: "RecordId", title: "#", width: "35px", editable: false },
            { field: "StockHeaderId", title: "Stocke Header ID", width: "100px", editable: false, hidden: true },
            { field: "StockDetailId", title: "Stocke Detail ID", width: "100px", editable: false, hidden: true },
            { field: "CatalogCode", title: "Code", width: "50px", editable: false, hidden: false },
            { field: "CatalogName", title: "Product", width: "300px", editable: false },
            { field: "AllocQty", title: "Allocate Qty.", width: "150px", format: "{0:n1}", editor: AllocQtyEditor, editable: true },
            { field: "LoadQty", title: "Load Qty(Mil)", format: "{0:n1}", editor: LoadQtyEditor, editable: false, hidden: true },
            { field: "BalanceQty", title: "Allocated Balance Qty(Mil)", format: "{0:n1}", editor: LoadQtyEditor, editable: false, hidden: true },
            { field: "VehicleBalanceQty", title: "Vehicle Balance Qty(Mil)", format: "{0:n1}", editable: false, hidden: true },
            { field: "ReturnQty", title: "Allocated Return Qty(Mil)", format: "{0:n1}", editable: false, hidden: true },
            { field: "VehicleReturnQty", title: "Vehicle Return Qty(Mil)", format: "{0:n1}", editable: false, hidden: true },
            { field: "ReturningQty", title: "Returning Qty(Mil)", format: "{0:n1}", editable: false, hidden: true },
            { field: "Sku", title: "SKU", format: "{0:n1}", editable: false, hidden: true },
            { field: "LoadQtyOld", title: "LoadQtyOld", format: "{0:n1}", editable: false, hidden: true },
            { field: "AllocationQtyTotal", title: "Total Allocated Qty.", width: "150px", format: "{0:n1}", editable: false, hidden: false },
            { field: "StockTotalId", title: "StockTotalId", width: "150px", editable: false, hidden: true },
            { template: load_url, width: "50px", hidden: true },
            { template: url, width: "60px", hidden: true }
        ],
        editable: true, // enable editing
        pageable: true,
        height: 400,
        sortable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 100,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        StockHeaderId: { type: "number", validation: { required: true } },
                        StockDetailId: { type: "number", validation: { required: true } },
                        CatalogCode: { editable: false, validation: { required: true } },
                        CatalogName: { editable: false, validation: { required: true } },
                        AllocQty: { type: "number", validation: { required: true } },
                        LoadQty: { type: "number", validation: { required: true }, editable: false },
                        BalanceQty: { type: "number", validation: { required: true }, editable: false },
                        VehicleBalanceQty: { type: "number", validation: { required: true }, editable: false },
                        ReturnQty: { type: "number", validation: { required: true }, editable: false },
                        VehicleReturnQty: { type: "number", validation: { required: true }, editable: false },
                        ReturningQty: { type: "number", validation: { required: true }, editable: false },
                        Unload: { validation: { required: true }, editable: false },
                        Sku: { type: "number", validation: { required: true }, editable: false },
                        LoadQtyOld: { type: "number", validation: { required: true }, editable: false },
                        AllocationQtyTotal: { type: "number", validation: { required: true }, editable: false },
                        StockTotalId: { type: "number", validation: { required: true }, editable: false },
                        RecordId: { type: "number", validation: { required: true }, editable: false }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/load_stock/load_stock.asmx/GetProduct", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { allocDate: alloDate, distributorId: distributorId, divisionId: divisionId, pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        $("#div_loader").hide();
                        if (textStatus == "success") {
                            var entityGrid = $("#StockGrid").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function loadproductlist_adjusment(alloDate, distributorId, divisionId) {
    var take_grid = $("#MainContent_hfPageIndex").val();
    $("#div_loader").show();
    $("#hdnSelectedAllocDate").val(alloDate);

    //check for conditions to enable/disable Unload link
    //check for conditions to enable/disable Load link

    var url = "javascript:unloadStock('#=StockHeaderId#','#=CatalogCode#','#=CatalogName#','#=AllocQty#','#=LoadQty#','#=BalanceQty#','#=ReturnQty#','#=LoadQtyOld#','#=VehicleBalanceQty#','#=VehicleReturnQty#');"
    url = "<a href=\"" + url + "\">Unload</a>";

    var load_url = "javascript:loadStock('#=StockHeaderId#','#=CatalogCode#','#=CatalogName#','#=AllocQty#','#=LoadQty#','#=BalanceQty#','#=Sku#');"
    load_url = "<a href=\"" + load_url + "\">Load</a>";

    $("#StockGridAdjusment").kendoGrid({
        height: 350,
        selectable: true,
        columns: [
            { field: "RecordId", title: "#", width: "35px", editable: false },
            { field: "StockHeaderId", title: "Stocke Header ID", width: "100px", editable: false, hidden: true },
            { field: "StockDetailId", title: "Stocke Detail ID", width: "100px", editable: false, hidden: true },
            { field: "CatalogCode", title: "Code", width: "50px", editable: false, hidden: false },
            { field: "CatalogName", title: "Product", width: "300px", editable: false },
            { field: "AllocationQtyTotal", title: "As At Date Qty.", width: "150px", format: "{0:n1}", editable: false, hidden: false },
            { field: "AllocQty", title: "Physical Qty.", width: "150px", format: "{0:n1}", editor: AllocQtyEditor, editable: true },
            {
                field: "AdjustQty", title: "Ajdust Qty.", width: "150px", format: "{0:n1}", editable: false,
                editor: function (cont, options) {
                    $("<span>" + options.model.AdjustQty + "</span>").appendTo(cont);
                }
            },
            { field: "LoadQty", title: "Load Qty(Mil)", format: "{0:n1}", editor: LoadQtyEditor, editable: false, hidden: true },
            { field: "BalanceQty", title: "Allocated Balance Qty(Mil)", format: "{0:n1}", editor: LoadQtyEditor, editable: false, hidden: true },
            { field: "VehicleBalanceQty", title: "Vehicle Balance Qty(Mil)", format: "{0:n1}", editable: false, hidden: true },
            { field: "ReturnQty", title: "Allocated Return Qty(Mil)", format: "{0:n1}", editable: false, hidden: true },
            { field: "VehicleReturnQty", title: "Vehicle Return Qty(Mil)", format: "{0:n1}", editable: false, hidden: true },
            { field: "ReturningQty", title: "Returning Qty(Mil)", format: "{0:n1}", editable: false, hidden: true },
            { field: "Sku", title: "SKU", format: "{0:n1}", editable: false, hidden: true },
            { field: "LoadQtyOld", title: "LoadQtyOld", format: "{0:n1}", editable: false, hidden: true },
            { field: "StockTotalId", title: "StockTotalId", width: "150px", editable: false, hidden: true },
            { template: load_url, width: "50px", hidden: true },
            { template: url, width: "60px", hidden: true }
        ],
        editable: true, // enable editing
        save: function (data) {
            var AjQty = 0;

            AjQty = data.values.AllocQty - data.model.AllocationQtyTotal;
            if (data.values.AllocQty == null) {
                data.model.set("AllocQty", 0);
            }

            var test = data.model.set("AdjustQty", AjQty);
        },
        pageable: true,
        height: 400,
        sortable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 100,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        StockHeaderId: { type: "number", validation: { required: true } },
                        StockDetailId: { type: "number", validation: { required: true } },
                        CatalogCode: { editable: false, validation: { required: true } },
                        CatalogName: { editable: false, validation: { required: true } },
                        AllocQty: { type: "number", validation: { required: true } },
                        AdjustQty: { type: "number", validation: { required: false } },
                        LoadQty: { type: "number", validation: { required: true }, editable: false },
                        BalanceQty: { type: "number", validation: { required: true }, editable: false },
                        VehicleBalanceQty: { type: "number", validation: { required: true }, editable: false },
                        ReturnQty: { type: "number", validation: { required: true }, editable: false },
                        VehicleReturnQty: { type: "number", validation: { required: true }, editable: false },
                        ReturningQty: { type: "number", validation: { required: true }, editable: false },
                        Unload: { validation: { required: true }, editable: false },
                        Sku: { type: "number", validation: { required: true }, editable: false },
                        LoadQtyOld: { type: "number", validation: { required: true }, editable: false },
                        AllocationQtyTotal: { type: "number", validation: { required: true }, editable: false },
                        StockTotalId: { type: "number", validation: { required: true }, editable: false },
                        RecordId: { type: "number", validation: { required: true }, editable: false }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/load_stock/load_stock.asmx/GetProductForStockAdjustment", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { allocDate: alloDate, distributorId: distributorId, divisionId: divisionId, pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        $("#div_loader").hide();
                        if (textStatus == "success") {
                            var entityGrid = $("#StockGridAdjusment").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function loadDistributor(distId, selectedMonth) {
    $("#div_loader").show();
    $("#DistributorGrid").kendoGrid({
        height: 350,
        selectable: true,
        columns: [
            { field: "DistributorId", title: "Distributor ID", width: "100px", editable: false, hidden: true },
            { field: "TargetId", title: "Target ID", width: "100px", editable: false, hidden: true },
            { field: "Name", title: "Distributor", width: "280px", editable: false },
            { field: "Target", title: "Target Qty.(Mil)", width: "100px", format: NUMBER_FORMAT_THREE_DECIAML, editor: AllocQtyEditor, editable: true },
            { field: "CreatedDate", width: "150px", title: "CreatedDate", value: Date(), format: DATE_FORMAT, hidden: true },
            { field: "LastModifiedDate", width: "150px", title: "LastModifiedDate", value: Date(), format: DATE_FORMAT, hidden: true },
            { field: "EffStartDate", width: "150px", title: "EffStartDate", value: Date(), format: DATE_FORMAT, hidden: true },
            { field: "EffEndDate", width: "150px", title: "EffEndDate", value: Date(), format: DATE_FORMAT, hidden: true }
        ],

        editable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 200,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        TargetId: { type: "number", validation: { required: true } },
                        DistributorId: { type: "number", validation: { required: true } },
                        Name: { editable: false, validation: { required: true } },
                        Target: { type: "number", validation: { required: true } },
                        CreatedDate: { type: "date", validation: { required: true } },
                        LastModifiedDate: { type: "date", validation: { required: true } },
                        EffStartDate: { type: "date", validation: { required: true } },
                        EffEndDate: { type: "date", validation: { required: true } }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetDistributorTargetById", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { DistributorId: distId, selectedMonth: selectedMonth },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            //loadMarketTargets(distId,startdate,enddate);
                            loadMarketTargets(distId, selectedMonth);
                            $("#div_loader").hide();
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }

        }
    });
}

function LoadQtyEditor(container, options) {
    $('<input type="text" name="LoadQty" data-text-field="LoadQty" data-value-field="LoadQty" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoNumericTextBox({
            format: "{0:n2}",
            change: function changeallocated() {
                settingallocationgrid(options.model.uid, options.field);
            }
        });
}

function AllocQtyEditor(container, options) {
    $('<input type="text" name="AllocQty" data-text-field="AllocQty" data-value-field="AllocQty" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoNumericTextBox({
            format: "{0:n2}",
            change: function changeallocated() {
                settingallocationgrid(options.model.uid, options.field);
                //break;
            }
        });
}

function settingallocationgrid(uid,field) {
     var model = $("#StockGrid").data("kendoGrid").dataSource.getByUid(uid);

     if(model!=undefined) {
          model.fields.BalanceQty.editable=true;
          if (model.LoadQty <= model.AllocQty)
          {
             var balance = model.AllocQty - model.LoadQty;
             model.set("BalanceQty", balance);
          }
          else
          {
             model.set("LoadQty", 0);
          }
          model.fields.BalanceQty.editable=false;
      }
}

function OpenOpportunityLeadCustomer(leadstage, custcode, enduser, leadid) {
    if (leadstage == 'Customer') {
        window.location = '../../lead_customer/transaction/customer_entry.aspx?custid=' + custcode + "&ref=opp&fm=opp";
    }
    else if (leadstage == 'End User') {
        window.location = '../../lead_customer/transaction/EndUser_entry.aspx?custid=' + custcode + '&eduid='+enduser+'&ref=opp&fm=opp'
        //window.location = '../../lead_customer/transaction/enduser_entry.aspx?enduserid=' + enduser + "&ref=opp&fm=opp";
    }
    else {
        window.location = '../../lead_customer/transaction/leadentry.aspx?leadid=' + leadid + "&ref=opp&fm=opp";
    }
}

function OpportunityGrid_onchange() {
    $('#div_amount_sum').html($('#div_amount_max').html());
    $('#div_units_sum').html($('#div_units_max').html());
    $('#div_tonnes_sum').html($('#div_tonnes_max').html());
    $('#div_rowCount_sum').html($('#div_rowCount_max').html());
}

var gridclientoppertunityindex = 0
function loadClientOpportunityGrid(pipelineStageId) {
    var take_grid = $("#MainContent_hfPageIndex").val();
    $("#ClientOpportunityGrid").html("");
    $("#ClientOpportunityGrid").kendoGrid({
        height: 150,
        columns: [
            { field: "OpportunityOwner", title: "Client Type" },
            { field: "Count", title: "No. of Opportunities", width: "120px", attributes: { style: "text-align: right" }, headerTemplate: "<div style =\"text-align: right;\">No. of Opportunities</div>" },
            { field: "Value", title: "Total($)", format: "{0:n2}", attributes: { style: "text-align: right" }, headerTemplate: "<div style =\"text-align: right;\">Total($)</div>" },
            { field: "Units", title: "Total(Units)", format: "{0:n2}", attributes: { style: "text-align: right" }, headerTemplate: "<div style =\"text-align: right;\">Total(Units)</div>" },
            { field: "Tonnes", title: "Total(Tonnes)", format: "{0:n2}", attributes: { style: "text-align: right" }, headerTemplate: "<div style =\"text-align: right;\">Total(Tonnes)</div>" }
        ],
        editable: false,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        OpportunityOwner: { validation: { required: true } },
                        Count: { type: "number", validation: { required: true } },
                        Value: { type: "number", validation: { required: true } },
                        Units: { type: "number", validation: { required: true } },
                        Tonnes: { type: "number", validation: { required: true } }
                    }
                }
            },
            transport: {
                read: {
                    url: "../../service/pipelines_stage/pipelines_stage_service.asmx/GetOppDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pipelineStageId: pipelineStageId, status: "1", clientType: "", pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            var entityGrid = $("#ClientOpportunityGrid").data("kendoGrid");
                            if ((take_grid != '') && (gridoppertunityindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridoppertunityindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function RemindersDataSource(type) {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/GetRemindersForHome",
                contentType: 'application/json; charset=utf-8',
                datatype: "json",
                data: function () {
                    return {
                        type: type
                    }
                } 
            },
            parameterMap: function (data, operation) {
                if (operation != "read") {
                    return JSON.stringify({ products: data.models })
                } else {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }, 
        serverFiltering: true,
        serverSorting: true,
        serverPaging: true,
        pageSize: 10,
        schema: {
            data: "d.Data",
            total: "d.Total",
            model: { // define the model of the data source. Required for validation and property types.
                //id: "SourceId",
                fields: {
                    LeadName: { validation: { required: true } },
                    Subject: { validation: { required: true } },
                    StartDate: { type: "Date", format: DATE_FORMAT },
                    EndDate: { type: "Date", format: DATE_FORMAT },
                    Priority: { validation: { required: true } },
                    StatusDesc: { validation: { required: true } }
                }
            }
        }
    });

    return dataSource;
}

function DisplayRemindersPast() {
    var dataSource = RemindersDataSource('Past');
    $("#reminders_listView_past").html("");
    $("#reminders_listView_past").kendoGrid({

        columns: [
            {
                template: kendo.template($("#reminders_template").html())
                , headerAttributes: {
                    style: "display: none"
                }
            }
        ],
        dataBound: pendingreminder_dataBound,
        editable: false, // enable editing
        pageable: true,
        sortable: false,
        filterable: false,
        dataSource: dataSource
    });
}

function pendingreminder_dataBound(e) {
    var grid = $('#reminders_listView_past').data('kendoGrid');
    $("div#reminders_listView_past .k-grid-header").hide();
    if (grid.dataSource == null || grid.dataSource._total == 0) {
        $("div#reminders_listView_past div.k-grid-content").css("height", "35px");
    }
    else {
        $("div#reminders_listView_past div.k-grid-content").css("height", "465px");
    }
}

function DisplayRemindersToday() {
    var dataSource = RemindersDataSource('Today');
    $("#reminders_listView").html("");
    $("#reminders_listView").kendoGrid({

        columns: [
            { //template: '<div class="product"><div class="container"> <h3><a href="javascript:OpenLeadCusttomerActivity(\'#:ActivityID#\',\'#:CustCode#\',\'#:LeadId#\')">#:LeadName#</a></h3><h4>#:Subject#</h4> <p><span>From :</span> #:kendo.toString(StartDate, "yyyy/MMM/dd" )# - #:kendo.toString(EndDate, "yyyy/MMM/dd" )#</p> <p><span>Priority :</span> #:Priority#     <span>Status :</span> #:StatusDesc#</p> </div></div>'
                template: kendo.template($("#reminders_template").html())
                , headerAttributes: {
                    style: "display: none"
                }
            }
        ],
        dataBound: remindergrid_dataBound,
        editable: false, // enable editing
        pageable: true,
        sortable: false,
        filterable: false,
        dataSource: dataSource
    });
}

function remindergrid_dataBound(e) {
    var grid = $('#reminders_listView').data('kendoGrid');
    $("div#reminders_listView .k-grid-header").hide();
    if (grid.dataSource == null || grid.dataSource._total == 0) {
        $("div#reminders_listView div.k-grid-content").css("height", "35px");
    }
    else {
        $("div#reminders_listView div.k-grid-content").css("height", "465px");
    }
}

function fileValidator() {
    alert("Invalid File Format");
}

function OpenLeadCusttomerActivity(actid, custid, leadid) {
    if (custid == null || custid == '') {
        window.location = ROOT_PATH +'activity_planner/transaction/activityentry.aspx?leadid=' + leadid + '&activityId='+actid + '&fm=home';
    } else {
        window.location = ROOT_PATH +'activity_planner/transaction/activityentry.aspx?custid=' + custid + '&activityId='+actid + '&fm=home';
    }
}


function myLiveEventHandler(event) {
    if (event.handled !== true) {
        alert("in");
        DisplayOutstandingActivity('2', '10');
        event.handled = true;
    }
    return false;
}

var iddd = -1;

function OutstandingActivityDataSource() {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/GetOutstandingActiesForHome",
                contentType: 'application/json; charset=utf-8',
                datatype: "json"
            }, //read
            parameterMap: function (data, operation) {
                return JSON.stringify(data);
            }
        }, // transport
        serverFiltering: true,
        serverSorting: true,
        serverPaging: true,
        pageSize: 10,
        schema: {
            data: "d.Data",
            total: "d.Total",
            model: { // define the model of the data source. Required for validation and property types.
                //id: "SourceId",
                fields: {
                    StartDate: { type: "Date", format: DATE_FORMAT },
                    EndDate: { type: "Date", format: DATE_FORMAT }
                }
            }
        } // schema
    });

    return dataSource;
}

function DisplayOutstandingActivity(startindex, rowcount) {
    var dataSource = OutstandingActivityDataSource();
    $("#listView_outstanding_activity").html("");
    $("#listView_outstanding_activity").kendoGrid({

        columns: [
            {
                template: kendo.template($("#outstanding_template").html())
                , headerAttributes: {
                    style: "display: none"
                }
            }
        ],
        dataBound: function () {
            $("div#listView_outstanding_activity .k-grid-header").hide();
        },
        editable: false, // enable editing
        pageable: true,
        sortable: false,
        filterable: false,
        dataSource: dataSource//,
        //rowTemplate: kendo.template($("#outstanding_template").html())
    });
}

function OpportunitiesDataSource() {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                type: "POST",
                url: ROOT_PATH + "service/pipelines_stage/pipelines_stage_service.asmx/GetOpportunitiesForHome",
                contentType: 'application/json; charset=utf-8',
                datatype: "json"
            }, //read
            parameterMap: function (data, operation) {
                return JSON.stringify(data);
            }
        }, // transport
        serverFiltering: true,
        serverSorting: true,
        serverPaging: true,
        pageSize: 10,
        schema: {
            data: "d.Data",
            total: "d.Total",
            model: { // define the model of the data source. Required for validation and property types.
                //id: "SourceId",
                fields: {
                    CloseDate: { type: "Date", format: DATE_FORMAT }
                }
            }
        } // schema
    });

    return dataSource;
}

function DisplayOpportunities() {
    var dataSource = OpportunitiesDataSource();
    $("#listView").html("");
    $("#listView").kendoGrid({
        columns: [
            {
                template: kendo.template($("#opportunities_template").html()),
                headerAttributes: {
                    style: "display: none"
                }
            }
        ],
        dataBound: function () {
            $("div#listView .k-grid-header").hide();
        },
        editable: false, // enable editing
        pageable: true,
        sortable: false,
        filterable: false,
        dataSource: dataSource//,
        //rowTemplate: kendo.template($("#outstanding_template").html())
    });
}


function OpenHomeOpportunity(oppid, custid, leadid) {
    if (custid == null || custid == '') {
        window.location = ROOT_PATH +'pipelines_stage/transaction/opportunityentry.aspx?fm=home&leadid=' + leadid + '&oppid='+oppid;
    } else {
        window.location = ROOT_PATH +'pipelines_stage/transaction/opportunityentry.aspx?fm=home&custid=' + custid + '&oppid='+oppid;
    }
}

function DisplayMessages() {
    $("#message_grid").html("");
    $("#message_grid").kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetRepTargetsByDistributor",
                    contentType: "application/json; charset=utf-8",
                    type: "POST"
                }
            },
            schema: {
                data: "d.Data"
            },
            group: {
                field: "RepName", aggregates:
                              [
                                  { field: "TargetMonthText", aggregate: "count" },
                                  { field: "StickTarget", aggregate: "count",template:'<div>dfdf</div>' }
                              ]
            },
            aggregate: [
                          { field: "TargetMonthText", aggregate: "count" },
                          { field: "StickTarget", aggregate: "count" }
                       ]
        },
        dataBound: function () {
            $("div#message_grid .k-grid-header").hide();
        },
        columns: [
                            { field: "TargetMonthText"},
                            { field: "StickTarget", title: '<div style=\'display: none\;></div>', groupHeaderTemplate: "Total Customers: #= value #" }
                 ]
    });
}

function DisplayHomeRep() {
    $("#rephome_grid").html("");
    $("#rephome_grid").kendoGrid({
        dataSource: {
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetRepTargetsByDistributor",
                    contentType: "application/json; charset=utf-8",
                    type: "POST"
                }
            },
            schema: {
                data: "d.Data"
            },
            group: {
                field: "State", aggregates:
                              [
                                  { field: "Name", aggregate: "count" },
                                  { field: "NewCustomers", aggregate: "count",template:'<div>dfdf</div>' }
                              ]
            },
            aggregate: [
                          { field: "Name", aggregate: "count" },
                          { field: "NewCustomers", aggregate: "count" }
                       ]
        },
        dataBound: function () {
            $("div#message_grid .k-grid-header").hide();
        },
        columns: [
                            { field: "Name"},
                            { field: "NewCustomers", title: '<div style=\'display: none\;></div>', groupHeaderTemplate: "Total Customers: #= value #" }
                 ]
    });
}

function DisplayAvailabilityGraph() {

    var chart = new Highcharts.Chart({                
   chart: {renderTo: 'div_content',
   width:820,
   zoomType: 'xy', marginBottom: 100 },                
   title: {                    
   text: 'SALES SUMMARY'
   },                
   subtitle: {                   
    text: ''                
    },                
    xAxis: [{ categories: ['Week1','Week2','Week3','Week4','Week5','December','November','October'] }], 
    yAxis: [{                    
     labels: {                       
      enabled: false,                       
       formatter: function () {                         
          return this.value ;                      
            },                      
              style: {                          
                color: '#4572A7'                      
                  }                  
                    },                  
                      title: {                     
                         text: ' ',                      
                          style: {                       
                               color: '#4572A7'                    
                                   }
                                      }                 
                                         ,opposite: true             
                                            }, {
                                                title: {
                                                    text: 'No.',style: {color: '#4572A7'}
                                                },
                                                labels: {
                                                            formatter: function () {
                                                                    return this.value;
                                                            },
                                                style: {   
                                                            color: '#4572A7'
                                                       }},
                                                opposite: false
                                                }],
                                                tooltip: {
                                                        formatter: function () {
                                                            if(this.series.name == '1'){
                                                                return ('Actual Activities : ' + this.x + ',' + this.y );
                                                            }
                                                            else{
                                                                return ('Planned Activities : ' +this.series.name + ',' + this.y)
                                                            }}},
                                                legend: {                    
                                                        align: 'center',
                                                        x: 0,
                                                        verticalAlign: 'bottom',
                                                        y: 15,                    
                                                        floating: true,                    
                                                        backgroundColor: '#FFFFFF'                
                                                },                
                                                plotOptions: {                    
                                                    column: {                         
                                                        allowPointSelect: true,                         
                                                        cursor: 'pointer',                         
                                                        point: {                             
                                                            events: {                                 
                                                                click: function (e) {var n=this.series.name.split('-');                                      
                                                                GridColumnAnalysisGraph(this.x,this.series.name);                                  
                                                            }                             
                                                        }                         
                                                    },                        
                                                    stacking: 'normal',                        
                                                    dataLabels: {                            
                                                        enabled: true,                            
                                                        color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'                        
                                                    }                    
                                                },
                                                line: {                         
                                                    allowPointSelect: true,                         
                                                    cursor: 'pointer',                         
                                                    point: {                             
                                                        events: {                                 
                                                            click: function (e) {                                       
                                                                GridLineAnalysisGraph(this.x);                                  
                                                            }                             
                                                        }                         
                                                    },                    
                                                }                
                                            },                
                                            series: 
                                            [
                                                { name: '1', color: '#4572A7',type: 'line', yAxis: 1,showInLegend: false, data: [5,24,17,0,4,5,0,146]}
                                            ]});  
}

function DisplayProductPieChart() {
                var chart = new Highcharts.Chart({ chart: { renderTo: 'div_content', plotBackgroundColor: null, plotBorderWidth: null, plotShadow: false },
                     title: { text: 'Stock Summary' },
                     tooltip: { formatter: function () 
                     {
                         return '<b>' + this.point.rowcount + ',' + this.point.y + '(' + Math.round(this.percentage * 100) / 100 + ' %)</b>';
                     } 
                 },
                 plotOptions: { pie: { allowPointSelect: true, cursor: 'pointer', point: { events: { click: function (e) { loadOpportunityGrid($('#MainContent_HiddenFieldPipelineStageId').val(), this.name); } } }, dataLabels: { enabled: true, color: '#000000', connectorColor: '#000000', formatter: function () { if (this.point.des == '') { return '<b>' + this.point.rowcount + ',' + this.point.y + '</b>'; } else { return '<b>' + this.point.des + '</b>'; } } }, showInLegend: true} }, series: [{ type: 'pie', name: 'state', rowcount: 'rowcount', des: 'des', data: [{ name: 'JPGL', y: 30, rowcount: '1', des: 'JPGL', color: '#2e5b94' }, { name: 'B&H', y: 3, rowcount: '2', des: 'B&H', color: '#e38928' }, { name: 'DH L', y: 25, rowcount: '3', des: 'DH L', color: '#892a27'}]}]
                 });

}

function DisplayAppointments(startindex, rowcount) {
    //Load Panel Bar
    $("#panelbarcontentappointments").kendoPanelBar({
        expandMode: "single"
    });
}



function GetColorCode(ActivityCode) {
    var root = "../process_forms/processmaster.aspx?fm=activityplanner&type=activityType&ActivityCode=" + ActivityCode + "&title=" + $("div.rsAdvTitle").html();

    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            //var EditWrapper = 
            $("div.rsAdvTitle").html(msg);
            //                 EditWrapper =  EditWrapper + msg ; //  "</div><div id='div_title' style='background-color:#92d050'>"+text+"</div>";
            //                 $("div.rsAdvTitle").html(EditWrapper);
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg; // "Sorry there was an error: ";
                //$("#" + tagertdiv).html(msg);
            }
        }
    });
}


function ConfirmApproval(objMsg) {
    if (confirm(objMsg)) {
        $.ajax({
            type: "POST",
            url: "activity_scheduler.aspx/SendToAppointments",
            data: "",
            cache: false,
            contentType: "application/json",
            dataType: "json",
            success: function (msg) {
                if (msg != "") {
                    //$("#MainContent_div_message").css("display", "block;");
                    //$("#MainContent_div_message").css("padding", "4px 4px 0 4px;");
                    
                    $("#MainContent_div_message").html(msg.d);
                    $("#MainContent_div_message").show();
                    //window.location = "../../activity_planner/transaction/activity_scheduler.aspx?flag=sent";
                }
            }
        });
    }
    else {
        //window.location = "../../activity_planner/transaction/activity_scheduler.aspx?flag=cancel";
    }
}

function SendToCalendar(id, start, end) {
    var start_date = start.getDate() + "-" + (start.getMonth() + 1) + "-" + start.getFullYear() + " " + start.getHours() + ":" + start.getMinutes();
    var end_date = end.getDate() + "-" + (end.getMonth() + 1) + "-" + end.getFullYear() + " " + end.getHours() + ":" + end.getMinutes();

    var root = "../process_forms/processmaster.aspx?fm=activityplanner&type=SendToAppointments&id=" + id + "&start=" + start_date + "&end=" + end_date;

    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg != "") {
                $("#MainContent_div_message").html(msg);
                document.getElementById('MainContent_div_message').style.display = "block";
                hideStatusDiv("MainContent_div_message");
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
}

function OpenContact(id) {
    var root = "../process_forms/processmaster.aspx?fm=activityplanner&type=OpenContact&id=" + id;

    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg != "") {
                window.location = msg;
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
}

function SetMessage(objMsg) {
    $("#MainContent_div_message").html(objMsg);
    $("#MainContent_div_message").css("padding", "4px 4px 0");
    $("#MainContent_div_message").show();
}

function loadEntryPageTabs(page, tabArray,selectedtab) {
    var tabstrip = $("#tabstrip").kendoTabStrip().data("kendoTabStrip");

    var getEffects = function () {
        return (("expand:vertical ") + ("fadeIn"));
    };

    for (var i = 0; i < tabArray.length; i++) {
        tabstrip.append({ text: tabArray[i][0], contentUrl: ROOT_PATH + tabArray[i][1] });
        tabstrip.select(selectedtab);
    }

    $("li.k-link").click(function(){
        
    });

//    var ts = $("#tabstrip").data("kendoTabStrip");
//    ts.tabGroup.on('click','li',function(e){        
//        //$('body,html').animate({ scrollTop: 800}, 800);
//        $(window).scrollTop($(document).height());
//    });

       
}

function reportloadcustomer(id, targetform, tagertdiv,radiochecked) {
    var url = ROOT_PATH + "lead_customer/process_forms/processmaster.aspx?fm=" + targetform + "&type=report" + "&id=" + id + "&tdiv=" + tagertdiv + "&istonne=" + radiochecked;

    $("#" + tagertdiv).show();
    $("#" + tagertdiv).html('loading.....');
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {

            $("#" + tagertdiv).html(msg, id);
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg; // "Sorry there was an error: ";
                $("#" + tagertdiv).html(msg);
            }
        }
    });
}

function LoadCustomerSalesGrid(customercode) {
//    $("#div_sales_grid").kendoGrid({
//        height: 150,
//        columns: [
//                    { field: "SOrderNo", title: "Order No" },
//                    { field: "DateRequired", title: "Req. Date" },
//                    { field: "CatlogCode", title: "Cat. Code", format: "{0:n2}" },
//                    { field: "Price", title: "Price", format: "{0:n2}" },
//                    { field: "Description", title: "Description" },
//                    { field: "OrderDate", title: "Order Date" }
//                ],
//        editable: false,
//        dataSource: {
//            serverSorting: true,
//            serverPaging: true,
//            serverFiltering: true,
//            pageSize: 20,
//            schema: {
//                data: "d.Data",
//                total: "d.Total",
//                model: {
//                    fields: {
//                        SOrderNo: { type: "String" },
//                        DateRequired: { type: "DateTime" },
//                        CatlogCode: { type: "number" },
//                        Price: { type: "number" },
//                        Description: { type: "number" },
//                        OrderDate: { type: "DateTime" }
//                    }
//                }
//            },
//            transport: {
//                read: {
//                    url: ROOT_PATH + "service/lead_customer/customer_entry_Service.asmx/GetSalesHistoryDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
//                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
//                    data: { customercode: customercode },
//                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
//                },
//                parameterMap: function (data, operation) {
//                    data = $.extend({ sort: null, filter: null }, data);
//                    return JSON.stringify(data);
//                }
//            }
//        }
//    });
$("#div_sales_grid").html("");
    $("#div_sales_grid").kendoGrid({
        columns: [
                    { field: "SOrderNo", title: "Order No", width: "60px" },
                    { field: "DateRequired", title: "Req. Date", width: "80px",format: DATE_FORMAT2},
                    { field: "CatlogCode", title: "Cat. Code", format: "{0:n2}", width: "65px"},
                    { field: "Price", title: "Price", format: "{0:n2}", width: "55px" },
                    { field: "Description", title: "Description", width: "150px" },
                    { field: "OrderDate", title: "Order Date",format: DATE_FORMAT2, width: "80px" }
                ],
        sortable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: {
                    fields: {
                        SOrderNo: { type: "string" },
                        DateRequired: { type: "date" },
                        CatlogCode: { type: "string" },
                        Price: { type: "number" },
                        Description: { type: "string" },
                        OrderDate: { type: "date" }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/customer_entry_Service.asmx/GetSalesHistoryDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.svc service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { customercode: customercode },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for svc
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

function loadLeadActivityGrid(targetleadactivitygrid) {
    $("#"+targetleadactivitygrid).html("");
    $("#"+targetleadactivitygrid).kendoGrid({
        //height: 300,
        columns: [

                        { field: "ActivityID", width: "85px", hidden: true },

                        { template: '<a href="javascript:OpenLeadActivity(\'#=ActivityID#\')">#=Subject#</a>',
                            field: "Subject", width: "250px"
                        },
                        { field: "StartDate", title: "Start Date", width: "100px", format:DATE_FORMAT },
                        { field: "EndDate", title: "End Date", width: "80px", format: DATE_FORMAT },
                        { field: "StatusDescription", title: "Status", width: "80px" },

                        { field: "AssignedTo", title: "Assigned To", width: "80px" },
                        { field: "SentMail", title: "Sent Mail", width: "80px" },
                        { field: "PriorityDescription", title: "Priority", width: "80px" },
                        { field: "ActivityTypeDescription", title: "Type", width: "80px" },
                        { field: "AppointmentID", title: "Type", width: "80px", hidden: true }


                ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: false,
        dataBound: LeadActivityGrid_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        ActivityID: { validation: { required: true} },
                        Subject: { validation: { required: true} },
                        StartDate: { type: "Date", editable: true, nullable: true },
                        EndDate: { type: "Date", validation: { required: true} },
                        StatusDescription: { validation: { required: true} },
                        AssignedTo: { validation: { required: true} },
                        SentMail: { validation: { required: true} },
                        PriorityDescription: { validation: { required: true} },
                        ActivityTypeDescription: { validation: { required: true} },
                        AppointmentID: { validation: { required: true }, type: "number" }

                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH+ "service/lead_customer/lead_entry_Service.asmx/GetActivityDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            change: emptyGridFix
        }
    });
}


function emptyGridFix() {
    if (this.total() > 0) return; // continue only for empty grid
    var msg = this.options.emptyMsg;
    if (!msg) msg = 'There are no records to display'; // Default message
    $(this.options.table).parent().html('<div class="grid-no-data">'+msg+'</div>')
}



function LeadActivityGrid_onDataBound(arg) {
    var dataView = this.dataSource.view();
    for (var i = 0; i < dataView.length; i++) {
        var uid = dataView[i].uid;
        if (dataView[i].AppointmentID == 0) {
            //document.getElementById(rowID).style.backgroundColor = '';
        }
        else {
            $("#LeadActivityGrid tbody").find("tr[data-uid=" + uid + "]").css('background-color', '#d8bfd8');
        }
    }
}

function OpenLeadActivity(activityId) {
    var leadid = $("input[id=MainContent_HiddenFieldLeadId]").val();
    window.location = "../../activity_planner/transaction/activityentry.aspx?activityid="
                        + activityId + "&leadId=" + leadid;
}


function loadLeadContactPersonGrid() {
    $("#LeadContactPersonGrid").html("");
    $("#LeadContactPersonGrid").kendoGrid({
        height: 300,
        columns: [

                        { field: "ContactType", width: "85px", hidden: true },
                        { field: "Origin", width: "85px", hidden: true },
                        { field: "ContactPersonID", width: "85px", hidden: true },

                        { field: "KeyContactChecked", title: " ", width: "30px", template: '<input type="checkbox"/>' },

                        { field: "Title", title: "Title", width: "80px" },
                        { template: '<a href="javascript:OpenContactPerson(\'#=ContactPersonID#\')">#=FirstName#</a>',
                            field: "First Name", width: "250px"
                        },

                        { field: "LastName", title: "Last Name", width: "120px" },
                        { field: "Position", title: "Position", width: "100px" },
                        { field: "Telephone", title: "Telephone", width: "80px" },
                        { field: "Mobile", title: "Mobile", width: "80px" },
                        { field: "EmailAddress", title: "Email Address", width: "80px" }

                ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: false,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        ContactType: { validation: { required: true} },
                        Origin: { validation: { required: true} },
                        ContactPersonID: { editable: true, nullable: true },
                        KeyContactChecked: { validation: { required: true} },
                        Title: { validation: { required: true} },
                        FirstName: { validation: { required: true} },
                        LastName: { validation: { required: true} },
                        Position: { validation: { required: true} },
                        Telephone: { validation: { required: true} },
                        Mobile: { validation: { required: true} },
                        EmailAddress: { validation: { required: true} }

                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH+ "service/lead_customer/lead_entry_Service.asmx/GetContactPersonDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },            
                change: emptyGridFix
        }
    });
}

function OpenContactPerson(contactPersonId) {
    var leadid = $("input[id=MainContent_HiddenFieldLeadId]").val();
    window.location = ROOT_PATH+"lead_customer/transaction/contact_person_entry.aspx?leadid="
                        + leadid + "&personid=" + contactPersonId;
}

function OpenCustomerContactPerson(Origin,ContactType) {
    var custid = $("input[id=HiddenFieldContactPerson_CustCode]").val();
    window.location = ROOT_PATH+"lead_customer/transaction/contact_person_entry.aspx?custid="
                        + custid + "&Origin=" + Origin + "&ContactType=" + ContactType;
}

function OpenEndUserContactPerson(contactPersonId) {
    var custid = $("input[id=MainContent_HiddenFieldCustomerCode]").val();
    var eduid = $("input[id=MainContent_HiddenEndUserCode]").val();
    window.location = ROOT_PATH+"lead_customer/transaction/contact_person_entry.aspx?custid="
                        + custid +"&eduid="+eduid+"&personid=" + contactPersonId;
}



function loadLeademailGrid(cust_id,enduser_id) {
    $("#LeademailGrid").html("");
    $("#LeademailGrid").kendoGrid({
        height: 200,
        columns: [

                        { field: "ImportMailID", width: "85px", hidden: true },
                        { field: "EmailID", width: "80px", hidden: true },

                        { template: '<a href="javascript:loadEmail(\'#=EmailID#\')"><img src=' + ROOT_PATH + 'assets/images/email_attac.png /></a>',
                            field: "From", width: "25px"
                        },
                        { field: "From", title: "Email Address", width: "100px" },
                        { field: "Subject", title: "Subject", width: "100px" },
                        { field: "Date", title: "Date", width: "80px", format: DATE_FORMAT },
                        { field: "FileName", title: "FileName", width: "80px", hidden: true }
                ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: false,
        //dataBound: LeadCustomerGrid_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        ImportMailID: { validation: { required: true} },
                        EmailID: { validation: { required: true} },
                        From: { validation: { required: true} },
                        Date: { type: "Date", validation: { required: true} },
                        Subject: { validation: { required: true} },
                        FileName: { validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH+ "service/lead_customer/lead_entry_Service.asmx/GetEmailDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { cust_id: cust_id},
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                }
            },            
                change: emptyGridFix
        },
        dataBound: mailGrid_dataBound
    });
}

function mailGrid_dataBound(e) {
    var grid = $('#LeademailGrid').data('kendoGrid');

    if (grid.dataSource == null || grid.dataSource._total == 0) {
        grid.content.find('div').each(function () {
            $(this).hide();
        });
        grid.content.append('<div class="grid-no-data">There are no records to display</div>');
    }
    else {
        var noDataDiv = $('.no-data', grid.content);

        if (noDataDiv != null) {
            $(noDataDiv).remove();
        }

        $('div', grid.content).each(function () {
            $(this).show();
        });
    }
}

function loadEmail(emid) {
    var url = ROOT_PATH + 'lead_customer/process_forms/processmaster.aspx?type=get&fm=leademail&emid=' + emid;
    //var url = ROOT_PATH + url;
    $("#div_loader").show();

    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            $("#div_loader").hide();
            if (msg != "") {
                $("#div_loader").hide();

                $(msg).find();

                var fname = $(msg).find("div#div_emailattach").attr('rel');

                var len = msg.indexOf("<");
                //var url = msg.substring(0, len)
                var attachments = msg.substring(len - 1, msg.length);
                //Load mail attachment.
                $("#div_mailAttachments").html(attachments);

                //Load mail content.
                url = "<iframe width=\"100%\" frameBorder=\"0\" src=\"../../docs/" + fname + "\"></iframe>";
                $("#div_mailcontent").html(url);
            }
        },
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                $("#div_loader").hide();
                var msg = msg; // "Sorry there was an error: ";
            }
        }
    });
}

function loadMailContent(contentUrl) {

    var url = ROOT_PATH + "lead_customer/process_forms/processmaster.aspx?type=get&fm=customeremailcontent&ne=" + contentUrl;
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg != "") {
                $("#div_loader").hide();
                $("#div_promt").html(msg);
                var window = $("#div_promt");


                var onClose = function () {
                    //undo.show();
                }

                if (!window.data("kendoWindow")) {
                    window.kendoWindow({
                        width: "600px",
                        title: "email",
                        close: onClose
                    });
                }

                window.data("kendoWindow").open();

            }
        },
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg; // "Sorry there was an error: ";
            }
        }
    });
}

function loadLeadOpportunityGrid(cust_id,leadid,enduserid) {
    $("#LeadOpportunityGrid").html("");
    $("#LeadOpportunityGrid").kendoGrid({   
        height: 300,
        columns: [

                        { field: "OpportunityID", width: "85px", hidden: true },

                        { template: '<a href="javascript:deleteOpportunity(\'#=OpportunityID#\',\'#=Name#\',0)">Delete</a>',
                            field: "", width: "50px"
                        },
                        { template: '<a href="javascript:OpenLeadOpportunity(\'#=OpportunityID#\',\'#=LeadID#\',\'#=CustCode#\',\'#=EndUserCode#\',\'\')">#=Name#</a>',
                            field: "Name", width: "250px"
                        },
                        { field: "CloseDate", title: "Close Date", width: "90px", format: DATE_FORMAT3 },
                        { field: "PipelineStage", title: "Pipeline Stage", width: "90px" },
                        { field: "Probability", title: "Probability (%)", attributes: { style: "text-align: right" }, width: "90px" },
                        { field: "Amount", title: "Amount", width: "80px", headerTemplate: "<div style =\"text-align: center;\">Amount</div>", attributes: { style: "text-align: right"} },
                        { field: "Units", title: "Units", width: "70px", headerTemplate: "<div style =\"text-align: center;\">Units</div>", attributes: { style: "text-align: right"} },
                        { field: "Tonnes", title: "Tonnes", width: "70px", headerTemplate: "<div style =\"text-align: center;\">Tonnes</div>", attributes: { style: "text-align: right" }, hidden: (enduserid != '' ? true : false) },
                        { field: "Description", title: "Description", width: "80px" }
                ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: false,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        OpportunityID: { validation: { required: true} },
                        Name: { validation: { required: true} },
                        CloseDate: {  type: "Date", validation: { required: true} },
                        PipelineStage: { validation: { required: true} },
                        Probability: { validation: { required: true} },
                        Amount: { validation: { required: true} },
                        Units: { validation: { required: true} },
                        Tonnes: { validation: { required: true} },
                        Description: { validation: { required: true} }

                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH+ "service/lead_customer/lead_entry_Service.asmx/GetLeadOpportunitiesWithCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { cust_id: cust_id , enduserid: enduserid},
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },            
            change: emptyGridFix
        },
        dataBound: OpportunityGrid_dataBound
    });
}

function OpportunityGrid_dataBound(e) {
    var grid = $('#LeadOpportunityGrid').data('kendoGrid');

    if (grid.dataSource == null || grid.dataSource._total == 0) {
        grid.content.find('div').each(function () {
            $(this).hide();
        });
        grid.content.append('<div class="grid-no-data">There are no records to display</div>');
    }
    else {
        var noDataDiv = $('.no-data', grid.content);

        if (noDataDiv != null) {
            $(noDataDiv).remove();
        }

        $('div', grid.content).each(function () {
            $(this).show();
        });
    }
}

function deleteOpportunity(id, OpportunityName, indexOpportunity) {
    var confirmResult = confirm("Opportunity " + OpportunityName + " will be deleted. Do you want to continue?");
    if (confirmResult == true) {
        var url = ROOT_PATH + "lead_customer/process_forms/processmaster.aspx?fm=Opportunity&type=Delete" + "&id=" + id;
        
        //$("#" + tagertdiv).html('loading.....');
        $.ajax({
            url: url,
            dataType: "html",
            cache: false,
            success: function (msg) {
                if (msg == "true") {
                    $("#MainContent_div_message").show();
                    $("#MainContent_div_message").html("Successfully Deleted.");
                    //OpportunityDelete(indexOpportunity);
                    var grid = $("#LeadOpportunityGrid").data("kendoGrid");
                    grid.dataSource.read();
                }
            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg; // "Sorry there was an error: ";
                    $("#MainContent_div_message").show();
                    $("#MainContent_div_message").html(msg);
                }
            }
        });
    }
    else {
        //alert("Not Deleted");
    }
}

//function OpenLeadOpportunity(opportunityId,leadid,eduid,custid,fm) {
function OpenLeadOpportunity(opportunityId,leadid,custid,enduserid,fm) {
    var url = ROOT_PATH + "pipelines_stage/transaction/opportunityentry.aspx?";

    if(leadid!=undefined && leadid!='' && leadid!=null && leadid!='0'){
        url += "leadid="+leadid+"&oppid=" + opportunityId;
    }
    else if (custid != undefined && custid != '' && custid != null && custid != '0' && enduserid != undefined && enduserid != '' && enduserid != null && enduserid != '0' && enduserid != 'null') {
        url += "custid="+custid+"&oppid=" + opportunityId+"&eduid="+enduserid;
    }
    else if(custid!=undefined && custid!='' && custid!=null && custid!='0'){
        url += "custid="+custid+"&oppid=" + opportunityId;
    }
    
    if(fm != '')
    {
        url += "&fm="+fm;
    }

    /*window.location = ROOT_PATH+ "pipelines_stage/transaction/opportunityentry.aspx?leadid="
                        + $("#MainContent_HiddenFieldLeadId").val() + "&oppid=" + opportunityId;   */
    
    window.location=url;
}

function loadLeaddocumentGrid() {
$("#LeadDocumentGrid").html("");
    var leadurl = ROOT_PATH + 'lead_customer/process_forms/processmaster.aspx?type=get&fm=leadentry&docid=' + "#=DocumentID#";
    $("#LeadDocumentGrid").kendoGrid({
        height: 300,
        columns: [

                        { field: "Extention", width: "85px", hidden: true },
                        { field: "TempFileName", width: "80px", hidden: true },
                        { field: "DocumentID", width: "80px", hidden: true },
                        { field: "Path", width: "80px", hidden: true },

                        { template: '<a href="javascript:deleteDocument(\'#=DocumentID#\',\'#=DocumentName#\',0)">Delete</a>',
                            field: "", width: "20px"
                        },

                        { template: '<a href="javascript:OpenLeadDocument(\'#=DocumentID#\',\'#=Extention#\',\'#=Path#\',\'lead\')">#=DocumentName#</a>',
                            field: "DocumentName", title: "Document Name", width: "200px"
                        },
                        { field: "AttachedBy", title: "Attached By", width: "100px" },
                        { field: "AttachedDate", title: "Attached Date", width: "100px", format: DATE_FORMAT3 }
                ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: false,
        //dataBound: LeadCustomerGrid_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        Extention: { validation: { required: true} },
                        TempFileName: { validation: { required: true} },
                        DocumentID: { validation: { required: true} },
                        DocumentName: { validation: { required: true} },
                        AttachedBy: { validation: { required: true} },
                        AttachedDate: { type: "Date", validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH+ "service/lead_customer/lead_entry_Service.asmx/GetDocumenDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function deleteDocument(id, DocumentName, indexDocument) {
    var confirmResult = confirm("Document " + DocumentName + " will be deleted. \nDo you want to continue?");
    if (confirmResult == true) {
        var url = ROOT_PATH + "lead_customer/process_forms/processmaster.aspx?fm=document&type=Delete" + "&id=" + id;

        //$("#" + tagertdiv).html('loading.....');
        $.ajax({
            url: url,
            dataType: "html",
            cache: false,
            success: function (msg) {
                if (msg == "1") {
                    $("#MainContent_div_message").show();
                    $("#MainContent_div_message").html("Successfully Deleted.");
                    //DocumentDelete(indexDocument);
                    var grid = $("#LeadDocumentGrid").data("kendoGrid");
                    grid.dataSource.read();
                }
            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg; // "Sorry there was an error: ";
                    $("#MainContent_div_message").show();
                    $("#MainContent_div_message").html(msg);
                }
            }
        });
    }
    else {
        alert("Not Deleted");
    }
}

function OpenLeadDocument(docid, extention, tempfilename, leadorcust) {
    var url = '';
    if ((extention != ".jpg" && extention != ".JPEG" && extention != ".JPG") && (extention != ".gif" && extention != ".GIF") && (extention != ".png" && extention != ".PNG")
            && (extention != ".txt" && extention != ".TXT")) {
        url = ROOT_PATH + "docs/" + tempfilename;
        window.location = url;
    }
    else {
        if(leadorcust=='cust'){
        url='lead_customer/process_forms/processmaster.aspx?type=get&fm=customerentry&docname='  + tempfilename +'&ext=' + extention;
        }else if(leadorcust=="lead"){
        url= 'lead_customer/process_forms/processmaster.aspx?type=get&fm=leadentry&docname='  + tempfilename +'&ext=' + extention;
        }
        loadDocument(url);
    }
}

function loadDocument(url) {
    var url = ROOT_PATH + url;
    $("#div_loader").show();
    //$("#" + tagertdiv).html('loading.....');

    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg != "") {
                $("#div_loader").hide();
                $("#div_promt").html(msg);
                var window = $("#div_promt");


                var onClose = function () {
                    //undo.show();
                }

                if (!window.data("kendoWindow")) {
                    window.kendoWindow({
                        width: "800px",
                        height: "600px", 
                        title: "Document Viewer",
                        close: onClose
                    });
                }

                window.data("kendoWindow").open();
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg; // "Sorry there was an error: ";
                // $("#" + tagertdiv).html(msg);
            }
        }
    });
}

function Leaddocumentupload() {

        $(".upload").kendoUpload({
            async: {
                saveUrl: ROOT_PATH + "lead_customer/process_forms/processmaster.aspx",
                autoUpload: true
            },
            upload: function onUpload(e) {                    
                //var uploadId = e.sender.wrapper.prevObject.attr("value");
                var LeadId =$('#MainContent_HiddenFieldLeadId').val();
                e.sender.options.async.saveUrl = ROOT_PATH + "lead_customer/process_forms/processmaster.aspx?fm=leaddocument&type=insert&leadid="+LeadId;
            },
            success: function onSuccess(e) {                
                var grid = $("#LeadDocumentGrid").data("kendoGrid");
                    grid.dataSource.read();
            }
        });
}


function loadLeadAddressGrid() {
    var LeadId =$('#MainContent_HiddenFieldLeadId').val();

            var url = "javascript:addAddress('lead_customer/process_forms/processmaster.aspx?fm=contactdetails&type=insert&leadid="+
                LeadId + " &leadadid=#=LeadAddressID#&typ=#=AddressTypeDescription#&add1=#=Address1#&add2=#=Address2#&cty=#=City#&sta=#=State#&pstcde=#=PostCode#&name=#=Name#&adcntac=#=Contact#&adtele=#=Telephone#&adfax=#=Fax#&cntry=#=Country#','');"

            url = "<a href=\"" + url + "\">#=Address1#</a>";
        $("#LeadCustAddressGrid").html("");            
        $("#LeadCustAddressGrid").kendoGrid({
            height: 300,
            columns: [
            { field: "AddressTypeDescription",  width: "85px",  hidden: true},
            { template: url,field: "Address 1", width: "250px"},
            { field: "Address2", title: "Address 2", width: "105px" },
            { field: "State", title: "State", width: "70px" },
            { field: "City", title: "City", width: "130px" },
            { field: "PostCode", title: "Post Code", width: "80px" },
            "Country"

    ],
            editable: false, // enable editing
            pageable: true,
            sortable: false,
            filterable: false,
            selectable: "single",
            columnMenu: false,
            //dataBound: LeadCustomerGrid_onDataBound,
            dataSource: {
                serverSorting: true,
                serverPaging: true,
                serverFiltering: true,
                pageSize: 10,
                schema: {
                    data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                    total: "d.Total",
                    model: { // define the model of the data source. Required for validation and property types.
                        //id: "SourceId",
                        fields: {
                            AddressTypeDescription: { validation: { required: true} },
                            Address1: { validation: { required: true} },
                            Address2: { editable: true, nullable: true },
                            State: { validation: { required: true} },
                            City: { validation: { required: true} },
                            PostCode: { validation: { required: true} },
                            Country: { validation: { required: true} }

                        }
                    }
                },
                transport: {

                    read: {
                        url: ROOT_PATH + "service/lead_customer/lead_entry_Service.asmx/GetContactAddressDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                        contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                        //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                        type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                    },
                    parameterMap: function (data, operation) {
                        if (operation != "read") {
                            return JSON.stringify({ products: data.models })
                        } else {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }
            }
        });
    }


function addAddress(targeturl, addType) {
    var url = ROOT_PATH + targeturl;
    // url = encodeurl(url);
    $("#window").html("");
    $("#div_loader").show();
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            $("#div_loader").hide();
            if (msg != "") {

//                $("#window").html(msg);                                    
//                $("#window").data("kendoWindow").open();

                var wnd = $("#window").kendoWindow({
                            title: "Add/Edit Address",
                            modal: true,
                            visible: false,
                            resizable: false
                        }).data("kendoWindow");

                        $("#window").html(msg);
                        wnd.center().open();         
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg; // "Sorry there was an error: ";
                $("#div_loader").hide();
                //$("#" + tagertdiv).html(msg);
            }
        }
    });
}


function addBankAccount(targeturl, addType) {

    if ($("#MainContent_HiddenFieldIsDistributorSaved").val() != 1) {
        var saveMsg = GetInformationMessageDiv("Please save Distributer Details to add Bank Accounts.", "MainContent_div_message");
        $('#MainContent_div_message').css('display', 'block');
        $("#MainContent_div_message").html(saveMsg);
        hideStatusDiv("MainContent_div_message");
        return;
    }
    
    var url = ROOT_PATH + targeturl;
    // url = encodeurl(url);
    $("#window").html("");
    $("#div_loader").show();
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            $("#div_loader").hide();
            if (msg != "") {

//                $("#window").html(msg);                                    
//                $("#window").data("kendoWindow").open();

                var wnd = $("#window").kendoWindow({
                            title: "Add/Edit Bank Account",
                            modal: true,
                            visible: false,
                            resizable: false
                        }).data("kendoWindow");

                        $("#window").html(msg);
                        wnd.center().open();         
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg; // "Sorry there was an error: ";
                $("#div_loader").hide();
                //$("#" + tagertdiv).html(msg);
            }
        }
    });
}


function insertAddress(targeturl) {
    //var ROOT_PATH = '/Peercore.CRM.WebCRM/'
    
    var url = ROOT_PATH + targeturl;
    $("#div_loader").show();
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            $("#div_loader").hide();

            if (msg != "") {
                                    
                    if (msg == '0') {

                        //return false;
                    }
                    else if (msg == '1') {
                        //$("#MainContent_div_message").show("fast");

                       // $('#MainContent_div_message').html('Successfully Saved.');

                        var test = "<div id='messagegreen'  class='message-green' rel='sucess'><table border='0' width='100%' cellpadding='0' cellspacing='0'>";
                        test = test + "<tr><td class='green-left'>Successfully Saved.</td><td class='green-right'><a class='close-green'><img src='"
                            + ROOT_PATH + "assets/images/icon_close_green.gif' alt='' /></a></td></tr></table></div>";

                    //                    $("#MainContent_div_message").show("fast");
                        $("#MainContent_div_message").html(test);
                        document.getElementById('MainContent_div_message').style.display = "block";

                        $("#window").data("kendoWindow").close();
                        hideStatusDiv("MainContent_div_message");

//                        var grid = $("#LeadAddressGrid").data("kendoGrid");
//                        grid.dataSource.read();

                        grid = $("#LeadCustAddressGrid").data("kendoGrid");
                        grid.dataSource.read();
//                        var url = document.location.href.substr(0, document.location.href.indexOf('&'));
//                        
//                        if (url == '') {
//                               window.location = document.location.href+ "&ref=add#tabs";        
//                        }
//                        else {
//                            url = url + "&ref=add#tabs";
//                            if (url == document.location.href)
//                                window.location.reload();
//                            else
//                                window.location = url;
//                        }
                        return true;
                    }
                                    
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg; // "Sorry there was an error: ";
                $("#div_loader").hide();
                //$("#" + tagertdiv).html(msg);
            }
        }
    });
}

function insertBankDetails(targeturl) {
    //var ROOT_PATH = '/Peercore.CRM.WebCRM/'
    
    var url = ROOT_PATH + targeturl;
    $("#div_loader").show();
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            $("#div_loader").hide();

            if (msg != "") {
                                    
                    if (msg == '0') {

                        //return false;
                    }
                    else if (msg == '1') {
                        
                        var test = GetSuccesfullMessageDiv("Bank Account Successfully Saved.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(test);
                        hideStatusDiv("MainContent_div_message");

                        $("#window").data("kendoWindow").close();

                        grid = $("#DistBankGrid").data("kendoGrid");
                        grid.dataSource.read();

                        return true;
                    }
                                    
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg; // "Sorry there was an error: ";
                $("#div_loader").hide();
                //$("#" + tagertdiv).html(msg);
            }
        }
    });
}

function showcheactivedeactiveModal(message, isconfirm, leadstage) {

        $("#save").css("display", "none");
        $("#no").css("display", "inline-block");
        $("#yes").css("display", "inline-block");

       var  wnd = $("#modalWindow").kendoWindow({
            title: "Delete confirmation",
            modal: true,
            visible: false,
            resizable: false,
            width: 300
        }).data("kendoWindow");

        $("#div_confirm_message").text(message);
        wnd.center().open();

        $("#yes").click(function () {
            if (isconfirm == '1') {
                window.location = ROOT_PATH + 'lead_customer/transaction/lead_contacts.aspx?cht=lead&ty=' + leadstage;
            }
            else {
                $("#MainContent_HiddenFieldIsDeactivate").val('1');
                document.getElementById("MainContent_buttonActiveDeactive").click();
            }
            wnd.close();
        });

        $("#no").click(function () {
            wnd.close();
        });
    }

    function showchecklistModal(div_id) {
        var msg = $(div_id).html();
        $(div_id).css("display", "block");
        $("#save").css("display", "block");
        $("#no").css("display", "none");
        $("#yes").css("display", "none");
        $("#div_confirm_message").css("display", "none");

        var wnd = $("#modalWindow").kendoWindow({
            title: "Checklist",
            modal: true,
            visible: false,
            resizable: false,
            width: 300
        }).data("kendoWindow");

        //$("#div_confirm_message").html(msg);
        wnd.center().open();

        $("#save").click(function () {
            $("#MainContent_HiddenFieldHasClickedCheckList").val('1');
            var count='0';
            var counttotal = $('#MainContent_HiddenFieldConvertCount').val();
            var isSave=0;

            $('input[type=checkbox]').each(function(){

                //If Count Started, reason for check real checkList count is, its loop the double of the real count. 
                if(count < counttotal){

                    var isChecked= $(this).is(':checked');
                    if(isChecked==false){
                        isSave = 1;
                    }
                }
                count++;
           });

           if(isSave == 0){

                if ($("#MainContent_HiddenFieldHasClickedCheckList").val() == '0') {
                    return false;
                }

                if ($("#MainContent_HiddenFieldHasClickedCheckList").val() != '0') {
                    document.getElementById("MainContent_buttonConvert").click();
                }

                if (isconfirm == '1') {
                    window.location = ROOT_PATH + 'lead_customer/transaction/lead_contacts.aspx?cht=lead&ty=' + leadstage;
                }
                else {
                    $("#MainContent_HiddenFieldIsDeactivate").val('1');
                    document.getElementById("MainContent_buttonActiveDeactive").click();
                }
                wnd.close();
            }
            else{
                $("#MainContent_HiddenFieldHasClickedCheckList").val('');
                $("#div_confirm_message").css("display", "block");
                $("#div_confirm_message").html("All checklist items must be checked.");
            }
        }); 
    }

    function showconversionMessage() {
        var msg = 'Request Will be sent to Convert this Contact to a Customer. <br /> Please click yes to continue';

        $("#save").css("display", "none");
        $("#no").css("display", "inline-block");
        $("#yes").css("display", "inline-block");

        var wnd = $("#modalWindow").kendoWindow({
            title: "Customer Convertion",
            modal: true,
            visible: false,
            resizable: false,
            width: 350
        }).data("kendoWindow");

        $("#MainContent_div_duplicaterecord").html(msg);
        $("#MainContent_div_duplicaterecord").css("display", "block");
        $("#div_confirm_message").css("display", "none");
        wnd.center().open();

        $("#yes").click(function () {
            $("#MainContent_HiddenFieldConversionSent").val('1');
            document.getElementById("MainContent_buttonConvert").click();
            wnd.close();
        });

        $("#no").click(function () {
            wnd.close();
        });
    }

function loadLeadAddressMap(div_id,leadid,custcode) {
        //var entityGrid = $("#LeadAddressGrid").data("kendoGrid");
        var entityGrid = $(div_id).data("kendoGrid");
        var selectedRows = entityGrid.dataItem(entityGrid.select());

        if (selectedRows == "" ||  selectedRows == undefined) {
            alert('Please select the Row');
        }
        else {
                //var row = selectedRows[i];
                var strAddress = "";
                var state = getstatus(selectedRows["State"]);

                if (selectedRows["Address1"] != '' && selectedRows["Address1"] != null && selectedRows["Address2"] != '' && selectedRows["Address2"] != null)
                    strAddress = selectedRows["Address1"]+", " + selectedRows["Address2"];
                else if (selectedRows["Address1"] != '' && selectedRows["Address1"] != null)
                    strAddress = selectedRows["Address1"];

                if (selectedRows["Address1"] != '' && selectedRows["Address1"] != null && selectedRows["City"] != '' && selectedRows["City"] != null)
                    strAddress += ", " + selectedRows["City"];
                else if (selectedRows["City"] != '' && selectedRows["City"] != null)
                    strAddress += selectedRows["City"];


                if (selectedRows["Address1"] != '' && selectedRows["Address1"] != null && selectedRows["City"] != '' && selectedRows["City"] != null &&
            state != '' && state != null)
                    strAddress += ", " + state;
                else if (strAddress != '' && strAddress != null && state != '' && state != null)
                    strAddress += ", " + state;
                else if (strAddress == '' && strAddress == null && state != '' && state != null)
                    strAddress += state;

                strAddress = strAddress.replace("&", " ");
         
                var url =  "";
                if(custcode != '')
                {
                    url = "map.aspx?address=" + strAddress + "&ty=Customer&custcode=" + custcode ;
                }
                else if(leadid != '')
                {
                    url = "map.aspx?address=" + strAddress + "&leadid=" + leadid ;
                }else{
                    url = "map.aspx?address=" + strAddress ;
                }
                window.location.href = url;
            }
        }

function getstatus(state) {

    switch (state.replace(/^\s\s*/, '').replace(/\s\s*$/, '')) {
        case "ACT":
            state = "Australian Capital Territory";
            break;
        case "NSW":
            state = "New South Wales";
            break;
        case "NT":
            state = "Northern Territory";
            break;
        case "QLD":
            state = "Queensland";
            break;
        case "VIC":
            state = "Victoria";
            break;
        case "TAS":
            state = "Tasmania";
            break;
        case "WA":
            state = "Western Australia";
            break;
    }
    return state;
}

function originator(targeturl) {
            $("#div_loader").show();
            var url = ROOT_PATH + targeturl;
            $.ajax({
                url: url,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    $("#div_loader").hide();
                    if (msg != "") {

                        var wnd = $("#modalWindow").kendoWindow({
                            title: "Originator Select",
                            modal: true,
                            visible: false,
                            resizable: false,
                            width: 300
                        }).data("kendoWindow");

                        $("#modalWindow").html(msg);
                        wnd.center().open();

                        $("#jqi_state0_buttonOk").click(function () {

                            var selected_email = '';
                            $('#originatorTable tr').each(function () {
                                if ($(this).children(':eq(0)').find('#a').is(':checked')) {
                                    
                                    var emailaddress = $(this).children(':eq(0)').find('#a').val();
                                    
                                    selected_email = selected_email + emailaddress;
                                }
                            });
                            $('#MainContent_txtAssignto').val(selected_email);

                            wnd.close();
                        });
                    }
                },
                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                        $("#div_loader").hide();
                        //$("#" + tagertdiv).html(msg);
                    }
                }
            });
        }


function ClearText(obj_text) {
    var _text = $("#" + obj_text).val();
    if (_text.indexOf('(Max 500 Characters)') == 0 || _text.indexOf('(Max 2000 Characters)') == 0)
        $("#" + obj_text).val("");


}

/*Start CustomerEntry*/
function loadCustomerAddressGrid(custid,targetdiv) {
    var url = encodeURI("javascript:addAddress('lead_customer/process_forms/processmaster.aspx?fm=contactdetails&type=insert&ty=Customer&custid=" +
        custid + " &leadadid=#=LeadAddressID#&typ=#=AddressTypeDescription#&add1=#=Address1#&add2=#=Address2#&cty=#=City#&sta=#=State#&pstcde=#=PostCode#&name=#=Name#&adcntac=#=Contact#&adtele=#=Telephone#&adfax=#=Fax#&cntry=#=Country#&assinto=#=AssigneeNo#','');");

    url = "<a href=\"" + url + "\">#=Address1#</a>";
    
    $("#"+targetdiv).html("");                 
    $("#"+targetdiv).kendoGrid({
        height: 300,
        columns: [
            { field: "Address1",  width: "85px",  hidden: true},
            { template: url,field: "Address 1", width: "250px"},
            { field: "Address2", title: "Address 2", width: "105px" },
            { field: "City", title: "City", width: "100px" },
            { field: "State", title: "State", width: "50px" },
            { field: "PostCode", title: "Post Code", width: "80px" },
            { field: "Country", title: "Country", width: "80px" },
            { field: "Name", title: "Name", width: "130px" },
            { field: "Contact", title: "Contact", width: "80px" },
            { field: "Telephone", title: "Telephone", width: "80px" },
            { field: "Fax", title: "Fax", width: "80px" }
    ],
    editable: false, // enable editing
    pageable: true,
    sortable: true,
    filterable: true,
    selectable: "single",
    columnMenu: false,
//    resizable: true,
    //dataBound: LeadCustomerGrid_onDataBound,
    dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        Address1: { validation: { required: true} },
                        Address2: { editable: true, nullable: true },
                        State: { validation: { required: true} },
                        City: {   validation: { required: true} },
                        PostCode: { validation: { required: true} },
                        Country: { validation: { required: true} },
                        Name: { validation: { required: true} },
                        Contact: { validation: { required: true} },
                        Telephone: { validation: { required: true} },
                        Fax: { validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH +"service/lead_customer/customer_entry_Service.asmx/GetCustomerAddressDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function loadDistributorBankGrid(distid,targetdiv) {
    var url = encodeURI("javascript:addBankAccount('lead_customer/process_forms/processmaster.aspx?fm=bankdetails&type=insert&ty=dist&distid=" +
        distid + "&typ=DIST&accno=#=AccountNumber#&bnk=#=Bank#&brch=#=Branch#','');");

    url = "<a href=\"" + url + "\">#=Bank#</a>";
    
    $("#"+targetdiv).html("");                 
    $("#"+targetdiv).kendoGrid({
        height: 300,
        columns: [
            { field: "AccountNumber",title: "Account Number",  width: "85px"},
            { template: url,field: "Bank", width: "250px"},
            { field: "Branch", title: "Branch", width: "105px" },

    ],
    editable: false, // enable editing
    pageable: true,
    sortable: true,
    filterable: true,
    selectable: "single",
    columnMenu: false,
//    resizable: true,
    //dataBound: LeadCustomerGrid_onDataBound,
    dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        AccountNumber: { validation: { required: true} },
                        Bank: { editable: true, nullable: true },
                        Branch: { validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH +"service/lead_customer/distributor_Service.asmx/GetDistributorBankDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}



function loadCustomerContactPersonGrid(custid,targetdiv) {
//    var url =ROOT_PATH+ "lead_customer/transaction/contact_person_entry.aspx?custid="+custid+"&contype="+"#=ContactType#&ori="+"#=Origin#";

//    url = "<a href=\"" + url + "\">#=TypeDescription#</a>";
   
    //?custid=TWOFORQ0&personid=18575
    $("#"+targetdiv).html("");
    $("#" + targetdiv).kendoGrid({
        height: 300,
        columns: [
        //            { template: url,field: "TypeDescription",title: "Contact Type",  width: "85px"},
            {field: "KeyContactChecked", title: " ", width: "25px", template: '<input type="checkbox" name="checkbox" disabled="true" #= (KeyContact== "Y") ? checked="checked":  "N" #/>' },
            { template: '<a href="javascript:OpenCustomerContactPerson(\'#=Origin#\',\'#=ContactType#\')">#=TypeDescription#</a>', field: "TypeDescription", title: "Contact Type", width: "85px" },
            { field: "Origin", title: "Origin", width: "100px" },
            { field: "Contact", title: "Contact", width: "105px" },
            { field: "Position", title: "Position", width: "130px" },
            { field: "Telephone", title: "Telephone", attributes: { style: "text-align: right" } , width: "80px" },
            { field: "Fax", title: "Fax", attributes: { style: "text-align: right" } , width: "80px" },
            { field: "Mobile", title: "Mobile", attributes: { style: "text-align: right" } , width: "80px" },
            { field: "EmailAddress", title: "Email Address", width: "220px" },
            { field: "ContactType", title: "", hidden: true }
    ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: false,
       // resizable: true,
        selectable: "single",
        columnMenu: false,
        //dataBound: LeadCustomerGrid_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        ContactType: { validation: { required: true} },
                        Origin: { editable: true, nullable: true },
                        Contact: { validation: { required: true} },
                        Position: { validation: { required: true} },
                        Telephone: { validation: { required: true} },
                        Fax: { validation: { required: true} },
                        Mobile: { validation: { required: true} },
                        EmailAddress: { validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/customer_entry_Service.asmx/GetCustomerContactPersonDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function loadCustomerEndUserGrid(custid,targetdiv,enduserStatus,type) {
 //   var url =ROOT_PATH+ "lead_customer/transaction/enduser_entry.aspx?custid="+custid+ "&ty=" + type +"&eduid="+"#=EndUserCode#&";
    var url = ROOT_PATH + "lead_customer/transaction/enduser_entry.aspx?custid=" + custid + "&ty=customer&eduid=" + "#=EndUserCode#&";

    url = "<a href=\"" + url + "\">#=EndUserCode#</a>";
   
    //?custid=TWOFORQ0&personid=18575
    $("#"+targetdiv).html("");            
    $("#"+targetdiv).kendoGrid({
        height: 300,
        columns: [
            { template: url,field: "EndUserCode",title: "End User Code",  width: "100px"},
            { field: "Name",title: "Name", width: "150px"},
            { field: "Contact", title: "Contact", width: "105px" },
            { field: "Address1", title: "Address 1", width: "130px" },
            { field: "Address2", title: "Address 2", width: "80px" },
            { field: "City", title: "City", width: "80px" },
            { field: "State", title: "State", width: "80px" },
            { field: "PostCode", title: "Post Code", width: "80px" }
    ],
    editable: false, // enable editing
    pageable: true,
    sortable: true,
    filterable: false,
    selectable: "single",
    columnMenu: false,
    //dataBound: LeadCustomerGrid_onDataBound,
    dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total"
            },
            transport: {
                read: {
                    url: ROOT_PATH +"service/lead_customer/customer_entry_Service.asmx/GetCustomerEndUserDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { statuscode: enduserStatus},
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

function loadCustomerActivityGrid(custid,targetdiv) {
    var url =ROOT_PATH+ "activity_planner/transaction/activityentry.aspx?custid="+custid+"&activityId="+"#=ActivityID#";

    url = "<a href=\"" + url + "\">#=Subject#</a>";
    $("#"+targetdiv).html("");             
    $("#"+targetdiv).kendoGrid({
        //height: '100%',
        columns: [
            { template: url,field: "Subject",title: "Subject",  width: "85px"},
            { field: "StartDate", title: "Activity date", width: "90px", format: DATETIME_FORMAT2 },
            { field: "StatusDescription", title: "Status", width: "105px" },
            { field: "AssignedTo", title: "Assigned To", width: "130px" },
            { field: "SentMail", title: "Sent Mail", width: "70px" },
            { field: "PriorityDescription", title: "Priority", width: "80px" },
            { field: "ActivityTypeDescription", title: "Type", width: "80px" }
    ],
    editable: false, // enable editing
    pageable: true,
    sortable: true,
    filterable: false,
    selectable: "single",
    columnMenu: false,
    dataBound: LeadActivityGrid_onDataBound,
    dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                   fields: {
                        Subject: { type: "string" },
                        StartDate: { type: "date" },
                        StatusDescription: { type: "string" },
                        AssignedTo: { type: "string" },
                        SentMail: { type: "string" },
                        PriorityDescription: { type: "string" },
                        ActivityTypeDescription: { type: "string" }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH +"service/lead_customer/customer_entry_Service.asmx/GetCustomerActivityDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    //data: { statuscode: enduserStatus},
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

function Customerdocumentupload(document_custid) {

        $(".upload").kendoUpload({
            async: {
                saveUrl: ROOT_PATH +"lead_customer/process_forms/processmaster.aspx",
                autoUpload: true
            },
            upload: function onUpload(e) {
                e.sender.options.async.saveUrl = ROOT_PATH+"lead_customer/process_forms/processmaster.aspx?fm=custdocument&type=insert&custid="+document_custid;
            },
            success: function onSuccess(e) {                
                var grid = $("#LeadDocumentGrid").data("kendoGrid");
                    grid.dataSource.read();
            }
        });
}

function loadCustomerDocumentGrid(custid, targetdiv) {
    var cust_url = 'lead_customer/process_forms/processmaster.aspx?type=get&fm=customerentry&docid=' + "#=DocumentID#";

    //var openurl = "<a href=\"" + url + "\">#=DocumentName#</a>";
    $("#"+targetdiv).html("");
    $("#"+targetdiv).kendoGrid({
        height: '100%',
        columns: [
                        { field: "Extention", width: "85px", hidden: true },
                        { field: "TempFileName", width: "80px", hidden: true },
                        { field: "DocumentID", width: "80px", hidden: true },
                        { field: "TempFileName", width: "80px", hidden: true },

                        { template: '<a href="javascript:deleteDocument(\'#=DocumentID#\',\'#=DocumentName#\',0)">Delete</a>',
                            field: "", width: "20px"
                        },

                        { template: '<a href="javascript:OpenLeadDocument(\'#=DocumentID#\',\'#=Extention#\',\'#=Path#\',\'cust\')">#=DocumentName#</a>',
                            field: "DocumentName", title: "Document Name", width: "200px"
                        },
                        { field: "AttachedBy", title: "Attached By", width: "100px" },
                        { field: "AttachedDate", title: "Attached Date", width: "100px", format: DATE_FORMAT3 }
                ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: false,
        //dataBound: LeadCustomerGrid_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    fields: {
                        Extention: { validation: { required: true} },
                        TempFileName: { validation: { required: true} },
                        DocumentID: { validation: { required: true} },
                        DocumentName: { validation: { required: true} },
                        AttachedBy: { validation: { required: true} },
                        AttachedDate: { type: "date", validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH +"service/lead_customer/customer_entry_Service.asmx/GetCustomerDocumentDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            },
            change: emptyGridFix
        }
        //dataBound: grid_dataBound
    });
}

function grid_dataBound(e) {
    var grid = $('#LeadDocumentGrid').data('kendoGrid');

    if (grid.dataSource == null || grid.dataSource._total == 0) {
        grid.content.find('div').each(function () {
            $(this).hide();
        });
        grid.content.append('<div class="grid-no-data">There are no records to display</div>');
    }
    else {
        var noDataDiv = $('.no-data', grid.content);

        if (noDataDiv != null) {
            $(noDataDiv).remove();
        }

        $('div', grid.content).each(function () {
            $(this).show();
        });
    }
}

function emptyGridFix() {
    
    if (this.total() > 0) return; // continue only for empty grid
    var msg = this.options.emptyMsg;
    if (!msg) msg = 'No records to display'; // Default message
    $(this.options.table).parent().html('<div class="empty-grid">' + msg + '</div>')
}

function savetransfer(targeturl) {
    var url = ROOT_PATH + targeturl;
    $("#div_loader").show();
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            $("#div_loader").hide();
            if (msg != "") {
                $("#MainContent_div_message").show();
                $("#MainContent_div_message").html(msg);
                $("#MainContent_div_message").css("padding","2px");
                $("div#cust_window").data("kendoWindow").close();
            }
        },
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg; // "Sorry there was an error: ";
                $("#div_loader").hide();
                //$("#" + tagertdiv).html(msg);
            }
        }
    });
}

function transferenduser(targeturl) {
    var entityGrid = $("#CustomerEndUser").data("kendoGrid");
    var selectedRows = entityGrid.dataItem(entityGrid.select());
    var count = entityGrid.select().length;
    var endusercode = '';
    if (count == 0) {
        $("#cust_window").html('Please select the Row');
        $("#cust_window").data("kendoWindow").open();
    }
    else{
        endusercode=selectedRows["EndUserCode"];
    }

    if (count > 0) {
        var url = ROOT_PATH + targeturl + '&eduid=' + endusercode;
        $("#div_loader").show();
        $.ajax({
            url: url,
            dataType: "html",
            cache: false,
            success: function (msg) {
                $("#div_loader").hide();
                if (msg != "") {
                    $("#cust_window").html(msg);
                    $("#cust_window").data("kendoWindow").open();
                }
            },
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg; // "Sorry there was an error: ";
                    $("#div_loader").hide();
                    //$("#" + tagertdiv).html(msg);
                }
            }
        });
    }
}

function loadnewcustomer(griddiv, windowdiv) {
    var cust_url = 'lead_customer/process_forms/processmaster.aspx?type=get&fm=customerentry&docid=' + "#=DocumentID#";

    $("#" + griddiv).kendoGrid({
        height: '250px',
        columns: [
            { template: '<a href="javascript:setCustCode(\'#=Code#\');">#=Code#</a>', field: "Code", title: "Customer Code", width: "85px" },
            { field: "Description", title: "Name", width: "80px"}
    ],
        editable: false, // enable editing
        pageable: false,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        //dataBound: LeadCustomerGrid_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    fields: {
                        Code: { type: "string" },
                        Description: { type: "string" }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/customer_entry_Service.asmx/GetNewCustomerDataAndCount",
                    contentType: "application/json; charset=utf-8",
                    type: "POST",
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#newcust_window").data("kendoWindow").open();
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

function setCustCode(cust_code) {
    $('#txtnewcustcode').val(cust_code);
    $("#newcust_window").data("kendoWindow").close();
}

/*End CustomerEntry*/

function catalog(targeturl) {
            var url = ROOT_PATH + targeturl
            $("#div_loader").show();
            $.ajax({
                url: url,
                dataType: "html",
                cache: false,
                success: function (msg) {

                    if (msg != "") {
                        $("#div_loader").hide();

                        var wnd = $("#modalWindow").kendoWindow({
                            title: "Lookup",
                            modal: true,
                            visible: false,
                            resizable: false,
                            width: 450
                        }).data("kendoWindow");

                        LookupAllCatalog("div_confirm_message");
                        //$("#div_confirm_message").html(msg);
//                        wnd.center().open();

//                        $('#buttonOk').click(function () {
//                            var asset_type_name = '';
//                            var selected_email = '';
//                            var conversion = '';
//                            var price = '';
//                            $('#AssetTypeTable tr').each(function () {
//                                if ($(this).children(':eq(0)').find('#a').is(':checked')) {
//                                    var emailaddress = $(this).children(':eq(0)').find('#a').val();
//                                    selected_email = selected_email + emailaddress;
//                                    asset_type_name = $(this).children(':eq(0)').find('#b').val();
//                                    conversion = $(this).children(':eq(0)').find('#c').val();
//                                    price = $(this).children(':eq(0)').find('#d').val();
//                                }
//                            });
//                            $('#MainContent_HiddenFieldCode').val(selected_email);
//                            $('#MainContent_HiddenFieldDesc').val(asset_type_name);
//                            $('#MainContent_HiddenFieldPrice').val(price);
//                            $('#MainContent_HiddenFieldConversion').val(conversion);
//                            loadProductGrid();
//                            wnd.close();
//                        });

                        //                        $("#jqi_state0_buttonOk").click(function () {
                        //                        });
                    }
                },
                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg; // "Sorry there was an error: ";
                        //$("#" + tagertdiv).html(msg);
                    }
                }
            });
        }

 function LoadContactPersonEntry() {
    $("#MainContent_txtEmail").kendoValidator().data("kendoValidator");
    $("#MainContent_txtFirstName").kendoValidator().data("kendoValidator");
            

    $("#tabstrip").kendoTabStrip({
        animation: {
            open: {
                effects: "fadeIn"
            }
        }
    });

    $(".upload").kendoUpload({
        async: {
            saveUrl: "../process_forms/processmaster.aspx?fm=contactpersonimagesave&type=insert",
            autoUpload: true
        },
        upload: function onUpload(e) {
            e.sender.options.async.saveUrl = "../process_forms/processmaster.aspx?fm=contactpersonimagesave&type=insert";

            var files = e.files; 	
            // Check the extension of each file and abort the upload if it is not .jpg
            $.each(files, function () {
                if (this.extension != ".jpg" && this.extension != ".gif" && this.extension != ".tiff" && this.extension != ".png" && this.extension != ".bmp") {
                    e.preventDefault();
                }
            });

        },
        success: function onSuccess(e) {
            //var ROOT_PATH = '/Peercore.CRM.WebCRM/'
            var ROOT_PATH = '/'
            var sessionId = $('#MainContent_hfSessionId').val();
            var filename = "contactperson_" + sessionId + "_" + e.files[0].name;
            $('#MainContent_hfImageName').val(filename);
            $("#MainContent_div_preview").html('<img src="' + ROOT_PATH + 'docs/contactperson/' + filename + '">');
        }
    });
}

function loadPromotionGrid() {
    $("#loadPromotionGrid").html("");
    var a = $("#MainContent_HiddenFieldPromoName").val()
    var promotion = [];
    if ($("#MainContent_HiddenFieldPromoName").val() != '') {
        promotion = [
                { "PromoName": $("#MainContent_HiddenFieldPromoName").val() }
            ];
    }

    $("#loadPromotionGrid").kendoGrid({
        height: 100,
        columns: [
                { template: '<a href="javascript:deletePromotion()">Delete</a>',
                    field: "", width: "20px"
                },
                { field: "PromoName", title: "Promotions", width: "200px" }

        ],
       // editable: true, // enable editing
        pageable: false,
        sortable: false,
        filterable: false,

        dataSource: {
            pageSize: 5,
            data: promotion,
           // change: dataSourceChange,
            //total: 2//,
            schema: {
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                   //     CatlogCode: {validation: { required: true} },
                        PromoName: { validation: { required: true} }
                    }
                }
            }
        },
        dataBound: loadPromotionGrid_dataBound
    });
}

function loadPromotionGrid_dataBound(e) {
    var grid = $('#loadPromotionGrid').data('kendoGrid');

    if (grid.dataSource == null || grid.dataSource._total == 0) {
        grid.content.find('div').each(function () {
            $(this).hide();
        });
        grid.content.append('<div class="grid-no-data">There are no records to display</div>');
    }
    else {
        var noDataDiv = $('.grid-no-data', grid.content);

        if (noDataDiv != null) {
            $(noDataDiv).hide();
        }
    }
}


function loadProductGrid() {
    $("#loadProductGrid").html("");
    var product = [];
    if ($("#MainContent_HiddenFieldCode").val() != '') {
        product =[
                { "CatlogCode":  $("#MainContent_HiddenFieldCode").val() , "Description": $("#MainContent_HiddenFieldDesc").val(), "Price": $("#MainContent_HiddenFieldPrice").val() }
            ];
    }

    $("#loadProductGrid").kendoGrid({
        height: 100,
        columns: [
                { template: '<a href="javascript:deleteProduct()">Delete</a>',
                    field: "", width: "40px"
                },
                { field: "CatlogCode", title: "Product Code", width: "100px", editable: false },
                { field: "Description", title: "Description", width: "100px" },
                { field: "Price", title: "Price", width: "100px", editable: true, format: NUMBER_FORMAT_DECIAML }

        ],
        editable: true, // enable editing
        pageable: false,
        sortable: false,
        filterable: false,
               
        dataSource: {
            pageSize: 5,
            data: product,
            change : dataSourceChange,
            //total: 2//,
            schema: {
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        CatlogCode: { editable: false, validation: { required: true} },
                        Description: { editable: false, validation: { required: true} },
                        Price: { type: "number", validation: { required: true} }
                    }
                }
            }
        },
        dataBound: loadProductGrid_dataBound
    });
}


function dataSourceChange(e, a, b) {
    var _data = this.data();
    if (_data.length > 0)
        $("#MainContent_HiddenFieldPrice").val(_data[0].Price);
    SetTonnesAndAmount();
}

function loadProductGrid_dataBound(e) {
    var grid = $('#loadProductGrid').data('kendoGrid');

    if (grid.dataSource == null || grid.dataSource._total == 0) {
        grid.content.find('div').each(function () {
            $(this).hide();
        });
        grid.content.append('<div class="grid-no-data">There are no records to display</div>');
    }
    else {
        var noDataDiv = $('.grid-no-data', grid.content);

        if (noDataDiv != null) {
            $(noDataDiv).hide();
        }
    }
}

function deleteProduct() {
    $("#MainContent_HiddenFieldCode").val('');
    $("#MainContent_HiddenFieldDesc").val('');
    $("#MainContent_HiddenFieldPrice").val('');
    
    loadProductGrid();
}
function deletePromotion() {
    $("#MainContent_HiddenFieldCustCode").val('');
    $("#MainContent_HiddenFieldPromoid").val('');
    $("#MainContent_HiddenFieldPromoName").val('');

    loadPromotionGrid();
}

function opportunityentrypageLoad() {
    loadProductGrid();

//    $("#MainContent_mtxtAmount").kendoNumericTextBox({
//        format: "#.00"
//    });

//    $("#MainContent_mtxtUnits").kendoNumericTextBox({
//        format: "#.00"
//    });

//    $("#MainContent_txtTonnes").kendoNumericTextBox({
//        format: "#.00"
//    });

//    $("#MainContent_txtName").kendoValidator().data("kendoValidator");
//    $("#MainContent_mtxtUnits").kendoValidator().data("kendoValidator");

    $("#id_assignedto").click(function () {
        originator('activity_planner/process_forms/processmaster.aspx?fm=activityentry&type=query&querytype=originator');
        //originator('lead_customer/process_forms/processmaster.aspx?fm=leadentry&type=query&querytype=originator');
    });

        var text = $("#MainContent_txtDescription").val();
        if (text == "" || text == null || text == undefined) {
            $("#MainContent_txtDescription").val('(Max 500 Characters)');
        } else if (text.indexOf('(Max 500 Characters)') == 0) {
            $("#MainContent_txtDescription").val();
        } else {
            $("#MainContent_txtDescription").val(text);
        }
    }


    function GridLineAnalysisGraph(id) {
        $("#grid").html("");
        $("#grid").kendoGrid({
            height: 400,
            columns: [
                    { field: "ActivityID", width: "85px", hidden: true },
                    { field: "AppointmentId", width: "85px", hidden: true },
                    { field: "LeadId", width: "0px", hidden: true },
                    { field: "CustCode", width: "0px", hidden: true },
                    { field: "EndUserCode", width: "0px", hidden: true },
                    { field: "ActivityType", width: "0px", hidden: true },

                    { template: '<a href="javascript:LoadActivityFromAnalysisGraph(\'#=ActivityID#\',\'#=LeadStage#\',\'#=LeadId#\',\'#=CustCode#\')">#=TypeDesc#</a>',
                        field: "TypeDesc", width: "80px", title: "Activity Type"
                    },
                    { field: "LeadName", title: "Name", width: "150px" },
                    { field: "LeadStage", title: "Lead Stage", width: "70px" },
                    { field: "Subject", title: "Subject", width: "130px" },
                    { field: "StartDate", title: "Start Date", width: "80px", format: DATE_FORMAT3 },
                    { field: "EndDate", title: "End Date", width: "80px", format: DATE_FORMAT3 },
                    { field: "Comments", title: "Comments", width: "80px", template: '<div style="height: 20px; ">#=Comments#</div>' }

                ],
            editable: false, // enable editing
            pageable: true,
            sortable: true,
            dataSource: {
                serverSorting: true,
                serverPaging: true,
                serverFiltering: true,
                pageSize: 20,
                schema: {
                    data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                    total: "d.Total",
                    model: { // define the model of the data source. Required for validation and property types.
                        id: "ActivityID",
                        fields: {
                            ActivityID: { validation: { required: true} },
                            AppointmentId: { validation: { required: true} },
                            CategoryDescription: { editable: true, nullable: true },
                            Category: { validation: { required: true} },
                            LeadName: { validation: { required: true} },
                            LeadStage: { validation: { required: true} },
                            Subject: { validation: { required: true} },
                            Body: { validation: { required: true} },
                            StartDate: { type: "Date", validation: { required: true} },
                            EndDate: { type: "Date", validation: { required: true} }

                        }
                    }
                },
                transport: {

                    read: {
                        url: "../../Service/lead_customer/common.asmx/GetActivityByType", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                        contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                        data: { id: id },
                        type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                    },
                    parameterMap: function (data, operation) {
                        if (operation != "read") {
                            return JSON.stringify({ products: data.models })
                        } else {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }
            }
        });
    }

    function LoadActivityFromAnalysisGraph(activityId, leadStage, leadId, CustCode) {

        if (leadStage == "Customer" || leadStage == "End User") {
            window.location = '../../activity_planner/transaction/activityentry.aspx?activityid=' + activityId + '&custid=' + CustCode + '&fm=dsbd1';

        } else if (leadStage == "Lead" || leadStage == "Quote") {
            window.location = '../../activity_planner/transaction/activityentry.aspx?activityid=' + activityId + '&leadId=' + leadId + '&fm=dsbd1';
        }
    }

    function GridColumnAnalysisGraph(id, name) {
        $("#grid").html("");
        $("#grid").kendoGrid({
            height: 400,
            columns: [
                    { field: "ActivityId", width: "85px", hidden: true },
                    { field: "AppointmentId", width: "85px", hidden: true },
                    { field: "LeadId", width: "0px", hidden: true },
                    { field: "CustCode", width: "0px", hidden: true },
                    { field: "EndUserCode", width: "0px", hidden: true },
                    { field: "Category", width: "0px", hidden: true },

                    { template: '<a href="javascript:LoadActivityFromAnalysisGraph(\'#=ActivityId#\',\'#=LeadStage#\',\'#=LeadId#\',\'#=CustCode#\')">#=CategoryDescription#</a>',
                        field: "CategoryDescription", width: "80px", title: "Activity Type"
                    },
            //                    { field: "CategoryDescription", title: "Activity Type Desc", width: "85px" },
                    {field: "LeadName", title: "Name", width: "150px" },
                    { field: "LeadStage", title: "Lead Stage", width: "70px" },
                    { field: "Subject", title: "Subject", width: "130px" },
                    { field: "StartTime", title: "Start Date", width: "80px", format: "{0: yyyy-MM-dd}" },
                    { field: "EndTime", title: "End Date", width: "80px", format: "{0: yyyy-MM-dd}" },
                    { field: "Body", title: "Comments", width: "80px", template: '<div style="height: 20px; ">#=Body#</div>' }

                ],
            editable: false, // enable editing
            pageable: true,
            sortable: true,
            dataSource: {
                serverSorting: true,
                serverPaging: true,
                serverFiltering: true,
                pageSize: 20,
                schema: {
                    data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                    total: "d.Total",
                    model: { // define the model of the data source. Required for validation and property types.
                        id: "ActivityID",
                        fields: {
                            ActivityID: { validation: { required: true} },
                            AppointmentId: { validation: { required: true} },
                            CategoryDescription: { editable: true, nullable: true },
                            Category: { validation: { required: true} },
                            LeadName: { validation: { required: true} },
                            LeadStage: { validation: { required: true} },
                            Subject: { validation: { required: true} },
                            Body: { validation: { required: true} },
                            StartTime: { type: "Date", validation: { required: true} },
                            EndTime: { type: "Date", validation: { required: true} }

                        }
                    }
                },
                transport: {

                    read: {
                        url: "../../Service/lead_customer/common.asmx/GetAppointmentsByType", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                        contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                        data: { id: id, type: name },
                        type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                    },
                    parameterMap: function (data, operation) {
                        if (operation != "read") {
                            return JSON.stringify({ products: data.models })
                        } else {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }
            }
        });
    }

    function loadendusercustomer(targetdiv, endcode, customercode) {
    $("#" + targetdiv).html("");
    $("#" + targetdiv).kendoGrid({
        height: '150px',
        columns: [
            { title: " ", width: "10px",hidden: true, template: '<input type="radio" name="radio" data-bind="checked: IsPrimaryDist" #= IsPrimaryDist ? checked="checked" : "" #/>' },
                    { template: '<a href="javascript:OpenEndUser(\'#=EndUserCode#\',\'#=CustomerCode#\')">#=CustomerCode#</a>', 
            field: "CustomerCode", title: "Code", width: "85px" },
            { field: "Name", title: "Distributor", width: "80px" }
    ],
        editable: false, // enable editing
        pageable: false,
        sortable: true,
        filterable: false,
        selectable: "single",
        columnMenu: false,
        dataBound: endusercustomerGrid_dataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: false,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    fields: {
                        Code: { type: "string" },
                        Description: { type: "string" },
                        IsPrimaryDist: { type: "boolean" }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/enduser_entry_Service.asmx/GetEndUserCustomerDataAndCount",
                    contentType: "application/json; charset=utf-8",
                    type: "POST",
                    data: { endusercode: endcode, customercode: customercode}
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}


function endusercustomerGrid_dataBound(args) {
    var entityGrid = $("#div_grid_enduser").data("kendoGrid"); //
    var data = entityGrid.dataSource.data();
    var totalNumber = data.length;
    for (var i = 0; i < totalNumber; i++) {
        var currentDataItem = data[i];

        if (currentDataItem.CustomerCode.trim() == $("#MainContent_HiddenFieldCustomerCode").val().trim()) {
            entityGrid.select('tr:eq(' + (i+1) + ')');
            break;
        }
        //VersionIdArray[i] = currentDataItem.VersionId;
    }
}


function LeadStagingChartLoad(category) {
    var url = ROOT_PATH + "lead_customer/transaction/lead_contacts.aspx?cht=lead&ty=" + category ;
    window.location = url;
}

/*Start user login*/
function UserLogin(uname, password, errorControl) {
    var url = encodeURI(ROOT_PATH + 'process_forms/processmaster.aspx?fm=ln' + '&type=ln' + '&uname=' + uname + '&pwd=' + password);
    $("div#div_loader").show();
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg == "true") {
                $("#div_loader").hide();
                window.location = ROOT_PATH + 'default.aspx';
            }
            else {
                $("div#div_loader").hide();
                $("#" + errorControl).show()
                $("#" + errorControl).html(msg);
            }
        },
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = "Sorry but there was an error: ";
                $("#" + errorControl).show()
                // $("#" + errorControl).html(msg + xhr.status + " " + xhr.statusText);
            }
        }
    });
}
/*End user login*/

function focusTo(e, button) {
    var evt = e || window.event; // IE compatibility
    if (evt.keyCode == 13) {
        //document.getElementById(button).click();
        signinsubmit();
//        evt.preventDefault();
//        e.returnValue = false;
        if (evt.preventDefault) {
            evt.preventDefault();
        } else {
            evt.returnValue = false;
        }
    }
}

/*Start End User Details*/

function loadEnduserContactPersonGrid(custid,enduserid,targetdiv) {
    $("#"+targetdiv).html("");
    $("#"+targetdiv).kendoGrid({
        height: '100%',
        columns: [
            { field: "ContactPersonID", width: "85px", hidden: true },
            { field: "KeyContactChecked",title: " ", width: "15px" , template: '<input type="checkbox" name="checkbox" disabled="true" #= (KeyContact== "Y") ? checked="checked":  "N" #/>' },
            { field: "Title",title: "Title", width: "40px"},
            { template:'<a href="javascript:OpenEndUserContactPerson(\'#=ContactPersonID#\')">#=FirstName#</a>', field: "FirstName",title: "First Name",  width: "85px"},
            //{ field: "FirstName",template: url, title: "First Name",  width: "85px"},
            { field: "LastName",title: "Last Name", width: "85px"},
            { field: "Position", title: "Position", width: "130px" },
            { field: "Telephone", title: "Telephone", width: "80px" },
            { field: "Mobile", title: "Mobile", width: "80px" },
            { field: "EmailAddress", title: "Email Address", width: "80px" }
    ],
    editable: false, // enable editing
    pageable: true,
    sortable: true,
    filterable: false,
    selectable: "single",
    columnMenu: false,
    //dataBound: LeadCustomerGrid_onDataBound,
    //dataBound: EnduserContactPersonGrid_dataBound,
    dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        Title: { validation: { required: true} },
                        FirstName: { editable: true, nullable: true },
                        LastName: { validation: { required: true} },
                        Position: { validation: { required: true} },
                        Telephone: { validation: { required: true} },
                        Mobile: { validation: { required: true} },
                        EmailAddress: { validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH +"service/lead_customer/enduser_entry_Service.asmx/GetEndUserContactPersonDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { custid: custid},
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }//,
           //change: emptyGridFix
        }//,
       // dataBound: EnduserContactPersonGrid_dataBound
    });
 //  dataBound: EnduserContactPersonGrid_dataBound
}



function EnduserContactPersonGrid_dataBound(e) {
    var grid = $('#EnduserContactPersonGrid').data('kendoGrid');

    if (grid.dataSource == null || grid.dataSource._total == 0) {
        grid.content.find('div').each(function () {
            $(this).hide();
        });
        grid.content.append('<div class="grid-no-data">There are no records to display</div>');
    }
    else {
        var noDataDiv = $('.no-data', grid.content);

        if (noDataDiv != null) {
            $(noDataDiv).remove();
        }

        $('div', grid.content).each(function () {
            $(this).show();
        });
    }
}

function loadEnduserActivityGrid(custid,enduserid,targetdiv) {
    var url =ROOT_PATH+ "activity_planner/transaction/activityentry.aspx?custid="+custid+"&eduid="+enduserid+"&activityId="+"#=ActivityID#";

    url = "<a href=\"" + url + "\">#=Subject#</a>";
    $("#"+targetdiv).html();            
    $("#"+targetdiv).kendoGrid({
        //height: '100%',
        columns: [
            { field: "Subject",template: url,title: "Subject",  width: "85px"},
            { field: "StartDate",title: "Activity Date", width: "80px",format: DATE_FORMAT},
            { field: "StatusDescription", title: "Status", width: "105px" },
            { field: "AssignedTo", title: "Assigned To", width: "130px" },
            { field: "SentMail", title: "Sent Mail", width: "80px" },
            { field: "PriorityDescription", title: "Priority", width: "80px" },
            { field: "ActivityTypeDescription", title: "Type", width: "80px" },
            { field: "AppointmentID", title: "Type", width: "80px", hidden: true }
    ],
    editable: false, // enable editing
    pageable: true,
    sortable: true,
    filterable: false,
    dataBound: enduserActivityGrid_onDataBound,
    selectable: "single",
    columnMenu: false,
    dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                   fields: {
                        Subject: { type: "string" },
                        StartDate: { type: "date" },
                        StatusDescription: { type: "string" },
                        AssignedTo: { type: "string" },
                        SentMail: { type: "string" },
                        PriorityDescription: { type: "string" },
                        ActivityTypeDescription: { type: "string" }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH +"service/lead_customer/enduser_entry_Service.asmx/GetEndUserActivityDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { custid: custid},
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

function enduserActivityGrid_onDataBound(arg) {
    var dataView = this.dataSource.view();
    for (var i = 0; i < dataView.length; i++) {
        var uid = dataView[i].uid;
        if (dataView[i].AppointmentID == 0) {
        }
        else {
            $("#EndUserActivityGrid tbody").find("tr[data-uid=" + uid + "]").css('background-color', '#d8bfd8');
            $("#LeadActivityGrid tbody").find("tr[data-uid=" + uid + "]").css('background-color', '#d8bfd8');
        }
    }
}

function EndUserdocumentupload(cust_id,enduserid,targetdiv) {
        $(".upload").kendoUpload({
            async: {
                saveUrl: ROOT_PATH + "lead_customer/process_forms/processmaster.aspx",
                autoUpload: true
            },
            upload: function onUpload(e) {                    
                //var uploadId = e.sender.wrapper.prevObject.attr("value");
                //var LeadId =$('#MainContent_HiddenFieldLeadId').val();
                e.sender.options.async.saveUrl = ROOT_PATH + "lead_customer/process_forms/processmaster.aspx?fm=enduerdocument&type=insert&custid="+cust_id+"&eduid="+enduserid;
            },
            success: function onSuccess(e) {                
                var grid = $("#"+targetdiv).data("kendoGrid");
                    grid.dataSource.read();
            }
        });
}

function loadEndUserdocumentGrid(custid,enduserid,targetdiv) {
    var leadurl = ROOT_PATH + 'lead_customer/process_forms/processmaster.aspx?type=get&fm=enduserentry&docid=' + "#=DocumentID#";
    $("#"+targetdiv).html("");
    $("#"+targetdiv).kendoGrid({
        height: 300,
        columns: [

                        { field: "Extention", width: "85px", hidden: true },
                        { field: "TempFileName", width: "80px", hidden: true },
                        { field: "DocumentID", width: "80px", hidden: true },
                        { field: "TempFileName", width: "80px", hidden: true },

                        { template: '<a href="javascript:deleteDocument(\'#=DocumentID#\',\'#=DocumentName#\',0)">Delete</a>',
                            field: "", width: "20px"
                        },

                        { template: '<a href="javascript:OpenLeadDocument(\'#=DocumentID#\',\'#=Extention#\',\'#=Path#\',\'lead\')">#=DocumentName#</a>',
                            field: "DocumentName",title:"Title", width: "200px"
                        },
                        { field: "AttachedBy", title: "Attached By", width: "100px" },
                        { field: "AttachedDate", title: "Attached Date", width: "100px", format: DATE_FORMAT3 }
                ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: false,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        Extention: { validation: { required: true} },
                        TempFileName: { validation: { required: true} },
                        DocumentID: { validation: { required: true} },
                        DocumentName: { validation: { required: true} },
                        AttachedBy: { validation: { required: true} },
                        AttachedDate: { type: "Date", validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH+ "service/lead_customer/enduser_entry_Service.asmx/GetDocumenDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { custid: custid},
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function loadEnduserPriceList(custid,enduserid,targetdiv){
    var editurl = ROOT_PATH + 'lead_customer/process_forms/processmaster.aspx?type=get&custid='+custid+'&eduid='+enduserid+'&fm=enduserentryprice&docid=' + "#=CatlogCode#";
    HiddenFieldEndUser_CustCode
    HiddenFieldEndUser_EndUsercode

 //   var editurl = ROOT_PATH + 'lead_customer/process_forms/processmaster.aspx?type=get&fm=enduserentryprice&docid=' + "#=CatlogCode#";

    editurl="<a href=\"" + editurl + "\">#=CatlogCode#</a>";
    //var sss="#=CatlogCode";
    $("#"+targetdiv).html("");
    $("#"+targetdiv).kendoGrid({
        height: 300,
        columns: [
                        { template: "<div id=\"div_effdate\" > #: EffectiveDateString # </div>",field: "EffectiveDateString",hidden: true},
                        { template: "<div id=\"div_lastupdate\" > #: EffectiveDateString # </div>",field: "EffectiveDateString",hidden: true},
                        { field: "CatlogCode", title: "Catalog Code", width: "40px" },
                        { field: "Description",title:"Description", width: "200px"},
                        { field: "Price", title: "Cust Price", width: "100px",format: "{0:n2}" }
                ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: false,
        dataBound: EndUserGrid_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        EffectiveDate: { validation: { required: true}},
                        LastUpdated: { validation: { required: true} },
                        CatlogCode: { validation: { required: true} },
                        Description: { validation: { required: true} },
                        Price: { type: "number", validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH+ "service/lead_customer/enduser_entry_Service.asmx/GetEnduserPriceAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { custid: custid, effectivedate:''},
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            //$("div#div_Effective").html(model);
                        }
                    }
                },
                parameterMap: function (data, operation) {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                }
            }
        }
    });
}

function EndUserGrid_onDataBound(arg){
    $('#div_Effective').html($('#div_effdate').html());
    $('#div_LastUpdate').html($('#div_lastupdate').html());
}

function loadEnduserSales(custid,year,period,incproduct,targetdiv){

//    var editurl = ROOT_PATH + 'lead_customer/process_forms/processmaster.aspx?type=get&fm=enduserentryprice&docid=' + "#=CatlogCode#";

//    editurl="<a href=\"" + editurl + "\">#=CatlogCode#</a>";

   var dataSource = new kendo.data.DataSource({
            type: (dataType != null) ? dataType : null,
            transport: {
                read: {
                    url: ROOT_PATH+ "service/lead_customer/enduser_entry_Service.asmx/GetEnduserSalesAndCount",
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { custid: custid,year: year,period: period,incproduct: incproduct},
                    // dataSourceCallback is used to fix the name of the JSONP call back so that it's not randomized (thereby nullifying any caching or 304ability
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    jsonpCallback: (dataSourceCallback != null) ? dataSourceCallback : ""
                }
            }
  });
}

function showenduserexistmsg(endusername) {
        $("#save").css("display", "none");
        $("#no").css("display", "inline-block");
        $("#yes").css("display", "inline-block");

        var message="This End User already exists as '" + endusername + "' for the current distributor. Do you want to overwrite the details?";

       var  wnd = $("#endusermodalWindow").kendoWindow({
            title: "End User",
            modal: true,
            visible: false,
            resizable: false,
            width: 400
        }).data("kendoWindow");

        $("#div_enduserconfirm_message").text(message);
        wnd.center().open();

        $("#yes").click(function () {
            $("#MainContent_HiddenFieldExistClickOk").val('1');
            document.getElementById("MainContent_buttonbar_buttinSave").click();
            wnd.close();
        });

        $("#no").click(function () {
            $("#MainContent_HiddenFieldExistClickOk").val('0');
            //document.getElementById("MainContent_buttonbar_buttinSave").click();
            wnd.close();
        });
    }

    function saveconfirmationmsg() {
        $("#save").css("display", "none");
        $("#no").css("display", "inline-block");
        $("#yes").css("display", "inline-block");

        var message = "Do you want to approve the customer changes?";

        var wnd = $("#customermodalWindow").kendoWindow({
            title: "Customer Modification",
            modal: true,
            visible: false,
            resizable: false,
            width: 400
        }).data("kendoWindow");

        $("#div_customerconfirm_message").text(message);
        wnd.center().open();

        $("#yes").click(function () {
            $("#MainContent_HiddenFieldSaveClickOk").val('1');
            document.getElementById("MainContent_buttonbar_buttonApprove").click();
            wnd.close();
        });

        $("#no").click(function () {
            $("#MainContent_HiddenFieldSaveClickOk").val('0');
            //document.getElementById("MainContent_buttonbar_buttinSave").click();
            wnd.close();
        });
    }

    function showenduserexist(endusername) {

        $("#ok").css("display", "inline-block");
            var message = endusername ;
    //    var message = "This End User already exists as '" + endusername + "' for the current distributor.";

        var wnd = $("#endusermodalWindow2").kendoWindow({
            title: "End User",
            modal: true,
            visible: false,
            resizable: false,
            width: 400
        }).data("kendoWindow");

        $("#div_enduserok_message").text(message);
        wnd.center().open();


        $("#ok").click(function () {
           // $("#MainContent_HiddenFieldExistClickOk").val('0');
            //document.getElementById("MainContent_buttonActiveDeactive").click();
            wnd.close();
        });
    }

function showotherenduserexistmsg(strmessage){
        $("#save").css("display", "none");
         $("#no").css("display", "inline-block");
        $("#yes").css("display", "inline-block");

        var message="This End User already exists as '" + endusername + "' for the current distributor. Do you want to overwrite the details?";

       var  wnd = $("#endusermodalWindow").kendoWindow({
            title: "End User",
            modal: true,
            visible: false,
            resizable: false,
            width: 400
        }).data("kendoWindow");

        $("#div_enduserconfirm_message").text(message);
        wnd.center().open();

        $("#yes").click(function () {
            $("#MainContent_HiddenFieldExistClickOk").val('1');
            document.getElementById("MainContent_buttonActiveDeactive").click();
            wnd.close();
        });

        $("#no").click(function () {
            $("#MainContent_HiddenFieldExistClickOk").val('0');
            //document.getElementById("MainContent_buttonActiveDeactive").click();
            wnd.close();
        });
}

function showrepexistmsg(repName,tempmsg) {
        $("#save").css("display", "none");
        $("#no").css("display", "inline-block");
        $("#yes").css("display", "inline-block");

        //var message="This route has already assigned '" + repName + "' as distribution representative. Do you want to overwrite the details "+ tempmsg +" ?";
        var message="This route has already assigned '" + repName + "' as distribution representative. Do you want to assign this route ?";

       var  wnd = $("#repmodalWindow").kendoWindow({
            title: "Route Assign",
            modal: true,
            visible: false,
            resizable: false,
            width: 400
        }).data("kendoWindow");

        $("#div_repconfirm_message").text(message);
        wnd.center().open();

        $("#yes").click(function () {
            $("#MainContent_HiddenFieldExistClickOk").val('1');
            document.getElementById("MainContent_buttonbar_buttinSave").click();
            wnd.close();
        });

        $("#no").click(function () {
            $("#MainContent_HiddenFieldExistClickOk").val('0');
            //document.getElementById("MainContent_buttonbar_buttinSave").click();
            wnd.close();
        });
    }

    function showroutetempexist(tempName,tempmsg) {
        $("#save").css("display", "none");
        $("#no").css("display", "inline-block");
        $("#yes").css("display", "inline-block");

        var message="This DR already has template '" + tempName + "' . Do you want to Save as a new teplate ?";

       var  wnd = $("#schedulemodalWindow").kendoWindow({
            title: "Schedule Entry",
            modal: true,
            visible: false,
            resizable: false,
            width: 400
        }).data("kendoWindow");

        $("#div_repconfirm_message").text(message);
        wnd.center().open();

        $("#yes").click(function () {
            $("#MainContent_HiddenFieldExistClickOk").val('1');
            document.getElementById("MainContent_buttonbar_buttinSave").click();
            wnd.close();
        });

        $("#no").click(function () {
            $("#MainContent_HiddenFieldExistClickOk").val('0');
            //document.getElementById("MainContent_buttonbar_buttinSave").click();
            wnd.close();
        });
    }

//function getColumns(columns) {
//var columnArray = [];

//columnArray.push({ 
//                    field: "IsSelected", 
//                    sortable: false, 
//                    width: "30px", 
//                    title: "&nbsp;", 
//                    headerTemplate: '<p align="center"><input id="headerSelect" type="checkbox" /></p>', 
//                    template: '<p align="center" # if(IsReadOnly === "True") { # title="ColumnTitle" # } # /> <input type="checkbox" id="#=KeyField#" # if(IsReadOnly === "True") { # disabled /> # } else { if(IsSelected === "True") { # checked="checked" /> # } else { # /> # } if(saveItem(KeyField)){} } # </p>' 
//                });


//$.each(columns, function (index, value) {
//    var parameters = value.split('|');
//    if (parameters[2] == 'False')
//        columnArray.push({ 
//            field: parameters[0], 
//            title: parameters[1], 
//            template: '<p title=\'#=' + parameters[0] + '#\' # if(IsReadOnly === "True") { # style="color:gray;white-space:nowrap;" # }# style="white-space:nowrap;">#=' + parameters[0] + '#</p>' });
//    else
//        columnArray.push({ 
//            field: parameters[0], 
//            title: parameters[1],
//             hidden: true });
//});
//            return columnArray;
//}

/*End End User Details*/



function hideStatusDiv(targetstatusdiv) {    
    setTimeout(function () {
        $('#' + targetstatusdiv).slideUp('slow');
    }, 5000);
}

function loaddistributorLookup(griddiv, windowdiv, status) {

var link = "javascript:setDistributorCode(\'#=Code#\');"
if (status =="1"){
    link = "javascript:setDistributorCodeName(\'#=Code#\',\'#=Description#\');"
}
else if (status =="2"){
    link = "javascript:setCustCodeName(\'#=Code#\',\'#=Description#\');"
}
else if (status =="3"){
    link = "javascript:setDistributorCodeToEnduser(\'#=Code#\',\'#=Description#\');"
}
    $("#" + griddiv).html("");
    $("#" + griddiv).kendoGrid({
        height: 350,
        columns: [
            { template: '<a href="'+link+'">#=Code#</a>', field: "Code", title: "Customer Code", width: "85px" },
            { field: "Description", title: "Name", width: "80px" }
        ],
        editable: false, // enable editing
        pageable: false,
        sortable: true,
        filterable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "Originator",
                    fields: {
                        Code: { type: "string" },
                        Description: { type: "string" }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetDistributorCodes", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    //data: { prodWithNoSales: prodWithNoSales, activeOnly: activeOnly },
                    type: "POST" ,//use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#newcust_window").data("kendoWindow").open();
                            $("div#div_loader").hide();
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

//------------------------------------------------

function loadcustomerLookup(griddiv, windowdiv, status) {
   var link = "javascript:setCustomerRepCode(\'#=Code#\');"

    $("#" + griddiv).html("");
    $("#" + griddiv).kendoGrid({
        height: 350,
        columns: [
            { template: '<a href="' + link + '">#=Code#</a>', field: "Code", title: "Originator", width: "85px" },
            { template: '<a href="' + link + '">#=Description#</a>', field: "Description", title: "Name", width: "80px" }
        ],
        editable: false, // enable editing
        pageable: false,
        sortable: true,
        filterable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "Originator",
                    fields: {
                        Code: { type: "string" },
                        Description: { type: "string" }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetCustomerRepCodes", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    //data: { prodWithNoSales: prodWithNoSales, activeOnly: activeOnly },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#newcust_window").data("kendoWindow").open();
                            $("div#div_loader").hide();
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
} 


//---------------------------------------------------

        
function setDistributorCode(cust_code) {
    $('#txtnewcustcode').val(cust_code);
    $('#HiddenCustCode').val(cust_code); 
    $("#newcust_window").data("kendoWindow").close();
}

function setDistributorCodeName(cust_code,cust_name) {
    $('#txtnewcustcode').val(cust_code+" - "+cust_name);
    $('#HiddenCustCode').val(cust_code); 
    $("#newcust_window").data("kendoWindow").close();
}

function setCustCodeName(cust_code,cust_name) {
    $('#HiddenFieldEndUser_CustCode').val(cust_code); 
    $('#MainContent_txtCustomerName').val(cust_name); 
    $("#newcust_window").data("kendoWindow").close();
}
        
function setEndUserCode(cust_code) {
    $('#MainContent_buttonbar1_txtEndUser').val(cust_code);
    $("#newcust_window").data("kendoWindow").close();
}

function setEndUserCodeAndName(cust_code,name) {
    $('#HiddenFieldEndUser_EndUsercode').val(cust_code);
    $('#MainContent_txtEnduser').val(name);             
    $("#newcust_window").data("kendoWindow").close();
}
function setDistributorCodeToEnduser(cust_code, description) {
    $('#MainContent_TextBoxDist').val(description);
    $('#MainContent_HiddenFieldCustomerName').val(description);
    $('#MainContent_HiddenFieldCustomerCode').val(cust_code);
    $("#newcust_window").data("kendoWindow").close();
}

function setCustomerRepCode(rep_code) {
    $('#MainContent_txtAssignedTo').val(rep_code);
    $("#newcust_window").data("kendoWindow").close();
}



function loadEndUserLookup(griddiv, windowdiv, custCode, status) {
    var link = "javascript:setEndUserCode(\'#=Code#\');"
    if (status =="1"){
        link = "javascript:setEndUserCodeAndName(\'#=Code#\',\'#=Description#\');"
    }

    $("#" + griddiv).html("");
    $("#" + griddiv).kendoGrid({
        height: 350,
        columns: [
            { template: '<a href="'+link+'">#=Code#</a>', field: "Code", title: "Customer Code", width: "85px" },
            { field: "Description", title: "Name", width: "80px" }
        ],
        editable: false, // enable editing
        pageable: false,
        sortable: true,
        filterable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,


            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "Originator",
                    fields: {
                        Code: { type: "string" },
                        Description: { type: "string" }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH+ "service/lead_customer/common.asmx/GetEndUserLookup", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { custCode: custCode },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#newcust_window").data("kendoWindow").open();
                            $("div#div_loader").hide();
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

        
function GridCallCyclesGraph(id) {
$("#gridCallCyclesGraph").html("");
$("#gridCallCyclesGraph").kendoGrid({
    height: 400,
    columns: [
        { field: "CallCycleID", width: "85px", hidden: true },
        { field: "SourceId", width: "85px", hidden: true },
        { field: "Description", title: "Call Cycle", width: "150px" },
        { field: "DueOn", title: "Due On", format: DATE_FORMAT3 },
        { field: "Name", title: "Organisation" },
        { field: "Originator", title: "Rep" },
        { field: "LeadStage", title: "Lead Stage" },
        { field: "Address", title: "Address" },
        { field: "City", title: "City" },
        { field: "State", title: "State" },
        { field: "PostalCode", title: "Post Code" }

    ],
    editable: false, // enable editing
    pageable: true,
    sortable: true,
    dataSource: {
        serverSorting: true,
        serverPaging: true,
        serverFiltering: true,
        pageSize: 10,
        schema: {
            data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
            total: "d.Total",
            model: { // define the model of the data source. Required for validation and property types.
                id: "CallCycleID",
                fields: {
                    CallCycleID: { validation: { required: true} },
                    SourceId: { validation: { required: true} },
                    Description: { editable: true, nullable: true },
                    DueOn: { type: "Date",validation: { required: true} },
                    Name: { validation: { required: true} },
                    Originator: { validation: { required: true} },
                    LeadStage: { validation: { required: true} },
                    Address: { validation: { required: true} },
                    City: {  validation: { required: true} },
                    State: { validation: { required: true} },
                    PostalCode: { validation: { required: true} }
                }
            }
        },
        transport: {

            read: {
                url: ROOT_PATH + "Service/lead_customer/common.asmx/GetAllCallCycles", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                data: { id: id },
                type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
            },
            parameterMap: function (data, operation) {
                if (operation != "read") {
                    return JSON.stringify({ products: data.models })
                } else {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    }
});
}


function GridLeadCustOppCount(rep) {
    $("#gridLeadCustOppCount").html("");
    $("#gridLeadCustOppCount").kendoGrid({
        height: 200,
        columns: [
            { template: '<a href="javascript:GridLeadCustOpp(\'#=Originator#\')">#=Originator#</a>',
                field: "Originator", title: "Rep"
            },
            { field: "Amount", title: "Amount" ,format: "${0:n2}",attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">Amount</div>"},
            { field: "Tonnes", title: "Units" ,format: "{0:n2}",attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">Units</div>"},
            { field: "Ratio", title: "Conversion Ratio" ,format: "{0:n2}",attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">Conversion Ratio</div>"}

        ],
        editable: false, // enable editing
        pageable: false,
        sortable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "Originator",
                    fields: {
                        Originator: { validation: { required: true} },
                        Amount: { validation: { required: true},type: "number" },
                        Tonnes: { validation: { required: true},type: "number"},
                        Ratio: { validation: { required: true},type: "number" }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH+  "Service/lead_customer/common.asmx/GetLeaderCustomerOpportunityCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { rep: rep },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function GridLeadCustOpp(rep) {
    $("#gridLeadCustOpp").html("");
    $("#gridLeadCustOpp").kendoGrid({
        height: 400,
        columns: [
            { field: "OpportunityId", hidden: true },
            { field: "LeadId", hidden: true },
            { field: "CustomerCode", hidden: true },

            { template: '<a href="javascript:OpenLeadOpportunity(\'#=OpportunityId#\',\'#=LeadId#\',\'#=CustomerCode#\',\'dsbd2\')">#=Originator#</a>',
                field: "Originator", title: "Rep"
            },

//                    { field: "Originator", title: "Rep" },
            { field: "Opportunity", title: "Opportunity", width: "150px" },
            { field: "RepName", title: "Rep Name" },
            { field: "Stage", title: "Status" },
            { field: "CreatedDate", title: "Created Date", format: DATE_FORMAT3 },
            { field: "Amount", title: "Amount" ,format: "${0:n2}",attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">Amount</div>"},
            { field: "Tonnes", title: "Units" ,format: "{0:n0}",attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">Units</div>"},
            { field: "State", title: "State" },
            { field: "MarketDescription", title: "Market" }

        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "Originator",
                    fields: {
                        Originator: { validation: { required: true} },
                        Opportunity: { validation: { required: true} },
                        RepName: { validation: { required: true} },
                        Stage: { validation: { required: true} },

                        CreatedDate: { type: "Date", validation: { required: true} },
                        Amount: { validation: { required: true} },
                        Tonnes: { validation: { required: true} },
                        State: { validation: { required: true} },
                        MarketDescription: { validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH+  "Service/lead_customer/common.asmx/GetLeaderCustomerOpportunity", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { rep: rep },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function GetDateHeader(startDate,monthId) {
    var monthname = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
    var basemonth = startDate.getMonth() + monthId;
    var baseyear = startDate.getFullYear();
    startDate = new Date(baseyear, basemonth, 1);
    basemonth = startDate.getMonth();
    baseyear = startDate.getFullYear();
    return monthname[basemonth] + " <br /> " + baseyear;
}

function GridEndUserEnquiry(month, year, prodWithNoSales, activeOnly) {
    var sumtemp = "<div style =\"text-align: right;\">#=sum#</div>";
    var basedate = new Date(year, 6, 1);
    var basemonth = basedate.getMonth() ;

    var StartDate = new Date(year, basemonth + parseInt(month) - 12, 1);

    basemonth = StartDate.getMonth();
    year = StartDate.getFullYear();
	StartDate = new Date(year, basemonth - 12, 1);

	$("#gridEndUserEnquiry").html("");
	$("#gridEndUserEnquiry").kendoGrid({
        height: 700,
        columns: [
            { field: "CustomerDisplay", hidden: true, groupHeaderTemplate: "#= value #" },
            { field: "EndUserDisplay", hidden: true, groupHeaderTemplate: "#= value #" },

            { field: "CustomerCode", hidden: true },
            { field: "CustomerName", hidden: true },
            { field: "EndUserCode", hidden: true },
            { field: "EndUserName", hidden: true },

            { field: "CatlogCode", title: "Catalog Code", width: "95px" ,groupFooterTemplate: "Total: "},
            { field: "Description", title: "Description", width: "150px" },
            { field: "Price", title: "Price", width: "60px" ,format: "{0:n2}",attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">Price</div>"},
            { field: "CurPrice", title: "Cur. Price", width: "80px" ,format: "{0:n2}",attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">Cur. Price</div>"},

            { field: "Cases1", groupFooterTemplate: sumtemp ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">"+GetDateHeader(StartDate,0)+"</div>"},
            { field: "Cases2", groupFooterTemplate: sumtemp ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">"+GetDateHeader(StartDate,1)+"</div>"},
            { field: "Cases3", groupFooterTemplate: sumtemp ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">"+GetDateHeader(StartDate,2)+"</div>"},
            { field: "Cases4", groupFooterTemplate: sumtemp ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">"+GetDateHeader(StartDate,3)+"</div>"},
            { field: "Cases5", groupFooterTemplate: sumtemp ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">"+GetDateHeader(StartDate,4)+"</div>"},
            { field: "Cases6", groupFooterTemplate: sumtemp ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">"+GetDateHeader(StartDate,5)+"</div>"},
            { field: "Cases7", groupFooterTemplate: sumtemp ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">"+GetDateHeader(StartDate,6)+"</div>"},
            { field: "Cases8", groupFooterTemplate: sumtemp ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">"+GetDateHeader(StartDate,7)+"</div>"},
            { field: "Cases9", groupFooterTemplate: sumtemp ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">"+GetDateHeader(StartDate,8)+"</div>"},
            { field: "Cases10", groupFooterTemplate: sumtemp ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">"+GetDateHeader(StartDate,9)+"</div>"},
            { field: "Cases11", groupFooterTemplate: sumtemp ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">"+GetDateHeader(StartDate,10)+"</div>"},
            { field: "Cases12", groupFooterTemplate: sumtemp ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">"+GetDateHeader(StartDate,11)+"</div>"},

            { field: "TotalCases", width: "80px", groupFooterTemplate: sumtemp ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">Total Cases</div>" }

        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 1000,


            group: [{ field: "CustomerDisplay", aggregates: [
                                { field: "TotalCases", aggregate: "sum" },
                                { field: "Cases1", aggregate: "sum" },
                                { field: "Cases2", aggregate: "sum" },
                                { field: "Cases3", aggregate: "sum" },
                                { field: "Cases4", aggregate: "sum" },
                                { field: "Cases5", aggregate: "sum" },
                                { field: "Cases6", aggregate: "sum" },
                                { field: "Cases7", aggregate: "sum" },
                                { field: "Cases8", aggregate: "sum" },
                                { field: "Cases9", aggregate: "sum" },
                                { field: "Cases10", aggregate: "sum" },
                                { field: "Cases11", aggregate: "sum" },
                                { field: "Cases12", aggregate: "sum" }
                                ]
            }, { field: "EndUserDisplay", aggregates: [
                                { field: "TotalCases", aggregate: "sum" },
                                { field: "Cases1", aggregate: "sum" },
                                { field: "Cases2", aggregate: "sum" },
                                { field: "Cases3", aggregate: "sum" },
                                { field: "Cases4", aggregate: "sum" },
                                { field: "Cases5", aggregate: "sum" },
                                { field: "Cases6", aggregate: "sum" },
                                { field: "Cases7", aggregate: "sum" },
                                { field: "Cases8", aggregate: "sum" },
                                { field: "Cases9", aggregate: "sum" },
                                { field: "Cases10", aggregate: "sum" },
                                { field: "Cases11", aggregate: "sum" },
                                { field: "Cases12", aggregate: "sum" }
                                ]
            }],

            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "Originator",
                    fields: {
                        CustomerCode: { validation: { required: true} },
                        CustomerName: { validation: { required: true} },
                        EndUserCode: { validation: { required: true} },
                        EndUserName: { validation: { required: true} },

                        CatlogCode: { validation: { required: true} },
                        Description: { validation: { required: true} },
                        Price: { validation: { required: true} },
                        CurPrice: { validation: { required: true} },

                        Cases1: { validation: { required: true} },
                        Cases2: { validation: { required: true} },
                        Cases3: { validation: { required: true} },
                        Cases4: { validation: { required: true} },
                        Cases5: { validation: { required: true} },
                        Cases6: { validation: { required: true} },
                        Cases7: { validation: { required: true} },
                        Cases8: { validation: { required: true} },
                        Cases9: { validation: { required: true} },
                        Cases10: { validation: { required: true} },
                        Cases11: { validation: { required: true} },
                        Cases12: { validation: { required: true} },

                        TotalCases: { validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "Service/lead_customer/common.asmx/GetEnduserSalesWithAllProducts", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { prodWithNoSales: prodWithNoSales, activeOnly: activeOnly },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function GridEoiSales() {
    $("#gridEoiSales").html("");
    $("#gridEoiSales").kendoGrid({
        height: 700,
        columns: [


            { field: "CostYear", hidden: true, groupHeaderTemplate: "#= value #" },
            { field: "Description", title: " ", width: "265px" },

            { field: "Cases1", title: "JUL", width: "60px" ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">JUL</div>"},
            { field: "Cases2", title: "AUG", width: "60px" ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">AUG</div>"},
            { field: "Cases3", title: "SEPT", width: "60px" ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">SEPT</div>"},
            { field: "Quarter1", title: "QTR 1", width: "60px", attributes: { class: "eossales"} ,headerTemplate: "<div style =\"text-align: right;\">QTR 1</div>"},

            { field: "Cases4", title: "OCT", width: "60px" ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">OCT</div>"},
            { field: "Cases5", title: "NOV", width: "60px" ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">NOV</div>"},
            { field: "Cases6", title: "DEC", width: "60px" ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">DEC</div>"},
            { field: "Quarter2", title: "QTR 2", width: "60px", attributes: { class: "eossales"},headerTemplate: "<div style =\"text-align: right;\">QTR 2</div>" },
            { field: "MidYearSales", title: "QTR 1-2", width: "70px", attributes: { class: "eossales"} ,headerTemplate: "<div style =\"text-align: right;\">QTR 1-2</div>"},

            { field: "Cases7", title: "JAN", width: "60px" ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">JAN</div>"},
            { field: "Cases8", title: "FEB", width: "60px" ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">FEB</div>"},
            { field: "Cases9", title: "MAR", width: "60px" ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">MAR</div>"},
            { field: "Quarter3", title: "QTR 3", width: "60px", attributes: { class: "eossales"} ,headerTemplate: "<div style =\"text-align: right;\">QTR 3</div>"},
            { field: "ThirdQtrSales", title: "QTR 1-2-3", width: "85px", attributes: { class: "eossales"} ,headerTemplate: "<div style =\"text-align: right;\">QTR 1-2-3</div>"},

            { field: "Cases10", title: "APR", width: "60px" ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">APR</div>"},
            { field: "Cases11", title: "MAY", width: "60px" ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">MAY</div>"},
            { field: "Cases12", title: "JUN", width: "60px" ,attributes: { style: "text-align: right"},headerTemplate: "<div style =\"text-align: right;\">JUN</div>"},
            { field: "Quarter4", title: "QTR 4", width: "60px", attributes: { class: "eossales"} ,headerTemplate: "<div style =\"text-align: right;\">QTR 4</div>"},
            { field: "EndOfYearSales", title: "YEAR", width: "60px", attributes: { class: "eossales"} ,headerTemplate: "<div style =\"text-align: right;\">YEAR</div>"}

        ],
        editable: false, // enable editing
        pageable: false,
        sortable: false,
        resizable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 1000,
            resizable: true,

            group: [{ field: "CostYear", attributes: { class: "homegridheader", style: "color:\\#0c0"}}],
                    

            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "Originator",
                    fields: {
                                
                        Description: { validation: { required: true} },

                        Cases1: { validation: { required: true} ,type: "number"},
                        Cases2: { validation: { required: true} ,type: "number"},
                        Cases3: { validation: { required: true} ,type: "number"},
                        Quarter1: { validation: { required: true} ,type: "number"},
                        Cases4: { validation: { required: true} ,type: "number"},
                        Cases5: { validation: { required: true} ,type: "number"},
                        Cases6: { validation: { required: true} ,type: "number"},
                        Quarter2: { validation: { required: true} ,type: "number"},
                        MidYearSales: { validation: { required: true} ,type: "number"},
                        Cases7: { validation: { required: true} ,type: "number"},
                        Cases8: { validation: { required: true} ,type: "number"},
                        Cases9: { validation: { required: true} ,type: "number"},
                        Quarter3: { validation: { required: true} ,type: "number"},
                        ThirdQtrSales: { validation: { required: true} ,type: "number"},
                        Cases10: { validation: { required: true} ,type: "number"},
                        Cases11: { validation: { required: true} ,type: "number"},
                        Cases12: { validation: { required: true} ,type: "number"},
                        Quarter3: { validation: { required: true} ,type: "number"},
                        EndOfYearSales: { validation: { required: true} ,type: "number"}
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH+  "Service/lead_customer/common.asmx/GetEOISales", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    //data: { prodWithNoSales: prodWithNoSales, activeOnly: activeOnly },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

/*Start Numeric Script Pack */
(function ($) { function getElementSelection(that) { var position = {}; if (that.selectionStart === undefined) { that.focus(); var select = document.selection.createRange(); position.length = select.text.length; select.moveStart("character", -that.value.length); position.end = select.text.length; position.start = position.end - position.length; } else { position.start = that.selectionStart; position.end = that.selectionEnd; position.length = position.end - position.start; } return position; } function setElementSelection(that, start, end) { if (that.selectionStart === undefined) { that.focus(); var r = that.createTextRange(); r.collapse(true); r.moveEnd("character", end); r.moveStart("character", start); r.select(); } else { that.selectionStart = start; that.selectionEnd = end; } } function runCallbacks($this, io) { $.each(io, function (k, val) { if (typeof (val) === "function") { io[k] = val($this, io, k); } else { if (typeof (val) === "string") { var kind = val.substr(0, 4); if (kind === "fun:") { var fun = $.autoNumeric[val.substr(4)]; if (typeof (fun) === "function") { io[k] = $.autoNumeric[val.substr(4)]($this, io, k); } else { io[k] = null; } } else { if (kind === "css:") { io[k] = $(val.substr(4)).val(); } } } } }); } function convertKeyToNumber(io, key) { if (typeof (io[key]) === "string") { io[key] *= 1; } } function autoCode($this, options) { var io = $.extend({}, $.fn.autoNumeric.defaults, options); if ($.metadata) { io = $.extend(io, $this.metadata()); } runCallbacks($this, io); var vmax = io.vMax.toString().split("."); var vmin = (!io.vMin && io.vMin !== 0) ? [] : io.vMin.toString().split("."); convertKeyToNumber(io, "vMax"); convertKeyToNumber(io, "vMin"); convertKeyToNumber(io, "mDec"); io.aNeg = io.vMin < 0 ? "-" : ""; if (typeof (io.mDec) !== "number") { io.mDec = Math.max((vmax[1] ? vmax[1] : "").length, (vmin[1] ? vmin[1] : "").length); } if (io.altDec === null && io.mDec > 0) { if (io.aDec === "." && io.aSep !== ",") { io.altDec = ","; } else { if (io.aDec === "," && io.aSep !== ".") { io.altDec = "."; } } } var aNegReg = io.aNeg ? "([-\\" + io.aNeg + "]?)" : "(-?)"; io._aNegReg = aNegReg; io._skipFirst = new RegExp(aNegReg + "[^-" + (io.aNeg ? "\\" + io.aNeg : "") + "\\" + io.aDec + "\\d].*?(\\d|\\" + io.aDec + "\\d)"); io._skipLast = new RegExp("(\\d\\" + io.aDec + "?)[^\\" + io.aDec + "\\d]\\D*$"); var allowed = (io.aNeg ? io.aNeg : "-") + io.aNum + "\\" + io.aDec; if (io.altDec && io.altDec !== io.aSep) { allowed += io.altDec; } io._allowed = new RegExp("[^" + allowed + "]", "gi"); io._numReg = new RegExp(aNegReg + "(?:\\" + io.aDec + "?(\\d+\\" + io.aDec + "\\d+)|(\\d*(?:\\" + io.aDec + "\\d*)?))"); return io; } function autoStrip(s, io, strip_zero) { if (io.aSign) { while (s.indexOf(io.aSign) > -1) { s = s.replace(io.aSign, ""); } } s = s.replace(io._skipFirst, "$1$2"); s = s.replace(io._skipLast, "$1"); s = s.replace(io._allowed, ""); if (io.altDec) { s = s.replace(io.altDec, io.aDec); } var m = s.match(io._numReg); s = m ? [m[1], m[2], m[3]].join("") : ""; if (strip_zero) { var strip_reg = "^" + io._aNegReg + "0*(\\d" + (strip_zero === "leading" ? ")" : "|$)"); strip_reg = new RegExp(strip_reg); s = s.replace(strip_reg, "$1$2"); } return s; } function truncateDecimal(s, aDec, mDec) { if (aDec && mDec) { var parts = s.split(aDec); if (parts[1] && parts[1].length > mDec) { if (mDec > 0) { parts[1] = parts[1].substring(0, mDec); s = parts.join(aDec); } else { s = parts[0]; } } } return s; } function fixNumber(s, aDec, aNeg) { if (aDec && aDec !== ".") { s = s.replace(aDec, "."); } if (aNeg && aNeg !== "-") { s = s.replace(aNeg, "-"); } if (!s.match(/\d/)) { s += "0"; } return s; } function presentNumber(s, aDec, aNeg) { if (aNeg && aNeg !== "-") { s = s.replace("-", aNeg); } if (aDec && aDec !== ".") { s = s.replace(".", aDec); } return s; } function autoCheck(s, io) { s = autoStrip(s, io); s = truncateDecimal(s, io.aDec, io.mDec); s = fixNumber(s, io.aDec, io.aNeg); var value = +s; return value >= io.vMin && value <= io.vMax; } function checkEmpty(iv, io, signOnEmpty) { if (iv === "" || iv === io.aNeg) { if (io.wEmpty === "zero") { return iv + "0"; } else { if (io.wEmpty === "sign" || signOnEmpty) { return iv + io.aSign; } else { return iv; } } } return null; } function autoGroup(iv, io) { iv = autoStrip(iv, io); var empty = checkEmpty(iv, io, true); if (empty !== null) { return empty; } var digitalGroup = ""; if (io.dGroup === 2) { digitalGroup = /(\d)((\d)(\d{2}?)+)$/; } else { if (io.dGroup === 4) { digitalGroup = /(\d)((\d{4}?)+)$/; } else { digitalGroup = /(\d)((\d{3}?)+)$/; } } var ivSplit = iv.split(io.aDec); if (io.altDec && ivSplit.length === 1) { ivSplit = iv.split(io.altDec); } var s = ivSplit[0]; if (io.aSep) { while (digitalGroup.test(s)) { s = s.replace(digitalGroup, "$1" + io.aSep + "$2"); } } if (io.mDec !== 0 && ivSplit.length > 1) { if (ivSplit[1].length > io.mDec) { ivSplit[1] = ivSplit[1].substring(0, io.mDec); } iv = s + io.aDec + ivSplit[1]; } else { iv = s; } if (io.aSign) { var has_aNeg = iv.indexOf(io.aNeg) !== -1; iv = iv.replace(io.aNeg, ""); iv = io.pSign === "p" ? io.aSign + iv : iv + io.aSign; if (has_aNeg) { iv = io.aNeg + iv; } } return iv; } function autoRound(iv, mDec, mRound, aPad) { iv = (iv === "") ? "0" : iv.toString(); var ivRounded = ""; var i = 0; var nSign = ""; var rDec = (typeof (aPad) === "boolean" || aPad === null) ? (aPad ? mDec : 0) : +aPad; var truncateZeros = function (ivRounded) { var regex = rDec === 0 ? (/(\.[1-9]*)0*$/) : rDec === 1 ? (/(\.\d[1-9]*)0*$/) : new RegExp("(\\.\\d{" + rDec + "}[1-9]*)0*$"); ivRounded = ivRounded.replace(regex, "$1"); if (rDec === 0) { ivRounded = ivRounded.replace(/\.$/, ""); } return ivRounded; }; if (iv.charAt(0) === "-") { nSign = "-"; iv = iv.replace("-", ""); } if (!iv.match(/^\d/)) { iv = "0" + iv; } if (nSign === "-" && +iv === 0) { nSign = ""; } if ((+iv) > 0) { iv = iv.replace(/^0*(\d)/, "$1"); } var dPos = iv.lastIndexOf("."); var vdPos = dPos === -1 ? iv.length - 1 : dPos; var cDec = (iv.length - 1) - vdPos; if (cDec <= mDec) { ivRounded = iv; if (cDec < rDec) { if (dPos === -1) { ivRounded += "."; } while (cDec < rDec) { var zeros = "000000".substring(0, rDec - cDec); ivRounded += zeros; cDec += zeros.length; } } else { if (cDec > rDec) { ivRounded = truncateZeros(ivRounded); } else { if (cDec === 0 && rDec === 0) { ivRounded = ivRounded.replace(/\.$/, ""); } } } return nSign + ivRounded; } var rLength = dPos + mDec; var tRound = +iv.charAt(rLength + 1); var ivArray = iv.substring(0, rLength + 1).split(""); var odd = (iv.charAt(rLength) === ".") ? (iv.charAt(rLength - 1) % 2) : (iv.charAt(rLength) % 2); if ((tRound > 4 && mRound === "S") || (tRound > 4 && mRound === "A" && nSign === "") || (tRound > 5 && mRound === "A" && nSign === "-") || (tRound > 5 && mRound === "s") || (tRound > 5 && mRound === "a" && nSign === "") || (tRound > 4 && mRound === "a" && nSign === "-") || (tRound > 5 && mRound === "B") || (tRound === 5 && mRound === "B" && odd === 1) || (tRound > 0 && mRound === "C" && nSign === "") || (tRound > 0 && mRound === "F" && nSign === "-") || (tRound > 0 && mRound === "U")) { for (i = (ivArray.length - 1); i >= 0; i -= 1) { if (ivArray[i] !== ".") { ivArray[i] = +ivArray[i] + 1; if (ivArray[i] < 10) { break; } else { if (i > 0) { ivArray[i] = "0"; } } } } } ivArray = ivArray.slice(0, rLength + 1); ivRounded = truncateZeros(ivArray.join("")); return nSign + ivRounded; } function autoNumericHolder(that, options) { this.options = options; this.that = that; this.$that = $(that); this.formatted = false; this.io = autoCode(this.$that, this.options); this.value = that.value; } autoNumericHolder.prototype = { init: function (e) { this.value = this.that.value; this.io = autoCode(this.$that, this.options); this.ctrlKey = e.ctrlKey; this.cmdKey = e.metaKey; this.shiftKey = e.shiftKey; this.selection = getElementSelection(this.that); if (e.type === "keydown" || e.type === "keyup") { this.kdCode = e.keyCode; } this.which = e.which; this.processed = false; this.formatted = false; }, setSelection: function (start, end, setReal) { start = Math.max(start, 0); end = Math.min(end, this.that.value.length); this.selection = { start: start, end: end, length: end - start }; if (setReal === undefined || setReal) { setElementSelection(this.that, start, end); } }, setPosition: function (pos, setReal) { this.setSelection(pos, pos, setReal); }, getBeforeAfter: function () { var value = this.value; var left = value.substring(0, this.selection.start); var right = value.substring(this.selection.end, value.length); return [left, right]; }, getBeforeAfterStriped: function () { var parts = this.getBeforeAfter(); parts[0] = autoStrip(parts[0], this.io); parts[1] = autoStrip(parts[1], this.io); return parts; }, normalizeParts: function (left, right) { var io = this.io; right = autoStrip(right, io); var strip = right.match(/^\d/) ? true : "leading"; left = autoStrip(left, io, strip); if ((left === "" || left === io.aNeg)) { if (right > "") { right = right.replace(/^0*(\d)/, "$1"); } } var new_value = left + right; if (io.aDec) { var m = new_value.match(new RegExp("^" + io._aNegReg + "\\" + io.aDec)); if (m) { left = left.replace(m[1], m[1] + "0"); new_value = left + right; } } if (io.wEmpty === "zero" && (new_value === io.aNeg || new_value === "")) { left += "0"; } return [left, right]; }, setValueParts: function (left, right) { var io = this.io; var parts = this.normalizeParts(left, right); var new_value = parts.join(""); var position = parts[0].length; if (autoCheck(new_value, io)) { new_value = truncateDecimal(new_value, io.aDec, io.mDec); if (position > new_value.length) { position = new_value.length; } this.value = new_value; this.setPosition(position, false); return true; } return false; }, signPosition: function () { var io = this.io, aSign = io.aSign, that = this.that; if (aSign) { var aSignLen = aSign.length; if (io.pSign === "p") { var hasNeg = io.aNeg && that.value && that.value.charAt(0) === io.aNeg; return hasNeg ? [1, aSignLen + 1] : [0, aSignLen]; } else { var valueLen = that.value.length; return [valueLen - aSignLen, valueLen]; } } else { return [1000, -1]; } }, expandSelectionOnSign: function (setReal) { var sign_position = this.signPosition(); var selection = this.selection; if (selection.start < sign_position[1] && selection.end > sign_position[0]) { if ((selection.start < sign_position[0] || selection.end > sign_position[1]) && this.value.substring(Math.max(selection.start, sign_position[0]), Math.min(selection.end, sign_position[1])).match(/^\s*$/)) { if (selection.start < sign_position[0]) { this.setSelection(selection.start, sign_position[0], setReal); } else { this.setSelection(sign_position[1], selection.end, setReal); } } else { this.setSelection(Math.min(selection.start, sign_position[0]), Math.max(selection.end, sign_position[1]), setReal); } } }, checkPaste: function () { if (this.valuePartsBeforePaste !== undefined) { var parts = this.getBeforeAfter(); var oldParts = this.valuePartsBeforePaste; delete this.valuePartsBeforePaste; parts[0] = parts[0].substr(0, oldParts[0].length) + autoStrip(parts[0].substr(oldParts[0].length), this.io); if (!this.setValueParts(parts[0], parts[1])) { this.value = oldParts.join(""); this.setPosition(oldParts[0].length, false); } } }, skipAllways: function (e) { var kdCode = this.kdCode, which = this.which, ctrlKey = this.ctrlKey, cmdKey = this.cmdKey; if (kdCode === 17 && e.type === "keyup" && this.valuePartsBeforePaste !== undefined) { this.checkPaste(); return false; } if ((kdCode >= 112 && kdCode <= 123) || (kdCode >= 91 && kdCode <= 93) || (kdCode >= 9 && kdCode <= 31) || (kdCode < 8 && (which === 0 || which === kdCode)) || kdCode === 144 || kdCode === 145 || kdCode === 45) { return true; } if ((ctrlKey || cmdKey) && kdCode === 65) { return true; } if ((ctrlKey || cmdKey) && (kdCode === 67 || kdCode === 86 || kdCode === 88)) { if (e.type === "keydown") { this.expandSelectionOnSign(); } if (kdCode === 86) { if (e.type === "keydown" || e.type === "keypress") { if (this.valuePartsBeforePaste === undefined) { this.valuePartsBeforePaste = this.getBeforeAfter(); } } else { this.checkPaste(); } } return e.type === "keydown" || e.type === "keypress" || kdCode === 67; } if (ctrlKey || cmdKey) { return true; } if (kdCode === 37 || kdCode === 39) { var aSep = this.io.aSep, start = this.selection.start, value = this.that.value; if (e.type === "keydown" && aSep && !this.shiftKey) { if (kdCode === 37 && value.charAt(start - 2) === aSep) { this.setPosition(start - 1); } else { if (kdCode === 39 && value.charAt(start) === aSep) { this.setPosition(start + 1); } } } return true; } if (kdCode >= 34 && kdCode <= 40) { return true; } return false; }, processAllways: function () { var parts; if (this.kdCode === 8 || this.kdCode === 46) { if (!this.selection.length) { parts = this.getBeforeAfterStriped(); if (this.kdCode === 8) { parts[0] = parts[0].substring(0, parts[0].length - 1); } else { parts[1] = parts[1].substring(1, parts[1].length); } this.setValueParts(parts[0], parts[1]); } else { this.expandSelectionOnSign(false); parts = this.getBeforeAfterStriped(); this.setValueParts(parts[0], parts[1]); } return true; } return false; }, processKeypress: function () { var io = this.io; var cCode = String.fromCharCode(this.which); var parts = this.getBeforeAfterStriped(); var left = parts[0], right = parts[1]; if (cCode === io.aDec || (io.altDec && cCode === io.altDec) || ((cCode === "." || cCode === ",") && this.kdCode === 110)) { if (!io.mDec || !io.aDec) { return true; } if (io.aNeg && right.indexOf(io.aNeg) > -1) { return true; } if (left.indexOf(io.aDec) > -1) { return true; } if (right.indexOf(io.aDec) > 0) { return true; } if (right.indexOf(io.aDec) === 0) { right = right.substr(1); } this.setValueParts(left + io.aDec, right); return true; } if (cCode === "-" || cCode === "+") { if (!io.aNeg) { return true; } if (left === "" && right.indexOf(io.aNeg) > -1) { left = io.aNeg; right = right.substring(1, right.length); } if (left.charAt(0) === io.aNeg) { left = left.substring(1, left.length); } else { left = (cCode === "-") ? io.aNeg + left : left; } this.setValueParts(left, right); return true; } if (cCode >= "0" && cCode <= "9") { if (io.aNeg && left === "" && right.indexOf(io.aNeg) > -1) { left = io.aNeg; right = right.substring(1, right.length); } this.setValueParts(left + cCode, right); return true; } return true; }, formatQuick: function () { var io = this.io; var parts = this.getBeforeAfterStriped(); var value = autoGroup(this.value, this.io); var position = value.length; if (value) { var left_ar = parts[0].split(""); var i; for (i = 0; i < left_ar.length; i += 1) { if (!left_ar[i].match("\\d")) { left_ar[i] = "\\" + left_ar[i]; } } var leftReg = new RegExp("^.*?" + left_ar.join(".*?")); var newLeft = value.match(leftReg); if (newLeft) { position = newLeft[0].length; if (((position === 0 && value.charAt(0) !== io.aNeg) || (position === 1 && value.charAt(0) === io.aNeg)) && io.aSign && io.pSign === "p") { position = this.io.aSign.length + (value.charAt(0) === "-" ? 1 : 0); } } else { if (io.aSign && io.pSign === "s") { position -= io.aSign.length; } } } this.that.value = value; this.setPosition(position); this.formatted = true; } }; function getData($that) { var data = $that.data("autoNumeric"); if (!data) { data = {}; $that.data("autoNumeric", data); } return data; } function getHolder($that, options) { var data = getData($that); var holder = data.holder; if (holder === undefined && options) { holder = new autoNumericHolder($that.get(0), options); data.holder = holder; } return holder; } function getOptions($that) { var data = $that.data("autoNumeric"); if (data && data.holder) { return data.holder.options; } return {}; } function onInit(options) { options = options || {}; var iv = $(this), holder = getHolder(iv, options); if (holder.io.aForm && (this.value || holder.io.wEmpty !== "empty")) { iv.autoNumericSet(iv.autoNumericGet(options), options); } } function onKeyDown(e) { var iv = $(e.target), holder = getHolder(iv); holder.init(e); if (holder.skipAllways(e)) { holder.processed = true; return true; } if (holder.processAllways()) { holder.processed = true; holder.formatQuick(); e.preventDefault(); return false; } else { holder.formatted = false; } return true; } function onKeyPress(e) { var iv = $(e.target), holder = getHolder(iv); var processed = holder.processed; holder.init(e); if (holder.skipAllways(e)) { return true; } if (processed) { e.preventDefault(); return false; } if (holder.processAllways() || holder.processKeypress()) { holder.formatQuick(); e.preventDefault(); return false; } else { holder.formatted = false; } } function onKeyUp(e) { var iv = $(e.target), holder = getHolder(iv); holder.init(e); var skip = holder.skipAllways(e); holder.kdCode = 0; delete holder.valuePartsBeforePaste; if (skip) { return true; } if (this.value === "") { return true; } if (!holder.formatted) { holder.formatQuick(); } } function onFocusIn(e) { var iv = $(e.target), holder = getHolder(iv); holder.inVal = iv.val(); var onempty = checkEmpty(holder.inVal, holder.io, true); if (onempty !== null) { iv.val(onempty); } } function onFocusOut(e) { var iv = $(e.target), holder = getHolder(iv); var io = holder.io, value = iv.val(), origValue = value; if (value !== "") { value = autoStrip(value, io); if (checkEmpty(value, io) === null && autoCheck(value, io)) { value = fixNumber(value, io.aDec, io.aNeg); value = autoRound(value, io.mDec, io.mRound, io.aPad); value = presentNumber(value, io.aDec, io.aNeg); } else { value = ""; } } var groupedValue = checkEmpty(value, io, false); if (groupedValue === null) { groupedValue = autoGroup(value, io); } if (groupedValue !== origValue) { iv.val(groupedValue); } if (groupedValue !== holder.inVal) { iv.change(); delete holder.inVal; } } $.fn.autoNumeric = function (options) { return this.each(function () { onInit.call(this, options); }).unbind(".autoNumeric").bind({ "keydown.autoNumeric": onKeyDown, "keypress.autoNumeric": onKeyPress, "keyup.autoNumeric": onKeyUp, "focusin.autoNumeric": onFocusIn, "focusout.autoNumeric": onFocusOut }); }; function autoGet(obj) { if (typeof (obj) === "string") { obj = obj.replace(/\[/g, "\\[").replace(/\]/g, "\\]"); obj = "#" + obj.replace(/(:|\.)/g, "\\$1"); } return $(obj); } $.autoNumeric = {}; $.autoNumeric.Strip = function (ii) { var $that = autoGet(ii); var options = getOptions($that); if (arguments[1] && typeof (arguments[1]) === "object") { options = $.extend({}, options, arguments[1]); } var io = autoCode($that, options); var iv = autoGet(ii).val(); iv = autoStrip(iv, io); iv = fixNumber(iv, io.aDec, io.aNeg); if (+iv === 0) { iv = "0"; } return iv; }; $.autoNumeric.Format = function (ii, iv) { var $that = autoGet(ii); var options = getOptions($that); if (arguments[2] && typeof (arguments[2]) === "object") { options = $.extend({}, options, arguments[2]); } iv.toString(); var io = autoCode($that, options); iv = autoRound(iv, io.mDec, io.mRound, io.aPad); iv = presentNumber(iv, io.aDec, io.aNeg); if (!autoCheck(iv, io)) { iv = autoRound("", io.mDec, io.mRound, io.aPad); } return autoGroup(iv, io); }; $.fn.autoNumericGet = function () { if (arguments[0]) { return $.autoNumeric.Strip(this, arguments[0]); } return $.autoNumeric.Strip(this); }; $.fn.autoNumericSet = function (iv) { if (arguments[1]) { return this.val($.autoNumeric.Format(this, iv, arguments[1])); } return this.val($.fn.autoNumeric.Format(this, iv)); }; $.autoNumeric.defaults = { aNum: "0123456789", aSep: ",", dGroup: "3", aDec: ".", altDec: null, aSign: "", pSign: "p", vMax: "999999999.99", vMin: "0.00", mDec: null, mRound: "S", aPad: true, wEmpty: "empty", aForm: false }; $.fn.autoNumeric.defaults = $.autoNumeric.defaults; $.fn.autoNumeric.Strip = $.autoNumeric.Strip; $.fn.autoNumeric.Format = $.autoNumeric.Format; })(jQuery);
/*Start Allow only Numeric value*/

/*Start Numeric Script Pack */
eval(function (p, a, c, k, e, r) { e = function (c) { return (c < a ? '' : e(parseInt(c / a))) + ((c = c % a) > 35 ? String.fromCharCode(c + 29) : c.toString(36)) }; if (!''.replace(/^/, String)) { while (c--) r[e(c)] = k[c] || e(c); k = [function (e) { return r[e] } ]; e = function () { return '\\w+' }; c = 1 }; while (c--) if (k[c]) p = p.replace(new RegExp('\\b' + e(c) + '\\b', 'g'), k[c]); return p } ('r.E.W=7(c,d){c=c||".";d=q d=="7"?d:7(){};6.K(7(e){g a=e.i?e.i:e.h?e.h:0;2(a==k&&6.N.J()=="G"){5 3}f 2(a==k){5 j}g b=j;2((e.4&&a==y)||(e.4&&a==v))5 3;2((e.4&&a==t)||(e.4&&a==u))5 3;2((e.4&&a==V)||(e.4&&a==S))5 3;2((e.4&&a==R)||(e.4&&a==Q))5 3;2((e.4&&a==P)||(e.4&&a==O)||(e.L&&a==p))5 3;2(a<I||a>H){2(a==p&&6.l.F==0)5 3;2(a==c.n(0)&&6.l.o(c)!=-1){b=j}2(a!=8&&a!=9&&a!=k&&a!=D&&a!=C&&a!=M&&a!=B&&a!=A){b=j}f{2(q e.i!="z"){2(e.h==e.m&&e.m!=0){b=3}f 2(e.h!=0&&e.i==0&&e.m==0){b=3}}}2(a==c.n(0)&&6.l.o(c)==-1){b=3}}f{b=3}5 b}).x(7(){g a=r(6).w();2(a!=""){g b=T U("^\\\\d+$|\\\\d*"+c+"\\\\d+");2(!b.s(a)){d.X(6)}}});5 6}', 60, 60, '||if|true|ctrlKey|return|this|function||||||||else|var|keyCode|charCode|false|13|value|which|charCodeAt|indexOf|45|typeof|jQuery|exec|120|88|65|val|blur|97|undefined|46|39|36|35|fn|length|input|57|48|toLowerCase|keypress|shiftKey|37|nodeName|86|118|90|122|67|new|RegExp|99|numeric|apply'.split('|'), 0, {}))
/*End Numeric Script Pack */


/*Start Number formatter*/
function NumberFormat(num, inputDecimal) { this.VERSION = "Number Format v1.5.4"; this.COMMA = ","; this.PERIOD = "."; this.DASH = "-"; this.LEFT_PAREN = "("; this.RIGHT_PAREN = ")"; this.LEFT_OUTSIDE = 0; this.LEFT_INSIDE = 1; this.RIGHT_INSIDE = 2; this.RIGHT_OUTSIDE = 3; this.LEFT_DASH = 0; this.RIGHT_DASH = 1; this.PARENTHESIS = 2; this.NO_ROUNDING = -1; this.num; this.numOriginal; this.hasSeparators = false; this.separatorValue; this.inputDecimalValue; this.decimalValue; this.negativeFormat; this.negativeRed; this.hasCurrency; this.currencyPosition; this.currencyValue; this.places; this.roundToPlaces; this.truncate; this.setNumber = setNumberNF; this.toUnformatted = toUnformattedNF; this.setInputDecimal = setInputDecimalNF; this.setSeparators = setSeparatorsNF; this.setCommas = setCommasNF; this.setNegativeFormat = setNegativeFormatNF; this.setNegativeRed = setNegativeRedNF; this.setCurrency = setCurrencyNF; this.setCurrencyPrefix = setCurrencyPrefixNF; this.setCurrencyValue = setCurrencyValueNF; this.setCurrencyPosition = setCurrencyPositionNF; this.setPlaces = setPlacesNF; this.toFormatted = toFormattedNF; this.toPercentage = toPercentageNF; this.getOriginal = getOriginalNF; this.moveDecimalRight = moveDecimalRightNF; this.moveDecimalLeft = moveDecimalLeftNF; this.getRounded = getRoundedNF; this.preserveZeros = preserveZerosNF; this.justNumber = justNumberNF; this.expandExponential = expandExponentialNF; this.getZeros = getZerosNF; this.moveDecimalAsString = moveDecimalAsStringNF; this.moveDecimal = moveDecimalNF; this.addSeparators = addSeparatorsNF; if (inputDecimal == null) { this.setNumber(num, this.PERIOD); } else { this.setNumber(num, inputDecimal); } this.setCommas(true); this.setNegativeFormat(this.LEFT_DASH); this.setNegativeRed(false); this.setCurrency(false); this.setCurrencyPrefix("$"); this.setPlaces(2); } function setInputDecimalNF(val) { this.inputDecimalValue = val; } function setNumberNF(num, inputDecimal) { if (inputDecimal != null) { this.setInputDecimal(inputDecimal); } this.numOriginal = num; this.num = this.justNumber(num); } function toUnformattedNF() { return (this.num); } function getOriginalNF() { return (this.numOriginal); } function setNegativeFormatNF(format) { this.negativeFormat = format; } function setNegativeRedNF(isRed) { this.negativeRed = isRed; } function setSeparatorsNF(isC, separator, decimal) { this.hasSeparators = isC; if (separator == null) { separator = this.COMMA; } if (decimal == null) { decimal = this.PERIOD; } if (separator == decimal) { this.decimalValue = (decimal == this.PERIOD) ? this.COMMA : this.PERIOD; } else { this.decimalValue = decimal; } this.separatorValue = separator; } function setCommasNF(isC) { this.setSeparators(isC, this.COMMA, this.PERIOD); } function setCurrencyNF(isC) { this.hasCurrency = isC; } function setCurrencyValueNF(val) { this.currencyValue = val; } function setCurrencyPrefixNF(cp) { this.setCurrencyValue(cp); this.setCurrencyPosition(this.LEFT_OUTSIDE); } function setCurrencyPositionNF(cp) { this.currencyPosition = cp; } function setPlacesNF(p, tr) { this.roundToPlaces = !(p == this.NO_ROUNDING); this.truncate = (tr != null && tr); this.places = (p < 0) ? 0 : p; } function addSeparatorsNF(nStr, inD, outD, sep) { nStr += ""; var dpos = nStr.indexOf(inD); var nStrEnd = ""; if (dpos != -1) { nStrEnd = outD + nStr.substring(dpos + 1, nStr.length); nStr = nStr.substring(0, dpos); } var rgx = /(\d+)(\d{3})/; while (rgx.test(nStr)) { nStr = nStr.replace(rgx, "$1" + sep + "$2"); } return nStr + nStrEnd; } function toFormattedNF() { var pos; var nNum = this.num; var nStr; var splitString = new Array(2); if (this.roundToPlaces) { nNum = this.getRounded(nNum); nStr = this.preserveZeros(Math.abs(nNum)); } else { nStr = this.expandExponential(Math.abs(nNum)); } if (this.hasSeparators) { nStr = this.addSeparators(nStr, this.PERIOD, this.decimalValue, this.separatorValue); } else { nStr = nStr.replace(new RegExp("\\" + this.PERIOD), this.decimalValue); } var c0 = ""; var n0 = ""; var c1 = ""; var n1 = ""; var n2 = ""; var c2 = ""; var n3 = ""; var c3 = ""; var negSignL = (this.negativeFormat == this.PARENTHESIS) ? this.LEFT_PAREN : this.DASH; var negSignR = (this.negativeFormat == this.PARENTHESIS) ? this.RIGHT_PAREN : this.DASH; if (this.currencyPosition == this.LEFT_OUTSIDE) { if (nNum < 0) { if (this.negativeFormat == this.LEFT_DASH || this.negativeFormat == this.PARENTHESIS) { n1 = negSignL; } if (this.negativeFormat == this.RIGHT_DASH || this.negativeFormat == this.PARENTHESIS) { n2 = negSignR; } } if (this.hasCurrency) { c0 = this.currencyValue; } } else { if (this.currencyPosition == this.LEFT_INSIDE) { if (nNum < 0) { if (this.negativeFormat == this.LEFT_DASH || this.negativeFormat == this.PARENTHESIS) { n0 = negSignL; } if (this.negativeFormat == this.RIGHT_DASH || this.negativeFormat == this.PARENTHESIS) { n3 = negSignR; } } if (this.hasCurrency) { c1 = this.currencyValue; } } else { if (this.currencyPosition == this.RIGHT_INSIDE) { if (nNum < 0) { if (this.negativeFormat == this.LEFT_DASH || this.negativeFormat == this.PARENTHESIS) { n0 = negSignL; } if (this.negativeFormat == this.RIGHT_DASH || this.negativeFormat == this.PARENTHESIS) { n3 = negSignR; } } if (this.hasCurrency) { c2 = this.currencyValue; } } else { if (this.currencyPosition == this.RIGHT_OUTSIDE) { if (nNum < 0) { if (this.negativeFormat == this.LEFT_DASH || this.negativeFormat == this.PARENTHESIS) { n1 = negSignL; } if (this.negativeFormat == this.RIGHT_DASH || this.negativeFormat == this.PARENTHESIS) { n2 = negSignR; } } if (this.hasCurrency) { c3 = this.currencyValue; } } } } } nStr = c0 + n0 + c1 + n1 + nStr + n2 + c2 + n3 + c3; if (this.negativeRed && nNum < 0) { nStr = '<font color="red">' + nStr + "</font>"; } return (nStr); } function toPercentageNF() { nNum = this.num * 100; nNum = this.getRounded(nNum); return nNum + "%"; } function getZerosNF(places) { var extraZ = ""; var i; for (i = 0; i < places; i++) { extraZ += "0"; } return extraZ; } function expandExponentialNF(origVal) { if (isNaN(origVal)) { return origVal; } var newVal = parseFloat(origVal) + ""; var eLoc = newVal.toLowerCase().indexOf("e"); if (eLoc != -1) { var plusLoc = newVal.toLowerCase().indexOf("+"); var negLoc = newVal.toLowerCase().indexOf("-", eLoc); var justNumber = newVal.substring(0, eLoc); if (negLoc != -1) { var places = newVal.substring(negLoc + 1, newVal.length); justNumber = this.moveDecimalAsString(justNumber, true, parseInt(places)); } else { if (plusLoc == -1) { plusLoc = eLoc; } var places = newVal.substring(plusLoc + 1, newVal.length); justNumber = this.moveDecimalAsString(justNumber, false, parseInt(places)); } newVal = justNumber; } return newVal; } function moveDecimalRightNF(val, places) { var newVal = ""; if (places == null) { newVal = this.moveDecimal(val, false); } else { newVal = this.moveDecimal(val, false, places); } return newVal; } function moveDecimalLeftNF(val, places) { var newVal = ""; if (places == null) { newVal = this.moveDecimal(val, true); } else { newVal = this.moveDecimal(val, true, places); } return newVal; } function moveDecimalAsStringNF(val, left, places) { var spaces = (arguments.length < 3) ? this.places : places; if (spaces <= 0) { return val; } var newVal = val + ""; var extraZ = this.getZeros(spaces); var re1 = new RegExp("([0-9.]+)"); if (left) { newVal = newVal.replace(re1, extraZ + "$1"); var re2 = new RegExp("(-?)([0-9]*)([0-9]{" + spaces + "})(\\.?)"); newVal = newVal.replace(re2, "$1$2.$3"); } else { var reArray = re1.exec(newVal); if (reArray != null) { newVal = newVal.substring(0, reArray.index) + reArray[1] + extraZ + newVal.substring(reArray.index + reArray[0].length); } var re2 = new RegExp("(-?)([0-9]*)(\\.?)([0-9]{" + spaces + "})"); newVal = newVal.replace(re2, "$1$2$4."); } newVal = newVal.replace(/\.$/, ""); return newVal; } function moveDecimalNF(val, left, places) { var newVal = ""; if (places == null) { newVal = this.moveDecimalAsString(val, left); } else { newVal = this.moveDecimalAsString(val, left, places); } return parseFloat(newVal); } function getRoundedNF(val) { val = this.moveDecimalRight(val); if (this.truncate) { val = val >= 0 ? Math.floor(val) : Math.ceil(val); } else { val = Math.round(val); } val = this.moveDecimalLeft(val); return val; } function preserveZerosNF(val) { var i; val = this.expandExponential(val); if (this.places <= 0) { return val; } var decimalPos = val.indexOf("."); if (decimalPos == -1) { val += "."; for (i = 0; i < this.places; i++) { val += "0"; } } else { var actualDecimals = (val.length - 1) - decimalPos; var difference = this.places - actualDecimals; for (i = 0; i < difference; i++) { val += "0"; } } return val; } function justNumberNF(val) { newVal = val + ""; var isPercentage = false; if (newVal.indexOf("%") != -1) { newVal = newVal.replace(/\%/g, ""); isPercentage = true; } var re = new RegExp("[^\\" + this.inputDecimalValue + "\\d\\-\\+\\(\\)eE]", "g"); newVal = newVal.replace(re, ""); var tempRe = new RegExp("[" + this.inputDecimalValue + "]", "g"); var treArray = tempRe.exec(newVal); if (treArray != null) { var tempRight = newVal.substring(treArray.index + treArray[0].length); newVal = newVal.substring(0, treArray.index) + this.PERIOD + tempRight.replace(tempRe, ""); } if (newVal.charAt(newVal.length - 1) == this.DASH) { newVal = newVal.substring(0, newVal.length - 1); newVal = "-" + newVal; } else { if (newVal.charAt(0) == this.LEFT_PAREN && newVal.charAt(newVal.length - 1) == this.RIGHT_PAREN) { newVal = newVal.substring(1, newVal.length - 1); newVal = "-" + newVal; } } newVal = parseFloat(newVal); if (!isFinite(newVal)) { newVal = 0; } if (isPercentage) { newVal = this.moveDecimalLeft(newVal, 2); } return newVal; }
/*End  Number formatter*/


function isFill(id) {
    var ReturnVal = false;

    if ($("#" + id).val() != '') {
        ReturnVal = true;
    }
    return ReturnVal;
}

//function loadCustomerActivityGrid(usertype,usertypeid,endusercode,targethistoygrid) {
//    if(usertype=='Customer'){
//        loadCustomerActivityGrid(usertypeid,targethistoygrid);
//    }
//    else if(usertype=='Lead'){
//        loadLeadActivityGrid(targethistoygrid);
//    }
//    else if(usertype=='EndUser'){
//        loadEnduserActivityGrid(usertypeid, endusercode, targethistoygrid);
//    }
//}
//}

function CatlogCodetextEditor(container, options) {
    $('<input  data-bind="value:' + options.field + '" readonly="readonly"/>')
    .appendTo(container);
}

function CatlogCodeDropDownEditor(container, options) {
    $('<input id="txttexts" data-bind="value:' + options.field + '"/> <input id="btnLookup" type="button" value="?" />')
    .appendTo(container);

    $("#btnLookup").click(function (options) {
        var uid = $(".k-grid-edit-row").data("uid");
        loadcatlogcodelookup("div_customer_lookup", uid);
    });
}

function setcuscode(CatlogCode, Description, uid) {
    var model = $("#PricelistGrid").data("kendoGrid").dataSource.getByUid(uid);
    //set new values
    model.set("Description", Description);
    model.set("CatlogCode", CatlogCode);
    $("#div_customer_lookup").data("kendoWindow").close();
}

function loadEnduserPriceListmaster(custid, targetdiv, effectivedate) {

    var editurl = ROOT_PATH + 'lead_customer/process_forms/processmaster.aspx?type=get&fm=enduserentryprice&docid=' + "#=CatlogCode#";

    editurl = "<a href=\"" + editurl + "\">#=CatlogCode#</a>";

    $("#" + targetdiv).html("");
    $("#" + targetdiv).kendoGrid({
        height: 300,
        columns: [
                { template: "<div id=\"div_effdate\" > #: EffectiveDateString # </div>", field: "EffectiveDateString", hidden: true },
                { template: "<div id=\"div_lastupdate\" > #: EffectiveDateString # </div>", field: "EffectiveDateString", hidden: true },
                { field: "CatlogCode", title: "Catalog Code", width: "200px", editor: CatlogCodeDropDownEditor }, //template: editurl,
                {field: "Description", title: "Description", editor: CatlogCodetextEditor },
                { field: "Price", title: "Cust Price", width: "100px", format: "{0:n2}" },
                { command: ["destroy"], title: "&nbsp;", width: "100px" }
        ],
        editable: true, // enable editing
        pageable: true,
        sortable: true,
        filterable: false,
        toolbar: ["create"],
        dataBound: EndUserpriceListGrid_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        EffectiveDate: { type: "Date", validation: { required: true} },
                        EffectiveDateString: { validation: { required: true }, editable: false },
                        LastUpdated: { type: "Date", validation: { required: true} },
                        CatlogCode: { validation: { required: true} },
                        Description: { validation: { required: true }, editable: true },
                        Price: { type: "number", validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/enduser_entry_Service.asmx/GetEnduserPriceAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { custid: custid, effectivedate: effectivedate },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            //$("div#div_Effective").html(model);
                            //alert(sss)
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

function EndUserpriceListGrid_onDataBound(arg) {
    if ($("#HiddenField_Mode").val() == 'edit') {
        $('#div_LastUpdate').html($('#div_lastupdate').html());
        $('#div_Originator').html($('#div_originator').html());
    }
    else {
        $('#div_LastUpdate').html('');
    }
}

function loadcatlogcodelookup(griddiv, uid) {
    var link = "javascript:setcuscode(\'#=CatlogCode#\',\'#=Description#\',\'" + uid + "\');"
    $("#" + griddiv).html("");
    $("#" + griddiv).kendoGrid({
        height: 300,
        columns: [
                { template: '<a href="' + link + '">#=CatlogCode#</a>', field: "Catalog Code", title: "Catalog Code", width: "85px" },
                    
                    {field: "Description", title: "Description", width: "200px"},
                    { field: "DeliveryFrequency", title: "Cust Price", width: "100px" }
            ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,


            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "Originator",
                    fields: {
                        CatlogCode: { validation: { required: true} },
                        Description: { validation: { required: true }, editable: true },
                        DeliveryFrequency: { validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/lead_customer/enduser_entry_Service.asmx/GetProductLookupAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    //data: { custid: $("#HiddenFieldEndUser_CustCode").val(), effectivedate: '' },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {

                            var wnd = $("#div_customer_lookup").kendoWindow({
                                title: "Customer Lookup",
                                modal: true,
                                visible: false,
                                resizable: false,
                                width: 600
                            }).data("kendoWindow");

                            //$("#modalWindow").html(msg);
                            wnd.center().open();

                            $("div#div_loader").hide();
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function loadActivityHistoryGrid(enduser_code, cust_code, leadId) {
    $("#activityHistoryGrid").html("");
    $("#activityHistoryGrid").kendoGrid({
        height: 300,
        columns: [

                        { field: "ActivityID", width: "85px", hidden: true },

                        { template: '<a href="javascript:OpenActivityHistory(\'#=ActivityID#\')">#=Subject#</a>',
                            field: "Subject", width: "250px"
                        },
                        { field: "StartDate", title: "Start Date", width: "100px", format: DATE_FORMAT3 },
                        { field: "EndDate", title: "End Date", width: "80px", format: DATE_FORMAT3 },
                        { field: "Comments", title: "Comments" }
                ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: false,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        ActivityID: { validation: { required: true} },
                        Subject: { validation: { required: true} },
                        StartDate: { type: "Date", editable: true, nullable: true },
                        EndDate: { type: "Date", validation: { required: true} },
                        Comments: { validation: { required: true} }

                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/lead_customer/lead_entry_Service.asmx/GetActivityHistoryDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { enduserCode: enduser_code, custCode: cust_code, leadId: leadId },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            change: emptyGridFix
        }
    });
}




        function OpenActivityHistory(activityId) {
            
            var enduser_code = $("#MainContent_HiddenFieldEndUserCode").val();
            var cust_code = $("#MainContent_HiddenFieldCustomerCode").val();
            var leadId = $("#MainContent_HiddenFieldLeadId").val();
            
            var url = '';
            if (enduser_code != '') {
                url = ROOT_PATH + "activity_planner/transaction/activityentry.aspx?fm=acthis&activityid="
                        + activityId + "&custid=" + cust_code + "&eduid=" + enduser_code;
            }
            else if (cust_code != '') {
                url = ROOT_PATH + "activity_planner/transaction/activityentry.aspx?fm=acthis&activityid="
                        + activityId + "&custid=" + cust_code;
            }
            else if (leadId != '') {
                url = ROOT_PATH + "activity_planner/transaction/activityentry.aspx?fm=acthis&activityid="
                        + activityId + "&leadId=" + leadId;
            }
            window.location = url;
        }

function scheduletemplates(targeturl) {
    $("#div_loader").show();
    var url = ROOT_PATH + targeturl;
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg != "") {

                var wnd = $("#schedule_window").kendoWindow({
                    title: "Add/Edit Address",
                    modal: true,
                    visible: false,
                    resizable: false
                }).data("kendoWindow");

                $("#schedule_subwindow").html(msg);
                wnd.center().open();

                //Enable buttons.
                $("#MainContent_buttonbar_buttinSave").removeClass('dissavebtn').addClass('savebtn');
                $("#MainContent_buttonbar_buttonAddCallCycle").removeClass('discall_cycle_activities').addClass('call_cycle_activities');
                $("#MainContent_buttonbar_buttonAddleadsCustomer").removeClass('disadd_leads_customer').addClass('add_leads_customer');
                $("#MainContent_buttonbar_buttonSaveNewTemplate").removeClass('dissave_new_template').addClass('save_new_template');

                $("#MainContent_buttonbar_buttinSave").prop('disabled', false);
                $("#MainContent_buttonbar_buttonAddCallCycle").prop('disabled', false);
                $("#MainContent_buttonbar_buttonAddleadsCustomer").prop('disabled', false);
                $("#MainContent_buttonbar_buttonSaveNewTemplate").prop('disabled', false);

                $("#MainContent_buttonbar_buttonSaveNewTemplate").live('click', function () {
                    return showtemplatesavedialog();
                });
                $("#MainContent_buttonbar_buttonAddCallCycle").live('click', function () {
                    return showcallcycledialog();
                });

                $("#jqi_state0_buttonOk").click(function () {

                    var callcycle_id = '';
                    var template = '';

                    $('#scheduleTable tr').each(function () {
                        if ($(this).children(':eq(0)').find('#a').is(':checked')) {
                            callcycle_id = $(this).find("#f").html();
                            template = $(this).find("#d").html();
                        }
                    });

                    $('#MainContent_txtScheduleTemplate').val(template);
                    $("#MainContent_HiddenFieldCallCycleId").val(callcycle_id);

                    loadTemplate(callcycle_id);
                    wnd.close();
                });
                $("#div_loader").hide();
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
                $("#div_loader").hide();
                //$("#" + tagertdiv).html(msg);
            }
        }
    });
}

function loadTemplate(callcycleid) {
    $("#div_loader").show();

    var url = ROOT_PATH + "call_cycle/process_forms/processmaster.aspx?fm=templateentry&type=get&callcycid=" + callcycleid;
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg != "") {
                $("#div_loader").hide();
                $("#div_schedule").html(msg);
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
                $("#div_loader").hide();
                //$("#" + tagertdiv).html(msg);
            }
        }
    });
}

function loadRouteTemplate(repcode){
    $("#div_loader").show();
    
    var url = ROOT_PATH + "call_cycle/process_forms/processmaster.aspx?fm=routesequence&type=get&repcode=" + repcode;

    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg != "") {
                $("#div_loader").hide();
                $("#div_schedule").html(msg);
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
                $("#div_loader").hide();
                //$("#" + tagertdiv).html(msg);
            }
        }
    });
}

function LoadTemplatesBySession() {
    $("#div_loader").show();

    var url = ROOT_PATH + "call_cycle/process_forms/processmaster.aspx?fm=templatesession&type=get";
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg != "") {
                $("#div_loader").hide();
                $("#div_schedule").html(msg);
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
                $("#div_loader").hide();
                //$("#" + tagertdiv).html(msg);
            }
        }
    });
}

function changeTemplate(callcycleid,targetday,sourceid,clickedindex,sortindex){//alert(sortindex);
    $("#div_loader").show();
    //alert(targetday);
    var url = ROOT_PATH + "call_cycle/process_forms/processmaster.aspx?fm=templateentry&type=change&callcycid="+callcycleid+"&tgtday="+targetday+"&srcid="+sourceid+"&cliindex="+clickedindex+"&sortindex="+sortindex;
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg != "") {
                $("#div_loader").hide();
                $("#div_schedule").html(msg);
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
                $("#div_loader").hide();
                //$("#" + tagertdiv).html(msg);
            }
        }
    });
}

function changeRouteTemplate(repcode,targetday,sourceid,clickedindex,sortindex){//alert(sortindex);
    $("#div_loader").show();
    //alert(targetday);
    var url = ROOT_PATH + "call_cycle/process_forms/processmaster.aspx?fm=routesequence&type=change&repcode="+repcode+"&tgtday="+targetday+"&srcid="+sourceid+"&cliindex="+clickedindex+"&sortindex="+sortindex;
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg != "") {
                $("#div_loader").hide();
                $("#div_schedule").html(msg);
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
                $("#div_loader").hide();
                //$("#" + tagertdiv).html(msg);
            }
        }
    });
}

function showcallcycledialog() {
    $("#div_loader").show();
    $("#div_loader").hide();

    var wnd = $("#callcyle_window").kendoWindow({
        title: "Add/Edit Address",
        modal: true,
        visible: false,
        resizable: false
    }).data("kendoWindow");

    $("#callcyle_window");
    wnd.center().open();
    //$("#callcyle_window").load(url);

    return false;
}

function loadLeadCustomer(CallCycleID, type) {

    $("#loadLeadCustomerGrid").html("");
    $("#loadLeadCustomerGrid").kendoGrid({
        height: 300,
        columns: [//DeleteCallCycle
            {
                template: '<a href="javascript:DeleteCallCycle(#=Contact.SourceId#,\'#=Contact.CustomerCode#\',\'#=Contact.EndUserCode#\',#=CreateActivity#)">Delete</a>',
                width: "50px"
            },
            { field: "Contact.SourceId", title: "LeadID", hidden: true },
            { field: "Contact.CustomerCode", title: "Customer Code", hidden: true },
            { field: "Contact.EndUserCode", title: "EndUser Code", hidden: true },
            { field: "Contact.Name", title: "Name", width: "130px" },
            { field: "LeadStage.StageName", title: "Lead Stage", width: "130px", hidden: true },
            { field: "Contact.Address", title: "Address", width: "250px" },
            { field: "Contact.City", title: "City", width: "70px" },
            { field: "Contact.State", title: "State", width: "70px" },
            { field: "Contact.PostalCode", title: "Postal Code", width: "70px" }
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 1000,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "SourceId",
                    fields: {
                        CallCycleID: { validation: { required: true } },
                        Comments: { validation: { required: true } },
                        Description: { validation: { required: true } },
                        DueOn: { type: "Date", validation: { required: true } }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/call_cycle/call_cycle_service.asmx/GetCallCycleContacts", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { CallCycleID: CallCycleID, type: type },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function OpenSelectLead(CallCycleID, Comments, Description, DueOn, CreatedDate, CreatedBy, RepName) {
    $("#MainContent_HiddenFieldCallCycleID").val(CallCycleID);
    loadLeadCustomer(CallCycleID, 1);

    $("#MainContent_txtDescription").val(Comments);
    $("#MainContent_txtSchedule").val(Description);
    $("#MainContent_HiddenField_DueDate").val(DueOn);
    $("#MainContent_tbCreated").html("Created By: " + CreatedBy + ", " + CreatedDate);

    $('#MainContent_txtDRName').val(RepName);
}

function loadCallCycle() {
    $("#MainContent_CallCyclegrid").html("");
    $("#MainContent_CallCyclegrid").kendoGrid({
        height: 300,
        columns: [


            { field: "CallCycleID", title: "CallCycleID", hidden: true },
            { field: "Comments", title: "Comments", hidden: true },
            //{ field: "Description", title: "Call Cycle", width: "130px" },
            {
                template: '<a href="javascript:OpenSelectLead(#=CallCycleID#,\'#=Comments#\',\'#=Description#\',\'#=DueOnString#\',\'#=CreatedDateString#\',\'#=CreatedBy#\',\'#=RepName#\')">#=Description#</a>',
                field: "Schedule", width: "130px"
            },
            { field: "DueOn", title: "Due On", width: "70px", format: DATE_FORMAT3, hidden: true },
            { field: "RepName", title: "Rep Name", width: "70px", hidden: true },
            { field: "DueOnString", title: "DueOnString", width: "70px", hidden: true },
            { field: "CreatedDateString", title: "CreatedDateString", width: "70px", hidden: true },
            { field: "CreatedBy", title: "CreatedBy", width: "70px", hidden: true }

        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        //filterable: true,
        selectable: "single",
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "SourceId",
                    fields: {
                        CallCycleID: { validation: { required: true } },
                        Comments: { validation: { required: true } },
                        Description: { validation: { required: true } },
                        DueOn: { type: "Date", validation: { required: true } },
                        RepName: { validation: { required: true } }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/call_cycle/call_cycle_service.asmx/GetCallCycle", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}


function AddLeadCustomerGrid(LeadStage, CustomerCode, SourceId) {
    var root = ROOT_PATH + "call_cycle/process_forms/processmaster.aspx?fm=LeadCustomerentry&type=query&LeadStage=" + LeadStage +
        "&CustomerCode=" + encodeurl(CustomerCode) + "&LeadId=" + SourceId;

    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg == "true") {
                var callid = $("#MainContent_HiddenFieldCallCycleID").val();
                loadLeadCustomer(callid, 0);
                $("#new_window").data("kendoWindow").close();
            } else if (msg == "true1") {
                //debugger;
                var callid = $("#MainContent_HiddenFieldCallCycleID").val();
                loadLeadCustomer(callid, 0);
                $("#new_window").data("kendoWindow").close();

                var test = "<div id='messagered' class='message-red' rel='error'><table border='0' width='100%' cellpadding='0' cellspacing='0'>";
                test = test + "<tr><td class='red-left'>Outlet already added.</td><td class='red-right'><a class='close-red'><img src='"
                    + ROOT_PATH + "assets/images/icon_close_red.gif' alt='' /></a></td></tr></table></div>";

                //                    $("#MainContent_div_message").show("fast");                    
                $("#MainContent_div_message").html(test);
                $("#MainContent_div_message").css("display", "block");
                hideStatusDiv("MainContent_div_message");
                //document.getElementById('MainContent_div_message').style.display = "block";
            } else if (msg == "true2") {
                //debugger;

                $("#new_window").data("kendoWindow").close();
                var callid = $("#MainContent_HiddenFieldCallCycleID").val();
                var existsMsg = GetErrorMessageDiv("Outlet is already added to a Different Schedule.", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(existsMsg);
                hideStatusDiv("MainContent_div_message");
                //loadLeadCustomer(callid, 0);

            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
}

function loadLeadCustomerLookup(activeInactiveChecked, reqSentIsChecked, repType) {
    var distUserName = $("#MainContent_hfSelectedDistributorUsername").val();
    var marketId = $("#MainContent_hfSelectedMarketId").val();
    var volume = $("#MainContent_hfSelectedVolume").val();
    var outletTypeId = $("#MainContent_hfSelectedOutletTypeId").val();
    var town = $("#MainContent_hfSelectedTown").val();

    var deleteCC = "javascript:DeleteAddCustomerRoute(#=CustomerCode#);";

    $("#div_custgrid").html("");
    $("#div_custgrid").kendoGrid({
        height: 550,
        columns: [
            { text: "HasSelect", title: "", headerTemplate: '<input id="checkAlloutlets" type="checkbox" class="myCheckbox" />', width: "15px", template: "<input id=\"check_datarow_outlets\" alt=\"#= uid# \" type=\"checkbox\"   #= HasSelect ? checked='checked' : '' #  class=\"check_row\"/>" },
            //{ template: '<a href="javascript:AddLeadCustomerGrid(\'#=LeadStage#\',\'#=CustomerCode#\',\'#=SourceId#\')">#=Name#</a>',
            //    field: "Name", width: "250px", footerTemplate: "<div id=\"div_rowCount_sum\" ></div>"
            //},
            { field: "Name", width: "220px", footerTemplate: "<div id=\"div_rowCount_sum\" ></div>", title: "Customer" },
            { field: "Address", title: "Address", width: "300px", hidden: false },
            { field: "City", title: "Town", width: "130px", hidden: true },
            { field: "LeadStage", title: "Lead Stage", width: "85px", hidden: true },
            { field: "CustomerCode", title: "Cust Code", width: "105px", hidden: true },
            { field: "SourceId", title: "CRM Ref. No.", width: "80px", hidden: true },
            { template: "<a href=\"" + deleteCC + "\">Delete</a>", width: "150px" }
            //            { template: '<a "">#= HasSelect ? checked="checked" : "fff" #</a>',
            //                field: "Name", width: "250px"
            //            },
            //            { field: "HasSelect", title: "HasSelect", width: "130px", hidden: false }
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        //columnMenu: true,
        //dataBound: LeadCustomerGrid_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "SourceId",
                    fields: {
                        Name: { validation: { required: true } },
                        LeadStage: { validation: { required: true } },
                        CustomerCode: { editable: true, nullable: true },
                        State: { validation: { required: true } },
                        City: { validation: { required: true } },
                        SourceId: { validation: { required: true } },
                        Address: { validation: { required: true } },
                        HasSelect: { type: "boolean" },
                        rowCount: { type: "number", validation: { required: true } }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/lead_customer/lead_contacts_Service.asmx/GetOutletsForLookupInScheduleEntry", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer', pgindex: 0, dist: distUserName, marketId: marketId, volume: volume, outletTypeId: outletTypeId, town: town },
                    type: "POST",//use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            //$("#new_window").data("kendoWindow").open();
                            $("div#div_loader").hide();
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

$('#check_datarow_outlets').live('click', function (e) {
    var cb = $(this);
    var checked = cb.is(':checked');
    var selected_uid = cb[0].alt.replace(/\s+/g, '');

    setValueToGridDataOutlets(checked, selected_uid);

    if ($('[id=checkAllOutlets]').is(':checked')) {
        $('[id=checkAllOutlets]')[0].checked = false;
    }
});

function setValueToGridDataOutlets(val, rowuid) {
                
    var grid = $("#div_custgrid").data("kendoGrid");
    var item_grid = grid.dataSource.getByUid(rowuid);
    if (item_grid != undefined && item_grid != null)
        item_grid.set("HasSelect", val);
}

$("#checkAlloutlets").live('click', function (e) {
                
    var state = $(this).is(':checked');
    var grid = $('#div_custgrid').data('kendoGrid');
    $.each(grid.dataSource.view(), function () {
        if (this['HasSelect'] != state)
            this.dirty = true;
        this['HasSelect'] = state;
    });
    grid.refresh();
});

function AddEndUserToGrid(EndUserCode, CustomerCode) {
    var root = ROOT_PATH + "call_cycle/process_forms/processmaster.aspx?fm=EndUser&type=query&CustomerCode=" + encodeurl(CustomerCode) + "&EndUserCode=" + encodeurl(EndUserCode);

    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg == "true") {
                var callid = $("#MainContent_HiddenFieldCallCycleID").val();
                loadLeadCustomer(callid, 0);
                $("#new_window").data("kendoWindow").close();
            }
            else if(msg == "true1")
            {
                var callid = $("#MainContent_HiddenFieldCallCycleID").val();
                loadLeadCustomer(callid, 0);
                $("#new_window").data("kendoWindow").close();

                var test = "<div id='messagered' class='message-red' rel='error'><table border='0' width='100%' cellpadding='0' cellspacing='0'>";
                    test = test + "<tr><td class='red-left'>Lead/Customer already added.</td><td class='red-right'><a class='close-red'><img src='"
                + ROOT_PATH + "assets/images/icon_close_red.gif' alt='' /></a></td></tr></table></div>";

                    //                    $("#MainContent_div_message").show("fast");
                    $("#MainContent_div_message").html(test);
                    document.getElementById('MainContent_div_message').style.display = "block";
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
}


function loadEndUserLookup(activeInactiveChecked, reqSentIsChecked, repType) {
    $("#div_custgrid").html("");
    $("#div_custgrid").kendoGrid({
        //height: 400,
        columns: [
        { field: "CustomerCode", title: "CustomerCode", width: "105px" },
        { template: '<a href="javascript:AddEndUserToGrid(\'#=EndUserCode#\',\'#=CustomerCode#\')">#=Name#</a>',
            field: "Name", width: "250px"
        },
        { field: "EndUserCode", title: "Code", width: "105px", hidden: true },
        { field: "CustomerName", title: "CustomerName", width: "105px", hidden: true }
    ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "EndUserCode",
                    fields: {
                        CustomerCode: { validation: { required: true} },
                        Name: { validation: { required: true} },
                        EndUserCode: { validation: { required: true} },
                        CustomerName: { validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/lead_contacts_Service.asmx/GetDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'Enduser', pgindex: 0 },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        //alert(textStatus);
                        if (textStatus == "success") {

                            $("#new_window").data("kendoWindow").open();
                            $("div#div_loader").hide();
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }

        }
    });
}

function SetLeadStage(LeadStage) {
    var root = ROOT_PATH + "call_cycle/process_forms/processmaster.aspx?fm=LeadStage&type=query&LeadStage=" + LeadStage;
    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            
            if (msg == "true") {
                if (LeadStage == "enduser") {
                    loadEndUserLookup(1, 0, 'F');
                } else {
                    loadLeadCustomerLookup(1, 0, 'F');
                }
                         
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
}

function LoadCheckListCustomer(CheckListId) {
    var distUserName = $("#MainContent_hfSelectedDistributorUsername").val();
    var marketId = $("#MainContent_hfSelectedMarketId").val();
    var volume = $("#MainContent_hfSelectedVolume").val();
    var outletTypeId = $("#MainContent_hfSelectedOutletTypeId").val();
    var town = $("#MainContent_hfSelectedTown").val();
    
    var deleteCC = "javascript:DeleteAddCustomerRoute(#=CustomerCode#);";

    $("#div_custgrid").html("");
    $("#div_custgrid").kendoGrid({
        height: 600,
        columns: [//<input id="checkAlloutlet" type="checkbox" class="myCheckbox" />
            { text: "HasSelect", title: "",  headerTemplate: '', width: "30px", template: "<input id=\"check_datarow_route\" alt=\"#= uid# \" type=\"checkbox\"   #= HasSelect ? checked='checked' : '' #  class=\"check_row_outlet\"/>" },
            { field: "CustomerCode", title: "Cust Code", width: "105px", hidden: false },
            { field: "Name", width: "200px", footerTemplate: "<div id=\"div_rowCount_sum\" ></div>", title: "Customer"},
            { field: "Address", title: "Address", width: "300px", hidden: false },
            { field: "RouteName", title: "Route", width: "160px", hidden: false },
            { field: "RepCode", title: "Rep Code", width: "90px", hidden: false },
            { field: "RepName", title: "Rep Name", width: "250px", hidden: false }
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        //columnMenu: true,
        //dataBound: LeadCustomerGrid_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 15,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "SourceId",
                    fields: {
                        Name: { validation: { required: true} },
                        LeadStage: { validation: { required: true} },
                        CustomerCode: { editable: true, nullable: true },
                        State: { validation: { required: true} },
                        City: { validation: { required: true} },
                        SourceId: { validation: { required: true} },
                        Address: { validation: { required: true} },
                        RouteName: { validation: { required: true} },
                        RepCode: { validation: { required: true} },
                        RepName: { validation: { required: true} },
                        HasSelect: { type: "boolean"},
                        rowCount: { type: "number", validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/lead_customer/lead_contacts_Service.asmx/GetDataAndCountForCheckList", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { CheckListId: CheckListId, activeInactiveChecked: 1, reqSentIsChecked: 0, repType: 'F', gridName: 'LeadAndCustomer',pgindex:0, dist:distUserName, marketId:marketId, volume:volume, outletTypeId:outletTypeId, town:town },
                    type: "POST" ,//use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            var newcust_window = $("#div_custgrid"),
                                        newcust_undo = $("#undo")
                                    .bind("click", function () {
                                        newcust_window.data("kendoWindow").open();
                                        newcust_undo.hide();
                                    });

                                    var onClose = function () {
                                        newcust_undo.show();
                                    }

                                    if (!newcust_window.data("kendoWindow")) {
                                        newcust_window.kendoWindow({
                                            width: "900px",
                                            height: "350px",
                                            title: "Routes",
                                            close: onClose
                                        });
                                    }

                                    newcust_window.data("kendoWindow").center().open();

                                    $("div#div_loader").hide();
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });


}

function DeleteCallCycle(leadId, CustomerCode, EndUserCode, CreateActivity) {
    //if (confirm("delete?")) {
    var DueOn = $("#MainContent_HiddenField_DueDate").val();
    //debugger;
    var CallCycleId = $("#MainContent_HiddenFieldCallCycleID").val();
    var root = ROOT_PATH + "call_cycle/process_forms/processmaster.aspx?fm=CallCycle&type=Delete&leadid=" + leadId + "&custcode="
            + CustomerCode + "&enduser=" + EndUserCode + "&id=" + CallCycleId + "&DueOn=" + DueOn + "&isnew=" + CreateActivity;

    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg == "true") {

                loadLeadCustomer(CallCycleId, 0);

                var test = "<div id='messagegreen'  class='message-green' rel='sucess'><table border='0' width='100%' cellpadding='0' cellspacing='0'>";
                test = test + "<tr><td class='green-left'>Contact/Customer was successfully deleted.</td><td class='green-right'><a class='close-green'><img src='"
            + ROOT_PATH + "assets/images/icon_close_green.gif' alt='' /></a></td></tr></table></div>";

                //                    $("#MainContent_div_message").show("fast");
                $("#MainContent_div_message").html(test);
                document.getElementById('MainContent_div_message').style.display = "block";

                // 
                //$("#new_window").data("kendoWindow").close();
            } else {
                var test = "<div id='messagered' class='message-red' rel='error'><table border='0' width='100%' cellpadding='0' cellspacing='0'>";
                test = test + "<tr><td class='red-left'>" + msg + "</td><td class='red-right'><a class='close-red'><img src='"
            + ROOT_PATH + "assets/images/icon_close_red.gif' alt='' /></a></td></tr></table></div>";

                //                    $("#MainContent_div_message").show("fast");
                $("#MainContent_div_message").html(test);
                document.getElementById('MainContent_div_message').style.display = "block";
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
    //}
}

function CheckEndUser(EndUserCode, CustomerCode, control) {
    var isChecked = 0;
    if ($(control).attr("checked") == "checked") {
        isChecked = 1;
    }

    var root = ROOT_PATH + "call_cycle/process_forms/processmaster.aspx?fm=checkEndUser&type=query&CustomerCode=" + encodeurl(CustomerCode) +
            "&EndUserCode=" + encodeurl(EndUserCode) + "&isChecked=" + isChecked; ;

    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg == "true") {
                $("#MainContent_HiddenFieldCallCycleCount").val("1");
                //var callid = $("#MainContent_HiddenFieldCallCycleID").val();
                //loadLeadCustomer(callid, 0);
                //$("#new_window").data("kendoWindow").close();
            } 
            else {
                $("#MainContent_HiddenFieldCallCycleCount").val("0");
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
}

function CheckLeadCustomer(LeadStage, CustomerCode, SourceId, control) {
    var isChecked=0;
    if ($(control).attr("checked") == "checked") {
        isChecked=1;
    }

    var root = ROOT_PATH + "call_cycle/process_forms/processmaster.aspx?fm=checkLeadCustomer&type=query&LeadStage=" + LeadStage +
                    "&CustomerCode=" + encodeurl(CustomerCode) + "&LeadId=" + SourceId + "&isChecked=" + isChecked;

    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg == "true") {
                $("#MainContent_HiddenFieldCallCycleCount").val("1");
            }
            else {
                $("#MainContent_HiddenFieldCallCycleCount").val("0");
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
}

function setvalue(){
    $("#MainContent_HiddenFieldDue1").val($("#MainContent_dueon1").val());
    $("#MainContent_HiddenFieldDue2").val($("#MainContent_dueon2").val());
    $("#MainContent_HiddenFieldDue3").val($("#MainContent_dueon3").val());
    $("#MainContent_HiddenFieldDue4").val($("#MainContent_dueon4").val());
    $("#MainContent_HiddenFieldDue5").val($("#MainContent_dueon5").val());

    //Time
    $("#MainContent_HiddenFieldTime1").val($("#MainContent_time1").val());
    $("#MainContent_HiddenFieldTime2").val($("#MainContent_time2").val());
    $("#MainContent_HiddenFieldTime3").val($("#MainContent_time3").val());
    $("#MainContent_HiddenFieldTime4").val($("#MainContent_time4").val());
    $("#MainContent_HiddenFieldTime5").val($("#MainContent_time5").val());

    var checkboxitem='';
    if($("#MainContent_Checkbox1").is(':checked'))
        checkboxitem="1";
    if($("#MainContent_Checkbox2").is(':checked'))
        checkboxitem += ",2";
    if($("#MainContent_Checkbox3").is(':checked'))
        checkboxitem += ",3";
    if($("#MainContent_Checkbox4").is(':checked'))
        checkboxitem += ",4";
    if($("#MainContent_Checkbox5").is(':checked'))
        checkboxitem += ",5";

    $("#MainContent_HiddenFieldCheck").val(checkboxitem);
    document.getElementById("MainContent_buttonok").click();
}

function OpenCallCycles(code,description){
    $('#MainContent_txtScheduleTemplate').val(description);
    $("#MainContent_HiddenFieldCallCycleId").val(code);

    loadTemplate(code);

    var wnd = $("#schedule_window").data("kendoWindow");
    wnd.close();
}


function showtemplatesavedialog(){
    $("#div_loader").show();
    
    $("#div_loader").hide();

   var wnd = $("#template_save_window").kendoWindow({
                            title: "Peerless crm",
                            modal: true,
                            visible: false,
                            resizable: false
   }).data("kendoWindow");

   //$("#callcyle_window");
    wnd.center().open();  
    //$("#callcyle_window").load(url);

    return false;
}

function setTemplateSavevalue(){
    $("#MainContent_HiddenField1TemplateName").val($("#MainContent_txtTemplateName").val());
}

function DeleteCallCycleItem(callcycleid,clickedindex,dayorder){//alert(sortindex);
    if (confirm("Are you sure you want to delete this contact?")) {
        $("#div_loader").show();
        //alert(targetday);
        var url = ROOT_PATH + "call_cycle/process_forms/processmaster.aspx?fm=templateCallCycle&type=Delete&callcycid="+callcycleid+"&cliindex="+clickedindex+"&dayorder="+dayorder;
        $.ajax({
            url: url,
            dataType: "html",
            cache: false,
            success: function (msg) {
                if (msg != "") {
                    $("#div_loader").hide();
                    $("#div_schedule").html(msg);
                    var sucessMsg = GetSuccesfullMessageDiv("Contact/Customer was successfully deleted.", "MainContent_div_message");   
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    hideStatusDiv('MainContent_div_message');
                }
            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                    $("#div_loader").hide();
                    //$("#" + tagertdiv).html(msg);
                }
            }
        });
    }
}

function DeleteRouteSequecneItem(repcode,clickedindex,dayorder,routeId){//alert(sortindex);
    if (confirm("Are you sure you want to delete this contact?")) {
        $("#div_loader").show();
        //alert(targetday);
        var url = ROOT_PATH + "call_cycle/process_forms/processmaster.aspx?fm=routesequence&type=Delete&repcode=" + repcode + "&cliindex=" + clickedindex + "&dayorder=" + dayorder + "&routeMasterId=" + routeId;
        $.ajax({
            url: url,
            dataType: "html",
            cache: false,
            success: function (msg) {
                if (msg != "") {
                    $("#div_loader").hide();
                    $("#div_schedule").html(msg);

                    var sucessMsg = GetSuccesfullMessageDiv("Contact/Customer was successfully deleted.", "MainContent_div_message");   
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    hideStatusDiv('MainContent_div_message');

//                    var test = "<div id='messagegreen'  class='message-green' rel='sucess'><table border='0' width='100%' cellpadding='0' cellspacing='0'>";
//                    test = test + "<tr><td class='green-left'>Contact/Customer was successfully deleted.</td><td class='green-right'><a class='close-green'><img src='"
//                        + ROOT_PATH + "assets/images/icon_close_green.gif' alt='' /></a></td></tr></table></div>";
//                    $("#MainContent_div_message").html(test);
//                    document.getElementById('MainContent_div_message').style.display = "block";
                }
            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                    $("#div_loader").hide();
                    //$("#" + tagertdiv).html(msg);
                }
            }
        });
    }
}

function GetAllCallCyclesList() {
//Enable buttons.
                $("#MainContent_buttonbar_buttinSave").removeClass('dissavebtn').addClass('savebtn');
                $("#MainContent_buttonbar_buttonAddCallCycle").removeClass('discall_cycle_activities').addClass('call_cycle_activities');
                $("#MainContent_buttonbar_buttonAddleadsCustomer").removeClass('disadd_leads_customer').addClass('add_leads_customer');
                $("#MainContent_buttonbar_buttonSaveNewTemplate").removeClass('dissave_new_template').addClass('save_new_template');
                
                $("#MainContent_buttonbar_buttinSave").prop('disabled', false);
                $("#MainContent_buttonbar_buttonAddCallCycle").prop('disabled', false);
                $("#MainContent_buttonbar_buttonAddleadsCustomer").prop('disabled', false);
                $("#MainContent_buttonbar_buttonSaveNewTemplate").prop('disabled', false);

                $("#MainContent_buttonbar_buttonSaveNewTemplate").live('click', function () {
                  return showtemplatesavedialog();
              });
               $("#MainContent_buttonbar_buttonAddCallCycle").live('click',function(){
                    return showcallcycledialog();
               });
                $("#div_loader").hide();

    $("#schedule_subwindow").html("");
    $("#schedule_subwindow").kendoGrid({
        // height: 400,
        columns: [
        { template: '<a href="javascript:OpenCallCycles(\'#=Code#\',\'#=Description#\')">#=Code#</a>',
            field: "Code", width: "50px", title: "Schedule Id"
        },
        { template: '<a href="javascript:OpenCallCycles(\'#=Code#\',\'#=Description#\')">#=Description#</a>', 
        field: "Description", title: "Schedule", width: "150px" }
    ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        //columnMenu: true,
        //dataBound: LeadCustomerGrid_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "SourceId",
                    fields: {
                        Code: { validation: { required: true} },
                        Description: { validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/call_cycle/call_cycle_service.asmx/GetAllCallCyclesList", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                    type: "POST" ,//use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#schedule_window").data("kendoWindow").open();
                            $("div#div_loader").hide();
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function loadSalesInfoTabs(type) {
    var original = $("#tabstrip1").clone(true);
    original.find(".k-state-active").removeClass("k-state-active");

    var getEffects = function () {
        return (("expand:vertical ") + ("fadeIn"));
    };

    var initSalesTabStrip = function () {
        $("#tabstrip1").kendoTabStrip({
            contentUrls: [, ROOT_PATH + "process_forms/loadcrmdetails.aspx"]
        });
    };
    initSalesTabStrip();

    if(type =='DIST'){
        var tabStrip = $("#tabstrip1").kendoTabStrip().data("kendoTabStrip");
        var lastIndex = tabStrip.items().length - 1;
        $(tabStrip.items()[lastIndex]).hide();
        //$("#distributorTab").text("new tab text");
        $("#distributorTab").children(".k-link").text("Customer");
        
    }
}


function loadEndUserSalesInfo(itemindex){
    switch (itemindex) {
        case "1":
            //Grid
            loadEndUserGragh();
        break;
        case "2":
            //Graph
            loadEndUserGragh();
        break;
        case "3":
            //Trend Graph
            loadEndUserGragh();
        break;
    }
}

function loadEndUserGragh(){
    
}

/*Start TextBox Lenght Validation*/
function doKeypress(control) {
    maxLength = control.attributes["maxLength"].value;
    value = control.value;
    if (maxLength && value.length > maxLength - 1) {
        event.returnValue = false;
        maxLength = parseInt(maxLength);
    }
}

function doBeforePaste(control) {
    maxLength = control.attributes["maxLength"].value;
    if (maxLength) {
        event.returnValue = false;
    }
}

function doPaste(control) {
    maxLength = control.attributes["maxLength"].value;
    value = control.value;
    if (maxLength) {
        event.returnValue = false;
        maxLength = parseInt(maxLength);
        var oTR = control.document.selection.createRange();
        var iInsertLength = maxLength - value.length + oTR.text.length;
        var sData = window.clipboardData.getData("Text").substr(0, iInsertLength);
        oTR.text = sData;
    }
}

/*End TextBox Lenght Validation*/
var businessSalesuser = "";
function GridBusinessSales(targetSalesinfogrid,type, header, detailType, sort,methodName) {
    var user = "";
    if(methodName=="GetBusinessSales"){
        user = "";
        businessSalesuser = "";
    }
    else{
        user = "1";
        businessSalesuser = "1";
    }
    var BusinessSales_onChange=  function (arg) {
        $("#" + targetSalesinfogrid).find('.eossales_selected').attr('class', 'eossales');
        $("#" + targetSalesinfogrid).find('.k-state-selected').find('.eossales').attr('class', 'eossales_selected');
            var selectedRows = this.dataItem(this.select());   
            if(methodName=="GetBusinessSales"){
                $("#MainContent_custsalesinfo_txtSelection").val(selectedRows["Code"]);
            }
            else{
                $("#txtSelection").val(selectedRows["MC"]);
            } 
        }

    //alert(header[0]);
    var listcol = [];
    var listaggregate = [];

        listcol = [

                    { field: "MC", title: "MC", hidden: true },
                    { template: '<div id="div_FlowContent'+user+'">#=FlowContent#</div>', field: "FlowContent", hidden: true},

                    { field: "Target1", template: "#=Target1# / #=Actual1#", sortable: false, filterable: false,
                        title: "M1D",width:"90px", headerTemplate: "<div style =\"text-align: right;\">" + header[0] + "</div>", footerTemplate: "<div style =\"text-align: right;\" id=\"div_A1_max"+user+"\" ></div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
                    { field: "Target2", template: '#=Target2# / #=Actual2#', sortable: false, filterable: false,
                        title: "M2D",width:"90px", headerTemplate: "<div style =\"text-align: right;\">" + header[1] + "</div>", footerTemplate: "<div style =\"text-align: right;\" id=\"div_A2_max"+user+"\" ></div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
                    { field: "Target3", template: '#=Target3# / #=Actual3#', sortable: false, filterable: false,
                        title: "M3D",width:"90px", headerTemplate: "<div style =\"text-align: right;\">" + header[2] + "</div>", footerTemplate: "<div style =\"text-align: right;\" id=\"div_A3_max"+user+"\" ></div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
                    { field: "TargetTotal", template: '#=TargetTotal# / #=ActualTotal#', sortable: false, filterable: false,
                        title: "MTD",width:"90px", headerTemplate: "<div style =\"text-align: right;\">" + header[3] + "</div>", footerTemplate: "<div style =\"text-align: right;\" id=\"div_AT_max"+user+"\" ></div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
                    { field: "Name",  sortable: true, filterable: true,
                        title: "DR",width:"220px", headerTemplate: header[4], footerTemplate: "<div id=\"div_RowCount_max"+user+"\" ></div>", format: "{0:n0}", attributes: { class: "eossales"} },
                    { field: "TargetTYTD", template: '#=TargetTYTD# / #=ActualTYTD#', sortable: false, filterable: false,
                        title: "MTYTDD",width:"90px", headerTemplate: "<div style =\"text-align: right;\">" + header[5] + "</div>", footerTemplate: "<div style =\"text-align: right;\" id=\"div_ATYTD_max"+user+"\" ></div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
                    { field: "TargetLYTD", template: '#=TargetLYTD# / #=ActualLYTD#', sortable: false, filterable: false,
                        title: "MLYTDD",width:"90px", headerTemplate: "<div style =\"text-align: right;\">" + header[6] + "</div>", footerTemplate: "<div style =\"text-align: right;\" id=\"div_ALYTD_max"+user+"\" ></div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
                    { field: "ActualPercentage", template: '#=TargetPercentage# / #=ActualPercentage#', sortable: false, filterable: false,
                        title: "MPD",width:"120px", headerTemplate: "<div style =\"text-align: right;\">" + header[7] + "</div>", footerTemplate: "<div style =\"text-align: right;\" id=\"div_APD_max"+user+"\" ></div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
                    { field: "TargetYear1",template: '#=TargetYear1# / #=ActualYear1#', sortable: false, filterable: false,
                         title: "MY1D",width:"90px", headerTemplate: "<div style =\"text-align: right;\">" + header[8] + "</div>", footerTemplate: "<div style =\"text-align: right;\" id=\"div_AY1_max"+user+"\" ></div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
                    { field: "TargetYear2", template: '#=TargetYear2# / #=ActualYear2#', sortable: false, filterable: false,
                        title: "MY2D",width:"90px", headerTemplate: "<div style =\"text-align: right;\">" + header[9] + "</div>", footerTemplate: "<div style =\"text-align: right;\" id=\"div_AY2_max"+user+"\" ></div>", format: "{0:n0}", attributes: { style: "text-align: right"} },

                    { field: "Target1Tot", title: "MC", hidden: true, footerTemplate: "<div id=\"div_T1_tot_max"+user+"\" >#: max #</div>", format: "{0:n2}" },
                    { field: "Target2Tot", title: "MC", hidden: true, footerTemplate: "<div id=\"div_T2_tot_max"+user+"\" >#: max #</div>", format: "{0:n2}" },
                    { field: "Target3Tot", title: "MC", hidden: true, footerTemplate: "<div id=\"div_T3_tot_max"+user+"\" >#: max #</div>", format: "{0:n2}" },
                    { field: "TargetTot", title: "MC", hidden: true, footerTemplate: "<div id=\"div_TT_tot_max"+user+"\" >#: max #</div>", format: "{0:n2}" },

                    { field: "RowCount", title: "RowCount", hidden: true, footerTemplate: "<div id=\"div_RowCount_tot_max"+user+"\" >#: max #</div>", format: "{0:n2}" },
                    { field: "TargetTYTDTot", title: "MC", hidden: true, footerTemplate: "<div id=\"div_TTYTD_tot_max"+user+"\" >#: max #</div>", format: "{0:n2}" },
                    { field: "TargetLYTDTot", title: "MC", hidden: true, footerTemplate: "<div id=\"div_TLYTD_tot_max"+user+"\" >#: max #</div>", format: "{0:n2}" },
                    { field: "TargetPercentage", title: "MC", hidden: true, footerTemplate: "<div id=\"div_TPD_tot_max"+user+"\" >#: max #</div>", format: "{0:n2}" },
                    { field: "TargetYear1Tot", title: "MC", hidden: true, footerTemplate: "<div id=\"div_TY1_tot_max"+user+"\" >#: max #</div>", format: "{0:n2}" },
                    { field: "TargetYear2Tot", title: "MC", hidden: true, footerTemplate: "<div id=\"div_TY2_tot_max"+user+"\" >#: max #</div>", format: "{0:n2}" },

                    { field: "Actual1Tot", title: "MC", hidden: true, footerTemplate: "<div id=\"div_A1_tot_max"+user+"\" >#: max #</div>", format: "{0:n2}" },
                    { field: "Actual2Tot", title: "MC", hidden: true, footerTemplate: "<div id=\"div_A2_tot_max"+user+"\" >#: max #</div>", format: "{0:n2}" },
                    { field: "Actual3Tot", title: "MC", hidden: true, footerTemplate: "<div id=\"div_A3_tot_max"+user+"\" >#: max #</div>", format: "{0:n2}" },
                    { field: "ActualTot", title: "MC", hidden: true, footerTemplate: "<div id=\"div_AT_tot_max"+user+"\" >#: max #</div>", format: "{0:n2}" },
                    { field: "ActualTYTDTot", title: "MC", hidden: true, footerTemplate: "<div id=\"div_ATYTD_tot_max"+user+"\" >#: max #</div>", format: "{0:n2}" },
                    { field: "ActualLYTDTot", title: "MC", hidden: true, footerTemplate: "<div id=\"div_ALYTD_tot_max"+user+"\" >#: max #</div>", format: "{0:n2}" },
                    { field: "ActualPercentage", title: "MC", hidden: true, footerTemplate: "<div id=\"div_APD_tot_max"+user+"\" >#: max #</div>", format: "{0:n2}" },
                    { field: "ActualYear1Tot", title: "MC", hidden: true, footerTemplate: "<div id=\"div_AY1_tot_max"+user+"\" >#: max #</div>", format: "{0:n2}" },
                    { field: "ActualYear2Tot", title: "MC", hidden: true, footerTemplate: "<div id=\"div_AY2_tot_max"+user+"\" >#: max #</div>", format: "{0:n2}" }
                ];
        listaggregate = [
                    { field: "Target1Tot", aggregate: "max" },
                    { field: "Target2Tot", aggregate: "max" },
                    { field: "Target3Tot", aggregate: "max" },
                    { field: "TargetTot", aggregate: "max" },
                    { field: "RowCount", aggregate: "max" },
                    { field: "TargetTYTDTot", aggregate: "max" },
                    { field: "TargetLYTDTot", aggregate: "max" },
                    { field: "TargetPercentage", aggregate: "max" },
                    { field: "TargetYear1Tot", aggregate: "max" },
                    { field: "TargetYear2Tot", aggregate: "max" },

                    { field: "Actual1Tot", aggregate: "max" },
                    { field: "Actual2Tot", aggregate: "max" },
                    { field: "Actual3Tot", aggregate: "max" },
                    { field: "ActualTot", aggregate: "max" },
                    { field: "ActualTYTDTot", aggregate: "max" },
                    { field: "ActualLYTDTot", aggregate: "max" },
                    { field: "ActualPercentage", aggregate: "max" },
                    { field: "ActualYear1Tot", aggregate: "max" },
                    { field: "ActualYear2Tot", aggregate: "max" }

                    ];

            $("#" + targetSalesinfogrid).html("");
            $("#" + targetSalesinfogrid).kendoGrid({
                height: 700,
                columns: listcol,
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                resizable: true,
                filterable: true,
                selectable: "single",
                change: BusinessSales_onChange,
                dataBound: BusinessSalesGrid_onDataBound,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 20,
                    resizable: true,

            // group: [{ field: "CostYear", attributes: { class: "homegridheader", style: "color:\\#0c0"}}],


            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "Originator",
                    fields: {

                        MC: { validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "Service/lead_customer/common.asmx/"+methodName,//"GetBusinessSalesEndUser", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { detailType: detailType, sortText: sort ,type:type},
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            aggregate: listaggregate

        }
    });
    //setvis(type);
//            var grid = $("#gridbusinesssales").data("kendoGrid");
//            grid.hideColumn('M1D');
}

function BusinessSalesGrid_onDataBound(arg) {
        this.select(arg.sender.tbody.find("tr:first"));
        $('#div_A1_max'+businessSalesuser).html('<b>' + $('#div_T1_tot_max'+businessSalesuser).html() + ' / '+  $('#div_A1_tot_max'+businessSalesuser).html() + '</b>');
        $('#div_A2_max'+businessSalesuser).html('<b>' + $('#div_T2_tot_max'+businessSalesuser).html() + ' / '+  $('#div_A2_tot_max'+businessSalesuser).html() + '</b>');
        $('#div_A3_max'+businessSalesuser).html('<b>' + $('#div_T3_tot_max'+businessSalesuser).html() + ' / '+  $('#div_A3_tot_max'+businessSalesuser).html() + '</b>');
        $('#div_AT_max'+businessSalesuser).html('<b>' + $('#div_TT_tot_max'+businessSalesuser).html() + ' / '+  $('#div_AT_tot_max'+businessSalesuser).html() + '</b>');
        $('#div_RowCount_max'+businessSalesuser).html('<b>' + $('#div_RowCount_tot_max'+businessSalesuser).html() + '</b>');

        $('#div_ATYTD_max'+businessSalesuser).html('<b>' + $('#div_TTYTD_tot_max'+businessSalesuser).html() + ' / '+  $('#div_ATYTD_tot_max'+businessSalesuser).html() + '</b>');
        $('#div_ALYTD_max'+businessSalesuser).html('<b>' + $('#div_TLYTD_tot_max'+businessSalesuser).html() + ' / '+  $('#div_ALYTD_tot_max'+businessSalesuser).html() + '</b>');
        $('#div_APD_max'+businessSalesuser).html('<b>' + $('#div_TPD_tot_max'+businessSalesuser).html() + ' / '+  $('#div_APD_tot_max'+businessSalesuser).html() +'</b>');
        $('#div_AY1_max'+businessSalesuser).html('<b>' + $('#div_TY1_tot_max'+businessSalesuser).html() + ' / '+  $('#div_AY1_tot_max'+businessSalesuser).html() + '</b>');
        $('#div_AY2_max'+businessSalesuser).html('<b>' + $('#div_TY2_tot_max'+businessSalesuser).html() + ' / '+  $('#div_AY2_tot_max'+businessSalesuser).html() + '</b>');

        if(businessSalesuser==""){
            $("#MainContent_custsalesinfo_lblCurrentFlow").html( $('#div_FlowContent'+businessSalesuser).html());
            $("#MainContent_custsalesinfo_lblCurrentFlow").slideDown('slow');
        }else{
            $("#lblCurrentFlowEnduser").html( $('#div_FlowContent'+businessSalesuser).html());
        }  
}

function Customer_onChange(arg) {
    var selectedRows = this.dataItem(this.select());            
    $("#MainContent_custsalesinfo_txtSelection").val(selectedRows["MC"]);
    $("#MainContent_custsalesinfo_HiddenFieldDescription").val(selectedRows["MD"]);
}

function EndUser_onChange(arg) {
    var selectedRows = this.dataItem(this.select());            
    $("#txtSelection").val(selectedRows["MC"]);
}

        
function SetSalesInfo(detailType,targetSalesinfogrid,methodName,pgindex) {

    var entityGrid = $("#"+targetSalesinfogrid).data("kendoGrid");
    var selectedRows = entityGrid.dataItem(entityGrid.select());
    
    var code =  "";
    var desc =  "";
    var displayOption ="";

    if(methodName=='GetBusinessSales'){
        displayOption = $("#MainContent_custsalesinfo_DropDownDisplayOption").val()
    }
    else{
        displayOption = $("#DropDownDisplayOption").val()
    }
    if (methodName == "GetBusinessSales") {
        LoadSalesInfoChart(displayOption, "cust");
    }
    else {
        LoadSalesInfoChart(displayOption, "enduser");
    }

    
    if ((selectedRows == "" || selectedRows == undefined) && pgindex != 2) {
        alert('Please select the Row');
        return ;
    }
    else {
        if(pgindex != 2){
            code = encodeurl(selectedRows["Code"]);
            desc = selectedRows["Name"];   
        }else{
            code = '';
            desc = ''; 
        }
       var market = $("#MainContent_custsalesinfo_DropDownMarket").val();
       var isCmp = 0;
       if ($("#MainContent_custsalesinfo_custsalesinfo_RadioButtonCMP").attr("checked")) {
            isCmp = 1;
       }
        var root = ROOT_PATH + "business_sales/process_forms/processmaster.aspx?fm=SalesInfo&type=Set&code=" + code +
                    "&desc=" + desc + "&dtype=" + detailType + "&option=" + displayOption +"&user="+methodName + "&pgindex="+ pgindex
                    + "&market=" + market+"&isCmp=" +isCmp;

        $.ajax({
            url: root,
            dataType: "html",
            cache: false,
            success: function (msg) {
                if (msg == "true") {
                $("#MainContent_custsalesinfo_DropDownGroupBy").val(detailType);
                    LoadTitle(detailType, "n", "0", displayOption,"",targetSalesinfogrid,methodName);
                    if(methodName =="GetBusinessSales"){
                        SetSalesInfoCss(detailType,"cust");
                    }
                    else{
                        SetSalesInfoCss(detailType,"enduser");
                    }
                    //Show Next button.
//                    if(pgindex!="0"){                        
//                        $("a#href_close").css("display","block");
//                        $("a#href_close").bind("click",function(){
//                        
//                            loadbackstate(detailType,code,desc,targetSalesinfogrid,methodName);
//                        });
//                    }else{
//                        $("a#href_close").css("display","none");
//                    }
                }
                else {
                    //$("#MainContent_HiddenFieldCallCycleCount").val("0");
                }
            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}


function LoadSalesInfoChart(dispOp,user) {
    var root = ROOT_PATH + "business_sales/process_forms/processmaster.aspx?fm=SalesInfoChart&type=query&dispOp=" + dispOp +"&user="+user;

    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg != "") {
                if(user =="cust"){
                    $("#div_trend").html(msg);
                }
                else{
                    $("#div_trend1").html(msg);
                }
            }
            else {
                //$("#MainContent_HiddenFieldCallCycleCount").val("0");
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });

}

function LoadSalesInfoTrendChart(code,user) {

    var accumulate = '0';
//    if ($('#MainContent_custsalesinfo_rbTrendGraphAccumulate').is(':checked')) {
//        accumulate = '1'; 
//    }

    var graph = "";

    if(user =="cust"){
        graph = $("#MainContent_custsalesinfo_custsalesinfo_DropDownGraphType").val();
        if ($('#MainContent_custsalesinfo_custsalesinfo_rbTrendGraphAccumulate').is(':checked')) {
            accumulate = '1'; 
        }
    }
    else if(user =="cust_sh"){
        graph = $("#MainContent_custsalesinfo_salesHistoryDetails_DropDownGraphTypeSalesHistory").val();
        if ($('#MainContent_custsalesinfo_salesHistoryDetails_rbTrendGraphAccumulateSalesHistory').is(':checked')) {
            accumulate = '1'; 
        }
    }
    else if(user =="enduser_sh"){
       graph = $("#salesHistoryDetailsEnduser_DropDownGraphTypeSalesHistoryEndUser").val();
       if ($('#salesHistoryDetailsEnduser_rbTrendGraphAccumulateSalesHistoryEndUser').is(':checked')) {
            accumulate = '1'; 
       }
    }
    else{
       graph = $("#enduserinfo_DropDownGraphTypeenduser").val();
       if ($('#enduserinfo_rbTrendGraphAccumulateenduser').is(':checked')) {
            accumulate = '1'; 
       }
    }
    var root = ROOT_PATH + "business_sales/process_forms/processmaster.aspx?fm=TrendChart&type=query&graph=" + graph + "&Accum="+accumulate+"&code=" + code+"&user="+user;


    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg != "") {
                if(user =="cust"){
                    $("#div_chart").html(msg);
                }
                else if(user =="cust_sh"){
                    $("#div_chart_SalesHistory").html(msg);
                }
                else if(user =="enduser_sh"){
                    $("#div_chart_SalesHistoryEndUser").html(msg);
                }
                else{
                   $("#div_chart_enduser").html(msg);
                }
            }
            else {
                //$("#MainContent_HiddenFieldCallCycleCount").val("0");
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });

}

function LoadTitle( detailType, sastFlag, month, type, sort,targetSalesinfogrid,methodName) {
    if(methodName=='GetBusinessSales'){
        $("#MainContent_custsalesinfo_HiddenFieldDetailType").val(detailType); 
    }else{
        $("#HiddenFieldDetailType").val(detailType); 
    }
    $.ajax({
        type: "POST",
//        url: ROOT_PATH + "business_sales/transaction/businesssales_enquiry.aspx/LoadTitle",
        url: ROOT_PATH + "business_sales/process_forms/processmaster.aspx/SetRepTargetInfo",
        data: "{detailTypeparam : '" + detailType + "',sastFlag:'" + sastFlag + "',monthparam:'" + month + "'}",
        cache: false,
        contentType: "application/json",
        dataType: "json",
        success: function (msg) {
            // Replace the div's content with the page method's return.
            //debugger;
            GridBusinessSales(targetSalesinfogrid, type, msg.d, detailType, sort,methodName);
            GridBusinessDailySales("gridDailySales", type, msg.d, detailType, sort,"GetTodaySalesDrilldown");
            
        }
    });
}

function loadSortSalesInfoGrid(targetSalesinfogrid,methodName,user) {
    var id = $("#MainContent_custsalesinfo_DropDownListSort").val();
    var displayOption = $("#MainContent_custsalesinfo_DropDownDisplayOption").val();
    var detailType = $("#MainContent_custsalesinfo_HiddenFieldDetailType").val();
    if(user != "cust"){
        id = $("#DropDownListSort").val();
        displayOption = $("#DropDownDisplayOption").val();
        detailType = $("#HiddenFieldDetailType").val();
    }

    var sort = "";
    if (id == "6") {
        sort = "M1T";
    }
    else if (id == "3") {
        sort = "M1D";
    }
    else if (id == "9") {
        sort = "MTYTDT";
    }
    else if (id == "10") {
        sort = "MTYTDD";
    }
    else if (id == "11") {
        sort = "MLYTDT";
    }
    else if (id == "12") {
        sort = "MLYTDD";
    }
    else if (id == "1") {
        sort = "MC";
    }

    LoadTitle( detailType, "n", "0", displayOption, sort,targetSalesinfogrid,methodName)
}

function loadbackstate(back_detailtype,back_code,back_desc,info_tgtgrid,info_mthdName){
    if(info_mthdName=='GetBusinessSales'){
        SetSalesInfo(back_detailtype,info_tgtgrid,info_mthdName,"2",back_code,back_desc);
    }
}


function LoadBackSalesInfo(user) {

    var targetSalesinfogrid =  "";
    var methodName =  "";
    var displayOption ="";

    if(user=='cust'){
        displayOption = $("#MainContent_custsalesinfo_DropDownDisplayOption").val();
        targetSalesinfogrid = "gridCustomerSales";
        methodName =  "GetBusinessSales"
    }
    else{
        displayOption = $("#DropDownDisplayOption").val();
        targetSalesinfogrid = "gridEndUserSales";
        methodName =  "GetBusinessSalesEndUser";
    }
  

    var root = ROOT_PATH + "business_sales/process_forms/processmaster.aspx?fm=LoadBackSalesInfo&type=Set&option=" + displayOption +"&user="+user;

    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg != "") {
                var res = msg.split("|"); 
                $("#MainContent_custsalesinfo_DropDownMarket").val(res[1]);
                $("#MainContent_custsalesinfo_DropDownGroupBy").val(res[0]);
                
                LoadTitle(res[0], "n", "0", displayOption,"",targetSalesinfogrid,methodName);
                if(user =="cust"){
                    SetSalesInfoCss(msg,"cust");
                }
                else{
                    SetSalesInfoCss(msg,"enduser");
                }
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
    
}

function SetSalesInfoCss(detailType,user){
    if(user == "cust"){
        $("#a_bus_area").removeClass("current");
        $("#a_market").removeClass("current");
        $("#a_brand").removeClass("current");
        $("#a_cat_group").removeClass("current");
        $("#a_cat_sub_group").removeClass("current");
        $("#a_product").removeClass("current");
        $("#a_customer").removeClass("current");
        $("#a_custgroup").removeClass("current");
        $("#a_rep").removeClass("current");
        $("#a_repgroup").removeClass("current");
        $("#a_state").removeClass("current");

        switch (detailType)
        {
            case "bus area":
                $("#a_bus_area").addClass("current");
                break;
            case "market":
                $("#a_market").addClass("current");
                break;
            case "brand":
                $("#a_brand").addClass("current");
                break;
            case "cat_group":
                $("#a_cat_group").addClass("current");
                break;
            case "cat_sub_group":
                $("#a_cat_sub_group").addClass("current");
                break;
            case "product":
                $("#a_product").addClass("current");
                break;
            case "customer":
                $("#a_customer").addClass("current");
                break;
            case "custgroup":
                $("#a_custgroup").addClass("current");
                break;
            case "rep":
                $("#a_rep").addClass("current");
                break;
            case "repgroup":
                $("#a_repgroup").addClass("current");
                break;
            case "state":
                $("#a_state").addClass("current");
                break;
        }
    }
    else {
        $("#a_end_brand").removeClass("current");
        $("#a_end_product").removeClass("current");
        $("#a_end_customer").removeClass("current");
        $("#a_end_rep").removeClass("current");
        $("#a_end_state").removeClass("current");

        switch (detailType)
        {
            case "brand":
                $("#a_end_brand").addClass("current");
                break;
            case "product":
                $("#a_end_product").addClass("current");
                break;
            case "customer":
                $("#a_end_customer").addClass("current");
                break;
            case "rep":
                $("#a_end_rep").addClass("current");
                break;
            case "state":
                $("#a_end_state").addClass("current");
                break;
        }
    }
    SetSalesInfopageTitle(detailType, user);
}

function SetSalesInfopageTitle(detailType,user){
    var headerText = "";
    switch (detailType){
        case "bus area":
            headerText = "Business Sales Enquiry - Business Area";
            break;
        case "market":
            headerText = "Business Sales Enquiry - Market";
            break;
        case "brand":
            headerText = "Business Sales Enquiry - Brand";
            break;
        case "cat_group":
            headerText = "Business Sales Enquiry - Product Group";
            break;
        case "cat_sub_group":
            headerText = "Business Sales Enquiry - Product Sub Group";
            break;
        case "product":
            headerText = "Business Sales Enquiry - Product";
            break;
        case "customer":
            headerText = "Sales Targets - Customer";
            break;
        case "custgroup":
            headerText = "Business Sales Enquiry - Customer Group";
            break;
        case "rep":
            headerText = "Sales Targets - Rep";
            break;
        case "repgroup":
            headerText = "Business Sales Enquiry - Sales Reps";
            break;
        case "state":
            headerText = "Business Sales Enquiry - State";
            break;
        default:
            headerText = "Business Sales Enquiry";
            break;
    }
    if (user == "cust")
        $("#div_report_header").html(headerText);
    else
        $("#div_report_header1").html(headerText); ;
}
function AppointmentsDataSource1() {
    var dataSource = new kendo.data.DataSource({
        transport: {
            read: {
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/GetPendingAppointmentsForHome",
                contentType: 'application/json; charset=utf-8',
                datatype: "json"
            }, //read
            parameterMap: function (data, operation) {
                return JSON.stringify(data);
            }
        }, // transport
        serverFiltering: true,
        serverSorting: true,
        serverPaging: true,
        pageSize: 10,
        schema: {
            data: "d.Data",
            total: "d.Total",
            model: { // define the model of the data source. Required for validation and property types.
                //id: "SourceId",
                fields: {
                    StartTime: { type: "Date", format: DATE_FORMAT },
                    EndTime: { type: "Date", format: DATE_FORMAT }
                }
            }
        } // schema
    });

    return dataSource;
}

function AppointmentsActivityPending() {
    $("#div_content_pending_appointments").removeAttr("class");
    $("#div_content_pending_appointments").attr("class", "divshow");
    $("#pendingapp_div_content_pending_appointments").attr("class", "divshow");

    var dataSource = AppointmentsDataSource1();
    $("#listView_pending_appointments").html("");
    $("#listView_pending_appointments").kendoGrid({

        columns: [
            { template: kendo.template($("#appointments_pending_template").html())
                , headerAttributes: {
                    style: "display: none"
                }
            }
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: false,
        filterable: false,
        dataSource: dataSource,
        dataBound: pendingapointment_dataBound
        //rowTemplate: kendo.template($("#outstanding_template").html())
    });
}

function pendingapointment_dataBound(e) {
    var grid = $('#listView_pending_appointments').data('kendoGrid');

    if (grid.dataSource == null || grid.dataSource._total == 0) {
        $("div#listView_pending_appointments div.k-grid-content").css("height", "35px");
    }
    else {
        $("div#listView_pending_appointments div.k-grid-content").css("height", "465px");
    }
}


function GetErrorMessage(msg) {
    var test = "<div id='messagered' class='message-red' rel='error'><table border='0' width='100%' cellpadding='0' cellspacing='0'>";
    test = test + "<tr><td class='red-left'>" + msg + "</td><td class='red-right'><a class='close-red'><img src='"
                + ROOT_PATH + "assets/images/icon_close_red.gif' alt='' /></a></td></tr></table></div>";
    return test;
}


function LookupAllCatalog(griddiv, repType) {
    
    $("#" + griddiv).kendoGrid({
        height: '380',
        columns: [
            { template: '<a href="javascript:SetCatlogCode(\'#=CatlogCode#\',\'#=Description#\',#=Conversion#,#=Price#);">#=CatlogCode#</a>', field: "CatlogCode", title: "Catlog Code", width: "120px" },
            { field: "Description", title: "Description" },
            { field: "DeliveryFrequency", title: "Delivery Frequency", width: "130px" },
            { field: "Conversion", title: "Conversion", width: "80px", hidden: true },
            { field: "Price", title: "Price", width: "80px", hidden: true }
    ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        //dataBound: LeadCustomerGrid_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    fields: {
                        CatlogCode: { type: "string" },
                        Description: { type: "string" }
                        
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/lead_entry_service.asmx/GetCustomerCatalogDataAndCount",
                    contentType: "application/json; charset=utf-8",
                    data: { repType: repType },
                    type: "POST",
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            var wnd = $("#modalWindow").kendoWindow({
                                title: "Lookup",
                                modal: true,
                                visible: false,
                                resizable: false,
                                width: 550
                            }).data("kendoWindow");
                            wnd.center().open();
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

function LookupAllPromotions(griddiv, repType) {

    $("#" + griddiv).kendoGrid({
        height: '380',
        columns: [
//            { template: '<a href="javascript:SetCatlogCode(\'#=CatlogCode#\',\'#=Description#\',#=Conversion#,#=Price#);">#=CatlogCode#</a>', field: "CatlogCode", title: "Catlog Code", width: "120px" },
//            { field: "Description", title: "Description" },
//            { field: "DeliveryFrequency", title: "Delivery Frequency", width: "130px" },
//            { field: "Conversion", title: "Conversion", width: "80px", hidden: true },
//            { field: "Price", title: "Price", width: "80px", hidden: true }
                    { field: "PromotionId", title: "Promotion Id", width: "80px", hidden: true },
                    { template: '<a href="javascript:SetPromotionCode(\'#=PromotionId#\',\'#=PromotionName#\');">#=PromotionName#</a>', field: "PromotionName", title: "Promotion Name", width: "100px" }
    ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        //dataBound: LeadCustomerGrid_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    fields: {
                        PromotionId: { validation: { required: true} },
                        PromotionName: { validation: { required: true} }

                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetAllPromotions",
                    contentType: "application/json; charset=utf-8",
                    data: { repType: repType },
                    type: "POST",
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            var wnd = $("#modalWindow").kendoWindow({
                                title: "Promotions",
                                modal: true,
                                visible: false,
                                resizable: false,
                                width: 550
                            }).data("kendoWindow");
                            wnd.center().open();
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

function HandleSlidder() {
    var root = ROOT_PATH + "process_forms/processmaster.aspx?type=get&fm=home";

    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg != "") {
                if (msg == "op") {
                    //Open Slider.
                    $('#style-selector').animate({
                        right: '0'
                    });
                    $('a#link_slidder').animate({
                        right: '300px'
                    });

                    $("a#id_click").removeClass('icon-chevron-left');
                    $("a#id_click").addClass('icon-chevron-right return');

                    setTimeout(function () {
                        $("div#maincontentarea").removeClass("maincontentarea").addClass("maincontentarea_load");
                    }, 100);
                }
                else if (msg == "cl") {
                    $('#style-selector').animate({
                        right: '-292px'
                    });

                    $('a#link_slidder').animate({
                        right: '-2px'
                    });

                    $("a#id_click").removeClass('icon-chevron-right return');
                    $("a#id_click").addClass('icon-chevron-left');

                    setTimeout(function () {
                        $("div#maincontentarea").removeClass("maincontentarea_load").addClass("maincontentarea");
                    }, 80);
                }
            }
            else {
                //bydefault open the slider.
                OpenSlider();
            }
        },
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
}

function SetCatlogCode(catlogCode, Description, Conversion, Price) {
    $('#MainContent_HiddenFieldCode').val(catlogCode);
    $('#MainContent_HiddenFieldDesc').val(Description);
    $('#MainContent_HiddenFieldPrice').val(Price);
    $('#MainContent_HiddenFieldConversion').val(Conversion);
    var b = $('#MainContent_HiddenFieldCode').val();
    $("#modalWindow").data("kendoWindow").close();
    loadProductGrid();
}

function SetPromotionCode(promotionId, promotionName) {
    $('#MainContent_HiddenFieldPromoid').val(promotionId);
    $('#MainContent_HiddenFieldPromoName').val(promotionName);
    var a = $('#MainContent_HiddenFieldPromoid').val();
}
function SetCatlogCode(catlogCode, Description, Conversion, Price) {
    $('#MainContent_HiddenFieldCode').val(catlogCode);
    $('#MainContent_HiddenFieldDesc').val(Description);
    $('#MainContent_HiddenFieldPrice').val(Price);
    $('#MainContent_HiddenFieldConversion').val(Conversion);


    $("#modalWindow").data("kendoWindow").close();
    loadPromotionGrid();
}

function loadHolidaySchedular(start_date,end_date,originator, startDate) {
    //get drpdown values
    var cmbase = $('#MainContent_cmbase').val();
    var cmbdis = $('#MainContent_cmbdis').val();
    var cmbrep = $('#MainContent_cmbrep').val();

    //set originator based on drodown selected value
    if (cmbase != 'ALL'){    
        originator = cmbase;
        if(cmbdis != 'ALL'){
            originator = cmbdis;
            if(cmbrep != 'ALL'){
                originator = cmbrep;
            }
        }
    }

//debugger;
var date = new Date();
    $("#scheduler").html("");
    $("#scheduler").show();
    //alert(startDate);
  $("#scheduler").kendoScheduler({
//        date: new Date("2013/6/13"),
//        startTime: new Date("2013/6/13 07:00 AM"),
        //date: $.now(),
        //startTime: $.now(),
        date: startDate,
        dataBound: scheduler_dataBound,
        //selectable: true,
        //change: scheduler_change,
        save: scheduler_save,
        navigate: scheduler_navigate,
        moveEnd: scheduler_moveEnd,
        edit: scheduler_edit,
        startTime: startDate,
        height: 600,
        views: [
            {type: "month" , selected: true }
        ],
      //  timezone: "Etc/UTC",
        dataSource: {
            batch: true,
            sync: function() {
                this.read();
            },
            transport: {
                read: {
                    url: ROOT_PATH+ "service/calendar/calendar_service.asmx/GetHolidays",
                   //specify the URL which data should return the records. This is the Read method of the Products.svc service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { startdate: start_date, enddate:end_date , username : originator},
                    type: "POST"
                },
                update: {
                    url: ROOT_PATH+ "service/calendar/calendar_service.asmx/update",
                    contentType: "application/json; charset=utf-8",// tells the web service to serialize JSON
                    data: { username : originator, test : 123 },
                    type: "POST",
                    beforeSend: function (req) {
                        //debugger;
                        //req.abort("");
                    }
                },
                create: {
                    url: ROOT_PATH+ "service/calendar/calendar_service.asmx/create",
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    dataType: "json",
                    data: { username : originator },
                    type: "POST",
                    beforeSend: function (req) {
                        //debugger;
                    }
                },
                destroy: {
                    url: ROOT_PATH+ "service/calendar/calendar_service.asmx/destroy",
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    //data: { testcode: 123 },
                    type: "POST"
                },
//                parameterMap: function(options, operation) {
//                    if (operation !== "read" && options.models) {
//                    options.models = $.extend(options.models, data);
//                        return {models: kendo.stringify(options.models)};
//                    }
//                }
                parameterMap: function (data, operation) {
                   // data = $.extend( data);
                   return JSON.stringify(data);
                   
                   //return {models: JSON.stringify(data.models)};
                }
            },
            schema: {
                data: "d",
                model: {
                    id: "taskId",
                    fields: {
                        taskId: { from: "HolidayId", type: "number" },
                        title: { from: "Title", defaultValue: "No title", validation: { required: true } },
                        start: { type: "date", from: "Date" },
                        end: { type: "date", defaultValue:"2014-04-08T19:30:00.000Z" ,from: "EndDate" },
                        ends: { type: "date", defaultValue:"2014-04-08T19:30:00.000Z" , from: "End" },
                        starts: { type: "date", from: "Start" , defaultValue:"2014-04-08T19:30:00.000Z"},
                        startTimezone: { from: "StartTimezone" },
                        endTimezone: { from: "EndTimezone"  },
                        description: { from: "Description" },
                        typedescription: { from: "TypeDescription" },
                        holidaytype: { from: "HolidayType", defaultValue: "POYA" },                     
                        //extensionData: { from: "ExtensionData",defaultValue:null },
                        recurrenceId: { from: "RecurrenceID",defaultValue:0},
                        recurrenceRule: { from: "RecurrenceRule" },
                        recurrenceException: { from: "RecurrenceException" },
                        ownerId: { from: "OwnerID", defaultValue: 1 },
                        isAllDay: { type: "boolean", from: "IsAllDay" }
                    }
                }
            }
        },
//        navigate: function (e) {
//        var view = $("#scheduler").data("kendoScheduler").view();
//                            //$("#scheduler").data("kendoScheduler").view().startDate().toString("dd/MM/yyyy")
//                          
//                            var start = view.startDate();
//                            var end =  view.endDate();
//            // loadHolidaySchedular(view.startDate().toISOString(),view.endDate().toISOString());
//            $("#scheduler").hide();
//            $("#scheduler").html("");
//            loadHolidaySchedular_temp(new Date(start.getUTCFullYear(), start.getUTCMonth()-1, start.getUTCDate(),  start.getUTCHours(), start.getUTCMinutes(), start.getUTCSeconds()),new Date(end.getUTCFullYear(), end.getUTCMonth()-1, end.getUTCDate(),  end.getUTCHours(), end.getUTCMinutes(), end.getUTCSeconds()));

//        },
        resources: [
            {
                field: "holidaytype",
                title: "Holiday Type",
                dataSource: [
                    { text: "Poya Day", value: "POYA", color: "#fcc40d" },
                    { text: "Merchantile Holiday", value: "MERC", color: "#51a0ed" },
                    { text: "Bank Holiday", value: "BANK", color: "#56ca85" },
                    { text: "Rep On Leave", value: "RONL", color: "#345567" },
                    { text: "Other", value: "OTHE", color: "#235122" },
                ]
            }
            //,
//            {
//                field: "ownerId1",
//                title: "User",
//                dataSource: [
//                    { text: "Kav1", value: 1, color: "#f8a398" },
//                    { text: "Kav2", value: 2, color: "#51a0ed" },
//                    { text: "Kav3", value: 3, color: "#56ca85" },
//                     { text: "Kav4", value: 4, color: "#56ca83" }
//                ]
//            }
        ],
        editable:{
            template:$("#editor").html()
        }
    });

//        var scheduler = $("#scheduler").data("kendoScheduler");
//    var view = scheduler.view();
//    alert(view.startDate(1));
}

function scheduler_change(e) {
    //alert(e.start);
    $("#MainContent_HiddenFieldLastEnteredDate").val(e.start.toDateString());
    //alert(e.start.toDateString());
}

function scheduler_navigate(e) {
        //kendoConsole.log(kendo.format("navigate:: action:{0}; view:{1}; date:{2:d};", e.action, e.view, e.date));
        $("#MainContent_HiddenFieldNavigatedDate").val(e.date.toDateString());
        //alert(e.date);
    }

function scheduler_save(e) {        
    //Call to a Service and Find out whether any activity exists for the perticular date for the current user
    // and whether the itinerary for the user is in approved stage
    //debugger;
    if($("#MainContent_hfHolidayDateChangedTo").val() == ''){
        $("#MainContent_hfHolidayDateChangedTo").val(e.event.start.toDateString());
    }
    var holidayDate = e.event.start.toDateString();
    //debugger;
    var proceedOrNot = ValidationBeforeSaveHoliday(holidayDate, e.event.id);
    //debugger; 
    if(proceedOrNot == 'discard')
    {   //debugger;
        e.preventDefault();
    }

    //Show Loader
    $("#div_loader").show();
}

function scheduler_dataBound(e) {
        //Hide Loader
        $("#div_loader").hide();
    }

function scheduler_moveEnd(e) {
    //debugger;
    //alert("From" + e.event.start.toDateString() + " To " + e.start.toDateString());
    //alert(e.start.toDateString());
    $("#MainContent_hfHolidayDateChangedFrom").val(e.event.start.toDateString());
    $("#MainContent_hfHolidayDateChangedTo").val(e.start.toDateString());    
}

function scheduler_edit(e) {
    //debugger;
    $("#MainContent_hfHolidayDateChangedFrom").val(e.event.start.toDateString());
}

function ValidationBeforeSaveHoliday(holidayDate, holidayId) {
    //debugger;
    var param = { 'holidayDate': holidayDate};
    
    var holidayChangedFromDate = $("#MainContent_hfHolidayDateChangedFrom").val();
    var holidayChangedToDate = $("#MainContent_hfHolidayDateChangedTo").val();

    var returnValue = 'notSet';

    //$("div#div_loader").show();
    var url = ROOT_PATH + "service/calendar/calendar_service.asmx/IsActivityExistsForTheSavedHoliday";
    $.ajax({
        type: "POST",
        async: false,
        url: url,
        data: JSON.stringify(param),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.hasOwnProperty('d')) {
                msg = response.d;
            } else {
                msg = response;
            }
            if (msg == "true") {
                //if yes show confirmation msg
                if (confirm("Your Itinerary has been already approved, Do you want to Save the changes ?")) {
                    //Confirmed - Deactivate activity for the day and send mail    
                    $("div#div_loader").show();                    
                    var urlD = ROOT_PATH + "service/calendar/calendar_service.asmx/DeactivateActivityForTheSavedHoliday";

                    var param2 = { 'holidayDate': holidayDate, 'holidayChangedFromDate': holidayChangedFromDate, 'holidayChangedToDate': holidayChangedToDate, 'holidayId': holidayId  };

                    $.ajax({
                        type: "POST",
                        async: false,
                        url: urlD,
                        data: JSON.stringify(param2),
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (response) {
                            if (response.hasOwnProperty('d')) {
                                msg = response.d;
                            } else {
                                msg = response;
                            }
                            if (msg == "true") {
                                //Activity for the day has been deleted and a notification mail has been sent to RM
                                var sucessMsg = GetSuccesfullMessageDiv("Change was Successfully Saved, Approval Mail will be sent.", "MainContent_div_message");
                                $('#MainContent_div_message').css('display', 'block');
                                $("#MainContent_div_message").html(sucessMsg);
                                $("div#div_loader").hide();
                                hideStatusDiv("MainContent_div_message");
                                returnValue = 'proceed';
                                //return returnValue;
                            }
                            else if (msg == "mailNotSent") {
                                var errorMsg = GetErrorMessageDiv("Change was Successfully Saved, error in sending approval mail.", "MainContent_div_message");
                                $('#MainContent_div_message').css('display', 'block');
                                $("#MainContent_div_message").html(errorMsg);
                                $("div#div_loader").hide();
                                hideStatusDiv("MainContent_div_message");
                                returnValue = 'mailNotSent';
                                //return returnValue;
                            }
                            else {
                                var errorMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message");
                                $('#MainContent_div_message').css('display', 'block');
                                $("#MainContent_div_message").html(errorMsg);
                                $("div#div_loader").hide();
                                hideStatusDiv("MainContent_div_message");
                                returnValue = 'error';
                                //return returnValue;
                            }
                        },
                        error: function (response) {
                            var errorMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(errorMsg);
                            $("div#div_loader").hide();
                            hideStatusDiv("MainContent_div_message");
                            returnValue = 'error';
                            //return returnValue;
                        }
                    });
                }
                else {
                    //discard change
                    $("div#div_loader").hide();
                    //debugger;
                    //e.preventDefault();
                    returnValue = 'discard';
                    //return returnValue;
                }
            }
            else if (msg == "false") {
                //proceed to save holiday
                $("div#div_loader").hide();
                returnValue = 'proceed';
                //return returnValue;
            }
            else {
                var errorMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);
                $("div#div_loader").hide();
                hideStatusDiv("MainContent_div_message");
                returnValue = 'error';
                //return returnValue;
            }
        },
        error: function (response) {
            var errorMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message");
            $('#MainContent_div_message').css('display', 'block');
            $("#MainContent_div_message").html(errorMsg);
            //$("div#div_loader").hide();
            hideStatusDiv("MainContent_div_message");
            returnValue = 'error';
            //return returnValue;
        }
    });

    $("#MainContent_hfHolidayDateChangedFrom").val('');
    $("#MainContent_hfHolidayDateChangedTo").val('');
    $("div#div_loader").hide();
    return returnValue;
}

//        var scheduler = $("#scheduler").data("kendoScheduler");
//    var view = scheduler.view();
//    alert(view.startDate(1));
//}

//function loadActivityPlanner() {
//debugger;
//  $("#scheduler").kendoScheduler({
////        date: new Date("2013/6/13"),
////        startTime: new Date("2013/6/13 07:00 AM"),
//        date: $.now(),
//        startTime: $.now(),
//        height: 600,
//        views: [

//            {type: "month" , selected: true }
//        ],
//        //timezone: "Etc/UTC",
//        dataSource: {
//            batch: true,
//            transport: {
//                read: {
//                    url: ROOT_PATH+ "service/calendar/calendar_service.asmx/GetHolidays",

//                   //specify the URL which data should return the records. This is the Read method of the Products.svc service.
//                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
//                    data: { testcode:e.view },
//                    type: "POST"
//                },
//                update: {
//                    url: ROOT_PATH+ "service/calendar/calendar_service.asmx/update",
//                    contentType: "application/json; charset=utf-8",// tells the web service to serialize JSON
//                    //data: { testcode: 123 },
//                    type: "POST"
//                },
//                create: {
//                    url: ROOT_PATH+ "service/calendar/calendar_service.asmx/create",
//                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
//                    //data: { testcode: 123 },
//                    type: "POST"
//                },
//                destroy: {
//                    url: ROOT_PATH+ "service/calendar/calendar_service.asmx/destroy",
//                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
//                    //data: { testcode: 123 },
//                    type: "POST"
//                },
////                parameterMap: function(options, operation) {
////                    if (operation !== "read" && options.models) {
////                    options.models = $.extend(options.models, data);
////                        return {models: kendo.stringify(options.models)};
////                    }
////                }
//                parameterMap: function (data, operation) {
//                   // data = $.extend( data);
//                    return JSON.stringify(data);
//                }
//            },
//            schema: {
//                data: "d",
//                model: {
//                    id: "taskId",
//                    fields: {
//                        taskId: { from: "HolidayId", type: "number" },
//                        title: { from: "Title", defaultValue: "No title", validation: { required: true } },
//                        start: { type: "date", from: "Date" },
//                        end: { type: "date", defaultValue:"2014-04-08T19:30:00.000Z" ,from: "EndDate" },
//                        ends: { type: "date", defaultValue:"2014-04-08T19:30:00.000Z" , from: "End" },
//                          starts: { type: "date", from: "Start" , defaultValue:"2014-04-08T19:30:00.000Z"},
//                       startTimezone: { from: "StartTimezone" },
//                       endTimezone: { from: "EndTimezone"  },
//                       description: { from: "Description" },
//                       typedescription: { from: "TypeDescription" },
//                       holidaytype: { from: "HolidayType" },
//                       
//                    //   extensionData: { from: "ExtensionData",defaultValue:null },
//                       recurrenceId: { from: "RecurrenceID",defaultValue:0},
//                      recurrenceRule: { from: "RecurrenceRule" },
//                     recurrenceException: { from: "RecurrenceException" },
//                     ownerId: { from: "OwnerID", defaultValue: 1 },
//                        isAllDay: { type: "boolean", from: "IsAllDay" }

//                    }
//                }
//            }
//        },
//        resources: [
//            {
//                field: "ownerId",
//                title: "Owner",
//                dataSource: [
//                    { text: "Alex", value: 1, color: "#f8a398" },
//                    { text: "Bob", value: 2, color: "#51a0ed" },
//                    { text: "Charlie", value: 3, color: "#56ca85" },
//                     { text: "Charlie", value: 4, color: "#56ca83" }
//                ]
//            }
//            //,
////            {
////                field: "ownerId1",
////                title: "User",
////                dataSource: [
////                    { text: "Kav1", value: 1, color: "#f8a398" },
////                    { text: "Kav2", value: 2, color: "#51a0ed" },
////                    { text: "Kav3", value: 3, color: "#56ca85" },
////                     { text: "Kav4", value: 4, color: "#56ca83" }
////                ]
////            }
//        ],
//        editable:{
//            template:$("#editor").html()
//        }
//    });


//}

//}

function ReturnedCheques_onDataBound(arg) {

}

function CheckPaymentSettlement(control) {
    var isChecked = 0;
    if ($(control).attr("checked") == "checked") {
        isChecked = 1;
    }
}

$('#check_datarow').live('click', function (e) {
                
    var cb = $(this);
    var checked = cb.is(':checked');
    var selected_uid = cb[0].alt.replace(/\s+/g, '');

    setValueToGridData(checked, selected_uid);

    if ($('[id=checkAll]').is(':checked')) {

        $('[id=checkAll]')[0].checked = false;
    }
});

function setValueToGridData(val, rowuid) {
                
    var grid = $("#div_Cheques").data("kendoGrid");
    var item_grid = grid.dataSource.getByUid(rowuid);
    if (item_grid != undefined && item_grid != null)
        item_grid.set("HasSelect", val);
}

$("#checkAll").live('click', function (e) {
                
    var state = $(this).is(':checked');
    var grid = $('#div_Cheques').data('kendoGrid');
    $.each(grid.dataSource.view(), function () {
        if (this['HasSelect'] != state)
            this.dirty = true;
        this['HasSelect'] = state;
    });
    grid.refresh();
});

function loadCheques(createdDate, toDate, distributorAccountId, ChequeStatus) {
    var take_grid=$("#MainContent_hfPageIndex").val();
    var DATE_FORMAT = "{0: yyyy-MM-dd}"
    $("#div_loader").show();
    $("#div_Cheques").html("");
    $("#div_Cheques").kendoGrid({
        columns: [
        //{ title: "", headerTemplate: '<input type="checkbox" class="myCheckbox" onchange="CheckPaymentSettlementHeader(this)" />', width: "30px", template: '<input type="checkbox" onchange="CheckPaymentSettlement(,this)"' },
    { text: "HasSelect", title: "",  headerTemplate: '<input id="checkAll" type="checkbox" class="myCheckbox" />', width: "30px", template: "<input id=\"check_datarow\" alt=\"#= uid# \" type=\"checkbox\"   #= HasSelect ? checked='checked' : '' #  class=\"check_row\"/>" },
    { field: "PaymentSettlementId", width: "80px", title: "Payment Settlement Id", hidden: true },
    { field: "ChequeNumber", width: "250px", title: "Cheque Number", editable: true }, //template: editurl,
    { field: "OutletBank", width: "250px", title: "Outlet Bank", editable: true },
    { field: "Amount", width: "200px", title: "Amount", editable: true },
    { field: "Status", width: "150px", title: "Cheque Status", editable: false },
    { field: "ChequeDate", width: "150px", title: "Cheque Date", value: Date(), format: DATE_FORMAT, hidden: true },
    { field: "CreatedBy", width: "150px", title: "Rep", editable: false },
    { field: "CreatedDate", width: "150px", title: "CreatedDate", value: Date(), format: DATE_FORMAT, hidden: true },
    { field: "LastModifiedDate", width: "150px", title: "LastModifiedDate", value: Date(), format: DATE_FORMAT, hidden: true }],
        editable: true, // enable editing
        pageable: true,
        sortable: true,
        height:400,
        filterable: true,
        //toolbar: ["create"],
        dataBound: ReturnedCheques_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        ChequeNumber: { validation: { required: true} },
                        OutletBank: { validation: { required: true} },
                        Amount: { type: "number", validation: { required: true} },
                        Status: { editable: false  },
                        CreatedDate: { type: "date", validation: { required: true} },
                        CreatedBy: { editable: false },
                        ChequeDate: { type: "date", validation: { required: true} },
                        LastModifiedDate: { type: "date", validation: { required: true} },
                        HasSelect: { type: "boolean"}
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetChequesDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { createdDate: createdDate, toDate: toDate, distributorAccountId: distributorAccountId, selectedChequeStatus: ChequeStatus, pgindex:take_grid},
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#div_Cheques").data("kendoGrid");
                            if((take_grid!='') && (gridindex =='0')){
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex ='1';
                            }
                        }
//                        if (textStatus == "success") {
//                            $("#div_loader").hide();
//                        }
                    }
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

function AssignedRoutes_onDataBound(arg) {
//    
//    if ($('.myCheckbox').attr("checked") == "checked") {
//        $('.myCheckbox').prop('checked', false);
//    }

//    var lead_stage = $("#MainContent_HiddenFieldLeadType").val();
//    if (lead_stage.toLowerCase() != 'enduser') {
//        $('#div_row_count').html('<b># Recs : '+ $('#div_rowCount_max').html()+'</b>'); 
//        $('#div_rowCount_sum').html('Total Contacts : ' + $('#div_rowCount_max').html());
//    } else {
//        $('#div_row_count').html('<b># Recs : '+ $('#div_ViewRowCount_max').html()+'</b>'); 
//        $('#div_rowCount_sum').html('Total End Users : ' + $('#div_rowCount_max').html());
//    }
}

function UnassignRouteFromSalesRep(routeId, routeName, repCode, repName) {
    var confirmResult = confirm("Are you sure? Do you want to unassign " + routeName + " from sales rep " + repName);
    if (confirmResult == true) {
        var url = ROOT_PATH + "lead_customer/process_forms/processmaster.aspx?fm=UnassignedRoutefromRep&type=Delete" + "&id=" + routeId + "&repcode=" + repCode;
        
        //$("#" + tagertdiv).html('loading.....');
        $.ajax({
            url: url,
            dataType: "html",
            cache: false,
            success: function (msg) {
                if (msg == "true") {
//                    $("#MainContent_div_message").show();
//                    $("#MainContent_div_message").html("Successfully Deleted.");
                    alert("Successfully Deleted.");
                    //OpportunityDelete(indexOpportunity);
                    loadAssignedRoutes();
                }
            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg; // "Sorry there was an error: ";
                    $("#MainContent_div_message").show();
                    $("#MainContent_div_message").html(msg);
                }
            }
        });
    }
    else {
        //alert("Not Deleted");
    }
}

function loadAssignedRoutes() {
    var take_grid=$("#MainContent_hfPageIndex").val();
    var DATE_FORMAT = "{0: yyyy-MM-dd}"
    $("#div_loader").show();
    $("#div_assignedRoutes").html("");
    $("#div_assignedRoutes").kendoGrid({
        columns: [
    { field: "RouteMasterId", width: "80px", title: "Route ID", hidden: true },
    { field: "RepCode", width: "250px", title: "Rep Code", editable: false, hidden: true },
    { field: "RouteName", width: "250px", title: "Route Name", editable: false}, //template: editurl,
    { field: "RepName", width: "250px", title: "Rep Name", editable: false },
    { template: '<a href="javascript:UnassignRouteFromSalesRep(\'#=RouteMasterId#\',\'#=RouteName#\',\'#=RepCode#\',\'#=RepName#\')">Delete</a>', field: "", width: "120px" }],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        height: 400,
        filterable: true,
        dataBound: AssignedRoutes_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        RouteMasterId: { type: "number", editable: false },
                        RouteName: { editable: false },
                        RepCode: { editable: false, editable: false },
                        RepName: { editable: false }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/call_cycle/call_cycle_service.asmx/GetAssignedRoutes", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: {pgindex:take_grid},
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#div_assignedRoutes").data("kendoGrid");
                            if((take_grid!='') && (gridindex =='0')){
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex ='1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

/*
    ConvertMilsIntoSticks(2) => 2000 sticks.
*/
function ConvertMilsIntoSticks(mil){
    //1000 sticks == 1 mil
    var sticks=0;
    sticks=mil * 1000;
    return sticks;
}

function ConvertPacksIntoSticks(pack,sku){
    //1000 sticks == 1 mil
    var sticks=0;
    sticks=pack * sku;
    return sticks;
}

function ConvertStickIntoMiles(stick){
    //1 mil==1000 sticks
    var miles=0;
    miles=stick / 1000;
    return miles;
}


function DatedCheques_onDataBound(arg) {

}

function loadDatedCheques(createdDate, toDate) {
    var take_grid=$("#MainContent_hfPageIndex").val();
    var DATE_FORMAT = "{0: yyyy-MM-dd}"
    $("#div_loader").show();
    $("#div_DatedCheques").html("");
    $("#div_DatedCheques").kendoGrid({
        columns: [
    { field: "PaymentSettlementId", width: "80px", title: "Payment Settlement Id", hidden: true },
    { field: "ChequeNumber", width: "250px", title: "Cheque Number"}, //template: editurl,
    { field: "OutletBank", width: "250px", title: "Outlet Bank" },
    { field: "Amount", width: "200px", title: "Amount" },
    { field: "ChequeDate", width: "150px", title: "Cheque Date", value: Date(), format: DATE_FORMAT}],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        height:400,
        filterable: true,
        //toolbar: ["create"],
        dataBound: DatedCheques_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        ChequeNumber: { validation: { required: true} },
                        OutletBank: { validation: { required: true} },
                        Amount: { type: "number", validation: { required: true} },                        
                        ChequeDate: { type: "date", validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetDatedChequesDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { createdDate: createdDate, toDate: toDate, pgindex:take_grid},
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#div_DatedCheques").data("kendoGrid");
                            if((take_grid!='') && (gridindex =='0')){
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex ='1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

function loadMarketTargets(distId,selectedMonth)
{
    $("#div_loader").show();
    $("#MarketTargetsGrid").kendoGrid({
        height: 350,
        selectable: true,
        columns: [
                    { field: "TargetId", title: "Target ID", width:"100px", editable: false, hidden:true },
                    { field: "MarketId", title: "Market ID", width:"100px", editable: false, hidden:true },
                    { field: "MarketName", title: "Market", width:"220px", editable: false },
                    { field: "Target", title: "Target Qty.(Mil)" , width:"100px", format: NUMBER_FORMAT_THREE_DECIAML,editable: true  },
                    { field: "CreatedDate", width: "150px", title: "CreatedDate", value: Date(), format: DATE_FORMAT, hidden: true },
                    { field: "LastModifiedDate", width: "150px", title: "LastModifiedDate", value: Date(), format: DATE_FORMAT, hidden: true },
                    { field: "EffStartDate", width: "150px", title: "EffStartDate", value: Date(), format: DATE_FORMAT, hidden: true },
                    { field: "EffEndDate", width: "150px", title: "EffEndDate", value: Date(), format: DATE_FORMAT, hidden: true }
                ],
                
        editable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 200,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        TargetId: { type: "number", validation: { required: true} },
                        MarketId: { type: "number", validation: { required: true} },
                        MarketName: {editable: false, validation: { required: true} },
                        Target: { type: "number", validation: { required: true} },
                        CreatedDate: { type: "date", validation: { required: true} },
                        LastModifiedDate: { type: "date", validation: { required: true} },
                        EffStartDate: { type: "date", validation: { required: true} },
                        EffEndDate: { type: "date", validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetMarketTargetByDistributorId", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { DistributorId: distId , selectedMonth: selectedMonth },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            //loadRouteTargets(distId,startdate,enddate);
                            loadRouteTargets(distId,selectedMonth);
                            $("#div_loader").hide();
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }

        }
    });
}

function loadRouteTargets(distId,selectedMonth)
{
    $("#div_loader").show();
    $("#RouteTargetsGrid").kendoGrid({
        height: 350,
        selectable: true,
        columns: [
                    { field: "TargetId", title: "Target ID", width:"100px", editable: false, hidden:true },
                    { field: "RouteMasterId", title: "Route ID", width:"100px", editable: false, hidden:true },
                    { field: "RouteName", title: "Route", width:"220px", editable: false },
                    { field: "Target", title: "Target Qty.(Mil)", width:"100px" , format: NUMBER_FORMAT_THREE_DECIAML,editable: true  },
                    { field: "CreatedDate", width: "150px", title: "CreatedDate", value: Date(), format: DATE_FORMAT, hidden: true },
                    { field: "LastModifiedDate", width: "150px", title: "LastModifiedDate", value: Date(), format: DATE_FORMAT, hidden: true },
                    { field: "EffStartDate", width: "150px", title: "EffStartDate", value: Date(), format: DATE_FORMAT, hidden: true },
                    { field: "EffEndDate", width: "150px", title: "EffEndDate", value: Date(), format: DATE_FORMAT, hidden: true }
                ],
                
        editable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 200,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        TargetId: { type: "number", validation: { required: true} },
                        RouteMasterId: { type: "number", validation: { required: true} },
                        RouteName: {editable: false, validation: { required: true} },
                        Target: { type: "number", validation: { required: true} },
                        CreatedDate: { type: "date", validation: { required: true} },
                        LastModifiedDate: { type: "date", validation: { required: true} },
                        EffStartDate: { type: "date", validation: { required: true} },
                        EffEndDate: { type: "date", validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetRouteTargetByDistributorId", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { DistributorId: distId , selectedMonth: selectedMonth },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }

        }
    });
}

function GetErrorMessageDiv(msg, div) {
    var test = "<div id='messagered' class='message-red' rel='error'><table border='0' width='100%' cellpadding='0' cellspacing='0'>";
    test = test + "<tr><td class='red-left'>" + msg + "</td><td class='red-right'><a class='close-red' onclick=CloseMessageDiv('"+div+"')><img src='"
                + ROOT_PATH + "assets/images/icon_close_red.gif' alt='' /></a></td></tr></table></div>";
    return test;
}

function GetSuccesfullMessageDiv(msg, div) {
    var test = "<div id='messagegreen' class='message-green' rel='sucess'><table border='0' width='100%' cellpadding='0' cellspacing='0'>";
    test = test + "<tr><td class='green-left'>" + msg + "</td><td class='green-right'><a class='close-green' onclick=CloseMessageDiv('"+div+"')><img src='"
                + ROOT_PATH + "assets/images/icon_close_green.gif' alt='' /></a></td></tr></table></div>";
    return test;
}

function GetInformationMessageDiv(msg, div) {
    var test = "<div id='messageblue' class='message-blue' rel='error'><table border='0' width='100%' cellpadding='0' cellspacing='0'>";
    test = test + "<tr><td class='blue-left'>" + msg + "</td><td class='blue-right'><a class='close-blue' onclick=CloseMessageDiv('"+div+"')><img src='"
                + ROOT_PATH + "assets/images/icon_close_blue.PNG' alt='' /></a></td></tr></table></div>";
    return test;
}

function CloseMessageDiv(div_hide) {
            $('#'+div_hide).fadeOut("slow");
}

function divHide() {
            $('#MainContent_div_message').fadeOut("slow");
}

function loadCustomerTargets(routeMasterId) {
    var take_grid=$("#MainContent_hfPageIndex").val();
    var DATE_FORMAT = "{0: yyyy-MM-dd}"
    $("#div_loader").show();
    $("#div_customerTargets").html("");
    $("#div_customerTargets").kendoGrid({
        columns: [
            { field: "TargetId", title: "Target ID", width:"100px", editable: false, hidden:true },
            { field: "CustomerCode", title: "Cust Code", width:"100px", editable: false, hidden:true },
            { field: "Name", title: "Customer", width:"220px", editable: false },
            { field: "StickTarget", title: "Target Qty.(Mil)", width:"100px" , format: NUMBER_FORMAT_THREE_DECIAML,editable: true  },
            { field: "CreatedDate", width: "150px", title: "CreatedDate", value: Date(), format: DATE_FORMAT, hidden: true },
            { field: "LastCalledDate", width: "150px", title: "LastCalledDate", value: Date(), format: DATE_FORMAT, hidden: true },
            { field: "ModifiedDate", width: "150px", title: "ModifiedDate", value: Date(), format: DATE_FORMAT, hidden: true },
            { field: "StartDate", width: "150px", title: "StartDate", value: Date(), format: DATE_FORMAT, hidden: true },
            { field: "EffStartDate", width: "150px", title: "EffStartDate", value: Date(), format: DATE_FORMAT, hidden: true },
            { field: "EffEndDate", width: "150px", title: "EffEndDate", value: Date(), format: DATE_FORMAT, hidden: true }],
        editable: true, // enable editing
        pageable: true,
        sortable: true,
        height:400,
        filterable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 500,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        TargetId: { type: "number", validation: { required: true} },
                        CustomerCode: {validation: { required: true} },
                        Name: {editable: false, validation: { required: true} },
                        StickTarget: { type: "number", validation: { required: true} },
                        CreatedDate: { type: "date", validation: { required: true} },
                        LastCalledDate: { type: "date", validation: { required: true} },
                        ModifiedDate: { type: "date", validation: { required: true} },
                        StartDate: { type: "date", validation: { required: true} },
                        EffStartDate: { type: "date", validation: { required: true} },
                        EffEndDate: { type: "date", validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetCustomerTargetsDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { routeMasterId: routeMasterId, pgindex:take_grid},
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#div_customerTargets").data("kendoGrid");
                            if((take_grid!='') && (gridindex =='0')){
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex ='1';
                            }
                        }
//                        if (textStatus == "success") {
//                            $("#div_loader").hide();
//                        }
                    }
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

function OpenMarket(MarketId, MarketName) {
    $('#MainContent_txtMarketName').val(MarketName);
    $("#MainContent_HiddenFieldMarketID").val(MarketId);
    $("#MainContent_HiddenFieldMarketName").val(MarketName);

    $("#div_gridTitleMarketObjectives").css('display', 'block');
    $("#div_marketObjectives").css('display', 'block');
    loadMarketObjectives($('#MainContent_HiddenFieldMarketID').val());

    var wnd = $("#markets_window").data("kendoWindow");
    wnd.close();
}

function GetAllMarketsList() {
    //Enable buttons.
    $("#MainContent_buttonbar_buttinSave").removeClass('dissavebtn').addClass('savebtn');
    $("#MainContent_buttonbar_buttonAddCallCycle").removeClass('discall_cycle_activities').addClass('call_cycle_activities');
    $("#MainContent_buttonbar_buttonAddleadsCustomer").removeClass('disadd_leads_customer').addClass('add_leads_customer');
    $("#MainContent_buttonbar_buttonSaveNewTemplate").removeClass('dissave_new_template').addClass('save_new_template');

    $("#MainContent_buttonbar_buttinSave").prop('disabled', false);
    $("#MainContent_buttonbar_buttonAddCallCycle").prop('disabled', false);
    $("#MainContent_buttonbar_buttonAddleadsCustomer").prop('disabled', false);
    $("#MainContent_buttonbar_buttonSaveNewTemplate").prop('disabled', false);

    $("#MainContent_buttonbar_buttonSaveNewTemplate").live('click', function () {
        return showtemplatesavedialog();
    });
    $("#MainContent_buttonbar_buttonAddCallCycle").live('click', function () {
        return showcallcycledialog();
    });
    $("#div_loader").show();

    $("#markets_subwindow").html("");
    $("#markets_subwindow").kendoGrid({
        height: 353,
        columns: [
            { field: "MarketId", title: "Market ID", width: "100px", editable: false, hidden: true },
            { template: '<a href="javascript:OpenMarket(\'#=MarketId#\',\'#=MarketName#\')">#=MarketName#</a>',
                field: "MarketName", title: "Market", width: "150px", editable: false
            }
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "MarketId",
                    fields: {
                        MarketId: { validation: { required: true} },
                        MarketName: { validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetAllMarket", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { type: "", param: "" },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#markets_window").data("kendoWindow").open();
                            $("#div_loader").hide();
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function updateMarketObjective(ObjectiveId, Code, Description) {
    $("#hdnSelectedObjectiveId").val(ObjectiveId);
    $("#txtCode").val(Code);
    $("#txtDescription").val(Description);

    $("#window").css("display", "block");
    var wnd = $("#window").kendoWindow({
        title: "Add/Edit Objective",
        modal: true,
        visible: false,
        resizable: false
    }).data("kendoWindow");

    wnd.center().open();
}

function loadMarketObjectives(marketId) {
    var DATE_FORMAT = "{0: yyyy-MM-dd}";
    var take_grid = $("#MainContent_hfPageIndex").val();
    var url = "javascript:updateMarketObjective('#=ObjectiveId#','#=Code#','#=Description#');"
    $("#div_loader").show();
    $("#div_marketObjectives").html("");
    $("#div_marketObjectives").kendoGrid({
        height: 375,
        columns: [
            { field: "ObjectiveId", title: "Objective ID", width: "85px", hidden: true },
            { template: "<a href=\"" + url + "\">#=Code#</a>", field: "Code", title: "Code", width: "100px" },
            { template: "<a href=\"" + url + "\">#=Description#</a>", field: "Description", title: "Objective", width: "300px" },
            { field: "CreatedDate", width: "150px", title: "CreatedDate", value: Date(), format: DATE_FORMAT, hidden: true },
            { field: "LastModifiedDate", width: "150px", title: "LastModifiedDate", value: Date(), format: DATE_FORMAT, hidden: true },
        ],
        editable: false,
        pageable: true,
        sortable: true,
        filterable: true,
        //toolbar: ["create"],
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "ObjectiveId",
                    fields: {
                        ObjectiveId: { type: "number", validation: { required: true} },
                        Code: { validation: { required: true} },
                        Description: { validation: { required: true} },
                        CreatedDate: { type: "date", validation: { required: true} },
                        LastModifiedDate: { type: "date", validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetAllMarketObjectives", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: {marketId: marketId, pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#div_marketObjectives").data("kendoGrid");
                            if((take_grid!='') && (gridindex =='0')){
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex ='1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

function loadLeadCustomersForRep(repCode) {

            $("#loadLeadCustomerGrid").html("");
            $("#loadLeadCustomerGrid").kendoGrid({
                height: 300,
                columns: [//DeleteCallCycle
                {template: '<a href="javascript:DeleteCallCycle(#=Contact.SourceId#,\'#=Contact.CustomerCode#\',\'#=Contact.EndUserCode#\',#=CreateActivity#)">Delete</a>',
                         width: "50px"
                },
                { field: "Contact.SourceId", title: "LeadID", hidden: true },
                { field: "Contact.CustomerCode", title: "Customer Code", hidden: true },
                { field: "Contact.EndUserCode", title: "EndUser Code", hidden: true },
                { field: "Contact.Name", title: "Name", width: "130px" },
                { field: "LeadStage.StageName", title: "Lead Stage", width: "130px", hidden: true },
                {field: "Contact.Address", title: "Address", width: "70px" },
                { field: "Contact.City", title: "City", width: "70px" },
                { field: "Contact.State", title: "State", width: "70px" },
                { field: "Contact.PostalCode", title: "Postal Code", width: "70px" }
            ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                //filterable: true,
                selectable: "single",
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 1000,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            id: "SourceId",
                            fields: {
                                CallCycleID: { validation: { required: true} },
                                Comments: { validation: { required: true} },
                                Description: { validation: { required: true} },
                                DueOn: { type: "Date", validation: { required: true} }
                            }
                        }
                    },
                    transport: {

                        read: {
                            url: ROOT_PATH + "service/call_cycle/call_cycle_service.asmx/GetCallCycleContactsForRep", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { repCode: repCode},
                            type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }
                }
            });
}






function loadProgram() {
    $("#program_subwindow").html("");
    $("#program_subwindow").kendoGrid({
        height: 300,
        columns: [

                    
        {field: "ProgramId", title: "ProgramId",  hidden: true },
        //{ field: "TireList[0]", title: "Comments", hidden: false },
        //{ field: "Description", title: "Call Cycle", width: "130px" },
        {template: '<a href="javascript:OpenSelectProgram(#=ProgramId#,\'#=ProgramName#\',\'#=Status#\',\'#=Volume#\')">#=ProgramName#</a>',
            field: "Program Name", width: "130px"
        },
        
        //{ field: "CreatedDateString", title: "CreatedDateString", width: "70px", hidden: true },
        { field: "CreatedBy", title: "CreatedBy", width: "70px", hidden: true }
                    
    ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        //filterable: true,
        selectable: "single",
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "SourceId",
                    fields: {
                        ProgramId: { validation: { required: true} },
                        ProgramName: { validation: { required: true} },
                        Status: { validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetAllPrograms", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#program_window").data("kendoWindow").open();
                            $("div#div_loader").hide();
                        }
                        }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}


function OpenSelectProgram(ProgramId, ProgramName, Status,Volume) {

    var res = Volume.split(",");
    for(var i = 0;i < res.length; i++){
     //   alert(res[i]);
        if(res[i] == "5"){
            $("#MainContent_check_volume5").attr("checked","checked");
        }else if(res[i] == "6"){
            $("#MainContent_check_volume6").attr("checked","checked");
        }else if(res[i] == "7"){
            $("#MainContent_check_volume7").attr("checked","checked");
        }else if(res[i] == "8"){
            $("#MainContent_check_volume8").attr("checked","checked");
        }else if(res[i] == "9"){
            $("#MainContent_check_volume9").attr("checked","checked");
        }
        
    }

    //if(v=='v'){
       // $("#MainContent_RadioButtonVolume").attr("checked","checked");
        $("#MainContent_RadioButtonRandom").attr("checked","checked");
    //}

    //k $("#MainContent_HiddenFieldProgramID").val(ProgramId);
    //k loadCustomertoProgram(1, 0, 'F',ProgramId);

    //k $("#MainContent_txtProgramName").val(ProgramName);

    
        var url = ROOT_PATH + "programs/transaction/program_entry.aspx?programid="+ProgramId;
        window.location=url;
    //$("#MainContent_HiddenField_DueDate").val(DueOn);
    //$("#MainContent_tbCreated").html("Created By: " + CreatedBy + ", " + CreatedDate);

    //$('#MainContent_txtDRName').val(RepName);             
}


function loadCustomertoProgram(activeInactiveChecked, reqSentIsChecked, repType,ProgramId) {
    //debugger;
    $("#loadLeadCustomerGrid").html("");
    $("#loadLeadCustomerGrid").kendoGrid({
        // height: 400,
        columns: [
        { title: "", headerTemplate: '<input type="checkbox" class="myCheckbox" onchange="CheckLeadCustomeHeader(this)" />', width: "30px", template: '<input type="checkbox" onchange="CheckCustomerProgram(\'#=CustomerCode#\',this)" #=SelectedToCall#/>' },
//        { template: '<a href="javascript:AddLeadCustomerGrid(\'#=LeadStage#\',\'#=CustomerCode#\',\'#=SourceId#\')">#=Name#</a>',
//            field: "Name", width: "250px", footerTemplate: "<div id=\"div_rowCount_sum\" ></div>"
//        },
        { field: "Name", title: "Name", width: "250px" },
       // { field: "LeadStage", title: "Lead Stage", width: "85px" },
        { field: "CustomerCode", title: "Cust Code", width: "105px", hidden: true },
        { field: "SourceId", title: "CRM Ref. No.", width: "80px", hidden: true },
        { field: "CustomerCode", title: "Cust Code", width: "105px" },
            //{ field: "State", title: "State", width: "60px", hidden: false },
            { field: "City", title: "City", width: "130px", hidden: false },
            { field: "SourceId", title: "CRM Ref. No.", width: "98px", hidden: true },
            { field: "Address", title: "Address", width: "250px", template: '<div style="height: 20px; ">#=Address#</div>' }
    ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        //columnMenu: true,
        //dataBound: LeadCustomerGrid_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "SourceId",
                    fields: {
                        Name: { validation: { required: true} },
                        LeadStage: { validation: { required: true} },
                        CustomerCode: { editable: true, nullable: true },
                        State: { validation: { required: true} },
                        City: { validation: { required: true} },
                        SourceId: { validation: { required: true} },
                        Address: { validation: { required: true} },
                        rowCount: { type: "number", validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/lead_customer/lead_contacts_Service.asmx/GetAllProgramCustomer", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer',pgindex:0 ,ProgramId:ProgramId},
                    type: "POST" ,//use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            //$("#new_window").data("kendoWindow").open();
                            $("div#div_loader").hide();
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}


function CheckCustomerProgram(CustomerCode, control) {
    var isChecked=0;
    if ($(control).attr("checked") == "checked") {
        isChecked=1;
    }

    var root = ROOT_PATH + "programs/process_forms/processmaster.aspx?fm=checkCustomerProgram&type=query&CustomerCode=" + encodeurl(CustomerCode) + "&isChecked=" + isChecked;

    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
//            if (msg == "true") {
//                $("#MainContent_HiddenFieldCallCycleCount").val("1");
//            }
//            else {
//                $("#MainContent_HiddenFieldCallCycleCount").val("0");
//            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });

}

function CheckVolumeProgram(control) {

        var isChecked=0;
    if ($(control).attr("checked") == "checked") {
        isChecked=1;
    }

    var root = ROOT_PATH + "programs/process_forms/processmaster.aspx?fm=checkvolumeprogram&type=query&volume=" + control.value + "&isChecked=" + isChecked;

    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
        },
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
//    alert(control.value);
}


function LoadSelectedProgramTargetDetails(programTargetId, programId) {
    $('#MainContent_txtProgramTarget').val('');
    $('#MainContent_txtTargetProductPacks').val('');
    $('#MainContent_HiddenFieldTargetProductID').val('');
    $('#MainContent_txtTargetProductName').val('');
    $('#MainContent_HiddenFieldTargetProductSKU').val('');
    $('#MainContent_txtTargetProductPacks').val('');

    $('#MainContent_txtBonusTarget').val('');
    $('#MainContent_txtBonusProductPacks').val('');
    $('#MainContent_HiddenFieldBonusProductID').val('');
    $('#MainContent_txtBonusProductName').val('');
    $('#MainContent_HiddenFieldBonusProductSKU').val('');
    $('#MainContent_txtBonusProductPacks').val('');

    if(programTargetId == 0){
        $('#MainContent_txtProgramName').attr("readonly", false);
    }
    else{
        $('#MainContent_txtProgramName').attr("readonly", true);
    }

    var param;
    var urlString;
    if(programTargetId == 0 && programId > 0)
    {        
        param = { "programId": programId };
        urlString = ROOT_PATH + "service/lead_customer/common.asmx/LoadSelectedProgramTargetDetailsByProgramId";
    }
    else
    {
        param = { "programTargetId": programTargetId };
        urlString = ROOT_PATH + "service/lead_customer/common.asmx/LoadSelectedProgramTargetDetails";
    }

    $.ajax({
        type: "POST",
        //url: ROOT_PATH + "service/lead_customer/common.asmx/LoadSelectedProgramTargetDetails",
        url: urlString,
        data: JSON.stringify(param),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.hasOwnProperty('d')) {
                msg = response.d;
            } else {
                msg = response;
            }
            var json = JSON.parse(msg);

            var programJson = (json["Program"]);
            var programName = programJson["ProgramName"];
            var programId = programJson["ProgramId"];

            var programTargetId = json["ProgramTargetId"];
            var targetPercentage = json["TargetPercentage"];
            var targetMilles = json["TargetMilles"];
            var targetIncentiveAmount = json["TargetIncentiveAmount"];
            var targetIncentivePreIssueProductId = json["TargetIncentivePreIssueProductId"];
            var targetIncentivePreIssueProductName = json["TargetIncentivePreIssueProductName"];
            var targetIncentivePreIssueProductSku = json["TargetIncentivePreIssueProductSku"];
            var targetIncentivePreIssueAmountPacks = json["TargetIncentivePreIssueAmountPacks"];
            var bonusPercentage = json["BonusPercentage"];
            var bonusMilles = json["BonusMilles"];
            var bonusIncentiveAmount = json["BonusIncentiveAmount"];
            var bonusIncentivePreIssueProductId = json["BonusIncentivePreIssueProductId"];
            var bonusIncentivePreIssueProductName = json["BonusIncentivePreIssueProductName"];
            var bonusIncentivePreIssueProductSku = json["BonusIncentivePreIssueProductSku"];
            var bonusIncentivePreIssueAmountPacks = json["BonusIncentivePreIssueAmountPacks"];
            var effStartDate = json["EffStartDateString"];
            var effEndDate = json["EffEndDateString"];

            $('#MainContent_HiddenFieldSelectedProgramTargetID').val(programTargetId);
            $('#MainContent_HiddenFieldSelectedProgramID').val(programId);
            $('#MainContent_HiddenFieldSelectedProgramName').val(programName);
            $('#MainContent_txtProgramName').val(programName);
            //debugger;
            //Target Values
            if (targetPercentage != null && targetPercentage > 0) {
                $('#MainContent_rdbTargetPercentage').prop('checked', true);
                $('#MainContent_txtProgramTarget').val(targetPercentage);
            }
            if (targetMilles != null && targetMilles > 0) {
                $('#MainContent_rdbTargetMilles').prop('checked', true);
                $('#MainContent_txtProgramTarget').val(targetMilles);
            }
            if (targetIncentiveAmount != null && targetIncentiveAmount > 0) {
                $('#MainContent_rdbTargetDiscount').prop('checked', true);
                $("#MainContent_divTargetProduct").css("display", "none");
                $('#MainContent_txtTargetProductPacks').val(targetIncentiveAmount);
                $("#MainContent_lblTargetAmount").html("( LKR )");
            }
            if (targetIncentivePreIssueProductId != null && targetIncentivePreIssueProductId > 0) {
                $('#MainContent_rdbTargetPreIssue').prop('checked', true);
                $("#MainContent_divTargetProduct").css("display", "block");
                $('#MainContent_HiddenFieldTargetProductID').val(targetIncentivePreIssueProductId);
                $('#MainContent_txtTargetProductName').val(targetIncentivePreIssueProductName);
                $('#MainContent_HiddenFieldTargetProductSKU').val(targetIncentivePreIssueProductSku);
                $('#MainContent_txtTargetProductPacks').val(targetIncentivePreIssueAmountPacks);
                $("#MainContent_lblTargetAmount").html("( Packs )");
            }

            //Bonus Target Values
            if (bonusPercentage != null && bonusPercentage > 0) {
                $('#MainContent_rdbBonusPercentage').prop('checked', true);
                $('#MainContent_txtBonusTarget').val(bonusPercentage);
            }
            if (bonusMilles != null && bonusMilles > 0) {
                $('#MainContent_rdbBonusMilles').prop('checked', true);
                $('#MainContent_txtBonusTarget').val(bonusMilles);
            }
            if (bonusIncentiveAmount != null && bonusIncentiveAmount > 0) {
                $('#MainContent_rdbBonusDiscount').prop('checked', true);
                $("#MainContent_divBonusProduct").css("display", "none");
                $('#MainContent_txtBonusProductPacks').val(bonusIncentiveAmount);
                $("#MainContent_lblBonusAmount").html("( LKR )");
            }
            if (bonusIncentivePreIssueProductId != null && bonusIncentivePreIssueProductId > 0) {
                $('#MainContent_rdbBonusPreIssue').prop('checked', true);
                $("#MainContent_divBonusProduct").css("display", "block");
                $('#MainContent_HiddenFieldBonusProductID').val(bonusIncentivePreIssueProductId);
                $('#MainContent_txtBonusProductName').val(bonusIncentivePreIssueProductName);
                $('#MainContent_HiddenFieldBonusProductSKU').val(bonusIncentivePreIssueProductSku);
                $('#MainContent_txtBonusProductPacks').val(bonusIncentivePreIssueAmountPacks);
                $("#MainContent_lblBonusAmount").html("( Packs )");
            }

            //Date Ranges
            if (effStartDate != null) {
                $('#dtpDate').val(effStartDate);
            }
            if (effEndDate != null) {
                $('#dtpDateTo').val(effEndDate);
            }
        },
        error: function (response) {

        }
    });

}

function LoadProgramTargetsGrid(programId, fromDate, toDate) {
    var take_grid = $("#MainContent_hfPageIndex").val();
    var url = ROOT_PATH + "programs/master/program_target_entry.aspx?ptid=#=ProgramTargetId#";
    //var url = "javascript:updateMarketObjective('#=ObjectiveId#','#=Code#','#=Description#');"
    $("#div_loader").show();
    $("#div_ProgramTargetsGrid").html("");
    $("#div_ProgramTargetsGrid").kendoGrid({
        height: 375,
        columns: [
    { field: "ProgramTargetId", title: "Program Target ID", width: "85px", hidden: true },
    { field: "Program.ProgramId", title: "Program ID", width: "85px", hidden: true },
    { template: "<a href=\"" + url + "\">#=Program.ProgramName#</a>", field: "Program.Name", title: "Program", width: "85px", hidden: false },
    { field: "EffStartDate", width: "150px", title: "Effective Start Date", value: Date(), format: DATE_FORMAT, hidden: false },
    { field: "EffEndDate", width: "150px", title: "Effective End Date", value: Date(), format: DATE_FORMAT, hidden: false }
    ],
        editable: false,
        pageable: true,
        sortable: true,
        filterable: true,
        //toolbar: ["create"],
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "ObjectiveId",
                    fields: {
                        ProgramTargetId: { type: "number", validation: { required: true} },
                        //ProgramId: { type: "number", validation: { required: true} },
                        //Name: { validation: { required: true} },
                        EffStartDate: { type: "date", validation: { required: true} },
                        EffEndDate: { type: "date", validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetProgramTargetsDateAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { programId: programId, fromDate: fromDate, toDate: toDate,  pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#div_ProgramTargetsGrid").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

/*Save Market in Market Entry*/
function MarketEntrySaveMarket() {
    if (($("#txtName").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }

    if ($("#txtName").val() != '') {
        var marketName = $("#txtName").val();
        var selectedMarketId = $("#hdnSelectedMarketId").val();

        var url = ROOT_PATH + "sales_info/process_forms/processmaster.aspx?fm=marketentry&type=insert&selectedMarketId=" + selectedMarketId + "&marketName=" + marketName;

        $.ajax({
            url: url,
            dataType: "html",
            cache: false,
            success: function (msg) {
                if (msg == "true") {
                    var sucessMsg = GetSuccesfullMessageDiv("Market was successfully Saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    closepopup();
                    loadMarketGrid('1', "MarketGrid");
                    hideStatusDiv("MainContent_div_message");

                }
                else if (msg == "exists") {
                    var existsMsg = GetErrorMessageDiv("Market already exists !.", "MainContent_div_message_popup");

                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(existsMsg);
                    //closepopup();
                    hideStatusDiv("MainContent_div_message_popup");
                }
                else {
                    var errorMsg = GetErrorMessageDiv("Error occurred !.", "MainContent_div_message");

                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(errorMsg);
                    closepopup();
                    hideStatusDiv("MainContent_div_message");
                }

            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}

/*Load Market Grid in Market Entry Page*/
function loadMarketGrid(marketid, targetdiv) {
    var url = "javascript:updateMarket('#=MarketId#','#=MarketName#');"

    url = "<a href=\"" + url + "\">#=MarketName#</a>";

    $("#" + targetdiv).html("");
    $("#" + targetdiv).kendoGrid({
        height: 375,
        columns: [
    { field: "MarketId", title: "Market Id", width: "85px", hidden: true },
    { template: url, field: "MarketName", title: "Market Name", width: "250px" },
    { field: "Status", title: "Status", width: "105px", hidden: true }
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        MarketId: { validation: { required: true} },
                        MarketName: { editable: true, nullable: true },
                        Status: { validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetAllMarket", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { type: "", param: "" },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

/*Update Market in Market Entry Page*/
function updateMarket(id, name) {
    $("#hdnSelectedMarketId").val(id);
    $("#txtName").val(name);

    $("#window").css("display", "block");
    var wnd = $("#window").kendoWindow({
        title: "Add/Edit Market",
        modal: true,
        visible: false,
        resizable: false
    }).data("kendoWindow");
    $("#txtName").focus();
    wnd.center().open();
}

/*Load TME Lookup in Market Assign Page*/
function get_tmes(targeturl) {
    $("#div_loader").show();
    var url = ROOT_PATH + targeturl;
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            $("#div_loader").hide();
            if (msg != "") {
                var wnd = $("#modalWindowOriginators").kendoWindow({
                    title: "ASM List",
                    modal: true,
                    visible: false,
                    resizable: false,
                    width: 300
                }).data("kendoWindow");
                $("#modalWindowOriginators").html(msg);
                wnd.center().open();

                $("#jqi_state0_buttonOk").click(function () {
                    var originatorName = '';
                    var originatorId = '';
                    $('#originatorTable tr').each(function () {
                        if ($(this).children(':eq(0)').find('#a').is(':checked')) {

                            //var emailaddress = $(this).children(':eq(0)').find('#a').val();
                            //selected_email = selected_email + emailaddress;

                            originatorName = $(this).children(':eq(0)').find('#a').val();
                            originatorId = $(this).children(':eq(0)').find('#b').val();

                        }
                    });

                    $('#MainContent_txtSelectedOriginatorName').val(originatorName);
                    $('#HiddenFieldSelectedOriginatorId').val(originatorId);
                    load_originator_markets_grid(originatorId);
                    originator = originatorId;
                    wnd.close();
                    $("#div_address").show();
                    $("#MainContent_buttonbar_buttinSave").prop('disabled', false);
                    $("#MainContent_buttonbar_buttonDeactivate").prop('disabled', false);

                });
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
                $("#div_loader").hide();
                //$("#" + tagertdiv).html(msg);
            }
        }
    });
}



/*Load Assigned Markets Grid in Market Assign Page*/
function load_originator_markets_grid(originatorId) {
    $("#loadMarketAssignGrid").html("");
    $("#loadMarketAssignGrid").kendoGrid({
        height: 250,
        columns: [
        { template: '<a href="javascript:deleteMarket(\'#=MarketId#\',\'#=MarketName#\');">Delete</a>',
            field: "", width: "40px"
        },
        { field: "MarketId", title: "Market Id", width: "100px", hidden: true },
        { field: "MarketName", title: "Market Name", width: "100px" }
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    fields: {
                        MarketId: { validation: { required: true} },
                        MarketName: { validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetOriginatorMarkets", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { type: "ORIGINATOR_MARKETS", param: originatorId },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

/*Load Unassigned Markets List in Market Assign Page*/
function load_unassign_markets(targetdiv) {
    $("#" + targetdiv).html("");
    $("#" + targetdiv).kendoGrid({
        height: 380,
        columns: [
    { field: "MarketId", title: "Market Id", width: "85px", hidden: true },
    { template: '<a href="javascript:SetMarkets(\'#=MarketId#\',\'#=MarketName#\');">#=MarketName#</a>', field: "MarketName", title: "Market Name", width: "120px" }
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    fields: {
                        MarketId: { validation: { required: true} },
                        MarketName: { editable: true, nullable: true }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetAllMarket", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { type: "UNASSIGN_MARKETS", param: "" },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            var wnd = $("#modalWindowUnassignMarkets").kendoWindow({
                                title: "Unassign Markets",
                                modal: true,
                                visible: false,
                                resizable: false,
                                width: 550
                            }).data("kendoWindow");
                            wnd.center().open();
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}


/*Set markets in Market Assign Page*/
function SetMarkets(marketid, name) {
    $('#MainContent_HiddenFieldId').val(marketid);
    $('#MainContent_HiddenFieldName').val(name);
    $("#modalWindowUnassignMarkets").data("kendoWindow").close();
    var originatorId = originator;
    update_originator_markets('sales_info/process_forms/processmaster.aspx?fm=updatemarkets&type=update&marketid=' + marketid + '&marketname=' + name + '&originatorId=' + originatorId);
}

/*update markets in Market Assign Page*/
function update_originator_markets(targeturl) {
    $("#div_loader").show();
    var url = ROOT_PATH + targeturl;
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            $("#div_loader").hide();
            if (msg == "true") {
                load_originator_markets_grid('');
            }
            else {
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(GetErrorMessageDiv(msg, "MainContent_div_message"));
                hideStatusDiv("MainContent_div_message");
            }
        },
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
                $("#div_loader").hide();
            }
        }
    });
}

/*Delete market Click in Market Assign Page*/
function deleteMarket(marketid, marketname) {            
            $("#marketmodalWindow").show()
            showdeletemarketconfirmation(marketid, marketname);
}

/*Delete market Click in Market Assign Page*/
function deleteProductCategory(cat_id, cat_code) {            
            $("#productcategorymodalWindow").show()
            showdelete_productcategory_confirmation(cat_id, cat_code);
}

/*Delete market in Market Assign Page*/
function delete_selected_market(targeturl) {
    $("#div_loader").show();
    var url = ROOT_PATH + targeturl;
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            $("#div_loader").hide();
            if (msg == "true") {
                load_originator_markets_grid('');
            }
        },
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
                $("#div_loader").hide();
            }
        }
    });
}

/*Delete market in Product Category Assign Page*/
//function delete_selected_productcategory(targeturl) {
//    $("#div_loader").show();
//    var url = ROOT_PATH + targeturl;
//    $.ajax({
//        url: url,
//        dataType: "html",
//        cache: false,
//        success: function (msg) {
//            $("#div_loader").hide();
//            if (msg == "true") {
//                loadProductCategoryGrid();
//            }
//        },
//        error: function (msg, textStatus) {
//            if (textStatus == "error") {
//                var msg = msg;
//                $("#div_loader").hide();
//            }
//        }
//    });
//}

function delete_selected_productcategory(targeturl) {
    $("#div_loader").show();
    var url = ROOT_PATH + targeturl;
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            $("#div_loader").hide();
            if (msg == "true") {
                var sucessMsg = GetSuccesfullMessageDiv("Successfully Deleted.", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(sucessMsg);
                hideStatusDiv("MainContent_div_message");
                loadProductCategoryGrid();
            }
        },
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
                $("#div_loader").hide();
            }
        }
    });
}

/*Delete market in Product Category Assign Page*/
function delete_selected_invoice(targeturl) {
    $("#div_loader").show();
    var url = ROOT_PATH + targeturl;
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            $("#div_loader").hide();
            if (msg == "true") {
                var fromDate = $("#txtStartDate").val();
                var toDate = $("#txtEndDate").val();
                loadInvoice(fromDate, toDate);
            }
        },
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
                $("#div_loader").hide();
            }
        }
    });
}

/*Delete market in Product Category Assign Page*/
function delete_selected_customer(targeturl) {
    $("#div_loader").show();
    var url = ROOT_PATH + targeturl;
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            $("#div_loader").hide();
            if (msg == "true") {
                LoadReqSentLeadCustomerGrid();
            }
        },
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
                $("#div_loader").hide();
            }
        }
    });
}

/*Show Delete market Confirmation in Market Assign Page*/
function showdeletemarketconfirmation(marketid, marketname) {
        $("#save").css("display", "none");
        $("#no").css("display", "inline-block");
        $("#yes").css("display", "inline-block");

        var message = "Do you want to delete '" + marketname + "' market from the current ASM?";
                     
        var wnd = $("#marketmodalWindow").kendoWindow({
            title: "Delete Market",
            modal: true,
            visible: false,
            resizable: false,
            width: 400
        }).data("kendoWindow");

        $("#div_marketconfirm_message").text(message);
        wnd.center().open();

        $("#yes").click(function () {
            delete_selected_market('sales_info/process_forms/processmaster.aspx?fm=deletemarkets&type=delete&marketid=' + marketid);
            wnd.close();
        });

        $("#no").click(function () {
            wnd.close();
        });
}

/*Show Delete market Confirmation in Market Assign Page*/
function showdelete_productcategory_confirmation(cat_id, cat_code) {
        $("#save").css("display", "none");
        $("#no").css("display", "inline-block");
        $("#yes").css("display", "inline-block");

        var message = "Do you want to delete '" + cat_code + "' product category?";
                     
        var wnd = $("#productcategorymodalWindow").kendoWindow({
            title: "Delete Product Category",
            modal: true,
            visible: false,
            resizable: false,
            width: 400
        }).data("kendoWindow");

        $("#div_productcategoryconfirm_message").text(message);
        wnd.center().open();

//        $("#yes").click(function () {
//            //delete_selected_market('sales_info/process_forms/processmaster.aspx?fm=deletemarkets&type=delete&marketid=' + marketid);
//            delete_selected_productcategory('common_templates/process_forms/processmaster.aspx?fm=deleteproductcategory&type=delete&catid=' + cat_id);
//            wnd.close();
//        });

        $("#yes").click(function () {
             showPopup(cat_id,0);
            //delete_selected_market('sales_info/process_forms/processmaster.aspx?fm=deletemarkets&type=delete&marketid=' + marketid);
//            delete_selected_productcategory('common_templates/process_forms/processmaster.aspx?fm=deleteproductcategory&type=delete&catid=' + cat_id);
            wnd.close();
        });

        $("#no").click(function () {
            wnd.close();
        });
}

function MarketObjectiveSave(){
            if (($("#txtCode").val()).trim() == '') {
                var sucessMsg = GetErrorMessageDiv("Please Enter an Objective Code.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }

            if (($("#txtDescription").val()).trim() == '') {
                var sucessMsg = GetErrorMessageDiv("Please Enter a Description.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }

            if ($("#txtCode").val() != '' && $("#txtDescription").val() != '') {
                var code = $("#txtCode").val();
                var description = $("#txtDescription").val();
                var selectedObjectiveId = $("#hdnSelectedObjectiveId").val();
                var marketId = $("#MainContent_HiddenFieldMarketID").val();

                var url = ROOT_PATH + "lead_customer/process_forms/processmaster.aspx?fm=marketobjectives&type=insert&selectedObjectiveId=" + selectedObjectiveId
                                    + "&marketId=" + marketId
                                    + "&code=" + code
                                    +"&description=" + description;

                $.ajax({
                    url: url,
                    dataType: "html",
                    cache: false,
                    success: function (msg) {
                        //debugger;
                        if (msg == "true") {
                            var sucessMsg = GetSuccesfullMessageDiv("Objective was successfully Saved.", "MainContent_div_message");

                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(sucessMsg);

                            closepopup();
                            loadMarketObjectives($('#MainContent_HiddenFieldMarketID').val());
                            hideStatusDiv("MainContent_div_message");
                        }else if (msg == "codeExists") {
                            var errorMsg = GetErrorMessageDiv("Objective Code already exists.", "MainContent_div_message_popup");
                            $('#MainContent_div_message_popup').css('display', 'block');
                            $("#MainContent_div_message_popup").html(errorMsg);                            
                            hideStatusDiv("MainContent_div_message_popup");
                        }
                        else if (msg == "false") {
                            var errorMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message");

                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(errorMsg);
                            closepopup();
                            hideStatusDiv("MainContent_div_message");
                        }

                    },
                    error: function (msg, textStatus) {
                        if (textStatus == "error") {
                            var errorMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message");

                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(errorMsg);
                            closepopup();
                            hideStatusDiv("MainContent_div_message");
                        }
                    }
                });
            }
}

function AddMarketObjectives(){
                if($("#MainContent_HiddenFieldMarketID").val() != ''){
                $("#window").css("display", "block");
                $("#hdnSelectedObjectiveId").val('');

                $("#txtCode").val('');
                $("#txtDescription").val('');

                var wnd = $("#window").kendoWindow({
                    title: "Add/Edit Objective",
                    modal: true,
                    visible: false,
                    resizable: false
                }).data("kendoWindow");

                wnd.center().open();
                $("#txtCode").focus();
            }
            else {
                var infoMsg = GetInformationMessageDiv("Please select a Market", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(infoMsg);
                hideStatusDiv("MainContent_div_message");
            }
}

 
 function RouteMasterSave() {

                if (($("#txtName").val()).trim() == '') {
                var sucessMsg = GetErrorMessageDiv("Please Enter Name.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }
            
            if ($("#txtName").val() != '') {
                
                var routeName = $("#txtName").val();
                var selectedRouteId = $("#hdnSelectedRouteId").val();

                var url = ROOT_PATH + "call_cycle/process_forms/processmaster.aspx?fm=routeentry&type=insert&selectedRouteId=" + selectedRouteId + "&routeName=" + routeName;

                $.ajax({
                    url: url,
                    dataType: "html",
                    cache: false,
                    success: function (msg) {
                        if (msg == "true") {
                            var sucessMsg = GetSuccesfullMessageDiv("Route was successfully Saved.", "MainContent_div_message");

                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(sucessMsg);

                            closepopup();

                            if ($("#MainContent_HiddenFieldUserType").val() == "ASE") {
                                loadRouteMasterGridTme();
                                $("#MainContent_div_routes_rep").css("display", "none");
                                $("#MainContent_div_routes_tme").css("display", "block");
                            }
                            else {
                                loadRouteMasterGrid();
                                $("#MainContent_div_routes_tme").css("display", "none");
                                $("#MainContent_div_routes_rep").css("display", "block");                
                            }
                            hideStatusDiv("MainContent_div_message");
                        }
                        else if (msg == "exists") {
                            var existsMsg = GetErrorMessageDiv("Route already exists !.", "MainContent_div_message_popup");

                            $('#MainContent_div_message_popup').css('display', 'block');
                            $("#MainContent_div_message_popup").html(existsMsg);
                            //document.getElementById('MainContent_div_message').style.display = "block";
                            //closepopup();
                            hideStatusDiv("MainContent_div_message_popup");
                        }

                    },
                    // error: function (XMLHttpRequest, textStatus, errorThrown) {
                    error: function (msg, textStatus) {
                        if (textStatus == "error") {
                            var msg = msg;
                        }
                    }
                });

            }
         
}

function updateRouteMaster(id, name) {
            $("#hdnSelectedRouteId").val(id);
            $("#txtName").val(name);

            $("#window").css("display", "block");
            var wnd = $("#window").kendoWindow({
                title: "Add/Edit Route",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");

            wnd.center().open();
 }

 function AddRoute(){
                $("#window").css("display", "block");
            $("#hdnSelectedRouteId").val('');

            $("#txtName").val('');

            var wnd = $("#window").kendoWindow({
                title: "Add/Edit Route",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");

            wnd.center().open();
            $("#txtName").focus();
 }

 function getStartOfWeek(d) {
              var day = d.getDay(),
                 diff = d.getDate() - day + 1; // (day == 1 ? -6 : 1);
              return new Date(d.setDate(diff));
}

 function targetAllocationSave() {
                entityGrid1 = $("#DistributorGrid").data("kendoGrid");
            $("#MainContent_HiddenFieldDistributorTargets").val(JSON.stringify(entityGrid1.dataSource.view()));
            entityGrid2 = $("#MarketTargetsGrid").data("kendoGrid");
            $("#MainContent_HiddenFieldMarketTargets").val(JSON.stringify(entityGrid2.dataSource.view()));
            entityGrid3 = $("#RouteTargetsGrid").data("kendoGrid");
            $("#MainContent_HiddenFieldRouteTargets").val(JSON.stringify(entityGrid3.dataSource.view()));
 }

function showRouteTargetConfirmMg(routeName, routeTarget, calculatedRouteTarget) {
            //$("#save").css("display", "none");
            $("#no").css("display", "inline-block");
            $("#yes").css("display", "inline-block");

            var message = "Total Targets for Outlets ( " + calculatedRouteTarget + " ) is higher than route target ( " + routeTarget + " ). Do you want to overwrite the route target as " + calculatedRouteTarget + " ?";

            var wnd = $("#routeTargetmodalWindow").kendoWindow({
                title: "Route Target",
                modal: true,
                visible: false,
                resizable: false,
                width: 400
            }).data("kendoWindow");
            $("#MainContent_div_message").css("display", "none");
            $("#div_routeTargetConfirm_message").text(message);
            wnd.center().open();

            $("#yes").click(function () {
                $("#MainContent_HiddenFieldOverwriteClickOk").val('1');
                document.getElementById("MainContent_buttonbar_buttinSave").click();
                wnd.close();
                //$("#MainContent_div_message").css("display", "block");
            });

            $("#no").click(function () {
                $("#MainContent_HiddenFieldOverwriteClickOk").val('0');
                //var jsonarray = $("#MainContent_HiddenFieldCustomerTargets").val();                
                //rebindCustomerTargets(jsonarray);
                wnd.close();
                loadCustomerTargets($('#MainContent_HiddenFieldRouteMasterID').val());
                loadSelectedRouteTarget($('#MainContent_HiddenFieldRouteMasterID').val());
                //$("#MainContent_div_message").css("display", "block");
            });
        }


       function loadSelectedRouteTarget(routeMasterId) {
            var param = { "routeid": routeMasterId };
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/call_cycle/call_cycle_service.asmx/GetRouteTargetFromRouteId",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.hasOwnProperty('d')) {
                        msg = response.d;
                    } else {
                        msg = response;
                    }
                    //debugger;
                    var json = JSON.parse(msg);

                    var targetId = json["TargetId"];
                    var target = json["Target"];
                    $('#MainContent_lblRouteTarget').html(target);
                    $('#MainContent_HiddenFieldSelectedRouteTarget').val(target);
                    $('#MainContent_HiddenFieldSelectedRouteTargetId').val(targetId);
                    //alert(target + " - " + targetId);
                },
                error: function (response) {
                    //alert("Oops, something went horribly wrong");
                }
            });
        }

 function programTargetFilterClick() {
                        $("#div_mainProgramTargets").css("display", "block");
                var programId = $("#MainContent_HiddenFieldSelectedProgramID").val();
                var fromDate = $("#dtpDate").val();
                var toDate = $("#dtpDateTo").val();
                if ($("#MainContent_HiddenFieldSelectedProgramID").val() != '' && $("#MainContent_txtProgramName").val() != '') {
                    LoadProgramTargetsGrid(programId, fromDate, toDate);
                }
                else {
                    $("#div_mainProgramTargets").css("display", "block");
                    LoadProgramTargetsGrid(0, $("#dtpDate").val(), $("#dtpDateTo").val());
                    $('#MainContent_HiddenFieldSelectedProgramID').val('');
                    $('#MainContent_HiddenFieldSelectedProgramName').val('');
                }
 }

 function programTargetEnterSaveValidations(){
 jQuery("#MainContent_txtProgramName").validate({
                expression: "if (VAL) return true; else return false;",
                message: "Please Enter Program."
            });
            jQuery("#MainContent_txtProgramTarget").validate({
                expression: "if (VAL) return true; else return false;",
                message: "Please Enter a Program Target."
            });
            jQuery("#MainContent_txtBonusTarget").validate({
                expression: "if (VAL) return true; else return false;",
                message: "Please Enter a Bonus Target."
            });
            jQuery("#MainContent_txtTargetProductPacks").validate({
                expression: "if (VAL) return true; else return false;",
                message: "Please Enter a Value for Target Incentive."
            });
            jQuery("#MainContent_txtBonusProductPacks").validate({
                expression: "if (VAL) return true; else return false;",
                message: "Please Enter a Value for Bonus Incentive."
            });

            jQuery("#MainContent_txtTargetProductName").validate({
                expression: "if (isChecked('MainContent_rdbTargetPreIssue','target')) return true; else return false;",
                message: "Please Select a Product."
            });

            jQuery("#MainContent_txtBonusProductName").validate({
                expression: "if (isChecked('MainContent_rdbBonusPreIssue','bonus') ) return true; else return false;",
                message: "Please Select a Product."
            });
 }

function IsPasswordsMatch(id1, id2) {
    var ReturnVal = false;
    var password = $("#" + id1).val();
    var confirmPassword = $("#" + id2).val();

    // Check for equality with the password inputs
    if (password != confirmPassword) {
        ReturnVal = false;
    } else {
        ReturnVal = true;
    }
    return ReturnVal;
}


function distributorEntryValidations() {
    $('#MainContent_TxtPassword').prop('type', 'password');
    $('#MainContent_TxtVerifyPassword').prop('type', 'password');

    jQuery("#MainContent_txtName").validate({
        expression: "if (VAL) return true; else return false;",
        message: "Please Enter Name."
    });

    jQuery("#MainContent_TxtUserName").validate({
        expression: "if (VAL) return true; else return false;",
        message: "Please Enter User Name."
    });

    jQuery("#MainContent_TxtPassword").validate({
        expression: "if (VAL) return true; else return false;",
        message: "Please Enter Password."
    });

    jQuery("#MainContent_TxtVerifyPassword").validate({
        expression: "if (VAL) return true; else return false;",
        message: "Please Enter Password."
    });

    jQuery("#MainContent_TextBoxEmail").validate({
        expression: "if (isFill(SelfID))" +
                " {" +
	        "        if (VAL.match(/^[^\\W][a-zA-Z0-9\\_\\-\\.]+([a-zA-Z0-9\\_\\-\\.]+)*\\@[a-zA-Z0-9_]+(\\.[a-zA-Z0-9_]+)*\\.[a-zA-Z]{2,4}$/))" +
		    "            return true; " +
	        "        else " +
		        "           return false; " +
                "   } else {return true; }",
        message: "Please enter valid Email id."
    });

//                    jQuery("#MainContent_TxtPassword").validate({
//                        expression: "if (IsPasswordsMatch('MainContent_TxtPassword','MainContent_TxtVerifyPassword')) return true; else return false;",
//                        message: "Passwords do not Match"
//                    });

    jQuery("#MainContent_TxtVerifyPassword").validate({
        expression: "if (IsPasswordsMatch('MainContent_TxtPassword','MainContent_TxtVerifyPassword')) return true; else return false;",
        message: "Passwords do not Match"
    });

     jQuery("#MainContent_txtDistributor").validate({
        expression: "if (VAL) return true; else return false;",
        message: "Please select Distributor."
    });   
}

//Stock Allocation Button Unload Click
function StockAllocationButtomUnloadHandler() {
    var balanceQty = $("#hdnSelectedBalanceQty").val();
    var vehicleBalanceQty = $("#hdnSelectedVehicleBalanceQty").val();
    var unloadType = $("#MainContent_dropDownUnloadType").val();
    var unloadQty = $("#txtUnloadQty").val(); //unloading amount

    var selectedStockHeaderId = $("#hdnSelectedStockHeaderId").val();
    var selectedCatalogCode = $("#hdnSelectedCatalogCode").val();

    var hdnSelectedAllocQty = $("#hdnSelectedAllocQty").val();
    var hdnSelectedLoadQty = $("#hdnSelectedLoadQty").val();

    var hdnSelectedAllocDate = $("#hdnSelectedAllocDate").val();
    var SelectedTotalReturnQty = $("#hdnSelectedTotalReturnQty").val();
    var SelectedVehicleReturnQty = $("#hdnSelectedVehicleReturnQty").val();

    var unloadFrom = $("#MainContent_dropDownUnloadFrom option:selected").text();
    //debugger;

    if(parseFloat(unloadQty) <= 0 || unloadQty == '' || unloadQty == null)
    {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Proper Unload Quantity !", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }

    //not null validation
    jQuery("#txtUnloadQty").validate({
        expression: "if (VAL) return true; else return false;",
        message: "Please Enter Valid Amount to Unload."
    });
    // debugger;
    if ((parseFloat(unloadQty) <= parseFloat(balanceQty) && unloadFrom == "Allocated Balance") || (parseFloat(unloadQty) <= parseFloat(vehicleBalanceQty) && unloadFrom == "Vehicle Balance") || (unloadFrom == "Both")) {
        if ($("#txtUnloadQty").val() != '') {

            var url = ROOT_PATH + "loading_stocks/process_forms/processmaster.aspx?fm=loadstock&type=update&selectedStockHeaderId=" + selectedStockHeaderId
            + "&selectedCatalogCode=" + selectedCatalogCode
            + "&balanceQty=" + balanceQty
            + "&unloadQty=" + unloadQty
            + "&allocQty=" + hdnSelectedAllocQty
            + "&loadQty=" + hdnSelectedLoadQty
            + "&unloadType=" + unloadType
            + "&allocDate=" + hdnSelectedAllocDate
            + "&totalReturnQty=" + SelectedTotalReturnQty
            + "&vehicleBalanceQty=" + vehicleBalanceQty
            + "&SelectedVehicleReturnQty=" + SelectedVehicleReturnQty
            + "&unloadFrom=" + unloadFrom;

            $.ajax({
                url: url,
                dataType: "html",
                cache: false,
                success: function (msg, textStatus) {
                    var sucessMsg = GetSuccesfullMessageDiv("Successfully Unloaded.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    closepopupStockUnload();
                    hideStatusDiv("MainContent_div_message");
                    loadproductlist($("#MainContent_dtpCloseDate").val(), $("#MainContent_HiddenFieldAssignedRouteId").val());
                },
                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                    }
                }
            });
        }
    }
    else {
        var sucessMsg = GetInformationMessageDiv("Please Enter a Proper Unload Quantity !", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }

    $("#hdnSelectedStockHeaderId").val("");
    $("#hdnSelectedCatalogCode").val("");

    $("#hdnSelectedAllocQty").val("");
    $("#hdnSelectedLoadQty").val("");
}

//Stock Allocation Unload Form dropDown Changed Handler
function StockAllocationUnloadFromDropDownHandler(end) {
    if (end == "Allocated Balance") {
        //alert("Allocated");
        $("#MainContent_lblBalanceQty").html($("#hdnSelectedBalanceQty").val());
        $("#txtUnloadQty").val($("#hdnSelectedBalanceQty").val());
        $("#txtUnloadQty").prop('disabled', false);
    } else if (end == "Vehicle Balance") {
        //alert("Vehicle");
        $("#MainContent_lblBalanceQty").html($("#hdnSelectedVehicleBalanceQty").val());
        $("#txtUnloadQty").val($("#hdnSelectedVehicleBalanceQty").val());
        $("#txtUnloadQty").prop('disabled', false);
    } else if (end == "Both") {
        //alert("both");
        $("#MainContent_lblBalanceQty").html(parseFloat($("#hdnSelectedVehicleBalanceQty").val()) + parseFloat($("#hdnSelectedBalanceQty").val()));
        $("#txtUnloadQty").val(parseFloat($("#hdnSelectedVehicleBalanceQty").val()) + parseFloat($("#hdnSelectedBalanceQty").val()));
        $("#txtUnloadQty").prop('disabled', true);
    }

}



function ShowHideContent(clicktab) {
    //Remove all class
    $("#div_content > div").removeAttr("class");
    $("#div_content > div").attr("class", "divhidden");
 
    switch (clicktab) {
        case "1":
            //Messages
            $("#div_sub_content_reps").attr("class", "divshow");
           GetRepSalesDetails('rep','div_reps'); 
            break;
        case "2":
            //Opportunities           
            $("#div_sub_content_distributor").attr("class", "divshow");
            GetDistributorSalesDetails('distributor','div_distributor'); 
            break;
        case "3":
            //Appointments
            $("#div_sub_content_products").attr("class", "divshow");
            GetProductSalesDetails('product','div_products'); 
            break;
        case "4":
            //Reminders
            //$("#panelbarcontentreminders").kendoPanelBar({ expandMode: "single" });
            $("#div_sub_content_customers").attr("class", "divshow");
            GetCustomerSalesDetails('All','customer','div_customers');            
            break;
        case "5":
            //Outstanding Activities
            $("#div_sub_content_markets").attr("class", "divshow");
            GetMarketSalesDetails('market','div_markets'); 
            break;
    }
}

function GetRepSalesDetails(type,grid)  {
    $("#"+grid).html("");
    $("#"+grid).kendoGrid({
        //height: 400,
        columns: [
            { field: "AccountName", title: "Rep", width: "40px" },
            { field: "Amount", title: "Amount(Rs)", width: "15px" , attributes:{style:"text-align:right;"} ,format:NUMBER_FORMAT_DECIAML}
        ],
        editable: false, // enable editing
        pageable: false,
        sortable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "Originator",
                    fields: {
                        AccountName: { validation: { required: true} },
                        Amount: { validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetSalesDetails", //specify the URL which data should return the records. This is the Read method of the Products.svc service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: {  repcode: '',type:type },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for svc
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {

                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function GetDistributorSalesDetails(type,grid)  {//alert(grid);
    $("#"+grid).html("");
    $("#"+grid).kendoGrid({
        //height: 400,
        columns: [
            { field: "AccountName", title: "Distributor", width: "40px" },
            { field: "Amount", title: "Amount(Rs)", width: "15px" , attributes:{style:"text-align:right;"} ,format:NUMBER_FORMAT_DECIAML}
        ],
        editable: false, // enable editing
        pageable: false,
        sortable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "Originator",
                    fields: {
                        AccountName: { validation: { required: true} },
                        Amount: { validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetSalesDetails", //specify the URL which data should return the records. This is the Read method of the Products.svc service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: {  repcode: 'All',type:type },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for svc
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {

                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function GetProductSalesDetails(type,grid)  {
    $("#"+grid).html("");
    $("#"+grid).kendoGrid({
        //height: 400,
        columns: [
            { field: "AccountName", title: "Product", width: "40px" },
            { field: "Amount", title: "Amount(Rs)", width: "15px" , attributes:{style:"text-align:right;"} ,format:NUMBER_FORMAT_DECIAML}
        ],
        editable: false, // enable editing
        pageable: false,
        sortable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "Originator",
                    fields: {
                        AccountName: { validation: { required: true} },
                        Amount: { validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetSalesDetails", //specify the URL which data should return the records. This is the Read method of the Products.svc service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: {  repcode: '',type:type },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for svc
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {

                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}
function GetCustomerSalesDetails(cus_repcode,type,grid)  {
    $("#"+grid).html("");
    $("#"+grid).kendoGrid({
        //height: 400,
        columns: [
            { field: "AccountName", title: "Customer", width: "40px" },
            { field: "Amount", title: "Amount(Rs)", width: "15px" , attributes:{style:"text-align:right;"} ,format:NUMBER_FORMAT_DECIAML}
        ],
        editable: false, // enable editing
        pageable: false,
        sortable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "Originator",
                    fields: {
                        AccountName: { validation: { required: true} },
                        Amount: { validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetSalesDetails", //specify the URL which data should return the records. This is the Read method of the Products.svc service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: {  repcode: cus_repcode,type:type },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for svc
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {

                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}
function GetMarketSalesDetails(type,grid)  {
    $("#"+grid).html("");
    $("#"+grid).kendoGrid({
        //height: 400,
        columns: [
            { field: "AccountName", title: "Market", width: "40px" },
            { field: "Amount", title: "Amount(Rs)", width: "15px" , attributes:{style:"text-align:right;"} ,format:NUMBER_FORMAT_DECIAML}
        ],
        editable: false, // enable editing
        pageable: false,
        sortable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "Originator",
                    fields: {
                        AccountName: { validation: { required: true} },
                        Amount: { validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetSalesDetails", //specify the URL which data should return the records. This is the Read method of the Products.svc service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: {  repcode: '',type:type },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for svc
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {

                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}



function SetDailySalesInfo(detailType,targetSalesinfogrid,methodName,pgindex) {

    var entityGrid = $("#"+targetSalesinfogrid).data("kendoGrid");
    var selectedRows = entityGrid.dataItem(entityGrid.select());
    
    var code =  "";
    var desc =  "";
    var displayOption ="";

    displayOption = $("#MainContent_custsalesinfo_DropDownDisplayOption").val()
 
 //$("#MainContent_custsalesinfo_DropDownGroupBy").val(detailType);
//LoadDailyTitle(detailType, "n", "0", displayOption,"",targetSalesinfogrid,methodName);

    //LoadSalesInfoChart(displayOption, "cust");
        
//    if ((selectedRows == "" || selectedRows == undefined) && pgindex != 2) {
//        alert('Please select the Row');
//        return ;
//    }
//    else {
//        if(pgindex != 2){
//            code = encodeurl(selectedRows["Code"]);
//            desc = selectedRows["Name"];   
//        }else{
//            code = '';
//            desc = ''; 
//        }
//       var market = $("#MainContent_custsalesinfo_DropDownMarket").val();

        var isCmp = 0;
       if ($("#MainContent_custsalesinfo_custsalesinfo_RadioButtonCMP").attr("checked")) {
            isCmp = 1;
       }
        var root = ROOT_PATH + "business_sales/process_forms/processmaster.aspx?fm=DailySalesInfo&type=Set&isCmp=" + isCmp;

        $.ajax({
            url: root,
            dataType: "html",
            cache: false,
            success: function (msg) {
                if (msg == "true") {
                    LoadDailyTitle(detailType, "n", "0", displayOption,"",targetSalesinfogrid,methodName);
                }
                else {
                    //$("#MainContent_HiddenFieldCallCycleCount").val("0");
                }
            },
             
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
//    }
}


function LoadDailyTitle( detailType, sastFlag, month, type, sort,targetSalesinfogrid,methodName) {
    if(methodName=='GetBusinessSales'){
        $("#MainContent_custsalesinfo_HiddenFieldDetailType").val(detailType); 
    }else{
        $("#HiddenFieldDetailType").val(detailType); 
    }
    $.ajax({
        type: "POST",
//        url: ROOT_PATH + "business_sales/transaction/businesssales_enquiry.aspx/LoadTitle",
        url: ROOT_PATH + "business_sales/process_forms/processmaster.aspx/SetRepTargetInfo",
        data: "{detailTypeparam : '" + detailType + "',sastFlag:'" + sastFlag + "',monthparam:'" + month + "'}",
        cache: false,
        contentType: "application/json",
        dataType: "json",
        success: function (msg) {
            // Replace the div's content with the page method's return.
            //debugger;
            GridBusinessDailySales(targetSalesinfogrid, type, msg.d, detailType, sort,methodName);
        }
    });
}


function GridBusinessDailySales(targetSalesinfogrid,type, header, detailType, sort,methodName) {
    var user = "";
//    if(methodName=="GetBusinessSales"){
        user = "d";
        businessSalesuser = "";
//    }
//    else{
//        user = "1";
//        businessSalesuser = "1";
//    }
    var BusinessDailySales_onChange=  function (arg) {
        $("#" + targetSalesinfogrid).find('.eossales_selected').attr('class', 'eossales');
        $("#" + targetSalesinfogrid).find('.k-state-selected').find('.eossales').attr('class', 'eossales_selected');
            var selectedRows = this.dataItem(this.select());   
            if(methodName=="GetBusinessSales"){
                $("#MainContent_custsalesinfo_txtSelection").val(selectedRows["Code"]);
            }
            else{
                $("#txtSelection").val(selectedRows["MC"]);
            } 
        }

    //alert(header[0]);
    var listcol = [];
    var listaggregate = [];

        listcol = [

                    //{ field: "MC", title: "MC", hidden: true },
                    //{ template: '<div id="div_FlowContent'+user+'">#=FlowContent#</div>', field: "FlowContent", hidden: true},
                    { field: "Name",  sortable: true, filterable: true,
                        title: "DR",width:"220px", headerTemplate: header[4], footerTemplate: "<div id=\"div_RowCount_max1"+user+"\" ></div>", format: "{0:n0}", attributes: { class: "eossales"} },
                    { field: "TyTodayActual", template: "#=TyTodayActual#", sortable: false, filterable: false,
                        title: "TyTodayActual",width:"90px", headerTemplate: "<div style =\"text-align: right;\">" + "Today’s sales" + "</div>", footerTemplate: "<div style =\"text-align: right;\" id=\"div_A1_max1"+user+"\" ></div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
                    { field: "TyTtmtdActual", template: '#=TyTtmtdActual#', sortable: false, filterable: false,
                        title: "TyTtmtdActual",width:"90px", headerTemplate: "<div style =\"text-align: right;\">" + "This Month TD" + "</div>", footerTemplate: "<div style =\"text-align: right;\" id=\"div_A2_max1"+user+"\" ></div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
                    { field: "TyLmtdActual", template: '#=TyLmtdActual# ', sortable: false, filterable: false,
                        title: "TyLmtdActual",width:"90px", headerTemplate: "<div style =\"text-align: right;\">" + "Last Year MTD " + "</div>", footerTemplate: "<div style =\"text-align: right;\" id=\"div_A3_max1"+user+"\" ></div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
                    { field: "LyTmtdActual", template: '#=LyTmtdActual#', sortable: false, filterable: false,
                        title: "MTD",width:"90px", headerTemplate: "<div style =\"text-align: right;\">" + "This Year LMTD" + "</div>", footerTemplate: "<div style =\"text-align: right;\" id=\"div_AT_max1"+user+"\" ></div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
                    
                    
                    { field: "TyTodayActualTot", title: "TyTodayActualTot", hidden: true, footerTemplate: "<div id=\"div_T1_tot_max1"+user+"\" >#: max #</div>", format: "{0:n2}" },
                    { field: "TyTtmtdActualTot", title: "TyTtmtdActualTot", hidden: true, footerTemplate: "<div id=\"div_T2_tot_max1"+user+"\" >#: max #</div>", format: "{0:n2}" },
                    { field: "TyLmtdActualTot", title: "TyLmtdActualTot", hidden: true, footerTemplate: "<div id=\"div_T3_tot_max1"+user+"\" >#: max #</div>", format: "{0:n2}" },
                    { field: "LyTmtdActualTot", title: "LyTmtdActualTot", hidden: true, footerTemplate: "<div id=\"div_TT_tot_max1"+user+"\" >#: max #</div>", format: "{0:n2}" },

                    { field: "RowCount", title: "RowCount", hidden: true, footerTemplate: "<div id=\"div_RowCount_tot_max1"+user+"\" >#: max #</div>", format: "{0:n2}" }
                    
                ];
        listaggregate = [
                    { field: "TyTodayActualTot", aggregate: "max" },
                    { field: "TyTtmtdActualTot", aggregate: "max" },
                    { field: "TyLmtdActualTot", aggregate: "max" },
                    { field: "LyTmtdActualTot", aggregate: "max" },
                    { field: "RowCount", aggregate: "max" }

                    ];

            $("#" + targetSalesinfogrid).html("");
            $("#" + targetSalesinfogrid).kendoGrid({
                height: 700,
                columns: listcol,
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                resizable: true,
                filterable: true,
                selectable: "single",
                change: BusinessDailySales_onChange,
                dataBound: BusinessDailySalesGrid_onDataBound,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 20,
                    resizable: true,

            // group: [{ field: "CostYear", attributes: { class: "homegridheader", style: "color:\\#0c0"}}],


            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "Originator",
                    fields: {

                        MC: { validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "Service/lead_customer/common.asmx/"+methodName,//"GetBusinessSalesEndUser", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { detailType: detailType, sortText: sort ,type:type},
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            aggregate: listaggregate

        }
    });
    //setvis(type);
//            var grid = $("#gridbusinesssales").data("kendoGrid");
//            grid.hideColumn('M1D');
}



function BusinessDailySalesGrid_onDataBound(arg) {
        this.select(arg.sender.tbody.find("tr:first"));
        $('#div_A1_max1d').html('<b>' + $('#div_T1_tot_max1d').html() + '</b>');
        $('#div_A2_max1d').html('<b>' + $('#div_T2_tot_max1d').html() + '</b>');
        $('#div_A3_max1d').html('<b>' + $('#div_T3_tot_max1d').html() + '</b>');
        $('#div_AT_max1d').html('<b>' + $('#div_TT_tot_max1d').html() + '</b>');
        $('#div_RowCount_max1d').html('<b>' + $('#div_RowCount_tot_max1d').html() + '</b>');

//        if(businessSalesuser==""){
//            $("#MainContent_custsalesinfo_lblCurrentFlow").html( $('#div_FlowContent'+businessSalesuser).html());
//            $("#MainContent_custsalesinfo_lblCurrentFlow").slideDown('slow');
//        }else{
//            $("#lblCurrentFlowEnduser").html( $('#div_FlowContent'+businessSalesuser).html());
//        }  
}


//For New Entry in Schedule Entry Page (Loads assigned customers for the selected rep)
function loadLeadCustomersForNewEntry(repCode) {

            $("#loadLeadCustomerGrid").html("");
            $("#loadLeadCustomerGrid").kendoGrid({
                height: 300,
                columns: [//DeleteCallCycle
                {template: '<a href="javascript:DeleteCallCycle(#=Contact.SourceId#,\'#=Contact.CustomerCode#\',\'#=Contact.EndUserCode#\',#=CreateActivity#)">Delete</a>',
                         width: "50px"
                },
                { field: "Contact.SourceId", title: "LeadID", hidden: true },
                { field: "Contact.CustomerCode", title: "Customer Code", hidden: true },
                { field: "Contact.EndUserCode", title: "EndUser Code", hidden: true },
                { field: "Contact.Name", title: "Name", width: "130px" },
                { field: "LeadStage.StageName", title: "Lead Stage", width: "130px", hidden: true },
                {field: "Contact.Address", title: "Address", width: "250px" },
                { field: "Contact.City", title: "City", width: "70px" },
                { field: "Contact.State", title: "State", width: "70px" },
                { field: "Contact.PostalCode", title: "Postal Code", width: "70px" }
            ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                filterable: true,
                selectable: "single",
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 1000,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            id: "SourceId",
                            fields: {
                                CallCycleID: { validation: { required: true} },
                                Comments: { validation: { required: true} },
                                Description: { validation: { required: true} },
                                DueOn: { type: "Date", validation: { required: true} }
                            }
                        }
                    },
                    transport: {

                        read: {
                            url: ROOT_PATH + "service/call_cycle/call_cycle_service.asmx/GetCallCycleContactsForNewEntry", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { repCode: repCode },
                            type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }
                }
            });
}

function LoadPendingApprovalsCountForHome() {
    $.ajax({
        type: "POST",
        url: ROOT_PATH + "service/lead_customer/lead_contacts_service.asmx/GetPendingCustomersCount",
        //data: JSON.stringify(param),
        contentType: "application/json; charset=utf-8",
        //dataType: "json",
        success: function (response) {
            if (response.hasOwnProperty('d')) {
                msg = response.d;
                var pendingCount = response.d;
                $("#spanPendinApprovals").html(pendingCount)
            }
        },
        error: function (response) {
        }
    });
}

/*Save Brand in Brand Entry*/
function BrandEntrySaveBrand() {
    if (($("#txtCode").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Code.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    if (($("#txtName").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }

    if ($("#txtName").val() != '' && $("#txtCode").val() != '') {
        var brandCode = $("#txtCode").val();
        var brandName = $("#txtName").val();
        var selectedBrandId = $("#hdnSelectedBrandId").val();

        var url = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=brandentry&type=insert&selectedBrandId=" + selectedBrandId + "&brandCode=" + encodeurl(brandCode) + "&brandName=" + brandName;

        $.ajax({
            url: url,
            dataType: "html",
            cache: false,
            success: function (msg) {
                if (msg == "true") {
                    var sucessMsg = GetSuccesfullMessageDiv("Brand was successfully Saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    closepopup();
                    loadBrandGrid();
                    hideStatusDiv("MainContent_div_message");

                }
                else if (msg == "codeExists") {
                    var existsMsg = GetErrorMessageDiv("Brand Code already exists !.", "MainContent_div_message_popup");

                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(existsMsg);
                    //closepopup();
                    hideStatusDiv("MainContent_div_message_popup");
                }
                else if (msg == "nameExists") {
                    var existsMsg = GetErrorMessageDiv("Brand Name already exists !.", "MainContent_div_message_popup");

                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(existsMsg);
                    //closepopup();
                    hideStatusDiv("MainContent_div_message_popup");
                }
                else {
                    var errorMsg = GetErrorMessageDiv("Error occurred !.", "MainContent_div_message");

                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(errorMsg);
                    closepopup();
                    hideStatusDiv("MainContent_div_message");
                }

            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}

/*Save Area Entry*/
function AreaEntrySave() {
    if (($("#txtName").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Area Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    
    if ($("#txtName").val() != '') {
        //var brandCode = $("#txtCode").val();
        var areaName = $("#txtName").val();
        var selectedAreaId = $("#hdnSelectedAreaId").val();
        var url = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=areaentry&type=insert&selectedAreaId=" + selectedAreaId + "&areaName=" + areaName;
        
        $.ajax({
            url: url,
            dataType: "html",
            cache: false,
            success: function (msg) {
                if (msg == "true") {
                    var sucessMsg = GetSuccesfullMessageDiv("Area was successfully Saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    closepopup();
                    loadAreaGrid();
                    hideStatusDiv("MainContent_div_message");

                }
                else if (msg == "nameExists") {
                    var existsMsg = GetErrorMessageDiv("Area Name already exists !.", "MainContent_div_message_popup");

                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(existsMsg);
                    //closepopup();
                    hideStatusDiv("MainContent_div_message_popup");
                }
                else {
                    var errorMsg = GetErrorMessageDiv("Error occurred !.", "MainContent_div_message");

                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(errorMsg);
                    closepopup();
                    hideStatusDiv("MainContent_div_message");
                }
            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}

/*Save Area Entry*/
function OutletTypeEntrySave() {
    if (($("#txtName").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Outlet Type.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    
    if ($("#txtName").val() != '') {
        //var brandCode = $("#txtCode").val();
        var outlettypeName = $("#txtName").val();
        var selectedOutletTypeId = $("#hdnSelectedOutletTypeId").val();
        var url = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=outlettypeentry&type=insert&selectedOutletTypeId=" + selectedOutletTypeId + "&outlettypeName=" + outlettypeName;
        
        $.ajax({
            url: url,
            dataType: "html",
            cache: false,
            success: function (msg) {
                if (msg == "true") {
                    var sucessMsg = GetSuccesfullMessageDiv("Outlet Type was successfully Saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    closepopup();
                    loadOutletTypes();
                    hideStatusDiv("MainContent_div_message");

                }
                else if (msg == "nameExists") {
                    var existsMsg = GetErrorMessageDiv("Outlet Type already exists !.", "MainContent_div_message_popup");

                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(existsMsg);
                    //closepopup();
                    hideStatusDiv("MainContent_div_message_popup");
                }
                else {
                    var errorMsg = GetErrorMessageDiv("Error occurred !.", "MainContent_div_message");

                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(errorMsg);
                    closepopup();
                    hideStatusDiv("MainContent_div_message");
                }
            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}

/*Save Brand in Brand Entry*/
function ProductEntrySaveFlavor() {
    if (($("#txtName").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Flavor Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }

    if ($("#txtName").val() != '') {
        var flavorName = $("#txtName").val();
        var selectedFlavorId = $("#hdnSelectedFlavorId").val();

        var url = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=productflavorentry&type=insert&selectedFlavorId=" + selectedFlavorId + "&flavorName=" + flavorName;

        $.ajax({
            url: url,
            dataType: "html",
            cache: false,
            success: function (msg) {
                if (msg == "true") {
                    var sucessMsg = GetSuccesfullMessageDiv("Product flavor was successfully Saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    closepopup();
                    loadProductFlavourGrid();
                    hideStatusDiv("MainContent_div_message");
                }
                else if (msg == "nameExists") {
                    var existsMsg = GetErrorMessageDiv("Product flavor Name already exists!.", "MainContent_div_message_popup");

                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(existsMsg);
                    //closepopup();
                    hideStatusDiv("MainContent_div_message_popup");
                }
                else {
                    var errorMsg = GetErrorMessageDiv("Error occurred!.", "MainContent_div_message");

                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(errorMsg);
                    closepopup();
                    hideStatusDiv("MainContent_div_message");
                }

            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}

/*Save Brand in Brand Entry*/
function ProductEntrySavePackingMethod() {
    if (($("#txtName").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Packing Method Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }

    if ($("#txtName").val() != '') {
        var packingName = $("#txtName").val();
        var selectedPackingId = $("#hdnSelectedPackingMethodId").val();

        var url = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=productpackingmethod&type=insert&selectedPackingId=" + selectedPackingId + "&packingName=" + packingName;

        $.ajax({
            url: url,
            dataType: "html",
            cache: false,
            success: function (msg) {
                if (msg == "true") {
                    var sucessMsg = GetSuccesfullMessageDiv("Product flavor was successfully Saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    closepopup();
                    loadPackingMethodGrid();
                    hideStatusDiv("MainContent_div_message");
                }
                else if (msg == "nameExists") {
                    var existsMsg = GetErrorMessageDiv("Product flavor Name already exists!.", "MainContent_div_message_popup");

                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(existsMsg);
                    //closepopup();
                    hideStatusDiv("MainContent_div_message_popup");
                }
                else {
                    var errorMsg = GetErrorMessageDiv("Error occurred!.", "MainContent_div_message");

                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(errorMsg);
                    closepopup();
                    hideStatusDiv("MainContent_div_message");
                }

            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}

/*Save Brand in Brand Entry*/
function ProductCategoryEntrySaveCategory() {
    if (($("#txtCode").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Code.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    if (($("#txtName").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }

    if ($("#txtName").val() != '' && $("#txtCode").val() != '') {
        var categoryCode = $("#txtCode").val();
        var categoryName = $("#txtName").val();
        var selectedCategoryId = $("#hdnSelectedCategoryId").val();

        var url = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=productcategoryentry&type=insert&selectedCategoryId=" + selectedCategoryId + "&categoryCode=" + encodeurl(categoryCode) + "&categoryName=" + categoryName;

        $.ajax({
            url: url,
            dataType: "html",
            cache: false,
            success: function (msg) {
                if (msg == "true") {
                    var sucessMsg = GetSuccesfullMessageDiv("Category was successfully Saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    closepopup();
                    loadProductCategoryGrid();
                    hideStatusDiv("MainContent_div_message");

                }
                else if (msg == "codeExists") {
                    var existsMsg = GetErrorMessageDiv("Category Code already exists !.", "MainContent_div_message_popup");

                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(existsMsg);
                    //closepopup();
                    hideStatusDiv("MainContent_div_message_popup");
                }
                else if (msg == "nameExists") {
                    var existsMsg = GetErrorMessageDiv("Category Name already exists !.", "MainContent_div_message_popup");

                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(existsMsg);
                    //closepopup();
                    hideStatusDiv("MainContent_div_message_popup");
                }
                else {
                    var errorMsg = GetErrorMessageDiv("Error occurred !.", "MainContent_div_message");

                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(errorMsg);
                    closepopup();
                    hideStatusDiv("MainContent_div_message");
                }

            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}


/*Save Brand in Brand Entry*/
function BrandEntrySaveBrand() {
    if (($("#txtCode").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Code.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    if (($("#txtName").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }

    if ($("#txtName").val() != '' && $("#txtCode").val() != '') {
        var brandCode = $("#txtCode").val();
        var brandName = $("#txtName").val();
        var selectedBrandId = $("#hdnSelectedBrandId").val();

        var url = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=brandentry&type=insert&selectedBrandId=" + selectedBrandId + "&brandCode=" + encodeurl(brandCode) + "&brandName=" + brandName;

        $.ajax({
            url: url,
            dataType: "html",
            cache: false,
            success: function (msg) {
                if (msg == "true") {
                    var sucessMsg = GetSuccesfullMessageDiv("Brand was successfully Saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    closepopup();
                    loadBrandGrid();
                    hideStatusDiv("MainContent_div_message");

                }
                else if (msg == "codeExists") {
                    var existsMsg = GetErrorMessageDiv("Brand Code already exists !.", "MainContent_div_message_popup");

                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(existsMsg);
                    //closepopup();
                    hideStatusDiv("MainContent_div_message_popup");
                }
                else if (msg == "nameExists") {
                    var existsMsg = GetErrorMessageDiv("Brand Name already exists !.", "MainContent_div_message_popup");

                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(existsMsg);
                    //closepopup();
                    hideStatusDiv("MainContent_div_message_popup");
                }
                else {
                    var errorMsg = GetErrorMessageDiv("Error occurred !.", "MainContent_div_message");

                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(errorMsg);
                    closepopup();
                    hideStatusDiv("MainContent_div_message");
                }

            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}


/*Save Division in Division Entry*/
function DivisionEntrySaveDivision() {
//    if (($("#txtCode").val()).trim() == '') {
//        var sucessMsg = GetErrorMessageDiv("Please Enter a Code.", "MainContent_div_message_popup");
//        $('#MainContent_div_message_popup').css('display', 'block');
//        $("#MainContent_div_message_popup").html(sucessMsg);
//        hideStatusDiv("MainContent_div_message_popup");
//        return;
//    }
    if (($("#txtName").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }

    if ($("#txtName").val() != '' && $("#txtCode").val() != '') {
        var divisionCode = $("#txtCode").val();
        var divisionName = $("#txtName").val();
        var selectedDivisionId = $("#hdnSelectedDivisionId").val();

        var url = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=divisionentry&type=insert&selectedDiviId=" + selectedDivisionId + "&code=" + encodeurl(divisionCode) + "&name=" + divisionName;

        $.ajax({
            url: url,
            dataType: "html",
            cache: false,
            success: function (msg) {
                if (msg == "true") {
                    var sucessMsg = GetSuccesfullMessageDiv("Brand was successfully Saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    closepopup();
                    loadDivisionGrid();
                    hideStatusDiv("MainContent_div_message");

                }
                else if (msg == "codeExists") {
                    var existsMsg = GetErrorMessageDiv("Brand Code already exists !.", "MainContent_div_message_popup");

                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(existsMsg);
                    //closepopup();
                    hideStatusDiv("MainContent_div_message_popup");
                }
                else if (msg == "nameExists") {
                    var existsMsg = GetErrorMessageDiv("Brand Name already exists !.", "MainContent_div_message_popup");

                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(existsMsg);
                    //closepopup();
                    hideStatusDiv("MainContent_div_message_popup");
                }
                else {
                    var errorMsg = GetErrorMessageDiv("Error occurred !.", "MainContent_div_message");

                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(errorMsg);
                    closepopup();
                    hideStatusDiv("MainContent_div_message");
                }

            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}

/*Load Brand Grid in Product Flavour Entry Page*/
function loadPackingMethodGrid() {
    var take_grid = $("#MainContent_hfPageIndex").val();
    var url = "javascript:updatePackingMethod('#=PackingId#','#=PackingName#');";

    var urlName = "<a href=\"" + url + "\">#=PackingName#</a>";

    $("#div_loader").show();
    $("#divPackingMethodGrid").html("");

    $("#divPackingMethodGrid").kendoGrid({
        height: 375,
        columns: [
            { field: "PackingId", title: "Id", width: "85px" },
            { template: urlName, field: "PackingName", title: "Packing Name", width: "250px" },
            { template: '<a href="javascript:DeletePackingMethod(\'#=PackingId#\');">Delete</a>', field: "", width: "40px" },
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        PackingId: { validation: { required: true } },
                        PackingName: { editable: false, nullable: true }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/load_stock/load_stock.asmx/GetAllPackingMethodDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#divPackingMethodGrid").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

/*Load Brand Grid in Product Flavour Entry Page*/
function loadProductFlavourGrid() {
    var take_grid = $("#MainContent_hfPageIndex").val();
    var url = "javascript:updateFlavor('#=FlavorId#','#=FlavorName#');";

    var urlName = "<a href=\"" + url + "\">#=FlavorName#</a>";

    $("#div_loader").show();
    $("#divProductFlavorGrid").html("");

    $("#divProductFlavorGrid").kendoGrid({
        height: 375,
        columns: [
            { field: "FlavorId", title: "Id", width: "85px" },
            { template: urlName, title: "Flavor Name", width: "250px" },
            { template: '<a href="javascript:DeleteProductFlavour(\'#=FlavorId#\');">Delete</a>', field: "", width: "40px" },
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 200,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        FlavorId: { validation: { required: true} },
                        FlavorName: { editable: false, nullable: true }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/load_stock/load_stock.asmx/GetAllFlavorsDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#divProductFlavorGrid").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function DeleteProductFlavour(fID) {
       
//    var r = confirm("Do you want to delete this flavour?");
//    if (r == true) {
//        var root = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=deleteproductflavour&type=delete&selectedFlavorId=" + fID ;
//        $.ajax({
//            url: root,
//            dataType: "html",
//            cache: false,
//            success: function (msg) {        
//                    if (msg == "true") {
//                                  var sucessMsg = GetSuccesfullMessageDiv("Product flavor was successfully Deleted.", "MainContent_div_message");
//                                  $('#MainContent_div_message').css('display', 'block');
//                                  $("#MainContent_div_message").html(sucessMsg);
//                                  closepopup();
//                                  loadProductFlavourGrid();
//                                  hideStatusDiv("MainContent_div_message");
//                                        }
//                                         else   {
//                    var existsMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message_popup");

//                    $('#MainContent_div_message_popup').css('display', 'block');
//                    $("#MainContent_div_message_popup").html(existsMsg);
//                    closepopup();
//                    hideStatusDiv("MainContent_div_message_popup");
//                }
//                
//            },
//            // error: function (XMLHttpRequest, textStatus, errorThrown) {
//            error: function (msg, textStatus) {
//                if (textStatus == "error") {
//                    var msg = msg;
//                }
//            }
//        });
//    } else {
//    } 
    var r = confirm("Do you want to delete this flavour?");
    if (r == true) {
        showPopup(fID, 0);
    } 
    else { }     
}

function DeletePackingMethod(pID) {

var r = confirm("Do you want to delete this packing method?");
            if (r == true) {                
               showPopup(pID,0);
            } else {
                   }     
               
    }

  function delete_selected_DeletePackingMethod(packing_id)  {
    var root = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=deletepackingmethod&type=delete&selectedPackingId=" + packing_id ;
        $.ajax({
            url: root,
            dataType: "html",
            cache: false,
            success: function (msg) {        
                    if (msg == "true") {
                                  var sucessMsg = GetSuccesfullMessageDiv("Packing method was successfully Deleted.", "MainContent_div_message");
                                  $('#MainContent_div_message').css('display', 'block');
                                  $("#MainContent_div_message").html(sucessMsg);
                                  closepopup();
                                  loadPackingMethodGrid();
                                  hideStatusDiv("MainContent_div_message");
                                        }
                                         else   {
                    var existsMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message_popup");

                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(existsMsg);
                    closepopup();
                    hideStatusDiv("MainContent_div_message_popup");
                }
                
            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
        }

function delete_selected_productFlavour(flavour_id)  {
    var root = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=deleteproductflavour&type=delete&selectedFlavorId=" + flavour_id ;
    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {        
                if (msg == "true") {
                                var sucessMsg = GetSuccesfullMessageDiv("Product flavor was successfully Deleted.", "MainContent_div_message");
                                $('#MainContent_div_message').css('display', 'block');
                                $("#MainContent_div_message").html(sucessMsg);
                                closepopup();
                                loadProductFlavourGrid();
                                hideStatusDiv("MainContent_div_message");
                                    }
                                        else   {
                var existsMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message_popup");

                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(existsMsg);
                closepopup();
                hideStatusDiv("MainContent_div_message_popup");
            }
                
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
}

/*Load Brand Grid in Brand Entry Page*/
function loadBrandGrid() {
    var take_grid = $("#MainContent_hfPageIndex").val();
    var url = "javascript:updateBrand('#=BrandId#','#=BrandCode#','#=BrandName#');";

    var urlName = "<a href=\"" + url + "\">#=BrandName#</a>";
    var urlCode = "<a href=\"" + url + "\">#=BrandCode#</a>";
    $("#div_loader").show();
    $("#divBrandGrid").html("");
    $("#divBrandGrid").kendoGrid({
        height: 375,
        columns: [
            { field: "BrandId", title: "Brand Id", width: "85px", hidden: true },
            { template: urlCode, field: "BrandCode", title: "Brand Code", width: "75px" },
            { template: urlName, field: "BrandName", title: "Brand Name", width: "250px" },
            { template: '<a href="javascript:DeleteBrand(\'#=BrandId#\');">Delete</a>', field: "", width: "40px" },
            { field: "Status", title: "Status", width: "105px", hidden: true }
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        BrandId: { validation: { required: true} },
                        BrandCode: { editable: false, nullable: true },
                        BrandName: { editable: false, nullable: true },
                        Status: { validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/load_stock/load_stock.asmx/GetAllBrandsDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#divBrandGrid").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function DeleteBrand(bID) {
    var r = confirm("Do you want to delete this brand?");
        if (r == true) {
            showPopup(bID, 0);
        } else {
        }     
               
    }

  function delete_selected_brand(brand_id)  {
    var root = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=deletebrand&type=delete&selectedBrandId=" + brand_id ;
        $.ajax({
            url: root,
            dataType: "html",
            cache: false,
            success: function (msg) {        
                    if (msg == "true") {
                                  var sucessMsg = GetSuccesfullMessageDiv("Brand was successfully Deleted.", "MainContent_div_message");
                                  $('#MainContent_div_message').css('display', 'block');
                                  $("#MainContent_div_message").html(sucessMsg);
                                  closepopup();
                                  loadBrandGrid();
                                  hideStatusDiv("MainContent_div_message");
                                        }
                                         else   {
                    var existsMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message_popup");

                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(existsMsg);
                    closepopup();
                    hideStatusDiv("MainContent_div_message_popup");
                }
                
            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
        }


function loadAreaGrid() {
    var take_grid = $("#MainContent_hfPageIndex").val();
    var url = "javascript:updateArea('#=AreaID#','#=AreaCode#');";

    var urlName = "<a href=\"" + url + "\">#=AreaCode#</a>";
    //var urlCode = "<a href=\"" + url + "\">#=BrandCode#</a>";
    $("#div_loader").show();
    $("#divAreaGrid").html("");
    $("#divAreaGrid").kendoGrid({
        height: 375,
        columns: [
            { field: "AreaID", title: "Area Id", width: "85px" },
            //{ template: urlCode, field: "BrandCode", title: "Brand Code", width: "75px" },
            { template: urlName, field: "AreaCode", title: "Area Name", width: "250px" },
            { template: '<a href="javascript:DeleteArea(\'#=AreaID#\');">Delete</a>', field: "", width: "40px" },
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { //Define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        AreaID: { validation: { required: true} },
                        //BrandCode: { editable: false, nullable: true },
                        AreaCode: { editable: false, nullable: true }//,
                        //Status: { validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetAllArea", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#divAreaGrid").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function DeleteArea(aID) {
    var r = confirm("Do you want to delete this area?");
            if (r == true) {               
               showPopup(aID,0);
            } else {
                   }               
    }

function delete_selected_area(area_id) {
    var root = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=deletearea&type=delete&selectedAreaId=" + area_id;
    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg == "true") {
                var sucessMsg = GetSuccesfullMessageDiv("Area was successfully Deleted.", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(sucessMsg);
                LoadAllAreas();
                closepopup();
                hideStatusDiv("MainContent_div_message");
            }
            else {
                var existsMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message_popup");

                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(existsMsg);
                closepopup();
                hideStatusDiv("MainContent_div_message_popup");
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
}

function loadOutletTypes() {
    var take_grid = $("#MainContent_hfPageIndex").val();
    var url = "javascript:updateOutletType('#=Outlettype_id#','#=Outlettype_name#');";

    var urlName = "<a href=\"" + url + "\">#=Outlettype_name#</a>";
    //var urlCode = "<a href=\"" + url + "\">#=BrandCode#</a>";
    $("#div_loader").show();
    $("#divOutletTypeGrid").html("");
    $("#divOutletTypeGrid").kendoGrid({
        height: 375,
        columns: [
            { field: "Outlettype_id", title: "Type Id", width: "85px" },
            //{ template: urlCode, field: "BrandCode", title: "Brand Code", width: "75px" },
            { template: urlName, field: "Outlettype_name", title: "Type Name", width: "250px" },
            { template: '<a href="javascript:DeleteOutletType(\'#=Outlettype_id#\');">Delete</a>', field: "", width: "40px" },
        ],
        editable: false, // enable editing
        pageable: false,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 500,
            schema: {
                data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { //Define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        Outlettype_id: { validation: { required: true } },
                        //BrandCode: { editable: false, nullable: true },
                        Outlettype_name: { editable: false, nullable: true }//,
                        //Status: { validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetAllOutletTypes", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#divOutletTypeGrid").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function DeleteOutletType(outletID) {
//    var txt;
//    var r = confirm("Do you want to delete this type?");
//    if (r == true) {

//        var root = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=deleteOutletType&type=delete&selectedOutletId=" + outletID ;
//        $.ajax({
//            url: root,
//            dataType: "html",
//            cache: false,
//            success: function (msg) {        
//                    if (msg == "true") {
//                                  var sucessMsg = GetSuccesfullMessageDiv("Outlet Type was successfully Deleted.", "MainContent_div_message");
//                                  $('#MainContent_div_message').css('display', 'block');
//                                  $("#MainContent_div_message").html(sucessMsg);
//                                  closepopup();
//                                  loadOutletTypes();
//                                  hideStatusDiv("MainContent_div_message");
//                                        }
//                                         else   {
//                    var existsMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message_popup");

//                    $('#MainContent_div_message_popup').css('display', 'block');
//                    $("#MainContent_div_message_popup").html(existsMsg);
//                    closepopup();
//                    hideStatusDiv("MainContent_div_message_popup");
//                }
//                
//            },
//            // error: function (XMLHttpRequest, textStatus, errorThrown) {
//            error: function (msg, textStatus) {
//                if (textStatus == "error") {
//                    var msg = msg;
//                }
//            }
//        });
//    }

    var r = confirm("Do you want to delete this outlet?");
            if (r == true) {
            showPopup(outletID,0);
            } else {
            }             
}

function delete_selected_OutletType(outletTypeId){
    
    var root = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=deleteOutletType&type=delete&selectedOutletId=" + outletTypeId ;
        $.ajax({
            url: root,
            dataType: "html",
            cache: false,
            success: function (msg) {        
                    if (msg == "true") {
                                  var sucessMsg = GetSuccesfullMessageDiv("Outlet Type was successfully Deleted.", "MainContent_div_message");
                                  $('#MainContent_div_message').css('display', 'block');
                                  $("#MainContent_div_message").html(sucessMsg);
                                  closepopup();
                                  loadOutletTypes();
                                  hideStatusDiv("MainContent_div_message");
                                        }
                                         else   {
                    var existsMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message_popup");

                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(existsMsg);
                    closepopup();
                    hideStatusDiv("MainContent_div_message_popup");
                }
                
            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }


/*Load Brand Grid in Brand Entry Page*/
function loadProductCategoryGrid() {
    var take_grid = $("#MainContent_hfPageIndex").val();
    var url = "javascript:updateProductCategory('#=Id#','#=Code#','#=Description#');";

    var urlName = "<a href=\"" + url + "\">#=Description#</a>";
    var urlCode = "<a href=\"" + url + "\">#=Code#</a>";
    $("#div_loader").show();
    $("#divCategoryGrid").html("");
    $("#divCategoryGrid").kendoGrid({
        height: 375,
        columns: [
            { template: '<a href="javascript:deleteProductCategory(\'#=Id#\',\'#=Code#\');">Delete</a>', field: "", width: "40px" },
            { field: "Id", title: "Category Id", width: "85px", hidden: true },
            { template: urlCode, field: "Code", title: "Category Code", width: "75px" },
            { template: urlName, field: "Description", title: "Category Name", width: "250px" }
//            { field: "Status", title: "Status", width: "105px", hidden: true }
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        Id: { validation: { required: true} },
                        Code: { editable: false, nullable: true },
                        Description: { editable: false, nullable: true }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/load_stock/load_stock.asmx/GetAllProductCategoryDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#divCategoryGrid").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

/*Update Brand in Brand Entry Page*/
function updatePackingMethod(id, name) {
    $("#hdnSelectedPackingMethodId").val(id);
    $("#txtName").val(name);

    $("#window").css("display", "block");
    var wnd = $("#window").kendoWindow({
        title: "Add/Edit Method",
        modal: true,
        visible: false,
        resizable: false
    }).data("kendoWindow");
    $("#txtName").focus();
    wnd.center().open();
}

/*Update Brand in Brand Entry Page*/
function updateFlavor(id, name) {
    $("#hdnSelectedFlavorId").val(id);
    $("#txtName").val(name);

    $("#window").css("display", "block");
    var wnd = $("#window").kendoWindow({
        title: "Add/Edit Flavor",
        modal: true,
        visible: false,
        resizable: false
    }).data("kendoWindow");
    $("#txtName").focus();
    wnd.center().open();
}

/*Update Brand in Brand Entry Page*/
function updateBrand(id, code, name) {
    $("#hdnSelectedBrandId").val(id);
    $("#txtCode").val(code);
    $("#txtName").val(name);

    $("#window").css("display", "block");
    var wnd = $("#window").kendoWindow({
        title: "Add/Edit Brand",
        modal: true,
        visible: false,
        resizable: false
    }).data("kendoWindow");
    $("#txtName").focus();
    $("#txtCode").prop('disabled', true);
    wnd.center().open();
}

/*Update Brand in Brand Entry Page*/
function updateArea(id, name) {
    $("#hdnSelectedAreaId").val(id);
    //$("#txtCode").val(code);
    $("#txtName").val(name);

    $("#window").css("display", "block");
    var wnd = $("#window").kendoWindow({
        title: "Add/Edit Area",
        modal: true,
        visible: false,
        resizable: false
    }).data("kendoWindow");
    $("#txtName").focus();
    //$("#txtCode").prop('disabled', true);
    wnd.center().open();
}

/*Update Brand in Brand Entry Page*/
function updateOutletType(id, name) {
    $("#hdnSelectedOutletTypeId").val(id);
    //$("#txtCode").val(code);
    $("#txtName").val(name);

    $("#window").css("display", "block");
    var wnd = $("#window").kendoWindow({
        title: "Add/Edit Outlet Type",
        modal: true,
        visible: false,
        resizable: false
    }).data("kendoWindow");
    $("#txtName").focus();
    //$("#txtCode").prop('disabled', true);
    wnd.center().open();
}

/*Update Brand in Brand Entry Page*/
function updateProductCategory(id, code, name) {
    $("#hdnSelectedCategoryId").val(id);
    $("#txtCode").val(code);
    $("#txtName").val(name);

    $("#window").css("display", "block");
    var wnd = $("#window").kendoWindow({
        title: "Add/Edit Category",
        modal: true,
        visible: false,
        resizable: false
    }).data("kendoWindow");
    $("#txtName").focus();
    $("#txtCode").prop('disabled', true);
    wnd.center().open();
}

/*Load Product Price Grid in Product Price Entry Page*/
function loadProductPriceGrid() {

    var take_grid = $("#MainContent_hfPageIndex").val();
    var url = "javascript:updateProductPrice('#=ProductPriceId#','#=ProductId#','#=Code#','#=Name#','#=RetailPrice#','#=OutletPrice#','#=DistributorPrice#');";

    var urlName = "<a href=\"" + url + "\">#=Name#</a>";
    var urlCode = "<a href=\"" + url + "\">#=Code#</a>";
    $("#div_loader").show();
    $("#divProductPriceGrid").html("");
    $("#divProductPriceGrid").kendoGrid({
        height: 375,
        columns: [
            { field: "ProductPriceId", title: "Product Price Id", width: "85px", hidden: true },
            { field: "ProductId", title: "Product Id", width: "85px", hidden: true },
            { template: urlCode, field: "Code", title: "Code", width: "75px" },
            { template: urlName, field: "Name", title: "Product", width: "250px" },
            { field: "RetailPrice", title: "Retailer Price", width: "250px", format: "{0:n2}" },
            { field: "OutletPrice", title: "Consumer Price", width: "250px", format: "{0:n2}", hidden: true },
            { field: "DistributorPrice", title: "Distributor Price", width: "250px", format: "{0:n2}" },

            { field: "Status", title: "Status", width: "105px", hidden: true }
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        ProductPriceId: { validation: { required: true} },
                        ProductId: { validation: { required: true} },
                        Code: { editable: false, nullable: true },
                        Name: { editable: false, nullable: true },
                        RetailPrice: { type: "number", editable: false, nullable: true },
                        OutletPrice: { type: "number", editable: false, nullable: true },
                        DistributorPrice: { type: "number", editable: false, nullable: true },
                        Status: { validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/load_stock/load_stock.asmx/GetAllProductPricesDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#divProductPriceGrid").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

/*Update Product Price in Product Price Entry Page*/
function updateProductPrice(productPriceId, productId, code, name, retailPrice, outletPrice, distPrice) {
    $("#hdnSelectedProductPriceId").val(productPriceId);
    $("#hdnSelectedProductId").val(productId);
    $("#txtRetailPrice").val(retailPrice);
    $("#txtConsumerPrice").val(outletPrice);
    $("#txtDistributorPrice").val(distPrice);
    $("#hdnSelectedRetailPriceOld").val(retailPrice);
    $("#hdnSelectedConsumerPriceOld").val(outletPrice);
    $("#hdnSelectedDistributorPriceOld").val(distPrice);
    $('#MainContent_DropDownListProducts option[value=0]').text(name);

    $("#window").css("display", "block");
    var wnd = $("#window").kendoWindow({
        title: "Add/Edit Price",
        modal: true,
        visible: false,
        resizable: false
    }).data("kendoWindow");
    $("#txtName").focus();
    $("#MainContent_DropDownListProducts").prop('disabled', true);
    wnd.center().open();
}

/*Save Product Price in Product Price Entry*/
function ProductPriceEntrySaveProductPrice() {
    if ($("#MainContent_DropDownListProducts").val() == '0' && $("#MainContent_DropDownListProducts option:selected").text() == '- Select Product -') {
        var errorMsg = GetErrorMessageDiv("Please Select a Product.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(errorMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    if (($("#txtRetailPrice").val()).trim() == '' || parseFloat($("#txtRetailPrice").val()) <= 0) {
        var errorMsg = GetErrorMessageDiv("Please Enter Retail Price.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(errorMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    //    if (($("#txtConsumerPrice").val()).trim() == '' || parseFloat($("#txtConsumerPrice").val()) <= 0) {
    //        var errorMsg = GetErrorMessageDiv("Please Enter Consumer Price.", "MainContent_div_message_popup");
    //        $('#MainContent_div_message_popup').css('display', 'block');
    //        $("#MainContent_div_message_popup").html(errorMsg);
    //        hideStatusDiv("MainContent_div_message_popup");
    //        return;
    //    }
    if (($("#txtDistributorPrice").val()).trim() == '' || parseFloat($("#txtDistributorPrice").val()) <= 0) {
        var errorMsg = GetErrorMessageDiv("Please Enter Distributor Price.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(errorMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }

    if (parseFloat($("#txtRetailPrice").val()) > 0 && parseFloat($("#txtDistributorPrice").val()) > 0) {
        var retailPrice = $("#txtRetailPrice").val();
        var consumerPrice = $("#txtConsumerPrice").val();
        var distPrice = $("#txtDistributorPrice").val();
        var selectedProductPriceId = $("#hdnSelectedProductPriceId").val();
        var selectedProductId = $("#hdnSelectedProductId").val();

        var retailPriceOld = $("#hdnSelectedRetailPriceOld").val();
        var consumerPriceOld = $("#hdnSelectedConsumerPriceOld").val();
        var distPriceOld = $("#hdnSelectedDistributorPriceOld").val();

        if (selectedProductPriceId != '' && parseFloat(retailPriceOld) == parseFloat(retailPrice) && parseFloat(distPriceOld) == parseFloat(distPrice)) {
            var sucessMsg = GetSuccesfullMessageDiv("Price was successfully Saved.", "MainContent_div_message");
            $('#MainContent_div_message').css('display', 'block');
            $("#MainContent_div_message").html(sucessMsg);
            closepopup();
            loadProductPriceGrid();
            hideStatusDiv("MainContent_div_message");
            return;
        }

        var url = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=productpriceentry&type=insert&selectedProductPriceId=" + selectedProductPriceId + "&selectedProductId=" + selectedProductId + "&retailPrice=" + retailPrice
            + "&consumerPrice=" + consumerPrice + "&distPrice=" + distPrice;

        $.ajax({
            url: url,
            dataType: "html",
            cache: false,
            success: function (msg) {
                if (msg == "true") {
                    var sucessMsg = GetSuccesfullMessageDiv("Price was successfully Saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    closepopup();
                    loadProductPriceGrid();
                    hideStatusDiv("MainContent_div_message");
                }
                else {
                    var errorMsg = GetErrorMessageDiv("Error occurred !.", "MainContent_div_message");

                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(errorMsg);
                    closepopup();
                    hideStatusDiv("MainContent_div_message");
                }

            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}

function loadDiscountSchemeDetails(headerId, isSession) {

    var take_grid = $("#MainContent_hfPageIndex").val();
    var url = "javascript:updateSchemeDetails('#=IndexId#', '#=DetailsTypeId#', '#=SchemeTypeId#', '#=SchemeDetails#', '#=DiscountValue#', '#=DiscountPercentage#', #=IsRetail#, '#=FreeIssueCondition#');";

    var urlName = "<a href=\"" + url + "\">Edit</a>";
    //var urlCode = "<a href=\"" + url + "\">#=BrandCode#</a>";
    $("#div_loader").show();
    $("#divSchemeHeaderGrid").html("");
    $("#divSchemeHeaderGrid").kendoGrid({
        height: 375,
        columns: [
            { field: "SchemeHeaderId", title: "SchemeHeaderId", width: "85px", hidden: true },
            //                        { template: urlName, field: "SchemeHeaderName", title: "Scheme Name", width: "250px" },
            { template: '<a href="javascript:ShowSchemeDetailfirmation(#=IndexId#)">Delete</a>', width: "60px" },
            { template: urlName, width: "60px" },
            { field: "SchemeDetails", title: "Scheme Details", width: "225px" },
            { field: "DetailsTypeName", title: "Detail Type", width: "125px" },
            { field: "SchemeTypeName", title: "Scheme Type", width: "125px" },
            { field: "IsRetail", title: "Is Retail", width: "80px" },
            //                        { field: "SchemeDetails", title: "Scheme Details", width: "125px"},
            { field: "DiscountValue", title: "Discoun tValue", width: "125px" },
            { field: "DiscountPercentage", title: "Discount Percentage", width: "125px" },

        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 50,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        SchemeHeaderId: { validation: { required: true } },
                        SchemeHeaderName: { editable: false, nullable: true },
                        DetailsTypeName: { editable: false, nullable: true },
                        SchemeTypeName: { editable: false, nullable: true }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/discount_scheme/discount_scheme_service.asmx/GetSchemeDetailsDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { headerid: headerId, pgindex: take_grid, isSession: isSession },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#divSchemeHeaderGrid").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function updateSchemeDetails(indexId, DetailTypeId, SchemeTypeId, SchemeDetails, DiscountValue, DiscountPercentage, isRetail, FreeIssueCond) {
    $("#txtMinBValue").val(0);
    $("#txtDiscountValue").val(DiscountValue);
    $("#txtPercentage").val(DiscountPercentage);
    $("#DropDownListDetailType").val(DetailTypeId);
    $("#DropDownListSchemeType").val(SchemeTypeId);
    $("#txtSchemeDetails").val(SchemeDetails);
    $("#MainContent_uclOptionLevel_ddlFreeProductCondition").val(FreeIssueCond);

    if (isRetail == null) {
        $('#cmbShopType').val("Any");
    } else if (isRetail) {
        $('#cmbShopType').val("Retailer");
    } else if (!isRetail) {
        $('#cmbShopType').val("Wholesale");
    }

    //$('#chkIsRetailer').prop('checked', isRetail);
    SetSchemeTypeUi(SchemeTypeId);

    var root = ROOT_PATH + "discount_scheme/process_forms/processmaster.aspx?fm=loadschemedetail&type=query&id=" + indexId;

    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg == "1") {
                GetOptionLevelList();
                $('#div_scheme_detail').css('display', 'none');
                $('#itemcontent').css('display', 'block');

                $('#a_add_scheme').css('display', 'inline-block');
                $('#a_cancel').css('display', 'inline-block');
                $('#MainContent_buttonbar_buttinSave').css('display', 'none');
                $('#a_add_option').css('display', 'none');

                if (DetailTypeId == 1 || DetailTypeId == 4) {
                    $('#div_Percentage').css('display', 'none');
                    $('#div_value').css('display', 'block');
                    $('#div_free_product').css('display', 'none');
                    $('#div_isForEvery').css('display', 'block');
                }
                else if (DetailTypeId == 2 || DetailTypeId == 5) {
                    $('#div_Percentage').css('display', 'block');
                    $('#div_value').css('display', 'none');
                    $('#div_free_product').css('display', 'none');
                    $('#div_isForEvery').css('display', 'block');
                }
                else if (DetailTypeId == 3) {
                    GetFreeProductList();
                    $('#div_Percentage').css('display', 'none');
                    $('#div_value').css('display', 'none');
                    $('#div_free_product').css('display', 'block');
                    $('#div_isForEvery').css('display', 'block');
                }

            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
}


            /*Load Brand Grid in Brand Entry Page*/
function loadDivisionGrid() {
    var take_grid = $("#MainContent_hfPageIndex").val();
    var url = "javascript:updateDivision('#=DivisionId#','#=DivisionCode#','#=DivisionName#');";

    var urlName = "<a href=\"" + url + "\">#=DivisionName#</a>";
    var urlCode = "<a href=\"" + url + "\">#=DivisionCode#</a>";
    $("#div_loader").show();
    $("#divDivisionGrid").html("");
    $("#divDivisionGrid").kendoGrid({
        height: 375,
        columns: [
            { field: "DivisionId", title: "Division Id", width: "85px", hidden: true },
            { template: urlCode, field: "DivisionCode", title: "Division Code", width: "75px" },
            { template: urlName, field: "DivisionName", title: "Division Name", width: "250px" },
            { field: "IsActive", title: "Status", width: "105px", hidden: true },
            { template: '<a href="javascript:DeleteDivision(\'#=DivisionId#\');">Delete</a>', field: "", width: "40px" },
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        DivisionId: { validation: { required: true} },
                        DivisionCode: { editable: false, nullable: true },
                        DivisionName: { editable: false, nullable: true },
                        Status: { validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/discount_scheme/discount_scheme_service.asmx/GetAllDivisionsDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#divDivisionGrid").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function DeleteDivision(dID) {

var r = confirm("Do you want to delete this division?");
            if (r == true) {                
               showPopup(dID,0);
            } else {
                   }  
    }

  function delete_selected_DeleteDivision(division_id)  {
    var root = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=deletedivision&type=delete&selecteddivisionId=" + division_id ;
        $.ajax({
            url: root,
            dataType: "html",
            cache: false,
            success: function (msg) {        
                    if (msg == "true") {
                                  var sucessMsg = GetSuccesfullMessageDiv("Division was successfully Deleted.", "MainContent_div_message");
                                  $('#MainContent_div_message').css('display', 'block');
                                  $("#MainContent_div_message").html(sucessMsg);
                                  closepopup();
                                  loadDivisionGrid();
                                  hideStatusDiv("MainContent_div_message");
                                        }
                                         else   {
                    var existsMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message_popup");

                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(existsMsg);
                    closepopup();
                    hideStatusDiv("MainContent_div_message_popup");
                }
                
            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
        }


/*Update Brand in Brand Entry Page*/
function updateDivision(id, code, name) {
    $("#hdnSelectedDivisionId").val(id);
    $("#txtCode").val(code);
    $("#txtName").val(name);

    $("#window").css("display", "block");
    var wnd = $("#window").kendoWindow({
        title: "Add/Edit Division",
        modal: true,
        visible: false,
        resizable: false
    }).data("kendoWindow");
    $("#txtName").focus();
    $("#txtCode").prop('disabled', true);
    wnd.center().open();
}


/*Load Assigned Markets Grid in Market Assign Page*/
function load_division_products_grid(divisionId) {
    $("#loadProductAssignGrid").html("");
    $("#loadMarketAssignGrid").kendoGrid({
        height: 250,
        columns: [
        { template: '<a href="javascript:deleteMarket(\'#=ProductId#\',\'#=Name#\');">Delete</a>',
            field: "", width: "40px"
        },
        { field: "ProductId", title: "Product Id", width: "100px", hidden: true },
        { field: "Name", title: "Product Name", width: "100px" }
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    fields: {
                        ProductId: { validation: { required: true} },
                        Name: { validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/common.asmx/GetOriginatorMarkets", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { type: "ORIGINATOR_MARKETS", param: divisionId },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}



function GetOptionLevelList() {
        $("#grid_option_level").html("");
        $("#grid_option_level").kendoGrid({
            // height: 400,
            columns: [
                { template: '<a href="javascript:ShowOptionLevelfirmation(#=IndexId#)">Delete</a>',
                    width: "60px"
                },
                {
                    template: '<a href="javascript:EditOptionLevel(#=IndexId#,#=MaxQuantity#,#=MinQuantity#,#=MaxValue#,#=MinValue#,#=OperatorId#,#=IsBill#,#=IsForEvery#,#=LineCount#)">Edit</a>',
                    width: "50px"
                },
                { template: '<a href="javascript:AddCombination(#=IndexId#)">Add Combination</a>',
                     width: "130px"
                },
            //{field: "ProductName", title: "SKU", width: "85px", hidden: false },
//            {field: "ProductCatagoryName", title: "Item Group", width: "100px", hidden: false },
//            { field: "BrandName", title: "Brand", width: "100px", hidden: false },
//            { field: "CatlogName", title: "Flavor", width: "100px", hidden: false },
//            { field: "ProductPackingName", title: "Packing Method", width: "120px", hidden: false },
//            { field: "IsHvp", title: "HVP", width: "70px", hidden: false },
            { field: "LineCount", title: "Line Count", width: "90px", hidden: false },
            { field: "MinQuantity", title: "Min Quantity", width: "90px", hidden: false },
            { field: "MaxQuantity", title: "Max Quantity", width: "90px", hidden: false },
            { field: "MinValue", title: "Min Value", width: "90px", hidden: false },
            { field: "MaxValue", title: "Max Value", width: "90px", hidden: false }
        ],
            editable: false, // enable editing
            pageable: false,
            sortable: false,
            filterable: false,
            selectable: "single",
            columnMenu: false,
            //change: OptionLevel_onChange,
            detailInit: OptionLevelDetailInit,
            dataBound: function () {
                
                var that = this;

                $.each(that.tbody.find("tr.k-master-row"), function (key, value) {
                    that.expandRow(value);
                });

            },
            dataSource: {
                serverSorting: true,
                serverPaging: true,
                serverFiltering: true,
                pageSize: 20,
                schema: {
                    data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                    total: "d.Total",
                    model: { // define the model of the data source. Required for validation and property types.
                        id: "SourceId",
                        fields: {
                        }
                    }
                },
                transport: {

                    read: {
                        url: ROOT_PATH + "service/discount_scheme/discount_scheme_service.asmx/GetOptionLevelList", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                        contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                        //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer', pgindex: take_grid },
                        type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                        complete: function (jqXHR, textStatus) {
                        }
                    },
                    parameterMap: function (data, operation) {
                        if (operation != "read") {
                            return JSON.stringify({ products: data.models })
                        } else {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }//,
                //aggregate: [{ field: "rowCount", aggregate: "max"}]
            }
        });
    }

    function OptionLevelDetailInit(e) {
        indexId = e.data.IndexId;
        $("<div/>").appendTo(e.detailCell).kendoGrid({
            columns: [
                    { template: '<a href="javascript:ShowCombinationfirmation(#=IndexId#,'+indexId+')">Delete</a>',
                        width: "50px"
                    },
//                    { template: '<a href="javascript:EditCombination(#=IndexId#,\'#=BrandName#\',\'#=ProductPackingName#\',\'#=FlavorName#\',\'#=ProductCatagoryName#\',\'#=ProductName#\',#=BrandId#,#=ProductPackingId#,#=FlavorId#,#=ProductCategoryId#,#=ProductId#,#=IsHvp#,#=IsExceptProduct#,'+indexId+')">Edit</a>',
//                        width: "50px"
//                    },
                        { template: '<a href="javascript:EditCombination(#=IndexId#,\'#=BrandName#\',\'#=ProductPackingName#\',\'#=FlavorName#\',\'#=ProductCatagoryName#\',\'#=ProductName#\',#=BrandId#,#=ProductPackingId#,#=FlavorId#,#=ProductCategoryId#,#=ProductId#,#=IsHvp#,#=IsExceptProduct#,\'#=IsExcept#\','+indexId+')">Edit</a>',
                        width: "50px"
                    },
                { field: "ProductCatagoryName", title: "Item Group", width: "50px", hidden: false },
                { field: "BrandName", title: "Brand", width: "65px", hidden: false },
                { field: "FlavorName", title: "Flavor", width: "50px", hidden: false },
                { field: "ProductPackingName", title: "Packing Method", width: "80px", hidden: false },
                { field: "ProductName", title: "Product", width: "200px", hidden: false },
                { field: "IsHvp", title: "HVP", width: "50px", hidden: true },
                { field: "Hvp", title: "HVP", width: "50px", hidden: false },
                { field: "IsExceptProduct", title: "IsExceptProduct", width: "70px", hidden: true },
                { field: "IsExcept", title: "Level Type", width: "70px" }
            ],
            editable: false, // enable editing
            pageable: false,
            sortable: false,
            filterable: false,
            selectable: "single",
            columnMenu: false,
            dataSource: e.data.OptionLevelCombinationList,
            dataBound: OptionLevelDetail_onDataBound
        });
    }
      
function OptionLevelDetail_onDataBound(arg) {

    var dataView = this.dataSource.view();
    for (var i = 0; i < dataView.length; i++) {
        var uid = dataView[i].uid;
        //if (dataView[i].IsExceptProduct == true) {            
        //    $("#grid_option_level tbody").find("tr[data-uid=" + uid + "]").css('background-color', '#d8bfd8');
        //}
        if (dataView[i].IsExcept == 'Compulsory') {            
            $("#grid_option_level tbody").find("tr[data-uid=" + uid + "]").css('background-color', '#B3E2B9');
        }
        if (dataView[i].IsExcept == 'Exception') {            
            $("#grid_option_level tbody").find("tr[data-uid=" + uid + "]").css('background-color', '#d8bfd8');
        }
    }
}

      
function RemoveCombinationFromSession(indexId,optionId) {
    var root = ROOT_PATH + "discount_scheme/process_forms/processmaster.aspx?fm=removecombination&type=query&cid=" + indexId + "&oid=" + optionId;

    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg == "1") {
                var grid = $("#grid_option_level").data("kendoGrid");
                grid.dataSource.read();
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });

}

function ShowRemoveFromSessiononfirmation(message,msgTitle, indexId, type,optionId) {
    $("#btnRemoveNo").css("display", "inline-block");
    $("#btnRemoveYes").css("display", "inline-block");

    var wndRemove = $("#schememodalWindow").kendoWindow({
            //title: msgTitle,
            title: "Delete Confirmation",
            modal: true,
            visible: false,
            resizable: false,
            width: 400
        }).data("kendoWindow");

    $("#div_schemeconfirm_message").text(message);
    wndRemove.center().open();
    $('#btnRemoveYes').unbind('click');
    $("#btnRemoveYes").click(function () {
        wndRemove.close();
        if (type == 1) {
            RemoveOptionLevelFromSession(indexId);
        }
        else if (type == 2) {
            RemoveCombinationFromSession(indexId,optionId);
        } else if (type == 3) {
            RemoveFreeProductToSession(indexId);
        }else if (type == 4) {
            RemoveSchemedetailFromSession(indexId);
        }
    });

    $('#btnRemoveNo').unbind('click');
    $("#btnRemoveNo").click(function () {
        wndRemove.close();
    });
}



////        function EditOptionLevel(ProductId, Qty, Product) {
//function EditCombination(id, brand, packt, cat, itemg, pro, brandid, packtid, catid, itemgid, proid, ishvp, isexc, pIndex) {

//    $("#HiddenField_IndexId").val(pIndex);
//    $("#HiddenField_combination_indexId").val(id);
//    $("#HiddenField_ProductId").val(proid);
//    $("#txtProduct").val("");
//    $("#txtSku").val(pro);
//    $("#cmbBrand").val(brandid);
//    $("#cmdPackingType").val(packtid)
//    $("#cmbCatlog").val(catid);
//    $("#cmbItemGroup").val(itemgid)

//    $("#div_qty_limit").css("display", "block");
//    $("#div_bill").css("display", "none");
//    $("#div_typebill").css("display", "none");
//    $("#div_between").css("display", "none");
//    $('#chkIsBill').prop('checked', false);
//   // $('#div_isForEvery').css('display', 'none');
//    SetIsHVP(ishvp);
//    $('#chkIsExcept').prop('checked', isexc);

//    $("#btnOptionAdd").css("display", "none");
//    $("#btnCombinationAdd").css("display", "inline-block");
//    $("#div_min_val").css("display", "none");
//    $("#div_max_val").css("display", "none");
//    $("#div_isexcept").css("display", "block");


//    wnd.center().open();
//}

function EditCombination(id, brand, packt, cat, itemg, pro, brandid, packtid, catid, itemgid, proid, ishvp, isexc, isexcnew, pIndex) {

    $("#HiddenField_IndexId").val(pIndex);
    $("#HiddenField_combination_indexId").val(id);
    $("#HiddenField_ProductId").val(proid);
    $("#txtProduct").val("");
    $("#txtSku").val(pro);
    $("#cmbBrand").val(brandid);
    $("#cmdPackingType").val(packtid);
    $("#cmbCatlog").val(catid);
    $("#cmbItemGroup").val(itemgid);
    $("#cmbFlavor").val(catid); 
    $("#ddlLevelType").val(isexcnew.substring(0, 1));  

    $("#div_qty_limit").css("display", "block");
    $("#div_bill").css("display", "none");
    $("#div_typebill").css("display", "none");
    $("#div_between").css("display", "none");
    $('#chkIsBill').prop('checked', false);
   // $('#div_isForEvery').css('display', 'none');
    SetIsHVP(ishvp);
    $('#chkIsExcept').prop('checked', isexc);

    $("#btnOptionAdd").css("display", "none");
    $("#btnCombinationAdd").css("display", "inline-block");
    $("#div_min_val").css("display", "none");
    $("#div_max_val").css("display", "none");
    $("#div_isexcept").css("display", "block");


    wnd.center().open();
}


function SetIsHVP(ishvp){
   
    if(ishvp == false){
        $('#cmbIsHVP').val("No");
    }
    else if(ishvp == true){
        $('#cmbIsHVP').val("Yes");
    }
    else{
        $('#cmbIsHVP').val("Any");
    }
    
}

function AddCombination(indexId) {

    $("#HiddenField_IndexId").val(indexId);

    $("#HiddenField_combination_indexId").val("0");
    $("#txtBrand").val("Any");
    $("#txtPackingType").val("Any");
    $("#txtCatlog").val("Any");
    $("#txtItemGroup").val("Any");
    $("#cmbBrand").val("0");
    $("#cmdPackingType").val(0);
    $("#cmbFlavor").val("0");
    $("#cmbItemGroup").val("0");
    $("#txtMinQuantity").val("0");
    $("#txtMaxQuantity").val("0");
    $("#txtMinValue").val("0");
    $("#txtMaxValue").val("0");
    $("#DropDownListOperator").val(0);

    $("#HiddenField_ProductId").val("0");
    $("#txtProduct").val("");
    $("#txtSku").val("Any");


    //$("#div_qty_limit").css("display", "block");
    //$("#div_bill").css("display", "none");
    //$("#div_typebill").css("display", "none");
    //$("#div_between").css("display", "none");

    //$("#div_min_val").css("display", "none");
    //$("#div_max_val").css("display", "none");
    $("#div_isexcept").css("display", "block");

    //$('#chkIsBill').prop('checked', false);
    $('#cmbIsHVP').val("Any");
    $('#chkIsExcept').prop('checked', false);

    $("#btnOptionAdd").css("display", "none");
    $("#btnCombinationAdd").css("display", "inline-block");

    wnd.center().open();
}



function AddCombinationToSession() {
    var ishvp = 3;
    var isexc = 0;

//    if ($('#chbIsHVP').is(":checked")) {
//        ishvp = 1;
//    }
    if ($('#chkIsExcept').is(":checked")) {
        isexc = 1;
    }

    if ($('#cmbIsHVP').val()=="No") {
        ishvp = 0;
    }
    if ($('#cmbIsHVP').val()=="Yes") {
        ishvp = 1;
    }

    //$('#cmvIsHVP').val("Any");

    //alert($('#chbIsHVP').prop('indeterminate'));

    var root = ROOT_PATH + "discount_scheme/process_forms/processmaster.aspx?fm=addcombination&type=query&oid=" + $("#HiddenField_IndexId").val()
                    + "&cid=" + $("#HiddenField_combination_indexId").val()
                    + "&brand=" + $("#cmbBrand option:selected").text()
                    + "&packt=" + $("#cmdPackingType option:selected").text()
                    + "&fla=" + $("#cmbFlavor option:selected").text()
                    + "&itemg=" + $("#cmbItemGroup option:selected").text()
                    + "&pro=" + $("#txtSku").val()
                    + "&brandid=" + $("#cmbBrand").val()
                    + "&packtid=" + $("#cmdPackingType").val() 
                    + "&flaid=" + $("#cmbFlavor").val()
                    + "&itemgid=" + $("#cmbItemGroup").val()
                    + "&proid=" + $("#HiddenField_ProductId").val()
                    //+ "&isexc=" + isexc
                    + "&isexc=" + $("#ddlLevelType option:selected").val()
                    + "&ishvp=" + ishvp;

    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg == "1") {

                //var selectedItem = grid.dataItem(grid.select());
                //grid.select().index()
                $("#HiddenField_IndexId").val("0");
                var grid = $("#grid_option_level").data("kendoGrid");
                grid.dataSource.read();
                //var grid = $("#grid").data("kendoGrid");
                // expands first master row
                //grid.expandRow(grid.tbody.find(">tr.k-master-row:nth-child(" + grid.select().index() + ")"));

                //var grid = $("#grid_combination").data("kendoGrid");
                //grid.dataSource.read();
            }
        },
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
}   

function AddSchemeToSession() {
    var isret = 2;
    var val = 0;
    var per = 0;

    if ($("#txtSchemeDetails").val().length == 0) {
        var sucessMsg = GetErrorMessageDiv("Please Enter Scheme Details", "MainContent_div_message");
        $('#MainContent_div_message').css('display', 'block');
        $("#MainContent_div_message").html(sucessMsg);
        
        hideStatusDiv("MainContent_div_message");
        return;
    }

    if ($('#cmbShopType').val() == "Retailer") {
        isret = 1;
    } else if ($('#cmbShopType').val() == "Wholesale") {
        isret = 0;
    }

    if ($("#DropDownListDetailType").val() == 1 || $("#DropDownListDetailType").val() == 4) {
        if ($("#txtDiscountValue").val().length == 0) {
            var sucessMsg = GetErrorMessageDiv("Please Enter Discount Value", "MainContent_div_message");
            $('#MainContent_div_message').css('display', 'block');
            $("#MainContent_div_message").html(sucessMsg);
            hideStatusDiv("MainContent_div_message");
            return;
        } else if ($("#txtDiscountValue").val() == '0') {
            var sucessMsg = GetErrorMessageDiv("Please Enter Discount Value", "MainContent_div_message");
            $('#MainContent_div_message').css('display', 'block');
            $("#MainContent_div_message").html(sucessMsg);
            hideStatusDiv("MainContent_div_message");
            return;
        }
        per = 0;
        val = $("#txtDiscountValue").val();

    } else if ($("#DropDownListDetailType").val() == 2 || $("#DropDownListDetailType").val() == 5) {
        if ($("#txtPercentage").val().length == 0) {
            var sucessMsg = GetErrorMessageDiv("Please Enter Discount Percentage", "MainContent_div_message");
            $('#MainContent_div_message').css('display', 'block');
            $("#MainContent_div_message").html(sucessMsg);
            hideStatusDiv("MainContent_div_message");
            return;
        } else if ($("#txtPercentage").val() == '0') {
            var sucessMsg = GetErrorMessageDiv("Please Enter Discount Percentage", "MainContent_div_message");
            $('#MainContent_div_message').css('display', 'block');
            $("#MainContent_div_message").html(sucessMsg);
            hideStatusDiv("MainContent_div_message");
            return;
        }

        per = $("#txtPercentage").val();
        val = 0;
    }

    var a = $("#MainContent_uclOptionLevel_ddlFreeProductCondition option:selected").val();
    
    var root = ROOT_PATH + "discount_scheme/process_forms/processmaster.aspx?fm=addschemedetail&type=query&id=" + $("#HiddenField_IndexId").val()
        + "&stypeid=" + $("#DropDownListSchemeType").val()
        + "&per=" + per
        + "&val=" + val
        + "&dtypeid=" + $("#DropDownListDetailType").val()
        + "&isret=" + isret
        + "&stext=" + $("#txtSchemeDetails").val()
        + "&freeprodcond=" + $("#ddlFreeProductCondition option:selected").text()
        + "&stype=" + $("#DropDownListSchemeType option:selected").text()
        + "&dtype=" + $("#DropDownListDetailType option:selected").text()
        + "&freeisshuecondition=" + $("#MainContent_uclOptionLevel_ddlFreeProductCondition option:selected").val();


    $.ajax({
        url: root,
        dataType: "html",
        cache: false,
        success: function (msg) {
            if (msg == "1") {
                $('#div_scheme_detail').css('display', 'block');
                $('#itemcontent').css('display', 'none');
                loadDiscountSchemeDetails($('#MainContent_HiddenFieldDiscountSchemeId').val(), 1);

                $('#a_add_scheme').css('display', 'none');
                $('#a_cancel').css('display', 'none');

                $('#div_free_product').css('display', 'none');

                $('#MainContent_buttonbar_buttinSave').css('display', 'inline-block');
                $('#a_add_option').css('display', 'inline-block');

                //var grid = $("#grid_option_level").data("kendoGrid");
                //grid.dataSource.read();
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });

}
        
        
        function RemoveOptionLevelFromSession(indexId) {

            $("#div_combination").css("display", "none");
            var root = ROOT_PATH + "discount_scheme/process_forms/processmaster.aspx?fm=removeoptionlevel&type=query&id=" + indexId;

            $.ajax({
                url: root,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg == "1") {
                        var grid = $("#grid_option_level").data("kendoGrid");
                        grid.dataSource.read();
                    }
                },
                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                    }
                }
            });

        }

        function RemoveSchemedetailFromSession(indexId) {

            var root = ROOT_PATH + "discount_scheme/process_forms/processmaster.aspx?fm=removeschemedetail&type=query&id=" + indexId;

            $.ajax({
                url: root,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg == "1") {
                       loadDiscountSchemeDetails($('#MainContent_HiddenFieldDiscountSchemeId').val(),1);
                    }
                },
                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                    }
                }
            });

        }

        //        function EditOptionLevel(ProductId, Qty, Product) {
        function EditOptionLevel(id, maxq, minq, maxv, minv, opeid, isbill, isfor, linecount) {
            $("#div_combination").css("display", "none");
            $("#HiddenField_IndexId").val(id);

//            $("#cmbBrand").val(brandid);
//            $("#cmdPackingType").val(packtid);
//            $("#cmbCatlog").val(catid);
//            $("#cmbItemGroup").val(itemgid);
            $("#txtLineCount").val(linecount);
            $("#txtMinQuantity").val(minq);
            $("#txtMaxQuantity").val(maxq);
            $("#txtMinValue").val(minv);
            $("#txtMaxValue").val(maxv);
            
            $('#chkIsForEvery').prop('checked', isfor);

            SetSchemeTypeUi($("#DropDownListSchemeType").val());

            $('#chkIsBill').prop('checked', isbill);
            if (isbill) {
               // $("#div_qty_limit").css("display", "none");
                $("#div_bill").css("display", "block");
                $("#txtMinBValue").val(minv);
                $("#txtMaxBValue").val(maxv);
                $("#txtMinValue").val("0");
                $("#txtMaxValue").val("0");               

                $("#DropDownListOperator").val(opeid);
                if (opeid == 6) {
                    $("#div_between").css("display", "block");
                } else {
                    $("#div_between").css("display", "none");
                }
            } else {
                //$("#div_qty_limit").css("display", "block");
                $("#div_bill").css("display", "none");
                $("#div_qty_val").css("display", "block");
                
                $("#txtMinValue").val(minv);
                $("#txtMaxValue").val(maxv);
                $("#txtMinBValue").val("0");
                $("#txtMaxBValue").val("0");
                $("#DropDownListOperator").val(0);
            }

            

            //$('#chbIsHVP').prop('checked', ishvp);

//            $("#div_min_val").css("display", "block");
//            $("#div_max_val").css("display", "block");
            //$("#div_isexcept").css("display", "none");

           // $("#btnOptionAdd").css("display", "inline-block");
            //$("#btnCombinationAdd").css("display", "none");

            //wnd.center().open();
        }

        function AddOptionLevel() {
            $("#div_combination").css("display", "none");
            $("#HiddenField_IndexId").val("0");
//            $("#txtBrand").val("Any");
//            $("#txtPackingType").val("Any");
//            $("#txtCatlog").val("Any");
//            $("#txtItemGroup").val("Any");
            $("#cmbBrand").val("0");
            $("#cmdPackingType").val(0)
            $("#cmbCatlog").val("0");
            $("#cmbItemGroup").val("0");
            $("#txtMinQuantity").val("0");
            $("#txtMaxQuantity").val("0");
            $("#txtMinValue").val("0");
            $("#txtMaxValue").val("0");
            $("#DropDownListOperator").val(0);
                        
            $("#div_qty_limit").css("display", "block");
            $("#div_bill").css("display", "none");
            $('#chkIsBill').prop('checked', false);
            $('#chbIsHVP').prop('checked', false);
            $('#chkIsForEvery').prop('checked', false);

            $("#div_min_val").css("display", "block");
            $("#div_max_val").css("display", "block");
            $("#div_isexcept").css("display", "none");

            $('#div_isForEvery').css('display', 'none');

            $("#btnOptionAdd").css("display", "inline-block");
            $("#btnCombinationAdd").css("display", "none");

            wnd.center().open();
        }
        
        function LoadOptionLevelPopUp() {

            $("#btnOptionNo").css("display", "inline-block");
            $("#btnOptionAdd").css("display", "inline-block");
            $("#btnCombinationAdd").css("display", "none");

            wnd = $("#div_option_level_form").kendoWindow({
                title: "Add Option Level",
                modal: true,
                visible: false,
                resizable: false,
                width: 700
            }).data("kendoWindow");

            //$("#div_confirm_message").text(message);
            //wnd.center().open();
            wnd.close();
            $("#btnOptionAdd").click(function () {
                AddOptionLevelToSession();
                wnd.close();
            });

            $("#btnOptionNo").click(function () {
                wnd.close();
                $("#HiddenField_IndexId").val("0");
            });

            $("#btnCombinationAdd").click(function () {
                AddCombinationToSession();
                wnd.close();
            });

        }

        function AddOptionLevelToSession() {
            var ishvp = 0;
            var isbill = 0;
            var opeid = 0;
            var maxb = 0;
            var minb = 0;

            var isfor = 0;

            if ($('#chkIsBill').is(":checked")) {
                isbill = 1;
                opeid = $("#DropDownListOperator").val();
                minb = $("#txtMinBValue").val();
                if (opeid == 6) {
                    maxb = $("#txtMaxBValue").val();
                }
            } else {
                minb = $("#txtMinValue").val();
                maxb = $("#txtMaxValue").val();
                opeid = 0;
                isbill = 0;
            }


            if ($('#chbIsHVP').is(":checked")) {
                ishvp = 1;
            }
            if ($('#chkIsForEvery').is(":checked")) {
                isfor = 1;
            }

            var root = ROOT_PATH + "discount_scheme/process_forms/processmaster.aspx?fm=addoptionlevel&type=query&id=" + $("#HiddenField_IndexId").val()
                         + "&brand=" + $("#cmbBrand option:selected").text()
                         + "&packt=" + $("#cmdPackingType option:selected").text()
                         + "&fla=" + $("#cmbFlavor option:selected").text()
                         + "&itemg=" + $("#cmbItemGroup option:selected").text()
                         + "&brandid=" + $("#cmbBrand").val()
                         + "&packtid=" + $("#cmdPackingType").val()
                         + "&flaid=" + $("#cmbFlavor").val()
                         + "&itemgid=" + $("#cmbItemGroup").val()
                         + "&linecount=" + $("#txtLineCount").val()
                         + "&minq=" + $("#txtMinQuantity").val()
                         + "&maxq=" + $("#txtMaxQuantity").val()
                         + "&minv=" + minb //$("#txtMinValue").val()
                         + "&maxv=" + maxb //$("#txtMaxValue").val()
                         + "&ishvp=" + ishvp
                         + "&isbill=" + isbill
                         + "&opeid=" + opeid
                         + "&isfor=" + isfor

            $.ajax({
                url: root,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg == "1") {
                        var grid = $("#grid_option_level").data("kendoGrid");
                        grid.dataSource.read();
                        $("#HiddenField_IndexId").val('0');
                        $("#txtLineCount").val("0");
                        $("#txtMinQuantity").val("0");
                        $("#txtMaxQuantity").val("0");
                        $("#txtMinValue").val("0");
                        $("#txtMaxValue").val("0");
                        $("#txtMinBValue").val("0");
                        $("#txtMaxBValue").val("0");
                        $("#DropDownListOperator").val(0);
                        $('#chkIsForEvery').prop('checked', false);
                        $('#chkIsBill').prop('checked', false);
                    }
                },
                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                    }
                }
            });
        }    


        function GetFreeProductList() {

        // var take_grid = $("#MainContent_hfPageIndex").val();
        $("#dis_product").html("");
        $("#dis_product").kendoGrid({
            // height: 400,
            columns: [
                { template: '<a href="javascript:ShowFreeProductfirmation(#=ProductId#)">Delete</a>',
                    width: "50px"
                },
            { template: '<a href="javascript:EditFreeProduct(#=ProductId#,#=Qty#,\'#=ProductName#\',#=ProductPackingId#,\'#=ProductPackingName#\')">#=ProductName#</a>',
                field: "ProductName", width: "200px", title: "SKU"
            },
            //{field: "ProductName", title: "SKU", width: "85px", hidden: false },
            {field: "ProductPackingName", title: "Packing Method", width: "98px", hidden: false },
            {field: "Qty", title: "Qty", width: "120px", hidden: false },
            { field: "ProductCatagoryName", title: "Item Group", width: "105px", hidden: true },
            { field: "BrandName", title: "Brand", width: "60px", hidden: true },
            { field: "CatlogName", title: "Flavor", width: "130px", hidden: true },
            
            { field: "ItemTypeName", title: "HVP", width: "250px", hidden: true }
        ],
            editable: false, // enable editing
            pageable: false,
            sortable: false,
            filterable: false,
            selectable: "single",
            columnMenu: false,
            //dataBound: LeadCustomerGrid_onDataBound,
            dataSource: {
                serverSorting: true,
                serverPaging: true,
                serverFiltering: true,
                pageSize: 20,
                schema: {
                    data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                    total: "d.Total",
                    model: { // define the model of the data source. Required for validation and property types.
                        id: "SourceId",
                        fields: {
                            //                                Name: { validation: { required: true} },
                            //                                LeadStage: { validation: { required: true} },
                            //                                CustomerCode: { editable: true, nullable: true },
                            //                                State: { validation: { required: true} },
                            //                                City: { validation: { required: true} },
                            //                                SourceId: { validation: { required: true} },
                            //                                Address: { validation: { required: true} },
                            //                                rowCount: { type: "number", validation: { required: true} }
                        }
                    }
                },
                transport: {

                    read: {
                        url: ROOT_PATH + "service/discount_scheme/discount_scheme_service.asmx/GetFreeProductList", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                        contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                        //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer', pgindex: take_grid },
                        type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                        complete: function (jqXHR, textStatus) {
                            //                                if (textStatus == "success") {
                            //                                    var entityGrid = $("#grid").data("kendoGrid");
                            //                                    if ((take_grid != '') && (gridindex == '0')) {
                            //                                        entityGrid.dataSource.page((take_grid - 1));
                            //                                        gridindex = '1';
                            //                                    }
                            //                                }
                        }
                    },
                    parameterMap: function (data, operation) {
                        if (operation != "read") {
                            return JSON.stringify({ products: data.models })
                        } else {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }//,
                //aggregate: [{ field: "rowCount", aggregate: "max"}]
            }
        });
    }

    
    function LoadPopUp() {

        //$("#save").css("display", "none");
        $("#btnNo").css("display", "inline-block");
        $("#btnYes").css("display", "inline-block");

        wnd1 = $("#div_free_product_form").kendoWindow({
            title: "Add Free Product",
            modal: true,
            visible: false,
            resizable: false,
            width: 400
        }).data("kendoWindow");

        //$("#div_confirm_message").text(message);
        wnd1.center().close();

        $("#btnYes").click(function () {
            AddFreProductToSession();
            wnd1.close();
        });

        $("#btnNo").click(function () {
            wnd1.close();
        });

//        wndRemove = $("#schememodalWindow").kendoWindow({
//                title: msgTitle,
//                modal: true,
//                visible: false,
//                resizable: false,
//                width: 400
//            }).data("kendoWindow");
    }

    


    function loadFreeProductList() {
        $("#Product_subwindow").html("");
        $("#Product_subwindow").kendoGrid({
            height: 300,
            columns: [
                    { field: "ProductId", title: "ProductId", hidden: true, width: "130px" },
                    { field: "Code", title: "Product Code", hidden: false, width: "130px" },
                    { template: '<a href="javascript:OpenSelectFreeProduct(#=ProductId#,\'#=Name#\',#=CategoryId#,#=FlavourId#,#=PackingId#,#=BrandId#,#=IsHighValue#)">#=Name#</a>',
                        field: "Product Name"
                    }

                ],
            editable: false, // enable editing
            pageable: true,
            sortable: true,
            //filterable: true,
            selectable: "single",
            dataSource: {
                serverSorting: true,
                serverPaging: true,
                serverFiltering: true,
                pageSize: 10,
                schema: {
                    data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                    total: "d.Total",
                    model: { // define the model of the data source. Required for validation and property types.
                        id: "SourceId",
                        fields: {
                            ProgramId: { validation: { required: true} },
                            ProgramName: { validation: { required: true} },
                            Status: { validation: { required: true} }
                        }
                    }
                },
                transport: {

                    read: {
                        url: ROOT_PATH + "service/load_stock/load_stock.asmx/GetAllProductsDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                        contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                        data: { pgindex: 0 }, //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                        type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                        complete: function (jqXHR, textStatus) {
                            if (textStatus == "success") {
                                //$("#program_window").data("kendoWindow").open();

                                var newcust_window = $("#program_window"),
                                    newcust_undo = $("#undo")
                                .bind("click", function () {
                                    newcust_window.data("kendoWindow").open();
                                    newcust_undo.hide();
                                });

                                var onClose = function () {
                                    newcust_undo.show();
                                }

                                if (!newcust_window.data("kendoWindow")) {
                                    newcust_window.kendoWindow({
                                        width: "500px",
                                        height: "400px",
                                        title: "Lookup",
                                        close: onClose
                                    });
                                }

                                newcust_window.data("kendoWindow").center().open();

                                $("div#div_loader").hide();
                            }
                        }
                    },
                    parameterMap: function (data, operation) {
                        if (operation != "read") {
                            return JSON.stringify({ products: data.models })
                        } else {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }
            }
        });
    }

    
    function RemoveFreeProductToSession(ProductId) {

        var root = ROOT_PATH + "discount_scheme/process_forms/processmaster.aspx?fm=removefreeproduct&type=query&id=" + ProductId;

        $.ajax({
            url: root,
            dataType: "html",
            cache: false,
            success: function (msg) {
                if (msg == "1") {
                    var grid = $("#dis_product").data("kendoGrid");
                    grid.dataSource.read();
                }
            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });

    }

    function AddFreProductToSession() {

        var root = ROOT_PATH + "discount_scheme/process_forms/processmaster.aspx?fm=addfreeproduct&type=query&id=" + $("#HiddenField_ProductId").val()
                         + "&qty=" + $("#txtQty").val() + "&name=" + $("#txtProduct").val() + "&ptypeid=" + $("#cmdPackingType1").val()
                         + "&ptype=" + $("#cmdPackingType1 option:selected").text() ;

        $.ajax({
            url: root,
            dataType: "html",
            cache: false,
            success: function (msg) {
                if (msg == "1") {
                    var grid = $("#dis_product").data("kendoGrid");
                    grid.dataSource.read();
                }
            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });

    }

    
    function EditFreeProduct(ProductId, Qty, Product,ProductPackingId,ProductPackingName) {
        $("#HiddenField_ProductId").val(ProductId);
        $("#txtQty").val(Qty);
        $("#txtProduct").val(Product);
        //LoadPopUp();
        wnd1.center().open();
    }

    function AddFreeProduct() {
        $("#HiddenField_ProductId").val(0);
        $("#txtQty").val(0);
        $("#txtProduct").val('');
        //LoadPopUp();
        wnd1.center().open();
    }



    
function get_divisions(targeturl) {
    $("#div_loader").show();
    var url = ROOT_PATH + targeturl;
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            $("#div_loader").hide();
            if (msg != "") {
                var wnd = $("#modalWindowDivisions").kendoWindow({
                    title: "Division List",
                    modal: true,
                    visible: false,
                    resizable: false,
                    width: 300
                }).data("kendoWindow");

                $("#modalWindowDivisions").html(msg);
                wnd.center().open();

                $("#jqi_state0_buttonOk").click(function () {
               
                    var divisionName = '';
                    var divisionId = '';
                    $('#divisionTable tr').each(function () {
                        if ($(this).children(':eq(0)').find('#a').is(':checked')) {

                            //var emailaddress = $(this).children(':eq(0)').find('#a').val();
                            //selected_email = selected_email + emailaddress;

                            divisionName = $(this).children(':eq(0)').find('#a').val();
                            divisionId = $(this).children(':eq(0)').find('#b').val();
                        }
                    });

                    $('#MainContent_txtSelectedDivisionName').val(divisionName);
                    $('#HiddenFieldSelectedDivisionId').val(divisionId);
                    load_division_product_grid(divisionId,1);
                    //originator = originatorId;
                    wnd.close();
                    $("#div_address").show();
                    $("#MainContent_buttonbar_buttinSave").prop('disabled', false);
                    $("#MainContent_buttonbar_buttonDeactivate").prop('disabled', false);
                });
            }
        },
        // error: function (XMLHttpRequest, textStatus, errorThrown) {
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
                $("#div_loader").hide();
                //$("#" + tagertdiv).html(msg);
            }
        }
    });
}



function load_division_product_grid(divisionId,status) {
    $("#loadProductAssignGrid").html("");
    $("#loadProductAssignGrid").kendoGrid({
        height: 300,
        columns: [
        { template: '<a href="javascript:DeleteProductDivisionToSession(#=DivisionId#, #=ProductId#);">Delete</a>',
            field: "", width: "100px"
        },
        { field: "DivisionId", title: "Division Id", width: "60px", hidden: true },
        { field: "ProductId", title: "Product Id", width: "60px", hidden: true },
        { field: "Name", title: "Product Name" },
        { field: "FgCode", title: "Product Code" }
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 1000,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    fields: {
                        MarketId: { validation: { required: true} },
                        MarketName: { validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/discount_scheme/discount_scheme_service.asmx/GetProductsByDivisionId", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { divisionId: divisionId ,status : status},
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function AddProductDivisionToSession(pid,pname) {
    //$("#modalWindowUnassignMarkets").data("kendoWindow").close();
        var root = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=addproductdivision&type=query&pid=" + pid
                         + "&pname=" + pname + "&dname=" + $("#MainContent_txtSelectedDivisionName").val() 
                         + "&did=" + $("#HiddenFieldSelectedDivisionId").val() ;

        $.ajax({
            url: root,
            dataType: "html",
            cache: false,
            success: function (msg) {
                if (msg == "true") {
                    
                    var msg = GetSuccesfullMessageDiv("Successfully Saved!", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(msg);
                    hideStatusDiv("MainContent_div_message");

                    load_division_product_grid($("#HiddenFieldSelectedDivisionId").val(),0);
                }
                else{
                    var errorMsg = GetErrorMessageDiv("Product Exists.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(errorMsg);
                    hideStatusDiv("MainContent_div_message");
                }
            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });

    }

function DeleteProductDivisionToSession(did, pid) {
        
        //$("#save").css("display", "none");
            $("#noConfirm").css("display", "inline-block");
            $("#yesConfirm").css("display", "inline-block");

            var message = "Are you sure, you want to delete the selected Product ?";

            var wnd = $("#confirmmodalWindow").kendoWindow({
                title: "Confirm Route Delete",
                modal: true,
                visible: false,
                resizable: false,
                width: 400
            }).data("kendoWindow");
            $("#MainContent_div_message").css("display", "none");
            $("#div_Confirm_message").text(message);
            wnd.center().open();

//            $("#yesConfirm").click(function () {
//                wnd.close();
//                
//                var root = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=deleteproductdivision&type=query&pid=" + pid + "&did=" + did;
//                $.ajax({
//                    url: root,
//                    dataType: "html",
//                    cache: false,
//                    success: function (msg) {
//                        if (msg == "y") {
////                            load_division_product_grid($("#HiddenFieldSelectedDivisionId").val(),0); 
//                        }
//                    },
//                    // error: function (XMLHttpRequest, textStatus, errorThrown) {
//                    error: function (msg, textStatus) {
//                        if (textStatus == "error") {
//                            var msg = msg;
//                        }
//                    }
//                });
//            });

            $("#yesConfirm").click(function () {
                 wnd.close();
                 showPopup(pid ,did,0);  
                             
            });

            $("#noConfirm").click(function () {
                wnd.close();
            });
    }

    function delete_selected_DivisionProduct(pid,did){
        var root = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=deleteproductdivision&type=query&pid=" + pid + "&did=" + did;
                $.ajax({
                    url: root,
                    dataType: "html",
                    cache: false,
                    success: function (msg) {
                        if (msg == "y") {
                            var msg = GetSuccesfullMessageDiv("Successfully Deleted !", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(msg);
                            hideStatusDiv("MainContent_div_message");
                            load_division_product_grid($("#HiddenFieldSelectedDivisionId").val(),0);                         
                                                         
                        }
                    },
                    // error: function (XMLHttpRequest, textStatus, errorThrown) {
                    error: function (msg, textStatus) {
                        if (textStatus == "error") {
                            var msg = msg;
                            var msg = GetErrorMessageDiv("Error Occured !", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(msg);
                            hideStatusDiv("MainContent_div_message");
                        }
                    }
                });    
    }




function AllProductLookupList(div_name) {
        $("#"+div_name).html("");
        $("#"+div_name).kendoGrid({
            height: 400,
            columns: [
                    { field: "ProductId", title: "ProductId", hidden: true, width: "130px" },
                    { template: '#=Name#', field: "Product Name" , width: "230px"},
                    { template: '#=FgCode#',field: "Product Code" , width: "100px"},
                    { title: "", width: "50px", template: '<button id="addproduct" onclick="AddProductDivisionToSession(#=ProductId#,\'#=Name#\')">Add</button>' }
                ],
            editable: false, // enable editing
            pageable: true,
            sortable: true,
            filterable: true,
            selectable: "single",
            dataSource: {
                serverSorting: true,
                serverPaging: true,
                serverFiltering: true,
                pageSize: 50,
                schema: {
                    data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                    total: "d.Total",
                    model: { // define the model of the data source. Required for validation and property types.
                        id: "SourceId",
                        fields: {
                            ProgramId: { validation: { required: true} },
                            ProgramName: { validation: { required: true} },
                            Status: { validation: { required: true} }
                        }
                    }
                },
                transport: {

                    read: {
                        url: ROOT_PATH + "service/load_stock/load_stock.asmx/GetAllProductsDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                        contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                        data: { pgindex: 0 }, //data: { activeInactiveChecked: activeInactiveChecked, reqSentIsChecked: reqSentIsChecked, repType: repType, gridName: 'LeadAndCustomer' },
                        type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                        complete: function (jqXHR, textStatus) {
                            if (textStatus == "success") {
                                //$("#program_window").data("kendoWindow").open();

                                var newcust_window = $("#"+div_name),
                                    newcust_undo = $("#undo")
                                .bind("click", function () {
                                    newcust_window.data("kendoWindow").open();
                                    newcust_undo.hide();
                                });

                                var onClose = function () {
                                    newcust_undo.show();
                                }

                                if (!newcust_window.data("kendoWindow")) {
                                    newcust_window.kendoWindow({
                                        width: "800px",
                                        height: "400px",
                                        title: "Products",
                                        close: onClose
                                    });
                                }

                                newcust_window.data("kendoWindow").center().open();

                                $("div#div_loader").hide();
                            }
                        }
                    },
                    parameterMap: function (data, operation) {
                        if (operation != "read") {
                            return JSON.stringify({ products: data.models })
                        } else {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }
            }
        });
    }

    function PrintDiscountScheme(id) {

        var root = ROOT_PATH + "discount_scheme/process_forms/processmaster.aspx?fm=printdiscountscheme&type=query&id=" + id;

        $.ajax({
            url: root,
            dataType: "html",
            cache: false,
            success: function (msg) {
                //loadpdf(msg);
                $("#a_show_pdf").attr('href',msg);
                

document.getElementById('a_show_pdf').click();


//                if (msg == "1") {
//                    var grid = $("#dis_product").data("kendoGrid");
//                    grid.dataSource.read();
//                }
            },
            // error: function (XMLHttpRequest, textStatus, errorThrown) {
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });

    }

    function loadRouteMasterGridTme() {
    var url = "javascript:updateRouteMaster('#=RouteMasterId#','#=RouteName#');"
    var showOutlets = "javascript:ShowOutletsWindowForTme('#=RouteMasterId#','#=RouteName#');";

    var deleteUrl = "javascript:DeleteRouteMaster('#=RouteMasterId#');"

    var take_grid = $("#MainContent_hfPageIndexRouteMasterGridTme").val();
    url = "<a href=\"" + url + "\">#=RouteName#</a>";
    //$("#div_loader").show();
    $("#RouteMasterGridTme").html("");
    $("#RouteMasterGridTme").kendoGrid({
        height: 500,
        columns: [
    { template: "<a href=\"" + deleteUrl + "\">Delete</a>", width: "50px" },
    { field: "RouteMasterId", title: "Route Id", width: "85px", hidden: true },
    { template: "<a href=\"" + showOutlets + "\">Add Outlets</a>", field: "", width: "75px", hidden: false },
    { template: url, field: "RouteName", title: "Route Name", width: "550px" },
    { field: "RepName", title: "Assigned To", width: "300px", hidden: false },  
    { field: "CallCycleId", title: "CallcycleId", width: "50px", hidden: true },
    { field: "Status", title: "Status", width: "105px", hidden: true }
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        detailTemplate: kendo.template($("#template").html()),
        detailInit: detailInit_callcycleOutletsInRoute,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 15,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        RouteMasterId: { validation: { required: true} },
                        RouteName: { editable: true, nullable: true },
                        CallCycleId: { editable: true, nullable: true },
                        Status: { validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/call_cycle/call_cycle_service.asmx/GetAllRouteMaster", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#RouteMasterGridTme").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

   function loadRouteMasterGridForRep(repCode) {
            //var take_grid = $("#MainContent_hfPageIndexRouteMasterGridRep").val();
            var take_grid = "1";

            $("#div_chkroutegrid").html("");
            $("#div_chkroutegrid").kendoGrid({
                height: 500,
                columns: [
   { text: "HasSelect", title: "",  headerTemplate: '<input id="checkAllroutes" type="checkbox" class="myCheckbox" />', width: "15px", template: "<input id=\"check_datarow_routes\" alt=\"#= uid# \" type=\"checkbox\"   #= HasSelect ? checked='checked' : '' #  class=\"check_row_route\"/>" },
            { field: "RouteMasterId", title: "Route Id", width: "85px", hidden: true },
            { field: "RouteName", title: "Route Name", width: "400px" },
            { field: "RepCode", title: "Rep Code", width: "85px", hidden: true },
            { field: "RepName", title: "Assigned To", width: "300px", hidden: true },   
            { field: "CallCycleId", title: "CallcycleId", width: "50px", hidden: true },         
            { field: "Status", title: "Status", width: "105px", hidden: true }
                ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                filterable: true,
                selectable: "single",
                columnMenu: false,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 100000,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            //id: "SourceId",
                            fields: {
                                                    HasSelect: { type: "boolean"},
                                RouteMasterId: { validation: { required: true} },
                                RouteName: { editable: true, nullable: true },
                                RepCode: { editable: true, nullable: true },
                                RepName: { editable: true, nullable: true },
                                CallCycleId: { editable: true, nullable: true },
                                Status: { validation: { required: true} }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/call_cycle/call_cycle_service.asmx/GetAllRouteMasterForRep", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { pgindex: take_grid, repCode: repCode },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {
                                    var newcust_window = $("#div_chkroutegrid"),
                                        newcust_undo = $("#undo")
                                    .bind("click", function () {
                                        newcust_window.data("kendoWindow").open();
                                        newcust_undo.hide();
                                    });

                                    var onClose = function () {
                                        newcust_undo.show();
                                    }

                                    if (!newcust_window.data("kendoWindow")) {
                                        newcust_window.kendoWindow({
                                            width: "900px",
                                            height: "350px",
                                            title: "Routes",
                                            close: onClose
                                        });
                                    }

                                    newcust_window.data("kendoWindow").center().open();

                                    $("div#div_loader").hide();
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }
            });
        }

    function loadRouteMasterGridForCheckList(checklistid) {
            //var take_grid = $("#MainContent_hfPageIndexRouteMasterGridRep").val();
            var take_grid = "1";

            $("#div_chkroutegrid").html("");
            $("#div_chkroutegrid").kendoGrid({
                height: 550,
                columns: [//<input id="checkAllroutes" type="checkbox" class="myCheckbox" />
                            { text: "HasSelect", title: "",  headerTemplate: '', width: "30px", template: "<input id=\"check_datarow_routes\" alt=\"#= uid# \" type=\"checkbox\"   #= HasSelect ? checked='checked' : '' #  class=\"check_row_route\"/>" },
                            { field: "RouteMasterId", title: "Route Id", width: "85px", hidden: true },
                            { field: "RouteName", title: "Route Name", width: "400px" },
                            { field: "RepCode", title: "Rep Code", width: "120px" },
                            { field: "RepName", title: "Rep Name", width: "300px" },   
                            { field: "CallCycleId", title: "CallcycleId", width: "50px", hidden: true },         
                            { field: "Status", title: "Status", width: "105px", hidden: true }
                        ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                filterable: true,
                selectable: "single",
                columnMenu: false,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 100000,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            //id: "SourceId",
                            fields: {
                                HasSelect: { type: "boolean"},
                                RouteMasterId: { validation: { required: true} },
                                RouteName: { editable: true, nullable: true },
                                RepCode: { editable: true, nullable: true },
                                RepName: { editable: true, nullable: true },
                                CallCycleId: { editable: true, nullable: true },
                                Status: { validation: { required: true} }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/call_cycle/call_cycle_service.asmx/GetAllRouteMasterForCheckList", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { CheckListId: checklistid, pgindex: take_grid },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {
                                    var newcust_window = $("#div_chkroutegrid"),
                                        newcust_undo = $("#undo")
                                    .bind("click", function () {
                                        newcust_window.data("kendoWindow").open();
                                        newcust_undo.hide();
                                    });

                                    var onClose = function () {
                                        newcust_undo.show();
                                    }

                                    if (!newcust_window.data("kendoWindow")) {
                                        newcust_window.kendoWindow({
                                            width: "900px",
                                            height: "350px",
                                            title: "Routes",
                                            close: onClose
                                        });
                                    }

                                    newcust_window.data("kendoWindow").center().open();

                                    $("div#div_loader").hide();
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }
            });
        }

    function loadSalesReps() {
            var take_grid = "1";

            $("#div_chkroutegrid").html("");
            $("#div_chkroutegrid").kendoGrid({
                height: 500,
                columns: [
                    { text: "HasSelect", title: "",  headerTemplate: '<input id="checkAllreps" type="checkbox" class="myCheckbox" />', width: "15px", template: "<input id=\"check_datarow_reps\" alt=\"#= uid# \" type=\"checkbox\"   #= HasSelect ? checked='checked' : '' #  class=\"check_row_reps\"/>" },
                    { field: "RepCode", title: "Rep Code", width: "85px" },
                    { field: "RepName", title: "Rep Name", width: "400px" }
                ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                filterable: true,
                selectable: "single",
                columnMenu: false,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 100000,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            //id: "SourceId",
                            fields: {
                                HasSelect: { type: "boolean"},
                                Originator: { editable: true, nullable: true },
                                RepName: { editable: true, nullable: true }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/lead_customer/common.asmx/GetAllRepSales", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { pgindex: take_grid },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {

                                    $("div#div_loader").hide();
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }
            });
        }


function detailInit_callcycleOutletsInRoute(e) {
    //var take_grid = $("#MainContent_hfPageIndexTemplateGrid").val();
    var detailRow = e.detailRow;
    detailRow.find(".tabstrip").kendoTabStrip({
        animation: {
            open: { effects: "fadeIn" }
        }
    });

    var deleteCC = "javascript:DeleteCustomerFromRoute(#=Contact.CustomerCode#," + e.data.RouteMasterId + ");";

    detailRow.find(".DivCallCycleOutletsInRoute").kendoGrid({
        columns: [//DeleteCallCycle
        //{template: "<a href=\"" + deleteCC + "\">Delete</a>", field: "", width: "50px", hidden: false },
        //                    {template: '<a href="javascript:DeleteCallCycle(#=Contact.SourceId#,\'#=Contact.CustomerCode#\',\'#=Contact.EndUserCode#\',#=CreateActivity#,#=CreateActivity#)">Delete</a>',
        //                    width: "50px"
        //                    },
            { field: "Contact.SourceId", title: "LeadID", hidden: true },
            { field: "Contact.CustomerCode", title: "Customer Code", hidden: true },
            { field: "Contact.EndUserCode", title: "EndUser Code", hidden: true },
            { field: "Contact.Name", title: "Name", width: "200px" },
            { field: "LeadStage.StageName", title: "Lead Stage", width: "130px", hidden: true },
            { field: "Contact.Address", title: "Address", width: "300px" },
//            { field: "Contact.City", title: "Town", width: "110px" },
            { field: "Contact.State", title: "State", width: "70px", hidden: true },
            { field: "Contact.PostalCode", title: "Postal Code", width: "70px", hidden: true },
            { template: "<a href=\"" + deleteCC + "\">Delete</a>", width: "200px" }
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: false,
        selectable: "single",
        //dataBound: TankIssue_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "SourceId",
                    fields: {
                        CallCycleID: { validation: { required: true} },
                        Comments: { validation: { required: true} },
                        Description: { validation: { required: true} },
                        DueOn: { type: "Date", validation: { required: true} }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/call_cycle/call_cycle_service.asmx/GetCallCycleContactsForSelectedRoute", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { callCycleID: e.data.CallCycleId, routeMasterId: e.data.RouteMasterId, pgindex: 0 },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

/*Delete market Click in Market Assign Page*/
    function DeleteCustomerFromRoute(customerId, routeId) {
        $("#repmodalWindow").show()
        showDeleteCustomerFromRoute(customerId, routeId);
    }

    /*Show Delete market Confirmation in Market Assign Page*/
    function showDeleteCustomerFromRoute(customerId, routeId) {
        $("#save").css("display", "none");
        $("#no").css("display", "inline-block");
        $("#yes").css("display", "inline-block");

        var message = "Do you want to delete this customer?";

        var wnd = $("#repmodalWindow").kendoWindow({
            title: "Delete Customer",
            modal: true,
            visible: false,
            resizable: false,
            width: 400
        }).data("kendoWindow");

        $("#div_repconfirm_message").text(message);
        wnd.center().open();

        $("#yes").click(function () {
            delete_selected_route_customer('lead_customer/process_forms/processmaster.aspx?fm=deleteroutecustomer&type=Delete&cusno=' + customerId + '&routeid=' + routeId);
            wnd.close();
            
            var sucessMsg = GetSuccesfullMessageDiv("Successfully Deleted.", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(sucessMsg);

                //hideStatusDiv("MainContent_div_outlets");
                $("#MainContent_div_outlets").css("display", "none");
                $("#MainContent_div_outlets_tme").css("display", "none");
                $("#MainContent_div_routes_tme").css("display", "block");
                $("#id_div_toolbar_container").css("display", "block");

                $("div#div_loader").hide();
                hideStatusDiv("MainContent_div_message");
        });

        $("#no").click(function () {
            wnd.close();

            var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);

                //hideStatusDiv("MainContent_div_outlets");
                $("#MainContent_div_outlets").css("display", "none");
                $("#MainContent_div_outlets_tme").css("display", "none");
                $("#MainContent_div_routes_tme").css("display", "block");
                $("#id_div_toolbar_container").css("display", "block");

                $("div#div_loader").hide();
                hideStatusDiv("MainContent_div_message");
        });
    }

/*Delete market Click in Market Assign Page*/
    function DeleteAddCustomerRoute(customerId) {
        $("#repmodalWindow").show()
        showDeleteAddCustomerRoute(customerId);
    }

    /*Show Delete market Confirmation in Market Assign Page*/
    function showDeleteAddCustomerRoute(customerId) {
        $("#save").css("display", "none");
        $("#no").css("display", "inline-block");
        $("#yes").css("display", "inline-block");

        var message = "Do you want to delete this customer?";

        var wnd = $("#repmodalWindow").kendoWindow({
            title: "Delete Customer",
            modal: true,
            visible: false,
            resizable: false,
            width: 400
        }).data("kendoWindow");

        $("#div_repconfirm_message").text(message);
        wnd.center().open();

        $("#yes").click(function () {
            delete_selected_route_customer('lead_customer/process_forms/processmaster.aspx?fm=deleteaddcustomerroute&type=Delete&cusno=' + customerId);
            wnd.close();

            var sucessMsg = GetSuccesfullMessageDiv("Successfully Deleted.", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(sucessMsg);

                //hideStatusDiv("MainContent_div_outlets");
                $("#MainContent_div_outlets").css("display", "none");
                $("#MainContent_div_outlets_tme").css("display", "none");
                $("#MainContent_div_routes_tme").css("display", "block");
                $("#id_div_toolbar_container").css("display", "block");

                $("div#div_loader").hide();
                hideStatusDiv("MainContent_div_message");
        });

        $("#no").click(function () {
            wnd.close();

            var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);

                //hideStatusDiv("MainContent_div_outlets");
                $("#MainContent_div_outlets").css("display", "none");
                $("#MainContent_div_outlets_tme").css("display", "none");
                $("#MainContent_div_routes_tme").css("display", "block");
                $("#id_div_toolbar_container").css("display", "block");

                $("div#div_loader").hide();
                hideStatusDiv("MainContent_div_message");
        });
    }

    /*Delete market in Product Category Assign Page*/
function delete_selected_route_customer(targeturl) {//kjkj
    $("#div_loader").show();
    var url = ROOT_PATH + targeturl;
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            $("#div_loader").hide();
            if (msg == "true") {
                //LoadReqSentLeadCustomerGrid();

                $("#MainContent_buttonbarNavi_hlBack").css("display", "none");
                $("#MainContent_div_outlets").css("display", "none");

                if ($("#MainContent_HiddenFieldUserType").val() == "ASE") {
                    loadRouteMasterGridTme();
                    $("#MainContent_div_routes_rep").css("display", "none");
                    $("#MainContent_div_routes_tme").css("display", "block");
                }
                else {
                    loadRouteMasterGrid();
                    $("#MainContent_div_routes_tme").css("display", "none");
                    $("#MainContent_div_routes_rep").css("display", "block");
                }
            }
        },
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
                $("#div_loader").hide();
            }
        }
    });
}

function DeleteCallCycleCustomerFromRoute(customerId, routeId) {
    //debugger;
    var url = ROOT_PATH + "call_cycle/process_forms/processmaster.aspx?fm=routemasterentry&type=Delete&id=" + routeId;
    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            //debugger;
            if (msg == "true") {
                var sucessMsg = GetSuccesfullMessageDiv("Route was successfully Deleted.", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(sucessMsg);
                if ($("#MainContent_HiddenFieldUserType").val() == "ASE") {
                    loadRouteMasterGridTme();
                    $("#MainContent_div_routes_rep").css("display", "none");
                    $("#MainContent_div_routes_tme").css("display", "block");
                }
                else {
                    loadRouteMasterGrid();
                    $("#MainContent_div_routes_tme").css("display", "none");
                    $("#MainContent_div_routes_rep").css("display", "block");
                }
                hideStatusDiv("MainContent_div_message");
            }
            else {
                var errorMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);
                hideStatusDiv("MainContent_div_message");
            }
        },
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var errorMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);
                hideStatusDiv("MainContent_div_message");
            }
        }
    });
}


function ShowOutletsWindowForTme(RouteMasterId, RouteName) {
    //hideStatusDiv("MainContent_div_routes_tme");
    $("#MainContent_div_routes_tme").css("display", "none");
    $("#MainContent_div_outlets_tme").css("display", "block");
    $("#MainContent_div_outlets").css("display", "block");

    $("#id_div_toolbar_container").css("display", "none");

    $("#MainContent_txtRouteNameTme").val("");
    $("#MainContent_txtRouteNameTme").val(RouteName);
    $("#MainContent_HiddenFieldRouteCodeTME").val(RouteMasterId);

    //Setting outlets grid
    $("#MainContent_hfSelectedDistributorUsername").val($("#MainContent_DropDownListDistributors").val());
    $("#MainContent_hfSelectedMarketId").val($("#MainContent_DropDownListMarkets").val());
    $("#MainContent_hfSelectedVolume").val($("#MainContent_DropDownListVolume").val());
    $("#MainContent_hfSelectedOutletTypeId").val($("#MainContent_DropDownListRetailType").val());
    $("#MainContent_hfSelectedTown").val($("#MainContent_DropDownListTown").val());

    SetLeadStage("Customer");
}

function DeleteRouteMasterConfirmed(routeId) {
    //debugger;
    var url = ROOT_PATH + "call_cycle/process_forms/processmaster.aspx?fm=routemasterentry&type=Delete&id=" + routeId;

    $.ajax({
        url: url,
        dataType: "html",
        cache: false,
        success: function (msg) {
            //debugger;
            if (msg == "true") {
                var sucessMsg = GetSuccesfullMessageDiv("Route was successfully Deleted.", "MainContent_div_message");

                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(sucessMsg);
                if ($("#MainContent_HiddenFieldUserType").val() == "ASE") {
                    loadRouteMasterGridTme();
                    $("#MainContent_div_routes_rep").css("display", "none");
                    $("#MainContent_div_routes_tme").css("display", "block");
                }
                else {
                    loadRouteMasterGrid();
                    $("#MainContent_div_routes_tme").css("display", "none");
                    $("#MainContent_div_routes_rep").css("display", "block");
                }
                hideStatusDiv("MainContent_div_message");
            }
            else {
                var errorMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);
                hideStatusDiv("MainContent_div_message");
            }
        },
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var errorMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);
                hideStatusDiv("MainContent_div_message");
            }
        }
    });
}


function SaveRouteAssignAndOutletsForTme() {
    $("div#div_loader").show();
    var routeMasterId = $("#MainContent_HiddenFieldRouteCodeTME").val();
    var routeName = $("#MainContent_txtRouteNameTme").val();

    //new route object
    var routeDTO = new Object();
    routeDTO.RouteMasterId = routeMasterId;
    routeDTO.RouteName = routeName;

    //debugger;
    //alert("sdf");
    //new originator object
    var originator = new Object();

    //new list to hold customer list
    var callCycleContactDTOList = GetSelectedOutletsList();
    //debugger;
    //new OriginatorRouteOutletsDTO object
    var originatorRouteOutletsDTO = new Object();

    originatorRouteOutletsDTO.Outlets = callCycleContactDTOList;
    originatorRouteOutletsDTO.Route = routeDTO;
    originatorRouteOutletsDTO.Originator = originator;

    var param = { 'originatorRouteOutletsDTO': originatorRouteOutletsDTO };
    $.ajax({
        type: "POST",
        url: ROOT_PATH + "service/call_cycle/call_cycle_service.asmx/SaveRouteAssignAndOutlets",
        //data: JSON.stringify(param),
        data: JSON.stringify(param),
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            if (response.hasOwnProperty('d')) {
                msg = response.d;
            } else {
                msg = response;
            }
            alert(msg);
            if (msg == "Deleted") {
                var sucessMsg = GetSuccesfullMessageDiv("Successfully Deleted.", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(sucessMsg);

                //hideStatusDiv("MainContent_div_outlets");
                $("#MainContent_div_outlets").css("display", "none");
                $("#MainContent_div_outlets_tme").css("display", "none");
                $("#MainContent_div_routes_tme").css("display", "block");
                $("#id_div_toolbar_container").css("display", "block");

                $("div#div_loader").hide();
                loadRouteMasterGridTme();
                hideStatusDiv("MainContent_div_message");
            }
            else {
                var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);

                //hideStatusDiv("MainContent_div_outlets");
                $("#MainContent_div_outlets").css("display", "none");
                $("#MainContent_div_outlets_tme").css("display", "none");
                $("#MainContent_div_routes_tme").css("display", "block");
                $("#id_div_toolbar_container").css("display", "block");

                $("div#div_loader").hide();
                loadRouteMasterGridTme();
                hideStatusDiv("MainContent_div_message");
            }
        },
        error: function (response) {
            //alert("Oops, something went horribly wrong");
            //debugger;
        }
    });
}

//To get Selected Customers in Route Entry Page
function GetSelectedOutletsList() {
    //debugger;
    var callCycleContactDTOList = [];
    
    var idstr = '';
    var items = $("#div_custgrid").data("kendoGrid").dataSource.data();

    var callCycleContactDTO = new Object();
    var leadCustomerDTO = new Object();
    for (var i = 0; i < items.length; i++) {

        callCycleContactDTO = new Object();
        leadCustomerDTO = new Object();

        var item = items[i];
        if (i == (items.length - 1) && (item != undefined && item != null && item != '' && item.HasSelect == true)) {
            idstr += item.CustomerCode;
            leadCustomerDTO.CustomerCode = item.CustomerCode;
            callCycleContactDTO.Contact = leadCustomerDTO;
            callCycleContactDTOList.push(callCycleContactDTO);
        }
        else if (item != undefined && item != null && item != '' && item.HasSelect == true) {
            idstr += item.CustomerCode + ",";
            leadCustomerDTO.CustomerCode = item.CustomerCode;
            callCycleContactDTO.Contact = leadCustomerDTO;
            callCycleContactDTOList.push(callCycleContactDTO);
        }
    }
    //debugger; 
    return callCycleContactDTOList;
}


function ShowOutletsWindowForRep(RouteMasterId, RouteName, RepCode, RepName) {
    //hideStatusDiv("MainContent_div_routes_rep");
    $("#MainContent_div_routes_rep").css("display", "none");
    $("#MainContent_div_outlets_rep").css("display", "block");
    $("#MainContent_div_outlets").css("display", "block");

    $("#id_div_toolbar_container").css("display", "none");

    $("#MainContent_txtRouteName").val("");
    $("#MainContent_txtDRName").val("");
    $("#MainContent_chktempararychk").prop("checked", false)

    $("#div_isTempSection").css("display", "none");
    $("#MainContent_AssignDate").kendoDatePicker({
        value: new Date(),
        format: "dd-MMM-yyyy"
        // timeFormat: "HH:mm"
    });
    //debugger; ;
    $("#MainContent_txtRouteName").val(RouteName);
    $("#MainContent_HiddenFieldRouteCodeRA").val(RouteMasterId);

    if (RepName != "" && RepName != null && RepName != "null") {
        $("#MainContent_txtDRName").val(RepName);
        $("#MainContent_HiddenFieldRepCodeRA").val(RepCode);
    }

    //Setting outlets grid
    SetLeadStage("Customer");
    //$("div#div_loader").show();
}

function loadTMEItinerary()
{
   $("#div_loader").show();
    $("#ItineraryGrid").kendoGrid({
        height: 350,
        selectable: true,
        columns: [
                    { field: "ItineraryDetailId", title: "Itinerary Detail ID", width:"100px", editable: false, hidden:true },
                    { field: "ItineraryHeaderId", title: "Itinerary Header ID", width:"100px", editable: false, hidden:true },
                    { field: "DayTypeColor", title: "DayType", width:"100px", editable: false, hidden:true},
                    { field: "ActivityId", title: "Activity ID", width:"100px", editable: false, hidden:true },
                    { field: "RouteId", title: "Route ID", width:"100px", editable: false, hidden:true },
                    //{ field: "Date", width: "150px", title: "Date", format: DATE_FORMAT, value: Date(), attributes: { class: (("#=DayType#" == 'HLDY') ? "HLDY" : (("#=DayType#" == 'WKND') ? "WKND" : "")) }},
                    { field: "Date", width: "150px", title: "Date", format: DATE_FORMAT, value: Date()},
                    { field: "TownsToVisit", title: "Towns to Visit", width:"220px", editable: false },
                    { field: "Sequence", title: "Sequence", width:"80px", editable: false, hidden:true},
                    { field: "SequenceRoute", title: "Sequence", width:"80px", editable: false},
                    { field: "DirectMiles", title: "Direct Miles" , width:"100px", format: NUMBER_FORMAT,editable: true, editor: DirectMilesEditor    },
                    { field: "LocalMiles", title: "Local Miles" , width:"100px", format: NUMBER_FORMAT,editable: true, editor: LocalMilesEditor  },
                    { field: "TotalMiles", title: "Total Miles" , width:"100px", format: NUMBER_FORMAT,editable: false},
                    { field: "WorkProgram", title: "Work Program", width:"100px", editable: true },
                    { field: "NightAt", title: "Night At", width:"100px", editable: true },
                    { field: "CreatedBy", title: "Created By", width:"100px", editable: false, hidden: true  },
                    { field: "CreatedDate", width: "150px", title: "CreatedDate", value: Date(), format: DATE_FORMAT, hidden: true },
                    { field: "LastModifiedDate", width: "150px", title: "LastModifiedDate", value: Date(), format: DATE_FORMAT, hidden: true }

                ], 
        dataBound: function () {
            dataView = this.dataSource.view();
            for (var i = 0; i < dataView.length; i++) {
                if (dataView[i].DayTypeColor!=null && dataView[i].DayTypeColor!="") {
                    var uid = dataView[i].uid;
                    //$("#ItineraryGrid tbody").find("tr[data-uid=" + uid + "]").removeClass("k-alt").addClass(dataView[i].DayType);
                    //$("#ItineraryGrid tbody").find("tr[data-uid=" + uid + "]").removeClass("k-alt").css("background-color", dataView[i].DayTypeColor);
                    $("#ItineraryGrid tbody").find("tr[data-uid=" + uid + "]").css("background-color", dataView[i].DayTypeColor);
                }                    
            }
        },              
        editable: true,        
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 200,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        ItineraryDetailId : { type: "number", validation: { required: true} },
                        ItineraryHeaderId : { type: "number", validation: { required: true} },
                        DayTypeColor : { validation: { required: false} , editable: false},
                        ActivityId : { type: "number", validation: { required: true} },
                        RouteId  : { type: "number", validation: { required: true} },
                        Month : {  validation: { required: true}, editable: false },
                        Date : { type: "date", validation: { required: true}, editable: false },
                        TownsToVisit : {  validation: { required: true} ,  editable: false},
                        Sequence : { type: "number" , editable: false},
                        SequenceRoute : { editable: false},
                        DirectMiles : { type: "number", validation: { required: true} },
                        LocalMiles : { type: "number", validation: { required: true} },
                        TotalMiles : { type: "number", validation: { required: true} ,  editable: false},
                    //    WorkProgram : {  validation: { required: true} },
                     //   NightAt : {  validation: { required: true} },
                        CreatedDate: { type: "date", validation: { required: true} },
                        LastModifiedDate: { type: "date", validation: { required: true} },
                        CreatedBy: { validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "service/call_cycle/call_cycle_service.asmx/GetTMEItinerary", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                 data: { },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            //loadRouteTargets(distId,startdate,enddate);
                         //   loadRouteTargets(distId,selectedMonth);
                            $("#div_loader").hide();
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }

        }
    });


}

function loadNotInvoicedCustomerGrid() {

    var take_grid=$("#MainContent_hfPageIndex").val();
    $("#grid").html("");
    $("#grid").kendoGrid({
       // height: 400,
        columns: [
            //{ title: "", headerTemplate: '<input type="checkbox" class="myCheckbox" onchange="CheckLeadCustomeHeader(this)" />', width: "30px", template: '<input type="checkbox" onchange="CheckLeadCustomer(\'#=LeadStage#\',\'#=CustomerCode#\',#=SourceId#,this)" #=SelectedToCall#/>' },
            { text: "HasSelect", title: "", headerTemplate: '<input id="checkAllcustomers" type="checkbox" class="myCheckbox" />', width: "40px", template: "<input id=\"check_datarow_customers\" alt=\"#= uid# \" type=\"checkbox\" #= HasSelect ? checked='checked' : '' # class=\"check_row\"/>" },
            //{ title: "", width: "30px", template: '<input type="checkbox" onchange="CheckLeadCustomer(\'#=LeadStage#\',\'#=CustomerCode#\',#=SourceId#,this)" #=SelectedToCall#/>' },
            { template: '<a href="javascript:DeleteCustomer(\'#=CustomerCode#\',\'#=SourceId#\');">Delete</a>', field: "", width: "60px" },
            { template: '<a href="javascript:OpenLeadCustomer(\'#=LeadStage#\',\'#=CustomerCode#\',\'#=SourceId#\')">#=Name#</a>',
                field: "Name", width: "220px" , footerTemplate: "<div id=\"div_rowCount_sum\" ></div>"
            },
            { field: "LeadStage", title: "Lead Stage", width: "85px", hidden: true },
            { field: "CustomerCode", title: "Cust Code", width: "105px" },
            { field: "State", title: "State", width: "60px", hidden: true },
            { field: "City", title: "City", width: "130px", hidden: true },
            { field: "SourceId", title: "CRM Ref. No.", width: "98px", hidden: true },
            { field: "Address", title: "Address", width: "230px", template: '<div style="height: 20px; ">#=Address#</div>' },
            { field: "DistributorCode", title: "Dis Code", width: "105px" },
            { field: "DistributorName", title: "Dis Name", width: "105px" },
            { field: "rowCount", title: "rowCount", format: "{0:n2}", hidden: true, footerTemplate: "<div id=\"div_rowCount_max\" > #: max # </div>" }
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: true,
        dataBound: LeadCustomerGrid_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "SourceId",
                    fields: {
                        Name: { validation: { required: true} },
                        LeadStage: { validation: { required: true} },
                        CustomerCode: { editable: true, nullable: true },
                        State: { validation: { required: true} },
                        City: { validation: { required: true} },
                        SourceId: { validation: { required: true} },
                        Address: { validation: { required: true} },
                        DistributorCode: { validation: { required: true} },
                        DistributorName: { validation: { required: true} },
                        rowCount: { type: "number", validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH+ "service/lead_customer/lead_contacts_Service.asmx/GetIdleCustomers", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { selectType : 'notInvoiced', gridName: 'LeadAndCustomer', pgindex:take_grid, date : '2016/01/01' },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            var entityGrid = $("#grid").data("kendoGrid");
                            if((take_grid!='') && (gridindex =='0')){
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex ='1';
                            }
                            }
                        }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            aggregate: [ { field: "rowCount", aggregate: "max" } ]
        }
    });
}

function loadIdleCustomerGrid() {
    var take_grid=$("#MainContent_hfPageIndex").val();
    $("#grid").html("");

    $("#grid").kendoGrid({
        // height: 400,
        columns: [
            //{ title: "", headerTemplate: '<input type="checkbox" class="myCheckbox" onchange="CheckLeadCustomeHeader(this)" />', width: "30px", template: '<input type="checkbox" onchange="CheckLeadCustomer(\'#=LeadStage#\',\'#=CustomerCode#\',#=SourceId#,this)" #=SelectedToCall#/>' },
            { text: "HasSelect", title: "", headerTemplate: '<input id="checkAllcustomers" type="checkbox" class="myCheckbox" />', width: "40px", template: "<input id=\"check_datarow_customers\" alt=\"#= uid# \" type=\"checkbox\" #= HasSelect ? checked='checked' : '' # class=\"check_row\"/>" },
            //{ title: "", width: "30px", template: '<input type="checkbox" onchange="CheckLeadCustomer(\'#=LeadStage#\',\'#=CustomerCode#\',#=SourceId#,this)" #=SelectedToCall#/>' },
            { template: '<a href="javascript:DeleteCustomer(\'#=CustomerCode#\',\'#=SourceId#\');">Delete</a>', field: "", width: "60px" },
            {
                template: '<a href="javascript:OpenLeadCustomer(\'#=LeadStage#\',\'#=CustomerCode#\',\'#=SourceId#\')">#=Name#</a>',
                field: "Name",
                width: "250px",
                footerTemplate: "<div id=\"div_rowCount_sum\" ></div>"
            },
            { field: "LeadStage", title: "Lead Stage", width: "85px", hidden: true },
            { field: "CustomerCode", title: "Cust Code", width: "105px" },
            { field: "State", title: "State", width: "60px", hidden: true },
            { field: "City", title: "City", width: "130px", hidden: true },
            { field: "SourceId", title: "CRM Ref. No.", width: "98px", hidden: true },
            { field: "Address", title: "Address", width: "250px", template: '<div style="height: 20px; ">#=Address#</div>' },
            { field: "DistributorCode", title: "Dis Code", width: "105px" },
            { field: "DistributorName", title: "Dis Name", width: "105px" },
            { field: "LastInvoiceDate", title: "Last Invoice date", width: "125px" },
            { field: "rowCount", title: "rowCount", format: "{0:n2}", hidden: true, footerTemplate: "<div id=\"div_rowCount_max\" > #: max # </div>" }
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: true,
        dataBound: LeadCustomerGrid_onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 20,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "SourceId",
                    fields: {
                        Name: { validation: { required: true} },
                        LeadStage: { validation: { required: true} },
                        CustomerCode: { editable: true, nullable: true },
                        State: { validation: { required: true} },
                        City: { validation: { required: true} },
                        SourceId: { validation: { required: true} },
                        Address: { validation: { required: true} },
                        DistributorCode: { validation: { required: true} },
                        DistributorName: { validation: { required: true} },
                        rowCount: { type: "number", validation: { required: true} }
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH+ "service/lead_customer/lead_contacts_Service.asmx/GetIdleCustomers", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: {date : selectedDate, selectType : 'idle',gridName: 'LeadAndCustomer' , pgindex:take_grid},
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            
                            var entityGrid = $("#grid").data("kendoGrid");
                            if((take_grid!='') && (gridindex =='0')){
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex ='1';
                            }
                            }
                        }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            aggregate: [ { field: "rowCount", aggregate: "max" } ]
        }
    });
}

function isNumber(evt) {
    var iKeyCode = (evt.which) ? evt.which : evt.keyCode
    if (iKeyCode != 46 && iKeyCode > 31 && (iKeyCode < 48 || iKeyCode > 57))
        return false;

    return true;
} 

//#region "Perfetti 2nd Phase"


function LoadTerritoriesByOriginator(OriginatorType, Originator) {
    Territories = [];
    var root = ROOT_PATH + "service/master/master.asmx/GetAllTerritoriesByOriginator";

    $.ajax({
        type: "POST",
        url: root,
        data: '{ OriginatorType: "' + OriginatorType + '", ' +
            ' Originator: "' + Originator + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (i) {
                var obj = { terrId: data.d[i].TerritoryId, terrName: data.d[i].TerritoryName };
                Territories.push(obj);
            });
        },
        failure: OnFail
    });

    return Territories;
}

function GetTerritoryName(terrId) {
    for (var i = 0; i < Territories.length; i++) {
        var t = Territories[i].terrId;
        if (Territories[i].terrId == terrId) {

            return Territories[i].terrName;
        }
    }
}

function LoadDistributerByOriginator(OriginatorType, Originator) {
    DistributerTerr = [];
    var root = ROOT_PATH + "service/master/master.asmx/GetAllDistributerByOriginator";

    $.ajax({
        type: "POST",
        url: root,
        data: '{ OriginatorType: "' + OriginatorType + '", ' +
            ' Originator: "' + Originator + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (data) {
            $.each(data.d, function (i) {
                
                var obj = { disId: data.d[i].DistributorId, disName: data.d[i].Name };

                DistributerTerr.push(obj);
            });
        },
        failure: OnFail
    });

    return DistributerTerr;
}

function GetDistributerName(disId) {
    for (var i = 0; i < DistributerTerr.length; i++) {
        var t = DistributerTerr[i].disId;
        if (DistributerTerr[i].disId == disId) {

            return DistributerTerr[i].disName;
        }
    }
}

function GetSalesTypeName(salesType) {
    debugger;
    for (var i = 0; i < SalesTypes.length; i++) {
        var t = SalesTypes[i].disId;
        if (SalesTypes[i].disId == salesType) {
            return SalesTypes[i].disName;
        }
    }
}

//#endregion

