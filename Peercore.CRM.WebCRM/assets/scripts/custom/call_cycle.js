﻿//#region Common Functions

function detailExpand(e) {
    var grid = e.sender;
    var rows = grid.element.find(".k-master-row").not(e.masterRow);

    rows.each(function (e) {
        grid.collapseRow(this);
    });
}

function onDataBound(e) {
    this.expandRow(this.tbody.find("tr.k-master-row").first().addClass("snir-expanded"));
    //this.expandRow(this.tbody.find("tr tr.k-master-row:nth-child(2)").addClass("snir-expanded"));
    //this.expandRow(this.tbody.find("tr tr.k-master-row").first().addClass("snir-expanded"));
}

function OnFail(response) {
    var existsMsg = GetErrorMessageDiv("Error occured !.", "MainContent_div_message_popup");
    $('#MainContent_div_message_popup').css('display', 'block');
    $("#MainContent_div_message_popup").html(existsMsg);
    closepopup();
    hideStatusDiv("MainContent_div_message_popup");
}

function OnSuccess(response) {
    var sucessMsg = GetSuccesfullMessageDiv("Successfully Deleted.", "MainContent_div_message");
    $('#MainContent_div_message').css('display', 'block');
    $("#MainContent_div_message").html(sucessMsg);
    closepopup();

    var childGrid = $(e.target).closest(".k-grid").data("kendoGrid");
    childGrid.dataSource.read();

    //LoadAllRegions();
    hideStatusDiv("MainContent_div_message");
}

//#endregion

//#region Route Functions

function LoadAllSRTerritoryByOriginator(OriginatorType, Originator) {

    var take_grid = $("#MainContent_hfPageIndex").val();

    //$("#div_loader").show();
    $("#gridAllTerritoryReps").html("");
    $("#gridAllTerritoryReps").kendoGrid({
        height: 500,
        columns: [
            { field: "RepId", title: "Rep Id", width: "85px", hidden: true },
            //{ field: "TerritoryId", title: "Rep Id", width: "85px", hidden: true },
            { field: "RepCode", title: "Rep Code", width: "100px" },
            { field: "RepName", title: "Rep Name", width: "220px" },
            {
                // the brandId column
                title: "Territory",
                field: "TerritoryId", // bound to the brandId field
                template: "# if (TerritoryStatus == 'A' ) { # #= GetTerritoryName(TerritoryId) # # } else { # <span id='span_ttname_#= RepId #' class='span_button'>#= GetTerritoryName(TerritoryId) #</span> #} #", // the template shows the name corresponding to the brandId field
                editor: function (container) { // use a dropdownlist as an editor
                
                    // create an input element with id and name set as the bound field (brandId)
                    var input = $('<input id="TerritoryId" name="TerritoryId">');

                    // append to the editor container
                    input.appendTo(container);

                    // initialize a dropdownlist
                    input.kendoDropDownList({
                        autoBind: false,
                        cascadeFrom: "Category",
                        dataTextField: "terrName",
                        dataValueField: "terrId",
                        optionLabel: "Select territory...",
                        dataSource: Territories // bind it to the brands array
                    }).appendTo(container);
                },
                width: "170px"
            },
            { command: "edit", width: "150px" }
        ],
        editable: "inline", // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        //detailTemplate: kendo.template($("#templateRoute").html()),
        //detailInit: detailInit_callcycleOutletsInRoute,
        //detailExpand: detailExpand,
        //dataBound: onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 15,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    id: "RepId",
                    fields: {
                        TerritoryId: { validation: { required: true } },
                        RepId: { validation: { required: true } },
                        RepCode: { editable: false, nullable: true },
                        RepName: { editable: false, nullable: true },
                        TerritoryCode: { editable: true, nullable: true },
                        TerritoryName: { editable: true, nullable: true },
                        TerritoryStatus: { editable: true, nullable: true }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/call_cycle/call_cycle_service.asmx/GetAllSRTerritoryByOriginator", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid, OriginatorType: OriginatorType, Originator: Originator },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            //$("#span_ttname").removeClass("span_button");
                            $("#div_loader").hide();
                            var entityGrid = $("#gridAllTerritoryReps").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                update: {
                    url: ROOT_PATH + "service/call_cycle/call_cycle_service.asmx/UpdateSRTerritory",
                    contentType: "application/json; charset=utf-8",
                    data: { }, //ko.mapping.toJSON(options.data) 
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#gridAllTerritoryReps").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

function LoadAllDBTerritoryByOriginator(OriginatorType, Originator) {

    var take_grid = $("#MainContent_hfPageIndex").val();

    //$("#div_loader").show();
    $("#gridAllDistributerTerritory").html("");
    $("#gridAllDistributerTerritory").kendoGrid({
        height: 500,
        columns: [
            { field: "TerritoryId", title: "Territory Id", width: "120px", hidden: true },
            { field: "TerritoryCode", title: "Territory Code", width: "120px" },
            { field: "TerritoryName", title: "Territory Name", width: "200px" },
            { field: "AreaName", title: "Area Name", width: "200px" },
            { field: "AsmName", title: "Asm Name", width: "200px" },
            {
                title: "Distributer",
                field: "DistributerName", // bound to the brandId field
                template: GetDistributerName('#= DistributerId #'), // the template shows the name corresponding to the brandId field
                editor: function (container) { // use a dropdownlist as an editor
                    // create an input element with id and name set as the bound field (brandId)
                    var input = $('<input id="DistributerId" name="DistributerId">');

                    // append to the editor container
                    input.appendTo(container);

                    // initialize a dropdownlist
                    input.kendoDropDownList({
                        autoBind: false,
                        cascadeFrom: "Category",
                        dataTextField: "disName",
                        dataValueField: "disId",
                        optionLabel: "Select distributer...",
                        dataSource: DistributerTerr // bind it to the brands array
                    }).appendTo(container);
                }
            },
            { command: "edit", width: "150px" }
        ],
        editable: "inline",
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 15,
            schema: {
                data: "d.Data", 
                total: "d.Total",
                model: { 
                    id: "TerritoryId",
                    fields: {
                        TerritoryId: { validation: { required: true } },
                        TerritoryCode: { editable: false, nullable: true },
                        TerritoryName: { editable: false, nullable: true },
                        AreaCode: { editable: false, nullable: true },
                        AreaName: { editable: false, nullable: true },
                        AreaStatus: { editable: false, nullable: true },
                        AsmCode: { editable: false, nullable: true },
                        AsmName: { editable: false, nullable: true },
                        AsmStatus: { editable: false, nullable: true },
                        DistributerId: { editable: true, nullable: true },
                        DistributerCode: { editable: true, nullable: true },
                        DistributerName: { editable: true, nullable: true }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/call_cycle/call_cycle_service.asmx/GetAllTerritoriesForDistributer", 
                    contentType: "application/json; charset=utf-8", 
                    data: { pgindex: take_grid, OriginatorType: OriginatorType, Originator: Originator },
                    type: "POST",
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#gridAllDistributerTerritory").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                update: {
                    url: ROOT_PATH + "service/call_cycle/call_cycle_service.asmx/UpdateDBTerritory",
                    contentType: "application/json; charset=utf-8",
                    data: { Originator: Originator },
                    type: "POST",
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#gridAllDistributerTerritory").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

//#endregion
