﻿//#region Territory Products

function LoadTerritoriesforDiscountScheme(OriginatorType, Originator, ShemeHeaderId) {
    var take_grid = $("#MainContent_hfPageIndex").val();
    $("#div_loader").show();
    $("#gridAllTerritory").html("");

    var sharedDataSource = null;

    sharedDataSource = new kendo.data.DataSource({
        serverSorting: true,
        serverPaging: true,
        serverFiltering: true,
        pageSize: 500,
        schema: {
            data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
            total: "d.Total",
            model: { //Define the model of the data source. Required for validation and property types.
                //id: "SourceId",
                fields: {
                    TerritoryId: { validation: { required: true } },
                    TerritoryCode: { editable: false, nullable: true },
                    TerritoryName: { editable: false, nullable: true },
                    AreaName: { editable: false, nullable: true },
                    SchemeTerritoryStatus: { editable: false, nullable: true, filterable: false }
                }
            }
        },
        transport: {
            read: {
                url: ROOT_PATH + "service/discount_scheme/discount_scheme_service.asmx/GetAllTerritoriesforDiscountScheme", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                data: { pgindex: take_grid, SchemeHeaderId: ShemeHeaderId },
                type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                complete: function (jqXHR, textStatus) {
                    if (textStatus == "success") {
                        $("#div_loader").hide();
                        var entityGrid = $("#gridAllTerritory").data("kendoGrid");
                        if ((take_grid != '') && (gridindex == '0')) {
                            entityGrid.dataSource.page((take_grid - 1));
                            gridindex = '1';
                        }
                    }
                }
            },
            parameterMap: function (data, operation) {
                if (operation != "read") {
                    return JSON.stringify({ products: data.models })
                }
                else {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });

    var gridAllProducts = $("#gridAllTerritory").kendoGrid({
        dataSource: sharedDataSource,
        height: 450,
        editable: false,
        pageable: true,
        sortable: true,
        filterable: true,
        persistSelection: true,
        selectable: "row",
        columns: [
            { field: "TerritoryId", title: "Territory Id", width: "85px", hidden: true },
            { field: "TerritoryCode", title: "Territory Code", width: "100px" },
            { field: "TerritoryName", title: "Territory Name", width: "200px" },
            { field: "AreaName", title: "Area Name", width: "200px" },
            {
                field: "SchemeTerritoryStatus",
                template: "<input type=\"checkbox\" #= SchemeTerritoryStatus ? checked='checked' : '' # class=\"option-input checkbox check_rowPT\"/>",
                headerTemplate: '<input type=\"checkbox\" class=\"option-input checkbox checkAllPT\"/>',
                width: "100px",
                sortable: false
            }
        ]
    });

    $("#gridAllTerritory .k-grid-content").on("change", "input.check_rowPT", function (e) {
        var grid = $("#gridAllTerritory").data("kendoGrid");
        var checked = $(this).is(':checked');

        var col = $(this).closest('td');
        dataItem = grid.dataItem($(e.target).closest("tr"));
        dataItem.set(grid.columns[col.index()].field, checked);

        SaveUpdateDiscountTerritory(Originator, dataItem.TerritoryId, checked);
    });

    $("#gridAllTerritory .k-grid-header").on('change', "input.checkAllPT", function (e) {
        var grid = $('#gridAllTerritory').data('kendoGrid');
        var checked = $(this).is(':checked');

        grid.table.find("tr").find("td:last input").attr("checked", checked);

        var items = $("#gridAllTerritory").data("kendoGrid").dataSource.data();
        for (i = 0; i < items.length; i++) {
            var item = items[i];
            item.SchemeTerritoryStatus = "A";
    
            SaveUpdateDiscountTerritory(Originator, item.TerritoryId, checked);
        }

        if (!checked) {
            $("#gridAllTerritory").data("kendoGrid").clearSelection();
        }

        alert('Successfully Updated!')
    });
}

function SaveUpdateDiscountTerritory(Originator, TerritoryId, IsCheced) {
    var e = document.getElementById("MainContent_ddlScheme");
    var ShemeHeaderId = e.options[e.selectedIndex].value;

    var url = ROOT_PATH + "service/discount_scheme/discount_scheme_service.asmx/SaveUpdateDiscountTerritory"

    if (ShemeHeaderId == "0") {
        var errorMsg = GetErrorMessageDiv("Please select discount scheme.", "MainContent_div_message");
        $('#MainContent_div_message').css('display', 'block');
        $("#MainContent_div_message").html(errorMsg);

        hideStatusDiv("MainContent_div_message");

        return;
    }

    $.ajax({
        type: "POST",
        url: url,
        data: '{ originator: "' + Originator + '", ' +
            ' scheme_header_id: "' + ShemeHeaderId + '", ' +
            ' territory_id: "' + TerritoryId + '", ' +
            ' is_checked: "' + IsCheced + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg.d.Status == true) {
               // var sucessMsg = GetSuccesfullMessageDiv("Region was successfully Saved.", "MainContent_div_message");
               // $('#MainContent_div_message').css('display', 'block');
               // $("#MainContent_div_message").html(sucessMsg);
               // //closepopup();
               //// LoadTerritoriesforDiscountScheme(0, Originator, ShemeHeaderId);
               // hideStatusDiv("MainContent_div_message");
            }
            else {
                var errorMsg = GetErrorMessageDiv("Error occurred!.", "MainContent_div_message");

                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);

                hideStatusDiv("MainContent_div_message");
            }
        },
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
}

//#endregion

