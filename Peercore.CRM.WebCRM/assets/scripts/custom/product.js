﻿//#region Territory Products

function LoadTerritoriesforProductTerritory(OriginatorType, Originator) {
    
    $.ajax({
        type: "POST",
        url: ROOT_PATH + "service/master/master.asmx/GetAllTerritoriesByOriginator",
        data: '{ OriginatorType: "' + OriginatorType + '", ' +
            ' Originator: "' + Originator + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var territoryArr = [];
            if (response.hasOwnProperty('d')) {
                msg = response.d;

                $.each(msg, function (key, value) {
                    var val = {
                        label: value.TerritoryName,
                        value: value.TerritoryId
                    };

                    territoryArr.push(val);
                });

                console.log(territoryArr);

                $('#txtTerritoryFilter').autocomplete({
                    source: territoryArr,
                    select: function (e, ui) { debugger;
                        //$("#txtTerritoryFilter").val(ui.item.value);
                        $("#txtTerritoryFilter").val(ui.item.label);

                        //LoadProductsforProductTerritory(OriginatorType, Originator, ui.item.value);

                        return false;
                    }
                });
            }
        },
        error: function (response) {
        }
    });
}

function LoadProductsforProductTerritory(OriginatorType, Originator, TerritoryId) {
    var take_grid = $("#MainContent_hfPageIndex").val();
    $("#div_loader").show();
    $("#gridAllProducts").html("");

    var sharedDataSource = null;

    sharedDataSource = new kendo.data.DataSource({
        serverSorting: true,
        serverPaging: true,
        serverFiltering: true,
        pageSize: 10,
        schema: {
            data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
            total: "d.Total",
            model: { //Define the model of the data source. Required for validation and property types.
                //id: "SourceId",
                fields: {
                    ProductId: { validation: { required: true } },
                    FgCode: { editable: false, nullable: true },
                    Code: { editable: false, nullable: true },
                    Name: { editable: false, nullable: true },
                    ProductTerritoryStatus: { editable: false, nullable: true }
                }
            }
        },
        transport: {
            read: {
                url: ROOT_PATH + "service/product/product.asmx/GetAllProductsByTerritoryId", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                data: { pgindex: take_grid, TerritoryId: TerritoryId },
                type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                complete: function (jqXHR, textStatus) {
                    if (textStatus == "success") {
                        $("#div_loader").hide();
                        var entityGrid = $("#gridAllProducts").data("kendoGrid");
                        if ((take_grid != '') && (gridindex == '0')) {
                            entityGrid.dataSource.page((take_grid - 1));
                            gridindex = '1';
                        }
                    }
                }
            },
            parameterMap: function (data, operation) {
                if (operation != "read") {
                    return JSON.stringify({ products: data.models })
                }
                else {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });

    var gridAllProducts = $("#gridAllProducts").kendoGrid({
        dataSource: sharedDataSource,
        height: 375,
        editable: false,
        pageable: true,
        sortable: true,
        filterable: true,
        persistSelection: true,
        selectable: "row",
        columns: [
            { field: "ProductId", title: "Product Id", width: "85px", hidden: true },
            { field: "FgCode", title: "Fg-Code", width: "100px" },
            { field: "Code", title: "Product Code", width: "100px" },
            { field: "Name", title: "Product Name", width: "220px" },
            {
                field: "ProductTerritoryStatus",
                template: "<input type=\"checkbox\" #= ProductTerritoryStatus ? checked='checked' : '' # class=\"option-input checkbox check_rowPT\" />",
                headerTemplate: '<input type=\"checkbox\" class=\"option-input checkbox checkAllPT\" />',
                width: "100px",
                sortable: false
            }
        ]
    });

   
    $("#gridAllProducts .k-grid-content").on("change", "input.check_rowPT", function (e) {
        var grid = $("#gridAllProducts").data("kendoGrid");
        var checked = $(this).is(':checked');

        var col = $(this).closest('td');
        dataItem = grid.dataItem($(e.target).closest("tr"));
        dataItem.set(grid.columns[col.index()].field, checked);

        SaveUpdateProductTerritory(Originator, TerritoryId, dataItem.ProductId, checked);
    });

    $("#gridAllProducts .k-grid-header").on('change', "input.checkAllPT", function (e) {
        var grid = $('#gridAllProducts').data('kendoGrid');
        var checked = $(this).is(':checked');
 
        grid.table.find("tr").find("td:last input").attr("checked", checked);

        var items = $("#gridAllProducts").data("kendoGrid").dataSource.data();
        for (i = 0; i < items.length; i++) {
            var item = items[i];
            item.SchemeTerritoryStatus = "A";
            
            SaveUpdateProductTerritory(Originator, TerritoryId, item.ProductId, checked);
        }

        if (!checked) {
            $("#gridAllProducts").data("kendoGrid").clearSelection();
        }

        alert('Successfully Updated!')
    });
    
    //.on('click', ".check_rowPT1", function (e) {
    //    var $cb = $(this);
    //    var checked = $cb.is(':checked');

    //    var checked = this.checked,
    //        row = $(this).closest("tr"),
    //        grid = $("#gridAllProducts").data("kendoGrid"),
    //        dataItem = grid.dataItem(row);

    //    console.log("Product ID ==> ", dataItem.ProductId, " , TerritoryId ==> ", TerritoryId, " Checked ==>", checked);
    //    SaveUpdateProductTerritory(Originator, TerritoryId, dataItem.ProductId, checked);

    //}).on('click', ".checkAllPT", function (e) {
    //    var $cb = $(this);
    //    var checked = $cb.is(':checked');
    //    var grid = $('#gridAllProducts').data('kendoGrid');
    //    grid.table.find("tr").find("td:last input").attr("checked", checked);

    //    var items = $("#gridAllProducts").data("kendoGrid").dataSource.data();
    //    for (i = 0; i < items.length; i++) {
    //        var item = items[i];
    //        item.ProductTerritoryStatus = "A";

    //        //console.log(item);
    //        console.log("Product ID ==> ", item.ProductId, " , TerritoryId ==> ", TerritoryId, " Checked ==>", checked);
    //        SaveUpdateProductTerritory(Originator, TerritoryId, dataItem.ProductId, checked);
    //    }

    //    if (!checked) {
    //        $("#gridAllProducts").data("kendoGrid").clearSelection();
    //    }
    //});
}

function LoadTerritoriesforProducts(OriginatorType, Originator, ProductId) {
    var take_grid = $("#MainContent_hfPageIndex").val();
    $("#div_loader").show();
    $("#gridAllTerritories").html("");

    var sharedDataSource = null;

    sharedDataSource = new kendo.data.DataSource({
        serverSorting: true,
        serverPaging: true,
        serverFiltering: true,
        pageSize: 1000,
        schema: {
            data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
            total: "d.Total",
            model: { //Define the model of the data source. Required for validation and property types.
                //id: "SourceId",
                fields: {
                    TerritoryId: { validation: { required: true } },
                    TerritoryCode: { editable: false, nullable: true },
                    TerritoryName: { editable: false, nullable: true },
                    RepCode: { editable: false, nullable: true },
                    RepName: { editable: false, nullable: true },
                    ProductTerritoryStatus: { editable: false, nullable: true }
                }
            }
        },
        transport: {
            read: {
                url: ROOT_PATH + "service/product/product.asmx/GetAllTerritoriesByProductId", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                data: { pgindex: take_grid, ProductId: ProductId },
                type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                complete: function (jqXHR, textStatus) {
                    if (textStatus == "success") {
                        $("#div_loader").hide();
                        var entityGrid = $("#gridAllTerritories").data("kendoGrid");
                        if ((take_grid != '') && (gridindex == '0')) {
                            entityGrid.dataSource.page((take_grid - 1));
                            gridindex = '1';
                        }
                    }
                }
            },
            parameterMap: function (data, operation) {
                if (operation != "read") {
                    return JSON.stringify({ products: data.models })
                }
                else {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });

    var gridAllProducts = $("#gridAllTerritories").kendoGrid({
        dataSource: sharedDataSource,
        height: 500,
        editable: false,
        pageable: true,
        sortable: true,
        filterable: true,
        persistSelection: true,
        selectable: "row",
        columns: [
            { field: "TerritoryId", title: "Territory Id", width: "85px", hidden: true },
            { field: "TerritoryCode", title: "Territory Code", width: "100px" },
            { field: "TerritoryName", title: "Territory Name", width: "150px" },
            //{ field: "RepCode", title: "Rep Name", width: "100px" },
            //{ field: "RepName", title: "Rep Name", width: "150" },
            {
                field: "ProductTerritoryStatus",
                template: "<input type=\"checkbox\" #= ProductTerritoryStatus=='A' ? checked='checked' : '' # class=\"option-input checkbox check_rowPT\"/>",
                headerTemplate: '<input type=\"checkbox\" class=\"option-input checkbox checkAllPT\"/>',
                width: "100px",
                sortable: false
            }
        ]
    });

    $("#gridAllTerritories .k-grid-content").on("change", "input.check_rowPT", function (e) {
        var grid = $("#gridAllTerritories").data("kendoGrid");
        var checked = $(this).is(':checked');

        var col = $(this).closest('td');
        dataItem = grid.dataItem($(e.target).closest("tr"));
        dataItem.set(grid.columns[col.index()].field, checked);

        SaveUpdateProductTerritory(Originator, dataItem.TerritoryId, ProductId, checked);
    });

    $("#gridAllTerritories .k-grid-header").on('change', "input.checkAllPT", function (e) {
        var grid = $('#gridAllTerritories').data('kendoGrid');
        var checked = $(this).is(':checked');

        grid.table.find("tr").find("td:last input").attr("checked", checked);

        var items = $("#gridAllTerritories").data("kendoGrid").dataSource.data();
        for (i = 0; i < items.length; i++) {
            var item = items[i];
            item.SchemeTerritoryStatus = "A";

            SaveUpdateProductTerritory(Originator, item.TerritoryId, ProductId, checked);
        }

        if (!checked) {
            $("#gridAllTerritories").data("kendoGrid").clearSelection();
        }

        alert('Successfully Updated!')
    });

    //.on('click', ".check_rowPT1", function (e) {
    //    var $cb = $(this);
    //    var checked = $cb.is(':checked');

    //    var checked = this.checked,
    //        row = $(this).closest("tr"),
    //        grid = $("#gridAllProducts").data("kendoGrid"),
    //        dataItem = grid.dataItem(row);

    //    console.log("Product ID ==> ", dataItem.ProductId, " , TerritoryId ==> ", TerritoryId, " Checked ==>", checked);
    //    SaveUpdateProductTerritory(Originator, TerritoryId, dataItem.ProductId, checked);

    //}).on('click', ".checkAllPT", function (e) {
    //    var $cb = $(this);
    //    var checked = $cb.is(':checked');
    //    var grid = $('#gridAllProducts').data('kendoGrid');
    //    grid.table.find("tr").find("td:last input").attr("checked", checked);

    //    var items = $("#gridAllProducts").data("kendoGrid").dataSource.data();
    //    for (i = 0; i < items.length; i++) {
    //        var item = items[i];
    //        item.ProductTerritoryStatus = "A";

    //        //console.log(item);
    //        console.log("Product ID ==> ", item.ProductId, " , TerritoryId ==> ", TerritoryId, " Checked ==>", checked);
    //        SaveUpdateProductTerritory(Originator, TerritoryId, dataItem.ProductId, checked);
    //    }

    //    if (!checked) {
    //        $("#gridAllProducts").data("kendoGrid").clearSelection();
    //    }
    //});
}

function SaveUpdateProductTerritory(Originator, TerritoryId, ProductId, IsCheced) {

    var url = ROOT_PATH + "service/product/product.asmx/SaveUpdateProductTerritory"

    if (TerritoryId == "0") {
        //var errorMsg = GetErrorMessageDiv("Please select territory.", "MainContent_div_message");
        //$('#MainContent_div_message').css('display', 'block');
        //$("#MainContent_div_message").html(errorMsg);

        ////closepopup();
        //hideStatusDiv("MainContent_div_message");

        return;
    }

    $.ajax({
        type: "POST",
        url: url,
        data: '{ originator: "' + Originator + '", ' +
            ' territory_id: "' + TerritoryId + '", ' +
            ' product_id: "' + ProductId + '", ' +
            ' is_checked: "' + IsCheced + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            if (msg.d.Status == true) {
                //var sucessMsg = GetSuccesfullMessageDiv("Region was successfully Saved.", "MainContent_div_message");
                //$('#MainContent_div_message').css('display', 'block');
                //$("#MainContent_div_message").html(sucessMsg);
                ////closepopup();
                ////LoadProductsforProductTerritory(0, Originator, TerritoryId);
                //hideStatusDiv("MainContent_div_message");
            }
            else {
                var errorMsg = GetErrorMessageDiv("Error occurred!.", "MainContent_div_message");

                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);
                //closepopup();
                hideStatusDiv("MainContent_div_message");
            }
        },
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
}

//#endregion

//#region Territory Product Stocks

function LoadTerritoriesforStockProduct(OriginatorType, Originator) {

    $.ajax({
        type: "POST",
        url: ROOT_PATH + "service/master/master.asmx/GetAllTerritoriesByOriginator",
        data: '{ OriginatorType: "' + OriginatorType + '", ' +
                ' Originator: "' + Originator + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (response) {
            var territoryArr = [];
            if (response.hasOwnProperty('d')) {
                msg = response.d;

                $.each(msg, function (key, value) {
                    var val = {
                        label: value.TerritoryName,
                        value: value.TerritoryId
                    };

                    territoryArr.push(val);
                });

                $('#txtTerritoryFilter').autocomplete({
                    source: territoryArr,
                    select: function (e, ui) {
                        $("#txtTerritoryFilter").val(ui.item.label);
                        $("#MainContent_hfTerritoryId").val(ui.item.value);

                        LoadStockGrid();

                        return false;
                    }
                });
            }
        },
        error: function (response) {
        }
    });
}

function LoadProductStockByTerritoryIdforStockProduct(alloDate, territoryId) {

    var take_grid = $("#MainContent_hfPageIndex").val();

    $("#div_loader").show();
    $("#hdnSelectedAllocDate").val(alloDate);

    var url = "javascript:unloadStock('#=StockHeaderId#','#=CatalogCode#','#=CatalogName#','#=AllocQty#','#=LoadQty#','#=BalanceQty#','#=ReturnQty#','#=LoadQtyOld#','#=VehicleBalanceQty#','#=VehicleReturnQty#');"
    url = "<a href=\"" + url + "\">Unload</a>";

    var load_url = "javascript:loadStock('#=StockHeaderId#','#=CatalogCode#','#=CatalogName#','#=AllocQty#','#=LoadQty#','#=BalanceQty#','#=Sku#');"
    load_url = "<a href=\"" + load_url + "\">Load</a>";

    $("#StockGrid").kendoGrid({
        height: 350,
        selectable: true,
        columns: [
            { field: "RecordId", title: "#", width: "35px", editable: false },
            //{ field: "StockHeaderId", title: "Stocke Header ID", width: "100px", editable: false, hidden: true },
            //{ field: "StockDetailId", title: "Stocke Detail ID", width: "100px", editable: false, hidden: true },
            { field: "CatalogCode", title: "Code", width: "50px", editable: false, hidden: false },
            { field: "CatalogName", title: "Product", width: "300px", editable: false },
            { field: "AllocQty", title: "Allocate Qty.", width: "150px", format: "{0:n1}", editor: AllocQtyEditor, editable: true },
            //{ field: "LoadQty", title: "Load Qty(Mil)", format: "{0:n1}", editor: LoadQtyEditor, editable: false, hidden: true },
            //{ field: "BalanceQty", title: "Allocated Balance Qty(Mil)", format: "{0:n1}", editor: LoadQtyEditor, editable: false, hidden: true },
            //{ field: "VehicleBalanceQty", title: "Vehicle Balance Qty(Mil)", format: "{0:n1}", editable: false, hidden: true },
            //{ field: "ReturnQty", title: "Allocated Return Qty(Mil)", format: "{0:n1}", editable: false, hidden: true },
            //{ field: "VehicleReturnQty", title: "Vehicle Return Qty(Mil)", format: "{0:n1}", editable: false, hidden: true },
            //{ field: "ReturningQty", title: "Returning Qty(Mil)", format: "{0:n1}", editable: false, hidden: true },
            //{ field: "Sku", title: "SKU", format: "{0:n1}", editable: false, hidden: true },
            //{ field: "LoadQtyOld", title: "LoadQtyOld", format: "{0:n1}", editable: false, hidden: true },
            { field: "AllocationQtyTotal", title: "Total Allocated Qty.", width: "150px", format: "{0:n1}", editable: false, hidden: false },
            //{ field: "StockTotalId", title: "StockTotalId", width: "150px", editable: false, hidden: true },
            //{ template: load_url, width: "50px", hidden: true },
            //{ template: url, width: "60px", hidden: true }
        ],
        editable: true, // enable editing
        pageable: true,
        height: 400,
        sortable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 100,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        StockHeaderId: { type: "number", validation: { required: true } },
                        StockDetailId: { type: "number", validation: { required: true } },
                        CatalogCode: { editable: false, validation: { required: true } },
                        CatalogName: { editable: false, validation: { required: true } },
                        AllocQty: { type: "number", validation: { required: true } },
                        LoadQty: { type: "number", validation: { required: true }, editable: false },
                        BalanceQty: { type: "number", validation: { required: true }, editable: false },
                        VehicleBalanceQty: { type: "number", validation: { required: true }, editable: false },
                        ReturnQty: { type: "number", validation: { required: true }, editable: false },
                        VehicleReturnQty: { type: "number", validation: { required: true }, editable: false },
                        ReturningQty: { type: "number", validation: { required: true }, editable: false },
                        Unload: { validation: { required: true }, editable: false },
                        Sku: { type: "number", validation: { required: true }, editable: false },
                        LoadQtyOld: { type: "number", validation: { required: true }, editable: false },
                        AllocationQtyTotal: { type: "number", validation: { required: true }, editable: false },
                        StockTotalId: { type: "number", validation: { required: true }, editable: false },
                        RecordId: { type: "number", validation: { required: true }, editable: false }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/load_stock/load_stock.asmx/GetAllProductStockByTerritoryId", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { allocDate: alloDate, territoryId: territoryId, pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        $("#div_loader").hide();
                        if (textStatus == "success") {
                            var entityGrid = $("#StockGrid").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function LoadProductStockAdjusmentByTerritoryIdforStockProduct(alloDate, territoryId) {
    var take_grid = $("#MainContent_hfPageIndex").val();
    $("#div_loader").show();
    $("#hdnSelectedAllocDate").val(alloDate);

    //check for conditions to enable/disable Unload link
    //check for conditions to enable/disable Load link

    var url = "javascript:unloadStock('#=StockHeaderId#','#=CatalogCode#','#=CatalogName#','#=AllocQty#','#=LoadQty#','#=BalanceQty#','#=ReturnQty#','#=LoadQtyOld#','#=VehicleBalanceQty#','#=VehicleReturnQty#');"
    url = "<a href=\"" + url + "\">Unload</a>";

    var load_url = "javascript:loadStock('#=StockHeaderId#','#=CatalogCode#','#=CatalogName#','#=AllocQty#','#=LoadQty#','#=BalanceQty#','#=Sku#');"
    load_url = "<a href=\"" + load_url + "\">Load</a>";

    $("#StockGridAdjusment").kendoGrid({
        height: 350,
        selectable: true,
        columns: [
            { field: "RecordId", title: "#", width: "35px", editable: false },
            //{ field: "StockHeaderId", title: "Stocke Header ID", width: "100px", editable: false, hidden: true },
            //{ field: "StockDetailId", title: "Stocke Detail ID", width: "100px", editable: false, hidden: true },
            { field: "CatalogCode", title: "Code", width: "50px", editable: false, hidden: false },
            { field: "CatalogName", title: "Product", width: "300px", editable: false },
            { field: "AllocationQtyTotal", title: "As At Date Qty.", width: "150px", format: "{0:n1}", editable: false, hidden: false },
            { field: "AllocQty", title: "Physical Qty.", width: "150px", format: "{0:n1}", editor: AllocQtyEditor, editable: true },
            {
                field: "AdjustQty", title: "Ajdust Qty.", width: "150px", format: "{0:n1}", editable: false,
                editor: function (cont, options) {
                    $("<span>" + options.model.AdjustQty + "</span>").appendTo(cont);
                }
            },
            //{ field: "LoadQty", title: "Load Qty(Mil)", format: "{0:n1}", editor: LoadQtyEditor, editable: false, hidden: true },
            //{ field: "BalanceQty", title: "Allocated Balance Qty(Mil)", format: "{0:n1}", editor: LoadQtyEditor, editable: false, hidden: true },
            //{ field: "VehicleBalanceQty", title: "Vehicle Balance Qty(Mil)", format: "{0:n1}", editable: false, hidden: true },
            //{ field: "ReturnQty", title: "Allocated Return Qty(Mil)", format: "{0:n1}", editable: false, hidden: true },
            //{ field: "VehicleReturnQty", title: "Vehicle Return Qty(Mil)", format: "{0:n1}", editable: false, hidden: true },
            //{ field: "ReturningQty", title: "Returning Qty(Mil)", format: "{0:n1}", editable: false, hidden: true },
            //{ field: "Sku", title: "SKU", format: "{0:n1}", editable: false, hidden: true },
            //{ field: "LoadQtyOld", title: "LoadQtyOld", format: "{0:n1}", editable: false, hidden: true },
            { field: "StockTotalId", title: "StockTotalId", width: "150px", editable: false, hidden: true },
            { template: load_url, width: "50px", hidden: true },
            { template: url, width: "60px", hidden: true }
        ],
        editable: true, // enable editing
        save: function (data) {
            var AjQty = 0;

            AjQty = data.values.AllocQty - data.model.AllocationQtyTotal;
            if (data.values.AllocQty == null) {
                data.model.set("AllocQty", 0);
            }

            var test = data.model.set("AdjustQty", AjQty);
        },
        pageable: true,
        height: 400,
        sortable: true,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 100,
            schema: {
                data: "d.Data",
                total: "d.Total",
                model: {
                    fields: {
                        StockHeaderId: { type: "number", validation: { required: true } },
                        StockDetailId: { type: "number", validation: { required: true } },
                        CatalogCode: { editable: false, validation: { required: true } },
                        CatalogName: { editable: false, validation: { required: true } },
                        AllocQty: { type: "number", validation: { required: true } },
                        AdjustQty: { type: "number", validation: { required: false } },
                        LoadQty: { type: "number", validation: { required: true }, editable: false },
                        BalanceQty: { type: "number", validation: { required: true }, editable: false },
                        VehicleBalanceQty: { type: "number", validation: { required: true }, editable: false },
                        ReturnQty: { type: "number", validation: { required: true }, editable: false },
                        VehicleReturnQty: { type: "number", validation: { required: true }, editable: false },
                        ReturningQty: { type: "number", validation: { required: true }, editable: false },
                        Unload: { validation: { required: true }, editable: false },
                        Sku: { type: "number", validation: { required: true }, editable: false },
                        LoadQtyOld: { type: "number", validation: { required: true }, editable: false },
                        AllocationQtyTotal: { type: "number", validation: { required: true }, editable: false },
                        StockTotalId: { type: "number", validation: { required: true }, editable: false },
                        RecordId: { type: "number", validation: { required: true }, editable: false }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/load_stock/load_stock.asmx/GetAllProductStockByTerritoryId", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { allocDate: alloDate, territoryId: territoryId, pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        $("#div_loader").hide();
                        if (textStatus == "success") {
                            var entityGrid = $("#StockGridAdjusment").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

//#endregion