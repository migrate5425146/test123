﻿//#region Common Functions

function detailExpand(e) {
    var grid = e.sender;
    var rows = grid.element.find(".k-master-row").not(e.masterRow);

    rows.each(function (e) {
        grid.collapseRow(this);
    });
}

function onDataBound(e) {
    this.expandRow(this.tbody.find("tr.k-master-row").first().addClass("snir-expanded"));
    //this.expandRow(this.tbody.find("tr tr.k-master-row:nth-child(2)").addClass("snir-expanded"));
    //this.expandRow(this.tbody.find("tr tr.k-master-row").first().addClass("snir-expanded"));
}

function OnFail(response) {
    var existsMsg = GetErrorMessageDiv("Error occured !.", "MainContent_div_message_popup");
    $('#MainContent_div_message_popup').css('display', 'block');
    $("#MainContent_div_message_popup").html(existsMsg);
    closepopup();
    hideStatusDiv("MainContent_div_message_popup");
}

function OnSuccess(response) {
    var sucessMsg = GetSuccesfullMessageDiv("Successfully Deleted.", "MainContent_div_message");
    $('#MainContent_div_message').css('display', 'block');
    $("#MainContent_div_message").html(sucessMsg);
    closepopup();

    var childGrid = $(e.target).closest(".k-grid").data("kendoGrid");
    childGrid.dataSource.read();

    //LoadAllRegions();
    hideStatusDiv("MainContent_div_message");
}

//#endregion

//#region Region Functions

//Load All Regions 
function LoadAllRegions() {
    var take_grid = $("#MainContent_hfPageIndex").val();
    var url = "javascript:UpdateRegion('#=RegionId#','#=RegionCode#','#=RegionName#');";

    var urlRCode = "<a href=\"" + url + "\">#=RegionCode#</a>";
    var urlRName = "<a href=\"" + url + "\">#=RegionName#</a>";

    $("#div_loader").show();
    $("#gridAllRegions").html("");

    var tblAllregions = $("#gridAllRegions").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { //Define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        RegionId: { validation: { required: true } },
                        RegionCode: { editable: false, nullable: true },
                        RegionName: { editable: false, nullable: true },
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/master/master.asmx/GetAllRegions", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#gridAllRegions").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 375,
        editable: false,
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        detailTemplate: kendo.template($("#templateRegion").html()),
        detailInit: detailRegionInfoByRegionId,
        detailExpand: detailExpand,
        dataBound: onDataBound,
        //toolbar: [{ text: "Refresh", className: "btn-refresh" }],
        columns: [
            { field: "RegionId", title: "Region Id", width: "85px", hidden: true },
            { template: urlRCode, field: "RegionCode", title: "Region Code", width: "85px" },
            { template: urlRName, field: "RegionName", title: "Region Name", width: "250px" },
            { template: '<a href="javascript:DeleteRegion(\'#=RegionId#\');">Delete</a>', field: "", width: "40px" }
        ]
    }).on("click", ".btn-refresh", function (e) {
        var childGrid = $(e.target).closest(".k-grid").data("kendoGrid");
        childGrid.dataSource.read();
    });
}

function detailRegionInfoByRegionId(e) {
    var detailRow = e.detailRow;

    detailRow.find(".tabstrip").kendoTabStrip({
        animation: {
            open: { effects: "fadeIn" }
        }
    });

    detailLoadAreasByRegionId(detailRow, e);
    detailLoadRSMByRegionId(detailRow, e);
}

function detailLoadAreasByRegionId(detailRow, e) {

    var take_grid = $("#MainContent_hfPageIndex").val();

    var url = "javascript:EditArea('#=AreaId#','#=RegionId#','#=AreaCode#','#=AreaName#');";

    var urlAreaCode = "<a href=\"" + url + "\">#=AreaCode#</a>";
    var urlAreaName = "<a href=\"" + url + "\">#=AreaName#</a>";

    var subgrid = detailRow.find(".region-areas").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { //Define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        AreaId: { validation: { required: true } },
                        AreaCode: { editable: false, nullable: true },
                        AreaName: { editable: false, nullable: true },
                        RegionId: { editable: false, nullable: true }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/master/master.asmx/GetAllAreaByRegionId", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid, RegionId: e.data.RegionId },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models });
                    }
                    else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            filter: { field: "RegionId", operator: "eq", value: e.data.RegionId }
        },
        scrollable: false,
        sortable: true,
        pageable: true,
        filterable : true,
        toolbar: [{ text: "Refresh", className: "btn-refresh" }],
        columns: [
            { field: "AreaId", title: "Area Id", width: "85px", hidden: true },
            { template: urlAreaCode, field: "AreaCode", title: "Area Code", width: "105px" },
            { template: urlAreaName, field: "AreaName", title: "Area Name", width: "200px" },
            { template: '<a class="a_DeleteAreaFromRegion" href="javascript:DeleteAreaFromRegion(\'#=AreaId#\');">Remove</a>', field: "", width: "70px" }
        ]
    }).on("click", ".btn-refresh", function (e) {
        var childGrid = $(e.target).closest(".k-grid").data("kendoGrid");
        childGrid.dataSource.read();
    });

    //expand and collapse rows on click
    subgrid.on('click', 'tr', function () {
        if ($(this).hasClass("snir-expanded")) {
            $(subgrid).data().kendoGrid.collapseRow($(this));
            $(this).removeClass("snir-expanded");
            $(this).addClass("snir-collapsed");
        } else {
            $(subgrid).data().kendoGrid.expandRow($(this));
            $(this).addClass("snir-expanded");
            $(this).removeClass("snir-collapsed");
        }
    })
}

function detailLoadRSMByRegionId(detailRow, e) {
    var take_grid = $("#MainContent_hfPageIndex").val();

    var url = "javascript:UpdateRSM('#=RsmId#','#=RsmCode#','#=RsmName#','#=RsmUserName#','#=RegionId#','#=RsmTel1#','#=RsmTel2#','#=RsmEmail#');";

    var urlRSMCode = "<a href=\"" + url + "\">#=RsmCode#</a>";
    //var urlRSMName = "<a href=\"" + url + "\">#=RsmName#</a>";
    //var urlRSMUserName = "<a href=\"" + url + "\">#=RsmUserName#</a>";
    //var urlRegionCode = "<a href=\"" + url + "\">#=RegionCode#</a>";
    //var urlRegionName = "<a href=\"" + url + "\">#=RegionName#</a>";
    //var urlRSMTel1 = "<a href=\"" + url + "\">#=RsmTel1#</a>";
    //var urlRSMTel2 = "<a href=\"" + url + "\">#=RsmTel2#</a>";
    //var urlRSMEmail = "<a href=\"" + url + "\">#=RsmEmail#</a>";

    var subgridRSM = detailRow.find(".region-Rsm").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { //Define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        RsmId: { validation: { required: true } },
                        RsmCode: { editable: false, nullable: true },
                        RsmName: { editable: false, nullable: true },
                        RsmUserName: { editable: false, nullable: true },
                        RegionId: { editable: false, nullable: true },
                        RegionCode: { editable: false, nullable: true },
                        RegionName: { editable: false, nullable: true },
                        RsmTel1: { editable: false, nullable: true },
                        RsmTel2: { editable: false, nullable: true },
                        RsmEmail: { editable: false, nullable: true },
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/master/master.asmx/GetRsmDetailsByRegionId", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid, RegionId: e.data.RegionId },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $(".region-Rsm").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models });
                    }
                    else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            filter: { field: "RegionId", operator: "eq", value: e.data.RegionId }
        },
        editable: false,
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        columns: [
            { field: "RsmId", title: "RSM Id", width: "85px", hidden: true },
            { field: "RsmCode", title: "RSM Code", width: "105px" },
            { field: "RsmName", title: "RSM Name", width: "200px" },
            { field: "RsmUserName", title: "User Name", width: "100px" },
            { field: "RegionId", title: "Region Id", width: "85px", hidden: true },
            //{ template: urlRegionCode, field: "RegionCode", title: "Region Code", width: "100px" },
            //{ template: urlRegionName, field: "RegionName", title: "Region Name", width: "150px" },
            //{ template: urlRSMTel1, field: "RsmTel1", title: "Tel 1", width: "100px" },
            //{ template: urlRSMTel2, field: "RsmTel2", title: "Tel 2", width: "100px" },
            //{ template: urlRSMEmail, field: "RsmEmail", title: "Email", width: "120px" },
            { template: '<a href="javascript:DeleteRSM(\'#=RsmId#\');">Delete</a>', field: "", width: "70px" }
        ],
    });
    //expand and collapse rows on click
    subgridRSM.on('click', 'tr', function () {
        if ($(this).hasClass("snir-expanded")) {
            $(subgrid).data().kendoGrid.collapseRow($(this));
            $(this).removeClass("snir-expanded");
            $(this).addClass("snir-collapsed");
        }
        else {
            $(subgrid).data().kendoGrid.expandRow($(this));
            $(this).addClass("snir-expanded");
            $(this).removeClass("snir-collapsed");
        }
    })
}

/*Update Region Entry Page*/
function UpdateRegion(id, code, name) {
    $("#hdnSelectedRegionId").val(id);
    $("#txtregioncode").val(code);
    $("#txtregionname").val(name);

    $("#window").css("display", "block");
    var wnd = $("#window").kendoWindow({
        title: "Add/Edit Region",
        modal: true,
        visible: false,
        resizable: false
    }).data("kendoWindow");

    $("#txtregioncode").focus();

    wnd.center().open();
}

/*Delete Region Entry Page*/
function DeleteRegion(regionid) {
    var r = confirm("Do you want to delete this region?");
    if (r == true) {
        DeleteRegionByRegionId(regionid);
    } 
}

function DeleteRegionByRegionId(regionid) {
    var root = ROOT_PATH + "service/master/master.asmx/DeleteRegion";

    $.ajax({
        type: "POST",
        url: root,
        data: '{region_id: "' + regionid + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess,
        failure: OnFail
    });

    function OnFail(response) {
        var existsMsg = GetErrorMessageDiv("Error occured !.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(existsMsg);
        closepopup();
        hideStatusDiv("MainContent_div_message_popup");
    }

    function OnSuccess(response) {
        var sucessMsg = GetSuccesfullMessageDiv("Region was successfully Deleted.", "MainContent_div_message");
        $('#MainContent_div_message').css('display', 'block');
        $("#MainContent_div_message").html(sucessMsg);
        closepopup();
        LoadAllRegions();
        hideStatusDiv("MainContent_div_message");
    }
}

/*Save Region Entry Page*/
function SaveRegion() {
    if (($("#txtregioncode").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Region Code.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else if (($("#txtregionname").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Region Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else {
        //var brandCode = $("#txtCode").val();
        var regionCode = $("#txtregioncode").val();
        var regionName = $("#txtregionname").val();
        var selectedRegionId = $("#hdnSelectedRegionId").val();
        var selectedCompId = $("#hdnSelectedCompId").val();

        var url = ROOT_PATH + "service/master/master.asmx/SaveRegion"

        $.ajax({
            type: "POST",
            url: url,
            data: '{ comp_id: "' + selectedCompId + '", ' +
                ' region_id: "' + selectedRegionId + '", ' +
                ' region_code: "' + regionCode + '", ' +
                ' region_name: "' + regionName + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                console.log(msg);
                if (msg.d.Status == true) {
                    var sucessMsg = GetSuccesfullMessageDiv("Region was successfully Saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    closepopup();
                    LoadAllRegions();
                    hideStatusDiv("MainContent_div_message");
                }
                else if (msg.d.StatusCode == "Exsist") {
                    var existsMsg = GetErrorMessageDiv("Region already exists in system!.", "MainContent_div_message_popup");

                    $('#MainContent_div_message_popup').css('display', 'block');
                    $("#MainContent_div_message_popup").html(existsMsg);
                    //closepopup();
                    hideStatusDiv("MainContent_div_message_popup");
                }
                else {
                    var errorMsg = GetErrorMessageDiv("Error occurred !.", "MainContent_div_message");

                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(errorMsg);
                    closepopup();
                    hideStatusDiv("MainContent_div_message");
                }
            },
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}

//#endregion

//#region Area Functions

function LoadAllAreas() {
    var take_grid = $("#MainContent_hfPageIndex").val();
    var url = "javascript:EditArea('#=AreaId#','#=RegionId#','#=AreaCode#','#=AreaName#');";

    var urlAreaCode = "<a href=\"" + url + "\">#=AreaCode#</a>";
    var urlAreaName = "<a href=\"" + url + "\">#=AreaName#</a>";

    $("#div_loader").show();
    $("#gridAllAreas").html("");

    var tblAllregions = $("#gridAllAreas").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 14,
            schema: {
                data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { //Define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        AreaId: { validation: { required: true } },
                        RegionId: { validation: { required: true } },
                        RegionCode: { validation: { required: true } },
                        RegionName: { validation: { required: true } },
                        RegionStatus: { validation: { required: true } },
                        AreaCode: { editable: false, nullable: true },
                        AreaName: { editable: false, nullable: true }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/master/master.asmx/GetAllArea", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#gridAllAreas").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models });
                    }
                    else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 460,
        editable: false,
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        detailTemplate: kendo.template($("#templateArea").html()),
        detailInit: detailAreaInfoByAreaId,
        detailExpand: detailExpand,
        dataBound: onDataBound,
        //toolbar: [{ text: "Refresh", className: "btn-refresh" }],
        columns: [
            { field: "AreaId", title: "Area Id", width: "85px", hidden: true },
            { field: "RegionId", title: "Region Id", width: "85px", hidden: true },
            { template: urlAreaCode, field: "AreaCode", title: "Area Code", width: "60px" },
            { template: urlAreaName, field: "AreaName", title: "Area Name", width: "150px" },
            //{ template: "#if(RegionStatus=='A'){# #: RegionName # #}else{# #: RegionCode # #}#", field: "", title: "Region Name", width: "150px" },
            {
                template: "# if (RegionStatus == 'A' ) { # #=RegionName# # } else { # <span class='span_button'>#=RegionName#</span> #} #",
                field: "", title: "Region Name", width: "150px"
            },
            { template: '<a href="javascript:DeleteArea(\'#=AreaId#\');">Delete</a>', field: "", width: "40px" }
        ]
    }).on("click", ".btn-refresh", function (e) {
        var childGrid = $(e.target).closest(".k-grid").data("kendoGrid");
        childGrid.dataSource.read();
    });
}

function detailAreaInfoByAreaId(e) {
    var detailRow = e.detailRow;

    detailRow.find(".tabstrip").kendoTabStrip({
        animation: {
            open: { effects: "fadeIn" }
        }
    });

    detailLoadTerritoriesByAreaId(detailRow, e);
    detailLoadASMByAreaId(detailRow, e);
}

function detailLoadTerritoriesByAreaId(detailRow, e) {

    var take_grid = $("#MainContent_hfPageIndex").val();

    var url = "javascript:EditTerritory('#=TerritoryId#','#=AreaId#','#=TerritoryCode#','#=TerritoryName#');";

    var urlTerritoryCode = "<a href=\"" + url + "\">#=TerritoryCode#</a>";
    var urlTerritoryName = "<a href=\"" + url + "\">#=TerritoryName#</a>";

    var subgridTerritory = detailRow.find(".area-territory").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { //Define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        TerritoryId: { validation: { required: true } },
                        TerritoryCode: { editable: false, nullable: true },
                        TerritoryName: { editable: false, nullable: true },
                        AreaId: { editable: false, nullable: true }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/master/master.asmx/GetAllTerritoryByAreaId", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid, AreaId: e.data.AreaId },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models });
                    }
                    else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            filter: { field: "AreaId", operator: "eq", value: e.data.AreaId }
        },
        scrollable: false,
        sortable: true,
        pageable: true,
        filterable: true,
        toolbar: [{ text: "Refresh", className: "btn-refresh" }],
        columns: [
            { field: "TerritoryId", title: "Territory Id", width: "85px", hidden: true },
            { template: urlTerritoryCode, field: "TerritoryCode", title: "Territory Code", width: "105px" },
            { template: urlTerritoryName, field: "TerritoryName", title: "Territory Name", width: "200px" },
            { template: '<a href="javascript:DeleteTerritoryFromArea(\'#=TerritoryId#\');">Remove</a>', field: "", width: "70px" }
        ],
    });
    //expand and collapse rows on click
    subgridTerritory.on('click', 'tr', function () {
        if ($(this).hasClass("snir-expanded")) {
            $(subgrid).data().kendoGrid.collapseRow($(this));
            $(this).removeClass("snir-expanded");
            $(this).addClass("snir-collapsed");
        } else {
            $(subgrid).data().kendoGrid.expandRow($(this));
            $(this).addClass("snir-expanded");
            $(this).removeClass("snir-collapsed");
        }
    })
}

function detailLoadASMByAreaId(detailRow, e) {

    var take_grid = $("#MainContent_hfPageIndex").val();

    //var url = "javascript:UpdateASM('#=AsmId#','#=AsmCode#','#=AsmName#','#=AsmUserName#','#=AreaId#','#=AsmTel1#','#=AsmTel2#','#=AsmEmail#');";

    //var urlAsmCode = "<a href=\"" + url + "\">#=AsmCode#</a>";
    //var urlRSMName = "<a href=\"" + url + "\">#=RsmName#</a>";
    //var urlRSMUserName = "<a href=\"" + url + "\">#=RsmUserName#</a>";
    //var urlRegionCode = "<a href=\"" + url + "\">#=RegionCode#</a>";
    //var urlRegionName = "<a href=\"" + url + "\">#=RegionName#</a>";
    //var urlRSMTel1 = "<a href=\"" + url + "\">#=RsmTel1#</a>";
    //var urlRSMTel2 = "<a href=\"" + url + "\">#=RsmTel2#</a>";
    //var urlRSMEmail = "<a href=\"" + url + "\">#=RsmEmail#</a>";

    var subgridASM = detailRow.find(".area-asm").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { //Define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        AsmId: { validation: { required: true } },
                        AsmCode: { editable: false, nullable: true },
                        AsmName: { editable: false, nullable: true },
                        //RsmUserName: { editable: false, nullable: true },
                        AreaId: { editable: false, nullable: true },
                        //RegionCode: { editable: false, nullable: true },
                        //RegionName: { editable: false, nullable: true },
                        //RsmTel1: { editable: false, nullable: true },
                        //RsmTel2: { editable: false, nullable: true },
                        //RsmEmail: { editable: false, nullable: true }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/master/master.asmx/GetAllAsmByAreaId", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid, AreaId: e.data.AreaId },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $(".area-asm").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models });
                    }
                    else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            filter: { field: "AreaId", operator: "eq", value: e.data.AreaId }
        },
        editable: false,
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        columns: [
            { field: "AsmId", title: "ASM Id", width: "85px", hidden: true },
            { field: "AsmCode", title: "ASM Code", width: "105px" },
            { field: "AsmName", title: "ASM Name", width: "200px" },
            //{ field: "AsmUserName", title: "User Name", width: "100px" },
            { field: "AreaId", title: "Area Id", width: "85px", hidden: true },
            //{ template: urlRegionCode, field: "RegionCode", title: "Region Code", width: "100px" },
            //{ template: urlRegionName, field: "RegionName", title: "Region Name", width: "150px" },
            //{ template: urlRSMTel1, field: "RsmTel1", title: "Tel 1", width: "100px" },
            //{ template: urlRSMTel2, field: "RsmTel2", title: "Tel 2", width: "100px" },
            //{ template: urlRSMEmail, field: "RsmEmail", title: "Email", width: "120px" },
            { template: '<a href="javascript:DeleteASM(\'#=AsmId#\');">Delete</a>', field: "", width: "70px" }
        ],
    });
    //expand and collapse rows on click
    subgridASM.on('click', 'tr', function () {
        if ($(this).hasClass("snir-expanded")) {
            $(subgrid).data().kendoGrid.collapseRow($(this));
            $(this).removeClass("snir-expanded");
            $(this).addClass("snir-collapsed");
        }
        else {
            $(subgrid).data().kendoGrid.expandRow($(this));
            $(this).addClass("snir-expanded");
            $(this).removeClass("snir-collapsed");
        }
    })
}

function EditArea(areaId, regionId, areaCode, areaName) {
    //console.log('areaId => ', areaId, ', regionId => ', regionId, ', areaCode => ', areaCode, ', areaName => ', areaName);
    $("#hdnSelectedAreaId").val(areaId);
    $("#hdnSelectedRegionId").val(regionId);
    $("#txtAreaCode").val(areaCode);
    $("#txtAreaName").val(areaName);
    $("#MainContent_ddlRegion").val(regionId);

    $("#hdnSelectedAreaCode").val(areaCode);
    $("#hdnSelectedAreaName").val(areaName);

    $("#windowArea").css("display", "block");
    var wnd = $("#windowArea").kendoWindow({
        title: "Edit Area",
        modal: true,
        visible: false,
        resizable: false
    }).data("kendoWindow");

    $("#txtAreaCode").focus();

    wnd.center().open();
}

//#region Delete Area from Region
function DeleteAreaFromRegion(areaId) {
    var r = confirm("Do you want to remove this area from region?");
    if (r == true) {
        DeleteAreaFromRegionByAreaId(areaId);
    }
}

function DeleteAreaFromRegionByAreaId(areaId) {
    var root = ROOT_PATH + "service/master/master.asmx/DeleteAreaFromRegionByAreaId";

    $.ajax({
        type: "POST",
        url: root,
        data: '{area_id: ' + areaId + ' }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function OnSuccess(response) {
            var sucessMsg = GetSuccesfullMessageDiv("Successfully Deleted.", "MainContent_div_message");
            $('#MainContent_div_message').css('display', 'block');
            $("#MainContent_div_message").html(sucessMsg);
            closepopupArea();

            //LoadAllRegions();
            hideStatusDiv("MainContent_div_message");
        },
        failure: OnFail
    });
}
//#endregion

/*Save Area Entry Page*/
function SaveArea() {
    var e = document.getElementById("MainContent_ddlRegion");
    var selectedRegionId = e.options[e.selectedIndex].value;

    if (($("#txtAreaCode").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Area Code.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");

        return;
    }
    else if (($("#txtAreaName").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Area Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");

        return;
    }
    else if (selectedRegionId == 0) {
        var sucessMsg = GetErrorMessageDiv("Please Select Region.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");

        return;
    }
    else {
        $("#hdnSelectedRegionId").val($("#MainContent_ddlRegion").val());

        var areaCode = $("#txtAreaCode").val();
        var areaName = $("#txtAreaName").val();
        var selectedAreaId = $("#hdnSelectedAreaId").val();
        var selectedRegionId = $("#hdnSelectedRegionId").val();

        var selectedAreaCode = $("#hdnSelectedAreaCode").val();
        var selectedAreaName = $("#hdnSelectedAreaName").val();

        var url = ROOT_PATH + "service/master/master.asmx/SaveUpdateArea"

        $.ajax({
            type: "POST",
            url: url,
            data: '{ region_id: "' + selectedRegionId + '", ' +
                ' area_id: "' + selectedAreaId + '", ' +
                ' area_code_old: "' + selectedAreaCode + '", ' +
                ' area_name_old: "' + selectedAreaName + '", ' +
                ' area_code: "' + areaCode + '", ' +
                ' area_name: "' + areaName + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (msg.d.Status == true) {
                    var sucessMsg = GetSuccesfullMessageDiv("Successfully saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    LoadAllAreas();
                    closepopupArea();
                    hideStatusDiv("MainContent_div_message");
                }
                else {
                    if (msg.d.StatusCode == "Exsist") {
                        var existsMsg = GetErrorMessageDiv("Area already exists in system.", "MainContent_div_message_popup");

                        $('#MainContent_div_message_popup').css('display', 'block');
                        $("#MainContent_div_message_popup").html(existsMsg);
                        //closepopupArea();

                        hideStatusDiv("MainContent_div_message_popup");
                    }
                    else {
                        var errorMsg = GetErrorMessageDiv("Error occurred.", "MainContent_div_message");

                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        //closepopupArea();

                        hideStatusDiv("MainContent_div_message");
                    }
                }
            },
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}

function SaveAreaFromRegion() {
    var e = document.getElementById("MainContent_ddlRegion");
    var selectedRegionId = e.options[e.selectedIndex].value;

    if (($("#txtAreaCode").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Area Code.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");

        return;
    }
    else if (($("#txtAreaName").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Area Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");

        return;
    }
    else if (selectedRegionId == 0) {
        var sucessMsg = GetErrorMessageDiv("Please Select Region.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");

        return;
    }
    else {
        $("#hdnSelectedRegionId").val($("#MainContent_ddlRegion").val());
        var areaCode = $("#txtAreaCode").val();
        var areaName = $("#txtAreaName").val();
        var selectedAreaId = $("#hdnSelectedAreaId").val();
        var selectedRegionId = $("#hdnSelectedRegionId").val();

        var selectedAreaCode = $("#hdnSelectedAreaCode").val();
        var selectedAreaName = $("#hdnSelectedAreaName").val();

        var url = ROOT_PATH + "service/master/master.asmx/SaveUpdateArea"

        $.ajax({
            type: "POST",
            url: url,
            data: '{ region_id: "' + selectedRegionId + '", ' +
                ' area_id: "' + selectedAreaId + '", ' +
                ' area_code_old: "' + selectedAreaCode + '", ' +
                ' area_name_old: "' + selectedAreaName + '", ' +
                ' area_code: "' + areaCode + '", ' +
                ' area_name: "' + areaName + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (msg.d.Status == true) {
                    var sucessMsg = GetSuccesfullMessageDiv("Successfully saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    LoadAllRegions();
                    closepopupArea();
                    hideStatusDiv("MainContent_div_message");
                }
                else {
                    if (msg.d.StatusCode == "Exsist") {
                        var existsMsg = GetErrorMessageDiv("Area already exists in system.", "MainContent_div_message_popup");

                        $('#MainContent_div_message_popup').css('display', 'block');
                        $("#MainContent_div_message_popup").html(existsMsg);
                        //closepopupArea();

                        hideStatusDiv("MainContent_div_message_popup");
                    }
                    else {
                        var errorMsg = GetErrorMessageDiv("Error occurred.", "MainContent_div_message");

                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        //closepopupArea();

                        hideStatusDiv("MainContent_div_message");
                    }
                }
            },
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}

//#endregion

//#region Territory Functions
function LoadAllTerritories() {
    var take_grid = $("#MainContent_hfPageIndex").val();
    var url = "javascript:EditTerritory('#=TerritoryId#','#=AreaId#','#=TerritoryCode#','#=TerritoryName#');";

    var urlTerritoryCode = "<a href=\"" + url + "\">#=TerritoryCode#</a>";
    var urlTerritoryName = "<a href=\"" + url + "\">#=TerritoryName#</a>";

    $("#div_loader").show();
    $("#gridAllTerritory").html("");

    var tblAllterritory = $("#gridAllTerritory").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { //Define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        AreaId: { validation: { required: true } },
                        AreaCode: { validation: { required: true } },
                        AreaName: { validation: { required: true } },
                        AreaStatus: { validation: { required: true } },
                        TerritoryId: { validation: { required: true } },
                        TerritoryCode: { editable: false, nullable: true },
                        TerritoryName: { editable: false, nullable: true }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/master/master.asmx/GetAllTerritory", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#gridAllTerritory").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models });
                    }
                    else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 375,
        editable: false,
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        detailTemplate: kendo.template($("#templateTerritory").html()),
        detailInit: detailTerritoryInfoByTerritoryId,
        dataBound: onDataBound,
        //toolbar: [{ text: "Refresh", className: "btn-refresh" }],
        columns: [
            { field: "TerritoryId", title: "Territory Id", width: "85px", hidden: true },
            { field: "AreaId", title: "Area Id", width: "85px", hidden: true },
            { template: urlTerritoryCode, field: "TerritoryCode", title: "Territory Code", width: "100px" },
            { template: urlTerritoryName, field: "TerritoryName", title: "Territory Name", width: "200px" },
            {
                template: "# if (AreaStatus == 'A' ) { # #=AreaName# # } else { # <span class='span_button'>#=AreaName#</span> #} #",
                field: "", title: "Area Name", width: "200px"
            },
            {
                field: "CanAddOutlet", title: "Can Add Outlets", width: "120px",
                template: "<input type=\"checkbox\" #= CanAddOutlet ? checked='checked' : '' # class=\"check_row\" id='#=TerritoryId #'/>"
            },
            {
                field: "IsGeoFence", title: "Is GeoFence", width: "120px",
                template: "<input type=\"checkbox\" #= IsGeoFence ? checked='checked' : '' # class=\"check_row_geofence\" id='#=TerritoryId #'/>"
            },
            { template: '<a href="javascript:DeleteTerritory(\'#=TerritoryId#\');">Delete</a>', field: "", width: "80px" }
        ]
    }).on("click", ".btn-refresh", function (e) {
        var childGrid = $(e.target).closest(".k-grid").data("kendoGrid");
        childGrid.dataSource.read();
    });
}

//Adding click event on check box 
$('.check_row').live('click', function (e) {

    var url = ROOT_PATH + "service/master/master.asmx/UpdateTerritoryCanAddOutlet"

    $.ajax({
        type: "POST",
        url: url,
        data: '{ territory_id: "' + this.id + '", ' +
            ' can_add_outlet: "' + this.checked + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {
            
        },
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
});

$('.check_row_geofence').live('click', function (e) {

    var url = ROOT_PATH + "service/master/master.asmx/UpdateTerritoryGeoFence"

    $.ajax({
        type: "POST",
        url: url,
        data: '{ territory_id: "' + this.id + '", ' +
            ' is_geofence: "' + this.checked + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function (msg) {

        },
        error: function (msg, textStatus) {
            if (textStatus == "error") {
                var msg = msg;
            }
        }
    });
});

function detailTerritoryInfoByTerritoryId(e) {
    var detailRow = e.detailRow;

    detailRow.find(".tabstrip").kendoTabStrip({
        animation: {
            open: { effects: "fadeIn" }
        }
    });

    detailLoadRoutesByTerritoryId(detailRow, e);
    detailLoadSRByTerritoryId(detailRow, e);
}

function detailLoadRoutesByTerritoryId(detailRow, e) {
    var take_grid = $("#MainContent_hfPageIndex").val();

    var url = "javascript:EditRouteMaster('#=RouteMasterId#','#=RouteMasterCode#','#=RouteMasterName#','#=TerritoryId#');";

    var urlRouteCode = "<a href=\"" + url + "\">#=RouteMasterCode#</a>";
    var urlRouteName = "<a href=\"" + url + "\">#=RouteMasterName#</a>";

    var subgrid = detailRow.find(".territory-route").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { //Define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        RouteMasterId: { validation: { required: true } },
                        RouteMasterCode: { editable: false, nullable: true },
                        RouteMasterName: { editable: false, nullable: true },
                        TerritoryId: { editable: false, nullable: true },
                        TerritoryStatus: { editable: false, nullable: true }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/routes/routes.asmx/GetAllRouteMasterByTerritoryId", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid, TerritoryId: e.data.TerritoryId },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models });
                    }
                    else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            filter: { field: "TerritoryId", operator: "eq", value: e.data.TerritoryId }
        },
        scrollable: false,
        sortable: true,
        pageable: true,
        filterable:true,
        toolbar: [{ text: "Refresh", className: "btn-refresh" }],
        columns: [
            { field: "RouteMasterId", title: "Route Id", width: "85px", hidden: true },
            {
                template: "# if (TerritoryStatus == 'A' ) { # " + urlRouteCode + " # } else { # <span class='span_button'>" + urlRouteCode + "</span> #} #",
                field: "RouteMasterCode", title: "Route Code", width: "105px"
            },
            {
                template: "# if (TerritoryStatus == 'A' ) { # " + urlRouteName + " # } else { # <span class='span_button'>" + urlRouteName + "</span> #} #",
                field: "RouteMasterName", title: "Route Name", width: "105px"
            },
            { template: '<a href="javascript:DeleteRouteFromTerritory(\'#=RouteMasterId#\');">Remove</a>', field: "", width: "70px" }
        ],
    });
    //expand and collapse rows on click
    subgrid.on('click', 'tr', function () {
        if ($(this).hasClass("snir-expanded")) {
            $(subgrid).data().kendoGrid.collapseRow($(this));
            $(this).removeClass("snir-expanded");
            $(this).addClass("snir-collapsed");
        } else {
            $(subgrid).data().kendoGrid.expandRow($(this));
            $(this).addClass("snir-expanded");
            $(this).removeClass("snir-collapsed");
        }
    })
}

function detailLoadSRByTerritoryId(detailRow, e) {
    var take_grid = $("#MainContent_hfPageIndex").val();

    var url = "javascript:UpdateRSM('#=RepId#','#=RepCode#','#=RepName#');";

    var urlRepCode = "<a href=\"" + url + "\">#=RepCode#</a>";

    var subgrid = detailRow.find(".territory-sr").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { //Define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        RepId: { validation: { required: true } },
                        RepCode: { editable: false, nullable: true },
                        RepName: { editable: false, nullable: true }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/master/master.asmx/GetAllSRByTerritoryId", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid, TerritoryId: e.data.TerritoryId },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $(".territory-sr").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models });
                    }
                    else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            filter: { field: "TerritoryId", operator: "eq", value: e.data.TerritoryId }
        },
        editable: false,
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        columns: [
            { field: "RepId", title: "Rep Id", width: "85px", hidden: true },
            { field: "RepCode", title: "Rep Code", width: "105px" },
            //{ field: "RepName", title: "Rep Name", width: "200px" },
            {
                template: "# if (TerritoryStatus == 'A' ) { # #=RepName# # } else { # <span class='span_button'>#=RepName#</span> #} #",
                field: "", title: "Rep Name", width: "200px"
            },
            { template: '<a href="javascript:DeleteSRFromTerritory(\'#=TerritoryId#\', \'#=RepId#\');">Delete</a>', field: "", width: "70px" }
        ],
    });
    //expand and collapse rows on click
    subgrid.on('click', 'tr', function () {
        if ($(this).hasClass("snir-expanded")) {
            $(subgrid).data().kendoGrid.collapseRow($(this));
            $(this).removeClass("snir-expanded");
            $(this).addClass("snir-collapsed");
        }
        else {
            $(subgrid).data().kendoGrid.expandRow($(this));
            $(this).addClass("snir-expanded");
            $(this).removeClass("snir-collapsed");
        }
    })
}

function EditTerritory(territory_id, area_id, territory_code, territory_name) {
    //console.log('areaId => ', areaId, ', regionId => ', regionId, ', areaCode => ', areaCode, ', areaName => ', areaName);
    $("#hdnSelectedTerritoryId").val(territory_id);
    $("#hdnSelectedAreaId").val(area_id);
    $("#txtTerritoryCode").val(territory_code);
    $("#txtTerritoryName").val(territory_name);
    $("#MainContent_ddlArea").val(area_id);
    $("#hdnSelectedTerritoryCode").val(territory_code);
    $("#hdnSelectedTerritoryName").val(territory_name);

    $("#windowTerritory").css("display", "block");
    var wnd = $("#windowTerritory").kendoWindow({
        title: "Edit Territory",
        modal: true,
        visible: false,
        resizable: false
    }).data("kendoWindow");

    $("#txtTerritoryCode").focus();

    wnd.center().open();
}

function SaveAreaTerritory() {
    var e = document.getElementById("MainContent_ddlArea");
    var selectedAreaId = e.options[e.selectedIndex].value;

    if (($("#txtTerritoryCode").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Territory Code.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");

        return;
    }
    else if (($("#txtTerritoryName").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Territory Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");

        return;
    }
    else if (selectedAreaId == 0) {
        var sucessMsg = GetErrorMessageDiv("Please Select Area.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");

        return;
    }
    else {
        $("#hdnSelectedAreaId").val($("#MainContent_ddlArea").val());
        var territoryCode = $("#txtTerritoryCode").val();
        var territoryName = $("#txtTerritoryName").val();
        var selectedTerritoryId = $("#hdnSelectedTerritoryId").val();
        var selectedAreaId = $("#hdnSelectedAreaId").val();

        var url = ROOT_PATH + "service/master/master.asmx/SaveUpdateTerritory"

        $.ajax({
            type: "POST",
            url: url,
            data: '{ area_id: "' + selectedAreaId + '", ' +
                ' territory_id: "' + selectedTerritoryId + '", ' +
                ' territory_code_old: "' + selectedTerritoryCode + '", ' +
                ' territory_code: "' + territoryCode + '", ' +
                ' territory_name_old: "' + selectedTerritoryName + '", ' +
                ' territory_name: "' + territoryName + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (msg.d.Status == true) {
                    var sucessMsg = GetSuccesfullMessageDiv("successfully Saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    LoadAllAreas();
                    closepopupTerritory();
                    hideStatusDiv("MainContent_div_message");
                }
                else {
                    if (msg.d.StatusCode == "Exsist") {
                        var existsMsg = GetErrorMessageDiv("Territory Name already exists in system!.", "MainContent_div_message_popup");

                        $('#MainContent_div_message_popup').css('display', 'block');
                        $("#MainContent_div_message_popup").html(existsMsg);
                        //closepopup();

                        hideStatusDiv("MainContent_div_message_popup");
                    }
                    else {
                        var errorMsg = GetErrorMessageDiv("Error occurred !.", "MainContent_div_message");

                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        //closepopup();

                        hideStatusDiv("MainContent_div_message");
                    }
                }
            },
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}

function SaveTerritory() {
    var e = document.getElementById("MainContent_ddlArea");
    var selectedAreaId = e.options[e.selectedIndex].value;

    if (($("#txtTerritoryCode").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Territory Code.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");

        return;
    }
    else if (($("#txtTerritoryName").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please Enter a Territory Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");

        return;
    }
    else if (selectedAreaId == 0) {
        var sucessMsg = GetErrorMessageDiv("Please Select Area.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");

        return;
    }
    else {
        var territoryCode = $("#txtTerritoryCode").val();
        var territoryName = $("#txtTerritoryName").val();
        var selectedTerritoryId = $("#hdnSelectedTerritoryId").val();
        var selectedTerritoryCode = $("#hdnSelectedTerritoryCode").val();
        var selectedTerritoryName = $("#hdnSelectedTerritoryName").val();
        var originator = $("#MainContent_hfOriginator").val();
        var originatorType = $("#MainContent_hfOriginatorType").val();

        var url = ROOT_PATH + "service/master/master.asmx/SaveUpdateTerritory"

        $.ajax({
            type: "POST",
            url: url,
            data: '{ area_id: "' + selectedAreaId + '", ' +
                ' territory_id: "' + selectedTerritoryId + '", ' +
                ' territory_code_old: "' + selectedTerritoryCode + '", ' +
                ' territory_code: "' + territoryCode + '", ' +
                ' territory_name_old: "' + selectedTerritoryName + '", ' +
                ' territory_name: "' + territoryName + '", ' +
                ' created_by: "' + originator + '" }',
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function (msg) {
                if (msg.d.Status == true) {
                    var sucessMsg = GetSuccesfullMessageDiv("successfully Saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    LoadAllTerritories();
                    closepopupTerritory();
                    hideStatusDiv("MainContent_div_message");
                }
                else {
                    if (msg.d.StatusCode == "Exsist") {
                        var existsMsg = GetErrorMessageDiv("Territory already exists in system.", "MainContent_div_message_popup");

                        $('#MainContent_div_message_popup').css('display', 'block');
                        $("#MainContent_div_message_popup").html(existsMsg);
                        //closepopup();

                        hideStatusDiv("MainContent_div_message_popup");
                    }
                    else {
                        var errorMsg = GetErrorMessageDiv("Error occurred !.", "MainContent_div_message");

                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        //closepopup();

                        hideStatusDiv("MainContent_div_message");
                    }
                }
            },
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}

/*Delete Area from Region*/
function DeleteTerritoryFromArea(territoryId) {
    var r = confirm("Do you want to remove this territory from area?");
    if (r == true) {
        DeleteTerritoryFromAreaByTerritoryId(territoryId);
    }
}

function DeleteTerritoryFromAreaByTerritoryId(territoryId) {
    var root = ROOT_PATH + "service/master/master.asmx/DeleteTerritoryFromAreaByTerritoryId";

    $.ajax({
        type: "POST",
        url: root,
        data: '{territory_id: ' + territoryId + ' }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function OnSuccess(response) {
            var sucessMsg = GetSuccesfullMessageDiv("Successfully Deleted.", "MainContent_div_message");
            $('#MainContent_div_message').css('display', 'block');
            $("#MainContent_div_message").html(sucessMsg);
            LoadAllAreas();
            closepopupTerritory();
            hideStatusDiv("MainContent_div_message");
        },
        failure: OnFail
    });
}

function DeleteRouteFromTerritory(routeId) {
    var r = confirm("Do you want to remove this route from territory?");
    if (r == true) {
        DeleteRouteFromTerritoryByRouteId(routeId);
    }
}

function DeleteRouteFromTerritoryByRouteId(routeId) {
    var root = ROOT_PATH + "service/routes/routes.asmx/DeleteRouteFromTerritoryByRouteId";

    $.ajax({
        type: "POST",
        url: root,
        data: '{ routeId: ' + routeId + ' }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function OnSuccess(response) {
            var sucessMsg = GetSuccesfullMessageDiv("Successfully Deleted.", "MainContent_div_message");
            $('#MainContent_div_message').css('display', 'block');
            $("#MainContent_div_message").html(sucessMsg);
            LoadAllTerritories();
            //closepopupTerritory();
            hideStatusDiv("MainContent_div_message");
        },
        failure: OnFail
    });
}

/*Delete Region Entry Page*/

function DeleteTerritory(territoryId) {
    var r = confirm("Do you want to delete this Territory?");
    if (r == true) {
        DeleteTerritoryTerritoryId(territoryId);
    }
}

function DeleteTerritoryTerritoryId(territoryId) {
    var root = ROOT_PATH + "service/master/master.asmx/DeleteTerritory";

    $.ajax({
        type: "POST",
        url: root,
        data: '{ territory_id : "' + territoryId + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function OnSuccess(response) {
            LoadAllTerritories();
        },
        failure: OnFail
    });
}

//#endregion

//#region RSM Functions

function LoadAllRSM() {
    var take_grid = $("#MainContent_hfPageIndex").val();

    var url = "javascript:EditRSM('#=RsmId#','#=RsmCode#','#=RsmName#','#=RsmUserName#','#=RegionId#','#=RsmTel1#','#=RsmTel2#','#=RsmEmail#');";

    var urlRSMCode = "<a href=\"" + url + "\">#=RsmCode#</a>";
    var urlRSMName = "<a href=\"" + url + "\">#=RsmName#</a>";
    var urlRSMUserName = "<a href=\"" + url + "\">#=RsmUserName#</a>";

    $("#div_loader").show();
    $("#gridAllRSM").html("");
    $("#gridAllRSM").kendoGrid({
        height: 375,
        columns: [
            { field: "RsmId", title: "RSM Id", width: "85px", hidden: true },
            { template: urlRSMCode, field: "RsmCode", title: "RSM Code", width: "105px" },
            { template: urlRSMName, field: "RsmName", title: "RSM Name", width: "200px" },
            { template: urlRSMUserName, field: "RsmUserName", title: "User Name", width: "100px" },
            //{ field: "RegionId", title: "Region Id", width: "85px", hidden: true },
            //{ template: urlRegionCode, field: "RegionCode", title: "Region Code", width: "100px", hidden: true },
            //{ template: urlRegionName, field: "RegionName", title: "Region Name", width: "150px" },
            {
                template: "# if (RegionStatus == 'A' ) { # #=RegionName# # } else { # <span class='span_button'>#=RegionName#</span> #} #",
                field: "RegionName", title: "Region Name", width: "110px"
            },
            { field: "RsmTel1", title: "Mobile", width: "90px" },
            //{ field: "RsmTel2", title: "Tel 2", width: "100px", hidden: true },
            { field: "RsmEmail", title: "Email", width: "120px" },
            { template: '<a href="javascript:DeleteRSM(\'#=RsmId#\');">Delete</a>', field: "", width: "70px" }
        ],
        editable: false,
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        detailTemplate: kendo.template($("#templateRSM").html()),
        detailInit: detailRSMInfoByRsmId,
        detailExpand: detailExpand,
        dataBound: onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { //Define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        RsmId: { validation: { required: true } },
                        RsmCode: { editable: false, nullable: true },
                        RsmName: { editable: false, nullable: true },
                        RsmUserName: { editable: false, nullable: true },
                        RegionId: { editable: false, nullable: true },
                        RegionCode: { editable: false, nullable: true },
                        RegionName: { editable: false, nullable: true },
                        RsmTel1: { editable: false, nullable: true },
                        RsmTel2: { editable: false, nullable: true },
                        RsmEmail: { editable: false, nullable: true },
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/master/master.asmx/GetAllRSM", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#gridAllRSM").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read")
                    {
                        return JSON.stringify({ products: data.models });
                    }
                    else
                    {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function detailRSMInfoByRsmId(e) {
    var detailRow = e.detailRow;

    detailRow.find(".tabstrip").kendoTabStrip({
        animation: {
            open: { effects: "fadeIn" }
        }
    });

    detailLoadAreasByRsmId(detailRow, e);
    //detailLoadASMByAreaId(detailRow, e);
}

function detailLoadAreasByRsmId(detailRow, e) {

    var take_grid = $("#MainContent_hfPageIndex").val();

    var url = "javascript:EditArea('#=AreaId#','#=RegionId#','#=AreaCode#','#=AreaName#');";

    var urlAreaCode = "<a href=\"" + url + "\">#=AreaCode#</a>";
    var urlAreaName = "<a href=\"" + url + "\">#=AreaName#</a>";

    var subgridArea = detailRow.find(".rsm-area").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 14,
            schema: {
                data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { //Define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        AreaId: { validation: { required: true } },
                        RegionId: { validation: { required: true } },
                        RegionCode: { validation: { required: true } },
                        RegionName: { validation: { required: true } },
                        RegionStatus: { validation: { required: true } },
                        AreaCode: { editable: false, nullable: true },
                        AreaName: { editable: false, nullable: true }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/master/master.asmx/GetAllArea", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models });
                    }
                    else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 460,
        editable: false,
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        //toolbar: [{ text: "Refresh", className: "btn-refresh" }],
        columns: [
            { field: "AreaId", title: "Area Id", width: "85px", hidden: true },
            { field: "RegionId", title: "Region Id", width: "85px", hidden: true },
            { field: "AreaCode", title: "Area Code", width: "60px" },
            { field: "AreaName", title: "Area Name", width: "150px" },
            //{ template: "#if(RegionStatus=='A'){# #: RegionName # #}else{# #: RegionCode # #}#", field: "", title: "Region Name", width: "150px" },
            {
                template: "# if (RegionStatus == 'A' ) { # #=RegionName# # } else { # <span class='span_button'>#=RegionName#</span> #} #",
                field: "", title: "Region Name", width: "150px"
            },
            { template: '<a href="javascript:DeleteArea(\'#=AreaId#\');">Delete</a>', field: "", width: "40px" }
        ]
    });

    //expand and collapse rows on click
    subgridArea.on('click', 'tr', function () {
        if ($(this).hasClass("snir-expanded")) {
            $(subgrid).data().kendoGrid.collapseRow($(this));
            $(this).removeClass("snir-expanded");
            $(this).addClass("snir-collapsed");
        } else {
            $(subgrid).data().kendoGrid.expandRow($(this));
            $(this).addClass("snir-expanded");
            $(this).removeClass("snir-collapsed");
        }
    })
}


/*Update Region Entry Page*/
function EditRSM(id, code, name, username,
    regionid, tel1, tel2, email)
{
    if (tel1) {
        tel1 = '';
    }
    if (tel2) {
        tel2 = '';
    }
    if (email) {
        email = '';
    }

    $("#hdnSelectedRsmId").val(id);
    $("#txtrsmcode").val(code);
    $("#txtrsmname").val(name);
    $("#txtrsmusername").val(username);
    $("#txtrsmtel1").val(tel1);
    $("#txtrsmemail").val(email);
    $("#MainContent_ddlRegion").val(regionid);

    $("#window").css("display", "block");
    var wnd = $("#window").kendoWindow({
        title: "Add/Edit Rsm",
        modal: true,
        visible: false,
        resizable: false
    }).data("kendoWindow");

    $("#txtrsmcode").focus();

    wnd.center().open();
}

/*Delete Region Entry Page*/
function DeleteRSM(rsm_id) {
    debugger;
    var r = confirm("Do you want to delete this RSM?");
    if (r == true) {
        DeleteRsmByRsmId(rsm_id);
    }
}

function DeleteRsmByRsmId(rsm_id) {
    var root = ROOT_PATH + "service/master/master.asmx/DeleteRsmByRsmId";

    $.ajax({
        type: "POST",
        url: root,
        data: '{ rsm_id: "' + rsm_id + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess,
        failure: OnFail
    });
}

function SaveRSM() {
    var e = document.getElementById("MainContent_ddlRegion");
    var selectedRegionId = e.options[e.selectedIndex].value;
    
    var originator = $("#MainContent_hfRSMOriginator").val();
    var selectedRsmId = $("#hdnSelectedRsmId").val();

    if (($("#txtrsmcode").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please enter a RSM Code.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else if (($("#txtrsmname").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please enter a RSM Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else if (($("#txtrsmusername").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please enter a User Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else if (selectedRegionId == '') {
        var sucessMsg = GetErrorMessageDiv("Please select region.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else {
        //var brandCode = $("#txtCode").val();
        var rsmCode = $("#txtrsmcode").val();
        var rsmName = $("#txtrsmname").val();
        var rsmUserName = $("#txtrsmusername").val();
        var rsmTel1 = $("#txtrsmtel1").val();
        var rsmEmail = $("#txtrsmemail").val();
        
        var url = ROOT_PATH + "service/master/master.asmx/SaveRSM"

        var data = '{ region_id: "' + selectedRegionId + '", ' +
            ' rsm_id: "' + selectedRsmId + '", ' +
            ' rsm_code: "' + rsmCode + '", ' +
            ' rsm_name: "' + rsmName + '", ' +
            ' rsm_username: "' + rsmUserName + '", ' +
            ' rsm_tel1: "' + rsmTel1 + '", ' +
            ' rsm_email: "' + rsmEmail + '", ' +
            ' created_by: "' + originator + '" }';

        console.log(data);

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            contentType: "application/json;",
            dataType: "json",
            success: function (msg) {
                if (msg.d.Status == true) {
                    var sucessMsg = GetSuccesfullMessageDiv("RSM was successfully Saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    closepopup();
                    LoadAllRSM();
                    hideStatusDiv("MainContent_div_message");
                }
                else {
                    var errorMsg = GetErrorMessageDiv("Error occurred !.", "MainContent_div_message");

                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(errorMsg);
                    closepopup();
                    hideStatusDiv("MainContent_div_message");
                }
            },
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}

//#endregion RSM

//#region ASM Functions

function LoadAllASM() {
    var take_grid = $("#MainContent_hfPageIndex").val();

    var url = "javascript:EditASM('#=AsmId#','#=AsmCode#','#=AsmName#','#=AsmUserName#','#=AreaId#','#=AsmTel1#','#=AsmTel2#','#=AsmEmail#');";

    var urlAsmCode = "<a href=\"" + url + "\">#=AsmCode#</a>";
    var urlAsmName = "<a href=\"" + url + "\">#=AsmName#</a>";
    var urlAsmUserName = "<a href=\"" + url + "\">#=AsmUserName#</a>";

    $("#div_loader").show();
    $("#gridAllASM").html("");
    $("#gridAllASM").kendoGrid({
        height: 375,
        columns: [
            { field: "AsmId", title: "Asm Id", width: "85px", hidden: true },
            { template: urlAsmCode, field: "AsmCode", title: "ASM Code", width: "105px" },
            { template: urlAsmName, field: "AsmName", title: "ASM Name", width: "200px" },
            { template: urlAsmUserName, field: "", title: "User Name", width: "100px" },
            //{ field: "RegionId", title: "Region Id", width: "85px", hidden: true },
            //{ template: urlRegionCode, field: "RegionCode", title: "Region Code", width: "100px", hidden: true },
            //{ template: urlRegionName, field: "RegionName", title: "Region Name", width: "150px" },
            {
                template: "# if (AreaStatus == 'A' ) { # #=AreaName# # } else { # <span class='span_button'>#=AreaName#</span> #} #",
                field: "", title: "Area Name", width: "110px"
            },
            { template: "<a href=\"" + url + "\">#=AsmTel1#</a>", field: "", title: "Mobile", width: "90px" },
            //{ field: "RsmTel2", title: "Tel 2", width: "100px", hidden: true },
            { template: "<a href=\"" + url + "\">#=AsmEmail#</a>", field: "", title: "Email", width: "120px" },
            { template: '<a href="javascript:DeleteASM(\'#=AsmId#\');">Delete</a>', field: "", width: "70px" },
        ],
        editable: false,
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        detailTemplate: kendo.template($("#templateASM").html()),
        detailInit: detailASMInfoByAsmId,
        detailExpand: detailExpand,
        dataBound: onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { //Define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        AsmId: { validation: { required: true } },
                        AsmCode: { editable: false, nullable: true },
                        AsmName: { editable: false, nullable: true },
                        AsmUserName: { editable: false, nullable: true },
                        AreaId: { editable: false, nullable: true },
                        AreaCode: { editable: false, nullable: true },
                        AreaName: { editable: false, nullable: true },
                        AsmTel1: { editable: false, nullable: true },
                        AsmTel2: { editable: false, nullable: true },
                        AsmEmail: { editable: false, nullable: true },
                        IsNotIMEIuser: { editable: false, nullable: true }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/master/master.asmx/GetAllASM", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#gridAllASM").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models });
                    }
                    else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}

function detailASMInfoByAsmId(e) {
    var detailRow = e.detailRow;

    detailRow.find(".tabstrip").kendoTabStrip({
        animation: {
            open: { effects: "fadeIn" }
        }
    });

    detailLoadTerritoryByAsmId(detailRow, e);
    detailLoadSRByAsmId(detailRow, e);
    detailLoadIMEIByAsmId(detailRow, e);
}

function detailLoadSRByAsmId(detailRow, e) {

    console.log(e.data.AreaId);

    var take_grid = $("#MainContent_hfPageIndex").val();

    var url = "javascript:EditSR('#=RepId#','#=RepCode#','#=RepName#','#=UserName#', '#=TerritoryId#','#=Mobile#','','');";

    var urlRepCode = "<a href=\"" + url + "\">#=RepCode#</a>";
    var urlRepName = "<a href=\"" + url + "\">#=RepName#</a>";

    var subgrid = detailRow.find(".asm-sr").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { //Define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        RepId: { validation: { required: true } },
                        RepCode: { editable: false, nullable: true },
                        RepName: { editable: false, nullable: true },
                        UserName: { editable: false, nullable: true },
                        Mobile: { editable: false, nullable: true }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/master/master.asmx/GetAllSRByAreaId", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid, AreaId: e.data.AreaId },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models });
                    }
                    else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            filter: { field: "AreaId", operator: "eq", value: e.data.AreaId }
        },
        editable: false,
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        toolbar: [{ text: "Refresh", className: "btn-refresh" }],
        columns: [
            { field: "RepId", title: "Rep Id", width: "85px", hidden: true },
            { /*template: urlRepCode,*/ field: "RepCode", title: "Rep Code", width: "105px" },
            { /*template: urlRepName,*/ field: "RepName", title: "Rep Name", width: "150px" },
            {
                template: "# if (TerritoryStatus == 'A' ) { # #=TerritoryName# # } else { # <span class='span_button'>#=TerritoryName#</span> #} #",
                field: "", title: "Territory Name",
                width: "110px"
            },
            //{ template: '<a href="javascript:Remove(\'#=RsmId#\');">Remove</a>', field: "", width: "70px" }
        ],
    });
    //expand and collapse rows on click
    subgrid.on('click', 'tr', function () {
        if ($(this).hasClass("snir-expanded")) {
            $(subgrid).data().kendoGrid.collapseRow($(this));
            $(this).removeClass("snir-expanded");
            $(this).addClass("snir-collapsed");
        }
        else {
            $(subgrid).data().kendoGrid.expandRow($(this));
            $(this).addClass("snir-expanded");
            $(this).removeClass("snir-collapsed");
        }
    })
}

function detailLoadTerritoryByAsmId(detailRow, e) {

    console.log(e.data.AreaId);

    var take_grid = $("#MainContent_hfPageIndex").val();

    var url = "javascript:EditTerritory('#=TerritoryId#','#=AreaId#','#=TerritoryCode#','#=TerritoryName#');";

    var urlTerritoryCode = "<a href=\"" + url + "\">#=TerritoryCode#</a>";
    var urlTerritoryName = "<a href=\"" + url + "\">#=TerritoryName#</a>";

    var subgridTerritory = detailRow.find(".asm-territory").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { //Define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        TerritoryId: { validation: { required: true } },
                        TerritoryCode: { editable: false, nullable: true },
                        TerritoryName: { editable: false, nullable: true },
                        AreaId: { editable: false, nullable: true }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/master/master.asmx/GetAllTerritoryByAreaId", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid, AreaId: e.data.AreaId },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models });
                    }
                    else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            filter: { field: "AreaId", operator: "eq", value: e.data.AreaId }
        },
        scrollable: false,
        sortable: true,
        pageable: true,
        toolbar: [{ text: "Refresh", className: "btn-refresh" }],
        columns: [
            { field: "TerritoryId", title: "Territory Id", width: "85px", hidden: true },
            { field: "TerritoryCode", title: "Territory Code", width: "105px" },
            { field: "TerritoryName", title: "Territory Name", width: "200px" },
            //{ template: '<a href="javascript:RemoveTerritoryFromArea(\'#=TerritoryId#\');">Remove</a>', field: "", width: "70px" }
        ],
    });
    //expand and collapse rows on click
    subgridTerritory.on('click', 'tr', function () {
        if ($(this).hasClass("snir-expanded")) {
            $(subgrid).data().kendoGrid.collapseRow($(this));
            $(this).removeClass("snir-expanded");
            $(this).addClass("snir-collapsed");
        } else {
            $(subgrid).data().kendoGrid.expandRow($(this));
            $(this).addClass("snir-expanded");
            $(this).removeClass("snir-collapsed");
        }
    })
}

function detailLoadIMEIByAsmId(detailRow, e) {

    console.log(e.data.AsmId);

    var take_grid = $("#MainContent_hfPageIndex").val();

    var subgrid = detailRow.find(".asm-imei").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { //Define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        id: { validation: { required: true } },
                        mobile_no: { editable: false, nullable: true },
                        emei_no: { editable: false, nullable: true },
                        apk_version: { editable: false, nullable: true }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/master/master.asmx/GetAllOriginatorImeiByAsmId", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid, AsmId: e.data.AsmId },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models });
                    }
                    else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            filter: { field: "AsmId", operator: "eq", value: e.data.AsmId }
        },
        editable: false,
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        toolbar: [{ text: "Refresh", className: "btn-refresh" }],
        columns: [
            { field: "id", title: "Id", width: "85px", hidden: true },
            { field: "mobile_no", title: "Mobile", width: "105px", hidden: true },
            {
                template: "# if (status == 'A' ) { # #=emei_no# # } else { # <span class='span_button'>#=emei_no#</span> #} #",
                field: "", title: "IMEI", width: "150px"
            },
            { field: "apk_version", title: "Apk Version", width: "150px" },
            { field: "last_sync_date", title: "Last Sync Date", width: "150px" },
            { template: '<a href="javascript:DeleteIMEIByAsmId(' + e.data.AsmId + ', \'#=emei_no#\');">Delete</a>', field: "", width: "70px" }
        ],
    });
    //expand and collapse rows on click
    subgrid.on('click', 'tr', function () {
        if ($(this).hasClass("snir-expanded")) {
            $(subgrid).data().kendoGrid.collapseRow($(this));
            $(this).removeClass("snir-expanded");
            $(this).addClass("snir-collapsed");
        }
        else {
            $(subgrid).data().kendoGrid.expandRow($(this));
            $(this).addClass("snir-expanded");
            $(this).removeClass("snir-collapsed");
        }
    })
}

function EditASM(id, code, name, username,
    areaid, tel1, tel2, email) {

    if (tel1) {
        tel1 = '';
    }
    if (tel2) {
        tel2 = '';
    }
    if (email) {
        email = '';
    }

    $("#hdnSelectedAsmId").val(id);
    $("#txtasmcode").val(code);
    $("#txtasmname").val(name);
    $("#txtasmusername").val(username);
    $("#txtasmtel1").val(tel1);
    $("#txtasmemail").val(email);
    $("#MainContent_ddlArea").val(areaid);

    $("#windowASM").css("display", "block");
    var wnd = $("#windowASM").kendoWindow({
        title: "Add/Edit Asm",
        modal: true,
        visible: false,
        resizable: false
    }).data("kendoWindow");

    $("#txtasmcode").focus();

    wnd.center().open();
}

function DeleteASM(asm_id) {
    debugger;
    var r = confirm("Do you want to delete this Asm?");
    if (r == true) {
        DeleteAsmByAsmId(asm_id);
    }
}

function DeleteAsmByAsmId(asm_id) {
    var root = ROOT_PATH + "service/master/master.asmx/DeleteAsmByAsmId";

    $.ajax({
        type: "POST",
        url: root,
        data: '{ asm_id: "' + asm_id + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess,
        failure: OnFail
    });
}

function DeleteIMEIByAsmId(asm_id, emei_no) {
    var root = ROOT_PATH + "service/master/master.asmx/DeleteIMEIByAsmId";

    $.ajax({
        type: "POST",
        url: root,
        data: '{ asm_id: "' + asm_id + '", ' +
            ' emei_no: "' + emei_no + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess,
        failure: OnFail
    });
}

function SaveAsm() {
    var e = document.getElementById("MainContent_ddlArea");
    var selectedAreaId = e.options[e.selectedIndex].value;

    var originator = $("#MainContent_hfOriginator").val();
    var selectedAsmId = $("#hdnSelectedAsmId").val();

    if (($("#txtasmcode").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please enter a Asm Code.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else if (($("#txtasmname").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please enter a Asm Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else if (($("#txtasmusername").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please enter a User Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else if (selectedAreaId == '') {
        var sucessMsg = GetErrorMessageDiv("Please select area.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else {
        //var brandCode = $("#txtCode").val();
        var asmCode = $("#txtasmcode").val();
        var asmName = $("#txtasmname").val();
        var asmUserName = $("#txtasmusername").val();
        var asmTel1 = $("#txtasmtel1").val();
        var asmEmail = $("#txtasmemail").val();

        var url = ROOT_PATH + "service/master/master.asmx/SaveAsm"

        var data = '{ area_id: "' + selectedAreaId + '", ' +
            ' asm_id: "' + selectedAsmId + '", ' +
            ' asm_code: "' + asmCode + '", ' +
            ' asm_name: "' + asmName + '", ' +
            ' asm_username: "' + asmUserName + '", ' +
            ' asm_tel1: "' + asmTel1 + '", ' +
            ' asm_email: "' + asmEmail + '", ' +
            ' created_by: "' + originator + '" }';

        console.log(data);

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            contentType: "application/json;",
            dataType: "json",
            success: function (msg) {
                if (msg.d.Status == true) {
                    var sucessMsg = GetSuccesfullMessageDiv("Asm was successfully Saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    closepopup();
                    LoadAllASM();
                    hideStatusDiv("MainContent_div_message");
                }
                else {
                    var errorMsg = GetErrorMessageDiv("Error occurred !.", "MainContent_div_message");

                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(errorMsg);
                    closepopup();
                    hideStatusDiv("MainContent_div_message");
                }
            },
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}

//#endregion RSM

//#region SR Functions

function EditSR(id, code, name, username,
    territoryid, tel1, tel2, email) {

    if (tel1) {
        tel1 = '';
    }
    if (tel2) {
        tel2 = '';
    }
    if (email) {
        email = '';
    }

    $("#hdnSelectedRepId").val(id);
    $("#txtsrcode").val(code);
    $("#txtsrname").val(name);
    $("#txtsrusername").val(username);
    $("#txtsrtel1").val(tel1);
    $("#txtsremail").val(email);
    $("#MainContent_ddlTerritory").val(territoryid);

    $("#windowSR").css("display", "block");
    var wnd = $("#windowSR").kendoWindow({
        title: "Add/Edit SR",
        modal: true,
        visible: false,
        resizable: false
    }).data("kendoWindow");

    $("#txtsrcode").focus();

    wnd.center().open();
}

function SaveSR() {
    var e = document.getElementById("MainContent_ddlArea");
    var selectedAreaId = e.options[e.selectedIndex].value;

    var originator = $("#MainContent_hfOriginator").val();
    var selectedAsmId = $("#hdnSelectedAsmId").val();

    if (($("#txtasmcode").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please enter a Asm Code.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else if (($("#txtasmname").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please enter a Asm Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else if (($("#txtasmusername").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please enter a User Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else if (selectedAreaId == '') {
        var sucessMsg = GetErrorMessageDiv("Please select area.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else {
        //var brandCode = $("#txtCode").val();
        var asmCode = $("#txtasmcode").val();
        var asmName = $("#txtasmname").val();
        var asmUserName = $("#txtasmusername").val();
        var asmTel1 = $("#txtasmtel1").val();
        var asmEmail = $("#txtasmemail").val();

        var url = ROOT_PATH + "service/master/master.asmx/SaveAsm"

        var data = '{ area_id: "' + selectedAreaId + '", ' +
            ' asm_id: "' + selectedAsmId + '", ' +
            ' asm_code: "' + asmCode + '", ' +
            ' asm_name: "' + asmName + '", ' +
            ' asm_username: "' + asmUserName + '", ' +
            ' asm_tel1: "' + asmTel1 + '", ' +
            ' asm_email: "' + asmEmail + '", ' +
            ' created_by: "' + originator + '" }';

        console.log(data);

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            contentType: "application/json;",
            dataType: "json",
            success: function (msg) {
                if (msg.d.Status == true) {
                    var sucessMsg = GetSuccesfullMessageDiv("Asm was successfully Saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    closepopup();
                    LoadAllASM();
                    hideStatusDiv("MainContent_div_message");
                }
                else {
                    var errorMsg = GetErrorMessageDiv("Error occurred !.", "MainContent_div_message");

                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(errorMsg);
                    closepopup();
                    hideStatusDiv("MainContent_div_message");
                }
            },
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}

function DeleteSRFromTerritory(territoryId, repId) {
    var r = confirm("Do you want to remove this Rep from Territory?");
    if (r == true) {
        DeleteConfirmSRFromTerritory(territoryId, repId);
    }
}

function DeleteConfirmSRFromTerritory(territoryId, repId) {
    var root = ROOT_PATH + "service/master/master.asmx/DeleteConfirmSRFromTerritory";

    $.ajax({
        type: "POST",
        url: root,
        data: '{ territoryId : "' + territoryId + '", repId : "' + repId + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: function OnSuccess(response) {
            LoadAllTerritories();
        },
        failure: OnFail
    });
}

//#endregion SR

//#region Distributer Functions

function LoadAllTerritoryByDistributer(DistId) {
    var take_grid = $("#MainContent_hfPageIndex").val();

    $("#div_loader").show();
    $("#gridDistributerTerritory").html("");

    var tblAllterritory = $("#gridDistributerTerritory").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { //Define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        AreaId: { validation: { required: true } },
                        AreaCode: { validation: { required: true } },
                        AreaName: { validation: { required: true } },
                        AreaStatus: { validation: { required: true } },
                        TerritoryId: { validation: { required: true } },
                        TerritoryCode: { editable: false, nullable: true },
                        TerritoryName: { editable: false, nullable: true },
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/master/master.asmx/GetAllTerritoryByDistributerId", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid, DistributerId: DistId },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#gridDistributerTerritory").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models });
                    }
                    else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        },
        height: 150,
        editable: "popup",
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        columns: [
            { field: "TerritoryId", title: "Territory Id", width: "85px", hidden: true },
            { field: "TerritoryCode", title: "Territory Code", width: "120px" },
            { field: "TerritoryName", title: "Territory Name", width: "220px" },
            { field: "AreaName", title: "Area Name", width: "220px" },
            { template: '<a href="javascript:DeleteTerritory(\'#=TerritoryId#\');">Remove</a>', field: "", width: "160px" }
        ]
    });
}

//#endregion Distributer

//#region Route Functions

function LoadAllRoutes() {
    
    var url = "javascript:EditRouteMaster('#=RouteMasterId#','#=RouteMasterCode#','#=RouteMasterName#','#=TerritoryId#');"
    //var showOutlets = "javascript:ShowOutletsWindowForRep('#=RouteMasterId#','#=RouteName#','#=RepCode#','#=RepName#');";

    var deleteUrl = "javascript:DeleteRouteMaster('#=RouteMasterId#');"

    var take_grid = $("#MainContent_hfPageIndex").val();
    var urlRouteMasterCode = "<a href=\"" + url + "\">#=RouteMasterCode#</a>";
    var urlRouteMasterName = "<a href=\"" + url + "\">#=RouteMasterName#</a>";

    //$("#div_loader").show();
    $("#gridAllRoutes").html("");
    $("#gridAllRoutes").kendoGrid({
        height: 500,
        columns: [
            { field: "RouteMasterId", title: "Route Id", width: "85px", hidden: true },
            //{ template: "<a href=\"" + showOutlets + "\">Add Outlets</a>", field: "", width: "75px", hidden: false }, 
            { template: urlRouteMasterCode, field: "RouteMasterCode", title: "Route Code", width: "120px" },
            { template: urlRouteMasterName, field: "RouteMasterName", title: "Route Name", width: "220px" },
            { field: "TerritoryName", title: "Territory", width: "220px" },
            //{ field: "RepName", title: "Assigned To", width: "300px", hidden: false },
            //{ field: "CallCycleId", title: "CallcycleId", width: "50px", hidden: true },
            //{ field: "Status", title: "Status", width: "105px" },
            { template: "<a href=\"" + deleteUrl + "\">Delete</a>", width: "50px" },
        ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        filterable: true,
        selectable: "single",
        columnMenu: false,
        detailTemplate: kendo.template($("#templateRoute").html()),
        detailInit: detailInit_callcycleOutletsInRoute,
        detailExpand: detailExpand,
        dataBound: onDataBound,
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 15,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        RouteMasterId: { validation: { required: true } },
                        RouteMasterCode: { editable: true, nullable: true },
                        RouteMasterName: { editable: true, nullable: true },
                        //RepCode: { editable: true, nullable: true },
                        //RepName: { editable: true, nullable: true },
                        TerritoryId: { editable: true, nullable: true },
                        TerritoryName: { editable: true, nullable: true },
                        //Status: { validation: { required: true } }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/routes/routes.asmx/GetAllRouteMaster", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { pgindex: take_grid },
                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                    complete: function (jqXHR, textStatus) {
                        if (textStatus == "success") {
                            $("#div_loader").hide();
                            var entityGrid = $("#gridAllRoutes").data("kendoGrid");
                            if ((take_grid != '') && (gridindex == '0')) {
                                entityGrid.dataSource.page((take_grid - 1));
                                gridindex = '1';
                            }
                        }
                    }
                },
                parameterMap: function (data, operation) {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    });
}

function detailInit_callcycleOutletsInRoute(e) {
    var detailRow = e.detailRow;

    detailRow.find(".tabstrip").kendoTabStrip({
        animation: {
            open: { effects: "fadeIn" }
        }
    });

    detailLoadOutletsByRouteId(detailRow, e);
}

function detailLoadOutletsByRouteId(detailRow, e) {
   
    var take_grid = $("#MainContent_hfPageIndex").val();

    var url = "javascript:EditOutlet('#=OutletCode#','#=RouteMasterId#','#=OutletName#');";

    var urlOutletCode = "<a href=\"" + url + "\">#=OutletCode#</a>";
    var urlOutletName = "<a href=\"" + url + "\">#=OutletName#</a>";

    var subgrid = detailRow.find(".route-outlet").kendoGrid({
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { //Define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        RouteMasterId: { validation: { required: true } },
                        OutletCode: { editable: false, nullable: true },
                        OutletName: { editable: false, nullable: true } //,
                        //TerritoryId: { editable: false, nullable: true }
                    }
                }
            },
            transport: {
                read: {
                    url: ROOT_PATH + "service/lead_customer/customer_entry_Service.asmx/GetAllAllCustomerByRouteMasterId",
                    contentType: "application/json; charset=utf-8", 
                    data: { pgindex: take_grid, RouteMasterId: e.data.RouteMasterId },
                    type: "POST" 
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models });
                    }
                    else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            },
            filter: { field: "RouteMasterId", operator: "eq", value: e.data.RouteMasterId }
        },
        scrollable: false,
        sortable: true,
        pageable: true,
        filterable: true,
        toolbar: [{ text: "Refresh", className: "btn-refresh" }],
        columns: [
            { field: "RouteMasterId", title: "Route Id", width: "85px", hidden: true },
            {
                template: "# if (Status == 'N' ) { # " + urlOutletCode + " # } else { # <span class='span_button'>" + urlOutletCode +"</span> #} #",
                field: "OutletCode", title: "Outlet Code",
                width: "110px"
            },
            {
                template: "# if (Status == 'N' ) { # " + urlOutletName + " # } else { # <span class='span_button'>" + urlOutletName + "</span> #} #",
                field: "OutletName", title: "Outlet Name",
                width: "110px"
            },
            { template: '<a href="javascript:DeleteOutletFromRoute(\'#=RouteMasterId#\');">Remove</a>', field: "", width: "70px" }
        ],
    });

    //expand and collapse rows on click
    subgrid.on('click', 'tr', function () {
        if ($(this).hasClass("snir-expanded")) {
            $(subgrid).data().kendoGrid.collapseRow($(this));
            $(this).removeClass("snir-expanded");
            $(this).addClass("snir-collapsed");
        } else {
            $(subgrid).data().kendoGrid.expandRow($(this));
            $(this).addClass("snir-expanded");
            $(this).removeClass("snir-collapsed");
        }
    });
}

function EditRouteMaster(route_master_id, route_master_code, route_master_name, territory_id ) {
    //console.log('areaId => ', areaId, ', regionId => ', regionId, ', areaCode => ', areaCode, ', areaName => ', areaName);
    $("#hdnSelectedRouteId").val(route_master_id);
    $("#hdnSelectedTerritoryId").val(territory_id);
    if (route_master_code == 'null')
        $("#txtRouteCode").val('');
    else
        $("#txtRouteCode").val(route_master_code);
    $("#txtRouteName").val(route_master_name);
    $("#MainContent_ddlTerritory").val(territory_id);
    $("#hdnSelectedRouteCode").val(route_master_code);

    $("#windowRoute").css("display", "block");
    var wnd = $("#windowRoute").kendoWindow({
        title: "Edit Route",
        modal: true,
        visible: false,
        resizable: false
    }).data("kendoWindow");

    $("#txtRouteCode").focus();

    wnd.center().open();
}

function SaveRoute() {
    var e = document.getElementById("MainContent_ddlTerritory");
    var selectedTerritoryId = e.options[e.selectedIndex].value;

    var originator = $("#MainContent_hfOriginator").val();
    var originatorType = $("#MainContent_hfOriginatorType").val();
    var selectedRouteId = $("#hdnSelectedRouteId").val();
    var selectedRouteCode = $("#hdnSelectedRouteCode").val();

    if (($("#txtRouteCode").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please enter a Route Code.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else if (($("#txtRouteName").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please enter a Route Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else if (selectedTerritoryId == '') {
        var sucessMsg = GetErrorMessageDiv("Please select territory.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else {
        //var brandCode = $("#txtCode").val();
        var routeCode = $("#txtRouteCode").val();
        var routeName = $("#txtRouteName").val();

        var url = ROOT_PATH + "service/routes/routes.asmx/SaveRoute"

        var data = '{ route_id: "' + selectedRouteId + '", ' +
            ' territory_id: "' + selectedTerritoryId + '", ' +
            ' route_code_old: "' + selectedRouteCode + '", ' +
            ' route_code: "' + routeCode + '", ' +
            ' route_name: "' + routeName + '", ' +
            ' created_by: "' + originator + '" }';

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            contentType: "application/json;",
            dataType: "json",
            success: function (msg) {
                if (msg.d.Status == true) {
                    var sucessMsg = GetSuccesfullMessageDiv("Route was successfully Saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    closepopupRoute();
                    LoadAllRoutes();
                    hideStatusDiv("MainContent_div_message");
                }
                else if (msg.d.StatusCode == "Exsist") {
                    var errorMsg = GetErrorMessageDiv("Route code " + routeCode + " already exsists.", "MainContent_div_message");

                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(errorMsg);
                    closepopup();
                    hideStatusDiv("MainContent_div_message");
                }
                else {
                    var errorMsg = GetErrorMessageDiv("Error occurred !.", "MainContent_div_message");

                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(errorMsg);
                    closepopup();
                    hideStatusDiv("MainContent_div_message");
                }
            },
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}

function SaveRouteFromTerritory() {
    var e = document.getElementById("MainContent_ddlTerritory");
    var selectedTerritoryId = e.options[e.selectedIndex].value;

    var originator = $("#MainContent_hfOriginator").val();
    var originatorType = $("#MainContent_hfOriginatorType").val();
    var selectedRouteId = $("#hdnSelectedRouteId").val();
    var selectedRouteCode = $("#hdnSelectedRouteCode").val();

    if (($("#txtRouteCode").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please enter a Route Code.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
       // hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else if (($("#txtRouteName").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please enter a Route Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else if (selectedTerritoryId == '') {
        var sucessMsg = GetErrorMessageDiv("Please select territory.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else {
        //var brandCode = $("#txtCode").val();
        var routeCode = $("#txtRouteCode").val();
        var routeName = $("#txtRouteName").val();

        var url = ROOT_PATH + "service/routes/routes.asmx/SaveRoute"

        var data = '{ route_id: "' + selectedRouteId + '", ' +
            ' territory_id: "' + selectedTerritoryId + '", ' +
            ' route_code_old: "' + selectedRouteCode + '", ' +
            ' route_code: "' + routeCode + '", ' +
            ' route_name: "' + routeName + '", ' +
            ' created_by: "' + originator + '" }';

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            contentType: "application/json;",
            dataType: "json",
            success: function (msg) {
                if (msg.d.Status == true) {
                    var sucessMsg = GetSuccesfullMessageDiv("Route was successfully Saved.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    closepopupRoute();
                    LoadAllTerritories();
                    hideStatusDiv("MainContent_div_message");
                }
                else if (msg.d.StatusCode == "Exsist") {
                    var errorMsg = GetErrorMessageDiv("Route code " + routeCode + " already exsists.", "MainContent_div_message");

                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(errorMsg);
                    //closepopupRoute();
                    hideStatusDiv("MainContent_div_message");
                }
                else {
                    var errorMsg = GetErrorMessageDiv("Error occurred !.", "MainContent_div_message");

                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(errorMsg);
                    //closepopupRoute();
                    hideStatusDiv("MainContent_div_message");
                }
            },
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}

function DeleteRouteMaster(route_master_id) {

    var r = confirm("Do you want to delete this Route?");
    if (r == true) {
        DeleteRouteMasterById(route_master_id);
    }
}

function DeleteRouteMasterById(route_master_id) {
    var root = ROOT_PATH + "service/routes/routes.asmx/DeleteRouteMasterByRouteId";

    $.ajax({
        type: "POST",
        url: root,
        data: '{ routeId: "' + route_master_id + '" }',
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        success: OnSuccess,
        failure: OnFail
    });
}
//#endregion Route

//#region "Outlets"

function EditOutlet(OutletCode, RouteMasterId, OutletName) {
    //console.log('areaId => ', areaId, ', regionId => ', regionId, ', areaCode => ', areaCode, ', areaName => ', areaName);
    $("#txtOutletCode").val(OutletCode);
    $("#txtOutletName").val(OutletName);
    $("#MainContent_ddlRoute").val(RouteMasterId);

    $("#windowOutlet").css("display", "block");
    var wnd = $("#windowOutlet").kendoWindow({
        title: "Edit Route",
        modal: true,
        visible: false,
        resizable: false
    }).data("kendoWindow");

    $("#txtOutletCode").focus();

    wnd.center().open();
}

function UpdateOutletInRouteMaster() {
    var e = document.getElementById("MainContent_ddlRoute");
    var selectedRouteId = e.options[e.selectedIndex].value;

    var originator = $("#MainContent_hfOriginator").val();
    var originatorType = $("#MainContent_hfOriginatorType").val();

    if (($("#txtOutletCode").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please enter a Outlet Code.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else if (($("#txtOutletName").val()).trim() == '') {
        var sucessMsg = GetErrorMessageDiv("Please enter a Outlet Name.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else if (selectedRouteId == '') {
        var sucessMsg = GetErrorMessageDiv("Please select route.", "MainContent_div_message_popup");
        $('#MainContent_div_message_popup').css('display', 'block');
        $("#MainContent_div_message_popup").html(sucessMsg);
        hideStatusDiv("MainContent_div_message_popup");
        return;
    }
    else {
        //var brandCode = $("#txtCode").val();
        var outletCode = $("#txtOutletCode").val();
        var outletName = $("#txtOutletName").val();

        var url = ROOT_PATH + "service/routes/routes.asmx/SaveRoute"

        var data = '{ route_id: "' + selectedRouteId + '", ' +
            ' outlet_code: "' + outletCode + '", ' +
            ' outlet_name: "' + outletName + '", ' +
            ' created_by: "' + originator + '" }';

        alert('Please use outlet master page | Outlet bulk transfer to update outlet current route ');

        $.ajax({
            type: "POST",
            url: url,
            data: data,
            contentType: "application/json;",
            dataType: "json",
            success: function (msg) {
                //if (msg.d.Status == true) {
                //    var sucessMsg = GetSuccesfullMessageDiv("Outlet was successfully Saved.", "MainContent_div_message");
                //    $('#MainContent_div_message').css('display', 'block');
                //    $("#MainContent_div_message").html(sucessMsg);
                //    closepopupRoute();
                //    LoadAllRoutes();
                //    hideStatusDiv("MainContent_div_message");
                //}
                //else if (msg.d.StatusCode == "Exsist") {
                //    var errorMsg = GetErrorMessageDiv("Route code " + routeCode + " already exsists.", "MainContent_div_message");

                //    $('#MainContent_div_message').css('display', 'block');
                //    $("#MainContent_div_message").html(errorMsg);
                //    closepopup();
                //    hideStatusDiv("MainContent_div_message");
                //}
                //else {
                //    var errorMsg = GetErrorMessageDiv("Error occurred !.", "MainContent_div_message");

                //    $('#MainContent_div_message').css('display', 'block');
                //    $("#MainContent_div_message").html(errorMsg);
                //    closepopup();
                //    hideStatusDiv("MainContent_div_message");
                //}
            },
            error: function (msg, textStatus) {
                if (textStatus == "error") {
                    var msg = msg;
                }
            }
        });
    }
}

//#endregion







































//#region Region Functions

////Load All Regions 
//function LoadAllRegions() {
//    var take_grid = $("#MainContent_hfPageIndex").val();
//    var url = "javascript:UpdateRegion('#=RegionId#','#=RegionCode#','#=RegionName#');";

//    var urlRCode = "<a href=\"" + url + "\">#=RegionCode#</a>";
//    var urlRName = "<a href=\"" + url + "\">#=RegionName#</a>";

//    $("#div_loader").show();
//    $("#divRegions").html("");
//    $("#divRegions").kendoGrid({
//        height: 375,
//        columns: [
//            { field: "RegionId", title: "Region Id", width: "85px", hidden: true },
//            { template: urlRCode, field: "RegionCode", title: "Region Code", width: "85px" },
//            { template: urlRName, field: "RegionName", title: "Region Name", width: "250px" },
//            { template: '<a href="javascript:DeleteRegion(\'#=RegionId#\');">Delete</a>', field: "", width: "40px" }
//        ],
//        editable: false,
//        pageable: true,
//        sortable: true,
//        filterable: true,
//        selectable: "single",
//        columnMenu: false,
//        detailTemplate: kendo.template($("#templateRegion").html()),
//        detailInit: detailInit,
//        dataBound: function () {
//            this.expandRow(this.tbody.find("tr.k-master-row").first());
//        },
//        dataSource: {
//            serverSorting: true,
//            serverPaging: true,
//            serverFiltering: true,
//            pageSize: 10,
//            schema: {
//                data: "d.Data", //ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
//                total: "d.Total",
//                model: { //Define the model of the data source. Required for validation and property types.
//                    //id: "SourceId",
//                    fields: {
//                        RegionId: { validation: { required: true } },
//                        RegionCode: { editable: false, nullable: true },
//                        RegionName: { editable: false, nullable: true },
//                    }
//                }
//            },
//            transport: {
//                read: {
//                    url: , //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
//                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
//                    data: { pgindex: take_grid },
//                    type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
//                    complete: function (jqXHR, textStatus) {
//                        if (textStatus == "success") {
//                            $("#div_loader").hide();
//                            var entityGrid = $("#divRegions").data("kendoGrid");
//                            if ((take_grid != '') && (gridindex == '0')) {
//                                entityGrid.dataSource.page((take_grid - 1));
//                                gridindex = '1';
//                            }
//                        }
//                    }
//                },
//                parameterMap: function (data, operation) {
//                    if (operation != "read") {
//                        return JSON.stringify({ products: data.models })
//                    } else {
//                        data = $.extend({ sort: null, filter: null }, data);
//                        return JSON.stringify(data);
//                    }
//                }
//            }
//        }
//    });
//}

//function detailInit(e) {
//    var detailRow = e.detailRow;

//    detailRow.find(".tabstrip").kendoTabStrip({
//        animation: {
//            open: { effects: "fadeIn" }
//        }
//    });

//    //detailRow.find(".region-areas-info").kendoGrid({
//    //    dataSource: {
//    //        type: "odata",
//    //        transport: {
//    //            read: ROOT_PATH + "service/master/master.asmx/GetAllRSM"
//    //        },
//    //        serverPaging: true,
//    //        serverSorting: true,
//    //        serverFiltering: true,
//    //        pageSize: 7//,
//    //        //filter: { field: "EmployeeID", operator: "eq", value: e.data.EmployeeID }
//    //    },
//    //    scrollable: false,
//    //    sortable: true,
//    //    pageable: true,
//    //    columns: [
//    //        { field: "RsmId", title: "ID", width: "70px" },
//    //        { field: "RsmCode", title: "Ship Country", width: "110px" },
//    //        { field: "RsmName", title: "Ship Address" },
//    //        //{ field: "ShipName", title: "Ship Name", width: "300px" }
//    //    ]
//    //});
//}

///*Update Region Entry Page*/
//function UpdateRegion(id, code, name) {
//    $("#hdnSelectedRegionId").val(id);
//    $("#txtregioncode").val(code);
//    $("#txtregionname").val(name);

//    $("#window").css("display", "block");
//    var wnd = $("#window").kendoWindow({
//        title: "Add/Edit Region",
//        modal: true,
//        visible: false,
//        resizable: false
//    }).data("kendoWindow");

//    $("#txtregioncode").focus();

//    wnd.center().open();
//}

///*Delete Region Entry Page*/
//function DeleteRegion(aID) {
//    debugger;
//    var r = confirm("Do you want to delete this region?");
//    if (r == true) {
//        DeleteRegionByRegionId(aID);
//    }
//}

//function DeleteRegionByRegionId(region_id) {
//    var root = ROOT_PATH + "service/master/master.asmx/DeleteRegion";

//    $.ajax({
//        type: "POST",
//        url: root,
//        data: '{region_id: "' + region_id + '" }',
//        contentType: "application/json; charset=utf-8",
//        dataType: "json",
//        success: OnSuccess,
//        failure: OnFail
//    });

//    function OnFail(response) {
//        var existsMsg = GetErrorMessageDiv("Error occured !.", "MainContent_div_message_popup");
//        $('#MainContent_div_message_popup').css('display', 'block');
//        $("#MainContent_div_message_popup").html(existsMsg);
//        closepopup();
//        hideStatusDiv("MainContent_div_message_popup");
//    }

//    function OnSuccess(response) {
//        var sucessMsg = GetSuccesfullMessageDiv("Region was successfully Deleted.", "MainContent_div_message");
//        $('#MainContent_div_message').css('display', 'block');
//        $("#MainContent_div_message").html(sucessMsg);
//        closepopup();
//        LoadAllRegions();
//        hideStatusDiv("MainContent_div_message");
//    }
//}

//function SaveRegion() {
//    if (($("#txtregioncode").val()).trim() == '') {
//        var sucessMsg = GetErrorMessageDiv("Please Enter a Region Code.", "MainContent_div_message_popup");
//        $('#MainContent_div_message_popup').css('display', 'block');
//        $("#MainContent_div_message_popup").html(sucessMsg);
//        hideStatusDiv("MainContent_div_message_popup");
//        return;
//    }
//    else if (($("#txtregionname").val()).trim() == '') {
//        var sucessMsg = GetErrorMessageDiv("Please Enter a Region Name.", "MainContent_div_message_popup");
//        $('#MainContent_div_message_popup').css('display', 'block');
//        $("#MainContent_div_message_popup").html(sucessMsg);
//        hideStatusDiv("MainContent_div_message_popup");
//        return;
//    }
//    else {
//        //var brandCode = $("#txtCode").val();
//        var regionCode = $("#txtregioncode").val();
//        var regionName = $("#txtregionname").val();
//        var selectedRegionId = $("#hdnSelectedRegionId").val();
//        var selectedCompId = $("#hdnSelectedCompId").val();

//        var url = ROOT_PATH + "service/master/master.asmx/SaveRegion"

//        $.ajax({
//            type: "POST",
//            url: url,
//            data: '{ comp_id: "' + selectedCompId + '", ' +
//                ' region_id: "' + selectedRegionId + '", ' +
//                ' region_code: "' + regionCode + '", ' +
//                ' region_name: "' + regionName + '" }',
//            contentType: "application/json; charset=utf-8",
//            dataType: "json",
//            success: function (msg) {
//                if (msg.d.Status == true) {
//                    var sucessMsg = GetSuccesfullMessageDiv("Region was successfully Saved.", "MainContent_div_message");
//                    $('#MainContent_div_message').css('display', 'block');
//                    $("#MainContent_div_message").html(sucessMsg);
//                    closepopup();
//                    LoadAllRegions();
//                    hideStatusDiv("MainContent_div_message");
//                }
//                //else if (msg.status == "nameExists") {
//                //    var existsMsg = GetErrorMessageDiv("Region Name already exists in system!.", "MainContent_div_message_popup");

//                //    $('#MainContent_div_message_popup').css('display', 'block');
//                //    $("#MainContent_div_message_popup").html(existsMsg);
//                //    //closepopup();
//                //    hideStatusDiv("MainContent_div_message_popup");
//                //}
//                else {
//                    var errorMsg = GetErrorMessageDiv("Error occurred !.", "MainContent_div_message");

//                    $('#MainContent_div_message').css('display', 'block');
//                    $("#MainContent_div_message").html(errorMsg);
//                    closepopup();
//                    hideStatusDiv("MainContent_div_message");
//                }
//            },
//            error: function (msg, textStatus) {
//                if (textStatus == "error") {
//                    var msg = msg;
//                }
//            }
//        });
//    }
//}

//#endregion