﻿<%@ Page Title="mSales - Stock Maintanance" Language="C#" AutoEventWireup="true"
    CodeFile="loading_stocks.aspx.cs" MasterPageFile="~/Site.master" Inherits="loading_stocks_transaction_loading_stocks" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField runat="server" ID="HiddenFieldStockGrid" />
    <asp:HiddenField runat="server" ID="HiddenFieldAssignedRouteId" />
    <asp:HiddenField runat="server" ID="HiddenFieldRouteMasterID" />
    <asp:HiddenField runat="server" ID="HiddenFieldRouteName" />
    <asp:HiddenField runat="server" ID="HiddenFieldRepCode" />
    <asp:HiddenField runat="server" ID="HiddenFieldRepName" />
    <asp:HiddenField runat="server" ID="HiddenFieldOriginator" />
    <asp:HiddenField runat="server" ID="HiddenFieldLoadMiles" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="HiddenFieldPacks" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="HiddenFieldSKU" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="HiddenFieldLoadUID" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="HiddenFieldUnLoadUID" ClientIDMode="Static" />
    <asp:HiddenField runat="server" ID="HiddenFieldTempRepName" />
    <asp:HiddenField runat="server" ID="HiddenFieldOriginalRepName" />
    <asp:HiddenField runat="server" ID="HiddenFieldGridDataChanged" Value="0" />
    <asp:HiddenField runat="server" ID="HiddenFieldDistributorName" />
    <asp:HiddenField runat="server" ID="HiddenFieldDistributorID" />
    <asp:HiddenField runat="server" ID="HiddenFieldDistributorUserName" />

    <asp:HiddenField ID="hfOriginator" runat="server" />
    <asp:HiddenField ID="hfOriginatorType" runat="server" />
    <asp:HiddenField ID="hfTerritoryId" runat="server" />

    <%--<div id="stockMovementmodalWindow" style="display: none">
        <div id="div_stockMovementConfirm_message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>--%>

    <div class="divcontectmainforms" style="color: black">


        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div>
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                </div>
            </div>

            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
                        <div class="back"></div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>


        <!--<div class="clearall">
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none">
        </div>
        <div class="clearall">
        </div>-->


        <div id="stockUploadWindow" style="display: none">
            <div class="formtextdiv">
                As At Date
            </div>
            <div class="formdetaildiv_right">
                <asp:TextBox ID="dtpUploadDate" runat="server"></asp:TextBox>
                <asp:Label ID="Label1" runat="server" Text="" Font-Bold="True" Font-Size="Large"></asp:Label>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Stock File
            </div>
            <div class="formdetaildiv_right">
                <asp:FileUpload ID="fileSelect" runat="server" />
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv">
            </div>
            <div class="formdetaildiv_right">
                <a href="../../docs/templates/Stock_Upload_Sample.xlsx">Sample Upload File</a>
            </div>
        </div>
        <div id="stockWindow">
            <div class="formtextdiv">
                As At Date
            </div>
            <div class="formdetaildiv_right">
                <asp:TextBox ID="dtpCloseDate" runat="server"></asp:TextBox>
                <asp:Label ID="Label2" runat="server" Text="" Font-Bold="True" Font-Size="Large"></asp:Label>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv" id="lblTerritory">
                Territory
            </div>
            <div class="formdetaildiv_right" id="div5">
                <span class="wosub mand">
                    <%--<asp:DropDownList ID="ddlTerritory" runat="server" ClientIDMode="Static" Style="width: 300px;">
                    </asp:DropDownList>
                    <asp:Label ID="Label4" runat="server" Text="*" Font-Bold="True" Font-Size="Large"
                        ForeColor="#FF3300"></asp:Label>
                    <div runat="server" id="div_autocomplete_dist">
                    </div>--%>

                    <input type="text" class="textboxwidth" id="txtTerritoryFilter" maxlength="30" placeholder="Select Territory" />
                    <img id="img2" src="~/assets/images/box_downarrow.png" alt="popicon" runat="server" border="0" style="height: 15px; width: 15px" />
                </span>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv" id="divLblStockAllocType">
                Stock Allocation Type
            </div>
            <div class="formdetaildiv_right" id="divStockAllocType">
                <span class="wosub mand">
                    <asp:DropDownList ID="ddlStockAllocateType" runat="server" ClientIDMode="Static"
                        Style="width: 300px;">
                        <asp:ListItem Value="G" Text="GRN"></asp:ListItem>
                        <asp:ListItem Value="A" Text="Adjustment" Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                    <asp:Label ID="Label6" runat="server" Text="*" Font-Bold="True" Font-Size="Large"
                        ForeColor="#FF3300"></asp:Label>
                    <button class="k-button" id="btnLoadGrid" type="button">
                        Reload Table</button>
                </span>
            </div>
            <div class="clearall">
            </div>
            <div id="div_assignee_block" style="display: none;">
                <div class="formtextdiv" id="div1">
                    <asp:Label ID="Label3" runat="server" Text="Assignee"></asp:Label>
                </div>
                <div class="formdetaildiv_right">
                    <asp:Label ID="lblTempRep" runat="server" CssClass="assignee"></asp:Label>
                </div>
            </div>
            <div class="clearall">
            </div>
            <div class="grid_container">
                <div id="StockGrid" style="display: none;">
                </div>
                <div id="StockGridAdjusment">
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="modalWindow" style="display: none; height: 450px;">
        </div>
    </div>


    <script type="text/javascript">
        hideStatusDiv("MainContent_div_message");

        $(document).ready(function () {
            var todayDate = kendo.toString(kendo.parseDate(new Date()), 'dd-MMM-yyyy');

            $("#MainContent_dtpCloseDate").kendoDatePicker({
                format: "dd-MMM-yyyy",
                change: allocationDate_onChange,
                value: todayDate
            });

            $("#MainContent_dtpUploadDate").kendoDatePicker({
                format: "dd-MMM-yyyy",
                change: allocationDate_onChange,
                value: todayDate
            });

            function allocationDate_onChange() {
                LoadStockGrid();
            }

            var Originator = $("#<%= hfOriginator.ClientID %>").val();
            var OriginatorType = $("#<%= hfOriginatorType.ClientID %>").val();

            $("#a_add_stock").css("display", "inline-block");
            $("#MainContent_buttonbar_buttonFileUpload").css("display", "none");

            $("#a_add_stock").click(function () {
                $("#stockWindow").css("display", "none");
                $("#stockUploadWindow").css("display", "inline-block");
                $("#a_add_stock").css("display", "none");
                $("#a_add_stock_allocation").css("display", "inline-block");

                $("#MainContent_buttonbar_buttonFileUpload").css("display", "inline-block");
                $("#MainContent_buttonbar_buttinSave").css("display", "none");
            });

            $("#a_add_stock_allocation").click(function () {
                $("#stockUploadWindow").css("display", "none");
                $("#stockWindow").css("display", "inline-block");
                $("#a_add_stock_allocation").css("display", "none");
                $("#a_add_stock").css("display", "inline-block");

                $("#MainContent_buttonbar_buttinSave").css("display", "inline-block");
                $("#MainContent_buttonbar_buttonFileUpload").css("display", "none");
            });

            LoadTerritoriesforStockProduct(OriginatorType, Originator);
            LoadStockGrid();
        });

        function closepopupStockUnload() {
            var wnd = $("#window").kendoWindow({
                title: "Unload Stock",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#window").css("display", "none");
        }

        $("#btnLoadGrid").click(function () {
            LoadStockGrid();
        });

        $("#ddlStockAllocateType").change(function () {
            LoadStockGrid();
        });

        function LoadStockGrid() {
            var TerritoryId = $("#<%= hfTerritoryId.ClientID %>").val();

            var divA = document.getElementById("ddlStockAllocateType");
            var divAVal = divA.options[divA.selectedIndex].value;

            $("#StockGridAdjusment").css("display", "block");
            $("#StockGrid").css("display", "none");

            if (divAVal == 'G') {
                $("#StockGridAdjusment").css("display", "none");
                $("#StockGrid").css("display", "block");
                LoadProductStockByTerritoryIdforStockProduct($('#MainContent_dtpCloseDate').val(), TerritoryId);
            }
            else if (divAVal == 'A') {
                $("#StockGridAdjusment").css("display", "block");
                $("#StockGrid").css("display", "none");
                LoadProductStockAdjusmentByTerritoryIdforStockProduct($('#MainContent_dtpCloseDate').val(), TerritoryId);
            }
        }

        jQuery(function () {
            jQuery("#MainContent_dtpCloseDate").validate({
                expression: "if (VAL) return true; else return false;",
                message: "Please Enter The Date"
            });
        });

        //$(document).ready(function () {
        //    $("#a_add_stock").css("display", "inline-block");
        //    $("#a_add_stock").click(function () {
        //        $("#stockWindow").css("display", "inline-block");
        //        $("#ddlDistributor").css("display", "none");
        //        $("#divLblStockAllocType").css("display", "none");
        //        $("#divStockAllocType").css("display", "none");
        //        $("#divLblDivision").css("display", "none");
        //        $("#divDivision").css("display", "none");
        //        $("#MainContent_Label4").css("display", "none");
        //        $("#lblDistributor").css("display", "none");
        //    });

        //    if ($('#MainContent_HiddenFieldDistributorID').val() != '') {
        //        loadproductlist($('#MainContent_dtpCloseDate').val(), $('#MainContent_HiddenFieldDistributorID').val());
        //    }



        //    if ($("#MainContent_dtpCloseDate").val() != "" && $("#MainContent_HiddenFieldAssignedRouteId").val() != "")
        //        loadproductlist($("#MainContent_dtpCloseDate").val(), $("#MainContent_HiddenFieldAssignedRouteId").val());

        //    //Unload button click
        //    $("#buttonUnload").click(function () {
        //        StockAllocationButtomUnloadHandler();
        //    });

        //    $("#btn_insert").click(function () {
        //        insertloadrecord();
        //    });
        //});

        $("#MainContent_buttonbar_buttinSave").click(function () {
            var divA = document.getElementById("ddlStockAllocateType");
            var divAVal = divA.options[divA.selectedIndex].value;

            if (divAVal == 'G') {
                var entityGrid = $("#StockGrid").data("kendoGrid");
            }

            if (divAVal == 'A') {
                var entityGrid = $("#StockGridAdjusment").data("kendoGrid");
            }

            $("#MainContent_HiddenFieldStockGrid").val(JSON.stringify(entityGrid.dataSource.view()));
        });

        $("#btnRead").click(function () {
            if ($("#MainContent_HiddenFieldDistributorID").val() != null && $("#MainContent_HiddenFieldDistributorID").val().length > 0) {

            } else {
                $("#MainContent_txtDistPermanent").val("");
            }
        });

        //function ShowStockMovementDateChangedConfirmMg() {
        //    $("#no").css("display", "inline-block");
        //    $("#yes").css("display", "inline-block");

        //    var message = "Stock Allocation Data has been Changed, Do you want to Save before leave the page?";

        //    var wnd = $("#stockMovementmodalWindow").kendoWindow({
        //        title: "Save Stock Allocation",
        //        modal: true,
        //        visible: false,
        //        resizable: false,
        //        width: 400
        //    }).data("kendoWindow");

        //    $("#MainContent_div_message").css("display", "none");
        //    $("#div_stockMovementConfirm_message").text(message);
        //    wnd.center().open();

        //    $("#yes").click(function () {
        //        var entityGrid = $("#StockGrid").data("kendoGrid");
        //        $("#MainContent_HiddenFieldStockGrid").val(JSON.stringify(entityGrid.dataSource.view()));
        //        document.getElementById("MainContent_buttonbar_buttinSave").click();
        //        wnd.close();
        //    });

        //    $("#no").click(function () {
        //        wnd.close();
        //    });
        //}

    </script>
</asp:Content>
