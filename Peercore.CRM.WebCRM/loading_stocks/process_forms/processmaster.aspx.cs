﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using CRMServiceReference;
using Peercore.CRM.Common;

public partial class loading_stocks_process_forms_processmaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["querytype"] == "DamageReason")
        {
            Response.Expires = -1;
            Response.Clear();
            string repgroup = GetDamage();
            Response.ContentType = "text/html";
            Response.Write(repgroup);
            Response.End();
        }

        if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != "")
        {
            if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
            {
                if (Request.QueryString["type"] == "get")
                {
                    if (Request.QueryString["fm"] == "loadstock")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        SetSelectedStockAllocationDate();
                        Response.ContentType = "text/html";
                        //Response.Write(saveHeaderText);
                        Response.End();
                    }
                    //if (Request.QueryString["fm"] == "leadentry" || Request.QueryString["fm"] == "customerentry")
                    //{

                    //}
                }
                else if (Request.QueryString["type"] == "update")
                {
                    if (Request.QueryString["fm"] == "loadstock")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string saveHeaderText = UnloadStock();
                        Response.ContentType = "text/html";
                        //Response.Write(saveHeaderText);
                        Response.End();
                    }
                }
                else if (Request.QueryString["type"] == "delete")
                {
                    //if (Request.QueryString["fm"] == "document")
                    //{

                    //}
                    //else if (Request.QueryString["fm"] == "Opportunity")
                    //{

                    //}
                }
                else if (Request.QueryString["type"] == "query")
                {
                    //if (Request.QueryString["fm"] == "custenduser")
                    //{

                    //}
                }
                else if (Request.QueryString["type"] == "insert")
                {
                    //if (Request.QueryString["fm"] == "contactdetails")
                    //{

                    //}
                }
                else if (Request.QueryString["type"] == "lookup")
                {

                }
                else if (Request.QueryString["type"] == "report")
                {
                    //if (Request.QueryString["fm"] == "endusersales")
                    //{

                    //}
                }
            }
        }
    }

    private string UnloadStock()
    {
        StringBuilder sb = new StringBuilder();
        CommonClient commonClient = new CommonClient();
        string str = string.Empty;

        try
        {
            LoadStockDetailDTO loadStockDetailDTO = new LoadStockDetailDTO();

            string unloadFrom = "";
            if (Request.QueryString["unloadFrom"] != null)
                unloadFrom = Request.QueryString["unloadFrom"];

            if(Request.QueryString["selectedStockHeaderId"] != null)
                loadStockDetailDTO.StockHeaderId = Convert.ToInt32(Request.QueryString["selectedStockHeaderId"]);
            if (Request.QueryString["selectedCatalogCode"] != null)
                loadStockDetailDTO.CatalogCode =Server.HtmlDecode( Request.QueryString["selectedCatalogCode"]);            
            if (Request.QueryString["balanceQty"] != null)
                loadStockDetailDTO.BalanceQty = Convert.ToDouble(Request.QueryString["balanceQty"]);
            if (Request.QueryString["allocQty"] != null)
                loadStockDetailDTO.AllocQty = Convert.ToDouble(Request.QueryString["allocQty"]);
            if (Request.QueryString["loadQty"] != null)
                loadStockDetailDTO.LoadQty = Convert.ToDouble(Request.QueryString["loadQty"]);

            if (Request.QueryString["vehicleBalanceQty"] != null)
                loadStockDetailDTO.VehicleBalanceQty = Convert.ToDouble(Request.QueryString["vehicleBalanceQty"]);

            if (Request.QueryString["unloadQty"] != null)
                loadStockDetailDTO.ReturningQty = Convert.ToDouble(Request.QueryString["unloadQty"]);

            if (Request.QueryString["unloadType"] != null)
            {
                string returnType = Request.QueryString["unloadType"];
                if (returnType.Equals("Fresh"))
                    loadStockDetailDTO.ReturnReason = "FRSH";
                else
                    loadStockDetailDTO.ReturnReason = "DAMG";
            }
        
                DateTime allocDate = DateTime.Now;
            if(Request.QueryString["selectedStockHeaderId"] != null)
                allocDate = Convert.ToDateTime(Request.QueryString["allocDate"]);

            if ((loadStockDetailDTO.BalanceQty < loadStockDetailDTO.ReturningQty && unloadFrom.Equals("Allocated Balance")) || (loadStockDetailDTO.VehicleBalanceQty < loadStockDetailDTO.ReturningQty && unloadFrom.Equals("Vehicle Balance")))//validation
            {
                str = "Unload Failed";
                sb.Append(str);
                return sb.ToString();
            }

            if (unloadFrom.Equals("Allocated Balance"))
            {
                if (Request.QueryString["totalReturnQty"] != null)
                    loadStockDetailDTO.ReturnQty = loadStockDetailDTO.ReturningQty + Convert.ToDouble(Request.QueryString["totalReturnQty"]);
                if (Request.QueryString["SelectedVehicleReturnQty"] != null)
                    loadStockDetailDTO.VehicleReturnQty = Convert.ToDouble(Request.QueryString["SelectedVehicleReturnQty"]);

                //reducing Balance Quantity
                loadStockDetailDTO.BalanceQty = loadStockDetailDTO.BalanceQty - loadStockDetailDTO.ReturningQty;

                //to set returning amount (to insert to stock movement)
                loadStockDetailDTO.VehicleReturningQty = 0;
            }
            else if (unloadFrom.Equals("Vehicle Balance"))
            {
                if (Request.QueryString["totalReturnQty"] != null)
                    loadStockDetailDTO.ReturnQty = Convert.ToDouble(Request.QueryString["totalReturnQty"]);
                if (Request.QueryString["SelectedVehicleReturnQty"] != null)
                    loadStockDetailDTO.VehicleReturnQty = loadStockDetailDTO.ReturningQty + Convert.ToDouble(Request.QueryString["SelectedVehicleReturnQty"]);

                //reducing Balance Quantity
                loadStockDetailDTO.VehicleBalanceQty = loadStockDetailDTO.VehicleBalanceQty - loadStockDetailDTO.ReturningQty;

                //to set returning amount (to insert to stock movement)
                loadStockDetailDTO.VehicleReturningQty = loadStockDetailDTO.ReturningQty;
                loadStockDetailDTO.ReturningQty = 0;
            }
            else if (unloadFrom.Equals("Both"))
            {
                if (Request.QueryString["totalReturnQty"] != null)
                    loadStockDetailDTO.ReturnQty = loadStockDetailDTO.BalanceQty + Convert.ToDouble(Request.QueryString["totalReturnQty"]);
                if (Request.QueryString["SelectedVehicleReturnQty"] != null)
                    loadStockDetailDTO.VehicleReturnQty = loadStockDetailDTO.VehicleBalanceQty + Convert.ToDouble(Request.QueryString["SelectedVehicleReturnQty"]);                              

                //to set returning amount (to insert to stock movement)
                loadStockDetailDTO.VehicleReturningQty = loadStockDetailDTO.VehicleBalanceQty;
                loadStockDetailDTO.ReturningQty = loadStockDetailDTO.BalanceQty;

                //reducing Balance Quantities
                loadStockDetailDTO.BalanceQty = 0;
                loadStockDetailDTO.VehicleBalanceQty = 0;
            }

            //reducing Balance Quantity
            //loadStockDetailDTO.BalanceQty = loadStockDetailDTO.BalanceQty - loadStockDetailDTO.ReturningQty;

            bool status = false;
            //Comment by Irosh 2019/11/05
            //status = commonClient.UnLoadStock(loadStockDetailDTO, allocDate);
            
            str = status ? "true" : "false";
            //return str;
            sb.Append(str);
        }
        catch (Exception)
        {
            sb.Append("fail");
            return sb.ToString();
            //return "Unload Failed !";
        }
        return sb.ToString();
    }



    public string GetDamage()
    {
        StringBuilder txt = new StringBuilder();
        try
        {
            txt.Append("<table border=\"0\" id=\"repgroupTable\" width=\"300px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
            txt.Append("<tr class=\"detail-popsheader\">");
            txt.Append("</tr>");
            txt.Append("<tr>");
            txt.Append("<td class=\"tblborder\" align=\"left\">Damage Qty</td>");
            txt.Append("<td align=\"left\" class=\"tblborder\"><input type=\"text\"></td>");
            txt.Append("</tr>");

            txt.Append("<tr>");
            txt.Append("<td class=\"tblborder\" align=\"left\">Damage Reason</td>");
            txt.Append("<td align=\"left\" class=\"tblborder\"><input type=\"text\"></td>");
            txt.Append("</tr>");

            txt.Append("</table>");
        }
        catch (Exception)
        {
            
            throw;
        }
        return txt.ToString();
    }

    private void SetSelectedStockAllocationDate()
    {
        string stockAllocationDate = "";
        if (Request.QueryString["stockAllocationDate"] != null)
            stockAllocationDate = Request.QueryString["stockAllocationDate"];
        Session["STOCK_ALLOCATION_DATE"] = stockAllocationDate;
    }
}