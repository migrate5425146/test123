﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="program_entry.aspx.cs" Inherits="programs_transaction_program_entry" %>
<%@ MasterType TypeName="SiteMaster" %>
<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<asp:HiddenField ID="HiddenFieldProgramID" runat="server" />

<asp:HiddenField ID="HiddenField_DueDate" runat="server" />
<script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
   <asp:HiddenField ID="HiddenFieldExistClickOk" runat="server" />
   <asp:HiddenField runat="server" ID="HiddenFieldRepCode" />

       <div id="schedulemodalWindow" style="display: none">
        <div id="div_repconfirm_message">
        </div>
        <div class="clearall" style="margin-bottom:15px"></div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>
        <div id="program_window" style="display: none">
        <div id="program_subwindow"></div>
    </div>

    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div>
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
            <div class="back"></div></asp:HyperLink>
                    </div>
                </div>
            </div>
        
        <div id="new_window">
            <div id="div_custgrid"></div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div id="div_info" runat="server" style="display: none">
        </div>
      <div class="formleft">
        <div class="formtextdiv">Program Name</div>
        <div class="formdetaildiv">
        <span class="wosub mand">   
        <asp:TextBox ID="txtProgramName" runat="server" Width="178px" ></asp:TextBox>
        <asp:Label ID="tbContact" runat="server" Text="*" Font-Bold="True" Font-Size="Large" ForeColor="#FF3300"></asp:Label>
        </span> 
        </div>
                        <div style="line-height:30px; margin-top:3px;">
                <a href="#" id="id_schedule">
                        <img id="img1" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a></div>
        <div class="clearall"></div>
        <div class="radiobutton" id="radioVolume" runat="server">
        <asp:RadioButton GroupName="A" Checked="true" ID="RadioButtonVolume" runat="server" Text="&nbsp;Volume"/>
        </div>
        <div class="radiobutton" id="radioRandom" runat="server">
        <asp:RadioButton ID="RadioButtonRandom" GroupName="A" runat="server" Text="&nbsp;Random" /> 
        </div>
        <div class="clearall"></div>      
      <div id="div_volume" runat="server">
        <asp:CheckBoxList ID="checkBoxListVolume" runat="server" CssClass="test">
        <asp:ListItem Value=5>Volume 5</asp:ListItem>
        <asp:ListItem Value=6>Volume 6</asp:ListItem>
        <asp:ListItem Value=7>Volume 7</asp:ListItem>
        <asp:ListItem Value=8>Volume 8</asp:ListItem>
        <asp:ListItem Value=9>Volume 9</asp:ListItem>
        </asp:CheckBoxList>
     </div>
        <div id="tbCreated" runat="server" class="specialevents" style="float:right;"></div>                   
       </div>

       <div class="formright" style="display: none"><div id="program_grid" style="margin-right:10px;" runat="server">Right Grid Goes Here</div></div>
        </div>

     <div style="margin:10px;">
     
     <div class="clearall"></div>
     <div id="div_random" runat="server">
     <asp:Label ID="Label2" runat="server" Text="Random" CssClass="specialevents"></asp:Label>
     <div class="clearall"></div>
     <div id="loadLeadCustomerGrid"></div>
     </div>
     
     
     <div id="grid"></div>
     

     </div>
     </div>

     <script type="text/javascript">
         hideStatusDiv("MainContent_div_message");
         $(document).ready(function () {

             if ($("#MainContent_RadioButtonVolume").attr("checked")) {
                 $('#MainContent_div_random').css('display', 'none');
                 $('#MainContent_div_volume').css('display', 'block');
             }

             $("#id_schedule").click(function () {
                 loadProgram(); // scheduletemplates('call_cycle/process_forms/processmaster.aspx?fm=templateentry&type=query');
             });


           //  loadProgram();

             $("#MainContent_RadioButtonRandom").change(function () {
                 if ($("#MainContent_RadioButtonRandom").attr("checked")) {
                     $('#MainContent_div_random').css('display', 'block');
                     $('#MainContent_div_volume').css('display', 'none');
                 }
             });

             $("#MainContent_RadioButtonVolume").change(function () {
                 if ($("#MainContent_RadioButtonVolume").attr("checked")) {
                     $('#MainContent_div_random').css('display', 'none');
                     $('#MainContent_div_volume').css('display', 'block');
                 }
             });

             var callid = $("#MainContent_HiddenFieldCallCycleID").val();
             //             if (callid != '') {
             //                 loadLeadCustomer(callid, 1);
             //             } else {
             //                 loadLeadCustomer(0, 1);
             //             }
             loadCustomertoProgram(1, 0, 'F',0);



             var newcust_window = $("#program_window"),
                        newcust_undo = $("#undo")
                                .bind("click", function () {
                                    newcust_window.data("kendoWindow").open();
                                    newcust_undo.hide();
                                });

             var onClose = function () {
                 newcust_undo.show();
             }

             if (!newcust_window.data("kendoWindow")) {
                 newcust_window.kendoWindow({
                     width: "500px",
                     height: "400px",
                     title: "Lookup",
                     close: onClose
                 });
             }

             newcust_window.data("kendoWindow").close();
         });

         $("#id_addleadCust").click(function () {
             SetLeadStage("Customer");
             $("div#div_loader").show();
             //loadLeadCustomerLookup(1, 0, 'F');
             //$("div#div_loader").hide();
         });

         $("#id_addEnduser").click(function () {
             SetLeadStage("enduser");
             $("div#div_loader").show();


             //$("div#div_loader").hide();
         });

         function setRepCode(repCode) {
             $("#MainContent_HiddenFieldRepCode").val(repCode);
         }
         
     </script>
</asp:Content>

