﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="program_targets.aspx.cs" Inherits="programs_master_program_targets" %>
<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnablePageMethods="true">
    </telerik:RadScriptManager>
    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="HiddenFieldIsContinueWithSave" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedProgramTargetID" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedProgramID" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedProgramName" runat="server" />

    <asp:HiddenField ID="HiddenFieldTargetProductID" runat="server" />
    <asp:HiddenField ID="HiddenFieldTargetProductName" runat="server" />
    <asp:HiddenField ID="HiddenFieldTargetProductSKU" runat="server" />

    <asp:HiddenField ID="HiddenFieldBonusProductID" runat="server" />
    <asp:HiddenField ID="HiddenFieldBonusProductName" runat="server" />
    <asp:HiddenField ID="HiddenFieldBonusProductSKU" runat="server" />    
    
    <div id="div_promt" style="display: none">
    </div>
    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div>
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 35%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
                        <div class="back"></div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div id="div_info" runat="server" style="display: none">
        </div>
        <div class="formtextdiv">
            Program</div>
        <div class="formdetaildiv_right" id="div_TextBoxPermanent">
            <span class="wosub mand">
                <asp:TextBox ID="txtProgramName" CssClass="tb" runat="server" Style="width: 300px;
                    text-transform: uppercase"></asp:TextBox>
                <asp:Label ID="Label1" runat="server" Text="*" Font-Bold="True" Font-Size="Large"
                    ForeColor="#FF3300"></asp:Label>
                <div runat="server" id="div_autocomplete">
                </div>
            </span>
        </div>
        <div class="clearall">
        </div>
        <div class="formtextdiv">
            From Date
        </div>
        <div class="formdetaildiv_right" style="margin-top: -10px;">
            <div class="datepicker" id="div_Date">
                <asp:TextBox ID="dtpDate" runat="server" ClientIDMode="Static" Style="width: 120px;"></asp:TextBox>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div class="formtextdiv">
            To Date
        </div>
        <div class="formdetaildiv_right" style="margin-top: -10px;">
            <div class="datepicker" id="div_Date_to">
                <asp:TextBox ID="dtpDateTo" runat="server" ClientIDMode="Static" Style="width: 120px;"></asp:TextBox>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div class="formtextdiv">
        </div>
        <div class="formdetaildivlead">
            <input type="button" id="btn_filter" value="Search" class="searchbtn1" />
        </div>
        <div class="clearall">
        </div>
        <div class="grid_container" id="div_mainProgramTargets" >
            <div class="grid_title">
                Targets
            </div>
            <div id="div_ProgramTargetsGrid">
            </div>
        </div>        
        <div class="clearall">
        </div>
    </div>
    <div>
        &nbsp;</div>
    <script type="text/javascript">
        $("#div_mainProgramTargets").css("display", "none");
        $(document).ready(function () {
            hideStatusDiv("MainContent_div_message");
           // debugger;
            $("#dtpDate").kendoDatePicker({
                //display month and year in the input
                //value: new Date(),
                format: "dd-MMM-yyyy"
            });
            $("#dtpDateTo").kendoDatePicker({
                // display month and year in the input
                //value: new Date(),
                format: "dd-MMM-yyyy"
            });

            if ($("#MainContent_HiddenFieldSelectedProgramID").val() != '' && $("#MainContent_txtProgramName").val() != '') {
                $("#div_mainProgramTargets").css("display", "block");
                var programId = $("#MainContent_HiddenFieldSelectedProgramID").val();
                var fromDate = $("#dtpDate").val();
                var toDate = $("#dtpDateTo").val();
                LoadProgramTargetsGrid(programId, fromDate, toDate);
            }
            else {
                $("#div_mainProgramTargets").css("display", "block");
                LoadProgramTargetsGrid(0, $("#dtpDate").val(), $("#dtpDateTo").val());
                $('#MainContent_HiddenFieldSelectedProgramID').val('');
                $('#MainContent_HiddenFieldSelectedProgramName').val('');
            }

            $("#btn_filter").click(function () {
                programTargetFilterClick() 
            });
        });

    </script>
</asp:Content>

