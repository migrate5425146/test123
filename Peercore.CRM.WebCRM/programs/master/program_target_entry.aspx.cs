﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System.Text;

public partial class programs_master_program_target_entry : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            //Set the Toolbar buttons
            buttonbar.VisibleSave(true);
            buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
            buttonbar.EnableSave(true);

            buttonbar.VisibleDeactivate(true);
            buttonbar.onButtonClear = new usercontrols_buttonbar.ButtonClear(ButtonClear_Click);
            buttonbar.EnableDeactivate(true);

            if (!IsPostBack)
            {
                DateTime today = DateTime.Today;
                dtpDate.Text = new DateTime(today.Year, today.Month, 1).ToString("dd-MMM-yyyy");
                dtpDateTo.Text = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month)).ToString("dd-MMM-yyyy");

                div_autocomplete.InnerHtml = SetAutocomplete("1");
                div_autocompleteTargetProduct.InnerHtml = SetAutocompleteTargetProduct("1");
                div_autocompleteBonusProduct.InnerHtml = SetAutocompleteBonusProduct("1");

                //Remove Other Session.
                ClearSession();

                if (Request.QueryString["ptid"] != null && Request.QueryString["ptid"] != string.Empty)
                {
                    //ViewState["CallCycleId"] = Request.QueryString["callcycid"].ToString();
                    int programTargetId = Convert.ToInt32(Request.QueryString["ptid"]);
                    ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "LoadSelectedProgramTargetDetails(" + programTargetId + ", 0);", true);
                    // SetBackLink();
                    hlBack.NavigateUrl = "~/programs/master/program_targets.aspx";
                }

                rdbTargetPercentage.Checked = true;
                rdbTargetDiscount.Checked = true;
                rdbBonusPercentage.Checked = true;
                rdbBonusDiscount.Checked = true;
            }

            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Program Targets Entry / Update", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    protected void ButtonSave_Click(object sender)
    {
        this.SaveProgramTargets();
    }

    protected void ButtonClear_Click(object sender)
    {
        ClearFields();
    }

    private void ClearFields()
    {
        HiddenFieldSelectedProgramTargetID.Value = "";
        HiddenFieldSelectedProgramID.Value = "";
        HiddenFieldSelectedProgramName.Value = "";

        HiddenFieldTargetProductID.Value = "";
        HiddenFieldTargetProductName.Value = "";
        HiddenFieldTargetProductSKU.Value = "";

        HiddenFieldBonusProductID.Value = "";
        HiddenFieldBonusProductName.Value = "";
        HiddenFieldBonusProductSKU.Value = "";

        txtProgramName.Text = "";
        txtProgramTarget.Text = "";
        txtTargetProductName.Text = "";
        txtTargetProductPacks.Text = "";

        txtBonusTarget.Text = "";
        txtBonusProductName.Text = "";
        txtBonusProductPacks.Text = "";

        DateTime today = DateTime.Today;
        dtpDate.Text = new DateTime(today.Year, today.Month, 1).ToString("dd-MMM-yyyy");
        dtpDateTo.Text = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month)).ToString("dd-MMM-yyyy");
    }

    private void ClearSession()
    {
        Session[CommonUtility.CUSTOMER_DATA] = null;
    }

    private string SetAutocomplete(string inactive)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script type=\"text/javascript\">");
        sb.Append("     $(function () {");
        sb.Append("         $(\".tb\").autocomplete({");
        sb.Append("             source: function (request, response) {");
        sb.Append("                 $.ajax({");
        sb.Append("                     type: \"POST\",");
        sb.Append("                     contentType: \"application/json; charset=utf-8\",");
        sb.Append("                     url: \"" + ConfigUtil.ApplicationPath + "service/lead_customer/common.asmx/GetProgramTargetsByProgram\",");
        sb.Append("                     data: \"{'name':'\" + request.term + \"'}\",");
        sb.Append("                     dataType: \"json\",");
        sb.Append("                     async: true,");
        sb.Append("                     dataFilter: function(data) {");
        sb.Append("                           return data;");
        sb.Append("                     },");
        sb.Append("                     success: function (data) {");
        sb.Append("                     response($.map(data.d, function(item) {");
        sb.Append("                     return {");
        sb.Append("                         label: item.Program.ProgramName,");
        sb.Append("                         desc: item.Program.ProgramId,");
        sb.Append("                         programTargetId: item.ProgramTargetId,");
        sb.Append("                         targetPercentage: item.TargetPercentage,");
        sb.Append("                         targetMilles: item.TargetMilles,");
        sb.Append("                         targetIncentiveAmount: item.TargetIncentiveAmount,");
        sb.Append("                         targetIncentivePreIssueProductId: item.TargetIncentivePreIssueProductId,");
        sb.Append("                         targetIncentivePreIssueProductName: item.TargetIncentivePreIssueProductName,");
        sb.Append("                         targetIncentivePreIssueProductSku: item.TargetIncentivePreIssueProductSku,");
        sb.Append("                         targetIncentivePreIssueAmountPacks: item.TargetIncentivePreIssueAmountPacks,");
        sb.Append("                         bonusPercentage: item.BonusPercentage,");
        sb.Append("                         bonusMilles: item.BonusMilles,");
        sb.Append("                         bonusIncentiveAmount: item.BonusIncentiveAmount,");
        sb.Append("                         bonusIncentivePreIssueProductId: item.BonusIncentivePreIssueProductId,");
        sb.Append("                         bonusIncentivePreIssueProductName: item.BonusIncentivePreIssueProductName,");
        sb.Append("                         bonusIncentivePreIssueProductSku: item.BonusIncentivePreIssueProductSku,");
        sb.Append("                         bonusIncentivePreIssueAmountPacks: item.BonusIncentivePreIssueAmountPacks,");
        sb.Append("                         effStartDate: item.EffStartDateString,");
        sb.Append("                         effEndDate: item.EffEndDateString");
        sb.Append("                     };");
        sb.Append("                     }));");

        sb.Append("                     },");
        sb.Append("                     error: function (result) {");
        sb.Append("                     }");
        sb.Append("                 });");
        sb.Append("             },");
        sb.Append("             minLength: 2,");
        sb.Append("            select: function( event, ui ) {");
        sb.Append("                 var selectedObj = ui.item;");
        sb.Append("                 $('#MainContent_HiddenFieldSelectedProgramTargetID').val(selectedObj.programTargetId);");
        sb.Append("                 $('#MainContent_HiddenFieldSelectedProgramID').val(selectedObj.desc);");
        sb.Append("                 $('#MainContent_HiddenFieldSelectedProgramName').val(selectedObj.label);");
        sb.Append("                 if(selectedObj.label != '')");
        sb.Append("                 {");
        sb.Append("                     LoadSelectedProgramTargetDetails(selectedObj.programTargetId, selectedObj.desc);");
        sb.Append("                 }");
        sb.Append("            }");
        sb.Append("         });");
        sb.Append("     });");
        sb.Append("</script>");
        sb.Append("");

        return sb.ToString();
    }

    private string SetAutocompleteTargetProduct(string inactive)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script type=\"text/javascript\">");
        sb.Append("     $(function () {");
        sb.Append("         $(\".tb1\").autocomplete({");
        sb.Append("             source: function (request, response) {");
        sb.Append("                 $.ajax({");
        sb.Append("                     type: \"POST\",");
        sb.Append("                     contentType: \"application/json; charset=utf-8\",");
        sb.Append("                     url: \"" + ConfigUtil.ApplicationPath + "service/lead_customer/common.asmx/GetProductsByName\",");
        sb.Append("                     data: \"{'name':'\" + request.term + \"'}\",");
        sb.Append("                     dataType: \"json\",");
        sb.Append("                     async: true,");
        sb.Append("                     dataFilter: function(data) {");
        sb.Append("                           return data;");
        sb.Append("                     },");
        sb.Append("                     success: function (data) {");
        sb.Append("                     response($.map(data.d, function(item) {");
        sb.Append("                     return {");
        sb.Append("                         label: item.Name,");
        sb.Append("                         desc: item.ProductId,");
        sb.Append("                         sku: item.Sku");
        sb.Append("                     };");
        sb.Append("                     }));");

        sb.Append("                     },");
        sb.Append("                     error: function (result) {");
        sb.Append("                     }");
        sb.Append("                 });");
        sb.Append("             },");
        sb.Append("             minLength: 2,");
        sb.Append("            select: function( event, ui ) {");
        sb.Append("                 var selectedObj = ui.item;");
        sb.Append("                 $('#MainContent_HiddenFieldTargetProductID').val(selectedObj.desc);");
        sb.Append("                 $('#MainContent_HiddenFieldTargetProductName').val(selectedObj.label);");
        sb.Append("                 $('#MainContent_HiddenFieldTargetProductSKU').val(selectedObj.sku);");
        //sb.Append("                 if(selectedObj.label != '')");
        //sb.Append("                 {");
        //sb.Append("                     loadTargetDetails();");
        //sb.Append("                 }");
        sb.Append("            }");
        sb.Append("         });");
        sb.Append("     });");
        sb.Append("</script>");
        sb.Append("");

        return sb.ToString();
    }

    private string SetAutocompleteBonusProduct(string inactive)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script type=\"text/javascript\">");
        sb.Append("     $(function () {");
        sb.Append("         $(\".tb2\").autocomplete({");
        sb.Append("             source: function (request, response) {");
        sb.Append("                 $.ajax({");
        sb.Append("                     type: \"POST\",");
        sb.Append("                     contentType: \"application/json; charset=utf-8\",");
        sb.Append("                     url: \"" + ConfigUtil.ApplicationPath + "service/lead_customer/common.asmx/GetProductsByName\",");
        sb.Append("                     data: \"{'name':'\" + request.term + \"'}\",");
        sb.Append("                     dataType: \"json\",");
        sb.Append("                     async: true,");
        sb.Append("                     dataFilter: function(data) {");
        sb.Append("                           return data;");
        sb.Append("                     },");
        sb.Append("                     success: function (data) {");
        sb.Append("                     response($.map(data.d, function(item) {");
        sb.Append("                     return {");
        sb.Append("                         label: item.Name,");
        sb.Append("                         desc: item.ProductId,");
        sb.Append("                         sku: item.Sku");
        sb.Append("                     };");
        sb.Append("                     }));");

        sb.Append("                     },");
        sb.Append("                     error: function (result) {");
        sb.Append("                     }");
        sb.Append("                 });");
        sb.Append("             },");
        sb.Append("             minLength: 2,");
        sb.Append("            select: function( event, ui ) {");
        sb.Append("                 var selectedObj = ui.item;");
        sb.Append("                 $('#MainContent_HiddenFieldBonusProductID').val(selectedObj.desc);");
        sb.Append("                 $('#MainContent_HiddenFieldBonusProductName').val(selectedObj.label);");
        sb.Append("                 $('#MainContent_HiddenFieldBonusProductSKU').val(selectedObj.sku);");
        //sb.Append("                 if(selectedObj.label != '')");
        //sb.Append("                 {");
        //sb.Append("                     loadTargetDetails();");
        //sb.Append("                 }");
        sb.Append("            }");
        sb.Append("         });");
        sb.Append("     });");
        sb.Append("</script>");
        sb.Append("");

        return sb.ToString();
    }

    public void SaveProgramTargets()
    {
        CommonClient commonClient = new CommonClient();
        CommonUtility commonUtility = new CommonUtility();

        try
        {
            if (HiddenFieldSelectedProgramID.Value == "" || HiddenFieldSelectedProgramID.Value == String.Empty)
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Please Select a Program.");
                return;
            }

            int programTargetId = !String.IsNullOrEmpty(HiddenFieldSelectedProgramTargetID.Value) ? Convert.ToInt32(HiddenFieldSelectedProgramTargetID.Value) : 0;
            int programId = !String.IsNullOrEmpty(HiddenFieldSelectedProgramID.Value) ? Convert.ToInt32(HiddenFieldSelectedProgramID.Value) : 0;

            double programTarget = !String.IsNullOrEmpty(txtProgramTarget.Text) ? Convert.ToDouble(txtProgramTarget.Text) : 0;

            int targetIncentiveProductID = !String.IsNullOrEmpty(HiddenFieldTargetProductID.Value) ? Convert.ToInt32(HiddenFieldTargetProductID.Value) : 0;
            double targetIncentiveAmount = !String.IsNullOrEmpty(txtTargetProductPacks.Text) ? Convert.ToDouble(txtTargetProductPacks.Text) : 0;
            int targetIncentivePacks = !String.IsNullOrEmpty(txtTargetProductPacks.Text) ? Convert.ToInt32(txtTargetProductPacks.Text) : 0;
            int targetIncentiveProductSku = !String.IsNullOrEmpty(HiddenFieldTargetProductSKU.Value) ? Convert.ToInt32(HiddenFieldTargetProductSKU.Value) : 0;

            double bonusTarget = !String.IsNullOrEmpty(txtBonusTarget.Text) ? Convert.ToDouble(txtBonusTarget.Text) : 0;

            int bonusIncentiveProductID = !String.IsNullOrEmpty(HiddenFieldBonusProductID.Value) ? Convert.ToInt32(HiddenFieldBonusProductID.Value) : 0;
            double bonusIncentiveAmount = !String.IsNullOrEmpty(txtBonusProductPacks.Text) ? Convert.ToDouble(txtBonusProductPacks.Text) : 0;
            int bonusIncentivePacks = !String.IsNullOrEmpty(txtBonusProductPacks.Text) ? Convert.ToInt32(txtBonusProductPacks.Text) : 0;
            int bonusIncentiveProductSku = !String.IsNullOrEmpty(HiddenFieldBonusProductSKU.Value) ? Convert.ToInt32(HiddenFieldBonusProductSKU.Value) : 0;

            DateTime effStartDate = Convert.ToDateTime(dtpDate.Text);
            DateTime effEndDate = Convert.ToDateTime(dtpDateTo.Text);

            bool isSucc = false;

            ProgramTargetsDTO programTargetsDTO = new ProgramTargetsDTO();

            ProgramDTO programDTO = new ProgramDTO();
            programDTO.ProgramId = programId;

            programTargetsDTO.ProgramTargetId = programTargetId;
            programTargetsDTO.Program = programDTO;

            //setting Program Target Value
            if (rdbTargetPercentage.Checked)
                programTargetsDTO.TargetPercentage = programTarget;
            else
                programTargetsDTO.TargetMilles = programTarget;

            //Setting Program Target Incentive
            if (rdbTargetDiscount.Checked)
                programTargetsDTO.TargetIncentiveAmount = targetIncentiveAmount;
            else
            {
                programTargetsDTO.TargetIncentivePreIssueProductId = targetIncentiveProductID;
                programTargetsDTO.TargetIncentivePreIssueAmountPacks = targetIncentivePacks;
                programTargetsDTO.TargetIncentivePreIssueProductSku = targetIncentiveProductSku;
            }

            //Setting Bonus Target Value
            if (rdbBonusPercentage.Checked)
                programTargetsDTO.BonusPercentage = bonusTarget;
            else
                programTargetsDTO.BonusMilles = bonusTarget;

            //Setting Bonus Target Incentive
            if (rdbBonusDiscount.Checked)
                programTargetsDTO.BonusIncentiveAmount = bonusIncentiveAmount;
            else
            {
                programTargetsDTO.BonusIncentivePreIssueProductId = bonusIncentiveProductID;
                programTargetsDTO.BonusIncentivePreIssueAmountPacks = bonusIncentivePacks;
                programTargetsDTO.BonusIncentivePreIssueProductSku = bonusIncentiveProductSku;
            }

            //Setting Effective Date Range
            programTargetsDTO.EffStartDate = effStartDate;
            programTargetsDTO.EffEndDate = effEndDate;

            if (programTargetsDTO.ProgramTargetId == 0)//new insert entry
            {
                programTargetsDTO.CreatedBy = UserSession.Instance.UserName;
            }
            else if (programTargetsDTO.ProgramTargetId > 0)//update entry
            {
                programTargetsDTO.LastModifiedBy = UserSession.Instance.UserName;
            }

            List<ProgramTargetsDTO> programTargetsDTOList = new List<ProgramTargetsDTO>();
            programTargetsDTOList.Add(programTargetsDTO);
            isSucc = commonClient.SaveProgramTargets(programTargetsDTOList);

            if (isSucc == true)
            {
                div_message.Attributes.Add("style", "display:block");
                div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved.");
                ClearFields();
            }
            else
            {
                div_message.Attributes.Add("style", "display:none");
                div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
            }
        }
        catch (Exception ex)
        {
            throw;
        }
    }
}