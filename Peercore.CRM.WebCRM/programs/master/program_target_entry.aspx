﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="program_target_entry.aspx.cs" Inherits="programs_master_program_target_entry" %>
<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnablePageMethods="true">
    </telerik:RadScriptManager>
    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="HiddenFieldIsContinueWithSave" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedProgramTargetID" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedProgramID" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedProgramName" runat="server" />

    <asp:HiddenField ID="HiddenFieldTargetProductID" runat="server" />
    <asp:HiddenField ID="HiddenFieldTargetProductName" runat="server" />
    <asp:HiddenField ID="HiddenFieldTargetProductSKU" runat="server" />

    <asp:HiddenField ID="HiddenFieldBonusProductID" runat="server" />
    <asp:HiddenField ID="HiddenFieldBonusProductName" runat="server" />
    <asp:HiddenField ID="HiddenFieldBonusProductSKU" runat="server" />    
    
    <div id="div_promt" style="display: none">
    </div>
    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div>
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 35%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
                        <div class="back"></div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div id="div_info" runat="server" style="display: none">
        </div>
        <div class="formtextdiv">
            Program</div>
        <div class="formdetaildiv_right" id="div_TextBoxPermanent">
            <span class="wosub mand">
                <asp:TextBox ID="txtProgramName" CssClass="tb" runat="server" Style="width: 300px;
                    text-transform: uppercase"></asp:TextBox>
                <asp:Label ID="Label1" runat="server" Text="*" Font-Bold="True" Font-Size="Large"
                    ForeColor="#FF3300"></asp:Label>
                <div runat="server" id="div_autocomplete">
                </div>
            </span>
        </div>
        <div class="clearall">
        </div>
        <div class="formtextdiv">
            From Date
        </div>
        <div class="formdetaildiv_right" style="margin-top: -10px;">
            <div class="datepicker" id="div_Date">
                <asp:TextBox ID="dtpDate" runat="server" ClientIDMode="Static" Style="width: 120px;"></asp:TextBox>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div class="formtextdiv">
            To Date
        </div>
        <div class="formdetaildiv_right" style="margin-top: -10px;">
            <div class="datepicker" id="div_Date_to">
                <asp:TextBox ID="dtpDateTo" runat="server" ClientIDMode="Static" Style="width: 120px;"></asp:TextBox>
            </div>
        </div>
        <div class="clearall">
        </div>

        <div class="formleft">
            <fieldset>
                <legend>Program Target</legend>
                <div class="formtextdiv">
                    &nbsp;
                </div>
                <div class="formdetaildiv_right" id = "divGrpProgramTarget">
                    <asp:RadioButton ID="rdbTargetPercentage" GroupName = "grpProgramTarget"  runat="server" Text=" % " CssClass="radibutton_width" />
                    <asp:RadioButton ID="rdbTargetMilles" GroupName = "grpProgramTarget" runat="server" Text=" Milles " CssClass="radibutton_width" />
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Program Target</div>
                <div class="formdetaildiv_right">
                        <asp:TextBox id = "txtProgramTarget" runat="server" CssClass="input-large1" ></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
                <fieldset>
                    <legend>Program Target Incentive</legend>
                <div class="formtextdiv">
                    &nbsp;
                </div>
                    <div class="formdetaildiv_right" id="divGrpProgramIncentive">
                        <asp:RadioButton ID="rdbTargetDiscount" GroupName="grpProgramIncentive" 
                            runat="server" Text="Discount" CssClass="radibutton_width" />
                        <asp:RadioButton ID="rdbTargetPreIssue" GroupName="grpProgramIncentive" runat="server"
                            Text="Free Issue" CssClass="radibutton_width" />
                    </div>
                    <div class="clearall">
                    </div>
                    <div id="divTargetProduct" runat="server">
                        <div class="formtextdiv">
                            Product</div>
                        <div class="formdetaildiv_right">
                            <asp:TextBox ID="txtTargetProductName" CssClass="tb1 input-large1" runat="server"></asp:TextBox>
                            <div runat="server" id="div_autocompleteTargetProduct">
                            </div>
                        </div>
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                        Amount</div>
                    <div class="formdetaildiv_right">
                        <asp:TextBox ID="txtTargetProductPacks" runat="server" CssClass="input-large1"></asp:TextBox>
                        <asp:Label ID="lblTargetAmount" runat="server" Text="( LKR )" Font-Bold="True"></asp:Label>
                    </div>
                    <div class="clearall">
                    </div>
                </fieldset>
            </fieldset>            
        </div>
        <div class="formright">
            <fieldset>
                <legend>Program Bonus Target</legend>
                <div class="formtextdiv">
                    &nbsp;
                </div>
                <div class="formdetaildiv_right" id = "divGrpBonusTarget">
                    <asp:RadioButton ID="rdbBonusPercentage" GroupName = "grpBonusTarget"  runat="server" Text=" % " CssClass="radibutton_width" />
                    <asp:RadioButton ID="rdbBonusMilles" GroupName = "grpBonusTarget" runat="server" Text=" Milles " CssClass="radibutton_width" />
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Bonus Target</div>
                <div class="formdetaildiv_right">
                    <asp:TextBox id = "txtBonusTarget" runat="server" CssClass="input-large1"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
                <fieldset>
                    <legend>Program Bonus Incentive</legend>
                <div class="formtextdiv">
                    &nbsp;
                </div>
                    <div class="formdetaildiv_right" id="divGrpBonusIncentive">
                        <asp:RadioButton ID="rdbBonusDiscount" GroupName="grpBonusIncentive" 
                            runat="server" Text="Discount" CssClass="radibutton_width" />
                        <asp:RadioButton ID="rdbBonusPreIssue" GroupName="grpBonusIncentive" runat="server"
                            Text="Free Issue" CssClass="radibutton_width" />
                    </div>
                    <div class="clearall">
                    </div>
                    <div id="divBonusProduct" runat = "server">
                        <div class="formtextdiv">
                            Product</div>
                        <div class="formdetaildiv_right">
                            <asp:TextBox ID="txtBonusProductName" CssClass="tb2 input-large1" runat="server"></asp:TextBox>
                            <div runat="server" id="div_autocompleteBonusProduct">
                            </div>
                        </div>
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                        Amount</div>
                    <div class="formdetaildiv_right">
                        <asp:TextBox ID="txtBonusProductPacks" runat="server" CssClass="input-large1"></asp:TextBox>
                        <asp:Label ID="lblBonusAmount" runat="server" Text="( LKR )" Font-Bold="True"></asp:Label>
                    </div>
                    <div class="clearall">
                    </div>
                </fieldset>
            </fieldset>
        </div>
        <div class="clearall">
        </div>
    </div>
    <div>
        &nbsp;</div>
    <script type="text/javascript">
        $(document).ready(function () {

            hideStatusDiv("MainContent_div_message");

            $("#MainContent_divTargetProduct").css("display", "none");
            $("#MainContent_divBonusProduct").css("display", "none");

            $("#dtpDate").kendoDatePicker({
                //display month and year in the input
                //value: new Date(),
                format: "dd-MMM-yyyy"
            });
            $("#dtpDateTo").kendoDatePicker({
                // display month and year in the input
                //value: new Date(),
                format: "dd-MMM-yyyy"
            });

            $('#MainContent_txtProgramTarget').prop('type', 'number');
            $('#MainContent_txtProgramTarget').prop('step', 'any');

            $('#MainContent_txtBonusTarget').prop('type', 'number');
            $('#MainContent_txtBonusTarget').prop('step', 'any');

            $('#MainContent_txtTargetProductPacks').prop('type', 'number');
            $('#MainContent_txtTargetProductPacks').prop('step', 'any');

            $('#MainContent_txtBonusProductPacks').prop('type', 'number');
            $('#MainContent_txtBonusProductPacks').prop('step', 'any');

            if ($('#MainContent_HiddenFieldSelectedProgramTargetID').val() != '')
            {
                LoadSelectedProgramTargetDetails($('#MainContent_HiddenFieldSelectedProgramTargetID').val(), 0)
            }
        });                 

        //showing and hiding of Product Name Text Boxes
        $('#MainContent_rdbTargetPreIssue').change(function () {
            if ($(this).is(":checked")) {
                $("#MainContent_divTargetProduct").css("display", "block");
                $("#MainContent_lblTargetAmount").html("( Packs )");
            }
        });

        $('#MainContent_rdbTargetDiscount').change(function () {
            if ($(this).is(":checked")) {
                $("#MainContent_divTargetProduct").css("display", "none");
                $("#MainContent_lblTargetAmount").html("( LKR )");
            }
        });

        $('#MainContent_rdbBonusPreIssue').change(function () {
            if ($(this).is(":checked")) {
                $("#MainContent_divBonusProduct").css("display", "block");
                $("#MainContent_lblBonusAmount").html("( Packs )");
            }
        });
        $('#MainContent_rdbBonusDiscount').change(function () {
            if ($(this).is(":checked")) {
                $("#MainContent_divBonusProduct").css("display", "none");
                $("#MainContent_lblBonusAmount").html("( LKR )");
            }
        });

        //Resetting radio buttons
        $("#MainContent_buttonbar_buttonDeactivate").click(function () {
            $('#MainContent_rdbTargetPercentage').prop('checked', true);
            $('#MainContent_rdbTargetDiscount').prop('checked', true);
            $('#MainContent_rdbBonusPercentage').prop('checked', true);
            $('#MainContent_rdbBonusDiscount').prop('checked', true);
        });

        function isChecked(id,type) {
            var ReturnVal = true;
            if ($("#" + id).is(":checked")) {
                if (type == 'target') {
                    if ($("#MainContent_txtTargetProductName").val() == '' || $("#MainContent_txtTargetProductName").val() == undefined)
                        ReturnVal = false;
                }else if(type == 'bonus')
                    if ($("#MainContent_txtBonusProductName").val() == '' || $("#MainContent_txtBonusProductName").val() == undefined)
                        ReturnVal = false;
            }
            return ReturnVal;
        }

        $("#MainContent_buttonbar_buttinSave").click(function () {
            //Null validation
            programTargetEnterSaveValidations();
        });
        
    </script>
</asp:Content>

