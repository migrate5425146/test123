﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using System.Text;
using CRMServiceReference;

public partial class programs_master_program_targets : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
                DateTime today = DateTime.Today;
                dtpDate.Text = new DateTime(today.Year, today.Month, 1).ToString("dd-MMM-yyyy");
                dtpDateTo.Text = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month)).ToString("dd-MMM-yyyy");
                
                div_autocomplete.InnerHtml = SetAutocomplete("1");
                //div_autocompleteTargetProduct.InnerHtml = SetAutocompleteTargetProduct("1");
                //div_autocompleteBonusProduct.InnerHtml = SetAutocompleteBonusProduct("1");

                //Remove Other Session.
                ClearSession();
                HiddenFieldIsContinueWithSave.Value = string.Empty;
            }

            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Program Targets ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    //protected void ButtonSave_Click(object sender)
    //{
    //    //this.SaveProgramTargets();
    //}

    private void ClearSession()
    {
        Session[CommonUtility.CUSTOMER_DATA] = null;
    }

    private string SetAutocomplete(string inactive)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script type=\"text/javascript\">");
        sb.Append("     $(function () {");
        sb.Append("         $(\".tb\").autocomplete({");
        sb.Append("             source: function (request, response) {");
        sb.Append("                 $.ajax({");
        sb.Append("                     type: \"POST\",");
        sb.Append("                     contentType: \"application/json; charset=utf-8\",");
        sb.Append("                     url: \"" + ConfigUtil.ApplicationPath + "service/lead_customer/common.asmx/GetProgramTargetsByProgram\",");
        sb.Append("                     data: \"{'name':'\" + request.term + \"'}\",");
        sb.Append("                     dataType: \"json\",");
        sb.Append("                     async: true,");
        sb.Append("                     dataFilter: function(data) {");
        sb.Append("                           return data;");
        sb.Append("                     },");
        sb.Append("                     success: function (data) {");
        sb.Append("                     response($.map(data.d, function(item) {");
        sb.Append("                     return {");
        sb.Append("                         label: item.Program.ProgramName,");
        sb.Append("                         desc: item.Program.ProgramId");
        sb.Append("                     };");
        sb.Append("                     }));");

        sb.Append("                     },");
        sb.Append("                     error: function (result) {");
        sb.Append("                     }");
        sb.Append("                 });");
        sb.Append("             },");
        sb.Append("             minLength: 2,");
        sb.Append("            select: function( event, ui ) {");
        sb.Append("                 var selectedObj = ui.item;");
        sb.Append("                 $('#MainContent_HiddenFieldSelectedProgramID').val(selectedObj.desc);");
        sb.Append("                 $('#MainContent_HiddenFieldSelectedProgramName').val(selectedObj.label);");
        //sb.Append("                 if(selectedObj.label != '')");
        //sb.Append("                 {");
        //sb.Append("                     LoadProgramTargetsGrid(selectedObj.desc);");
        //sb.Append("                 }");
        sb.Append("            }");
        sb.Append("         });");
        sb.Append("     });");
        sb.Append("</script>");
        sb.Append("");

        return sb.ToString();
    }
    
}