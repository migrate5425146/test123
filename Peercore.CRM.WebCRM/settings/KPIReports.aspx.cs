﻿using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class settings_KPIReports : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            

            if (!IsPostBack)
            {
                LoadMonths();
                LoadYears();
                GetAllReportBrands();
            }
            OriginatorString.Value = UserSession.Instance.UserName;
            
            Master.SetBreadCrumb("KPI Report Settings ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
        
        ddlTargetType.SelectedValue = "area";
    }

    protected void excelUpload_Click(object sender, EventArgs e)
    {
        CommonUtility commonUtility = new CommonUtility();
        HttpSessionState session = HttpContext.Current.Session;
        string sessionID = session.SessionID;
        //DateTime targetDate = DateTime.Now;

        try
        {
            if (string.IsNullOrEmpty(hdnSelectedDate.Value) || hdnSelectedDate.Value == "")
            {
                Response.Write("<script>alert('Select target date to Upload!');</script>");
                return;
            }

            if (xmlUpload.HasFile)
            {
                string FileName = Path.GetFileName(xmlUpload.PostedFile.FileName);
                string Extension = Path.GetExtension(xmlUpload.PostedFile.FileName);
                string FolderPath = HttpContext.Current.Request.PhysicalApplicationPath + "\\uploads";
                DateTime targetDate = Convert.ToDateTime(hdnSelectedDate.Value);

                string[] validFileTypes = { ".xls", ".xlsx" };

                string FilePath = FolderPath + "\\" + FileName;

                bool isValidType = false;
                isValidType = validFileTypes.Contains(Extension);

                if (!isValidType)
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileValidator()", true);
                }
                else
                {
                    xmlUpload.SaveAs(FilePath);
                    KPIReportClient paymentsClient = new KPIReportClient();
                    string UserName = UserSession.Instance.UserName;
                    string Key = ConfigurationManager.AppSettings["All"];

                    if (paymentsClient.UploadKPIReport(FilePath, UserName, Key, targetDate))
                    {
                        xmlUpload.Attributes.Clear();
                        div_message.Attributes.Add("style", "display:block");
                        div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Uploaded.");
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileFormatValidator()", true);
                    }
                    xmlUpload.Dispose();
                }
            }
            else
            {
                Response.Write("<script>alert('Select file to upload!');</script>");
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void excelUploadWeek_Click(object sender, EventArgs e)
    {
        CommonUtility commonUtility = new CommonUtility();
        HttpSessionState session = HttpContext.Current.Session;
        string sessionID = session.SessionID;
        //DateTime targetDate = DateTime.Now;

        try
        {
            if (string.IsNullOrEmpty(hdnSelectedDate.Value) || hdnSelectedDate.Value == "")
            {
                Response.Write("<script>alert('Select target date to Upload!');</script>");
                return;
            }

            if (xmlUploadWeek.HasFile)
            {
                string FileName = Path.GetFileName(xmlUploadWeek.PostedFile.FileName);
                string Extension = Path.GetExtension(xmlUploadWeek.PostedFile.FileName);
                string FolderPath = HttpContext.Current.Request.PhysicalApplicationPath + "\\uploads";
                DateTime targetDate = Convert.ToDateTime(hdnSelectedDate.Value);

                string[] validFileTypes = { ".xls", ".xlsx" };

                string FilePath = FolderPath + "\\" + FileName;

                bool isValidType = false;
                isValidType = validFileTypes.Contains(Extension);

                if (!isValidType)
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileValidator()", true);
                }
                else
                {
                    xmlUploadWeek.SaveAs(FilePath);
                    KPIReportClient paymentsClient = new KPIReportClient();
                    string UserName = UserSession.Instance.UserName;
                    string Key = ConfigurationManager.AppSettings["All"];

                    if (paymentsClient.UploadKPIReportTargetWeekly(FilePath, UserName, Key, targetDate))
                    {
                        xmlUploadWeek.Attributes.Clear();
                        div_message.Attributes.Add("style", "display:block");
                        div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Uploaded.");
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileFormatValidator()", true);
                    }
                    xmlUploadWeek.Dispose();
                }
            }
            else
            {
                Response.Write("<script>alert('Select file to Upload!');</script>"); 
                //Response.Write("GetErrorMessageDiv('Error Occurred.', 'MainContent_div_message')");
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    public void LoadMonths()
    {
        try
        {
            var months = CultureInfo.CurrentCulture.DateTimeFormat.MonthNames;
            for (int i = 0; i < months.Length - 1; i++)
            {
                ddlMonth.Items.Add(new ListItem(months[i], i.ToString()));
            }
            ddlMonth.SelectedIndex = DateTime.Now.Month - 1;
        }
        catch (Exception)
        {

            throw;
        }
    }

    public void LoadYears()
    {
        try
        {
            int years = (DateTime.Now.Year + 1);
            for (int i = years; i >= (years - 5); i--)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlYear.SelectedIndex = 1;
        }
        catch (Exception)
        {

            throw;
        }
    }

    public void GetAllReportBrands()
    {
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.StartIndex = 1;
            args.RowCount = 50;
            args.OrderBy = "  name asc";

            KPIReportClient kpiReportClient = new KPIReportClient();
            List<KPIReportBrandsModel> data = new List<KPIReportBrandsModel>();
            data = kpiReportClient.GetAllKPIReportsBrands();

            if (data.Count != 0)
            {
                ddlReportBrands.Items.Clear();
                ddlReportBrands.Items.Add(new ListItem("- Select -", "0"));

                foreach (KPIReportBrandsModel item in data)
                {
                    ddlReportBrands.Items.Add(new ListItem(item.BrandName, item.BrandId.ToString()));
                }

                ddlReportBrands.Enabled = true;
            }
            else
            {
                ddlReportBrands.Items.Add(new ListItem("- Select -", "0"));
                ddlReportBrands.Enabled = false;
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

	protected void btnAddEmailRecipients_Click(object sender, EventArgs e)
	{
        KPIReportClient reportService = new KPIReportClient();

        bool delstatus = false;

        delstatus = reportService.UpdateKPIEmailRecipients(txtEmailRecipients.Text);

        if (delstatus)
        {
            txtEmailRecipients.Text = "";
            Response.Write("<script>alert('Successfully Added!');</script>");
        }
        else
		{
            Response.Write("<script>alert('Alredy Exsist!');</script>");
        }
    }
}