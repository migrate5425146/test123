﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="loadcrmdetails.aspx.cs" Inherits="process_forms_loadcrmdetails" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="HiddenFieldRepType" runat="server" />
    <div class="panel_container">
        <div class="area1" id="div_content">
            <div id="div_sub_content_reps" class="divshow" rel="1">
                <div id="div_reps">
                </div>
            </div>
            <div id="div_sub_content_distributor" class="divhidden" rel="2">
                <div id="div_distributor">
                </div>
            </div>
            <div id="div_sub_content_products" class="divhidden" rel="3">
                <div id="div_products">
                </div>
            </div>
            <div id="div_sub_content_customers" class="divhidden" rel="4">
                <div id="div_customers">
                </div>
            </div>
            <div id="div_sub_content_markets" class="divhidden" rel="5">
                <div id="div_markets">
                </div>
            </div>
        </div>
        <div class="area2">
            <div class="divcontectleft" id="div_1" runat="server">
                <div class="homemsgboxtopic" rel="1">
                    <div class="topic">
                        Reps</div>
                    <a class="collaps" href="#">
                        <img src="assets/images/maximizeicon.png" border="0" align="right" /></a></div>
<%--                <div id="div_message_smal" runat="server" class="msgtext">
                </div>--%>
            </div>
            <div class="clearall">
                &nbsp;</div>
            <div class="divcontectleft" id="div_2" runat="server">
                <div class="homemsgboxtopic" rel="2">
                    <div class="topic">
                        Distributor</div>
                    <a class="collaps" href="#">
                        <img src="assets/images/maximizeicon.png" border="0" align="right" /></a></div>
                <div id="div_opportunities" runat="server" class="msgtext">
                </div>
            </div>
            <div class="clearall">
                &nbsp;</div>
            <div class="divcontectleft" id="div_3" runat="server">
                <div class="homemsgboxtopic" rel="3">
                    <div class="topic">
                        Products</div>
                    <a class="collaps" href="#">
                        <img src="assets/images/maximizeicon.png" border="0" align="right" /></a></div>
                <div id="div_appointments" runat="server" class="msgtext">
                </div>
            </div>
            <div class="clearall">
                &nbsp;</div>
            <div class="divcontectleft" id="div_4" runat="server">
                <div class="homemsgboxtopic" rel="4">
                    <div class="topic">
                        Customer</div>
                    <a class="collaps" href="#">
                        <img src="assets/images/maximizeicon.png" border="0" align="right" /></a></div>
                <div id="div_reminders" runat="server" class="msgtext">
                </div>
            </div>
            <div class="clearall">
                &nbsp;</div>
            <div class="divcontectleft" id="div_5" runat="server">
                <div class="homemsgboxtopic" rel="5">
                    <div class="topic">
                        Markets</div>
                    <a class="collaps" href="#">
                        <img src="assets/images/maximizeicon.png" border="0" align="right" /></a></div>
                <div id="div_activities" runat="server" class="msgtext">
                </div>
            </div>
            <div class="clearall">
                &nbsp;</div>
        </div>
        <div style="clear: both">
        </div>
        <!-- css height patch -->
    </div>
    </form>
</body>
<script type="text/javascript">
    $("#div_sub_content_reps").attr("class", "divshow");
    GetRepSalesDetails('rep', 'div_reps'); 

    $("div#div_content div.k-grid-header-wrap").hide();

    $("div#div_1 div.homemsgboxtopic a").click(function () {
        var i = "0";

        var click_tab = $(this).parent().attr("rel");
        var $containers = $("#div_content > div");
        
        $containers.filter(function (index) {
            if ($(this).attr("class") == "divshow") {
                if (i == "0") {
                    
                    var needtoshowid = $(this).attr("id");
                    var showrel = $(this).attr("rel");
                    //alert(click_tab);
                    ShowHideContent(click_tab);
                }
                i++;
            }
        });
    });

    $("div#div_2 div.homemsgboxtopic a").click(function () {
        var i = "0";
        
        var click_tab = $(this).parent().attr("rel");
        var $containers = $("#div_content > div");

        $containers.filter(function (index) {
            if ($(this).attr("class") == "divshow") {
                if (i == "0") {
                    var needtoshowid = $(this).attr("id");
                    var showrel = $(this).attr("rel");
                    
                    ShowHideContent(click_tab);
                }
                i++;
            }
        });
    });

    $("div#div_3 div.homemsgboxtopic a").click(function () {
        var i = "0";

        var click_tab = $(this).parent().attr("rel");
        var $containers = $("#div_content > div");

        $containers.filter(function (index) {
            if ($(this).attr("class") == "divshow") {
                if (i == "0") {
                    var needtoshowid = $(this).attr("id");
                    var showrel = $(this).attr("rel");
                    //alert(click_tab);
                    ShowHideContent(click_tab);
                }
                i++;
            }
        });
    });

    $("div#div_4 div.homemsgboxtopic a").click(function () {
        var i = "0";

        var click_tab = $(this).parent().attr("rel");
        var $containers = $("#div_content > div");

        $containers.filter(function (index) {
         //   if ($(this).attr("class") == "divshow") {
                if (i == "0") {
                    var needtoshowid = $(this).attr("id");
                    var showrel = $(this).attr("rel");
                    //alert(click_tab);
                    ShowHideContent(click_tab);
                }
                i++;
           // }
        });
    });

    $("div#div_5 div.homemsgboxtopic a").click(function () {
        var i = "0";

        var click_tab = $(this).parent().attr("rel");
        var $containers = $("#div_content > div");

        $containers.filter(function (index) {
          //  if ($(this).attr("class") == "divshow") {
                if (i == "0") {
                    var needtoshowid = $(this).attr("id");
                    var showrel = $(this).attr("rel");
                    //alert(click_tab);
                    ShowHideContent(click_tab);
                }
                i++;
         //   }
        });
    });
    
</script>
</html>
