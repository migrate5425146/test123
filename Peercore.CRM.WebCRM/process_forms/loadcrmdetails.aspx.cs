﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

using Peercore.CRM.Entities;
using Peercore.CRM.Shared;
using CRMServiceReference;
using Peercore.CRM.Common;
using System.Text;

public partial class process_forms_loadcrmdetails : PageBase
{
    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadSummaryCount();
    }
    #endregion

    #region Methods
    private void LoadSummaryCount()
    {
        string originator = UserSession.Instance.FilterByUserName == "" ?
                        UserSession.Instance.UserName : UserSession.Instance.FilterByUserName;

        string date_condition = DateTime.Parse(DateTime.Now.ToString()).ToString(ConfigUtil.DateTimePatternForSql);

        ActivityClient activityClient = new ActivityClient();

        #region Args Setting
        StringBuilder sb = null;
        ArgsDTO argsDTO = new ArgsDTO();
        argsDTO.ChildOriginators = UserSession.Instance.ChildOriginators;
        argsDTO.Originator = UserSession.Instance.UserName;
        argsDTO.DefaultDepartmentId = (!string.IsNullOrEmpty(UserSession.Instance.DefaultDepartmentId)) ? UserSession.Instance.DefaultDepartmentId : ConfigUtil.DefaultDepartmentID;
        argsDTO.CRMAuthLevel = (!string.IsNullOrWhiteSpace(UserSession.Instance.FilterByUserName)) ? 0 : UserSession.Instance.CRMAuthLevel;
        argsDTO.SStartDate = date_condition;

        #endregion

        //Load Messages Summary.
        #region Messages
        //try
        //{
        //    MessagesClient messagesClient = new MessagesClient();
        //    MessagesDTO messagesDTO = messagesClient.GetMessageCount(originator, date_condition);

        //    if (messagesDTO != null)
        //    {
        //        if (messagesDTO.rowCount == 1)
        //        {
        //            if (!string.IsNullOrEmpty(messagesDTO.Message))
        //                div_message_smal.InnerHtml = messagesDTO.Message.Trim();
        //        }
        //        else if (messagesDTO.rowCount > 1)
        //        {
        //            div_message_smal.InnerHtml = messagesDTO.rowCount.ToString() + " New Message(s)";
        //        }
        //        else
        //        {
        //            div_message_smal.InnerHtml = "No New Message(s)";
        //        }
        //    }
        //}
        //catch (Exception)
        //{
        //    throw;
        //}
        #endregion

        //Load Opportunities Summary.
        #region Opportunities
        //try
        //{
            
        //    OpportunityClient opportunityClient = new OpportunityClient();
        //    int opportunityCount = opportunityClient.GetOpportunityCountForHome(0, argsDTO);

        //    if (opportunityCount != null)
        //    {
        //        if (opportunityCount == 0)
        //        {
        //            div_opportunities.InnerHtml = "No Opportunities that ends next week";
        //        }
        //        else
        //        {
        //            if (opportunityCount == 1)
        //            {
        //                div_opportunities.InnerHtml = "There is 1 opportunity that ends next week";
        //            }
        //            else
        //            {
        //                div_opportunities.InnerHtml = "There are " + opportunityCount.ToString() + " Opportunities that end next week";
        //            }
        //        }
        //    }
           
        //}
        //catch (Exception)
        //{
            
        //    throw;
        //}
        #endregion

        //Load Appointments Summary.
        #region Appointments

        //try
        //{
        //    //today.ToString("dd/MM/yy HH:mm:ss")
        //    argsDTO.SStartDate = DateTime.Today.ToString(ConfigUtil.DateTimePatternForSql);
        //    argsDTO.SEndDate = DateTime.Today.AddDays(1).ToString(ConfigUtil.DateTimePatternForSql);

        //    AppointmentClient appointmentClient = new AppointmentClient();
        //    List<KeyValuePairOfstringint> appointmentcount = appointmentClient.GetAppointmentsCountForHome(argsDTO);

        //    if (appointmentcount != null)
        //    {
        //        if (appointmentcount.Count != 0)
        //        {
        //            sb = new StringBuilder();
        //            foreach (KeyValuePairOfstringint item in appointmentcount)
        //            {
        //                if (item.key.Equals("pending"))
        //                {
        //                    if (item.value != 0)
        //                    {
        //                        sb.Append(item.value.ToString() + " Appointments are due today.</br>");
        //                    }
        //                    else
        //                    {
        //                        sb.Append("There are no appointments today. </br>");
        //                    }
        //                }
        //                else if (item.key.Equals("today"))
        //                {
        //                    if (item.value != 0)
        //                    {
        //                        sb.Append(item.value.ToString() + " Appointments are pending.</br>");
        //                    }
        //                    else
        //                    {
        //                        sb.Append("There are no pending appointments. </br>");
        //                    }
        //                }
        //            }

        //            div_appointments.InnerHtml = sb.ToString();
        //        }
        //    }
        //}
        //catch (Exception)
        //{
        //    throw;
        //}
        #endregion

        //Load Reminders Summary.
        //#region Reminders

        //try
        //{
        //    argsDTO.SStartDate = null;
        //    argsDTO.SEndDate = null;

        //    #region Current Reminders
        //    argsDTO.SToday = "Today";
        //    argsDTO.ManagerMode = UserSession.Instance.ManagerMode;

        //    argsDTO.ActivityDayStartDate = DateTime.Now.AddDays(-1).ToString(ConfigUtil.DateTimePatternForSql);
        //    argsDTO.ActivityDayEndDate = DateTime.Now.ToString(ConfigUtil.DateTimePatternForSql);

        //    int todayActivityCount = activityClient.GetScheduledActivitiesForHomeCount(argsDTO);
        //    #endregion

        //    #region Expired Reminder
        //    argsDTO.SToday = "Past";
        //    argsDTO.ActivityDayStartDate = DateTime.Now.AddDays(-1).ToString(ConfigUtil.DateTimePatternForSql);
        //    argsDTO.ActivityDayEndDate = null;
        //    int pendingActivityCount = activityClient.GetScheduledActivitiesForHomeCount(argsDTO);

        //    #endregion

        //    sb = new StringBuilder();

        //    switch (todayActivityCount)
        //    {
        //        case 0:
        //            sb.Append("There are no Reminders today.</br> ");
        //            break;
        //        case 1:
        //            sb.Append("1 Reminder is due today.</br> ");
        //            break;
        //        default:
        //            sb.Append(todayActivityCount + " Reminders are due today.</br> ");
        //            break;
        //    }

        //    switch (pendingActivityCount)
        //    {
        //        case 0:
        //            sb.Append(" There are no Past Reminders.</br> ");
        //            break;
        //        case 1:
        //            sb.Append(" 1 Reminder is pending.\n ");
        //            break;
        //        default:
        //            sb.Append(" " + pendingActivityCount + " Reminders are pending.</br>");
        //            break;
        //    }

        //    div_reminders.InnerHtml = sb.ToString();
        //}
        //catch (Exception)
        //{
            
        //    throw;
        //}

        
    
        //#endregion

        //Load Outstanding Activities.
        #region Outstanding Activities

        //try
        //{
        //    int outstandingActivityCount = activityClient.GetOutstandingActivitiesCount(argsDTO);

        //    sb = new StringBuilder();

        //    if (outstandingActivityCount != null)
        //    {
        //        switch (outstandingActivityCount)
        //        {
        //            case 0:
        //                sb.Append("There are no Outstanding Activities </br> ");
        //                break;
        //            case 1:
        //                sb.Append("There is 1 Outstanding Activity.</br> ");
        //                break;
        //            default:
        //                sb.Append("There are " + outstandingActivityCount + " Outstanding activities.</br> ");
        //                break;
        //        }
        //        div_activities.InnerHtml = sb.ToString();
        //    }
        //}
        //catch (Exception)
        //{
            
        //    throw;
        //}
        #endregion


        #region Reminders

        //try
        //{
        //    argsDTO.SStartDate = "";// DateTime.Today.ToString(ConfigUtil.DateTimePatternForSql);
        //    argsDTO.SEndDate = "";// DateTime.Today.AddDays(1).ToString(ConfigUtil.DateTimePatternForSql);


        //    argsDTO.SToday = "Today";
        //    argsDTO.SSource = "count";
        //    argsDTO.Floor = "";
        //    argsDTO.Status = "";

        //    int outstandingActivityCount = activityClient.GetRemindersForHomeCount(argsDTO);

        //    sb = new StringBuilder();

        //    if (outstandingActivityCount != null)
        //    {
        //        switch (outstandingActivityCount)
        //        {
        //            case 0:
        //                sb.Append("There are no Reminders today. </br> ");
        //                break;
        //            case 1:
        //                sb.Append("There is 1 Reminders today.</br> ");
        //                break;
        //            default:
        //                sb.Append("There are " + outstandingActivityCount + " Reminders today.</br> ");
        //                break;
        //        }
        //    }

        //    argsDTO.SToday = "Past";
        //    outstandingActivityCount = activityClient.GetRemindersForHomeCount(argsDTO);
        //    if (outstandingActivityCount != null)
        //    {
        //        switch (outstandingActivityCount)
        //        {
        //            case 0:
        //                sb.Append("There are no Reminders Past. </br> ");
        //                break;
        //            case 1:
        //                sb.Append("There is 1 Reminders Pending.</br> ");
        //                break;
        //            default:
        //                sb.Append("There are " + outstandingActivityCount + " Reminders Pending.</br> ");
        //                break;
        //        }   
        //        div_reminders.InnerHtml = sb.ToString();
        //    }
        //}
        //catch (Exception)
        //{

        //    throw;
        //}
        #endregion

        
    }
    #endregion
}