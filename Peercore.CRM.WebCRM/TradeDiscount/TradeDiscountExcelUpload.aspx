﻿<%@ Page Title="mSales | Trade Discount" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="TradeDiscountExcelUpload.aspx.cs" Inherits="TradeDiscount_TradeDiscountExcelUpload" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <style type="text/css">
        input, textarea, .uneditable-input {
            margin-bottom: 4px !important;
        }

        .message-green {
            padding: 0 !important;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server" />

    <div id="div_main" class="divcontectmainforms">
        <div id="div_message" runat="server" style="display: none;"></div>

        <div class="toolbar_container" id="id_div_toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div style="display: none" id="id_div_buttonBar">
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                    <asp:Button ID="excelUpload" runat="server" ToolTip="Upload" Text="Upload Excel" CssClass="savebtn" OnClick="excelUpload_Click" />
                </div>
            </div>

            <div class="toolbar_right" id="div3" style="display: block;">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" id="div_backButton">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/TradeDiscount/TradeDiscountViwer.aspx">
                            <div class="back"></div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>

            <div class="clearall">
            </div>

            <div class="formleft" style="width: 100%">
                <div class="formdetaildiv_right">
                    <div class="formtextdiv" style="width: 200px !important;">
                        Select Excel File
                    </div>
                    <div id="div_upload" runat="server" style="background-color: #FFFFFF; float: right; overflow: hidden;">
                        <asp:FileUpload ID="xmlUpload" runat="server" Style="float: left; font-size: 11px; height: 22px; margin-left: 10px; margin-top: 3px;" />
                        <%--<asp:Button Text="Review" runat="server" ID="btnReview" Style="float: left;" Visible="false" />--%>
                        <%--<asp:Button Text="Upload" runat="server" ID="excelUpload" Style="float: left;" OnClick="excelUpload_Click" />--%>
                        <asp:RegularExpressionValidator ID="FileUpLoadValidator" runat="server" ErrorMessage="Please Select Excel File."
                            ValidationExpression="^.*\.xls[xm]?$" ControlToValidate="xmlUpload" ForeColor="Red" Style="float: left;"></asp:RegularExpressionValidator>
                        <div class="clearall">
                        </div>
                        <div class="formdetaildiv_right">
                            <a href="../../docs/templates/Trade_Discounts.xlsx">Sample Upload File</a>
                        </div>
                    </div>

                </div>

            </div>

            <div id="div_tradediscounts" runat="server" style="display: block">
                <div style="min-width: 200px; overflow: auto">
                    <div style="float: left; width: 98.5%; overflow: auto; margin: 0px 10px;">
                        <div id="divTradeDiscountsExcelData">
                        </div>
                        <asp:GridView ID="GridView1" runat="server" OnPageIndexChanging="PageIndexChanging"
                            AllowPaging="True" AutoGenerateColumns="False" style="width: 100%">
                            <Columns>
                                <asp:BoundField DataField="date" HeaderText="Date" />
                                <asp:BoundField DataField="dbCode" HeaderText="DB Code" />
                                <asp:BoundField DataField="vouType" HeaderText="Vou Type" >
                                <HeaderStyle Width="350px" />
                                </asp:BoundField>
                                <asp:BoundField DataField="vouNo" HeaderText="Vou No" />
                                <asp:BoundField DataField="saleVal" HeaderText="Sales " />
                                <asp:BoundField DataField="discountVal" HeaderText="Discounts" />
                                <asp:BoundField DataField="uploadStatus" HeaderText="Status" >
                                <ItemStyle Font-Bold="True" ForeColor="#006600" />
                                </asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <script type="text/javascript">

        var take_grid = $("#MainContent_hfPageIndex").val();

        $(document).ready(function () {

        });

        function fileUploaded() {
            alert(1);
        }
    </script>
</asp:Content>

