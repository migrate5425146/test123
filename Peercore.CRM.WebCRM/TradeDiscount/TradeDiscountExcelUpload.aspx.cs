﻿using CRMServiceReference;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;
using ExcelApp = Microsoft.Office.Interop.Excel;

public partial class TradeDiscount_TradeDiscountExcelUpload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
            }

            OriginatorString.Value = UserSession.Instance.UserName;

            Master.SetBreadCrumb("Distributor Trade Discount ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    protected void excelUpload_Click(object sender, EventArgs e)
    {
        CommonUtility commonUtility = new CommonUtility();
        HttpSessionState session = HttpContext.Current.Session;
        string sessionID = session.SessionID;

        try
        {
            if (xmlUpload.HasFile)
            {
                string FileName = Path.GetFileName(xmlUpload.PostedFile.FileName);
                string Extension = Path.GetExtension(xmlUpload.PostedFile.FileName);
                string FolderPath = HttpContext.Current.Request.PhysicalApplicationPath + "uploads";
                //string logPath = Server.MapPath(ConfigurationManager.AppSettings["LogPath"]);
                //string logUrl = ConfigurationManager.AppSettings["LogURL"];

                string[] validFileTypes = { ".xls", ".xlsx" };

                string FilePath = FolderPath + "\\" + FileName;

                bool isValidType = false;
                isValidType = validFileTypes.Contains(Extension);

                if (!isValidType)
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileValidator()", true);
                }
                else
                {
                    xmlUpload.SaveAs(FilePath);
                    DistributorClient dbClient = new DistributorClient();
                    string UserName = UserSession.Instance.UserName;

                    LogError("TradeDiscount 1.");

                    DataTable dtFileData = UploadTradeDiscountsToDataTable(FilePath);

                    LogError("TradeDiscount 2. - Excel Row Count" + dtFileData.Rows.Count.ToString());

                    if (dtFileData.Rows.Count > 0)
                    {
                        List<TradeDiscountUploadModel> tradeDiscountList = new List<TradeDiscountUploadModel>();
                        TradeDiscountUploadModel tradeDiscount;
                        foreach (DataRow item in dtFileData.Rows)
                        {
                            tradeDiscount = new TradeDiscountUploadModel();
                            tradeDiscount.date = item[0].ToString();
                            tradeDiscount.dbCode = item[1].ToString();
                            tradeDiscount.vouType = item[2].ToString();
                            tradeDiscount.vouNo = item[3].ToString();
                            tradeDiscount.saleVal = double.Parse(item[4].ToString());
                            tradeDiscount.discountVal = double.Parse(item[5].ToString());
                            tradeDiscount.uploadStatus = "UnSuccess";

                            tradeDiscountList.Add(tradeDiscount);
                        }

                        LogError("TradeDiscount 3. - DataTable Created");

                        tradeDiscountList = dbClient.SaveTradeDiscount(tradeDiscountList);

                        //Bind Data to GridView
                        //GridView1.Caption = Path.GetFileName(FilePath);
                        GridView1.DataSource = tradeDiscountList;
                        GridView1.DataBind();

                        LogError("TradeDiscount 4. - Save Successfull");

                        xmlUpload.Attributes.Clear();
                        div_message.Attributes.Add("style", "display:block");
                        div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Uploaded.");

                        try
                        {
                            CommonClient cClient = new CommonClient();
                            cClient.CreateTransactionLog(UserName,
                                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                TransactionTypeModules.Insert,
                                TransactionModules.TradeDiscount,
                                UserName + " upload trade discount file " + FileName);
                        }
                        catch { }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileFormatValidator()", true);
                    }

                    xmlUpload.Dispose();
                }
            }
            else
            {
                //Response.Write("<script>alert('Select file to Upload!');</script>");
            }
        }
        catch (Exception ex) { LogError("8-" + ex.Message); }
    }

    protected void PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        string FolderPath = ConfigurationManager.AppSettings["FolderPath"];
        string FileName = GridView1.Caption;
        string Extension = Path.GetExtension(FileName);
        string FilePath = Server.MapPath(FolderPath + FileName);

        GridView1.PageIndex = e.NewPageIndex;
        GridView1.DataBind();
    }

    public DataTable UploadTradeDiscountsToDataTable(string FilePath)
    {
        DataTable dt = new DataTable();
        DataTable objDS = new DataTable();

        try
        {
            using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(FilePath, false))
            {
                WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                string relationshipId = sheets.First().Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                Worksheet workSheet = worksheetPart.Worksheet;
                SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();

                foreach (Cell cell in rows.ElementAt(0))
                {
                    dt.Columns.Add(GetCellValue(spreadSheetDocument, cell).Trim());
                }

                foreach (Row row in rows)
                {
                    DataRow tempRow = dt.NewRow();
                    try
                    {
                        int columnIndex = 0;
                        foreach (Cell cell in row.Descendants<Cell>())
                        {
                            int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
                            cellColumnIndex--;
                            if (columnIndex < cellColumnIndex)
                            {
                                do
                                {
                                    tempRow[columnIndex] = "";
                                    columnIndex++;
                                }
                                while (columnIndex < cellColumnIndex);
                            }
                            tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);

                            columnIndex++;
                        }

                        if (tempRow[0].ToString() != "")
                            dt.Rows.Add(tempRow);
                    }
                    catch (Exception ex) { }
                }
            }

            dt.Rows.RemoveAt(0);
            dt.AcceptChanges();

            return dt;
        }
        catch (Exception ex)
        {
            return dt;
        }
    } 
    
    #region - Common Methods -

    //--using openXml
    public static string GetCellValue(SpreadsheetDocument document, Cell cell)
    {
        try
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            if (cell.CellValue == null)
                return "";
            string value = cell.CellValue.InnerXml;
            if (cell.CellValue == null)
            {
                return "";
            }
            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    public static int? GetColumnIndexFromName(string columnName)
    {
        //return columnIndex;
        string name = columnName;
        int number = 0;
        int pow = 1;
        for (int i = name.Length - 1; i >= 0; i--)
        {
            number += (name[i] - 'A' + 1) * pow;
            pow *= 26;
        }
        return number;
    }

    public static string GetColumnName(string cellReference)
    {
        // Create a regular expression to match the column name portion of the cell name.
        Regex regex = new Regex("[A-Za-z]+");
        Match match = regex.Match(cellReference);
        return match.Value;
    }

    #endregion

    public static void LogError(string error)
    {
        try
        {
            //string logFilePath = @"C:\Logs\Log-" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";
            string logFilePath = @"D:\Logs\ErrorLog_" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";
            FileInfo logFileInfo = new FileInfo(logFilePath);
            DirectoryInfo logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirInfo.Exists) logDirInfo.Create();
            File.WriteAllText(logFilePath, String.Empty);

            using (FileStream fileStream = new FileStream(logFilePath, FileMode.Append))
            {
                using (StreamWriter log = new StreamWriter(fileStream))
                {
                    log.WriteLine(error);
                }
            }
        }
        catch (Exception)
        {
        }
    }
}