﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="TradeDiscountSettings.aspx.cs" Inherits="TradeDiscount_TradeDiscountSettings" %>

<%@ MasterType TypeName="SiteMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server" />
    <asp:HiddenField ID="hdnSelectedTrdId" runat="server" />

    <div id="div_main" class="divcontectmainforms">
            <div style="width: 100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr style="display:none;">
                                <td colspan="2" class="textalignbottom">Select Month
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="dtpMonth" maxlength="150" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr style="text-align: right;">
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonUpdateDiscountProcess" type="button">Update Approved Discounts</button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

    </div>

    <script type="text/javascript">

        //var take_grid = $("#MainContent_hfPageIndex").val();

        $(document).ready(function () {
            $("#dtpMonth").kendoDatePicker({
                value: new Date(),
                // defines the start view
                start: "year",

                // defines when the calendar should return date
                depth: "year",

                // display month and year in the input
                format: "MMMM yyyy"
            });
        });

        //Save button click
        $("#buttonUpdateDiscountProcess").click(function () {
            UpdateDiscountProcess();
        });

        //function formatDate(date) {
        //    var d = new Date(date),
        //        month = '' + (d.getMonth() + 1),
        //        day = '' + d.getDate(),
        //        year = d.getFullYear();

        //    if (month.length < 2)
        //        month = '0' + month;
        //    if (day.length < 2)
        //        day = '0' + day;

        //    return [year, month, day].join('-');
        //}

        function UpdateDiscountProcess() {
            //if (($("#dtpMonth").val()).trim() == '') {
            //    var sucessMsg = GetErrorMessageDiv("Month cannot be blank.", "MainContent_div_message");
            //    $('#MainContent_div_message').css('display', 'block');
            //    $("#MainContent_div_message").html(sucessMsg);
            //    hideStatusDiv("MainContent_div_message");
            //    return;
            //}

            //if ($("#dtpMonth").val() != '') {
                
            //}

            $("#div_loader").show();

            var dtpMonth = $("#dtpMonth").val();

            var param = {
                "dtpMonth": dtpMonth
            };

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/distributor_Service.asmx/UpdateTradeDiscountApprovedProcess",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    var sucessMsg = GetSuccesfullMessageDiv("successfully Updated.", "MainContent_div_message");
                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(sucessMsg);
                    //closepopup();
                    //LoadTradeDiscountByDistributor();
                    //ClearControls();
                    hideStatusDiv("MainContent_div_message");

                    $("#div_loader").hide();

                },
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                        $("#div_loader").hide();
                    }
                }
            });

        }

        //function closepopup() {
        //    var wnd = $("#window").kendoWindow({
        //        title: "Edit Asset",
        //        modal: true,
        //        visible: false,
        //        resizable: false
        //    }).data("kendoWindow");
        //    wnd.center().close();
        //    $("#window").css("display", "none");
        //}

    </script>
</asp:Content>

