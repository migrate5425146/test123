﻿using CRMServiceReference;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web;
using System.Web.UI;

public partial class TradeDiscount_TradeDiscountReport : System.Web.UI.Page
{
    private ReportDocument repDoc = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        Master.SetBreadCrumb("Trade Discounts Report", "#", "");
        this.repDoc = new ReportDocument();
        buttonbar1.onButtonFilter = new usercontrols_buttonbar_reports.ButtonFilter(ButtonFilter_Click);
        buttonbar1.onButtonExport = new usercontrols_buttonbar_reports.ButtonExport(ButtonExport_Click);

        buttonbar1.onDropDownListAse = new usercontrols_buttonbar_reports.DropDownListAse(DropDownListAse_SelectionChanged);
        buttonbar1.onDropDownListDistributors = new usercontrols_buttonbar_reports.DropDownListDistributors(DropDownListDistributors_SelectionChanged);
        buttonbar1.onDropDownListShowReportForASMDB = new usercontrols_buttonbar_reports.DropDownListShowReportForASMDB(DropDownListShowReportForASMDB_SelectionChanged);
        buttonbar1.onDropDownListDBActiveInactive = new usercontrols_buttonbar_reports.DropDownListDBActiveInactive(DropDownListDBActiveInactive_SelectionChanged);

        if (!IsPostBack)
        {
            buttonbar1.SetVisible(21);
            LoadOptions();
        }
    }

    private void Page_Unload(object sender, EventArgs e)
    {
        if (this.repDoc != null)
        {
            this.repDoc.Close();
            this.repDoc.Dispose();
        }
    }

    protected void ButtonFilter_Click(object sender)
    {
        try
        {
            buttonbar1.HandleExportButtonEnable(false);
            buttonbar1.HandleFilterButtonEnable(false);

            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            //if(buttonbar1.GetRepsValue)
            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument();
            string rptId = DateTime.Now.ToString("yymmddhhmm");
            SavePDF(Server.MapPath(GenerateFileName("TradeDiscount", rptId)) + ".pdf", GenerateFileName("TradeDiscount", rptId) + ".pdf", repDoc);
        }
        catch (Exception ex)
        {

        }
        finally
        {
            buttonbar1.HandleExportButtonEnable(true);
            buttonbar1.HandleFilterButtonEnable(true);
        }
    }

    protected void ButtonExport_Click(object sender)
    {
        try
        {
            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            //if(buttonbar1.GetRepsValue)
            this.repDoc = new ReportDocument();
            repDoc = GetReportDocumentExcel();
            string rptId = DateTime.Now.ToString("yyyyMMddhhmmssms");
            SaveExcel(Server.MapPath(GenerateFileName("TradeDiscount_", rptId)) + ".xlsx", GenerateFileName("TradeDiscount_", rptId) + ".xlsx", repDoc);
        }
        catch (Exception ex)
        {
        }
    }

    private void SaveExcel(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.ExcelWorkbook, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);

        System.IO.FileInfo file = new System.IO.FileInfo(FilePath);

        if (file.Exists)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.WriteFile(file.FullName);
            HttpContext.Current.Response.End();
        }
    }

    protected void DropDownListAse_SelectionChanged(object sender)
    {
        LoadDistributors();
    }

    protected void DropDownListDBActiveInactive_SelectionChanged(object sender)
    {
        LoadDistributors();
    }

    protected void DropDownListDistributors_SelectionChanged(object sender)
    {
        
    }
    
    protected void DropDownListShowReportForASMDB_SelectionChanged(object sender)
    {
        buttonbar1.HandleShowReportForASMDBDropDownChanged();
    }

    private void LoadOptions()
    {
        LoadASEs();
        LoadDistributors();

        if (UserSession.Instance.OriginatorString == "ASE")
        {
            buttonbar1.SetAsesSelect(UserSession.Instance.UserName);
            buttonbar1.HandleASEDropdownEnable(false);

            buttonbar1.HandleASEDropdownVisible(false);

            LoadDistributors();
        }
        else if (UserSession.Instance.OriginatorString == "DIST")
        {
            buttonbar1.SetAsesSelect("ALL");
            buttonbar1.SetReportForSelectASMDB("dis");
            buttonbar1.SetDistributorsSelect(UserSession.Instance.UserName);

            buttonbar1.HandleSalesReportForASMDBDropdownEnable(false);
            buttonbar1.HandleSalesReportForASMDBDropdownVisible(false);

            buttonbar1.HandleASEDropdownEnable(false);
            buttonbar1.HandleDistributerDropdownEnable(false);

            buttonbar1.HandleASEDropdownVisible(false);
            buttonbar1.HandleDistributerDropdownVisible(false);

            buttonbar1.HandleDBActiveInactiveDropdownVisible(false);

        }

        buttonbar1.HandleShowReportForASMDBDropDownChanged();
    }

    #region Load Drop Downs

    private void LoadASEs()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";
        args.AdditionalParams = " dept_string = 'ASE' ";
        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> aseList = new List<OriginatorDTO>();
        aseList = originatorClient.GetOriginatorsByCatergory(args);

        if (buttonbar1.GetShowReprtForValue().Equals("ase"))//Dont add ALL to drop down if ASE User selected
            if (aseList.Count > 0)
                aseList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetAses(aseList, "UserName");
    }

    private void LoadDistributors()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";

        string selectedDBActiveInactive = buttonbar1.GetDBActiveInactiveValue();
        string selectedASE = buttonbar1.GetAsesValue();

        if (selectedASE.Equals("ALL"))
            args.AdditionalParams = " dept_string = 'DIST' And crm_distributor_territory.status = '" + selectedDBActiveInactive + "' ";
        else
            args.AdditionalParams = " dept_string = 'DIST'  AND crm_mast_asm.originator = '" + selectedASE + "' And crm_distributor_territory.status = '" + selectedDBActiveInactive + "' ";

        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> distList = new List<OriginatorDTO>();
        distList = originatorClient.GetDistributersByASMOriginator(args);
        if (distList.Count > 0)
            distList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetDistributors(distList, "UserName");
    }

    #endregion

    private string GenerateFileName(string ori_filename, string sessionid)
    {
        string fn = "";
        DateTime d = DateTime.Now;
        fn += "../docs/" + ori_filename.Trim() + "_" + UserSession.Instance.UserName + DateTime.Now.Millisecond.ToString();

        return fn;
    }

    private void SavePDF(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.PortableDocFormat, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);
        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript1", "loadpdf('" + generatefilename + "');", true);
    }

    private DateTime? GetMonth()
    {
        try
        {
            DateTime fromDate = DateTime.Parse(buttonbar1.GetMonth());
            return fromDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GetReportForASMDB()
    {
        try
        {
            string reportfor = buttonbar1.GetShowReprtForASMDBValue();
            return reportfor;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private ReportDocument GetReportDocument()
    {
        ReportDocument rptDoc = new ReportDocument();

        rptDoc.Load(Server.MapPath("TradeDiscountReport.rpt"));
        //rptDoc.SetParameterValue("rptTitle1", GetReportSubName());
        //rptDoc.SetParameterValue("rptTitle2", "Month " + GetMonth().Value.ToString("MMM-yyyy"));

        rptDoc.SetParameterValue("@StartDate", buttonbar1.GetFromDateText());
        rptDoc.SetParameterValue("@EndDate", buttonbar1.GetToDateText());
        rptDoc.SetParameterValue("@ReportFor", buttonbar1.GetShowReprtForASMDBValue());
        rptDoc.SetParameterValue("@AseVal", buttonbar1.GetAsesValue());
        rptDoc.SetParameterValue("@DistributorVal", buttonbar1.GetDistributorsValue());

        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        Tables CrTables;

        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

        CrTables = rptDoc.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        return rptDoc;
    }
    
    private ReportDocument GetReportDocumentExcel()
    {
        ReportDocument rptDoc = new ReportDocument();

        rptDoc.Load(Server.MapPath("TradeDiscountReportExcel.rpt"));
        //rptDoc.SetParameterValue("rptTitle1", GetReportSubName());
        //rptDoc.SetParameterValue("rptTitle2", "Month " + GetMonth().Value.ToString("MMM-yyyy"));

        rptDoc.SetParameterValue("@StartDate", buttonbar1.GetFromDateText());
        rptDoc.SetParameterValue("@EndDate", buttonbar1.GetToDateText());
        rptDoc.SetParameterValue("@ReportFor", buttonbar1.GetShowReprtForASMDBValue());
        rptDoc.SetParameterValue("@AseVal", buttonbar1.GetAsesValue());
        rptDoc.SetParameterValue("@DistributorVal", buttonbar1.GetDistributorsValue());

        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        Tables CrTables;

        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

        CrTables = rptDoc.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        return rptDoc;
    }
}