﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="SRRouteView.aspx.cs" Inherits="dashboard_views_SRRouteView" %>

<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="hfOriginator" runat="server" />
    <asp:HiddenField ID="hfOriginatorType" runat="server" />
    <asp:HiddenField ID="hfTerritoryId" runat="server" />

    <div class="divcontectmainforms">
        <div class="title_bar">
            <div style="width: 90%; float: left;">
            </div>
            <div style="width: 10%; float: right;">
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Default.aspx">
                        <div class="back"></div>
                </asp:HyperLink>
            </div>
        </div>
        <div>
            <div class="formtextdiv">
                Select Date
            </div>
            <div class="formdetaildiv_right">
                <asp:TextBox ID="dtpDate" runat="server"></asp:TextBox>
                <asp:Label ID="Label1" runat="server" Text="" Font-Bold="True" Font-Size="Large"></asp:Label>
            </div>
            <%--<div class="clearall">
            </div>--%>
            <div class="formtextdiv">
                Select SR
            </div>
            <asp:DropDownList ID="ddlRepList" runat="server"
                CssClass="dropdown_small" Width="200px">
            </asp:DropDownList>

            <input type="button" id="btnLoadMap" value="Load Map" class="push_notify" />
            <div class="clearall">
            </div>
            <%--<fieldset id="form">
                <p>
                    <input class="checkbox" id="Nutrition" name="Nutrition" type="checkbox" value="Nutrition" />
                    <label for="Nutrition">Nutrition</label>
                </p>
            </fieldset>--%>
            <div id="dvMap" style="height: 800px; width: 100%;">
            </div>
            <div class="clearall">
            </div>
            <%--<div align="left" style="width: 100%;">
                <table id="HtmlTable" width="80%" border="1" style="border-color: #275D18; text-align: center;"
                    cellpadding="3" cellspacing="0">
                </table>
            </div>--%>
        </div>
    </div>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAxL4vaMJtW0dFOsFo1tpv2pUI-SJQ5a0k"></script>

    <script type="text/javascript">
        hideStatusDiv("MainContent_div_message");

        var map = undefined;
        var markersList = [];

        //You can calculate directions (using a variety of methods of transportation) by using the DirectionsService object.
        var directionsService = new google.maps.DirectionsService();

        //Define a DirectionsRenderer variable.
        var _directionsRenderer;

        $(document).ready(function () {
            initialize();

            var todayDate = kendo.toString(kendo.parseDate(new Date()), 'dd-MMM-yyyy');

            $("#MainContent_dtpDate").kendoDatePicker({
                format: "dd-MMM-yyyy",
                change: dtpDate_onChange,
                value: todayDate
            });
        });

        $("#btnLoadMap").click(function () {
            var originatorType = $("#MainContent_hfOriginatorType").val();
            var originator = $("#MainContent_hfOriginator").val();

            var divA = document.getElementById("MainContent_ddlRepList");

            var selectRep = divA.options[divA.selectedIndex].value;
            var selectDate = $('#MainContent_dtpDate').val()

            //var apiUrl = ROOT_PATH + "/api/dashboard";
            var apiUrl = "https://localhost:44362/api/dashboard";
            var serviceUrl = ROOT_PATH + "service/master/master.asmx/LoadSRRouteLocations";

            markersList = [];

            //You can calculate directions (using a variety of methods of transportation) by using the DirectionsService object.
            directionsService = new google.maps.DirectionsService();

            //Define a DirectionsRenderer variable.
            _directionsRenderer = null;
            initialize();

            $.ajax({
                type: "POST",
                url: serviceUrl,
                data: '{ "apiUri": "' + apiUrl + '", ' +
                    ' "selectRep": "' + selectRep + '", ' +
                    ' "selectDate": "' + selectDate + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    DrawMap(data.d.Data);
                },
                failure: OnFail
            });
        });

        function dtpDate_onChange() {
            //LoadStockGrid();
        }

        // Sets the map on all markers in the array.
        function setMapOnAll(map) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(map);
            }
        }

        function DrawMap(locations) {

            var invDesc = '';
            var custDesc = '';
            var htmlTable = '';
            //Create a html tr count variable
            var _htmlTrCount = 0;

            locationList = [];

            jQuery.each(locations, function (i, val) {

                if (val.invType == 'I') {
                    invDesc = '<div id="content"> ' +
                        '<h2 id="firstHeading" class="firstHeading"><span style="color: #993366;">' + val.custName + ' (' + val.custCode + ')</span></h2> ' +
                        '<p><span style="color: #333399;">Invoice No: ' + val.invNo + '</span><br /><span style="color: #333399;">Invoice Total: ' + val.invTotal.toString() + '</span></p> ' +
                        '<div id="bodyContent"> ' +
                        '<p>Customer:<br /><span style="color: #339966;">Address: ' + val.custAddress + '</span><br /> ' +
                        '<span style = "color: #339966;"> Telephone: ' + val.custTel + '</span > <br /> ' +
                        '<span style="color: #339966;">Email: ' + val.custEmail + '</span> ' +
                        '</p> ' +
                        '</div> ' +
                        '</div> ';
                }
                else if (val.invType == 'O') {
                    invDesc = '<div id="content"> ' +
                        '<h2 id="firstHeading" class="firstHeading"><span style="color: #993366;">' + val.custName + ' (' + val.custCode + ')</span></h2> ' +
                        '<p><span style="color: #333399;">Order No: ' + val.invNo + '</span><br /><span style="color: #333399;">Order Total: ' + val.invTotal.toString() + '</span></p> ' +
                        '<div id="bodyContent"> ' +
                        '<p>Customer:<br /><span style="color: #339966;">Address: ' + val.custAddress + '</span><br /> ' +
                        '<span style = "color: #339966;"> Telephone: ' + val.custTel + '</span > <br /> ' +
                        '<span style="color: #339966;">Email: ' + val.custEmail + '</span> ' +
                        '</p> ' +
                        '</div> ' +
                        '</div> ';
                }

                custDesc = '<div id="content"> ' +
                    '<h2 id="firstHeading" class="firstHeading"><span style="color: #993366;">' + val.custName + ' (' + val.custCode + ')</span></h2> ' +
                    '<div id="bodyContent"> ' +
                    '<p><span style="color: #339966;">Address: ' + val.custAddress + '</span><br /> ' +
                    '<span style = "color: #339966;"> Telephone: ' + val.custTel + '</span > <br /> ' +
                    '<span style="color: #339966;">Email: ' + val.custEmail + '</span> ' +
                    '</p> ' +
                    '</div> ' +
                    '</div> ';

                var location = {
                    custCode: val.custCode,
                    invNo: val.invNo,
                    label: val.SequenceId,
                    title: val.custCode + " " + val.custName,
                    lat: parseFloat(val.invLatitude),
                    lng: parseFloat(val.invLongitude),
                    invdesc: invDesc,
                    custlat: parseFloat(val.custLatitude),
                    custlng: parseFloat(val.custLongitude),
                    custdesc: custDesc
                };

                locationList.push(location);

                //_htmlTrCount++;

                //var invLatlng = new google.maps.LatLng(parseFloat(val.invLatitude), parseFloat(val.invLongitude));
                //var custLatlng = new google.maps.LatLng(parseFloat(val.custLatitude), parseFloat(val.custLongitude));

                //htmlTable = '</br>';
                //CreateHTMTable(htmlTable, invLatlng, custLatlng, val.custName + ' (' + val.custCode + ')');
            });

            var infoWindow = new google.maps.InfoWindow();
            var lat_lng = new Array();
            var lat_lng_cust = new Array();
            var latlngbounds = new google.maps.LatLngBounds();

            for (i = 0; i < locationList.length; i++) {
                var data = locationList[i];

                if (parseFloat(data.lat) > 0 && parseFloat(data.lng)) {
                    var myLatlng = new google.maps.LatLng(data.lat, data.lng);
                    lat_lng.push(myLatlng);
                    var marker = new google.maps.Marker({
                        position: myLatlng,
                        map: map,
                        label: data.label.toString(),
                        title: data.label.toString()

                    });

                    latlngbounds.extend(marker.position);

                    (function (marker, data) {
                        google.maps.event.addListener(marker, "click", function (e) {
                            infoWindow.setContent(data.invdesc);
                            infoWindow.open(map, marker);
                        });
                    })(marker, data);
                }

                if (parseFloat(data.custlat) > 0 && parseFloat(data.custlng)) {
                    var myCustLatlng = new google.maps.LatLng(data.custlat, data.custlng);
                    lat_lng_cust.push(myCustLatlng);
                    var markerCust = new google.maps.Marker({
                        position: myCustLatlng,
                        map: map,
                        //label: data.label.toString(),
                        title: data.label.toString(),
                        icon: {
                            url: 'https://chart.googleapis.com/chart?chst=d_bubble_text_small&chld=bb|' + data.label.toString() /*+ ' - ' + data.custCode*/ + '|f5faac|363823'
                        }
                    });

                    (function (markerCust, data) {
                        google.maps.event.addListener(markerCust, "click", function (e) {
                            infoWindow.setContent(data.custdesc);
                            infoWindow.open(map, markerCust);
                        });
                    })(markerCust, data);
                }
            }

            map.setCenter(latlngbounds.getCenter());
            map.fitBounds(latlngbounds);

            getRoutePointsAndWaypoints(lat_lng);

            //CreateHTMTable() will help you to create a dynamic html table
            function CreateHTMTable(htmlTable, _invlatlng, _custlatlng, _otletName) {
                debugger;
                // The markers for The Dakota and The Frick Collection                var mk1 = new google.maps.Marker({
                    position: _invlatlng,
                    map: map,
                    suppressMarkers: true
                });                var mk2 = new google.maps.Marker({
                    position: _custlatlng,
                    map: map,
                    suppressMarkers: true
                });

                var distance = haversine_distance(mk1, mk2);

                htmlTable = htmlTable + "<tr id=\"" + _htmlTrCount + "\">";
                htmlTable = htmlTable + "<td style=\"width: 80px;\">" + _htmlTrCount + "</td>";
                htmlTable = htmlTable + "<td style=\"width: 100px;\"><span id=\"outlet_" + _htmlTrCount + "\">" + _otletName + "</span></td>";
                htmlTable = htmlTable + "<td style=\"width: 100px;\"><span id=\"lat_" + _htmlTrCount + "\">" + _invlatlng + "</span></td>";
                htmlTable = htmlTable + "<td style=\"width: 100px;\"><span id=\"lng_" + _htmlTrCount + "\">" + _custlatlng + "</span></td>";
                htmlTable = htmlTable + "<td style=\"width: 100px;\"><span id=\"dir_" + _htmlTrCount + "\">" + distance + "</span></td>";
                htmlTable = htmlTable + "</tr>";
                $("#HtmlTable").append(htmlTable);
            }

            function haversine_distance(mk1, mk2) {                var R = 3958.8; // Radius of the Earth in miles                var rlat1 = mk1.position.lat() * (Math.PI / 180); // Convert degrees to radians                var rlat2 = mk2.position.lat() * (Math.PI / 180); // Convert degrees to radians                var difflat = rlat2 - rlat1; // Radian difference (latitudes)                var difflon = (mk2.position.lng() - mk1.position.lng()) * (Math.PI / 180); // Radian difference (longitudes)                var d = 2 * R * Math.asin(Math.sqrt(Math.sin(difflat / 2) * Math.sin(difflat / 2) + Math.cos(rlat1) * Math.cos(rlat2) * Math.sin(difflon / 2) * Math.sin(difflon / 2)));                return (d * 1.60934).toFixed(2);            }
        }

        function initialize() {
            var mapOptions = {
                center: new google.maps.LatLng(7.4693167, 80.8009264),
                zoom: 8,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };

            map = new google.maps.Map(document.getElementById('dvMap'), mapOptions);
        }

        function chunkArray(myArray, chunk_size) {
            var index = 0;
            var arrayLength = myArray.length;
            var tempArray = [];

            for (index = 0; index < arrayLength; index += chunk_size) {
                if (index == 0) {
                    myChunk = myArray.slice(index, index + chunk_size);
                }
                else {
                    myChunk = myArray.slice(index-1, index + chunk_size);
                }
                // Do something if you want with the group
                tempArray.push(myChunk);
            }

            return tempArray;
        }

        //getRoutePointsAndWaypoints() will help you to pass points and waypoints to drawRoute() function
        function getRoutePointsAndWaypoints(_mapPointsAll) {
            debugger;
            var result = chunkArray(_mapPointsAll, 22);

            for (var k = 0; k <= result.length - 1; k++) {

                var _mapPoints = result[k];

                //Define a variable for waypoints.
                var _waypoints = new Array();

                if (_mapPoints.length > 2) //Waypoints will be come.
                {
                    for (var j = 1; j < _mapPoints.length - 1; j++) {
                        var address = _mapPoints[j];
                        if (address !== "") {
                            _waypoints.push({
                                location: address,
                                stopover: true  //stopover is used to show marker on map for waypoints
                            });
                        }
                    }

                    var rendererOptions = {
                        map: map,
                        suppressMarkers: true
                    }

                    _directionsRenderer = new google.maps.DirectionsRenderer(rendererOptions);

                    //Call a drawRoute() function
                    drawRoute(_directionsRenderer, _mapPoints[0], _mapPoints[_mapPoints.length - 1], _waypoints);

                } else if (_mapPoints.length > 1) {
                    var rendererOptions = {
                        map: map,
                        suppressMarkers: true
                    }
                    _directionsRenderer = new google.maps.DirectionsRenderer(rendererOptions);

                    //Call a drawRoute() function only for start and end locations
                    drawRoute(_directionsRenderer, _mapPoints[_mapPoints.length - 2], _mapPoints[_mapPoints.length - 1], _waypoints);
                } else {
                    var rendererOptions = {
                        map: map,
                        suppressMarkers: true
                    }
                    _directionsRenderer = new google.maps.DirectionsRenderer(rendererOptions);

                    //Call a drawRoute() function only for one point as start and end locations.
                    drawRoute(_directionsRenderer, _mapPoints[_mapPoints.length - 1], _mapPoints[_mapPoints.length - 1], _waypoints);
                }
            }
        }

        //drawRoute() will help actual draw the route on map.
        function drawRoute(directionsRenderer, originAddress, destinationAddress, _waypoints) {
            //Define a request variable for route .
            var _request = '';

            //This is for more then two locatins
            if (_waypoints.length > 0) {
                _request = {
                    origin: originAddress,
                    destination: destinationAddress,
                    waypoints: _waypoints, //an array of waypoints
                    optimizeWaypoints: true, //set to true if you want google to determine the shortest route or false to use the order specified.
                    travelMode: google.maps.DirectionsTravelMode.WALKING
                };
            } else {
                //This is for one or two locations. Here noway point is used.
                _request = {
                    origin: originAddress,
                    destination: destinationAddress,
                    travelMode: google.maps.DirectionsTravelMode.WALKING
                };
            }

            //This will take the request and draw the route and return response and status as output
            directionsService.route(_request, function (_response, _status) {
                if (_status == google.maps.DirectionsStatus.OK) {
                    directionsRenderer.setDirections(_response);
                }
            });
        }



    </script>
</asp:Content>

