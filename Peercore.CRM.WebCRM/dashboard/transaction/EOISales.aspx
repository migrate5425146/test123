﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="EOISales.aspx.cs" Inherits="dashboard_transaction_EOISales" %>
<%@ Register Src="~/usercontrols/buttonbar_analysisgraph.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ Register Src="~/usercontrols/toolbar_analysisgraph.ascx" TagPrefix="ucl" TagName="toolbar" %>
<%@ MasterType TypeName="SiteMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<div id="newcust_window">
                <div id="div_newcustgrid">
                </div>
               
            </div>

<div class="divcontectmainforms">
    <div class="title_bar">
        <div style="width:90%; float:left;">
                    <ucl:toolbar ID="toolbar1" runat="server" />
        </div>
        <div style="width:10%; float:right;">
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/dashboard/transaction/EndUserEnquiry.aspx">
                        <div class="back"></div>
                        </asp:HyperLink>
        </div>    
     </div>
        <%--<div class="homemsgboxtopic">
            <div style="float: left; width: 65%">
                Hot Operator Activity Report
            </div>
            <div style="float: right; width: 35%;" align="right">
            <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/dashboard/transaction/AnalysisGraph.aspx?rptid=3">
                        <img src="../../assets/images/back_button.png" alt="" border="0" />
            </asp:HyperLink>                
            </div>
        </div>--%>
        <div class="clearall"></div>
        <div align="right">
            <div class="hoributtongraph" align="left">
                <div id="div_message" class="savemsg" runat="server" style="display:none" ></div>        
                <div style="padding-left:10px;"><ucl:buttonbar ID="buttonbar1" runat="server" /></div>
            </div>
        </div>
        <%--<div class="clearall">&nbsp;</div> --%> 
        <div style="margin-top:25px;">
        


            <div style="min-width: 200px; overflow: auto;">
            
            <div id="gridEoiSales">
            </div>
                
            </div>
    </div></div>

    <script type="text/javascript">
        $(document).ready(function () {
            onChange_SummaryReport();
            $("#div_summaryreport").css("display", "block");
            $("#MainContent_toolbar1_a_sales").css("background-color", "#ff6600");
            $("#MainContent_buttonbar1_btnFilter").css("margin-top", "10px");
            $("#btnexport").css("margin-top", "10px");
            
            
//            $("#div_distributor").css("display", "block");
            //            $("#div_distributor").css("display", "block");
            //            $("#div_enduser").css("display", "block");
            //GridLeadCustOppCount('');
            //GridEndUserEnquiry();

            var newcust_window = $("#newcust_window"),
                        newcust_undo = $("#undo")
                                .bind("click", function () {
                                    newcust_window.data("kendoWindow").open();
                                    newcust_undo.hide();
                                });

            var onClose = function () {
                newcust_undo.show();
            }

            if (!newcust_window.data("kendoWindow")) {
                newcust_window.kendoWindow({
                    width: "500px",
                    height: "400px",
                    title: "Lookup",
                    close: onClose
                });
            }

            newcust_window.data("kendoWindow").close();

            $("#id_distributor").click(function () {
                $("div#div_loader").show();
                loaddistributorLookup('div_newcustgrid', 'newcust_window','1');
            });
        });


        
	</script>

</asp:Content>

