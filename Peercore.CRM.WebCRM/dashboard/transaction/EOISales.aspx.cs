﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using Peercore.CRM.Shared;
using System.Text;
using Peercore.CRM.Common;

public partial class dashboard_transaction_EOISales : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        Master.SetBreadCrumb("Dashboard", "#", "");
        Master.SetBreadCrumb("Sales Report", "#", "");
        buttonbar1.onButtonFilter = new usercontrols_buttonbar_analysisgraph.ButtonFilter(ButtonFilter_Click);
        //buttonbar1.onButtonExport = new usercontrols_buttonbar_analysisgraph.ButtonExport(ButtonExport_Click);
        //buttonbar1.onDropDownListReps = new usercontrols_buttonbar_analysisgraph.DropDownListReps(DropDownListReps_SelectionChanged);
        //buttonbar1.onDropDownListSector = new usercontrols_buttonbar_analysisgraph.DropDownListSector(DropDownListSector_SelectionChanged);

        if (!IsPostBack)
        {
            buttonbar1.SetVisible(5);
            LoadReps();
            //LoadOptions();
            ButtonFilter_Click(this);
        }

    }

    private void LoadReps()
    {
        OriginatorClient originatorClient = new OriginatorClient();

        try
        {
            OriginatorDTO originator = new OriginatorDTO();
            ArgsDTO args = new ArgsDTO();
            args.Originator = UserSession.Instance.UserName;
            args.ManagerMode = UserSession.Instance.ManagerMode;

            List<OriginatorDTO> lstOriginator = originatorClient.GetChildOriginatorsList(args);
            originator = originatorClient.GetOriginator(args.Originator);
            originator.RepCode = UserSession.Instance.RepCode;
            originator.UserName = args.Originator;
            lstOriginator.Add(originator);

            // ALL
            //originator = new OriginatorDTO();
            //originator.OriginatorId = 0;
            //originator.UserName = "ALL";
            //originator.Name = "National";
            //lstOriginator.Add(originator);

            lstOriginator.OrderBy(s => s.Name);

            buttonbar1.SetReps(lstOriginator, "RepCode");

            buttonbar1.SetRepsSelect(UserSession.Instance.RepCode);
        }
        catch (Exception oException)
        {
        }

    }

    //private void LoadOptions()
    //{
    //    try
    //    {
    //        SalesClient salesClient = new SalesClient();
    //        ArgsDTO args = new ArgsDTO();
    //        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
    //        List<EndUserSalesDTO> endUserSalesList = salesClient.GetCostPeriod(args);

    //        buttonbar1.SetCostPeriodLoad(endUserSalesList);
    //        //cmbCostPeriod.DisplayMemberPath = "Value";
    //        //cmbCostPeriod.SelectedValuePath = "Key";

    //        endUserSalesList = salesClient.GetCostYears();

    //        buttonbar1.SetCostYearLoad(endUserSalesList);

    //        EndUserSalesDTO costPeriod = salesClient.GetCurrentCostPeriod();
    //        buttonbar1.SetCostPeriodSelect(costPeriod.Period.ToString());
    //        buttonbar1.SetCostYearSelect(costPeriod.Year.ToString());


    //    }
    //    catch (Exception)
    //    {
    //        throw;
    //    }
    //}



    protected void ButtonFilter_Click(object sender)
    {
        ArgsDTO argsDTO = new ArgsDTO();

        string sreport = buttonbar1.GetSummaryReport();
        if (sreport == "0")
        {
            argsDTO.CustomerCode = buttonbar1.GetDistributorCode();
            argsDTO.RepCode = "";
        }
        else
        {
            argsDTO.CustomerCode = "";
            argsDTO.RepCode = buttonbar1.GetRepsValue();
        }
        
        argsDTO.RepType = UserSession.Instance.RepType;


        Session[CommonUtility.GLOBAL_SETTING] = argsDTO;

        if (!string.IsNullOrEmpty(argsDTO.RepCode) || !string.IsNullOrEmpty(argsDTO.CustomerCode))
            ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "GridEoiSales();", true);
    }
}