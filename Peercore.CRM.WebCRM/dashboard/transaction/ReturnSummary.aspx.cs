﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using Peercore.CRM.Shared;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Net;
using System.Configuration;

public partial class dashboard_transaction_ReturnSummary : System.Web.UI.Page
{
    
    private void LoadASEs()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";
        args.AdditionalParams = " dept_string = 'ASE' ";
        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> aseList = new List<OriginatorDTO>();
        aseList = originatorClient.GetOriginatorsByCatergory(args);

        if (buttonbar1.GetShowReprtForValue().Equals("ase"))//Dont add ALL to drop down if ASE User selected
            if (aseList.Count > 1)
                aseList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetAses(aseList, "UserName");
    }

    private void LoadDistributors()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";

        string selectedASE = buttonbar1.GetAsesValue();

        if (selectedASE.Equals("ALL"))
            args.AdditionalParams = " dept_string = 'DIST' ";
        else
            args.AdditionalParams = " dept_string = 'DIST'  AND crm_mast_asm.originator = '" + selectedASE + "' ";

        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> distList = new List<OriginatorDTO>();
        distList = originatorClient.GetDistributersByASMOriginator(args);
        if (distList.Count > 1)
            distList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetDistributors(distList, "UserName");
    }

    private void LoadReps()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";

        string strASM = GetASEString();
        string strDIST = GetDisString();

        if (strDIST.Equals(""))
        {
            if (strASM.Equals(""))
                args.AdditionalParams = " originator.dept_string = 'DR' ";
            else
                args.AdditionalParams = " originator.dept_string = 'DR'  AND crm_mast_asm.originator = '" + strASM + "' ";
        }
        else
        {
            args.AdditionalParams = " originator.dept_string = 'DR'  AND crm_distributor.originator = '" + strDIST + "' ";
        }

        if (UserSession.Instance.OriginatorString == "DIST")
        {
            strDIST = UserSession.Instance.UserName;
            args.AdditionalParams = " originator.dept_string = 'DR'  AND crm_distributor.originator = '" + strDIST + "' ";
        }

        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> repList = new List<OriginatorDTO>();
        repList = originatorClient.GetAllSRByOriginator(args);
        if (repList.Count > 1)
            repList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetReps(repList, "UserName");
    }

    private OriginatorDTO GetDistributorById(string Id)
    {
        OriginatorClient originatorClient = new OriginatorClient();
        OriginatorDTO distList = new OriginatorDTO();
        distList = originatorClient.GetOriginator(Id);

        return distList;
    }



    private void LoadOptions()
    {
        LoadASEs();


        if (UserSession.Instance.OriginatorString == "ASE")
        {
            buttonbar1.SetReportForSelect("rep");
            buttonbar1.HandleSalesReportForDropdownEnable(false);

            buttonbar1.SetAsesSelect(UserSession.Instance.UserName);
            buttonbar1.HandleASEChanged_for_TradeLoadSummary();
            buttonbar1.HandleASEDropdownEnable(false);

            buttonbar1.HandleASEDropdownVisible(false);
            buttonbar1.HandleSalesReportForDropdownVisible(false);
        }

        LoadDistributors();

        if (UserSession.Instance.OriginatorString == "DIST")
        {
            buttonbar1.SetReportForSelect("distributor");
            buttonbar1.HandleSalesReportForDropdownEnable(false);

            buttonbar1.SetAsesSelect("ALL");
            buttonbar1.HandleASEChanged_for_TradeLoadSummary();

            buttonbar1.SetDistributorsSelect(UserSession.Instance.UserName);
            buttonbar1.HandleDistributorChanged_for_TradeLoadSummary();

            buttonbar1.SetTradeloadRepTypeSelect("p");

            buttonbar1.HandleASEDropdownEnable(false);
            buttonbar1.HandleDistributerDropdownEnable(false);
            buttonbar1.HandleTradeloadRepTypeDropdownEnable(false);

            buttonbar1.HandleASEDropdownVisible(false);
            buttonbar1.HandleDistributerDropdownVisible(false);
            buttonbar1.HandleTradeloadRepTypeDropdownVisible(false);
            buttonbar1.HandleSalesReportForDropdownVisible(false);
        }

        LoadReps();
    }

    private string GetASEString()
    {
        try
        {
            string reportfor = buttonbar1.GetAsesValue();
            if (reportfor == "ALL")
                reportfor = "";
            return reportfor;
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    private string GetDisString()
    {
        try
        {
            string reportfor = buttonbar1.GetDistributorsValue();
            if (reportfor == "ALL")
                reportfor = "";
            return reportfor;
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    private DateTime? GetFromDate()
    {
        try
        {
            DateTime fromDate = DateTime.Parse(buttonbar1.GetFromDateText());
            return fromDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private DateTime? GetToDate()
    {
        try
        {
            DateTime toDate = DateTime.Parse(buttonbar1.GetToDateText()).AddDays(1);
            return toDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GetReportSubName_Ase()
    {
        string reportName = "";

        //if (buttonbar1.GetShowReprtForValue().Equals("ase"))
        //{
        reportName = "ASM : " + buttonbar1.GetAsesText();
        //}
        //else
        //{
        //    reportName = "ASE : ALL";
        //}

        if (buttonbar1.GetAsesText() == "")
            reportName = "ASM : ALL";

        return reportName;
    }

    private string GetReportSubName_Dis()
    {
        string strDisValue = "";
        string reportName = "";

        //if (buttonbar1.GetShowReprtForValue().Equals("distributor"))
        //{
        strDisValue = buttonbar1.GetDistributorsValue();
        if (strDisValue == "ALL")
        {
            reportName = "Distributor : ALL";
        }
        else
        {
            reportName = "Distributor : " + buttonbar1.GetDistributorsText();
        }
        //}
        //else if (buttonbar1.GetShowReprtForValue().Equals("rep"))
        //{
        //    reportName = "Distributor : ALL";
        //}

        if (buttonbar1.GetDistributorsValue() == "")
            reportName = "Distributor : ALL";

        return reportName;
    }

    private string GetReportSubName_DisCode()
    {
        string strDisValue = "";
        string reportName = "";

        strDisValue = buttonbar1.GetDistributorsValue();

        if ((strDisValue == "ALL") || (strDisValue == ""))
        {
            reportName = "";
        }
        else
        {
            reportName = GetDistributorById(strDisValue).OriginatorId.ToString();
        }

        return reportName;
    }

    private string GetReportSubName_Rep()
    {
        string reportName = "";

        //if (buttonbar1.GetShowReprtForValue().Equals("rep"))
        //{
        reportName = "Rep : " + buttonbar1.GetRepsText();
        //}
        if (buttonbar1.GetRepsText() == "")
            reportName = "Rep : ALL";

        return reportName;
    }

    private string FilterByReport()
    {
        string reportFilter = "";

        if (buttonbar1.GetShowReprtForValue().Equals("ase"))
        {
            if (buttonbar1.GetAsesValue().Equals("ALL"))
            {
                reportFilter = "";
            }
            else
            {
                reportFilter = "ase_id = '" + buttonbar1.GetAsesValue() + "'";
            }
        }

        if (buttonbar1.GetShowReprtForValue().Equals("distributor"))
        {
            if (buttonbar1.GetDistributorsValue().Equals("ALL") || buttonbar1.GetDistributorsValue().Equals(""))
            {
                reportFilter = "ase_id = '" + buttonbar1.GetAsesValue() + "'";
            }
            else
            {
                reportFilter = "dis_id = '" + buttonbar1.GetDistributorsValue() + "'";
            }
        }

        if (buttonbar1.GetShowReprtForValue().Equals("rep"))
        {
            if (buttonbar1.GetRepsValue().Equals("ALL") || buttonbar1.GetRepsValue().Equals(""))
            {
                if (buttonbar1.GetDistributorsValue().Equals("ALL") || buttonbar1.GetDistributorsValue().Equals(""))
                {
                    reportFilter = "ase_id = '" + buttonbar1.GetAsesValue() + "'";
                }
                else
                {
                    reportFilter = "dis_id = '" + buttonbar1.GetDistributorsValue() + "'";
                }
            }
            else
            {
                reportFilter = "rep_id = '" + buttonbar1.GetRepsValue() + "'";
            }
        }

        return reportFilter;
    }

    #region report methods

    private string GenerateFileName(string ori_filename, string sessionid)
    {
        string fn = "";
        DateTime d = DateTime.Now;
        fn += "../../docs/" + ori_filename.Trim() + "_" + UserSession.Instance.UserName + "_" + sessionid.Trim() + DateTime.Now.Millisecond.ToString();

        return fn;
    }

    private void SavePDF(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.PortableDocFormat, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);
        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript1", "loadpdf('" + generatefilename + "');", true);
    }

    private void SaveExcel(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.ExcelWorkbook, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);
        System.IO.FileInfo file = new System.IO.FileInfo(FilePath);
        if (file.Exists)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.WriteFile(file.FullName);
            HttpContext.Current.Response.End();
        }
    }

    private ReportDocument GetReportDocument()
    {
        ReportDocument rptDoc = new ReportDocument();

        if (buttonbar1.GetVisitNoteValue().Equals("visitnote"))
        {
            rptDoc.Load(Server.MapPath("../reports/visit_note_summary.rpt"));

            if (UserSession.Instance.OriginatorString == "DIST")
            {
                if (buttonbar1.GetRepsValue() == "ALL")
                {
                    rptDoc.SetParameterValue("@RepCodeList", "dis_code = '" + buttonbar1.GetDistributorsValue() + "'");
                }
                else if (buttonbar1.GetRepsValue().Length == 0)
                {
                    rptDoc.SetParameterValue("@RepCodeList", "dis_code = '" + buttonbar1.GetDistributorsValue() + "'");
                }
                else
                {
                    rptDoc.SetParameterValue("@RepCodeList", "rep_code = '" + buttonbar1.GetRepsValue() + "'");
                }
            }
            else
            {
                if (buttonbar1.GetAsesValue() == "ALL")
                {
                    rptDoc.SetParameterValue("@RepCodeList", "");
                }
                else
                {
                    if (buttonbar1.GetDistributorsValue() == "ALL")
                    {
                        rptDoc.SetParameterValue("@RepCodeList", "ase_code = '" + buttonbar1.GetAsesValue() + "'");
                    }
                    else if (buttonbar1.GetDistributorsValue().Length == 0)
                    {
                        rptDoc.SetParameterValue("@RepCodeList", "ase_code = '" + buttonbar1.GetAsesValue() + "'");
                    }
                    else
                    {
                        if (buttonbar1.GetRepsValue() == "ALL")
                        {
                            rptDoc.SetParameterValue("@RepCodeList", "dis_code = '" + buttonbar1.GetDistributorsValue() + "'");
                        }
                        else if (buttonbar1.GetRepsValue().Length == 0)
                        {
                            rptDoc.SetParameterValue("@RepCodeList", "dis_code = '" + buttonbar1.GetDistributorsValue() + "'");
                        }
                        else
                        {
                            rptDoc.SetParameterValue("@RepCodeList", "rep_code = '" + buttonbar1.GetRepsValue() + "'");
                        }
                    }
                }
            }
        }
        else if (buttonbar1.GetVisitNoteValue().Equals("invoicesummary"))
        {
            rptDoc.Load(Server.MapPath("../reports/CrystalReport3.rpt"));

            if (UserSession.Instance.OriginatorString == "DIST")
            {
                if (buttonbar1.GetRepsValue() == "ALL")
                {
                    rptDoc.SetParameterValue("@RepCodeList", "dis_id = '" + buttonbar1.GetDistributorsValue() + "'");
                }
                else if (buttonbar1.GetRepsValue().Length == 0)
                {
                    rptDoc.SetParameterValue("@RepCodeList", "dis_id = '" + buttonbar1.GetDistributorsValue() + "'");
                }
                else
                {
                    rptDoc.SetParameterValue("@RepCodeList", "rep_code = '" + buttonbar1.GetRepsValue() + "'");
                }
            }
            else
            {
                if (buttonbar1.GetAsesValue() == "ALL")
                {
                    rptDoc.SetParameterValue("@RepCodeList", "");
                }
                else
                {
                    if (buttonbar1.GetDistributorsValue() == "ALL")
                    {
                        rptDoc.SetParameterValue("@RepCodeList", "ase_id = '" + buttonbar1.GetAsesValue() + "'");
                    }
                    else if (buttonbar1.GetDistributorsValue().Length == 0)
                    {
                        rptDoc.SetParameterValue("@RepCodeList", "ase_id = '" + buttonbar1.GetAsesValue() + "'");
                    }
                    else
                    {
                        if (buttonbar1.GetRepsValue() == "ALL")
                        {
                            rptDoc.SetParameterValue("@RepCodeList", "dis_id = '" + buttonbar1.GetDistributorsValue() + "'");
                        }
                        else if (buttonbar1.GetRepsValue().Length == 0)
                        {
                            rptDoc.SetParameterValue("@RepCodeList", "dis_id = '" + buttonbar1.GetDistributorsValue() + "'");
                        }
                        else
                        {
                            rptDoc.SetParameterValue("@RepCodeList", "rep_code = '" + buttonbar1.GetRepsValue() + "'");
                        }
                    }
                }
            }
        }
        else if (buttonbar1.GetVisitNoteValue().Equals("cancelinvoice"))
        {
            rptDoc.Load(Server.MapPath("../reports/invoice_cancel.rpt"));

            if (UserSession.Instance.OriginatorString == "DIST")
            {
                if (buttonbar1.GetRepsValue() == "ALL")
                {
                    rptDoc.SetParameterValue("@RepCodeList", "dis_id = '" + buttonbar1.GetDistributorsValue() + "'");
                }
                else if (buttonbar1.GetRepsValue().Length == 0)
                {
                    rptDoc.SetParameterValue("@RepCodeList", "dis_id = '" + buttonbar1.GetDistributorsValue() + "'");
                }
                else
                {
                    rptDoc.SetParameterValue("@RepCodeList", "rep_code = '" + buttonbar1.GetRepsValue() + "'");
                }
            }
            else
            {
                if (buttonbar1.GetAsesValue() == "ALL")
                {
                    rptDoc.SetParameterValue("@RepCodeList", "");
                }
                else
                {
                    if (buttonbar1.GetDistributorsValue() == "ALL")
                    {
                        rptDoc.SetParameterValue("@RepCodeList", "ase_id = '" + buttonbar1.GetAsesValue() + "'");
                    }
                    else if (buttonbar1.GetDistributorsValue().Length == 0)
                    {
                        rptDoc.SetParameterValue("@RepCodeList", "ase_id = '" + buttonbar1.GetAsesValue() + "'");
                    }
                    else
                    {
                        if (buttonbar1.GetRepsValue() == "ALL")
                        {
                            rptDoc.SetParameterValue("@RepCodeList", "dis_id = '" + buttonbar1.GetDistributorsValue() + "'");
                        }
                        else if (buttonbar1.GetRepsValue().Length == 0)
                        {
                            rptDoc.SetParameterValue("@RepCodeList", "dis_id = '" + buttonbar1.GetDistributorsValue() + "'");
                        }
                        else
                        {
                            rptDoc.SetParameterValue("@RepCodeList", "rep_code = '" + buttonbar1.GetRepsValue() + "'");
                        }
                    }
                }
            }
        }

        rptDoc.SetParameterValue("rptTitle1", GetReportSubName_Ase());
        rptDoc.SetParameterValue("rptTitle2", GetReportSubName_Dis());
        rptDoc.SetParameterValue("rptTitle5", GetReportSubName_DisCode());
        rptDoc.SetParameterValue("rptTitle3", GetReportSubName_Rep());
        rptDoc.SetParameterValue("rptTitle4", "Period : " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " - " + GetToDate().Value.AddDays(-1).ToString("dd-MMM-yyyy"));

        rptDoc.SetParameterValue("@StartDate", GetFromDate());
        rptDoc.SetParameterValue("@EndDate", GetToDate());
        rptDoc.SetParameterValue("@AdditionalParams", FilterByReport());
        //rptDoc.SetParameterValue("@hvp_value", GetHvpValue());

        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        Tables CrTables;

        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

        CrTables = rptDoc.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        return rptDoc;
    }



    private string GetHvpValue()
    {
        try
        {
            string hvpValue = buttonbar1.GetFilterByHVPValue();
            return hvpValue;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    #endregion

    #region Events

    private ReportDocument repDoc = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        Master.SetBreadCrumb("Return Summary", "#", "");
        this.repDoc = new ReportDocument();
        buttonbar1.onButtonFilter = new usercontrols_buttonbar_reports.ButtonFilter(ButtonFilter_Click);
        buttonbar1.onButtonExport = new usercontrols_buttonbar_reports.ButtonExport(ButtonExport_Click);

        buttonbar1.onDropDownListReps = new usercontrols_buttonbar_reports.DropDownListReps(DropDownListReps_SelectionChanged);
        buttonbar1.onDropDownListDistributors = new usercontrols_buttonbar_reports.DropDownListDistributors(DropDownListDistributors_SelectionChanged);
        buttonbar1.onDropDownListAse = new usercontrols_buttonbar_reports.DropDownListAse(DropDownListAse_SelectionChanged);
        

        if (!IsPostBack)
        {
            buttonbar1.SetVisible(23);
            LoadOptions();
        }
    }

    private void Page_Unload(object sender, EventArgs e)
    {
        if (this.repDoc != null)
        {
            this.repDoc.Close();
            this.repDoc.Dispose();
        }
    }

    protected void ButtonFilter_Click(object sender)
    {
        try
        {
            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument();
            SavePDF(Server.MapPath(GenerateFileName("ReturnSummary", Session.SessionID.ToString())) + ".pdf", GenerateFileName("ReturnSummary", Session.SessionID.ToString()) + ".pdf", repDoc);
        }
        catch (Exception ex)
        {
        }
    }

    protected void DropDownListReps_SelectionChanged(object sender)
    {

    }

    protected void DropDownListDistributors_SelectionChanged(object sender)
    {
        LoadReps();

        buttonbar1.HandleShowReportForDropDownChanged();
        //buttonbar1.EnableRepDropDown(true);

        buttonbar1.HandleDistributorChanged_for_TradeLoadSummary();
        if (UserSession.Instance.OriginatorString == "ASE")
        {
            buttonbar1.HandleASEDropdownEnable(false);
        }
    }

    protected void DropDownListAse_SelectionChanged(object sender)
    {
        LoadDistributors();
        LoadReps();

        buttonbar1.HandleShowReportForDropDownChanged();
    }

    protected void ButtonExport_Click(object sender)
    {
        try
        {
            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            //if(buttonbar1.GetRepsValue)
            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument();
            SaveExcel(Server.MapPath(GenerateFileName("ReturnSummary", Session.SessionID.ToString())) + ".xlsx", GenerateFileName("ReturnSummary", Session.SessionID.ToString()) + ".xlsx", repDoc);
        }
        catch (Exception ex)
        {
        }
    }

    protected void DropDownListShowReportFor_SelectionChanged(object sender)
    {
        //Laod ASE's     
        LoadASEs();
        LoadDistributors();
        LoadReps();

        buttonbar1.HandleShowReportForDropDownChanged();

        if (UserSession.Instance.OriginatorString == "ASE")
        {
            buttonbar1.SetAsesSelect(UserSession.Instance.UserName);
            buttonbar1.HandleASEChanged_for_TradeLoadSummary();
            buttonbar1.HandleASEDropdownEnable(false);
        }
        else if (UserSession.Instance.OriginatorString == "DIST")
        {
            buttonbar1.SetReportForSelect("distributor");
            buttonbar1.HandleSalesReportForDropdownEnable(false);

            buttonbar1.SetAsesSelect("ALL");
            buttonbar1.HandleASEChanged_for_TradeLoadSummary();

            buttonbar1.SetDistributorsSelect(UserSession.Instance.UserName);
            buttonbar1.HandleDistributorChanged_for_TradeLoadSummary();

            buttonbar1.HandleASEDropdownEnable(false);
            buttonbar1.HandleDistributerDropdownEnable(false);

            buttonbar1.HandleASEDropdownVisible(false);
            buttonbar1.HandleDistributerDropdownVisible(false);
            buttonbar1.HandleSalesReportForDropdownVisible(false);
        }
    }
    #endregion
}