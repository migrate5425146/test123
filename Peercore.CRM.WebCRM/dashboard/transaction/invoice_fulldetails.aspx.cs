﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.DataAccess.datasets;
using CRMServiceReference;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Net;
using System.Configuration;

public partial class dashboard_transaction_invoice_fulldetails : System.Web.UI.Page
{
    private ReportDocument repDoc = null;

    #region Load Drop Downs

    private void LoadASEs()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";
        args.AdditionalParams = " dept_string = 'ASE' ";
        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> aseList = new List<OriginatorDTO>();
        aseList = originatorClient.GetOriginatorsByCatergory(args);

        if (buttonbar1.GetShowReprtForValue().Equals("ase"))//Dont add ALL to drop down if ASE User selected
            if (aseList.Count > 1)
                aseList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetAses(aseList, "UserName");
    }

    private void LoadDistributors()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";

        string selectedASE = buttonbar1.GetAsesValue();

        if (selectedASE.Equals("ALL"))
            args.AdditionalParams = " dept_string = 'DIST' ";
        else
            args.AdditionalParams = " dept_string = 'DIST'  AND crm_mast_asm.originator = '" + selectedASE + "' ";

        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> distList = new List<OriginatorDTO>();
        distList = originatorClient.GetDistributersByASMOriginator(args);

        if (distList.Count > 1)
            distList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetDistributors(distList, "UserName");
    }

    private void LoadReps()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";

        string strASM = GetASEString();
        string strDIST = GetDisString();

        if (strDIST.Equals(""))
        {
            if (strASM.Equals(""))
                args.AdditionalParams = " originator.dept_string = 'DR' ";
            else
                args.AdditionalParams = " originator.dept_string = 'DR'  AND crm_mast_asm.originator = '" + strASM + "' ";
        }
        else
        {
            args.AdditionalParams = " originator.dept_string = 'DR'  AND crm_distributor.originator = '" + strDIST + "' ";
        }

        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> repList = new List<OriginatorDTO>();
        repList = originatorClient.GetAllSRByOriginator(args);
        if (repList.Count > 1)
            repList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetReps(repList, "UserName");
    }

    private string GetASEString()
    {
        try
        {
            string reportfor = buttonbar1.GetAsesValue();
            if (reportfor == "ALL")
                reportfor = "";
            return reportfor;
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    private string GetDisString()
    {
        try
        {
            string reportfor = buttonbar1.GetDistributorsValue();
            if (reportfor == "ALL")
                reportfor = "";
            return reportfor;
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    #endregion

    private void LoadOptions()
    {
        LoadASEs();


        if (UserSession.Instance.OriginatorString == "ASE")
        {
            buttonbar1.SetReportForSelect("rep");
            buttonbar1.HandleSalesReportForDropdownEnable(false);

            buttonbar1.SetAsesSelect(UserSession.Instance.UserName);
            buttonbar1.HandleASEChanged_for_TradeLoadSummary();
            buttonbar1.HandleASEDropdownEnable(false);

            buttonbar1.HandleASEDropdownVisible(false);
            buttonbar1.HandleSalesReportForDropdownVisible(false);
        }

        LoadDistributors();

        if (UserSession.Instance.OriginatorString == "DIST")
        {
            buttonbar1.SetReportForSelect("distributor");
            buttonbar1.HandleSalesReportForDropdownEnable(false);

            buttonbar1.SetAsesSelect("ALL");
            buttonbar1.HandleASEChanged_for_TradeLoadSummary();

            buttonbar1.SetDistributorsSelect(UserSession.Instance.UserName);
            buttonbar1.HandleDistributorChanged_for_TradeLoadSummary();

            buttonbar1.SetTradeloadRepTypeSelect("p");

            buttonbar1.HandleASEDropdownEnable(false);
            buttonbar1.HandleDistributerDropdownEnable(false);
            buttonbar1.HandleTradeloadRepTypeDropdownEnable(false);

            buttonbar1.HandleASEDropdownVisible(false);
            buttonbar1.HandleDistributerDropdownVisible(false);
            buttonbar1.HandleTradeloadRepTypeDropdownVisible(false);
            buttonbar1.HandleSalesReportForDropdownVisible(false);
        }

        LoadReps();
    }

    private DateTime? GetFromDate()
    {
        try
        {
            DateTime fromDate = Convert.ToDateTime(buttonbar1.GetFromDateText());
            return fromDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private DateTime? GetToDate()
    {
        try
        {
            DateTime toDate = Convert.ToDateTime(buttonbar1.GetToDateText());
            return toDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GetReportSubName_Ase()
    {
        string reportName = "";
        reportName = "ASM : " + buttonbar1.GetAsesText();

        return reportName;
    }

    private string GetReportSubName_Dis()
    {
        string reportName = "";

        string strDisValue = buttonbar1.GetDistributorsValue();

        if (GetDistributorById(strDisValue) == null)
        {
            reportName = "Distributor : " + buttonbar1.GetDistributorsText();
        }
        else
        {
            reportName = "Distributor : " + buttonbar1.GetDistributorsText();
        }

        return reportName;
    }

    private string GetReportSubName_DisCode()
    {
        string reportName = "";

        string strDisValue = buttonbar1.GetDistributorsValue();

        if (GetDistributorById(strDisValue) == null)
        {
            reportName = "";
        }
        else
        {
            reportName = "DB Code : " + GetDistributorById(strDisValue).OriginatorId;
        }

        return reportName;
    }

    private OriginatorDTO GetDistributorById(string Id)
    {
        OriginatorClient originatorClient = new OriginatorClient();
        OriginatorDTO distList = new OriginatorDTO();
        distList = originatorClient.GetOriginator(Id);

        return distList;
    }

    private string GetReportSubName_Rep()
    {
        string reportName = "";
        reportName = "Rep : " + buttonbar1.GetRepsText();

        return reportName;
    }

    #region report methods

    private string GenerateFileName(string ori_filename, string sessionid)
    {
        string fn = "";
        DateTime d = DateTime.Now;
        fn += "../../docs/" + ori_filename.Trim() + "_" + UserSession.Instance.UserName + "_" + sessionid.Trim() + DateTime.Now.Millisecond.ToString();

        return fn;
    }

    private void SavePDF(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        try
        {
            rptDoc.ExportToDisk(ExportFormatType.PortableDocFormat, downloadAsFilename);
            string FilePath = Server.MapPath(generatefilename);
            ScriptManager.RegisterStartupScript(this, GetType(), "modalscript1", "loadpdf('" + generatefilename + "');", true);
        }
        catch (Exception)
        {
            throw;
        }
    }

    private void SaveExcel(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.ExcelWorkbook, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);
        System.IO.FileInfo file = new System.IO.FileInfo(FilePath);
        if (file.Exists)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.WriteFile(file.FullName);
            HttpContext.Current.Response.End();
        }
    }

    private ReportDocument GetReportDocument()
    {
        ReportDocument rptDoc = new ReportDocument();

        try
        {
            rptDoc.Load(Server.MapPath("../reports/invoice_detail_report3.rpt"));

            if (buttonbar1.GetShowReprtForValue().Equals("ase"))
            {
                rptDoc.SetParameterValue("rptTitle1", GetReportSubName_Ase());
                rptDoc.SetParameterValue("rptTitle5", "");
                rptDoc.SetParameterValue("rptTitle2", "Distributor : ALL");
                rptDoc.SetParameterValue("rptTitle3", "Rep : ALL");
            }
            else if (buttonbar1.GetShowReprtForValue().Equals("distributor"))
            {
                rptDoc.SetParameterValue("rptTitle1", GetReportSubName_Ase());
                rptDoc.SetParameterValue("rptTitle2", GetReportSubName_Dis());
                rptDoc.SetParameterValue("rptTitle5", GetReportSubName_DisCode());
                rptDoc.SetParameterValue("rptTitle3", "Rep : ALL");
            }
            else if (buttonbar1.GetShowReprtForValue().Equals("rep"))
            {
                rptDoc.SetParameterValue("rptTitle1", GetReportSubName_Ase());
                rptDoc.SetParameterValue("rptTitle2", GetReportSubName_Dis());
                rptDoc.SetParameterValue("rptTitle3", GetReportSubName_Rep());
                rptDoc.SetParameterValue("rptTitle5", GetReportSubName_DisCode());
            }

            rptDoc.SetParameterValue("rptTitle4", "Period : " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " - " + GetToDate().Value.ToString("dd-MMM-yyyy"));

            rptDoc.SetParameterValue("@StartDate", GetFromDate());
            rptDoc.SetParameterValue("@EndDate", GetToDate());
            string strQuantityType = buttonbar1.GetQuantityType();
            rptDoc.SetParameterValue("@AdditionalParams", FilterByReport());
            rptDoc.SetParameterValue("@QuantityType", strQuantityType); //Added by Rangan

            if (buttonbar1.GetAsesValue() == "ALL")
            {
                rptDoc.SetParameterValue("@RepCodeList", "");
            }
            else
            {
                if (buttonbar1.GetDistributorsValue() == "ALL")
                {
                    rptDoc.SetParameterValue("@RepCodeList", "ase_id = '" + buttonbar1.GetAsesValue() + "'");
                }
                else if (buttonbar1.GetDistributorsValue().Length == 0)
                {
                    rptDoc.SetParameterValue("@RepCodeList", "ase_id = '" + buttonbar1.GetAsesValue() + "'");
                }
                else
                {
                    if (buttonbar1.GetRepsValue() == "ALL")
                    {
                        rptDoc.SetParameterValue("@RepCodeList", "dis_id = '" + buttonbar1.GetDistributorsValue() + "'");
                    }
                    else if (buttonbar1.GetRepsValue().Length == 0)
                    {
                        rptDoc.SetParameterValue("@RepCodeList", "dis_id = '" + buttonbar1.GetDistributorsValue() + "'");
                    }
                    else
                    {
                        rptDoc.SetParameterValue("@RepCodeList", "rep_code = '" + buttonbar1.GetRepsValue() + "'");
                    }
                }
            }
            

            TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
            TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
            ConnectionInfo crConnectionInfo = new ConnectionInfo();
            Tables CrTables;

            string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

            crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
            crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
            crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
            crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

            //crConnectionInfo.ServerName = @"ACTINIUM\PSLSQLDEV2012";
            //crConnectionInfo.DatabaseName = "perfetti_test";
            //crConnectionInfo.UserID = "sa";
            //crConnectionInfo.Password = "sun$hin3";

            CrTables = rptDoc.Database.Tables;
            foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
            {
                crtableLogoninfo = CrTable.LogOnInfo;
                crtableLogoninfo.ConnectionInfo = crConnectionInfo;
                CrTable.ApplyLogOnInfo(crtableLogoninfo);
            }
            //  CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
            //  CrystalReportViewer1.ReportSource = rptDoc;
            //Session["DailyEffectiveReportSource"] = rptDoc;
            // CrystalReportViewer1.DataBind();
        }
        catch (Exception ex)
        {
            throw;
        }
        return rptDoc;
    }

    private string FilterByReport()
    {
        string reportFilter = "";

        if (buttonbar1.GetShowReprtForValue().Equals("ase"))
        {
            if (buttonbar1.GetAsesValue().Equals("ALL"))
            {
                reportFilter = "";
            }
            else
            {
                reportFilter = "ase_id = '" + buttonbar1.GetAsesValue() + "'";
            }
        }

        if (buttonbar1.GetShowReprtForValue().Equals("distributor"))
        {
            if (buttonbar1.GetDistributorsValue().Equals("ALL") || buttonbar1.GetDistributorsValue().Equals(""))
            {
                reportFilter = "ase_id = '" + buttonbar1.GetAsesValue() + "'";
            }
            else
            {
                if (buttonbar1.GetRepsValue().Equals("ALL") || buttonbar1.GetRepsValue().Equals(""))
                {
                    if (buttonbar1.GetDistributorsValue().Equals("ALL") || buttonbar1.GetDistributorsValue().Equals(""))
                    {
                        reportFilter = "ase_id = '" + buttonbar1.GetAsesValue() + "'";
                    }
                    else
                    {
                        reportFilter = "dis_id = '" + buttonbar1.GetDistributorsValue() + "'";
                    }
                }
                else
                {
                    reportFilter = "rep_id = '" + buttonbar1.GetRepsValue() + "'";
                }
            }
        }

        if (buttonbar1.GetShowReprtForValue().Equals("rep"))
        {
            if (buttonbar1.GetRepsValue().Equals("ALL") || buttonbar1.GetRepsValue().Equals(""))
            {
                if (buttonbar1.GetDistributorsValue().Equals("ALL") || buttonbar1.GetDistributorsValue().Equals(""))
                {
                    reportFilter = "ase_id = '" + buttonbar1.GetAsesValue() + "'";
                }
                else
                {
                    reportFilter = "dis_id = '" + buttonbar1.GetDistributorsValue() + "'";
                }
            }
            else
            {
                reportFilter = "rep_id = '" + buttonbar1.GetRepsValue() + "'";
            }
        }

        return reportFilter;
    }

    //private ReportDocument GetReportDocument_Excel()
    //{

    //    ReportDocument rptDoc = new ReportDocument();
    //    try
    //    {
    //        rptDoc.Load(Server.MapPath("../reports/invoice_detail_report3_excel.rpt"));
    //        //rptDoc.SetParameterValue("rptTitle1", GetReportSubName());

    //        if (buttonbar1.GetShowReprtForValue().Equals("ase"))
    //        {
    //            rptDoc.SetParameterValue("rptTitle1", GetReportSubName_Ase());
    //            rptDoc.SetParameterValue("rptTitle5", "");
    //            rptDoc.SetParameterValue("rptTitle2", "Distributor : ALL");
    //            rptDoc.SetParameterValue("rptTitle3", "Rep : ALL");
    //        }
    //        else if (buttonbar1.GetShowReprtForValue().Equals("distributor"))
    //        {
    //            rptDoc.SetParameterValue("rptTitle1", GetReportSubName_Ase());
    //            rptDoc.SetParameterValue("rptTitle2", GetReportSubName_Dis());
    //            rptDoc.SetParameterValue("rptTitle5", GetReportSubName_DisCode());
    //            rptDoc.SetParameterValue("rptTitle3", "Rep : ALL");
    //        }
    //        else if (buttonbar1.GetShowReprtForValue().Equals("rep"))
    //        {
    //            rptDoc.SetParameterValue("rptTitle1", GetReportSubName_Ase());
    //            rptDoc.SetParameterValue("rptTitle2", GetReportSubName_Dis());
    //            rptDoc.SetParameterValue("rptTitle3", GetReportSubName_Rep());
    //            rptDoc.SetParameterValue("rptTitle5", GetReportSubName_DisCode());
    //        }

    //        rptDoc.SetParameterValue("rptTitle4", "Period : " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " - " + GetToDate().Value.ToString("dd-MMM-yyyy"));

    //        rptDoc.SetParameterValue("@StartDate", GetFromDate());
    //        rptDoc.SetParameterValue("@EndDate", GetToDate());
    //        if (buttonbar1.GetShowReprtForValue().Equals("rep"))
    //            rptDoc.SetParameterValue("@RepCodeList", "crm_invoice_details_with_discounts.rep_code = '" + buttonbar1.GetRepsValue() + "' ");
    //        //else
    //        //    rptDoc.SetParameterValue("@RepCodeList", GetRepsString().Replace("originator", "crm_invoice_details_with_discounts.rep_code"));
    //        rptDoc.SetParameterValue("@AdditionalParams", "");


    //        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
    //        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
    //        ConnectionInfo crConnectionInfo = new ConnectionInfo();
    //        Tables CrTables;

    //        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

    //        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
    //        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
    //        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
    //        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

    //        //crConnectionInfo.ServerName = @"ACTINIUM\PSLSQLDEV2012";
    //        //crConnectionInfo.DatabaseName = "perfetti_test";
    //        //crConnectionInfo.UserID = "sa";
    //        //crConnectionInfo.Password = "sun$hin3";

    //        CrTables = rptDoc.Database.Tables;
    //        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
    //        {
    //            crtableLogoninfo = CrTable.LogOnInfo;
    //            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
    //            CrTable.ApplyLogOnInfo(crtableLogoninfo);
    //        }
    //        //  CrystalReportViewer1.ToolPanelView = CrystalDecisions.Web.ToolPanelViewType.None;
    //        //  CrystalReportViewer1.ReportSource = rptDoc;
    //        //Session["DailyEffectiveReportSource"] = rptDoc;
    //        // CrystalReportViewer1.DataBind();
    //    }
    //    catch (Exception ex)
    //    {
    //        throw;
    //    }
    //    return rptDoc;
    //}

    #endregion

    #region Events

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        Master.SetBreadCrumb("Invoice Details", "#", "");
        this.repDoc = new ReportDocument();
        buttonbar1.onButtonFilter = new usercontrols_buttonbar_reports.ButtonFilter(ButtonFilter_Click);
        buttonbar1.onButtonExport = new usercontrols_buttonbar_reports.ButtonExport(ButtonExport_Click);

        buttonbar1.onDropDownListReps = new usercontrols_buttonbar_reports.DropDownListReps(DropDownListReps_SelectionChanged);
        buttonbar1.onDropDownListDistributors = new usercontrols_buttonbar_reports.DropDownListDistributors(DropDownListDistributors_SelectionChanged);
        buttonbar1.onDropDownListAse = new usercontrols_buttonbar_reports.DropDownListAse(DropDownListAse_SelectionChanged);
        buttonbar1.onDropDownListShowReportFor = new usercontrols_buttonbar_reports.DropDownListShowReportFor(DropDownListShowReportFor_SelectionChanged);
        buttonbar1.onDropDownListQuantityType = new usercontrols_buttonbar_reports.DropDownListQuantityType(DropDownListQuantityType_SelectionChanged);

        if (!IsPostBack)
        {
            buttonbar1.SetVisible(3);
            LoadOptions();
        }
        else
        {
            string control_id = getPostBackControlName();
        }
    }

    private void Page_Unload(object sender, EventArgs e)
    {
        if (this.repDoc != null)
        {
            this.repDoc.Close();
            this.repDoc.Dispose();
        }
    }

    protected void ButtonFilter_Click(object sender)
    {
        try
        {
            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            //if(buttonbar1.GetRepsValue)
            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument();
            SavePDF(Server.MapPath(GenerateFileName("InvoiceDetails", Session.SessionID.ToString())) + ".pdf", GenerateFileName("InvoiceDetails", Session.SessionID.ToString()) + ".pdf", repDoc);
        }
        catch (Exception ex)
        {
        }
    }

    protected void DropDownListReps_SelectionChanged(object sender)
    {

    }

    protected void DropDownListDistributors_SelectionChanged(object sender)
    {
        LoadReps();

        buttonbar1.HandleDistributorChanged_for_TradeLoadSummary();

        if (UserSession.Instance.OriginatorString == "ASE")
        {
            buttonbar1.HandleASEDropdownEnable(false);
        }
    }

    protected void DropDownListAse_SelectionChanged(object sender)
    {
        LoadDistributors();
        LoadReps();

        buttonbar1.HandleShowReportForDropDownChanged();
        //buttonbar1.EnableDistributorDropDown(true);
    }

    protected void ButtonExport_Click(object sender)
    {
        try
        {
            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            //if(buttonbar1.GetRepsValue)
            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument();
            SaveExcel(Server.MapPath(GenerateFileName("InvoiceDetails", Session.SessionID.ToString())) + ".xlsx", GenerateFileName("InvoiceDetails", Session.SessionID.ToString()) + ".xlsx", repDoc);
        }
        catch (Exception ex)
        {
        }
    }

    protected void DropDownListShowReportFor_SelectionChanged(object sender)
    {
        //Laod ASE's     
        LoadASEs();
        if (UserSession.Instance.OriginatorString == "ASE")
        {
            buttonbar1.SetAsesSelect(UserSession.Instance.UserName);
            buttonbar1.HandleASEChanged_for_TradeLoadSummary();
            buttonbar1.HandleASEDropdownEnable(false);
        }

        LoadDistributors();
        LoadReps();

        buttonbar1.HandleShowReportForDropDownChanged();

        if (UserSession.Instance.OriginatorString == "ASE")
        {
            buttonbar1.HandleASEDropdownEnable(false);
        }
    }
    private string GetQuantitytype() //Added by Rangan
    {
        string quantitytype = "";



        return quantitytype;
    }
    private string getPostBackControlName()
    {
        Control control = null;
        //first we will check the "__EVENTTARGET" because if post back made by       the controls
        //which used "_doPostBack" function also available in Request.Form collection.
        string ctrlname = Page.Request.Params["__EVENTTARGET"];
        if (ctrlname != null && ctrlname != String.Empty)
        {
            control = Page.FindControl(ctrlname);
        }
        // if __EVENTTARGET is null, the control is a button type and we need to
        // iterate over the form collection to find it
        else
        {
            string ctrlStr = String.Empty;
            Control c = null;
            foreach (string ctl in Page.Request.Form)
            {
                //handle ImageButton they having an additional "quasi-property" in their Id which identifies
                //mouse x and y coordinates
                if (ctl.EndsWith(".x") || ctl.EndsWith(".y"))
                {
                    ctrlStr = ctl.Substring(0, ctl.Length - 2);
                    c = Page.FindControl(ctrlStr);
                }
                else
                {
                    c = Page.FindControl(ctl);
                }
                if (c is System.Web.UI.WebControls.Button ||
                         c is System.Web.UI.WebControls.ImageButton)
                {
                    control = c;
                    break;
                }
            }
        }
        return control.ID;

    }

    protected void DropDownListQuantityType_SelectionChanged(object sender)
    {

    }

    #endregion
}