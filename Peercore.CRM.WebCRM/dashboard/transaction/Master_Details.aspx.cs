﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.DataAccess.datasets;
using CRMServiceReference;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Net;
using System.Configuration;

public partial class dashboard_transaction_Master_Details : System.Web.UI.Page
{
    private void LoadOptions()
    {
        //Laod ASE's     
        LoadASEs();

        if (UserSession.Instance.OriginatorString == "ASE")
        {
            buttonbar1.SetAsesSelect(UserSession.Instance.UserName);
            buttonbar1.HandleASEChanged_for_TradeLoadSummary();
            buttonbar1.HandleASEDropdownEnable(false);
        }

        LoadDistributors();
        //LoadReps();

        LoadFilterByBrands();
        LoadFilterByItemGroup();
        LoadFilterByPackingMethod();
    }

    private void LoadASEs()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";
        args.AdditionalParams = " dept_string = 'ASE' ";
        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> aseList = new List<OriginatorDTO>();
        aseList = originatorClient.GetOriginatorsByCatergory(args);
        if (aseList.Count > 0)
            aseList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetAses(aseList, "UserName");
    }

    private void LoadDistributors()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";

        string selectedASE = buttonbar1.GetAsesValue();

        if (selectedASE.Equals("ALL"))
            args.AdditionalParams = " dept_string = 'DIST' ";
        else
            args.AdditionalParams = " dept_string = 'DIST'  AND crm_mast_asm.originator = '" + selectedASE + "' ";

        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> distList = new List<OriginatorDTO>();
        distList = originatorClient.GetDistributersByASMOriginator(args);
        if (distList.Count > 0)
            distList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetDistributors(distList, "UserName");
    }

    private void LoadReps()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";

        string strASM = GetASEString();
        string strDIST = GetDisString();

        if (strDIST.Equals(""))
        {
            if (strASM.Equals(""))
                args.AdditionalParams = " originator.dept_string = 'DR' ";
            else
                args.AdditionalParams = " originator.dept_string = 'DR'  AND crm_mast_asm.originator = '" + strASM + "' ";
        }
        else
        {
            args.AdditionalParams = " originator.dept_string = 'DR'  AND crm_distributor.originator = '" + strDIST + "' ";
        }

        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> repList = new List<OriginatorDTO>();
        repList = originatorClient.GetAllSRByOriginator(args);
        if (repList.Count > 0)
            repList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        buttonbar1.SetReps(repList, "UserName");
    }

    private string GetDistributorsString()
    {
        ListItemCollection distList = buttonbar1.GetAllDistributorsInDropDown();
        string distString = "";

        if (distList != null)
        {
            if (distList.Count > 0)
            {
                distString = " originator IN (";
                foreach (ListItem item in distList)
                {
                    distString = distString + "'" + item.Value + "',";
                }
                distString = distString.Replace("'ALL',", "");
                distString = distString.Remove(distString.Length - 1, 1);//Removing Last Comma
                distString = distString + ") ";
            }
        }

        return distString;
    }

    private string GetRepsString()
    {
        ListItemCollection repsList = buttonbar1.GetAllRepsInDropDown();
        string repString = "";

        if (repsList != null)
        {
            if (repsList.Count > 0)
            {
                repString = " rep_code IN (";
                foreach (ListItem item in repsList)
                {
                    repString = repString + "'" + item.Value + "',";
                }
                repString = repString.Replace("'ALL',", "");
                repString = repString.Remove(repString.Length - 1, 1);//Removing Last Comma
                repString = repString + ") ";
            }
        }

        return repString;
    }

    private string GenerateFileName(string ori_filename, string sessionid)
    {
        /// TODO:
        /// Generate file name according to system path.
        string fn = "";
        DateTime d = DateTime.Now;
        fn += ori_filename.Trim() + "_" + UserSession.Instance.UserName + "_" + DateTime.Now.Millisecond.ToString();

        return fn;
    }

    private void SavePDF(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.PortableDocFormat, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);
        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript1", "loadpdf('" + generatefilename + "');", true);
    }

    private void SaveExcel(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.ExcelWorkbook, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);

        System.IO.FileInfo file = new System.IO.FileInfo(FilePath);
        if (file.Exists)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.WriteFile(file.FullName);
            HttpContext.Current.Response.End();
        }
    }

    private DateTime? GetMonth()
    {
        try
        {
            DateTime fromDate = Convert.ToDateTime(buttonbar1.GetMonth());
            return fromDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GetReportFor()
    {
        try
        {
            string reportfor = buttonbar1.GetShowReprtForValue();
            return reportfor;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GetASEString()
    {
        try
        {
            string reportfor = buttonbar1.GetAsesValue();
            if (reportfor == "ALL")
                reportfor = "";
            return reportfor;
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    private string GetDisString()
    {
        try
        {
            string reportfor = buttonbar1.GetDistributorsValue();
            if (reportfor == "ALL")
                reportfor = "";
            return reportfor;
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    private string GetActiveInactive()
    {
        try
        {
            string activeInactive = buttonbar1.GetFilterByActiveInactiveValue();
            return activeInactive;
        }
        catch (Exception ex)
        {
            return "";
        }
    }

    private DateTime? GetFromDate()
    {
        try
        {
            DateTime fromDate = Convert.ToDateTime(buttonbar1.GetFromDateText());
            return fromDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private DateTime? GetToDate()
    {
        try
        {
            DateTime toDate = Convert.ToDateTime(buttonbar1.GetToDateText());
            return toDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private void LoadFilterByBrands()
    {
        ArgsDTO args = new ArgsDTO();
        args.StartIndex = 1;
        args.RowCount = 1000;
        args.ROriginator = UserSession.Instance.OriginatorString;
        args.Originator = UserSession.Instance.OriginalUserName;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.OrderBy = " name asc";
        CommonClient commonClient = new CommonClient();
        List<BrandDTO> brandList = new List<BrandDTO>();
        brandList = commonClient.GetAllBrandsDataAndCount(args);
        if (brandList.Count > 0)
            brandList.Insert(0, new BrandDTO() { BrandId = 0, BrandName = "ALL" });
        buttonbar1.SetBrands(brandList, "BrandId");
    }

    private void LoadFilterByPackingMethod()
    {
        ArgsDTO args = new ArgsDTO();
        args.StartIndex = 1;
        args.RowCount = 1000;
        args.ROriginator = UserSession.Instance.OriginatorString;
        args.Originator = UserSession.Instance.OriginalUserName;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.OrderBy = " packing_name asc";

        CommonClient commonClient = new CommonClient();
        List<ProductPackingDTO> packingsList = new List<ProductPackingDTO>();
        packingsList = commonClient.GetAllProductPackings(args);
        if (packingsList.Count > 0)
            packingsList.Insert(0, new ProductPackingDTO() { PackingId = 0, PackingName = "ALL" });
        buttonbar1.SetPackingMethod(packingsList, "PackingId");
    }

    private void LoadFilterByItemGroup()
    {
        ArgsDTO args = new ArgsDTO();
        args.StartIndex = 1;
        args.RowCount = 1000;
        args.ROriginator = UserSession.Instance.OriginatorString;
        args.Originator = UserSession.Instance.OriginalUserName;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.OrderBy = " id ";

        CommonClient commonClient = new CommonClient();
        List<ProductCategoryDTO> categoryList = new List<ProductCategoryDTO>();
        categoryList = commonClient.GetAllProductCategories(args);
        if (categoryList.Count > 0)
            categoryList.Insert(0, new ProductCategoryDTO() { Id = 0, Description = "ALL" });
        buttonbar1.SetItemGroup(categoryList, "Id");
    }

    //private void LoadFilterByFlavor()
    //{
    //    ArgsDTO args = new ArgsDTO();
    //    args.StartIndex = 1;
    //    args.RowCount = 1000;
    //    args.ROriginator = UserSession.Instance.OriginatorString;
    //    args.Originator = UserSession.Instance.OriginalUserName;
    //    args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
    //    args.OrderBy = " flavor_name asc";

    //    CommonClient commonClient = new CommonClient();
    //    List<FlavorDTO> flavorsList = new List<FlavorDTO>();
    //    flavorsList = commonClient.GetAllFlavor();
    //    if (flavorsList.Count > 0)
    //        flavorsList.Insert(0, new FlavorDTO() { FlavorId = 0, FlavorName = "ALL" });
    //    buttonbar1.SetFlavor(flavorsList, "FlavorId");
    //}

    private string GetReportSubName()
    {
        string selectedASE = buttonbar1.GetAsesValue();
        string selectedDIST = buttonbar1.GetDistributorsValue();

        //string reportName = "ALL ASE";

        //if (!selectedASE.Equals("ALL") && !selectedASE.Equals(""))
        //{
        //    reportName = "For ASE - " + buttonbar1.GetAsesText();

        //    if (!selectedDIST.Equals("ALL") && !selectedDIST.Equals(""))
        //    {
        //        reportName = "For Distributor - " + buttonbar1.GetDistributorsText();

        //        if (!selectedREP.Equals("ALL") && !selectedREP.Equals(""))
        //        {
        //            reportName = "For Rep - " + buttonbar1.GetRepsText();
        //        }
        //    }
        //}

        //Edit By Irosh Fernando=========================
        string reportName_Ase = "ALL ASM, ";
        string reportName_Dis = "ALL Distributor, ";

        if (!selectedASE.Equals("ALL") && !selectedASE.Equals(""))
        {
            reportName_Ase = "ASM - " + buttonbar1.GetAsesText() + ", ";
        }

        if (!selectedDIST.Equals("ALL") && !selectedDIST.Equals(""))
        {
            reportName_Dis = "Distributor - " + buttonbar1.GetDistributorsText();
        }

        //===============================================

        return "For " + reportName_Ase + reportName_Dis;
    }

    private ReportDocument GetReportDocument()
    {
        ReportDocument rptDoc = new ReportDocument();

        if (GetReportFor() == "outlet")
        {
            rptDoc.Load(Server.MapPath("../reports/master_outlets.rpt"));
            rptDoc.SetParameterValue("@AseVal", GetASEString());
            rptDoc.SetParameterValue("@DisVal", GetDisString());
        }
        else if (GetReportFor() == "products")
        {
            rptDoc.Load(Server.MapPath("../reports/master_products.rpt"));
            rptDoc.SetParameterValue("@BrandId", Int32.Parse(buttonbar1.GetFilterByBrandValue()));
            rptDoc.SetParameterValue("@PackingId", Int32.Parse(buttonbar1.GetFilterByPackingMethodValue()));
            rptDoc.SetParameterValue("@Hvp", buttonbar1.GetFilterByHVPValue());
            rptDoc.SetParameterValue("@CategoryId", Int32.Parse(buttonbar1.GetFilterByItemGroupValue()));
            rptDoc.SetParameterValue("@ActiveInactive", GetActiveInactive());
        }
        else if (GetReportFor() == "ase")
        {
            rptDoc.Load(Server.MapPath("../reports/master_ase.rpt"));
        }
        else if (GetReportFor() == "distributor")
        {
            rptDoc.Load(Server.MapPath("../reports/master_distributors.rpt"));
            rptDoc.SetParameterValue("@AseVal", GetASEString());
            rptDoc.SetParameterValue("@ActiveInactive", GetActiveInactive());
        }
        else if (GetReportFor() == "reps")
        {
            rptDoc.Load(Server.MapPath("../reports/master_reps.rpt"));
            rptDoc.SetParameterValue("@AseVal", GetASEString());
            rptDoc.SetParameterValue("@DisVal", GetDisString());
            rptDoc.SetParameterValue("@ActiveInactive", GetActiveInactive());
        }
        else if (GetReportFor() == "routes")
        {
            rptDoc.Load(Server.MapPath("../reports/master_route_list.rpt"));
            rptDoc.SetParameterValue("@AseVal", GetASEString());
            rptDoc.SetParameterValue("@ActiveInactive", GetActiveInactive());
        }
        else if (GetReportFor() == "territory")
        {
            rptDoc.Load(Server.MapPath("../reports/master_territory_list.rpt"));
            rptDoc.SetParameterValue("@AseVal", GetASEString());
            rptDoc.SetParameterValue("@ActiveInactive", GetActiveInactive());
        }

        rptDoc.SetParameterValue("rptTitle1", GetReportSubName());

        //rptDoc.SetParameterValue("rptTitle1", GetReportSubName());
        //rptDoc.SetParameterValue("rptTitle2", "Month " + GetMonth().Value.ToString("MMM-yyyy"));
        //rptDoc.SetParameterValue("rptMonth", GetMonth().Value.ToString("MM"));
        //rptDoc.SetParameterValue("@Month", GetMonth());
        //rptDoc.SetParameterValue("@EndDate", GetToDate());
        //rptDoc.SetParameterValue("@BrandId", Int32.Parse(buttonbar1.GetFilterByBrandValue()));
        //rptDoc.SetParameterValue("@PackingId", Int32.Parse(buttonbar1.GetFilterByPackingMethodValue()));
        //rptDoc.SetParameterValue("@FlavorId", Int32.Parse(buttonbar1.GetFilterByFlavorValue()));
        //rptDoc.SetParameterValue("@CategoryId", Int32.Parse(buttonbar1.GetFilterByItemGroupValue()));
        //rptDoc.SetParameterValue("@ReportFor", GetReportFor());
        //rptDoc.SetParameterValue("@AseVal", "");
        //rptDoc.SetParameterValue("@DistributorVal", "");

        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        Tables CrTables;

        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

        CrTables = rptDoc.Database.Tables;

        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        return rptDoc;
    }

    private ReportDocument GetReportDocument_Excel()
    {
        ReportDocument rptDoc = new ReportDocument();

        if (GetReportFor() == "outlet")
        {
            rptDoc.Load(Server.MapPath("../reports/master_outlets.rpt"));
            rptDoc.SetParameterValue("@AseVal", GetASEString());
            rptDoc.SetParameterValue("@DisVal", GetDisString());
        }
        else if (GetReportFor() == "products")
        {
            rptDoc.Load(Server.MapPath("../reports/master_products.rpt"));
            rptDoc.SetParameterValue("@BrandId", Int32.Parse(buttonbar1.GetFilterByBrandValue()));
            rptDoc.SetParameterValue("@PackingId", Int32.Parse(buttonbar1.GetFilterByPackingMethodValue()));
            rptDoc.SetParameterValue("@Hvp", buttonbar1.GetFilterByHVPValue());
            rptDoc.SetParameterValue("@CategoryId", Int32.Parse(buttonbar1.GetFilterByItemGroupValue()));
        }
        else if (GetReportFor() == "ase")
        {
            rptDoc.Load(Server.MapPath("../reports/master_ase.rpt"));
        }
        else if (GetReportFor() == "distributor")
        {
            rptDoc.Load(Server.MapPath("../reports/master_distributors.rpt"));
            rptDoc.SetParameterValue("@AseVal", GetASEString());
        }
        else if (GetReportFor() == "reps")
        {
            rptDoc.Load(Server.MapPath("../reports/master_reps.rpt"));
            rptDoc.SetParameterValue("@AseVal", GetASEString());
            rptDoc.SetParameterValue("@DisVal", GetDisString());
        }
        else if (GetReportFor() == "routes")
        {
            rptDoc.Load(Server.MapPath("../reports/master_route_list.rpt"));
            rptDoc.SetParameterValue("@AseVal", GetASEString());
        }
        else if (GetReportFor() == "territory")
        {
            rptDoc.Load(Server.MapPath("../reports/master_territory_list.rpt"));
            rptDoc.SetParameterValue("@AseVal", GetASEString());
        }

        rptDoc.SetParameterValue("rptTitle1", GetReportSubName());

        //rptDoc.SetParameterValue("rptTitle1", GetReportSubName());
        //rptDoc.SetParameterValue("rptTitle2", "Month " + GetMonth().Value.ToString("MMM-yyyy"));
        //rptDoc.SetParameterValue("rptMonth", GetMonth().Value.ToString("MM"));
        //rptDoc.SetParameterValue("@Month", GetMonth());
        //rptDoc.SetParameterValue("@EndDate", GetToDate());
        //rptDoc.SetParameterValue("@BrandId", Int32.Parse(buttonbar1.GetFilterByBrandValue()));
        //rptDoc.SetParameterValue("@PackingId", Int32.Parse(buttonbar1.GetFilterByPackingMethodValue()));
        //rptDoc.SetParameterValue("@FlavorId", Int32.Parse(buttonbar1.GetFilterByFlavorValue()));
        //rptDoc.SetParameterValue("@CategoryId", Int32.Parse(buttonbar1.GetFilterByItemGroupValue()));
        //rptDoc.SetParameterValue("@ReportFor", GetReportFor());
        //rptDoc.SetParameterValue("@AseVal", "");
        //rptDoc.SetParameterValue("@DistributorVal", "");

        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        Tables CrTables;

        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

        CrTables = rptDoc.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }
        return rptDoc;
    }

    #region Events

    private ReportDocument repDoc = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        Master.SetBreadCrumb("Master", "#", "");
        this.repDoc = new ReportDocument();
        buttonbar1.onButtonFilter = new usercontrols_buttonbar_reports.ButtonFilter(ButtonFilter_Click);
        buttonbar1.onButtonExport = new usercontrols_buttonbar_reports.ButtonExport(ButtonExport_Click);

        buttonbar1.onDropDownListShowReportFor = new usercontrols_buttonbar_reports.DropDownListShowReportFor(DropDownListReportFor_SelectionChanged);
        buttonbar1.onDropDownListReps = new usercontrols_buttonbar_reports.DropDownListReps(DropDownListReps_SelectionChanged);
        buttonbar1.onDropDownListAse = new usercontrols_buttonbar_reports.DropDownListAse(DropDownListAse_SelectionChanged);
        buttonbar1.onDropDownListDistributors = new usercontrols_buttonbar_reports.DropDownListDistributors(DropDownListDistributer_SelectionChanged);

        if (!IsPostBack)
        {
            buttonbar1.SetVisible(5);
            LoadOptions();
        }
    }

    private void Page_Unload(object sender, EventArgs e)
    {
        if (this.repDoc != null)
        {
            this.repDoc.Close();
            this.repDoc.Dispose();
        }
    }

    protected void ButtonFilter_Click(object sender)
    {
        try
        {
            string FileName = GenerateFileName("MasterFiles_" + GetReportFor(), Session.SessionID.ToString());
            ReportDocument rptDoc = GetReportDocument();
            SavePDF(Server.MapPath("../../docs/" + FileName) + ".pdf", "../../docs/" + FileName + ".pdf", rptDoc);
        }
        catch (Exception ex)
        {
        }
    }

    protected void DropDownListReportFor_SelectionChanged(object sender)
    {
        LoadOptions();
        buttonbar1.HandleShowReportForDropDownChangedMasterDetails();
    }

    protected void DropDownListAse_SelectionChanged(object sender)
    {
        LoadDistributors();
        //LoadReps();
    }
    
    protected void DropDownListDistributer_SelectionChanged(object sender)
    {
        //LoadReps();
    }

    protected void DropDownListReps_SelectionChanged(object sender)
    {
    }

    protected void ButtonExport_Click(object sender)
    {
        try
        {
            ReportDocument rptDoc = GetReportDocument();
            SaveExcel(Server.MapPath(GenerateFileName("MasterFiles_" + GetReportFor(), Session.SessionID.ToString())) + ".xlsx", GenerateFileName("MasterFiles_" + GetReportFor(), Session.SessionID.ToString()) + ".xlsx", rptDoc);
        }
        catch (Exception ex)
        {
        }
    }

    #endregion
}