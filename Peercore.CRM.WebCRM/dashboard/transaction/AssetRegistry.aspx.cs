﻿using CRMServiceReference;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


public partial class dashboard_transaction_AssetRegistry : System.Web.UI.Page
{
    private ReportDocument repDoc = null;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Master.SetBreadCrumb("Asset Registry", "#", "");
            this.repDoc = new ReportDocument();
            buttonbar1.onButtonFilter = new usercontrols_buttonbar_reports.ButtonFilter(ButtonFilter_Click);
            buttonbar1.onButtonExport = new usercontrols_buttonbar_reports.ButtonExport(ButtonExport_Click);

            buttonbar1.onDropDownListAssetReports = new usercontrols_buttonbar_reports.DropDownListAssetReports(DropDownListShowReportFor_SelectionChanged);


            if (!IsPostBack)
            {
                //Session["report_name"] = null;
                buttonbar1.SetVisible(19);
                LoadAssetTypes();
            }
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    protected void DropDownListShowReportFor_SelectionChanged(object sender)
    {
        LoadAssetTypes();
        buttonbar1.HandleFilterVisibilityForAssetRegistry();

    }


    private ReportDocument GetReportDocument()
    {
        ReportDocument rptDoc = new ReportDocument();

        if (buttonbar1.GetAssetsReportName() == "asset_detail")
        {
            rptDoc.Load(Server.MapPath("../reports/asset_detail.rpt"));

            rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);
            rptDoc.SetParameterValue("@assetCode", GetAssetCode());
            rptDoc.SetParameterValue("@assetSerial", GetAssetSerial());
            rptDoc.SetParameterValue("@assetStatus", GetAssetStatus());
            rptDoc.SetParameterValue("@assetType", GetAssetType());

        }
        else if (buttonbar1.GetAssetsReportName() == "asset_registry")
        {
            rptDoc.Load(Server.MapPath("../reports/asset_registry.rpt"));

            rptDoc.SetParameterValue("rptTitleDatePeriod", "Period : " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " - " + GetToDate().Value.ToString("dd-MMM-yyyy"));
            rptDoc.SetParameterValue("@StartDate", GetFromDate());
            rptDoc.SetParameterValue("@EndDate", GetToDate());
            rptDoc.SetParameterValue("@assetCode", GetAssetCode());
            rptDoc.SetParameterValue("@assetSerial", GetAssetSerial());
            rptDoc.SetParameterValue("@assetStatus", GetAssetStatus());
            rptDoc.SetParameterValue("@assetType", GetAssetType());
            rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);
        }
        else if (buttonbar1.GetAssetsReportName() == "monthly_payments")
        {
            rptDoc.Load(Server.MapPath("../reports/asset_bill_payment.rpt"));

            rptDoc.SetParameterValue("rptTitleDatePeriod", "Period : " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " - " + GetToDate().Value.ToString("dd-MMM-yyyy"));
            rptDoc.SetParameterValue("@StartDate", GetFromDate());
            rptDoc.SetParameterValue("@EndDate", GetToDate());
            rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);
        }
        else if (buttonbar1.GetAssetsReportName() == "monthly_deductions")
        {
            rptDoc.Load(Server.MapPath("../reports/asset_payment_deduction.rpt"));

            rptDoc.SetParameterValue("rptTitleDatePeriod", "Period : " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " - " + GetToDate().Value.ToString("dd-MMM-yyyy"));
            rptDoc.SetParameterValue("@StartDate", GetFromDate());
            rptDoc.SetParameterValue("@EndDate", GetToDate());
            rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);
        }


        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        Tables CrTables;

        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

        CrTables = rptDoc.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        return rptDoc;
    }

    private ReportDocument GetReportDocument_Excel()
    {
        ReportDocument rptDoc = new ReportDocument();

        if (buttonbar1.GetAssetsReportName() == "asset_detail")
        {
            rptDoc.Load(Server.MapPath("../reports/asset_detail.rpt"));

            rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);
        }
        else if (buttonbar1.GetAssetsReportName() == "asset_registry")
        {
            rptDoc.Load(Server.MapPath("../reports/asset_registry.rpt"));

            rptDoc.SetParameterValue("rptTitleDatePeriod", "Period : " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " - " + GetToDate().Value.ToString("dd-MMM-yyyy"));
            rptDoc.SetParameterValue("@StartDate", GetFromDate());
            rptDoc.SetParameterValue("@EndDate", GetToDate());
            rptDoc.SetParameterValue("@assetCode", GetAssetCode());
            rptDoc.SetParameterValue("@assetSerial", GetAssetSerial());
            rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);
        }
        else if (buttonbar1.GetAssetsReportName() == "monthly_payments")
        {
            rptDoc.Load(Server.MapPath("../reports/asset_bill_payment.rpt"));

            rptDoc.SetParameterValue("rptTitleDatePeriod", "Period : " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " - " + GetToDate().Value.ToString("dd-MMM-yyyy"));
            rptDoc.SetParameterValue("@StartDate", GetFromDate());
            rptDoc.SetParameterValue("@EndDate", GetToDate());
            rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);
        }
        else if (buttonbar1.GetAssetsReportName() == "monthly_deductions")
        {
            rptDoc.Load(Server.MapPath("../reports/asset_payment_deduction.rpt"));

            rptDoc.SetParameterValue("rptTitleDatePeriod", "Period : " + GetFromDate().Value.ToString("dd-MMM-yyyy") + " - " + GetToDate().Value.ToString("dd-MMM-yyyy"));
            rptDoc.SetParameterValue("@StartDate", GetFromDate());
            rptDoc.SetParameterValue("@EndDate", GetToDate());
            rptDoc.SetParameterValue("@originator", UserSession.Instance.UserName);
        }


        TableLogOnInfos crtableLogoninfos = new TableLogOnInfos();
        TableLogOnInfo crtableLogoninfo = new TableLogOnInfo();
        ConnectionInfo crConnectionInfo = new ConnectionInfo();
        Tables CrTables;

        string[] strConnection = ConfigurationManager.ConnectionStrings[("PeercoreCRM")].ConnectionString.Split(new char[] { ';' });

        crConnectionInfo.ServerName = strConnection[0].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.DatabaseName = strConnection[1].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.UserID = strConnection[2].Split(new char[] { '=' }).GetValue(1).ToString();
        crConnectionInfo.Password = StringCipher.Decrypt(strConnection[3].Split(new char[] { '=' }).GetValue(1).ToString());

        CrTables = rptDoc.Database.Tables;
        foreach (CrystalDecisions.CrystalReports.Engine.Table CrTable in CrTables)
        {
            crtableLogoninfo = CrTable.LogOnInfo;
            crtableLogoninfo.ConnectionInfo = crConnectionInfo;
            CrTable.ApplyLogOnInfo(crtableLogoninfo);
        }

        return rptDoc;
    }


    private string GenerateFileName(string ori_filename, string sessionid)
    {
        /// Generate file name according to system path.
        string fn = "";
        DateTime d = DateTime.Now;
        fn += "../../docs/" + ori_filename.Trim() + "_" + UserSession.Instance.UserName + "_" + sessionid.Trim() + DateTime.Now.Millisecond.ToString();
        return fn;
    }

    private void SavePDF(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.PortableDocFormat, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);
        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript1", "loadpdf('" + generatefilename + "');", true);
    }

    private void SaveExcel(string downloadAsFilename, string generatefilename, ReportDocument rptDoc)
    {
        rptDoc.ExportToDisk(ExportFormatType.ExcelWorkbook, downloadAsFilename);
        string FilePath = Server.MapPath(generatefilename);

        FileInfo file = new FileInfo(FilePath);
        if (file.Exists)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
            HttpContext.Current.Response.AddHeader("Content-Length", file.Length.ToString());
            HttpContext.Current.Response.ContentType = "application/octet-stream";
            HttpContext.Current.Response.WriteFile(file.FullName);
            HttpContext.Current.Response.End();
        }
    }

    protected void ButtonFilter_Click(object sender)
    {
        try
        {
            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument();
            SavePDF(Server.MapPath(GenerateFileName("AssetRegistry", Session.SessionID.ToString())) + ".pdf", GenerateFileName("AssetRegistry", Session.SessionID.ToString()) + ".pdf", repDoc);
        }
        catch (Exception ex)
        {
        }
    }

    protected void ButtonExport_Click(object sender)
    {
        try
        {
            if (this.repDoc != null)
            {
                this.repDoc.Close();
                this.repDoc.Dispose();
            }

            this.repDoc = new ReportDocument();
            repDoc = GetReportDocument();
            SaveExcel(Server.MapPath(GenerateFileName("AssetRegistry", Session.SessionID.ToString())) + ".xlsx", GenerateFileName("AssetRegistry", Session.SessionID.ToString()) + ".xlsx", repDoc);
        }
        catch (Exception ex)
        {
        }
    }

    private DateTime? GetFromDate()
    {
        try
        {
            DateTime fromDate = DateTime.Parse(buttonbar1.GetFromDateText());
            return fromDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private DateTime? GetToDate()
    {
        try
        {
            DateTime toDate = DateTime.Parse(buttonbar1.GetToDateText());
            return toDate;
        }
        catch (Exception ex)
        {
            return null;
        }
    }

    private string GetAssetCode()
    {
        try
        {
            string assetCode = buttonbar1.GetAssetCode();
            return assetCode;
        }
        catch (Exception)
        {

            throw;
        }
    }

    private string GetAssetSerial()
    {
        try
        {
            string assetSerial = buttonbar1.GetAssetSerial();
            return assetSerial;
        }
        catch (Exception)
        {

            throw;
        }
    }

    private string GetAssetStatus()
    {
        try
        {
            string assetStatus = buttonbar1.GetAssetDetailStatus();
            return assetStatus;
        }
        catch (Exception)
        {

            throw;
        }
    }

    private string GetAssetType()
    {
        try
        {
            string assetType = buttonbar1.GetAssetType();
            return assetType;
        }
        catch (Exception)
        {

            throw;
        }
    }

    private void LoadAssetTypes()
    {
        ArgsModel args = new ArgsModel();
        args.OrderBy = "asset_type_name ASC";
        args.StartIndex = 1;
        args.RowCount = 500;
        AssetsClient assetClient = new AssetsClient();
        List<AssetTypeModel> assetTypeList = new List<AssetTypeModel>();

        assetTypeList = assetClient.GetAllAssetTypes(args);
        if (assetTypeList.Count > 0)
            assetTypeList.Insert(0, new AssetTypeModel() { AssetTypeId = 0, AssetTypeName = "ALL" });
        buttonbar1.SetAssetTypes(assetTypeList);
    }

}