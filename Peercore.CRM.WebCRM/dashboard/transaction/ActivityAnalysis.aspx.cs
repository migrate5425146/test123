﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using Peercore.CRM.Shared;
using System.Text;

public partial class dashboard_transaction_ActivityAnalysis : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        Master.SetBreadCrumb("Activity Analysis", "#", "");
        buttonbar1.onButtonFilter = new usercontrols_buttonbar_analysisgraph.ButtonFilter(ButtonFilter_Click);
        buttonbar1.onButtonExport = new usercontrols_buttonbar_analysisgraph.ButtonExport(ButtonExport_Click);
        buttonbar1.onDropDownListReps = new usercontrols_buttonbar_analysisgraph.DropDownListReps(DropDownListReps_SelectionChanged);
        buttonbar1.onDropDownListSector = new usercontrols_buttonbar_analysisgraph.DropDownListSector(DropDownListSector_SelectionChanged);

        if (!IsPostBack)
        {
            buttonbar1.SetVisible(1);
            LoadReps();
            LoadSectors();
        }
        
    }
    private void LoadReps()
    {
        OriginatorClient originatorClient = new OriginatorClient();

        try
        {
            OriginatorDTO originator = new OriginatorDTO();
            ArgsDTO args = new ArgsDTO();
            args.Originator = UserSession.Instance.UserName;
            args.ManagerMode = UserSession.Instance.ManagerMode;

            List<OriginatorDTO> lstOriginator = originatorClient.GetChildOriginatorsList(args);
            originator = originatorClient.GetOriginator(args.Originator);

            lstOriginator.Add(originator);

            // ALL
            originator = new OriginatorDTO();
            originator.OriginatorId = 0;
            originator.UserName = "ALL";
            originator.Name = "ALL";
            lstOriginator.Add(originator);

            lstOriginator.OrderBy(s => s.Name);

            buttonbar1.SetReps(lstOriginator);
            buttonbar1.SetRepsSelect("ALL");
        }
        catch (Exception oException)
        {
        }

    }

    private void LoadSectors()
    {
        OriginatorClient oOriginator = new OriginatorClient();
        try
        {
            List<OriginatorDTO> lstOriginator = oOriginator.GetAllRepCodes(buttonbar1.GetRepsValue() != null ? buttonbar1.GetRepsValue().ToString() : UserSession.Instance.Originator);

            OriginatorDTO originator = new OriginatorDTO();

            // ALL
            originator.RepCode = "ALL";
            lstOriginator.Add(originator);

            lstOriginator.OrderBy(s => s.RepCode);

            buttonbar1.SetSector(lstOriginator);
            //buttonbar1.SetSectorSelect(args.Originator);
            ConfigureGraph();
        }
        catch (Exception oException)
        {
        }
    }

    private void ConfigureGraph()
    {
        List<AppointmentCountViewDTO> appointmentCountlst=null;

        string series = "";
        string title = "";
        string title1 = "";
        string name = "";

        ActivityClient oActivity = new ActivityClient();
        AppointmentClient oAppointment = new AppointmentClient();
        OriginatorClient oOriginator = new OriginatorClient();
        OriginatorDTO originator = null;

        List<AppointmentViewDTO> listAppointments = new List<AppointmentViewDTO>();
        List<CustomerActivitiesDTO> listActivity = new List<CustomerActivitiesDTO>();
        string[] sArrMonths = { "", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" };
        string userName = "", fromDate = "", toDate = "", sector = "";
        

        if (!string.IsNullOrEmpty(buttonbar1.GetRepsValue()) && buttonbar1.GetRepsValue() != "ALL")
        {
            originator = oOriginator.GetOriginator(buttonbar1.GetRepsValue());
            userName = buttonbar1.GetRepsValue();// UserSession.Instance.Originator;// buttonbar1.GetRepsValue();
        }
        else
        {
            originator = new OriginatorDTO();
            originator.UserName = "ALL";
            originator.RepType = "";
            userName = buttonbar1.GetRepsValue();
        }
        if (!string.IsNullOrEmpty(buttonbar1.GetSectorValue()))
            sector = buttonbar1.GetSectorValue().ToString().Trim();

        fromDate = new DateTime(DateTime.Today.AddMonths(-3).Year, DateTime.Today.AddMonths(-3).Month, 1).ToString();
        toDate = (new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1)).AddMonths(1).AddDays(-1).ToString();


        // Get Appointments
        listAppointments = new List<AppointmentViewDTO>();
        ArgsDTO args = new ArgsDTO();
        args.Originator = (userName == "ALL" ? "" : userName);
        args.SStartDate = DateTime.Parse(fromDate).ToString("dd-MMM-yyyy") + " 00:00:00";
        args.SEndDate = DateTime.Parse(toDate).ToString("dd-MMM-yyyy") + " 23:59:59";
        args.Sector = (sector == "ALL" ? "" : sector);
        args.OriginatorId = originator.OriginatorId;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.ChildOriginators = UserSession.Instance.ChildOriginators;

        CurrentUserSettings = args;

        try
        {
           appointmentCountlst = oAppointment.GetAllAppointmentsByTypeCount(args);
            
        }
        catch (Exception oException)
        {
        }

        // Get Activities
        listActivity = new List<CustomerActivitiesDTO>();
        AppointmentCountViewDTO appointmentCount = null;
        try
        {

            args.OrderBy = " activity_id asc";
            args.ActivityStatus = "";
            args.AddressType = "";
            appointmentCount = oActivity.GetAllActivitiesByTypeCount(args);
        }
        catch (Exception oException)
        {
        }

        if (originator != null && originator.UserName != "ALL")
            title = "ACTIVITY ANALYSIS FOR " + originator.Name.ToUpper()
                + (originator.RepType == "B" ? " - BAKERY" : (originator.RepType == "F" ? " - FOOD SERVICES" : ""));
        else
            title = "ACTIVITY ANALYSIS";

        title1 = "Month of " + sArrMonths[DateTime.Today.Month] + " " + DateTime.Today.Year.ToString();

        foreach (AppointmentCountViewDTO item in appointmentCountlst)
        {

            series += "{ name: '" + item.Description + "', color: '" + item.Color + "',type: 'column', yAxis: 1,data: [";
            series += item.Week1 + "," + item.Week2 + "," + item.Week3 + "," + item.Week4 + "," + item.Week5 + "," + item.Month1 + "," + item.Month2 + "," + item.Month3;
            series +=  "]},";
        }
        name = "'Week1','Week2','Week3','Week4','Week5','" + DateTime.Today.AddMonths(-1).ToString("MMMM") + "','" + DateTime.Today.AddMonths(-2).ToString("MMMM") + "','" + DateTime.Today.AddMonths(-3).ToString("MMMM") + "'";
        
        
        series += "{ name: '1', color: '#4572A7',type: 'line', yAxis: 1,showInLegend: false, data: [";
        series += appointmentCount.Week1 + "," + appointmentCount.Week2 + "," + appointmentCount.Week3 + "," + appointmentCount.Week4 + "," + appointmentCount.Week5 + "," + appointmentCount.Month1 + "," + appointmentCount.Month2 + "," + appointmentCount.Month3;
        series += "]}";



        StringBuilder sb = new StringBuilder();

        sb.Append("<script type=\"text/javascript\">");

        sb.Append("    $(function () {");
        sb.Append("        var chart;");
        sb.Append("        $(document).ready(function () {");
        sb.Append("            chart = new Highcharts.Chart({");
        sb.Append("                chart: {");
        sb.Append("                    renderTo: 'MainContent_container',");
        sb.Append("                    zoomType: 'xy',");
        sb.Append("                    marginBottom: 100");
        sb.Append("                },");
        sb.Append("                title: {");
        sb.Append("                    text: '" + title + "'");
        sb.Append("                },");
        sb.Append("                subtitle: {");
        sb.Append("                    text: '" + title1 + "'");
        sb.Append("                },");
        sb.Append("                xAxis: [{");
        sb.Append("                    categories: [");
        sb.Append(name);
        sb.Append("                                 ]");
        sb.Append("                }],");
        sb.Append("                yAxis: [{ ");
        sb.Append("                    labels: {");
        sb.Append("                        enabled: false,");
        sb.Append("                        formatter: function () {");
        sb.Append("                            return this.value ;");
        sb.Append("                        },");
        sb.Append("                        style: {");
        sb.Append("                            color: '#4572A7'");
        sb.Append("                        }");
        sb.Append("                    },");
        sb.Append("                    title: {");
        sb.Append("                        text: ' ',");
        sb.Append("                        style: {");
        sb.Append("                            color: '#4572A7'");
        sb.Append("                        }");
        sb.Append("                    }");
        sb.Append("                    ,opposite: true");
        sb.Append("                }, { ");
        sb.Append("                    title: {");
        sb.Append("                        text: '# of Activities',");
        sb.Append("                        style: {");
        sb.Append("                            color: '#4572A7'");
        sb.Append("                        }");
        sb.Append("                    },");
        sb.Append("                    labels: {");
        //sb.Append("                        enabled: false,");
        sb.Append("                        formatter: function () {");
        sb.Append("                            return this.value;");
        sb.Append("                        },");
        sb.Append("                        style: {");
        sb.Append("                            color: '#4572A7'");
        sb.Append("                        }");
        sb.Append("                    },");
        sb.Append("                    opposite: false");
        sb.Append("                }],");
        sb.Append("                tooltip: {");
        sb.Append("                    formatter: function () {");
        sb.Append("                         if(this.series.name == '1'){");
        sb.Append("                             return ('Actual Activities : ' + this.x + ',' + this.y );");
        sb.Append("                         }");
        sb.Append("                         else{ ");
        sb.Append("                             return ('Planned Activities : ' +this.series.name + ',' + this.y)");
        sb.Append("                         }");
        //sb.Append("                        return ");
        //sb.Append("                (this.series.name == '' ? ('Actual Activities : ' + this.x + ',' + this.y ) : ('Planned Activities : ' +this.series.name + ',' + this.y+ this.x)) ");
       // sb.Append("                ;");
        sb.Append("                    }");
        sb.Append("                },");
        sb.Append("                legend: {");
        sb.Append("                    align: 'center',");
        sb.Append("                    x: 0,");
        sb.Append("                    verticalAlign: 'bottom',");
        sb.Append("                    y: 15,");
        sb.Append("                    floating: true,");
        sb.Append("                    backgroundColor: '#FFFFFF'");
        sb.Append("                },");
        sb.Append("                plotOptions: {");
        sb.Append("                    column: {");

        sb.Append("                         allowPointSelect: true,");
        sb.Append("                         cursor: 'pointer',");
        sb.Append("                         point: {");
        sb.Append("                             events: {");
        sb.Append("                                 click: function (e) {var n=this.series.name.split('-'); ");
        sb.Append("                                     GridColumnAnalysisGraph(this.x,this.series.name); ");
        sb.Append("                                 }");
        sb.Append("                             }");
        sb.Append("                         },");

        
        sb.Append("                        stacking: 'normal',");
        sb.Append("                        dataLabels: {");
        sb.Append("                            enabled: true,");
        sb.Append("                            color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'");
        sb.Append("                        }");
        sb.Append("                    }");



        sb.Append("                    ,line: {");
        sb.Append("                         allowPointSelect: true,");
        sb.Append("                         cursor: 'pointer',");
        sb.Append("                         point: {");
        sb.Append("                             events: {");
        sb.Append("                                 click: function (e) {");
        sb.Append("                                     GridLineAnalysisGraph(this.x); ");
        sb.Append("                                 }");
        sb.Append("                             }");
        sb.Append("                         },");
        sb.Append("                    }");


        sb.Append("                },");
        sb.Append("                series: [");
        sb.Append(series);
        sb.Append("                ]");
        sb.Append("            });");
        sb.Append("        });");

        sb.Append("    });");
        sb.Append("</script>");

        container.InnerHtml = sb.ToString();

    }

    protected void ButtonFilter_Click(object sender)
    {
        ConfigureGraph();
    }
    protected void DropDownListReps_SelectionChanged(object sender)
    {
        LoadSectors();
    }
    protected void DropDownListSector_SelectionChanged(object sender)
    {
    }

    protected void ButtonExport_Click(object sender)
    {
    }
}