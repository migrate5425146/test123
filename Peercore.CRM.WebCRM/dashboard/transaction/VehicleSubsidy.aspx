﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="VehicleSubsidy.aspx.cs" Inherits="dashboard_transaction_VehicleSubsidy" %>

<%@ Register Src="~/usercontrols/buttonbar_reports.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ Register Src="~/usercontrols/toolbar_reports.ascx" TagPrefix="ucl" TagName="toolbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="divcontectmainforms">
    </div>
    <div align="right">
        <div class="bg" align="left">
            <div id="div_message" class="savemsg" runat="server" style="display: none">
            </div>
            <div style="padding-left: 10px;">
                <ucl:buttonbar ID="buttonbar1" runat="server" />
            </div>
        </div>
    </div>
    <div class="clearall">
        &nbsp;
    </div>
    <div style="margin-top: 25px;">
        <div style="min-width: 500px; height: 500px;" id="target">
            <embed src="" width="100%" height="100%" />
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            $("#MainContent_toolbar1_a_trade_load_summary").css("background-color", "#ff6600");
        });

        function loadpdf(name) {
            $("#target embed").attr("src", name);
        }
    </script>
</asp:Content>

