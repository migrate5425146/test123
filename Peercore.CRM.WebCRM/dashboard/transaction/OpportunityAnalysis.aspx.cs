﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using Peercore.CRM.Shared;
using System.Text;
using Peercore.CRM.Common;

public partial class dashboard_transaction_OpportunityAnalysis : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        Master.SetBreadCrumb("Dashboard", "#", "");
        Master.SetBreadCrumb("Opportunity Conversion", "#", "");

        buttonbar1.onButtonFilter = new usercontrols_buttonbar_analysisgraph.ButtonFilter(ButtonFilter_Click);
        //buttonbar1.onButtonExport = new usercontrols_buttonbar_analysisgraph.ButtonExport(ButtonExport_Click);
        //buttonbar1.onDropDownListReps = new usercontrols_buttonbar_analysisgraph.DropDownListReps(DropDownListReps_SelectionChanged);
        //buttonbar1.onDropDownListSector = new usercontrols_buttonbar_analysisgraph.DropDownListSector(DropDownListSector_SelectionChanged);

        if (!IsPostBack)
        {
            buttonbar1.SetVisible(3);
            LoadReps();
            LoadOptions();
            LoadGrid();
        }

    }

    private void LoadReps()
    {
        OriginatorClient originatorClient = new OriginatorClient();

        try
        {
            OriginatorDTO originator = new OriginatorDTO();
            ArgsDTO args = new ArgsDTO();
            args.Originator = UserSession.Instance.UserName;
            args.ManagerMode = UserSession.Instance.ManagerMode;

            List<OriginatorDTO> lstOriginator = originatorClient.GetChildOriginatorsList(args);
            originator = originatorClient.GetOriginator(args.Originator);
            originator.UserName = args.Originator;
            lstOriginator.Add(originator);

            // ALL
            originator = new OriginatorDTO();
            originator.OriginatorId = 0;
            originator.UserName = "ALL";
            originator.Name = "National";
            lstOriginator.Add(originator);

            lstOriginator.OrderBy(s => s.Name);

            buttonbar1.SetReps(lstOriginator);
            buttonbar1.SetRepsSelect("ALL");
        }
        catch (Exception oException)
        {
        }

    }

    private void LoadOptions()
    {
        try
        {
            //brState oLead = new brState();
            OriginatorClient originatorClient = new OriginatorClient();

            //List<State> stateList = oLead.GetStates();
            List<StateDTO> stateList = originatorClient.GetDistinctRepStates();
            StateDTO defaultState = new StateDTO()
            {
                StateID = 0,
                StateName = "-ALL-"
            };
            stateList.Insert(0, defaultState);

            buttonbar1.SetStageLoad(stateList);
            buttonbar1.SetStateSelect("ALL");

            CommonClient oSales = new CommonClient();
            List<LookupTableDTO> lookupTable = oSales.GetSalesMarkets();

            LookupTableDTO lookup = new LookupTableDTO();
            lookup.TableCode = "ALL";
            lookup.TableDescription = "-ALL-";
            lookupTable.Insert(0, lookup);

            buttonbar1.SetMarketLoad(lookupTable);
            buttonbar1.SetMarketSelect("ALL");

            buttonbar1.SetFromDateText(DateTime.Today.AddYears(-1).ToString("dd-MMM-yyyy"));
            buttonbar1.SetToDateText(DateTime.Today.ToString("dd-MMM-yyyy"));

        }
        catch (Exception)
        {
            throw;
        }
    }

    public void LoadGrid()
    {
        try
        {
            CommonClient commonClient = new CommonClient();

            ArgsDTO args = new ArgsDTO();

            args.Originator = buttonbar1.GetRepsValue();
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.SStartDate = buttonbar1.GetFromDateText();
            args.SEndDate = buttonbar1.GetToDateText();
            args.StartIndex = 1;
            args.RowCount = 500;
            args.OrderBy = "";
            args.Market = buttonbar1.GetMarketValue();
            args.State = buttonbar1.GetStateValue();
            
            Session[CommonUtility.GLOBAL_SETTING] = args;

            List<LeadCustomerOpportunityDAO> leadCustOppList = commonClient.GetLeaderCustomerOpportunityCount(args);
            AnalysisGraph(leadCustOppList);


        }
        catch (Exception oException)
        {
        }
    }

    public List<string> SetSeriesStateData(List<LeadCustomerOpportunityDAO> leadCustOppList)
    {
        List<string> sList=new List<string>();
        string series = "";
        string categories = "";
        int count = leadCustOppList.Count;
        int index = 0;
        foreach (LeadCustomerOpportunityDAO item in leadCustOppList)
        {
            
            series += ",{ name: '" + item.Name + "', data: [";
            string dataset = "";
            double pr =(item.Won/double.Parse( item.rowCount.ToString()))*100;
            for (int i = 0; i < count; i++)
            {
                if (i == index)
                    dataset += ",{y:" + item.Amount + ",amount:'" + item.Amount + "',des:'" + item.Originator + "',pr:'" + item.Ratio + "'}";
                else
                    dataset += ",{y:0}";
            }
            series += dataset.Remove(0, 1); 
            series += "] }";

            categories = categories + ",'" + item.Name + "'";
            index++;
        }
        categories = categories.Length > 0 ? categories.Remove(0, 1) : categories;
        series = series.Length > 0 ? series.Remove(0, 1) : series;

        sList.Add(categories);
        sList.Add(series);

        return sList;
    }

    public void AnalysisGraph(List<LeadCustomerOpportunityDAO> leadCustOppList)
    {
        string reportHeader = "";
        string reportDate = "";
        string reportClick = "";


        List<string> sList =SetSeriesStateData(leadCustOppList);


        StringBuilder sb = new StringBuilder();

        sb.Append("<div>");
        sb.Append("<script type=\"text/javascript\">");
        sb.Append("        $(function () {");
        sb.Append("            var chart;");
        sb.Append("            $(document).ready(function () {");
        sb.Append("                chart = new Highcharts.Chart({");
        sb.Append("                    chart: {");
        sb.Append("                        renderTo: 'MainContent_container',");
        sb.Append("                        type: 'column',marginBottom: 100");
        sb.Append("                    },");
        sb.Append("                    title: {");
        sb.Append("                        text: 'Sales Reps` Conversion of Opportunities to Actual Salses'");
        sb.Append("                    },");
        sb.Append("                    xAxis: {");
        sb.Append("                        categories: [" + sList [0]+ "]");
        sb.Append("                    },");
        sb.Append("                    yAxis: {");
        sb.Append("                        min: 0,");
        sb.Append("                        title: {");
        sb.Append("                            text: 'Amount'");
        sb.Append("                        },");
        sb.Append("                        stackLabels: {");
        sb.Append("                            enabled: false,");
        sb.Append("                            style: {");
        sb.Append("                                fontWeight: 'bold',");
        sb.Append("                                color: (Highcharts.theme && Highcharts.theme.textColor) || 'gray'");
        sb.Append("                            }");
        sb.Append("                        }");
        sb.Append("                    },");
        sb.Append("                    legend: {");
        sb.Append("                        align: 'center',");
        sb.Append("                        x: 0,");
        sb.Append("                        verticalAlign: 'bottom',");
        sb.Append("                        y: 0,");
        sb.Append("                        floating: true,");
        sb.Append("                        backgroundColor: (Highcharts.theme && Highcharts.theme.legendBackgroundColorSolid) || 'white',");
        sb.Append("                        borderColor: '#CCC',");
        sb.Append("                        borderWidth: 1,");
        sb.Append("                        shadow: false");
        sb.Append("                    },");
        sb.Append("                    tooltip: {");
        sb.Append("                        formatter: function () {");
        sb.Append("                            return 'Amount: ' + this.y + ' Conversion Ratio: ' +");
        sb.Append("                        this.point.pr + ' %' ;");
        //sb.Append("                        'Total: ' + this.point.pr;");
        sb.Append("                        }");
        sb.Append("                    },");
        sb.Append("                    plotOptions: {");
        sb.Append("                        column: {");
        sb.Append("                            stacking: 'normal',");
        sb.Append("                            point: {");
        sb.Append("                                 events: {");
        sb.Append("                                     click: function() {");
        sb.Append("                                         GridLeadCustOpp(this.des);");
        sb.Append("                                     }");
        sb.Append("                                 }");
        sb.Append("                             },");
        sb.Append("                            dataLabels: {");
        sb.Append("                                enabled: false,");
        sb.Append("                                color: (Highcharts.theme && Highcharts.theme.dataLabelsColor) || 'white'");
        sb.Append("                            }");
        sb.Append("                        }");
        sb.Append("                    },");
        sb.Append("                    series: [");
        sb.Append(sList [1]);
        sb.Append("                    ]");
        sb.Append("                });");
        sb.Append("            });");

        sb.Append("        });");
        sb.Append("</script>");
        sb.Append("</div>");

        container.InnerHtml = sb.ToString();
    }
    protected void ButtonFilter_Click(object sender)
    {
        LoadGrid();
    }
}