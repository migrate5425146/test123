﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using System.Web.Services;
using CRMServiceReference;
using Peercore.CRM.Entities;
using Peercore.CRM.Common;



public partial class dashboard_transaction_ChangePassword : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        string userName = UserSession.Instance.OriginalUserName;
        UserName.Text = userName;

        //Remove the session before setting the breadcrumb.
        Session.Remove(CommonUtility.BREADCRUMB);
        Master.SetBreadCrumb("Change Password ", "#", "");
    }
    protected void changePassword_Click(object sender, EventArgs e)
    {        
        string password = currentPassword.Text;
        OriginatorClient Originator = new OriginatorClient();

        OriginatorDTO Dto = new OriginatorDTO();
        string userName = UserSession.Instance.OriginalUserName;
        Dto = Originator.GetLoginDetails(userName, password);

        CommonUtility commonUtility = new CommonUtility();

        if (Dto.AccessToken == null)
        {
            div_message.Attributes.Add("style", "display:block;padding:2px");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Current password you entered is invalid");            
            return;
            
        }
        else
        {
            if (newPassword.Text == null || newPassword.Text == "" || newPassword.Text == " ")
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Password cannot be blank ");
                return;
            }
            else if (newPassword.Text == confirmPassword.Text)
            {
                Originator.UpdatePassword(userName, newPassword.Text);
                ScriptManager.RegisterStartupScript(this, Page.GetType(), "key", "MyFunc()", true);
            }
            else
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Invalid confirm password");
                return;
            }
        }

    }
    protected void Cancle_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/Default.aspx");
    }
}
