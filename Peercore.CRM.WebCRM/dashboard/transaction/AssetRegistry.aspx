﻿<%@ Page Title="mSales | Reports" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="AssetRegistry.aspx.cs" Inherits="dashboard_transaction_AssetRegistry" %>

<%@ Register Src="~/usercontrols/buttonbar_reports.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ Register Src="~/usercontrols/toolbar_reports.ascx" TagPrefix="ucl" TagName="toolbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div class="divcontectmainforms">
        <div class="title_bar">
            <div style="width: 90%; float: left;">
                <ucl:toolbar ID="toolbar1" runat="server" />
            </div>
            <div style="width: 10%; float: right;">
                <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Default.aspx"> <div class="back"></div>
                </asp:HyperLink>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div align="right">
            <div class="bg" align="left">
                <div id="div_message" class="savemsg" runat="server" style="display: none">
                </div>
                <div style="padding-left: 10px;">
                    <ucl:buttonbar ID="buttonbar1" runat="server" />
                </div>
            </div>
        </div>
        <div class="clearall">
            &nbsp;
        </div>
        <div style="margin-top: 25px;">
            <div style="min-width: 500px; height: 500px;" id="target">
                <embed src="" width="100%" height="100%" />
                <%--  <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" HasCrystalLogo="False"
                        HasToggleGroupTreeButton="False" EnableDatabaseLogonPrompt="False" EnableParameterPrompt="False"
                        BorderWidth="1px" Height="300px" ReuseParameterValuesOnRefresh="True" Width="450px" />--%>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            $("#MainContent_toolbar1_a_asset_registry_reports").css("background-color", "#ff6600");
            //$("#btnShowAdvancedOptions").css("display", "none");

        });

        function loadpdf(name) {
            $("#target embed").attr("src", name);
            console.log(name)
        }

    </script>
</asp:Content>



