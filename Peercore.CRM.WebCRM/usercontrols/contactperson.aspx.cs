﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;

public partial class usercontrols_contactperson : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ArgsDTO argsDTO = (ArgsDTO)(Session[CommonUtility.GLOBAL_SETTING]);
      //  string LStageName = argsDTO.LeadStage.ToString();
        string LStageName = (!string.IsNullOrEmpty(argsDTO.LeadStage)) ? argsDTO.LeadStage.ToString() : string.Empty;

        if ((Request.QueryString["leadid"] != null && Request.QueryString["leadid"] != string.Empty))
        {
            id_AddContactPerson.HRef = ConfigUtil.ApplicationPath + "lead_customer/transaction/contact_person_entry.aspx?leadid=" + Request.QueryString["leadid"].ToString()+ "&fm=lead"+((!string.IsNullOrEmpty(LStageName))?"&ty="+ LStageName : "");
        }
        else if (Session[CommonUtility.LEAD_DATA] != null)
        {
            KeyValuePair<string, string> LeadEntry = (KeyValuePair<string, string>)Session[CommonUtility.LEAD_DATA];
            id_AddContactPerson.HRef = ConfigUtil.ApplicationPath + "lead_customer/transaction/contact_person_entry.aspx?leadid=" + LeadEntry.Key;
        }
    }

    public void EnableDisableAddButton(bool isEnable)
    {
        if (isEnable)
            id_AddContactPerson.Disabled = false;
        else
            id_AddContactPerson.Disabled = true;
    }
}