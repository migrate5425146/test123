﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;

public partial class usercontrols_contactdetails : System.Web.UI.Page
{
    KeyValuePair<string, string> LeadEntry = new KeyValuePair<string, string>();
    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[CommonUtility.LEAD_DATA] != null)
        {
            LeadEntry = (KeyValuePair<string, string>)Session[CommonUtility.LEAD_DATA];
        }
        
        LoadContactDetails();
        
    }
    #endregion

    #region Methods
    private void LoadContactDetails()
    {
        // Load the preferred contact
        ArgsDTO argsDTO = (ArgsDTO)(Session[CommonUtility.GLOBAL_SETTING]);
        argsDTO.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        CommonClient oLookupTable = new CommonClient();
        List<LookupTableDTO> preferredMethodList = new List<LookupTableDTO>();
        try
        {
            preferredMethodList = oLookupTable.GetLookupTables("CONT", argsDTO);

            ddlPreferredMethod.DataSource = preferredMethodList;
            ddlPreferredMethod.DataTextField = "TableDescription";
            ddlPreferredMethod.DataValueField = "TableCode";
            ddlPreferredMethod.DataBind();

            foreach (LookupTableDTO item in preferredMethodList)
            {
                if (item.DefaultValue == "Y")
                {
                    ddlPreferredMethod.SelectedValue = item.TableCode; break;
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    #endregion
}