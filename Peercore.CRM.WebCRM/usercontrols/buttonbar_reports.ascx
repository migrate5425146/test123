﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="buttonbar_reports.ascx.cs"
    Inherits="usercontrols_buttonbar_reports" %>
<script type="text/javascript">   

</script>
<div id="div_basic_options">
    <div class="dropdown_large" id="div_asset_registry_reports" runat="server" visible="false" style="width: 170px">
        <asp:Label ID="lblAssetRegisry" runat="server" Text="Report Name" Visible="true"></asp:Label>
        <asp:DropDownList ID="ddlAssetRegistery" runat="server" Visible="true" AutoPostBack="true" 
            Width="160px" OnSelectedIndexChanged="ddlAssetRegistery_SelectedIndexChanged">
            <asp:ListItem Text="Asset Detail" Value="asset_detail" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Asset Registry" Value="asset_registry"></asp:ListItem>
            <asp:ListItem Text="Monthly Bill Payments" Value="monthly_payments"></asp:ListItem>
            <asp:ListItem Text="Monthly Deductions" Value="monthly_deductions"></asp:ListItem>
        </asp:DropDownList>
    </div>

    <div class="dropdown_large" id="div_other_reports" runat="server" visible="false"
        style="width: 105px">
        <asp:Label ID="lblOtherReprots" runat="server" Text="Report Name" Visible="true"></asp:Label>
        <asp:DropDownList ID="ddlOtherReport" runat="server" Visible="true" OnSelectedIndexChanged="ddlOtherReport_SelectedIndexChanged"
            Width="100px">
            <asp:ListItem Text="SFA Working Hours" Value="sfa_working_hours" Selected="True"></asp:ListItem>
            <asp:ListItem Text="SFA Invoice Summary" Value="sfa_invoice_summary"></asp:ListItem>
            <asp:ListItem Text="SFA Order Summary" Value="sfa_order_summary"></asp:ListItem>
            <asp:ListItem Text="Daily Absent Reps" Value="daily_absent_reps"></asp:ListItem>
            <asp:ListItem Text="ASM Visits" Value="asm_visits"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="datepicker" id="div_Month" runat="server" visible="false">
        <asp:Label ID="lblMonth" runat="server" Text="Month" Visible="true"></asp:Label>
        <asp:TextBox ID="dtpMonth" runat="server" Style="width: 120px;" ClientIDMode="Static"></asp:TextBox>
    </div>
    <div class="dropdown_large" id="div_showReport" runat="server" visible="false" style="width: 140px">
        <asp:Label ID="Label3" runat="server" Text="Report" Visible="true"></asp:Label>
        <asp:DropDownList ID="cmbreport" runat="server" Visible="true" OnSelectedIndexChanged="cmbShowReport_SelectedIndexChanged"
            AutoPostBack="true" Width="130px">
            <%--<asp:ListItem Text="Rep Working hours" Value="rep_hours" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Distributors" Value="distributor" ></asp:ListItem>
            <asp:ListItem Text="Reps" Value="rep" ></asp:ListItem>--%>
        </asp:DropDownList>
    </div>
    <div class="dropdown_large" id="div_Idle_cust_Days" runat="server" visible="false"
        style="width: 105px">
        <asp:Label ID="Ibl_showDays" runat="server" Text="Idle Time" Visible="true"></asp:Label>
        <asp:DropDownList ID="cmbShowIdleDays" runat="server" Visible="true" OnSelectedIndexChanged="cmbShowCustIdleDays_SelectedIndexChanged"
            AutoPostBack="true" Width="100px">
            <%--<asp:ListItem Text="ASMs" Value="ase" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Distributors" Value="distributor" ></asp:ListItem>
            <asp:ListItem Text="Reps" Value="rep" ></asp:ListItem>--%>
        </asp:DropDownList>
    </div>
    <div class="datepicker" id="div_FromDate" runat="server" visible="false">
        <asp:Label ID="lbStartDate" runat="server" Text="Start Date" Visible="true"></asp:Label>
        <asp:TextBox ID="dtpFromDate" runat="server" Style="width: 120px;" ClientIDMode="Static"></asp:TextBox>
    </div>
    <div class="datepicker" id="div_ToDate" runat="server" visible="false">
        <asp:Label ID="lbEndDate" runat="server" Text="End Date" Visible="true"></asp:Label>
        <asp:TextBox ID="dtpToDate" runat="server" Style="width: 120px;" ClientIDMode="Static"></asp:TextBox>
    </div>
    <div class="dropdown_large" id="div_trade_load_report_type" runat="server" visible="false"
        style="width: 105px">
        <asp:Label ID="Label5" runat="server" Text="Report Type" Visible="true"></asp:Label>
        <asp:DropDownList ID="ddlTradeLoadRepType" runat="server" Visible="true" Width="100px">
            <asp:ListItem Text="Product Wise" Value="p" Selected="True"></asp:ListItem>
            <asp:ListItem Text="ASM Wise" Value="a"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="dropdown_large" id="divUploadSummaryReportType" runat="server" visible="false"
        style="width: 105px">
        <asp:Label ID="lblUSReportType" runat="server" Text="Report Type" Visible="true"></asp:Label>
        <asp:DropDownList ID="ddlUploadSummaryRepType" runat="server" Visible="true" Width="100px">
            <asp:ListItem Text="Stock Upload" Value="su" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Target Summary" Value="ts"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="dropdown_large" id="div_ReportTypeQtyValue" runat="server" visible="false" style="width: 105px">
        <asp:Label ID="blReportTypeQtyValue" runat="server" Text="Report Type" Visible="true"></asp:Label>
        <asp:DropDownList ID="cmbReportTypeQtyValue" runat="server" Visible="true" Width="100px">
            <asp:ListItem Text="Quantity" Value="q" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Value" Value="v"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="dropdown_large" id="div_report_type" runat="server" visible="false" style="width: 105px">
        <asp:Label ID="blReportType" runat="server" Text="Report Type" Visible="true"></asp:Label>
        <asp:DropDownList ID="cmbReportType" runat="server" Visible="true" Width="100px">
            <asp:ListItem Text="Quantity" Value="q" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Value" Value="v"></asp:ListItem>
            <asp:ListItem Text="Free Issues" Value="f"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="dropdown_large" id="div_RepsItemSales_report_type" runat="server" visible="false"
        style="width: 105px">
        <asp:Label ID="bl_RepsItemSalesReportType" runat="server" Text="Report Type" Visible="true"></asp:Label>
        <asp:DropDownList ID="cmb_RepsItemSalesReportType" runat="server" Visible="true"
            Width="100px" OnSelectedIndexChanged="cmb_RepsItemSalesReportType_SelectedIndexChanged" AutoPostBack="true">
            <asp:ListItem Text="Quantity" Value="q" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Value" Value="v"></asp:ListItem>
            <asp:ListItem Text="Invoice Count" Value="ic"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="dropdown_large" id="div_RepsItemSalesBrand_report_type" runat="server" visible="false"
        style="width: 105px">
        <asp:Label ID="bl_RepsItemSalesBrandReportType" runat="server" Text="Report Type" Visible="true"></asp:Label>
        <asp:DropDownList ID="cmb_RepsItemSalesBrandReportType" runat="server" Visible="true"
            Width="100px" OnSelectedIndexChanged="cmb_RepsItemSalesBrandReportType_SelectedIndexChanged" AutoPostBack="true">
            <asp:ListItem Text="Quantity" Value="q" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Value" Value="v"></asp:ListItem>
            <asp:ListItem Text="Invoice Count" Value="ic"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="dropdown_large" id="div_Divisions" runat="server" visible="false" style="width: 105px">
        <asp:Label ID="Label4" runat="server" Text="Division" Visible="true"></asp:Label>
        <asp:DropDownList ID="cmbDivisions" runat="server" Visible="true" Width="100px"
            OnSelectedIndexChanged="cmbDivisions_SelectedIndexChanged" AutoPostBack="true">
        </asp:DropDownList>
    </div>
    <div class="dropdown_large" id="div_showReportFor" runat="server" visible="false"
        style="width: 105px">
        <asp:Label ID="lbl_showReportFor" runat="server" Text="Report For" Visible="true"></asp:Label>
        <asp:DropDownList ID="cmbShowReportFor" runat="server" Visible="true" OnSelectedIndexChanged="cmbShowReportFor_SelectedIndexChanged"
            AutoPostBack="true" Width="100px">
            <asp:ListItem Text="ASM" Value="ase" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Distributors" Value="distributor"></asp:ListItem>
            <asp:ListItem Text="Reps" Value="rep"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="dropdown_large" id="div_showReportForASMDB" runat="server" visible="false"
        style="width: 105px">
        <asp:Label ID="lbl_showReportForASMDB" runat="server" Text="Report For" Visible="true"></asp:Label>
        <asp:DropDownList ID="cmbShowReportForASMDB" runat="server" Visible="true" OnSelectedIndexChanged="cmbShowReportForASMDB_SelectedIndexChanged"
            AutoPostBack="true" Width="100px">
            <asp:ListItem Text="ASM" Value="ase" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Distributor" Value="dis"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <%--<div class="dropdown_large" id="div_product_filter_by" runat="server" visible="false" style="width:105px">
        <asp:Label ID="Label3" runat="server" Text="Filter By" Visible="true"></asp:Label>
        <asp:DropDownList ID="cmbProductFilterBy" runat="server" Visible="true" OnSelectedIndexChanged="cmbProductFilterBy_SelectedIndexChanged"
            AutoPostBack="true" Width="100px">
            <asp:ListItem Text="Brand" Value="brand" Selected="True"></asp:ListItem>
             <asp:ListItem Text="Packing Method" Value="pck_methd" ></asp:ListItem>
             <asp:ListItem Text="Item Group" Value="item_group"></asp:ListItem>
             <asp:ListItem Text="High Value Product" Value="hvp"></asp:ListItem>
        </asp:DropDownList>
    </div>

    <div class="dropdown_large_x" id="div_all_filter_by" runat="server" visible="false" style="padding-left: 5px">
        <asp:Label ID="lvlAllFilterBy" runat="server" Text="Brand" Visible="true"></asp:Label>
        <asp:DropDownList ID="cmbAllFilterBy" runat="server" Visible="true" OnSelectedIndexChanged="cmbAllFilterBy_SelectedIndexChanged"
            AutoPostBack="true" Width="190px">
        </asp:DropDownList>
    </div>--%>
    <div class="dropdown_large" id="divDBActiveInactive" runat="server" visible="false"
        style="padding-left: 10px">
        <asp:Label ID="Label8" runat="server" Text="DB Active/Inactive" Visible="true" Width="120px"></asp:Label>
        <asp:DropDownList ID="ddlDBActiveInactive" runat="server" Visible="true" Width="100px" 
            AutoPostBack="true" OnSelectedIndexChanged="ddlDBActiveInactive_SelectedIndexChanged">
            <asp:ListItem Text="Active" Value="A" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Inactive" Value="D"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="dropdown_large_x" id="div_ase" runat="server" visible="false" style="padding-left: 5px">
        <asp:Label ID="lvlAse" runat="server" Text="ASM" Visible="true"></asp:Label>
        <asp:DropDownList ID="cmbAse" runat="server" Visible="true" OnSelectedIndexChanged="cmbAse_SelectedIndexChanged"
            AutoPostBack="true" Width="190px">
        </asp:DropDownList>
    </div>
    <div class="dropdown_large_x" id="div_distributor" runat="server" visible="false"
        style="padding-left: 5px">
        <asp:Label ID="lbDistributor" runat="server" Text="Distributor" Visible="true"></asp:Label>
        <asp:DropDownList ID="cmbDistributors" runat="server" Visible="true" OnSelectedIndexChanged="cmbDistributors_SelectedIndexChanged"
            AutoPostBack="true" Width="190px">
        </asp:DropDownList>
    </div>
    <div class="dropdown_large_x" id="div_territory" runat="server" visible="false"
        style="padding-left: 5px">
        <asp:Label ID="lblTerritory" runat="server" Text="Territory" Visible="true"></asp:Label>
        <asp:DropDownList ID="cmbTerritory" runat="server" Visible="true" OnSelectedIndexChanged="cmbTerritory_SelectedIndexChanged"
            AutoPostBack="true" Width="190px">
        </asp:DropDownList>
    </div>
    <div class="dropdown_large_x" id="div_mssr_report_type" runat="server" visible="false"
        style="width: 105px">
        <asp:Label ID="Label2" runat="server" Text="Report Type" Visible="true"></asp:Label>
        <asp:DropDownList ID="cmbMSSRReportType" runat="server" Visible="true" Width="100px">
            <asp:ListItem Text="Master" Value="master" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Details" Value="details"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="dropdown_large_x" id="div_rep" runat="server" visible="false" style="padding-left: 5px">
        <asp:Label ID="lblRep" runat="server" Text="Rep" Visible="true"></asp:Label>
        <asp:DropDownList ID="cmbReps" runat="server" Visible="true" Width="190px">
        </asp:DropDownList>
    </div>
    <div class="dropdown_large_x" id="div_VisitNote" runat="server" visible="false" style="padding-left: 5px; width: 100px;">
        <asp:Label ID="Label1" runat="server" Text="For Visit Note" Visible="true" Style="display: block;"></asp:Label>
        <asp:DropDownList ID="cmbVisitNote" runat="server" Visible="true" Width="90px">
            <asp:ListItem Text="Invoice" Value="invoicesummary" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Visit Note" Value="visitnote"></asp:ListItem>
            <asp:ListItem Text="Cancel Invoices" Value="cancelinvoice"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="dropdown_large_x" id="div_OrderSummary" runat="server" visible="false" style="padding-left: 5px; width: 100px;">
        <asp:Label ID="Label6" runat="server" Text="Report Type" Visible="true" Style="display: block;"></asp:Label>
        <asp:DropDownList ID="cmbOrderReportType" runat="server" Visible="true" Width="90px" OnSelectedIndexChanged="cmbOrderReportType_SelectedIndexChanged">
            <asp:ListItem Text="Invoiced Orders" Value="invoiced" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Not Invoiced Orders" Value="notinvoiced"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <div class="dropdown_large_x" id="div_quantitytype" runat="server" visible="false" style="padding-left: 5px; width: 100px;">
        <asp:Label ID="Label9" runat="server" Text="Quantity Type" Visible="true" Style="display: block;"></asp:Label>
        <asp:DropDownList ID="cmbQuantityType" runat="server" Visible="true" Width="90px" OnSelectedIndexChanged="cmbQuantityType_SelectedIndexChanged">
            <asp:ListItem Text="Units" Value="N" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Cases" Value="C"></asp:ListItem>
            <asp:ListItem Text="Tonnage" Value="T"></asp:ListItem>
        </asp:DropDownList>
    </div>
    <%--<div class="dropdown_large_x" id="div_product_filter_by" runat="server" visible="false" style="padding-left: 5px">
        <asp:Label ID="Label2" runat="server" Text="Filter By" Visible="true"></asp:Label>
        <asp:DropDownList ID="cmbProductFilterBy" runat="server" Visible="true"
             Width="150px">
             <asp:ListItem Text="Brand" Value="brand" Selected="True"></asp:ListItem>
             <asp:ListItem Text="Packing Method" Value="pck_methd" ></asp:ListItem>
             <asp:ListItem Text="Item Group" Value="item_group"></asp:ListItem>
             <asp:ListItem Text="High Value Product" Value="hvp"></asp:ListItem>
        </asp:DropDownList>
    </div>--%>
    &nbsp;&nbsp;&nbsp;<asp:Button ID="btnFilter" runat="server" Text="" CssClass="dashboardfilterbtnreport"
        OnClick="btnFilter_Click" />
    <%--<input id="btnexport" type="button" value="" onclick="ExportData()" class="dashboardexportbtnreport" />--%>
    <asp:Button ID="btnexport1" runat="server" Text="" CssClass="dashboardexportbtnreport"
        OnClick="btnexport_Click" Visible="false" />
    <input id="btnShowAdvancedOptions" type="button" value="Show Advanced Options" style="float: right; display: block; width: 175px" />
    <input id="btnHideAdvancedOptions" type="button" value="Hide Advanced Options" style="float: right; display: none; width: 175px" />
</div>
<div class="clearall">
</div>
<div class="clearall">
</div>
<div id="div_advanced_options" style="display: none; padding-bottom: 5px; padding-right: 5px">
    <fieldset>
        <legend style="color: White">Advanced Options</legend>

        <div>
            <div class="dropdown_large" id="div_filterBy_code" runat="server" visible="false">
                <asp:Label ID="lbl_asset_code" runat="server" Text="Asset Code " Visible="true"></asp:Label>
                <asp:TextBox ID="txtAssetCode" runat="server" Width="120px"></asp:TextBox>
            </div>

            <div class="dropdown_large" id="div_filterBy_serial" runat="server" visible="false">
                <asp:Label ID="lbl_asset_serial" runat="server" Text="Asset Serial " Visible="true"></asp:Label>
                <asp:TextBox ID="txtAssetSerial" runat="server" Width="120px"></asp:TextBox>
            </div>

            <div class="dropdown_large" id="div_filterBy_status" runat="server" visible="false">
                <asp:Label ID="lbl_asset_status" runat="server" Text="Asset Status " Visible="true"></asp:Label>
                <asp:DropDownList ID="ddlAssetStatus" runat="server" Visible="true" Width="120px">
                    <asp:ListItem Text="- Select -" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Assigned" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Un-Assigned" Value="2"></asp:ListItem>
                </asp:DropDownList>
            </div>
            
            <div class="dropdown_large" id="div_filterBy_asset_detail_status" runat="server" visible="false">
                <asp:Label ID="lbl_asset_detail_status" runat="server" Text="Asset Status " Visible="true"></asp:Label>
                <asp:DropDownList ID="ddlAssetDetailStatus" runat="server" Visible="true" Width="120px">
                    <asp:ListItem Text="- Select -" Value="0"></asp:ListItem>
                    <asp:ListItem Text="Assigned" Value="1"></asp:ListItem>
                    <asp:ListItem Text="Available" Value="2"></asp:ListItem>
                </asp:DropDownList>
            </div>

            <div class="dropdown_large" id="div_filterBy_type" runat="server" visible="false">
                <asp:Label ID="lbl_asset_type" runat="server" Text="Asset Type" Visible="true"></asp:Label>
                <asp:DropDownList ID="ddlAssetType" runat="server" Visible="true" Width="120px">
                </asp:DropDownList>
            </div>

        </div>


        <div class="dropdown_large" id="div_orderBy" runat="server" visible="false">
            <asp:Label ID="lbl_orderBy" runat="server" Text="Order By" Visible="true"></asp:Label>
            <asp:DropDownList ID="cmbOrderBy" runat="server" Visible="true" Width="120px">
                <asp:ListItem Text="Product Name" Value="productname" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Product Code" Value="productcode"></asp:ListItem>
            </asp:DropDownList>
        </div>
        <div class="dropdown_large" id="div_groupBy" runat="server" visible="false">
            <asp:Label ID="lbl_groupBy" runat="server" Text="Group By" Visible="true"></asp:Label>
            <asp:DropDownList ID="cmbGroupBy" runat="server" Visible="true" Width="120px">
                <asp:ListItem Text="All" Value="all" Selected="True"></asp:ListItem>
                <asp:ListItem Text="Brand" Value="brand"></asp:ListItem>
                <asp:ListItem Text="Flavor" Value="flavor"></asp:ListItem>
                <asp:ListItem Text="Item Group" Value="itemgroup"></asp:ListItem>
                <asp:ListItem Text="Packing Method" Value="packingmethod"></asp:ListItem>
                <%--<asp:ListItem Text="HVP" Value="hvp"></asp:ListItem>--%>
            </asp:DropDownList>
        </div>
        <div id="div_sfa_invoice_summary" runat="server" visible="false">
            <div class="dropdown_large" id="div1" runat="server" visible="true"
                style="padding-left: 10px">
                <asp:Label ID="lblSalesRep" runat="server" Text="Sales Rep" Visible="true"></asp:Label>
                <asp:DropDownList ID="ddlSFAInvoiceSalesRep" runat="server" Visible="true" Width="100px">
                </asp:DropDownList>
            </div>
            <div class="dropdown_large" id="div2" runat="server" visible="true"
                style="padding-left: 10px">
                <asp:Label ID="lblSFAInvoiceProd1" runat="server" Text="Product 1" Visible="true"></asp:Label>
                <asp:DropDownList ID="ddlSFAInvoiceProduct1" runat="server" Visible="true" Width="100px">
                </asp:DropDownList>
            </div>
            <div class="dropdown_large" id="div3" runat="server" visible="true"
                style="padding-left: 10px">
                <asp:Label ID="lblSFAInvoiceProd2" runat="server" Text="Product 2" Visible="true"></asp:Label>
                <asp:DropDownList ID="ddlSFAInvoiceProduct2" runat="server" Visible="true" Width="100px">
                </asp:DropDownList>
            </div>
            <div class="dropdown_large" id="div4" runat="server" visible="true"
                style="padding-left: 10px">
                <asp:Label ID="lblSFAInvoiceAmount" runat="server" Text="Invoice Amount" Visible="true"></asp:Label>
                <asp:TextBox ID="txtSFAInvoiceAmount" runat="server" Visible="true" Width="120px">
                </asp:TextBox>
            </div>
        </div>
        <div>
            <div class="dropdown_large" id="div_filterBy_brand" runat="server" visible="false"
                style="padding-left: 10px">
                <asp:Label ID="lbl_brand" runat="server" Text="Brand" Visible="true"></asp:Label>
                <asp:DropDownList ID="cmbBrand" runat="server" Visible="true" Width="150px">
                </asp:DropDownList>
            </div>
            <div class="dropdown_large" id="div_filterBy_packingMethod" runat="server" visible="false"
                style="padding-left: 10px">
                <asp:Label ID="lbl_packingMethod" runat="server" Text="Packing Method" Visible="true"></asp:Label>
                <asp:DropDownList ID="cmbPackingMethod" runat="server" Visible="true" Width="150px">
                </asp:DropDownList>
            </div>
            <div class="dropdown_large" id="div_filterBy_itemGroup" runat="server" visible="false"
                style="padding-left: 10px">
                <asp:Label ID="lbl_itemGroup" runat="server" Text="Item Group" Visible="true"></asp:Label>
                <asp:DropDownList ID="cmbItemGroup" runat="server" Visible="true" Width="150px">
                </asp:DropDownList>
            </div>
            <div class="dropdown_large" id="div_filterBy_flavor" runat="server" visible="false"
                style="padding-left: 10px">
                <asp:Label ID="lbl_flavor" runat="server" Text="Flavor" Visible="true"></asp:Label>
                <asp:DropDownList ID="cmbFlavor" runat="server" Visible="true" Width="150px">
                </asp:DropDownList>
            </div>
            <div class="dropdown_large" id="div_filterBy_product" runat="server" visible="false"
                style="padding-left: 10px; width: 205px;">
                <asp:Label ID="lbl_product" runat="server" Text="Product" Visible="true"></asp:Label>
                <asp:DropDownList ID="cmpProduct" runat="server" Visible="true" Width="200px">
                </asp:DropDownList>
            </div>
            <div class="dropdown_large" id="div_filterBy_hvp" runat="server" visible="false"
                style="padding-left: 10px">
                <asp:Label ID="lbl_hvp" runat="server" Text="HVP" Visible="true" Width="120px"></asp:Label>
                <asp:DropDownList ID="cmbHvp" runat="server" Visible="true" Width="100px">
                    <asp:ListItem Text="ALL" Value="ALL" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Yes" Value="yes"></asp:ListItem>
                    <asp:ListItem Text="No" Value="no"></asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="dropdown_large" id="div_filterBy_active_inactive" runat="server" visible="false"
                style="padding-left: 10px">
                <asp:Label ID="Label7" runat="server" Text="Active/Inactive" Visible="true" Width="120px"></asp:Label>
                <asp:DropDownList ID="cmbActiveInactive" runat="server" Visible="true" Width="100px">
                    <asp:ListItem Text="Active" Value="A"></asp:ListItem>
                    <asp:ListItem Text="Inactive" Value="D"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
    </fieldset>
</div>
<div class="clearall">
</div>
<asp:HiddenField ID="HiddenCustCode" runat="server" Value="" ClientIDMode="Static" />
<script type="text/javascript">

    $(document).ready(function () {
        $("#dtpMonth").kendoDatePicker({
            // defines the start view
            start: "year",

            // defines when the calendar should return date
            depth: "year",

            // display month and year in the input
            format: "MMMM yyyy"
        });

        $("#dtpFromDate").kendoDatePicker({
            // display month and year in the input
            format: "dd-MMM-yyyy"
        });

        $("#dtpToDate").kendoDatePicker({
            // display month and year in the input
            format: "dd-MMM-yyyy"
        });
        //SetPeriod();

        $("#btnShowAdvancedOptions").click(function () {
            $("#div_advanced_options").css("display", "block");
            $("#btnHideAdvancedOptions").css("display", "block");
            $("#btnShowAdvancedOptions").css("display", "none");
        });

        $("#btnHideAdvancedOptions").click(function () {
            $("#div_advanced_options").css("display", "none");
            $("#btnShowAdvancedOptions").css("display", "block");
            $("#btnHideAdvancedOptions").css("display", "none");

        });
    });

    function Hide_showAdvancedOptions() {
        $("#btnShowAdvancedOptions").css("display", "none");
    }

    function GetDateString(day, month, year) {
        var monthname = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var stoDate = "";
        if (day <= 9) {
            stoDate = "0"
        }
        stoDate = stoDate + day + "-";
        //        if (month <= 9) {
        //            stoDate = stoDate + "0"
        //        }
        stoDate = stoDate + monthname[month - 1] + "-" + year;
        return stoDate;
    }

    function SetPeriod() {

        var index = $("#cmbDateRange").val();
        var fromDate = new Date();

        var month = fromDate.getMonth() + 1;
        var day = fromDate.getDate();
        var year = fromDate.getFullYear();

        var sfromDate = "";
        var stoDate = GetDateString(day, month, year);

        if (index == 1) {//"Week"
            day = day - 7;
            fromDate = new Date(year, month - 1, day);
        }
        else if (index == 2) {//Month
            month = month - 1;
            fromDate = new Date(year, month - 1, day);
        }
        else if (index == 3) {//3 Months
            month = month - 3;
            fromDate = new Date(year, month - 1, day);
        }
        else if (index == 4) {//6 Months
            month = month - 6;
            fromDate = new Date(year, month - 1, day);
        }
        sfromDate = GetDateString(fromDate.getDate(), fromDate.getMonth() + 1, fromDate.getFullYear());

        $("#dtpFromDate").val(sfromDate);
        $("#dtpToDate").val(stoDate);
    }

    function onChange_SummaryReport() {
        var index = $("#cmbSummaryReport").val();
        //alert(index);
        if (index == "0") {
            $("#div_distributor").css("display", "block");
            $("#div_repgroup").css("display", "none");
        }
        else {
            $("#div_distributor").css("display", "none");
            $("#div_repgroup").css("display", "block");
        }
    }

</script>
