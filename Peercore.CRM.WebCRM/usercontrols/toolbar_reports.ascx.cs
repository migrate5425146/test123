﻿using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usercontrols_toolbar_reports : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (UserSession.Instance.OriginatorString == "DIST")
        {
            a_master_details.Visible = false;
        }
    }
}