﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="contactaddress.aspx.cs" Inherits="usercontrols_contactaddress" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    
<div class="tab_button">
    <div class="addlinkdiv" style="float: left" id="div_address">
        <a id="id_address" runat="server" href="#">Add Address</a>
    </div>
    <div style="float: left; width: 3px">
        &nbsp;</div>
    <div class="maplink" style="float: left" id="div_map">
        <a id="btnMap"  >Map</a>
    </div>

    
    <div class="clearall">
    </div>
</div>
<div style="min-width: 200px; overflow: auto">
    


    <div style="float: left; width: 100%; overflow: auto">
        <div id="LeadCustAddressGrid"></div>
    </div>

</div>

<script type="text/javascript">
    $(document).ready(function () {
        loadLeadAddressGrid();

        var window = $("#window"),
                        undo = $("#undo")
                                .bind("click", function () {
                                    window.data("kendoWindow").open();
                                    undo.hide();
                                });

        var onClose = function () {
            undo.show();
        }

        if (!window.data("kendoWindow")) {
            window.kendoWindow({
                width: "600px",
                title: "Contact Person Entry",
                close: onClose
            });
        }
        $("#window").data("kendoWindow").close();
    });


    $("#btnMap").click(function () {
        loadLeadAddressMap('#LeadCustAddressGrid', $("#MainContent_HiddenFieldLeadId").val(), '');
    });
</script>
    </form>
</body>
</html>
