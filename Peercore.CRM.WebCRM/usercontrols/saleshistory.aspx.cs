﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using Peercore.CRM.Common;
using System.Text;

public partial class usercontrols_saleshistory : PageBase
{
    #region Properties
    private KeyValuePair<string, string> CustomerEntry
    {
        get
        {
            if (Session[CommonUtility.CUSTOMER_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.CUSTOMER_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.CUSTOMER_DATA] = value;
        }
    }
    #endregion

    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        CustomerClient customerClient = new CustomerClient();
        KeyValuePair<string, string> customer_value = CustomerEntry;

        if (!string.IsNullOrEmpty(customer_value.Key))
        {
           // DateTime lastdelivery = customerClient.GetLastDelivery(customer_value.Key);
            int outstandingDeliverry = customerClient.GetOutstandingDeliveriesCount(customer_value.Key);


        //if (lastdelivery != null)
        //{
        //    lbNotDelivered.Text = lastdelivery.ToString("dd-MMM-yyyy");
        //}

        if (outstandingDeliverry != 0)
        {
            lbNonRequired.Text = outstandingDeliverry.ToString();
        }
        }

        //LoadSalesData(Server.UrlDecode(customer_value.Key));
        //GetDeliveryInfo(Server.UrlDecode(customer_value.Key));
    }
    #endregion

    #region Methods
    private void LoadSalesData(string customerCode)
    {
        try
        {
            if (customerCode != null)
            {
                List<EndUserSalesDTO> debtorsBarometerList = null;

                try
                {
                    decimal out_total = 0;

                    if (!string.IsNullOrWhiteSpace(customerCode))
                        debtorsBarometerList = new SalesClient().GetDebtorsDetails(customerCode);

                    if (debtorsBarometerList != null)
                    {
                        if (debtorsBarometerList.Count != 0)
                            debtors_container.InnerHtml = RenderDebtorsSales(customerCode, debtorsBarometerList, out out_total);
                    }

                    lbDebtorsTotal.Text = "Total: " + out_total.ToString();
                }
                catch (Exception)
                {
                    lbDebtorsTotal.Text = "Total: 0";
                }

                try
                {
                    EndUserSalesDTO salesGraphData = null;

                    string date = DateTime.Today.ToString(ConfigUtil.DatePatternForSql);
                    salesGraphData = new SalesClient().GetSalesData(customerCode, date);

                    if (salesGraphData != null)
                        monthSales_container.InnerHtml= RenderSalesGraphs(salesGraphData);

                    lbTotSales.Text += string.Format("{0:0}", salesGraphData.TotalSales);
                    lbSalesSummary.Text += string.Format("{0:0.00}", salesGraphData.SalesSummary);
                }
                catch (Exception)
                {
                    lbTotSales.Text += string.Format("{0:0}", 0);
                    lbSalesSummary.Text += string.Format("{0:0.00}", 0);
                }
            }
            else
            {
                lbDebtorsTotal.Text = "Total: 0";
                lbTotSales.Text += string.Format("{0:0}", 0);
                lbSalesSummary.Text += string.Format("{0:0.00}", 0);
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void GetDeliveryInfo(string customerCode)
    {
        //int iNonRequired = 0;
        //int iNotDelivered = 0;

        //try
        //{
        //    lbDelivery.Text = string.Format("DELIVERY{0}BETWEEN{0}{1}{0}{2}", "<br />", DateTime.Today.AddMonths(-1).ToString("dd-MMM-yyyy"), DateTime.Today.ToString("dd-MMM-yyyy"));
        //    CommonClient common = new CommonClient();
        //    List<CustomerActivitiesDTO> deliveryList = common.GetDeliveryInfo(customerCode);

        //    if (deliveryList != null)
        //    {
        //        foreach (CustomerActivitiesDTO delivery in deliveryList)
        //        {
        //            if (delivery.Status == "C")
        //                iNonRequired += delivery.DelCount;
        //            else
        //                iNotDelivered += delivery.DelCount;
        //        }
        //    }

        //    lbNonRequired.Text = iNonRequired.ToString();
        //    lbNotDelivered.Text = iNotDelivered.ToString();
        //}
        //catch (Exception ex)
        //{
        //}
    }

    private string RenderDebtorsSales(string customerCode, List<EndUserSalesDTO> endUserSalesDTO,out decimal total)
    {
        StringBuilder txt = new StringBuilder();

        #region Setting Data.
        decimal dCur = 0, d30 = 0, d60 = 0, d90 = 0, dOver = 0;
        DateTime Ld_datenow = DateTime.Today;
        DateTime Ld_as_at_date = DateTime.Today;
        DateTime Ld_datethen = DateTime.Today;
        DateTime Ld_doc_date = DateTime.Today;
        TimeSpan Ld_datediff;
        decimal dTotal = 0;
        int Li_TransType = 0;
        string Lv_Status = "";
        decimal Lv_UnallocDiscount = 0;

        foreach (EndUserSalesDTO item in endUserSalesDTO)
        {
            /* ADD DISC IF RCT IS UNALLOCATED */
            if (Li_TransType == 0 && Lv_Status == "")
                item.DueAmount += Lv_UnallocDiscount;

            // FOR LEDGER TYPE - 'MON'
            Ld_datenow = new DateTime(Ld_as_at_date.Year, Ld_as_at_date.Month, 1);

            if (item.GracePeriod != 0)
            {
                //if due_date_rad = 1 THEN 
                Ld_datethen = new DateTime(Ld_doc_date.Year, item.Documentdate.Month, 1);
                Ld_datediff = Ld_datenow - Ld_datethen;

            }
            else
            {
                Ld_datethen = new DateTime(Ld_doc_date.Year, item.Documentdate.Month, 1);
                Ld_datediff = Ld_datenow - Ld_datethen;
            }

            if (Ld_datediff.Days <= 0)
                dCur += item.DueAmount;
            else if (Ld_datediff.Days > 20 && Ld_datediff.Days < 40)
                d30 += item.DueAmount;
            else if (Ld_datediff.Days > 50 && Ld_datediff.Days < 70)
                d60 += item.DueAmount;
            else if (Ld_datediff.Days > 80 && Ld_datediff.Days < 100)
                d90 += item.DueAmount;
            else
                dOver += item.DueAmount;

            dTotal += item.DueAmount;
        }
        total = dTotal;
        #endregion

        txt.Append("<script language=\"javascript\" type=\"text/javascript\">");
        txt.Append(" var chart;");
        txt.Append(" $(document).ready(function () {");

        txt.Append(" chart = new Highcharts.Chart({");
        txt.Append(" chart: {");
        txt.Append(" renderTo: 'debtors_container',");
        txt.Append(" type: 'column'");
        txt.Append(" },");
        txt.Append(" title: {");
        txt.Append(" text: ''");
        txt.Append(" },");
        txt.Append(" xAxis: {");

        txt.Append(" title: {");
        txt.Append(" text: 'Days'");
        txt.Append(" },");

        txt.Append(" categories: ['CUR', '30', '60', '90', 'OVER']");
        txt.Append(" },");
        txt.Append(" yAxis: {");

        txt.Append(" min: 0,");
        txt.Append(" title: {");
        txt.Append(" text: 'Outstanding$'");
        txt.Append(" },");
        txt.Append(" stackLabels: {");
        txt.Append(" enabled: true,");
        txt.Append(" style: {");
        txt.Append(" fontWeight: 'bold',");
        txt.Append(" color: 'gray',");
        txt.Append(" backgroundColor: 'red'");
        txt.Append(" }");

        txt.Append(" }");
        txt.Append(" },");
        txt.Append(" legend: {");
        txt.Append(" enabled: false");
        txt.Append(" },");
        txt.Append(" tooltip: {");
        txt.Append(" formatter: function () {");
        txt.Append(" return '' +");
        txt.Append(" this.x + ': ' + Highcharts.numberFormat(this.y, 2);");
        txt.Append(" }");
        txt.Append(" },");
        txt.Append(" plotOptions: {");
        txt.Append(" column: {");
        txt.Append(" pointPadding: 0.2,");
        txt.Append(" borderWidth: 0");
        txt.Append(" }");
        txt.Append(" },");
        txt.Append(" series: [{");
        txt.Append(" name: 'Salesqq',");
        txt.Append(" data: [{ y: " + (dCur != null ? dCur : 0) + ", color: '#4E9258' }, { y: " + (d30 != null ? d30 : 0)
            + ", color: '#4E9258' }, { y: " + (d60 != null ? d60 : 0) + ", color: '#FF6600' }");
        txt.Append(" , { y: " + (d90 != null ? d90 : 0) + ", color: '#FF6600' }, { y: " + (dOver != null ? dOver : 0) + ", color: '#FF6600'}],");
        txt.Append(" dataLabels: {");
        txt.Append(" enabled: false, rotation: -90, color: '#FFFFFF', align: 'right', x: -3, y: 10,");
        txt.Append(" formatter: function () {");
        txt.Append(" return this.y;");
        txt.Append(" },");
        txt.Append(" style: {");
        txt.Append(" fontSize: '13px',");
        txt.Append(" fontFamily: 'Verdana, sans-serif'");
        txt.Append(" }");
        txt.Append(" }");
        txt.Append(" }]");
        txt.Append(" });");
        txt.Append(" });");
        txt.Append(" ");
        txt.Append(" ");

        txt.Append("</script>");
        return txt.ToString();
    }

    private string RenderSalesGraphs(EndUserSalesDTO endUserSalesDTO)
    {
        StringBuilder txt = new StringBuilder();
        txt.Append("<script language=\"javascript\" type=\"text/javascript\">");
        txt.Append(" var chart;");
        txt.Append(" $(document).ready(function () {");

        txt.Append(" chart = new Highcharts.Chart({");
        txt.Append(" chart: {");
        txt.Append(" renderTo: 'monthSales_container',");
        txt.Append(" type: 'column'");
        txt.Append(" },");
        txt.Append(" title: {");
        txt.Append(" text: ''");
        txt.Append(" },");
        txt.Append(" xAxis: {");
        txt.Append(" categories: ['" + endUserSalesDTO.Tonnes1 + "', '" + endUserSalesDTO.Tonnes2 + "', '" + endUserSalesDTO.Tonnes3 + "']");
        txt.Append(" },");
        txt.Append(" yAxis: {");

        txt.Append(" min: 10,");
        txt.Append(" title: {");
        txt.Append(" text: 'Sales (Litres)'");
        txt.Append(" },");
        txt.Append(" stackLabels: {");
        txt.Append(" enabled: true,");
        txt.Append(" style: {");
        txt.Append(" fontWeight: 'bold',");
        txt.Append(" color: 'gray',");
        txt.Append(" backgroundColor: 'red'");
        txt.Append(" }");

        txt.Append(" }");
        txt.Append(" },");
        txt.Append(" legend: {");
        txt.Append(" enabled: false");
        txt.Append(" },");
        txt.Append(" tooltip: {");
        txt.Append(" formatter: function () {");
        txt.Append(" return '' +");
        txt.Append(" this.x + ': ' + Highcharts.numberFormat(this.y, 2);");
        txt.Append(" }");
        txt.Append(" },");
        txt.Append(" plotOptions: {");
        txt.Append(" column: {");
        txt.Append(" pointPadding: 0.2,");
        txt.Append(" borderWidth: 0");
        txt.Append(" }");
        txt.Append(" },");
        txt.Append(" series: [{");
        txt.Append(" name: 'Salesqq',");
        txt.Append(" data: [{ y: " + endUserSalesDTO.Tonnes1 + ", color: '#82CAFA' }, { y: " + endUserSalesDTO.Tonnes2
            + ", color: '#82CAFA' }, { y: " + endUserSalesDTO.Tonnes3 + ", color: '#82CAFA' }");
        txt.Append("  ],");
        txt.Append(" dataLabels: {");
        txt.Append(" enabled: false, rotation: -90, color: '#FFFFFF', align: 'right', x: -3, y: 10,");
        txt.Append(" formatter: function () {");
        txt.Append(" return this.y;");
        txt.Append(" },");
        txt.Append(" style: {");
        txt.Append(" fontSize: '13px',");
        txt.Append(" fontFamily: 'Verdana, sans-serif'");
        txt.Append(" }");
        txt.Append(" }");
        txt.Append(" }]");
        txt.Append(" });");
        txt.Append(" });");
        txt.Append(" ");
        txt.Append(" ");
        txt.Append("</script>");

        return txt.ToString();
    }
    #endregion
}