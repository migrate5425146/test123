﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;

public partial class usercontrols_activity : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        ArgsDTO argsDTO = (ArgsDTO)(Session[CommonUtility.GLOBAL_SETTING]);
        string LStageName = argsDTO.LeadStage.ToString();

        if (Request.QueryString["leadId"] != null && Request.QueryString["leadId"] != string.Empty)
        {
            id_leadactivity.HRef = ConfigUtil.ApplicationPath + "activity_planner/transaction/activityentry.aspx?leadId=" + Request.QueryString["leadId"].ToString()+"&fm=lead"+ ((!string.IsNullOrEmpty(LStageName))?"&ty="+LStageName:"");
        }
        else if (Session[CommonUtility.LEAD_DATA] != null)
        {
            KeyValuePair<string, string> LeadEntry = (KeyValuePair<string, string>)Session[CommonUtility.LEAD_DATA];
            id_leadactivity.HRef = ConfigUtil.ApplicationPath + "lead_customer/transaction/contact_person_entry.aspx?leadid=" + LeadEntry.Key;
        }
    }
}