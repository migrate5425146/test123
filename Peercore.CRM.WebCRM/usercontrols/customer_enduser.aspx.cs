﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Common;

public partial class usercontrols_customer_enduser : PageBase
{
    #region Properties
    private KeyValuePair<string, string> CustomerEntry
    {
        get
        {
            if (Session[CommonUtility.CUSTOMER_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.CUSTOMER_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.CUSTOMER_DATA] = value;
        }
    }
    #endregion Properties

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(CustomerEntry.Key))
        {
            HiddenFieldEndUser_CustCode.Value = CustomerEntry.Key;
            HiddenFieldFormType.Value = Request.QueryString["ty"].ToString();
            id_enduser.HRef = ConfigUtil.ApplicationPath + "lead_customer/transaction/enduser_entry.aspx?custid=" + Server.UrlEncode(CustomerEntry.Key);
            id_transferenduser.HRef = "javascript:transferenduser('lead_customer/process_forms/processmaster.aspx?fm=custenduser&type=Update&custid=" + Server.UrlEncode(CustomerEntry.Key) + "')";
        }
        else
        {
            id_enduser.HRef = "javascript:void('0')";
            id_transferenduser.HRef = "javascript:void('0')";
        }
    }
}