﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Common;
using CRMServiceReference;
using Peercore.CRM.Shared;

public partial class usercontrols_customer_contactdetails : PageBase
{
    #region Properties
    private KeyValuePair<string, string> CustomerEntry
    {
        get
        {
            if (Session[CommonUtility.CUSTOMER_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.CUSTOMER_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.CUSTOMER_DATA] = value;
        }
    }
    #endregion Properties

    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        LoadContactDetails();
        DisableControls();
    }
    #endregion

    #region Methods
    private void LoadContactDetails()
    {
        // Load the preferred contact
        #region Args Setting
        ArgsDTO args = new ArgsDTO();
        args.ChildOriginators = UserSession.Instance.ChildOriginators;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.Originator = UserSession.Instance.UserName;
        
        #endregion
        CommonClient oLookupTable = new CommonClient();
        List<LookupTableDTO> preferredMethodList = new List<LookupTableDTO>();
        try
        {
            preferredMethodList = oLookupTable.GetLookupTables("CONT", args);

            //ddlPreferredMethod.DataSource = preferredMethodList;
            //ddlPreferredMethod.DataTextField = "TableDescription";
            //ddlPreferredMethod.DataValueField = "TableCode";
            //ddlPreferredMethod.DataBind();

            foreach (LookupTableDTO item in preferredMethodList)
            {
                if (item.DefaultValue == "Y")
                {
                   // ddlPreferredMethod.SelectedValue = item.TableCode; break;
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    private void SetContactDetails()
    {
        //KeyValuePair<string, string> CustomerEntry = (KeyValuePair<string, string>)Session[CommonUtility.CUSTOMER_DATA];
        //CustomerClient oCustomer = new CustomerClient();
        //CustomerDTO customer = oCustomer.GetCustomer(Server.UrlDecode(CustomerEntry.Key));

        //if (!string.IsNullOrWhiteSpace(customer.Fax))
        //{
        //    mtxtFax.Text = customer.Fax.Replace(" ", "").Trim();
        //}

        //if (!string.IsNullOrWhiteSpace(customer.Telephone))
        //{
        //    mtxtPhone.Text = customer.Telephone.Replace(" ", "").Trim();
        //}

        //if (!string.IsNullOrWhiteSpace(customer.Mobile))
        //{
        //    mtxtMobile.Text = customer.Mobile.Replace(" ", "").Trim();
        //}

        //if (!string.IsNullOrWhiteSpace(customer.Email))
        //{
        //    txtEmail.Text = customer.Email;
        //}

        //if (!string.IsNullOrWhiteSpace(customer.Website))
        //{
        //    txtWebsite.Text = customer.Website;
        //}

        //if (!string.IsNullOrWhiteSpace(customer.PreferredContact))
        //{
        //    ddlPreferredMethod.SelectedValue = customer.PreferredContact;
        //}
    }

    private void DisableControls()
    {
        txtcustAddress1.Enabled = false;
      //  txtcustAddress2.Enabled = false;
        txtCustCity.Enabled = false;
   //     txtCustState.Enabled = false;
    //    txtCustPostcode.Enabled = false;
        mtxtPhone.Enabled = false;
     //   mtxtMobile.Enabled = false;
     //   mtxtFax.Enabled = false;
      //  txtWebsite.Enabled = false;
      //  txtEmail.Enabled = false;
      //  ddlPreferredMethod.Enabled = false;
    }
    #endregion
}