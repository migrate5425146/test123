﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="sales_history_details.ascx.cs" Inherits="usercontrols_sales_history_details" %>
<%--<div class="selected">
     Business Sales History Details - This Year
</div>--%>
<div class="toolbar3" id="lblSalesHistoryFlow">
     Month : FEB | text1 | text2
</div>
<div>
    <ul id="panelbarSalesHistory">
        <li class="k-state-active" id="7">Month Totals
            <div id="gridSalesHistoryMonth">sdf
            </div>
        </li>

        <li id="8">Year Totals
            <div id="gridSalesHistoryYear">
            sdf
            </div>
        </li>
        <li id="9">Graph
            <div>
                <asp:DropDownList ID="DropDownGraphTypeSalesHistory" runat="server" Width="180px">
                    <asp:ListItem Value="Sales" Text="Sales" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="Tonnes" Text="Tonnes"></asp:ListItem>
                    <asp:ListItem Value="Sales/Tonne" Text="Sales/Tonne" ></asp:ListItem>
                    <asp:ListItem Value="Sales vs Budget ($)" Text="Sales vs Budget ($)" ></asp:ListItem>
                    <asp:ListItem Value="Sales vs Budget (T)" Text="Sales vs Budget (T)" ></asp:ListItem>
                    <asp:ListItem Value="Last Sales vs Budget ($)" Text="Last Sales vs Budget ($)" ></asp:ListItem>
                    <asp:ListItem Value="Last Sales vs Budget (T)" Text="Last Sales vs Budget (T)" ></asp:ListItem>
                </asp:DropDownList>   
                
                <asp:RadioButton runat="server" ID="rbTrendGraphAccumulateSalesHistory" ValidationGroup="A" GroupName="A" Checked="true" /><span>Accumulated</span>
                <asp:RadioButton runat="server" ID="rbTrendGraphMonthlySalesHistory" ValidationGroup="A" GroupName="A" /><span>Monthly</span>
                             
            </div>
            <div id="div_chart_SalesHistory">
            </div>
        </li>
    </ul>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        //SetSalesHistoryFlow();
        $("#lblSalesHistoryFlow").html($("#MainContent_custsalesinfo_lblCurrentFlow").text());
        $("#panelbarSalesHistory").kendoPanelBar({
            expandMode: "single",
            select: onSelect
        });

        function onSelect(e) {
            var item = $(e.item)[0].id;
            if (item != undefined && item != '') {
                switch (item) {
                    case "7":
                        loadSalesHistoryMonth("gridSalesHistoryMonth", $("#MainContent_custsalesinfo_txtSelection").val());
                        break;
                    case "8":
                        loadSalesHistoryYear("gridSalesHistoryYear", $("#MainContent_custsalesinfo_txtSelection").val());
                        break;
                    case "9":
                        LoadSalesInfoTrendChart($("#MainContent_custsalesinfo_txtSelection").val(), "cust_sh");
                        break;
                }
            }
        }
    });

$("#MainContent_custsalesinfo_salesHistoryDetails_DropDownGraphTypeSalesHistory").change(function () {
    LoadSalesInfoTrendChart($("#MainContent_custsalesinfo_txtSelection").val(), "cust_sh");
});

$("#MainContent_custsalesinfo_salesHistoryDetails_rbTrendGraphAccumulateSalesHistory").change(function () {
    LoadSalesInfoTrendChart($("#MainContent_custsalesinfo_txtSelection").val(), "cust_sh");
});

$("#MainContent_custsalesinfo_salesHistoryDetails_rbTrendGraphMonthlySalesHistory").change(function () {
    LoadSalesInfoTrendChart($("#MainContent_custsalesinfo_txtSelection").val(), "cust_sh");
});

function SetSalesHistoryFlow() {
    var detailType = $("#MainContent_custsalesinfo_HiddenFieldDetailType");
    var description = $("#MainContent_custsalesinfo_HiddenFieldDescription");
    var code = $("#MainContent_custsalesinfo_txtSelection")
    var text = "";alert(detailType);
    switch(detailType){
        case 'bus area':
            text = '<span class=\"tlbcolor\">Bus Area : </span>' + description + ' <span class=\"rnb\"> &raquo; </span>';
            break;
        case 'market':
            text = '<span class=\"tlbcolor\"> Market : </span>' + description + ' <span class=\"rnb\"> &raquo; </span>';
            break;
        case 'brand':
             text = '<span class=\"tlbcolor\"> Brand : </span>' + description + ' <span class=\"rnb\"> &raquo; </span>';
            break;
        case 'cat_group':
            text = '<span class=\"tlbcolor\"> Cat Group : </span>' + description + ' <span class=\"rnb\"> &raquo; </span>';
            break;
        case 'cat_sub_group':
             text = '<span class=\"tlbcolor\"> Sub Group : </span>' + description + ' <span class=\"rnb\"> &raquo; </span>';
            break;
        case 'product':
             text = '<span class=\"tlbcolor\"> Product : </span>' + description + ' <span class=\"rnb\"> &raquo; </span>';
            break;
        case 'customer':
             text = '<span class=\"tlbcolor\"> Customer : </span>' + description + ' <span class=\"rnb\"> &raquo; </span>';
            break;
        case 'rep':
             text = '<span class=\"tlbcolor\"> Rep : </span>' + description + ' <span class=\"rnb\"> &raquo; </span>';
            break;
    }
    $("#lblSalesHistoryFlow").html($("#MainContent_custsalesinfo_lblCurrentFlow").text() );
}


function loadSalesHistoryMonth(targetgrid, code) {
    $("#" + targetgrid).html("");
    $("#" + targetgrid).kendoGrid({
        height: 450,
        columns: [
            { field: "Period", title: "Period" },
            { field: "Tonne", title: "(T)", headerTemplate: "<div style =\"text-align: right;\">(T)</div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
            { field: "BudgetTonne", title: "Bgt-T", headerTemplate: "<div style =\"text-align: right;\">Bgt-T</div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
            { field: "Sales", title: "Sales", headerTemplate: "<div style =\"text-align: right;\">Sales</div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
            { field: "BudgetSales", title: "Bgt-$", headerTemplate: "<div style =\"text-align: right;\">Bgt-$</div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
            { field: "ST", title: "S/T", headerTemplate: "<div style =\"text-align: right;\">S/T</div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
            { field: "Profit", title: "Profit", headerTemplate: "<div style =\"text-align: right;\">Profit</div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
            { field: "PT", title: "P/T", headerTemplate: "<div style =\"text-align: right;\">P/T</div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
            { field: "Percentage", title: "%P", headerTemplate: "<div style =\"text-align: right;\">%P</div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
            { field: "LastYearTonne", title: "LY (T)", headerTemplate: "<div style =\"text-align: right;\">LY (T)</div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
            { field: "LastYearBudgetTonne", title: "LY Bgt-T", headerTemplate: "<div style =\"text-align: right;\">LY Bgt-T</div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
            { field: "LastYearSales", title: "LY Sales", headerTemplate: "<div style =\"text-align: right;\">LY Sales</div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
            { field: "LastYearBudgetSales", title: "LY Bgt-$", headerTemplate: "<div style =\"text-align: right;\">LY Bgt-$</div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
            { field: "LastYearST", title: "LY S/T", headerTemplate: "<div style =\"text-align: right;\">LY S/T</div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
            { field: "LastYearProfit", title: "LY Profit", headerTemplate: "<div style =\"text-align: right;\">LY Profit</div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
            { field: "LastYearPT", title: "LY P/T", headerTemplate: "<div style =\"text-align: right;\">LY P/T</div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
            { field: "LastYearPercentage", title: "LY %P", headerTemplate: "<div style =\"text-align: right;\">LY %P</div>", format: "{0:n0}", attributes: { style: "text-align: right"} }

    ],
    editable: false, // enable editing
    pageable: true,
    sortable: true,
    //filterable: true,
    selectable: "single",
    dataSource: {
        serverSorting: true,
        serverPaging: true,
        serverFiltering: true,
        pageSize: 12,
        schema: {
            data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
            total: "d.Total",
            model: { // define the model of the data source. Required for validation and property types.
                //id: "SourceId",
                fields: {
                    Period: { validation: { required: true} },
                    Tonne: { type: "number", validation: { required: true} },
                    BudgetTonne: { type: "number", validation: { required: true} },
                    Sales: { type: "number", validation: { required: true} },
                    BudgetSales: { type: "number", validation: { required: true} },

                    ST: { type: "number", validation: { required: true} },
                    Profit: { type: "number", validation: { required: true} },
                    PT: { type: "number", validation: { required: true} },
                    Percentage: { type: "number", validation: { required: true} },

                    LastYearTonne: { type: "number", validation: { required: true} },
                    LastYearBudgetTonne: { type: "number", validation: { required: true} },
                    LastYearSales: { type: "number", validation: { required: true} },
                    LastYearBudgetSales: { type: "number", validation: { required: true} },

                    LastYearST: { type: "number", validation: { required: true} },
                    LastYearProfit: { type: "number", validation: { required: true} },
                    LastYearPT: { type: "number", validation: { required: true} },
                    LastYearPercentage: { type: "number", validation: { required: true} }
                }
            }
        },
        transport: {

            read: {
                url: ROOT_PATH + "Service/lead_customer/common.asmx/GetSalesHistoryMonth", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                data: { code: code },
                type: "POST"// ,//use HTTP POST request as the default GET is not allowed for ASMX
//                complete: function (jqXHR, textStatus) {
//                    if (textStatus == "success") {
//                        $("#div_sales_window").data("kendoWindow").open();
//                        $("div#div_loader").hide();
//                    }
//                }
            },
            parameterMap: function (data, operation) {
                if (operation != "read") {
                    return JSON.stringify({ products: data.models })
                } else {
                    data = $.extend({ sort: null, filter: null }, data);
                    return JSON.stringify(data);
                }
            }
        }
    }
});
}

function loadSalesHistoryYear(targetgrid, code) {
    $("#" + targetgrid).html("");
    $("#" + targetgrid).kendoGrid({
        height: 200,
        columns: [
            { field: "Header", title: "Header" },
            { field: "CurrentYear", title: "Current Year", headerTemplate: "<div style =\"text-align: right;\">Current Year</div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
            { field: "LastYear", title: "Last Y.T.D", headerTemplate: "<div style =\"text-align: right;\">Last Y.T.D</div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
            { field: "Year1", title: "Year 1", headerTemplate: "<div style =\"text-align: right;\">Year 1</div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
            { field: "Year2", title: "Year 2", headerTemplate: "<div style =\"text-align: right;\">Year 2</div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
            { field: "Year3", title: "Year 3", headerTemplate: "<div style =\"text-align: right;\">Year 3</div>", format: "{0:n0}", attributes: { style: "text-align: right"} },
            { field: "Year4", title: "Year 4", headerTemplate: "<div style =\"text-align: right;\">Year 4</div>", format: "{0:n0}", attributes: { style: "text-align: right"} }

    ],
        editable: false, // enable editing
        pageable: true,
        sortable: true,
        //filterable: true,
        selectable: "single",
        dataSource: {
            serverSorting: true,
            serverPaging: true,
            serverFiltering: true,
            pageSize: 10,
            schema: {
                data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                total: "d.Total",
                model: { // define the model of the data source. Required for validation and property types.
                    //id: "SourceId",
                    fields: {
                        Header: { validation: { required: true} },
                        CurrentYear: { type: "number", validation: { required: true} },
                        LastYTD: { type: "number", validation: { required: true} },
                        Year1: { type: "number", validation: { required: true} },
                        Year2: { type: "number", validation: { required: true} },

                        Year3: { type: "number", validation: { required: true} },
                        Year4: { type: "number", validation: { required: true} }
                        
                    }
                }
            },
            transport: {

                read: {
                    url: ROOT_PATH + "Service/lead_customer/common.asmx/GetSalesHistoryYear", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                    contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                    data: { code: code },
                    type: "POST" //use HTTP POST request as the default GET is not allowed for ASMX
                },
                parameterMap: function (data, operation) {
                    if (operation != "read") {
                        return JSON.stringify({ products: data.models })
                    } else {
                        data = $.extend({ sort: null, filter: null }, data);
                        return JSON.stringify(data);
                    }
                }
            }
        }
    });
}
</script>

