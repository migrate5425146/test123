﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Common;

public partial class usercontrols_enduser_contactperson : PageBase
{
    #region Properties
    private KeyValuePair<string, string> EndUserEntry
    {
        get
        {
            if (Session[CommonUtility.ENDUSER_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.ENDUSER_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.ENDUSER_DATA] = value;
        }
    }
    #endregion Properties

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((!string.IsNullOrWhiteSpace(EndUserEntry.Key))
            && (Request.QueryString["custid"] != null && Request.QueryString["custid"] != string.Empty))
        {
            HiddenFieldContactPerson_CustCode.Value = Request.QueryString["custid"].ToString();
            HiddenFieldContactPerson_EnduserCode.Value = EndUserEntry.Key;

            id_contactperson.HRef = ConfigUtil.ApplicationPath + "lead_customer/transaction/contact_person_entry.aspx?eduid=" + Server.UrlEncode(EndUserEntry.Key) + "&custid=" + Server.UrlEncode(HiddenFieldContactPerson_CustCode.Value);
        }
        else
        {
            id_contactperson.HRef = "javascript:void('0')";
        }
    }
}