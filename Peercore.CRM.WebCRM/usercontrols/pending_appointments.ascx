﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="pending_appointments.ascx.cs" Inherits="usercontrols_pending_appointments" %>
<div id="div_content_pending_appointments" runat="server" class="divhidden">
        <div class="demo-section">
            <div id="listView_pending_appointments">
            </div>
            <div id="pager_pending_appointments" class="k-pager-wrap"  style="display:none">
            </div>
        </div>
         <script type="text/x-kendo-tmpl" id="appointments_pending_template">
              <div class="product">
                <div class="container">
                    <h3><a href='activity_planner/transaction/activity_scheduler.aspx?fm=home&sdate=#:kendo.toString(StartTime, "yyyy/MMM/dd" )#'>#:Subject#</a></h3>
                    <p><span>From :</span> #:kendo.toString(StartTime, DATETIME_FORMAT )# -  #:kendo.toString(EndTime, DATETIME_FORMAT )#</p>
                </div>
            </div>
        </script>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            AppointmentsActivityPending();
        });
    </script>