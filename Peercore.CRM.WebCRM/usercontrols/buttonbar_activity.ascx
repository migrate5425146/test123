﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="buttonbar_activity.ascx.cs" Inherits="usercontrols_buttonbar_activity" %>
<div class="buttonbar">
    <asp:Button ID="buttinSave" runat="server" OnClick="buttonSave_Click" CssClass="savebtn" Text="Save"/>
    <asp:Button ID="buttonClear" runat="server" OnClick="buttonClear_Click" CssClass="clearbtn" Text="Clear" />
    <asp:Button ID="buttonSendtocalendar" runat="server" Text="Send to calendar" OnClick="buttonSendtocalendar_Click" CssClass="sendtocalbtn" />
    <asp:Button ID="buttonSaveAndSendtocalendar" runat="server" Text="Save & Send to calendar" OnClick="buttonSaveAndSendtocalendar_Click" CssClass="saveandsendcal" />
    <asp:Button ID="buttonOpportunity" runat="server" Text="Add Opportunity" OnClick="buttonOpportunity_Click" CssClass="opportunityadd" ValidationGroup="l" />
</div>