﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Common;

public partial class usercontrols_enduser_activity : PageBase
{
    #region Properties
    private KeyValuePair<string, string> EndUserEntry
    {
        get
        {
            if (Session[CommonUtility.ENDUSER_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.ENDUSER_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.ENDUSER_DATA] = value;
        }
    }
    #endregion Properties

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((!string.IsNullOrWhiteSpace(EndUserEntry.Key))
            && (Request.QueryString["custid"] != null && Request.QueryString["custid"] != string.Empty))
        {
            HiddenFieldActivity_CustCode.Value = Server.UrlEncode(Request.QueryString["custid"].ToString());
            HiddenFieldActivity_EnduserCode.Value = EndUserEntry.Key;

            id_activity.HRef = ConfigUtil.ApplicationPath + "activity_planner/transaction/activityentry.aspx?eduid=" + Server.UrlEncode(EndUserEntry.Key) + "&custid=" + Server.UrlEncode(HiddenFieldActivity_CustCode.Value);
        }
        else
        {
            id_activity.HRef = "javascript:void('0')";
        }
    }
}