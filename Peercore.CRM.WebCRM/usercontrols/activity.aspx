﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="activity.aspx.cs" Inherits="usercontrols_activity" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>


</head>
<body>
    <form id="form1" runat="server">
        <div class="tab_button">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="width:40%" align="left">
                <div class="addlinkdiv" style="width: 75px">
                    <a id="id_leadactivity" runat="server">Add Activity</a>
                </div>
            </td>
            <td align="right">
                <table border="0" width="140px">
                    <tr>
                        <td>
                            <div style="width: 15px; height: 15px; float: right; background-color: #d8bfd8; border: 1px solid #000">
                            </div>
                        </td>
                        <td>
                            <strong>Sent To Calendar</strong>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div class="clearall">
    </div>
</div>
<div style="min-width: 200px; overflow: auto">
<div id="LeadActivityGrid"></div>
    
</div>


<script type="text/javascript">

    $(document).ready(function () {
        loadLeadActivityGrid('LeadActivityGrid');
    });

    
            
</script>
    </form>
</body>
</html>
