﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="contactdetails.aspx.cs" Inherits="usercontrols_contactdetails" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="window">
        </div>
    <div style="min-width: 200px; overflow: auto">
    <div>
        <div class="formleft">

            <div class="formtextdiv">Address</div>
            <div class="formdetaildivlead"> :
                <asp:TextBox ID="txtCustAddress" runat="server" onblur="settingucontrolvalues()" Style="text-transform: uppercase"></asp:TextBox>
            </div>
            <div class="clearall"></div>

            <div class="formtextdiv">City</div>
            <div class="formdetaildivlead"> :
                <asp:TextBox ID="txtCustCity" runat="server" onblur="settingucontrolvalues()" Style="text-transform: uppercase"></asp:TextBox>
            </div>
            <div class="clearall"></div>

            <div class="formtextdiv">State</div>
            <div class="formdetaildivlead"> :
                <asp:TextBox ID="txtCustState" runat="server" onblur="settingucontrolvalues()" MaxLength="4" Style="text-transform: uppercase"></asp:TextBox>
            </div>
            <div class="clearall"></div>
            <div class="formtextdiv">Postcode</div>
            <div class="formdetaildivlead"> :
                <asp:TextBox ID="txtCustPostcode" runat="server" onblur="settingucontrolvalues()" MaxLength="4"></asp:TextBox>
            </div>
            <div class="clearall"></div>
            
        </div>

        <div class="formright">
             <div class="formtextdiv">Phone</div>
            <div class="formdetaildivlead"> :
                <asp:TextBox ID="mtxtPhone" runat="server" onblur="settingucontrolvalues()" MaxLength="14" CssClass="input_numeric_tp"></asp:TextBox>
            </div>
            <div class="clearall"></div>

            <div class="formtextdiv">Mobile</div>
            <div class="formdetaildivlead"> :
                <asp:TextBox ID="mtxtMobile" runat="server" onblur="settingucontrolvalues()" MaxLength="14" CssClass="input_numeric_tp"></asp:TextBox>
            </div>
            <div class="clearall"></div>

            <div class="formtextdiv">Fax</div>
            <div class="formdetaildivlead"> :
                <asp:TextBox ID="mtxtFax" runat="server" onblur="settingucontrolvalues()" MaxLength="14" CssClass="input_numeric_tp"></asp:TextBox>
            </div>
            <div class="clearall"></div>

            <div class="formtextdiv">Website</div>
            <div class="formdetaildivlead"> :
                <asp:TextBox ID="txtWebsite" runat="server" onblur="settingucontrolvalues()"></asp:TextBox>
            </div>
            <div class="clearall"></div>
              
            <div class="formtextdiv">Email</div>
            <div class="formdetaildivlead"> :
                <span class="wosub mand">
                <asp:TextBox ID="txtEmail" runat="server"  ></asp:TextBox>
                </span>
            </div>
            <div class="clearall"></div>

            <div class="formtextdiv">Pref. Method</div>
            <div class="formdetaildivlead"> :
                <asp:DropDownList ID="ddlPreferredMethod" runat="server" Width="160"  onblur="settingucontrolvalues()" />
            </div>
            <div class="clearall"></div>
            
           
            
           
        </div>
    </div>

</div>
    </form>

    <script type="text/javascript">

        $(document).ready(function () {

            loadcontactdetail();

//            $("#mtxtPhone").kendoNumericTextBox({
//                format: "#"
//            });

//            $("#mtxtMobile").kendoNumericTextBox({
//                format: "#"
//            });

//            $("#mtxtFax").kendoNumericTextBox({
//                format: "#"
//            });
            //$("#txtEmail").kendoValidator().data("kendoValidator");
        });

        jQuery(function ($) {
            $('input.input_numeric_tp').autoNumeric({ aSep: null, aDec: null, mDec: 0 });
        });

        $("#txtEmail").blur(function () {
            settingucontrolvalues();
            
            if ($("#txtEmail").val() != null && $("#txtEmail").val().length > 0) {
                jQuery("#txtEmail").validate({
                    expression: "if (isFill(SelfID))" +
                                " {" +
	                        "        if (VAL.match(/^[^\\W][a-zA-Z0-9\\_\\-\\.]+([a-zA-Z0-9\\_\\-\\.]+)*\\@[a-zA-Z0-9_]+(\\.[a-zA-Z0-9_]+)*\\.[a-zA-Z]{2,4}$/))" +
		                    "            return true; " +
	                        "        else " +
		                        "           return false; " +
                                "   } else {return true; }",
                    message: "Please enter valid Email id"
                });
            } else {
                jQuery("#txtEmail").validate({
                    onsubmit: true
                });
            }
        });
        

        function loadcontactdetail() {
//            alert($("#txtCustPostcode").val())
//            document.getElementById("txtCustPostcode").value = document.getElementById("MainContent_HiddenFieldCustPostcode").value;

            document.getElementById("mtxtPhone").value = document.getElementById("MainContent_HiddenFieldContactPhone").value;
            document.getElementById("mtxtMobile").value = document.getElementById("MainContent_HiddenFieldContactMobile").value;
            document.getElementById("mtxtFax").value = document.getElementById("MainContent_HiddenFieldContactFax").value;
            document.getElementById("txtWebsite").value = document.getElementById("MainContent_HiddenFieldContactWebSite").value;
            document.getElementById("txtEmail").value = document.getElementById("MainContent_HiddenFieldContactEmail").value;
            document.getElementById("ddlPreferredMethod").value = document.getElementById("MainContent_HiddenFieldContactPrefMethod").value;

            document.getElementById("txtCustAddress").value = document.getElementById("MainContent_HiddenFieldCustAddress").value;
            document.getElementById("txtCustCity").value = document.getElementById("MainContent_HiddenFieldCustCity").value;
            document.getElementById("txtCustState").value = document.getElementById("MainContent_HiddenFieldCustState").value;
            document.getElementById("txtCustPostcode").value = document.getElementById("MainContent_HiddenFieldCustPostcode").value;
        }
        function settingucontrolvalues() {
            document.getElementById("MainContent_HiddenFieldContactPhone").value = document.getElementById("mtxtPhone").value.replace(/[\!\#\$\%\^\*\(\)\"'"\''\~\`]*/g, "");
            document.getElementById("MainContent_HiddenFieldContactMobile").value = document.getElementById("mtxtMobile").value.replace(/[\!\#\$\%\^\*\(\)\"'"\''\~\`]*/g, "");
            document.getElementById("MainContent_HiddenFieldContactFax").value = document.getElementById("mtxtFax").value.replace(/[\!\#\$\%\^\*\(\)\"'"\''\~\`]*/g, "");
            document.getElementById("MainContent_HiddenFieldContactWebSite").value = document.getElementById("txtWebsite").value;
            document.getElementById("MainContent_HiddenFieldContactEmail").value = document.getElementById("txtEmail").value;
            document.getElementById("MainContent_HiddenFieldContactPrefMethod").value = document.getElementById("ddlPreferredMethod").value;

            document.getElementById("MainContent_HiddenFieldCustAddress").value = document.getElementById("txtCustAddress").value;
            document.getElementById("MainContent_HiddenFieldCustCity").value = document.getElementById("txtCustCity").value;
            document.getElementById("MainContent_HiddenFieldCustState").value = document.getElementById("txtCustState").value;
            document.getElementById("MainContent_HiddenFieldCustPostcode").value = document.getElementById("txtCustPostcode").value;
        }
</script>
</body>
</html>
