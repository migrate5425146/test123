﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Common;

public partial class usercontrols_enduser_price : PageBase
{
    #region Properties
    private KeyValuePair<string, string> EndUserEntry
    {
        get
        {
            if (Session[CommonUtility.ENDUSER_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.ENDUSER_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.ENDUSER_DATA] = value;
        }
    }
    #endregion Properties

    protected void Page_Load(object sender, EventArgs e)
    {
        if ((!string.IsNullOrWhiteSpace(EndUserEntry.Key))
            && (Request.QueryString["custid"] != null && Request.QueryString["custid"] != string.Empty))
        {
            HiddenFieldEndUser_CustCode.Value = Server.UrlEncode(Request.QueryString["custid"].ToString());
            HiddenFieldEndUser_EndUsercode.Value = Server.UrlEncode(EndUserEntry.Key);

            id_pricelist.HRef = ConfigUtil.ApplicationPath + "lead_customer/transaction/pricelist_entry.aspx?eduid=" + Server.UrlEncode(EndUserEntry.Key) +
                "&custid=" + Server.UrlEncode(HiddenFieldEndUser_CustCode.Value) + "&effecdate=" + div_Effective.InnerText.Trim().Replace(" " ,"-") + "&ty=new";

            id_edit.HRef = ConfigUtil.ApplicationPath + "lead_customer/transaction/pricelist_entry.aspx?eduid=" + Server.UrlEncode(EndUserEntry.Key) +
                "&custid=" + Server.UrlEncode(HiddenFieldEndUser_CustCode.Value) + "&effecdate=" + div_Effective.InnerText.Trim().Replace(" ", "-") + "&ty=edit";
        }
        else
        {
            id_pricelist.HRef = "javascript:void('0')";
        }
    }
}