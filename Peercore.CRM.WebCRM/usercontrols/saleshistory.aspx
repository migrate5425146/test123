﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="saleshistory.aspx.cs" Inherits="usercontrols_saleshistory" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="min-width: 200px; overflow: auto;">
        <table width="100%">
            <tr>
                <td class="saleshistory_td_width">
                    <asp:Label ID="Label1" runat="server" Text="Last 3 months sales"></asp:Label>
                </td>
                <td style="font-weight: bold">
                    <asp:Label ID="Label2" runat="server" Text="Debtors Barometer"></asp:Label>
                </td>
                <td style="font-weight: bold">
                    <asp:Label ID="Label3" runat="server" Text="Outstanding Orders"></asp:Label>
                </td>
            </tr>
            <tr>
            <td>
                <asp:RadioButton GroupName="A" Checked="true" ID="RadioButtonTonnes" runat="server" Text="Tonnes" />
                 <asp:RadioButton ID="RadioButtonDollar" GroupName="A" runat="server" Text="Dollar" />

            </td>
            </tr>
            <tr>
                <td valign="top">
                    <div id="monthSales_container" runat="server" style="width: 202px; height: 202px; border: 1px solid #ccc;">
                    </div>
                </td>
                <td valign="top">
                    <div id="debtors_container"  runat="server" style="width: 202px; height: 202px; border: 1px solid #ccc;">
                    </div>
                </td>
                <td valign="top">
                    <div id="div_sales_grid"></div>
                </td>
            </tr>
            <tr>
                <td>
                    <table border="0">
                        <tr>
                            <td>
                                <asp:Label ID="lbSalesSummary" runat="server" Text="Avg. Wk. Sales: "></asp:Label>
                            </td>
                            <td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lbTotSales" runat="server" Text=""></asp:Label>
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </td>
                <td style="text-align:right; width="202">
                    <asp:Label ID="lbDebtorsTotal" runat="server" Text="Total($) : "></asp:Label>
                </td>
                <td>
                    <table border="0" align="right">
                        <tr>
                            <%--<td rowspan="2" valign="top" style="font-weight: bold">
                                <asp:Label ID="lbDelivery" runat="server" Text="DELIVERY"></asp:Label>
                            </td>--%>
                            <td style="text-align: right;">
                                <asp:Label ID="Label4" runat="server" Text="Last Delivery"></asp:Label>
                            </td>
                            <td style="background-color: #FF8537; width:100px; text-align: right;">
                                <asp:Label ID="lbNotDelivered" runat="server" Text="0"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: right;">
                                <asp:Label ID="Label7" runat="server" Text="Outstanding Deliveries"></asp:Label>
                            </td>
                            <td style="background-color: #ffe2f8; width: 20%; text-align: right;">
                                <asp:Label ID="lbNonRequired" runat="server" Text="0"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <span id="bha" runat="server"></span>
    </div>
    <script language="javascript" type="text/javascript">

        $(document).ready(function () {
            $('input[type=radio]').live('change', function () {

                if ($("#RadioButtonTonnes").is(':checked'))
                    istoneordollar = "t";
                else if ($("#RadioButtonDollar").is(':checked'))
                    istoneordollar = "r";
                reportloadcustomer(deodeurl(document.getElementById('MainContent_HiddenCustomerCode').value), 'customersales', 'monthSales_container', istoneordollar);
            
             });
        });

        var istoneordollar = 't';

        if ($("#RadioButtonTonnes").is(':checked'))
            istoneordollar = "t";
        else if ($("#RadioButtonDollar").is(':checked'))
            istoneordollar = "r";

        

        reportloadcustomer(document.getElementById('MainContent_HiddenCustomerCode').value, 'debtorssales', 'debtors_container','');
        reportloadcustomer(deodeurl(document.getElementById('MainContent_HiddenCustomerCode').value), 'customersales', 'monthSales_container', istoneordollar);
        LoadCustomerSalesGrid(document.getElementById('MainContent_HiddenCustomerCode').value);
    </script>
    </form>
</body>
</html>
