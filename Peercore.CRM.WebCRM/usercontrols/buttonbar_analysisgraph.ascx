﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="buttonbar_analysisgraph.ascx.cs" Inherits="usercontrols_buttonbar_analysisgraph" %>

<script type="text/javascript">
    

</script>

<div>

    <div class="dropdown_large" id="div_summaryreport" style="display:none">        
        <asp:DropDownList ID="cmbSummaryReport" runat="server" Visible="false" ClientIDMode="Static"
            onChange="onChange_SummaryReport()">
            <asp:ListItem Value="0" Text="Customer Summary"/>
            <asp:ListItem Value="1" Selected="True" Text="Rep Summary"/>
        </asp:DropDownList>
    </div>

    <div class="longwidthdiv" id="div_costyear" style="display:none">
        <asp:Label ID="lbCostYear" runat="server" Text="Year" Visible="false"></asp:Label>
        <asp:DropDownList ID="cmbCostYear" runat="server" Visible="false" ClientIDMode="Static" Width="78px">
        </asp:DropDownList>
    </div>

    <div class="longwidthdiv" id="div_costperiod" style="display:none">
        <asp:Label ID="lbCostPeriod" runat="server" Text="Period" Visible="false"></asp:Label>
        <asp:DropDownList ID="cmbCostPeriod" runat="server" Visible="false" ClientIDMode="Static" Width="78px">
        </asp:DropDownList>
    </div>

    <div class="dropdown_medium" id="div_repgroup" style="display:none">
    <asp:Label ID="lblRepGroup" runat="server" Text="Reps" Visible="false"></asp:Label>    
    <asp:DropDownList ID="cmbReps" runat="server" Visible="false" 
        onselectedindexchanged="cmbReps_SelectedIndexChanged" AutoPostBack="true" Width="120px">
    </asp:DropDownList>
    </div>
    <div class="dropdown_medium" id="div_sector" style="display: none">
        <asp:Label ID="lbSector" runat="server" Text="Sector" Visible="false"></asp:Label>
        <asp:DropDownList ID="cmbSector" runat="server" Visible="false" OnSelectedIndexChanged="cmbSector_SelectedIndexChanged"
            AutoPostBack="false" Width="125px">
        </asp:DropDownList>
    </div>

    <div class="dropdown" id="div_daterange" style="display:none; margin-top:25px;">
        <asp:DropDownList ID="cmbDateRange" runat="server" Visible="false" ClientIDMode="Static"
            onChange="SetPeriod()" height="25px">
            <asp:ListItem Value="0" Text="Today"/>
            <asp:ListItem Value="1" Text="Week"/>
            <asp:ListItem Value="2" Text="Month" Selected="True"/>
            <asp:ListItem Value="3" Text="3 Months"/>
            <asp:ListItem Value="4" Text="6 Months" />
        </asp:DropDownList>
    </div>

    <div class="longwidthdiv" id="div_state" style="display:none">
        <asp:Label ID="lbState" runat="server" Text="State" Visible="false"></asp:Label>
        <asp:DropDownList ID="cmbState" runat="server" Visible="false" ClientIDMode="Static" height="25px">
        </asp:DropDownList>
    </div>

    <div class="dropdown_large_x" id="div_market" style="display:none">
        <asp:Label ID="lbMarket" runat="server" Text="Market" Visible="false"></asp:Label>
        <asp:DropDownList ID="cmbMarket" runat="server" Visible="false" ClientIDMode="Static">
        </asp:DropDownList>
    </div>

   <div class="datepicker" id="div_FromDate" style="display:none">
        <asp:Label ID="lbStartDate" runat="server" Text="Start Date" Visible="false"></asp:Label> 
        <asp:TextBox ID="dtpFromDate" runat="server"  style="width:120px;" ClientIDMode="Static"></asp:TextBox>
   </div>

   <div class="datepicker" id="div_ToDate" style="display:none">
        <asp:Label ID="lbEndDate" runat="server" Text="End Date" Visible="false"></asp:Label> 
        <asp:TextBox ID="dtpToDate" runat="server"  style="width:120px;" ClientIDMode="Static"></asp:TextBox>
   </div>

   <div class="dropdown_large" id="div_distributor" style="display:none">
       <asp:Label ID="lbDistributor" runat="server" Text="Distributor" Visible="false"></asp:Label>
       <asp:TextBox ID="txtnewcustcode" runat="server" Visible="false" ClientIDMode="Static"></asp:TextBox>
       <a href="#" id="id_distributor">
                     <span style="display:inline-block;"><img id="Img1" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></span></a>
    </div>

    <div class="dropdown_large" id="div_enduser" style="display:none">
       <asp:Label ID="lbEndUser" runat="server" Text="EndUser" Visible="false"></asp:Label>
       <asp:TextBox ID="txtEndUser" runat="server" Visible="false"></asp:TextBox>
       <a href="#" id="id_endUser">
                    <span style="display:inline-block;"></span><img id="Img2" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></span></a>
    </div>


    <div id="div_products" style="width:30%; float:left; display:none; ">
        <asp:CheckBox ID="chkProducts" runat="server" Text="Include Products with No Sales"/>
    </div>
    <div id="div_activeendusers" style="width:25%; float:left; display:none;">
        <asp:CheckBox ID="chkActiveEndUsers" runat="server" Text="Active End Users Only" Checked="true"/>
    </div>

    &nbsp;<asp:Button ID="btnFilter" runat="server" Text="" CssClass="dashboardfilterbtnreport"
        onclick="btnFilter_Click" />
    <input id="btnexport" type="button" value="" onclick="ExportData()" class="dashboardexportbtnreport"/>
    <asp:Button ID="btnexport1" runat="server" Text="" CssClass="dashboardexportbtnreport"
        onclick="btnexport_Click" Visible="false" />
    
   </div> 
    
<div class="clearall"></div>
<asp:HiddenField ID="HiddenCustCode" runat="server" Value="" ClientIDMode="Static"/>
<script type="text/javascript">

    $(document).ready(function () {

        $("#dtpFromDate").kendoDatePicker({
            // display month and year in the input
            format: "dd-MMM-yyyy"
        });
        $("#dtpToDate").kendoDatePicker({
            // display month and year in the input
            format: "dd-MMM-yyyy"
        });
        //SetPeriod();
    });

    function GetDateString(day, month, year) {
        var monthname = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
        var stoDate = "";
        if (day <= 9) {
            stoDate = "0"
        }
        stoDate = stoDate + day+"-";
//        if (month <= 9) {
//            stoDate = stoDate + "0"
//        }
        stoDate = stoDate + monthname[month-1] + "-" + year;
        return stoDate;
    }

    function SetPeriod() {
        
        var index = $("#cmbDateRange").val();
        var fromDate = new Date();

        var month = fromDate.getMonth() + 1;
        var day = fromDate.getDate();
        var year = fromDate.getFullYear();

        var sfromDate = "";
        var stoDate = GetDateString(day, month, year);

        if (index == 1) {//"Week"
            day = day - 7;
            fromDate = new Date(year, month - 1, day);
        }
        else if (index == 2) {//Month
            month = month - 1;
            fromDate = new Date(year, month - 1, day);
        }
        else if (index == 3) {//3 Months
            month = month - 3;
            fromDate = new Date(year, month - 1, day);
        }
        else if (index == 4) {//6 Months
            month = month - 6;
            fromDate = new Date(year, month - 1, day);
        }
        sfromDate = GetDateString(fromDate.getDate(), fromDate.getMonth() + 1, fromDate.getFullYear());
        
        $("#dtpFromDate").val(sfromDate);
        $("#dtpToDate").val(stoDate);
    }

    function onChange_SummaryReport() {
        var index = $("#cmbSummaryReport").val();
        //alert(index);
        if (index == "0") {
            $("#div_distributor").css("display", "block");
            $("#div_repgroup").css("display", "none");
        }
        else {
            $("#div_distributor").css("display", "none");
            $("#div_repgroup").css("display", "block");
        }
    }
</script>


