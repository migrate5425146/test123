﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="enduser_price.aspx.cs" Inherits="usercontrols_enduser_price" %>

<!DOCTYPE html>
<html>
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="HiddenFieldEndUser_CustCode" runat="server" />
    <asp:HiddenField ID="HiddenFieldEndUser_EndUsercode" runat="server" />
    <div id="div_customer_contactperson">
        <div class="enduser_tab">
            <div class="addlinkdiv" style="float: left" id="div_contactperson">
                <a id="id_edit" runat="server" href="#">Edit Price List</a>                
            </div>
            <div class="addlinkdiv" style="float: left" id="div1">
                <a id="id_pricelist" runat="server" href="#">Add Price List</a>
            </div>
            <div class="div_datelable">
              <strong>  Effective Date : </strong><span>
                    <div id="div_Effective" runat="server">
                    </div>
                </span>
            </div>
            <div class="div_updatelable">
               <strong>   Last Updated :  </strong><span>
                    <div id="div_LastUpdate" runat="server">
                    </div>
                </span>
            </div>
        </div>
        <div class="clearall">
            &nbsp;</div>
        <div style="min-width: 200px; overflow: auto">
            <div style="float: left; width: 100%; overflow: auto">
                <div id="EndUserPriceGrid">
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var cust_code = $("#HiddenFieldEndUser_CustCode").val();
            var enduser_code = $("#HiddenFieldEndUser_EndUsercode").val();
            loadEnduserPriceList(cust_code, enduser_code, 'EndUserPriceGrid');
        });
    </script>
    </form>
</body>
</html>
