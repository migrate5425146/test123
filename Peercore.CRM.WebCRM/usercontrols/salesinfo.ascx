﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="salesinfo.ascx.cs" Inherits="usercontrols_salesinfo" %>
<%@ Register Src="~/usercontrols/CustomerSalesinfo.ascx" TagPrefix="ucl1" TagName="Customersalesinfo" %>
<%@ Register Src="~/usercontrols/sales_history_details.ascx" TagPrefix="ucl1" TagName="SalesHistoryDetails" %>


<asp:HiddenField ID="HiddenFieldRepCode" runat="server" />
    <asp:HiddenField ID="HiddenFieldDetailType" runat="server" />
<asp:HiddenField ID="HiddenFieldDescription" runat="server" />

<asp:HiddenField ID="HiddenFieldRepType" runat="server" />

    <script type="text/javascript">
        $(document).ready(function () {

            loadSalesInfoTabs($("#MainContent_custsalesinfo_HiddenFieldRepType").val());
        });
    </script>
    <style type="text/css">
        #tabstrip
        {
            height: 100%;
            border-width: 0;
        }
        
    </style>
    <div id="div_sales_window" style="display:none">
        <ucl1:SalesHistoryDetails ID="salesHistoryDetails" runat="server" />
    </div>
    <div>
        <div id="tabstrip1">
            <ul id="ul_tabstrip">
                
                <li class="k-state-active"  id="distributorTab">Distributor </li>
                <%--<li>Top 10</li>--%>
            </ul>
            <div>
                <div class="salestabs" style="display: none;">
                 <a href="#" class="tabslinks">Distributor</a>    <a href="#" class="tabslinks">Customer</a>
                </div>
                <div class="toolbar2">
               
                    <a id="href_reports" class="sprite_button" style="display:none"><span class="copy icon"></span>Reports</a>
                    <a id="href_print" class="sprite_button"  style="float: right; margin-right: 10px;display:none"><span class="print icon"></span>Print</a>
                    <a id="href_close" class="sprite_button" style="float: right; margin-right: 10px;display:block"><span
                        class="leftarrow icon"></span>Back</a> <span id="div_row_count" class="rowcounttext">
                    </span>
                    <h4 id="div_report_header" <%--style="padding-top:47px;--%> padding-left:5px;">Sales Targets - Distribution Sales Representative</h4>
                </div>
                 <div class="toolbar3" runat="server" id="lblCurrentFlow" style="display:none">
                   <span class="tlbcolor">Month : </span> FEB <span class="rnb">&raquo; </span>
                    <span class="tlbcolor"> Product Type : </span>Refinery Products <span class="rnb"> &raquo; </span>
                </div>
                <div class="toolbar4">
                    <asp:DropDownList ID="DropDownDisplayOption" runat="server" CssClass="dropdownwidth select-different">
                    
                        <asp:ListItem Value="M" Text="Targets In Mills" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="S" Text="Targets In Sticks" ></asp:ListItem>


                    </asp:DropDownList>
<%--                    <span>Sort by </span>
                    <asp:DropDownList ID="DropDownListSort" runat="server" CssClass="dropdownwidth">
                        <asp:ListItem Value="6" Text="MTD (U)" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="3" Text="MTD ($)"></asp:ListItem>
                        <asp:ListItem Value="9" Text="YTD (U)" ></asp:ListItem>
                        <asp:ListItem Value="10" Text="YTD ($)" ></asp:ListItem>
                        <asp:ListItem Value="11" Text="Last YTD (U)" ></asp:ListItem>
                        <asp:ListItem Value="12" Text="Last YTD ($)" ></asp:ListItem>
                        <asp:ListItem Value="1" Text="Code" ></asp:ListItem>
                    </asp:DropDownList>--%>
                    <span>Group by </span>
                    <asp:DropDownList ID="DropDownGroupBy" runat="server" CssClass="dropdownwidth select-different">
                        
                        <%--<asp:ListItem Value="market" Text="Market"></asp:ListItem>--%>
                        <asp:ListItem Value="brand" Text="Brand"></asp:ListItem>
                        <%--<asp:ListItem Value="product" Text="Product"></asp:ListItem>--%>
                        <asp:ListItem Value="customer" Text="Customer"></asp:ListItem>
                        <asp:ListItem Value="rep" Text="Rep" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="route" Text="Route"></asp:ListItem>
                    </asp:DropDownList>

                    <span>Market</span>
                    <asp:DropDownList ID="DropDownMarket" runat="server" CssClass="dropdownwidth select-different">
                        
                    </asp:DropDownList>

                    <div style="display: inline-block;">
                        <span>Selection</span>
                        <asp:TextBox ID="txtSelection" runat="server" ReadOnly="true" Width="100px"></asp:TextBox></div>
                </div>
                <div class="toolbar4">
                      <a id="a_brand" href="javascript:SetSalesInfo('brand','gridCustomerSales','GetBusinessSales','1')">Brand</a> | 
                        <%--<a id="a_product" href="javascript:SetSalesInfo('product','gridCustomerSales','GetBusinessSales','1')">Product</a> | --%>
                        <a id="a_customer" href="javascript:SetSalesInfo('customer','gridCustomerSales','GetBusinessSales','1')">Customer</a> | 
                        <a id="a_rep" href="javascript:SetSalesInfo('rep','gridCustomerSales','GetBusinessSales','1')">Rep</a>
                    <asp:Button ID="btnAllReps" Text="All Reps" runat="server" CssClass="button" 
                        onclick="btnAllReps_Click" />
                </div>
                <div id="div_salesinfo" runat="server">
                    <ucl1:Customersalesinfo ID="custsalesinfo" runat="server" />

                        <%--        <div id="message_grid">
                                </div>--%>
                </div>
            </div>
            <div></div>
           <%-- <div>
            <div>
                
                <div class="formtextdiv">Rep</div>
                <div class="formdetaildiv_right">
                    <asp:DropDownList ID="dropdowntopcustomer" runat="server" AppendDataBoundItems="true" CssClass="dropdownwidth select-different" style="height:15px;">
                            </asp:DropDownList>
                </div>

                <div class="clearall">
            </div>
            </div>
            <div class="homegridright1" id="maindiv_customer" runat="server">
                    <div class="homegridheader">
                        <div id="div_topcustomer_repcodeheader" >
                            Top 5 Customers
                        </div>
                        
                    </div>
                    <div id="div_customers1" style="height: 200px;">
                    </div>
                </div>
            
            </div>--%>
        </div>
    </div>

    <script type="text/javascript">
        $("#MainContent_custsalesinfo_dropdowntopcustomer").change(function () {
            $("#MainContent_HiddenFieldRepCode").val($(this).val());
            loadCustomersdetails($(this).val());
        });

        

        hideStatusDiv("MainContent_div_message");

        $("#MainContent_custsalesinfo_DropDownDisplayOption").change(function () {
            LoadTitle($("#MainContent_custsalesinfo_HiddenFieldDetailType").val(), "n", "0", $("#MainContent_custsalesinfo_DropDownDisplayOption").val(), "", "gridCustomerSales", "GetBusinessSales");
            //LoadDailyTitle($("#MainContent_custsalesinfo_HiddenFieldDetailType").val(), "n", "0", $("#MainContent_custsalesinfo_DropDownDisplayOption").val(), "", "gridDailySales", "GetTodaySalesDrilldown");
            LoadSalesInfoChart();
        });

        $("#MainContent_custsalesinfo_DropDownGroupBy").change(function () {
            //alert($("#MainContent_DropDownGroupBy").val());
            SetSalesInfo($("#MainContent_custsalesinfo_DropDownGroupBy").val(), "gridCustomerSales", "GetBusinessSales",2);
            //SetDailySalesInfo($("#MainContent_custsalesinfo_DropDownGroupBy").val(), "gridDailySales", "GetTodaySalesDrilldown", 2);
        });

        $("#MainContent_custsalesinfo_DropDownMarket").change(function () {
           
            //alert($("#MainContent_DropDownGroupBy").val());
            SetSalesInfo($("#MainContent_custsalesinfo_DropDownGroupBy").val(), "gridCustomerSales", "GetBusinessSales", 2);
            //SetDailySalesInfo($("#MainContent_custsalesinfo_DropDownGroupBy").val(), "gridDailySales", "GetTodaySalesDrilldown", 2);
        });

        $("#MainContent_custsalesinfo_DropDownListSort").change(function () {
            loadSortSalesInfoGrid("gridCustomerSales", "GetBusinessSales", "cust");
        });

        $("#MainContent_custsalesinfo_custsalesinfo_RadioButtonCMP").change(function () {
            SetDailySalesInfo($("#MainContent_custsalesinfo_DropDownGroupBy").val(), "gridDailySales", "GetTodaySalesDrilldown", 2);
        });
        $("#MainContent_custsalesinfo_custsalesinfo_RadioButtonAll").change(function () {
            SetDailySalesInfo($("#MainContent_custsalesinfo_DropDownGroupBy").val(), "gridDailySales", "GetTodaySalesDrilldown", 2);
        });

        
        $(document).ready(function () {


            //  DisplayMessages();
            $("#MainContent_custsalesinfo_HiddenFieldDetailType").val("rep");
            //alert($("#MainContent_custsalesinfo_HiddenFieldDetailType").val());
            LoadTitle($("#MainContent_custsalesinfo_HiddenFieldDetailType").val(), "n", "0", "M", "", "gridCustomerSales", "GetBusinessSales");
            SetSalesInfoCss($("#MainContent_custsalesinfo_HiddenFieldDetailType").val(), "cust");
            LoadSalesInfoChart($("#MainContent_DropDownDisplayOption").val(), "cust");

            //LoadDailyTitle($("#MainContent_custsalesinfo_HiddenFieldDetailType").val(), "n", "0", "M", "", "gridDailySales", "GetTodaySalesDrilldown");
            //loadCustomersdetails('All');

            $("#panelbarcontent").kendoPanelBar({
                expandMode: "single",
                select: onSelect
            });

            function onSelect(e) {
                var item = $(e.item)[0].id;
                //alert(item);

                if (item != undefined && item != '') {
                    switch (item) {
                        case "1":
                            //loadSortSalesInfoGrid("gridCustomerSales", "GetBusinessSales", "cust");
                            break;
                        case "2":
                            //LoadSalesInfoChart($("#MainContent_DropDownDisplayOption").val(), "cust");
                            //LoadDailyTitle($("#MainContent_custsalesinfo_HiddenFieldDetailType").val(), "n", "0", $("#MainContent_custsalesinfo_DropDownDisplayOption").val(), "", "gridDailySales", "GetTodaySalesDrilldown")
                            break;
                        case "3":
                            LoadSalesInfoTrendChart($("#MainContent_custsalesinfo_txtSelection").val(), "cust");
                            break;
                    }
                }
            }


            var div_sales_window = $("#div_sales_window"),
                        newcust_undo = $("#undo")
                                .bind("click", function () {
                                    div_sales_window.data("kendoWindow").open();
                                    newcust_undo.hide();
                                });

            var onClose = function () {
                newcust_undo.show();
            }

            if (!div_sales_window.data("kendoWindow")) {
                div_sales_window.kendoWindow({
                    width: "800px",
                    height: "600px",
                    title: "Perfetti | Business Sales History Details - This Year",
                    close: onClose
                });
            }

            div_sales_window.data("kendoWindow").close();



        });

        $("#href_print").click(function () {
            $("div#div_loader").show();
            $("#div_sales_window").data("kendoWindow").open();
            $("div#div_loader").hide();
            loadSalesHistoryMonth("gridSalesHistoryMonth", $("#MainContent_custsalesinfo_txtSelection").val());
        });


        $("#MainContent_custsalesinfo_custsalesinfo_DropDownGraphType").change(function () {
            LoadSalesInfoTrendChart($("#MainContent_custsalesinfo_txtSelection").val(), "cust");
        });

        $("#MainContent_custsalesinfo_custsalesinfo_rbTrendGraphAccumulate").change(function () {
            LoadSalesInfoTrendChart($("#MainContent_custsalesinfo_txtSelection").val(), "cust");
        });

        $("#MainContent_custsalesinfo_custsalesinfo_rbTrendGraphMonthly").change(function () {
            LoadSalesInfoTrendChart($("#MainContent_custsalesinfo_txtSelection").val(), "cust");
        });

        $("#href_close").click(function () {
            LoadBackSalesInfo("cust");
        });

        
        </script>

