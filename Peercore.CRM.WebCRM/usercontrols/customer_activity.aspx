﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="customer_activity.aspx.cs" Inherits="usercontrols_customer_activity" %>

<!DOCTYPE html>

<html>
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="div_customer_activity">
        <asp:HiddenField ID="HiddenFieldActivity_CustCode" runat="server" />
        <div id="div_customer_contactperson">
            <div class="tab_button">
                <div class="addlinkdiv" style="float: left" id="div_activity">
                    <a id="id_activity" runat="server" href="#">Add Activity</a>
                </div>
                    <div style="float: right; line-height:25px;" id="div1">
                    <div style="width: 15px; height: 15px; margin-top:5px; float:right; background-color: #d8bfd8; border:1px solid #000"></div>
                    <strong>Sent To Calendar&nbsp;</strong>
                </div>
                <div class="clearall">
                </div>

            </div>
            <div style="min-width: 200px; overflow: auto">
                <div style="float: left; width: 100%; overflow: auto">
                    <div id="LeadActivityGrid">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var cust_code = $("#HiddenFieldActivity_CustCode").val();

            loadCustomerActivityGrid(cust_code, 'LeadActivityGrid');
        });
    </script>
    </form>
</body>
</html>
