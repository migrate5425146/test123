﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Common;

public partial class usercontrols_distributor_bank : System.Web.UI.Page
{

    #region Properties
    private KeyValuePair<string, string> DistributorEntry
    {
        get
        {
            if (Session[CommonUtility.DISTRIBUTOR_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.DISTRIBUTOR_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.DISTRIBUTOR_DATA] = value;
        }
    }
    #endregion Properties

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(DistributorEntry.Key))
        {
            HiddenFieldBank_UserName.Value = DistributorEntry.Key;
            if (DistributorEntry.Value.Equals("ASE"))
                id_address.HRef = "javascript:addBankAccount('lead_customer/process_forms/processmaster.aspx?fm=bankdetails&type=insert&typ=ASE&distid=" + Server.UrlEncode(DistributorEntry.Key) + "','')";
            else
                id_address.HRef = "javascript:addBankAccount('lead_customer/process_forms/processmaster.aspx?fm=bankdetails&type=insert&typ=DIST&distid=" + Server.UrlEncode(DistributorEntry.Key) + "','')";
        }
        else
        {
            id_address.HRef = "javascript:void('0')";
        }

    }
}