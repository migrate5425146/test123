﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Peercore.CRM.Common;

#region Developer Comments
/*
 * Create By - Indunil Udayalal.
 * Create Date - 12/07/2013
 */
#endregion

public partial class usercontrols_breadcrumb : System.Web.UI.UserControl
{
    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        //Add(null);
    }
    #endregion

    #region Methods
    //Setting data 
    public void Add(string[] strData, string page)
    {
        if (!IsPostBack)
        {
            switch (page)
            {
                case "Home":
                    checkExisting(strData);
                    if (strData.Length >= 2)
                    {
                        HyperLink breadCrumbHyperLink = new HyperLink();
                        breadCrumbHyperLink.Text = strData[0];
                        breadCrumbHyperLink.NavigateUrl = strData[1];
                        breadCrumbHyperLink.CssClass = "selected_link";
                        PlaceHolderbreadCrumb.Controls.Add(breadCrumbHyperLink);
                    }

                    setSessionData();
                    break;
                case "":
                    checkExisting(strData);

                    if (Session["strData"] == null)
                    {
                        //Settting Home link.
                        HyperLink breadCrumbHomeHyperLink2 = new HyperLink();
                        breadCrumbHomeHyperLink2.Text = "Home";
                        breadCrumbHomeHyperLink2.NavigateUrl = ConfigUtil.ApplicationPath;
                        PlaceHolderbreadCrumb.Controls.Add(breadCrumbHomeHyperLink2);
                    }

                    //Settting Other link.
                    for (int i = 0; i < strData.Length; i++)
                    {
                        //Setting image for middle slots, and not the final slots.
                        if (i != 0 && i != (strData.Length - 1))
                        {
                            //Add empty spacing hyperlink...
                            //HyperLink gapHyperLink7 = new HyperLink();
                            //gapHyperLink7.ImageUrl = ConfigUtil.ApplicationPath + "Images/Controls/arrow_active.gif";
                            //PlaceHolderbreadCrumb.Controls.Add(gapHyperLink7);
                        }

                        HyperLink breadCrumbHyperLink4 = new HyperLink();
                        if (i == 0)
                        {
                            //Changed by Gayan - 18-08-2009
                            if (!String.IsNullOrEmpty(strData[i]) && strData.Length >= i + 1)
                            {
                                breadCrumbHyperLink4.Text = strData[i].ToString();
                            }
                        }

                        //make sure final link un-clicable.
                        if (i == (strData.Length - 1))
                        {
                            breadCrumbHyperLink4.CssClass = "selected_link";
                            //breadCrumbHyperLink4.Style.Add("color", "#FE8B34");
                        }
                        else
                        {
                            //Changed by Gayan - 18-08-2009
                            if (!String.IsNullOrEmpty(strData[i + 1]) && strData.Length >= i + 2)
                            {
                                breadCrumbHyperLink4.NavigateUrl = strData[i + 1].ToString();
                                breadCrumbHyperLink4.CssClass = "selected_link";
                                breadCrumbHyperLink4.Enabled = false;
                                //breadCrumbHyperLink4.Style.Add("color", "#FE8B34");
                            }
                        }

                        if (breadCrumbHyperLink4.Text != "")
                        {
                            //Add empty spacing hyperlink...
                            //HyperLink gapHyperLink6 = new HyperLink();
                            //gapHyperLink6.ImageUrl = ConfigUtil.ApplicationPath + "Images/Controls/arrow_active.gif";
                            //PlaceHolderbreadCrumb.Controls.Add(gapHyperLink6);

                            PlaceHolderbreadCrumb.Controls.Add(breadCrumbHyperLink4);
                        }
                    }

                    setSessionData();
                    break;
            }
        }
        else
        {
            if (Session["strData"] != null)
                populatePlaceHolder(((string[][])Session["strData"]));
            //else
            //    populatePlaceHolder(strData);
        }
    }

    public void Add(string[][] strData)
    {
        if (!IsPostBack)
        {
            try
            {
                if (strData != null)
                {
                    RenderBreadCrumb(strData);
                    setSessionData();
                }
                else
                {
                    RenderBreadCrumb(null);
                }
            }
            catch { }
        }
        else
        {
            if (Session["strData"] != null)
                populatePlaceHolder(((string[][])Session["strData"]));
        }
    }

    public void Add(string pageName, string strNavigateUrl, string page)
    {
        if (!string.IsNullOrWhiteSpace(strNavigateUrl))
            Add(new string[][] { new string[] { pageName, strNavigateUrl }, null});
        else
            Add(null);
    }

    private void setSessionData()
    {
        int intCount = 0;
        string[][] strData = new string[PlaceHolderbreadCrumb.Controls.Count][];
        foreach (HyperLink hl in PlaceHolderbreadCrumb.Controls)
        {
            if (hl.Text == "" && hl.NavigateUrl == "" && hl.ImageUrl == "")
            {
                strData[intCount] = new string[] { "", ConfigUtil.ApplicationPath + "Images/Controls/arrow_active.gif" };
            }
            if (hl.Text == "" && hl.NavigateUrl == "")
                strData[intCount] = new string[] { ((HyperLink)hl).ImageUrl, ((HyperLink)hl).NavigateUrl };
            else
                strData[intCount] = new string[] { ((HyperLink)hl).Text, ((HyperLink)hl).NavigateUrl };
            intCount++;
        }
        Session[CommonUtility.BREADCRUMB] = strData;
    }

    private bool checkExisting(string[] strDataAdd)
    {
        bool blnHasData = false;
        try
        {
            if (Session["strData"] != null)
            {
                string[][] strData = (string[][])Session["strData"];
                int intLimit = -1;
                for (int j = 0; j < strData.Length; j++)
                {
                    blnHasData = true;
                    if (strData[j][0].Equals(strDataAdd[0].ToString()) && strData[j][1].Equals(strDataAdd[1].ToString()))
                    {
                        //Response.Write("Breadcrumb already exists");
                        intLimit = j;
                    }
                }
                if (blnHasData)
                {
                    //set new array
                    if (intLimit > -1)
                    {
                        string[][] strSessionData = new string[intLimit][];
                        for (int j = 0; j < intLimit; j++)
                        {
                            strSessionData[j] = strData[j];
                        }
                        Session["strData"] = strSessionData;
                        strData = strSessionData;
                    }
                    populatePlaceHolder(strData);
                }
            }
        }
        catch (Exception error)
        {
        }
        return (blnHasData);
    }

    private void populatePlaceHolder(string[][] strData)
    {
        try
        {
            if (strData != null)
            {
                for (int j = 0; j < strData.Length; j++)
                {
                    //identify the last node, make that not clicable link.
                    if (j == (strData.Length - 1))
                    {
                        HyperLink breadCrumbHyperLink = new HyperLink();
                        string[] strContent = strData[j];
                        breadCrumbHyperLink.Text = strContent[0];
                        breadCrumbHyperLink.NavigateUrl = strContent[1];
                        breadCrumbHyperLink.CssClass = "selected_link";
                        //breadCrumbHyperLink.Style.Add("color", "#FE8B34");
                        breadCrumbHyperLink.Enabled = false;
                        PlaceHolderbreadCrumb.Controls.Add(breadCrumbHyperLink);
                    }
                    else
                    {
                        string[] strContent = strData[j];
                        if (strContent[0].Contains(ConfigUtil.ApplicationPath + "Images/Controls/arrow_active.gif"))
                        {
                            //Add empty spacing hyperlink...
                            //HyperLink gap4HyperLink = new HyperLink();
                            //gap4HyperLink.ImageUrl = strContent[0];
                            //PlaceHolderbreadCrumb.Controls.Add(gap4HyperLink);
                        }
                        else
                        {
                            HyperLink breadCrumbHyperLink = new HyperLink();
                            breadCrumbHyperLink.Text = strContent[0];
                            breadCrumbHyperLink.NavigateUrl = strContent[1];
                            // breadCrumbHyperLink.CssClass = "selected_link";
                            PlaceHolderbreadCrumb.Controls.Add(breadCrumbHyperLink);
                        }
                    }
                }
            }
        }
        catch (Exception error)
        {

        }
    }

    private void RenderBreadCrumb(string[][] strData)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<ul class=\"breadcrumb-beauty\">");

        if (strData != null)
        {
            sb.Append("<li>");
            sb.Append("<a href=\"" + ConfigUtil.ApplicationPath + "default.aspx" + "\"><span class=\"fs1\" data-icon=\"&#xe002;\"></span>Home </a>");
            sb.Append("</li>");
            for (int j = 0; j < strData.Length; j++)
            {
                string[] strContent = strData[j];
                if (strContent != null && (!string.IsNullOrWhiteSpace(strContent[0])))
                {
                    sb.Append("<li>");
                    sb.Append("<a href=\"" + strContent[1] + "\">" + "<strong>" + strContent[0] + "</strong>" + "</a>");
                    sb.Append("</li>");
                }
            }
        }
        else
        {
            sb.Append("<li>");
            sb.Append("<a href=\"" + ConfigUtil.ApplicationPath + "default.aspx" + "\"><span class=\"fs1\" data-icon=\"&#xe002;\"></span>Home </a>");
            sb.Append("</li>");
            sb.Append("<li>");
            sb.Append("</li>");
        }
        sb.Append("</ul>");
        div_breadcrumb.InnerHtml = sb.ToString();
        //return sb.ToString();
    }
    #endregion

    #region Comment Methods
    //public void Add(string productId, string pageName, string strLanguage, string strNavigateUrl, bool isLeftMenu, string page, string manufacture)
    //{
    //    ProductFacade facade = new ProductFacade(CurrentUser);
    //    Product _product = facade.GetProductForBreadCrumb(productId, strLanguage);
    //    if (_product != null)
    //    {
    //        string _productName = _product.ProductName.Length < 30 ? _product.ProductName.Trim() : _product.ProductName.Substring(0, 25);

    //        Add(new string[]
    //                { 
    //                    _product.MajCatDescriptionForBreadCrumb,_product.MinCatDescriptionForBreadCrumb,_product.MinCatDescription,_product.MajCatId.Trim(),_product.MinCatId.Trim(),
    //                     _product.Manufacturer.Trim(),_product.ManufactureId.Trim(),_productName.Trim()+"..."}, page);

    //    }
    //    else
    //    {
    //        Add(new string[][] { new string[] { pageName, strNavigateUrl }, new string[] { manufacture, "" } });
    //    }
    //}

    //public void Add(string minCat, string majCat, string navigationUrl, string majCatId, string minCatId, string manufacture, string manufactureId, string page, out string catString)
    //{
    //    Category _category = new Category();

    //    if ((!string.IsNullOrEmpty(minCat)) && ((!string.IsNullOrEmpty(majCat))))
    //    {
    //        Add(new string[] { minCat, majCat, navigationUrl, majCatId, minCatId, manufacture, manufactureId }, page);
    //    }
    //    else if ((!string.IsNullOrEmpty(minCatId)) && ((!string.IsNullOrEmpty(majCatId))))
    //    {
    //        CategoryFacade facade = new CategoryFacade();

    //        _category = facade.GetMinorCategoryName(int.Parse(minCatId), int.Parse(majCatId));

    //        Add(new string[] { _category.MinCategoryDescription, _category.MajCategoryDescription, _category.MajCategoryDescriptionForLeftMenu + "/" + "Category" + majCatId, majCatId, minCat, "", "" }, page);
    //    }
    //    catString = _category.MinCategoryDescription;
    //}

    /// <summary>
    /// This is setting cotrols for postback mode.
    /// </summary>
    /// <param name="strData"></param>
    //private void populatePlaceHolder(string[] strData)
    //{
    //    if (Session["strData"] == null)
    //    {
    //        //Settting Home link.
    //        HyperLink breadCrumbHomeHyperLink2 = new HyperLink();
    //        breadCrumbHomeHyperLink2.Text = "Home";
    //        breadCrumbHomeHyperLink2.NavigateUrl = ConfigUtil.ApplicationPath;
    //        PlaceHolderbreadCrumb.Controls.Add(breadCrumbHomeHyperLink2);
    //    }

    //    //Settting Other link.
    //    for (int i = 0; i < strData.Length; i++)
    //    {
    //        //Setting image for middle slots, and not the final slots.
    //        if (i != 0 && i != (strData.Length - 1))
    //        {
    //            //Add empty spacing hyperlink...
    //            //HyperLink gapHyperLink7 = new HyperLink();
    //            //gapHyperLink7.ImageUrl = "~/Images/Controls/arrow_active.gif";
    //            //PlaceHolderbreadCrumb.Controls.Add(gapHyperLink7);
    //        }

    //        HyperLink breadCrumbHyperLink4 = new HyperLink();
    //        if (i == 0)
    //            breadCrumbHyperLink4.Text = strData[i].ToString();

    //        //make sure final link un-clicable.
    //        if (i == (strData.Length - 1))
    //        {
    //            breadCrumbHyperLink4.CssClass = "selected_link";
    //            // breadCrumbHyperLink4.Style.Add("color", "#FE8B34");
    //        }
    //        else
    //        {
    //            breadCrumbHyperLink4.Text = strData[i + 1].ToString();
    //            breadCrumbHyperLink4.NavigateUrl = strData[i + 1].ToString();
    //            breadCrumbHyperLink4.Enabled = false;
    //            breadCrumbHyperLink4.CssClass = "selected_link";
    //            //breadCrumbHyperLink4.Style.Add("color", "#FE8B34");
    //        }

    //        if (breadCrumbHyperLink4.Text != "")
    //        {
    //            //Add empty spacing hyperlink...
    //            // HyperLink gapHyperLink6 = new HyperLink();
    //            //gapHyperLink6.ImageUrl = "~/Images/Controls/arrow_active.gif";
    //            //PlaceHolderbreadCrumb.Controls.Add(gapHyperLink6);

    //            PlaceHolderbreadCrumb.Controls.Add(breadCrumbHyperLink4);
    //        }
    //    }

    //    setSessionData();
    //}
    #endregion
}