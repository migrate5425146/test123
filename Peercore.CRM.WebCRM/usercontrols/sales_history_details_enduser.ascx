﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="sales_history_details_enduser.ascx.cs" Inherits="usercontrols_sales_history_details_enduser" %>
<div class="selected">
     Business Sales History Details - This Year
</div>
<div class="toolbar3" id="lblSalesHistoryEndUserFlow">
     Month : FEB | text1 | text2
</div>
<div>
    <ul id="panelbarSalesHistoryEndUser">
        <li class="k-state-active" id="7">Month Totals
            <div id="gridSalesHistoryMonthEndUser">sdf
            </div>
        </li>

        <li id="8">Year Totals
            <div id="gridSalesHistoryYearEndUser">
            sdf
            </div>
        </li>
        <li id="9">Graph
            <div>
                <asp:DropDownList ID="DropDownGraphTypeSalesHistoryEndUser" runat="server" CssClass="dropdownwidth" Width="120px">
                    <asp:ListItem Value="Sales" Text="Sales" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="Tonnes" Text="Tonnes"></asp:ListItem>
                    <asp:ListItem Value="Sales/Tonne" Text="Sales/Tonne" ></asp:ListItem>
                    <asp:ListItem Value="Sales vs Budget ($)" Text="Sales vs Budget ($)" ></asp:ListItem>
                    <asp:ListItem Value="Sales vs Budget (T)" Text="Sales vs Budget (T)" ></asp:ListItem>
                    <asp:ListItem Value="Last Sales vs Budget ($)" Text="Last Sales vs Budget ($)" ></asp:ListItem>
                    <asp:ListItem Value="Last Sales vs Budget (T)" Text="Last Sales vs Budget (T)" ></asp:ListItem>
                </asp:DropDownList>   
                
                <asp:RadioButton runat="server" ID="rbTrendGraphAccumulateSalesHistoryEndUser" ValidationGroup="A" GroupName="A" Checked="true" /><span>Accumulated</span>
                <asp:RadioButton runat="server" ID="rbTrendGraphMonthlySalesHistoryEndUser" ValidationGroup="A" GroupName="A" /><span>Monthly</span>
                             
            </div>
            <div id="div_chart_SalesHistoryEndUser">
            </div>
        </li>
    </ul>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        //SetSalesHistoryFlow();
        $("#lblSalesHistoryEndUserFlow").html($("#lblCurrentFlowEnduser").text());
        $("#panelbarSalesHistoryEndUser").kendoPanelBar({
            expandMode: "single",
            select: onSelect
        });

        function onSelect(e) {
            var item = $(e.item)[0].id;
            if (item != undefined && item != '') {
                switch (item) {
                    case "7":
                        break;
                    case "8":
                        break;
                    case "9":
                        LoadSalesInfoTrendChart($("#txtSelection").val(), "enduser_sh");
                        break;
                }
            }
        }
    });

    $("#salesHistoryDetailsEnduser_DropDownGraphTypeSalesHistoryEndUser").change(function () {
        LoadSalesInfoTrendChart($("#txtSelection").val(), "enduser_sh");
    });

    $("#salesHistoryDetailsEnduser_rbTrendGraphAccumulateSalesHistoryEndUser").change(function () {
        LoadSalesInfoTrendChart($("#txtSelection").val(), "enduser_sh");
    });

    $("#salesHistoryDetailsEnduser_rbTrendGraphMonthlySalesHistoryEndUser").change(function () {
        LoadSalesInfoTrendChart($("#txtSelection").val(), "enduser_sh");
    });

    
</script>