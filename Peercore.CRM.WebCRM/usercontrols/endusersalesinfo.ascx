﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="endusersalesinfo.ascx.cs" Inherits="usercontrols_endusersalesinfo" %>
<div>
    <ul id="panelbarcontent1">
        <li class="k-state-active" id="4">Grid
            <div id="gridEndUserSales">
                <%--<div style="float: left; width: 100%; overflow: auto">
                    <div id="gridEndUserSales">
                    </div>
                </div>--%>
            </div>
            <div style="margin-top:20px;height:5px; display:none;">&nbsp;</div>
        </li>

        <li id="5">Graph
            <div id="div_trend1">

            </div>
        </li>
        <li id="6">Trend Graph
            <div>
                <asp:DropDownList ID="DropDownGraphTypeenduser" runat="server" CssClass="dropdownwidth" Width="120px">
                    <asp:ListItem Value="Sales" Text="Sales" Selected="True"></asp:ListItem>
                    <asp:ListItem Value="Tonnes" Text="Tonnes"></asp:ListItem>
                    <asp:ListItem Value="Sales/Tonne" Text="Sales/Tonne" ></asp:ListItem>
                    <asp:ListItem Value="Sales vs Budget ($)" Text="Sales vs Budget ($)" ></asp:ListItem>
                    <asp:ListItem Value="Sales vs Budget (T)" Text="Sales vs Budget (T)" ></asp:ListItem>
                    <asp:ListItem Value="Last Sales vs Budget ($)" Text="Last Sales vs Budget ($)" ></asp:ListItem>
                    <asp:ListItem Value="Last Sales vs Budget (T)" Text="Last Sales vs Budget (T)" ></asp:ListItem>
                </asp:DropDownList>   
                
                <asp:RadioButton runat="server" ID="rbTrendGraphAccumulateenduser" ValidationGroup="A" GroupName="A" Checked="true" /><span>Accumulated</span>
                <asp:RadioButton runat="server" ID="rbTrendGraphMonthlyenduser" ValidationGroup="A" GroupName="A" /><span>Monthly</span>
                             
            </div>
            <div id="div_chart_enduser">
            </div>
        </li>
    </ul>
</div>

<script type="text/javascript">
    $("#panelbarcontent").kendoPanelBar({
        expandMode: "single",
        select: onSelect
    });

    function onSelect(e) {
        var item = $(e.item)[0].id;
        if (item != undefined && item != '') {
            switch (item) {
                case "1":
                    break;
                case "2":
                    break;
                case "3":
                    break;
            }
        }
    }
</script>

