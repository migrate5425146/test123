﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usercontrols_buttonbar_activity : System.Web.UI.UserControl
{
    #region Delegate
    public delegate void ButtonSave(object sender);
    public delegate void ButtonClear(object sender);
    public delegate void ButtonSendtocalendar(object sender);
    public delegate void ButtonSaveAndSendtocalendar(object sender);
    public delegate void ButtonOpportunity(object sender);
    #endregion

    #region Attribute
    public ButtonSave onButtonSave;
    public ButtonClear onButtonClear;
    public ButtonSendtocalendar onButtonSendtocalendar;
    public ButtonSaveAndSendtocalendar onButtonSaveAndSendtocalendar;
    public ButtonOpportunity onButtonOpportunity;
    #endregion

    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void buttonSave_Click(object sender, EventArgs e)
    {
        onButtonSave(this);
    }

    protected void buttonClear_Click(object sender,EventArgs e){
        onButtonClear(this);
    }

    protected void buttonSendtocalendar_Click(object sender,EventArgs e){
        onButtonSendtocalendar(this);
    }

    protected void buttonSaveAndSendtocalendar_Click(object sender,EventArgs e){
        onButtonSaveAndSendtocalendar(this);
    }

    protected void buttonOpportunity_Click(object sender, EventArgs e)
    {
        onButtonOpportunity(this);
    }
    #endregion

    #region Method
    public void EnableSendtoCalendar(bool isEnable)
    {
        if (!isEnable)
        {
            buttonSendtocalendar.Enabled = false;
            buttonSendtocalendar.CssClass = "dissendtocalbtn";
        }
        else
        {
            buttonSendtocalendar.Enabled = true;
            buttonSendtocalendar.CssClass = "sendtocalbtn";
        }
        
    }

    public void EnableSaveAndSendtocalendar(bool isEnable)
    {
        if (!isEnable)
        {
            buttonSaveAndSendtocalendar.Enabled = false;
            buttonSaveAndSendtocalendar.CssClass = "dissaveandsendcal";
        }
        else
        {
            buttonSaveAndSendtocalendar.Enabled = true;
            buttonSaveAndSendtocalendar.CssClass = "saveandsendcal";
        }
    }

    public void EnableSave(bool isEnable)
    {
        if (!isEnable)
        {
            buttinSave.Enabled = false;
            buttinSave.CssClass = "dissavebtn";
        }
        else
        {
            buttinSave.Enabled = true;
            buttinSave.CssClass = "savebtn";
        }
    }

    public void EnableOpportunity(bool isEnable)
    {
        if (!isEnable)
        {
            buttonOpportunity.Enabled = false;
            buttonOpportunity.CssClass = "dissavebtn";
        }
        else
        {
            buttonOpportunity.Enabled = true;
            buttonOpportunity.CssClass = "savebtn";
        }
    }
    #endregion
}