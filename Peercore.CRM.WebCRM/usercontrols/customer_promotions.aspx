﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="customer_promotions.aspx.cs" Inherits="usercontrols_customer_promotions" %>

<!DOCTYPE html>

<html>
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="HiddenFieldCustCode" runat="server" />
    <asp:HiddenField ID="HiddenFieldPromoid" runat="server" />
    <asp:HiddenField ID="HiddenFieldPromoName" runat="server" />
        <div id="modalWindow" style="display:none;height:450px;">
    <div id="div_confirm_message" ></div></div>
    <div id="div_customer_promotions">
        <div id="window">
        </div>
        <div class="tab_button">
            <div class="addlinkdiv" style="float: left" id="div_promo">
                <a id="id_promotions" runat="server" href="#">Add Promotions</a>
            </div>

            <div class="clearall">
            </div>
        </div>
        <div style="min-width: 200px; overflow: auto">
            <div style="float: left; width: 100%; overflow: auto">
                <div id="loadPromotionGrid">
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            var cust_code = $("#HiddenFieldAddress_CustCode").val();
            loadPromotionGrid();
        });

        $("#id_promotions").click(function () {

            //catalog('pipelines_stage/process_forms/processmaster.aspx?fm=oppportunityentry&type=query&querytype=product');
            var wnd = $("#modalWindow").kendoWindow({
                title: "Promotions",
                modal: true,
                visible: false,
                resizable: false,
                width: 550
            }).data("kendoWindow");

            LookupAllPromotions("div_confirm_message", $("#MainContent_HiddenFieldRepType").val());

            loadPromotionGrid();

        });

</script>

    </form>
</body>
</html>
