﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="buttonbar.ascx.cs" Inherits="usercontrols_buttonbar" %>
<div class="buttonbar">
    <asp:Button ID="buttinSave" runat="server" ToolTip="Save" Text="Save" OnClick="buttonSave_Click"
        CssClass="savebtn" Visible="false" />
    <asp:Button ID="buttonFileUpload" runat="server" ToolTip="Upload" Text="Upload" OnClick="buttonFileUpload_Click"
        CssClass="savebtn" Visible="false" />
    <asp:Button ID="buttonApprove" runat="server" ToolTip="Approve" Text="Approve" OnClick="buttonApprove_Click"
        CssClass="approvebtn" Visible="false" />
    <asp:Button ID="buttonDeactivate" runat="server" ToolTip="Clear" Text="Clear" OnClick="buttonDeactivate_Click"
        CssClass="clearbtn" Visible="false" />
    <asp:Button ID="buttonActiveDeactive" Text="Clear" runat="server" OnClick="buttonActiveDeactive_Click"
        CssClass="deactivebtn" Visible="false" />
    <asp:Button ID="buttonSaveandNew" runat="server" ToolTip="Save & New" Text="Save & New"
        CssClass="saveandsend" Visible="false" />
    <asp:Button ID="buttonOpenContactEntry" ToolTip="Open Contact Entry" runat="server"
        Text="New Organisation" Visible="false" OnClick="buttonOpenContactEntry_Click"
        CssClass="openleadentry" ClientIDMode="Static" />
    <asp:Button ID="buttonActivityHistory" ToolTip="Activity History" runat="server"
        Text="Activity History" Visible="false" OnClick="buttonActivityHistory_Click"
        CssClass="activity_history" />
    <asp:Button ID="buttonAddOption" runat="server" ToolTip="Add Options" Text="Add Options"
        CssClass="openleadentry" Visible="false"  />

    <a id="a_add_stock" class="add_options" style="display:none; cursor: pointer;">Stock Upload</a> 
    <a id="a_add_stock_allocation" class="add_options" style="display:none; cursor: pointer;">Stock Allocation</a> 
    <a id="a_add_option" class="add_options" style="display:none; cursor: pointer;">Add Options</a> 
    <a id="a_add_scheme" class="addlinkdiv" style="display:none; cursor: pointer;">Add Scheme</a>
    <a id="a_cancel" class="closebtn" style="display:none; cursor: pointer;">Cancel</a> 

    <asp:Button ID="buttonRecur" ToolTip="Recur" runat="server" Text="Recur" Visible="false"
        CssClass="recurbtn" OnClick="buttonRecur_Click" />
    <asp:Button ID="buttonTemplate" ToolTip="Template" runat="server" Text="Template"
        Visible="false" CssClass="templatebtn" OnClick="buttonTemplate_Click" />
    <asp:Button ID="buttonAddCallCycle" ToolTip="Create Call Cycle Activities" runat="server"
        Text="Create Call Cycle Activities" Visible="false" OnClick="buttonAddCallCycle_Click"
        CssClass="call_cycle_activities" OnClientClick="return showcallcycledialog();" />
    <asp:Button ID="buttonAddleadsCustomer" ToolTip="Add Outlets" runat="server"
        Text="Add Outlets" Visible="false" OnClick="buttonAddleadsCustomer_Click"
        CssClass="add_leads_customer" />
    <asp:Button ID="buttonSaveNewTemplate" ToolTip="Save as New Template" runat="server"
        Text="Save as New Template" Visible="false" OnClick="buttonSaveNewTemplate_Click"
        CssClass="save_new_template" OnClientClick="return showtemplatesavedialog();"/>
    <asp:Button ID="buttonSendMail" ToolTip="Send Mail" runat="server"
        Text="Send Mail" Visible="false" OnClick="buttonSendMail_Click"
        CssClass="send_mail" />
    <asp:Button ID="buttonAddNew" runat="server" ToolTip="Add New" Text="Add New" OnClick="buttonAddNew_Click"
        CssClass="call_cycle_activities" Visible="false" />
</div>
            
