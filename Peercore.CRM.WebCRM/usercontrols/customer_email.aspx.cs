﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;

public partial class usercontrols_customer_email : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        string pathTemp = HttpContext.Current.Request.PhysicalApplicationPath + "\\docs";
        if ((pathTemp.Length > 0) && (Directory.Exists(pathTemp)))
        {
            foreach (string file in Directory.GetFiles(pathTemp))
            {
                try
                {
                    File.Delete(file);
                }
                catch (Exception) { }
            }
        }

        if ((Request.QueryString["eduid"] != null && Request.QueryString["eduid"] != string.Empty) &&
            (Request.QueryString["custid"] != null && Request.QueryString["custid"] != string.Empty))
        {
            HiddenFieldEmail_CustCode.Value = Server.UrlEncode(Request.QueryString["custid"].ToString());
            HiddenFieldEmail_EnduserCode.Value = Server.UrlEncode(Request.QueryString["eduid"].ToString());
        }
        else if (Request.QueryString["custid"] != null && Request.QueryString["custid"] != string.Empty)
        {
            HiddenFieldEmail_CustCode.Value = Server.UrlEncode(Request.QueryString["custid"].ToString());
        }
        else if (Request.QueryString["leadid"] != null && Request.QueryString["leadid"] != string.Empty)
        {
            HiddenFieldEmail_LeadId.Value = Server.UrlEncode(Request.QueryString["leadid"].ToString());
        }
    }
}