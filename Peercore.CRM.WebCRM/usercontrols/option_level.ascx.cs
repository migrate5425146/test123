﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Entities;
using Peercore.CRM.Shared;
using CRMServiceReference;
using Peercore.CRM.Common;
using Telerik.Web.UI;
using System.Drawing;
using System.Data;

public partial class usercontrols_option_level : System.Web.UI.UserControl
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        //{

            if (!IsPostBack)
            {
                LoadData();
            }

        //}
        //else
        //{
        //    Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        //}
    }

    private void LoadData()
    {
        try
        {
            CommonClient commonClient = new CommonClient();

            List<LookupDTO> lookupList = commonClient.GetLookups("DetailsTypeLookup", "", null, null, false);
            lookupList = lookupList.OrderBy(c => c.Code).ToList();
            DropDownListDetailType.DataSource = lookupList;
            DropDownListDetailType.DataTextField = "Description";
            DropDownListDetailType.DataValueField = "Code";
            DropDownListDetailType.DataBind();

            lookupList = commonClient.GetLookups("SchemeTypeLookup", "", null, null, false);

            DropDownListSchemeType.DataSource = lookupList;
            DropDownListSchemeType.DataTextField = "Description";
            DropDownListSchemeType.DataValueField = "Code";
            DropDownListSchemeType.DataBind();


            List<object> objList = new List<object>();
            objList.Add(" ORDER BY packing_name asc ");
            lookupList = commonClient.GetLookups("PackingNameLookup", "", objList, null, false);

            lookupList.Insert(0, new LookupDTO() { Code = "0", Description = "Any" });

            cmdPackingType.DataSource = lookupList;
            cmdPackingType.DataTextField = "Description";
            cmdPackingType.DataValueField = "Code";
            cmdPackingType.DataBind();

            cmdPackingType1.DataSource = lookupList;
            cmdPackingType1.DataTextField = "Description";
            cmdPackingType1.DataValueField = "Code";
            cmdPackingType1.DataBind();


            objList = new List<object>();
            objList.Add("ORDER BY description asc");
            lookupList = commonClient.GetLookups("ProductCategoryLookup", "", objList, null, false);
            lookupList.Insert(0, new LookupDTO() { Code = "0", Description = "Any" });

            cmbItemGroup.DataSource = lookupList;
            cmbItemGroup.DataTextField = "Description";
            cmbItemGroup.DataValueField = "Code";
            cmbItemGroup.DataBind();


            ArgsDTO args = new ArgsDTO();
            //args.ChildOriginators = UserSession.Instance.ChildOriginators;
            //args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            //args.Originator = UserSession.Instance.UserName;
            //args.ManagerMode = true;
            //args.StartIndex = 1;
            //args.RowCount = 100;
            //args.AdditionalParams = "";
            //args.OrderBy = " description asc";

            List<FlavorDTO> flavorList = commonClient.GetAllFlavor();
            flavorList.Insert(0, new FlavorDTO() { FlavorId = 0, FlavorName = "Any" });
            cmbFlavor.DataSource = flavorList;
            cmbFlavor.DataTextField = "FlavorName";
            cmbFlavor.DataValueField = "FlavorId";
            cmbFlavor.DataBind();


            args = new ArgsDTO();
            args.StartIndex = 1;
            args.RowCount = 100;
            args.ROriginator = UserSession.Instance.OriginatorString;
            args.Originator = UserSession.Instance.OriginalUserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.OrderBy = " name asc";

            List<BrandDTO> brandList = commonClient.GetAllBrandsDataAndCount(args);
            brandList.Insert(0, new BrandDTO() { BrandId = 0, BrandName = "Any" });
            cmbBrand.DataSource = brandList;
            cmbBrand.DataTextField = "BrandName";
            cmbBrand.DataValueField = "BrandId";
            cmbBrand.DataBind();
        }
        catch (Exception)
        {
        }
    }
}