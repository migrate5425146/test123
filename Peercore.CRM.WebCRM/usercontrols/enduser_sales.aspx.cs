﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using CRMServiceReference;

public partial class usercontrols_enduser_sales : PageBase
{
    #region Properties
    private KeyValuePair<string, string> EndUserEntry
    {
        get
        {
            if (Session[CommonUtility.ENDUSER_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.ENDUSER_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.ENDUSER_DATA] = value;
        }
    }
    #endregion Properties

    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if ((!string.IsNullOrWhiteSpace(EndUserEntry.Key))
            && (Request.QueryString["custid"] != null && Request.QueryString["custid"] != string.Empty))
        {
            HiddenFieldEndSales_CustCode.Value = Server.UrlEncode(Request.QueryString["custid"].ToString());
            HiddenFieldEndSales_EnduserCode.Value = Server.UrlEncode(EndUserEntry.Key);

            LoadCostYear();

            ArgsDTO argsDTO = new ArgsDTO();
            argsDTO.RepCode =  UserSession.Instance.RepCode;
            argsDTO.CustomerCode =  HiddenFieldEndSales_CustCode.Value;
            argsDTO.EnduserCode =  HiddenFieldEndSales_EnduserCode.Value;
            argsDTO.SMonth = int.Parse(sales_period.SelectedItem.Value);
            argsDTO.SYear = int.Parse(sales_year.SelectedItem.Value);
            Session[CommonUtility.GLOBAL_SETTING] = argsDTO;
        }

        
    }
    
    #endregion

    #region Methods
    private void LoadCostYear()
    {
        // Cost Year & Cost Period
        //Cost Year
        SalesClient salesClient = new SalesClient();
        List<EndUserSalesDTO> salesYear = salesClient.GetCostYears();
        foreach (EndUserSalesDTO item in salesYear)
        {
            sales_year.Items.Add(new ListItem(item.Year.ToString(), item.Year.ToString()));
        }
        
        //Period
        ArgsDTO args = new ArgsDTO();
        if (!string.IsNullOrEmpty(UserSession.Instance.UserName))
        {
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        }

        List<EndUserSalesDTO> salesperiod = salesClient.GetCostPeriod(args);
        foreach (EndUserSalesDTO item in salesperiod)
        {
            sales_period.Items.Add(new ListItem(item.Description.ToString(), item.Period.ToString()));
        }

        EndUserSalesDTO endUserSalesDTO = salesClient.GetCurrentCostPeriod();

        if (endUserSalesDTO != null)
        {
            sales_period.SelectedValue = endUserSalesDTO.Period.ToString();
            sales_year.SelectedValue = endUserSalesDTO.Year.ToString();
        }
    }
    #endregion
}