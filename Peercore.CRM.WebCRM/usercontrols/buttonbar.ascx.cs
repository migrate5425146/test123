﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usercontrols_buttonbar : System.Web.UI.UserControl
{
    #region Delegate
    public delegate void ButtonSave(object sender);
    public delegate void ButtonApprove(object sender);
    public delegate void ButtonClear(object sender);
    public delegate void ButtonOpenContactEntry(object sender);
    public delegate void ButtonActiveDeactive(object sender);
    public delegate void ButtonActivityHistory(object sender);
    public delegate void ButtonRecur(object sender);
    public delegate void ButtonTemplate(object sender);
    public delegate void ButtonAddCallCycle(object sender);
    public delegate void ButtonAddLeadsCustomer(object sender);
    public delegate void ButtonSaveNewTemplate(object sender);
    public delegate void ButtonSendMail(object sender);
    public delegate void ButtonAddOption(object sender);
    public delegate void ButtonAddNew(object sender);
    public delegate void ButtonFileUpload(object sender);

    #endregion

    #region Attribute
    public ButtonSave onButtonSave;
    public ButtonApprove onButtonApprove;
    public ButtonClear onButtonClear;
    public ButtonOpenContactEntry onButtonOpenContactEntry;
    public ButtonActiveDeactive onButtonActiveDeactive;
    public ButtonActivityHistory onButtonActivityHistory;

    public ButtonRecur onButtonRecur;
    public ButtonTemplate onButtonTemplate;
    public ButtonAddCallCycle onButtonAddCallCycle;
    public ButtonAddLeadsCustomer onButtonAddLeadsCustomer;
    public ButtonSaveNewTemplate onButtonSaveNewTemplate;
    public ButtonSendMail onButtonSendMail;
    public ButtonAddOption onButtonAddOption;
    public ButtonAddNew onButtonAddNew;
    public ButtonFileUpload onButtonFileUpload;

    #endregion

    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void buttonSave_Click(object sender, EventArgs e)
    {
        onButtonSave(this);
    }
    
    protected void buttonFileUpload_Click(object sender, EventArgs e)
    {
        onButtonFileUpload(this);
    }

    protected void buttonApprove_Click(object sender, EventArgs e)
    {
       onButtonApprove(this);
    }

    protected void buttonDeactivate_Click(object sender, EventArgs e)
    {
        onButtonClear(this);
    }

    protected void buttonOpenContactEntry_Click(object sender, EventArgs e)
    {
        onButtonOpenContactEntry(this);
    }

    protected void buttonActiveDeactive_Click(object sender, EventArgs e)
    {
        onButtonActiveDeactive(this);
    }

    protected void buttonActivityHistory_Click(object sender, EventArgs e)
    {
        onButtonActivityHistory(this);
    }

    protected void buttonRecur_Click(object sender, EventArgs e)
    {
        onButtonRecur(this);
    }

    protected void buttonTemplate_Click(object sender, EventArgs e)
    {
        onButtonTemplate(this);
    }

    protected void buttonAddCallCycle_Click(object sender, EventArgs e)
    {
        onButtonAddCallCycle(this);
    }

    protected void buttonAddleadsCustomer_Click(object sender, EventArgs e)
    {
        onButtonAddLeadsCustomer(this);
    }

    protected void buttonSaveNewTemplate_Click(object sender, EventArgs e)
    {
        onButtonSaveNewTemplate(this);
    }

    protected void buttonSendMail_Click(object sender, EventArgs e)
    {
        onButtonSendMail(this);
    }


    protected void buttonAddOption_Click(object sender, EventArgs e)
    {
        onButtonAddOption(this);
    }

    protected void buttonAddNew_Click(object sender, EventArgs e)
    {
        onButtonAddNew(this);
    }
    

    #endregion

    #region Method

    public void VisibleAddOption(bool isVisible)
    {
        if (!isVisible)
            buttonAddOption.Visible = false;
        else
            buttonAddOption.Visible = true;
    }

    public void VisibleSave(bool isVisible)
    {
        if (!isVisible)
            buttinSave.Visible = false;
        else
            buttinSave.Visible = true;
    }
    
    public void VisibleFileUpload(bool isVisible)
    {
        if (!isVisible)
            buttonFileUpload.Visible = false;
        else
            buttonFileUpload.Visible = true;
    }

    public void VisibleApprove(bool isVisible)
    {
        if (!isVisible)
            buttonApprove.Visible = false;
        else
            buttonApprove.Visible = true;
    }

    public void VisibleSaveandNew(bool isVisible)
    {
        if (!isVisible)
            buttonSaveandNew.Visible = false;
        else
            buttonSaveandNew.Visible = true;
    }

    public void VisibleDeactivate(bool isVisible)
    {
        if (!isVisible)
            buttonDeactivate.Visible = false;
        else
            buttonDeactivate.Visible = true;
    }

    public void VisibleOpenContact(bool isVisible)
    {
        if (!isVisible)
            buttonOpenContactEntry.Visible = false;
        else
            buttonOpenContactEntry.Visible = true;
    }

    public void VisibleActiveDeactive(bool isVisible)
    {
        if (!isVisible)
            buttonActiveDeactive.Visible = false;
        else
            buttonActiveDeactive.Visible = true;
    }

    public void VisibleActivityHistory(bool isVisible)
    {
        if (!isVisible)
            buttonActivityHistory.Visible = false;
        else
            buttonActivityHistory.Visible = true;
    }

    public void VisibleRecur(bool isVisible)
    {
        if (!isVisible)
            buttonRecur.Visible = false;
        else
            buttonRecur.Visible = true;
    }

    public void VisibleTemplate(bool isVisible)
    {
        if (!isVisible)
            buttonTemplate.Visible = false;
        else
            buttonTemplate.Visible = true;
    }

    public void VisibleAddCallCycle(bool isVisible)
    {
        if (!isVisible)
            buttonAddCallCycle.Visible = false;
        else
            buttonAddCallCycle.Visible = true;
    }

    public void VisibleAddLeadsCustomer(bool isVisible)
    {
        if (!isVisible)
            buttonAddleadsCustomer.Visible = false;
        else
            buttonAddleadsCustomer.Visible = true;
    }

    public void VisibleSaveNewTemplate(bool isVisible)
    {
        if (!isVisible)
            buttonSaveNewTemplate.Visible = false;
        else
            buttonSaveNewTemplate.Visible = true;
    }

    public void VisibleSendMail(bool isVisible)
    {
        if (!isVisible)
            buttonSendMail.Visible = false;
        else
            buttonSendMail.Visible = true;
    }

    public void VisibleAddNew(bool isVisible)
    {
        if (!isVisible)
            buttonAddNew.Visible = false;
        else
            buttonAddNew.Visible = true;
    }


    public void EnableSave(bool isVisible)
    {
        if (!isVisible)
        {
            buttinSave.Enabled = false;
            buttinSave.CssClass = "dissavebtn";
        }
        else
        {
            buttinSave.Enabled = true;
            buttinSave.CssClass = "savebtn";
        }
    }

    public void EnableApprove(bool isVisible)
    {
        if (!isVisible)
        {
            buttonApprove.Enabled = false;
            buttonApprove.CssClass = "dissavebtn";
        }
        else
        {
            buttonApprove.Enabled = true;
            buttonApprove.CssClass = "savebtn";
        }
    }

    public void EnableSaveandNew(bool isVisible)
    {
        if (!isVisible)
        {
            buttonSaveandNew.Enabled = false;
            buttonSaveandNew.CssClass = "dissaveandsend";
        }
        else
        {
            buttonSaveandNew.Enabled = true;
            buttonSaveandNew.CssClass = "saveandsend";
        }
    }

    public void EnableDeactivate(bool isVisible)
    {
        if (!isVisible)
        {
            buttonDeactivate.Enabled = false;
            buttonSaveandNew.CssClass = "disclearbtn";
        }
        else
        {
            buttonDeactivate.Enabled = true;
            buttonSaveandNew.CssClass = "clearbtn";
        }
    }

    public void EnableOpenContact(bool isVisible)
    {
        if (!isVisible)
        {
            buttonOpenContactEntry.Enabled = false;
            buttonOpenContactEntry.CssClass = "disopenleadentry";
        }
        else
        {
            buttonOpenContactEntry.Enabled = true;
            buttonOpenContactEntry.CssClass = "openleadentry";
        }
    }

    public void EnableActiveDeactive(bool isVisible)
    {
        if (!isVisible)
        {
            buttonActiveDeactive.Enabled = false;
            buttonActiveDeactive.CssClass = "disdeactivebtn";
        }
        else
        {
            buttonActiveDeactive.Enabled = true;
            buttonActiveDeactive.CssClass = "deactivebtn";
        }
    }

    public void EnableActivityHistory(bool isVisible)
    {
        if (!isVisible)
        {
            buttonActivityHistory.Enabled = false;
            buttonActivityHistory.CssClass = "disactivity_history";
        }
        else
        {
            buttonActivityHistory.Enabled = true;
            buttonActivityHistory.CssClass = "activity_history";
        }
    }

    public void EnableRecur(bool isVisible)
    {
        if (!isVisible)
        {
            buttonRecur.Enabled = false;
        }
        else
        {
            buttonRecur.Enabled = true;
        }
    }

    public void EnableTemplate(bool isVisible)
    {
        if (!isVisible)
        {
            buttonTemplate.Enabled = false;
            buttonTemplate.CssClass = "distemplatebtn";
        }
        else
        {
            buttonTemplate.Enabled = true;
            buttonTemplate.CssClass = "templatebtn";
        }
    }

    public void EnableAddCallCycle(bool isVisible)
    {
        if (!isVisible)
        {
            buttonAddCallCycle.Enabled = false;
            buttonAddCallCycle.CssClass = "discall_cycle_activities";
        }
        else
        {
            buttonAddCallCycle.Enabled = true;
            buttonAddCallCycle.CssClass = "call_cycle_activities";
        }
    }

    public void EnableAddLeadsCustomer(bool isVisible)
    {
        if (!isVisible)
        {
            buttonAddleadsCustomer.Enabled = false;
            buttonAddleadsCustomer.CssClass = "disadd_leads_customer";
        }
        else
        {
            buttonAddleadsCustomer.Enabled = true;
            buttonAddleadsCustomer.CssClass = "add_leads_customer";
        }
    }

    public void EnableSaveNewTemplate(bool isVisible)
    {
        if (!isVisible)
        {
            buttonSaveNewTemplate.Enabled = false;
            buttonSaveNewTemplate.CssClass = "dissave_new_template";
        }
        else
        {
            buttonSaveNewTemplate.Enabled = true;
            buttonSaveNewTemplate.CssClass = "save_new_template";
        }
    }

    public void SetActiveDeactive(bool isActive)
    {
        if (!isActive)
        {
            buttonActiveDeactive.Text = "De-Activate";
            buttonActiveDeactive.CssClass = "deactivebtn";
        }
        else
        {
            buttonActiveDeactive.Text = "Activate";
            buttonActiveDeactive.CssClass = "activebtn";
        }
    }

    public void EnableAddOption(bool isEnable)
    {
        if (!isEnable)
        {
            buttonAddOption.Enabled = false;
            buttonAddOption.CssClass = "disopenleadentry";
        }
        else
        {
            buttonAddOption.Enabled = true;
            buttonAddOption.CssClass = "openleadentry";
        }
    }
    #endregion
}