﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class usercontrols_buttonbar_navigation : System.Web.UI.UserControl
{
    #region Delegate
    public delegate void ButtonNext(object sender);
    public delegate void ButtonPrevious(object sender);


    #endregion

    #region Attribute
    public ButtonNext onButtonNext;
    public ButtonPrevious onButtonPrevious;


    #endregion

    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void buttonNext_Click(object sender, EventArgs e)
    {
        onButtonNext(this);
    }

    protected void buttonPrevious_Click(object sender, EventArgs e)
    {
        onButtonPrevious(this);
    }



    #endregion

    #region Method

    public void VisibleNext(bool isVisible)
    {
        if (!isVisible)
            buttonNext.Visible = false;
        else
            buttonNext.Visible = true;
    }

    public void VisiblePrevious(bool isVisible)
    {
        if (!isVisible)
            buttonPrevious.Visible = false;
        else
            buttonPrevious.Visible = true;
    }



    #endregion
}