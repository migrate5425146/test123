﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="enduser_activity.aspx.cs"
    Inherits="usercontrols_enduser_activity" %>

<!DOCTYPE html>
<html>
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="div_enduser_activity">
        <asp:HiddenField ID="HiddenFieldActivity_CustCode" runat="server" />
        <asp:HiddenField ID="HiddenFieldActivity_EnduserCode" runat="server" />

        <div id="div_customer_contactperson">
            <div class="enduser_tab">
                <div class="addlinkdiv" style="float: left" id="div_activity">
                    <a id="id_activity" runat="server" href="#">Add Activity</a>
                </div>
                
                <div class="div_datelable" style="float:right; margin-top:5px;">
                    <div style="width: 15px; height: 15px; float: right; background-color: #d8bfd8; border: 1px solid #000; margin-left:5px;">
                    </div>
                    <strong>Sent To Calendar</strong>

            </div>
             <div class="clearall">
            &nbsp;</div>
            <div style="min-width: 200px; overflow: auto">
                <div style="float: left; width: 100%; overflow: auto">
                    <div id="EndUserActivityGrid">
                    </div>
                </div>
            </div>
        </div>
    </div>
     <script type="text/javascript">
         $(document).ready(function () {
             var cust_code = $("#HiddenFieldActivity_CustCode").val();
             var enduser_code = $("#HiddenFieldActivity_EnduserCode").val();

             if ((cust_code != null && cust_code != '') && (enduser_code != null && enduser_code != ''))
                 loadEnduserActivityGrid(cust_code, enduser_code, 'EndUserActivityGrid');
         });
    </script>
    </form>
</body>
</html>
