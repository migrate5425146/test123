﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="PipelineChart.ascx.cs" Inherits="usercontrols_PipelineChart" %>

<table style="width: 100%; height: 100%" border="0">
	<tr>
		<td style="width: 100%; height: 100%">
			<asp:chart id="ChartPipeline" runat="server" Palette="BrightPastel" Width="266px" Height="330px" BackColor="Transparent"
            BorderColor="Transparent" BorderWidth="2" BorderlineDashStyle="NotSet" ImageLocation="~/TempImages/ChartPic_#SEQ(245,0)" 
            BackSecondaryColor="#3e3e3e" BackGradientStyle="TopBottom" ImageType="Png" 
                onclick="ChartPipeline_Click" >

				<titles>
					<asp:Title ShadowColor="Transparent" Font="Trebuchet MS, 12.25pt, style=Bold" ShadowOffset="0" Text="Pipeline" ForeColor="White" >
						<position Y="3" Height="6.470197" Width="90"></position>
					</asp:Title>
				</titles>
				<legends>
					<asp:Legend Enabled="true" IsTextAutoFit="false"  Name="Default" BackColor="Transparent" Alignment="Center" ForeColor="White"
                     Docking="Bottom" Font="Trebuchet MS, 8.25pt, style=Bold">
                    </asp:Legend>
				</legends>
				<%--<borderskin SkinStyle="Emboss"></borderskin>--%>
				<series>
					<asp:Series IsValueShownAsLabel="True" XValueType="Double" Name="Default" ChartType="Funnel" IsVisibleInLegend = "true" 
                    CustomProperties="FunnelMinPointHeight=1" BorderColor="180, 26, 59, 105" LabelFormat="F1" YValueType="Double" Font="Trebuchet MS, 9.25pt, style=Bold" LabelForeColor="White" >
						<points>
						</points>
					</asp:Series>
				</series>
				<chartareas>
					<asp:ChartArea Name="ChartArea1" BorderColor="64, 64, 64, 64" BackSecondaryColor="Transparent" BackColor="Transparent" ShadowColor="Transparent" BorderWidth="0">
						<area3dstyle Rotation="10" Perspective="0" Inclination="0" IsRightAngleAxes="False" WallWidth="0" IsClustered="False"  />
						<position Y="12" Height="65" Width="100" X="5"></position>
						<axisy LineColor="64, 64, 64, 64">
							<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" ForeColor="White"/>
							<MajorGrid LineColor="64, 64, 64, 64" />
						</axisy>
						<axisx LineColor="64, 64, 64, 64">
							<LabelStyle Font="Trebuchet MS, 8.25pt, style=Bold" ForeColor="White"/>
							<MajorGrid LineColor="64, 64, 64, 64" />
						</axisx>
					</asp:ChartArea>
				</chartareas>
			</asp:chart>
		</td>
    </tr>
    <tr>
		<td align="center">
            <asp:RadioButtonList ID="rblCustomer" runat="server" AutoPostBack="True" Visible="false"
                RepeatDirection="Horizontal"
                onselectedindexchanged="rblCustomer_SelectedIndexChanged" 
                ForeColor="Black">
                <asp:ListItem Selected="true">Prospect</asp:ListItem>
                <asp:ListItem>Customer</asp:ListItem>
            </asp:RadioButtonList>
        </td>
	</tr>
</table>

<script type="text/javascript" >
    
</script>

