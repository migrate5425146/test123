﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using System.Globalization;
using Telerik.Web.UI;

public partial class usercontrols_buttonbar_analysisgraph : System.Web.UI.UserControl
{
    #region Delegate
    public delegate void ButtonFilter(object sender);
    public delegate void ButtonExport(object sender);
    //public delegate void DropDownListMonth(object sender);
    //public delegate void CheckBoxCheckedChanged(object sender);
    //public delegate void DropDownListFilterBy(object sender);
    public delegate void DropDownListReps(object sender);
    public delegate void DropDownListSector(object sender);
    
    #endregion

    #region Attribute
    public ButtonFilter onButtonFilter;
    public ButtonExport onButtonExport;
    //public DropDownListMonth onDropDownListMonth;
    //public CheckBoxCheckedChanged onCheckBoxCheckedChanged;
    //public DropDownListFilterBy onDropDownListFilterBy;
    public DropDownListReps onDropDownListReps;
    public DropDownListSector onDropDownListSector;

    #endregion

    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //if (dtpFromDate.SelectedDate == null)
            //{
            //    dtpFromDate.SelectedDate = DateTime.Now.Date;
            //    dtpToDate.SelectedDate = DateTime.Now.Date;
            //}
        }
    }

    //protected void cmbMonth_SelectionChanged(object sender, EventArgs e)
    //{
    //    onDropDownListMonth(this);
    //}
    
    protected void btnFilter_Click(object sender, EventArgs e)
    {
        onButtonFilter(this);
    }
    
    protected void btnexport_Click(object sender, EventArgs e)
    {
        onButtonExport(this);
    }

    //protected void chk1_CheckedChanged(object sender, EventArgs e)
    //{
    //    onCheckBoxCheckedChanged(this);
    //}

    //protected void cmbFilterBy_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    onDropDownListFilterBy(this);
    //}

    protected void cmbReps_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            onDropDownListReps(this);
        }
        catch (Exception ex)
        {
        }
    }

    protected void cmbSector_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            onDropDownListSector(this);
        }
        catch (Exception ex)
        {
        }
    }

    
    #endregion

    #region Method

    public void SetReps(List<OriginatorDTO> listOriginator, string valueField = "UserName")
    {
        cmbReps.DataSource = listOriginator;
        cmbReps.DataTextField = "Name";
        cmbReps.DataValueField = valueField;
        cmbReps.DataBind();
    }


    public void SetRepsSelect(string userName)
    {
        cmbReps.SelectedValue = userName;
    } 

    public string GetRepsValue()
    {
        return cmbReps.SelectedItem.Value;
    }

    public void SetSector(List<OriginatorDTO> listOriginator)
    {
        cmbSector.DataSource = listOriginator;
        cmbSector.DataTextField = "RepCode";
        cmbSector.DataValueField = "RepCode";
        cmbSector.DataBind();
    }


    public void SetSectorSelect(string userName)
    {
        cmbSector.SelectedItem.Value = userName;
    }

    public string GetSectorValue()
    {
        return cmbSector.SelectedItem.Value;
    }

    public void SetDateRangeSelect(string dateRange)
    {
        cmbDateRange.SelectedItem.Text = dateRange;
    }

    public string GetDateRangeText()
    {
        return cmbDateRange.SelectedItem.Text;
    }

    public string GetFromDateText()
    {
        return dtpFromDate.Text;
    }

    public int GetDateRangeIndex()
    {
        return cmbDateRange.SelectedIndex;
    }

    public void SetFromDateText(string fromDate)
    {
        dtpFromDate.Text = fromDate;
    }

    public string GetToDateText()
    {
        return dtpToDate.Text;
    }

    public void SetToDateText(string toDate)
    {
        dtpToDate.Text = toDate; 
    }

    public void SetStageLoad(List<StateDTO> listStage)
    {
        cmbState.DataSource = listStage;
        cmbState.DataTextField = "StateName";
        cmbState.DataValueField = "StateName";
        cmbState.DataBind();
    }

    public void SetStateSelect(string stateName)
    {
        cmbState.SelectedValue = stateName;
    }

    public string GetStateValue()
    {
        return cmbState.SelectedItem.Value;
    }


    public void SetMarketLoad(List<LookupTableDTO> listLookupTable)
    {
        cmbMarket.DataSource = listLookupTable;
        cmbMarket.DataTextField = "TableDescription";
        cmbMarket.DataValueField = "TableCode";
        cmbMarket.DataBind();
    }

    public void SetMarketSelect(string stateName)
    {
        cmbMarket.SelectedValue = stateName;
    }

    public string GetMarketValue()
    {
        return cmbMarket.SelectedItem.Value;
    }


    public void SetCostPeriodLoad(List<EndUserSalesDTO> endUserSalesList)
    {
        cmbCostPeriod.DataSource = endUserSalesList;
        cmbCostPeriod.DataTextField = "Description";
        cmbCostPeriod.DataValueField = "Period";
        cmbCostPeriod.DataBind();
    }

    public void SetCostPeriodSelect(string period)
    {
        cmbCostPeriod.SelectedValue = period;
    }

    public string GetCostPeriodValue()
    {
        return cmbCostPeriod.SelectedItem.Value;
    }

    public void SetCostYearLoad(List<EndUserSalesDTO> endUserSalesList)
    {
        cmbCostYear.DataSource = endUserSalesList;
        cmbCostYear.DataTextField = "Year";
        cmbCostYear.DataValueField = "Year";
        cmbCostYear.DataBind();
    }

    public void SetCostYearSelect(string period)
    {
        cmbCostYear.SelectedValue = period;
    }

    public string GetCostYearValue()
    {
        return cmbCostYear.SelectedItem.Value;
    }

    public string GetDistributorCode()
    {
        return HiddenCustCode.Value;
    }

    public string GetEndUserCode()
    {
        return txtEndUser.Text;
    }

    public string GetSummaryReport()
    {
        return cmbSummaryReport.SelectedItem.Value;
    }

    public bool GetProducts()
    {
        return chkProducts.Checked;
    }

    public bool GetActiveEndUsers()
    {
        return chkActiveEndUsers.Checked;
    }
    
    public void SetVisible(int rptId)
    {
        switch (rptId)
        {
            case 1:
                lblRepGroup.Visible = true;
                cmbReps.Visible = true;
                lbSector.Visible = true;
                cmbSector.Visible = true;
                break;
            case 2:
                cmbDateRange.Visible = true;
                lbStartDate.Visible = true;
                lbEndDate.Visible = true;
                break;

            case 3:
                lblRepGroup.Visible = true;
                cmbReps.Visible = true;
                cmbReps.AutoPostBack = false;
                lbState.Visible = true;
                cmbState.Visible = true;
                lbMarket.Visible = true;
                cmbMarket.Visible = true;
                lbStartDate.Visible = true;
                lbEndDate.Visible = true;
                 //dtpToDate.Text = DateTime.Today.AddYears(-1).ToString("dd-MMM-yyyy");
                 //dtpFromDate.Text = DateTime.Today.ToString("dd-MMM-yyyy");

                break;
            case 4:
                lblRepGroup.Visible = true;
                cmbReps.Visible = true;
                cmbReps.AutoPostBack = false;

                lbCostPeriod.Visible = true;
                cmbCostPeriod.Visible = true;
                lbCostYear.Visible = true;
                cmbCostYear.Visible = true;

                lbDistributor.Visible = true;
                txtnewcustcode.Visible = true;
                lbEndUser.Visible = true;
                txtEndUser.Visible = true;

                chkProducts.Visible = true;
                chkActiveEndUsers.Visible = true;
                break;
            case 5:
                //lblRepGroup.Visible = true;
                cmbReps.Visible = true;
                cmbReps.AutoPostBack = false;

                cmbSummaryReport.Visible = true;

                //lbDistributor.Visible = true;
                txtnewcustcode.Visible = true;
                break;
            
        }
    }

    

    #endregion
}
