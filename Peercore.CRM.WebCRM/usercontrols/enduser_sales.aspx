﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="enduser_sales.aspx.cs" Inherits="usercontrols_enduser_sales" %>

<!DOCTYPE html>

<html>
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <asp:HiddenField ID="HiddenFieldEndSales_CustCode" runat="server" />
        <asp:HiddenField ID="HiddenFieldEndSales_EnduserCode" runat="server" />
    <div id="div_customer_contactperson">
        <div class="enduser_tab">
                <div class="div_lable">
                    Year</div>
                <div class="div_lable">
                    :
                    <asp:DropDownList ID="sales_year" runat="server"></asp:DropDownList>
                </div>
                <div class="div_lable">
                    Period</div>
                <div class="div_lable">
                    :
                   <asp:DropDownList ID="sales_period" runat="server"></asp:DropDownList>
                </div>
                <div class="div_lable">
                    Include Products with No Sales</div>
                <div class="div_lable">
                    :
                   <asp:CheckBox ID="chkincludeproduct" runat="server" Checked="true" />
                </div>
                <div class="div_lable"><a runat="server" id="link_search" class="searchbtn" onclick="link_search_onclick()">Search</a>
                </div>
        </div>
        <div style="min-width: 200px; overflow: auto">
            <div style="float: left; width: 100%; overflow: auto">
                <div id="gridEndUserEnquiry">
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function () {
            var cust_code = $("#HiddenFieldContactPerson_CustCode").val();
            var year = $("#sales_year").val();
            var period = $("#sales_period").val();
            //var incproduct = $("#chkincludeproduct option:selected").val();
            var incproduct = $('#chkincludeproduct').attr('checked') ? "1" : "0";


            $("a#link_search").click(function () {
                // loadEnduserSales(cust_code, year, period, incproduct, 'EndUserPriceGrid');
            });

            //loadEnduserSales(cust_code, year, period, incproduct, 'EndUserSalesGrid');
            fiterEndUserSales(period, year);
        });

        function link_search_onclick() {
            fiterEndUserSales($("#sales_period").val(), $("#sales_year").val());
        }

        function fiterEndUserSales(month, year) {
            var incproduct = $('#chkincludeproduct').attr('checked') ? "1" : "0";
            var url = ROOT_PATH + "lead_customer/process_forms/processmaster.aspx?fm=endusersales&type=report&month=" + month + "&year=" + year;
            // url = encodeurl(url);
            //$("#div_loader").show();
            $.ajax({
                url: url,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    GridEndUserEnquiry(month, year, incproduct, 1);
                },
                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg; // "Sorry there was an error: ";
                        $("#div_loader").hide();
                        //$("#" + tagertdiv).html(msg);
                    }
                }
            });
        }
    </script>
    </form>
</body>
</html>
