﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="today_appointments.ascx.cs" Inherits="usercontrols_today_appointments" %>
<div id="div_content_today_appointments" runat="server" class="divhidden">
        <div class="demo-section">
            <div id="listView_today_appointments">
            </div>
            <div id="pager_today_appointments" class="k-pa ger-wrap" style="display:none">
            </div>
        </div>
        <script type="text/x-kendo-tmpl" id="appointments_today_template">
             <div class="product">
                <div class="container"> 
                    <h3><a href='activity_planner/transaction/activity_scheduler.aspx?fm=home&sdate=#:kendo.toString(StartTime, "yyyy/MMM/dd" )#'>#:Subject#</a></h3>
                    <p><span>From :</span> #:kendo.toString(StartTime, "dd/MMM/yyyy hh:mm tt" )# -  #:kendo.toString(EndTime, "dd/MMM/yyyy hh:mm tt" )#</p>
                </div>
            </div>
        </script>
    </div>

     <script type="text/javascript">
         $("#div_content_today_appointments").removeAttr("class");
         $("#div_content_today_appointments").attr("class", "divshow");
         
         $("#todaygapp_div_content_today_appointments").attr("class", "divshow");

         $(document).ready(function () {
             AppointmentsActivityToday();
         });

         function AppointmentsDataSource() {
             var dataSource = new kendo.data.DataSource({
                 transport: {
                     read: {
                         type: "POST",
                         url: ROOT_PATH + "service/lead_customer/common.asmx/GetTodayAppointmentsForHome",
                         contentType: 'application/json; charset=utf-8',
                         datatype: "json"
                     }, //read
                     parameterMap: function (data, operation) {
                         return JSON.stringify(data);
                     }
                 }, // transport
                 serverFiltering: true,
                 serverSorting: true,
                 serverPaging: true,
                 pageSize: 10,
                 schema: {
                     data: "d.Data",
                     total: "d.Total",
                     model: { // define the model of the data source. Required for validation and property types.
                         //id: "SourceId",
                         fields: {
                             StartTime: { type: "Date", format: "{0: yyyy-MM-dd}" },
                             EndTime: { type: "Date", format: "{0: yyyy-MM-dd}" }
                         }
                     }
                 } // schema
             });

             return dataSource;
         }

         function AppointmentsActivityToday() {
             var dataSource = AppointmentsDataSource();
             
             $("#listView_today_appointments").html("");
             $("#listView_today_appointments").kendoGrid({
                 
                 columns: [
                    { template: kendo.template($("#appointments_today_template").html())
                        , headerAttributes: {
                            style: "display: none"
                        }
                    }
                ],
                 editable: false, // enable editing
                 pageable: true,
                 sortable: false,
                 filterable: false,
                 dataSource: dataSource,
                 dataBound: todayapointment_dataBound
                 //rowTemplate: kendo.template($("#outstanding_template").html())
             });
         }

         function todayapointment_dataBound(e) {
             var grid = $('#listView_today_appointments').data('kendoGrid');

             if (grid.dataSource == null || grid.dataSource._total == 0) {
                 $("div#listView_today_appointments div.k-grid-content").css("height", "15px");
             }
             else {
                 $("div#listView_today_appointments div.k-grid-content").css("height", "465px");
             }
         }

    </script>