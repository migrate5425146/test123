﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;

public partial class usercontrols_contactaddress : System.Web.UI.Page
{
    KeyValuePair<string, string> LeadEntry = new KeyValuePair<string, string>();
    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session[CommonUtility.LEAD_DATA] != null)
        {
            LeadEntry = (KeyValuePair<string, string>)Session[CommonUtility.LEAD_DATA];
        }

        if (!string.IsNullOrWhiteSpace(LeadEntry.Key))
            id_address.HRef = "javascript:addAddress('lead_customer/process_forms/processmaster.aspx?fm=contactdetails&type=insert&leadid=" + LeadEntry.Key + "','')";
        else
        {
            id_address.HRef = "javascript:void('0')";
        }

    }
    #endregion
}