﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Common;

public partial class usercontrols_customer_documents : PageBase
{
    #region Properties
    private KeyValuePair<string, string> CustomerEntry
    {
        get
        {
            if (Session[CommonUtility.CUSTOMER_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.CUSTOMER_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.CUSTOMER_DATA] = value;
        }
    }
    #endregion Properties

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(CustomerEntry.Key))
        {
            HiddenFieldDocument_CustCode.Value = CustomerEntry.Key;
        }
    }
}