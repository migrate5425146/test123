﻿<%@ Control Language="C#" AutoEventWireup="true" CodeFile="customer_prevcode.ascx.cs" Inherits="usercontrols_customer_prevcode" %>


<telerik:RadGrid runat="server" ID="radCustomerprevcode" AllowPaging="true" 
    AllowSorting="true" AutoGenerateColumns="false" PageSize="10" AllowCustomPaging="true"
    AllowAutomaticInserts="False" AllowFilteringByColumn="true" Skin="CookersSkin" EnableEmbeddedSkins="false" >

    
    <MasterTableView  AllowMultiColumnSorting="true" EnableNoRecordsTemplate="true" TableLayout="Fixed">
        <NoRecordsTemplate>
            <div>There are no records to display</div>
        </NoRecordsTemplate>
        <Columns>
            <telerik:GridBoundColumn DataField="Code" SortExpression="cust_code" UniqueName="cust_code"
                HeaderText="Code" DataType="System.String"/>
            <telerik:GridBoundColumn DataField="Description" SortExpression="name" UniqueName="name"
                HeaderText="Description" DataType="System.String" />
         </Columns>
    </MasterTableView>

    <PagerStyle AlwaysVisible="true" Mode="NumericPages" />

    <ClientSettings>
        <%--<ClientEvents OnRowDataBound="radCustomerActivity_RowDataBound" OnDataBound="radCustomerActivity_OnDataBound" />--%>
        <DataBinding Location="../../Service/lead_customer/customer_entry_Service.asmx"
            SelectMethod="GetPrevCustomerDataAndCount" EnableCaching="true" />
    </ClientSettings>
    </telerik:RadGrid>

<telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanelActivity" runat="server" />   
 
<telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">

    <script type="text/javascript">



    </script>
</telerik:RadCodeBlock>
