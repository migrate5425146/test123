﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="customer_enduser.aspx.cs" Inherits="usercontrols_customer_enduser" %>

<!DOCTYPE html>

<html>
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div id="div_customer_enduser">
        <asp:HiddenField ID="HiddenFieldEndUser_CustCode" runat="server" />
        <asp:HiddenField ID="HiddenFieldFormType" runat="server" />
        <div id="div_customer_contactperson">
        
         <div id="cust_window">
            
        </div>
            <div id="newcust_window">
                <div id="div_newcustgrid">
                </div>
               
            </div>
            <div class="enduser_tab">
                <div class="addlinkdiv" style="float: left" id="div_contactperson">
                    <a id="id_enduser" runat="server" href="#">Add End User</a>
                </div>
               
                <div class="maplink" style="float: left" id="div_map">
                    <a id="id_transferenduser" runat="server" href="#">Transfer End User</a>
                </div>
                
                <div style="float: right" id="div1" class="div_datelable">
                    <asp:DropDownList ID="DropDownListStatus" runat="server" AppendDataBoundItems="true">
                        <asp:ListItem Text="All" Value="ALL"></asp:ListItem>
                        <asp:ListItem Text="Active" Selected="True" Value="Active"></asp:ListItem>
                        <asp:ListItem Text="Inactive" Value="Inactive"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="clearall">
               </div>
            <div style="min-width: 200px; overflow: auto">
                <div style="float: left; width: 100%; overflow: auto">
                    <div id="CustomerEndUser">
                    </div>
                </div>
            </div>
        </div>
    </div>
     <script type="text/javascript">
         $(document).ready(function () {
             var cust_code = $("#HiddenFieldEndUser_CustCode").val();
             var status = $("#DropDownListStatus").val();
             var type = $("#HiddenFieldFormType").val();
             loadCustomerEndUserGrid(cust_code, 'CustomerEndUser', status, type);

             var cust_window = $("#cust_window"),
                        cust_undo = $("#undo")
                                .bind("click", function () {
                                    cust_window.data("kendoWindow").open();
                                    cust_undo.hide();
                                });

             var onClose = function () {
                 cust_undo.show();
             }

             if (!cust_window.data("kendoWindow")) {
                 cust_window.kendoWindow({
                     width: "380px",
                     title: "Transfer End User",
                     close: onClose
                 });
             }

             var newcust_window = $("#newcust_window"),
                        newcust_undo = $("#undo")
                                .bind("click", function () {
                                    newcust_window.data("kendoWindow").open();
                                    newcust_undo.hide();
                                });

             var onClose = function () {
                 newcust_undo.show();
             }

             if (!newcust_window.data("kendoWindow")) {
                 newcust_window.kendoWindow({
                     width: "500px",
                     height: "350px",
                     title: "Transfer End User",
                     close: onClose
                 });
             }

             var cust_window = $("#cust_window").data("kendoWindow");
             cust_window.center();

             var newcust_window = $("#newcust_window").data("kendoWindow");
             newcust_window.center();
         });
        </script>
    </form>
</body>
</html>
