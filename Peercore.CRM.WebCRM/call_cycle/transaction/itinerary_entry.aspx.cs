﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Common;
using System.Web.Script.Serialization;
using CRMServiceReference;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Spreadsheet;
using SpreadsheetLight;
using SpreadsheetLight.Drawing;
using Peercore.CRM.Shared;
using System.Globalization;
using System.Text;

public partial class call_cycle_transaction_itinerary_entry : PageBase
{

    #region -Events-

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
            {
                buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
                //buttonbar.onButtonSaveAndExportExcel = new usercontrols_buttonbar.ButtonSaveAndExportExcel(ButtonSaveAndExportExcel_Click);

                buttonbar.VisibleSave(true);
                //buttonbar.VisibleSaveAndExportExcel(true);

                if (!IsPostBack)
                {
                    SetYearDropDown();
                    LoadGridData();
                    LoadRep();
                    LoadUIData();
                    
                }

                //Remove the session before setting the breadcrumb.
                Session.Remove(CommonUtility.BREADCRUMB);
                Master.SetBreadCrumb("Itinerary Entry ", "#", "");

            }
            else
            {
                Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void ButtonSave_Click(object sender)
    {
        CommonUtility commonUtility = new CommonUtility();
        bool status = this.SaveItinerary(false);

        if (status == true)
        {
            LoadGridData();
            div_message.Attributes.Add("style", "display:block;padding:4px");
            div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved !");

        }
        else
        {
            div_message.Attributes.Add("style", "display:block;padding:4px");
            div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    public bool SaveItinerary(bool hasSentmail)
    {
        // CommonUtility commonUtility = new CommonUtility();
        try
        {
            bool isSuccess = false;
            CallCycleClient callCycleService = new CallCycleClient();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = "";

            List<ItineraryDetailDTO> itineraryList = new List<ItineraryDetailDTO>();
            ItineraryHeaderDTO sessionHeader = (ItineraryHeaderDTO)Session[CommonUtility.ITINERARY_HEADER_DATA];

            ItineraryHeaderDTO itineraryHeader = new ItineraryHeaderDTO();
            itineraryHeader.CreatedBy = UserSession.Instance.UserName;
            itineraryHeader.ItineraryName = !string.IsNullOrEmpty(DropDownListRep.Text) ? DropDownListRep.Text : UserSession.Instance.Originator;
            itineraryHeader.ItineraryHeaderId = sessionHeader.ItineraryHeaderId;
            itineraryHeader.CallCycleId = sessionHeader.CallCycleId;
            itineraryHeader.HasSentMail = hasSentmail;
            itineraryHeader.Status = sessionHeader.Status;
            itineraryHeader.BaseTown = TxtBaseTown.Text;
            itineraryHeader.AssignedTo = UserSession.Instance.UserName;

            itineraryHeader.PrivateField = Convert.ToInt32(!String.IsNullOrEmpty(txtPrivate.Text) ? txtPrivate.Text : "0");
            itineraryHeader.Contigency = Convert.ToInt32(!String.IsNullOrEmpty(txtContigency.Text) ? txtContigency.Text : "0");


            jsonString = @"" + HiddenFieldItineraryDetails.Value.Replace("\"ExtensionData\":{},", "");
            itineraryList = serializer.Deserialize<List<ItineraryDetailDTO>>(jsonString);

            itineraryHeader.ItineraryDetailList = itineraryList;
            itineraryHeader.EffDate = new DateTime(itineraryList[0].Date.Year, itineraryList[0].Date.Month, 1);

            isSuccess = callCycleService.SaveItinerary(itineraryHeader);

            //if (isSuccess)
            //{
            //    div_message.Attributes.Add("style", "display:block;padding:4px");
            //    div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved !");
            //}
            //else
            //{
            //    div_message.Attributes.Add("style", "display:block;padding:4px");
            //    div_message.InnerHtml = commonUtility.GetInfoMessage(CommonUtility.ERROR_MESSAGE);
            //}

            return isSuccess;

        }
        catch (Exception e)
        {
            //div_message.Attributes.Add("style", "display:block;padding:4px");
            //div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);

            return false;
        }
    }

    protected void ButtonSaveAndExportExcel_Click(object sender)
    {
        //bool status = this.SaveItinerary();
        bool status = this.SaveItinerary(true);
        CommonUtility commonUtility = new CommonUtility();


        if (status == true)
        {
            this.CreateItineraryReport();
        }
        else
        {
            div_message.Attributes.Add("style", "display:block;padding:4px");
            div_message.InnerHtml = commonUtility.GetInfoMessage("Saving Failed.");
        }
    }

    protected void cmbMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        ReLoad();
    }

    private void ReLoad()
    {
        try
        {
            int month = int.Parse(cmbMonth.SelectedValue);
            int year = int.Parse(DropDownListYear.SelectedValue);
            LoadGridData(month, year);
            LoadUIData();
        }
        catch (Exception ex)
        {
        }
    }

    protected void DropDownListMonth_SelectedIndexChanged(object sender, EventArgs e)
    {
        ReLoad();
    }
    protected void DropDownListRep_SelectedIndexChanged(object sender, EventArgs e)
    {
        ReLoad();

    }
    #endregion

    #region -Private Methods-

    public void CreateItineraryReport()
    {
        CallCycleClient callCycleClient = new CallCycleClient();
        CommonClient commonClient = new CommonClient();

        ItineraryHeaderDTO itineraryHeader = new ItineraryHeaderDTO();
        List<ItineraryDetailDTO> dataList = new List<ItineraryDetailDTO>();
        List<CalendarHolidayDTO> holidayList = new List<CalendarHolidayDTO>();
        List<string> headerList = new List<string>();
        CommonUtility commonUtility = new CommonUtility();

        headerList.Add("DATE");
        headerList.Add("TOWNS TO VISIT");
        headerList.Add("SEQ");
        headerList.Add("DIRECT MILES");
        headerList.Add("LOCAL MILES");
        headerList.Add("TOTAL MILES");
        headerList.Add("WORK PROGRAMME");
        headerList.Add("NIGHT AT");

        string tme = UserSession.Instance.UserName;
        DateTime date = DateTime.Now;
        string month = "1";
        string year = "2014";

        //if (Request.QueryString["mon"] != null )
        //{
        //    month = Request.QueryString["mon"].ToString();
        //}
        month = cmbMonth.SelectedValue;
        year = DropDownListYear.SelectedValue;

        date = Convert.ToDateTime("01/" + month + "/" + year);

        itineraryHeader = callCycleClient.GetItineraryDetailsForUser(tme, date);
        dataList = itineraryHeader.ItineraryDetailList;
        holidayList = commonClient.GetAllHolidaytypes();

        if(dataList !=null)
        {
            if (dataList.Count > 0) {
                itineraryHeader.OfficialMiles = dataList[0].OfficialMiles;
            }
       
        }
        string fileName = CreateFileName();
        //bool status = GenerateExcelForItineraryReport(itineraryHeader, headerList, fileName, holidayList);
        bool status = true;

        bool mailSent = false;




        if (status)
        {
            //If the excel file created sucessfully, attach & send email
            mailSent = this.SendItineraryMailToRM(itineraryHeader, fileName);
        }

        if (mailSent)
        {
            div_message.Attributes.Add("style", "display:block;padding:4px");
            div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved and an Approval Mail has been sent!");
        }
        else
        {
            div_message.Attributes.Add("style", "display:block;padding:4px");
            div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }

    }

    //public bool GenerateExcelForItineraryReport(ItineraryHeaderDTO itineraryHeader, List<string> headerLists, string fileName, List<CalendarHolidayDTO> holidayList)
    //{

    //    bool status = false;

    //    try
    //    {
    //        SLDocument exlDoc = new SLDocument();

    //        //string path = HttpContext.Current.Request.PhysicalApplicationPath + "\\docs\\der_reports";
    //        //if (!Directory.Exists(path))
    //        //    Directory.CreateDirectory(path);

    //        //string savedocPath = ConfigUtil.SaveDocPath;
    //        //string filename = savedocPath + "der_" + createdBy + ".xlsx";

    //        List<ItineraryDetailDTO> dataList = new List<ItineraryDetailDTO>();
    //        dataList = itineraryHeader.ItineraryDetailList;

    //        var colorDateList = dataList.Where(p => p.DayTypeColor != "");

    //        int header1RowIndex = 11;
    //        int header2RowIndex = header1RowIndex + 1;
    //        int dataRowIndex = header2RowIndex + 1;

    //        int headerColumnIndex = 1;
    //        int dataColumnIndex = headerColumnIndex;
    //        int columnNo = 0;

    //        foreach (string col in headerLists)
    //        {
    //            //Draw the excel data column header cells.
    //            string header1colName = string.Empty;
    //            string header2colName = col.ToString();
    //            switch (col.ToString())
    //            {
    //                case "DATE":
    //                    {
    //                        header1colName = "DATE";
    //                        header2colName = string.Empty;
    //                        columnNo = 1;
    //                        break;
    //                    }

    //                case "TOWNS TO VISIT":
    //                    {
    //                        header1colName = "TOWNS TO VISIT";
    //                        header2colName = string.Empty;
    //                        columnNo = 2;
    //                        break;
    //                    }
    //                case "SEQ":
    //                    {
    //                        header1colName = "SEQ";
    //                        header2colName = string.Empty;
    //                        columnNo = 3;
    //                        break;
    //                    }
    //                case "DIRECT MILES":
    //                    {
    //                        header1colName = "DIRECT MILES";
    //                        header2colName = string.Empty;
    //                        columnNo = 4;
    //                        break;
    //                    }
    //                case "LOCAL MILES":
    //                    {
    //                        header1colName = "LOCAL MILES";
    //                        header2colName = string.Empty;
    //                        columnNo = 5;
    //                        break;
    //                    }

    //                case "TOTAL MILES":
    //                    {
    //                        header1colName = "TOTAL MILES";
    //                        header2colName = string.Empty;
    //                        columnNo = 6;
    //                        break;
    //                    }
    //                case "WORK PROGRAMME":
    //                    {
    //                        header1colName = "WORK PROGRAMME";
    //                        header2colName = string.Empty;
    //                        columnNo = 7;
    //                        break;
    //                    }
    //                case "NIGHT AT":
    //                    {
    //                        header1colName = "NIGHT AT";
    //                        header2colName = string.Empty;
    //                        columnNo = 8;
    //                        break;
    //                    }


    //                default:
    //                    break;
    //            }

    //            //Draw the excel data column for header1 cells.
    //            this.DrawExcelHeaderCellWithAutoFit(exlDoc, header1RowIndex, headerColumnIndex, header1colName);

    //            //Draw the excel data column for header2 cells.
    //            this.DrawExcelHeaderCellWithAutoFit(exlDoc, header2RowIndex, headerColumnIndex, header2colName);

    //            //Draw the excel data row cells.
    //            dataRowIndex = header2RowIndex + 1;
    //            int index = 0;
    //            List<string> propertyList = new List<string>();
    //            List<string> lineColorList = new List<string>();
    //            propertyList = CreateColumnPropertyList(dataList, columnNo, out lineColorList);
    //            foreach (string row in propertyList)
    //            {
    //                //List<string> propertyList= new List<string>();

    //                //propertyList = this.CreateObjectPropertyList(row);

    //                //foreach (string text in propertyList)
    //                //{
    //                this.DrawExcelDataRowCellWithAutoFit(exlDoc, dataRowIndex, headerColumnIndex, row, lineColorList[index]);
    //                index++;
    //                dataRowIndex++;
    //                //}
    //            }

    //            headerColumnIndex++;
    //        }

    //        //// merge all cells in the cell 
    //        //exlDoc.MergeWorksheetCells("A8", "A9");
    //        //exlDoc.MergeWorksheetCells("B8", "B9");
    //        //exlDoc.MergeWorksheetCells("C8", "C9");
    //        //exlDoc.MergeWorksheetCells("D8", "D9");

    //        //exlDoc.MergeWorksheetCells("AC8", "AC9");
    //        ////exlDoc.MergeWorksheetCells("W8", "W9");
    //        ////exlDoc.MergeWorksheetCells("X8", "X9");
    //        ////exlDoc.MergeWorksheetCells("Y8", "Y9");

    //        //exlDoc.MergeWorksheetCells("F8", "U8");
    //        //exlDoc.MergeWorksheetCells("V8", "Z8");
    //        //exlDoc.MergeWorksheetCells("AA8", "AB8");

    //        //exlDoc.SetCellValue("F8", "Availability");
    //        ////     exlDoc.SetCellValue("U8", "Merchandising");
    //        //exlDoc.SetCellValue("V8", "Call Evaluation - National Objectives");
    //        //exlDoc.SetCellValue("AA8", "Market Objectives");
    //        //exlDoc.SetCellValue("AC8", "Remarks");


    //        string imageName = ConfigUtil.ImagePath + "CTC_logo.png";
    //        SLPicture pic = new SLPicture(imageName);

    //        pic.SetPosition(0.5, 1.0);
    //        exlDoc.InsertPicture(pic);

    //        exlDoc.SetCellValue(2, 5, "ITINERARY");
    //        SetReportMasterData(exlDoc, itineraryHeader, dataRowIndex);
    //        SetReportSummaryGrid(exlDoc, itineraryHeader , dataRowIndex);
    //        SetLegendData(exlDoc, holidayList, dataRowIndex);

    //        exlDoc.SaveAs(fileName);
    //        status = true;

    //    }
    //    catch (Exception ex)
    //    {
    //        status = false;
    //        throw ex;
    //    }

    //    return status;

    //}

    public string CreateFileName()
    {
        string savedocPath = ConfigUtil.SaveDocPath;
        string filename = savedocPath + "Itinerary_TME_" + UserSession.Instance.OriginalUserName + ".xlsx";
        return filename;
    }

    private void LoadUIData()
    {
        if (Session[CommonUtility.ITINERARY_HEADER_DATA] != null)
        {
            ItineraryHeaderDTO headerDTO = new ItineraryHeaderDTO();
            headerDTO = (ItineraryHeaderDTO)Session[CommonUtility.ITINERARY_HEADER_DATA];
            //txtName.Text = !string.IsNullOrEmpty(headerDTO.ItineraryName) ? headerDTO.ItineraryName : UserSession.Instance.Originator;
            TxtBaseTown.Text = headerDTO.BaseTown;

            

            txtPrivate.Text = headerDTO.PrivateField.ToString();
            txtContigency.Text = headerDTO.Contigency.ToString();
            //  cmbMonth.SelectedValue = DateTime.ParseExact(headerDTO.Month, "MMMM", CultureInfo.InvariantCulture).Month.ToString();
        }
    }

    private void LoadRep()
    {
        if (UserSession.Instance.OriginatorString.Equals("ASE"))
        {
            List<KeyValuePair<string, string>> repLines = new List<KeyValuePair<string, string>>();
            repLines = GetRepsForDropdown();
            foreach (KeyValuePair<string, string> ValuePair in repLines)
            {
                DropDownListRep.Items.Add(new ListItem(ValuePair.Value.ToString(), ValuePair.Key.ToString()));
            }
            DropDownListRep.SelectedIndex = 0;
            if (repLines.Count > 0)
            {
                ArgsDTO args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
                args.RepCode = DropDownListRep.SelectedValue;
            }
        }
        else
        {
            DropDownListRep.Items.Add(new ListItem(UserSession.Instance.Originator, UserSession.Instance.UserName));
        }

        DropDownListRep.SelectedValue = UserSession.Instance.UserName;
    }


    private List<KeyValuePair<string, string>> GetRepsForDropdown()
    {
        List<KeyValuePair<string, string>> repLines = new List<KeyValuePair<string, string>>();
        OriginatorClient originatorService = new OriginatorClient();
        List<OriginatorDTO> originatorList = new List<OriginatorDTO>();

        //originatorList = originatorService.GetRepsByOriginator("", UserSession.Instance.UserName); 
        originatorList = originatorService.GetRepsByOriginator("", UserSession.Instance.ChildOriginators.Replace("originator", "o.originator"));


        if (originatorList != null)
        {
            foreach (OriginatorDTO reps in originatorList)
            {
                repLines.Add(new KeyValuePair<string, string>(reps.RepCode, reps.Name));
            }
        }
        //repLines.Add(new KeyValuePair<string, string>(6, "06 Months"));
        //repLines.Add(new KeyValuePair<string, string>(12, "01 Year"));
        //repLines.Add(new KeyValuePair<string, string>(0, "- All -"));

        return repLines;
    }

    private bool SendItineraryMailToRM(ItineraryHeaderDTO itineraryHeader, string fileName)
    {
        CommonUtility commonUtility = new CommonUtility();
        OriginatorClient originatorClient = new OriginatorClient();
        bool status = false;

        string setMonth = Request.QueryString["mon"];
        string setYear = Request.QueryString["yr"];
        if (setMonth.Length == 1)
            setMonth = "0" + setMonth;
        DateTime effDate = new DateTime(int.Parse(setYear), int.Parse(setMonth), 1);

        List<string> tolist = new List<string>();

        OriginatorClient originatorService = new OriginatorClient();
        List<OriginatorDTO> rmList = new List<OriginatorDTO>();
        string originator = UserSession.Instance.UserName;
        rmList = originatorService.GetLevelThreeParentOriginators(originator);
        foreach (OriginatorDTO rm in rmList)
        {
           tolist.Add(rm.Email);
        }

        //generate an access token for the RM (mail reciever) 
        //string newAccessToken = commonUtility.GetNewAccessToken(rmList[0].UserName);

        ////Get AccessToken For the User
        //OriginatorDTO oriRM = new OriginatorDTO();
        //oriRM = originatorClient.GetOriginator(rmList[0].UserName);
        //bool isUpdateTokenSuccess = false;
        //if (String.IsNullOrEmpty(oriRM.AccessToken))
        //{
        //    //update access token for the RM
        //    OriginatorDTO originatorDTO = new OriginatorDTO();
        //    originatorDTO.UserName = rmList[0].UserName;
        //    originatorDTO.AccessToken = newAccessToken;
        //    isUpdateTokenSuccess = originatorClient.UpdateOriginatorAccessToken(originatorDTO);
        //}
        //else
        //{
        //    newAccessToken = oriRM.AccessToken;
        //}
        string newAccessToken = "";// UserSession.Instance.AccessToken;

        //tolist = GetRMEmailListForTME();
       // tolist.Add("kavishas@peercore.com.au");
        //tolist.Add("milindad@peercore.com.au");
        List<string> cclist = new List<string>();

        //   cclist.Add(UserSession);
        //cclist.Add("kavishas@peercore.com.au");
        //     cclist.Add("prasadu@peercore.com.au");
        //cclist.Add("milindad@peercore.com.au");



        string MessageBody = "";
        string Subject = "Itinerary Report - " + itineraryHeader.ItineraryName;
        string encodedUsername = rmList[0].UserName;
        //encodedUsername = Server.UrlEncode(commonUtility.GetEncodedString(encodedUsername));
        //string ApproveLink = ConfigUtil.PublicIp + "/call_cycle/transaction/itinerary_status_update.aspx?ihid=" + itineraryHeader.ItineraryHeaderId + "&status=a";
        //string RejectLink = ConfigUtil.PublicIp + "/call_cycle/transaction/itinerary_status_update.aspx?ihid=" + itineraryHeader.ItineraryHeaderId + "&status=r";

        //string ApproveLink = "http://203.189.71.35:10146//call_cycle/transaction/itinerary_status_update.aspx?ihid=" + itineraryHeader.ItineraryHeaderId + "&status=a&ori=" + encodedUsername + "&key=" + newAccessToken;
        //string RejectLink = "http://203.189.71.35:10146//call_cycle/transaction/itinerary_status_update.aspx?ihid=" + itineraryHeader.ItineraryHeaderId + "&status=r&ori=" + encodedUsername + "&key=" + newAccessToken;

        string ApproveLink = ConfigUtil.PublicIp + "call_cycle/transaction/itinerary_status_update.aspx?ihid=" + itineraryHeader.ItineraryHeaderId + "&status=a&ori=" + encodedUsername + "&key=" + newAccessToken;
        string RejectLink = ConfigUtil.PublicIp + "call_cycle/transaction/itinerary_status_update.aspx?ihid=" + itineraryHeader.ItineraryHeaderId + "&status=r&ori=" + encodedUsername + "&key=" + newAccessToken;

        StringBuilder builder = new StringBuilder();

        builder.Append("Have attached the itinerary for ASM for the month of " + effDate.ToString("MMM yyyy")+ " - ");
        builder.Append(UserSession.Instance.Originator);
        builder.Append(". ");
        builder.AppendLine("</br>");
        builder.AppendLine("</br>");
        builder.Append(" Click the following link to Approve the itinerary.");
        builder.Append("</br>");
        builder.Append(ApproveLink);
        builder.Append("</br>");
        builder.Append("Click the following link to Reject the itinerary.");
        builder.Append("</br>");
        builder.Append(RejectLink);
        builder.Append("</br>");
        builder.Append("</br>");
        builder.Append("</br>");
        builder.Append("<div style=\"Color:#b7b7b7\">* This is a system generated mail, Please do not reply.</div>");


        //MessageBody = "Have attached the itinerary for TME "+ UserSession.Instance.Originator + ". "+Environment.NewLine
        //    + " Click the following link to Approve the itinerary." + Environment.NewLine + ApproveLink + Environment.NewLine
        //    + " Click the following link to Reject the itinerary. " + Environment.NewLine + RejectLink + Environment.NewLine;

        MessageBody = builder.ToString();

        List<string> attachementPathList = new List<string>();
        attachementPathList.Add(fileName);
        //ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "$('#div_loader').show();", true);
        status = commonUtility.SendEmailOut(tolist, cclist, Subject, MessageBody, attachementPathList);
        //ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "$('#div_loader').hide();", true);
        return status;
    }

    private void LoadGridData(int month = 0, int year = 0)
    {

        string parentOriginator = DropDownListRep.SelectedValue;// UserSession.Instance.UserName;
        DateTime currentDate = DateTime.Now;

        if (month == 0)
        {
            if (Request.QueryString["mon"] != null && Request.QueryString["mon"] != "")
            {
                string setMonth = Request.QueryString["mon"];
                string setYear = Request.QueryString["yr"];
                if (setMonth.Length == 1)
                    setMonth = "0" + setMonth;
                cmbMonth.SelectedValue = setMonth;
                DropDownListYear.SelectedValue = setYear;
                currentDate = new DateTime(int.Parse(setYear), int.Parse(setMonth), 1);
            }
            else
            {
                cmbMonth.SelectedValue = DateTime.Now.ToString("MM");
            }
        }
        else
        {
            currentDate = new DateTime(year, month, 1);
        }
        CallCycleClient callCycleClient = new CallCycleClient();
        ItineraryHeaderDTO data = new ItineraryHeaderDTO();
        data = callCycleClient.GetItineraryDetailsForUser(parentOriginator, currentDate);
        Session[CommonUtility.ITINERARY_HEADER_DATA] = data;

    }

    private List<string> GetRMEmailListForTME()
    {
        OriginatorClient originatorService = new OriginatorClient();
        List<OriginatorDTO> rmList = new List<OriginatorDTO>();
        List<string> rmEmailList = new List<string>();
        string originator = UserSession.Instance.UserName;
        rmList = originatorService.GetLevelThreeParentOriginators(originator);

        foreach (OriginatorDTO rm in rmList)
        {
            rmEmailList.Add(rm.Email);
        }

        return rmEmailList;
    }


    private void SetYearDropDown()
    {
        for (int i = DateTime.Now.Year; i <= DateTime.Now.Year + 10; i++)
            DropDownListYear.Items.Add(new ListItem(Convert.ToString(i), Convert.ToString(i)));
    }

    #endregion

    //#region -Excel Methods-

    ////--------------- Excel report creation methods - BEGIN-----------------------//

    //private void DrawExcelHeaderCellWithAutoFit(SLDocument doc, int rowIndex, int columnIndex, string data)
    //{
    //    doc.SetCellValue(rowIndex, columnIndex, data);

    //    SLStyle style = doc.CreateStyle();
    //    style.SetTopBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
    //    style.SetBottomBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
    //    style.SetRightBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
    //    style.SetLeftBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);

    //    style.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
    //    style.Font.Bold = true;

    //    doc.SetCellStyle(rowIndex, columnIndex, style);
    //    doc.AutoFitColumn(columnIndex);
    //}

    //private void DrawExcelDataRowCellWithAutoFit(SLDocument doc, int rowIndex, int columnIndex, string data, string colorCode)
    //{
    //    doc.SetCellValue(rowIndex, columnIndex, data);

    //    SLStyle style = doc.CreateStyle();
    //    style.SetTopBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
    //    style.SetBottomBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
    //    style.SetRightBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
    //    style.SetLeftBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);

    //    System.Drawing.Color myColor = System.Drawing.ColorTranslator.FromHtml("#FFF");
    //    //string hex = "#FFFFFF";
    //    if (colorCode != "")
    //    {
    //        myColor = System.Drawing.ColorTranslator.FromHtml(colorCode);
    //    }

    //    style.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
    //    style.Fill.SetPattern(PatternValues.Solid, myColor, myColor);
    //    doc.SetCellStyle(rowIndex, columnIndex, style);

    //    doc.AutoFitColumn(columnIndex);
    //}

    //private void DrawExcelHeaderCell(SLDocument doc, int rowIndex, int columnIndex, string data, double columnWidth)
    //{
    //    doc.SetCellValue(rowIndex, columnIndex, data);

    //    SLStyle style = doc.CreateStyle();
    //    style.SetTopBorder(BorderStyleValues.Thick, System.Drawing.Color.Black);
    //    style.SetBottomBorder(BorderStyleValues.Thick, System.Drawing.Color.Black);
    //    style.SetRightBorder(BorderStyleValues.Thick, System.Drawing.Color.Black);
    //    style.SetLeftBorder(BorderStyleValues.Thick, System.Drawing.Color.Black);

    //    style.SetHorizontalAlignment(HorizontalAlignmentValues.Center);

    //    doc.SetCellStyle(rowIndex, columnIndex, style);
    //    //doc.AutoFitColumn((columnIndex + 1));
    //    doc.SetColumnWidth(columnIndex, columnWidth);
    //}

    ////This is used to create a list of the data in a object list perticular row
    //private List<string> CreateObjectPropertyList(ItineraryDetailDTO itineraryDetailDTO)
    //{
    //    List<string> propertyList = new List<string>();

    //    propertyList.Add(!string.IsNullOrEmpty(itineraryDetailDTO.Date.ToString()) ? itineraryDetailDTO.Date.ToString() : "");
    //    propertyList.Add(!string.IsNullOrEmpty(itineraryDetailDTO.TownsToVisit) ? itineraryDetailDTO.TownsToVisit.ToString() : "");
    //    //propertyList.Add(!string.IsNullOrEmpty(itineraryDetailDTO.Sequence.ToString()) ? itineraryDetailDTO.Sequence.ToString() : "");
    //    propertyList.Add(!string.IsNullOrEmpty(itineraryDetailDTO.SequenceRoute.ToString()) ? itineraryDetailDTO.SequenceRoute.ToString() : "");
    //    propertyList.Add(!string.IsNullOrEmpty(itineraryDetailDTO.DirectMiles.ToString()) ? itineraryDetailDTO.DirectMiles.ToString() : "");
    //    propertyList.Add(!string.IsNullOrEmpty(itineraryDetailDTO.LocalMiles.ToString()) ? itineraryDetailDTO.LocalMiles.ToString() : "");
    //    propertyList.Add(!string.IsNullOrEmpty(itineraryDetailDTO.TotalMiles.ToString()) ? itineraryDetailDTO.TotalMiles.ToString() : "");
    //    propertyList.Add(!string.IsNullOrEmpty(itineraryDetailDTO.WorkProgram) ? itineraryDetailDTO.WorkProgram.ToString() : "");
    //    propertyList.Add(!string.IsNullOrEmpty(itineraryDetailDTO.NightAt) ? itineraryDetailDTO.NightAt.ToString() : "");

    //    return propertyList;
    //}

    ////This is used to create a list of the data in a perticular column
    //private List<string> CreateColumnPropertyList(List<ItineraryDetailDTO> itineraryDetailList, int index, out List<string> colorList)
    //{
    //    List<string> propertyList = new List<string>();
    //    colorList = new List<string>();

    //    switch (index)
    //    {
    //        case 1:
    //            foreach (ItineraryDetailDTO item in itineraryDetailList)
    //            {
    //                propertyList.Add(item.Date.ToShortDateString());
    //                colorList.Add(item.DayTypeColor);
    //            }
    //            break;

    //        case 2:
    //            foreach (ItineraryDetailDTO item in itineraryDetailList)
    //            {
    //                propertyList.Add(item.TownsToVisit);
    //                colorList.Add(item.DayTypeColor);
    //            }
    //            break;

    //        //case 3:
    //        //    foreach (ItineraryDetailDTO item in itineraryDetailList)
    //        //    {
    //        //        propertyList.Add(item.Sequence.ToString());
    //        //    }
    //        //    break;
    //        case 3:
    //            foreach (ItineraryDetailDTO item in itineraryDetailList)
    //            {
    //                propertyList.Add(item.SequenceRoute);
    //                colorList.Add(item.DayTypeColor);
    //            }
    //            break;

    //        case 4:
    //            foreach (ItineraryDetailDTO item in itineraryDetailList)
    //            {
    //                propertyList.Add(item.DirectMiles.ToString());
    //                colorList.Add(item.DayTypeColor);
    //            }
    //            break;
    //        case 5:
    //            foreach (ItineraryDetailDTO item in itineraryDetailList)
    //            {
    //                propertyList.Add(item.LocalMiles.ToString());
    //                colorList.Add(item.DayTypeColor);
    //            }
    //            break;
    //        case 6:
    //            foreach (ItineraryDetailDTO item in itineraryDetailList)
    //            {
    //                propertyList.Add(item.TotalMiles.ToString());
    //                colorList.Add(item.DayTypeColor);
    //            }
    //            break;

    //        case 7:
    //            foreach (ItineraryDetailDTO item in itineraryDetailList)
    //            {
    //                propertyList.Add(item.WorkProgram);
    //                colorList.Add(item.DayTypeColor);
    //            }
    //            break;

    //        case 8:
    //            foreach (ItineraryDetailDTO item in itineraryDetailList)
    //            {
    //                propertyList.Add(item.NightAt);
    //                colorList.Add(item.DayTypeColor);
    //            }
    //            break;


    //        default:
    //            break;
    //    }

    //    return propertyList;
    //}

    //private void SetReportMasterData(SLDocument doc, ItineraryHeaderDTO header, int dataRowIndex)
    //{



    //    SLStyle style = doc.CreateStyle();
    //    style.SetTopBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
    //    style.SetBottomBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
    //    style.SetRightBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
    //    style.SetLeftBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);

    //    style.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
    //    style.Font.Bold = true;

    //    doc.SetCellValue(8, 1, "Name");
    //    doc.SetCellValue(8, 2, header.ItineraryName);
    //    doc.SetCellValue(8, 4, "Base Town");
    //    doc.SetCellValue(8, 5, header.BaseTown);
    //    doc.SetCellValue(8, 7, "Month");
    //    doc.SetCellValue(8, 8, header.Month);
    //    //   dataRowIndex

    //    //  doc.SetCellStyle(rowIndex, columnIndex, style);
    //    //  doc.AutoFitColumn(columnIndex);

    //}

    //private void SetReportSummaryGrid(SLDocument doc, ItineraryHeaderDTO header, int dataRowIndex)
    //{
    //    SLStyle style = doc.CreateStyle();
    //    style.SetTopBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
    //    style.SetBottomBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
    //    style.SetRightBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
    //    style.SetLeftBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);

    //    style.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
    //    style.Font.Bold = true;

    //    SLStyle styleSet2 = doc.CreateStyle();
    //    styleSet2.SetTopBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
    //    styleSet2.SetBottomBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
    //    styleSet2.SetRightBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
    //    styleSet2.SetLeftBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);

    //    dataRowIndex++;
    //    doc.SetCellValue(dataRowIndex, 2, "PLANNED MILEAGE ");
    //    // doc.SetCellStyle(dataRowIndex,2,);
    //    //  dataRowIndex++;
    //    List<string> totalList = new List<string>();
    //    List<string> headerList = new List<string>();
    //    headerList.Add("OFFICIAL");
    //    headerList.Add("PRIVATE");
    //    headerList.Add("CONTINGENCY");
    //    headerList.Add("TOTAL");

    //    totalList.Add(header.OfficialMiles.ToString());
    //    totalList.Add(header.PrivateField.ToString());
    //    totalList.Add(header.Contigency.ToString());
    //    totalList.Add((header.OfficialMiles + header.PrivateField + header.Contigency).ToString());

    //    for (int i = 1; i <= headerList.Count; i++)
    //    {
    //        doc.SetCellValue(dataRowIndex + i, 2, headerList[i - 1].ToString());
    //        doc.SetCellStyle(dataRowIndex + i, 2, styleSet2);
    //        doc.SetCellValue(dataRowIndex + i, 3, totalList[i - 1].ToString());
    //        doc.SetCellStyle(dataRowIndex + i, 3, styleSet2);
    //    }

    //}

    //private void SetLegendData(SLDocument doc, List<CalendarHolidayDTO> holidayList, int dataRowIndex)
    //{
    //    SLStyle style = doc.CreateStyle();
    //    style.SetTopBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
    //    style.SetBottomBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
    //    style.SetRightBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);
    //    style.SetLeftBorder(BorderStyleValues.Medium, System.Drawing.Color.Black);

    //    style.SetHorizontalAlignment(HorizontalAlignmentValues.Left);
    //    style.Font.Bold = true;

    //    int index = 0;
    //    dataRowIndex = dataRowIndex + 2;

    //    SLStyle style2 = doc.CreateStyle();
    //    System.Drawing.Color myColor = System.Drawing.ColorTranslator.FromHtml("#FFF");
    //    //string hex = "#FFFFFF";


    //    foreach (CalendarHolidayDTO item in holidayList)
    //    {
    //        doc.SetCellValue(dataRowIndex + index, 6, item.TypeDescription);
    //        //doc.SetCellValue(dataRowIndex + index, 8, "");

    //        if (item.TypeColor != "")
    //        {
    //            myColor = System.Drawing.ColorTranslator.FromHtml(item.TypeColor);
    //        }
    //        style2.Fill.SetPattern(PatternValues.Solid, myColor, myColor);

    //        doc.SetCellStyle(dataRowIndex + index, 8, style2);

    //        index++;

    //    }

    //    //   dataRowIndex

    //    //  doc.SetCellStyle(rowIndex, columnIndex, style);
    //    //  doc.AutoFitColumn(columnIndex);

    //}

    ////--------------- Excel report creation methods - END-----------------------//.

    //#endregion


}