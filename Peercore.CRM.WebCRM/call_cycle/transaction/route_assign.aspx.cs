﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Common;
using System.Text;
using Peercore.CRM.Shared;
using CRMServiceReference;

public partial class call_cycle_transaction_route_assign : PageBase
{
    CommonUtility commonUtility = new CommonUtility();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
            buttonbar.VisibleSave(true);

            HiddenFieldOriginator.Value = UserSession.Instance.UserName;

            if (!IsPostBack)
            {
                div_autocomplete.InnerHtml = SetAutocomplete_Temp();
                div_autocomplete_route.InnerHtml = SetAutocomplete_Route();
            }

            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Route Assign ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    protected void ButtonSave_Click(object sender)
    {
        string jsonString = string.Empty;
        RouteDTO routeDTO = new RouteDTO();
        OriginatorDTO originatorDTO = new OriginatorDTO();
        CallCycleClient routeClient = new CallCycleClient();

        try
        {
            //  if (!string.IsNullOrEmpty(HiddenFieldStockGrid.Value))
            //  {
            if (txtRouteName.Text.Trim() == "")
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Route Name cannot be empty.");
                return;
            }

            if (txtDRName.Text.Trim() == "")
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("DR Name cannot be empty.");
                return;
            }

            if (string.IsNullOrEmpty(HiddenFieldRepCode.Value))
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Please Select a valid DR.");
                return;
            }


            if (string.IsNullOrEmpty(HiddenFieldRouteCode.Value))
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Please Select a valid Route.");
                return;
            }


            if (chktempararychk.Checked)
            {
                routeDTO.IsTempRep = true;
            }
            else {
                routeDTO.IsTempRep = false;
            }

            originatorDTO = CheckRepForRoute(Convert.ToInt32(HiddenFieldRouteCode.Value));

            if (originatorDTO != null)
            {
                if ((originatorDTO.RepCode != null) && (HiddenFieldExistClickOk.Value != "1"))
                {
                    string tempmsg = "";
                    if (routeDTO.IsTempRep == true)
                        tempmsg = " with temparary DR ";

                    ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "showrepexistmsg('" + originatorDTO.Name.Trim() + "','" + tempmsg + "');", true);
                    return;
                }
            }
            routeDTO.RouteName = txtRouteName.Text;
            routeDTO.RepCode = HiddenFieldRepCode.Value;
            routeDTO.CreatedBy = UserSession.Instance.UserName;
            routeDTO.RouteMasterId = Convert.ToInt32(HiddenFieldRouteCode.Value);
            routeDTO.TempRepAssignDate = Convert.ToDateTime(AssignDate.Text);

            bool status = routeClient.SaveRoute(routeDTO);
            
            if (status == true)
            {
                // div_info.Attributes.Add("style", "display:none");
                div_message.Attributes.Add("style", "display:block");

                HiddenFieldExistClickOk.Value = "0";
                div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved.");
                txtDRName.Text = "";
                txtRouteName.Text = "";
                chktempararychk.Checked = false;
            }
            else
            {
                div_message.Attributes.Add("style", "display:none");
                div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
            }
            // }
        }
        catch (Exception ex)
        {
            //handle Unique Key Constraint Violation Exception(Show message'Route is already assigned')
            throw;
        }
    }

    private string SetAutocomplete_Temp()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script type=\"text/javascript\">");
        sb.Append("     $(function () {");
        sb.Append("         $(\".tb\").autocomplete({");
        sb.Append("             source: function (request, response) {");
        sb.Append("                 $.ajax({");
        sb.Append("                     type: \"POST\",");
        sb.Append("                     contentType: \"application/json; charset=utf-8\",");
        sb.Append("                     url: \"" + ConfigUtil.ApplicationPath + "service/call_cycle/call_cycle_service.asmx/GetRepsByOriginator_For_Route_Assign\",");
        sb.Append("                     data: \"{'name':'\" + request.term + \"'}\",");
        sb.Append("                     dataType: \"json\",");
        sb.Append("                     async: true,");
        sb.Append("                     dataFilter: function(data) {");
        sb.Append("                           return data;");
        sb.Append("                     },");
        sb.Append("                     success: function (data) {");
        sb.Append("                     response($.map(data.d, function(item) {");
        sb.Append("                     return {");
        sb.Append("                         label: item.Name,");
        sb.Append("                         desc: item.RepCode");
        sb.Append("                     };");
        sb.Append("                     }));");

        sb.Append("                     },");
        sb.Append("                     error: function (result) {");
        sb.Append("                     }");
        sb.Append("                 });");
        sb.Append("             },");
        sb.Append("             minLength: 2,");
        sb.Append("            select: function( event, ui ) {");

        sb.Append("                  var selectedObj = ui.item;");

        //sb.Append("                 $('#MainContent_HiddenFieldCustomerIndustry').val(selectedObj.industry);");
        //sb.Append("                 $('#MainContent_HiddenFieldCustomerCode').val(selectedObj.label);");
        //sb.Append("                 $('#MainContent_HiddenFieldCustCode').val(selectedObj.label);");
        sb.Append("                 $('#MainContent_txtDRName').val(selectedObj.label);");
        sb.Append("                 setRepCode(selectedObj.desc);");
        //sb.Append("                 setcuscode(selectedObj.label,selectedObj.desc,'0');");
        //k   sb.Append("                 loadproductlist(selectedObj.label);");
        //sb.Append("                 $('ul.ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all').css('display','none');");
        sb.Append("            }");
        sb.Append("         });");
        sb.Append("     });");
        sb.Append("</script>");
        sb.Append("");

        return sb.ToString();
    }

    private string SetAutocomplete_Route()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script type=\"text/javascript\">");
        sb.Append("     $(function () {");
        sb.Append("         $(\".tb2\").autocomplete({");
        sb.Append("             source: function (request, response) {");
        sb.Append("                 $.ajax({");
        sb.Append("                     type: \"POST\",");
        sb.Append("                     contentType: \"application/json; charset=utf-8\",");
        sb.Append("                     url: \"" + ConfigUtil.ApplicationPath + "service/call_cycle/call_cycle_service.asmx/GetRouteNames\",");
        sb.Append("                     data: \"{'name':'\" + request.term + \"'}\",");
        sb.Append("                     dataType: \"json\",");
        sb.Append("                     async: true,");
        sb.Append("                     dataFilter: function(data) {");
        sb.Append("                           return data;");
        sb.Append("                     },");
        sb.Append("                     success: function (data) {");
        sb.Append("                     response($.map(data.d, function(item) {");
        sb.Append("                     return {");
        sb.Append("                         label: item.RouteName,");
        sb.Append("                         desc: item.RouteMasterId");
        sb.Append("                     };");
        sb.Append("                     }));");

        sb.Append("                     },");
        sb.Append("                     error: function (result) {");
        sb.Append("                     }");
        sb.Append("                 });");
        sb.Append("             },");
        sb.Append("             minLength: 2,");
        sb.Append("            select: function( event, ui ) {");

        sb.Append("                  var selectedObj = ui.item;");

        //sb.Append("                 $('#MainContent_HiddenFieldCustomerIndustry').val(selectedObj.industry);");
        //sb.Append("                 $('#MainContent_HiddenFieldCustomerCode').val(selectedObj.label);");
        //sb.Append("                 $('#MainContent_HiddenFieldCustCode').val(selectedObj.label);");
        sb.Append("                 $('#MainContent_txtRouteName').val(selectedObj.label);");
        sb.Append("                 setRouteCode(selectedObj.desc);");
        //sb.Append("                 setcuscode(selectedObj.label,selectedObj.desc,'0');");
        //k   sb.Append("                 loadproductlist(selectedObj.label);");
        //sb.Append("                 $('ul.ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all').css('display','none');");
        sb.Append("            }");
        sb.Append("         });");
        sb.Append("     });");
        sb.Append("</script>");
        sb.Append("");

        return sb.ToString();
    }

    private OriginatorDTO CheckRepForRoute(int routeMasterId)
    {
        CommonClient commonClient = new CommonClient();
        List<OriginatorDTO> lstOriginatorDTO = new List<OriginatorDTO>();
        OriginatorDTO ori = new OriginatorDTO();
        try
        {
            lstOriginatorDTO = commonClient.GetRepsByRouteMasterId("", routeMasterId);
            if (lstOriginatorDTO != null)
            {
                ori = lstOriginatorDTO[0];
                
            }
            return ori;
        }
        catch (Exception)
        {
            return null;
        }
    }
}