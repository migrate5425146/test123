﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="schedule_entry.aspx.cs" Inherits="call_cycle_transaction_schedule_entry" %>

<%@ MasterType TypeName="SiteMaster" %>
<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="HiddenFieldCallCycleID" runat="server" />
    <asp:HiddenField ID="HiddenField_DueDate" runat="server" />
    <asp:HiddenField ID="HiddenFieldExistClickOk" runat="server" />
    <asp:HiddenField runat="server" ID="HiddenFieldRepCode" />
    
    <div id="schedulemodalWindow" style="display: none">
        <div id="div_repconfirm_message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>

    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div>
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/lead_customer/transaction/lead_contacts.aspx">
            <div class="back"></div></asp:HyperLink>
                    </div>
                </div>
            </div>
            <div id="new_window">
                <div id="div_custgrid">
                </div>
            </div>
            <div class="clearall">
            </div>
            <div id="div_message" runat="server" style="display: none;">
            </div>
            <div id="div_info" runat="server" style="display: none">
            </div>
            <div class="formleft">
                <div class="formtextdiv">
                    Schedule</div>
                <div class="formdetaildiv">
                    <span class="wosub mand">
                        <asp:TextBox ID="txtSchedule" runat="server" Width="130px"></asp:TextBox>
                        <asp:Label ID="tbContact" runat="server" Text="*" Font-Bold="True" Font-Size="Large"
                            ForeColor="#FF3300"></asp:Label>
                    </span>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Description</div>
                <div class="formdetaildiv">
                    <asp:TextBox ID="txtDescription" TextMode="multiline" runat="server" Width="240px"
                        Height="100px" />
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv" id="id_username" runat="server">
                    DR</div>
                <div class="formdetaildiv">
                    <asp:TextBox ID="txtDRName" CssClass="tb" runat="server" Style="text-transform: uppercase"></asp:TextBox>
                    <div runat="server" id="div_autocomplete">
                    </div>
                </div>
                <div class="clearall">
                </div>
                <div id="tbCreated" runat="server" class="specialevents" style="float: right;">
                </div>
            </div>
            <div class="formright">
                <div id="CallCyclegrid" style="margin-right: 10px;" runat="server">
                    Right Grid Goes Here</div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div style="margin: 40px 0px 20px;">
            <div class="clearall">
            </div>
            <asp:Label ID="lblProduct" runat="server" Text="OUTLETS" CssClass="specialevents"></asp:Label>
            <%--<asp:Label ID="Label1" runat="server" Text="*" Font-Bold="True" Font-Size="Large" ForeColor="#FF3300"></asp:Label>--%>
            <div class="addnewdiv">
                <a id="id_addleadCust" href="#">Add Outlets</a>
            </div>
            <%--     <div class="addnewdiv" ><a id="id_addEnduser" href="#" >Add End User</a> </div>--%>
        </div>
        <div class="clearall">
        </div>
        <div style="margin: 10px;">
            <a name="grid"></a>
            <div id="loadLeadCustomerGrid">
            </div>
            <div id="grid">
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $(document).ready(function () {
            hideStatusDiv("MainContent_div_message");
            loadCallCycle();
            var callid = $("#MainContent_HiddenFieldCallCycleID").val();
            if (callid != '') {
                loadLeadCustomer(callid, 1);
            } else {
                loadLeadCustomer(0, 1);
            }
            //loadLeadCustomerGrid1(1, 0, 'F');


            //debugger;
            var newcust_window = $("#new_window"),
                        newcust_undo = $("#undo")
                                .bind("click", function () {
                                    newcust_window.data("kendoWindow").open();
                                    newcust_undo.hide();
                                });

            var onClose = function () {
                newcust_undo.show();
            }

            if (!newcust_window.data("kendoWindow")) {
                newcust_window.kendoWindow({
                    width: "800px",
                    height: "400px",
                    title: "Outlets",
                    close: onClose
                });
            }
            newcust_window.data("kendoWindow").close();
        });

        $("#id_addleadCust").click(function () {
            SetLeadStage("Customer");
            $("div#div_loader").show();
            //loadLeadCustomerLookup(1, 0, 'F');
            //$("div#div_loader").hide();
        });

        $("#id_addEnduser").click(function () {
            SetLeadStage("enduser");
            $("div#div_loader").show();


            //$("div#div_loader").hide();
        });

        function setRepCode(repCode) {
            $("#MainContent_HiddenFieldRepCode").val(repCode);
        }

         
    </script>
</asp:Content>
