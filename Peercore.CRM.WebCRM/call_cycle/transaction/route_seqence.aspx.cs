﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Peercore.CRM.Common;
using CRMServiceReference;
using Peercore.CRM.Shared;

public partial class call_cycle_transaction_route_seqence : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            call_cycle_service callCycleService = new call_cycle_service();
            List<OriginatorDTO> lstRepsList = new List<OriginatorDTO>();

            buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
            buttonbarNavi.onButtonNext = new usercontrols_buttonbar_navigation.ButtonNext(buttonNext_Click);
            buttonbarNavi.onButtonPrevious = new usercontrols_buttonbar_navigation.ButtonPrevious(buttonPrevious_Click);

            buttonbar.EnableSave(true);
            buttonbar.VisibleSave(true);

            if (!IsPostBack)
            {
                lstRepsList = callCycleService.GetRepsByOriginator_For_Route_Assign("");

                SetReps(lstRepsList, "RepCode");

                div_autocomplete_DR.InnerHtml = SetAutocomplete_DR("1");

                if (UserSession.Instance != null)
                {
                    if (UserSession.Instance.OriginatorString == "DSR" || UserSession.Instance.OriginatorString == "DR")
                    {
                        formtextdiv.Visible = false;
                        txtDRName.Visible = false;
                        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "loadRouteTemplate('" + UserSession.Instance.RepCode + "');", true);
                        //ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "loadRouteTemplate('" + UserSession.Instance.OriginatorString + "');", true);
                    }
                }
            }

            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Route Sequence ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    public void SetReps(List<OriginatorDTO> listOriginator, string valueField = "RepCode")
    {
        ddlDRList.DataSource = listOriginator;
        ddlDRList.DataTextField = "Name";
        ddlDRList.DataValueField = valueField;
        ddlDRList.DataBind();
    }

    protected void ButtonSave_Click(object sender)
    {
        try
        {
            ScheduleStruct schedul_struct = new ScheduleStruct();
            if (Session[CommonUtility.SCHEDULE_LOG] != null)
            {
                schedul_struct = (ScheduleStruct)Session[CommonUtility.SCHEDULE_LOG];
                CallCycleClient callCycleClient = new CallCycleClient();
                if (schedul_struct.DRRoutes != null)
                {
                    CommonUtility commonUtility = new CommonUtility();

                    if (callCycleClient.UpdateRouteSequence(schedul_struct.DRRoutes))
                    {
                        div_message.Attributes.Add("style", "display:block;");
                        div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved. Please Update the Route Planner in Route Schedular ");
                    }
                    else
                    {
                        div_message.Attributes.Add("style", "display:block;");
                        div_message.InnerHtml = commonUtility.GetErrorMessage("Error Occured.");
                    }
                }
            }

            ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "loadRouteTemplate('" + schedul_struct.RepCode.ToString() + "');", true);
            
            try
            {
                CommonClient cClient = new CommonClient();
                cClient.CreateTransactionLog(UserSession.Instance.OriginalUserName,
                    DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                    TransactionTypeModules.Update,
                    TransactionModules.RouteSequence,
                    UserSession.Instance.OriginalUserName + " update route sequence for " + schedul_struct.DRRoutes[0].RepCode);
            }
            catch { }

        }
        catch { }
    }

    private string SetAutocomplete_DR(string inactive)
    {
        StringBuilder sb = new StringBuilder();
            sb.Append("<script type=\"text/javascript\">");
            sb.Append("     $(function () {");
            sb.Append("         $(\".tb\").autocomplete({");
            sb.Append("             source: function (request, response) {");
            sb.Append("                 $.ajax({");
            sb.Append("                     type: \"POST\",");
            sb.Append("                     contentType: \"application/json; charset=utf-8\",");
            sb.Append("                     url: \"" + ConfigUtil.ApplicationPath + "service/call_cycle/call_cycle_service.asmx/GetRepsByOriginator\",");
            sb.Append("                     data: \"{'name':'\" + request.term + \"'}\",");
            sb.Append("                     dataType: \"json\",");
            sb.Append("                     async: true,");
            sb.Append("                     dataFilter: function(data) {");
            sb.Append("                           return data;");
            sb.Append("                     },");
            sb.Append("                     success: function (data) {");
            sb.Append("                     response($.map(data.d, function(item) {");
            sb.Append("                     return {");
            sb.Append("                         label: item.Name,");
            sb.Append("                         desc: item.RepCode");
            sb.Append("                     };");
            sb.Append("                     }));");
            sb.Append("                     },");
            sb.Append("                     error: function (result) {");
            sb.Append("                     }");
            sb.Append("                 });");
            sb.Append("             },");
            sb.Append("             minLength: 2,");
            sb.Append("            select: function( event, ui ) {");
            sb.Append("                  var selectedObj = ui.item;");
            sb.Append("                 $('#MainContent_HiddenFieldRepName').val(selectedObj.label);");
            sb.Append("                 $('#MainContent_HiddenFieldRepCode').val(selectedObj.desc);");
            sb.Append("                 if($('#MainContent_txtDRName').val() != '')");
            sb.Append("                 {");
            sb.Append("                   loadRouteTemplate($('#MainContent_HiddenFieldRepCode').val());");
            sb.Append("                 }");
            sb.Append("            }");
            sb.Append("         });");
            sb.Append("     });");
            sb.Append("</script>");
            sb.Append("");

        return sb.ToString();
    }

    protected void buttonNext_Click(object sender)
    {
        Response.Redirect(ConfigUtil.ApplicationPath + "call_cycle/transaction/template_entry.aspx"); 
    }

    protected void buttonPrevious_Click(object sender)
    {
        Response.Redirect(ConfigUtil.ApplicationPath + "call_cycle/transaction/route_entry.aspx");
    }
}