﻿<%@ Page Title="mSales - Route Schedule" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="template_entry.aspx.cs" Inherits="call_cycle_transaction_template_entry" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ Register Src="~/usercontrols/buttonbar_navigation.ascx" TagPrefix="ucl2" TagName="buttonbarNavi" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <meta name="”viewport”" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <script type="text/javascript" src="../../assets/scripts/jquery.ui.touch-punch.min.js"></script>
    <asp:HiddenField ID="HiddenFieldCallCycleId" runat="server" />
    <asp:HiddenField ID="HiddenFieldDue1" runat="server" />
    <asp:HiddenField ID="HiddenFieldDue2" runat="server" />
    <asp:HiddenField ID="HiddenFieldDue3" runat="server" />
    <asp:HiddenField ID="HiddenFieldDue4" runat="server" />
    <asp:HiddenField ID="HiddenFieldDue5" runat="server" />
    <asp:HiddenField ID="HiddenFieldTime1" runat="server" />
    <asp:HiddenField ID="HiddenFieldTime2" runat="server" />
    <asp:HiddenField ID="HiddenFieldTime3" runat="server" />
    <asp:HiddenField ID="HiddenFieldTime4" runat="server" />
    <asp:HiddenField ID="HiddenFieldTime5" runat="server" />
    <asp:HiddenField ID="HiddenFieldCheck" runat="server" />
    <asp:HiddenField ID="HiddenField1TemplateName" runat="server" />
    <asp:HiddenField ID="HiddenFieldHasClickedConfirmDialogBox" runat="server" />
    <asp:HiddenField ID="HiddenFieldHasChanged" runat="server" Value="0" />
    <div id="saveConfirmation" style="display: none">
        <div id="div_confirm_message">
            <strong>You have made some changes.
                <br />
                Do you want to save schedule template?</strong>
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>
    <div id="schedule_window" style="display: none">
        <div id="schedule_subwindow">
        </div>
    </div>
    <div id="callcyle_window" style="display: none">
        <div class="popup_div">
            <div class="col1">
                &nbsp;</div>
            <div class="col2">
                Day 1</div>
            <div class="col3">
                Day 2</div>
            <div class="col4">
                Day 3</div>
            <div class="col5">
                Day 4</div>
            <div class="col6">
                Day 5</div>
            <div class="clear">
            </div>
            <div class="col1">
                Due On</div>
            <div class="col2">
                <input type="text" id="dueon1" runat="server" style="width: 120px;" /></div>
            <div class="col3">
                <input type="text" id="dueon2" runat="server" style="width: 120px;" /></div>
            <div class="col4">
                <input type="text" id="dueon3" runat="server" style="width: 120px;" /></div>
            <div class="col5">
                <input type="text" id="dueon4" runat="server" style="width: 120px;" /></div>
            <div class="col6">
                <input type="text" id="dueon5" runat="server" style="width: 120px;" /></div>
            <div class="clear">
            </div>
            <div class="col1">
                Start Time</div>
            <div class="col2">
                <input type="text" id="time1" runat="server" style="width: 70px;" value="05:00" /></div>
            <div class="col3">
                <input type="text" id="time2" runat="server" style="width: 70px;" value="05:00" /></div>
            <div class="col4">
                <input type="text" id="time3" runat="server" style="width: 70px;" value="05:00" /></div>
            <div class="col5">
                <input type="text" id="time4" runat="server" style="width: 70px;" value="05:00" /></div>
            <div class="col6">
                <input type="text" id="time5" runat="server" style="width: 70px;" value="05:00" /></div>
            <div class="clear">
            </div>
            <div class="col1">
                Select</div>
            <div class="col2">
                <input type="checkbox" id="Checkbox1" runat="server" checked="checked" /></div>
            <div class="col3">
                <input type="checkbox" id="Checkbox2" runat="server" checked="checked" /></div>
            <div class="col4">
                <input type="checkbox" id="Checkbox3" runat="server" checked="checked" /></div>
            <div class="col5">
                <input type="checkbox" id="Checkbox4" runat="server" checked="checked" /></div>
            <div class="col6">
                <input type="checkbox" id="Checkbox5" runat="server" checked="checked" /></div>
            <div class="clear">
            </div>
            <%-- <input type="button" id="buttoncancel" runat="server" OnServerClick="buttonok_Click" class="btn" value="Cancel"/>--%>
            <input type="button" id="button3" runat="server" class="btn" value="Cancel" onclick="closepopup();" />
            <input type="button" onserverclick="buttonok_Click" id="buttonok" runat="server"
                onclick="setvalue();" class="btn" value="Ok" />
        </div>
    </div>
    <div id="template_save_window" style="display: none">
        <div class="popup_div">
            <div class="col">
                Template Name</div>
            <div class="col">
                <input type="text" id="txtTemplateName" runat="server" style="width: 120px;"></div>
            <input type="button" id="button1" runat="server" onserverclick="buttonok_Click" class="btn"
                value="Cancel" />
            <input type="button" onserverclick="buttonTemplateSave_Click" id="button2" runat="server"
                onclick="setTemplateSavevalue();" class="btn" value="Ok" />
        </div>
    </div>

    <%--<div id="div_change_route" style="display: none">
        <div id="Div2">bhathiya
        </div>
    </div>--%>

    <asp:HiddenField ID="hdfSDayId" runat="server" ClientIDMode="Static" />
    <asp:HiddenField ID="hdfSSortId" runat="server" ClientIDMode="Static" />
    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left1" id="div_content">
                <div class="hoributton">
                    <div>
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                  <div class="clearall"></div>
                </div>
            </div>
            <div class="toolbar_right2" id="div3">
                <div class="leadentry_title_bar">
                <ucl2:buttonbarNavi ID="buttonbarNavi" runat="server" />
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/default.aspx">
                        </asp:HyperLink>
                </div>
            </div>
            <div class="clearall">
            </div>
            <div id="div_message" runat="server" style="display: none;">
            </div>
            <div class="clearall">
            </div>
            <div class="formleft">
                <div class="formtextdiv" style="color:black;">
                    Schedule/Template</div>
                <div class="formdetaildiv_right">
                    <asp:TextBox ID="txtScheduleTemplate" runat="server" CssClass="input-large"></asp:TextBox>
                    <div id="div_LookupButton" runat="server">
                        <a href="#" id="id_schedule">
                            <img id="img1" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" />
                        </a>
                    </div>
                </div>
                <div class="clearall">
                </div>
            </div>
            <div class="formright">
                <div class="formtextdiv">
                    Start From
                </div>
                <div class="formdetaildiv_right">
                        <asp:TextBox ID="txtStartDate" runat="server"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div class="clearall">
        </div>
        <div id="div_schedule" style="overflow-x: scroll; overflow-y: scroll; height: 500px;">
        </div>
        <div id="div_custgrid">
        </div>
    </div>
    <script type="text/javascript">

        function SaveChangeRouteItem() {
            var SDayId = $('#hdfSDayId').val();
            var SSortId = $('#hdfSSortId').val();
            var TDayId = $('#cmdRoute').val();
            if (SDayId != TDayId)
                ChangeRoute(SDayId, SSortId, TDayId);
            $('#div_change_route').css('display', 'none');
            $('#hdfSDayId').val('0');
            $('#hdfSSortId').val('0');
        }

//        $("#a_change_route").click(function () {
//            var SDayId = $('#hdfSDayId').val(dayorder);
//            var SSortId = $('#hdfSSortId').val(clickedindex);
//            var TDayId = $('#cmdRoute').val(clickedindex);
//            if (SDayId != TDayId)
//                ChangeRoute(SDayId, SSortId, TDayId);
//            $('#div_change_route').css('display', 'none');
//            $('#hdfSDayId').val('0');
//            $('#hdfSSortId').val('0');
//        });

        function ChangeCallCycleItem(SDayId, SSortId) {
            if ($('#div_change_route').css('display') == 'block') {
                if ($('#hdfSDayId').val() == SDayId) {
                    $('#div_change_route').css('display', 'none');
                    $('#hdfSDayId').val('0');
                    $('#hdfSSortId').val('0');
                } else {
                    $('#div_change_route').css('display', 'block');
                    //$('#div_change_route').toggle();
                    $('#cmdRoute').val(SDayId);
                    $('#hdfSDayId').val(SDayId);
                    $('#hdfSSortId').val(SSortId);
                }
            } else {
                $('#div_change_route').css('display', 'block');
                //$('#div_change_route').toggle();
                $('#cmdRoute').val(SDayId);
                $('#hdfSDayId').val(SDayId);
                $('#hdfSSortId').val(SSortId);
            }
        }

        function ChangeRoute(SDayId, SSortId, TDayId) {
            $("#div_loader").show();
            //alert(targetday);
            var url = ROOT_PATH + "call_cycle/process_forms/processmaster.aspx?fm=changeroute&type=change&sdayid=" + SDayId + "&ssortid=" + SSortId + "&tdayid=" + TDayId;
            $.ajax({
                url: url,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg != "") {
                        $("#div_loader").hide();
                        $("#div_schedule").html(msg);
                    }
                },
                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                        $("#div_loader").hide();
                        //$("#" + tagertdiv).html(msg);
                    }
                }
            });
        }

        function SaveSchedule() {
            var wnd = $("#saveConfirmation").kendoWindow({
                title: "Schedule Template",
                modal: true,
                visible: false,
                resizable: false,
                width: 300
            }).data("kendoWindow");

            wnd.center().open();

            $("#yes").click(function () {
                $('#MainContent_HiddenFieldHasChanged').val('2');

                document.getElementById("MainContent_buttonok").click();
            });
            $("#no").click(function () {
                $('#MainContent_HiddenFieldHasChanged').val('1');
                document.getElementById("MainContent_buttonok").click();
            });
        }
    </script>
    <script type="text/javascript">
        function onChange(e) {
            //alert($("#MainContent_txtStartDate").val());
            var selectedDate = $("#MainContent_txtStartDate").val();
            var param = { "selectedDate": selectedDate };
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/call_cycle/call_cycle_service.asmx/IsItineraryHeaderApprovedForTheMonth",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.hasOwnProperty('d')) {
                        msg = response.d;
                    } else {
                        msg = response;
                    }
                    if (msg == "true") {
                        $("#MainContent_buttonbar_buttinSave").removeClass('savebtn').addClass('dissavebtn');
                        $("#MainContent_buttonbar_buttinSave").prop('disabled', true);
                        //Set Notification
                        var infoMsg = GetInformationMessageDiv("Your Itinerary For the Month " + selectedDate.substr(3) + " has been Approved. !", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(infoMsg);
                        hideStatusDiv("MainContent_div_message");
                    }
                    else {
                        $("#MainContent_buttonbar_buttinSave").removeClass('dissavebtn').addClass('savebtn');
                        $("#MainContent_buttonbar_buttinSave").prop('disabled', false);
                    }
                },
                error: function (response) {
                    //alert("Oops, something went horribly wrong");
                    $("#MainContent_buttonbar_buttinSave").removeClass('dissavebtn').addClass('savebtn');
                    $("#MainContent_buttonbar_buttinSave").prop('disabled', false);
                }
            });
        }


        $(document).ready(function () {
            //GetAllCallCyclesList();
            $("#MainContent_buttonbarNavi_hlBack").css("display", "none");
            $("#MainContent_txtStartDate").kendoDatePicker({
                //value: new Date(),
                change: onChange,
                format: "dd-MMM-yyyy"
                // timeFormat: "HH:mm"
            });
            hideStatusDiv("MainContent_div_message");
            $("#id_schedule").click(function () {
                GetAllCallCyclesList(); // scheduletemplates('call_cycle/process_forms/processmaster.aspx?fm=templateentry&type=query');
            });



            var window = $("#schedule_window"),
                        undo = $("#undo")
                                .bind("click", function () {
                                    window.data("kendoWindow").open();
                                    undo.hide();
                                });

            var onClose = function () {
                undo.show();
            }

            if (!window.data("kendoWindow")) {
                window.kendoWindow({
                    width: "550px",
                    height: "400px",
                    title: "Schedule Template",
                    close: onClose
                });
            }

            var callcyle_window = $("#callcyle_window"),
                        undo = $("#undo")
                                .bind("click", function () {
                                    callcyle_window.data("kendoWindow").open();
                                    undo.hide();
                                });

            var onClose = function () {
                undo.show();
            }

            if (!callcyle_window.data("kendoWindow")) {
                callcyle_window.kendoWindow({
                    width: "850px",
                    height: "200px",
                    close: onClose
                });
            }

            var template_save_window = $("#template_save_window"),
                        undo = $("#undo")
                                .bind("click", function () {
                                    callcyle_window.data("kendoWindow").open();
                                    undo.hide();
                                });

            var onClose = function () {
                undo.show();
            }

            if (!template_save_window.data("kendoWindow")) {
                template_save_window.kendoWindow({
                    title: "CTC crm",
                    width: "300px",
                    height: "130px",
                    close: onClose
                });
            }


            function drag(target, e) {
                // alert(target.parentNode.id);
                e.dataTransfer.setData('Text', target.id);
            }

            function drop(target, e) {
                var id = e.dataTransfer.getData('Text');


                target.appendChild(document.getElementById(id));
                e.preventDefault();
            }

            var today = new Date();
            var tomorrow = new Date();
            //tomorrow.setDate(today.getDate() - 2);
            tomorrow = getStartOfWeek(new Date());

            //              $("#MainContent_dueon1").kendoDatePicker({
            //                  value: tomorrow,
            //                  format: "dd-MMM-yyyy",
            //                  change: function () {
            //                      var month = this.value().getMonth();
            //                      var fullyear = this.value().getFullYear();
            //                      var day = this.value().getDate();
            //                      var StartDate = new Date(fullyear, month, day + 1);
            //                      $("#MainContent_dueon2").data("kendoDatePicker").value(StartDate);
            //                      StartDate = new Date(fullyear, month, day + 2);
            //                      $("#MainContent_dueon3").data("kendoDatePicker").value(StartDate);
            //                      StartDate = new Date(fullyear, month, day + 3);
            //                      $("#MainContent_dueon4").data("kendoDatePicker").value(StartDate);
            //                      StartDate = new Date(fullyear, month, day + 4);
            //                      $("#MainContent_dueon5").data("kendoDatePicker").value(StartDate);
            //                  }
            //              });

            //              $('#MainContent_dueon1').attr('disabled', 'disabled');

            //              tomorrow.setDate(today.getDate() - 1);
            //              $("#MainContent_dueon2").kendoDatePicker({
            //                  value: tomorrow,
            //                  format: "dd-MMM-yyyy",
            //                  change: function () {
            //                      var month = this.value().getMonth();
            //                      var fullyear = this.value().getFullYear();
            //                      var day = this.value().getDate();
            //                      var StartDate = new Date(fullyear, month, day + 1);
            //                      $("#MainContent_dueon3").data("kendoDatePicker").value(StartDate);
            //                      StartDate = new Date(fullyear, month, day + 2);
            //                      $("#MainContent_dueon4").data("kendoDatePicker").value(StartDate);
            //                      StartDate = new Date(fullyear, month, day + 3);
            //                      $("#MainContent_dueon5").data("kendoDatePicker").value(StartDate);
            //                  }
            //              });
            //              $('#MainContent_dueon2').attr('disabled', 'disabled');

            //              $("#MainContent_dueon3").kendoDatePicker({
            //                  value: new Date(),
            //                  format: "dd-MMM-yyyy",
            //                  change: function () {
            //                      var month = this.value().getMonth();
            //                      var fullyear = this.value().getFullYear();
            //                      var day = this.value().getDate();
            //                      var StartDate = new Date(fullyear, month, day + 1);
            //                      $("#MainContent_dueon4").data("kendoDatePicker").value(StartDate);
            //                      StartDate = new Date(fullyear, month, day + 2);
            //                      $("#MainContent_dueon5").data("kendoDatePicker").value(StartDate);
            //                  }
            //              });
            //              $('#MainContent_dueon3').attr('disabled', 'disabled');

            //              tomorrow.setDate(today.getDate() + 1);
            //              $("#MainContent_dueon4").kendoDatePicker({
            //                  value: tomorrow,
            //                  format: "dd-MMM-yyyy",
            //                  change: function () {
            //                      var month = this.value().getMonth();
            //                      var fullyear = this.value().getFullYear();
            //                      var day = this.value().getDate();
            //                      var StartDate = new Date(fullyear, month, day + 1);
            //                      $("#MainContent_dueon5").data("kendoDatePicker").value(StartDate);
            //                  }
            //              });
            //              $('#MainContent_dueon4').attr('disabled', 'disabled');

            //              tomorrow.setDate(today.getDate() + 2);
            //              $("#MainContent_dueon5").kendoDatePicker({
            //                  value: tomorrow,
            //                  format: "dd-MMM-yyyy"
            //              });
            //              $('#MainContent_dueon5').attr('disabled', 'disabled');

            //              $("#MainContent_time1").kendoTimePicker({
            //                  format: "HH:mm",
            //                  interval: 60
            //              });
            //              $('#MainContent_time1').attr('disabled', 'disabled');

            //              $("#MainContent_time2").kendoTimePicker({
            //                  format: "HH:mm",
            //                  interval: 60
            //              });
            //              $('#MainContent_time2').attr('disabled', 'disabled');

            //              $("#MainContent_time3").kendoTimePicker({
            //                  format: "HH:mm",
            //                  interval: 60
            //              });
            //              $('#MainContent_time3').attr('disabled', 'disabled');

            //              $("#MainContent_time4").kendoTimePicker({
            //                  format: "HH:mm",
            //                  interval: 60
            //              });
            //              $('#MainContent_time4').attr('disabled', 'disabled');

            //              $("#MainContent_time5").kendoTimePicker({
            //                  format: "HH:mm",
            //                  interval: 60
            //              });
            //              $('#MainContent_time5').attr('disabled', 'disabled');

        });

        function closepopup() {
            var wnd = $("#callcyle_window").kendoWindow({
                title: "Add/Edit Address",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
        }

        function OpenCallCycleForTME(code, description) {
            OpenCallCycles(code, description);
        }

    </script>
</asp:Content>
