﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using CRMServiceReference;
using Peercore.CRM.Common;
using System.ServiceModel.Description;

public partial class call_cycle_transaction_template_entry : PageBase
{
    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
            //buttonbar.onButtonAddCallCycle = new usercontrols_buttonbar.ButtonAddCallCycle(ButtonAddCallCycle_Click);
            //buttonbar.onButtonAddLeadsCustomer = new usercontrols_buttonbar.ButtonAddLeadsCustomer(ButtonLeadsCustomer_Click);
            buttonbarNavi.onButtonNext = new usercontrols_buttonbar_navigation.ButtonNext(buttonNext_Click);
            buttonbarNavi.onButtonPrevious = new usercontrols_buttonbar_navigation.ButtonPrevious(buttonPrevious_Click);
            //buttonbar.onButtonSaveNewTemplate = new usercontrols_buttonbar.ButtonSaveNewTemplate(ButtonSaveNewTemplate_Click);

            buttonbar.EnableSave(true);
            buttonbar.VisibleSave(true);
            //buttonbar.VisibleActivityHistory(false);
            //buttonbar.VisibleAddCallCycle(true);
            //buttonbar.VisibleAddLeadsCustomer(true);
            //buttonbar.VisibleSaveNewTemplate(true);

            if (!IsPostBack)
            {
                txtStartDate.Text = DateTime.Today.ToString("dd-MMM-yyyy");
                if (UserSession.Instance.OriginatorString == "ASE")
                {
                    //txtScheduleTemplate.ReadOnly = true;
                    //div_LookupButton.Attributes.Add("style", "display:none;");
                    //LoadScheduleForTME();
                }
            }

            if (!string.IsNullOrEmpty(HiddenFieldCallCycleId.Value))
            {
                ViewState["CallCycleId"] = HiddenFieldCallCycleId.Value;
            }
            else if (Request.QueryString["callcycid"] != null && Request.QueryString["callcycid"] != string.Empty)
            {
                ViewState["CallCycleId"] = Request.QueryString["callcycid"].ToString();
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "loadTemplate('" + ViewState["CallCycleId"].ToString() + "');", true);
                // SetBackLink();
                hlBack.NavigateUrl = "~/lead_customer/transaction/lead_contacts.aspx";
            }
            else if (ViewState["CallCycleId"] != null && ViewState["CallCycleId"] != "")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "loadTemplate('" + ViewState["CallCycleId"].ToString() + "');", true);
            }
            else
            {
                //buttonbar.EnableSave(false);
                //buttonbar.EnableTemplate(false);
                //buttonbar.EnableAddCallCycle(false);
                buttonbar.EnableAddLeadsCustomer(false);
                //buttonbar.EnableSaveNewTemplate(false);
            }

            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Route Scheduler ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    //protected void SetBackLink() {
    //    string a = Request.QueryString.ToString();
    //    hlBack.NavigateUrl
    //}

    protected void ButtonSave_Click(object sender)
    {
        ScheduleStruct schedul_struct = new ScheduleStruct();
        if (Session[CommonUtility.SCHEDULE_LOG] != null)
        {
            schedul_struct = (ScheduleStruct)Session[CommonUtility.SCHEDULE_LOG];

            List<CallCycleContactDTO> callcyclelist = AddCustomersToList(schedul_struct);

            CallCycleClient callCycleClient = new CallCycleClient();

            foreach (var operationDescription in callCycleClient.Endpoint.Contract.Operations)
            {
                DataContractSerializerOperationBehavior dcsob =
                    operationDescription.Behaviors.Find<DataContractSerializerOperationBehavior>();
                if (dcsob != null)
                {
                    dcsob.MaxItemsInObjectGraph = 2147483647;
                }
            }

            CommonUtility commonUtility = new CommonUtility();
            DistributorClient distributorClient = new DistributorClient();


            DateTime effDate = Convert.ToDateTime(txtStartDate.Text);
            ItineraryHeaderDTO itineraryHeaderDTO = callCycleClient.GetItineraryHeaderByUser(UserSession.Instance.UserName, effDate);
            if (!String.IsNullOrEmpty(itineraryHeaderDTO.Status))
            {
                if (itineraryHeaderDTO.Status.Trim().Equals("APRV"))
                {
                    buttonbar.EnableSave(false);
                    //Set Notification
                    div_message.Attributes.Add("style", "display:block;");
                    div_message.InnerHtml = commonUtility.GetInfoMessage("Your Itinerary For the Month " + effDate.ToString("MMM-yyyy") + " has been Approved. !");
                    return;
                }
            }

            if (callCycleClient.SaveSchedule(callcyclelist))
            {
                string OriginatorType = UserSession.Instance.OriginatorString;
                //CommonUtility commonUtility = new CommonUtility();
                string callcycle = "0";
                if (!string.IsNullOrEmpty(HiddenFieldCallCycleId.Value))
                {
                    callcycle = HiddenFieldCallCycleId.Value;
                }
                else if (Request.QueryString["callcycid"] != null && Request.QueryString["callcycid"] != string.Empty)
                {
                    callcycle = Request.QueryString["callcycid"].ToString();
                }
                else if (ViewState["CallCycleId"] != null && ViewState["CallCycleId"] != "")
                {
                    callcycle = ViewState["CallCycleId"].ToString();
                }
                //if (callCycleClient.CreateCallCycleActivitiesFromTemplate(callCycleDTO, customerList, args))
                ActivityDTO activitydto = new ActivityDTO();
                DateTime today = DateTime.Today;

                try
                {
                    activitydto.StartDate = Convert.ToDateTime(txtStartDate.Text);
                }
                catch
                {
                    div_message.Attributes.Add("style", "display:block;");
                    div_message.InnerHtml = commonUtility.GetErrorMessage("Start Date is not valid !");
                    return;
                }

                activitydto.EndDate = new DateTime(activitydto.StartDate.Year, activitydto.StartDate.Month, DateTime.DaysInMonth(activitydto.StartDate.Year, activitydto.StartDate.Month));
                activitydto.RepGroupName = schedul_struct.RepCode;

                string holiday = distributorClient.GetDistributorHoliday(0, null, activitydto.RepGroupName);

                activitydto.Comments = holiday;
                activitydto.CreatedBy = UserSession.Instance.UserName;
                activitydto.CallCycleID = Convert.ToInt32(callcycle);

                if (schedul_struct.RepCode == null)
                {
                    OriginatorType = "ASE";
                }
                else
                {
                    OriginatorType = "DR";
                }
                if (callCycleClient.SchedulePlanedRoutesForDR(activitydto, OriginatorType))
                {
                    div_message.Attributes.Add("style", "display:block;");
                    div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved.");
                }
                else
                {
                    div_message.Attributes.Add("style", "display:block;");
                    div_message.InnerHtml = commonUtility.GetErrorMessage("Error Occured");
                }

                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "loadTemplate('" + schedul_struct.CallCycleId.ToString() + "');", true);

                try
                {
                    CommonClient cClient = new CommonClient();
                    cClient.CreateTransactionLog(UserSession.Instance.OriginalUserName,
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        TransactionTypeModules.Update,
                        TransactionModules.RouteSchedule,
                        UserSession.Instance.OriginalUserName + " update route scedule for " + schedul_struct.RepCode);
                }
                catch { }

                buttonbar.EnableSave(true);
                buttonbar.EnableTemplate(true);
                buttonbar.EnableAddCallCycle(true);
                buttonbar.EnableAddLeadsCustomer(true);
                buttonbar.EnableSaveNewTemplate(true);
            }
        }
    }

    protected void ButtonAddCallCycle_Click(object sender)
    {
    }

    //protected void ButtonLeadsCustomer_Click(object sender)
    //{
    //    string callcycle = "";
    //    if (!string.IsNullOrEmpty(HiddenFieldCallCycleId.Value))
    //    {
    //        callcycle = HiddenFieldCallCycleId.Value;
    //    }
    //    else if (Request.QueryString["callcycid"] != null && Request.QueryString["callcycid"] != string.Empty)
    //    {
    //        callcycle = Request.QueryString["callcycid"].ToString();
    //    }
    //    else if (ViewState["CallCycleId"] != null && ViewState["CallCycleId"] != "")
    //    {
    //        callcycle = ViewState["CallCycleId"].ToString();
    //    }
    //    Response.Redirect(ConfigUtil.ApplicationPath + "call_cycle/transaction/schedule_entry.aspx?callcycid=" + callcycle);
    //}

    protected void ButtonSaveNewTemplate_Click(object sender)
    {
    }

    protected void buttonok_Click(object sender, EventArgs e)
    {
        string reloadMethod = "";
        string callcycle = "";
        CallCycleDTO callCycleDTO = new CallCycleDTO();
        List<CallCycleContactDTO> customerList = new List<CallCycleContactDTO>();

        //Create Schedule List.
        List<SchedularTransferComponentDTO> schedularList = new List<SchedularTransferComponentDTO>();//SetupScheduleList();

        ScheduleStruct schedul_struct = new ScheduleStruct();
        CallCycleClient callCycleClient = new CallCycleClient();
        ArgsDTO args = new ArgsDTO();
        if (UserSession.Instance != null)
        {
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.Originator = UserSession.Instance.UserName;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        }

        if (!string.IsNullOrEmpty(HiddenFieldCallCycleId.Value))
        {
            callcycle = HiddenFieldCallCycleId.Value;
        }
        else if (Request.QueryString["callcycid"] != null && Request.QueryString["callcycid"] != string.Empty)
        {
            callcycle = Request.QueryString["callcycid"].ToString();
        }
        else if (ViewState["CallCycleId"] != null && ViewState["CallCycleId"] != "")
        {
            callcycle = ViewState["CallCycleId"].ToString();
        }

        callCycleDTO.CallCycleID = int.Parse(callcycle);
        callCycleDTO.Description = txtScheduleTemplate.Text;

        if (Session[CommonUtility.SCHEDULE_LOG] != null)
        {
            schedul_struct = (ScheduleStruct)Session[CommonUtility.SCHEDULE_LOG];

            if (HiddenFieldHasChanged.Value == "0")
            {
                schedularList = SetupScheduleList();
            }

            if (schedul_struct.HasChanged && HiddenFieldHasChanged.Value == "0")
            {
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "SaveSchedule();", true);
                return;
            }
            else if (schedul_struct.HasChanged && HiddenFieldHasChanged.Value == "2")
            {
                List<CallCycleContactDTO> callcyclelist = AddCustomersToList(schedul_struct);
                callCycleClient.SaveSchedule(callcyclelist);
                reloadMethod = "LoadTemplatesBySession";
            }

            if (Session[CommonUtility.SCHEDULETIME_LOG] != null)
            {
                schedularList = Session[CommonUtility.SCHEDULETIME_LOG] as List<SchedularTransferComponentDTO>;
            }

            customerList = AddCustomersToList(schedul_struct);

            //if (callCycleClient.CreateCallCycleActivitiesFromTemplate(callCycleDTO, customerList, args))
            ActivityDTO activitydto = new ActivityDTO();
            DateTime today = DateTime.Today;
            activitydto.StartDate = DateTime.Now;
            activitydto.EndDate = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month));
            activitydto.Comments = "Sunday";
            activitydto.RepGroupName = schedul_struct.RepCode;
            activitydto.CreatedBy = UserSession.Instance.UserName;
            activitydto.CallCycleID = Convert.ToInt32(callcycle);

            if (callCycleClient.SchedulePlanedRoutesForDR(activitydto,UserSession.Instance.OriginatorString))
            {
                CommonUtility commonUtility = new CommonUtility();

                div_message.Attributes.Add("style", "display:block;");
                div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved.");

                if (HiddenFieldHasChanged.Value == "1")
                    reloadMethod = "LoadTemplatesBySession();";
                else
                    reloadMethod = "loadTemplate('" + schedul_struct.CallCycleId.ToString() + "');";

                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", reloadMethod, true);

                HiddenFieldHasChanged.Value = "0";

                buttonbar.EnableSave(true);
                buttonbar.EnableTemplate(true);
                buttonbar.EnableAddCallCycle(true);
                buttonbar.EnableAddLeadsCustomer(true);
                buttonbar.EnableSaveNewTemplate(true);
            }
        }
    }

    protected void buttonTemplateSave_Click(object sender, EventArgs e)
    {
        string ddd = HiddenField1TemplateName.Value;
        bool iNoRecs = false;

        try
        {
            CallCycleDTO callCycle = new CallCycleDTO();
            callCycle.CallCycleID = 0;
            callCycle.Description = HiddenField1TemplateName.Value;
            callCycle.DueOn = DateTime.Today;
            callCycle.Comments = "";
            callCycle.DelFlag = "N";
            callCycle.CCType = UserSession.Instance.RepType;

            List<SchedularTransferComponentDTO> schedularList = SetupScheduleList();
            List<CallCycleContactDTO> customerList = new List<CallCycleContactDTO>();
            ScheduleStruct schedul_struct = new ScheduleStruct();
            CallCycleClient callCycleClient = new CallCycleClient();

            schedul_struct = (ScheduleStruct)Session[CommonUtility.SCHEDULE_LOG];

            if (Session[CommonUtility.SCHEDULETIME_LOG] != null)
            {
                schedularList = Session[CommonUtility.SCHEDULETIME_LOG] as List<SchedularTransferComponentDTO>;
            }

            customerList = AddCustomersToList(schedul_struct);

            TimeSpan tsStartTime = new TimeSpan(12, 0, 0);
            int callcycleId = 0;
            iNoRecs = callCycleClient.CallCycleSave(callCycle, customerList, UserSession.Instance.UserName, tsStartTime, out callcycleId);

            if (iNoRecs)
            {
                ViewState["CallCycleId"] = HiddenFieldCallCycleId.Value = callcycleId.ToString();

                txtScheduleTemplate.Text = callCycle.Description;
                div_message.Attributes.Add("style", "display:block");
                div_message.InnerHtml = new CommonUtility().GetSucessfullMessage("Successfully Saved.");
                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "loadTemplate('" + ViewState["CallCycleId"].ToString() + "');", true);
            }
        }
        catch (Exception ex)
        {
            //MessageBox.Show(ex.Message);
        }
    }

    #endregion

    #region Methods

    private void BindControl()
    {

    }

    private List<CallCycleContactDTO> AddCustomersToList(ScheduleStruct scheduleStruct)
    {
        List<CallCycleContactDTO> customerList = new List<CallCycleContactDTO>();

        int dayOrder = 0;

        foreach (var dayCallCycle in scheduleStruct.DayCallCycleList)
        {
            if (dayCallCycle.DayList != null)
            {
                dayOrder = 1;

                foreach (CallCycleContactDTO item in dayCallCycle.DayList)
                {
                    if (item != null)
                    {
                        item.WeekDayId = dayCallCycle.WeekDayId;
                        item.DayOrder = dayOrder;
                        if (scheduleStruct.DRRoutes.Count >= 1)
                            item.RouteId = scheduleStruct.DRRoutes[dayCallCycle.WeekDayId-1].RouteMasterId;
                        
                        customerList.Add(item);
                    }

                    dayOrder++;
                }
            }
        }

        return customerList;
    }

    //private List<CallCycleContactDTO> AddCustomersToList(ScheduleStruct scheduleStruct, List<SchedularTransferComponentDTO> schedulerDetails = null)
    //{
    //    int iDayOrder = 0;
    //    List<CallCycleContactDTO> customerList = new List<CallCycleContactDTO>();

    //    SchedularTransferComponentDTO day1Schedule = null;
    //    SchedularTransferComponentDTO day2Schedule = null;
    //    SchedularTransferComponentDTO day3Schedule = null;
    //    SchedularTransferComponentDTO day4Schedule = null;
    //    SchedularTransferComponentDTO day5Schedule = null;

    //    if (schedulerDetails != null)
    //    {
    //        day1Schedule = new SchedularTransferComponentDTO();
    //        day1Schedule = schedulerDetails.FirstOrDefault(s => s.WeekDay == 1);
    //        day1Schedule.SelectedTime = day1Schedule.SelectedTime.Subtract(TimeSpan.FromHours(1));

    //        day2Schedule = new SchedularTransferComponentDTO();
    //        day2Schedule = schedulerDetails.FirstOrDefault(s => s.WeekDay == 2);
    //        day2Schedule.SelectedTime = day2Schedule.SelectedTime.Subtract(TimeSpan.FromHours(1));

    //        day3Schedule = new SchedularTransferComponentDTO();
    //        day3Schedule = schedulerDetails.FirstOrDefault(s => s.WeekDay == 3);
    //        day3Schedule.SelectedTime = day3Schedule.SelectedTime.Subtract(TimeSpan.FromHours(1));

    //        day4Schedule = new SchedularTransferComponentDTO();
    //        day4Schedule = schedulerDetails.FirstOrDefault(s => s.WeekDay == 4);
    //        day4Schedule.SelectedTime = day4Schedule.SelectedTime.Subtract(TimeSpan.FromHours(1));

    //        day5Schedule = new SchedularTransferComponentDTO();
    //        day5Schedule = schedulerDetails.FirstOrDefault(s => s.WeekDay == 5);
    //        day5Schedule.SelectedTime = day5Schedule.SelectedTime.Subtract(TimeSpan.FromHours(1));
    //    }

    //    if (scheduleStruct.DayList_1 != null)
    //    {
    //        foreach (CallCycleContactDTO item in scheduleStruct.DayList_1)
    //        {
    //            if (item != null)
    //            {
    //                item.WeekDayId = 1;
    //                //item.DayOrder = ++iDayOrder;

    //                if (day1Schedule != null && day1Schedule.SelectedTime < TimeSpan.FromHours(18))
    //                    day1Schedule.SelectedTime = day1Schedule.SelectedTime.Add(TimeSpan.FromHours(1));

    //                if (schedulerDetails != null)
    //                {
    //                    item.StartTime = day1Schedule.SelectedTime;
    //                    item.DueOn = day1Schedule.SelectedDate;
    //                    item.CreateActivity = day1Schedule.CreateActivity;
    //                }

    //                if (scheduleStruct.DRRoutes.Count >= 1)
    //                    item.RouteId = scheduleStruct.DRRoutes[0].RouteMasterId;

    //                customerList.Add(item);
    //            }
    //        }

    //    }
    //    iDayOrder = 0;


    //    if (scheduleStruct.DayList_2 != null)
    //    {

    //        foreach (CallCycleContactDTO item in scheduleStruct.DayList_2)
    //        {
    //            if (item != null)
    //            {
    //                item.WeekDayId = 2;
    //                //item.DayOrder = ++iDayOrder;

    //                if (day2Schedule != null && day2Schedule.SelectedTime < TimeSpan.FromHours(18))
    //                    day2Schedule.SelectedTime = day2Schedule.SelectedTime.Add(TimeSpan.FromHours(1));

    //                if (schedulerDetails != null)
    //                {
    //                    item.StartTime = day2Schedule.SelectedTime;
    //                    item.DueOn = day2Schedule.SelectedDate;
    //                    item.CreateActivity = day2Schedule.CreateActivity;
    //                }

    //                if (scheduleStruct.DRRoutes.Count >= 2)
    //                    item.RouteId = scheduleStruct.DRRoutes[1].RouteMasterId;

    //                customerList.Add(item);
    //            }
    //        }

    //    }
    //    iDayOrder = 0;

    //    if (scheduleStruct.DayList_3 != null)
    //    {

    //        foreach (CallCycleContactDTO item in scheduleStruct.DayList_3)
    //        {
    //            if (item != null)
    //            {
    //                item.WeekDayId = 3;
    //                //item.DayOrder = ++iDayOrder;

    //                if (day3Schedule != null && day3Schedule.SelectedTime < TimeSpan.FromHours(18))
    //                    day3Schedule.SelectedTime = day3Schedule.SelectedTime.Add(TimeSpan.FromHours(1));

    //                if (schedulerDetails != null)
    //                {
    //                    item.StartTime = day3Schedule.SelectedTime;
    //                    item.DueOn = day3Schedule.SelectedDate;
    //                    item.CreateActivity = day3Schedule.CreateActivity;
    //                }

    //                if (scheduleStruct.DRRoutes.Count >= 3)
    //                    item.RouteId = scheduleStruct.DRRoutes[2].RouteMasterId;

    //                customerList.Add(item);
    //            }
    //        }

    //        iDayOrder = 0;
    //    }


    //    if (scheduleStruct.DayList_4 != null)
    //    {
    //        foreach (CallCycleContactDTO item in scheduleStruct.DayList_4)
    //        {
    //            if (item != null)
    //            {
    //                item.WeekDayId = 4;
    //                //item.DayOrder = ++iDayOrder;

    //                if (day4Schedule != null && day4Schedule.SelectedTime < TimeSpan.FromHours(18))
    //                    day4Schedule.SelectedTime = day4Schedule.SelectedTime.Add(TimeSpan.FromHours(1));

    //                if (schedulerDetails != null)
    //                {
    //                    item.StartTime = day4Schedule.SelectedTime;
    //                    item.DueOn = day4Schedule.SelectedDate;
    //                    item.CreateActivity = day4Schedule.CreateActivity;
    //                }

    //                if (scheduleStruct.DRRoutes.Count >= 4)
    //                    item.RouteId = scheduleStruct.DRRoutes[3].RouteMasterId;

    //                customerList.Add(item);
    //            }
    //        }
    //    }

    //    iDayOrder = 0;


    //    if (scheduleStruct.DayList_5 != null)
    //    {
    //        foreach (CallCycleContactDTO item in scheduleStruct.DayList_5)
    //        {
    //            if (item != null)
    //            {
    //                item.WeekDayId = 5;
    //                // item.DayOrder = ++iDayOrder;

    //                if (day5Schedule != null && day5Schedule.SelectedTime < TimeSpan.FromHours(18))
    //                    day5Schedule.SelectedTime = day5Schedule.SelectedTime.Add(TimeSpan.FromHours(1));

    //                if (schedulerDetails != null)
    //                {
    //                    item.StartTime = day5Schedule.SelectedTime;
    //                    item.DueOn = day5Schedule.SelectedDate;
    //                    item.CreateActivity = day5Schedule.CreateActivity;
    //                }

    //                if (scheduleStruct.DRRoutes.Count >= 5)
    //                    item.RouteId = scheduleStruct.DRRoutes[4].RouteMasterId;

    //                customerList.Add(item);
    //            }
    //        }
    //    }

    //    return customerList;
    //}

    private List<SchedularTransferComponentDTO> SetupScheduleList()
    {
        List<SchedularTransferComponentDTO> schedularTransferComponentDTOList = new List<SchedularTransferComponentDTO>();

        SchedularTransferComponentDTO schedularTransferComponentDTO = null;

        #region First Section
        schedularTransferComponentDTO = new SchedularTransferComponentDTO();
        schedularTransferComponentDTO.WeekDay = 1;
        if (!string.IsNullOrWhiteSpace(HiddenFieldDue1.Value))
        {
            schedularTransferComponentDTO.SelectedDate = DateTime.Parse(HiddenFieldDue1.Value);
        }
        if (!string.IsNullOrWhiteSpace(HiddenFieldTime1.Value))
        {
            schedularTransferComponentDTO.SelectedTime = TimeSpan.Parse(HiddenFieldTime1.Value);
        }
        if (!string.IsNullOrWhiteSpace(HiddenFieldCheck.Value))
        {
            if (HiddenFieldCheck.Value.Contains("1"))
                schedularTransferComponentDTO.CreateActivity = true;
        }

        schedularTransferComponentDTOList.Add(schedularTransferComponentDTO);
        #endregion

        #region Second Section
        schedularTransferComponentDTO = new SchedularTransferComponentDTO();
        schedularTransferComponentDTO.WeekDay = 2;
        if (!string.IsNullOrWhiteSpace(HiddenFieldDue2.Value))
        {
            schedularTransferComponentDTO.SelectedDate = DateTime.Parse(HiddenFieldDue2.Value);
        }
        if (!string.IsNullOrWhiteSpace(HiddenFieldTime2.Value))
        {
            schedularTransferComponentDTO.SelectedTime = TimeSpan.Parse(HiddenFieldTime2.Value);
        }
        if (!string.IsNullOrWhiteSpace(HiddenFieldCheck.Value))
        {
            if (HiddenFieldCheck.Value.Contains("2"))
                schedularTransferComponentDTO.CreateActivity = true;
        }
        schedularTransferComponentDTOList.Add(schedularTransferComponentDTO);
        #endregion

        #region Third Section
        schedularTransferComponentDTO = new SchedularTransferComponentDTO();
        schedularTransferComponentDTO.WeekDay = 3;
        if (!string.IsNullOrWhiteSpace(HiddenFieldDue3.Value))
        {
            schedularTransferComponentDTO.SelectedDate = DateTime.Parse(HiddenFieldDue3.Value);
        }
        if (!string.IsNullOrWhiteSpace(HiddenFieldTime3.Value))
        {
            schedularTransferComponentDTO.SelectedTime = TimeSpan.Parse(HiddenFieldTime3.Value);
        }
        if (!string.IsNullOrWhiteSpace(HiddenFieldCheck.Value))
        {
            if (HiddenFieldCheck.Value.Contains("3"))
                schedularTransferComponentDTO.CreateActivity = true;
        }
        schedularTransferComponentDTOList.Add(schedularTransferComponentDTO);
        #endregion

        #region Fourth Section
        schedularTransferComponentDTO = new SchedularTransferComponentDTO();
        schedularTransferComponentDTO.WeekDay = 4;
        if (!string.IsNullOrWhiteSpace(HiddenFieldDue4.Value))
        {
            schedularTransferComponentDTO.SelectedDate = DateTime.Parse(HiddenFieldDue4.Value);
        }
        if (!string.IsNullOrWhiteSpace(HiddenFieldTime4.Value))
        {
            schedularTransferComponentDTO.SelectedTime = TimeSpan.Parse(HiddenFieldTime4.Value);
        }
        if (!string.IsNullOrWhiteSpace(HiddenFieldCheck.Value))
        {
            if (HiddenFieldCheck.Value.Contains("4"))
                schedularTransferComponentDTO.CreateActivity = true;
        }
        schedularTransferComponentDTOList.Add(schedularTransferComponentDTO);
        #endregion

        #region Fifth Section
        schedularTransferComponentDTO = new SchedularTransferComponentDTO();
        schedularTransferComponentDTO.WeekDay = 5;
        if (!string.IsNullOrWhiteSpace(HiddenFieldDue5.Value))
        {
            schedularTransferComponentDTO.SelectedDate = DateTime.Parse(HiddenFieldDue5.Value);
        }
        if (!string.IsNullOrWhiteSpace(HiddenFieldTime5.Value))
        {
            schedularTransferComponentDTO.SelectedTime = TimeSpan.Parse(HiddenFieldTime5.Value);
        }
        if (!string.IsNullOrWhiteSpace(HiddenFieldCheck.Value))
        {
            if (HiddenFieldCheck.Value.Contains("5"))
                schedularTransferComponentDTO.CreateActivity = true;
        }
        schedularTransferComponentDTOList.Add(schedularTransferComponentDTO);
        #endregion

        Session[CommonUtility.SCHEDULETIME_LOG] = schedularTransferComponentDTOList;
        return schedularTransferComponentDTOList;
    }

    protected void buttonNext_Click(object sender)
    {
        Response.Redirect(ConfigUtil.ApplicationPath + "activity_planner/transaction/activity_scheduler.aspx");
    }

    protected void buttonPrevious_Click(object sender)
    {
        Response.Redirect(ConfigUtil.ApplicationPath + "call_cycle/transaction/route_seqence.aspx");
    }

    private void LoadScheduleForTME()
    {
        CallCycleClient callCycleClient = new CallCycleClient();
        CommonUtility commonUtility = new CommonUtility();
        ArgsDTO argsDTO = new ArgsDTO();
        argsDTO.Originator = UserSession.Instance.UserName;
        argsDTO.OriginatorType = UserSession.Instance.Originator;

        argsDTO.ChildOriginators = " created_by = '" + UserSession.Instance.UserName + "' "; UserSession.Instance.ChildOriginators.Replace("originator", "created_by");
        argsDTO.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        argsDTO.AdditionalParams = " ";
        argsDTO.OrderBy = "description ASC";
        argsDTO.StartIndex = 1;
        argsDTO.RowCount = 10;

        try
        {

            List<LookupDTO> lookupList = callCycleClient.GetAllCallCyclesList(argsDTO);

            ItineraryHeaderDTO itineraryHeaderDTO = new ItineraryHeaderDTO();
            //get Itinerary Header by User
            DateTime effDate = Convert.ToDateTime(txtStartDate.Text);
            itineraryHeaderDTO = callCycleClient.GetItineraryHeaderByUser(UserSession.Instance.UserName, effDate);
            if (!String.IsNullOrEmpty(itineraryHeaderDTO.Status))
            {
                if (itineraryHeaderDTO.Status.Trim().Equals("APRV"))
                {
                    buttonbar.EnableSave(false);
                    //Set Notification
                    div_message.Attributes.Add("style", "display:block;");
                    div_message.InnerHtml = commonUtility.GetInfoMessage("Your Itinerary For the Month "+effDate.ToString("MMM-yyyy")+" has been Approved. !");
                }
                else
                {
                    buttonbar.EnableSave(true);
                }

            }

            //ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "OpenCallCycleForTME('" + lookupList[0].Code + "', '" + lookupList[0].Description + "');", true);
        }
        catch(Exception e){
            div_message.Attributes.Add("style", "display:block;");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Error Occured");
        }
    }
    


    #endregion
}