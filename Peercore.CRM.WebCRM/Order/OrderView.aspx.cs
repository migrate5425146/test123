﻿using CRMServiceReference;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web.UI.WebControls;

public partial class Order_OrderView : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
                ArgsDTO args = new ArgsDTO();
                args.OrderBy = "name ASC";
                args.AdditionalParams = " dept_string = 'ASE' ";
                args.StartIndex = 1;
                args.RowCount = 500;

                OriginatorClient originatorClient = new OriginatorClient();
                List<OriginatorDTO> aseList = new List<OriginatorDTO>();
                aseList = originatorClient.GetOriginatorsByCatergory(args);

                if (aseList.Count > 0)
                    aseList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

                SetAses(aseList, "UserName");

                LoadReps();

                DateTime today = DateTime.Today;
                txtStartDate.Text = new DateTime(today.Year, today.Month, 1).ToString("dd-MMM-yyyy");
                txtEndDate.Text = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month)).ToString("dd-MMM-yyyy");
            }

            Master.SetBreadCrumb("Pre-Sales", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        OriginatorString.Value = UserSession.Instance.UserName;
        OriginatorTypeString.Value = UserSession.Instance.OriginatorString;
    }

    public void SetAses(List<OriginatorDTO> listOriginator, string valueField = "UserName")
    {
        if (UserSession.Instance.OriginatorString == "ADMIN")
        {
            ddlASEList.DataSource = listOriginator;
            ddlASEList.DataTextField = "Name";
            ddlASEList.DataValueField = valueField;
            ddlASEList.DataBind();
        }
        else if (UserSession.Instance.OriginatorString == "SLC")
        {
            ddlASEList.DataSource = listOriginator;
            ddlASEList.DataTextField = "Name";
            ddlASEList.DataValueField = valueField;
            ddlASEList.DataBind();
        }
        else if (UserSession.Instance.OriginatorString == "ASE")
        {
            ddlASEList.Items.Clear();
            //span_cmbase.Visible = false;
            ddlASEList.Items.Add(new ListItem { Text = UserSession.Instance.Originator, Value = UserSession.Instance.UserName });
            ddlASEList.Enabled = false;
        }
    }

    private void LoadReps()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";

        string strASM = GetASEString();

        if (strASM.Equals(""))
            args.AdditionalParams = " originator.dept_string = 'DR' ";
        else
            args.AdditionalParams = " originator.dept_string = 'DR'  AND crm_mast_asm.originator = '" + strASM + "' ";

        args.StartIndex = 1;
        args.RowCount = 500;
        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> repList = new List<OriginatorDTO>();
        repList = originatorClient.GetAllSRByOriginator(args);
        if (repList.Count > 0)
            repList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

        SetReps(repList, "UserName");
    }
    private string GetASEString()
    {
        try
        {
            string reportfor = ddlASEList.SelectedValue;
            if (reportfor == "ALL")
                reportfor = "";
            return reportfor;
        }
        catch (Exception ex)
        {
            return "";
        }
    }
    public void SetReps(List<OriginatorDTO> listOriginator, string valueField = "UserName")
    {
        if (UserSession.Instance.OriginatorString == "ADMIN")
        {
            ddlRepList.DataSource = listOriginator;
            ddlRepList.DataTextField = "Name";
            ddlRepList.DataValueField = valueField;
            ddlRepList.DataBind();
        }
        else if (UserSession.Instance.OriginatorString == "SLC")
        {
            ddlRepList.DataSource = listOriginator;
            ddlRepList.DataTextField = "Name";
            ddlRepList.DataValueField = valueField;
            ddlRepList.DataBind();
        }
        else if (UserSession.Instance.OriginatorString == "ASE")
        {
            ddlRepList.DataSource = listOriginator;
            ddlRepList.DataTextField = "Name";
            ddlRepList.DataValueField = valueField;
            ddlRepList.DataBind();
        }
    }

    protected void ddlASEList_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadReps();
    }

    protected void fileuploadBulkDeleteOrder_Click(object sender, EventArgs e)
    {
        //string path = fileuploadBulkDeleteOutlet.PostedFile.FileName;
        string fileName = fileuploadBulkDeleteOrder.FileName;
        fileName = "Order-" + DateTime.Now.ToString("yyyyMMddHHmmss") + "-" + fileName;
        string errorLog = "";

        string folderPath = "~/uploads/bulkdelete/";
        if (!Directory.Exists(MapPath(folderPath)))
        {
            Directory.CreateDirectory(MapPath(folderPath));
        }

        try
        {
            string filePath = folderPath + fileName;
            string savePath = MapPath(filePath);
            fileuploadBulkDeleteOrder.SaveAs(savePath);

            if (File.Exists(savePath))
            {
                string[] readText = File.ReadAllLines(savePath);
                StringBuilder strbuild = new StringBuilder();

                OrderBulkDeleteErrorLog(fileName, "Error log: Order Bulk Delete");
                OrderBulkDeleteErrorLog(fileName, "==============================");

                foreach (string s in readText)
                {
                    try
                    {
                        if (!DeleteOrderByOrderNo(s))
                        {
                            OrderBulkDeleteErrorLog(fileName, s + " - Error");
                        }
                    }
                    catch (Exception ex) { OrderBulkDeleteErrorLog(fileName, s + " - " + ex.Message); }
                }

                errorLog = OrderBulkDeleteErrorLog(fileName, "=============End=============");

                try
                {
                    CommonClient cClient = new CommonClient();
                    cClient.CreateTransactionLog(UserSession.Instance.OriginalUserName,
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        TransactionTypeModules.Delete,
                        TransactionModules.Order,
                        UserSession.Instance.OriginalUserName + " bulk delete order list " + fileName);
                }
                catch { }
            }
        }
        catch (Exception ex)
        {
            errorLog = OrderBulkDeleteErrorLog(fileName, ex.Message);
        }

        linkBulkDeleteErrorLog.NavigateUrl = errorLog;
        linkBulkDeleteErrorLog.Visible = true;
    }

    private bool DeleteOrderByOrderNo(string orderNo)
    {
        try
        {
            OrderClient orderService = new OrderClient();

            bool status = orderService.DeleteOrderHeaderByOrderNo(UserSession.Instance.UserName, orderNo);

            return status;
        }
        catch (Exception)
        {
            return false;
        }
    }

    private string OrderBulkDeleteErrorLog(string fileName, string msg)
    {
        string folderPath = "~/docs/errorlogs/";
        if (!Directory.Exists(MapPath(folderPath)))
        {
            Directory.CreateDirectory(MapPath(folderPath));
        }

        string filePath = folderPath + fileName;
        string savePath = MapPath(filePath);

        File.AppendAllText(savePath,
                   msg + Environment.NewLine);

        return filePath;
    }
}