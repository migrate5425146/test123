﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

//using Peercore.CRM.BusinessRules;
using Peercore.CRM.Entities;
using CRMServiceReference;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;

public partial class _Default : PageBase
{
    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Home", "", "");
            if (!IsPostBack)
            {
                if (UserSession.Instance.OriginatorString == "DIST")
                {
                    barchart.Visible = false;
                    attendanceChart.Visible = false;
                    attendanceDetails.Visible = false;

                    Response.Redirect(ConfigUtil.ApplicationPath + "dashboard/transaction/TradeLoadSummary.aspx"); 
                }
                else
                {
                    //LoadOptions();
                    //LoadMasterDetails();
                    Loadcmbdata();
                    Load_Products();
                }
            }

            hfOriginatorString.Value = UserSession.Instance.OriginalUserName;
            hfOriginatorTypeString.Value = UserSession.Instance.OriginatorString;
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        
    }
    #endregion

    #region Methods
    private void LoadMasterDetails()
    {
        OriginatorClient originatorClient = new OriginatorClient();
        //  HiddenFieldRepCode.Value = originatorClient.GetRepCode(UserSession.Instance.Originator);

        // if (dropdowntopcustomer.SelectedIndex != 0)
        // {
        //     HiddenFieldRepCode.Value = dropdowntopcustomer.SelectedValue;
        // }
        // else
        // {
        HiddenFieldRepCode.Value = "ALL";
        //}

    }

    private void LoadOptions()
    {
        CommonClient commonClient = new CommonClient();
        OriginatorClient originatorClient = new OriginatorClient();

        try
        {
            if (CurrentUserSettings == null)
                CurrentUserSettings = new ArgsDTO();
            CurrentUserSettings.ChildOriginators = UserSession.Instance.ChildOriginators;
            CurrentUserSettings.Originator = UserSession.Instance.UserName;
            List<LookupTableDTO> lookupTable = commonClient.GetSalesMarkets();

            LookupTableDTO lookup = new LookupTableDTO();
            lookup.TableCode = "ALL";
            lookup.TableDescription = "ALL";
            lookupTable.Insert(0, lookup);

            //DropDownListMarket.DataSource = lookupTable;
            //DropDownListMarket.DataTextField = "TableDescription";
            //DropDownListMarket.DataValueField = "TableCode";
            //DropDownListMarket.DataBind();
            //DropDownListMarket.SelectedIndex = 0;

            List<OriginatorDTO> replist = originatorClient.GetRepForKeyAccounts(UserSession.Instance.UserName);
            if (replist != null)
            {
                if (replist.Count != 0)
                {
                    replist.Insert(0, new OriginatorDTO() { Name = "National", RepCode = "ALL" });
                }
            }

            //dropdowntopcustomer.Items.Clear();
            //foreach (OriginatorDTO item in replist)
            //{
            //    dropdowntopcustomer.Items.Add(new ListItem(item.Name.ToString(), item.RepCode.Trim()));
            //}
        }
        catch (Exception oException)
        {
        }
    }

    public void Loadcmbdata()
    {
        if (UserSession.Instance.OriginatorString == "REPORT")
        {
            Load_ASEs();
            // set graph title 
            GraphTitle.Value = "Last 12 Months sales";
            //set graph X title
            GraphDuration.Value = "Months";
        }
        else if (UserSession.Instance.OriginatorString == "SLC")
        {
            Load_ASEs();
            // set graph title 
            GraphTitle.Value = "Last 12 Months sales";
            //set graph X title
            GraphDuration.Value = "Months";
        }
        else if (UserSession.Instance.OriginatorString == "ADMIN")
        {
            Load_ASEs();
            // set graph title 
            GraphTitle.Value = "Last 12 Months sales";
            //set graph X title
            GraphDuration.Value = "Months";
        }
        else if (UserSession.Instance.OriginatorString == "MANAGER")
        {
            Load_ASEs();
            // set graph title 
            GraphTitle.Value = "Last 12 Months sales";
            //set graph X title
            GraphDuration.Value = "Months";
        }
        else if (UserSession.Instance.OriginatorString == "ASE")
        {
            cmbase.Items.Clear();
            //span_cmbase.Visible = false;
            cmbase.Items.Add(new ListItem { Text = UserSession.Instance.Originator, Value = UserSession.Instance.UserName });
            cmbase.Enabled = false;

            ddlASMforAttendance.Items.Clear();
            //span_cmbase.Visible = false;
            ddlASMforAttendance.Items.Add(new ListItem { Text = UserSession.Instance.Originator, Value = UserSession.Instance.UserName });
            ddlASMforAttendance.Enabled = false;

            // set graph title 
            GraphTitle.Value = "Last 12 Months sales For " + UserSession.Instance.Originator + "";
            //set graph X title
            GraphDuration.Value = "Month";
        }
    }

    private void Load_ASEs()
    {
        ArgsDTO args = new ArgsDTO();
        args.OrderBy = "name ASC";
        args.AdditionalParams = " dept_string = 'ASE' ";
        args.StartIndex = 1;
        args.RowCount = 500;

        OriginatorClient originatorClient = new OriginatorClient();
        List<OriginatorDTO> aseList = new List<OriginatorDTO>();
        aseList = originatorClient.GetOriginatorsByCatergory(args);

        if (aseList.Count > 0)
            aseList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = null });
        else
            aseList.Insert(0, new OriginatorDTO() { Name = "---No ASM---", UserName = "NO" });

        SetAses(aseList, "UserName");
    }

    private void Load_Products()
    {
        CommonClient commonClient = new CommonClient();
        List<ProductDTO> productList = new List<ProductDTO>();

        ArgsDTO args = new ArgsDTO();
        args.StartIndex = 0;
        args.RowCount = 200;
        args.ROriginator = UserSession.Instance.OriginatorString;
        args.Originator = UserSession.Instance.OriginalUserName;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.AdditionalParams = "";
        args.StartIndex = 1;
        args.OrderBy = " name asc";

        productList = commonClient.GetAllProductsDataAndCountWithOutImage(args);

        if (productList.Count > 0)
            productList.Insert(0, new ProductDTO() { Name = "ALL", ProductId = -1 });
        else
            productList.Insert(0, new ProductDTO() { Name = "--No Products--", ProductId = -1 });

        SetProducts(productList, "ProductId");
    }

    public void SetAses(List<OriginatorDTO> listOriginator, string valueField = "UserName")
    {
        cmbase.DataSource = listOriginator;
        cmbase.DataTextField = "Name";
        cmbase.DataValueField = valueField;
        cmbase.DataBind();

        ddlASMforAttendance.DataSource = listOriginator;
        ddlASMforAttendance.DataTextField = "Name";
        ddlASMforAttendance.DataValueField = valueField;
        ddlASMforAttendance.DataBind();
    }

    public void SetProducts(List<ProductDTO> listProduct, string valueField = "ProductId")
    {
        ddlProduct.DataSource = listProduct;
        ddlProduct.DataTextField = "Name";
        ddlProduct.DataValueField = valueField;
        ddlProduct.DataBind();
    }

    #endregion
}
