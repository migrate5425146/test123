﻿using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class lead_customer_master_CustomerCategory : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
            }

            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Customer Category ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        hfOriginatorString.Value = UserSession.Instance.OriginatorString;
        hfOriginatorType.Value = UserSession.Instance.UserName;
    }
}