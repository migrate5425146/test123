﻿<%@ Page Title="mSales - Outlets" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="lead_contacts.aspx.cs" Inherits="lead_customer_transaction_lead_contacts" %>

<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="CustomermodalWindow" style="display: none">
        <div id="div_CustomerConfirm_Message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>
    <asp:HiddenField ID="HiddenFieldLeadType" runat="server" />
    <asp:HiddenField ID="HiddenFieldIsChecked" runat="server" />
    <asp:HiddenField ID="HiddenFieldActiveInactiveChecked" runat="server" />
    <asp:HiddenField ID="HiddenFieldReqSentIsChecked" runat="server" />
    <asp:HiddenField ID="HiddenFieldActiveReqSent" runat="server" />
    <asp:HiddenField ID="HiddenFieldRepType" runat="server" />
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="HiddenFieldCallCycleCount" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server" />

    <div runat="server" id="div_message" style="display: none">
    </div>
    <div class="clearall">
    </div>
    <div runat="server" id="header" class="homemsgboxtopic" style="display: none;">
        CONTACTS
    </div>
    <div class="clear">
    </div>
    <div class="toolbar" id="toolbar" runat="server" style="overflow: hidden;">

        <a id="href_Schedule" class="sprite_button" style="display: none;"><span class="loop icon"></span>Schedule</a>

        <a id="href_Refresh" class="sprite_button"><span class="loop icon"></span>Refresh</a>

        <a id="href_map" class="sprite_button"><span class="pin icon"></span>Map</a>

        <div id="Select_Idle_cust" style="margin-left: 11px; display: none; float: left; margin-top: 11px;">
            <asp:DropDownList ID="DDL_select_event" onchange="IdleCustomer_cmb_onChange()" ClientIDMode="Static"
                runat="server">
                <asp:ListItem Value="">-- please select event--</asp:ListItem>
                <asp:ListItem Value="NotInvoiced">Not Invoiced customers</asp:ListItem>
                <asp:ListItem Value="Idle">Idle customers</asp:ListItem>
            </asp:DropDownList>
        </div>

        <div class="datepicker_cust" id="FromDate" style="display: none; float: left; margin-top: 12px; margin-left: 6px; width: auto;">
            <%--<asp:Label ID="lbStartDate" runat="server" Text="Start Date" Visible="true"></asp:Label>--%>
            <asp:TextBox ID="dtpFromDate" runat="server" Style="width: 200px; height: 23px;" ClientIDMode="Static" placeholder="--select date--"></asp:TextBox>
        </div>

        <select id="cmbSaveLoadFilter" onchange="LeadCustomer_cmbSaveLoadFilter_onChange()"
            style="display: none;">
            <option value="SLF" selected="selected">SaveLoad/Filter</option>
            <option value="S">Save</option>
            <option value="S">Save As</option>
            <option value="S">Load</option>
            <option value="S">Load From</option>
            <option value="">All</option>
        </select>

        <%--<a class="sprite_button"><span class="save icon"></span>Save/LoadFilter</a>--%>
        <%--<a id="href_CopyRow" class="sprite_button"><span class="copy icon"></span>Copy Rows</a>--%>
        <a id="href_Organisation" class="sprite_button"><span class="oranisation icon"></span>
            mSales</a>
        <a id="href_LoadInactiveCustomers" class="sprite_button"><span class="cross icon"></span>Inactive Outlets</a>
        <a id="href_LoadReqSent" class="sprite_button" style="display: none;"><span
            class="sendrequest icon"></span>Req.Sent</a>
        <%--<a id="href_PermenantDelete" class="sprite_button"><span class="cross icon"> </span>Delete</a>--%>
        <asp:DropDownList ID="cmbRepType" onchange="LeadCustomer_cmbRepType_onChange()" ClientIDMode="Static"
            runat="server" Style="display: none;">
            <asp:ListItem Value="B">Bakery</asp:ListItem>
            <asp:ListItem Value="F">Food Services</asp:ListItem>
            <asp:ListItem Value="">All</asp:ListItem>
        </asp:DropDownList>
        <span id="div_row_count" class="rowcounttext" style="display: none;"></span>
        <a id="id_delete_customers" runat="server" class="sprite_button" style="display: inline-block; margin-top: 11px; margin-left: 6px;"><span class="cross icon"></span>Delete</a>
        <a id="id_bulk_inactive" runat="server" class="sprite_button cd-popup-trigger" style="display: inline-block; margin-top: 11px; margin-left: 6px;"><span class="cross icon"></span>Bulk Delete</a>

        <a id="btnBack" class="sprite_button" style="float: right; margin-right: 10px; display: none"><span class="leftarrow icon"></span>Back</a>
        <a id="btnAdvancedOptions" class="sprite_button" style="float: right; margin-right: 10px;"><span class="loop icon"></span>Advanced Filter</a>

    </div>
    <div id="div_upload" runat="server" style="background-color: #FFFFFF; padding-left: 10px; padding-top: 10px; overflow: hidden; display: none;">
        <asp:FileUpload ID="xmlUpload" runat="server" Style="float: left; font-size: 11px; height: 22px; margin-left: 10px; margin-top: 3px;" />
        <asp:Button Text="Upload" runat="server" ID="excelUpload" OnClick="excelUpload_Click"
            CssClass="uploadbtn" Style="float: left;" />
        <asp:RegularExpressionValidator ID="FileUpLoadValidator" runat="server" ErrorMessage="Please Select Excel File."
            ValidationExpression="^.*\.xls[xm]?$" ControlToValidate="xmlUpload" ForeColor="Red"
            Style="float: left;"></asp:RegularExpressionValidator>
    </div>
    
    <div class="formleft" style="width: 95%">
        <asp:HyperLink ID="linkBulkDeleteErrorLog" runat="server" Visible="false" target="_blank"><p style="text-align:end;">Bulk delete error log..</p></asp:HyperLink>
    </div>

    <div class="clearall">
    </div>
    <div id="grid">
    </div>
    <ul id="testMenu" style="display: none">
        <li value="1"><a href="javascript:loadLeadCustomerMap();" class="maptoolbarbtn">Map</a>
        </li>
        <li value="2"><a href="javascript:loadLeadCustomerActivity();" class="maptoolbarbtn">Activity</a> </li>
        <li value="3"><a href="javascript:loadLeadCustomerOpportunity();" class="maptoolbarbtn">Opportunity</a> </li>
        <li value="4"><a href="javascript:loadLeadCustomerContactPerson();" class="maptoolbarbtn">Contact Person</a> </li>
    </ul>

    <div id="popup">
        <div>
            <b>Please enter admin password:</b>
        </div>
        <input style="width: 100%;" id="adminPass" type="password" />
        <button type="button" onclick="confirmAdmin()">
            Confirm</button>
        <button type="button" onclick="exitAdmin()">
            Cancel</button>
    </div>

    <div class="cd-popup" role="alert">
        <div class="cd-popup-container">
            <p>
                <textarea id="txtBulkDeleteCustList" rows="10" cols="50" placeholder="5000
5001
5003
5004
5005
5006" style="display: none;"></textarea>
                <asp:FileUpload ID="fileuploadBulkDeleteOutlet" accept=".txt" runat="server" />
                
            </p>
            <ul class="cd-buttons">
                <li>
                    <button ID="btnBulkDeleteOutlet" runat="server" onserverclick="fileuploadBulkDeleteOutlet_Click">Delete</button>
                </li>
                <li>
                    <button class="cd-popup-close">No</button></li>
            </ul>
        </div>
        <!-- cd-popup-container -->
    </div>
    <!-- cd-popup -->

    <!--Popup Script-->
    <script>
        jQuery(document).ready(function ($) {
            //open popup
            $('.cd-popup-trigger').on('click', function (event) {
                event.preventDefault();
                $('.cd-popup').addClass('is-visible');
            });

            //close popup
            $('.cd-popup').on('click', function (event) {
                if ($(event.target).is('.cd-popup-close') || $(event.target).is('.cd-popup')) {
                    event.preventDefault();
                    $(this).removeClass('is-visible');
                }
            });
            //close popup when clicking the esc keyboard button
            $(document).keyup(function (event) {
                if (event.which == '27') {
                    $('.cd-popup').removeClass('is-visible');
                }
            });
        });
    </script>
    <!--Popup Script-->

    <script type="text/javascript">

        hideStatusDiv("MainContent_div_message");

        $(document).ready(function () {
            $("#dtpFromDate").kendoDatePicker({
                // display month and year in the input
                format: "dd-MMM-yyyy",
                change: function (e) {
                    selectedDate = $("#dtpFromDate").data("kendoDatePicker").value();
                    //console.warn(selectedDate);
                    loadIdleCustomerGrid();
                }
            });

            var repType = $("#cmbRepType option:selected").val();
            var pageindex = $("#MainContent_hfPageIndex").val();

            $("#MainContent_HiddenFieldRepType").val(repType);

            var lead_stage = $("#MainContent_HiddenFieldLeadType").val();
            if (lead_stage.toLowerCase() == 'lead') {
                $('#cmbRepType').hide();
            }

            var OriginatorString = $("#<%= OriginatorString.ClientID %>").val();

            if (lead_stage.toLowerCase() == 'customer') {
                href_Organisation.innerHTML = "Add New Outlet";
                OutletsPage_loadLeadCustomerGrid($("#MainContent_HiddenFieldIsChecked").val(), $("#MainContent_HiddenFieldReqSentIsChecked").val(), $("#cmbRepType option:selected").val(), OriginatorString);
                $('#cmbRepType').hide();
                $('#href_LoadReqSent').hide();
            }
            else if (lead_stage.toLowerCase() != 'enduser') {
                OutletsPage_loadLeadCustomerGrid($("#MainContent_HiddenFieldIsChecked").val(), $("#MainContent_HiddenFieldReqSentIsChecked").val(), $("#cmbRepType option:selected").val(), OriginatorString);
            }
            else {
                href_Organisation.innerHTML = "End User";
                loadEndUserGrid($("#MainContent_HiddenFieldIsChecked").val(), $("#MainContent_HiddenFieldReqSentIsChecked").val(), $("#cmbRepType option:selected").val());
                $('#cmbRepType').hide();
                $('#href_LoadReqSent').hide();
            }

            pageLoadLeadCustomerGrid();
        });

        $("#btnAdvancedOptions").click(function () {
            $('#href_Refresh').hide();
            $('#href_map').hide();
            $('#href_LoadInactiveCustomers').hide();
            $('#href_Organisation').hide();
            $('#Select_Idle_cust').show();
            $('#btnAdvancedOptions').hide();
            $('#btnBack').show();
            $('#DDL_select_event').val('');
        });

        $("#btnBack").click(function () {
            $('#href_Refresh').show();
            $('#href_map').show();
            $('#href_LoadInactiveCustomers').show();
            $('#href_Organisation').show();
            $('#Select_Idle_cust').hide();
            $('#btnAdvancedOptions').show();
            $('#btnBack').hide();
            $('#FromDate').hide();

            OutletsPage_loadLeadCustomerGrid(
                $("#MainContent_HiddenFieldIsChecked").val(),
                $("#MainContent_HiddenFieldReqSentIsChecked").val(),
                $("#cmbRepType option:selected").val(),
                OriginatorString);
        });


        $("#btnBuldDelete").click(function () {
            var txtDeleteCustList = $("#txtBulkDeleteCustList").val();
            var val = txtDeleteCustList.split(/\s+/).join(',');
            //alert(val);
            DeleteBulkCustList(val);
        });

        //function addNewlines(str) {
        //    var line = "",
        //        results = []
        //    str.split(/\s+/).forEach(function (word) {
        //        if (line.length + word.length < 20) {
        //            line = (line + " " + word).trim()
        //        } else {
        //            results.push(line)
        //            line = word
        //        }
        //    })
        //    results.push(line)
        //    return results.join("\n")
        //}

        function DeleteBulkCustList(val) {
            var confirmResult = confirm("Do you want to delete all these customers? ");
            if (confirmResult == true) {

                var url = ROOT_PATH + "lead_customer/process_forms/processmaster.aspx?fm=deletebulkcust&type=Delete" + "&val=" + val;

                $.ajax({
                    url: url,
                    dataType: "html",
                    cache: false,
                    success: function (msg) {
                        if (msg == "1") {
                            $("#MainContent_div_message").show();
                            $("#MainContent_div_message").html("Successfully Deleted.");
                            var grid = $("#LeadDocumentGrid").data("kendoGrid");
                            grid.dataSource.read();
                        }
                    },
                    error: function (msg, textStatus) {
                        if (textStatus == "error") {
                            var msg = msg; // "Sorry there was an error: ";
                            $("#MainContent_div_message").show();
                            $("#MainContent_div_message").html(msg);
                        }
                    }
                });
            }
        }

        function IdleCustomer_cmb_onChange() {
            var IdleCustType = $("#DDL_select_event option:selected").val();

            if (IdleCustType == 'NotInvoiced') {
                $('#FromDate').hide();
                loadNotInvoicedCustomerGrid();
            }
            else if (IdleCustType == 'Idle') {
                $('#FromDate').show();
                loadIdleCustomerGrid();
            }
            else {
                $('#FromDate').hide();
            }
        }

        $("#id_delete_customers").click(function () {
            var callCycleContactDTOList = GetSelectedCustomerList();
            if (callCycleContactDTOList != "") {
                var r = confirm("Do you want to delete this customer/s?");
                if (r == true) {
                    showPopup();
                } else {
                }
            }
            else {
                var errorMsg = GetErrorMessageDiv("Please select an item to delete", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);
                hideStatusDiv("MainContent_div_message");
            }
        });

        function done() {
            document.getElementById("popup").style.display = "none";
            var password = document.getElementById("pass").value;

            if (password == 'P$k8%T3w') {
                deletecustomers();
            }

            //DO STUFF WITH PASSWORD HERE    
        };

        function exit() {
            return false;
        }

        function showPopup() {
            $("#adminPass").val("");
            var OriginatorString = $("#<%= OriginatorString.ClientID %>").val();
            if (OriginatorString != "ADMIN") {
                document.getElementById("popup").style.display = "block";
            }
            else {
                deletecustomers();
            }
        }

        function confirmAdmin() {

            var password = document.getElementById("adminPass").value;

            if (password == '') {
                var errorMsg = GetErrorMessageDiv("Please enter admin password", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);
                hideStatusDiv("MainContent_div_message");
                return;
            }

            var param = { "password": password };
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/GetAdminPassword",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.hasOwnProperty('d')) {
                        msg = response.d;
                        if (msg == 'true') {
                            deletecustomers();
                            document.getElementById("popup").style.display = "none";
                        }
                        else {
                            var errorMsg = GetErrorMessageDiv("Invalid admin password", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(errorMsg);
                            hideStatusDiv("MainContent_div_message");
                            $("#adminPass").val("");
                        }
                    } else {
                        msg = response;

                        var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        hideStatusDiv("MainContent_div_message");
                        $("#adminPass").val("");
                    }

                },
                error: function (response) {
                    //alert("Oops, something went horribly wrong");                    
                }
            });

        }

        function exitAdmin() {
            document.getElementById("popup").style.display = "none";
            return false;
        }

        function visibleUpload() {
            $('#MainContent_div_upload').show();
        }

        function divHide() {
            $('#MainContent_div_upload').hide();
        }

        $("#href_map").click(function () {
            loadLeadCustomerMap();
        });

        $("#href_Refresh").click(function () {
            refreshLeadCustomerGrid();
        });

        $("#href_CopyRow").click(function () {
            LeadCustomerGridCopyRow();
        });

        $("#href_LoadReqSent").click(function () {
            LoadReqSentLeadCustomerGrid();
        });

        $("#href_LoadInactiveCustomers").click(function () {
            var OriginatorString = $("#<%= OriginatorString.ClientID %>").val();
            LoadInactiveCustomers(OriginatorString);
        });

        $("#href_Organisation").click(function () {
            LeadCustomerGridOrganisation();
        });

        $("#href_Save").click(function () {
            HeaderMenueLeadCustomerGrid('server');
        });

        $("#href_SaveAs").click(function () {
            HeaderMenueLeadCustomerGrid('client')
        });

        $("#href_Load").click(function () {
            LoadDefaultSettingLeadCustomerGrid();
        });

        $("#href_LoadFrom").click(function () {
            visibleUpload();
        });

        $("#href_ClearAllFilter").click(function () {
            LeadCustomerClearSetting();
        });

        $("#href_Schedule").click(function () {
            var Count = $("#MainContent_HiddenFieldCallCycleCount").val();
            if (Count == "1")
                window.location.href = "../../call_cycle/transaction/schedule_entry.aspx";
            else
                alert("Select Organisations..");
        });

        // This code goes in your page
        //    function ctxCallback(item, trigger) {
        //        var trigVal = trigger.attr('id');
        //        var rowId = trigger.parent().attr('id'); // get the row of the parent since I used the TD as my trigger.

        //        if (typeof trigVal == 'undefined') trigVal = trigger.html();

        //        if (typeof rowId == 'undefined') rowId = '';
        //        else rowId = ' on row ' + rowId;

        //        alert('Trigger ' + trigVal + rowId + ' clicked: ' + item.text().trim());
        //    }

    </script>
</asp:Content>
