﻿<%@ Page Title="mSales - Upload Sales" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="sales_excel_upload.aspx.cs" Inherits="lead_customer_transaction_sales_excel_upload" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ Register Src="~/usercontrols/entrypage_tabs.ascx" TagPrefix="ucl2" TagName="tabentry" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <div id="customermodalWindow" style="display: none">
        <div id="div_customerconfirm_message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes
        </button>
        <button id="no" class="k-button">
            No
        </button>
    </div>
    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <ucl:buttonbar ID="buttonbar" runat="server" />
                </div>
            </div>
            <div class="toolbar_right" id="div1">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 35%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/lead_customer/transaction/lead_contacts.aspx?cht=lead&ty=Customer">  
                            <div class="back">
                            </div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none">
        </div>
        <div id="div_info" runat="server" style="display: none">
        </div>
        <div id="div_promt" style="display: none">
        </div>
        <div>
            <div class="formleft" style="width: 100%">
                <div class="formtextdiv" style="color:black">
                    Select Excel File</div>
                <div class="formdetaildiv_right" style="line-height: 26px;">
                    :
                    <div id="div_upload" runat="server" style="background-color: #FFFFFF; float: right;
                        overflow: hidden;">
                        <asp:FileUpload ID="xmlUpload" runat="server" Style="float: left; font-size: 11px;
                            height: 22px; margin-left: 10px; margin-top: 3px;" />
                        <asp:Button Text="Upload" runat="server" ID="excelUpload" Style="float: left;" OnClick="excelUpload_Click" />
                        <asp:RegularExpressionValidator ID="FileUpLoadValidator" runat="server" ErrorMessage="Please Select Excel File."
                            ValidationExpression="^.*\.xls[xm]?$" ControlToValidate="xmlUpload" ForeColor="Red"
                            Style="float: left;"></asp:RegularExpressionValidator>
                    </div>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                </div>
                <div class="formdetaildiv_right">
                    <a href="../../docs/templates/Reps_Target.xlsx">Sample Upload File</a>
                </div>
            </div>
            <div class="specialevents">
                <asp:Label ID="tbDuration" runat="server" Text=""></asp:Label></div>
            <div class="clearall">
            </div>
            <div class="specialevents">
                <asp:Label ID="tbWeeksSinceLastCalled" runat="server" Text=""></asp:Label></div>
            <div class="specialevents">
                <span id="tbCreated" runat="server"></span>
            </div>
            <div class="specialevents">
                <span id="tbModified" runat="server"></span>
            </div>
        </div>
    </div>
    <div class="divcontectmainforms" style="display: none;">
        <a name="tabs"></a>
        <ucl2:tabentry ID="tabentry1" runat="server" />
    </div>

    <asp:HiddenField ID="hfWebsite" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="hfPreferredMethod" runat="server"></asp:HiddenField>
    <script type="text/javascript">
        jQuery(function ($) {

        });
    </script>
</asp:Content>
