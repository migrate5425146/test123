﻿<%@ Page Title="mSales | Outlet Bulk Transfer" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="customer_bulk_transfer.aspx.cs" Inherits="lead_customer_transaction_customer_bulk_transfer" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
	<style>
		sprite_button {
			font-family: 'Poppins';
			height: 40px;
			width: auto;
			padding-left: 15px;
			padding-right: 15px;
			padding-top: 0;
			padding-bottom: 0;
			-webkit-box-shadow: 0 9px 16px 0 rgb(93 120 255 / 25%) !important;
			box-shadow: 0 9px 16px 0 rgb(93 120 255 / 25%) !important;
			color: #fff !important;
			background: #4f51d9 !important;
			border-color: #4f51d9 !important;
			font-size: 13px;
			line-height: 40px;
			border-radius: .25rem;
			display: inline-block;
			font-weight: 400;
			text-align: center;
			text-shadow: none !important;
			float: none;
			-webkit-transition: all .3s ease 0s;
			transition: all .3s ease 0s;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<asp:HiddenField ID="HiddenField_logURL" runat="server" />
    <asp:HiddenField ID="HiddenField_logURLStatus" runat="server" />

	<div id="customermodalWindow" style="display: none">
		<div id="div_customerconfirm_message">
		</div>
		<div class="clearall" style="margin-bottom: 15px">
		</div>
		<button id="yes" class="k-button">
			Yes
		</button>
		<button id="no" class="k-button">
			No
		</button>
	</div>
	<div class="divcontectmainforms" style="height: 400px;">
		<div id="div_message" runat="server" style="display: none">
		</div>
		<div id="div_info" runat="server" style="display: none">
		</div>
		<div id="div_promt" style="display: none">
		</div>
		<div>
			<div class="formleft" style="width: 100%">
				<div class="formtextdiv" style="color: black">
					Select New Route
				</div>
				<div class="formdetaildiv_right" style="line-height: 26px;">
					:
                    <span class="wosub mand">&nbsp;
                        <asp:DropDownList ID="ddlNewRoutes" runat="server" class="input-large1" Width="262px">
						</asp:DropDownList></span>
				</div>
				<div class="clearall">
				</div>
				<div class="formtextdiv" style="color: black">
					Select Excel File
				</div>
				<div class="formdetaildiv_right" style="line-height: 26px;">
					:
                    <div id="div_upload" runat="server" style="background-color: #FFFFFF; float: right; overflow: hidden;">
						<asp:FileUpload ID="xmlUpload" runat="server" Style="float: left; font-size: 11px; height: 22px; margin-left: 10px; margin-top: 3px;" />
						<asp:Button Text="Transfer" runat="server" ID="btnTransfer" CssClass="button " 
							Style="float: left; background-color: #4f51d9 !important; color: white; cursor: pointer; -webkit-box-shadow: 0 9px 16px 0 rgba(93,120,255,.25) !important; " 
							OnClick="btnTransfer_Click" />
						<asp:RegularExpressionValidator ID="FileUpLoadValidator" runat="server" ErrorMessage="Please Select Excel File."
							ValidationExpression="^.*\.xls[xm]?$" ControlToValidate="xmlUpload" ForeColor="Red"
							Style="float: left;"></asp:RegularExpressionValidator>
					</div>
				</div>
				<div class="clearall">
				</div>
				<div class="formtextdiv">
				</div>
				<div class="formdetaildiv_right">
					<a href="../../docs/templates/OutletBulkTransfer.xlsx">Sample Upload File</a>
				</div>
			</div>
			<div class="formleft" style="width: 50%">
				<asp:HyperLink ID="linkLog" runat="server"><p style="text-align:end;">Excel upload log..</p></asp:HyperLink>
			</div>
			<div class="clearall">
			</div>
		</div>
	</div>

	<script type="text/javascript">
		jQuery(function ($) {
			
		});


	</script>

</asp:Content>

