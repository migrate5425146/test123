﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="enduser_entry.aspx.cs" Inherits="lead_customer_transaction_enduser_entry" %>

    <%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>

<%@ Register Src="~/usercontrols/entrypage_tabs.ascx" TagPrefix="ucl2" TagName="tabentry" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">

    <script type="text/javascript" src="../../assets/scripts/jquery.maskedinput.js" ></script>
    <meta name="viewport" content="width=1190">
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnablePageMethods="true">
    </telerik:RadScriptManager>
    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <asp:HiddenField ID="HiddenFieldEndUserAddress1" runat="server" />
    <asp:HiddenField ID="HiddenFieldEndUserAddress2" runat="server" />
    <asp:HiddenField ID="HiddenFieldEndUserCity" runat="server" />
    <asp:HiddenField ID="HiddenFieldEndUserState" runat="server" />
    <asp:HiddenField ID="HiddenFieldEndUserPostcode" runat="server" />
    <asp:HiddenField ID="HiddenFieldEndUserContactPhone" runat="server" />
    <asp:HiddenField ID="HiddenFieldEndUserMobile" runat="server" />
    <asp:HiddenField ID="HiddenFieldEndUserFax" runat="server" />
    <asp:HiddenField ID="HiddenFieldEndUserWebSite" runat="server" />
    <asp:HiddenField ID="HiddenFieldEndUserEmail" runat="server" />
    <asp:HiddenField ID="HiddenFieldEndUserPrefMethod" runat="server" />
    <asp:HiddenField ID="HiddenEndUserCode" runat="server" />
    <asp:HiddenField ID="HiddenFieldCustomerCode" runat="server" />
    <asp:HiddenField ID="HiddenFieldCustomerName" runat="server" />
    <asp:HiddenField ID="HiddenFieldFormType" runat="server" />
    <asp:HiddenField ID="HiddenFieldExistClickOk" runat="server" />
    <asp:HiddenField ID="HiddenFieldOtherExistClickOk" runat="server" />
    <asp:HiddenField ID="HiddenFieldTabIndex" runat="server" Value="1"/>

    
    <div id="endusermodalWindow" style="display: none">
        <div id="div_enduserconfirm_message">
        </div>
        <div class="clearall" style="margin-bottom:15px"></div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>

    <div id="endusermodalWindow2" style="display: none">
        <div id="div_enduserok_message">
        </div>
        <div class="clearall" style="margin-bottom:15px"></div>
        <button id="ok" class="k-button">
            OK</button>

    </div>

    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <ucl:buttonbar ID="buttonbar" runat="server" />
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 35%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/lead_customer/transaction/lead_contacts.aspx?cht=lead&ty=End User">
                        <div class="back"></div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearall">
        </div>
        
        <div id="div_message" runat="server" style="display: none">
        </div>
        <div id="div_info" runat="server" style="display: none">
        </div>
        <div id="div_promt" style="display: none">
        </div>
        <div id="newcust_window">
                <div id="div_newcustgrid">
                </div>
         </div>
        <div>
            <%--layout new format--%>
            <div class="formleft">
                <div class="formtextdiv_extended">
                    Dist</div>
                <div class="formdetaildiv_right">
                    <span class="wosub mand">:
<%--                        <asp:TextBox ID="TextBoxDist" runat="server" ReadOnly="true" Width="175px" required
                            validationMessage="Please select customer"></asp:TextBox>--%>
                                                    <asp:TextBox ID="TextBoxDist" runat="server" ReadOnly="true" CssClass="input-large"></asp:TextBox>
                        <span style="color: Red">*</span> 
                        <a href="#" id="id_Dist" runat="server">
                            <img id="Img1" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a>
                        <a href="#" id="id_dist_disable" runat="server">
                            <img id="Img3" src="~/assets/images/popiconbw.png" alt="popicon" runat="server" border="0" /></a>
                    </span>
                </div>
                <div class="clearall">
                </div>
                
                <div class="formtextdiv_extended">
                    Code</div>
                <div class="formdetaildiv"><span class="wosub mand">
                    :
                    <asp:TextBox ID="txtCode" Style="text-transform: uppercase" runat="server" ReadOnly="true" onblur="getValidationMessage()" CssClass="input-large"></asp:TextBox><span style="color: Red">*</span></span>
                </div>
                <div class="clearall">
                </div>
               
                <div class="formtextdiv_extended">
                    Name</div>
                <div class="formdetaildiv"><span class="wosub mand">
                    :
                    <asp:TextBox ID="txtCustomerName" Style="text-transform: uppercase" runat="server" CssClass="input-large" MaxLength="40"></asp:TextBox><span style="color: Red">*</span></span></div>
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    Contact </div>
                <div class="formdetaildiv">
                    :
                    <asp:TextBox ID="txtContact" runat="server" CssClass="input-large" MaxLength="20"></asp:TextBox></div>
                <div class="clearall">
                </div>

                <div class="formtextdiv_extended">
                    Phone </div>
                <div class="formdetaildivlead">
                    :
                    <asp:TextBox ID="mtxtPhone" runat="server" CssClass="input-large-phone" MaxLength="14"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>

                <div class="formtextdiv_extended">
                    Email </div>
                <div class="formdetaildivlead"><span class="wosub mand">
                    :
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="input-large" MaxLength="120"></asp:TextBox></span>
                </div>
                <div class="clearall">
                </div>

                 <div class="formtextdiv_extended">
                    FS Rep </div>
                <div class="formdetaildivlead">
                    <div id="div_fsrep" runat="server"></div>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    Primary Distributor</div>
                <div class="formdetaildiv">
                    :
                    <asp:CheckBox ID="chkprimarychk" runat="server" />
                </div>

                 <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    </div>
                <div class="formdetaildiv">
                    <asp:RadioButton ID="radiobakery" Text="Bakery" runat="server" GroupName="Ba" />
                    <asp:RadioButton ID="radiofood" runat="server" Text="Food Services" GroupName="Ba" />
                </div>

                 <div class="clearall">
                </div>
            </div>

            <div class="formright">
                <div class="formtextdiv_extended">
                Address 1 </div>
            <div class="formdetaildivlead">
            :
                <asp:TextBox ID="txtAddress1" runat="server" CssClass="input-large" MaxLength="40"></asp:TextBox>
            </div>
            <div class="clearall">
            </div>
            <div class="formtextdiv_extended">
                Address 2 </div>
            <div class="formdetaildivlead">
            :
                <asp:TextBox ID="txtAddress2" runat="server" CssClass="input-large" MaxLength="40"></asp:TextBox>
            </div>
            <div class="clearall">
            </div>
                <div class="formtextdiv_extended">
                    City
                </div>
                <div class="formdetaildivlead">
                    :
                    <asp:TextBox ID="txtCustCity" runat="server" CssClass="input-large" MaxLength="20"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    State
                </div>
                <div class="formdetaildivlead">
                    :
                    <asp:TextBox ID="txtCustState" runat="server" CssClass="input-large" MaxLength="4"></asp:TextBox>
                </div>
                <div class="clearall">
                </div>
            <div class="formtextdiv_extended">
                Postcode </div>
            <div class="formdetaildivlead">
            :
                <asp:TextBox ID="txtCustPostcode" runat="server" CssClass="input-large" MaxLength="4"></asp:TextBox>
            </div>
            <div class="clearall">
            </div>
                <div class="formtextdiv_extended">
                    Assigned To</div>
                <div class="formdetaildiv">
                    :
                    <asp:TextBox ID="txtAssignedTo" runat="server" CssClass="input-large" ReadOnly="true" ></asp:TextBox>
                    <a href="#" id="a1_AssignedTo" runat="server">
                        <img id="Img2" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a>
                 </div>
                <div class="clearall">
                </div>

                <div class="names">
                  </div>
                <div class="formtextdiv_extended">
                    Rating</div>
                <div class="formdetaildiv">
                    <telerik:RadRating ID="rRating" runat="server" Width="100px" ItemCount="5" Value="0"
                        SelectionMode="Continuous" Precision="Half" Orientation="Horizontal" />
                </div>

                <div class="clearall">
                </div>
                <div class="formtextdiv_extended">
                    Bakery Rep</div>
                <div class="formdetaildiv">

                    <div id="div_BakeryRep" runat="server"></div>
                </div>

                <div class="clearall">
                </div>

            </div>

             <div class="clearall">
            </div>

            <div id="div_grid_enduser" style="margin:10px;" >
            </div>

            <div class="clearall">
            </div>
            <div class="specialevents">
                <asp:Label ID="tbDuration" runat="server" Text=""></asp:Label></div>
            <div class="clearall">
            </div>
            <div class="specialevents">
                <asp:Label ID="tbWeeksSinceLastCalled" runat="server" Text=""></asp:Label></div>
            <div class="specialevents">
                <span id="tbCreated" runat="server"></span>
            </div>
            <div class="specialevents">
                <span id="tbModified" runat="server"></span>
            </div>
        </div>
    </div>
    <div class="divcontectmainforms">
        <a name="tabs"></a>
        <ucl2:tabentry id="tabentry1" runat="server" />
    </div>
    <script type="text/javascript">

        function getValidationMessage() {
            document.getElementById("MainContent_HiddenEndUserCode").value = document.getElementById("MainContent_txtCode").value;

                var id = MainContent_HiddenEndUserCode.value;
            var custid = MainContent_HiddenFieldCustomerCode.value;
            var key = 0;
            //alert(MainContent_HiddenFieldCustomerCode.value);
            var url = ROOT_PATH + "lead_customer/process_forms/processmaster.aspx?fm=customerenduval&type=report" + "&id=" + id + "&custid="+custid+"&tdiv=endusermodalWindow";


           // $("#endusermodalWindow").html('loading.....');
            $.ajax({
                url: url,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg != '') {
                        key = 1;
                        $("#endusermodalWindow2").show();
                        showenduserexist(msg);
                    }
                    //  $("div#endusermodalWindow2").show();
                    //    $("#endusermodalWindow2").html(msg, id);
                },
                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg; // "Sorry there was an error: ";
                        $("#endusermodalWindow2").html(msg);
                    }
                }
            });


          if (key == 0) {
              var url = ROOT_PATH + "lead_customer/process_forms/processmaster.aspx?fm=othercustomerenduval&type=report" + "&id=" + id + "&custid=" + custid + "&tdiv=endusermodalWindow";
                // $("#endusermodalWindow").html('loading.....');
                $.ajax({
                    url: url,
                    dataType: "html",
                    cache: false,
                    success: function (msg) {
                        if (msg != '') {
                            $("#endusermodalWindow2").show();
                            showenduserexist(msg);
                        }
                        //  $("div#endusermodalWindow2").show();
                        //    $("#endusermodalWindow2").html(msg, id);
                    },
                    // error: function (XMLHttpRequest, textStatus, errorThrown) {
                    error: function (msg, textStatus) {
                        if (textStatus == "error") {
                            var msg = msg; // "Sorry there was an error: ";
                            $("#endusermodalWindow2").html(msg);
                        }
                    }
                });
        }
                  if (MainContent_HiddenFieldCustomerName.value != '')
                  $('#MainContent_TextBoxDist').val(MainContent_HiddenFieldCustomerName.value);
        }
        hideStatusDiv("MainContent_div_message");
        jQuery(function () {
                       if (MainContent_HiddenFieldCustomerName.value !='')
                          $('#MainContent_TextBoxDist').val(MainContent_HiddenFieldCustomerName.value);

            jQuery("#MainContent_TextBoxDist").validate({
                expression: "if (VAL) return true; else return false;",
                message: "Please select customer."
            });

            jQuery("#MainContent_txtCode").validate({
                expression: "if (VAL) return true; else return false;",
                message: "Please Enter Code.\n(TVSWNNQ)"
            });

            jQuery("#MainContent_txtCustomerName").validate({
                expression: "if (VAL) return true; else return false;",
                message: "Please Enter Name."
            });
        });

        jQuery(function ($) {
            $('input.input_numeric').autoNumeric({ aSep: null, aDec: null, mDec: 0 });
        });

        jQuery(function ($) {
            $.mask.definitions['~'] = '[+0123456789]';
            $('input.input-large-mobile').mask("~9?999999999999");
        });

        jQuery(function ($) {
            $('input.input-large-phone').mask("9?999999999");
        });

        jQuery(function ($) {
            $("#MainContent_TextBoxDist").kendoValidator().data("kendoValidator");
            $("#MainContent_txtCode").kendoValidator().data("kendoValidator");
            $("#MainContent_txtCustomerName").kendoValidator().data("kendoValidator");
            
            var items = [
                            ["Contact Person", "usercontrols/enduser_contactperson.aspx?custid=" + $("#MainContent_HiddenFieldCustomerCode").val() + ""],
                            ["Activity", "usercontrols/enduser_activity.aspx?custid=" + $("#MainContent_HiddenFieldCustomerCode").val() + ""],
                            ["Document", "usercontrols/enduser_document.aspx?custid=" + $("#MainContent_HiddenFieldCustomerCode").val() + ""],
                            ["Opportunities", "usercontrols/customer_oppotunity.aspx?custid=" + $("#MainContent_HiddenFieldCustomerCode").val() + "&eduid=" + $("#MainContent_HiddenEndUserCode").val() + ""],
                            ["Enduser Price", "usercontrols/enduser_price.aspx?custid=" + $("#MainContent_HiddenFieldCustomerCode").val() + "&eduid=" + $("#MainContent_HiddenEndUserCode").val() + ""],
                            ["Sales", "usercontrols/enduser_sales.aspx?custid=" + $("#MainContent_HiddenFieldCustomerCode").val() + "&eduid=" + $("#MainContent_HiddenEndUserCode").val() + ""]
                        ];
            loadEntryPageTabs("enduser", items, $("#MainContent_HiddenFieldTabIndex").val());

           loadendusercustomer('div_grid_enduser', $("#MainContent_HiddenEndUserCode").val(), $("#MainContent_HiddenFieldCustomerCode").val());
        });

        $(document).ready(function () {
            

            var newcust_window = $("#newcust_window"),
                        newcust_undo = $("#undo")
                                .bind("click", function () {
                                    newcust_window.data("kendoWindow").open();
                                    newcust_undo.hide();
                                });

            var onClose = function () {
                newcust_undo.show();
            }

            if (!newcust_window.data("kendoWindow")) {
                newcust_window.kendoWindow({
                    width: "500px",
                    height: "400px",
                    title: "Lookup",
                    close: onClose
                });
            }

            newcust_window.data("kendoWindow").close();

            $("#MainContent_id_Dist").click(function () {
                $("div#div_loader").show();
                loaddistributorLookup('div_newcustgrid', 'newcust_window','3');
            });

            $("#MainContent_a1_AssignedTo").click(function () {
                $("div#div_loader").show();
                loadcustomerLookup('div_newcustgrid', 'newcust_window', '3');
            });
        });

        $("#MainContent_buttonbar_buttinSave").click(function () {


          //  loadendusercustomer('div_grid_enduser', $("#MainContent_HiddenEndUserCode").val(), $("#MainContent_HiddenFieldCustomerCode").val());

            jQuery("#MainContent_txtCustomerName").validate({
                expression: "if (VAL){return true;} else{return false;}",
                message: "Please Enter Name."
            });

            if ($("#MainContent_txtEmail").val() != null && $("#MainContent_txtEmail").val().length > 0) {

                jQuery("#MainContent_txtEmail").validate({
                    expression: "if (isFill(SelfID))" +
                                " {" +
	                        "        if (VAL.match(/^[^\\W][a-zA-Z0-9\\_\\-\\.]+([a-zA-Z0-9\\_\\-\\.]+)*\\@[a-zA-Z0-9_]+(\\.[a-zA-Z0-9_]+)*\\.[a-zA-Z]{2,4}$/))" +
		                    "            return true; " +
	                        "        else " +
		                        "           return false; " +
                                "   } else {return true; }",
                    message: "Please enter valid Email id"
                });

            } else {
                jQuery("#MainContent_txtEmail").validate({
                    onsubmit: true
                });


            }

        });


        
    </script>
</asp:Content>
