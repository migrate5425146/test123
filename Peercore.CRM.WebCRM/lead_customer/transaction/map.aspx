﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="map.aspx.cs" Inherits="lead_customer_transaction_map" %>
<%@ MasterType TypeName="SiteMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
<meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
    <asp:HiddenField ID="hfAddress" runat="server" />
    <asp:HiddenField ID="hfCustCode" runat="server" />
    <asp:HiddenField ID="hfLeadId" runat="server" />
    <style type="text/css">
      html { height: 100% }
      body { height: 100%; margin: 0; padding: 0 }
      #map_canvas { height: 100% }
    </style>
    <script type="text/javascript"
      src="https://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false">
    </script>
    <script type="text/javascript" src="http://www.google.com/jsapi"></script>
    <script type="text/javascript">

        window.onload = InitializeMap;
        function InitializeMap() {
            var strAddress = document.getElementById("MainContent_hfAddress").value; 
            var geocoder = new google.maps.Geocoder();
            var map;

            geocoder.geocode({ 'address': strAddress },
        		function (results, status) {
        		    if (status == google.maps.GeocoderStatus.OK) {
        		        address = results[0].geometry.location;

        		        var myLatlng = results[0].geometry.location;
        		        var mapOptions = {
        		            zoom: 18,
        		            center: myLatlng,
        		            mapTypeId: google.maps.MapTypeId.SATELLITE
        		        }
        		        var map = new google.maps.Map(document.getElementById("map_canvas"), mapOptions);

        		        var marker = new google.maps.Marker({
        		            position: myLatlng,
        		            map: map,
        		            title: strAddress
        		        });

        		        var contentString = '<div>' + strAddress + '</div>';

        		        var infowindow = new google.maps.InfoWindow({
        		            content: contentString,
        		            size: new google.maps.Size(60, 15)
        		        });

        		        infowindow.open(map, marker);

        		        google.maps.event.addListener(marker, 'click', function () {
        		            infowindow.open(map, marker);
        		        });

        		    }
        		});
         

        }
    </script>

    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div>
                        
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/lead_customer/transaction/lead_contacts.aspx">
            <div class="back"></div></asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>

        <div class="clearall"></div>
        <div align="right">
            <div align="left">
                <div id="div_message" class="savemsg" runat="server" style="display:none" ></div>        
                <%--<div style="padding-left:10px"></div>--%>
            </div>
        </div>
        <div class="clearall">&nbsp;</div>  
        <div>
            <div id="map_canvas" class="map"></div>
        </div>
    </div>
</asp:Content>

