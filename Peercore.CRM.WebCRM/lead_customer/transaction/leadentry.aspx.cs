﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Ent = Peercore.CRM.Entities;
//using Biz = Peercore.CRM.BusinessRules;
using System.IO;
using System.Web.Services;
using Peercore.CRM.Entities;
//using Peercore.CRM.BusinessRules;
using Telerik.Web.UI;
using System.Web.UI.HtmlControls;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using System.Text;
using CRMServiceReference;
using System.Data;
using System.Collections;
using System.Configuration;

public partial class lead_customer_transaction_leadentry : PageBase
{
    #region Attributes
    // Ent.LeadEntity leadEntry = null;
    //usercontrols_customer_oppotunity oppotunity;
    //usercontrols_contactperson contactperson;
    //usercontrols_contactdetails contactdetails;
    CommonUtility commonUtility = new CommonUtility();
    #endregion

    #region Properties
    public string LeadId
    {
        get
        {
            if (ViewState[CommonUtility.LEAD_ID] != null && ViewState[CommonUtility.LEAD_ID] != string.Empty)
            {
                return ViewState[CommonUtility.LEAD_ID].ToString();
            }
            return null;
        }
        set
        {
            ViewState[CommonUtility.LEAD_ID] = value;
            HiddenFieldLeadId.Value = value;
        }
    }

    public int LeadStageId
    {
        get
        {
            if (ViewState[CommonUtility.LEADSTAGE_ID] != null && ViewState[CommonUtility.LEADSTAGE_ID] != "0")
            {
                return int.Parse(ViewState[CommonUtility.LEADSTAGE_ID].ToString());
            }
            return 0;
        }
        set { ViewState[CommonUtility.LEADSTAGE_ID] = value; }
    }

    private static string customerType;
    public static string CustomerType
    {
        get
        {
            return customerType;
        }
        set { customerType = value; }
    }

    private static int count;
    public static int Count
    {
        get { return count; }
        set { count = value; }
    }

    private static int contactPersoncount;
    public static int ContactPersonCount
    {
        get { return contactPersoncount; }
        set { contactPersoncount = value; }
    }

    private static int documentcount;
    public static int Documentcount
    {
        get { return documentcount; }
        set { documentcount = value; }
    }

    private static int activitycount;
    public static int Activitycount
    {
        get { return activitycount; }
        set { activitycount = value; }
    }

    private KeyValuePair<string, string> LeadEntry
    {
        get
        {
            if (Session[CommonUtility.LEAD_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.LEAD_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.LEAD_DATA] = value;
        }
    }
    #endregion

    #region Events
    protected void Page_Load(object sender, EventArgs e)
     {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
                SetBackLink();
                
                div_autocomplete.InnerHtml = SetAutocomplete();

                //Remove Other Session.
                ClearSession();
                HiddenFieldIsContinueWithSave.Value = string.Empty;

                PageLoad();
            }

            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Organisation Entry ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    private void PageLoad()
    {
        if (Session[CommonUtility.GLOBAL_SETTING] == null)
            BindLeadCustomers();

         if ((Request.QueryString["leadid"] != null && Request.QueryString["leadid"] != string.Empty && Request.QueryString["leadid"] != "0") ||
                (LeadId != null))
        {
            //RadTabStrip2.Tabs.Clear();
            //AddTab("Contact Details", "contactdetails", true);
            //AddTab("Contact Person", "contactperson", true);
            //AddTab("Activity", "activity", true);
            //AddTab("Documents", "document", true);
            //AddTab("Opportunities", "customer_oppotunity", true);
            //AddTab("Emails", "customer_email", true);

            //Master.SetCommonHeader("lead");
            string leadid = "";
            if (Request.QueryString["leadid"] != null)
                leadid = Request.QueryString["leadid"].ToString();
            else
                leadid = LeadId;
            
            PopulateGrids(leadid);
            HiddenFieldTabIndex.Value = "1";
            if (Request.QueryString["ref"] != null && Request.QueryString["ref"] != string.Empty)
            {
                if (Request.QueryString["ref"] == "act")
                {
                    HiddenFieldTabIndex.Value = "4";
                }
                if (Request.QueryString["ref"] == "con")
                {
                    HiddenFieldTabIndex.Value = "1";
                }
                if (Request.QueryString["ref"] == "per")
                {
                    HiddenFieldTabIndex.Value = "3";
                }
                if (Request.QueryString["ref"] == "add")
                {
                    HiddenFieldTabIndex.Value = "2";
                }
                if (Request.QueryString["ref"] == "doc")
                {
                    HiddenFieldTabIndex.Value = "5";
                }
                if (Request.QueryString["ref"] == "opp")
                {
                    HiddenFieldTabIndex.Value = "6";
                }
                if (Request.QueryString["ref"] == "mai")
                {
                    HiddenFieldTabIndex.Value = "7"; 
                }
            }
            else
            {
                //AddPageView(RadTabStrip2.FindTabByText("Contact Details"), true);
            }
        }
        else
        {
            //RadTabStrip2.Tabs.Clear();
            //Session.Remove(CommonUtility.LEAD_DATA);
            //AddTab("Contact Details", "contactdetails", true);
            //AddTab("Contact Person", "contactperson", false);
            //AddTab("Activity", "activity", false);
            //AddTab("Documents", "document", false);
            //AddTab("Opportunities", "customer_oppotunity", false);
            //AddTab("Emails", "customer_email", false);
            //AddPageView(RadTabStrip2.FindTabByText("Contact Details"), false);

            //Master.SetCommonHeader("lead");
            LoadLookupData();

            // buttonActiveDeactive.Visible = false;
            buttonActiveDeactive.Text = "INACTIVE";
            buttonActiveDeactive.CssClass = "disdeactivebtn";
            buttonConvert.Enabled = false;
            buttonConvert.CssClass = "disopenentry";
           // DisableTabs();
            buttonConvert.CssClass = "disconvertprosbtn";
            txtAssignto.Text = UserSession.Instance.UserName;
            HiddenAssignTo.Value = UserSession.Instance.UserName;

            SetLeadStage();
        }
    }

    private void BindLeadCustomers()
    {
        //CurrentUserSettings = new ArgsDTO();
        if (CurrentUserSettings == null)
            CurrentUserSettings = new ArgsDTO();
        CurrentUserSettings.ChildOriginators = UserSession.Instance.ChildOriginators;
        CurrentUserSettings.Originator = UserSession.Instance.UserName;
        CurrentUserSettings.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
    }

    private void SetBackLink()
    {
        if (Request.QueryString["ty"] != null && Request.QueryString["ty"] != string.Empty)
        {
            HiddenFieldLeadStage.Value = Request.QueryString["ty"].ToString();
        }

        if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty)
        {
            if (Request.QueryString["fm"] == "home")
            {
                hlBack.NavigateUrl = "~/default.aspx?fm=home";
            }
            else if (Request.QueryString["fm"] == "activity")
            {
                hlBack.NavigateUrl = "~/activity_planner/transaction/activity_scheduler.aspx";
            }
            else if (Request.QueryString["fm"] == "act")
            {
                hlBack.NavigateUrl = "~/calendar/transaction/appointment_entry.aspx";
            }
            //else if (Request.QueryString["fm"] == "dsbd")
            //{
            //    if (Request.QueryString["rptid"] != null && Request.QueryString["rptid"] != string.Empty)
            //    {
            //        if (Request.QueryString["rptid"].ToString() == "1" || Request.QueryString["rptid"].ToString() == "2")
            //        {
            //            hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/AnalysisGraph.aspx?rptid=" + Request.QueryString["rptid"].ToString()
            //                + "&month=" + Request.QueryString["month"].ToString() + "&fromdate=" + Request.QueryString["fromdate"].ToString() + "&todate="
            //                + Request.QueryString["todate"].ToString() + "&leadstage=" + Request.QueryString["leadstage"].ToString(); ;
            //        }
            //        else
            //        {
            //            hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/AnalysisGraph.aspx?rptid=" + Request.QueryString["rptid"].ToString();
            //        }
            //    }
            //    else
            //        hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/AnalysisGraph.aspx";
            //}
            //else if (Request.QueryString["fm"] == "dsbd3")
            //{
            //    hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/CheckListAnalysis.aspx";
            //}
            else if (Request.QueryString["fm"] == "opp")
            {
                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "pipelines_stage/transaction/opportunity.aspx?cht=pipe&fm=lead";//?cht=pipe&fm=lead&pipid=1";
            }
            else if (Request.QueryString["fm"] == "oppent")
            {
                StringBuilder additionalParam = new StringBuilder("?fm=Lead");

                if (Request.QueryString["oleadid"] != null && Request.QueryString["oleadid"] != string.Empty)
                    additionalParam.Append("&leadid=" + Request.QueryString["oleadid"]);

                if (Request.QueryString["oppid"] != null && Request.QueryString["oppid"] != string.Empty)
                    additionalParam.Append("&oppid=" + Request.QueryString["oppid"]);

                if (Request.QueryString["custid"] != null && Request.QueryString["custid"] != string.Empty)
                    additionalParam.Append("&custid=" + Request.QueryString["custid"]);

                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "pipelines_stage/transaction/opportunityentry.aspx" + additionalParam.ToString();
            }
            else if (Request.QueryString["fm"] == "lead")
            {
                if (Request.QueryString["ty"] != null && Request.QueryString["ty"] != string.Empty)
                {
                    hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "lead_customer/transaction/lead_contacts.aspx?ty=" + Request.QueryString["ty"] + "&fm=lead";
                }
            }
            else if (Request.QueryString["fm"] == "search")
            {
                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "lead_customer/transaction/search.aspx";
            }

            else if (Request.QueryString["fm"] == "dsbd1")
            {
                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/ActivityAnalysis.aspx";
            }
            else if (Request.QueryString["fm"] == "dsbd2")
            {
                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/OpportunityAnalysis.aspx";
            }
        }
    }

    protected void buttonSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            SaveLeadEntry();
        }
    }

    protected void buttonActivityHistory_Click(object sender, EventArgs e)
    {
        Response.Redirect(ConfigUtil.ApplicationPath + "activity_planner/transaction/activity_history.aspx?fm=acthis&leadid=" + Request.QueryString["leadid"]);
    }

    protected void buttonClear_Click(object sender, EventArgs e)
    {
        ClearControls();
    }

    protected void buttonActiveDeactive_Click(object sender, EventArgs e)
    {
        try
        {
            ArgsDTO args = new ArgsDTO();
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            CommonClient commonClient = new CommonClient();

            string activeDeactive = buttonActiveDeactive.Text;
            if (!string.IsNullOrWhiteSpace(activeDeactive))
            {
                if (ViewState[CommonUtility.SAVE_OBJECT] != null)
                {
                    LeadDTO lead_deatail = ViewState[CommonUtility.SAVE_OBJECT] as LeadDTO;

                    if (lead_deatail != null)
                    {
                        if (HiddenFieldIsDeactivate.Value != "1")
                        {
                            string message = "Do you want to " + activeDeactive.Trim() + " - " + Server.HtmlEncode(lead_deatail.LeadName.Trim()) + " ?";
                            ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "showcheactivedeactiveModal('" + message + "','');", true);
                        }
                        else
                        {
                            List<LookupTableDTO> lstLookupradstatus = commonClient.GetLookupTables("ACST", args);
                            List<LookupTableDTO> lstLookupradPriority = commonClient.GetLookupTables("ACPR", args);

                            CommonUtility commonUtility = new CommonUtility();

                            if (lead_deatail.DelFalg != "Y")
                            {
                                LeadClient leadClient = new LeadClient();
                                ActivityClient activityClient = new ActivityClient();
                                ActivityDTO activityDTO = new ActivityDTO();
                                activityDTO.Subject = "Deactivated";
                                activityDTO.StartDate = DateTime.Now;
                                activityDTO.EndDate = DateTime.Now;

                                if (lstLookupradstatus != null)
                                {
                                    if (lstLookupradPriority.Count != 0)
                                    {
                                        activityDTO.Status = lstLookupradstatus[0].TableCode;
                                    }
                                }

                                if (lstLookupradPriority != null)
                                {
                                    if (lstLookupradPriority.Count != 0)
                                    {
                                        activityDTO.Priority = lstLookupradPriority[0].TableCode;
                                    }
                                }

                                activityDTO.AssignedTo = UserSession.Instance.UserName;
                                activityDTO.LeadID = lead_deatail.LeadID;
                                activityDTO.Comments = "Deactivated";
                                activityDTO.SentMail = "N";
                                activityDTO.SendReminder = "N";
                                activityDTO.ReminderDate = DateTime.Now;
                                activityDTO.ActivityType = "INAC";
                                activityDTO.CreatedDate = DateTime.Now;
                                activityDTO.CreatedBy = UserSession.Instance.UserName;
                                activityDTO.CustomerCode = "";

                                bool isDeleted = leadClient.DeleteLead(lead_deatail, activityDTO);

                                if (isDeleted)
                                {
                                    string message = Server.HtmlEncode(lead_deatail.LeadName) + " is De-activated.";
                                    ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "showcheactivedeactiveModal('" + message + "','1','" + HiddenFieldLeadStage.Value + "');", true);
                                }
                            }
                            else
                            {
                                LeadClient leadClient = new LeadClient();
                                ActivityClient activityClient = new ActivityClient();
                                ActivityDTO activityDTO = new ActivityDTO();

                                LeadStageClient obLeadStage = new LeadStageClient();
                                List<LeadStageDTO> lstLeadStage = new List<LeadStageDTO>();

                                activityDTO.Subject = "Reactivated";
                                activityDTO.StartDate = DateTime.Now;
                                activityDTO.EndDate = DateTime.Now;

                                if (lstLookupradstatus != null)
                                {
                                    if (lstLookupradPriority.Count != 0)
                                    {
                                        activityDTO.Status = lstLookupradstatus[0].TableCode;
                                    }
                                }

                                if (lstLookupradPriority != null)
                                {
                                    if (lstLookupradPriority.Count != 0)
                                    {
                                        activityDTO.Priority = lstLookupradPriority[0].TableCode;
                                    }
                                }

                                activityDTO.AssignedTo = UserSession.Instance.UserName;
                                activityDTO.LeadID = lead_deatail.LeadID;
                                activityDTO.Comments = "Activated";
                                activityDTO.SentMail = "N";
                                activityDTO.SendReminder = "N";
                                activityDTO.ReminderDate = DateTime.Now;
                                activityDTO.ActivityType = "REAC";
                                activityDTO.CreatedDate = DateTime.Now;
                                activityDTO.CreatedBy = UserSession.Instance.UserName;
                                activityDTO.CustomerCode = "";

                                bool isActivate =  leadClient.ActivateLead(lead_deatail.LeadID, activityDTO);

                                if (isActivate)
                                {
                                    buttonActiveDeactive.Text = "De-Activate";
                                    buttonActiveDeactive.CssClass = "deactivebtn";

                                    GetLeadDetails(LeadId);

                                    div_message.Attributes.Add("style", "display:block;padding:2px");
                                    div_message.InnerHtml = commonUtility.GetSucessfullMessage("Contact Activated");

                                    div_info.Attributes.Add("style", "display:none");
                                    div_info.InnerHtml = "";

                                    buttonSave.Enabled = true;
                                    buttonClear.Enabled = true;
                                    buttonConvert.Enabled = true;

                                    buttonSave.CssClass = "savebtn";
                                    buttonClear.CssClass = "clearbtn";
                                    buttonConvert.CssClass = "convertprosbtn";

                                    if (ViewState["LeadStageList"] == null)
                                    {
                                        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
                                        lstLeadStage = obLeadStage.GetLeadStages(args);
                                        ViewState["LeadStageList"] = lstLeadStage;
                                    }
                                    else
                                        lstLeadStage = ViewState["LeadStageList"] as List<LeadStageDTO>;

                                    if (lstLeadStage.Any(s => s.StageName == "Customer"))
                                    {
                                        if (lead_deatail.LeadStageID == lstLeadStage.First(s => s.StageName == "Customer").StageId)
                                        {
                                            buttonSave.Enabled = false;
                                            buttonClear.Enabled = false;
                                            buttonConvert.Enabled = false;

                                            buttonSave.CssClass = "dissavebtn";
                                            buttonClear.CssClass = "disclearbtn";
                                            buttonConvert.CssClass = "disconvertprosbtn";
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //commonUtility.ErrorMaintain(ex);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    protected void buttonConvert_Click(object sender, EventArgs e)
    {
        div_message.Attributes.Add("style", "display:none");
        div_message.InnerHtml = "";
        div_info.Attributes.Add("style", "display:none");
        div_info.InnerHtml = "";


        if (string.IsNullOrWhiteSpace(HiddenFieldHasClickedCheckList.Value))
        {
            DisplayCheckList(LeadStageId);
            ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "showchecklistModal('#MainContent_div_duplicaterecord');", true);
            //HiddenFieldHasClickedCheckList.Value = "1";
        }
        else if (HiddenFieldHasClickedCheckList.Value != "0")
        {
            string IsOwnershipChange = string.Empty;

            //Do the save section.
            SaveCheckList(LeadStageId);
            Master.LeadAndCustomers(true);
        }
    }

    //protected void RadMultiPage1_PageViewCreated(object sender, RadMultiPageEventArgs e)
    //{
    //    try
    //    {
    //        string userControlName = GetControlName(e.PageView.ID);

    //        Control userControl = Page.LoadControl(ConfigUtil.ApplicationPath + "usercontrols/" + userControlName + ".ascx");
    //        userControl.ID = e.PageView.ID + "_userControl";

    //        e.PageView.Controls.Add(userControl);
    //    }
    //    catch (Exception ex)
    //    {
    //        //commonUtility.ErrorMaintain(ex);

    //        //div_info.Attributes.Add("style", "display:block");
    //        //div_message.Attributes.Add("style", "display:none");
    //        //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
    //    }
    //}

    //protected void RadTabStrip2_TabClick(object sender, RadTabStripEventArgs e)
    //{
    //    AddPageView(e.Tab, true);
    //    e.Tab.PageView.Selected = true;
    //}
    #endregion

    #region Methods

    private void ClearSession()
    {
        Session[CommonUtility.CUSTOMER_DATA] = null;
    }

    private void LoadLookupData()
    {
        try
        {
            CommonClient common_details = new CommonClient();
            List<LookupTableDTO> lookupList = new List<LookupTableDTO>();

            if (UserSession.Instance.UserName != null)
            {
                ArgsDTO args = new ArgsDTO();
                args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;

                //Load Market
                lookupList = common_details.GetLookupTables(ConfigUtil.MarketCode, args);
                if (lookupList.Count != 0)
                {
                    dropdownMarket.Items.Clear();
                    foreach (LookupTableDTO item in lookupList)
                    {
                        dropdownMarket.Items.Add(new ListItem(item.TableDescription.Trim(), item.TableCode.Trim()));

                        if (item.DefaultValue.Equals("Y"))
                        {
                            dropdownMarket.SelectedValue = item.TableCode.Trim();
                        }
                    }
                }

                //Load Source
                lookupList = new List<LookupTableDTO>();
                lookupList = common_details.GetLookupTables(ConfigUtil.SourceCode, args);

                if (lookupList.Count != 0)
                {
                    DropDownListSource.Items.Clear();
                    foreach (LookupTableDTO item in lookupList)
                    {
                        DropDownListSource.Items.Add(new ListItem(item.TableDescription.Trim(), item.TableCode.Trim()));

                        if (item.DefaultValue.Equals("Y"))
                        {
                            DropDownListSource.SelectedValue = item.TableCode.Trim();
                        }
                    }
                }

                //Load Channel
                lookupList = new List<LookupTableDTO>();
                lookupList = common_details.GetLookupTables(ConfigUtil.ChannelCode, args);

                if (lookupList.Count != 0)
                {
                    dropdownChannel.Items.Clear();
                    foreach (LookupTableDTO item in lookupList)
                    {
                        dropdownChannel.Items.Add(new ListItem(item.TableDescription.Trim(), item.TableCode.Trim()));

                        if (item.DefaultValue.Equals("Y"))
                        {
                            dropdownChannel.SelectedValue = item.TableCode.Trim();
                        }
                    }
                }
            }

            //Load LitersBy
            string[,] sArrLitersBy = {{"W", "M", "Y"}, 
                                   {"Weekly","Monthly","Yearly"}};

            List<KeyValuePair<string, string>> litersList = new List<KeyValuePair<string, string>>();
            dropdownWeek.Items.Clear();

            for (int i = 0; i < sArrLitersBy.GetLength(1); i++)
            {
                litersList.Add(new KeyValuePair<string, string>(sArrLitersBy[0, i], sArrLitersBy[1, i]));
            }

            foreach (KeyValuePair<string, string> item in litersList)
            {
                dropdownWeek.Items.Add(new ListItem(item.Value.Trim(), item.Key.Trim()));
            }
        }
        catch (Exception ex)
        {
            //commonUtility.ErrorMaintain(ex);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }
    
    private void SetLeadStage()
    {
        LeadStageClient obLeadStage = new LeadStageClient();
        List<LeadStageDTO> lstLeadStage = new List<LeadStageDTO>();
        LeadStageDTO currentLeadStage = new LeadStageDTO();
        ArgsDTO args = new ArgsDTO();
      //  string a = args.LeadStageName;
        try
        {
            if (ViewState["LeadStageList"] == null)
            {
                args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
                lstLeadStage = obLeadStage.GetLeadStages(args);
                ViewState["LeadStageList"] = lstLeadStage;
            }
            else
                lstLeadStage = ViewState["LeadStageList"] as List<LeadStageDTO>;

            if (LeadStageId > 0)
            {
                currentLeadStage = lstLeadStage.FirstOrDefault(s => s.StageId == LeadStageId);
            }
            else
            {
                currentLeadStage = lstLeadStage.OrderBy(s => s.StageOrder).FirstOrDefault();

                if (currentLeadStage != null && currentLeadStage.StageName != "Customer")
                    LeadStageId = currentLeadStage.StageId;
            }

            if (currentLeadStage != null)
            {
                if (currentLeadStage.StageName == "Customer")
                {
                    tbStage.Text = "Request Sent";

                    buttonActiveDeactive.CssClass = "disdeactivebtn";
                    buttonActiveDeactive.Enabled = false;
                    buttonSave.Enabled = false;
                    buttonClear.Enabled = false;
                    buttonSave.CssClass = "dissavebtn";
                    buttonClear.CssClass = "disclearbtn";
                }
                else
                {
                    tbStage.Text = currentLeadStage.StageName.ToUpper();
                    //tbStage.ForeColor = System.Drawing.Color.RoyalBlue;
                }
            }

            LeadStageDTO nextLeadStage = new LeadStageDTO();
            //Retreive the next stage in the order sequence
            nextLeadStage = lstLeadStage.FirstOrDefault(s => s.StageOrder == (currentLeadStage.StageOrder + 1));

            if (nextLeadStage != null)
            {
                buttonConvert.Text = "convert to " + nextLeadStage.StageName.ToLower();

                if (LeadId == "0" || LeadId == null)
                {
                    buttonConvert.Enabled = false;
                    buttonActiveDeactive.Enabled = false;
                }
                else
                {
                    buttonConvert.Enabled = true;
                    buttonActiveDeactive.Enabled = true;
                }
            }
            else // If the current stage is the last stage, hide the convert button
            {
                buttonConvert.Visible = false;
                //contactperson = (usercontrols_contactperson)LoadControl(ConfigUtil.ApplicationPath + "usercontrols/contactperson.ascx");
                //contactperson.EnableDisableAddButton(false);

                //oppotunity = (usercontrols_customer_oppotunity)LoadControl(ConfigUtil.ApplicationPath + "usercontrols/customer_oppotunity.ascx");
                //oppotunity.EnableDisableButton(false);

                buttonSave.Enabled = false;
                buttonClear.Enabled = false;

                //btnLeadAddress.IsEnabled = false;
                //btnMap.IsEnabled = false;

                //rgvAddress.IsEnabled = false;
            }
        }
        catch (Exception ex)
        {
            //commonUtility.ErrorMaintain(ex);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    private void PopulateGrids(string leadid)
    {
        LeadId = leadid;
        CustomerType = HiddenFieldUsertype.Value;
        KeyValuePair<string, string> lead_data = new KeyValuePair<string, string>(LeadId, CustomerType);

        //Setting session value.
        LeadEntry = lead_data;

        //Load DropDown Data.
        LoadLookupData();

        //Load Lead Details.
        GetLeadDetails(leadid);
    }

    private void GetLeadDetails(string leadid)
    {
        try
        {
            LeadClient lead_client = new LeadClient();
            LeadDTO lead_deatail = lead_client.GetLead(leadid);
            LeadStageId = lead_deatail.LeadStageID;
            HiddenFieldLeadId.Value = leadid.Trim();
            LeadId = leadid;
            ViewState[CommonUtility.SAVE_OBJECT] = lead_deatail as LeadDTO;

            if (lead_deatail != null)
            {
                //HiddenFieldUsertype.Value = leadEntry.SourceTypes.ToString();

                //Setting Name.
                if (!string.IsNullOrWhiteSpace(lead_deatail.LeadName))
                {
                    txtName.Text = HttpUtility.HtmlDecode(lead_deatail.LeadName);
                }

                //Setting Company.
                if (!string.IsNullOrWhiteSpace(lead_deatail.Company))
                {
                    txtCompany.Text = HttpUtility.HtmlDecode(lead_deatail.Company);
                }

                //Setting Market.
                if (!string.IsNullOrWhiteSpace(lead_deatail.Industry))
                {
                    dropdownMarket.SelectedValue = lead_deatail.Industry;
                }

                //Setting Channel.
                if (!string.IsNullOrWhiteSpace(lead_deatail.Business))
                {
                    dropdownChannel.SelectedValue = lead_deatail.Business;
                }

                //Setting Potential Litres
                if (!string.IsNullOrWhiteSpace(lead_deatail.LitersBy))
                {
                    dropdownWeek.SelectedValue = lead_deatail.LitersBy;
                }

                //Setting Potential Litres
                if (lead_deatail.PotentialLiters != 0)
                {
                    TextBoxLiters.Text = HttpUtility.HtmlDecode(lead_deatail.PotentialLiters.ToString());
                }
                else
                {
                    TextBoxLiters.Text = "0.00";
                }

                //Setting Description
                if (lead_deatail.Description.Length != 0)
                {
                    txtDescription.Text = HttpUtility.HtmlDecode(lead_deatail.Description);
                }

                //Setting Business Potential
                if (!string.IsNullOrWhiteSpace(lead_deatail.BusinessPotential))
                {
                    txtBusinessPotential.Text = HttpUtility.HtmlDecode(lead_deatail.BusinessPotential.Trim());
                }

                //Setting Referred By
                if (!string.IsNullOrWhiteSpace(lead_deatail.ReferredBy))
                {
                    txtRefBy.Text = HttpUtility.HtmlDecode(lead_deatail.ReferredBy);
                }

                //Setting Source
                if (!string.IsNullOrWhiteSpace(lead_deatail.LeadSource))
                    DropDownListSource.SelectedValue = lead_deatail.LeadSource;

                //Setting Assign to.
                if (!string.IsNullOrWhiteSpace(lead_deatail.Originator))
                {
                    txtAssignto.Text = lead_deatail.Originator.Trim();
                    HiddenAssignTo.Value = lead_deatail.Originator.Trim();
                }
                else
                {
                    txtAssignto.Text = UserSession.Instance.UserName;
                    HiddenAssignTo.Value = UserSession.Instance.UserName;
                }

                //Setting Rep Group.
                if (!string.IsNullOrWhiteSpace(lead_deatail.RepGroupName))
                {
                    //div_bdm_group.Text = lead_deatail.RepGroupName;
                    HiddenFieldBDMGroup.Value = lead_deatail.RepGroupID.ToString().Trim();
                }
                TextBoxAnnualRevenue.Text = lead_deatail.AnnualRevenue.ToString();
                TextBoxEmployees.Text = lead_deatail.NoOfEmployees.ToString();

                HiddenAssignTo.Value = lead_deatail.Originator;
                HiddenFieldContactPhone.Value = lead_deatail.Telephone;
                HiddenFieldContactFax.Value = lead_deatail.Fax;
                HiddenFieldContactMobile.Value = lead_deatail.Mobile;
                HiddenFieldContactEmail.Value = lead_deatail.EmailAddress;
                HiddenFieldContactPrefMethod.Value = lead_deatail.PreferredContact;
                HiddenFieldCustAddress.Value = lead_deatail.Address;
                HiddenFieldCustCity.Value = lead_deatail.City;
                HiddenFieldCustPostcode.Value = lead_deatail.PostCode;
                HiddenFieldCustState.Value = lead_deatail.State;

                SetLeadStage();

                // WEEKS SINCE LAST CALLED
                TimeSpan tsLastCalled;
                decimal dWeeks = 0;
                if (lead_deatail.LastCalledDate != null)
                {
                    tsLastCalled = DateTime.Now - Convert.ToDateTime(lead_deatail.LastCalledDate);
                    decimal result = decimal.Parse(tsLastCalled.Days.ToString()) / decimal.Parse("7");
                    dWeeks = decimal.Ceiling(result);
                    div_additionalText.InnerHtml = "WEEKS SINCE LAST CALLED : " + String.Format("{0:0.##}", dWeeks);
                }
                else
                {
                    div_additionalText.InnerHtml = "NO ACTIVITIES YET";
                }

                // CREATED BY / DATE and LAST MODIFIED BY / DATE
                div_created.InnerHtml = "Created By: " + lead_deatail.CreatedBy + ", " + lead_deatail.CreatedDate.ToString("dd-MMM-yyyy HH:mm:ss");

                div_modifiedby.InnerHtml = "Last Modified By: " + lead_deatail.LastModifiedBy + ", " + lead_deatail.LastModifiedDate.ToString("dd-MMM-yyyy HH:mm:ss");

                SetOpportunityConstraints(lead_deatail.LeadStageID);

                //Setting Rating
                rRating.Value = lead_deatail.Rating;

                if (lead_deatail.DelFalg == "Y")
                {
                    buttonActiveDeactive.Text = "Activate";
                    buttonActiveDeactive.CssClass = "activebtn";
                    buttonSave.Enabled = false;
                    buttonClear.Enabled = false;
                    buttonConvert.Enabled = false;

                    buttonSave.CssClass = "dissavebtn";
                    buttonClear.CssClass = "disclearbtn";
                    buttonConvert.CssClass = "disconvertprosbtn"; ;
                }
                else
                {
                    buttonActiveDeactive.Text = "De-Activate";
                    buttonActiveDeactive.CssClass = "deactivebtn";
                }

                //GetPrevCustCodeForCustomerEntry();
            }
            else
            {
                //Redirect the no data found page.
                Response.Redirect("");
            }
        }
        catch (Exception ex)
        {
            //commonUtility.ErrorMaintain(ex);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }


    }

    private void SaveLeadEntry()
    {
        LeadDTO lead_save = null;
        LeadClient leadClient = new LeadClient();
        CommonUtility commonUtility = new CommonUtility();
        LeadClient lead = new LeadClient();
        int leadid = 0;

        try
        {
            //LeadDTO lead_details = lead.GetLead(LeadEntry.Key);

            if (ViewState[CommonUtility.SAVE_OBJECT] != null)
                lead_save = ViewState[CommonUtility.SAVE_OBJECT] as LeadDTO;

            if (lead_save == null)
            {
                //New Creation.
                lead_save = new LeadDTO();
            }

            if (LeadStageId <= 0)
            {
                div_message.Attributes.Add("style", "display:block");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Cannot save lead details. The Lead stages are not entered."); 
                return;
            }

            if (txtName.Text.Trim() == "")
            {
                div_message.Attributes.Add("style", "display:block");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Short name cannot be empty."); 
                txtName.Focus();
                return;
            }
            /*
            if (!string.IsNullOrEmpty(HiddenFieldContactEmail.Value))
            {
                if (!clsGlobal.ValidateExpression(txtEmail.Text, GlobalConstant.ValidEmailExpression))
                {
                    GlobalValues.GetInstance().ShowMessage("Email address is invalid.", GlobalValues.MessageImageType.Warning);
                    txtEmail.Focus();
                    return;
                }
            }*/

            //lead = new LeadDTO();
            //lead.LeadID = ((txtLeadID.Text != null && txtLeadID.Text != "") ? Convert.ToInt32(txtLeadID.Text) : 0);
            lead_save.LeadID = string.IsNullOrEmpty(LeadId) ? 0 : int.Parse(LeadId);

            lead_save.LeadName = HttpUtility.HtmlEncode(txtName.Text.Trim());
            if (DropDownListSource.SelectedIndex != -1)
                lead_save.LeadSource = DropDownListSource.SelectedValue != null ? DropDownListSource.SelectedValue.ToString() : "N";
            lead_save.Company = HttpUtility.HtmlEncode(txtCompany.Text.Trim());
            lead_save.Originator = HiddenAssignTo.Value;
            lead_save.Telephone = HiddenFieldContactPhone.Value == null ? string.Empty : HiddenFieldContactPhone.Value.Trim();
            lead_save.Fax = HiddenFieldContactFax.Value == null ? string.Empty : HiddenFieldContactFax.Value.Trim();
            lead_save.Mobile = HiddenFieldContactMobile.Value == null ? string.Empty : HiddenFieldContactMobile.Value.Trim();
            lead_save.EmailAddress = HiddenFieldContactEmail.Value;
            //lead.AnnualRevenue = txtAnnualRevenue.Text != "" ? Convert.ToDouble(txtAnnualRevenue.Text) : 0;
            if (!string.IsNullOrEmpty(TextBoxAnnualRevenue.Text))
                lead_save.AnnualRevenue = Convert.ToDouble(TextBoxAnnualRevenue.Text.Trim());
            //lead.Business = txtBusiness.Text;

            if (dropdownMarket.SelectedIndex != -1)
            {
                lead_save.Industry = dropdownMarket.SelectedValue.Trim();
            }

            //Setting Channel.
            if (dropdownChannel.SelectedIndex != -1)
            {
                lead_save.Business = dropdownChannel.SelectedValue.Trim();
            }

            //lead.NoOfEmployees = txtNoOfEmployees.Text != "" ? Convert.ToInt32(txtNoOfEmployees.Text) : 0;
            if (!string.IsNullOrEmpty(TextBoxEmployees.Text))
                lead_save.NoOfEmployees = Convert.ToInt32(Convert.ToDouble(TextBoxEmployees.Text.Trim()));
            lead_save.Rating = rRating.Value != null ? Convert.ToInt32(rRating.Value) : 0;
            //lead.LeadStatus = cmbStatus.SelectedValue.ToString();
            lead_save.Website = HiddenFieldContactWebSite.Value;
            //lead.Address = txtAddress.Text;
            //lead.City = txtCity.Text;
            //lead.State = txtState.Text;
            //lead.PostCode = txtPostCode.Text;

            //Setting Business Potential
            lead_save.BusinessPotential = HttpUtility.HtmlEncode(txtBusinessPotential.Text);

            //Setting Referred By.
            lead_save.ReferredBy = HttpUtility.HtmlEncode(txtRefBy.Text);

            //lead.Probability = Convert.ToDouble(txtProbability.Text);
            lead_save.Description = txtDescription.Text;
            //lead.PreferredContact = txtPreferredContact.Text;
            //lead.Country = txtCountry.Text;
            if (!string.IsNullOrWhiteSpace(HiddenFieldContactPrefMethod.Value))
                lead_save.PreferredContact = HiddenFieldContactPrefMethod.Value != null ? HiddenFieldContactPrefMethod.Value.Trim() : "N";


            lead_save.LeadStageID = LeadStageId;
            //lead.Status = "L";
            //Setting Potential Litres            
            if (dropdownWeek.SelectedIndex != -1)
            {
                lead_save.LitersBy = dropdownWeek.SelectedValue != null ? dropdownWeek.SelectedValue.ToString() : "W"; ;
            }
            //lead.PotentialLiters = txtPotentialLiters.Text != "" ? Convert.ToDouble(txtPotentialLiters.Text) : 0;
            //Setting PotentialLiters
            lead_save.PotentialLiters = TextBoxLiters.Text.Length != 0 ? Convert.ToDouble(TextBoxLiters.Text.ToString().Trim()) : 0;

            //Setting BDM Group.
            lead_save.RepGroupID = ((!string.IsNullOrWhiteSpace(HiddenFieldBDMGroup.Value)) ? Convert.ToInt32(HiddenFieldBDMGroup.Value) : 0);

            lead_save.Address = HiddenFieldCustAddress.Value;
            lead_save.City = HiddenFieldCustCity.Value;
            lead_save.PostCode = HiddenFieldCustPostcode.Value;
            lead_save.State = HiddenFieldCustState.Value;


            lead_save.CreatedBy = UserSession.Instance.UserName;
            lead_save.CreatedDate = DateTime.Now.ToUniversalTime();
            lead_save.LastModifiedBy = UserSession.Instance.UserName;
            lead_save.LastModifiedDate = DateTime.Now.ToUniversalTime();
            lead_save.StartDate = DateTime.Now.ToUniversalTime();

            ArgsDTO args = new ArgsDTO();
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;

            if (lead_save.LeadID == 0 && HiddenFieldIsContinueWithSave.Value != "1")
            {
                LeadCustomerClient leadCustomerClient = new LeadCustomerClient();
                List<LeadCustomerDTO> existingLead = new List<LeadCustomerDTO>();
                existingLead = leadCustomerClient.LeadExist(lead_save, args);

                if (existingLead.Count > 0)
                {
                    //Show Dialog box with existing records.
                    DisplayDuplicateRecords(existingLead);
                    ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "showModal('#MainContent_div_duplicaterecord');", true);
                    return;
                }
            }

            if (leadClient.SaveLead(lead_save, args, out leadid))
            {
                //Succesfully saved.
                div_duplicaterecord.Attributes.Add("style", "display:none;padding:2px");
                div_duplicaterecord.InnerHtml = "";

                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved !");

                div_info.Attributes.Add("style", "display:none");
                div_info.InnerHtml = "";

                if (leadid == 0 && !UserSession.Instance.UserName.Equals(HiddenAssignTo.Value))
                {
                    CreateMailAndActivity(leadid);
                }
                if ((Request.QueryString["leadid"] == null || Request.QueryString["leadid"] == string.Empty) &&
                    (LeadId == null))
                {
                    LeadId = leadid.ToString();
                    ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "loadnewSave();", true);
                    PageLoad();
                }
                else
                {
                    GetLeadDetails(leadid.ToString());
                }

                /*if (Request.QueryString["leadid"] == null || Request.QueryString["leadid"] == string.Empty
                        || Request.QueryString["leadid"] == "0")
                {
                    Response.Redirect("leadentry.aspx?leadid=" + leadid);
                }*/
                buttonConvert.CssClass = "convertprosbtn";
                buttonConvert.Enabled = true;
                buttonActiveDeactive.Enabled = true;
            }
        }
        catch (Exception ex)
        {
            //Error Occured.
            //commonUtility.ErrorMaintain(ex);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }


    }

    private void CreateMailAndActivity(int iLeadId)
    {
        try
        {
            List<LookupTableDTO> lstLookupradpriority = new List<LookupTableDTO>();
            List<LookupTableDTO> lstLookupradstatus = new List<LookupTableDTO>();

            CommonUtility commonUtility = new CommonUtility();
            CommonClient commonClient = new CommonClient();
            ActivityClient activityClient = new ActivityClient();
            ActivityDTO activityDTO = new ActivityDTO();
            ArgsDTO args = new ArgsDTO();
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;

            string msg = tbStage.Text + " '" + txtName.Text + "' was assigned to you by " + UserSession.Instance.Originator;

            commonUtility.SendMail(ConfigUtil.FromMail, UserSession.Instance.OriginatorEmail,
                "Assigned a new " + tbStage.Text,
                msg);

            activityDTO.ActivityCount = 0;
            activityDTO.ActivityID = 0;

            activityDTO.ActivityType = activityClient.GetAllActivityTypes(UserSession.Instance.DefaultDepartmentId, false).First().Code;
            activityDTO.ActivityTypeDescription = null;
            activityDTO.AppointmentID = 0;
            activityDTO.AssignedTo = txtAssignto.Text;
            activityDTO.Comments = "";
            activityDTO.CreatedBy = UserSession.Instance.UserName;
            activityDTO.CreatedDate = DateTime.Now;
            activityDTO.CustomerCode = null;

            activityDTO.EndDate = DateTime.Now.AddHours(2);
            activityDTO.LastModifiedBy = UserSession.Instance.UserName;
            activityDTO.LastModifiedDate = DateTime.Now;
            activityDTO.LeadID = int.Parse(LeadId);
            activityDTO.LeadStageID = 0;
            activityDTO.ParentActivityID = 0;

            //Load Priority.
            lstLookupradpriority = commonClient.GetLookupTables("ACPR", args);
            foreach (LookupTableDTO obj in lstLookupradpriority)
            {
                if (obj.DefaultValue == "Y")
                {
                    activityDTO.Priority = obj.TableCode;
                    break;
                }
            }

            activityDTO.PriorityDescription = null;
            activityDTO.ReminderCreated = false;
            activityDTO.ReminderDate = DateTime.Now.AddHours(1);

            activityDTO.RepGroupID = 0;
            activityDTO.RepGroupName = null;
            activityDTO.SendReminder = "Y";
            activityDTO.SentMail = "N";
            activityDTO.StartDate = DateTime.Now;

            //Load Status.
            lstLookupradstatus = commonClient.GetLookupTables("ACST", args);
            foreach (LookupTableDTO obj in lstLookupradstatus)
            {
                if (obj.DefaultValue == "Y")
                {
                    activityDTO.Status = obj.TableCode;
                }
            }

            activityDTO.StatusDescription = null;
            activityDTO.Subject = "Assigned a new " + tbStage.Text;

            int iReminderActivityNo = 0;
            activityClient.SaveActivity(activityDTO, ref iReminderActivityNo);
        }
        catch (Exception ex)
        {

        }
    }

    private void DisplayDuplicateRecords(List<LeadCustomerDTO> ListleadCustomer)
    {
        if (ListleadCustomer.Count != 0)
        {
            StringBuilder txt = new StringBuilder();
            div_duplicaterecord.Attributes.Add("style", "display:none");


            txt.Append("<div>Possible duplicate record(s) exist.</div>");
            txt.Append("<div style=\"min-width:200px; overflow:auto\">");
            txt.Append("<table border=\"0\" id=\"repgroupTable\" width=\"1400px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");

            txt.Append("<tr class=\"detail-popsheader\">");
            txt.Append("<th>Name</th>");
            txt.Append("<th>Lead Stage</th>");
            txt.Append("<th>Customer Code</th>");
            txt.Append("<th>Company</th>");
            txt.Append("<th>Telephone</th>");
            txt.Append("<th>Mobile</th>");
            txt.Append("<th>Email</th>");
            txt.Append("<th>Address</th>");
            txt.Append("<th>Suburb</th>");
            txt.Append("<th>State</th>");
            txt.Append("<th>Post Code</th>");
            txt.Append("<th>Country</th>");

            txt.Append("</tr>");

            for (int i = 0; i < ListleadCustomer.Count; i++)
            {
                txt.Append("<tr>");
                txt.Append("<td class=\"tblborder\" align=\"left\">" + ListleadCustomer[i].Name + "</td>");
                txt.Append("<td class=\"tblborder\" align=\"left\">" + ListleadCustomer[i].LeadStage + "</td>");
                txt.Append("<td class=\"tblborder\" align=\"left\">" + ListleadCustomer[i].CustomerCode + "</td>");

                if (!string.IsNullOrWhiteSpace(ListleadCustomer[i].Company))
                    txt.Append("<td class=\"tblborder\" align=\"left\">" + ListleadCustomer[i].Company + "</td>");
                else
                    txt.Append("<td class=\"tblborder\" align=\"left\"></td>");

                if (!string.IsNullOrWhiteSpace(ListleadCustomer[i].Telephone))
                    txt.Append("<td class=\"tblborder\" align=\"left\">" + ListleadCustomer[i].Telephone + "</td>");
                else
                    txt.Append("<td class=\"tblborder\" align=\"left\"></td>");

                if (!string.IsNullOrWhiteSpace(ListleadCustomer[i].Mobile))
                    txt.Append("<td class=\"tblborder\" align=\"left\">" + ListleadCustomer[i].Mobile + "</td>");
                else
                    txt.Append("<td class=\"tblborder\" align=\"left\"></td>");

                if (!string.IsNullOrWhiteSpace(ListleadCustomer[i].Email))
                    txt.Append("<td class=\"tblborder\" align=\"left\">" + ListleadCustomer[i].Email + "</td>");
                else
                    txt.Append("<td class=\"tblborder\" align=\"left\"></td>");

                if (!string.IsNullOrWhiteSpace(ListleadCustomer[i].Address))
                    txt.Append("<td class=\"tblborder\" align=\"left\">" + ListleadCustomer[i].Address + "</td>");
                else
                    txt.Append("<td class=\"tblborder\" align=\"left\"></td>");

                if (!string.IsNullOrWhiteSpace(ListleadCustomer[i].City))
                    txt.Append("<td class=\"tblborder\" align=\"left\">" + ListleadCustomer[i].City + "</td>");
                else
                    txt.Append("<td class=\"tblborder\" align=\"left\"></td>");

                if (!string.IsNullOrWhiteSpace(ListleadCustomer[i].State))
                    txt.Append("<td class=\"tblborder\" align=\"left\">" + ListleadCustomer[i].State + "</td>");
                else
                    txt.Append("<td class=\"tblborder\" align=\"left\"></td>");

                if (!string.IsNullOrWhiteSpace(ListleadCustomer[i].PostalCode))
                    txt.Append("<td class=\"tblborder\" align=\"left\">" + ListleadCustomer[i].PostalCode + "</td>");
                else
                    txt.Append("<td class=\"tblborder\" align=\"left\"></td>");

                if (!string.IsNullOrWhiteSpace(ListleadCustomer[i].Country))
                    txt.Append("<td class=\"tblborder\" align=\"left\">" + ListleadCustomer[i].Country + "</td>");
                else
                    txt.Append("<td class=\"tblborder\" align=\"left\"></td>");

                txt.Append("</tr>");
            }
            txt.Append("</table>");
            txt.Append("</div>");
            txt.Append("<script type='text/javascript'>");
            txt.Append("$('div#jqi').css('height', '220px');");
            txt.Append("$('div#jqi').css('width', '900px');");
            txt.Append("$('div#jqi').css('left', '20%');");
            txt.Append("</script>");
            div_duplicaterecord.InnerHtml = txt.ToString();
        }
    }

    private void DisplayCheckList(int leadStageID)
    {
        LeadStageClient obLeadStage = new LeadStageClient();
        List<LeadStageDTO> lstLeadStage = new List<LeadStageDTO>();
        LeadStageDTO currentLeadStage = new LeadStageDTO();
        ArgsDTO args = new ArgsDTO();
        LeadStageDTO nextLeadStage = new LeadStageDTO();
        CommonClient leadClient = new CommonClient();

        List<CheckListDTO> checkList = leadClient.GetChecklist(leadStageID);

        if (checkList != null)
        {
            if (checkList.Count != 0)
            {
                if (ViewState["LeadStageList"] == null)
                {
                    args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
                    lstLeadStage = obLeadStage.GetLeadStages(args);
                    ViewState["LeadStageList"] = lstLeadStage;
                }
                else
                    lstLeadStage = ViewState["LeadStageList"] as List<LeadStageDTO>;

                if (LeadStageId > 0)
                {
                    currentLeadStage = lstLeadStage.FirstOrDefault(s => s.StageId == LeadStageId);
                }
                else
                {
                    currentLeadStage = lstLeadStage.OrderBy(s => s.StageOrder).FirstOrDefault();

                    if (currentLeadStage != null && currentLeadStage.StageName != "Customer")
                        LeadStageId = currentLeadStage.StageId;
                }

                nextLeadStage = lstLeadStage.FirstOrDefault(s => s.StageOrder == (currentLeadStage.StageOrder + 1));

                StringBuilder txt = new StringBuilder();
                div_duplicaterecord.InnerHtml = "";

                div_duplicaterecord.Attributes.Add("style", "display:none");

                txt.Append("<div id='div_checklistmessage' style='display:none'></div>");
                txt.Append("<div class='clearall'></div> ");
                txt.Append("<table border=\"0\" id=\"repgroupTable\" width=\"100%\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");

                txt.Append("<tr class=\"detail-popsheader\">");
                txt.Append("<th>Checklist Item</th>");
                txt.Append("<th></th>");

                txt.Append("</tr>");

                for (int i = 0; i < checkList.Count; i++)
                {
                    txt.Append("<tr>");
                    txt.Append("<td style=\"padding-left:5px;\" class=\"tblborder\">" + checkList[i].ChecklistName + "</td>");

                    string mandatory = "";

                    if (checkList[i].IsMandatory)
                        mandatory = "1";
                    else
                        mandatory = "0";

                    txt.Append("<td class=\"tblborder\" align=\"left\" id=\"" + checkList[i].ChecklistID.ToString() + "\" rel=\"" + mandatory.Trim() + "\"> <input type=\"checkbox\" rel=\"" + checkList[i].ChecklistName.ToString() + "\" name=\"group1\" id=\"a"+i+"\" ></input> </td>");

                    txt.Append("</tr>");
                }

                txt.Append("</table>");

                
                div_duplicaterecord.InnerHtml = txt.ToString();
            }
        }
        if (checkList == null)
            HiddenFieldConvertCount.Value = "0";
        else
            HiddenFieldConvertCount.Value = checkList.Count.ToString();

    }

    private bool SaveCheckList(int leadStageID)
    {
        bool hasError = false;

        LeadStageClient obLeadStage = new LeadStageClient();
        List<LeadStageDTO> lstLeadStage = new List<LeadStageDTO>();
        LeadStageDTO currentLeadStage = new LeadStageDTO();

        ArgsDTO args = new ArgsDTO();
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;

        string IsOwnershipChange = string.Empty;
        string sOwnershipEmailText = string.Empty;
        List<string> sSqls = new List<string>();
        LeadStageDTO nextLeadStage = new LeadStageDTO();
        LeadClient leadClient = new LeadClient();
        CheckListDTO checkListDTO = new CheckListDTO();
        List<CheckListDTO> items = new List<CheckListDTO>();
        CommonClient commonClient = new CommonClient();
        CommonUtility commonUtility = new CommonUtility();

        try
        {
            //if (HiddenFieldCheckYesClicked.Value == "Y")
            //    IsOwnershipChange = "Y";
            //else
            //    IsOwnershipChange = "N";

            //if (HiddenFieldCheckYesClicked.Value == "Y" && HiddenFieldConvertCustCode.Value == "")
            //{
            //    //GlobalValues.GetInstance().ShowMessage("Select previous customer code.", GlobalValues.MessageImageType.Warning);
            //    //object[] para = { "", clsGlobal.GetInstance().DefaultDeptID };
            //    //wCustomLookup oCustomLookup = new wCustomLookup("CustomerLookup", para);
            //    //oCustomLookup.WindowStartupLocation = WindowStartupLocation.CenterScreen;
            //    //oCustomLookup.LookupValueSelected += new wCustomLookup.LookupSelectedEventHandler(wLookup_Selected);
            //    //oCustomLookup.ShowDialog();
            //    //return;
            //}

            // Get the selected 
            //if (!string.IsNullOrWhiteSpace(HiddenFieldHasClickedCheckList.Value))
            //{
            //    string[] checklistid = HiddenFieldHasClickedCheckList.Value.Split(',');

            //    if (checklistid.Length != 0)
            //    {
            //        foreach (var item in checklistid)
            //        {
            //            items.Add(item);
            //        }
            //    }
            //}


             items = commonClient.GetChecklist(leadStageID);

            if (ViewState["LeadStageList"] == null)
            {
                args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
                lstLeadStage = obLeadStage.GetLeadStages(args);
                ViewState["LeadStageList"] = lstLeadStage;
            }
            else
                lstLeadStage = ViewState["LeadStageList"] as List<LeadStageDTO>;

            if (LeadStageId > 0)
            {
                currentLeadStage = lstLeadStage.FirstOrDefault(s => s.StageId == LeadStageId);
            }
            else
            {
                currentLeadStage = lstLeadStage.OrderBy(s => s.StageOrder).FirstOrDefault();

                if (currentLeadStage != null && currentLeadStage.StageName != "Customer")
                    LeadStageId = currentLeadStage.StageId;
            }

            if (currentLeadStage != null)
            {
                //Retreive the next stage in the order sequence
                nextLeadStage = lstLeadStage.FirstOrDefault(s => s.StageOrder == (currentLeadStage.StageOrder + 1));

                if (nextLeadStage != null)
                {
                    if (nextLeadStage.StageName != "Customer")
                    {
                        sSqls.Add(leadClient.UpdatePipelineStageQuery(int.Parse(LeadId), nextLeadStage.StageId));
                    }
                    else if (HiddenFieldConversionSent.Value == string.Empty)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "showconversionMessage();", true);
                        return false;
                    }
                }
            }
            if (ViewState["LeadStageList"] == null)
            {
                args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
                lstLeadStage = obLeadStage.GetLeadStages(args);
                ViewState["LeadStageList"] = lstLeadStage;
            }
            else
                lstLeadStage = ViewState["LeadStageList"] as List<LeadStageDTO>;

            if (LeadStageId > 0)
            {
                currentLeadStage = lstLeadStage.FirstOrDefault(s => s.StageId == LeadStageId);
            }
            else
            {
                currentLeadStage = lstLeadStage.OrderBy(s => s.StageOrder).FirstOrDefault();

                if (currentLeadStage != null && currentLeadStage.StageName != "Customer")
                    LeadStageId = currentLeadStage.StageId;
            }

            if (currentLeadStage != null)
            {
                //Retreive the next stage in the order sequence
                nextLeadStage = lstLeadStage.FirstOrDefault(s => s.StageOrder == (currentLeadStage.StageOrder + 1));
            }

            if (HiddenFieldConversionSent.Value == "1")
            {
                sSqls.Add(leadClient.UpdatePipelineStageQuery(int.Parse(LeadId), nextLeadStage.StageId));
            }

            // Delete checklist entries if already available (due to citrix problem etc.)
            //sSqls.Add(leadClient.DeleteQuery(int.Parse(LeadId), LeadStageId));

            SaveLeadCheckList(leadClient, checkListDTO, items, sSqls);

            List<LookupTableDTO> lstLookupradstatus = commonClient.GetLookupTables("ACST", args);
            List<LookupTableDTO> lstLookupradPriority = commonClient.GetLookupTables("ACPR", args);


            //insert details to activity
            ActivityClient activityClient = new ActivityClient();
            ActivityDTO activityDTO = new ActivityDTO();
            activityDTO.Subject = "Conversion";
            activityDTO.StartDate = DateTime.Now;
            activityDTO.EndDate = DateTime.Now;

            if (lstLookupradstatus != null && lstLookupradstatus.Count != 0)
                activityDTO.Status = lstLookupradstatus[0].TableCode;

            activityDTO.AssignedTo = UserSession.Instance.UserName;
            activityDTO.LeadID = int.Parse(LeadId);
            activityDTO.Comments = "Conversion from " + currentLeadStage.StageName + " to " + nextLeadStage.StageName;
            activityDTO.SentMail = "N";

            if (lstLookupradPriority != null && lstLookupradPriority.Count != 0)
                activityDTO.Priority = lstLookupradPriority[0].TableCode;

            activityDTO.SendReminder = "N";
            activityDTO.ReminderDate = DateTime.Now;
            activityDTO.ActivityType = "CONV";
            activityDTO.CreatedDate = DateTime.Now;
            activityDTO.CreatedBy = UserSession.Instance.UserName;
            activityDTO.CustomerCode = "";

            //activityDTO.LeadStageID = leadChecklist.PiepelineStageID;
            activityDTO.LeadStageID = nextLeadStage.StageId;

            LeadDTO LeadDTO = leadClient.GetLead(LeadId);
            activityDTO.CreatedDate = LeadDTO.CreatedDate;
            activityDTO.LastModifiedDate = LeadDTO.LastModifiedDate;

            //ActivityClient activityClient = new ActivityClient();
            sSqls.Add(activityClient.InsertConversionQuery(activityDTO));

            

            /*UPDATE LEAD START DATE*/
            //sSqls.Add(leadClient.GetLeadStartDate(LeadDTO.LeadID, DateTime.Now));

            /*OWERSHIP CHANGE*/
            //if (IsOwnershipChange == "Y")
            //{
            //    //sSqls.Add(leadClient.ChangeCustomerOwnershipQuery(LeadDTO.LeadID, IsOwnershipChange, HiddenFieldConvertCustCode.Value));
            //    sOwnershipEmailText = "<br /><br />This is a ownership change. \nPrevious Customer Code: " + HiddenFieldConvertCustCode.Value + "<br />Previous Customer Name: " + HiddenFieldConvertCustCode.Value + "<br />";
            //}

            //int noOfRecs = 0;
            bool noOfRecs = leadClient.ConvertLeadToCustomer(sSqls);

            if (noOfRecs)
            {
                HiddenFieldHasClickedCheckList.Value = "";
            }
            List<LeadAddressDTO> addressList = leadClient.GetLeadAddreses(int.Parse(LeadId));

            LeadAddressDTO leadAddressDTO = addressList.FirstOrDefault(a => a.AddressType == UserSession.Instance.AddressType);

            string address = string.Empty;

            if (leadAddressDTO != null)
            {
                string padding = "\t\t";
                address = leadAddressDTO.Address1 + " " + leadAddressDTO.Address2;
                address += "<br />" + padding + leadAddressDTO.City + " " + leadAddressDTO.State + " " + leadAddressDTO.PostCode;
                address += "<br />" + padding + leadAddressDTO.Country;
            }

            if (!string.IsNullOrEmpty(nextLeadStage.EmailAddress))
            {
                string subject = null;
                string message = null;
                if (noOfRecs && nextLeadStage.StageName.Equals("Customer"))
                {
                    subject = "Convert : '" + LeadDTO.LeadName + "' to " + nextLeadStage.StageName;
                    message = "The following Contact needs to be converted to a " + nextLeadStage.StageName + " in the Peercore system: <br /> <br />" +
                        " CRM Reference Number : " + LeadId.ToString() +
                        "<br /> <br /> Contact Name : " + LeadDTO.LeadName;
                    message += leadAddressDTO != null ? "<br /> <br /> Address: " + address : string.Empty;
                    message += sOwnershipEmailText;
                }
                else if (noOfRecs)
                {
                    subject = nextLeadStage.StageName;
                    message = "The following Contact is converted from " + currentLeadStage.StageName + " to a " + nextLeadStage.StageName +
                        " <br /><br /> CRM Reference Number : " + LeadId.ToString() +
                        "<br /><br /> Contact Name : " + LeadDTO.LeadName;
                    message += leadAddressDTO != null ? "<br /><br /> Address: " + address : string.Empty;
                    message += sOwnershipEmailText;
                }

                //Comment by Indunil, requested from bramen,12/12/2012
                //string frommail = ConfigurationManager.AppSettings["FromMail"].ToString();
                string frommail = UserSession.Instance.OriginatorEmail;
                commonUtility.SendMail(frommail, nextLeadStage.EmailAddress, subject, message);
            }
            GetLeadDetails(LeadId);

            if (nextLeadStage.StageName == "Customer")
            {
                //Disable Save button.
                buttonSave.Enabled = false;
                buttonClear.Enabled = false;
                buttonSave.CssClass = "dissavebtn";
                buttonClear.CssClass = "disclearbtn";

                HiddenFieldDisableAddContactDetails.Value = "1";
                HiddenFieldDisableAddContactPerson.Value = "1";
            }
        }

        catch (InvalidOperationException oException)
        {

        }
        return hasError;
    }

    private void SaveLeadCheckList(LeadClient leadClient, CheckListDTO checkListDTO, List<CheckListDTO> items, List<string> sSqls)
    {
        foreach (CheckListDTO chld in items)
        {
            if (chld.ChecklistID != 0)
            {
                checkListDTO.ChecklistID = chld.ChecklistID;
                checkListDTO.LeadId = int.Parse(LeadId);
                checkListDTO.PipelineStageID = LeadStageId;
                CommonClient commenClient = new CommonClient();
                sSqls.Add(commenClient.SaveQuery(checkListDTO));
            }
        }
    }




    private void BindDropDownControls()
    {
        List<KeyValuePair<string, string>> weeks = new List<KeyValuePair<string, string>>();
        weeks.Add(new KeyValuePair<string, string>("w", "Weekly"));
        weeks.Add(new KeyValuePair<string, string>("m", "Monthly"));
        weeks.Add(new KeyValuePair<string, string>("y", "Yearly"));

        foreach (KeyValuePair<string, string> item in weeks)
        {
            dropdownWeek.Items.Add(new ListItem(item.Value, item.Key));
        }
    }


    private void ClearControls()
    {
        div_message.InnerHtml = "";
        div_message.Attributes.Add("style", "display:none;padding:2px");

        div_info.Attributes.Add("style", "display:none");
        div_info.InnerHtml = "";

        txtCompany.Text = "";

        //LoadLookupData();
        txtDescription.Text = "";
        txtBusinessPotential.Text = "";
        txtRefBy.Text = "";

        txtAssignto.Text = UserSession.Instance.UserName.Trim();
        HiddenAssignTo.Value = UserSession.Instance.UserName.Trim();
        HiddenFieldIsContinueWithSave.Value = string.Empty;
        // Address Entry
        //ClearAddress();

        HiddenFieldContactPhone.Value = "";
        HiddenFieldContactFax.Value = "";
        HiddenFieldContactMobile.Value = "";
        HiddenFieldContactEmail.Value = "";
        HiddenFieldContactPrefMethod.Value = "";
        HiddenFieldCustAddress.Value = "";
        HiddenFieldCustCity.Value = "";
        HiddenFieldCustPostcode.Value = "";
        HiddenFieldCustState.Value = "";

        LeadId = null;
        HiddenFieldLeadId.Value = "0";
        rRating.Value = 0;
        LeadEntry = new KeyValuePair<string, string>();

        dropdownMarket.SelectedIndex = 0;
        DropDownListSource.SelectedIndex = 0;
        dropdownChannel.SelectedIndex = 0;
        LeadStageId = 0;

        if (ViewState[CommonUtility.SAVE_OBJECT] != null)
        {
            LeadDTO lead_save = ViewState[CommonUtility.SAVE_OBJECT] as LeadDTO;

            if (lead_save != null)
            {
                lead_save.LeadID = 0;
                lead_save.Company = string.Empty;
                lead_save.Description = string.Empty;
                lead_save.BusinessPotential = string.Empty;
                lead_save.ReferredBy = string.Empty;
                lead_save.Rating = 0;
                lead_save.LeadStageID = LeadStageId;
                lead_save.Industry = dropdownMarket.SelectedValue;
                lead_save.LeadSource = DropDownListSource.SelectedValue;
                lead_save.Business = dropdownChannel.SelectedValue;
            }
        }

        OnLeadIdChanged();

        SetLeadStage();

        SetOpportunityConstraints(LeadStageId);

        //AddPageView(RadTabStrip2.FindTabByText("Contact Details"), true);

        //Disable Buttons.
        //contactdetails = (usercontrols_contactdetails)LoadControl(ConfigUtil.ApplicationPath + "usercontrols/contactdetails.ascx");
        //contactdetails.EnableDisableAddButton(false);

    }

    private void OnLeadIdChanged()
    {
        if (!string.IsNullOrWhiteSpace(LeadId))
        {
            //EnableTabs();
            //EnableTabButtons();
            buttonConvert.Enabled = true;
            buttonActiveDeactive.Enabled = true;
        }
        else
        {
            //DisableTabs();
            //DisableTabButtons();
            buttonConvert.Enabled = false;
            buttonActiveDeactive.Enabled = false;
        }
    }

    //private void EnableTabs()
    //{
    //    //tabActivity.IsEnabled = true;
    //    //tabContactPerson.IsEnabled = true;
    //    //tabDocument.IsEnabled = true;
    //    //tabOpportunities.IsEnabled = true;
    //    //tabQuotations.IsEnabled = true;
    //    //tabEmails.IsEnabled = true;


    //    RadTabStrip2.FindTabByText("Contact Details").Enabled = true;
    //    RadTabStrip2.FindTabByText("Contact Person").Enabled = true;
    //    RadTabStrip2.FindTabByText("Activity").Enabled = true;
    //    RadTabStrip2.FindTabByText("Documents").Enabled = true;
    //    RadTabStrip2.FindTabByText("Emails").Enabled = true;
    //}

    //private void DisableTabs()
    //{
    //    //RadTabStrip2.FindTabByText("Contact Details").Enabled=false;
    //    RadTabStrip2.FindTabByText("Contact Person").Enabled = false;
    //    RadTabStrip2.FindTabByText("Activity").Enabled = false;
    //    RadTabStrip2.FindTabByText("Documents").Enabled = false;
    //    RadTabStrip2.FindTabByText("Emails").Enabled = false;
    //}

    private void SetOpportunityConstraints(int leadStageId)
    {
        List<LeadStageDTO> lstLeadStage = new List<LeadStageDTO>();
        LeadStageDTO currentLeadStage = new LeadStageDTO();
        LeadStageDTO nextLeadStage = new LeadStageDTO();
        LeadDTO lead_deatail = null;

        if (ViewState["LeadStageList"] != null)
        {
            lstLeadStage = ViewState["LeadStageList"] as List<LeadStageDTO>;

            currentLeadStage = lstLeadStage.FirstOrDefault(s => s.StageId == LeadStageId);

            if (!currentLeadStage.OpportunityAllowed)
            {
                //RadTabStrip2.FindTabByText("Opportunities").Visible = false;
            }
            else
            {
                //RadTabStrip2.FindTabByText("Opportunities").Visible = true;

                lead_deatail = ViewState[CommonUtility.SAVE_OBJECT] as LeadDTO;
                nextLeadStage = lstLeadStage.FirstOrDefault(s => s.StageOrder == (currentLeadStage.StageOrder + 1));

                //oppotunity = (usercontrols_customer_oppotunity)LoadControl(ConfigUtil.ApplicationPath + "usercontrols/customer_oppotunity.ascx");
               
               HiddenFieldMaxOpportunities.Value = currentLeadStage.MaxOpportunities.ToString();

                //Comment by Indunil,since grid bind throught web service, have maintain by hiddenfield the flag.
               //oppotunity.EnableDisableAddButton(currentLeadStage.MaxOpportunities, lead_deatail != null ? true : false,
               //        lead_deatail != null ? lead_deatail.DelFalg : string.Empty, nextLeadStage != null ? true : false);

               if (lead_deatail != null && lead_deatail.DelFalg != "Y" && nextLeadStage!=null)
                   HiddenFieldDeactivateoppotunity.Value = "0";
               else
                   HiddenFieldDeactivateoppotunity.Value = "1";
            }
        }
    }

    //private void SaveLeadEntry1()
    //{
    //    LeadDTO lead_save = null;
    //    LeadClient leadClient = new LeadClient();
    //    CommonUtility commonUtility = new CommonUtility();
    //    LeadClient lead = new LeadClient();
    //    int leadid = 0;

    //    try
    //    {
    //        //LeadDTO lead_details = lead.GetLead(LeadEntry.Key);

    //        if (ViewState[CommonUtility.SAVE_OBJECT] != null)
    //            lead_save = ViewState[CommonUtility.SAVE_OBJECT] as LeadDTO;

    //        if (lead_save == null)
    //        {
    //            //New Creation.
    //            lead_save = new LeadDTO();
    //        }

    //        //Setting Name.
    //        if (txtName.Text.Length != 0)
    //        {
    //            lead_save.LeadName =HttpUtility.HtmlEncode(txtName.Text.Trim());
    //        }

    //        //Setting Company.
    //        if (txtCompany.Text.Length != 0)
    //        {
    //            lead_save.Company =HttpUtility.HtmlEncode(txtCompany.Text.Trim());
    //        }

    //        //Setting Market.
    //        if (dropdownMarket.SelectedIndex != -1)
    //        {
    //            lead_save.Industry = dropdownMarket.SelectedValue.Trim();
    //        }

    //        //Setting Channel.
    //        if (dropdownChannel.SelectedIndex != -1)
    //        {
    //            lead_save.Business = dropdownChannel.SelectedValue.Trim();
    //        }

    //        //Setting Potential Litres            
    //        if (dropdownWeek.SelectedIndex != -1)
    //        {
    //            lead_save.LitersBy = dropdownWeek.SelectedValue != null ? dropdownWeek.SelectedValue.ToString() : "W"; ;
    //        }

    //        //Setting PotentialLiters
    //        lead_save.PotentialLiters = TextBoxLiters.Text.Length != 0 ? Convert.ToDouble(TextBoxLiters.Text.ToString().Trim()) : 0;

    //        //Setting Description.
    //        lead_save.Description = HttpUtility.HtmlEncode(txtDescription.Text);

    //        //Setting Rating.
    //        lead_save.Rating = rRating.Value != null ? Convert.ToInt32(rRating.Value) : 0;

    //        //Setting Business Potential
    //        lead_save.BusinessPotential = HttpUtility.HtmlEncode(txtBusinessPotential.Text);

    //        //Setting Referred By.
    //        lead_save.ReferredBy = HttpUtility.HtmlEncode(txtRefBy.Text);

    //        //Setting Source.
    //        if (DropDownListSource.SelectedIndex != -1)
    //            lead_save.LeadSource = DropDownListSource.SelectedValue != null ? DropDownListSource.SelectedValue.ToString() : "N";

    //        //Setting Assign To.
    //        lead_save.Originator = HiddenAssignTo.Value;

    //        //Setting BDM Group.
    //        lead_save.RepGroupID = ((!string.IsNullOrWhiteSpace(HiddenFieldBDMGroup.Value)) ? Convert.ToInt32(HiddenFieldBDMGroup.Value) : 0);

    //        //Setting LeadStage ID
    //        lead_save.LeadStageID = LeadStageId;

    //        //Setting Contact Details Properties.
    //        #region Contact Details Properties

    //        if (!string.IsNullOrWhiteSpace(HiddenFieldContactPhone.Value))
    //            lead_save.Telephone = HiddenFieldContactPhone.Value;

    //        if (!string.IsNullOrWhiteSpace(HiddenFieldContactMobile.Value))
    //            lead_save.Mobile = HiddenFieldContactMobile.Value;

    //        if (!string.IsNullOrWhiteSpace(HiddenFieldContactFax.Value))
    //            lead_save.Fax = HiddenFieldContactFax.Value;

    //        if (!string.IsNullOrWhiteSpace(HiddenFieldContactWebSite.Value))
    //            lead_save.Website = HiddenFieldContactWebSite.Value;

    //        if (!string.IsNullOrWhiteSpace(HiddenFieldContactEmail.Value))
    //            lead_save.EmailAddress = HiddenFieldContactEmail.Value;

    //        if (!string.IsNullOrWhiteSpace(HiddenFieldContactPrefMethod.Value))
    //            lead_save.PreferredContact = HiddenFieldContactPrefMethod.Value != null ? HiddenFieldContactPrefMethod.Value.Trim() : "N";

    //        #endregion

    //        lead_save.CreatedBy = UserSession.Instance.UserName;
    //        lead_save.CreatedDate = DateTime.Now.ToUniversalTime();
    //        lead_save.LastModifiedBy = UserSession.Instance.UserName;
    //        lead_save.LastModifiedDate = DateTime.Now.ToUniversalTime();
    //        lead_save.StartDate = DateTime.Now.ToUniversalTime();

    //        ArgsDTO args = new ArgsDTO();
    //        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;

    //        if (lead_save.LeadID == 0 && HiddenFieldIsContinueWithSave.Value != "1")
    //        {
    //            LeadCustomerClient leadCustomerClient = new LeadCustomerClient();
    //            List<LeadCustomerDTO> existingLead = new List<LeadCustomerDTO>();
    //            existingLead = leadCustomerClient.LeadExist(lead_save, args);

    //            if (existingLead.Count > 0)
    //            {
    //                //Show Dialog box with existing records.
    //                DisplayDuplicateRecords(existingLead);
    //                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "showModal('#MainContent_div_duplicaterecord');", true);
    //                return;
    //            }
    //        }

    //        //Save Object
    //        if (leadClient.SaveLead(out leadid, lead_save, args))
    //        {
    //            //Succesfully saved.
    //            div_duplicaterecord.Attributes.Add("style", "display:none;padding:2px");
    //            div_duplicaterecord.InnerHtml = "";

    //            div_message.Attributes.Add("style", "display:block;padding:2px");
    //            div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved !");

    //            div_info.Attributes.Add("style", "display:none");
    //            div_info.InnerHtml = "";

    //            if (leadid == 0 && !UserSession.Instance.UserName.Equals(HiddenAssignTo.Value))
    //            {
    //                CreateMailAndActivity(leadid);
    //            }
    //            if ((Request.QueryString["leadid"] == null || Request.QueryString["leadid"] == string.Empty ) &&
    //                (LeadId == null ))
    //            {
    //                LeadId = leadid.ToString();
    //                ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "loadnewSave();", true);
    //                PageLoad();
    //            }
    //            else
    //            {
    //                GetLeadDetails(leadid.ToString());
    //            }
                
    //            /*if (Request.QueryString["leadid"] == null || Request.QueryString["leadid"] == string.Empty
    //                    || Request.QueryString["leadid"] == "0")
    //            {
    //                Response.Redirect("leadentry.aspx?leadid=" + leadid);
    //            }*/
    //            buttonConvert.CssClass = "openentry";
    //            buttonConvert.Enabled = true;
    //            buttonActiveDeactive.Enabled = true;
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        //Error Occured.
    //        //commonUtility.ErrorMaintain(ex);

    //        //div_info.Attributes.Add("style", "display:block");
    //        //div_message.Attributes.Add("style", "display:none");
    //        //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
    //    }
    //}

    

    private int NavigationCount(int startIndex, int lastPageNumber, int currentIndex)
    {
        if ((lastPageNumber - 1) > startIndex)
        {
            //Set the initial value.(eg : 0+5)
            int index = startIndex + 6;

            //This is for first time navigation, its display only 5 slots.
            if (currentIndex == 0)
            {
                if (lastPageNumber > 5)
                    return 6;
                else
                    return lastPageNumber + 1;

            }
            else
            {
                if (lastPageNumber > index)
                    return index + 1;
                else
                    return lastPageNumber + 1;
            }
        }
        else
            return lastPageNumber + 1;
    }

    private int GetInitialValue(int startIndex, int lastPageNumber)
    {
        if ((lastPageNumber - 1) > startIndex)
        {
            //Set the initial value.(eg : 0+5)
            int index = startIndex + 6;

            //Maximum page number shold be 10, and lastpagenumber should ne greather than 10, otherwise show the whole 10 pages
            if (index >= 11 && lastPageNumber > 10)
            {
                int balaneIndex = index - 10;
                return balaneIndex + 1;
            }
            else
                return 1;
        }
        else
        {
            //When last page number less than 10.
            if (lastPageNumber < 10)
                return 1;
            else
                return startIndex - 4;
        }
    }

    
    private void GetPrevCustCodeForCustomerEntry()
    {
        //Setting Args
        ArgsDTO args = new ArgsDTO();
        args.StartIndex = ConfigUtil.StartIndex;
        args.RowCount = 10;
        int currentIndex = 0;
        StringBuilder txt = new StringBuilder();

        CommonClient commonClient = new CommonClient();
        List<object> para = new List<object>();
        para.Add("");
        para.Add(UserSession.Instance.DefaultDepartmentId);
        para.Add("");
        para.Add("a.name");

        if (Request.QueryString["si"] != null)
        {
            args.StartIndex = int.Parse(Request.QueryString["si"].ToString());
            currentIndex = int.Parse(Request.QueryString["si"].ToString());
        }

        if (Request.QueryString["rc"] != null)
            args.RowCount = int.Parse(Request.QueryString["rc"].ToString());

        List<LookupDTO> lookupList = commonClient.GetLookups("CustomerLookup", "", para, args, true);

        if (lookupList.Count != 0)
        {
            txt.Append("<table border=\"0\" id=\"customerTable\" width=\"417px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");

            int rowcount = lookupList[0].rowCount;

            for (int i = 0; i < lookupList.Count; i++)
            {
                txt.Append("<tr>");
                txt.Append("<td style=\"padding-left:5px;\" class=\"tblborder\"> <input type=\"radio\" name=\"id_prevcustocde\" value=" + lookupList[i].Code + " id=\"id_prevcustocde\"/>  </td>");
                txt.Append("<td class=\"tblborder\" align=\"left\">" + lookupList[i].Code + "</td>");
                txt.Append("<td class=\"tblborder\" align=\"left\">" + lookupList[i].Description + "</td>");
                txt.Append("</tr>");
            }
            txt.Append("</table>");

            txt.Append("<br />");

            txt.Append("<table border=\"1\" id=\"customerTable\" width=\"300px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
            txt.Append("<tr>");

            txt.Append(CreatePaginator(currentIndex, rowcount));
                    

            txt.Append("</tr>");
            txt.Append("</table>");

            txt.Append("<script type='text/javascript'>");
            txt.Append("var count=0;");
            txt.Append("$('a#id_navigation').click(function(){");
            txt.Append("$.fancybox.close();");
            txt.Append("$('a#id_custcode').click();");
            txt.Append("});");

            //Radio button click.
            txt.Append("if(count==0){");

            txt.Append("$('input[name=id_prevcustocde]').click(function(){");
            txt.Append("var ischecked= $('input[name=id_prevcustocde]').is(':checked');");
            txt.Append("if(ischecked==true){");
            txt.Append("$('#MainContent_HiddenFieldConvertCustCode').val($('input[name=id_prevcustocde]').val());");
            txt.Append("$('div#jqi_state_state0 #search_txt').val($('input[name=id_prevcustocde]').val());");
            txt.Append("$.fancybox.close();");
            txt.Append("}");
            txt.Append("});");
            txt.Append("count++;");
            txt.Append("}");

            txt.Append("</script>");
        }
        div_prevcustcode.InnerHtml=txt.ToString();
    }

    private string CreatePaginator(int currentPageNumber, int totrowcount)
    {
        StringBuilder sb = new StringBuilder();
        CommonUtility commonUtility = new CommonUtility();

        #region Navigation
        int rowCount=ConfigUtil.RowCount;
        int lastPageNumber = commonUtility.PageCount(totrowcount, rowCount);

        string new_url = "";


        sb.Append("<div class=\"paging\">");
        sb.Append("<ul>");

        if (lastPageNumber > 1)
        {
            new_url = "getAllPrevcustcode('lead_customer/process_forms/processmaster.aspx?fm=leadentry&type=query&querytype=prevcustcode&si=" + (currentPageNumber - 1) + "&rc=" + rowCount + "','');";

            if (currentPageNumber == 0)
                sb.Append("<li><a title='Previous' class=\"forw_link\">«</a></li>");
            else
                sb.Append("<li><a title='Previous' class=\"forw_link\" href=\"javascript:void(0);\" onclick=\"" + new_url.Trim() + "\">«</a></li>");
        }

        for (int i = GetInitialValue(currentPageNumber, lastPageNumber); i < NavigationCount(currentPageNumber, lastPageNumber, currentPageNumber); i++)
        {
            new_url = "getAllPrevcustcode('lead_customer/process_forms/processmaster.aspx?fm=leadentry&type=query&querytype=prevcustcode&si=" + (i - 1) + "&rc=" + rowCount + "','');";

            if (i == (currentPageNumber + 1))
                sb.Append("<li class=\"current\" style=\"background-color:#FFFFFF\">" + i.ToString() + "</li>");
            else
                sb.Append("<li><a href=\"javascript:void(0);\"  onclick=\"" + new_url.Trim() + "\" >" + i.ToString() + "</a></li>");
        }

        if (lastPageNumber > 1)
        {
            new_url = "getAllPrevcustcode('lead_customer/process_forms/processmaster.aspx?fm=leadentry&type=query&querytype=prevcustcode&si=" + (currentPageNumber + 1) + "&rc=" + rowCount + "','');";

            if ((currentPageNumber + 1) == lastPageNumber)
                sb.Append("<li><a title='Next' class=\"back_link\">»</a></li>");
            else
                sb.Append("<li><a title='Next' class=\"back_link\" href=\"javascript:void(0);\" onclick=\"" + new_url.Trim() + "\">»</a></li>");
        }

        sb.Append("</ul>");        
        sb.Append("</div>");
        //sb.Append("<div class=\"clear\"></div>");
        #endregion

        return sb.ToString();
    }

    private string SetAutocomplete()
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script type=\"text/javascript\">");
        sb.Append("     $(function () {");
        sb.Append("         $(\".tb\").autocomplete({");
        sb.Append("             source: function (request, response) {");
        sb.Append("                 $.ajax({");
        sb.Append("                     type: \"POST\",");
        sb.Append("                     contentType: \"application/json; charset=utf-8\",");
        sb.Append("                     url: \"../../Service/pipelines_stage/pipelines_stage_service.asmx/GetLeadNameList\",");
        sb.Append("                     data: \"{'name':'\" + request.term + \"'}\",");
        sb.Append("                     dataType: \"json\",");
        sb.Append("                     async: true,");
        sb.Append("                     success: function (data) {");
        //sb.Append("                         response(data.d);");

        sb.Append(" response($.map(data.d, function(item) { ");

        sb.Append("        return {");

        sb.Append("            label: item.LeadName,");
        sb.Append("            value: item.LeadID");
        sb.Append("        }");
        sb.Append("    }));");

        sb.Append("                     },");
        sb.Append("                     error: function (result) {");
        sb.Append("                     }");
        sb.Append("                 });");
        sb.Append("             },");
        sb.Append("             minLength: 2,");
        sb.Append("             select: function (event, ui ) {");
        sb.Append("                  if (ui.item) { window.location = 'leadentry.aspx?leadid='+ui.item.value+'&ty=Lead&fm=lead';  }       ");
        sb.Append("             }");
        sb.Append("         });");
        sb.Append("     });");
        sb.Append("</script>");
        sb.Append("");

        return sb.ToString();
    }
    #endregion
}