﻿using CRMServiceReference;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class lead_customer_transaction_customer_bulk_transfer : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
	{
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
                Master.SetBreadCrumb("Outlets Bulk Transfer ", "#", "");

                LoadRoutes();
            }
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    private void LoadRoutes()
    {
        #region Route List

        MasterClient masterlient = new MasterClient();
        ArgsModel args1 = new ArgsModel();
        args1.StartIndex = 1;
        args1.RowCount = 10000;
        args1.OrderBy = " route_master_name asc";

        List<RouteMasterModel> objRouteList = new List<RouteMasterModel>();
        objRouteList = masterlient.GetAllRoutesMaster(args1);

        if (objRouteList.Count != 0)
        {
            ddlNewRoutes.Items.Clear();
            ddlNewRoutes.Items.Add(new ListItem("- Select Route -", "0"));
            foreach (RouteMasterModel item in objRouteList)
            {
                var strLength = (item.RouteMasterCode == null) ? 0 : item.RouteMasterCode.Length;
                string result = (item.RouteMasterCode == null) ? "" : item.RouteMasterCode.PadRight(strLength + 20).Substring(0, strLength + 20);

                ddlNewRoutes.Items.Add(new ListItem(item.RouteMasterName + "  |  " + result, item.RouteMasterId.ToString()));
            }

            ddlNewRoutes.Enabled = true;
        }

        #endregion
    }

    protected void btnTransfer_Click(object sender, EventArgs e)
	{
        HiddenField_logURLStatus.Value = "D";
        linkLog.Visible = false;

        int ret = 0;

        try
        {
            if (ddlNewRoutes.SelectedIndex == 0)
			{
                Response.Write("<script>alert('Please select new route for outlets transfer.');</script>");
            }
            else if (!xmlUpload.HasFile)
			{
                Response.Write("<script>alert('Please select outlet list excel file.');</script>");
            }
            else 
            {
                string FileName = Path.GetFileName(xmlUpload.PostedFile.FileName);
                string Extension = Path.GetExtension(xmlUpload.PostedFile.FileName);
                string FolderPath = HttpContext.Current.Request.PhysicalApplicationPath + "\\uploads"; string[] validFileTypes = { ".xls", ".xlsx" };

                string FilePath = FolderPath + "\\" + FileName;

                bool isValidType = false;
                isValidType = validFileTypes.Contains(Extension);

                
                if (!isValidType)
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileValidator()", true);
                }
                else
                {
                    xmlUpload.SaveAs(FilePath);
                    //Task<int> t = Task.Run(() => SaveFile(FilePath));

                    ////if (t.IsCompleted)
                    ////{
                    //SalesClient salesClient = new SalesClient();
                    //string UserName = UserSession.Instance.UserName;
                    //ret = salesClient.UploadSalesTargets(FilePath, Extension, UserName);
                    ////}
                    
                    string UserName = UserSession.Instance.UserName;
                    ret = UploadOutletBulkTransfer(FilePath, UserName);

                    if (ret > 0)
                    {
                        Response.Write("<script>alert('Successfully " + ret + " Records Uploaded!');</script>");
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileFormatValidator()", true);
                    }
                }
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

	private int SaveFile(string filePath)
	{
		if (!System.IO.File.Exists(filePath))
		{
			try
			{
				xmlUpload.SaveAs(filePath);
				return 1;
			}
			catch
			{
				return 0;
			}
		}
		else
		{
			return 0;
		}
	}

    public int UploadOutletBulkTransfer(string filePath, string UserName)
    {
        try
        {
            string logPath = ConfigurationManager.AppSettings["LogPath"];
            string logUrl = ConfigurationManager.AppSettings["LogURL"];

            DataTable dt = new DataTable();
            DataTable dtErrLog = new DataTable();

            DataColumn dtColumnErrLog;
            DataRow myDataRowErrLog;

            // Create id column  
            dtColumnErrLog = new DataColumn();
            dtColumnErrLog.DataType = typeof(String);
            dtColumnErrLog.ColumnName = "ErrorMessage";
            dtColumnErrLog.Caption = "ErrorMessage";
            dtColumnErrLog.ReadOnly = false;
            dtColumnErrLog.Unique = false;
            // Add column to the DataColumnCollection.  
            dtErrLog.Columns.Add(dtColumnErrLog);

            using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(filePath, false))
            {
                WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                string relationshipId = sheets.First().Id.Value;
                WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                Worksheet workSheet = worksheetPart.Worksheet;
                SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                IEnumerable<Row> rows = sheetData.Descendants<Row>();

                foreach (Cell cell in rows.ElementAt(0))
                {
                    dt.Columns.Add(GetCellValue(spreadSheetDocument, cell).Trim());
                }

                foreach (Row row in rows) //this will also include your header row...
                {
                    DataRow tempRow = dt.NewRow();
                    try
                    {
                        for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
                        {
                            try
                            {
                                tempRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));
                            }
                            catch { }
                        }

                        if (tempRow[0].ToString() != "")
                            dt.Rows.Add(tempRow);
                    }
                    catch { }
                }
            }

            dt.Rows.RemoveAt(0);
            dt.AcceptChanges();

            int retResult = 0;

            CommonClient commonClient = new CommonClient();

            try
            {
                foreach (DataRow dr in dt.Rows)
                {
                    if (!commonClient.UploadOutletBulkTransfer(UserName, int.Parse(ddlNewRoutes.SelectedValue), dr["CustomerCode"].ToString(), dr["CustomerName"].ToString()))
                    {
                        // Error Log  
                        myDataRowErrLog = dtErrLog.NewRow();
                        myDataRowErrLog["ErrorMessage"] = dr["CustomerCode"].ToString() + " - " + dr["CustomerName"].ToString();
                        dtErrLog.Rows.Add(myDataRowErrLog);
                    }

                    retResult++;
                }

                try
                {
                    WriteAssetAssignLog(dtErrLog, logUrl, logPath);
                }
                catch { }
            }
            catch
            {
                retResult = 0;
                Response.Write("<script>alert('Upload excel file error');</script>");
            }

            return retResult;
        }
        catch (Exception ex)
        {
            return 0;
        }
    }

    public static string GetCellValue(SpreadsheetDocument document, Cell cell)
    {
        SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
        string value = cell.CellValue.InnerXml;

        if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
        {
            return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
        }
        else
        {
            return value;
        }
    }

    public void WriteAssetAssignLog(DataTable strLog, string logURL, string logPath)
    {
        try
        {
            string logFilePath = logPath + "OutletBulkTransfer-" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "." + "txt";
            string logURLPath = logURL + "OutletBulkTransfer-" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "." + "txt";

            FileInfo logFileInfo = new FileInfo(logFilePath);
            DirectoryInfo logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirInfo.Exists) logDirInfo.Create();
            File.WriteAllText(logFilePath, String.Empty);

            if (strLog.Rows.Count > 0)
            {
                using (FileStream fileStream = new FileStream(logFilePath, FileMode.Append))
                {
                    using (StreamWriter log = new StreamWriter(fileStream))
                    {
                        log.Write("Log Entry : Outlet Bulk Transfer ");
                        log.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                            DateTime.Now.ToLongDateString());
                        log.WriteLine("------------");
                        log.WriteLine(strLog.Rows.Count + " Records Not Inserted.");
                        log.WriteLine("------------");
                        foreach (DataRow row in strLog.Rows)
                        {
                            string errMsg = row["ErrorMessage"].ToString();
                            log.WriteLine("{0}", errMsg);
                        }
                    }
                }

                HiddenField_logURL.Value = logURLPath;
                HiddenField_logURLStatus.Value = "A";

                linkLog.NavigateUrl = logURLPath;
                linkLog.Visible = true;
            }
            else
            {
                HiddenField_logURLStatus.Value = "D";
                linkLog.Visible = false;
            }
        }
        catch (Exception ex)
        {
            CreateErrorLog(ex.Message);
        }
    }

    public void CreateErrorLog(string description)
    {
        string filename = "BulkOutletTransfer_" + DateTime.Now.ToString("yyyyMMdd");
        var path = System.IO.Path.GetDirectoryName(
            System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);
        string filePath = ConfigurationManager.AppSettings["CreateLoginLogPath"].ToString() + filename + ".txt";

        try
        {
            // Check if file already exists. If yes, delete it. 
            using (StreamWriter sw = new StreamWriter(filePath, true))
            {
                string sr = DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss:fff tt") + "  "  + description + Environment.NewLine;
                sw.Write(sr);
            }
        }
        catch (Exception Ex)
        {
            Console.WriteLine(Ex.ToString());
        }
    }
}
	