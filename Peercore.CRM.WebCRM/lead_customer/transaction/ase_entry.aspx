﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="ase_entry.aspx.cs" Inherits="lead_customer_transaction_ase_entry" %>

<%@ MasterType TypeName="SiteMaster" %>
<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ Register Src="~/usercontrols/entrypage_tabs.ascx" TagPrefix="ucl2" TagName="tabentry" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <script type="text/javascript" src="../../assets/scripts/jquery.maskedinput.js"></script>
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnablePageMethods="true">
    </telerik:RadScriptManager>
    <asp:HiddenField ID="HiddenFieldCustAddress1" runat="server" />
    <asp:HiddenField ID="HiddenFieldCustAddress2" runat="server" />
    <asp:HiddenField ID="HiddenFieldCustCity" runat="server" />
    <asp:HiddenField ID="HiddenFieldCustState" runat="server" />
    <asp:HiddenField ID="HiddenFieldCustPostcode" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactPhone" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactMobile" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactFax" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactWebSite" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactEmail" runat="server" />
    <asp:HiddenField ID="HiddenFieldContactPrefMethod" runat="server" />
    <asp:HiddenField ID="HiddenCustomerCode" runat="server" />
    <asp:HiddenField ID="HiddenFieldFormType" runat="server" />
    <asp:HiddenField ID="HiddenFieldSaveClickOk" runat="server" />
    <asp:HiddenField ID="HiddenFieldTabIndex" runat="server" Value="1" />
    <asp:HiddenField ID="HiddenFieldIsDistributorSaved" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedDistId" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedOriginatorId" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedOriginator" runat="server" />
    <asp:HiddenField ID="HiddenFieldLoadUserType" runat="server" />
    <div id="customermodalWindow" style="display: none">
        <div id="div_customerconfirm_message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>
    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <ucl:buttonbar ID="buttonbar" runat="server" />
                </div>
            </div>
            <div class="toolbar_right" id="div1">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 35%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
            <div class="back"></div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none">
        </div>
        <div id="modalWindowOriginators" style="display: none">
        </div>
        <div id="div_info" runat="server" style="display: none">
        </div>
        <div id="div_promt" style="display: none">
        </div>
        <div>
            <%--layout new format--%>
            <div class="formleft">
                <div class="formtextdiv">
                    Type
                </div>
                <div class="formdetaildiv_right">
                    :
                    <%-- <asp:DropDownList ID="DropDownListType" runat="server" CssClass="input-large1">
                        <asp:ListItem Value="DIST" Text="Distributor" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="TME" Text="Trade Marketing Executive"></asp:ListItem>
                    </asp:DropDownList>--%>
                    <asp:Label ID="LableType" runat="server" CssClass="input-large1"></asp:Label>
                    <a href="#" id="id_select_user" runat="server">
                        <img id="img1" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" /></a>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Name</div>
                <div class="formdetaildiv_right">
                    <span class="wosub mand">:
                        <asp:TextBox ID="txtName" runat="server" CssClass="input-large1"></asp:TextBox><span
                            style="color: Red">*</span></span></div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    User Name</div>
                <div class="formdetaildiv_right">
                    <span class="wosub mand">:
                        <asp:TextBox ID="TxtUserName" runat="server" CssClass="input-large1"></asp:TextBox><span
                            style="color: Red">*</span></span></div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Password</div>
                <div class="formdetaildiv_right">
                    <span class="wosub mand">:
                        <asp:TextBox ID="TxtPassword" runat="server" CssClass="input-large1" TextMode="Password"></asp:TextBox><span
                            style="color: Red">*</span></span></div>
                <div class="clearall">
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Verify Password</div>
                <div class="formdetaildiv_right">
                    <span class="wosub mand">:
                        <asp:TextBox ID="TxtVerifyPassword" runat="server" CssClass="input-large1" TextMode="Password"></asp:TextBox><span
                            style="color: Red">*</span></span></div>
                <div class="clearall">
                </div>
                <div class="clearall">
                </div>
                <%--                <div class="formtextdiv">
                    Prefix</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="TxtPrefixCode" runat="server" CssClass="input-large1"></asp:TextBox></div>--%>
                <%--<div class="formtextdiv" id="dh" runat="server">
                    Default Holiday </div>
                <div class="formdetaildiv_right" id="dh2" runat="server">
                    :
                    <asp:DropDownList ID="DropDownListHoliday" runat="server" CssClass="input-large1">
                        <asp:ListItem Value="Sunday" Text="Sunday" Selected="true"></asp:ListItem>
                        <asp:ListItem Value="Monday" Text="Monday"></asp:ListItem>
                        <asp:ListItem Value="Tuesday" Text="Tuesday"></asp:ListItem>
                        <asp:ListItem Value="Wednesday" Text="Wednesday"></asp:ListItem>
                        <asp:ListItem Value="Thursday" Text="Thursday"></asp:ListItem>
                        <asp:ListItem Value="Friday" Text="Friday"></asp:ListItem>
                        <asp:ListItem Value="Saturday" Text="Saturday"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="clearall">
                </div>--%>
            </div>
            <div class="formright">
                <div class="formtextdiv">
                    Address</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="TxtAddress1" runat="server" CssClass="input-large1"></asp:TextBox></div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Telephone</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="TxtTelephone" runat="server" CssClass="input-large1-phone"></asp:TextBox></div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Mobile</div>
                <div class="formdetaildiv_right">
                    :
                    <asp:TextBox ID="txtMobile" runat="server" MaxLength="14" CssClass="input-large1-mobile"></asp:TextBox></div>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Email</div>
                <div class="formdetaildiv_right">
                    <span class="wosub mand">:
                        <asp:TextBox ID="TextBoxEmail" runat="server" CssClass="input-large1"></asp:TextBox><span
                            style="color: Red">*</span></span></div>
                <div class="clearall">
                </div>
                <div class="formtextdiv" id="dh" runat="server" style="display: none">
                    Default Holiday
                </div>
                <div class="formdetaildiv_right" id="dh2" runat="server" style="display: none">
                    :
                    <asp:DropDownList ID="DropDownListHoliday" runat="server" CssClass="input-large1">
                        <asp:ListItem Value="Sunday" Text="Sunday" Selected="true"></asp:ListItem>
                        <asp:ListItem Value="Monday" Text="Monday"></asp:ListItem>
                        <asp:ListItem Value="Tuesday" Text="Tuesday"></asp:ListItem>
                        <asp:ListItem Value="Wednesday" Text="Wednesday"></asp:ListItem>
                        <asp:ListItem Value="Thursday" Text="Thursday"></asp:ListItem>
                        <asp:ListItem Value="Friday" Text="Friday"></asp:ListItem>
                        <asp:ListItem Value="Saturday" Text="Saturday"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="clearall">
                </div>
                <div class="formtextdiv" id="dh3" runat="server">
                    Default Holidays
                </div>
                <div class="formdetaildiv_right" id="dh4" runat="server">
                    <asp:CheckBoxList ID="chklstHolidays" runat="server" RepeatDirection="Horizontal"
                        CellSpacing="5">
                        <asp:ListItem Value="Sunday" Text="Sun"></asp:ListItem>
                        <asp:ListItem Value="Monday" Text="Mon"></asp:ListItem>
                        <asp:ListItem Value="Tuesday" Text="Tue"></asp:ListItem>
                        <asp:ListItem Value="Wednesday" Text="Wed"></asp:ListItem>
                        <asp:ListItem Value="Thursday" Text="Thu"></asp:ListItem>
                        <asp:ListItem Value="Friday" Text="Fri"></asp:ListItem>
                        <asp:ListItem Value="Saturday" Text="Sat"></asp:ListItem>
                    </asp:CheckBoxList>
                </div>
                <div class="clearall">
                </div>
                <%--                <div class="formtextdiv">
                    Is In TLP</div>
                <div class="formdetaildiv">
                    :
                    <asp:RadioButton ID="radioyes" Text=" Yes" runat="server" GroupName="istlp" />&nbsp;&nbsp;
                    <asp:RadioButton ID="radiono" runat="server" Text=" No" GroupName="istlp" />
                </div>
                <div class="clearall">
                </div>--%>
            </div>
            <div class="clearall">
            </div>
            <div class="specialevents">
                <asp:Label ID="tbDuration" runat="server" Text=""></asp:Label></div>
            <div class="clearall">
            </div>
            <div class="specialevents">
                <asp:Label ID="tbWeeksSinceLastCalled" runat="server" Text=""></asp:Label></div>
            <div class="specialevents">
                <span id="tbCreated" runat="server"></span>
            </div>
            <div class="specialevents">
                <span id="tbModified" runat="server"></span>
            </div>
        </div>
    </div>
    <div class="divcontectmainforms">
        <a name="tabs"></a>
        <ucl2:tabentry ID="tabentry1" runat="server" />
    </div>
    <script type="text/javascript">
        hideStatusDiv("MainContent_div_message");

        //            jQuery(function () {
        //                //distributorEntryValidations();
        //            });

        function DisableUserNameField() {
            //debugger;
            $("#MainContent_TxtUserName").prop('disabled', true);
        }

        function SetVisibleFalseClearButton() {
            //debugger;
            $("#MainContent_buttonbar_buttonDeactivate").css('visibility', 'hidden');
        }

        function EnableUserNameField() {
            $("#MainContent_TxtUserName").prop('disabled', false);
        }

        $("#MainContent_buttonbar_buttinSave").click(function () {
            //Null validation
            distributorEntryValidations();
        });
        jQuery(function ($) {
            var items = [
                            ["Bank Details", "usercontrols/distributor_bank.aspx"]
                        ];
            loadEntryPageTabs("customer", items, $("#MainContent_HiddenFieldTabIndex").val());
        });

        jQuery(function ($) {
            $.mask.definitions['~'] = '[+0123456789]';
            $('input.input-large1-mobile').mask("~9?999999999999");
        });

        jQuery(function ($) {
            $('input.input-large1-phone').mask("9?999999999");
        });

        $("#MainContent_id_select_user").click(function () {
            get_users_lookup('lead_customer/process_forms/processmaster.aspx?fm=distributorentry&type=select&deptString=' + $("#MainContent_HiddenFieldLoadUserType").val());
        });

        /*Load User Lookup in Distributor Entry Page*/
        function get_users_lookup(targeturl) {
            $("#div_loader").show();
            var url = ROOT_PATH + targeturl;
            $.ajax({
                url: url,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    $("#div_loader").hide();
                    if (msg != "") {
                        var wnd = $("#modalWindowOriginators").kendoWindow({
                            title: "Users",
                            modal: true,
                            visible: false,
                            resizable: false,
                            width: 300
                        }).data("kendoWindow");

                        $("#modalWindowOriginators").html(msg);
                        wnd.center().open();

                        $("#jqi_state0_buttonOk").click(function () {
                            //debugger;
                            var originatorName = '';
                            var originatorId = '';
                            $('#originatorTable tr').each(function () {
                                if ($(this).children(':eq(0)').find('#a').is(':checked')) {
                                    originatorName = $(this).children(':eq(0)').find('#a').val();
                                    originatorId = $(this).children(':eq(0)').find('#b').val();
                                }
                            });

                            $('#MainContent_txtSelectedOriginatorName').val(originatorName);
                            $('#MainContent_HiddenFieldSelectedOriginator').val(originatorId);
                            wnd.close();
                            DisableUserNameField();
                            GetDistributorDetails(originatorId);
                        });

                        $("#jqi_state0_buttonCancel").click(function () {
                            closeUsersPopup();
                        });
                    }
                },
                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                        $("#div_loader").hide();
                        //$("#" + tagertdiv).html(msg);
                    }
                }
            });
        }

        //            $("#jqi_state0_buttonCancel").click(function () {
        //                closeUsersPopup();
        //            });

        function closeUsersPopup() {
            var wnd = $("#modalWindowOriginators").kendoWindow({
                title: "Users",
                modal: true,
                visible: false,
                resizable: false,
                width: 300
            }).data("kendoWindow");
            wnd.center().close();
            //$("#modalWindowOriginators").css("display", "none");
        }

        function GetDistributorDetails(originator) {
            $("#div_loader").show();
            //debugger;
            var param = { "originator": originator };
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/distributor_Service.asmx/GetDistributorDetails",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.hasOwnProperty('d')) {
                        msg = response.d;
                    } else {
                        msg = response;
                    }
                    //debugger;
                    var json = JSON.parse(msg);

                    var distId = json["DistributorId"];
                    var name = json["Name"];
                    var username = json["Originator"];
                    var password = json["Password"];
                    var address = json["Adderess"];
                    var telephone = json["Telephone"];
                    var mobile = json["Mobile"];
                    var email = json["Email"];
                    var holidays = json["HolidaysList"];

                    //alert(distId);
                    $('#MainContent_HiddenFieldSelectedDistId').val(distId);
                    $('#MainContent_HiddenFieldSelectedOriginator').val(username);

                    $('#MainContent_txtName').val(name);
                    $('#MainContent_TxtUserName').val(username);
                    $('#MainContent_TxtPassword').val(password);
                    $('#MainContent_TxtVerifyPassword').val(password);
                    $('#MainContent_TxtAddress1').val(address);
                    $('#MainContent_TxtTelephone').val(telephone);
                    $('#MainContent_txtMobile').val(mobile);
                    $('#MainContent_TextBoxEmail').val(email);
                    //debugger;

                    //Unckeck All items
                    $("[id*=chklstHolidays] input:checkbox").prop('checked', false); // To uncheck all 

                    //Check Items by value
                    var selValue = new Array()
                    selValue = holidays;
                    var $ctrls = $("[id*=chklstHolidays]");
                    for (var i = 0; i < selValue.length; i++) {
                        $ctrls.find('input:checkbox[value=' + selValue[i] + ']').prop('checked', true);
                    }
                    $("#div_loader").hide();
                    enabletabs();

                    if ($("#MainContent_HiddenFieldLoadUserType").val() == "ASE")
                        $("#id_address").attr("href", "javascript:addBankAccount('lead_customer/process_forms/processmaster.aspx?fm=bankdetails&type=insert&typ=TME&distid=" + username + "','')");
                    else if ($("#MainContent_HiddenFieldLoadUserType").val() == "DIST")
                        $("#id_address").attr("href", "javascript:addBankAccount('lead_customer/process_forms/processmaster.aspx?fm=bankdetails&type=insert&typ=DIST&distid=" + username + "','')");
                    else if ($("#MainContent_HiddenFieldLoadUserType").val() == "DR")
                        $("#id_address").attr("href", "javascript:addBankAccount('lead_customer/process_forms/processmaster.aspx?fm=bankdetails&type=insert&typ=DR&distid=" + username + "','')");
                    $("#MainContent_HiddenFieldIsDistributorSaved").val("1");
                    //EnableUserNameField();
                    loadDistributorBankGrid(username, 'DistBankGrid');

                    $("#MainContent_buttonbar_buttinSave").removeClass('dissavebtn').addClass('savebtn');
                },
                error: function (response) {
                    //alert("Oops, something went horribly wrong");
                }
            });
        }

    </script>
</asp:Content>


