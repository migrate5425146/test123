﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class lead_customer_transaction_map : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Master.SetBreadCrumb("Map", "#", "");
            if (Request.QueryString["address"] != null)
            {
                hfAddress.Value = Request.QueryString["address"];
            }
            if (Request.QueryString["custcode"] != null)
            {
                hfCustCode.Value = Request.QueryString["custcode"];
                hlBack.NavigateUrl = "~/lead_customer/transaction/customer_entry.aspx?custid=" + Request.QueryString["custcode"] + "&ty=" + Request.QueryString["ty"] + "&ref=add#tabs";
            }
            if (Request.QueryString["leadid"] != null)
            {
                hfLeadId.Value = Request.QueryString["leadid"];
                hlBack.NavigateUrl = "~/lead_customer/transaction/leadentry.aspx?leadid=" + Request.QueryString["leadid"] + "&ref=add#tabs";
            }
        }
    }
}