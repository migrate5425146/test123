﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="pricelist_entry.aspx.cs" Inherits="lead_customer_transaction_pricelist_entry" %>
<%@ MasterType TypeName="SiteMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
<asp:HiddenField ID="HiddenFieldEndUser_CustCode" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="HiddenFieldEndUser_EndUsercode" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="HiddenField_Mode" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="HiddenField_PriceList" runat="server" ClientIDMode="Static"/>
<asp:HiddenField ID="HiddenField_Effecdate" runat="server" ClientIDMode="Static"/>


<div class="divcontectmainforms">
    <div class="toolbar_container">
        <div class="toolbar_left" id="div_content">
            <div class="buttonbar">
                <asp:Button ID="buttonSave" runat="server"  CssClass="savebtn" OnClick="buttonSave_Click" Text="Save"/><%--OnClick="buttonSave_Click"--%>
            </div>
        </div>
        <div class="toolbar_right" id="div3">
            <div class="leadentry_title_bar">
                <div style="float: right; width: 35%;" align="right">
                    <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/lead_customer/transaction/lead_contacts.aspx">
                    <div class="back"></div>
                    </asp:HyperLink>
                </div>
            </div>
        </div>
    </div>

    <div style="width=100%;padding-top:15px">

    <div id="div_message" runat="server" style="display: none;">
    </div>
    <div id="div_info" runat="server" style="display: none">
    </div>
    <div id="div_customer_lookup" ></div>
<div id="newcust_window">
                <div id="div_newcustgrid">
                </div>
               
            </div>
    </div>
    <div class="formleft">
        <div class="formtextdiv">
            Customer
        </div>
        <div class="formdetaildivlead">
            <asp:TextBox ID="txtCustomerName" runat="server" ReadOnly="true" Width="183px"></asp:TextBox>
            <a href="#" id="a_customer">
                <img id="Img1" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" />
            </a>
        </div>
        <div class="clearall">
        </div>
        <div class="formtextdiv">
            Enduser
        </div>
        <div class="formdetaildivlead">
            <asp:TextBox ID="txtEnduser" runat="server" ReadOnly="true" Width="183px"></asp:TextBox>
            <a href="#" id="a_enduser">
                <img id="Img2" src="~/assets/images/popicon.png" alt="popicon" runat="server" border="0" />
            </a>
        </div>
        <div class="clearall">
        </div>
    </div>
    <div class="formright">
        <div class="formtextdiv">
            Effective Date</div>
        <div class="formdetaildivlead">
            <asp:TextBox ID="dpEffectiveDate" runat="server"></asp:TextBox>
        </div>
        <div class="clearall">
        </div>
        <div class="formtextdiv">
            Last Updated</div>
        <div class="formdetaildivlead">
            <div id="div_LastUpdate" runat="server" ClientIDMode="Static"></div>
        </div>
        <div class="clearall">
        </div>

        <div class="formtextdiv">
            Originator</div>
        <div class="formdetaildivlead">
            <div id="div_Originator" runat="server" ClientIDMode="Static"></div>            
        </div>
        <div class="clearall">
        </div>
    </div>
</div>

<div class="clearall">&nbsp;</div>
<div style="min-width: 200px; margin:10px 10px;" >
    <div id="PricelistGrid"></div>
</div>

<script type="text/javascript">
    $("#MainContent_buttonSave").click(function () {
        var entityGrid = $("#PricelistGrid").data("kendoGrid");
        $("#HiddenField_PriceList").val(JSON.stringify(entityGrid.dataSource.view()));

    });

    $(document).ready(function () {


        var newcust_window = $("#newcust_window"),
                        newcust_undo = $("#undo")
                                .bind("click", function () {
                                    newcust_window.data("kendoWindow").open();
                                    newcust_undo.hide();
                                });

        var onClose = function () {
            newcust_undo.show();
        }

        if (!newcust_window.data("kendoWindow")) {
            newcust_window.kendoWindow({
                width: "500px",
                height: "400px",
                title: "Lookup",
                close: onClose
            });
        }

        newcust_window.data("kendoWindow").close();

        $("#a_customer").click(function () {
            if ($("#HiddenField_Mode").val() != 'edit') {
                $("div#div_loader").show();
                loaddistributorLookup('div_newcustgrid', 'newcust_window', 2);
            }
        });

        $("#a_enduser").click(function () {
            if ($("#HiddenField_Mode").val() != 'edit') {
                $("div#div_loader").show();
                loadEndUserLookup('div_newcustgrid', 'newcust_window', $('#HiddenFieldEndUser_CustCode').val(), 1);
            }
        });

        //loadEnduserPriceList1('STEWENN0', 'PricelistGrid', $("#MainContent_dpEffectiveDate").val());

    });

    function loadpage() {

        var cust_code = $("#HiddenFieldEndUser_CustCode").val();

        //var effecdate = $("#HiddenField_Effecdate").val();
        

        $("#MainContent_dpEffectiveDate").kendoDatePicker({
            value: $("#HiddenField_Effecdate").val(),
            format: "dd-MMM-yyyy"
        });

        if ($("#HiddenField_Mode").val() != 'edit') {
            loadEnduserPriceListmaster(cust_code, 'PricelistGrid', ''); 
            $('#div_LastUpdate').html($("#MainContent_dpEffectiveDate").val());
        }
        else {
            loadEnduserPriceListmaster(cust_code, 'PricelistGrid', $("#MainContent_dpEffectiveDate").val());
        }

    }
    
//    function OilTypeyDropDownEditor(container, options) {
//        $('<input required data-text-field="Description" data-value-field="CatlogCode" data-bind="value:' + options.field + '"/>')
//            .appendTo(container)
//            .kendoDropDownList({
//                autoBind: false,
//                dataSource: {
//                    transport: {
//                        read: {
//                            url: ROOT_PATH + "service/lead_customer/enduser_entry_Service.asmx/GetEnduserPriceAndCount1",
//                            contentType: "application/json; charset=utf-8",
//                            type: "POST",
//                            data: { custCode: '' }
//                        },
//                        parameterMap: function (data, operation) {
//                            //data = $.extend( data);
//                            return JSON.stringify(data);
//                        }
//                    },
//                    schema: {
//                        data: "d"
//                    }
//                }
//            });
//        }

        
</script>
</asp:Content>

