﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="customer_target_allocation.aspx.cs" Inherits="lead_customer_transaction_customer_target_allocation" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField runat="server" ID="HiddenFieldRouteMasterID" />
    <asp:HiddenField runat="server" ID="HiddenFieldRouteName" />
    <asp:HiddenField runat="server" ID="HiddenFieldSelectedRouteTarget" />
    <asp:HiddenField runat="server" ID="HiddenFieldSelectedRouteTargetId" />
    <asp:HiddenField runat="server" ID="HiddenFieldSuccessfullySaved" />
    <asp:HiddenField ID="HiddenFieldCustomerTargets" runat="server" />
    <asp:HiddenField runat="server" ID="HiddenFieldOverwriteClickOk" />
    <%--    <div class="divcontectmainforms">--%>
    <div id="routeTargetmodalWindow" style="display: none">
        <div id="div_routeTargetConfirm_message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>
    <div class="divcontectmainforms" style="min-height: 600px;">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div>
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/default.aspx">
            <div class="back"></div></asp:HyperLink>
                    </div>
                </div>
            </div>
            <div class="clearall">
            </div>
            <div id="div_message" runat="server" style="display: none;">
            </div>
            <div id="div_info" runat="server" style="display: none">
            </div>
            <div class="grid_container">
                <%--                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Effective Month :
                </div>
                <div class="formdetaildiv">
                    <asp:DropDownList ID="dropDownMonth" runat="server">
                    </asp:DropDownList>
                </div>--%>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Route
                </div>
                <div class="formdetaildiv">
                    <span class="wosub mand">
                        <asp:TextBox ID="txtRouteName" CssClass="tb" runat="server" Style="width: 200px;
                            text-transform: uppercase"></asp:TextBox>
                        <div runat="server" id="div_autocomplete">
                        </div>
                    </span>
                    <%--<asp:Label runat="server" ID="lblRouteTarget"></asp:Label>--%>
                </div>
                <div class="clearall">
                </div>
                <div id="div_RouteTarget">
                    <div class="formtextdiv">
                        Route Target Qty.(Mil)
                    </div>
                    <div class="formdetaildiv extra_5px">
                            <asp:Label runat="server" ID="lblRouteTarget"></asp:Label>
                     </div>
                </div>
            </div>
            <div class="clearall">
            </div>
            <div class="grid_container" id="div_mainCustomerTargets">
                <div class="grid_title" id="div_gridTitleCustomerTargets">
                    Customer Targets
                </div>
                <div id="div_customerTargets">
                </div>
            </div>
            <div class="clearall">
            </div>
        </div>
    </div>
    <script type="text/javascript">

        $("#div_gridTitleCustomerTargets").css('display', 'none');
        $("#div_RouteTarget").css('display', 'none');

        $(document).ready(function () {
            hideStatusDiv("MainContent_div_message");

            if ($('#MainContent_HiddenFieldRouteMasterID').val() != '') {
                $("#div_gridTitleCustomerTargets").css('display', 'block');
                $("#div_RouteTarget").css('display', 'block');
                if ($('#MainContent_HiddenFieldSuccessfullySaved').val() == '1') {
                    loadCustomerTargets($('#MainContent_HiddenFieldRouteMasterID').val());
                    $('#MainContent_HiddenFieldSuccessfullySaved').val("0");
                    loadSelectedRouteTarget($('#MainContent_HiddenFieldRouteMasterID').val());
                }
            }
        });

        $("#MainContent_buttonbar_buttinSave").click(function () {
            entityGrid = $("#div_customerTargets").data("kendoGrid");
            $("#MainContent_HiddenFieldCustomerTargets").val(JSON.stringify(entityGrid.dataSource.view()));
        });





//        function rebindCustomerTargets(jsnarray) {
////            var take_grid = $("#MainContent_hfPageIndex").val();
////            var DATE_FORMAT = "{0: yyyy-MM-dd}"
////            $("#div_loader").show();
//            $("#div_customerTargets").html("");
//            $("#div_customerTargets").kendoGrid({
//                dataSource: {
//                    transport: {
//                        read: function (options) {
//                            options.success(jsnarray);
//                        }
//                    },
//                    schema: {
//                        data: "d"
//                    }
//                }
//            });
//        }
        
    </script>
</asp:Content>
