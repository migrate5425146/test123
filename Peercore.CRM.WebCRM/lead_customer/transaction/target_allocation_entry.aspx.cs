﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Text;
using Peercore.CRM.Common;
using System.Web.Script.Serialization;
using CRMServiceReference;
using Peercore.CRM.Shared;

public partial class lead_customer_transaction_target_allocation_entry : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
            buttonbar.EnableSave(true);
            buttonbar.VisibleSave(true);

            if (!IsPostBack)
            {
                div_autocomplete.InnerHtml = SetAutocomplete_Distributor("1");

                List<string> months = System.Globalization.DateTimeFormatInfo.InvariantInfo.MonthNames.ToList();
                months.RemoveAt(12);
                foreach (string item in months)
                {
                    dropDownMonth.Items.Add(new ListItem(item.Trim(), months.IndexOf(item).ToString()));
                    if(months.IndexOf(item).ToString().Equals((DateTime.Today.Month - 1).ToString()))
                        dropDownMonth.SelectedValue = months.IndexOf(item).ToString();
                }
            }

            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Target Allocation ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

    }

    protected void ButtonSave_Click(object sender)
    {
        this.SaveTargets();
    }

    public void SaveTargets()
    {
        CommonUtility commonUtility = new CommonUtility();
        try
        {
            bool isSucc = false;

            int selectedMonth = Convert.ToInt32(dropDownMonth.SelectedValue);
            DateTime today = DateTime.Today;

            DateTime firstOfMonth = new DateTime(today.Year, selectedMonth + 1, 1);
            DateTime endOfMonth = new DateTime(today.Year, selectedMonth + 1, DateTime.DaysInMonth(today.Year, selectedMonth + 1));

            JavaScriptSerializer serializer = new JavaScriptSerializer();
            string jsonString = "";

            List<DistributorDTO> distributorTargetList = new List<DistributorDTO>();
            List<MarketDTO> marketTargetList = new List<MarketDTO>();
            List<RouteMasterDTO> routeTargetList = new List<RouteMasterDTO>();

            if (!string.IsNullOrEmpty(HiddenFieldDistributorTargets.Value))
            {
                jsonString = @"" + HiddenFieldDistributorTargets.Value.Replace("\"ExtensionData\":{},", "");
                distributorTargetList = serializer.Deserialize<List<DistributorDTO>>(jsonString);
            }

            if (!string.IsNullOrEmpty(HiddenFieldMarketTargets.Value))
            {
                jsonString = @"" + HiddenFieldMarketTargets.Value.Replace("\"ExtensionData\":{},", "");
                marketTargetList = serializer.Deserialize<List<MarketDTO>>(jsonString);
            }

            if (!string.IsNullOrEmpty(HiddenFieldRouteTargets.Value))
            {
                jsonString = @"" + HiddenFieldRouteTargets.Value.Replace("\"ExtensionData\":{},", "");
                routeTargetList = serializer.Deserialize<List<RouteMasterDTO>>(jsonString);
            }

            CommonClient commonClient = new CommonClient();
            DistributorClient distributorClient = new DistributorClient();

            if (distributorTargetList.Count != 0 || marketTargetList.Count != 0 || routeTargetList.Count != 0)
            {
                foreach (DistributorDTO item in distributorTargetList)
                {
                    if (item.TargetId == 0 && item.Target > 0)//new insert entry
                    {
                        item.EffStartDate = firstOfMonth;
                        item.EffEndDate = endOfMonth;
                        item.CreatedBy = UserSession.Instance.UserName;
                    }
                    else if (item.TargetId > 0)//update entry
                    {
                        item.LastModifiedBy = UserSession.Instance.UserName;
                    }
                }

                foreach (MarketDTO item in marketTargetList)
                {
                    if (item.TargetId == 0 && item.Target > 0)//new insert entry
                    {
                        item.EffStartDate = firstOfMonth;
                        item.EffEndDate = endOfMonth;
                        item.DistributorId = Convert.ToInt32(HiddenFieldDistributorID.Value);
                        item.CreatedBy = UserSession.Instance.UserName;
                    }
                    else if (item.TargetId > 0)//update entry
                    {
                        item.LastModifiedBy = UserSession.Instance.UserName;
                    }
                }

                foreach (RouteMasterDTO item in routeTargetList)
                {
                    if (item.TargetId == 0 && item.Target > 0)//new insert entry
                    {
                        item.EffStartDate = firstOfMonth;
                        item.EffEndDate = endOfMonth;
                        item.CreatedBy = UserSession.Instance.UserName;
                    }
                    else if (item.TargetId > 0)//update entry
                    {
                        item.LastModifiedBy = UserSession.Instance.UserName;
                    }
                }

                TargetAllocationDTO targetAllocationDTO = new TargetAllocationDTO();
                targetAllocationDTO.DistributorTargetList = distributorTargetList;
                targetAllocationDTO.MarketTargetList = marketTargetList;
                targetAllocationDTO.RouteTargetList = routeTargetList;

                isSucc = commonClient.SaveTargetAllocation(targetAllocationDTO);
            }

            if (isSucc)
            {
                div_message.Attributes.Add("style", "display:block;padding:4px");
                div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved !");
            }
            else
            {
                div_message.Attributes.Add("style", "display:block;padding:4px");
                div_message.InnerHtml = commonUtility.GetInfoMessage(CommonUtility.ERROR_MESSAGE);
            }

        }
        catch (Exception ex)
        {
            div_message.Attributes.Add("style", "display:block;padding:4px");
            div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    private string SetAutocomplete_Distributor(string inactive)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script type=\"text/javascript\">");
        sb.Append("     $(function () {");
        sb.Append("         $(\".tb\").autocomplete({");
        sb.Append("             source: function (request, response) {");
        sb.Append("                 $.ajax({");
        sb.Append("                     type: \"POST\",");
        sb.Append("                     contentType: \"application/json; charset=utf-8\",");
        sb.Append("                     url: \"" + ConfigUtil.ApplicationPath + "service/lead_customer/common.asmx/GetDistributorsByLikeName\",");
        sb.Append("                     data: \"{'name':'\" + request.term + \"'}\",");
        sb.Append("                     dataType: \"json\",");
        sb.Append("                     async: true,");
        sb.Append("                     dataFilter: function(data) {");
        sb.Append("                           return data;");
        sb.Append("                     },");
        sb.Append("                     success: function (data) {");
        sb.Append("                     response($.map(data.d, function(item) {");
        sb.Append("                     return {");
        sb.Append("                         label: item.Name,");
        sb.Append("                         desc: item.UserName,");
        sb.Append("                         distributorId: item.OriginatorId");
        sb.Append("                     };");
        sb.Append("                     }));");

        sb.Append("                     },");
        sb.Append("                     error: function (result) {");
        sb.Append("                     }");
        sb.Append("                 });");
        sb.Append("             },");
        sb.Append("             minLength: 2,");
        sb.Append("            select: function( event, ui ) {");
        sb.Append("                 var selectedObj = ui.item;");
        sb.Append("                 $('#MainContent_HiddenFieldDistributorName').val(selectedObj.label);");
        sb.Append("                 $('#MainContent_HiddenFieldDistributorID').val(selectedObj.distributorId);");
        sb.Append("                 $('#MainContent_HiddenFieldDistributorUserName').val(selectedObj.desc);");
        sb.Append("                 if(selectedObj.label != '')");
        sb.Append("                 {");
        sb.Append("                     $('#grid_wrapper').show();");
        sb.Append("                     loadDistributor(selectedObj.distributorId,$('#MainContent_dropDownMonth').val());");
        //sb.Append("                     loadDistributor(selectedObj.distributorId,$('#MainContent_StartDate').val(),$('#MainContent_EndDate').val());");
        sb.Append("                 }");
        sb.Append("            }");
        sb.Append("         });");
        sb.Append("     });");
        sb.Append("</script>");
        sb.Append("");

        return sb.ToString();
    }
}