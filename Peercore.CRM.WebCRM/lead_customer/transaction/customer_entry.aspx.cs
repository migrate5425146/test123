﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using System.Web.UI.HtmlControls;
using CRMServiceReference;
using System.IO;
using System.ServiceModel;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.CRM.BusinessRules;

public partial class lead_customer_transaction_customer_entry : PageBase
{
    #region Constant
    public const string SEARCH_TEXT = "SEARCH_TEXT";
    CommonUtility commonUtility = new CommonUtility();
    #endregion

    #region Properties
    private static string customerCode;
    public static string CustomerCode
    {
        get { return customerCode; }
        set { customerCode = value; }
    }

    private KeyValuePair<string, string> CustomerEntry
    {
        get
        {
            if (Session[CommonUtility.CUSTOMER_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.CUSTOMER_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.CUSTOMER_DATA] = value;
        }
    }
    #endregion Properties

    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            //Remove Other Session.
            ClearSession();

            RegisterJavascript();

            if (Request.QueryString["ty"] != null && Request.QueryString["ty"] == "pendingcustomer")
            {
                buttonbar.onButtonApprove = new usercontrols_buttonbar.ButtonApprove(buttonApprove_Click);
                buttonbar.EnableApprove(true);
                buttonbar.VisibleApprove(true);
            }
            else
            {
                buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
                buttonbar.onButtonActivityHistory = new usercontrols_buttonbar.ButtonActivityHistory(ButtonActivityHistory_Click);
                buttonbar.EnableSave(true);
                buttonbar.VisibleSave(true);
                buttonbar.VisibleActivityHistory(false);

                txtCode.ReadOnly = false;
            }

            UserSession.Instance.IsAmountVisible = false;
            UserSession.Instance.UnitsAbbriviation = "Litres";

            if (!IsPostBack)
            {
                txtCode.Text = "<<Auto>>";

                List<OutletTypeEntity> outletTypeList = OutletTypeBR.Instance.GetAllOutletTypeByAccessToken("");
                DropDownListOutletType.DataSource = outletTypeList;
                DropDownListOutletType.DataTextField = "OutletTypeName";
                DropDownListOutletType.DataValueField = "OutletTypeId";
                DropDownListOutletType.DataBind();

                div_autocomplete.InnerHtml = SetAutocomplete_Temp("1");

                SetBackLink();

                LoadOptions();
                //Master.SetCommonHeader("lead");
                //BindGlobalSettings();
                if (Request.QueryString["custid"] != null)
                {
                    string cust_code = Server.HtmlDecode(Request.QueryString["custid"].ToString());
                    HiddenCustomerCode.Value = Server.UrlEncode(cust_code);
                    if (Request.QueryString["ty"] != null)
                        HiddenFieldFormType.Value = Request.QueryString["ty"].ToString();
                    CustomerCode = cust_code;
                    CustomerEntry = new KeyValuePair<string, string>(cust_code, "cust_code");
                    GetCustomer();
                    PopulateGrids(CustomerCode);
                }
                else
                {
                    Session.Remove(CommonUtility.CUSTOMER_DATA);
                }

                HiddenFieldTabIndex.Value = "1";
                if (Request.QueryString["ref"] != null && Request.QueryString["ref"] != string.Empty)
                {
                    if (Request.QueryString["ref"] == "con")
                    {
                        HiddenFieldTabIndex.Value = "2";
                    }
                    if (Request.QueryString["ref"] == "add")
                    {
                        HiddenFieldTabIndex.Value = "3";
                    }
                    if (Request.QueryString["ref"] == "per")
                    {
                        HiddenFieldTabIndex.Value = "4";
                    }
                    if (Request.QueryString["ref"] == "end")
                    {
                        HiddenFieldTabIndex.Value = "5";
                    }
                    if (Request.QueryString["ref"] == "act")
                    {
                        HiddenFieldTabIndex.Value = "6";
                    }
                    if (Request.QueryString["ref"] == "doc")
                    {
                        HiddenFieldTabIndex.Value = "7";
                    }
                    if (Request.QueryString["ref"] == "opp")
                    {
                        HiddenFieldTabIndex.Value = "8";
                    }
                    if (Request.QueryString["ref"] == "mai")
                    {
                        HiddenFieldTabIndex.Value = "9";
                    }
                }
            }

            Session.Remove(CommonUtility.BREADCRUMB);
            //Remove the session before setting the breadcrumb.
            if (Request.QueryString["custid"] != null)
            {
                Master.Page.Title = "mSales - Edit Outlet";
                Master.SetBreadCrumb("Edit Outlet ", "#", "");
            }
            else
            {
                Master.Page.Title = "mSales - New Outlet";
                Master.SetBreadCrumb("New Outlet ", "#", "");
            }
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    protected void ButtonSave_Click(object sender)
    {
        CustomerClient oCustomer = new CustomerClient();
        CommonUtility commonUtility = new CommonUtility();

        try
        {
            if (Page.IsValid)
            {
                HiddenFieldSaveClickOk.Value = "1";
            }

            if (HiddenFieldSaveClickOk.Value == "1")
            {
                CustomerDTO customerContact = oCustomer.GetCustomer(Server.UrlDecode(CustomerEntry.Key));

                CustomerDTO customer = new CustomerDTO();
                customer.CustomerCode = txtCode.Text;
                customer.Name = txtName.Text;
                customer.Address = TxtAddress1.Text;
                customer.Telephone = TxtTelephone.Text;
                if (txtLongitude.Text.Length != 0)
                    customer.Longitude = Convert.ToDouble(txtLongitude.Text);

                if (txtLatitude.Text.Length != 0)
                    customer.Latitiude = Convert.ToDouble(txtLatitude.Text);

                customer.PeripheryId = DropDownListPeriphery.SelectedValue != null ? Convert.ToInt32(DropDownListPeriphery.SelectedValue) : 0;
                customer.OutletTypeId = DropDownListOutletType.SelectedValue != null ? Convert.ToInt32(DropDownListOutletType.SelectedValue) : 0;
                customer.VolumeId = DropDownListVolume.SelectedValue != null ? Convert.ToInt32(DropDownListVolume.SelectedValue) : 0;
                customer.CategoryId = DropDownListCategory.SelectedValue != null ? Convert.ToInt32(DropDownListCategory.SelectedValue) : 0;
                customer.IsRetail = DropDownIsRetail.SelectedValue != "1" ? false : true;

                int marketId = Convert.ToInt32(HiddenFieldMarketId.Value);
                //customer.MarketId = DropDownListMarkets.SelectedValue != null ? Convert.ToInt32(DropDownListMarkets.SelectedValue) : 0;
                customer.MarketId = marketId;

                customer.RepCode = UserSession.Instance.UserName; //HiddenFieldRepCode.Value;
                customer.RouteAssignId = Convert.ToInt32(HiddenFieldRouteAssignId.Value);
                customer.OutletUserCode = txtOutletUserCode.Text;

                if (customer.Name == null || customer.Name == "" || customer.Name == " ")
                {
                    div_info.Attributes.Add("style", "display:none");
                    div_info.InnerHtml = "";
                    div_message.Attributes.Add("style", "display:block");
                    div_message.InnerHtml = commonUtility.GetErrorMessage("Please Enter a customer");
                    return;
                }
                else if (customer.RouteAssignId == 0 || customer.RepCode == null || customer.RepCode == "")
                {
                    div_info.Attributes.Add("style", "display:none");
                    div_info.InnerHtml = "";
                    div_message.Attributes.Add("style", "display:block");
                    div_message.InnerHtml = commonUtility.GetErrorMessage("Please Assign a route");
                    return;
                }

                customer.isTLP = radioyes.Checked ? true :
                   radiono.Checked ? false : false;

                if (Request.QueryString["ty"] != null && Request.QueryString["ty"] != string.Empty)
                    customer.CustomerTypeOfApprove = Request.QueryString["ty"].ToString();

                try
                {
                    oCustomer = new CustomerClient();

                    string selectedImageName = !String.IsNullOrEmpty(HiddenFieldSelectedImageName.Value) ? HiddenFieldSelectedImageName.Value : null;
                    byte[] imageContent = null;
                    if (selectedImageName != null)
                    {
                        string imagePath = ConfigUtil.ApplicationPath + "docs/customer_images/" + selectedImageName;
                        imageContent = System.IO.File.ReadAllBytes(Server.MapPath(imagePath));

                        if (imageContent != null)
                        {
                            //  Rename Uploaded Image with the Product_Code.jpg
                            try
                            {
                                string oldPath = ConfigUtil.ApplicationPath + "docs/customer_images/" + selectedImageName;
                                string newPath = ConfigUtil.ApplicationPath + "docs/customer_images/" + "productDTO.Code" + ".jpg";

                                //if new file exists, DElete it
                                if (System.IO.File.Exists(Server.MapPath(newPath)))
                                    System.IO.File.Delete(Server.MapPath(newPath));

                                System.IO.File.Move(Server.MapPath(oldPath), Server.MapPath(newPath));
                            }
                            catch (Exception ex)
                            {
                                div_message.Attributes.Add("style", "display:block");
                                div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
                                //HiddenFieldSaveFailed.Value = "1";
                                return;
                            }
                        }
                    }
                    customer.ImageContent = imageContent;

                    bool iNoRecs = oCustomer.SaveCustomer(customer);

                    if (iNoRecs)
                    {
                        div_info.Attributes.Add("style", "display:none");
                        div_info.InnerHtml = "";

                        div_message.Attributes.Add("style", "display:block");
                        div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved.");

                        try
                        {
                            CommonClient cClient = new CommonClient();

                            if (txtCode.Text.Contains("Auto"))
                            {

                                cClient.CreateTransactionLog(UserSession.Instance.OriginalUserName,
                                    DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                    TransactionTypeModules.Insert,
                                    TransactionModules.Customer,
                                    UserSession.Instance.OriginalUserName + " added new customer " + txtName.Text);
                            }
                            else
                            {
                                cClient.CreateTransactionLog(UserSession.Instance.OriginalUserName,
                                    DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                    TransactionTypeModules.Insert,
                                    TransactionModules.Customer,
                                    UserSession.Instance.OriginalUserName + " update customer " + txtCode.Text + "-" + txtName.Text);
                            }
                        }
                        catch { }

                        //txtName.Text = "";
                        //TxtAddress1.Text = "";
                        //TxtTelephone.Text = "";
                        //txtLongitude.Text = "";
                        //txtLatitude.Text = "";
                        //txtDRName.Text = "";
                        //txtOutletUserCode.Text = "";
                        //DropDownListOutletType.SelectedIndex = -1;
                        //HiddenFieldSelectedImageName.Value = "";

                    }
                }
                catch (Exception e)
                {
                }
                finally
                {
                    if (oCustomer.State != CommunicationState.Closed)
                        oCustomer.Close();
                }
            }
        }
        catch (Exception ex)
        {
            //commonUtility.ErrorMaintain(ex);

            div_info.Attributes.Add("style", "display:block");
            div_message.Attributes.Add("style", "display:none");
            div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    protected void buttonApprove_Click(object sender)
    {
        CustomerClient oCustomer = new CustomerClient();
        CommonUtility commonUtility = new CommonUtility();
        try
        {
            if (Page.IsValid)
            {

                if (HiddenFieldSaveClickOk.Value != "1")
                {
                    ScriptManager.RegisterStartupScript(this, GetType(), "modalscript", "saveconfirmationmsg();", true);
                    return;
                }
                else
                {
                    HiddenFieldSaveClickOk.Value = "1";
                }

                if (HiddenFieldSaveClickOk.Value == "1")
                {
                    CustomerDTO customerContact = oCustomer.GetCustomer(Server.UrlDecode(CustomerEntry.Key));

                    CustomerDTO customer = new CustomerDTO();
                    customer.CustomerCode = txtCode.Text;
                    customer.Name = txtName.Text;
                    customer.Address = TxtAddress1.Text;
                    customer.Telephone = TxtTelephone.Text;
                    customer.Longitude = Convert.ToDouble(txtLongitude.Text);
                    customer.Latitiude = Convert.ToDouble(txtLatitude.Text);
                    customer.PeripheryId = DropDownListPeriphery.SelectedValue != null ? Convert.ToInt32(DropDownListPeriphery.SelectedValue) : 0;
                    customer.OutletTypeId = DropDownListOutletType.SelectedValue != null ? Convert.ToInt32(DropDownListOutletType.SelectedValue) : 0;
                    customer.VolumeId = DropDownListVolume.SelectedValue != null ? Convert.ToInt32(DropDownListVolume.SelectedValue) : 0;
                    customer.CategoryId = DropDownListCategory.SelectedValue != null ? Convert.ToInt32(DropDownListCategory.SelectedValue) : 0;
                    int marketId = Convert.ToInt32(HiddenFieldMarketId.Value);
                    //customer.MarketId = DropDownListMarkets.SelectedValue != null ? Convert.ToInt32(DropDownListMarkets.SelectedValue) : 0;
                    customer.MarketId = marketId;
                    customer.RouteAssignId = Convert.ToInt32(HiddenFieldRouteAssignId.Value);
                    customer.RepCode = HiddenFieldRepCode.Value;


                    customer.isTLP = radioyes.Checked ? true :
                       radiono.Checked ? false : false;

                    if (Request.QueryString["ty"].ToString() != null)
                        customer.CustomerTypeOfApprove = Request.QueryString["ty"].ToString();

                    try
                    {
                        oCustomer = new CustomerClient();
                        bool iNoRecs = oCustomer.SaveCustomer(customer);

                        if (iNoRecs)
                        {
                            div_info.Attributes.Add("style", "display:none");
                            div_info.InnerHtml = "";

                            div_message.Attributes.Add("style", "display:block");
                            div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved.");
                        }
                    }
                    catch (Exception e)
                    {
                    }
                    finally
                    {
                        if (oCustomer.State != CommunicationState.Closed)
                            oCustomer.Close();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //commonUtility.ErrorMaintain(ex);

            div_info.Attributes.Add("style", "display:block");
            div_message.Attributes.Add("style", "display:none");
            div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    protected void ButtonActivityHistory_Click(object sender)
    {
        Response.Redirect(ConfigUtil.ApplicationPath + "activity_planner/transaction/activity_history.aspx?fm=acthis&custid=" + HiddenCustomerCode.Value);
    }
    #endregion

    #region Methods
    private void SetBackLink()
    {
        if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty)
        {
            if (Request.QueryString["fm"] == "home")
            {
                hlBack.NavigateUrl = "~/default.aspx?fm=home";
            }
            else if (Request.QueryString["fm"] == "activity")
            {
                hlBack.NavigateUrl = "~/activity_planner/transaction/activity_scheduler.aspx";
            }
            else if (Request.QueryString["fm"] == "act")
            {
                hlBack.NavigateUrl = "~/calendar/transaction/appointment_entry.aspx";
            }
            else if (Request.QueryString["fm"] == "opp")
            {
                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "pipelines_stage/transaction/opportunity.aspx?cht=pipe&fm=lead";
            }
            else if (Request.QueryString["fm"] == "lead")
            {
                if (Request.QueryString["ty"] != null && Request.QueryString["ty"] != string.Empty)
                {
                    hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "lead_customer/transaction/lead_contacts.aspx?ty=" + Request.QueryString["ty"] + "&fm=lead";
                }
                else
                {
                    hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "lead_customer/transaction/lead_contacts.aspx?fm=" + Request.QueryString["fm"];
                }
            }
            else if (Request.QueryString["fm"] == "search")
            {
                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "lead_customer/transaction/search.aspx";
            }
            else if (Request.QueryString["fm"] == "dsbd1")
            {
                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/ActivityAnalysis.aspx";
            }
            else if (Request.QueryString["fm"] == "dsbd2")
            {
                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/OpportunityAnalysis.aspx";
            }
        }
        else if (Request.QueryString["ty"] != null && Request.QueryString["ty"] != string.Empty)
        {
            hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "lead_customer/transaction/lead_contacts.aspx?ty=" + Request.QueryString["ty"] + "&fm=lead";
        }
    }

    private void RegisterJavascript()
    {
        //ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "jquery-1.7.2.min", "http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js");
        ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "highcharts.js", string.Format("{0}{1}", ConfigUtil.ApplicationPath, "assets/scripts/highcharts.js"));
        ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "exporting.js", string.Format("{0}{1}", ConfigUtil.ApplicationPath, "assets/scripts/exporting.js"));
    }

    private void LoadOptions()
    {
        #region Args Setting
        ArgsDTO args = new ArgsDTO();
        args.ChildOriginators = UserSession.Instance.ChildOriginators;
        args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        args.Originator = UserSession.Instance.UserName;
        args.SStartDate = DateTime.Today.ToString(ConfigUtil.DateTimePatternForSql);
        args.SEndDate = DateTime.Today.AddDays(1).ToString(ConfigUtil.DateTimePatternForSql);
        #endregion

        //Load Potential Opportunity
        string[,] sArrLitersBy = {{"W", "M", "Y"},
                                   {"Weekly","Monthly","Yearly"}};

        DataTable dtLitersBy = new DataTable();
        DataRow drLitersBy;
        dtLitersBy.Columns.Add("Code", Type.GetType("System.String"));
        dtLitersBy.Columns.Add("LitersBy", Type.GetType("System.String"));

        for (int i = 0; i < sArrLitersBy.GetLength(1); i++)
        {
            drLitersBy = dtLitersBy.NewRow();
            drLitersBy["Code"] = sArrLitersBy[0, i];
            drLitersBy["LitersBy"] = sArrLitersBy[1, i];
            dtLitersBy.Rows.Add(drLitersBy);
        }


        #region commented
        //// Load Provinces
        //CommonClient oLookupTable = new CommonClient();
        //List<LookupTableDTO> provinceList = new List<LookupTableDTO>();
        //try
        //{
        //    provinceList = oLookupTable.GetLookupTables("CPRO", args);

        //    DropDownListProvince.DataSource = provinceList;
        //    DropDownListProvince.DataTextField = "TableDescription";
        //    DropDownListProvince.DataValueField = "TableCode";
        //    DropDownListProvince.DataBind();

        //    foreach (LookupTableDTO item in provinceList)
        //    {
        //        if (item.DefaultValue == "Y")
        //        {
        //            DropDownListProvince.SelectedValue = item.TableCode; break;
        //        }
        //    }
        //}
        //catch (Exception x)
        //{
        //    //commonUtility.ErrorMaintain(x);

        //    div_info.Attributes.Add("style", "display:block");
        //    div_message.Attributes.Add("style", "display:none");
        //    div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        //}
        //finally
        //{
        //    if (oLookupTable.State != CommunicationState.Closed)
        //        oLookupTable.Close();
        //}


        //// Load Geo Location
        //List<LookupTableDTO> locationList = new List<LookupTableDTO>();
        //try
        //{
        //    oLookupTable = new CommonClient();
        //    locationList = oLookupTable.GetLookupTables("CGEO", args);

        //    DropDownListGeoLocation.DataSource = locationList;
        //    DropDownListGeoLocation.DataTextField = "TableDescription";
        //    DropDownListGeoLocation.DataValueField = "TableCode";
        //    DropDownListGeoLocation.DataBind();

        //    foreach (LookupTableDTO item in provinceList)
        //    {
        //        if (item.DefaultValue == "Y")
        //        {
        //            DropDownListGeoLocation.SelectedValue = item.TableCode; break;
        //        }
        //    }
        //}
        //catch (Exception x)
        //{
        //    //commonUtility.ErrorMaintain(x);

        //    div_info.Attributes.Add("style", "display:block");
        //    div_message.Attributes.Add("style", "display:none");
        //    div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        //}
        //finally
        //{
        //    if (oLookupTable.State != CommunicationState.Closed)
        //        oLookupTable.Close();
        //}

        //// Load Status
        //List<LookupTableDTO> StatusList = new List<LookupTableDTO>();
        //try
        //{
        //    oLookupTable = new CommonClient();
        //    StatusList = oLookupTable.GetLookupTables("CSTA", args);

        //    DropDownListStatus.DataSource = StatusList;
        //    DropDownListStatus.DataTextField = "TableDescription";
        //    DropDownListStatus.DataValueField = "TableCode";
        //    DropDownListStatus.DataBind();

        //    foreach (LookupTableDTO item in StatusList)
        //    {
        //        if (item.DefaultValue == "Y")
        //        {
        //            DropDownListStatus.SelectedValue = item.TableCode; break;
        //        }
        //    }
        //}
        //catch (Exception x)
        //{
        //    //commonUtility.ErrorMaintain(x);

        //    div_info.Attributes.Add("style", "display:block");
        //    div_message.Attributes.Add("style", "display:none");
        //    div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        //}
        //finally
        //{
        //    if (oLookupTable.State != CommunicationState.Closed)
        //        oLookupTable.Close();
        //}


        //// Load Handled products
        //List<LookupTableDTO> prudctshList = new List<LookupTableDTO>();
        //try
        //{
        //    oLookupTable = new CommonClient();
        //    prudctshList = oLookupTable.GetLookupTables("CPRH", args);

        //    DropDownListProHandled.DataSource = prudctshList;
        //    DropDownListProHandled.DataTextField = "TableDescription";
        //    DropDownListProHandled.DataValueField = "TableCode";
        //    DropDownListProHandled.DataBind();

        //    foreach (LookupTableDTO item in prudctshList)
        //    {
        //        if (item.DefaultValue == "Y")
        //        {
        //            DropDownListProHandled.SelectedValue = item.TableCode; break;
        //        }
        //    }
        //}
        //catch (Exception x)
        //{
        //    //commonUtility.ErrorMaintain(x);

        //    div_info.Attributes.Add("style", "display:block");
        //    div_message.Attributes.Add("style", "display:none");
        //    div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        //}
        //finally
        //{
        //    if (oLookupTable.State != CommunicationState.Closed)
        //        oLookupTable.Close();
        //}

        //// Load Canvassing  Frequency
        //List<LookupTableDTO> cFrequencyList = new List<LookupTableDTO>();
        //try
        //{
        //    oLookupTable = new CommonClient();
        //    cFrequencyList = oLookupTable.GetLookupTables("CANF", args);

        //    DropDownListCanvFreq.DataSource = cFrequencyList;
        //    DropDownListCanvFreq.DataTextField = "TableDescription";
        //    DropDownListCanvFreq.DataValueField = "TableCode";
        //    DropDownListCanvFreq.DataBind();

        //    foreach (LookupTableDTO item in cFrequencyList)
        //    {
        //        if (item.DefaultValue == "Y")
        //        {
        //            DropDownListCanvFreq.SelectedValue = item.TableCode; break;
        //        }
        //    }
        //}
        //catch (Exception x)
        //{
        //    //commonUtility.ErrorMaintain(x);

        //    div_info.Attributes.Add("style", "display:block");
        //    div_message.Attributes.Add("style", "display:none");
        //    div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        //}
        //finally
        //{
        //    if (oLookupTable.State != CommunicationState.Closed)
        //        oLookupTable.Close();
        //}

        //// Load ASU 30 Share (%)
        //List<LookupTableDTO> asuList = new List<LookupTableDTO>();
        //try
        //{
        //    oLookupTable = new CommonClient();
        //    asuList = oLookupTable.GetLookupTables("CASU", args);

        //    DropDownListAsu.DataSource = asuList;
        //    DropDownListAsu.DataTextField = "TableDescription";
        //    DropDownListAsu.DataValueField = "TableCode";
        //    DropDownListAsu.DataBind();

        //    foreach (LookupTableDTO item in asuList)
        //    {
        //        if (item.DefaultValue == "Y")
        //        {
        //            DropDownListAsu.SelectedValue = item.TableCode; break;
        //        }
        //    }
        //}
        //catch (Exception x)
        //{
        //    //commonUtility.ErrorMaintain(x);

        //    div_info.Attributes.Add("style", "display:block");
        //    div_message.Attributes.Add("style", "display:none");
        //    div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        //}
        //finally
        //{
        //    if (oLookupTable.State != CommunicationState.Closed)
        //        oLookupTable.Close();
        //}

        //// Load Authority Assessment
        //List<LookupTableDTO> assessmentList = new List<LookupTableDTO>();
        //try
        //{
        //    oLookupTable = new CommonClient();
        //    assessmentList = oLookupTable.GetLookupTables("CAUA", args);

        //    DropDownListAssessment.DataSource = assessmentList;
        //    DropDownListAssessment.DataTextField = "TableDescription";
        //    DropDownListAssessment.DataValueField = "TableCode";
        //    DropDownListAssessment.DataBind();

        //    foreach (LookupTableDTO item in assessmentList)
        //    {
        //        if (item.DefaultValue == "Y")
        //        {
        //            DropDownListAssessment.SelectedValue = item.TableCode; break;
        //        }
        //    }
        //}
        //catch (Exception x)
        //{
        //    //commonUtility.ErrorMaintain(x);

        //    div_info.Attributes.Add("style", "display:block");
        //    div_message.Attributes.Add("style", "display:none");
        //    div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        //}
        //finally
        //{
        //    if (oLookupTable.State != CommunicationState.Closed)
        //        oLookupTable.Close();
        //}

        //// Load Periphery
        //List<LookupTableDTO> peripheryList = new List<LookupTableDTO>();
        //try
        //{
        //    oLookupTable = new CommonClient();
        //    peripheryList = oLookupTable.GetLookupTables("CPRI", args);

        //    DropDownListPeriphery.DataSource = peripheryList;
        //    DropDownListPeriphery.DataTextField = "TableDescription";
        //    DropDownListPeriphery.DataValueField = "TableCode";
        //    DropDownListPeriphery.DataBind();

        //    foreach (LookupTableDTO item in peripheryList)
        //    {
        //        if (item.DefaultValue == "Y")
        //        {
        //            DropDownListPeriphery.SelectedValue = item.TableCode; break;
        //        }
        //    }
        //}
        //catch (Exception x)
        //{
        //    //commonUtility.ErrorMaintain(x);

        //    div_info.Attributes.Add("style", "display:block");
        //    div_message.Attributes.Add("style", "display:none");
        //    div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        //}
        //finally
        //{
        //    if (oLookupTable.State != CommunicationState.Closed)
        //        oLookupTable.Close();
        //}


        //// Load Category
        //List<LookupTableDTO> categoryList = new List<LookupTableDTO>();
        //try
        //{
        //    oLookupTable = new CommonClient();
        //    categoryList = oLookupTable.GetLookupTables("CPRI", args);

        //    DropDownListCategory.DataSource = categoryList;
        //    DropDownListCategory.DataTextField = "TableDescription";
        //    DropDownListCategory.DataValueField = "TableCode";
        //    DropDownListCategory.DataBind();

        //    foreach (LookupTableDTO item in categoryList)
        //    {
        //        if (item.DefaultValue == "Y")
        //        {
        //            DropDownListCategory.SelectedValue = item.TableCode; break;
        //        }
        //    }
        //}
        //catch (Exception x)
        //{
        //    //commonUtility.ErrorMaintain(x);

        //    div_info.Attributes.Add("style", "display:block");
        //    div_message.Attributes.Add("style", "display:none");
        //    div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        //}
        //finally
        //{
        //    if (oLookupTable.State != CommunicationState.Closed)
        //        oLookupTable.Close();
        //}
        ////Load Sub Market
        //List<LookupTableDTO> channelList = new List<LookupTableDTO>();
        //try
        //{

        //    oLookupTable = new CommonClient();
        //    channelList = oLookupTable.GetLookupTables("CHAN", args);

        //    //dropdownSubMarket.DataSource = channelList;
        //    //dropdownSubMarket.DataTextField = "TableDescription";
        //    //dropdownSubMarket.DataValueField = "TableCode";
        //    //dropdownSubMarket.DataBind();

        //    foreach (LookupTableDTO item in channelList)
        //    {
        //        if (item.DefaultValue == "Y")
        //        {
        //         //   dropdownSubMarket.SelectedValue = item.TableCode; break;
        //        }
        //    }
        //}
        //catch (Exception x)
        //{
        //    //commonUtility.ErrorMaintain(x);

        //    div_info.Attributes.Add("style", "display:block");
        //    div_message.Attributes.Add("style", "display:none");
        //    div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        //}
        //finally
        //{
        //    if (oLookupTable.State != CommunicationState.Closed)
        //        oLookupTable.Close();
        //}

        ////Load Source
        //List<LookupTableDTO> sourceList = new List<LookupTableDTO>();
        //try
        //{
        //    oLookupTable = new CommonClient();
        //    sourceList = oLookupTable.GetLookupTables("SRCE", args);

        //    //dropdownSource.DataSource = sourceList;
        //    //dropdownSource.DataTextField = "TableDescription";
        //    //dropdownSource.DataValueField = "TableCode";
        //    //dropdownSource.DataBind();

        //    foreach (LookupTableDTO item in sourceList)
        //    {
        //        if (item.DefaultValue == "Y")
        //        {
        //          //  dropdownSource.SelectedValue = item.TableCode; break;
        //        }
        //    }
        //}
        //catch (Exception x)
        //{
        //    //commonUtility.ErrorMaintain(x);

        //    div_info.Attributes.Add("style", "display:block");
        //    div_message.Attributes.Add("style", "display:none");
        //    div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        //}
        //finally
        //{
        //    if (oLookupTable.State != CommunicationState.Closed)
        //        oLookupTable.Close();
        //}

        #endregion 

        CommonClient commonClient = new CommonClient();

        ArgsDTO args2 = new ArgsDTO();
        args2.OrderBy = " market_name asc ";
        args2.AdditionalParams = "";
        args2.StartIndex = 1;
        args2.RowCount = 300;

        List<MarketDTO> marketList = commonClient.GetAllMarket(args2);
        //marketList.Insert(0, new MarketDTO() { MarketName = "ALL", MarketId = 0 });

        DropDownListMarkets.DataSource = marketList;
        DropDownListMarkets.DataTextField = "MarketName";
        DropDownListMarkets.DataValueField = "MarketId";
        DropDownListMarkets.DataBind();

        commonClient = new CommonClient();
        List<CustomerCategory> custCategoryList = commonClient.GetAllCustomerCategory();
        custCategoryList.Insert(0, new CustomerCategory() { CategoryName = "Select Category", CategoryId = -1 });

        DropDownListCategory.DataSource = custCategoryList;
        DropDownListCategory.DataTextField = "CategoryName";
        DropDownListCategory.DataValueField = "CategoryId";
        DropDownListCategory.DataBind();
    }

    private void GetCustomer()
    {
        if ((Request.QueryString["custid"] != null) && (Request.QueryString["custid"] != string.Empty))
        {
            CustomerClient oCustomer = new CustomerClient();
            //  CustomerCode = "ADAMORN0";
            try
            {
                CustomerDTO customer = new CustomerDTO();
                if (Request.QueryString["ty"] != null && Request.QueryString["ty"] == "pendingcustomer")
                {
                    customer = oCustomer.GetCustomerPending(CustomerCode);
                }
                else
                {
                    customer = oCustomer.GetCustomer(CustomerCode);
                }


                if (customer == null)
                    return;

                #region Disable Controls
                DisableControls();
                #endregion

                //tbStage.Text = customer.CreditStatus.Equals("09") ? "CUSTOMER ON STOP" : "CUSTOMER";

                //if (customer.CreditStatus.Trim().Equals("09"))
                //{
                //    tbStage.ForeColor = System.Drawing.Color.Crimson;
                //}
                //else
                //{
                //    tbStage.ForeColor = System.Drawing.Color.RoyalBlue;
                //}

                //if (customer.StartDate != null)
                //    tbDuration.Text = customer.StartDate.Year > 1950 ? "CUSTOMER SINCE : " + customer.StartDate.ToString("MMM yyyy") : "";

                ////Setting Customer Name.
                //if (!string.IsNullOrWhiteSpace(customer.Name))
                //{
                //    //  txtCustomerName.Text = customer.Name;
                //}

                //Setting CustomerCode.
                if (!string.IsNullOrWhiteSpace(customer.CustomerCode))
                {
                    txtCode.Text = customer.CustomerCode;
                }

                //Setting ShortName.
                if (!string.IsNullOrWhiteSpace(customer.Name))
                {
                    txtName.Text = customer.Name;
                }

                if (!string.IsNullOrWhiteSpace(customer.Longitude.ToString()))
                {
                    txtLongitude.Text = customer.Longitude.ToString();
                }

                if (!string.IsNullOrWhiteSpace(customer.Latitiude.ToString()))
                {
                    txtLatitude.Text = customer.Latitiude.ToString();
                }

                if (!string.IsNullOrWhiteSpace(customer.PeripheryId.ToString()))
                {
                    DropDownListPeriphery.SelectedValue = customer.PeripheryId.ToString();
                }

                if (!string.IsNullOrWhiteSpace(customer.PeripheryId.ToString()))
                {
                    DropDownListOutletType.SelectedValue = customer.OutletTypeId.ToString();
                }

                if (!string.IsNullOrWhiteSpace(customer.PeripheryId.ToString()))
                {
                    DropDownListVolume.SelectedValue = customer.VolumeId.ToString();
                }

                if (!string.IsNullOrWhiteSpace(customer.CategoryId.ToString()))
                {
                    DropDownListCategory.SelectedValue = customer.CategoryId.ToString();
                }

                if (!string.IsNullOrWhiteSpace(customer.MarketId.ToString()))
                {
                    DropDownListMarkets.SelectedValue = customer.MarketId.ToString();
                    HiddenFieldMarketId.Value = customer.MarketId.ToString();
                    HiddenFieldMarketName.Value = customer.MarketName;
                    txtMarketName.Text = customer.MarketName;
                }

                if (customer.IsRetail == true)
                    DropDownIsRetail.SelectedValue = "1";
                else
                    DropDownIsRetail.SelectedValue = "0";

                if (!string.IsNullOrWhiteSpace(customer.Address))
                {
                    TxtAddress1.Text = customer.Address;
                }

                if (!string.IsNullOrWhiteSpace(customer.Telephone))
                {
                    TxtTelephone.Text = customer.Telephone;
                }
                if (!string.IsNullOrWhiteSpace((customer.isTLP).ToString()))
                {
                    if (customer.isTLP == true)
                    {
                        radioyes.Checked = true;
                    }

                    if (customer.isTLP == true)
                    {
                        radioyes.Checked = true;
                    }
                    else
                    {
                        radiono.Checked = true;
                    }

                }

                if (!string.IsNullOrWhiteSpace(customer.PrimaryRepName))
                {
                    txtDRName.Text = customer.PrimaryRepName;
                    HiddenFieldRepCode.Value = customer.RepCode;
                }

                if (!string.IsNullOrWhiteSpace(customer.AssignedRouteName))
                {
                    txtDRName.Text = customer.AssignedRouteName;
                    HiddenFieldRouteAssignId.Value = customer.RouteAssignId.ToString();
                }

                if (!string.IsNullOrWhiteSpace(customer.OutletUserCode))
                {
                    txtOutletUserCode.Text = customer.OutletUserCode;
                }

                txtCode.ReadOnly = true;

                #region commented
                ////Setting Description.
                //if (!string.IsNullOrWhiteSpace(customer.Description))
                //{
                //    txtDescription.Text = customer.Description;
                //}

                ////Setting Company.
                //if (!string.IsNullOrWhiteSpace(customer.Company))
                //{
                //    //  txtCompany.Text = customer.Company;
                //}

                ////Setting Industry.
                //if (!string.IsNullOrWhiteSpace(customer.Industry))
                //{
                //    // dropdownMarket.SelectedValue = customer.Industry;
                //}

                ////Setting Sub Market.
                //if (!string.IsNullOrWhiteSpace(customer.Business))
                //{
                //    dropdownSubMarket.SelectedValue = customer.Business;
                //}

                //Setting Rating.
                //       rRating.Value = customer.Rating;

                //Setting LeadSource.
                //if (!string.IsNullOrWhiteSpace(customer.LeadSource))
                //    dropdownSource.SelectedValue = customer.LeadSource;
                //else
                //    dropdownSource.SelectedIndex = 0;

                ////Setting Business Potential.
                //if (!string.IsNullOrWhiteSpace(customer.BusinessPotential))
                //{
                //    txtBusinessPotential.Text = customer.BusinessPotential;
                //}

                ////Setting ReferredBy.
                //if (!string.IsNullOrWhiteSpace(customer.ReferredBy))
                //{
                //    txtReferredBy.Text = customer.ReferredBy;
                //}

                //// WEEKS SINCE LAST CALLED
                //TimeSpan tsLastCalled;
                //decimal dWeeks = 0;
                //if (customer.LastCalledDate != null)
                //{
                //    tsLastCalled = DateTime.Now - Convert.ToDateTime(customer.LastCalledDate.Value.ToLocalTime());
                //    decimal result = decimal.Parse(tsLastCalled.Days.ToString()) / decimal.Parse("7");
                //    dWeeks = decimal.Round(result);
                //    tbWeeksSinceLastCalled.Text = "WEEKS SINCE LAST CALLED : " + String.Format("{0:0.##}", dWeeks);
                //}
                //else
                //{
                //    tbWeeksSinceLastCalled.Text = "NO ACTIVITIES YET";
                //}

                ////Setting PotentialLiters.
                //if (customer.LitersBy != "")
                //    dropdownPotentialOpportunity.SelectedValue = customer.LitersBy;
                //else
                //    dropdownPotentialOpportunity.SelectedIndex = 0;

                ////Setting Potential Opportunity.
                ////txtPotentialOpportunity.Text = customer.PotentialLiters == 0 ? "" : String.Format("{0:0.00}", customer.PotentialLiters);
                //if (customer.PotentialLiters != null)
                //    txtPotentialOpportunity.Text = customer.PotentialLiters.ToString(ConfigUtil.NumberFormat);

                ////Food Service Rep.
                //if (customer.PrimaryRepName != "")
                //    divfoodrep.InnerHtml = customer.PrimaryRepName;

                ////Bakery Rep.
                //if (customer.SecondaryRepName != "")
                //    divBakeryRep.InnerHtml = customer.SecondaryRepName;

                ////Setting Annual Revenue.
                //if (customer.AnnualRevenue != null)
                //{
                //    txtAnnualRevenue.Text = customer.AnnualRevenue.ToString(ConfigUtil.NumberFormat);
                //}

                //if (customer.NoOfEmployees != null)
                //{
                //    txtNoOfEmployees.Text = customer.NoOfEmployees.ToString();
                //}

                //if (!string.IsNullOrWhiteSpace(customer.PreferredContact))
                //{
                //    hfPreferredMethod.Value = customer.PreferredContact;
                //}

                //#region Setting Contact Details
                ////HiddenFieldContactPhone.Value = customer.Telephone;
                ////HiddenFieldContactFax.Value = customer.Fax;
                ////HiddenFieldContactMobile.Value = customer.Mobile;
                ////HiddenFieldContactEmail.Value = customer.Email;
                ////HiddenFieldContactPrefMethod.Value = customer.PreferredContact;
                ////HiddenFieldCustAddress1.Value = customer.Address;
                ////HiddenFieldCustAddress2.Value = customer.Address2;
                ////HiddenFieldCustCity.Value = customer.City;
                ////HiddenFieldCustPostcode.Value = customer.PostalCode;
                ////HiddenFieldCustState.Value = customer.State;
                //#endregion
                #endregion
            }
            catch (Exception ex)
            {
                //commonUtility.ErrorMaintain(ex);

                div_info.Attributes.Add("style", "display:block");
                div_message.Attributes.Add("style", "display:none");
                div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
            }
            finally
            {
                if (oCustomer.State != CommunicationState.Closed)
                    oCustomer.Close();
            }
        }
    }

    //private void BindGlobalSettings()
    //{
    //    CurrentUserSettings = new ArgsDTO();
    //    CurrentUserSettings.ChildOriginators = UserSession.Instance.ChildOriginators;
    //    CurrentUserSettings.Originator = UserSession.Instance.UserName;
    //    CurrentUserSettings.DefaultDepartmentId = ConfigUtil.DefaultDepartmentID;
    //}

    private void PopulateGrids(string customerCode)
    {
        CustomerCode = Server.UrlEncode(customerCode);
        KeyValuePair<string, string> lead_data = new KeyValuePair<string, string>(Server.UrlEncode(customerCode), "Customer");

        //Setting session value.
        CustomerEntry = lead_data;
    }

    private void ClearSession()
    {
        Session[CommonUtility.LEAD_DATA] = null;
    }

    private void DisableControls()
    {
        // txtCode.Enabled = false;
        // txtShortName.Enabled = false;
        // txtCustomerName.Enabled = false;
        // txtCompany.Enabled = false;

        // dropdownMarket.Enabled = false;
        // dropdownSubMarket.Enabled = false;
        // dropdownPotentialOpportunity.Enabled = false;
        // txtPotentialOpportunity.Enabled = false;

        //// rRating.Enabled = false;
        // txtBusinessPotential.Enabled = false;
        // txtReferredBy.Enabled = false;

        // dropdownSource.Enabled = false;
        // txtDescription.Enabled = false;
        // txtAnnualRevenue.Enabled = false;

        // txtNoOfEmployees.Enabled = false;
    }

    //private string SetAutocomplete_Temp()
    //{
    //    StringBuilder sb = new StringBuilder();
    //    sb.Append("<script type=\"text/javascript\">");
    //    sb.Append("     $(function () {");
    //    sb.Append("         $(\".tb\").autocomplete({");
    //    sb.Append("             source: function (request, response) {");
    //    sb.Append("                 $.ajax({");
    //    sb.Append("                     type: \"POST\",");
    //    sb.Append("                     contentType: \"application/json; charset=utf-8\",");
    //    sb.Append("                     url: \"" + ConfigUtil.ApplicationPath + "service/call_cycle/call_cycle_service.asmx/GetRepsByOriginator\",");
    //    sb.Append("                     data: \"{'name':'\" + request.term + \"'}\",");
    //    sb.Append("                     dataType: \"json\",");
    //    sb.Append("                     async: true,");
    //    sb.Append("                     dataFilter: function(data) {");
    //    sb.Append("                           return data;");
    //    sb.Append("                     },");
    //    sb.Append("                     success: function (data) {");
    //    sb.Append("                     response($.map(data.d, function(item) {");
    //    sb.Append("                     return {");
    //    sb.Append("                         label: item.Name,");
    //    sb.Append("                         desc: item.RepCode");
    //    sb.Append("                     };");
    //    sb.Append("                     }));");

    //    sb.Append("                     },");
    //    sb.Append("                     error: function (result) {");
    //    sb.Append("                     }");
    //    sb.Append("                 });");
    //    sb.Append("             },");
    //    sb.Append("             minLength: 2,");
    //    sb.Append("            select: function( event, ui ) {");

    //    sb.Append("                  var selectedObj = ui.item;");

    //    //sb.Append("                 $('#MainContent_HiddenFieldCustomerIndustry').val(selectedObj.industry);");
    //    //sb.Append("                 $('#MainContent_HiddenFieldCustomerCode').val(selectedObj.label);");
    //    //sb.Append("                 $('#MainContent_HiddenFieldCustCode').val(selectedObj.label);");
    //    sb.Append("                 $('#MainContent_txtDRName').val(selectedObj.label);");
    //    sb.Append("                 $('#MainContent_HiddenFieldRepCode').val(selectedObj.desc);");
    //    //sb.Append("                 setcuscode(selectedObj.label,selectedObj.desc,'0');");
    //    //k   sb.Append("                 loadproductlist(selectedObj.label);");
    //    //sb.Append("                 $('ul.ui-autocomplete ui-menu ui-widget ui-widget-content ui-corner-all').css('display','none');");
    //    sb.Append("            }");
    //    sb.Append("         });");
    //    sb.Append("     });");
    //    sb.Append("</script>");
    //    sb.Append("");

    //    return sb.ToString();
    //}

    private string SetAutocomplete_Temp(string inactive)
    {
        StringBuilder sb = new StringBuilder();
        sb.Append("<script type=\"text/javascript\">");
        sb.Append("     $(function () {");
        sb.Append("         $(\".tb\").autocomplete({");
        sb.Append("             source: function (request, response) {");
        sb.Append("                 $.ajax({");
        sb.Append("                     type: \"POST\",");
        sb.Append("                     contentType: \"application/json; charset=utf-8\",");
        sb.Append("                     url: \"" + ConfigUtil.ApplicationPath + "service/lead_customer/common.asmx/GetRouteRepLikeRouteNameForCustomer\",");
        sb.Append("                     data: \"{'name':'\" + request.term + \"','isTemp':'0'}\",");
        sb.Append("                     dataType: \"json\",");
        sb.Append("                     async: true,");
        sb.Append("                     dataFilter: function(data) {");
        sb.Append("                           return data;");
        sb.Append("                     },");
        sb.Append("                     success: function (data) {");
        sb.Append("                     response($.map(data.d, function(item) {");
        sb.Append("                     return {");
        sb.Append("                         label: item.RouteName,");
        sb.Append("                         desc: item.RouteId,");
        sb.Append("                         tempRepName: item.TempRepName,");
        sb.Append("                         originalRep: item.RepCode,");
        sb.Append("                         routeMasterId: item.RouteMasterId,");
        sb.Append("                         marketId: item.OriginatorType,");
        sb.Append("                         marketName: item.TMECode");
        sb.Append("                     };");
        sb.Append("                     }));");

        sb.Append("                     },");
        sb.Append("                     error: function (result) {");
        sb.Append("                     }");
        sb.Append("                 });");
        sb.Append("             },");
        sb.Append("             minLength: 2,");
        sb.Append("            select: function( event, ui ) {");

        sb.Append("                 var selectedObj = ui.item;");
        //sb.Append("                 $('#MainContent_txtDRName').val(selectedObj.label);");
        sb.Append("                 $('#MainContent_HiddenFieldRepCode').val(selectedObj.originalRep);");
        sb.Append("                 $('#MainContent_HiddenFieldAssignedRouteName').val(selectedObj.label);");
        sb.Append("                 $('#MainContent_HiddenFieldRouteAssignId').val(selectedObj.desc);");
        sb.Append("                 $('#MainContent_HiddenFieldRouteMasterId').val(selectedObj.routeMasterId);");
        sb.Append("                 if(selectedObj.marketId != null && selectedObj.marketId != ''){");
        sb.Append("                     $('#MainContent_HiddenFieldMarketId').val(selectedObj.marketId);");
        //sb.Append("                     alert(selectedObj.marketId);");
        sb.Append("                     $('#MainContent_HiddenFieldMarketName').val(selectedObj.marketName);");
        sb.Append("                     $('#MainContent_txtMarketName').val(selectedObj.marketName);");
        //sb.Append("                     alert(selectedObj.marketName);");
        sb.Append("                 }");
        sb.Append("                 else{");
        sb.Append("                     $('#MainContent_HiddenFieldMarketId').val('0');");
        sb.Append("                     $('#MainContent_HiddenFieldMarketName').val('');");
        sb.Append("                     $('#MainContent_txtMarketName').val(' - ');");
        sb.Append("                 }");
        // sb.Append("                 LoadReleventMarket(selectedObj.routeMasterId);");
        sb.Append("            }");
        sb.Append("         });");
        sb.Append("     });");
        sb.Append("</script>");
        sb.Append("");

        return sb.ToString();
    }
    #endregion
}