﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using System.IO;
using Peercore.CRM.Shared;
using System.Web.SessionState;
using System.Data;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data.OleDb;

public partial class lead_customer_transaction_customer_excel_upload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Master.SetBreadCrumb("Outlets Upload ", "#", "");

            //LoadASEs();
            //LoadDistributors(ddlASE.SelectedValue);
            //LoadReps(ddlDistributor.SelectedValue);

            LoadTerritory();
        }
    }

    private void LoadTerritory()
    {
        #region Territory

        MasterClient masterlient = new MasterClient();
        ArgsModel args1 = new ArgsModel();
        args1.StartIndex = 1;
        args1.RowCount = 10000;
        args1.OrderBy = " territory_name asc";

        List<TerritoryModel> objTrrList = new List<TerritoryModel>();
        objTrrList = masterlient.GetAllTerritoryByOriginator(args1, UserSession.Instance.OriginatorString,
                                                                                UserSession.Instance.UserName);

        if (objTrrList.Count != 0)
        {
            ddlTerritory.Items.Clear();
            ddlTerritory.Items.Add(new ListItem("- Select Territory -", "0"));
            foreach (TerritoryModel item in objTrrList)
            {
                var strLength = (item.TerritoryCode == null) ? 0 : item.TerritoryCode.Length;
                string result = (item.TerritoryCode == null) ? "" : item.TerritoryCode.PadRight(strLength + 20).Substring(0, strLength + 20);

                ddlTerritory.Items.Add(new ListItem(item.TerritoryName + "  |  " + result, item.TerritoryId.ToString()));
            }

            ddlTerritory.Enabled = true;
        }

        #endregion
    }

    //private void LoadASEs()
    //{
    //    ArgsDTO args = new ArgsDTO();
    //    args.OrderBy = "name ASC";
    //    args.AdditionalParams = " dept_string = 'ASE' ";
    //    args.StartIndex = 1;
    //    args.RowCount = 500;
    //    OriginatorClient originatorClient = new OriginatorClient();
    //    List<OriginatorDTO> aseList = new List<OriginatorDTO>();
    //    aseList = originatorClient.GetOriginatorsByCatergory(args);

    //    if (aseList.Count > 0)
    //        aseList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

    //    SetAses(aseList, "UserName");
    //}

    //private void LoadDistributors(string selectedASE)
    //{
    //    ArgsDTO args = new ArgsDTO();
    //    args.OrderBy = "name ASC";

    //    args.AdditionalParams = " dept_string = 'DIST'  AND crm_mast_asm.originator = '" + selectedASE + "' ";

    //    args.StartIndex = 1;
    //    args.RowCount = 500;
    //    OriginatorClient originatorClient = new OriginatorClient();
    //    List<OriginatorDTO> distList = new List<OriginatorDTO>();

    //    distList = originatorClient.GetDistributersByASMOriginator(args);
    //    if (distList.Count > 0)
    //        distList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

    //    SetDistributors(distList, "UserName");
    //}

    //private void LoadReps(string selectedDistributor)
    //{
    //    ArgsDTO args = new ArgsDTO();
    //    args.OrderBy = "name ASC";

    //    args.AdditionalParams = " originator.dept_string = 'DR'  AND crm_distributor.originator = '" + selectedDistributor + "' ";
    //    args.StartIndex = 1;
    //    args.RowCount = 1000;
    //    OriginatorClient originatorClient = new OriginatorClient();
    //    List<OriginatorDTO> repList = new List<OriginatorDTO>();

    //    repList = originatorClient.GetAllSRByOriginator(args);
    //    if (repList.Count > 0)
    //        repList.Insert(0, new OriginatorDTO() { Name = "ALL", UserName = "ALL" });

    //    SetReps(repList, "UserName");
    //}

    //public void SetAses(List<OriginatorDTO> listOriginator, string valueField = "UserName")
    //{
    //    ddlASE.DataSource = listOriginator;
    //    ddlASE.DataTextField = "Name";
    //    ddlASE.DataValueField = valueField;
    //    ddlASE.DataBind();
    //}

    //public void SetDistributors(List<OriginatorDTO> listOriginator, string valueField = "UserName")
    //{
    //    ddlDistributor.DataSource = listOriginator;
    //    ddlDistributor.DataTextField = "Name";
    //    ddlDistributor.DataValueField = valueField;
    //    ddlDistributor.DataBind();
    //}

    //public void SetReps(List<OriginatorDTO> listOriginator, string valueField = "UserName")
    //{
    //    ddlSalesRep.DataSource = listOriginator;
    //    ddlSalesRep.DataTextField = "Name";
    //    ddlSalesRep.DataValueField = valueField;
    //    ddlSalesRep.DataBind();
    //}

    //protected void ddlASE_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        LoadDistributors(ddlASE.SelectedValue);
    //        LoadReps(ddlDistributor.SelectedValue);
    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //}
    //protected void ddlDistributor_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        LoadReps(ddlDistributor.SelectedValue);
    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //}

    protected void excelUpload_Click(object sender, EventArgs e)
    {
        HttpSessionState session = HttpContext.Current.Session;
        string sessionID = session.SessionID;
        List<ColumnSettingDTO> lstColumnSetting = new List<ColumnSettingDTO>();

        try
        {
            if (xmlUpload.HasFile)
            {
                string FileName = Path.GetFileName(xmlUpload.PostedFile.FileName);
                string Extension = Path.GetExtension(xmlUpload.PostedFile.FileName);
                string FolderPath = HttpContext.Current.Request.PhysicalApplicationPath + "\\uploads"; 

                string[] validFileTypes = { ".xls", ".xlsx" };

                string FilePath = FolderPath + "\\" + FileName;

                bool isValidType = false;
                isValidType = validFileTypes.Contains(Extension);

                if (ddlTerritory.SelectedIndex <= 0)
                {
                    //ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileValidator()", true);                   
                }
                else if (!isValidType)
                {
                    //error
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileValidator()", true);
                }
                else
                {
                    xmlUpload.SaveAs(FilePath);
                    CustomerClient customerClient = new CustomerClient();
                    string UserName = UserSession.Instance.UserName;

                    if (customerClient.UploadExcelfile(FilePath, Extension, UserName, ddlTerritory.SelectedValue))
                    {
                        Response.Write("<script>alert('Successfully Uploaded!');</script>");
                        Response.Redirect("~/lead_customer/transaction/lead_contacts.aspx?cht=lead&ty=customer");
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileFormatValidator()", true);
                    }
                }
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnReview_Click(object sender, EventArgs e)
    {
        
    }

    public static string GetCellValue(SpreadsheetDocument document, Cell cell)
    {
        SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
        string value = cell.CellValue.InnerXml;

        if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
        {
            return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
        }
        else
        {
            return value;
        }
    }
}