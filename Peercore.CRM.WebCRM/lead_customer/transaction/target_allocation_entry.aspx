﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="target_allocation_entry.aspx.cs" Inherits="lead_customer_transaction_target_allocation_entry" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <link href="../../assets/stylesheets/core_theme2.css" rel="stylesheet" type="text/css" />
    <asp:HiddenField runat="server" ID="HiddenFieldDistributorName" />
    <asp:HiddenField runat="server" ID="HiddenFieldDistributorUserName" />
    <asp:HiddenField runat="server" ID="HiddenFieldDistributorID" />
    <asp:HiddenField runat="server" ID="HiddenFieldRouteMasterID" />
    <asp:HiddenField runat="server" ID="HiddenFieldRouteName" />
    <asp:HiddenField runat="server" ID="HiddenFieldRepCode" />
    <asp:HiddenField ID="HiddenFieldDistributorTargets" runat="server" />
    <asp:HiddenField ID="HiddenFieldMarketTargets" runat="server" />
    <asp:HiddenField ID="HiddenFieldRouteTargets" runat="server" />
    <%--    <div class="divcontectmainforms">--%>
    <div class="divcontectmainforms" style="min-height: 600px;">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div>
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/default.aspx">
            <div class="back"></div></asp:HyperLink>
                    </div>
                </div>
            </div>
            <div class="clearall">
            </div>
            <div id="div_message" runat="server" style="display: none;">
            </div>
            <div id="div_info" runat="server" style="display: none">
            </div>
            <div class="grid_container">
                <%--<div class="clearall">
                </div>
                <div class="formtextdiv">
                    Distributor :
                </div>
                <div class="formdetaildiv">
                    <span class="wosub mand">
                        <asp:TextBox ID="txtDistributorName" CssClass="tb" runat="server" Style="width: 200px;
                            text-transform: uppercase"></asp:TextBox>
                        <div runat="server" id="div_autocomplete">
                        </div>
                    </span>
                </div>--%>
                <div class="clearall">
                </div>
                <div class="formtextdiv">
                    Effective Month :
                </div>

                <div class="formdetaildiv">
                        <asp:DropDownList ID="dropDownMonth" runat="server"></asp:DropDownList>
                    <%--<span class="wosub mand">
                        <asp:TextBox ID="StartDate" runat="server"></asp:TextBox>
                    </span>--%>
                </div>
                <div class="clearall">
                </div>
                <%--Hidden--%>
<%--                <div class="formtextdiv">
                    Effective End Date :
                </div>
                <div class="formdetaildiv">
                    <span class="wosub mand">
                        <asp:TextBox ID="EndDate" runat="server"></asp:TextBox>
                    </span>
                </div>
                <div class="clearall">
                </div>--%>
                <%--Hidden--%>
                <div class="formtextdiv">
                    Distributor :
                </div>
                <div class="formdetaildiv">
                    <span class="wosub mand">
                        <asp:TextBox ID="txtDistributorName" CssClass="tb" runat="server" Style="width: 200px;
                            text-transform: uppercase"></asp:TextBox>
                        <div runat="server" id="div_autocomplete">
                        </div>
                    </span>
                </div>
<%--                <div id="div_panel" class="k-content" style="z-index:10000">
                    <div id="vertical" style="top: 60px;">
                        <div id="top-pane">
                            <div id="horizontal" style="height: 100%; width: 100%;">
                                <div id="left-pane">
                                    <div class="pane-content">
                                            <div id="DistributorGrid">
                                            </div>
                                    </div>
                                </div>
                                <div id="center-pane">
                                    <div class="pane-content">
                                            <div id="MarketTargetsGrid">
                                            </div>
                                    </div>
                                </div>
                                <div id="right-pane">
                                    <div class="pane-content">
                                            <div id="RouteTargetsGrid">
                                            </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>--%>
            </div>

            <div class="clearall">
            </div>
            <div id="grid_wrapper">
                <div class="homegridthreefourth_grid">
                    <div id="DistributorGrid">
                    </div>
                </div>
                <div class="homegridthreefourth_grid">
                    <div id="MarketTargetsGrid">
                    </div>
                </div>
                <div class="homegridthreefourth_grid">
                    <div id="RouteTargetsGrid">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function () {
            hideStatusDiv("MainContent_div_message");
            $("#grid_wrapper").hide();
            $("#vertical").kendoSplitter({
                orientation: "vertical",
                panes: [
                            { collapsible: false },
                            { collapsible: false, size: "200px" },
                            { collapsible: false, resizable: false, size: "200px" }
                        ]
            });

            $("#horizontal").kendoSplitter({
                panes: [
                            { collapsible: true, size: "430px" },
                            { collapsible: false, size: "370px" },
                            { collapsible: true, size: "360px" }
                        ]
            });

            $("#MainContent_StartDate").kendoDatePicker({
                //value: new Date(),
                format: "dd-MMM-yyyy"
                // timeFormat: "HH:mm"
            });

            //            $("#MainContent_EndDate").kendoDatePicker({
            //                //value: new Date(),
            //                format: "dd-MMM-yyyy"
            //                // timeFormat: "HH:mm"
            //            });
            if ($('#MainContent_HiddenFieldDistributorID').val() != '') {
                $('#grid_wrapper').show();
                loadDistributor($('#MainContent_HiddenFieldDistributorID').val(), $('#MainContent_dropDownMonth').val());
            }
        });

        $("#MainContent_buttonbar_buttinSave").click(function () {
            targetAllocationSave();
        });
    </script>
    <style scoped>
        #vertical
        {
            height: 300px;
            width: 1200px;
            margin: 0 auto;
            min-height: 400px;
        }
        
        #middle-pane
        {
            background-color: rgba(60, 70, 80, 0.10);
        }
        #bottom-pane
        {
            background-color: rgba(60, 70, 80, 0.15);
        }
        #left-pane, #center-pane, #right-pane
        {
            background-color: #E0ECFF;
            left:500px;
        }
        
        .pane-content
        {
            padding: 0px 12px 0px 0px;
        }
    </style>
</asp:Content>
