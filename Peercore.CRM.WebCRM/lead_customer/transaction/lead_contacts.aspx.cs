﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Services;
using System.Web.UI.WebControls;
//using Peercore.CRM.BusinessRules;
//using Peercore.CRM.Entities.CompositeEntities;
//using Telerik.Web.UI;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using System.Web.UI.HtmlControls;
using Peercore.CRM.Common;
using System.IO;
using System.Runtime.Serialization;
using System.Configuration;
using CRMServiceReference;
using System.Xml.Serialization;
using System.Text;
using System.Xml;
using System.Web.SessionState;

public partial class lead_customer_transaction_lead_contacts : PageBase
{
    public static string GetMapPath { get; set; }
    public const string CLIENT_COLUMN_SETTING = "CLIENT_COLUMN_SETTING";
    public const string CLIENT_SORT_FLAG = "CLIENT_SORT_FLAG";
    public const string CLEAR_FLAG = "CLEAR_FLAG";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
                if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
                {
                    HttpResponse.RemoveOutputCacheItem(ConfigUtil.ApplicationPath + "lead_customer/transaction/lead_contacts.aspx");
                    Response.CacheControl = "no-cache";
                    HiddenFieldIsChecked.Value = "1";
                    HiddenFieldActiveInactiveChecked.Value = "1";
                    HiddenFieldReqSentIsChecked.Value = "0";
                    HiddenFieldActiveReqSent.Value = "1";
                    
                    Session[CLIENT_SORT_FLAG] = false;
                    Session.Remove(CommonUtility.CALL_CYCLE_CONTACTS);
                    BindLeadCustomers();

                    if (Request.QueryString["cht"] != null)
                    {
                        if (Request.QueryString["cht"] != string.Empty)
                        {
                           // Master.SetCommonHeader(Request.QueryString["cht"].ToString());
                        }
                    }
                    else
                    {
                        //Master.SetCommonHeader("Pipeline & Opportunities");
                    }

                    //Remove the session before setting the breadcrumb.
                    Session.Remove(CommonUtility.BREADCRUMB);

                    string headertext = "mSales Outlets ";
                    if (!string.IsNullOrEmpty(Request.QueryString["ty"]))
                    {
                        ArgsDTO args = new ArgsDTO();
                        if (Session != null && Session[CommonUtility.GLOBAL_SETTING] != null)
                        {
                            args = Session[CommonUtility.GLOBAL_SETTING] as ArgsDTO;
                        }

                        args.LeadStage = Request.QueryString["ty"].Replace(" ", "").Trim();
                        Session[CommonUtility.GLOBAL_SETTING] = args;
                        HiddenFieldLeadType.Value = Request.QueryString["ty"].Replace(" ", "");
                        args.ActiveInactiveChecked = HiddenFieldIsChecked.Value == "1";
                        args.ReqSentIsChecked = HiddenFieldReqSentIsChecked.Value == "1";

                        if (Request.QueryString["ty"].ToString() == "pendingcustomer")
                        {
                            headertext += " - Pending Customer Approvals";
                           // toolbar.Visible = false;
                        }
                        else
                        {
                            //headertext += " - " + UppercaseFirst(Request.QueryString["ty"].ToString());
                        }
                    }
                    Master.SetBreadCrumb(headertext, ConfigUtil.ApplicationPath + "pipelines_stage/transaction/opportunity.aspx", "");
                }

                if (BackButtonForAll != null)
                {
                    if (Request.Url != null && Request.UrlReferrer != null && Request.Url.AbsolutePath != Request.UrlReferrer.AbsolutePath)
                    {
                        if ((Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty) ||
                             Request.UrlReferrer.AbsolutePath.Contains("search.aspx"))
                        {
                            List<string> back = (List<string>)BackButtonForAll;
                            BackButtonForAll = null;
                            if (back.Count == 1)
                            {
                                hfPageIndex.Value = (int.Parse(back[0]) + 1).ToString();
                            }
                        }
                    }
                }
            }
            else
            {
                BindLeadCustomers();
            }

            id_delete_customers.Visible = false;
            if (UserSession.Instance.OriginatorString == "ADMIN")
            {
                id_delete_customers.Visible = true;
            }
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        OriginatorString.Value = UserSession.Instance.OriginatorString;
    }

    static string UppercaseFirst(string s)
    {
        // Check for empty string.
        if (string.IsNullOrEmpty(s))
        {
            return string.Empty;
        }
        // Return char and concat substring.
        return char.ToUpper(s[0]) + s.Substring(1);
    }

    protected void excelUpload_Click(object sender, EventArgs e)
    {
        //HttpSessionState session = HttpContext.Current.Session;
        //string sessionID = session.SessionID;
        //List<ColumnSettingDTO> lstColumnSetting = new List<ColumnSettingDTO>();
        //const string CLIENT_SORT_ORDER = "CLIENT_SORT_ORDER";
        //const string CLIENT_SORT_NAME = "CLIENT_SORT_NAME";
        //try
        //{
        //    if (xmlUpload.HasFile)
        //    {
        //        string FileName = Path.GetFileName(xmlUpload.PostedFile.FileName);

        //        string Extension = Path.GetExtension(xmlUpload.PostedFile.FileName);

        //        string FolderPath = HttpContext.Current.Request.PhysicalApplicationPath + "\\uploads"; //ConfigurationManager.AppSettings["FolderPath"];

        //        string[] validFileTypes = { ".xls", ".xlsx" };

        //        //string FilePath = Server.MapPath(FolderPath + FileName);
        //        string FilePath = FolderPath + "\\" + FileName;

        //        bool isValidType = false;
        //        isValidType = validFileTypes.Contains(Extension);

        //        if (!isValidType)
        //        {
        //            //error
        //            ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileValidator()", true);
        //        }
        //        else
        //        {
        //            xmlUpload.SaveAs(FilePath);
        //            CustomerClient customerClient = new CustomerClient();
        //            string UserName = UserSession.Instance.UserName;
        //            if (customerClient.UploadExcelfile(FilePath, Extension, UserName, ASEOriginator, DistOriginator, RepOriginator))
        //            {
        //                Response.Redirect("~/lead_customer/transaction/lead_contacts.aspx?cht=lead&ty=customer");
        //            }
        //            else
        //            {
        //                ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileFormatValidator()", true);
        //            }
        //        }
        //    }
        //}
        //catch (Exception)
        //{

        //    throw;
        //}
    }

    private void BindLeadCustomers()
    {
        if (CurrentUserSettings == null)
            CurrentUserSettings = new ArgsDTO();
        CurrentUserSettings.ChildOriginators = UserSession.Instance.ChildOriginators;
        CurrentUserSettings.Originator = UserSession.Instance.UserName;
        CurrentUserSettings.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        CurrentUserSettings.ActiveInactiveChecked = HiddenFieldIsChecked.Value == "1";
        CurrentUserSettings.ReqSentIsChecked = HiddenFieldReqSentIsChecked.Value == "1";
        CurrentUserSettings.CRMAuthLevel = (!string.IsNullOrWhiteSpace(UserSession.Instance.FilterByUserName)) ? 0 : UserSession.Instance.CRMAuthLevel;
        if (Request.QueryString["ty"] != null)
        {
            CurrentUserSettings.LeadStage = Request.QueryString["ty"].Replace(" ","").Trim();
        }
        else
        {
            CurrentUserSettings.LeadStage = "";
        }

        if (!string.IsNullOrWhiteSpace(UserSession.Instance.FilterByUserName))
        {
            cmbRepType.SelectedValue = UserSession.Instance.FilterByRepType;
        }
        else
        {
            cmbRepType.SelectedValue= UserSession.Instance.RepType;
        }
    }

    protected void xlmUpload_Click(object sender, EventArgs e)
    {
        HttpSessionState session = HttpContext.Current.Session;
        string sessionID = session.SessionID;
        List<ColumnSettingDTO> lstColumnSetting = new List<ColumnSettingDTO>();
        const string CLIENT_SORT_ORDER = "CLIENT_SORT_ORDER";
        const string CLIENT_SORT_NAME = "CLIENT_SORT_NAME";



        try
        {
            if (xmlUpload.HasFile)
            {


                string[] validFileTypes = { "xml" };
                // string message;
                string ext = Path.GetExtension(xmlUpload.FileName);
                bool isValidType = false;

                for (int i = 0; i < validFileTypes.Length; i++)
                {
                    if (ext == "." + validFileTypes[i])
                    {
                        isValidType = true;
                        break;
                    }
                }

                if (!isValidType)
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileValidator()", true);
                }
                else
                {
                    Session[CLIENT_SORT_FLAG] = true;
                    XmlDocument myDoc = new XmlDocument();
                    myDoc.Load(xmlUpload.FileContent);

                    XmlNodeList portNo = myDoc.GetElementsByTagName("ColumnSettingDTO");
                    foreach (XmlNode node in portNo)
                    {
                        XmlElement ColumnSetting = (XmlElement)node;
                        ColumnSettingDTO dtoObj = new ColumnSettingDTO();
                        dtoObj.propertyName = ColumnSetting.GetElementsByTagName("propertyName")[0].InnerText;
                        dtoObj.visible = Convert.ToBoolean(ColumnSetting.GetElementsByTagName("visible")[0].InnerText);
                        dtoObj.header = ColumnSetting.GetElementsByTagName("header")[0].InnerText;
                        dtoObj.uniqueName = ColumnSetting.GetElementsByTagName("uniqueName")[0].InnerText;
                        dtoObj.sessionId = sessionID;
                        Session[CLIENT_SORT_ORDER] = ColumnSetting.GetElementsByTagName("sortType")[0].InnerText;
                        Session[CLIENT_SORT_NAME] = ColumnSetting.GetElementsByTagName("sortName")[0].InnerText;
                        lstColumnSetting.Add(dtoObj);
                    }
                    session[CLIENT_COLUMN_SETTING] = lstColumnSetting;
                }
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    [WebMethod(EnableSession = true)]
    public static string ClearFilters()
    {
        StringBuilder txt = new StringBuilder();

        try
        {
            HttpContext.Current.Session[CLEAR_FLAG] = true;
            txt.Append("set");
        }
        catch (Exception)
        {
            throw;
        }
        return txt.ToString();
    }

    protected void fileuploadBulkDeleteOutlet_Click(object sender, EventArgs e)
    {
        string fileName = fileuploadBulkDeleteOutlet.FileName;
        fileName = "Outlet-" + DateTime.Now.ToString("yyyyMMddHHmmss") + "-" + fileName;

        string errorLog = "";

        string folderPath = "~/uploads/bulkdelete/";
        if (!Directory.Exists(MapPath(folderPath)))
        {
            Directory.CreateDirectory(MapPath(folderPath));
        }

        try
        {
            string filePath = folderPath + fileName;
            string savePath = MapPath(filePath);
            fileuploadBulkDeleteOutlet.SaveAs(savePath);

            if (File.Exists(savePath))
            {
                string[] readText = File.ReadAllLines(savePath);
                StringBuilder strbuild = new StringBuilder();

                CustomerBulkDeleteErrorLog(fileName, "Error log: Outlet Bulk Delete");
                CustomerBulkDeleteErrorLog(fileName, "==============================");

                foreach (string s in readText)
                {
                    try
                    {
                        if (!DeleteCustomerByCusCode(s))
                        {
                            CustomerBulkDeleteErrorLog(fileName, s + " - Error");
                        }
                    }
                    catch (Exception ex) { CustomerBulkDeleteErrorLog(fileName, s + " - " + ex.Message); }
                }

                errorLog = CustomerBulkDeleteErrorLog(fileName, "=============End=============");

                try
                {
                    CommonClient cClient = new CommonClient();
                    cClient.CreateTransactionLog(UserSession.Instance.OriginalUserName,
                        DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                        TransactionTypeModules.Delete,
                        TransactionModules.Customer,
                        UserSession.Instance.OriginalUserName + " bulk delete customer list " + fileName);
                }
                catch { }
            }
        }
        catch (Exception ex)
        {
            errorLog = CustomerBulkDeleteErrorLog(fileName, ex.Message);
        }

        linkBulkDeleteErrorLog.NavigateUrl = errorLog;
        linkBulkDeleteErrorLog.Visible = true;
    }

    private bool DeleteCustomerByCusCode(string custCode)
    {
        try
        {
            CommonClient commService = new CommonClient();
            CustomerDTO delCustomer = new CustomerDTO();

            delCustomer.CustomerCode = custCode;
            delCustomer.CreatedBy = UserSession.Instance.UserName;
            bool status = commService.DeleteCustomerByCusCode(delCustomer);

            return status;
        }
        catch (Exception)
        {
            return false;
        }
    }

    private string CustomerBulkDeleteErrorLog(string fileName, string msg)
    {
        string folderPath = "~/docs/errorlogs/";
        if (!Directory.Exists(MapPath(folderPath)))
        {
            Directory.CreateDirectory(MapPath(folderPath));
        }

        string filePath = folderPath + fileName;
        string savePath = MapPath(filePath);

        File.AppendAllText(savePath,
                   msg + Environment.NewLine);

        return filePath;
    }
}