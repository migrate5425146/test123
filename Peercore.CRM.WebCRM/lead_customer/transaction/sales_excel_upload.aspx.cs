﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CRMServiceReference;
using System.IO;
using Peercore.CRM.Shared;
using System.Web.SessionState;
using System.Data;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data.OleDb;

public partial class lead_customer_transaction_sales_excel_upload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Master.SetBreadCrumb("Sales Territory Target Upload ", "#", "");
    }
    
    protected void excelUpload_Click(object sender, EventArgs e)
    {
        HttpSessionState session = HttpContext.Current.Session;
        string sessionID = session.SessionID;
        List<ColumnSettingDTO> lstColumnSetting = new List<ColumnSettingDTO>();
        const string CLIENT_SORT_ORDER = "CLIENT_SORT_ORDER";
        const string CLIENT_SORT_NAME = "CLIENT_SORT_NAME";

        try
        {
            if (xmlUpload.HasFile)
            {
                string FileName = Path.GetFileName(xmlUpload.PostedFile.FileName);
                string Extension = Path.GetExtension(xmlUpload.PostedFile.FileName);
                string FolderPath = HttpContext.Current.Request.PhysicalApplicationPath + "\\uploads"; string[] validFileTypes = { ".xls", ".xlsx" };

                string FilePath = FolderPath + "\\" + FileName;

                bool isValidType = false;
                isValidType = validFileTypes.Contains(Extension);

                if (!isValidType)
                {
                    //error
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileValidator()", true);
                }
                else
                {
                    xmlUpload.SaveAs(FilePath);
                    //CustomerClient customerClient = new CustomerClient();

                    SalesClient salesClient = new SalesClient();
                    string UserName = UserSession.Instance.UserName;
                    int ret = salesClient.UploadSalesTargets(FilePath, Extension, UserName);

                    if (ret > 0)
                    {
                        Response.Write("<script>alert('Successfully " + ret + " Records Uploaded!');</script>");
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileFormatValidator()", true);
                    }
                }
            }
        }
        catch (Exception)
        {

            throw;
        }
    }
    protected void btnReview_Click(object sender, EventArgs e)
    {

    }

    //--using openXml
    public static string GetCellValue(SpreadsheetDocument document, Cell cell)
    {
        SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
        string value = cell.CellValue.InnerXml;

        if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
        {
            return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
        }
        else
        {
            return value;
        }
    }
}

