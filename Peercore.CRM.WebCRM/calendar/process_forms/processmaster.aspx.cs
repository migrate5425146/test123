﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Peercore.CRM.Entities;
using System.Text;
//using Peercore.CRM.BusinessRules;
using Peercore.CRM.Shared;
//using Peercore.CRM.Entities.CompositeEntities;
using CRMServiceReference;
using System.Drawing;
using Peercore.CRM.Common;

public partial class calendar_process_forms_processmaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != "")
        {
            if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
            {
                if (Request.QueryString["type"] == "insert")
                {
                    if (Request.QueryString["fm"] == "calendar")
                    {
                        //Response.Expires = -1;
                        //Response.Clear();
                        //string calendarStatus = InsertCalendar();
                        //Response.ContentType = "text/html";
                        //Response.Write(calendarStatus);
                        //Response.End();
                    }
                }
                if (Request.QueryString["type"] == "update")
                {
                    if (Request.QueryString["fm"] == "calendar")
                    {
                        //Response.Expires = -1;
                        //Response.Clear();
                        //string calendarStatus = UpdateCalendar();
                        //Response.ContentType = "text/html";
                        //Response.Write(calendarStatus);
                        //Response.End();
                    }
                }
                if (Request.QueryString["type"] == "Delete")
                {

                    if (Request.QueryString["fm"] == "appointment")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string deleteActivity = DeleteAppointment(int.Parse(Request.QueryString["id"].ToString()));
                        Response.ContentType = "text/html";
                        Response.Write(deleteActivity);
                        Response.End();
                    }
                    if (Request.QueryString["fm"] == "activity")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string deleteActivity = DeleteActivity(int.Parse(Request.QueryString["id"].ToString()));
                        Response.ContentType = "text/html";
                        Response.Write(deleteActivity);
                        Response.End();
                    }

                    if (Request.QueryString["fm"] == "holiday")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string deleteActivity = DeleteHoliday(int.Parse(Request.QueryString["holidayid"].ToString()));
                        Response.ContentType = "text/html";
                        Response.Write(deleteActivity);
                        Response.End();
                    }
                }

                if (Request.QueryString["type"] == "get")
                {
                    if (Request.QueryString["fm"] == "holiday")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getHolidayType = GetColorCode(Request.QueryString["HolidayType"].ToString());
                        Response.ContentType = "text/html";
                        Response.Write(getHolidayType);
                        Response.End();
                    }
                }
            }
        }
    }


    public string DeleteAppointment(int appointmentId)
    {
        bool iNoofRecs = false;
        string rval = "";
        CommonUtility commonUtilityPara = new CommonUtility();
        CustomerActivitiesDTO customerActivities = new CustomerActivitiesDTO();
        ActivityDTO activity = new ActivityDTO();
        //activity.ActivityID = appointmentId;
        ActivityClient oActivity = new ActivityClient();
        AppointmentClient oAppointment = new AppointmentClient();

        AppointmentDTO crmAppointment = new AppointmentDTO();
        crmAppointment.AppointmentID = appointmentId;
        iNoofRecs = oAppointment.AppointmentUpdate(crmAppointment, false, true);

        if (iNoofRecs)
        {
            //customerActivities = oActivity.GetActivityAppoitmentId(activity.ActivityID);
            activity = oActivity.GetActivityByAppoitmentId(appointmentId);
      //      activity.AppointmentID = customerActivities.AppointmentId;
            iNoofRecs = oActivity.ResetAppointment(activity);

            if (activity == null || activity.ActivityID <= 0)
                rval = commonUtilityPara.GetSucessfullMessage("Successfully deleted.");
            else
            {
                rval = "1";
            }

        }
        else
            rval = commonUtilityPara.GetSucessfullMessage("Delete failed.");
        //}
        return rval;
    }

    public string DeleteActivity(int appointmentId)
    {
        string rval = "";
        CommonUtility commonUtilityPara = new CommonUtility();
        ActivityDTO activity = new ActivityDTO();
        activity.ActivityID = appointmentId;
        ActivityClient oActivity = new ActivityClient();

        CustomerActivitiesDTO customerActivities = new CustomerActivitiesDTO();
        AppointmentClient oAppointment = new AppointmentClient();
        customerActivities = oActivity.GetActivityAppoitmentId(activity.ActivityID);

        bool iNoofRecs = oActivity.DeleteActivity(activity);
        if (iNoofRecs)
        {
            rval = commonUtilityPara.GetSucessfullMessage("Successfully deleted.");
        }
        else
        {
            rval = commonUtilityPara.GetSucessfullMessage("Delete failed.");
        }
        return rval;
    }

    public string DeleteHoliday(int holidayId)
    {
        string rval = "";
        CommonUtility commonUtilityPara = new CommonUtility();
        CommonClient commonClient = new CommonClient();

        bool iNoofRecs = commonClient.DeleteHoliday(holidayId);
        if (iNoofRecs)
        {
            rval = commonUtilityPara.GetSucessfullMessage("Successfully deleted.");
        }
        else
        {
            rval = commonUtilityPara.GetSucessfullMessage("Delete failed.");
        }
        return rval;
    }

    private string GetColorCode(string holidayType)
    {
        string title = Request.QueryString["title"].ToString();
        CommonClient commonClient = new CommonClient();

        if (title.Contains("div_title"))
        {
            int index = title.LastIndexOf("</a>");
            if (index > 0)
                title = title.Substring(0, index) + "</a>";
        }

        CalendarHolidayDTO item = commonClient.GetHolidayTypeByCode(holidayType);

        string txt = title + "</div> <div id='div_title' class='shedular_title' style='background-color:" + item.TypeColor + "'>" + item.TypeDescription +"-"+ item.Description + "";
        return txt;
    }



    //private string InsertCalendar()
    //{
    //    int iNoRecs = 0;
    //    int iRow = 0;
    //    StringBuilder txt = new StringBuilder();
    //    brAppointment oAppointment = new brAppointment();
    //    CRMAppointment appointment = new CRMAppointment();

    //    appointment.Subject = Convert.ToString(Request.QueryString["subject"]);
    //    appointment.Body = Convert.ToString(Request.QueryString["description"]);

    //    string strartDate = Convert.ToString(Request.QueryString["startDate"]);
    //    string[] split = strartDate.Split(new Char[] { '-' });
    //    DateTime strartDateTime = Convert.ToDateTime(split[0] + "/" + split[1] + "/" + split[2] + " " + split[3] + ":" + split[4] + ":" + split[5]);
    //    appointment.StartTime = strartDateTime;

    //    string endDate = Convert.ToString(Request.QueryString["endDate"]);
    //    string[] split1 = endDate.Split(new Char[] { '-' });
    //    DateTime endDateTime = Convert.ToDateTime(split1[0] + "/" + split1[1] + "/" + split1[2] + " " + split1[3] + ":" + split1[4] + ":" + split1[5]);
    //    appointment.EndTime = endDateTime;

    //    appointment.Category = "COLD";
    //    appointment.CreatedBy = UserSession.Instance.UserName;

    //    try
    //    {
    //        iNoRecs = oAppointment.Insert(appointment);

    //        if (iNoRecs > 0)
    //        {
    //            if (Request.QueryString["originators"] != "")
    //            {
    //                string originators = Convert.ToString(Request.QueryString["originators"]);
    //                string[] split2 = originators.Split(new Char[] { '/' });
    //                AppointmentResource[] oAppResource = new AppointmentResource[split2.Length-1];

    //                foreach (string resource in split2)
    //                {
    //                    if (resource != "")
    //                    {
    //                        oAppResource[iRow] = new AppointmentResource();
    //                        oAppResource[iRow].AppointmentID = Convert.ToInt32(appointment.AppointmentID);
    //                        oAppResource[iRow].ResourceID = Convert.ToInt32(resource);
    //                        iRow++;
    //                    }
    //                }

    //                iNoRecs = oAppointment.InsertResources(oAppResource);

    //            }

    //            txt.Append("success");
    //        }
    //        else
    //        {
    //            txt.Append("error");
    //        }
    //    }
    //    catch (Exception)
    //    {

    //        throw;
    //    }
    //    return txt.ToString();
    //}

    //private string UpdateCalendar()
    //{
    //    StringBuilder txt = new StringBuilder();
    //    brAppointment oAppointment = new brAppointment();
    //    CRMAppointment appointment = new CRMAppointment();
    //    CDataHandle.Activity activity = new CDataHandle.Activity();
    //    brActivity oActivity = new brActivity();
    //    int iNoRecs = 0;
    //    int iRow = 0;

    //    CommonUtility commonUtility = new CommonUtility();

    //    try
    //    {

    //        appointment.Subject = Convert.ToString(Request.QueryString["subject"]);
    //        appointment.Body = Convert.ToString(Request.QueryString["description"]);

    //        string strartDate = Convert.ToString(Request.QueryString["startDate"]);
    //        string[] split = strartDate.Split(new Char[] { '-' });
    //        DateTime strartDateTime = Convert.ToDateTime(split[0] + "/" + split[1] + "/" + split[2] + " " + split[3] + ":" + split[4] + ":" + split[5]);
    //        appointment.StartTime = strartDateTime;

    //        string endDate = Convert.ToString(Request.QueryString["endDate"]);
    //        string[] split1 = endDate.Split(new Char[] { '-' });
    //        DateTime endDateTime = Convert.ToDateTime(split1[0] + "/" + split1[1] + "/" + split1[2] + " " + split1[3] + ":" + split1[4] + ":" + split1[5]);
    //        appointment.EndTime = endDateTime;

    //        appointment.Category = Convert.ToString(Request.QueryString["activityColor"]);
    //        appointment.AppointmentID = Convert.ToInt32(Request.QueryString["appoID"]);


    //        activity.StartDate = strartDateTime;
    //        activity.EndDate = endDateTime;
    //        activity.AppointmentID = appointment.AppointmentID;
    //        activity.LastModifiedBy = UserSession.Instance.UserName;
    //        activity.LastModifiedDate = DateTime.Now;

    //        iNoRecs = oAppointment.Update(appointment);

    //        if (iNoRecs > 0)
    //        {
    //            oActivity.ChangeActivityDate(activity, true);
    //        }

    //        if (iNoRecs > 0)
    //        {
    //            if (Request.QueryString["originators"] != "")
    //            {
    //                oAppointment.DeleteResources(appointment);

    //                string originators = Convert.ToString(Request.QueryString["originators"]);
    //                string[] split2 = originators.Split(new Char[] { '/' });
    //                AppointmentResource[] oAppResource = new AppointmentResource[split2.Length - 1];

    //                foreach (string resource in split2)
    //                {
    //                    if (resource != "")
    //                    {
    //                        oAppResource[iRow] = new AppointmentResource();
    //                        oAppResource[iRow].AppointmentID = Convert.ToInt32(appointment.AppointmentID);
    //                        oAppResource[iRow].ResourceID = Convert.ToInt32(resource);
    //                        iRow++;
    //                    }
    //                }

    //                oAppointment.InsertResources(oAppResource);
    //            }
    //            txt.Append("success");
    //        }
    //        else
    //        {
    //            txt.Append("error");
    //        }
    //    }
    //    catch (Exception)
    //    {
    //        throw;
    //    }
    //    return txt.ToString();
    //}

}