﻿<%@ Page Title="mSales - Holiday Entry" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="holiday_entry.aspx.cs" Inherits="calendar_transaction_holiday_entry" %>

<%@ Register Src="~/usercontrols/buttonbar_navigation.ascx" TagPrefix="ucl" TagName="buttonbar" %>

<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
    <script id="editor" type="text/x-kendo-template">
    <div class = "k-edit-form">
    <div class ="k-edit-field">
<h3>Holiday Entry</h3></div>
<div class ="k-edit-label">Title: </div><div class ="k-edit-field"><input name="title" style="width:220px" required/></div> 
<div class ="k-edit-label">Holiday Type: </div>
 <div id="container" class ="k-edit-field">
<input name="holidaytype" data-role="dropdownlist" data-source="dataSource" data-text-field="typedescription" data-value-field="type" />  </div>
<div class ="k-edit-label">Date: </div><div class ="k-edit-field"><input data-role="datepicker" name="start" style="width:220px" /></div>

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">


    <asp:HiddenField ID="HiddenFieldLastEnteredDate" runat="server" />
    <asp:HiddenField ID="HiddenFieldNavigatedDate" runat="server" />
    <asp:HiddenField ID="hfHolidayDateChangedFrom" runat="server" />
    <asp:HiddenField ID="hfHolidayDateChangedTo" runat="server" />
    <asp:HiddenField ID="HiddenFieldOriginator" runat="server" />
    <div id="modalWindow" style="display: none">
        <div id="div_confirm_message">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
        <button id="save" class="k-button">
            Save</button>
    </div>
    <div class="divcontectmainforms">
        <%-- 			<div id="schedulerholiday">

            </div>--%>
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div style="float: left" id="div_btn_update_route_planner" runat="server" visible="false">
                    <asp:Button id="btn_update_route_planner" runat="server" Height="30px" CssClass="update_route_planner"
                        Text="Update Route Planner" onclick="btn_update_route_planner_Click">
                    </asp:Button>
                    <%--<a class="show_route" id="btn_update_route_planner" clientidmode="Static" runat="server"
                        onclick="btn_update_route_planner_Click">Update Route Planner</a>--%>
                </div>
<%--                <div class="hoributton" style="float: right;" align="right">
                   
                </div>--%>

                <span id="span_cmbase" runat="server" visible="true">
                    <asp:Label ID="Label1" runat="server" Text="Type" Visible="false"></asp:Label>
                    <asp:DropDownList ID="cmbase" runat="server" Visible="true" AutoPostBack="true" Width="190px"
                        OnSelectedIndexChanged="cmboase_SelectedIndexChanged">
                        <%--<asp:ListItem Text="Distributor" Value="dis" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="ASE" Value="ase"></asp:ListItem>--%>
                    </asp:DropDownList>
                </span>
                
                <span id="span_cmbdis" runat="server" visible="false">
                    <asp:Label ID="Label4" runat="server" Text="Type" Visible="false"></asp:Label>
                    <asp:DropDownList ID="cmbdis" runat="server" Visible="true" AutoPostBack="true" Width="190px"
                        OnSelectedIndexChanged="cmbodis_SelectedIndexChanged">
                        <%--    <asp:ListItem Text="Distributor" Value="dis" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="ASE" Value="ase"></asp:ListItem>--%>
                    </asp:DropDownList>
                </span>
                
                <span id="span_cmbrep" runat="server" visible="false">
                    <asp:Label ID="Label3" runat="server" Text="Type" Visible="false"></asp:Label>
                    <asp:DropDownList ID="cmbrep" runat="server" Visible="true" AutoPostBack="true" Width="190px" OnSelectedIndexChanged="cmborep_SelectedIndexChanged">
                        <%--  <asp:ListItem Text="rep" Value="dis" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="j" Value="ase"></asp:ListItem>--%>
                    </asp:DropDownList>
                </span>

            </div>
            <div class="toolbar_right1" id="div3">
                <div class="leadentry_title_bar">
<%--                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
            <div class="back"></div></asp:HyperLink>--%>
            <ucl:buttonbar ID="buttonbar" runat="server" />

                        <%--                        <asp:HyperLink ID="HyperLinkNext" runat="server" NavigateUrl="~/call_cycle/transaction/route_entry.aspx">
            <div class="back"></div></asp:HyperLink>--%>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="activityColor" runat="server">
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: block;">
        </div>
        <div class="clearall">
        </div>
        <div id="scheduler">
        </div>
    </div>
    <script type="text/javascript">

        function call() {
            originator('activity_planner/process_forms/processmaster.aspx?fm=activityentry&type=query&querytype=originator');
            // alert("OK");
        }

        //            $("#id_assign").click(function () {
        //                originator('activity_planner/process_forms/processmaster.aspx?fm=activityentry&type=query&querytype=originator');
        //                //originator('lead_customer/process_forms/processmaster.aspx?fm=leadentry&type=query&querytype=originator');
        //            });
    </script>
    <script type="text/javascript">

        //            $(function () {
        //            debugger;
        //                window.APP = window.APP || {};
        //                window.APP.HolidayTypes = new kendo.data.ObservableArray([]);

        //                var types = new kendo.data.DataSource({
        //                    transport: {
        //                    url: ROOT_PATH+ "service/calendar/calendar_service.asmx/GetHolidayTypes",
        //                    },

        //                    change: function  (e) {
        //                        $.each(this.view(),function(){
        //                            window.APP.HolidayTypes.push(this);
        //                        });
        //                    }
        //                });

        //            });
        //            var dataSource = new kendo.data.DataSource({
        //                transport: {
        //                    read: {
        //                        url: ROOT_PATH + "service/calendar/calendar_service.asmx/GetHolidayTypes",
        //                         contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
        //                         type: "POST"
        //                    },
        //                     parameterMap: function (data, operation) {
        //                         // data = $.extend( data);
        //                         return JSON.stringify(data);
        //                     }
        //                 },
        //                 schema: {
        //                     id: "HolidayId",
        //                     model: {
        //                         fields: {
        //                             taskId: { from: "HolidayId", type: "number" },                   
        //                             type: {  type: "string", from: "HolidayType" },
        //                             typedescription: {  type: "string", from: "TypeDescription" }
        //                         }
        //                     }
        //                 }
        //             });


        //            var viewModel = new kendo.observable({
        //                dataSource: new kendo.data.DataSource({
        //                    data: [{
        //                        id: 1,
        //                        text: "Wellness Exam"
        //                    }, {
        //                        id: 2,
        //                        text: "Diagnostic Exam"
        //                    }, {
        //                        id: 3,
        //                        text: "Nail Trim"
        //                    }]
        //                })
        //            });
        //            kendo.bind($("#containerx"), viewModel);

        //            var dataSource = new kendo.data.DataSource({
        //                data: ["One", "Two"]
        //            });

        var dataSource = new kendo.data.DataSource({
            data: [{
                type: "POYA",
                typedescription: "Poya Day"
            }, {
                type: "MERC",
                typedescription: "Merchantile Holiday"
            }, {
                type: "BANK",
                typedescription: "Bank Holiday"
            },{
                type: "RONL",
                typedescription: "Rep On Leave"
            }, {
                type: "OTHE",
                typedescription: "Other"
            }]
        });

        //                        var dataSource = new kendo.data.DataSource({
        //                            transport: {
        //                                read: {
        //                                    url: ROOT_PATH + "service/calendar/calendar_service.asmx/GetHolidayTypes",
        //                                     contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
        //                                     type: "POST"
        //                                }
        //                             },
        //                             schema: {
        //                                 id: "HolidayId",
        //                                 model: {
        //                                     fields: {
        //                                         taskId: { from: "HolidayId", type: "number" },                   
        //                                         type: {  type: "string", from: "HolidayType" },
        //                                         typedescription: {  type: "string", from: "TypeDescription" }
        //                                     }
        //                                 }
        //                             }
        //                         });

        // kendo.init($("#container"));

        //            jQuery(function () { jQuery("\\#appointmentTypeDropDownList").kendoDropDownList({ dataSource: dataSource, dataTextField: "TypeDescription", dataValueField: "HolidayType" }); });


        $(document).ready(function () {
            hideStatusDiv("MainContent_div_message");
            //  var view = $("#scheduler").data("kendoScheduler").view();
            //   var view = scheduler.view();
            //   var startdate = view.startDate();
            //  var enddate = view.startDate();
            $("#MainContent_buttonbar_hlBack").css("display", "none");

            var date = new Date();
            loadHolidaySchedular((new Date(date.getFullYear(), date.getMonth(), 1)), (new Date(date.getFullYear(), date.getMonth() + 1)), $("#MainContent_HiddenFieldOriginator").val(), new Date());
        });

        //            function onNavigate(e) {
        //                alert(e.date);
        //            }


    </script>
    <%--        <script type="text/javascript">
            function onNavigate(e) {
                alert(e.date);
            }

         </script>--%>
</asp:Content>
