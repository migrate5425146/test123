﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Telerik.Web.UI;
using System.Web.UI.HtmlControls;
using System.Text;
using CRMServiceReference;
using Telerik.Charting;
using System.Drawing;
using System.Data;
using System.Web.Services;
using System.Configuration;
using System.Net.Mail;
using System.Net.Mime;
using Peercore.CRM.Common;

public partial class calendar_transaction_appointment_entry : PageBase
{
    #region - List -

    List<ActivityDTO> activityTypeList = new List<ActivityDTO>();
    List<AppointmentDTO> lstAppointment = new List<AppointmentDTO>();
    List<AppointmentDTO> appointmentList = new List<AppointmentDTO>();
    public const string APPOINTMENT_DELETE = "APPOINTMENT_DELETE";
    public const string ACTIVITY_DELETE = "ACTIVITY_DELETE";
    int i = 0;
    public static CommonUtility commonUtility = null;
    CommonUtility commonUtilityPara = new CommonUtility();

    #endregion

    #region - Constructor -

    protected void Page_Load(object sender, EventArgs e)
    {
        RadScheduler.FormCreated += new SchedulerFormCreatedEventHandler(RadScheduler_FormCreated);

        if (string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }


        //RadScheduler.TimeZoneID = System.TimeZone.CurrentTimeZone.StandardName;

        if (!IsPostBack)
        {
            BindLeadCustomers();
            LoadUI();
            LoadResource();
            setDefaultDate();
            //LoadSchedulerData();

            ActivityClient activityClient = new ActivityClient();
            List<ActivityDTO> activity = activityClient.GetAllActivityTypes(UserSession.Instance.DefaultDepartmentId, true);
            //int key = 0;
            RadScheduler.ResourceStyles.Clear();
            foreach (ActivityDTO item in activity)
            {
                ResourceStyleMapping resourceStyleMapping = new ResourceStyleMapping();
                resourceStyleMapping.Type = "ActivityTypes";
                resourceStyleMapping.Text = item.Code;
                resourceStyleMapping.Key = item.Code;
                resourceStyleMapping.BackColor = ColorTranslator.FromHtml(item.ActivityTypeColour);
                RadScheduler.ResourceStyles.Add(resourceStyleMapping);
                // key++;
            }

            LoadOptions();


            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Calendar", ConfigUtil.ApplicationPath + "/calendar/transaction/appointment_entry.aspx?cht=calendar", "");

       
        }

  //      RadScheduler.TimeZoneID = System.TimeZone.CurrentTimeZone.StandardName;

        if (Request.QueryString["fm"] == "home")
        {
            if (!string.IsNullOrEmpty(Request.QueryString["sdate"]))
            {
                RadScheduler.SelectedView = SchedulerViewType.DayView;

                RadScheduler.SelectedDate = DateTime.Parse(Request.QueryString["sdate"]);
            }
        }

        if (Request.QueryString["cht"] != null)
        {
            if (Request.QueryString["cht"] != string.Empty)
            {
                //Master.SetCommonHeader(Request.QueryString["cht"].ToString());
            }
        }
        else
        {
            //Master.SetCommonHeader("home");
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        RadScheduler.TimeZoneID = TimeZoneInfo.Local.Id;
    }


    void RadScheduler_FormCreated(object sender, SchedulerFormCreatedEventArgs e)
    {


        ActivityClient oBrActivityType = new ActivityClient();
        List<CustomerActivitiesDTO> cActivity = new List<CustomerActivitiesDTO>();

        List<ActivityDTO> lstActivityType = oBrActivityType.GetAllActivityTypes(UserSession.Instance.DefaultDepartmentId, true);
        StringBuilder txt = new StringBuilder();

        txt.Append("<table id=\"color_tble\">");

        foreach (ActivityDTO obj in lstActivityType)
        {
            txt.Append("<tr>");
            txt.Append("<td>");
            txt.Append("<input type=\"radio\" name=\"group1\" value=" + obj.Code + " id=\"a\" onclick=\"onclickActivity()\" style=\"float:left;\">");
            Color activityColor = System.Drawing.ColorTranslator.FromHtml(obj.ActivityTypeColour);
            String rtn = "#" + activityColor.R.ToString("X2") + activityColor.G.ToString("X2") + activityColor.B.ToString("X2");
            txt.Append("<div style=\"background-color:" + rtn + ";height:20px;width:20px;float:right;\" runat=\"server\"> </div>");
            txt.Append("<label style=\"width:200px;\">" + obj.Description + "</label>");
            txt.Append("</td>");
            txt.Append("</tr>");
        }
        txt.Append("</table>");

        HtmlGenericControl colorDiv = (HtmlGenericControl)e.Container.FindControl("div_color");
        colorDiv.InnerHtml = txt.ToString();

        if (e.Container.Mode == SchedulerFormMode.Insert || e.Container.Mode == SchedulerFormMode.AdvancedInsert)
        {
            RadDateTimePicker startDate = (RadDateTimePicker)e.Container.FindControl("StartInput");
            startDate.SelectedDate = e.Appointment.Start;
            RadDateTimePicker endDate = (RadDateTimePicker)e.Container.FindControl("EndInput");
            endDate.SelectedDate = e.Appointment.End;
        }
        if (e.Container.Mode == SchedulerFormMode.Edit || e.Container.Mode == SchedulerFormMode.AdvancedEdit)
        {
            Telerik.Web.UI.Appointment appoinment = e.Appointment as Telerik.Web.UI.Appointment;
            ActivityDTO activityDescription = lstActivityType.Find(c => c.ActivityTypeColour == ("#" + appoinment.BackColor.Name).ToUpper());

            cActivity = ViewState["cActivity"] as List<CustomerActivitiesDTO>;
            CustomerActivitiesDTO customerAct = cActivity.Find(x => x.ActivityID == Convert.ToInt32(appoinment.ID));

            if (customerAct != null)
            {
                HiddenField appoIdCRM = (HiddenField)e.Container.FindControl("txt_appoIdCRM");
                appoIdCRM.Value = customerAct.AppointmentId.ToString();

                Label activityHedding = (Label)e.Container.FindControl("lbl_hedding");
                activityHedding.Text = appoinment.Subject;

                Label activitydescription = (Label)e.Container.FindControl("lbl_description");
                activitydescription.Text = activityDescription.Description;

                HiddenField appoId = (HiddenField)e.Container.FindControl("txt_appoId");
                appoId.Value = appoinment.ID.ToString();

                TextBox subject = (TextBox)e.Container.FindControl("txtSubject");
                subject.Text = appoinment.Subject;

                TextBox description = (TextBox)e.Container.FindControl("txtDescription");
                description.Text = appoinment.Description;

                RadDateTimePicker startDate = (RadDateTimePicker)e.Container.FindControl("StartInput");
                startDate.SelectedDate = appoinment.Start;

                RadDateTimePicker endDate = (RadDateTimePicker)e.Container.FindControl("EndInput");
                endDate.SelectedDate = appoinment.End;

                HtmlGenericControl colorType = (HtmlGenericControl)e.Container.FindControl("div_color_type");
                Color activityColor = System.Drawing.ColorTranslator.FromHtml("#" + appoinment.BackColor.Name);
                String rtn = "#" + activityColor.R.ToString("X2") + activityColor.G.ToString("X2") + activityColor.B.ToString("X2");
                colorType.Style["background-color"] = rtn;
            }
        }
      
    }
    


    #endregion

    #region - Event -

    protected void radRepTerritory_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (radRepTerritory.SelectedValue == null)
                radRepTerritory.SelectedValue = "- All -";

            GetScheduledActivities( !radRepTerritory.SelectedValue.ToString().Equals("- All -") ?
                radRepTerritory.SelectedValue.ToString() :
                string.Empty);
        }
        catch (Exception ex)
        {
        }

    }

    public void GetScheduledActivities(string repTerritory = "")
    {
        try
        {
            if (string.IsNullOrEmpty(repTerritory))
                CurrentUserSettings.AdditionalParams = "";
            else
                CurrentUserSettings.AdditionalParams = " state = '" + repTerritory + "'";
            RadScheduler.Rebind();
        }
        catch (Exception oException)
        {
            
        }
    }

    protected void btnOpenActivity_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("../../activity_planner/transaction/activityentry.aspx");
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void btnPrint_Click(object sender, EventArgs e)
    {
        //string startDateTime = startDate.Value+ " 00:00:00";
        //string endDateTime = endDate.Value + " 23:59:59";
        string startDateTime = Server.UrlDecode(startDate.Value + " 00:00:00");
        string endDateTime = Server.UrlDecode(endDate.Value + " 23:59:59");

        try
        {
            Response.Redirect("../../calendar/Reports/AppointmentViewer.aspx?start=" + startDateTime + "&end=" + endDateTime);
        }
        catch (Exception)
        {
            throw;
        }
    }

    protected void RadScheduler_AppointmentDataBound(object sender, SchedulerEventArgs e)
    {
        Telerik.Web.UI.Appointment appointmentColor = e.Appointment.DataItem as Telerik.Web.UI.Appointment;
        if (appointmentColor == null)
        {
            if (appointmentList[i].CreatedBy == UserSession.Instance.UserName)
            {
                e.Appointment.AllowDelete = true;
                e.Appointment.AllowEdit = true;
            }
            else
            {
                e.Appointment.AllowDelete = false;
                e.Appointment.AllowEdit = false;
            }
            i++;
            return;
        }
        else
        {
            Color activityColor = appointmentColor.BackColor;
            e.Appointment.BackColor = activityColor;
            if (appointmentList[i].CreatedBy == UserSession.Instance.UserName)
            {
                e.Appointment.AllowDelete = true;
                e.Appointment.AllowEdit = true;
            }
            else
            {
                e.Appointment.AllowDelete = false;
                e.Appointment.AllowEdit = false;
            }
        }
        i++;
    }

    protected void radTimeRang_SelectedIndexChanged(object sender, EventArgs e)
    {
        //LoadSchedulerData();
    }

    #endregion - Event -

    #region - Private Method -

    private void BindLeadCustomers()
    {
        if (CurrentUserSettings == null)
            CurrentUserSettings = new ArgsDTO();
        CurrentUserSettings.ChildOriginators = UserSession.Instance.ChildOriginators;
        CurrentUserSettings.Originator = UserSession.Instance.UserName;
        CurrentUserSettings.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
        CurrentUserSettings.RepType = UserSession.Instance.RepType;

        CurrentUserSettings.LeadStage = "";
        CurrentUserSettings.AdditionalParams = "";
        CurrentUserSettings.ActiveInactiveChecked = true;

        CurrentUserSettings.CRMAuthLevel = (!string.IsNullOrWhiteSpace(UserSession.Instance.FilterByUserName)) ? 0 : UserSession.Instance.CRMAuthLevel;
    }

    private void LoadOptions()
    {
        CommonClient commonClient = new CommonClient();
        List<LookupTableDTO> lstLookupTableDTO = new List<LookupTableDTO>();
        ArgsDTO args = new ArgsDTO();

        try
        {
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
            args.ChildOriginators = UserSession.Instance.ChildOriginators;

            lstLookupTableDTO = commonClient.GetRepTerritories(args);
            lstLookupTableDTO.Insert(0, new LookupTableDTO() { TableCode = "- All -", TableDescription = "- All -" });


            radRepTerritory.DataSource = lstLookupTableDTO;
            radRepTerritory.DataTextField = "TableCode";
            radRepTerritory.DataValueField = "TableCode";
            radRepTerritory.DataBind();




        }
        catch (Exception)
        {

            throw;
        }
    }

    private DateTime FloorDate(string selectedDate)
    {
        DateTime Date = new DateTime();

        if (selectedDate == "2")
        {
            Date = DateTime.Now.AddMonths(-2);
        }
        else if (selectedDate == "6")
        {
            Date = DateTime.Now.AddMonths(-6);
        }
        else if (selectedDate == "1")
        {
            Date = DateTime.Now.AddMonths(-12);
        }
        else if (selectedDate == "0")
        {
            Date = DateTime.Now.AddMonths(-0);
        }

        return Date;
    }

    private void setDefaultDate()
    {
        DayOfWeek weekStart = DayOfWeek.Monday;
        DateTime startingDate = DateTime.Today;

        while (startingDate.DayOfWeek != weekStart)
            startingDate = startingDate.AddDays(-1);

        startDate.Value = startingDate.ToString("dd-MMM-yyyy");

        //while (startingDate.DayOfWeek != weekStart)
        //    startingDate = startingDate.AddDays(-1);

        DateTime endOfWeek = startingDate.AddDays(6);
        endDate.Value = endOfWeek.ToString("dd-MMM-yyyy"); ;

    }

    protected void LoadUI()
    {
        List<KeyValuePair<int, string>> lstPeriod = new List<KeyValuePair<int, string>>();
        lstPeriod = GetTimePeriods();
        foreach (KeyValuePair<int, string> ValuePair in lstPeriod)
        {
            radTimeRang.Items.Add(new ListItem(ValuePair.Value.ToString(), ValuePair.Key.ToString()));
        }
        radTimeRang.SelectedIndex = 0;
    }

    private void LoadResource()
    {
        List<OriginatorDTO> lstOriginator = new List<OriginatorDTO>();
        OriginatorClient OriginatorClient = new OriginatorClient();
        ActivityClient ActivityClient = new ActivityClient();
        StringBuilder txt = new StringBuilder();
        ArgsDTO args = new ArgsDTO();

        try
        {
            args.Originator = UserSession.Instance.Originator;
            args.ManagerMode = UserSession.Instance.ManagerMode;
            activityTypeList = ActivityClient.GetAllActivityTypes(UserSession.Instance.DefaultDepartmentId, true);
            ViewState["activityType"] = activityTypeList;

            txt.Append("<div style=\"min-width:200px; overflow:auto\">");
            txt.Append("<table border=\"0\" bgcolor=\"#3a3b3d\" style=\"width:2100px\">");
            txt.Append("<tr>");
            foreach (ActivityDTO ActivityDTO in activityTypeList)
            {
                Color col = ColorTranslator.FromHtml(ActivityDTO.ActivityTypeColour);
                string color = Color.FromArgb(col.A, col.R, col.G, col.B).Name;
                color = color.Substring(2,6);
                txt.Append("<td valign=\"top\"><div style=\"background-color:#" + color + " ; width:15px; height:15px; border:1px solid #999\"></div></td><td style=\"color:#FFF\" valign=\"top\">&nbsp;" + ActivityDTO.Description + "&nbsp;</td>");
            }
            txt.Append("</tr>");
            txt.Append("</table>");
            txt.Append("</div>");
            activityColor.InnerHtml = txt.ToString();
        }
        catch (Exception)
        {
            throw;
        }
    }

    private List<KeyValuePair<int, string>> GetTimePeriods()
    {
        List<KeyValuePair<int, string>> timeLines = new List<KeyValuePair<int, string>>();
        timeLines.Add(new KeyValuePair<int, string>(2, "02 Months"));
        timeLines.Add(new KeyValuePair<int, string>(6, "06 Months"));
        timeLines.Add(new KeyValuePair<int, string>(12, "01 Year"));
        timeLines.Add(new KeyValuePair<int, string>(0, "- All -"));

        return timeLines;
    }

    
    private void SendAppointmentMethod(DateTime start, DateTime end, string subject, string description, int appoId)
    {
        System.Net.Mail.SmtpClient smtp = new SmtpClient(ConfigurationManager.AppSettings["SmtpServer"]);
        smtp.Port = int.Parse(ConfigurationManager.AppSettings["SmtpPort"]);

        System.DateTime schBeginDate = start.ToUniversalTime();
        System.DateTime schEndDate = end.ToUniversalTime();

        smtp.Send(CreateMeetingRequest(schBeginDate, schEndDate, subject, description, "colombo", "Test - ASP.NET", "indunilu@peercore.com.au", "Sumudu",
                "sumuduf@peercore.com.au"));
    }

    public MailMessage CreateMeetingRequest(DateTime start, DateTime end, string subject, string summary,
    string location, string organizerName, string organizerEmail, string attendeeName, string attendeeEmail)
    {
        MailAddressCollection col = new MailAddressCollection();
        col.Add(new MailAddress(attendeeEmail, attendeeName));
        return CreateMeetingRequest(start, end, subject, summary, location, organizerName, organizerEmail, col);
    }

    public MailMessage CreateMeetingRequest(DateTime start, DateTime end, string subject, string summary,
   string location, string organizerName, string organizerEmail, MailAddressCollection attendeeList)
    {
        MailMessage msg = new MailMessage();

        //  Set up the different mime types contained in the message
        System.Net.Mime.ContentType textType = new System.Net.Mime.ContentType("text/plain");
        System.Net.Mime.ContentType HTMLType = new System.Net.Mime.ContentType("text/html");
        System.Net.Mime.ContentType calendarType = new System.Net.Mime.ContentType("text/calendar");

        //  Add parameters to the calendar header
        calendarType.Parameters.Add("method", "REQUEST");
        calendarType.Parameters.Add("name", "meeting.ics");

        //  Create message body parts
        //  create the Body in text format
        string bodyText = "Type:Single Meeting\r\nOrganizer: {0}\r\nStart Time:{1}\r\nEnd Time:{2}\r\nTime Zone:{3}\r\nLocation: {4}\r\n\r\n*~*~*~*~*~*~*~*~*~*\r\n\r\n{5}";
        bodyText = string.Format(bodyText,
            organizerName,
            start.ToLongDateString() + " " + start.ToLongTimeString(),
            end.ToLongDateString() + " " + end.ToLongTimeString(),
            System.TimeZone.CurrentTimeZone.StandardName,
            location,
            summary);

        AlternateView textView = AlternateView.CreateAlternateViewFromString(bodyText, textType);
        msg.AlternateViews.Add(textView);

        //create the Body in HTML format
        string bodyHTML = "<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 3.2//EN\">\r\n<HTML>\r\n<HEAD>\r\n<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=utf-8\">\r\n<META NAME=\"Generator\" CONTENT=\"MS Exchange Server version 6.5.7652.24\">\r\n<TITLE>{0}</TITLE>\r\n</HEAD>\r\n<BODY>\r\n<!-- Converted from text/plain format -->\r\n<P><FONT SIZE=2>Type:Single Meeting<BR>\r\nOrganizer:{1}<BR>\r\nStart Time:{2}<BR>\r\nEnd Time:{3}<BR>\r\nTime Zone:{4}<BR>\r\nLocation:{5}<BR>\r\n<BR>\r\n*~*~*~*~*~*~*~*~*~*<BR>\r\n<BR>\r\n{6}<BR>\r\n</FONT>\r\n</P>\r\n\r\n</BODY>\r\n</HTML>";
        bodyHTML = string.Format(bodyHTML,
            summary,
            organizerName,
            start.ToLongDateString() + " " + start.ToLongTimeString(),
            end.ToLongDateString() + " " + end.ToLongTimeString(),
            System.TimeZone.CurrentTimeZone.StandardName,
            location,
            summary);

        AlternateView HTMLView = AlternateView.CreateAlternateViewFromString(bodyHTML, HTMLType);
        msg.AlternateViews.Add(HTMLView);

        //create the Body in VCALENDAR format
        string calDateFormat = "yyyyMMddTHHmmssZ";
        string bodyCalendar = "BEGIN:VCALENDAR\r\nMETHOD:REQUEST\r\nPRODID:Microsoft CDO for Microsoft Exchange\r\nVERSION:2.0\r\nBEGIN:VTIMEZONE\r\nTZID:(GMT-06.00) Central Time (US & Canada)\r\nX-MICROSOFT-CDO-TZID:11\r\nBEGIN:STANDARD\r\nDTSTART:16010101T020000\r\nTZOFFSETFROM:-0500\r\nTZOFFSETTO:-0600\r\nRRULE:FREQ=YEARLY;WKST=MO;INTERVAL=1;BYMONTH=11;BYDAY=1SU\r\nEND:STANDARD\r\nBEGIN:DAYLIGHT\r\nDTSTART:16010101T020000\r\nTZOFFSETFROM:-0600\r\nTZOFFSETTO:-0500\r\nRRULE:FREQ=YEARLY;WKST=MO;INTERVAL=1;BYMONTH=3;BYDAY=2SU\r\nEND:DAYLIGHT\r\nEND:VTIMEZONE\r\nBEGIN:VEVENT\r\nDTSTAMP:{8}\r\nDTSTART:{0}\r\nSUMMARY:{7}\r\nUID:{5}\r\nATTENDEE;ROLE=REQ-PARTICIPANT;PARTSTAT=NEEDS-ACTION;RSVP=TRUE;CN=\"{9}\":MAILTO:{9}\r\nACTION;RSVP=TRUE;CN=\"{4}\":MAILTO:{4}\r\nORGANIZER;CN=\"{3}\":mailto:{4}\r\nLOCATION:{2}\r\nDTEND:{1}\r\nDESCRIPTION:{7}\\N\r\nSEQUENCE:1\r\nPRIORITY:5\r\nCLASS:\r\nCREATED:{8}\r\nLAST-MODIFIED:{8}\r\nSTATUS:CONFIRMED\r\nTRANSP:OPAQUE\r\nX-MICROSOFT-CDO-BUSYSTATUS:BUSY\r\nX-MICROSOFT-CDO-INSTTYPE:0\r\nX-MICROSOFT-CDO-INTENDEDSTATUS:BUSY\r\nX-MICROSOFT-CDO-ALLDAYEVENT:FALSE\r\nX-MICROSOFT-CDO-IMPORTANCE:1\r\nX-MICROSOFT-CDO-OWNERAPPTID:-1\r\nX-MICROSOFT-CDO-ATTENDEE-CRITICAL-CHANGE:{8}\r\nX-MICROSOFT-CDO-OWNER-CRITICAL-CHANGE:{8}\r\nBEGIN:VALARM\r\nACTION:DISPLAY\r\nDESCRIPTION:REMINDER\r\nTRIGGER;RELATED=START:-PT00H15M00S\r\nEND:VALARM\r\nEND:VEVENT\r\nEND:VCALENDAR\r\n";
        bodyCalendar = string.Format(bodyCalendar,
            start.ToUniversalTime().ToString(calDateFormat),
            end.ToUniversalTime().ToString(calDateFormat),
            location,
            organizerName,
            organizerEmail,
            Guid.NewGuid().ToString("B"),
            summary,
            subject,
            DateTime.Now.ToUniversalTime().ToString(calDateFormat),
            attendeeList.ToString());

        AlternateView calendarView = AlternateView.CreateAlternateViewFromString(bodyCalendar, calendarType);
        calendarView.TransferEncoding = TransferEncoding.SevenBit;
        msg.AlternateViews.Add(calendarView);

        //  Adress the message
        msg.From = new MailAddress(organizerEmail);
        foreach (MailAddress attendee in attendeeList)
        {
            msg.To.Add(attendee);
        }
        msg.Subject = subject;

        return msg;
    }


    
    #endregion
}