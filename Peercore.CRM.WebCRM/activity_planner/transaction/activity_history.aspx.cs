﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;

public partial class activity_planner_transaction_activity_history : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
                SetBackLink();
                if (Request.QueryString["custid"] != null)
                {
                    HiddenFieldCustomerCode.Value = Request.QueryString["custid"];
                }
                if (Request.QueryString["eduid"] != null)
                {
                    HiddenFieldEndUserCode.Value = Request.QueryString["eduid"];
                }
                if (Request.QueryString["leadid"] != null)
                {
                    HiddenFieldLeadId.Value = Request.QueryString["leadid"];
                }
            }
            Master.SetBreadCrumb("Activity History", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    private void SetBackLink()
    {
        if (Request.QueryString["eduid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["eduid"]) && Request.QueryString["custid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["custid"]))
        {
            hlBack.NavigateUrl = "~/lead_customer/transaction/EndUser_entry.aspx?fm=endUser&custid=" + Request.QueryString["custid"] + "&eduid=" + Request.QueryString["eduid"];
        }
        else if (Request.QueryString["eduid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["eduid"]))
        {
            hlBack.NavigateUrl = "~/lead_customer/transaction/EndUser_entry.aspx?fm=lead&custid=" + Request.QueryString["custid"] + "&eduid=" + Request.QueryString["eduid"];
        }
        else if (Request.QueryString["custid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["custid"]))
        {
            hlBack.NavigateUrl = "~/lead_customer/transaction/customer_entry.aspx?custid=" + Request.QueryString["custid"] + "&fm=lead";
        }
        else if (Request.QueryString["leadid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["leadid"]))
        {
            hlBack.NavigateUrl = "~/lead_customer/transaction/leadentry.aspx?leadid=" + Request.QueryString["leadid"] + "&ty=Lead&fm=lead";
        }
    }
}