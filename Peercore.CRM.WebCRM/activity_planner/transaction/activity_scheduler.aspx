﻿<%@ Page Title="mSales - Route Planner" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="activity_scheduler.aspx.cs" Inherits="activity_planner_transaction_activity_scheduler" %>

    <%@ Register Src="~/usercontrols/buttonbar_navigation.ascx" TagPrefix="ucl" TagName="buttonbarNavi" %>
<%--    <%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl2" TagName="buttonbar" %>--%>

<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <telerik:RadStyleSheetManager ID="RadStyleSheetManager1" runat="server">
    </telerik:RadStyleSheetManager>
    <asp:HiddenField runat="server" ID="HiddenFieldRepCode" />
    <asp:HiddenField runat="server" ID="HiddenFieldRouteId" />
    <asp:HiddenField runat="server" ID="HiddenFieldDate" />
    <asp:HiddenField ID="HiddenFieldCallCycleId" runat="server" />
    <asp:HiddenField runat="server" ID="HiddenField1" />
    <asp:HiddenField runat="server" ID="HiddenFieldEditingStartsFromDate" />
    <asp:HiddenField runat="server" ID="HiddenFieldEditingEndsToDate" />
    <telerik:RadScriptManager ID="RadScriptManager1" runat="server" EnablePageMethods="true">
    </telerik:RadScriptManager>
    <asp:HiddenField ID="HiddenFieldSelectedView" runat="server" />
    <telerik:RadAjaxLoadingPanel ID="RadAjaxLoadingPanel1" runat="server" Skin="Default">
    </telerik:RadAjaxLoadingPanel>
    <div class="divcontectmainforms">
        <%--        <div class="homemsgboxtopic">
            ACTIVITY PLANNER
        </div>--%>
        <div class="toolbar_container">
            <div id="div_content" class="toolbar_left1">
                <div class="buttonbar">
                    <asp:Button ID="btnSendToCalender" runat="server" Text="Send to calendar" CssClass="sendtocalbtn" style="display:none"
                        OnClick="btnSendToCalender_Click" />
                    <asp:Label ID="Label1" runat="server" Text="Status :" style="display:none"></asp:Label>
                    <asp:DropDownList ID="radStatus" runat="server" AutoPostBack="true" OnSelectedIndexChanged="radStatus_SelectedIndexChanged" style="display:none"
                        CssClass="dropdown_small">
                    </asp:DropDownList>
                    <asp:Label ID="Label2" runat="server" Text="Time Range :" style="display:none"></asp:Label>
                    <asp:DropDownList ID="radTimeRange" runat="server" AutoPostBack="true" OnSelectedIndexChanged="radTimeRange_SelectedIndexChanged" style="display:none">
                    </asp:DropDownList>
                    <asp:Label ID="Label3" runat="server" Text="Territory :" style="display:none"></asp:Label>
                    <asp:DropDownList ID="radRepTerritory" runat="server" AutoPostBack="true" OnSelectedIndexChanged="radRepTerritory_SelectedIndexChanged" style="display:none">
                    </asp:DropDownList>

                    <asp:Label ID="Label6" runat="server" Text="Sales Rep :" ></asp:Label>
                    <asp:DropDownList ID="DropDownListRep" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DropDownListRep_SelectedIndexChanged" 
                        CssClass="dropdown_small">
                    </asp:DropDownList>
                    <asp:Button ID="buttonItinerary" Visible=false runat="server" OnClick="buttonItinerary_Click" CssClass="itinerarybtn" Text="Itinerary" />
                   <%-- <ucl2:buttonbar ID="buttonbar" runat="server" />--%>
                </div>
                
            </div>
            <div class="toolbar_right1" id="div3">
                <div class="leadentry_title_bar">
                <ucl:buttonbarNavi ID="buttonbarNavi" runat="server" />
                
                
                </div>
            </div>
        </div>
        <%--<div style="background-color: #FFF; padding-top:10px; overflow: auto">
            
        </div>--%>
        <div class="clearall">
        </div>
        <div id="div_activity" runat="server" style="display:none">
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: block;">
        </div>
        <div class="clearall">
        </div>
            
            <div>
<%--            <div class="formtextdiv">
                DR 
            </div>
            <div class="formdetaildiv">
                    <asp:TextBox ID="txtDRName" CssClass="tb" runat="server" Style="width: 200px; text-transform: uppercase"></asp:TextBox>
                    <div runat="server" id="div_autocomplete">
                    </div>
            </div></div>

               <div class="clearall">
        </div>--%>
        <%--OnAppointmentUpdate="radActivityPlanner_AppointmentUpdate" OnAppointmentDataBound="radActivityPlanner_DataBound" SelectedDate="2012-06-06"
         OnClientAppointmentWebServiceInserting="OnClientAppointmentWebServiceInserting"
         StartInsertingInAdvancedForm="true"            StartEditingInAdvancedForm="true" 
         TimeZoneID="Sri Lanka Standard Time"
        --%>
        <telerik:RadScheduler runat="server" ID="radActivityPlanner" DataKeyField="ID" DataSubjectField="Subject"
            DataStartField="Start" DataEndField="End" DataDescriptionField="Description"
            SelectedView="MonthView" Skin="CookersSkin"
            EnableEmbeddedSkins="False" OverflowBehavior="Expand" Culture="en-AU"
            TimelineView-NumberOfSlots="7" 
                    OnClientAppointmentDeleting="OnClientAppointmentDeleting" AllowInsert="False" 
            OnClientRequestSuccess="OnClientRequestSuccess" OnClientAppointmentInserting="OnClientAppointmentInserting"
            OnClientAppointmentEditing="OnClientAppointmentInserting" OnClientAppointmentMoveEnd="OnClientAppointmentEndEditing"
            OnClientAppointmentMoveStart="OnClientAppointmentStartEditing" OnClientAppointmentMoving="OnClientAppointmentEditing"
            OnClientNavigationCommand="clientNavigationCommandHandler" OnClientAppointmentContextMenuItemClicked="OnClientAppointmentContextMenuItemClicked"
            StartInsertingInAdvancedForm="True"
            DisplayRecurrenceActionDialogOnMove="True" MonthView-HeaderDateFormat="MMMM yyyy"
            WeekView-HeaderDateFormat="d MMMM yyyy" TimelineView-HeaderDateFormat="d MMMM yyyy"
            OnClientFormCreated="OnClientFormCreated" EnableDescriptionField="True" 
                    AppointmentStyleMode="Default">
            <AdvancedForm Modal="false" />
            <WebServiceSettings Path="../../service/activity_planner/activity_service.asmx" 
         
                ResourcePopulationMode="ServerSide">
            </WebServiceSettings>
            <%--<TimelineView GroupBy="ActivityTypes" GroupingDirection="Vertical" />--%>
            <TimeSlotContextMenuSettings EnableDefault="true" />
            <AppointmentContextMenuSettings EnableDefault="true" />
            <ResourceStyles>
            </ResourceStyles>
<%--            <AppointmentContextMenus>
                <telerik:RadSchedulerContextMenu runat="server" ID="SchedulerAppointmentContextMenu">
                    <Items>
                        <telerik:RadMenuItem Text="Open Organisation / Customer / End User" Value="OpenContact" />
                        <telerik:RadMenuItem Text="Send To Calendar" Value="SendToCalendar" />
                        <telerik:RadMenuItem Text="Edit" Value="CommandEdit" />
                    </Items>
                </telerik:RadSchedulerContextMenu>
            </AppointmentContextMenus>--%>
            <AdvancedEditTemplate>
                <div id="Div1" style="padding: 10px; padding-top: 20px;">
                    <div class="homemsgboxtopic" style="padding-left: 10px; padding-top: 5px; padding-bottom: 3px">
                        <div style="float: left; width: 25px">
                            <img src="../../assets/images/calender.png" /></div>
                        <div>
                            Route Planning -
                            <asp:Label ID="lbl_hedding" runat="server"></asp:Label></div>
                    </div>
                    <div style="border: 1px solid #cccccc; padding: 10px">
                        <div id="div_color_type" style="height: 25px; padding-left: 10px; padding-top: 10px"
                            runat="server">
                            <asp:Label ID="lbl_description" runat="server" CssClass="activitytopic"></asp:Label></div>
                        <div>
                            &nbsp;</div>
                        <div id="Div2">
                            <div>
                                <asp:Label ID="Label3" runat="server" CssClass="formtextdiv">Date:</asp:Label>
                                <asp:Label ID="Label5" runat="server" CssClass="formtextdiv" Width = "250px"></asp:Label>
                                <div style = "display:none"><asp:TextBox runat="server" ID="actEndDate"></asp:TextBox></div>
                            </div>
                            <div>
                                &nbsp;
                            </div>
                            <div>
                                <asp:Label ID="Label1" runat="server" CssClass="formtextdiv">DR Name</asp:Label>
                                <asp:TextBox ID="txtRep" runat="server" Width="250px" CssClass="tb" Enabled="false"></asp:TextBox>
                                <div runat="server" id="div_autocomplete">
                                </div>
                            </div>
                            <div>
                                &nbsp;
                            </div>
                            <div>
                                <asp:HiddenField ID="txt_appoId" runat="server" />
                                <asp:HiddenField ID="txt_appoIdCRM" runat="server" />
                                <asp:Label ID="Label4" runat="server" CssClass="formtextdiv">Route Name</asp:Label>
                                <asp:TextBox ID="txtRoute" runat="server" Width="250px" CssClass="tbb" Enabled="false"></asp:TextBox>
                                <div runat="server" id="div_autocomplete_route">
                                </div>
                            </div>
                            <div>
                                &nbsp;</div>
                            <div style="text-align: right">
                                <input type="button" value="Save" id="btn_save" name="btn_save" onclick="save()"
                                    style="color: White" class="formsavebtn" />
                                <%--<asp:Button  Text="Save" ID="btn_save" OnClientClick="save()" runat="server" class="formsavebtn" ForeColor="White"/>--%>
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" CausesValidation="false"
                                    CssClass="formcancelbtn" ForeColor="White" />
                            </div>
                        </div>
                    </div>
                </div>
            </AdvancedEditTemplate>
            <AdvancedInsertTemplate>
                <div id="Div1" style="padding: 10px; padding-top: 20px;">
                    <div class="homemsgboxtopic" style="padding-left: 10px; padding-top: 5px; padding-bottom: 3px">
                        <div style="float: left; width: 25px">
                            <img src="../../assets/images/calender.png" /></div>
                        <div>
                            Route Planning
                        </div>
                    </div>
                    <div style="border: 1px solid #cccccc; padding: 10px">
                        <div id="div_color_type" style="height: 25px; padding-left: 10px; padding-top: 10px"
                            runat="server">
                            <asp:Label ID="lbl_description" runat="server" CssClass="activitytopic"></asp:Label></div>
                        <div>
                            &nbsp;</div>
                        <div id="Div2">
                            <div>
                                <asp:HiddenField ID="txt_appoId" runat="server" />
                                <asp:HiddenField ID="txt_appoIdCRM" runat="server" />
                                <asp:Label ID="Label4" runat="server" CssClass="formtextdiv">Route Name</asp:Label>
                                <asp:TextBox ID="txtRoute" runat="server" Width="250px" CssClass="tbb"></asp:TextBox>
                                <div runat="server" id="div_autocomplete_route">
                                </div>
                            </div>
                            <div>
                                &nbsp;</div>
                            <div>
                                <asp:Label ID="Label1" runat="server" CssClass="formtextdiv">DR Name</asp:Label>
                                <asp:TextBox ID="txtRep" runat="server" Width="250px" CssClass="tb"></asp:TextBox>
                                <div runat="server" id="div_autocomplete">
                                </div>
                            </div>
                            <div>
                                &nbsp;</div>
                            <div>
                                <asp:Label ID="Label3" runat="server" CssClass="formtextdiv">End time:</asp:Label>
                                <asp:Label ID="Label5" runat="server" CssClass="formtextdiv">Datess:</asp:Label>
                                <div style = "display:none"><asp:TextBox ID="actDate" runat="server" ></asp:TextBox></div>
                            </div>
                            <div>
                                &nbsp;</div>
                            <div style="text-align: right">
                                <input type="button" value="Save" id="btn_save" name="btn_save" onclick="save()"
                                    style="color: White" class="formsavebtn" />
                                <%--<asp:Button  Text="Save" ID="btn_save" OnClientClick="save()" runat="server" class="formsavebtn" ForeColor="White"/>--%>
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" CommandName="Cancel" CausesValidation="false"
                                    CssClass="formcancelbtn" ForeColor="White" />
                            </div>
                        </div>
                    </div>
                </div>
            </AdvancedInsertTemplate>
        </telerik:RadScheduler>
        <%-- </telerik:RadAjaxPanel>--%>
    </div>
    <script type="text/javascript" language="javascript">

        var lastArgs = null;
        var lastContext = null;
        var longTouchID = 0;
        var menuShown = false;

        function save() {
            //            var a = $("#ctl00_MainContent_radActivityPlanner_AdvancedInsertForm_txtRoute").val();
            //            alert(a);

            var root = "";

            if ($("#MainContent_HiddenFieldCallCycleId").val() != null || $("#MainContent_HiddenFieldCallCycleId").val() != "") {

                var repCode = $("#MainContent_HiddenFieldRepCode").val();
                var routeId = $("#MainContent_HiddenFieldRouteId").val();
                //var date = $("#ctl00_MainContent_radActivityPlanner_AdvancedEditForm_actEndDate").val().toString("MM/dd/yyyy hh:mm:mm tt");
                var date = $("#MainContent_HiddenFieldDate").val().toString("MM/dd/yyyy hh:mm:mm tt");
                var activityId =  $("#MainContent_HiddenFieldCallCycleId").val()

                root = "../../activity_planner/process_forms/processmaster.aspx?fm=activity&type=Update&actid=" + activityId + "&rep=" + repCode + "&route=" + routeId +
                "&start=" + date;
                

              //  alert("OK");
            }

            else {
                var route = $("#ctl00_MainContent_RadScheduler_Form_txtSubject").val();
                var rep = $("#ctl00_MainContent_RadScheduler_Form_txtDescription").val();
                var date = $("#ctl00_MainContent_RadScheduler_Form_StartInput_dateInput").val().toString("MM/dd/yyyy hh:mm:mm tt");

                    root = "../../calendar/process_forms/processmaster.aspx?fm=calendar&type=insert&appoID=" + apoinmentid + "&subject=" + subject + "&description=" + description +
                "&startDate=" + startdate + "&endDate=" + endtdate + "&activityColor=" + activityColor + "&appoIdCRM=" + apoinmentidCRM + "&originators=" + originatorpara;

                }

                $.ajax({
                    url: root,
                    dataType: "html",
                    cache: false,
                    success: function (msg) {
                        if (msg != "") {
                            $("#MainContent_div_message").html(msg);
                            document.getElementById('MainContent_div_message').style.display = "block";
                            hideStatusDiv("MainContent_div_message");
                            var scheduler = $find('<%=radActivityPlanner.ClientID %>');
                            scheduler.rebind();
                        }
                    },
                    // error: function (XMLHttpRequest, textStatus, errorThrown) {
                    error: function (msg, textStatus) {
                        if (textStatus == "error") {
                            var msg = msg;
                        }
                    }
                });
        }

        function longTouch() {
            if (menuShown) {
                menuShown = false;
                return;
            }
            longTouchID = 0;
            menuShown = true;

            var scheduler = $find("<%= radActivityPlanner.ClientID %>");
            var eventArgs = null;
            var target = null;
            if (lastContext.target.nodeName == 'TD') {
                target = scheduler._activeModel.getTimeSlotFromDomElement(lastContext.target);
                eventArgs = new Telerik.Web.UI.SchedulerTimeSlotContextMenuEventArgs(target.get_startTime(), target.get_isAllDay(), lastContext, target);
                scheduler._raiseTimeSlotContextMenu(eventArgs);
            } else {
                try {
                    target = scheduler.getAppointmentFromDomElement(lastContext.target);
                    eventArgs = new Telerik.Web.UI.SchedulerAppointmentContextMenuEventArgs(target, lastContext);
                    scheduler._raiseAppointmentContextMenu(eventArgs);
                }
                catch (err) {
                    target = scheduler._activeModel.getTimeSlotFromDomElement(lastContext.target);
                    eventArgs = new Telerik.Web.UI.SchedulerTimeSlotContextMenuEventArgs(target.get_startTime(), target.get_isAllDay(), lastContext, target);
                    scheduler._raiseTimeSlotContextMenu(eventArgs);
                }
            }
        }

        function clientNavigationCommandHandler(sender, eventArgs) {
            var a = eventArgs.get_command();
            if (eventArgs.get_command() == 0)
                $("#MainContent_HiddenFieldSelectedView").val("DayView");
            else if (eventArgs.get_command() == 1)
                $("#MainContent_HiddenFieldSelectedView").val("WeekView");
            else if (eventArgs.get_command() == 2)
                $("#MainContent_HiddenFieldSelectedView").val("MonthView");
            else if (eventArgs.get_command() == 3)
                $("#MainContent_HiddenFieldSelectedView").val("TimelineView");
        }


        function handleTouchStart(e) {
            if (menuShown) {
                menuShown = false;
                return;
            }
            lastContext = e;
            lastContext.target = e.originalTarget;
            lastContext.pageX = e.changedTouches[0].pageX;
            lastContext.pageY = e.changedTouches[0].pageY;
            lastContext.clientX = e.changedTouches[0].clientX;
            lastContext.clientY = e.changedTouches[0].clientY;
            longTouchID = setTimeout(longTouch, 1000);
        }

        function handleClick(e) {
            if (menuShown) {
                menuShown = false;
                document.body.removeEventListener('click', handleClick, true);
                e.stopPropagation();
                e.preventDefault();
            }
        }

        function handleTouchEnd(e) {
            if (longTouchID != 0)
                clearTimeout(longTouchID);

            if (menuShown) {
                document.body.addEventListener('click', handleClick, true);
                e.preventDefault();
            }
        }

        function pageLoad() {
            $("#ctl00_MainContent_radActivityPlanner_Form_div_color").hide();
            if ($telerik.isMobileSafari || $telerik.isAndroid) {
                var scrollArea = $telerik.$('.rsContent', $get("<%= radActivityPlanner.ClientID %>"))[0];
                scrollArea.addEventListener('touchstart', handleTouchStart, false);
                scrollArea.addEventListener('touchend', handleTouchEnd, false);
            }
        }
    </script>
    <telerik:RadCodeBlock ID="RadCodeBlock1" runat="server">
        <script type="text/javascript">
            hideStatusDiv("MainContent_div_message");
            //rcbHovered rcbItem

            $("#ctl00_MainContent_radActivityPlanner_AdvancedEditForm_ResActivityTypes_Input").change(function () {
                SetActivityTypesColor();
            });
            $("#ctl00_MainContent_radActivityPlanner_AdvancedInsertForm_ResActivityTypes_Input").change(function () {
                SetActivityTypesColor();
            });

            function SetActivityTypesColor() {
                var scheduler = $find('<%=radActivityPlanner.ClientID %>');

                var count = scheduler.get_resources().getResourcesByType("ActivityTypes").get_count();

                for (var i = 0; i < count; i++) {
                    var key = scheduler.get_resources().getResourcesByType("ActivityTypes").getResource(i).get_key();
                    var text = scheduler.get_resources().getResourcesByType("ActivityTypes").getResource(i).get_text();
                    if ($("li.rcbHovered").html() == text)
                        GetColorCode(key);
                }
            }

            function OnClientAppointmentDeleting(sender, eventArgs) {
                eventArgs.set_cancel(true);
                DeleteActivity(eventArgs.get_appointment().get_id());
            }

            function OnClientAppointmentEditing(sender, eventArgs) {
                //debugger;
                menuShown = false;
                $('#ctl00_MainContent_radActivityPlanner_SchedulerAppointmentContextMenu_detached').css("display", "none");
                $('#ctl00_MainContent_radActivityPlanner_timeSlotContextMenu_detached').css("display", "none");
            }

            function OnClientAppointmentStartEditing(sender, eventArgs) {
                //debugger;
                //alert(eventArgs._appointment._start.toDateString());
                $("#MainContent_HiddenFieldEditingStartsFromDate").val(eventArgs._appointment._start.toDateString());
                menuShown = false;
                $('#ctl00_MainContent_radActivityPlanner_SchedulerAppointmentContextMenu_detached').css("display", "none");
                $('#ctl00_MainContent_radActivityPlanner_timeSlotContextMenu_detached').css("display", "none");
            }

            function OnClientAppointmentEndEditing(sender, eventArgs) {
                //debugger;
                var appointment = eventArgs.get_appointment();
                var routeName = appointment._subject;
                var toDate = eventArgs.get_newStartTime();
                var today = new Date((new Date()).toDateString());
                //alert(appoint.toDateString());
                $("#MainContent_HiddenFieldEditingEndsToDate").val(toDate.toDateString());
                var OldDate = new Date(appointment._start.toDateString());
                var NewDate = new Date(toDate.toDateString());
                //debugger;
                if (OldDate.toDateString() == NewDate.toDateString()) {
                    eventArgs.set_cancel(true);
                }
                else { 
                    if (OldDate < today) {
                        alert("Reschedulling past routes is not allowed !");
                        eventArgs.set_cancel(true);
                    }
                    else if (NewDate < today) {
                        alert("Reschedulling Routes to past is not allowed !");
                        eventArgs.set_cancel(true);
                    }
                    else {
                        
                        // Check if Itinerary is Approved
                        var ItineraryIsApproved = CheckForApprovedItinerary(OldDate.toDateString(), NewDate.toDateString());

                        // Check if New Date is Holiday
                        var isHoliday = CheckIsHoliday(OldDate.toDateString(), NewDate.toDateString());

                        if (ItineraryIsApproved == 'true' && isHoliday == 'true') {
                            var isYes = confirm("Your itinerary is approved for the month and, a holiday has been already added to " + NewDate.toDateString() + ", Do you want to reschedule the Activity?");
                            if (isYes) {
                                //Send Mail For Approval and Allow the Change
                                SendMailToRMForApprovalInRoutePlanner(routeName, OldDate.toDateString(), NewDate.toDateString(), appointment._id);

                                var sucessMsg = GetSuccesfullMessageDiv("Change was Successfully Saved, Approval Mail will be sent.", "MainContent_div_message");
                                $('#MainContent_div_message').css('display', 'block');
                                $("#MainContent_div_message").html(sucessMsg);
                                hideStatusDiv("MainContent_div_message");
                            }
                            else {
                                eventArgs.set_cancel(true);
                            }
                        }
                        else if (ItineraryIsApproved == 'true' && isHoliday == 'false') {
                            var isYes = confirm("Your itinerary is approved for the month, Do you want to reschedule the Activity?");
                            if (isYes) {
                                //Send Mail For Approval and Allow the Change
                                SendMailToRMForApprovalInRoutePlanner(routeName, OldDate.toDateString(), NewDate.toDateString(), appointment._id);

                                var sucessMsg = GetSuccesfullMessageDiv("Change was Successfully Saved, Approval Mail will be sent.", "MainContent_div_message");
                                $('#MainContent_div_message').css('display', 'block');
                                $("#MainContent_div_message").html(sucessMsg);
                                hideStatusDiv("MainContent_div_message");
                            }
                            else {
                                eventArgs.set_cancel(true);
                            }
                        }
                        else if (ItineraryIsApproved == 'false' && isHoliday == 'true') {
                            var isYes = confirm("A holiday has been already added to, " + NewDate.toDateString() + ", Do you want to reschedule the Activity?");
                            if (isYes) {
                                //No need to send the mail, Allow the change
                                var sucessMsg = GetSuccesfullMessageDiv("Change was Successfully Saved, Approval Mail will be sent.", "MainContent_div_message");
                                $('#MainContent_div_message').css('display', 'block');
                                $("#MainContent_div_message").html(sucessMsg);
                                hideStatusDiv("MainContent_div_message");
                            }
                            else {
                                eventArgs.set_cancel(true);
                            }
                        }
                        else if (ItineraryIsApproved == 'error' || isHoliday == 'error') {
                            var errorMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(errorMsg);
                            hideStatusDiv("MainContent_div_message");
                        }
                    }
                }
            }

            function OnClientAppointmentContextMenuItemClicked(sender, args) {
                switch (args.get_item().get_value()) {
                    case "SendToCalendar":
                        SendToCalendar(args.get_appointment().get_id(), args.get_appointment().get_start(), args.get_appointment().get_end());
                        break;
                    case "OpenContact":
                        OpenContact(args.get_appointment().get_id());
                        break;
                }
            }

            function OnClientFormCreated(sender, args) {
                try {
                    var $ = $telerik.$;
                    var text = args.get_appointment().get_resources().getResourcesByType("ActivityTypes").getResource(0).get_text();

                    GetColorCode(text);

                    var aSubject = args._appointment._subject.replace(args._appointment._attributes._data.LeadName, "").trim();

                    var ch = args._appointment._subject.replace(args._appointment._attributes._data.LeadName, "").trim().charAt(0);
                    if (ch == '-')
                        args._appointment._subject = aSubject.substring(1, aSubject.length).trim();
                    else
                        args._appointment._subject = aSubject;
                }
                catch (err) {
                }
            }

            function OnClientRequestSuccess(sender, args) {
                if (index == 1) {
                    index = 0;
                    var test = "<div id='messagegreen'  class='message-green' rel='sucess'><table border='0' width='100%' cellpadding='0' cellspacing='0'>";
                    test = test + "<tr><td class='green-left'>Successfully Saved.</td><td class='green-right'><a class='close-green' onclick=CloseMessageDiv('MainContent_div_message')><img src='"
                            + ROOT_PATH + "assets/images/icon_close_green.gif' alt='' /></a></td></tr></table></div>";

                    //                    $("#MainContent_div_message").show("fast");
                    $("#MainContent_div_message").html(test);
                    document.getElementById('MainContent_div_message').style.display = "block";
                    hideStatusDiv("MainContent_div_message");
                }
            }

            var index = 0;
            function OnClientAppointmentInserting(sender, args) {
                try {
                   
                    // alert("dd");
                    //                debugger;
                    //                var text = args.get_appointment().get_id() + args.get_appointment().get_subject();
                   //   alert(args.get_appointment().get_start());

                  //  debugger;

                   $("#MainContent_HiddenFieldCallCycleId").val(args.get_appointment().get_id());
                   $("#MainContent_HiddenFieldRepCode").val(args.get_appointment().get_internalID());
                   $("#MainContent_HiddenFieldRouteId").val(args.get_appointment().get_recurrenceParentID());
                   $("#MainContent_HiddenFieldDate").val(args.get_appointment().get_start().toDateString() + ' ' + args.get_appointment().get_start().toLocaleTimeString());
                    var a = $("#MainContent_HiddenFieldCallCycleId").val();
                    //                }
                    //                else {
                    //                    $("#MainContent_HiddenFieldCallCycleId").val(args.get_appointment().get_id());
                    //                    alert($("#MainContent_HiddenFieldCallCycleId").val());
                    //                }

                    $('#ctl00_MainContent_radActivityPlanner_AdvancedEditForm_txtRoute').val(args.get_appointment().get_subject());
                    $('#ctl00_MainContent_radActivityPlanner_AdvancedEditForm_txtRep').val(args.get_appointment().get_description());
                    $('#ctl00_MainContent_radActivityPlanner_AdvancedEditForm_actDate_dateInput').val(args.get_appointment().get_start());
                    $('#ctl00_MainContent_radActivityPlanner_AdvancedEditForm_Label5').html($("#MainContent_HiddenFieldDate").val());

                    $('#ctl00_MainContent_radActivityPlanner_AdvancedInsertForm_txtRoute').val(args.get_appointment().get_subject());
                    $('#ctl00_MainContent_radActivityPlanner_AdvancedInsertForm_txtRep').val(args.get_appointment().get_description());
                    $('#ctl00_MainContent_radActivityPlanner_AdvancedInsertForm_actDate_dateInput').val(args.get_appointment().get_start());
                    $('#ctl00_MainContent_radActivityPlanner_AdvancedInsertForm_Label5').html($("#MainContent_HiddenFieldDate").val());
                  //  $('#ctl00_MainContent_radActivityPlanner_AdvancedEditForm_actEndDate').val(args.get_appointment().get_start());
                    



                    index = 1;
                    menuShown = false;
                    $('#ctl00_MainContent_radActivityPlanner_SchedulerAppointmentContextMenu_detached').css("display", "none");
                    $('#ctl00_MainContent_radActivityPlanner_timeSlotContextMenu_detached').css("display", "none");
                }
                catch (rr) {
                }
            }


            function DeleteAppointment(id) {
                if (confirm("Do you want to delete the related appointment for this activity?")) {
                    var root = "../process_forms/processmaster.aspx?fm=appointment&type=Delete&id=" + id;

                    $.ajax({
                        url: root,
                        dataType: "html",
                        cache: false,
                        success: function (msg) {
                            if (msg != "") {
                                $("#MainContent_div_message").html(msg);
                                document.getElementById('MainContent_div_message').style.display = "block";
                                hideStatusDiv("MainContent_div_message");
                                var scheduler = $find('<%=radActivityPlanner.ClientID %>');
                                scheduler.rebind();
                            }
                        },
                        // error: function (XMLHttpRequest, textStatus, errorThrown) {
                        error: function (msg, textStatus) {
                            if (textStatus == "error") {
                                var msg = msg;
                            }
                        }
                    });
                }
                else {
                    //window.location = "../../activity_planner/transaction/activity_scheduler.aspx?flag=cancel";
                }
            }

            function DeleteActivity(id) {

                var root = "../process_forms/processmaster.aspx?fm=activity&type=Delete&id=" + id;

                $.ajax({
                    url: root,
                    dataType: "html",
                    cache: false,
                    success: function (msg) {
                        if (msg != "") {
                            if (msg == "1") {
                                DeleteAppointment(id);
                            }
                            else {
                                $("#MainContent_div_message").html(msg);
                                document.getElementById('MainContent_div_message').style.display = "block";
                                hideStatusDiv("MainContent_div_message");
                                var scheduler = $find('<%=radActivityPlanner.ClientID %>');
                                scheduler.rebind();
                            }
                        }
                    },
                    // error: function (XMLHttpRequest, textStatus, errorThrown) {
                    error: function (msg, textStatus) {
                        if (textStatus == "error") {
                            var msg = msg;
                        }
                    }
                });

            }
        
        </script>
    </telerik:RadCodeBlock>
    <script type="text/javascript">
        $(function () {
            $(".tb").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: ROOT_PATH + "service/call_cycle/call_cycle_service.asmx/GetRepsByOriginator",
                        data: "{'name':'" + request.term + "'}",
                        dataType: "json",
                        async: true,
                        dataFilter: function (data) {
                            return data;
                        },
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.Name,
                                    desc: item.RepCode
                                };
                            }));

                        },
                        error: function (result) {
                        }
                    });
                },
                minLength: 2,
                select: function (event, ui) {
                    var selectedObj = ui.item;
                    $('#ctl00_MainContent_radActivityPlanner_AdvancedInsertForm_txtDRName').val(selectedObj.label);
                    setRepCode(selectedObj.desc);

                }
            });
        });

        function setRepCode(repCode) {
            $("#MainContent_HiddenFieldRepCode").val(repCode);
        }


    </script>
    <script type="text/javascript">
        $(function () {
            $(".tbb").autocomplete({
                source: function (request, response) {
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: ROOT_PATH + "service/load_stock/load_stock.asmx/GetAllRoutes",
                        data: "{'name':'" + request.term + "'}",
                        dataType: "json",
                        async: true,
                        dataFilter: function (data) {
                            return data;
                        },
                        success: function (data) {
                            response($.map(data.d, function (item) {
                                return {
                                    label: item.RouteName,
                                    desc: item.RouteId
                                };
                            }));

                        },
                        error: function (result) {
                        }
                    });
                },
                minLength: 2,
                select: function (event, ui) {

                    var selectedObj = ui.item;
                    $('#ctl00_MainContent_radActivityPlanner_AdvancedInsertForm_txtRoute').val(selectedObj.label);
                    setRouteId(selectedObj.desc);
                }
            });
        });

        function setRouteId(routid) {
            $("#MainContent_HiddenFieldRouteId").val(routid);
        }

        $(document).ready(function () {
            $("#MainContent_buttonbarNavi_hlBack").css("display", "none");
            $("#ctl00_MainContent_radActivityPlanner_AdvancedInsertForm_actDate").kendoDateTimePicker({
                value: new Date(),
                format: "dd-MMM-yyyy HH:mm",
                timeFormat: "HH:mm"
            });

            $("#ctl00_MainContent_radActivityPlanner_AdvancedEditForm_actEndDate").kendoDateTimePicker({
               //value: new Date(),
                format: "dd-MMM-yyyy HH:mm",
                timeFormat: "HH:mm"
            });

        });
    </script>
</asp:Content>
