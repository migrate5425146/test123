﻿<%@ Page Title="mSales" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="activity_history.aspx.cs" Inherits="activity_planner_transaction_activity_history" %>
<%@ MasterType TypeName="SiteMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">

    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    
    <asp:HiddenField ID="HiddenFieldLeadId" runat="server" />
    <asp:HiddenField ID="HiddenFieldCustomerCode" runat="server" />
    <asp:HiddenField ID="HiddenFieldEndUserCode" runat="server" />

    <div class="divcontectmainforms">
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                    
                    <div class="clearall">
                    </div>

            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/lead_customer/transaction/leadentry.aspx">
                        
                        <div class="back">
                        </div></asp:HyperLink>
                </div>
            </div>
        </div>
 
        <div style="min-width: 200px; margin:10px 10px;">
        <div id="activityHistoryGrid">
        </div>
    </div>
    </div>
    <script type="text/javascript">
        hideStatusDiv("MainContent_div_message");
        $(document).ready(function () {
            var enduser_code = $("#MainContent_HiddenFieldEndUserCode").val();
            var cust_code = $("#MainContent_HiddenFieldCustomerCode").val();
            var leadId = $("#MainContent_HiddenFieldLeadId").val();
            loadActivityHistoryGrid(enduser_code, cust_code,leadId)
        });


        
    
    </script>
</asp:Content>

