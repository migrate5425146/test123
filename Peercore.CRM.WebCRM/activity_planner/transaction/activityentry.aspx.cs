﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Entities;
//using Peercore.CRM.BusinessRules;
using Peercore.CRM.Shared;
using CRMServiceReference;
using Peercore.CRM.Common;
using Telerik.Web.UI;
using System.Drawing;
using System.Data;

public partial class activity_planner_transaction_activityentry : PageBase
{
    #region Attribute
    usercontrols_buttonbar_activity activitycontrol;
    CommonUtility commonUtility = new CommonUtility();
    #endregion

    #region Properties

    private const string CUSTOMER_DATA = "LEAD_DATA";

    private static string leadId;
    public static string LeadId
    {
        get
        {
            return leadId;
        }
        set { leadId = value; }
    }

    private static string customerType;
    public static string CustomerType
    {
        get
        {
            return customerType;
        }
        set { customerType = value; }
    }

    private KeyValuePair<string, string> LeadEntry
    {
        get
        {
            if (Session[CommonUtility.LEAD_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.LEAD_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.LEAD_DATA] = value;
        }
    }

    private static string customerCode;
    public static string CustomerCode
    {
        get { return customerCode; }
        set { customerCode = value; }
    }

    private KeyValuePair<string, string> CustomerEntry
    {
        get
        {
            if (Session[CommonUtility.CUSTOMER_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.CUSTOMER_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.CUSTOMER_DATA] = value;
        }
    }

    private KeyValuePair<string, string> EndUserEntry
    {
        get
        {
            if (Session[CommonUtility.ENDUSER_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[CommonUtility.ENDUSER_DATA];
            }
            return new KeyValuePair<string, string>();
        }
        set
        {
            Session[CommonUtility.ENDUSER_DATA] = value;
        }
    }

    private List<ActivityDTO> fixedActivityTypes = new List<ActivityDTO>();

    List<ActivityDTO> lstActivityDTO = new List<ActivityDTO>();
    List<LookupTableDTO> lstLookupradpriority = new List<LookupTableDTO>();
    List<LookupTableDTO> lstLookupradstatus = new List<LookupTableDTO>();

    #endregion

    #region - Private Method -

    private void LoadSpecificDetails()
    {
        //Load Drop Down Details.
        LoadDropDownDetails();

        if (Request.QueryString["activityId"] != null && Request.QueryString["activityId"] != string.Empty)
        {
            ViewState[CommonUtility.ACTIVITY_ID] = Request.QueryString["activityId"];
        }

        if (Request.QueryString["appointmentid"] != "" && !string.IsNullOrEmpty(Request.QueryString["eduid"]))
        {
            HiddenFieldUsertype.Value = "Enduser";
            ActivityEntryForCustomer(Convert.ToInt32(Request.QueryString["appointmentid"]), Server.HtmlDecode(Convert.ToString(Request.QueryString["custid"])));
            HiddenFieldCustomerCode.Value = Request.QueryString["custid"].ToString();
            HiddenFieldEndUser.Value = Request.QueryString["eduid"].ToString();
            //EndUserEntry = new KeyValuePair<string, string>();
        }
        else if (Request.QueryString["appointmentid"] != "" && !string.IsNullOrEmpty(Request.QueryString["custid"]))
        {
            HiddenFieldUsertype.Value = "customer";
            ActivityEntryForCustomer(Convert.ToInt32(Request.QueryString["appointmentid"]),  Server.HtmlDecode(Convert.ToString(Request.QueryString["custid"])));
            HiddenFieldCustomerCode.Value = Request.QueryString["custid"].ToString();
        }
        else if (Request.QueryString["appointmentid"] != "" && !string.IsNullOrEmpty(Request.QueryString["leadId"]))
        {
            HiddenFieldUsertype.Value = "lead";
            ActivityEntryForLead(Convert.ToInt32(Request.QueryString["appointmentid"]), Convert.ToString(Request.QueryString["leadId"]));
            HiddenFieldLeadId.Value = Request.QueryString["leadId"].ToString();
        }

        //Load Grid Data.
        LoadUI(HiddenFieldUsertype.Value);
    }

    private void SetBackLink()
    {
        //bool isfrmHome = false;

        if (Request.QueryString["leadid"] != null && Request.QueryString["leadid"] != string.Empty && int.Parse(Request.QueryString["leadid"]) != 0)
        {
            if ((Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty) && (Request.QueryString["ty"] != null && Request.QueryString["ty"] != string.Empty))
                hlBack.NavigateUrl = "~/lead_customer/transaction/leadentry.aspx?leadid=" + Request.QueryString["leadid"] + "&fm=" + Request.QueryString["fm"] + "&ty=" + Request.QueryString["ty"] + "&ref=act#tabs";
            else
                hlBack.NavigateUrl = "~/lead_customer/transaction/leadentry.aspx?leadid=" + Request.QueryString["leadid"] + "&fm=lead&ref=act#tabs";


        }

        else if (Request.QueryString["eduid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["eduid"]))
        {
            if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty)
                hlBack.NavigateUrl = "~/lead_customer/transaction/enduser_entry.aspx?custid=" + Request.QueryString["custid"] + "&eduid=" + Request.QueryString["eduid"] + "&fm=" + Request.QueryString["fm"] + "&ref=act#tabs";
            else
                hlBack.NavigateUrl = "~/lead_customer/transaction/enduser_entry.aspx?custid=" + Request.QueryString["custid"] + "&eduid=" + Request.QueryString["eduid"] + "&fm=endUser&ref=act#tabs";
        }
        else
        {
            if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty)
                hlBack.NavigateUrl = "~/lead_customer/transaction/customer_entry.aspx?custid=" + Server.UrlEncode(Request.QueryString["custid"]) + "&fm=" + Request.QueryString["fm"] + "&ref=act#tabs";
            else
                hlBack.NavigateUrl = "~/lead_customer/transaction/customer_entry.aspx?custid=" + Server.UrlEncode(Request.QueryString["custid"]) + "&fm=lead&ty=customer&ref=act#tabs";
        }


        if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty)
        {
            if (Request.QueryString["fm"] == "acthis")
            {
                if (Request.QueryString["eduid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["eduid"]))
                {
                    hlBack.NavigateUrl = "~/activity_planner/transaction/activity_history.aspx?custid=" + Request.QueryString["custid"] + "&eduid=" + Request.QueryString["eduid"];
                }
                else if (Request.QueryString["custid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["custid"]))
                {
                    hlBack.NavigateUrl = "~/activity_planner/transaction/activity_history.aspx?custid=" + Request.QueryString["custid"];
                }
                else if (Request.QueryString["leadid"] != null && !string.IsNullOrWhiteSpace(Request.QueryString["leadid"]))
                {
                    hlBack.NavigateUrl = "~/activity_planner/transaction/activity_history.aspx?leadid=" + Request.QueryString["leadid"];
                }
            }
            else if (Request.QueryString["fm"] == "home")
            {
                hlBack.NavigateUrl = "~/default.aspx";
            }
            else if (Request.QueryString["fm"] == "cal")
            {
                hlBack.NavigateUrl = "~/calendar/transaction/appointment_entry.aspx?cht=calendar";
            }
        }

        //else if (Request.QueryString["activityId"] != null && Request.QueryString["activityId"] != string.Empty)
        //{
        //    if (int.Parse(Request.QueryString["activityId"]) == 0)
        //    {
        //        hlBack.NavigateUrl = "~/lead_customer/transaction/lead_contacts.aspx?cht=lead";
        //    }
        //}

        //else if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != string.Empty)
        //{
        //    if (Request.QueryString["fm"] == "home")
        //    {
        //        isfrmHome = true;
        //        if (Request.QueryString["pn"] != null && Request.QueryString["pn"] != string.Empty)
        //            hlBack.NavigateUrl = "~/default.aspx?pn=" + Request.QueryString["pn"].ToString();
        //        else
        //            hlBack.NavigateUrl = "~/default.aspx";
        //    }
        //    else if (Request.QueryString["fm"] == "dsbd")
        //    {
        //        if (Request.QueryString["rptid"] != null && Request.QueryString["rptid"] != string.Empty)
        //        {
        //            if (Request.QueryString["rptid"].ToString() == "3")
        //            {
        //                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/AnalysisGraph.aspx?rptid=" + Request.QueryString["rptid"].ToString()
        //                    + "&month=" + Request.QueryString["month"].ToString() + "&fromdate=" + Request.QueryString["fromdate"].ToString() + "&todate="
        //                    + Request.QueryString["todate"].ToString() + "&leadstage=" + Request.QueryString["leadstage"].ToString()
        //                    + "&bdm=" + Request.QueryString["bdm"].ToString() + "&actsta=" + Request.QueryString["actsta"].ToString();
        //            }
        //            else
        //            {
        //                hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/AnalysisGraph.aspx?rptid=" + Request.QueryString["rptid"].ToString();
        //            }
        //        }
        //        //hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/AnalysisGraph.aspx?rptid=" + Request.QueryString["rptid"].ToString();
        //        else
        //            hlBack.NavigateUrl = ConfigUtil.ApplicationPath + "dashboard/transaction/AnalysisGraph.aspx";
        //    }
        //}

        //if (Request.QueryString["appointmentid"] != "" && !string.IsNullOrEmpty(Request.QueryString["custid"]))
        //{
        //    if (Request.QueryString["fm"] == null || Request.QueryString["fm"] == string.Empty)
        //        hlBack.NavigateUrl = "~/lead_customer/transaction/customer_entry.aspx?custid=" + Request.QueryString["custid"] + "&ref=act#tabs";
        //    else if (Request.QueryString["fm"] == "activity")
        //        hlBack.NavigateUrl = "~/lead_customer/transaction/customer_entry.aspx?custid=" + Request.QueryString["custid"] + "&fm=act&ref=act#tabs";
        //}
        //else if (Request.QueryString["appointmentid"] != "" && !string.IsNullOrEmpty(Request.QueryString["leadId"]))
        //{
        //    if (Request.QueryString["fm"] == null || Request.QueryString["fm"] == string.Empty)
        //        hlBack.NavigateUrl = "~/lead_customer/transaction/leadentry.aspx?leadid=" + Request.QueryString["leadId"] + "&ref=act#tabs";
        //    else if (Request.QueryString["fm"] == "activity")
        //        hlBack.NavigateUrl = "~/lead_customer/transaction/leadentry.aspx?leadid=" + Request.QueryString["leadId"] + "&fm=act&ref=act#tabs";
        //    else if (isfrmHome)
        //        hlBack.NavigateUrl = "~/lead_customer/transaction/leadentry.aspx?leadid=" + Request.QueryString["leadId"] + "&ref=act&fm=home#tabs";
        //}
    }

    private void ActivityEntryForCustomer(int appointmentId, string customerCode)
    {
        //radActivity.Visible = false;
        ViewState[CommonUtility.CUSTOMER_CODE] = customerCode;
        KeyValuePair<string, string> customer_data = new KeyValuePair<string, string>(customerCode, "Customer");

        LeadEntry = new KeyValuePair<string, string>();

        //Setting session value.
        CustomerEntry = customer_data;
    }

    private void ActivityEntryForLead(int appointmentId, string leadID)
    {
        //radCustomerActivity.Visible = false;
        ViewState[CommonUtility.LEAD_CODE] = leadID;
        //CustomerType = HiddenFieldUsertype.Value;
        KeyValuePair<string, string> lead_data = new KeyValuePair<string, string>(leadID, "Lead");

        CustomerEntry = new KeyValuePair<string, string>();

        //Setting session value.
        LeadEntry = lead_data;
    }

    //Setting DropDown data
    private void LoadDropDownDetails()
    {
        try
        {
            ActivityClient oActivityType = new ActivityClient();
            CommonClient commonClient = new CommonClient();

            //Setting Args Value
            ArgsDTO args = new ArgsDTO();
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;

            fixedActivityTypes = oActivityType.GetAllFixedActivityTypes(false);

            //Load Activity Types.
            lstActivityDTO = oActivityType.GetAllActivityTypes(UserSession.Instance.DefaultDepartmentId, false);
            radactivitytype.Items.Clear();
            radactivitytype.Items.Add(new ListItem("", ""));
            foreach (ActivityDTO obj in lstActivityDTO)
            {
                radactivitytype.Items.Add(new ListItem(obj.Description, obj.Code));
            }
            ViewState["ActivityTypes"] = lstActivityDTO as List<ActivityDTO>;

            //Load Priority.
            lstLookupradpriority = commonClient.GetLookupTables("ACPR", args);
            radpriority.Items.Clear();
            foreach (LookupTableDTO obj in lstLookupradpriority)
            {
                if (obj.DefaultValue == "Y")
                {
                    radpriority.Items.Add(new ListItem(obj.TableDescription, obj.TableCode));
                    radpriority.SelectedValue = obj.TableCode;
                }
                else
                {
                    radpriority.Items.Add(new ListItem(obj.TableDescription, obj.TableCode));
                }
            }

            //Load Status.
            lstLookupradstatus = commonClient.GetLookupTables("ACST", args);
            radstatus.Items.Clear();
            foreach (LookupTableDTO obj in lstLookupradstatus)
            {
                if (obj.DefaultValue == "Y")
                {
                    radstatus.Items.Add(new ListItem(obj.TableDescription, obj.TableCode));
                    radstatus.SelectedValue = obj.TableCode;
                }
                else
                {
                    radstatus.Items.Add(new ListItem(obj.TableDescription, obj.TableCode));
                }
            }
        }
        catch (Exception x)
        {
            //commonUtility.ErrorMaintain(x);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    private void LoadUI(string leadType)
    {
        if (Convert.ToInt32(ViewState[CommonUtility.ACTIVITY_ID]) != 0 && ViewState[CommonUtility.ACTIVITY_ID] != null)
        {
            buttonbarforActivity_Id.EnableSendtoCalendar(true);
            OpenActivity();
            AddEditActivityEntry.Value = "Edit";
        }
        else
        {
            ClearAllControls();
            buttonbarforActivity_Id.EnableSendtoCalendar(false);
            ViewState[CommonUtility.APPOINTMENT_ID] = null;
            div_activitytype.InnerHtml = "";
            //Default Settings.
            startDatePicker.Text = DateTime.Now.ToLocalTime().ToString(ConfigUtil.DateTimePattern);
            HiddenFieldStartDate.Value = DateTime.Now.ToLocalTime().ToString(ConfigUtil.DateTimePattern);
            endDatePicker.Text = DateTime.Now.ToLocalTime().ToString(ConfigUtil.DateTimePattern);
            HiddenFieldEndDate.Value = DateTime.Now.ToLocalTime().ToString(ConfigUtil.DateTimePattern);
            txtAssignto.Text = UserSession.Instance.UserName;
            txtcomments.Text = "(Max 500 Characters)";
            reminderDatePicker.Text = DateTime.Now.ToLocalTime().ToString(ConfigUtil.DateTimePattern);
            HiddenFieldReminderDate.Value = DateTime.Now.ToLocalTime().ToString(ConfigUtil.DateTimePattern);
            radactivitytype.SelectedIndex = 0;

            if (leadType == "customer")
            {
                if (ViewState[CommonUtility.CUSTOMER_CODE] != null && ViewState[CommonUtility.CUSTOMER_CODE] != string.Empty)
                {
                    GetCustomerDetails(ViewState[CommonUtility.CUSTOMER_CODE].ToString());
                }
                radpriority.SelectedIndex = 0;
            }
            else if (leadType == "lead")
            {
                if (ViewState[CommonUtility.LEAD_CODE] != null && ViewState[CommonUtility.LEAD_CODE] != string.Empty)
                {
                    GetLeadDetails(ViewState[CommonUtility.LEAD_CODE].ToString());
                }
            }
            else if (leadType == "Enduser")
            {
                if (Request.QueryString["custid"] != null && Request.QueryString["custid"] != string.Empty && Request.QueryString["eduid"] != null && Request.QueryString["eduid"] != string.Empty)
                {
                    GetEnduserDetails(Request.QueryString["custid"].ToString(), Request.QueryString["eduid"].ToString());
                }
            }
        }
    }

    private void OpenActivity()
    {
        ActivityClient ActivityClient = new ActivityClient();
        bool bSentMail = false;
        try
        {

            ActivityDTO activityDTO = ActivityClient.GetActivity(Convert.ToInt32(ViewState[CommonUtility.ACTIVITY_ID]));

            if (activityDTO != null)
            {
                ViewState["ActivityDTO"] = activityDTO as ActivityDTO;

                if (!string.IsNullOrWhiteSpace(activityDTO.Subject))
                    txtsubject.Text = activityDTO.Subject;

                if (activityDTO.StartDate != null)
                {
                    string start_date = activityDTO.StartDate.ToString(ConfigUtil.DateTimePattern);
                    //DateTime.Parse(tempFrom, iFormatProvider).ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss");
                    startDatePicker.Text = DateTime.Parse(start_date).ToString(ConfigUtil.DateTimePattern);
                    HiddenFieldStartDate.Value = DateTime.Parse(start_date).ToString(ConfigUtil.DateTimePattern);
                }

                if (activityDTO.EndDate != null)
                {
                    string end_date = activityDTO.EndDate.ToString(ConfigUtil.DateTimePattern);
                    endDatePicker.Text = DateTime.Parse(end_date).ToString(ConfigUtil.DateTimePattern);
                    HiddenFieldEndDate.Value = DateTime.Parse(end_date).ToString(ConfigUtil.DateTimePattern);
                }

                if (activityDTO.ActivityType != null)
                {
                    HiddenFieldactivitytype.Value = activityDTO.ActivityType;
                }

                if (activityDTO.Status != "")
                {
                    //LookupTableDTO desiredObject = lstLookupradstatus.Find(o => o.TableCode == activityDTO.Status);
                    radstatus.SelectedValue = activityDTO.Status;
                }
                else
                    radstatus.SelectedIndex = 0;

                txtAssignto.Text = activityDTO.AssignedTo;

                if (ViewState[CommonUtility.CUSTOMER_CODE] != null && ViewState[CommonUtility.CUSTOMER_CODE] != string.Empty)
                    GetCustomerDetails(ViewState[CommonUtility.CUSTOMER_CODE].ToString());
                else if (activityDTO.LeadID != 0)
                    GetLeadDetails(activityDTO.LeadID.ToString());

                if (!string.IsNullOrWhiteSpace(activityDTO.Comments))
                    txtcomments.Text = activityDTO.Comments;

                checkemail.Checked = activityDTO.SentMail == "Y" ? true : false;
                checkemail.Disabled = activityDTO.SentMail == "N" ? false : true;

                bSentMail = activityDTO.SentMail == "Y" ? true : false;

                if (activityDTO.Priority != "")
                {
                    //HardCode
                    //LookupTableDTO desiredObject = lstLookupradpriority.Find(o => o.TableCode == activityDTO.Priority);
                    radpriority.SelectedValue = activityDTO.Priority;
                }
                else
                    radpriority.SelectedIndex = 0;

                checkreminder.Checked = activityDTO.SendReminder == "Y" ? true : false;

                //if (activityDTO.ReminderDate.HasValue && activityDTO.SendReminder == "Y")
                //{
                //    string reminderdate = activityDTO.ReminderDate.Value.ToString("dd-MMM-yyyy HH:mm:ss");
                //    reminderDatePicker.Text = activityDTO.ReminderDate.Value.ToString("dd-MMM-yyyy HH:mm:ss");
                //    HiddenFieldReminderDate.Value = activityDTO.ReminderDate.Value.ToString("dd-MMM-yyyy HH:mm:ss");
                //}
                //else
                //{
                //    reminderDatePicker.Text = DateTime.Now.ToLocalTime().ToString("dd-MMM-yyyy hh:mm tt");
                //    HiddenFieldReminderDate.Value = DateTime.Now.ToLocalTime().ToString("dd-MMM-yyyy HH:mm:ss");
                //}

                if (activityDTO.ReminderDate.HasValue)
                {
                    string reminderdate = activityDTO.ReminderDate.Value.ToString(ConfigUtil.DateTimePattern);
                    reminderDatePicker.Text = activityDTO.ReminderDate.Value.ToString(ConfigUtil.DateTimePattern);
                    HiddenFieldReminderDate.Value = activityDTO.ReminderDate.Value.ToString(ConfigUtil.DateTimePattern);
                }
                else
                {
                    reminderDatePicker.Text = DateTime.Now.ToLocalTime().ToString("dd-MMM-yyyy HH:mm");
                    HiddenFieldReminderDate.Value = DateTime.Now.ToLocalTime().ToString(ConfigUtil.DateTimePattern);
                }

                if (activityDTO.ReminderCreated == true)
                {
                    checkreminder.Disabled = true;
                    reminderDatePicker.Enabled = false;
                }
                else
                {
                    checkreminder.Disabled = false;
                    reminderDatePicker.Enabled = true;
                }

                if (activityDTO.ActivityType != "")
                {
                    if (!fixedActivityTypes.Any(f => f.Code == activityDTO.ActivityType))
                    {
                        radactivitytype.Visible = true;
                        div_activitytype.InnerHtml = "";
                        HiddenFieldactivitytype.Value = "";
                        div_activitytype.Visible = false;
                        //radactivitytype.Enabled = true;
                        ActivityDTO desiredObject = lstActivityDTO.Find(o => o.Code == activityDTO.ActivityType);
                        if (desiredObject != null)
                            radactivitytype.SelectedValue = desiredObject.Code;
                    }
                    else
                    {
                        //radactivitytype.Enabled = false;
                        //radactivitytype.SelectedValue = fixedActivityTypes.FirstOrDefault(f => f.Code == activityDTO.ActivityType).Description;
                        div_activitytype.Visible = true;
                        radactivitytype.Visible = false;
                        div_activitytype.InnerHtml = fixedActivityTypes.FirstOrDefault(f => f.Code == activityDTO.ActivityType).Description;
                        HiddenFieldactivitytype.Value = fixedActivityTypes.FirstOrDefault(f => f.Code == activityDTO.ActivityType).Code;
                    }
                }
                else
                    radactivitytype.SelectedIndex = 0;

                //txtRepgroup.Text = activityDTO.RepGroupName;

                if (activityDTO.CreatedDate!=null)
                    tbCreated.Text = "Created By: " + activityDTO.CreatedBy + ", " + activityDTO.CreatedDate.Value.ToLocalTime().ToString(ConfigUtil.DateTimePattern);

                if (activityDTO.LastModifiedDate != null)
                    tbModified.Text = "Last Modified By: " + activityDTO.LastModifiedBy + ", " + activityDTO.LastModifiedDate.Value.ToLocalTime().ToString(ConfigUtil.DateTimePattern);

                //ViewState["RelatedID"] = ActivityDTO.LeadID.ToString();
                //ViewState["RepGroupID"] = ActivityDTO.RepGroupID.ToString();
                ViewState["SentMail"] = activityDTO.SentMail == "Y" ? "1" : "0";
                ViewState[CommonUtility.APPOINTMENT_ID] = activityDTO.AppointmentID.ToString();

                if (activityDTO.AppointmentID > 0)
                {
                    buttonbarforActivity_Id.EnableSendtoCalendar(false);
                    buttonbarforActivity_Id.EnableSaveAndSendtocalendar(false);
                }
                else
                {
                    buttonbarforActivity_Id.EnableSendtoCalendar(true);
                    buttonbarforActivity_Id.EnableSaveAndSendtocalendar(true);
                }

                ViewState[CommonUtility.PARENTACTIVITY_ID] = activityDTO.ParentActivityID;

                // Disable Editing of Conversion / Inactivated Activities
                if (activityDTO.ActivityType == "CONV" || activityDTO.ActivityType == "INAC" || activityDTO.ActivityType == "REAC")
                {
                    buttonbarforActivity_Id.EnableSave(false);
                    buttonbarforActivity_Id.EnableSendtoCalendar(false);
                    buttonbarforActivity_Id.EnableSaveAndSendtocalendar(false);
                }
                else
                {
                    buttonbarforActivity_Id.EnableSave(true);
                    if (activityDTO.AppointmentID == 0)
                    {
                        buttonbarforActivity_Id.EnableSendtoCalendar(true);
                        buttonbarforActivity_Id.EnableSaveAndSendtocalendar(true);
                    }
                }
            }
        }
        catch (Exception x)
        {
            //commonUtility.ErrorMaintain(x);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    private void OpenActivityProxy()
    {
        ActivityClient ActivityClient = new ActivityClient();
        bool bSentMail = false;
        try
        {
            ActivityDTO activityDTO = ActivityClient.GetActivity(Convert.ToInt32(ViewState[CommonUtility.ACTIVITY_ID]));

            if (activityDTO != null)
            {
                ViewState["ActivityDTO"] = activityDTO as ActivityDTO;

                if (!string.IsNullOrWhiteSpace(activityDTO.Subject))
                    txtsubject.Text = activityDTO.Subject;

                if (activityDTO.StartDate != null)
                {
                    string start_date = activityDTO.StartDate.ToString(ConfigUtil.DateTimePattern);
                    //DateTime.Parse(tempFrom, iFormatProvider).ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss");
                    startDatePicker.Text = DateTime.Parse(start_date).ToString("dd-MMM-yyyy HH:mm");
                    HiddenFieldStartDate.Value = DateTime.Parse(start_date).ToString("dd-MMM-yyyy HH:mm");
                }

                if (activityDTO.EndDate != null)
                {
                    endDatePicker.Text = activityDTO.EndDate.ToString("dd-MMM-yyyy HH:mm tt");
                    HiddenFieldEndDate.Value = activityDTO.EndDate.ToString("dd-MMM-yyyy HH:mm tt");
                }

                if (activityDTO.Status != "")
                {
                    //LookupTableDTO desiredObject = lstLookupradstatus.Find(o => o.TableCode == activityDTO.Status);
                    radstatus.SelectedValue = activityDTO.Status;
                }
                else
                    radstatus.SelectedIndex = 0;

                txtAssignto.Text = activityDTO.AssignedTo;

                if (ViewState[CommonUtility.CUSTOMER_CODE] != null && ViewState[CommonUtility.CUSTOMER_CODE] != string.Empty)
                    GetCustomerDetails(ViewState[CommonUtility.CUSTOMER_CODE].ToString());
                else if (activityDTO.LeadID != 0)
                    GetLeadDetails(activityDTO.LeadID.ToString());

                if (!string.IsNullOrWhiteSpace(activityDTO.Comments))
                    txtcomments.Text = activityDTO.Comments;

                checkemail.Checked = activityDTO.SentMail == "Y" ? true : false;
                checkemail.Disabled = activityDTO.SentMail == "N" ? false : true;

                bSentMail = activityDTO.SentMail == "Y" ? true : false;

                if (activityDTO.Priority != "")
                {
                    //HardCode
                    //LookupTableDTO desiredObject = lstLookupradpriority.Find(o => o.TableCode == activityDTO.Priority);
                    radpriority.SelectedValue = activityDTO.Priority;
                }
                else
                    radpriority.SelectedIndex = 0;

                checkreminder.Checked = activityDTO.SendReminder == "Y" ? true : false;

                //if (activityDTO.ReminderDate.HasValue && activityDTO.SendReminder == "Y")
                //{
                //    string reminderdate = activityDTO.ReminderDate.Value.ToString("dd-MMM-yyyy HH:mm:ss");
                //    // reminderDatePicker.SelectedDate = activityDTO.ReminderDate.Value.ToString("dd-MMM-yyyy HH:mm:ss");
                //}
                //else
                //{
                //    reminderDatePicker.Text = DateTime.Now.ToLocalTime().ToString("dd-MMM-yyyy hh:mm tt");
                //    HiddenFieldReminderDate.Value = DateTime.Now.ToLocalTime().ToString("dd-MMM-yyyy hh:mm tt");
                //}

                if (activityDTO.ReminderDate.HasValue)
                {
                    //string reminderdate = activityDTO.ReminderDate.Value.ToString("dd-MMM-yyyy HH:mm:ss");
                  // reminderDatePicker.Text = activityDTO.ReminderDate.Value.ToString("dd-MMM-yyyy HH:mm:ss");

                   string reminderdate = activityDTO.ReminderDate.Value.ToString(ConfigUtil.DateTimePattern);
                   reminderDatePicker.Text = DateTime.Parse(reminderdate).ToString("dd-MMM-yyyy HH:mm tt");
                   HiddenFieldReminderDate.Value = DateTime.Parse(reminderdate).ToString("dd-MMM-yyyy HH:mm tt");
                }
                else
                {
                    reminderDatePicker.Text = DateTime.Now.ToLocalTime().ToString("dd-MMM-yyyy HH:mm tt");
                    HiddenFieldReminderDate.Value = DateTime.Now.ToLocalTime().ToString("dd-MMM-yyyy HH:mm tt");
                }

                if (activityDTO.ReminderCreated == true)
                {
                    checkreminder.Disabled = true;
                    reminderDatePicker.Enabled = false;
                }
                else
                {
                    checkreminder.Disabled = false;
                    reminderDatePicker.Enabled = true;
                }

                if (activityDTO.ActivityType != "")
                {
                    if (!fixedActivityTypes.Any(f => f.Code == activityDTO.ActivityType))
                    {
                        //radactivitytype.Enabled = true;
                        ActivityDTO desiredObject = lstActivityDTO.Find(o => o.Code == activityDTO.ActivityType);
                        if (desiredObject != null)
                            radactivitytype.SelectedValue = desiredObject.Code;
                    }
                    else
                    {
                        //radactivitytype.Enabled = false;
                        radactivitytype.SelectedValue = fixedActivityTypes.FirstOrDefault(f => f.Code == activityDTO.ActivityType).Description;
                    }
                }
                else
                    radactivitytype.SelectedIndex = 0;

                //txtRepgroup.Text = activityDTO.RepGroupName;

                tbCreated.Text = "Created By: " + activityDTO.CreatedBy + ", " + activityDTO.CreatedDate.Value.ToLocalTime().ToString(ConfigUtil.DateTimePattern);

                tbModified.Text = "Last Modified By: " + activityDTO.LastModifiedBy + ", " + activityDTO.LastModifiedDate.Value.ToLocalTime().ToString(ConfigUtil.DateTimePattern);

                //ViewState["RelatedID"] = ActivityDTO.LeadID.ToString();
                //ViewState["RepGroupID"] = ActivityDTO.RepGroupID.ToString();
                ViewState["SentMail"] = activityDTO.SentMail == "Y" ? "1" : "0";
                ViewState[CommonUtility.APPOINTMENT_ID] = activityDTO.AppointmentID.ToString();

                if (activityDTO.AppointmentID > 0)
                {
                    buttonbarforActivity_Id.EnableSendtoCalendar(false);
                    buttonbarforActivity_Id.EnableSaveAndSendtocalendar(false);
                }
                else
                {
                    buttonbarforActivity_Id.EnableSendtoCalendar(true);
                    buttonbarforActivity_Id.EnableSaveAndSendtocalendar(true);
                }

                ViewState[CommonUtility.PARENTACTIVITY_ID] = activityDTO.ParentActivityID;

                // Disable Editing of Conversion / Inactivated Activities
                if (activityDTO.ActivityType == "CONV" || activityDTO.ActivityType == "INAC" || activityDTO.ActivityType == "REAC")
                {
                    buttonbarforActivity_Id.EnableSave(true);
                    buttonbarforActivity_Id.EnableSendtoCalendar(false);
                    buttonbarforActivity_Id.EnableSaveAndSendtocalendar(false);
                }
                else
                {
                    buttonbarforActivity_Id.EnableSave(true);
                    if (activityDTO.ParentActivityID == 0)
                    {
                        buttonbarforActivity_Id.EnableSendtoCalendar(true);
                        buttonbarforActivity_Id.EnableSaveAndSendtocalendar(true);
                    }
                }
            }
        }
        catch (Exception)
        {

            throw;
        }
    }

    private void GetCustomerDetails(string custCode)
    {
        CustomerClient CustomerClient = new CustomerClient();
        CustomerDTO customerDTO = null;
        try
        {
            if (ViewState[CommonUtility.CUSTOMER_DETAILS] != null)
                customerDTO = ViewState[CommonUtility.CUSTOMER_DETAILS] as CustomerDTO;
            else
            {
                customerDTO = CustomerClient.GetCustomer(Server.UrlDecode(custCode));
                ViewState[CommonUtility.CUSTOMER_DETAILS] = customerDTO as CustomerDTO;
            }

            if (customerDTO != null)
            {
                if (!string.IsNullOrWhiteSpace(customerDTO.Name))
                    txtcustomer.Text = customerDTO.Name;
            }
        }
        catch (Exception x)
        {
            //commonUtility.ErrorMaintain(x);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    private void GetEnduserDetails(string customercode,string endusercode)
    {
        EndUserClient EnduserClient = new EndUserClient();
        EndUserDTO enduserDTO = null;
        ArgsDTO args = new ArgsDTO();

        args.EnduserCode = Server.UrlDecode(endusercode);
        args.CustomerCode = Server.UrlDecode(customercode);

        try
        {
            if (ViewState[CommonUtility.ENDUSER_DATA] != null)
                enduserDTO = ViewState[CommonUtility.ENDUSER_DATA] as EndUserDTO;
            else
            {
                enduserDTO = EnduserClient.GetEndUser(args);
                ViewState[CommonUtility.CUSTOMER_DETAILS] = enduserDTO as EndUserDTO;
            }

            if (enduserDTO != null)
            {
                if (!string.IsNullOrWhiteSpace(enduserDTO.Name))
                    txtEndUser.Text = enduserDTO.Name;

                if (!string.IsNullOrWhiteSpace(enduserDTO.CustomerName))
                    txtcustomer.Text = enduserDTO.CustomerName;
            }
        }
        catch (Exception x)
        {
        }
    }

    private void GetLeadDetails(string leadID)
    {
        LeadClient LeadClient = new LeadClient();
        LeadDTO leadDTO = null;
        try
        {
            if (ViewState[CommonUtility.LEAD_DETAILS] != null)
                leadDTO = ViewState[CommonUtility.LEAD_DETAILS] as LeadDTO;
            else
            {
                leadDTO = LeadClient.GetLead(leadID);
                ViewState[CommonUtility.LEAD_DETAILS] = leadDTO as LeadDTO;
            }

            if (leadDTO != null)
            {
                if (!string.IsNullOrWhiteSpace(leadDTO.LeadName))
                    txtcontact.Text = Server.UrlDecode(leadDTO.LeadName);
            }
        }
        catch (Exception x)
        {
            //commonUtility.ErrorMaintain(x);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    private void LoadActivities()
    {
        ActivityClient ActivityClient = new ActivityClient();
        List<ActivityDTO> lstActivities = new List<ActivityDTO>();
        ArgsDTO ArgsDTO = new ArgsDTO();

        try
        {
            //lstActivities = ActivityClient.GetActivitiesForCustomer(Convert.ToString(ViewState[CommonUtility.CUSTOMER_CODE]), ArgsDTO);
        }
        catch (Exception x)
        {
            //commonUtility.ErrorMaintain(x);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    private bool SaveActivity()
    {
        bool _hasSave = false;

        string sActivityType = "";
        int iReminderActivityId = 0;

        string UpdateActivitytype = div_activitytype.InnerHtml;

        ActivityClient activityClient = new ActivityClient();
        ActivityDTO activityDTO = null;

        if (ViewState["ActivityDTO"] != null && ViewState["ActivityDTO"] != string.Empty)
            activityDTO = ViewState["ActivityDTO"] as ActivityDTO;
        else
            activityDTO = new ActivityDTO();

        //TODO, HAVE TO CHECK , IS THIS REALLY NEED VALIDATION.
        if (ViewState[CommonUtility.CUSTOMER_CODE] == null || ViewState[CommonUtility.CUSTOMER_CODE] == "")
        {
            if (txtcontact.Text.Trim() == "")
            {
                txtcontact.Focus();
                div_message.Attributes.Add("style", "display:block;");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Contact cannot be empty.");
                return false;
            }
        }
        if (string.IsNullOrEmpty(radactivitytype.SelectedValue.ToString()) && string.IsNullOrEmpty(UpdateActivitytype))
        {
            div_message.Attributes.Add("style", "display:block;");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Please Select the Activity Type");
            return false;
        }

        // For Call Cycle and Service Call Activity the status cannot be changed to 'Completed'
        if (HiddenFieldactivitytype.Value != null && HiddenFieldactivitytype.Value.Trim() == "CYCL")
            sActivityType = HiddenFieldactivitytype.Value.Trim();
        else
            sActivityType = radactivitytype.SelectedValue.ToString();

        //kavisha- To make it like windows crm
        //if (sActivityType == "SERV" || sActivityType == "CYCL")
        //{
        //    if (radstatus.SelectedValue.ToString() == "COMP")
        //    {
        //        div_message.Attributes.Add("style", "display:block;");
        //        div_message.InnerHtml = commonUtility.GetErrorMessage("Please create Service Checklist to complete this Activity.");
        //        return false;
        //    }
        //}


        if (radstatus.SelectedValue != null && radstatus.SelectedValue.ToString() == "COMP")
        {
            if (String.IsNullOrWhiteSpace(txtcomments.Text))
            {
                div_message.Attributes.Add("style", "display:block;");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Comments must be entered if the Activity is Completed.");
                return false;
            }
        }

        //Edit Part.
        activityDTO.ActivityID = ((ViewState[CommonUtility.ACTIVITY_ID] != null && ViewState[CommonUtility.ACTIVITY_ID] != "0") ? Convert.ToInt32(ViewState[CommonUtility.ACTIVITY_ID].ToString()) : 0);
        activityDTO.Subject = txtsubject.Text;
        activityDTO.StartDate = !string.IsNullOrEmpty(startDatePicker.Text) ? DateTime.Parse(DateTime.Parse(startDatePicker.Text).ToString(ConfigUtil.DateTimePattern)) : DateTime.MinValue;
        activityDTO.EndDate = !string.IsNullOrEmpty(endDatePicker.Text) ? DateTime.Parse(DateTime.Parse(endDatePicker.Text).ToString(ConfigUtil.DateTimePattern)) : DateTime.MinValue;
        activityDTO.Status = radstatus.SelectedValue.ToString();
        activityDTO.AssignedTo = txtAssignto.Text;
        activityDTO.LeadID = ((ViewState[CommonUtility.LEAD_CODE] != null && ViewState[CommonUtility.LEAD_CODE] != "0") ? Convert.ToInt32(ViewState[CommonUtility.LEAD_CODE].ToString()) : 0); ;
        if (txtcomments.Text.IndexOf("(Max 500 Characters)") != 0)
            activityDTO.Comments = txtcomments.Text;
        else
            activityDTO.Comments = "";

        activityDTO.SentMail = checkemail.Checked == true ? "Y" : "N";
        activityDTO.Priority = radpriority.SelectedValue.ToString();
        activityDTO.SendReminder = checkreminder.Checked == true ? "Y" : "N";
     //Kavisha-To make equal to windows -   if (checkreminder.Checked == true)
          //  activityDTO.ReminderDate = DateTime.Parse(DateTime.Parse(reminderDatePicker.Text).ToString("dd-MMM-yyyy HH:mm:ss"));
        activityDTO.ReminderDate = DateTime.Parse(DateTime.Parse(reminderDatePicker.Text).ToString(ConfigUtil.DateTimePattern));
        /*if (HiddenFieldactivitytype.Value != null && HiddenFieldactivitytype.Value.Trim() == "CYCL")
            activityDTO.ActivityType = HiddenFieldactivitytype.Value.Trim();
        else
            activityDTO.ActivityType = radactivitytype.SelectedValue.ToString();*/

        activityDTO.ActivityType = string.IsNullOrEmpty(sActivityType) ? UpdateActivitytype.Substring(0,4).ToUpper() : sActivityType;
    //    activityDTO.ActivityType = sActivityType;

        activityDTO.CreatedBy = UserSession.Instance.UserName;
        activityDTO.CreatedDate = DateTime.Parse(DateTime.Now.ToString(ConfigUtil.DateTimePattern));
        activityDTO.LastModifiedBy = UserSession.Instance.UserName;
        activityDTO.LastModifiedDate = DateTime.Parse(DateTime.Now.ToString(ConfigUtil.DateTimePattern));
        activityDTO.LeadStageID = 0;
        activityDTO.RepGroupID = activityDTO.RepGroupID != null ? activityDTO.RepGroupID : 0;
        activityDTO.ParentActivityID = activityDTO.ParentActivityID != null ? activityDTO.ParentActivityID : 0;

        if (ViewState[CommonUtility.CUSTOMER_CODE] != null && ViewState[CommonUtility.CUSTOMER_CODE] != string.Empty)
          activityDTO.CustomerCode = ViewState[CommonUtility.CUSTOMER_CODE].ToString();

        if (HiddenFieldEndUser.Value != null && HiddenFieldEndUser.Value != string.Empty)
            activityDTO.EndUserCode = HiddenFieldEndUser.Value;

        // Set the Reminder Created
        if ((checkreminder.Disabled == true) && (reminderDatePicker.Enabled == false))
            activityDTO.ReminderCreated = true;

        bool sendMailToAll = false;
        bool sendToAssignTo = false;

        if (activityDTO.ActivityID == 0)
        {
            sendMailToAll = true;
            sendToAssignTo = checkemail.Checked ? true : false;
        }
        else if (activityDTO.ActivityID != 0 && ViewState["SentMail"] == "0")
        {
            sendToAssignTo = checkemail.Checked ? true : false;
        }

        try
        {
            _hasSave = activityClient.SaveActivity(activityDTO, ref iReminderActivityId);

            if (_hasSave)
            {
                if (checkreminder.Checked == true &&
                    checkreminder.Disabled == false)
                {
                    commonUtility.SendToOutlookAsTask(activityDTO.Subject, activityDTO.Comments, DateTime.Parse(startDatePicker.Text), DateTime.Parse(startDatePicker.Text), iReminderActivityId);
                }

                if (ViewState[CommonUtility.ACTIVITY_ID] != null && ViewState[CommonUtility.ACTIVITY_ID] != ""
                            && ViewState[CommonUtility.ACTIVITY_ID] != "-1")
                {
                    int activityId = 0;
                    int.TryParse(ViewState[CommonUtility.ACTIVITY_ID].ToString(), out activityId);
                    ActivityClient activityClient_obj = new ActivityClient();
                    CustomerActivitiesDTO customerActivitiesDTO = activityClient_obj.GetActivityAppoitmentId(activityId);

                    if (customerActivitiesDTO != null)
                    {
                        if (customerActivitiesDTO.AppointmentId > 0)
                        {
                            // Create a new appointment for this ACTIVITY
                            AppointmentClient appointmentClient = new AppointmentClient();
                            AppointmentDTO appointment_obj = new AppointmentDTO();
                            appointment_obj.AppointmentID = customerActivitiesDTO.AppointmentId;
                            appointment_obj.Subject = txtsubject.Text;
                            appointment_obj.Body = txtcomments.Text;
                            DateTime starttime = DateTime.Parse(startDatePicker.Text);
                            if (starttime != null)
                                appointment_obj.StartTime = starttime;

                            DateTime endtime = DateTime.Parse(endDatePicker.Text);
                            if (endtime != null)
                                appointment_obj.EndTime = endtime;

                            /* if (div_activitytype.InnerHtml != null && div_activitytype.InnerHtml.Trim() == "CYCL")
                                 appointment_obj.Category = div_activitytype.InnerHtml.Trim();
                             else
                                 appointment_obj.Category = radactivitytype.SelectedValue.ToString();*/

                            appointment_obj.Category = sActivityType;
                            appointment_obj.CreatedBy = UserSession.Instance.UserName;

                            appointmentClient.AppointmentUpdate(appointment_obj, false, false);
                        }
                    }
                }
            }

            if (sendMailToAll || sendToAssignTo)
                SendMail(sendMailToAll, sendToAssignTo);

            // If Reminder Activity is created, disable the reminder controls
            if (activityDTO.ReminderCreated == true)
            {
                checkreminder.Disabled = true;
                reminderDatePicker.Enabled = false;
            }

            if (_hasSave)
            {
                tbModified.Text = "Last Modified By: " + activityDTO.LastModifiedBy + ", " + activityDTO.LastModifiedDate.Value.ToString(ConfigUtil.DateTimePattern);

                if (ViewState[CommonUtility.APPOINTMENT_ID] == "0")
                    buttonbarforActivity_Id.EnableSendtoCalendar(true);

                if (checkemail.Checked)
                    checkemail.Disabled = true;

                //Response.Redirect(ConfigUtil.ApplicationPath + "activity_planner/transaction/activityentry.aspx?ty=cr");
                div_message.Attributes.Add("style", "display:block;");
                div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved.");
            }

            if (iReminderActivityId > 0)
            {
                AddEditActivityEntry.Value = "Edit";
                ViewState[CommonUtility.ACTIVITY_ID] = iReminderActivityId.ToString();
                OpenActivityProxy();
            }
        }
        catch (Exception ex)
        {
            /*commonUtility.ErrorMaintain(x);

            div_info.Attributes.Add("style", "display:block");
            div_message.Attributes.Add("style", "display:none");
            div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);*/
        }

        return _hasSave;
    }

    private bool SendMail(bool sendMailToAll, bool sendToAssignTo)
    {
        CommonClient commonClient = new CommonClient();
        CustomerClient customerClient = new CustomerClient();
        LeadClient leadClient = new LeadClient();
        ActivityClient activityClient = new ActivityClient();
        CustomerDTO customerDetail = new CustomerDTO();
        LeadDTO leadDetail = new LeadDTO();

        string message = null;
        string emailTemplate = string.Empty;

        TimeSpan span = TimeZone.CurrentTimeZone.GetUtcOffset(DateTime.Now);
        string utcString = "";

        //string utcString = !span.IsNegative()
        //    ? "(UTC+" + string.Format("{0}:{1}", span.Hours.ToString("00"), span.Minutes.ToString("00")) + ")"
        //    : "(UTC" + string.Format("{0}:{1}", span.Hours.ToString("00"), span.Minutes.ToString("00")) + ")";

        utcString = "(UTC+" + string.Format("{0}:{1}", span.Hours.ToString("00"), span.Minutes.ToString("00")) + ")";

        ViewState["ActivityTypes"] = lstActivityDTO as List<ActivityDTO>;
        if (ViewState["ActivityTypes"] != null)
        {
            List<ActivityDTO> activityTypes = ViewState["ActivityTypes"] as List<ActivityDTO>;
            if (activityTypes.Count != 0)
            {
                ActivityDTO activityDTO = activityTypes.FirstOrDefault(i => i.Code == radactivitytype.SelectedValue.Trim());
                if (activityDTO != null)
                {
                    emailTemplate = activityDTO.EmailTemplate;

                    string emailList = string.Empty;

                    if (sendMailToAll)
                    {
                        emailList = !string.IsNullOrEmpty(activityDTO.EmailAddresses) ? activityDTO.EmailAddresses : string.Empty;
                    }

                    if (sendToAssignTo)
                    {
                        OriginatorClient originatorClient = new OriginatorClient();
                        string AssignToMailAddress = originatorClient.GetOriginator(txtAssignto.Text).Email;
                        emailList = (emailList.EndsWith(";") || string.IsNullOrEmpty(emailList)) ? emailList + AssignToMailAddress : emailList + ";" + AssignToMailAddress;
                    }

                    if (!string.IsNullOrEmpty(emailList))
                    {
                        emailList = emailList.EndsWith(";") ? emailList.Remove((emailList.Length - 1), 1) : emailList;

                        if (ViewState[CommonUtility.CUSTOMER_DETAILS] != null)
                        {
                            if (!string.IsNullOrEmpty(ViewState[CommonUtility.CUSTOMER_DETAILS].ToString()))
                            {
                                customerDetail = ViewState[CommonUtility.CUSTOMER_DETAILS] as CustomerDTO;
                                if (customerDetail != null)
                                {
                                    if (!string.IsNullOrWhiteSpace(customerDetail.Name))
                                        message = "Customer Name : " + customerDetail.Name + "\n";
                                }
                            }
                        }
                        else if (ViewState[CommonUtility.LEAD_DETAILS] != null)
                        {
                            leadDetail = ViewState[CommonUtility.LEAD_DETAILS] as LeadDTO;
                            if (leadDetail != null)
                            {
                                if (!string.IsNullOrWhiteSpace(leadDetail.LeadName))
                                    message = "Lead Name : " + leadDetail.LeadName + "\n";
                            }
                        }

                        string Comments = !string.IsNullOrEmpty(txtcomments.Text) ? "\nComments : " + txtcomments.Text : string.Empty;

                        message += "Activity Type : " + radactivitytype.SelectedValue.Trim()
                            + "\nPriority : " + radpriority.SelectedValue.Trim()
                            + "\nStatus : " + radstatus.SelectedValue.Trim()
                            + Comments + "\n\n" + emailTemplate;

                        string frommail = UserSession.Instance.OriginatorEmail;

                        return commonUtility.SendMail(frommail, emailList, txtsubject.Text + " on "
                            + startDatePicker.Text//.SelectedDate.Value.ToString("dd-MMM-yyyy HH:mm")
                            + " (UTC - " + startDatePicker.Text + ")", message);//.SelectedDate.Value.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm") 
                    }
                }
            }
        }

        return false;
    }

    public void SentToAppointments()
    {
        string sOutlookSubject = "";

        if (string.IsNullOrEmpty(startDatePicker.Text))
        {
            div_info.Visible = true;
            div_info.InnerHtml = "Select the Due Date.";
            return;
        }

        bool hassave = false;
        bool hasSent = false;

        // Create a new appointment for this ACTIVITY
        AppointmentClient appointmentClient = new AppointmentClient();
        AppointmentDTO appointmentDTO = new AppointmentDTO();

        appointmentDTO.Subject = txtsubject.Text;

        if (txtcomments.Text.IndexOf("(Max 500 Characters)") != 0)
            appointmentDTO.Body = txtcomments.Text;
        else
            appointmentDTO.Body = "";

        DateTime starttime = DateTime.Parse(startDatePicker.Text);

        if (starttime != null)
            appointmentDTO.StartTime = starttime;

        DateTime endtime = DateTime.Parse(endDatePicker.Text);

        if (endtime != null)
            appointmentDTO.EndTime = endtime;

        if (div_activitytype.InnerHtml != null && div_activitytype.InnerHtml != string.Empty)
            appointmentDTO.Category = div_activitytype.InnerHtml.Trim();
        else
            appointmentDTO.Category = radactivitytype.SelectedValue.ToString();

        appointmentDTO.CreatedBy = UserSession.Instance.UserName;

        try
        {
            if (ViewState[CommonUtility.ACTIVITY_ID] != null)
            {
                int appointmentId = 0;
                hassave = appointmentClient.AppointmentInsert(appointmentDTO, true, Convert.ToInt32(ViewState[CommonUtility.ACTIVITY_ID]), out appointmentId);

                if (hassave)
                {
                    if (ConfigUtil.SendAppointmentsToOutlook == true)
                    {
                        if (txtcontact.Text.Trim() != "")
                            sOutlookSubject = txtcontact.Text.Trim() + " - " + appointmentDTO.Subject;
                        else if (txtcustomer.Text.Trim() != "")
                            sOutlookSubject = txtcustomer.Text.Trim() + " - " + appointmentDTO.Subject;
                        else
                            sOutlookSubject = appointmentDTO.Subject;

                        /*hasSent= commonUtility.SendToOutlookAsAppointment(appointmentDTO.Subject, appointmentDTO.Body, starttime.ToUniversalTime(), endtime.ToUniversalTime(), UserSession.Instance.OriginatorEmail,
                             UserSession.Instance.UserName, UserSession.Instance.UserName, UserSession.Instance.OriginatorEmail);*/

                        /* hasSent = commonUtility.SendToOutlookAsAppointment(appointmentDTO.Subject, appointmentDTO.Body, starttime, endtime, "indunilu@peercore.com.au",
                             UserSession.Instance.UserName, UserSession.Instance.UserName, "indunilu@peercore.com.au");*/

                        hasSent = true;
                    }

                    if (hasSent)
                    {
                        div_message.Attributes.Add("style", "display:block;");
                        //div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Sent.");
                        div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved.");
                        buttonbarforActivity_Id.EnableSendtoCalendar(false);
                        buttonbarforActivity_Id.EnableSaveAndSendtocalendar(false);
                    }
                    else
                    {
                        div_info.Attributes.Add("style", "display:block;padding:8px");
                        div_info.InnerHtml = commonUtility.GetErrorMessage("Sorry, There is an issue while send an appointment.");
                        buttonbarforActivity_Id.EnableSendtoCalendar(false);
                        buttonbarforActivity_Id.EnableSaveAndSendtocalendar(false);
                    }
                }
            }
        }
        catch (InvalidOperationException oException)
        {

        }
    }

    private void ClearAllControls()
    {
        //Clear ViewState Values.
        ViewState["ActivityDTO"] = null;
        LoadDropDownDetails();

        div_info.Visible = false;
        div_info.InnerHtml = "";

        txtsubject.Text = string.Empty;
        txtAssignto.Text = string.Empty;
        //txtRepgroup.Text = string.Empty;
        checkemail.Checked = false;
        checkreminder.Checked = false;

        tbCreated.Text = string.Empty;
        tbModified.Text = string.Empty;

        AddEditActivityEntry.Value = "New";
    }
    #endregion

    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            txtcomments.Attributes.Add("maxLength", txtcomments.MaxLength.ToString());
            buttonbarforActivity_Id.onButtonSave = new usercontrols_buttonbar_activity.ButtonSave(ButtonSave_Click);
            buttonbarforActivity_Id.onButtonClear = new usercontrols_buttonbar_activity.ButtonClear(ButtonClear_Click);
            buttonbarforActivity_Id.onButtonSendtocalendar = new usercontrols_buttonbar_activity.ButtonSendtocalendar(ButtonSendtocalendar_Click);
            buttonbarforActivity_Id.onButtonSaveAndSendtocalendar = new usercontrols_buttonbar_activity.ButtonSaveAndSendtocalendar(ButtonSaveAndSendtocalendar_Click);
            buttonbarforActivity_Id.onButtonOpportunity = new usercontrols_buttonbar_activity.ButtonOpportunity(ButtonOpportunity_Click);

            if (!IsPostBack)
            {
                //Setting Form Specific Data.
                LoadSpecificDetails();
                SetBackLink();
            }

            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Activity Entry ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    protected void ButtonSave_Click(object sender)
    {
        try
        {
            if (SaveActivity())
            {
                div_message.Attributes.Add("style", "display:block;");
                div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved.");
            }
        }
        catch (Exception ex)
        {
            //commonUtility.ErrorMaintain(ex);

            div_info.Attributes.Add("style", "display:block");
            div_message.Attributes.Add("style", "display:none");
            div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    protected void ButtonClear_Click(object sender)
    {
        ViewState[CommonUtility.ACTIVITY_ID] = null;

        div_message.InnerHtml = "";
        div_message.Attributes.Add("style", "display:none");

        div_info.InnerHtml = "";
        div_info.Attributes.Add("style", "display:none");

        if (!string.IsNullOrWhiteSpace(HiddenFieldUsertype.Value))
            LoadUI(HiddenFieldUsertype.Value);
        else if (!string.IsNullOrWhiteSpace(CustomerEntry.Key))
        {
            HiddenFieldUsertype.Value = "customer";
            LoadUI(HiddenFieldUsertype.Value);
        }
        else if (!string.IsNullOrWhiteSpace(LeadEntry.Key))
        {
            HiddenFieldUsertype.Value = "lead";
            LoadUI(HiddenFieldUsertype.Value);
        }
    }

    protected void ButtonSendtocalendar_Click(object sender)
    {
        try
        {
            SentToAppointments();
        }
        catch (Exception ex)
        {
            //commonUtility.ErrorMaintain(ex);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    protected void ButtonSaveAndSendtocalendar_Click(object sender)
    {
        try
        {
            if (SaveActivity())
            {
                SentToAppointments();
            }
            else
            {
                div_info.Attributes.Add("style", "display:block");
                div_info.InnerHtml = commonUtility.GetErrorMessage("Sorry, There is a problem while saving the data.");
            }
        }
        catch (Exception ex)
        {
            //commonUtility.ErrorMaintain(ex);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    protected void ButtonOpportunity_Click(object sender)
    {
        try
        {
            string url = "";
            if (!string.IsNullOrEmpty(HiddenFieldCustomerCode.Value))
                url = "../../pipelines_stage/transaction/opportunityentry.aspx?fm=act&custid=" + HiddenFieldCustomerCode.Value;
            else
                url = "../../pipelines_stage/transaction/opportunityentry.aspx?fm=act&leadId=" + HiddenFieldLeadId.Value;
            Response.Redirect(url);
        }
        catch (Exception ex)
        {
            //commonUtility.ErrorMaintain(ex);

            //div_info.Attributes.Add("style", "display:block");
            //div_message.Attributes.Add("style", "display:none");
            //div_info.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
        }
    }

    //protected void startDatePicker_SelectedDateChanged(object sender, EventArgs e)
    //{
    //    if (!string.IsNullOrEmpty(endDatePicker.Text))
    //    {
    //        if (Convert.ToDateTime(endDatePicker.Text) < Convert.ToDateTime(startDatePicker.Text))
    //            endDatePicker.Text = startDatePicker.Text;
    //    }

    //    if (!string.IsNullOrEmpty( reminderDatePicker.Text))
    //    {
    //        if (Convert.ToDateTime(reminderDatePicker.Text) < Convert.ToDateTime(startDatePicker.Text))
    //            reminderDatePicker.Text = startDatePicker.Text;//.SelectedDate.Value.ToString("dd-MMM-yyyy hh:mm tt");
    //    }
    //}

    //protected void endDatePicker_SelectedDateChanged(object sender, EventArgs e)
    //{
    //    if ( !string.IsNullOrEmpty(endDatePicker.Text ))
    //    {
    //        if (Convert.ToDateTime(endDatePicker.Text) < Convert.ToDateTime(startDatePicker.Text))
    //            endDatePicker.Text = startDatePicker.Text;
    //    }
    //}

    //protected void radactivitytype_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    div_info.Attributes.Add("style", "display:none;");
    //    div_info.InnerHtml = "";

    //    //When Activity type is service call, and user try to change even the serivce check list intered.
    //    if (ViewState["ActivityDTO"] != null)
    //    {
    //        ActivityDTO activityDTO = ViewState["ActivityDTO"] as ActivityDTO;
    //        if (activityDTO != null)
    //        {
    //            if ((activityDTO.ActivityType == "SERV") && (radactivitytype.SelectedValue != "SERV"))
    //            {
    //                if (Request.QueryString["srvcall"] != null && Request.QueryString["srvcall"] != string.Empty)
    //                {
    //                    if (Request.QueryString["srvcall"] == "1")
    //                    {
    //                        div_info.Attributes.Add("style", "display:block;padding:8px");
    //                        div_info.InnerHtml = commonUtility.GetErrorMessage("Already entered the service check list, please delete the existing service check list record and try again.");
    //                    }
    //                }
    //            }
    //        }
    //    }
    //}
    #endregion
}