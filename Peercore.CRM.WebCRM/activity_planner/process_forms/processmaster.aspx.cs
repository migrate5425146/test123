﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Peercore.CRM.Entities;
using System.Text;
//using Peercore.CRM.BusinessRules;
using Peercore.CRM.Shared;
//using Peercore.CRM.Entities.CompositeEntities;
using CRMServiceReference;
using System.Drawing;
using Peercore.CRM.Common;
using System.Globalization;

public partial class activity_planner_process_forms_processmaster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != "")
        {
            if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
            {
                if (Request.QueryString["type"] == "get")
                {
                    if (Request.QueryString["fm"] == "activitytype")
                    {
                        //Response.Expires = -1;
                        //Response.Clear();
                        //string getActivityType = GetActivityType(Request.QueryString["id"].ToString());
                        //Response.ContentType = "text/html";
                        //Response.Write(getActivityType);
                        //Response.End();
                    }
                }
                else if (Request.QueryString["type"] == "Update")
                {
                    if (Request.QueryString["fm"] == "activitytype")
                    {
                        //Response.Expires = -1;
                        //Response.Clear();
                        //string saveActyvitytpye = UpdateActivityType();
                        //Response.ContentType = "text/html";
                        //Response.Write(saveActyvitytpye);
                        //Response.End();
                    }

                    if (Request.QueryString["fm"] == "activity")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string saveActyvitytpye = UpdateActivity();
                        Response.ContentType = "text/html";
                        Response.Write(saveActyvitytpye);
                        Response.End();
                    }   
                }
                else if (Request.QueryString["type"] == "Delete")
                {
                    if (Request.QueryString["fm"] == "activitytype")
                    {
                        //Response.Expires = -1;
                        //Response.Clear();
                        //string deleteActivity = DeleteActivityType(Request.QueryString["id"].ToString());
                        //Response.ContentType = "text/html";
                        //Response.Write(deleteActivity);
                        //Response.End();
                    }
                    if (Request.QueryString["fm"] == "activity")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string deleteActivity = DeleteActivity(int.Parse( Request.QueryString["id"].ToString()));
                        Response.ContentType = "text/html";
                        Response.Write(deleteActivity);
                        Response.End();
                    }
                    if (Request.QueryString["fm"] == "appointment")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string deleteActivity = DeleteAppointment(int.Parse(Request.QueryString["id"].ToString()));
                        Response.ContentType = "text/html";
                        Response.Write(deleteActivity);
                        Response.End();
                    }
                }
                else if (Request.QueryString["type"] == "insert")
                {
                    if (Request.QueryString["fm"] == "activitytype")
                    {
                        //Response.Expires = -1;
                        //Response.Clear();
                        //string activityType = InsertAcitvityType();
                        //Response.ContentType = "text/html";
                        //Response.Write(activityType);
                        //Response.End();
                    }
                }
                else if (Request.QueryString["type"] == "query")
                {
                    if (Request.QueryString["fm"] == "leadentry")
                    {
                        if (Request.QueryString["querytype"] == "originator")
                        {
                            //Response.Expires = -1;
                            //Response.Clear();
                            //string assigntoStr = GetAssignToForLeadEntry();
                            //Response.ContentType = "text/html";
                            //Response.Write(assigntoStr);
                            //Response.End();
                        }
                        if (Request.QueryString["querytype"] == "repgroup")
                        {
                            Response.Expires = -1;
                            Response.Clear();
                            string repgroup = GetRepGroupForLeadEntry();
                            Response.ContentType = "text/html";
                            Response.Write(repgroup);
                            Response.End();
                        }
                    }
                    else if (Request.QueryString["fm"] == "activityentry")
                    {
                        if (Request.QueryString["querytype"] == "originator")
                        {
                            Response.Expires = -1;
                            Response.Clear();
                            string assigntoStr = GetAssignToForLeadEntry();
                            Response.ContentType = "text/html";
                            Response.Write(assigntoStr);
                            Response.End();
                        }
                        if (Request.QueryString["querytype"] == "repgroup")
                        {
                            Response.Expires = -1;
                            Response.Clear();
                            string repgroup = GetRepGroupForLeadEntry();
                            Response.ContentType = "text/html";
                            Response.Write(repgroup);
                            Response.End();
                        }
                    }
                }
                else if (Request.QueryString["type"] == "activityType")
                {
                    if (Request.QueryString["value"] != null && Request.QueryString["value"] != "")
                    {
                        //Response.Expires = -1;
                        //Response.Clear();
                        //string getActivityType = ActivityColor(Request.QueryString["value"].ToString());
                        //Response.ContentType = "text/html";
                        //Response.Write(getActivityType);
                        //Response.End();
                    }
                    if (Request.QueryString["ActivityCode"] != null && Request.QueryString["ActivityCode"] != "")
                    {
                        Response.Expires = -1;
                        Response.Clear();
                        string getActivityType = GetColorCode(Request.QueryString["ActivityCode"].ToString());
                        Response.ContentType = "text/html";
                        Response.Write(getActivityType);
                        Response.End();
                    }
                    
                }
                else if (Request.QueryString["type"] == "SendToAppointments")
                {
                    Response.Expires = -1;
                    Response.Clear();
                    string getActivityType = SendToAppointments(int.Parse(Request.QueryString["id"]),
                                                    DateTime.Parse(Request.QueryString["start"]),
                                                        DateTime.Parse(Request.QueryString["end"]));
                    Response.ContentType = "text/html";
                    Response.Write(getActivityType);
                    Response.End();

                }
                else if (Request.QueryString["type"] == "OpenContact")
                {
                    Response.Expires = -1;
                    Response.Clear();
                    string getActivityType = OpenContactEntry(int.Parse(Request.QueryString["id"]));
                    Response.ContentType = "text/html";
                    Response.Write(getActivityType);
                    Response.End();

                }
            }
        }
    }

    public string DeleteActivity(int appointmentId)
    {
        string rval = "";
        CommonUtility commonUtilityPara = new CommonUtility();
        ActivityDTO activity = new ActivityDTO();
        activity.ActivityID = appointmentId;
        ActivityClient oActivity = new ActivityClient();

        CustomerActivitiesDTO customerActivities = new CustomerActivitiesDTO();
        AppointmentClient oAppointment = new AppointmentClient();
        customerActivities = oActivity.GetActivityAppoitmentId(activity.ActivityID);

        bool iNoofRecs = oActivity.DeleteActivity(activity);
        if (iNoofRecs)
        {
            if (customerActivities.AppointmentId > 0 && customerActivities.DelFlag != "Y")
            {
                rval = "1";
            }
            else
            {
                rval = commonUtilityPara.GetSucessfullMessage("Successfully deleted.");
                //GlobalValues.GetInstance().ShowMessage("Successfully deleted.", GlobalValues.MessageImageType.Information);
            }
        }
        else
        {
            rval = commonUtilityPara.GetSucessfullMessage("Delete failed.");
        }
        return rval;
    }

    public string DeleteAppointment(int appointmentId)
    {
        string rval = "";
        CommonUtility commonUtilityPara = new CommonUtility();
        CustomerActivitiesDTO customerActivities = new CustomerActivitiesDTO();
        ActivityDTO activity = new ActivityDTO();
        activity.ActivityID = appointmentId;
        ActivityClient oActivity = new ActivityClient();
        AppointmentClient oAppointment = new AppointmentClient();
        customerActivities = oActivity.GetActivityAppoitmentId(activity.ActivityID);

        bool iNoofRecs = false;

                activity.AppointmentID = customerActivities.AppointmentId;

                //if (clsGlobal.GetInstance().ConfirmMessageBox("Do you want to delete the related appointment for this activity?") == true)
                //{
                iNoofRecs = oActivity.ResetAppointment(activity);
                if (iNoofRecs)
                {
                    AppointmentDTO crmAppointment = new AppointmentDTO();
                    crmAppointment.AppointmentID = customerActivities.AppointmentId;
                    iNoofRecs = oAppointment.AppointmentUpdate(crmAppointment, false, true);
                    if (iNoofRecs)
                        rval = commonUtilityPara.GetSucessfullMessage("Successfully deleted.");
                    else
                        rval = commonUtilityPara.GetSucessfullMessage("Delete failed.");
                }
                else
                    rval = commonUtilityPara.GetSucessfullMessage("Delete failed.");

                //}
                return rval;
    }

    public string SendToAppointments(int id, DateTime startTime, DateTime EndTime)
    {
        ActivityClient oActivity = new ActivityClient();
        AppointmentClient oAppointment = new AppointmentClient();
        StringBuilder txt = new StringBuilder();
        CommonUtility commonUtilityPara = new CommonUtility();

        try
        {
            //if (sendtoAppointments != null)
            //{
            CustomerActivitiesDTO activity = oActivity.GetActivityAppoitmentId(Convert.ToInt32(id));
            bool iNoRecs = false;
            if (activity.AppointmentId > 0)
            {

                string str = commonUtilityPara.GetSucessfullMessage("Activity has already been sent to calendar.");
                return str;
                //Response.Redirect("../../activity_planner/transaction/activity_scheduler.aspx?flag=alreadysentactivity");
            }
            

            ActivityDTO existingActivity = oActivity.GetActivity(Convert.ToInt32(id));
            //List<CustomerActivitiesDTO> cActivity = ViewState["cActivity"] as List<CustomerActivitiesDTO>;

            //CustomerActivitiesDTO customerActivitiesType = cActivity.Find(c => c.TypeColour == "#" + Convert.ToString(sendtoAppointments.BackColor.Name).ToUpper());

            // Create a new appointment for this ACTIVITY
           // AppointmentClient oAppointment = new AppointmentClient();
            AppointmentDTO appointment = new AppointmentDTO();
            appointment.Subject = existingActivity.Subject;
            appointment.Body = existingActivity.Comments;
            appointment.StartTime = existingActivity.StartDate;
            appointment.EndTime = existingActivity.EndDate;
            appointment.Category = existingActivity.ActivityType;
            appointment.CreatedBy = UserSession.Instance.UserName;
            int appointmentId = 0;
            iNoRecs = oAppointment.AppointmentInsert(appointment, true, Convert.ToInt32(id), out appointmentId);

            if (iNoRecs)
            {
                string str = commonUtilityPara.GetSucessfullMessage("Sent to Calendar");
                return str;
                //this.ClientScript.RegisterStartupScript(typeof(Page), "Popup", "SetMessage('" + str + "');", true);
                //Response.Redirect("../../activity_planner/transaction/activity_scheduler.aspx?flag=sentcalander");
            }
            //}
        }
        catch (Exception)
        {

            throw;
        }
        return "";
    }

    public string OpenContactEntry(int activityId)
    {
        string url = "";
        ActivityClient oActivity =new ActivityClient();
        ActivityDTO activity = oActivity.GetActivity(activityId);
        try
        {
            if (activity != null)
            {
                if (activity.CustomerCode != null && activity.CustomerCode != "")
                {
                    url = "../../lead_customer/transaction/customer_entry.aspx?custid=" + activity.CustomerCode + "&fm=activity&ref=act#tabs";
                }
                else if (activity.LeadID > 0)
                {
                    url = "../../lead_customer/transaction/leadentry.aspx?leadid=" + activity.LeadID.ToString() + "&fm=activity&ref=act#tabs";
                }
            }
        }
        catch (Exception)
        {
            throw;
        }
        return url;
    }

    private string GetColorCode(string activityCode)
    {
        string title = Request.QueryString["title"].ToString();
        ActivityClient activityClient = new ActivityClient();
        ArgsDTO args =new ArgsDTO();
        args.DefaultDepartmentId= "PS";

        if (title.Contains("div_title"))
        {
            int index = title.LastIndexOf("</a>");
            if (index > 0)
                title = title.Substring(0, index) + "</a>";
        }

        ActivityDTO item = activityClient.GetActivityType(activityCode, args);

        string txt = title + "</div> <div id='div_title' class='shedular_title' style='background-color:" + item.ActivityTypeColour + "'>" + item.Description + "";
        return txt;
    }

    private string GetRepGroupForLeadEntry()
    {
        StringBuilder txt = new StringBuilder();
        ArgsDTO args = new ArgsDTO();
        CommonClient commonClient = new CommonClient();
        KeyValuePairOfstringstring decriptioncol = new KeyValuePairOfstringstring();
        decriptioncol.key = "rep_group_name";
        decriptioncol.value = "BDM Group";
        args.StartIndex = ConfigUtil.StartIndex;
        args.RowCount = ConfigUtil.MaxRowCount;

        KeyValuePairOfstringstring codecol = new KeyValuePairOfstringstring();
        codecol.key = "rep_group_id";
        codecol.value = "BDM Group ID";

        List<LookupDTO> lookupList = commonClient.GetLookupsForBDM("crm_rep_group",
                        decriptioncol,
                        codecol, "", args, "");

        txt.Append("<table border=\"0\" id=\"repgroupTable\" width=\"300px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
        txt.Append("<tr class=\"detail-popsheader\">");
        txt.Append("<th></th>");
        txt.Append("<th>BDM Group ID</th>");
        txt.Append("<th>BDM Group</th>");
        txt.Append("</tr>");
        for (int i = 0; i < lookupList.Count; i++)
        {
            txt.Append("<tr>");
            txt.Append("<td style=\"padding-left:5px;\" class=\"tblborder\"> <input type=\"radio\" name=\"group1\" value=" + lookupList[i].Description + " id=\"a\"/>  </td>");
            txt.Append("<td class=\"tblborder\" align=\"left\">" + lookupList[i].Code + "</td>");
            txt.Append("<td align=\"left\" class=\"tblborder\">" + lookupList[i].Description + "</td>");
            txt.Append("</tr>");
        }
        txt.Append("</table>");

        txt.Append("<script type='text/javascript'>");

        txt.Append("$('#jqi_state0_buttonOk').click(function () {");
        txt.Append("var selected_email = '';");
        txt.Append("$('#repgroupTable tr').each(function () {");
        txt.Append("    if ($(this).children(':eq(0)').find('#a').is(':checked')) {");
        txt.Append("        var emailaddress = $(this).children(':eq(0)').find('#a').val();");
        txt.Append("        selected_email = selected_email + emailaddress;");
        txt.Append("    }");
        txt.Append("});");
        txt.Append("$('#MainContent_txtRepgroup').val(selected_email);");
        txt.Append("})");

        txt.Append("</script>");

        return txt.ToString();
    }

    private string GetAssignToForLeadEntry()
    {
        StringBuilder txt = new StringBuilder();
        CommonClient commonClient = new CommonClient();
        ArgsDTO args = new ArgsDTO();

        if (UserSession.Instance != null)
            args.ChildOriginators = UserSession.Instance.ChildOriginators;

        args.StartIndex = ConfigUtil.StartIndex;
        args.RowCount = ConfigUtil.MaxRowCount;
        List<LookupDTO> lookupList = commonClient.GetLookups("OriginatorLookup", args.ChildOriginators, null, args, false);

        txt.Append("<table border=\"0\" id=\"originatorTable\" width=\"270px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
        txt.Append("<tr class=\"detail-popsheader\">");
        txt.Append("<th></th>");
        txt.Append("<th>Originator</th>");
        txt.Append("<th>Name</th>");
        txt.Append("</tr>");
        for (int i = 0; i < lookupList.Count; i++)
        {
            txt.Append("<tr>");
            txt.Append("<td style=\"padding-left:5px;\" class=\"tblborder\"> <input type=\"radio\" name=\"group1\" value=" + lookupList[i].Code + " id=\"a\"/>  </td>");
            txt.Append("<td align=\"left\" class=\"tblborder\">" + lookupList[i].Code + "</td>");
            txt.Append("<td align=\"left\" class=\"tblborder\">" + lookupList[i].Description + "</td>");
            txt.Append("</tr>");
        }
        txt.Append("</table>");

        txt.Append("<button id=\"jqi_state0_buttonOk\" class=\"k-button\">Ok</button>");

        txt.Append("<script type='text/javascript'>");

        

        txt.Append("</script>");

        return txt.ToString();
    }

    public string UpdateActivity()
    {
        string rval = "";
        CommonUtility commonUtilityPara = new CommonUtility();
        ActivityDTO activity = new ActivityDTO();
      //  activity.ActivityID = appointmentId;
        ActivityClient oActivity = new ActivityClient();

        string datetime = (Request.QueryString["start"]).ToString();
       /* activity.StartDate = DateTime.ParseExact(datetime,"dd/MM/yyyy HH:mm:ss",
                                  CultureInfo.InvariantCulture);*/

        activity.StartDate = DateTime.Parse(DateTime.Parse(datetime).ToString(ConfigUtil.DateTimePattern));

        activity.ActivityID = int.Parse(Request.QueryString["actid"]);
        activity.RepCode = Request.QueryString["rep"];
        activity.RouteID = int.Parse(Request.QueryString["route"]);
        activity.CreatedBy = UserSession.Instance.UserName;

        bool iNoofRecs = oActivity.UpdateActivityFromPlanner(activity, UserSession.Instance.OriginatorString);
        if (iNoofRecs)
        {
 
                rval = commonUtilityPara.GetSucessfullMessage("Successfully deleted.");

        }
        else
        {
            rval = commonUtilityPara.GetSucessfullMessage("Delete failed.");
        }
        return rval;
    }


    /*
    

    private string GetActivityType(string activitytypeId)
    {
        ActivityClient activityClient = new ActivityClient();
        ActivityDTO activityTypeEntity = activityClient.GetActivityType(activitytypeId);
        StringBuilder txt = new StringBuilder();
        txt.Append("What would you like to change this to? ");
        txt.Append("<div class=\"field\">");
        txt.Append("<label for=\"editTypeCode\">Activity Type :<span class=\"required\">*</span> :</label><input type=\"text\" id=\"editTypeCode\" name=\"editTypeCode\" value=\"" + activityTypeEntity.TypeCode + "\" />");
        txt.Append("<input type=\"text\" id=\"editcolour\" name=\"editcolour\" style=\"background:" + activityTypeEntity.Colour.ToString() + "\" size=\"4\" value=" + activityTypeEntity.Colour.ToString() + " />");
        txt.Append("</br>");
        txt.Append("<label for=\"editdescription\">Description :<span class=\"required\">*</span> :</label><input type=\"text\" id=\"editdescription\" name=\"editdescription\" value=\"" + activityTypeEntity.TypeDescription + "\" />");
        txt.Append("</br>");
        txt.Append("<label for=\"editemailAddress\">Email Addresses :<span class=\"required\">*</span> :</label><input type=\"text\" id=\"editemailAddress\" name=\"editemailAddress\" value=\"" + activityTypeEntity.EmailAddress + "\" />");
        txt.Append("</br>");
        txt.Append("<label>(separated by semicolon)</label>");
        txt.Append("<input type=\"button\" value=\"?\" onclick=\"email_address('/Peercore.CRM.WebCRM/lead_customer/process_forms/processmaster.aspx?fm=activityType&type=lookup');\"/>");
        txt.Append("</br>");
        txt.Append("<label for=\"editemailAddressTemp\">Email Template :<span class=\"required\">*</span> :</label><input type=\"text\" id=\"editemailAddressTemp\" name=\"editemailAddressTemp\" value=\"" + activityTypeEntity.EmailTemplate + "\" />");
        txt.Append("</br>");
        txt.Append("<label for=\"editisfixed\">Is Fixed:</label><input type=\"text\" id=\"editisfixed\" name=\"editisfixed\" size=\"4\" value=\"" + activityTypeEntity.Isfixed.ToString() + "\"/>");
        txt.Append("</br>");
        if (activityTypeEntity.ShowInPlanner == "Y")
        {
            txt.Append("<label for=\"editecheck\"></label><input id=\"checkActivity\" type=\"checkbox\" Checked=\"true\"/>");
        }
        else if (activityTypeEntity.ShowInPlanner == "N")
        {
            txt.Append("<label for=\"editecheck\"></label><input id=\"checkActivity\" type=\"checkbox\"/>");
        }
        txt.Append("Display in Activity Planner");
        txt.Append("</br>");
        txt.Append("</div>");

        txt.Append("<script type='text/javascript'>");
        txt.Append("$('#jqi_state0_buttonSave').click(function() {");
        txt.Append("var typecode=$('#editTypeCode').val();");
        txt.Append("var description=$('#editdescription').val();");
        txt.Append("var emailaddress=$('#editemailAddress').val();");
        txt.Append("var emailaddresstemp=$('#editemailAddressTemp').val();");
        txt.Append("var colour=$('#editcolour').val().replace('#', '');");
        txt.Append("var isfixed=$('#editisfixed').val();");

        txt.Append("if ($('#checkActivity').is(':checked'))");
        txt.Append("{");
        txt.Append("var chkactivity = true");
        txt.Append("}");
        txt.Append("else");
        txt.Append("{");
        txt.Append("var chkactivity = false");
        txt.Append("}");

        txt.Append("saveData('/Peercore.CRM.WebCRM/activity_planner/process_forms/processmaster.aspx?fm=activitytype&id=" + activitytypeId + "&type=Update&typecode='+typecode+'&description='+description+'&emailaddress='+emailaddress+'&emailaddresstemp='+emailaddresstemp+'&colour='+colour+'&chkactivity='+chkactivity+'&isfixed='+isfixed);");
        txt.Append("});");
        txt.Append("</script>");

        return txt.ToString();
    }

    private string UpdateActivityType()
    {
        StringBuilder txt = new StringBuilder();
        int iNoRecs = 0;
        brActivity oActivity = new brActivity();
        CDataHandle.Activity objActivity = new CDataHandle.Activity();

        objActivity.ActivityID = Convert.ToInt32(Request.QueryString["appoID"]);
        objActivity.Subject = Convert.ToString(Request.QueryString["subject"]);
        objActivity.Comments = Convert.ToString(Request.QueryString["description"]);

        string strartDate = Convert.ToString(Request.QueryString["startDate"]);
        string[] split = strartDate.Split(new Char[] { '-' });
        DateTime strartDateTime = Convert.ToDateTime(split[0] + "/" + split[1] + "/" + split[2] + " " + split[3] + ":" + split[4] + ":" + split[5]);
        objActivity.StartDate = strartDateTime;

        string endDate = Convert.ToString(Request.QueryString["endDate"]);
        string[] split1 = endDate.Split(new Char[] { '-' });
        DateTime endDateTime = Convert.ToDateTime(split1[0] + "/" + split1[1] + "/" + split1[2] + " " + split1[3] + ":" + split1[4] + ":" + split1[5]);
        objActivity.EndDate = endDateTime;

        objActivity.ActivityType = Convert.ToString(Request.QueryString["activityColor"]);
        objActivity.LastModifiedBy = UserSession.Instance.UserName;
        objActivity.LastModifiedDate = DateTime.Now;

        CRMAppointment appointment = new CRMAppointment();
        brAppointment oAppointment = new brAppointment();
        if (Request.QueryString["appoIdCRM"] != null && Convert.ToInt32(Request.QueryString["appoIdCRM"]) > 0)
        {
            appointment.StartTime = strartDateTime;
            appointment.EndTime = endDateTime;
            appointment.AppointmentID = Convert.ToInt32(Request.QueryString["appoIdCRM"]);
        }

        iNoRecs = oActivity.ChangeActivityDate(objActivity);
        if (iNoRecs > 0 && appointment != null)
            oAppointment.Update(appointment, true);

        if (iNoRecs > 0)
            txt.Append("success");
        else
            txt.Append("error");

        return txt.ToString();
    }

    private string InsertAcitvityType()
    {
        int iNoRecs = 0;
        int iReminderActivityId = 0;
        StringBuilder txt = new StringBuilder();

        LookupTable activityStatus = new brLookupTable().GetDefaultLookup("ACST", UserSession.Instance.DefaultDepartmentIdCookers);
        LookupTable activityPriority = new brLookupTable().GetDefaultLookup("ACPR", UserSession.Instance.DefaultDepartmentIdCookers);

        brActivity oActivity = new brActivity();
        CDataHandle.Activity activity = new CDataHandle.Activity();

        activity.Subject = Convert.ToString(Request.QueryString["subject"]);
        activity.Comments = "";

        string strartDate = Convert.ToString(Request.QueryString["startDate"]);
        string[] split = strartDate.Split(new Char[] { '-' });
        DateTime strartDateTime = Convert.ToDateTime(split[0] + "/" + split[1] + "/" + split[2] + " " + split[3] + ":" + split[4] + ":" + split[5]);
        activity.StartDate = strartDateTime;

        string endDate = Convert.ToString(Request.QueryString["endDate"]);
        string[] split1 = endDate.Split(new Char[] { '-' });
        DateTime endDateTime = Convert.ToDateTime(split1[0] + "/" + split1[1] + "/" + split1[2] + " " + split1[3] + ":" + split1[4] + ":" + split1[5]);
        activity.EndDate = endDateTime;

        activity.ReminderDate = strartDateTime;

        if (activityStatus != null)
            activity.Status = activityStatus.TableCode;

        if (activityPriority != null)
            activity.Priority = activityPriority.TableCode;

        //activity.ActivityType = (app.Category != null ? app.Category.CategoryName : "");
        activity.ActivityType = "COLD";
        activity.CreatedBy = UserSession.Instance.UserName;
        activity.CreatedDate = DateTime.Now;
        activity.AssignedTo = UserSession.Instance.UserName;
        activity.SendReminder = "N";

        try
        {
            iNoRecs = oActivity.Save(activity, ref iReminderActivityId);

            if (iNoRecs > 0)
            {
                txt.Append("success");
            }
            else
                txt.Append("error");
        }
        catch (Exception)
        {

            throw;
        }
        return txt.ToString();
    }

    private string DeleteActivityType(string activityId)
    {
        bool success = false;
        StringBuilder txt = new StringBuilder();
        success = ActivityTypeBR.Instance.DeleteActivityType(activityId);
        if (success)
        {
            txt.Append("success");
        }
        else
            txt.Append("error");

        return txt.ToString();
    }

    private string ActivityColor(string activityCode)
    {
        StringBuilder txt = new StringBuilder();
        ActivityClient obrActivity = new ActivityClient();
        ActivityDTO activityType = new ActivityDTO();

        try
        {
            activityType = obrActivity.GetActivityType(activityCode);

            Color activityColor = System.Drawing.ColorTranslator.FromHtml(activityType.ActivityTypeColour);
            String rtn = "#" + activityColor.R.ToString("X2") + activityColor.G.ToString("X2") + activityColor.B.ToString("X2");

            txt.Append(activityType.Description + "/" + rtn);
        }
        catch (Exception)
        {
            throw;
        }
        return txt.ToString();
    }
    */
}