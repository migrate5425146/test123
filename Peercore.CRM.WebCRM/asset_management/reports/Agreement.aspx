﻿<%@ Page Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="Agreement.aspx.cs" Inherits="dashboard_transaction_Agreement" EnableEventValidation="false" %>

<%@ Register Src="~/usercontrols/buttonbar_reports.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ Register Src="~/usercontrols/toolbar_reports.ascx" TagPrefix="ucl" TagName="toolbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
	<asp:HiddenField ID="hiddenUser" runat="server" ClientIDMode="Static" />
	<asp:HiddenField ID="hiddenMobile" runat="server" ClientIDMode="Static" />
	<asp:HiddenField ID="hiddenSIM" runat="server" ClientIDMode="Static" />
	<asp:HiddenField ID="hiddenPrinter" runat="server" ClientIDMode="Static" />
	<div class="divcontectmainforms">
	</div>
	<div align="right">
		<div class="bg" align="left">
			<div id="div_message" class="savemsg" runat="server" style="display: none">
			</div>

			<div style="padding-left: 10px; margin-bottom: 2px;">
				<table border="0" style="margin-top: 8px;">
					<tr>
						<td style="color: white;">
							<asp:Label ID="lblUser" runat="server" Text="User Type" Width="120px"></asp:Label>
						</td>
						<td>
							<asp:DropDownList ID="ddlUserType" runat="server" Visible="true" Width="250px" AutoPostBack="true" OnSelectedIndexChanged="ddlUserType_SelectedIndexChanged">
								<%--onchange="LoadUsers()" --%>
								<%--<asp:ListItem Text="Select User Type" Value="" Selected="True"></asp:ListItem>--%>
								<asp:ListItem Text="Sales Rep" Value="DR"></asp:ListItem>
								<asp:ListItem Text="Distributor" Value="DIST"></asp:ListItem>
								<%--<asp:ListItem Text="Outlet" Value="CUST"></asp:ListItem>--%>
								<%--<asp:ListItem Text="ASC" Value="ASC"></asp:ListItem>--%>
								<%--<asp:ListItem Text="Admin" Value="ADMIN"></asp:ListItem>--%>
								<%--<asp:ListItem Text="SLC" Value="SLC"></asp:ListItem>--%>
							</asp:DropDownList>
						</td>
						<td style="color: white;">
							<div class="rep">
							<asp:Label ID="lblReps" runat="server" Text="Sales Rep Name" Width="120px"></asp:Label>
							</div>
							<div class="cust">
							<asp:Label ID="lblOutlet" runat="server" Text="Outlet Code" Width="120px"></asp:Label>
							</div>
						</td>
						<td>
							<div class="rep">
							<asp:DropDownList ID="ddlUserCode" runat="server" class="input-large1" Width="250px" AutoPostBack="true" ClientIDMode="Static" OnSelectedIndexChanged="ddlUserCode_SelectedIndexChanged">
								<%--onchange="onChangeUser()" --%>
							</asp:DropDownList>
							</div>
							<div class="cust">
                                <asp:TextBox ID="txtCustomer" class="input-large1" Width="250px" runat="server"></asp:TextBox>
							</div>
						</td>
						<td>
							<div class="cust">
								<asp:Button ID="btnLoadUserDetails" runat="server" Text="Load" OnClick="btnLoadUserDetails_Click" />
							</div>
							<%-- <input type="submit" value="Print" id="btnPrintReport" />--%>
						</td>
						<td>
							<div class="cust">
                                <asp:Label ID="lblcusName" class="input-large1" ForeColor="White" runat="server" ></asp:Label>
							</div>
						</td>
					</tr>
				</table>
			</div>
			<div class="clearall">
            </div>
			<div style="padding-left: 10px;">
				<table border="0" style="margin-top: 8px;">
					<tr>
						<td style="color: white;">
							<asp:Label ID="lblAgreement" runat="server" Text="Agreement Type" Width="120px"></asp:Label>
						</td>
						<td>
							<asp:DropDownList ID="ddlAgreement" runat="server" Visible="true" Width="250px" onchange="onChangeAgreement(this)">
								<asp:ListItem Text="Mobile Agreement" Value="mobile_agreement" Selected="True"></asp:ListItem>
								<asp:ListItem Text="Printer Agreement" Value="printer_agreement"></asp:ListItem>
							</asp:DropDownList>
						</td>
					</tr>
					<tr id="mobile">
						<td style="color: white;">
							<asp:Label ID="lblMobile" runat="server" Text="Mobile" Width="120px"></asp:Label>
						</td>
						<td>
							<asp:DropDownList ID="ddlMobile" runat="server" class="input-large1" Width="250px" AutoPostBack="true" ClientIDMode="Static">
								<%--onchange ="onChangeMobile()"--%>
							</asp:DropDownList>
						</td>
						<td style="color: white;">
							<asp:Label ID="lblSim" runat="server" Text="SIM" Width="120px"></asp:Label>
						</td>
						<td>
							<asp:DropDownList ID="ddlSIM" runat="server" class="input-large1" Width="250px" onchange="onChangeSIM()" ClientIDMode="Static">
							</asp:DropDownList>
						</td>
					</tr>
					<tr id="printer" style="display: none;">
						<td style="color: white;">
							<asp:Label ID="lblPrinter" runat="server" Text="Printer" Width="120px"></asp:Label>
						</td>
						<td>
							<asp:DropDownList ID="ddlPrinter" runat="server" class="input-large1" Width="250px" onchange="onChangePrinter()" ClientIDMode="Static">
							</asp:DropDownList>
						</td>
					</tr>
					<tr>
						<td></td>
						<td>
							<asp:Button ID="btnPrintReport" runat="server" Text="Print Agreement" />
							<%-- <input type="submit" value="Print" id="btnPrintReport" />--%>
						</td>
					</tr>
				</table>

			</div>

		</div>
	</div>
	<div class="clearall">
		&nbsp;
	</div>
	<div style="margin-top: 25px;">
		<div style="min-width: 500px; height: 500px;" id="target">
			<embed src="" width="100%" height="100%" />
		</div>
	</div>
	<script type="text/javascript">
        $(document).ready(function () {

            $("#MainContent_toolbar1_a_agreement").css("background-color", "#ff6600");
            //var u = resetPage();

            //if (u != null) {
            //    console.log('user ',u);
            //    /*document.getElementById("ddlUserCode").value = u;*/
            //    $("#ddlUserCode option[value=" + u + "]").attr('selected', 'selected');
            //}
			var agreement = $("#<%= ddlAgreement.ClientID %>").val();
            var userType = $("#<%= ddlUserType.ClientID %>").val();

			onChangeAgreement(agreement);

			if (userType != "") {
				if (userType == "CUST") {
					$(".rep").hide();
					$(".cust").show();
				}
				else {
					$(".rep").show();
                    $(".cust").hide();
                }
            } else {
                $(".rep").hide();
                $(".cust").hide();
            }

        });

        function loadpdf(name) {

            $("#target embed").attr("src", name);
        }

        function onChangeAgreement(ddlAgreement) {

            var agreement = document.getElementById('<%=ddlAgreement.ClientID%>').value;
            /*clearFields();*/
            if (agreement == 'mobile_agreement') {
                $("#printer").hide();
                $("#mobile").show();
            }
            if (agreement == 'printer_agreement') {
                $("#printer").show();
                $("#mobile").hide();
            }
        }

        function LoadUsers() {
            clearFields();
            var strUser = document.getElementById('<%=ddlUserType.ClientID%>').value;
            //console.log(ddluserTypes);
            var params = "{'userType':'" + strUser + "'}";

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/asset_management/asset_service.asmx/LoadUserCodes",
                data: params,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {
                    var ddlUserCode = $("[id*=ddlUserCode]");

                    ddlUserCode.empty().append('<option selected="selected" value="0">- select -</option>');

                    $.each(r.d, function () {
                        ddlUserCode.append($("<option></option>").val(this['Originator']).html(this['Originator'] + " - " + this['Name']));
                    });


                },
                error: function () {
                    alert("An error has occurred during processing your request.");
                    //var existsMsg = GetErrorMessageDiv("An error has occurred during processing your request.", "MainContent_div_message_popup");
                    //$('#MainContent_div_message').css('display', 'block');
                    //$("#MainContent_div_message").html(existsMsg);

                    //hideStatusDiv("MainContent_div_message");
                    return;
                }
            });
        }

        function onChangeUser() {
            var agreement = $("#<%= ddlAgreement.ClientID %>").val();
            var user = $("#<%= ddlUserCode.ClientID %>").val();

            $("#hiddenUser").val(user);

            //document.getElementById("#hiddenUser").value = user;

            console.log(user);

            if (agreement == 'mobile_agreement') {
                loadAssetDetail(user, 'SIM', 'ddlSIM');
                loadAssetDetail(user, 'Phone', 'ddlMobile');
            }
            else if (agreement == 'printer_agreement') {
                loadAssetDetail(user, 'Printer', 'ddlPrinter');
            }
        }

        function onChangeMobile() {
            var mobile = $("#<%= ddlMobile.ClientID %>").val();
            $("#hiddenMobile").val(mobile);
        }

        function onChangeSIM() {
            var sim = $("#<%= ddlSIM.ClientID %>").val();
            $("#hiddenSIM").val(sim);
        }

        function onChangePrinter() {
            var printer = $("#<%= hiddenPrinter.ClientID %>").val();
            $("#hiddenPrinter").val(printer);
        }

        function loadAssetDetail(user, asset, ddl) {

            var params = { "user": user, "asset": asset };

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/asset_management/asset_service.asmx/LoadAssetsForAgreement",
                data: JSON.stringify(params),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (r) {

                    var ddlcode = $("[id*=" + ddl + "]");
                    ddlcode.empty().append('<option selected="selected" value="0">- select -</option>');
                    $.each(r.d, function () {
                        ddlcode.append($("<option></option>").val(this['AssetCode']).html(this['AssetCode'] + " - " + this['SerialNo']));
                    });
                },
                error: function () {
                    alert("An error has occurred during processing your request.");
                    //var existsMsg = GetErrorMessageDiv("An error has occurred during processing your request.", "MainContent_div_message_popup");
                    //$('#MainContent_div_message').css('display', 'block');
                    //$("#MainContent_div_message").html(existsMsg);

                    //hideStatusDiv("MainContent_div_message");
                    return;
                }
            });

        }

        function clearFields() {
            $("#<%= ddlMobile.ClientID %>").empty();
            $("#<%= ddlPrinter.ClientID %>").empty();
			$("#<%= ddlSIM.ClientID %>").empty();
		}

		$("#btnPrintReport").on("click", function () {

			var userCode = $("#<%= ddlUserCode.ClientID %>").val();

			var url = ROOT_PATH + 'dashboard/transaction/Agreement.aspx?fm=print&user=' + userCode;
			$.ajax({
				url: url,
				dataType: "html",
				cache: false,
				success: function (msg) {
					alert('post');
					return;
				},
				error: function (msg, textStatus) {
					if (textStatus == "error") {
						var msg = msg;
						/*$("#div_loader").hide();*/

					}
				}
			});
		});

		function resetPage() {
			LoadUsers();
			var agreement = $("#<%= ddlAgreement.ClientID %>").val();

			onChangeAgreement(agreement);

			var user = $("#hiddenUser").val();

			if (agreement == 'mobile_agreement') {
				loadAssetDetail(user, 'SIM', 'ddlSIM');
				loadAssetDetail(user, 'Phone', 'ddlMobile');

				var mob = $("#hiddenMobile").val();
				var sm = $("#hiddenSIM").val();


			}
			else if (agreement == 'printer_agreement') {
				loadAssetDetail(user, 'Printer', 'ddlPrinter');

				var prn = $("#hiddenPrinter").val();
			}

			document.getElementById("ddlUserCode").value = "00678";

			$("#<%= ddlUserCode.ClientID %>").val(user);
			$("#<%= ddlMobile.ClientID %>").val(mob);
			$("#<%= ddlSIM.ClientID %>").val(sm);
			$("#<%= ddlPrinter.ClientID %>").val(prn);

            document.getElementById('ddlUserCode').innerHTML = user;

            return user;
        }


    </script>
</asp:Content>
