﻿using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class asset_management_payments_payment_upload : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != "")
            {
                if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
                {
                    if (Request.QueryString["type"] == "delete")
                    {
                        if (Request.QueryString["fm"].ToLower() == "deletepayment")
                        {
                            Response.Expires = -1;
                            Response.Clear();
                            string getdocumentContent = DeletePayments();
                            Response.ContentType = "text/html";
                            Response.Write(getdocumentContent);
                            Response.End();
                        }
                    }
                }
            }

            if (!IsPostBack)
            {
                //
            }

            Master.SetBreadCrumb("SR - Monthly Payments ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
        OriginatorString.Value = UserSession.Instance.UserName;
    }

    protected void excelUpload_Click(object sender, EventArgs e)
    {
        CommonUtility commonUtility = new CommonUtility();
        HttpSessionState session = HttpContext.Current.Session;
        string sessionID = session.SessionID;

        try
        {
            if (xmlUpload.HasFile)
            {
                string FileName = Path.GetFileName(xmlUpload.PostedFile.FileName);
                string Extension = Path.GetExtension(xmlUpload.PostedFile.FileName);
                string FolderPath = HttpContext.Current.Request.PhysicalApplicationPath + "\\uploads";

                string[] validFileTypes = { ".xls", ".xlsx" };

                string FilePath = FolderPath + "\\" + FileName;

                bool isValidType = false;
                isValidType = validFileTypes.Contains(Extension);

                if (!isValidType)
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileValidator()", true);
                }
                else
                {
                    xmlUpload.SaveAs(FilePath);
                    PaymentsClient paymentsClient = new PaymentsClient();
                    string UserName = UserSession.Instance.UserName;

                    if (paymentsClient.UploadPayments(FilePath, UserName))
                    {
                        xmlUpload.Attributes.Clear();
                        div_message.Attributes.Add("style", "display:block");
                        div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Uploaded.");
                        // Response.Redirect("~/asset_management/master/asset.aspx");
                        // Response.Write("<script>alert('Successfully Uploaded!');</script>");
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileFormatValidator()", true);
                    }
                    xmlUpload.Dispose();
                }
            }
            else
            {
                Response.Write("<script>alert('Select file to Upload!');</script>");
            }
        }
        catch (Exception)
        {
            throw;
        }
    }

    private string DeletePayments()
    {
        try
        {
            int Id = 0;
            PaymentsClient paymentService = new PaymentsClient();

            if (Request.QueryString["paymentid"] != null && Request.QueryString["paymentid"] != "")
            {
                Id = Convert.ToInt32(Request.QueryString["paymentid"].ToString());
            }

            bool status = paymentService.DeletePayment(Id, UserSession.Instance.UserName);

            return "true";
        }
        catch (Exception)
        {
            return "false";
        }
    }

}
