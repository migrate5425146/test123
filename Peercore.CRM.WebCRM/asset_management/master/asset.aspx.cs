﻿using CRMServiceReference;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.SessionState;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class asset_management_master_asset : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != "")
            {
                if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
                {
                    if (Request.QueryString["type"] == "delete")
                    {
                        if (Request.QueryString["fm"].ToLower() == "deleteasset")
                        {
                            Response.Expires = -1;
                            Response.Clear();
                            string getdocumentContent = DeleteAsset();
                            Response.ContentType = "text/html";
                            Response.Write(getdocumentContent);
                            Response.End();
                        }
                    }
                }
            }

            if (!IsPostBack)
            {
            }
            OriginatorString.Value = UserSession.Instance.UserName;
            LoadAssetTypeDropdown();

            Master.SetBreadCrumb("Asset Master ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    protected void excelUpload_Click(object sender, EventArgs e)
    {
        CommonUtility commonUtility = new CommonUtility();
        HttpSessionState session = HttpContext.Current.Session;
        string sessionID = session.SessionID;

        try
        {
            if (xmlUpload.HasFile)
            {
                string FileName = Path.GetFileName(xmlUpload.PostedFile.FileName);
                string Extension = Path.GetExtension(xmlUpload.PostedFile.FileName);
                string FolderPath = HttpContext.Current.Request.PhysicalApplicationPath + "\\uploads";
                string logPath = Server.MapPath(ConfigurationManager.AppSettings["LogPath"]);
                string logUrl = ConfigurationManager.AppSettings["LogURL"];

                string[] validFileTypes = { ".xls", ".xlsx" };

                string FilePath = FolderPath + "\\" + FileName;

                bool isValidType = false;
                isValidType = validFileTypes.Contains(Extension);

                if (!isValidType)
                {
                    ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileValidator()", true);
                }
                else
                {
                    xmlUpload.SaveAs(FilePath);
                    AssetsClient assetClient = new AssetsClient();
                    string UserName = UserSession.Instance.UserName;

                    LogError("Asset-5-" + logPath);
                    if (assetClient.UploadAssetExcel(FilePath, UserName, logPath))
                    {
                        LogError("6");
                        xmlUpload.Attributes.Clear();
                        div_message.Attributes.Add("style", "display:block");
                        div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Uploaded.");

                        //string logFilePath = logPath + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";

                        //using (System.IO.StreamReader Reader = new System.IO.StreamReader(logFilePath))
                        //{
                        //    string fileContent = Reader.ReadToEnd();
                        //    if (!string.IsNullOrEmpty(fileContent))
                        //    {
                        //        string cleanMessage = fileContent.Replace("\r\n", " | ");
                        //        Page page = HttpContext.Current.CurrentHandler as Page;
                        //        string script = string.Format("alert('{0}');", cleanMessage);
                        //        if (page != null && !page.ClientScript.IsClientScriptBlockRegistered("alert"))
                        //        {
                        //            page.ClientScript.RegisterClientScriptBlock(page.GetType(), "alert", script, true /* addScriptTags */);
                        //        }
                        //    }

                        //}

                        string pattern = "*.txt";
                        var dirInfo = new DirectoryInfo(logPath);
                        var file = (from f in dirInfo.GetFiles(pattern) orderby f.LastWriteTime descending select f).First();

                        string logFilePath = logUrl + file;

                        linkLog.Visible = true;
                        linkLog.NavigateUrl = logFilePath;

                        LogError("7");

                        try
                        {
                            CommonClient cClient = new CommonClient();
                            cClient.CreateTransactionLog(UserName,
                                DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                                TransactionTypeModules.Insert,
                                TransactionModules.AssetMaster,
                                UserName + " upload asset master file " + FileName);
                        }
                        catch { }
                    }
                    else
                    {
                        ScriptManager.RegisterClientScriptBlock(this, GetType(), "filevalidate", "fileFormatValidator()", true);
                    }

                    xmlUpload.Dispose();
                }
            }
            else
            {
                Response.Write("<script>alert('Select file to Upload!');</script>");
            }
        }
        catch (Exception ex) { LogError("8-" + ex.Message); }
    }

    public static void LogError(string error)
    {
        try
        {
            //string logFilePath = @"C:\Logs\Log-" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";
            string logFilePath = @"D:\Logs\ErrorLog_" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";
            FileInfo logFileInfo = new FileInfo(logFilePath);
            DirectoryInfo logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
            if (!logDirInfo.Exists) logDirInfo.Create();
            File.WriteAllText(logFilePath, String.Empty);

            using (FileStream fileStream = new FileStream(logFilePath, FileMode.Append))
            {
                using (StreamWriter log = new StreamWriter(fileStream))
                {
                    log.WriteLine(error);
                }
            }
        }
        catch (Exception)
        {
        }
    }

    private void LoadAssetTypeDropdown()
    {
        ArgsModel args = new ArgsModel();
        args.StartIndex = 1;
        args.RowCount = 50;
        args.OrderBy = "  asset_type_name asc";

        AssetsClient assetClient = new AssetsClient();
        List<AssetTypeModel> data = new List<AssetTypeModel>();
        data = assetClient.GetAllAssetTypes(args);

        if (data.Count != 0)
        {
            ddlAssetTypes.Items.Clear();
            ddlAssetTypes.Items.Add(new ListItem("- Select -", "0"));
            foreach (AssetTypeModel item in data)
            {
                ddlAssetTypes.Items.Add(new ListItem(item.AssetTypeName, item.AssetTypeId.ToString()));

            }
            ddlAssetTypes.Enabled = true;
        }
        else
        {
            ddlAssetTypes.Items.Add(new ListItem("- Select -", "0"));
            ddlAssetTypes.Enabled = false;
        }
    }

    private string DeleteAsset()
    {
        try
        {
            int Id = 0;
            AssetsClient assetService = new AssetsClient();

            if (Request.QueryString["assetid"] != null && Request.QueryString["assetid"] != "")
            {
                Id = Convert.ToInt32(Request.QueryString["assetid"].ToString());
            }
            List<AssetAssignModel> assignAssets = assetService.GetAllAssignAssetsByAssetId(Id);
            if (assignAssets.Count > 0)
            {
                return "assigned";
            }

            bool status = assetService.DeleteAsset(Id, UserSession.Instance.UserName);
            if (status)
                return "true";
            return "false";
        }
        catch (Exception)
        {
            return "false";
        }
    }

}
