﻿<%@ Page Title="mSales | Asset Types" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="assets_type.aspx.cs" Inherits="asset_management_master_assets_type" %>

<%@ MasterType TypeName="SiteMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server" />
    <asp:HiddenField ID="saveUpdateOption" runat="server" />
    <asp:HiddenField ID="hdnSelectedAssetTypeId" runat="server" ClientIDMode="Static" />

    <div id="div_main" class="divcontectmainforms">
        <div id="window" style="display: none; width: 500px;">
            <div style="width: 100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td colspan="2" class="textalignbottom">Asset Type 
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtassettype" maxlength="30" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2" class="textalignbottom">Description 
                                </td>
                                <td colspan="4">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtdescrption" maxlength="200" />
                                    </span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonSaveAssetType" type="button">Save</button>
                                    <button class="k-button" id="buttonClear">Clear</button>
                                    <asp:HiddenField ID="hdnSelectedId" runat="server" ClientIDMode="Static" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="hoributton">
            <%-- <div class="addlinkdiv" style="float: left" id="div_address">--%>
            <%--<a id="id_add_asset_type"  clientidmode="Static" runat="server" href="#">Add Asset Type</a>--%>
            <a id="id_add_asset_type" class="sprite_button" style="float: left; margin-left: 5px; margin-top: 0px;">
                <span class="plus icon"></span>Add Asset Type</a>
        </div>
        <br />
        <div class="toolbar_container" style="display: none;">
            <div class="toolbar_left" id="div_content">
            </div>

            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
                        <div class="back">
                        </div>
                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div class="clearall">
        </div>
        <div style="min-width: 200px; overflow: auto">
            <div style="float: left; width: 100%; overflow: auto">

                <div id="grid_asset_type"></div>

            </div>
        </div>
    </div>

    <div id="AssetmodalWindow" style="display: none">
        <div id="div_AssetConfirm_Message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>

    <script type="text/javascript">

        var take_grid = $("#MainContent_hfPageIndex").val();

        $(document).ready(function () {
            LoadAllAssetTypes();
            $("#txtassettype").focus();

        });

        //Load popup for Add new Asset Type
        $("#id_add_asset_type").click(function () {
            $("#window").css("display", "block");
            ClearControls();
            $("#hdnSelectedId").val('');

            $("#txtassettype").val('');

            var wnd = $("#window").kendoWindow({
                title: "Add/Edit Asset Type",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            $("#txtassettype").focus();
            wnd.center().open();
        });

        //Load popup for Add new Asset Type
        $("#id_reload").click(function () {
            LoadAllAssetTypes();
        });

        //Save button click
        $("#buttonSaveAssetType").click(function () {
            saveAssetType();
        });

        $("#buttonClear").click(function () {
            ClearControls();
        });

        function deleteAssetType(asset_id, assetType) {
            $("#AssetmodalWindow").show()
            deleteType(asset_id, assetType);
        }

        function deleteType(asset_id, assetType) {

            $("#save").css("display", "none");
            $("#no").css("display", "inline-block");
            $("#yes").css("display", "inline-block");

            var message = "Do you want to delete '" + assetType + "' ?";

            var wnd = $("#AssetmodalWindow").kendoWindow({
                title: "Delete Asset Type",
                modal: true,
                visible: false,
                resizable: false,
                width: 400
            }).data("kendoWindow");

            $("#div_AssetConfirm_Message").text(message);
            wnd.center().open();

            $("#yes").click(function () {

                var root = 'asset_management/master/assets_type.aspx?fm=deleteAssetType&type=delete&assetid=' + asset_id;
                delete_asset_type(root);

            });

            $("#no").click(function () {
                closepopupAsset();
            });

        }

        function delete_asset_type(targeturl) {
            $("#div_loader").show();
            var url = ROOT_PATH + targeturl;

            $.ajax({
                url: url,
                dataType: "html",
                cache: false,
                success: function (msg) {
                    if (msg == "true") {
                        var sucessMsg = GetSuccesfullMessageDiv("Asset type was successfully Deleted.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        closepopupAsset();
                        LoadAllAssetTypes();
                        hideStatusDiv("MainContent_div_message");
                        $("#div_loader").hide();
                        return;
                    }
                    else {
                        var existsMsg = GetErrorMessageDiv("Error occured", "MainContent_div_message_popup");

                        $('#MainContent_div_message_popup').css('display', 'block');
                        $("#MainContent_div_message_popup").html(existsMsg);
                        closepopupAsset();
                        hideStatusDiv("MainContent_div_message_popup");
                        return;
                    }
                },
                error: function (msg) {
                    if (msg == "error") {
                        var msg = msg;
                        $("#div_loader").hide();
                    }
                }
            });
        }

        function saveAssetType() {

            var selectedTypeId = $("#hdnSelectedAssetTypeId").val();

            if (($("#txtassettype").val()).trim() == '') {
                var sucessMsg = GetErrorMessageDiv("Please Enter Asset Type.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }

            if (($("#txtassettype").val()).trim() == '') {
                var sucessMsg = GetErrorMessageDiv("Please Enter Asset Type.", "MainContent_div_message_popup");
                $('#MainContent_div_message_popup').css('display', 'block');
                $("#MainContent_div_message_popup").html(sucessMsg);
                hideStatusDiv("MainContent_div_message_popup");
                return;
            }

            if ($("#txtassettype").val()) {

                var Originator = $("#<%= OriginatorString.ClientID %>").val();
                var assetType = document.getElementById('txtassettype').value;
                var description = document.getElementById('txtdescrption').value;

                var param = { "assetType": assetType, "description": description, "Originator": Originator, "id": selectedTypeId };

                $.ajax({
                    type: "POST",
                    url: ROOT_PATH + "service/asset_management/asset_service.asmx/SaveAssetType",
                    data: JSON.stringify(param),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (response) {
                        var sucessMsg = GetSuccesfullMessageDiv("Asset Type was successfully Saved.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);
                        closepopup();
                        LoadAllAssetTypes();
                        ClearControls();
                        hideStatusDiv("MainContent_div_message");
                    },
                    error: function (response) {
                        if (textStatus == "error") {
                            var msg = msg;
                        }
                    }
                });
            }
        }

        function closepopup() {
            var wnd = $("#window").kendoWindow({
                title: "Add/Edit Asset Type",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#window").css("display", "none");
        }

        function closepopupAsset() {
            var wnd = $("#AssetmodalWindow").kendoWindow({
                title: "Add/Edit Asset Type",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#AssetmodalWindow").css("display", "none");
        }

        function ClearControls() {
            $("#txtassettype").val('');
            $("#txtdescrption").val('');
            $("#hdnSelectedAssetTypeId").val('');
        }

        function EditAssetType(id, name, description) {

            $("#hdnSelectedAssetTypeId").val(id);
            $("#txtassettype").val(name);
            $("#txtdescrption").val(description);

            $("#window").css("display", "block");
            var wnd = $("#window").kendoWindow({
                title: "Edit Asset Type",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");

            $("#txtassettype").focus();

            wnd.center().open();
        }

        function LoadAllAssetTypes() {

            var url = "javascript:EditAssetType('#=AssetTypeId#','#=AssetTypeName#','#=Description#');";

            var urlAssetTypeId = "<a href=\"" + url + "\">#=AssetTypeId#</a>";
            var urlAssetTypeName = "<a href=\"" + url + "\">#=AssetTypeName#</a>";
            var urlAssetDescription = "<a href=\"" + url + "\">#=Description#</a>";

            //$("#div_loader").show();
            $("#grid_asset_type").html("");
            $("#grid_asset_type").kendoGrid({
                columns: [
                    { field: "AssetTypeId", hidden: true },
                    //{ field: "AssetTypeName", title: "Asset Type", width: "100px" },
                    //{ field: "Description", title: "Description", width: "200px" },
                    { template: urlAssetTypeId, field: "AssetTypeId", width: "105px", hidden: true },
                    { template: urlAssetTypeName, field: "AssetTypeName", title: "Asset Type", width: "105px" },
                    { template: urlAssetDescription, field: "Description", title: "Description", width: "105px" },
                    //{ field: "createdDate", title: "Created Date", width: "200px", hidden: true },
                    {
                        template: '<a href="javascript:deleteAssetType(\'#=AssetTypeId#\', \'#=AssetTypeName#\');">Delete</a>',
                        field: "",
                        width: "60px"
                    }
                ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                filterable: true,
                navigatable: true,
                editable: "popup",
                dataBound: onDataBound,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 15,
                    batch: true,
                    schema: {
                        data: "d.Data",
                        total: "d.Total",
                        model: {
                            fields: {
                                AssetTypeId: { type: "number" },
                                AssetTypeName: { type: "string" },
                                Description: { type: "string" },
                                CreatedDate: { type: "date", format: "{0:MM/dd/yyyy}" }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/asset_management/asset_service.asmx/GetAllAssetTypes",
                            contentType: "application/json; charset=utf-8",
                            data: { pgindex: take_grid },
                            type: "POST",
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {

                                    var entityGrid = $("#grid_asset_type").data("kendoGrid");
                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));
                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }

            });
        }

        function ValidateType() {
            var type = document.getElementById("txtassettype");
            toLower = type.value.toLowerCase();
            $("#txtassettype").val(toLower);

            var params = "{'assetType':'" + toLower + "'}";
            console.log(params);
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/asset_management/asset_service.asmx/IsAssetTypeAvailable",
                data: params,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    console.log(response.d.Status);
                    if (response.d.Status) {
                        alert("Asset Type '" + toLower + "' Already Exist.");
                        $("#txtassettype").val("");
                        $("#txtassettype").focus();
                    }
                    else {
                        $("#txtassettype").val(type.value);
                    }
                },
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                        $("#div_loader").hide();
                    }
                }
            });

        }

    </script>
</asp:Content>





