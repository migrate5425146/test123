﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
//using Peercore.CRM.Entities;
//using Peercore.CRM.BusinessRules;
using System.Text;
using Peercore.CRM.Shared;
//using Peercore.CRM.Entities.CompositeEntities;
using CRMServiceReference;

public partial class pipelines_stage_process_forms_processmaster : System.Web.UI.Page
{
    #region Constant
    private const string PIPELINE_DATA = "PIPELINE_DATA";
    #endregion Constant
    #region Properties

    private KeyValuePair<string, string> PipelineChart
    {
        get
        {
            if (Session[PIPELINE_DATA] != null)
            {
                return (KeyValuePair<string, string>)Session[PIPELINE_DATA];
            }
            return new KeyValuePair<string, string>("0", PIPELINE_DATA);
        }
        set
        {
            Session[PIPELINE_DATA] = value;
        }
    }
    #endregion Properties
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["fm"] != null && Request.QueryString["fm"] != "")
        {
            if (Request.QueryString["type"] != null && Request.QueryString["type"] != "")
            {
                if (Request.QueryString["type"] == "get")
                {
                    if (Request.QueryString["fm"] == "pipelineStage")
                    {
                        //Response.Expires = -1;
                        //Response.Clear();
                        //string getPipeLinestage = GetPipeLineStage(Request.QueryString["id"].ToString());
                        //Response.ContentType = "text/html";
                        //Response.Write(getPipeLinestage);
                        //Response.End();
                    }
                    if (Request.QueryString["fm"] == "opportunity")
                    {
                        //Response.Expires = -1;
                        //Response.Clear();
                        //double getSumopportunity = GetSumopportunity();
                        //Response.ContentType = "text/html";
                        //Response.Write(getSumopportunity);
                        //Response.End();
                    }
                }
                else if (Request.QueryString["type"] == "Update")
                {
                    if (Request.QueryString["fm"] == "pipelineStage")
                    {
                        //Response.Expires = -1;
                        //Response.Clear();
                        //string savePipeLinestage = UpdatePipeLineStage(Request.QueryString["pps"], Request.QueryString["id"]);
                        //Response.ContentType = "text/html";
                        //Response.Write(savePipeLinestage);
                        //Response.End();
                    }
                }
                else if (Request.QueryString["type"] == "Delete")
                {
                    if (Request.QueryString["fm"] == "pipelineStage")
                    {
                        //Response.Expires = -1;
                        //Response.Clear();
                        //string deletePipeLinestage = DeletePipeLine(Request.QueryString["id"].ToString());
                        //Response.ContentType = "text/html";
                        //Response.Write(deletePipeLinestage);
                        //Response.End();
                    }
                }
                else if (Request.QueryString["type"] == "query")
                {
                    if (Request.QueryString["fm"] == "pipelineStage")
                    {
                        //Response.Expires = -1;
                        //Response.Clear();
                        //string InsertPipeLinestageQuery = GetInsertPipeLineQuery();
                        //Response.ContentType = "text/html";
                        //Response.Write(InsertPipeLinestageQuery);
                        //Response.End();
                    }
                    else if (Request.QueryString["fm"] == "oppportunityentry")
                    {
                        if (Request.QueryString["querytype"] == "originator")
                        {
                            //Response.Expires = -1;
                            //Response.Clear();
                            //string InsertoriginatorQuery = Getoriginator();
                            //Response.ContentType = "text/html";
                            //Response.Write(InsertoriginatorQuery);
                            //Response.End();
                        }
                        if (Request.QueryString["querytype"] == "repgroup")
                        {
                            //Response.Expires = -1;
                            //Response.Clear();
                            //string GetrepgroupQuery = Getrepgroup();
                            //Response.ContentType = "text/html";
                            //Response.Write(GetrepgroupQuery);
                            //Response.End();
                        }
                        if (Request.QueryString["querytype"] == "product")
                        {
                            Response.Expires = -1;
                            Response.Clear();
                            string GetrepgroupQuery = GetCatalogForoppportunityentry();
                            Response.ContentType = "text/html";
                            Response.Write(GetrepgroupQuery);
                            Response.End();
                        }
                    }
                }
                else if (Request.QueryString["type"] == "insert")
                {
                    if (Request.QueryString["fm"] == "pipelineStage")
                    {
                        //Response.Expires = -1;
                        //Response.Clear();
                        //string InsertPipeLinestage = InsertPipeLineStage(Request.QueryString["pps"].ToString());
                        //Response.ContentType = "text/html";
                        //Response.Write(InsertPipeLinestage);
                        //Response.End();
                    }
                    else if (Request.QueryString["fm"] == "oppportunityentry")
                    {
                        //Response.Expires = -1;
                        //Response.Clear();
                        //string Oppportunity = InsertOppportunity();
                        //Response.ContentType = "text/html";
                        //Response.Write(Oppportunity);
                        //Response.End();
                    }
                }
                else if (Request.QueryString["type"] == "query")
                {
                    if (Request.QueryString["fm"] == "pipelineStage")
                    {
                        //Response.Expires = -1;
                        //Response.Clear();
                        //string InsertPipeLinestageQuery = GetInsertPipeLineQuery();
                        //Response.ContentType = "text/html";
                        //Response.Write(InsertPipeLinestageQuery);
                        //Response.End();
                    }
                }
            }
        }
    }

    private string GetCatalogForoppportunityentry()
    {
        StringBuilder txt = new StringBuilder();

        //Setting Args
        ArgsDTO args = new ArgsDTO();
        args.StartIndex = ConfigUtil.StartIndex;
        args.RowCount = ConfigUtil.MaxRowCount;

        CommonClient commonClient = new CommonClient();
        //KeyValuePairOfstringstring decriptioncol = new KeyValuePairOfstringstring();
        //decriptioncol.key = "rep_group_name";
        //decriptioncol.value = "BDM Group";

        //KeyValuePairOfstringstring codecol = new KeyValuePairOfstringstring();
        //codecol.key = "rep_group_id";
        //codecol.value = "BDM Group ID";

        //List<object> par = new List<object>();
        //par.Add("");
        //List<LookupDTO> lookupList = commonClient.GetLookups("CatalogLookup", "", par, args, false);

        List<CatalogDTO> lookupList = commonClient.GetAllCatalog(args);

        txt.Append("<table border=\"0\" id=\"AssetTypeTable\" cellpadding=\"0\" cellspacing=\"0\">");
        txt.Append("<tr class=\"detail-popsheader\">");
        txt.Append("<th></th>");
        txt.Append("<th>Catalog Code</th>");
        txt.Append("<th>Description</th>");
        txt.Append("</tr>");
        for (int i = 0; i < lookupList.Count; i++)
        {
            txt.Append("<tr class=\"tblborder\">");
            txt.Append("<td class=\"tblborder\"> <input type=\"radio\" name=\"group1\" value=\"" + lookupList[i].CatlogCode + "\" id=\"a\"/> <input type=\"hidden\" name=\"group1\" value=\"" + lookupList[i].Description + "\" id=\"b\"/> ");
            txt.Append("<input type=\"hidden\" name=\"group1\" value=\"" + lookupList[i].Conversion + "\" id=\"c\"/> <input type=\"hidden\" name=\"group1\" value=\"" + lookupList[i].Price + "\" id=\"d\"/> </td>");
            txt.Append("<td align=\"left\" class=\"tblborder\">" + lookupList[i].CatlogCode + "</td>");
            txt.Append("<td align=\"left\" class=\"tblborder\">" + lookupList[i].Description + "</td>");
            txt.Append("</tr>");
        }
        txt.Append("</table>");

        txt.Append("<script type='text/javascript'>");

        //txt.Append("$('#jqi_state0_buttonOk').click(function () {");
        //txt.Append("var asset_type_name = '';");
        //txt.Append("var selected_email = '';");
        //txt.Append("$('#AssetTypeTable tr').each(function () {");
        //txt.Append("    if ($(this).children(':eq(0)').find('#a').is(':checked')) {");
        //txt.Append("        var emailaddress = $(this).children(':eq(0)').find('#a').val();");
        //txt.Append("        selected_email = selected_email + emailaddress;");
        //txt.Append("        asset_type_name = $(this).children(':eq(0)').find('#b').val();");
        //txt.Append("    }");
        //txt.Append("});");
        ////txt.Append("PageMethods.SetProductList(selected_email,asset_type_name)");

        ////txt.Append("UpdateMe();");
        //txt.Append("$('#MainContent_HiddenFieldCode').val(selected_email);");
        //txt.Append("$('#MainContent_HiddenFieldDesc').val(asset_type_name);");
        //txt.Append("$('#MainContent_HiddenFieldPrice').val('0');");
        //txt.Append("})");

        txt.Append("</script>");

        return txt.ToString();
    }

    /*
    private double GetSumopportunity()
    {
        OpportunityClient OpportunityClient = new OpportunityClient();
        ArgsDTO args = new ArgsDTO();
        double sum = 0;

        try
        {
            args.ChildOriginators = UserSession.Instance.ChildOriginators;
            args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;

            KeyValuePair<string, string> pipelineChart = PipelineChart;
            if (pipelineChart.Key.Equals("0"))
                args.AdditionalParams = ") AND ( O.pipeline_stage_id <> " + pipelineChart.Key;
            else
                args.AdditionalParams = ") AND ( O.pipeline_stage_id = " + pipelineChart.Key;

            sum = OpportunityClient.GetOpportunitySum(args);

        }
        catch (Exception)
        {
            
            throw;
        }
        return sum;
    }


    private string InsertOppportunity()
    {
        StringBuilder txt = new StringBuilder();
        bool success = false;
        OpportunityCompositeEntity originator = new OpportunityCompositeEntity();

        if (!string.IsNullOrEmpty(Request.QueryString["id"].ToString()))
        {
            originator = OriginatorBR.Instance.GetOgirinator();
            OpportunityEntity opportunityEntity = new OpportunityEntity();
            opportunityEntity.OpportunityID = Guid.Parse(Request.QueryString["id"]);
            opportunityEntity.LeadID = 0;
            opportunityEntity.Name = Request.QueryString["contact"].ToString();
            opportunityEntity.CloseDate = Convert.ToDateTime(Request.QueryString["datetime"]);
            opportunityEntity.Stage = Request.QueryString["pipeline"].ToString();
            opportunityEntity.Probability = Convert.ToDouble(Request.QueryString["properbility"] + "0");

            if (!string.IsNullOrEmpty(Request.QueryString["ammount"]))
                opportunityEntity.Amount = Convert.ToDouble(Request.QueryString["ammount"]);

            if (!string.IsNullOrEmpty(Request.QueryString["units"]))
                opportunityEntity.Units = Convert.ToDouble(Request.QueryString["units"]);

            opportunityEntity.Description = Request.QueryString["description"].ToString();
            opportunityEntity.LastModifiedBy = originator.OriginatorId;
            opportunityEntity.LastModifiedDate = Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yy"));
            opportunityEntity.CustCode = Request.QueryString["customer"].ToString();
            opportunityEntity.SourceId = originator.LeadID;
            opportunityEntity.RepGroupID = System.Guid.NewGuid();
            success = OpportunityBR.Instance.UpdateOpportunities(opportunityEntity);
            if (success)
                txt.Append("true");
            else
                txt.Append("false");

        }
        else
        {
            originator = OriginatorBR.Instance.GetOgirinator();
            OpportunityEntity opportunityEntity = new OpportunityEntity();

            opportunityEntity.OpportunityID = System.Guid.NewGuid();
            opportunityEntity.LeadID = 0;
            opportunityEntity.OriginatorId = System.Guid.NewGuid();
            opportunityEntity.Name = Request.QueryString["contact"].ToString();
            opportunityEntity.CloseDate = Convert.ToDateTime(Request.QueryString["datetime"]);
            opportunityEntity.Stage = Request.QueryString["pipeline"].ToString();
            opportunityEntity.Probability = Convert.ToDouble(Request.QueryString["properbility"] + "0");

            if (!string.IsNullOrEmpty(Request.QueryString["ammount"]))
                opportunityEntity.Amount = Convert.ToDouble(Request.QueryString["ammount"]);

            if (!string.IsNullOrEmpty(Request.QueryString["units"]))
                opportunityEntity.Units = Convert.ToDouble(Request.QueryString["units"]);

            opportunityEntity.Description = Request.QueryString["description"].ToString();
            opportunityEntity.CreatedBy = originator.OriginatorId;
            opportunityEntity.CreatedDate = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy hh:mm:mm tt"));
            opportunityEntity.LastModifiedBy = originator.OriginatorId;
            opportunityEntity.LastModifiedDate = Convert.ToDateTime(DateTime.Now.ToString("MM/dd/yyyy hh:mm:mm tt"));
            opportunityEntity.CustCode = Request.QueryString["customer"].ToString();
            opportunityEntity.SourceId = originator.LeadID;
            opportunityEntity.RepGroupID = System.Guid.NewGuid();
            success = OpportunityBR.Instance.InsertOpportunities(opportunityEntity);
            if (success)
                txt.Append("true");
            else
                txt.Append("false");
        }


        return txt.ToString();
    }

    private string Getrepgroup()
    {//
        StringBuilder txt = new StringBuilder();
        ArgsDTO args = new ArgsDTO();
        args.StartIndex =ConfigUtil.StartIndex;
        args.RowCount = ConfigUtil.MaxRowCount;
        
        CookersServiceReference.CommonClient commonClient = new CommonClient();
        KeyValuePairOfstringstring decriptioncol = new KeyValuePairOfstringstring();
        decriptioncol.key = "rep_group_name";
        decriptioncol.value = "BDM Group";

        KeyValuePairOfstringstring codecol = new KeyValuePairOfstringstring();
        codecol.key = "rep_group_id";
        codecol.value = "BDM Group ID";

        List<LookupDTO> lookupList = commonClient.GetLookupsForBDM("crm_rep_group",
                        decriptioncol,
                        codecol, "",args, "");


        txt.Append("<table border=\"0\" id=\"repgroupTable\" width=\"300px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
        txt.Append("<tr class=\"detail-popsheader\">");
        txt.Append("<th></th>");
        txt.Append("<th>BDM Group ID</th>");
        txt.Append("<th>BDM Group</th>");
        txt.Append("</tr>");
        for (int i = 0; i < lookupList.Count; i++)
        {
            txt.Append("<tr>");
            txt.Append("<td style=\"padding-left:5px;\" class=\"tblborder\"> <input type=\"radio\" name=\"group1\" value=\"" + lookupList[i].Description + "\" id=\"a\"/>  <input type=\"hidden\" name=\"group1\" value=\"" + lookupList[i].Code + "\" id=\"b\"/>  </td>");
            txt.Append("<td align=\"left\">" + lookupList[i].Code + "</td>");
            txt.Append("<td align=\"left\">" + lookupList[i].Description + "</td>");
            txt.Append("</tr>");
        }
        txt.Append("</table>");

        txt.Append("<script type='text/javascript'>");

        txt.Append("$('#jqi_state0_buttonOk').click(function () {");
        txt.Append("var asset_type_name = '';");
        txt.Append("var selected_email = '';");
        txt.Append("$('#repgroupTable tr').each(function () {");
        txt.Append("    if ($(this).children(':eq(0)').find('#a').is(':checked')) {");
        txt.Append("        var emailaddress = $(this).children(':eq(0)').find('#a').val();");
        txt.Append("        selected_email = selected_email + emailaddress;");
        txt.Append("        asset_type_name = $(this).children(':eq(0)').find('#b').val();");
        txt.Append("    }");
        txt.Append("});");
        txt.Append("$('#MainContent_txtRepGroup').val(selected_email);");
        txt.Append("$('#MainContent_RepGroupID').val(asset_type_name);");
        //
        txt.Append("})");

        txt.Append("</script>");

        return txt.ToString();

    }

    private string Getoriginator()
    {//OriginatorLookup
        StringBuilder txt = new StringBuilder();
        StringBuilder emailaddress = new StringBuilder();
        //string originator = UserSession.Instance.ChildOriginators.ToString().Remove(UserSession.Instance.ChildOriginators.ToString().Length - 1, 1);
        //List<OriginatorEntity> lstactivityemail = OriginatorBR.Instance.GetOriginators("(originator = '" + UserSession.Instance.UserName + "' OR originator IN(" + originator + "))");

        CommonClient commonClient = new CommonClient();
        List<LookupDTO> lookupList = commonClient.GetLookups("OriginatorLookup", UserSession.Instance.ChildOriginatorsCookers, new List<object>(), null, false);

        txt.Append("<table border=\"0\" id=\"repgroupTable\" width=\"300px\" class=\"detail-tbl\" cellpadding=\"0\" cellspacing=\"0\">");
        txt.Append("<tr class=\"detail-popsheader\">");
        txt.Append("<th></th>");
        txt.Append("<th>Originator</th>");
        txt.Append("<th>Name</th>");
        txt.Append("</tr>");
        for (int i = 0; i < lookupList.Count; i++)
        {
            txt.Append("<tr>");
            txt.Append("<td style=\"padding-left:5px;\" class=\"tblborder\"> <input type=\"radio\" name=\"group1\" value=\"" + lookupList[i].Code + "\" id=\"a\"/>  </td>");
            txt.Append("<td align=\"left\">" + lookupList[i].Code + "</td>");
            txt.Append("<td align=\"left\">" + lookupList[i].Description + "</td>");
            txt.Append("</tr>");
        }
        txt.Append("</table>");

        txt.Append("<script type='text/javascript'>");

        txt.Append("$('#jqi_state0_buttonOk').click(function () {");
        txt.Append("var selected_email = '';");
        txt.Append("$('#repgroupTable tr').each(function () {");
        txt.Append("    if ($(this).children(':eq(0)').find('#a').is(':checked')) {");
        txt.Append("        var emailaddress = $(this).children(':eq(0)').find('#a').val();");
        txt.Append("        selected_email = selected_email + emailaddress;");
        txt.Append("    }");
        txt.Append("});");
        txt.Append("$('#MainContent_txtAssignedTo').val(selected_email);");
        txt.Append("})");

        txt.Append("</script>");


        ////txt.Append("<table border=\"1\" id=\"originatorTable\">");

        ////for (int i = 0; i < lookupList.Count; i++)
        ////{
        ////    txt.Append("<tr>");
        ////    txt.Append("<td> <input type=\"radio\" name=\"group1\" value=" + lookupList[i].Code + " id=\"a\"/>  </td>");
        ////    txt.Append("<td align=\"center\">" + lookupList[i].Code + "</td>");
        ////    txt.Append("<td align=\"center\">" + lookupList[i].Description + "</td>");
        ////    txt.Append("</tr>");
        ////}
        ////txt.Append("</table>");

        ////txt.Append("<script type='text/javascript'>");
        ////txt.Append("selectedoriginator();");
        ////txt.Append("</script>");

        return txt.ToString();
    }

    

    private string InsertPipeLineStage(string pipeline)
    {
        StringBuilder txt = new StringBuilder();
        bool success = false;
        PipelineStageEntity pipelineStageEntity = PipelineStageBR.Instance.CreateObject(System.Guid.NewGuid());
        pipelineStageEntity.PipelineStage = pipeline;
        pipelineStageEntity.StageOrder = "10";
        pipelineStageEntity.DefaultDeptId = UserSession.Instance.DefaultDepartmentId;
        success = PipelineStageBR.Instance.InsertPipelineStageDetails(pipelineStageEntity);

        if (success == true)
        {
            txt.Append("pipe");
        }
        else
        {
            txt.Append("not");
        }
        return txt.ToString();
    }

    private string GetInsertPipeLineQuery()
    {
        StringBuilder query = new StringBuilder();
        query.Append("Insert Pipe Line Stage");
        query.Append("<div class=\"field\">");
        query.Append("<label for=\"insertPipeLine\">Pipe Line Stage <span class=\"required\">*</span> : </label><input type=\"text\" id=\"insertPipeLine\" name=\"editLeadStage\" />");
        query.Append("</div>");

        query.Append("<script type='text/javascript'>");
        query.Append("$('#jqi_state0_buttonSave').click(function() {");
        query.Append("var pipe=$('#insertPipeLine').val();");
        query.Append("InsertData('/Peercore.CRM.WebCRM/pipelines_stage/process_forms/processmaster.aspx?fm=pipelineStage&type=insert&pps='+pipe);");
        query.Append("});");
        query.Append("</script>");

        return query.ToString();
    }

    private string GetPipeLineStage(string PipeLineStageId)
    {
        PipelineStageEntity leadStageEntity = PipelineStageBR.Instance.GetPipelineStage(PipeLineStageId);
        StringBuilder txt = new StringBuilder();
        txt.Append("What would you like to change this to? ");
        txt.Append("<div class=\"field\"><label for=\"editPipeLineStage\">Pipe Line Stage: <span class=\"required\">*</span> :</label><input type=\"text\" id=\"editPipeLineStage\" name=\"editPipelineStage\" value=\"" + leadStageEntity.PipelineStage + "\" /></div>");

        //jqi_state0_buttonSave
        txt.Append("<script type='text/javascript'>");
        txt.Append("$('#jqi_state0_buttonSave').click(function() {");
        txt.Append("var pipe=$('#editPipeLineStage').val();");
        txt.Append("saveData('/Peercore.CRM.WebCRM/pipelines_stage/process_forms/processmaster.aspx?fm=pipelineStage&id=" + PipeLineStageId + "&type=Update&pps='+pipe);");
        txt.Append("});");
        txt.Append("</script>");
        return txt.ToString();
    }

    private string UpdatePipeLineStage(string pipeLineStage, string pipeLineStageId)
    {
        bool sucess = false;
        StringBuilder messagesucess = new StringBuilder();
        PipelineStageEntity pipelineStageEntity = PipelineStageBR.Instance.CreateObject(Guid.Parse(pipeLineStageId));
        pipelineStageEntity.PipelineStage = pipeLineStage;
        sucess = PipelineStageBR.Instance.UpdatePipelineStageDetails(pipelineStageEntity);
        if (sucess)
        {
            messagesucess.Append("pipeLineStage");
        }
        return messagesucess.ToString();
    }

    private string DeletePipeLine(string pipeLineStageId)
    {
        bool success = false;
        StringBuilder txt = new StringBuilder();
        success = PipelineStageBR.Instance.DeletePipeLineStage(pipeLineStageId);
        if (success)
        {
            txt.Append("pipeLineStage");
        }
        return txt.ToString();
    }
    */
}