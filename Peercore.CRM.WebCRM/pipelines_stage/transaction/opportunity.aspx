﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="opportunity.aspx.cs" Inherits="pipelines_stage_transaction_opportunity" %>
<%@ MasterType TypeName="SiteMaster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
<asp:HiddenField ID="hfPageIndex" runat="server" />
<asp:HiddenField ID="HiddenFieldPipelineStageId" runat="server" />
<div class="leadentry_title_bar" style="display:none">
        <div id="div_header" runat="server"></div>
    </div>
    <div class="pipelineContent">
        <div id="container" class="container" runat="server"></div>
        <div id="ClientOpportunityGrid" style="Float:left; min-width: 40px; min-height: 40px; height: 400px; width: 63%; margin-top:10px;"></div>        
    </div>
    
    <div style="width:100%;">
        <div id="OpportunityGrid"></div>
    </div>

    

    <script type="text/javascript">
        $(function () {
            loadOpportunityGrid($("#MainContent_HiddenFieldPipelineStageId").val(), "");
            loadClientOpportunityGrid($("#MainContent_HiddenFieldPipelineStageId").val(), "");
        });
    </script>
</asp:Content>

