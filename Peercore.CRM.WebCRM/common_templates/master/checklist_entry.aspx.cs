﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using CRMServiceReference;
using System.Web.Services;
using System.Web.Script.Services;
using System.Web.Script.Serialization;
using System.IO;

public partial class common_templates_master_checklist_entry : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
        buttonbar.VisibleSave(true);
        buttonbar.EnableSave(true);

        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Checklist ", "#", "");

            if (!IsPostBack)
            {
                LoadOptions();
            }
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }


    }

    private void LoadOptions()
    {
        LoadChecklistTypesToDropdown();
    }

    protected void ButtonSave_Click(object sender)
    {
        SaveChecklistDetails();
    }

    private void LoadChecklistTypesToDropdown()
    {
        CommonClient commonClient = new CommonClient();
        List<ChecklistTypeDTO> checklistTypeList = new List<ChecklistTypeDTO>();
        checklistTypeList = commonClient.GetChecklistTypeList();

        if (checklistTypeList.Count != 0)
        {
            DropDownListChecklistTypes.Items.Clear();
            // DropDownListChecklistTypes.Items.Add(new ListItem("- Select -", "0"));
            foreach (ChecklistTypeDTO item in checklistTypeList)
            {
                DropDownListChecklistTypes.Items.Add(new ListItem(item.Name, item.Id.ToString()));
            }

            DropDownListChecklistTypes.Enabled = true;
        }
    }

    public void SaveChecklistDetails() {
        string jsonString = string.Empty;
        ChecklistMasterDTO checkListMasterDTO = new ChecklistMasterDTO();

        List<ChecklistImageDTO> checklistImageList = new List<ChecklistImageDTO>();
        List<ChecklistCustomerDTO> checklistCustomerList = new List<ChecklistCustomerDTO>();
        List<ChecklistRepDTO> checklistRepList = new List<ChecklistRepDTO>();
        List<ChecklistRouteDTO> checklistRouteList = new List<ChecklistRouteDTO>();

        ChecklistCustomerDTO checklistCustomerObj = null;
        ChecklistImageDTO checklistImageObj = null;
        ChecklistRepDTO checklistRepObj = null;
        ChecklistRouteDTO checklistRouteObj = null;

        CommonClient commonClient = new CommonClient();
        CommonUtility commonUtility = new CommonUtility();

        try
        {
            if (txtName.Text.Trim() == "")
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Please Enter Product Name.");
                HiddenFieldSaveFailed.Value = "1";
                return;
            }

            string selectedImageName = !String.IsNullOrEmpty(HiddenFieldSelectedImageName.Value) ? HiddenFieldSelectedImageName.Value : null;

            try
            {
                if (int.Parse(hfCheckListId.Value) > 0)
                    checkListMasterDTO.ChecklistId = int.Parse(hfCheckListId.Value);
            }
            catch { checkListMasterDTO.ChecklistId = 0; }

            checkListMasterDTO.Task= txtName.Text;
            checkListMasterDTO.ChecklistTypeId = Convert.ToInt32(DropDownListChecklistTypes.SelectedValue);

            byte[] imageContent = null;
            if (selectedImageName != null)
            {
                string imagePath = ConfigUtil.ApplicationPath + "docs/checklist_images/" + selectedImageName;
                imageContent = System.IO.File.ReadAllBytes(Server.MapPath(imagePath));

                if (imageContent != null)
                {
                    //Rename Uploaded Image with the Product_Code.jpg
                    try
                    {
                        string oldPath = ConfigUtil.ApplicationPath + "docs/checklist_images/" + selectedImageName;
                        string newPath = oldPath;
                        //    string newPath = ConfigUtil.ApplicationPath + "docs/checklist_images/" + task.Substring(0,5) + ".jpg";
                        ////if new file exists, DElete it
                        //if (System.IO.File.Exists(Server.MapPath(newPath)))
                        //    System.IO.File.Delete(Server.MapPath(newPath));
                        // System.IO.File.Move(Server.MapPath(oldPath), Server.MapPath(newPath));
                    }
                    catch (Exception ex)
                    {
                        div_message.Attributes.Add("style", "display:block");
                        div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
                        HiddenFieldSaveFailed.Value = "1";
                        return;
                    }
                }

                checklistImageObj = new ChecklistImageDTO();
                checklistImageObj.ImageName = selectedImageName;
                checklistImageObj.ImageContent = imageContent;
                checklistImageList.Add(checklistImageObj);
                checkListMasterDTO.ImageList = checklistImageList;
            }

            bool status = false;
            status = commonClient.SaveChecklistMaster(checkListMasterDTO);

            if (status == true)
            {
                div_message.Attributes.Add("style", "display:block");
                div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved.");
                //HiddenFieldSelectedProductId.Value = "";
                HiddenFieldSelectedImageName.Value = "";
                //HiddenFieldSelectedLinkOrFileName.Value = "";
                HiddenFieldSaveFailed.Value = "0";
            }
            else
            {
                div_message.Attributes.Add("style", "display:block");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Task is already exsist!");
                HiddenFieldSaveFailed.Value = "1";
            }
        }
        catch (Exception ex)
        {
            div_message.Attributes.Add("style", "display:block");
            div_message.InnerHtml = commonUtility.GetErrorMessage(CommonUtility.ERROR_MESSAGE);
            HiddenFieldSaveFailed.Value = "1";
            throw;
        }
    }
}
