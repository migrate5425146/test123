﻿<%@ Page Title="mSales - Product Entry" Language="C#" MasterPageFile="~/Site.master"
    AutoEventWireup="true" CodeFile="product_entry.aspx.cs" Inherits="common_templates_master_product_entry" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedNavigationType" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedImageName" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedImageFullPath" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedLinkOrFileName" runat="server" />
    <asp:HiddenField ID="HiddenFieldSelectedProductId" runat="server" />
    <asp:HiddenField ID="HiddenFieldSaveFailed" runat="server" />
    <asp:HiddenField ID="hfSessionId" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server" />
    <asp:HiddenField ID="savedeleteOpt" runat="server" />
    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <div id="div_main" class="divcontectmainforms">
        <div id="window" style="display: none; width: 500px;">
            <div style="width: 100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="textalignbottom">
                                    Brand
                                </td>
                                <td colspan="3">
                                    <%--<asp:DropDownList ID="cmbBrand" runat="server" class="select-different"  Width="262px">
                                    </asp:DropDownList>--%>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">
                                    Category
                                </td>
                                <td colspan="3">
                                    <%--<asp:DropDownList ID="cmbCategory" runat="server" class="select-different"  Width="262px">
                                    </asp:DropDownList>--%>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">
                                    Code
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand"></span>
                                    <%--&nbsp;<input type="text" class="textboxwidth" id="txtCode"/>
                                    <span style="color: Red">*</span></span>--%>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">
                                    Name
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtrName" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">
                                    SKU
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth input_numeric_sku"
                                        id="txtSku" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonSave" type="button">
                                        Save</button>
                                    <button class="k-button" id="buttonClear">
                                        Clear</button>
                                    <asp:HiddenField ID="hdnSelectedProductId" runat="server" ClientIDMode="Static" />
                                    <asp:HiddenField ID="HiddenFieldBrandId" runat="server" ClientIDMode="Static" />
                                    <asp:HiddenField ID="HiddenFieldCategoryId" runat="server" ClientIDMode="Static" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <%--<div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div class="addlinkdiv" style="float: left" id="div_address">
                        <a id="id_add_product" clientidmode="Static" runat="server" href="#">Add Product</a>
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
            <div class="back"></div></asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
         <div class="clearall">
        </div>--%>
        <div class="toolbar_container" id="id_div_toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div style="display: none" id="id_div_buttonBar">
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                    <%--<div class="addlinkdiv" style="float: left" id="div_AddProduct">
                        <a id="id_add_product" clientidmode="Static" runat="server" href="#">Add Product</a>
                    </div>--%>
                    <%--<div class="addlinkdiv" style="float: left; margin-left: 5px;" id="div_DeleteProduct">
                        <a id="id_delete_products" clientidmode="Static" runat="server" href="#"><span class="cross icon">Delete Products</span></a>
                    </div>--%>
                    <a id="id_add_product" class="sprite_button" style="float: left; margin-left: 5px;
                        margin-top: 0px;"><span class="plus icon"></span>Add Product</a> <a id="id_delete_products"
                            class="sprite_button" style="float: left; margin-left: 5px; margin-top: 0px;"><span
                                class="cross icon"></span>Delete</a>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right" id="div_backButton">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
                            <div class="back"></div>
                        </asp:HyperLink>
                    </div>
                    <div style="float: right;" align="right" id="div_btn_show_products_grid">
                        <a class="campaigns" id="btn_show_products_grid" clientidmode="Static" runat="server"
                            href="#">Products</a>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div class="clearall">
        </div>
        <div id="div_products" runat="server" style="display: block">
            <div style="min-width: 200px; overflow: auto">
                <div style="float: left; width: 98.5%; overflow: auto; margin: 0px 10px;">
                    <div id="divProdutGrid">
                    </div>
                </div>
            </div>
        </div>
        <div id="div_newEntry" runat="server" style="display: block">
            <div class="formleft" style="margin-bottom: 20px;">
                <fieldset>
                    <legend>Product Details</legend>
                    <div class="formtextdiv">
                        Brand
                    </div>
                    <div class="formdetaildiv_right">
                        <asp:DropDownList ID="cmbBrand" runat="server" CssClass="input-large1">
                        </asp:DropDownList>
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                        Category
                    </div>
                    <div class="formdetaildiv_right">
                        <asp:DropDownList ID="cmbCategory" runat="server" CssClass="input-large1">
                        </asp:DropDownList>
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                        Packing
                    </div>
                    <div class="formdetaildiv_right">
                        <asp:DropDownList ID="cmbPacking" runat="server" CssClass="input-large1">
                        </asp:DropDownList>
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                        Case Configuration
                    </div>
                    <div class="formdetaildiv_right">
                        <asp:TextBox ID="txtCaseConfiguration" CssClass="tb2 input-xlarge" runat="server" MaxLength="4"></asp:TextBox>

                    </div>
                    <div class="clearall">
                    </div>

        
                    <div class="formtextdiv">
                        Tonnage Configuration
                    </div>
                    <div class="formdetaildiv_right">
                        <asp:TextBox ID="txtTonnageConfiguration" CssClass="tb2 input-xlarge" runat="server" MaxLength="4"></asp:TextBox>
                    </div>
                    <div class="clearall">
                    </div>



                    <div class="formtextdiv">
                        Flavour
                    </div>
                    <div class="formdetaildiv_right">
                        <asp:DropDownList ID="cmbFlavor" runat="server" CssClass="input-large1">
                        </asp:DropDownList>
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                        FG Code
                    </div>
                    <div class="formdetaildiv_right">
                        <asp:TextBox ID="txtFgCode" CssClass="tb2 input-xlarge" runat="server"></asp:TextBox>
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                        Code
                    </div>
                    <div class="formdetaildiv_right">
                        <asp:TextBox ID="txtCode" CssClass="tb2 input-xlarge" runat="server" onfocusout="onCheckProductCodeExsist()"></asp:TextBox>
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                        Name
                    </div>
                    <div class="formdetaildiv_right">
                        <asp:TextBox ID="txtName" CssClass="tb2 input-xlarge" runat="server"></asp:TextBox>
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                        Weight
                    </div>
                    <div class="formdetaildiv_right">
                        <asp:TextBox ID="txtWeight" CssClass="tb2 input-xlarge input_numeric_weight" runat="server"></asp:TextBox>
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                        Is High Value
                    </div>
                    <div class="formdetaildiv">
                        <asp:RadioButton ID="radioyes" Text=" Yes" runat="server" GroupName="isHighValue" />&nbsp;&nbsp;
                        <asp:RadioButton ID="radiono" runat="server" Text=" No" GroupName="isHighValue" />
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                        Is Promo Item
                    </div>
                    <div class="formdetaildiv">
                        <asp:RadioButton ID="radioPromoyes" Text=" Yes" runat="server" GroupName="isPromo" />&nbsp;&nbsp;
                        <asp:RadioButton ID="radioPromono" runat="server" Text=" No" GroupName="isPromo" />
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                        Is MT Item
                    </div>
                    <div class="formdetaildiv">
                        <asp:RadioButton ID="radioMTyes" Text=" Yes" runat="server" GroupName="isMT" />&nbsp;&nbsp;
                        <asp:RadioButton ID="radioMTno" runat="server" Text=" No" GroupName="isMT" />
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                        Is 0.5 Qty Product
                    </div>
                    <div class="formdetaildiv">
                        <asp:RadioButton ID="radioHalfQtyItemYes" Text=" Yes" runat="server" GroupName="isHalfQty" />&nbsp;&nbsp;
                        <asp:RadioButton ID="radioHalfQtyItemNo" runat="server" Text=" No" GroupName="isHalfQty" Checked="true" />
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                        Status
                    </div>
                    <div class="formdetaildiv_right">
                        <asp:DropDownList ID="ddlStatus" runat="server" CssClass="input-large1">
                            <asp:ListItem Value="0" Text="Active"></asp:ListItem>
                            <asp:ListItem Value="1" Text="Inactive"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="clearall">
                    </div>
                </fieldset>
                <div class="clearall">
                </div>
            </div>
            <div class="formright">
                <fieldset>
                    <legend>Product Image</legend>
                    <div class="formtextdiv">
                        Image
                    </div>
                    <div class="formdetaildiv">
                        <div id="div_preview" class="upload_image" runat="server">
                        </div>
                        <div class="clearall">
                        </div>
                    </div>
                    <div class="clearall">
                    </div>
                    <div class="formtextdiv">
                    </div>
                    <div class="formdetaildiv" style="width: 71%">
                        <input class="uploadProductImage" name="productImage" id="productImage" type="file"
                            upload-id="02ebeebf-98aa-459b-b41f-49028fa37e9c" />
                        <%--<asp:Label ID="Label26" runat="server" Text="Supported Formats : JPG, GIF, TIFF, PNG, BMP"></asp:Label>--%>
                        <asp:Label ID="Label26" runat="server" Text="Supported Formats : JPG, JPEG"></asp:Label>
                        <br />
                        <asp:Label ID="lblImageName" runat="server" Text=""></asp:Label>
                    </div>
                    <div class="clearall">
                    </div>
                </fieldset>
            </div>
        </div>
        <div class="clearall">
        </div>
    </div>
    <div id="popup">
        <div>
            <b>Please enter admin password:</b></div>
        <input style="width: 100%;" id="adminPass" type="password" />
        <button type="button" onclick="confirmAdmin()">
            Confirm</button>
        <button type="button" onclick="exitAdmin()">
            Cancel</button>
    </div>
    <script type="text/javascript">
        hideStatusDiv("MainContent_div_message");
        $(document).ready(function () {
            jQuery(function ($) {
                $('input.input_numeric_sku').autoNumeric({ aSep: null, aDec: null, mDec: 0 });
            });

            jQuery(function ($) {
                $('input.input_numeric_weight').autoNumeric({ aSep: null, aDec: '.', mDec: 2 });
            });

            ProductEntryInitializeComponents();
        });

        $("#id_add_product").click(function () {
            $('#MainContent_lblImageName').text("");
            $("#id_delete_products").hide();
            $("#id_add_product").hide();

            BtnAddProductHandler();
        });
        
        $("#id_delete_products").click(function () {
            $("#<%= savedeleteOpt.ClientID %>").val("0");
            var callCycleContactDTOList = GetSelectedProductsList();
            if (callCycleContactDTOList != "") {

                var r = confirm("Do you want to delete this Product/s?");

                if (r == true) {
                    var OriginatorString = $("#<%= OriginatorString.ClientID %>").val();

                    if (OriginatorString != "ADMIN") {
                        showPopup();
                    }
                    else {
                        deleteProduct();
                    }
                }
            }
            else {
                confirm("Please Select a product to delete ");
            }
        });

        $("#btn_show_products_grid").click(function () {
            $("#id_delete_products").show();
            $("#id_add_product").show();
            BtnShowProductsGridHandler();
        });

        function BtnShowProductsGridHandler() {
            $("#MainContent_div_newEntry").css("display", "none");
            $("#MainContent_div_products").css("display", "block");

            $("#div_AddProduct").css("display", "block");
            $("#div_backButton").css("display", "block");
            $("#div_btn_show_products_grid").css("display", "none");

            $("#id_div_buttonBar").css("display", "none");

            $("#MainContent_HiddenFieldSelectedProductId").val("");

            //$("#MainContent_div_preview").css("display", "none");
            $("#MainContent_div_preview").html('');
        }

        function BtnAddProductHandler() {
            $("#MainContent_div_newEntry").css("display", "block");
            $("#MainContent_div_products").css("display", "none");

            $("#div_AddProduct").css("display", "none");
            $("#div_backButton").css("display", "none");
            $("#div_btn_show_products_grid").css("display", "block");

            $("#id_div_buttonBar").css("display", "block");

            $("#MainContent_HiddenFieldSelectedProductId").val("");

            $("#MainContent_txtName").val("");
            $("#MainContent_txtFgCode").val("");
            $("#MainContent_txtCode").val("");
            $("#MainContent_txtWeight").val("");
            $("#MainContent_cmbBrand").val("0");
            $("#MainContent_cmbCategory").val("0");
            $("#MainContent_cmbFlavor").val("0");
            $("#MainContent_cmbPacking").val("0");

            $("#MainContent_radiono").prop('checked', true);
            $("#MainContent_radioyes").prop('checked', false);

            $("#MainContent_radioPromoyes").prop('checked', true);
            $("#MainContent_radioPromono").prop('checked', false);

            $("#MainContent_radioMTno").prop('checked', true);
            $("#MainContent_radioMTyes").prop('checked', false);

            $("#MainContent_radioHalfQtyItemNo").prop('checked', true);
            $("#MainContent_radioHalfQtyItemYes").prop('checked', false);

            $('#MainContent_HiddenFieldSelectedImageName').val();
            $('#MainContent_HiddenFieldSelectedImageFullPath').val();

            $("#MainContent_div_preview").css("display", "none");
            $("#MainContent_div_preview").html('');
        }

        function ProductEntryInitializeComponents() {
            if ($("#MainContent_HiddenFieldSaveFailed").val() == "1") {
                $("#MainContent_div_newEntry").css("display", "block");
                $("#MainContent_div_products").css("display", "none");

                $("#div_AddProduct").css("display", "none");
                $("#div_backButton").css("display", "none");
                $("#div_btn_show_products_grid").css("display", "block");

                $("#id_div_buttonBar").css("display", "block");
                $("#id_delete_products").css("display", "none");
                $("#id_add_product").css("display", "none");
            }
            else {
                $("#MainContent_div_newEntry").css("display", "none");
                $("#MainContent_div_products").css("display", "block");

                $("#MainContent_HiddenFieldSelectedProductId").val("");

                $("#div_AddCampaign").css("display", "block");
                $("#div_backButton").css("display", "block");
                $("#div_btn_show_products_grid").css("display", "none");

                $("#id_div_buttonBar").css("display", "none");
            }

            InitializeProductImageUpload();                     

            loadProductsGrid();
        }

        function InitializeProductImageUpload() {
            $("#productImage").kendoUpload({
                async: {
                    saveUrl: ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=productimage&type=insert",
                    autoUpload: true
                },
                upload: function onUpload(e) {
                    e.sender.options.async.saveUrl = ROOT_PATH + "common_templates/process_forms/processmaster.aspx?fm=productimage&type=insert";
                    var files = e.files;
                    // Check the extension of each file and abort the upload if it is not .jpg
                    $.each(files, function () {
                        //if (this.extension.toLowerCase() != ".jpg" && this.extension.toLowerCase() != ".gif" && this.extension.toLowerCase() != ".tif" && this.extension.toLowerCase() != ".tiff" && this.extension.toLowerCase() != ".png" && this.extension.toLowerCase() != ".bmp") {
                        if (this.extension.toLowerCase() != ".jpg" && this.extension.toLowerCase() != ".jpeg" && this.extension.toLowerCase() != ".png") {
                            e.preventDefault();
                            alert("Please Select an .JPG or .JPEG image!");
                        }
                    });
                },
                success: function onSuccess(e) {
                    //var ROOT_PATH = '/Peercore.CRM.WebCRM/'
                    //alert(filename);
                    var sessionId = $('#MainContent_hfSessionId').val();
                    //var filename = "CampaignImage_" + sessionId + "_" + e.files[0].name;
                    var filename = e.files[0].name;
                    //var fullFilename = e.files[0].rawFile.mozFullPath;
                    $('#MainContent_HiddenFieldSelectedImageName').val(filename);
                    $('#MainContent_HiddenFieldSelectedImageFullPath').val(ROOT_PATH + 'docs/product_images/' + filename);

                    $("#MainContent_lblImageName").html(filename);
                    $("#MainContent_div_preview").css("display", "block");
                    $("#MainContent_div_preview").html('<img src="' + ROOT_PATH + 'docs/product_images/' + filename + '">');
                    //$("#MainContent_divImagePreview").html('<img src="' + ROOT_PATH + 'docs/contactperson/' + filename + '">');
                }
            });
        }

        /*Load Product Grid in Product Entry Page*/
        function loadProductsGrid() {

            var take_grid = $("#MainContent_hfPageIndex").val();
            var url = "javascript:ShowEditProduct('#=ProductId#','#=Code#','#=FgCode#','#=Name#','#=BrandName#','#=CategoryName#','#=Sku#','#=BrandId#','#=CategoryId#','#=IsHighValue#','#=FlavourId#','#=PackingId#','#=Weight#','#=ImageName#','#=IsPromoItem#','#=Status#','#=IsMTItem#','#=IsHalfQtyItem#');";

            var urlName = "<a href=\"" + url + "\">#=Name#</a>";
            var urlCode = "<a href=\"" + url + "\">#=Code#</a>";
            var urlFgCode = "<a href=\"" + url + "\">#=FgCode#</a>";

            $("#div_loader").show();
            $("#divProdutGrid").html("");
            $("#divProdutGrid").kendoGrid({
                height: 550,
                columns: [
                    { text: "HasSelect", title: "", headerTemplate: '<input id="checkAllProducts" type="checkbox" class="myCheckbox" />', width: "40px", template: "<input id=\"check_datarow_products\" alt=\"#= uid# \" type=\"checkbox\" #= HasSelect ? checked='checked' : '' #  class=\"check_row\"/>" },
                    { field: "ProductId", title: "Product Id", width: "85px", hidden: true },
                    { field: "BrandId", title: "Brand Id", width: "85px", hidden: true },
                    { field: "CategoryId", title: "Category Id", width: "85px", hidden: true },
                    { template: urlCode, field: "Code", title: "Code", width: "75px" },
                    { template: urlFgCode, field: "FgCode", title: "FG Code", width: "75px" },
                    { template: urlName, field: "Name", title: "Product Name", width: "300px" },
                    { field: "BrandName", title: "Brand", width: "200px" },
                    { field: "CategoryName", title: "Category", width: "200px" },
                    { template: "#=Status#", field: "", title: "Status", width: "75px" }
                ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                filterable: true,
                selectable: "single",
                columnMenu: false,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 15,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            //id: "SourceId",
                            fields: {
                                ProductId: { validation: { required: true} },
                                Code: { editable: false, nullable: true },
                                Name: { editable: false, nullable: true },
                                BrandId: { editable: false, nullable: true },
                                BrandName: { editable: false, nullable: true },
                                CategoryId: { editable: false, nullable: true },
                                CategoryName: { editable: false, nullable: true },
                                Sku: { editable: false, nullable: true },
                                Status: { editable: false, nullable: true },
                                HasSelect: { type: "boolean"},
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/load_stock/load_stock.asmx/GetAllProductsDataAndCountForMaster", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { pgindex: take_grid },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {
                                    $("#div_loader").hide();
                                    var entityGrid = $("#divBrandGrid").data("kendoGrid");
                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));
                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }
                }
            });
        }

        $('#check_datarow_products').live('click', function (e) {
            var cb = $(this);
            var checked = cb.is(':checked');
            var selected_uid = cb[0].alt.replace(/\s+/g, '');

            setValueToGridDataProducts(checked, selected_uid);

            if ($('[id=checkAllProducts]').is(':checked')) {
                $('[id=checkAllProducts]')[0].checked = false;
            }
        });

        function setValueToGridDataProducts(val, rowuid) {
            var grid = $("#divProdutGrid").data("kendoGrid");
            var item_grid = grid.dataSource.getByUid(rowuid);
            if (item_grid != undefined && item_grid != null)
                item_grid.set("HasSelect", val);
        }

        $("#checkAllProducts").live('click', function (e) {
            var state = $(this).is(':checked');
            var grid = $('#divProdutGrid').data('kendoGrid');
            $.each(grid.dataSource.view(), function () {
                if (this['HasSelect'] != state)
                    this.dirty = true;
                this['HasSelect'] = state;
            });
            grid.refresh();
        });

        //To get Selected Customers in Route Entry Page
        function GetSelectedProductsList() {
            //debugger;
            var callCycleContactDTOList = [];
    
            var idstr = '';
            var items = $("#divProdutGrid").data("kendoGrid").dataSource.data();

            var callCycleContactDTO = new Object();
            var leadCustomerDTO = new Object();
            for (var i = 0; i < items.length; i++) {

                callCycleContactDTO = new Object();
                leadCustomerDTO = new Object();

                var item = items[i];
                if (i == (items.length - 1) && (item != undefined && item != null && item != '' && item.HasSelect == true)) {
                    idstr += item.ProductId;
                    callCycleContactDTOList.push(item.ProductId);
                }
                else if (item != undefined && item != null && item != '' && item.HasSelect == true) {
                    idstr += item.ProductId + ",";
                    callCycleContactDTOList.push(item.ProductId);
                }
            }
            //debugger; 
            return callCycleContactDTOList;
        }

        function ShowEditProduct(id, code, fgCode, name, category, brand, sku, brandId, categoryId, isHighValue, flavorId, packingId, weight, imageName, isPromoItem, status, isMTItem, isHalfQtyItem) {
            $("#id_delete_products").hide();
            $("#id_add_product").hide();

            $('#MainContent_HiddenFieldSelectedImageName').val();
            $('#MainContent_HiddenFieldSelectedImageFullPath').val();
            
            //debugger;
            $("#MainContent_div_newEntry").css("display", "block");
            $("#MainContent_div_products").css("display", "none");
            $("#div_AddProduct").css("display", "none");
            $("#div_backButton").css("display", "none");
            $("#div_btn_show_products_grid").css("display", "block");
            $("#id_div_buttonBar").css("display", "block");
            $("#id_delete_products").css("display", "none");
            $("#id_add_product").css("display", "none");

            $("#MainContent_HiddenFieldSelectedProductId").val(id);

            $("#MainContent_txtName").val(name);
            $("#MainContent_txtFgCode").val(fgCode);
            $("#MainContent_txtCode").val(code);
            $("#MainContent_txtWeight").val(weight);
            $("#MainContent_cmbBrand").val(brandId);
            $("#MainContent_cmbCategory").val(categoryId);
            $("#MainContent_cmbFlavor").val(flavorId);
            $("#MainContent_cmbPacking").val(packingId);

            if (status == 'A'){
                document.getElementById("MainContent_ddlStatus").selectedIndex = 0;
            }
            else{
                document.getElementById("MainContent_ddlStatus").selectedIndex = 1;
            }

            //Set Images to Preview
            $("#MainContent_lblImageName").html("*");

            LoadProductImage(id);

//            $("#MainContent_lblImageName").html(imageName);
//            //$('#MainContent_HiddenFieldSelectedImageName').val(imageName);
//            $("#MainContent_div_preview").css("display", "block");
            //            $("#MainContent_div_preview").html('<img src="' + ROOT_PATH + 'docs/product_images/' + imageName + '">');

            if (isHighValue == "true") {
                $("#MainContent_radiono").prop('checked', false);
                $("#MainContent_radioyes").prop('checked', true);
            }
            else {
                $("#MainContent_radiono").prop('checked', true);
                $("#MainContent_radioyes").prop('checked', false);
            }

            if (isPromoItem == "true") {
                $("#MainContent_radioPromono").prop('checked', false);
                $("#MainContent_radioPromoyes").prop('checked', true);
            }
            else {
                $("#MainContent_radioPromono").prop('checked', true);
                $("#MainContent_radioPromoyes").prop('checked', false);
            }

            if (isMTItem == "true") {
                $("#MainContent_radioMTno").prop('checked', false);
                $("#MainContent_radioMTyes").prop('checked', true);
            }
            else {
                $("#MainContent_radioMTno").prop('checked', true);
                $("#MainContent_radioMTyes").prop('checked', false);
            }

            if (isHalfQtyItem == "true") {
                $("#MainContent_radioHalfQtyItemNo").prop('checked', false);
                $("#MainContent_radioHalfQtyItemYes").prop('checked', true);
            }
            else {
                $("#MainContent_radioHalfQtyItemNo").prop('checked', true);
                $("#MainContent_radioHalfQtyItemYes").prop('checked', false);
            }
        }

        function LoadProductImage(productId) {
            $("#div_loader").show();
            var param = { "productId": productId };
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/SaveProductImageAndGetImageName",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.hasOwnProperty('d')) {
                        msg = response.d;
                    } else {
                        msg = response;
                    }
                    //debugger;
                    var json = JSON.parse(msg);

                    var imageName = json["ImageName"];
                    
                    $("#MainContent_lblImageName").html(imageName);
                    //$('#MainContent_HiddenFieldSelectedImageName').val(imageName);
                    $("#MainContent_div_preview").css("display", "block");
                    $("#MainContent_div_preview").html('<img src="' + ROOT_PATH + 'docs/product_images/' + imageName + '">');
                    $("#div_loader").hide();
                },
                error: function (response) {
                    //alert("Oops, something went horribly wrong");
                    $("#div_loader").hide();
                }
            });
        }

        function showPopup() {
             $("#adminPass").val("");
            document.getElementById("popup").style.display = "block";
        }

        function confirmAdmin() {
            var password = document.getElementById("adminPass").value;
            if (password == '') {
                var errorMsg = GetErrorMessageDiv("Please Enter admin Password", "MainContent_div_message");
                $('#MainContent_div_message').css('display', 'block');
                $("#MainContent_div_message").html(errorMsg);
                hideStatusDiv("MainContent_div_message");
                return;
            }

            var param = { "password": password };
            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/GetAdminPassword",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.hasOwnProperty('d')) {

                        msg = response.d;
                        if (msg == 'true') {
                            if ($("#<%= savedeleteOpt.ClientID %>").val() == '0')
                                deleteProduct();
                            if ($("#<%= savedeleteOpt.ClientID %>").val() == '1')
                                saveProduct();
                            document.getElementById("popup").style.display = "none";
                        }
                        else {
                            var errorMsg = GetErrorMessageDiv("Invalid admin Password", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(errorMsg);
                            hideStatusDiv("MainContent_div_message");
                            showPopup();
                        }
                    } else {
                        msg = response;

                        var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        hideStatusDiv("MainContent_div_message");
                    }

                },
                error: function (response) {
                    //alert("Oops, something went horribly wrong");                    
                }
            });
        }
        
        function saveProduct() {
            var param = { 'ProductDTOObj': '<%= Session["ProdDTO"] %>' };
            $.ajax({
                type: "POST",
                url: "product_entry.aspx/ASMSaveProduct",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.hasOwnProperty('d')) {
                        msg = response.d;
                        if (msg == 'true') {
                            var sucessMsg = GetSuccesfullMessageDiv("Successfully Saved.", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(sucessMsg);
                            hideStatusDiv("MainContent_div_message");
                            HiddenFieldSelectedProductId.Value = "";
                            HiddenFieldSelectedImageName.Value = "";
                            HiddenFieldSelectedLinkOrFileName.Value = "";
                            HiddenFieldSaveFailed.Value = "0";

                        }
                        else {
                            var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                            $('#MainContent_div_message').css('display', 'block');
                            $("#MainContent_div_message").html(errorMsg);
                            HiddenFieldSaveFailed.Value = "1";
                            hideStatusDiv("MainContent_div_message");

                        }
                    }
                },
                error: function (response) {
                }
            });

        }

        function exitAdmin() {                
            document.getElementById("popup").style.display = "none";
            return false;
        }

        function deleteProduct(){
            var callCycleContactDTOList = GetSelectedProductsList();

            var param = { 'DeleteProductListDTO': callCycleContactDTOList };

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/load_stock/load_stock.asmx/DeleteSelectedProductListformGrid",
                //data: JSON.stringify(param),
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.hasOwnProperty('d')) {
                        msg = response.d;
                    } else {
                        msg = response;
                    }
                    if (msg == "Deleted") {
                        var sucessMsg = GetSuccesfullMessageDiv("Successfully Deleted.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);

                        //hideStatusDiv("MainContent_div_outlets");
                        $("#MainContent_div_outlets").css("display", "none");
                        $("#MainContent_div_outlets_tme").css("display", "none");
                        $("#MainContent_div_routes_tme").css("display", "block");
                        $("#id_div_toolbar_container").css("display", "block");

                        $("div#div_loader").hide();
                        loadProductGrid();
                        hideStatusDiv("MainContent_div_message");
                    }
                    else if (msg == "DeletedDiscount") {
                        var sucessMsg = GetSuccesfullMessageDiv("Successfully Deleted. Some Products cannot be deleted!. Because product essist in discount scheme", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(sucessMsg);

                        //hideStatusDiv("MainContent_div_outlets");
                        $("#MainContent_div_outlets").css("display", "none");
                        $("#MainContent_div_outlets_tme").css("display", "none");
                        $("#MainContent_div_routes_tme").css("display", "block");
                        $("#id_div_toolbar_container").css("display", "block");

                        $("div#div_loader").hide();
                        loadProductGrid();
                        hideStatusDiv("MainContent_div_message");
                    }
                    else {
                        var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);

                        //hideStatusDiv("MainContent_div_outlets");
                        $("#MainContent_div_outlets").css("display", "none");
                        $("#MainContent_div_outlets_tme").css("display", "none");
                        $("#MainContent_div_routes_tme").css("display", "block");
                        $("#id_div_toolbar_container").css("display", "block");

                        $("div#div_loader").hide();
                        loadProductGrid();
                        hideStatusDiv("MainContent_div_message");
                    }
                },
                error: function (response) {
                    //alert("Oops, something went horribly wrong");
                    //debugger;
                }
            });
        }

        $("#MainContent_buttonbar_buttinSave").click(function () {
            $("#<%= savedeleteOpt.ClientID %>").val("1");
        });


        function onCheckProductCodeExsist() {
            var pCode = document.getElementById("MainContent_txtCode").value;

            var param = { "productCode": pCode };

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/product/product.asmx/GetProductByProductCode",
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.hasOwnProperty('d')) {
                        msg = response.d;
                        //console.log(msg);

                        var obj = JSON.parse(msg); 

                        if (obj.Code != null ) {
                            alert('Product code already exsist!');
                        }
                    } else {
                        msg = response;

                        var errorMsg = GetErrorMessageDiv("Error occurred in product validation.", "MainContent_div_message");
                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);
                        hideStatusDiv("MainContent_div_message");
                    }

                },
                error: function (response) {
                    //alert("Oops, something went horribly wrong");                    
                }
            });
        }  
    </script>
</asp:Content>
