﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;

public partial class common_templates_master_outlet_types : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
            }

            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Outlet Types ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        OriginatorString.Value = UserSession.Instance.OriginatorString;
    }
}