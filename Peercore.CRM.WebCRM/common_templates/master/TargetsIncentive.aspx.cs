﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using CRMServiceReference;

public partial class common_templates_master_TargetsIncentive : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
            }

            try
            {
                CommonClient client = new CommonClient();
                List<TargetEntity> targetList = new List<TargetEntity>();

                targetList = client.LoadTargetMaster();

                txtFirst14Days.Value = targetList[0].TargetInvVal.ToString();
                txtLast14Days.Value = targetList[1].TargetInvVal.ToString();
                txtTargetBreakDay.Value = targetList[0].TargetBreakDownDay.ToString();
            }
            catch { }

            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Targets Info ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

    }
}