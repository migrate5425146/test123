﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using System.Net;
using System.IO;
using System.Text;
using System.Configuration;
using CRMServiceReference;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using context = System.Web.HttpContext;

public partial class common_templates_master_MobileDetails : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
                string url = ConfigurationManager.AppSettings["MobileServiceUrl"].ToString() + "/newpin?accessToken=" + UserSession.Instance.AccessToken;

                SendLogToText("Start PageLoad");
                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

                //System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls12;

                SendLogToText("Pin loaded successfull.");
                try
                {
                    WebResponse response = request.GetResponse();
                    SendLogToText("Response return successfull.");
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                        SendLogToText("Pin read from response successfull.");
                        lblPin.Text = reader.ReadToEnd().Replace('"', ' ');
                    }

                    if (UserSession.Instance.OriginatorString == "ADMIN")
                    {
                        //id_routeassign.Visible = false;
                        trPin.Visible = true;
                    }
                }
                catch (WebException ex)
                {
                    SendErrorToText(ex);
                    //WebResponse errorResponse = ex.Response;
                    //using (Stream responseStream = errorResponse.GetResponseStream())
                    //{
                    //    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    //    String errorText = reader.ReadToEnd();
                    //    // log errorText
                    //}
                   // throw;
                }
            }

            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Mobile Security Details ", "#", "");
            SendLogToText("Page load successfull");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    protected void btnReset_Click(object sender, EventArgs e)
    {
        OriginatorClient orgClient = new OriginatorClient();
        orgClient.UpdateMobilePin();

        string url = ConfigurationManager.AppSettings["MobileServiceUrl"].ToString() + "/newpin?accessToken=" + UserSession.Instance.AccessToken;

        HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
        try
        {
            WebResponse response = request.GetResponse();
            using (Stream responseStream = response.GetResponseStream())
            {
                StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                lblPin.Text = reader.ReadToEnd().Replace('"', ' ');
            }
        }
        catch (WebException ex)
        {
            WebResponse errorResponse = ex.Response;
            using (Stream responseStream = errorResponse.GetResponseStream())
            {
                StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                String errorText = reader.ReadToEnd();
                // log errorText
            }
            throw;
        }
    }

    protected void btnNotifyIsSync_Click(object sender, EventArgs e)
    {
        CommonClient commonClient = new CommonClient();

        bool status = false;
        status = commonClient.PushNotifyGetSyncDetails();

        if (status)
        {
            Response.Write("<script>alert('Successfull');</script>");
            Response.Redirect("~/dashboard/transaction/RepsSyncDetails.aspx");
        }

    }

    protected void btnViewNotifyIsSync_Click(object sender, EventArgs e)
    {
        
    }

    private static String ErrorlineNo, Errormsg, extype, exurl, hostIp, ErrorLocation, HostAdd;

    public static void SendErrorToText(Exception ex)
    {
        var line = Environment.NewLine + Environment.NewLine;

        ErrorlineNo = ex.StackTrace.Substring(ex.StackTrace.Length - 7, 7);
        Errormsg = ex.GetType().Name.ToString();
        extype = ex.GetType().ToString();
        exurl = context.Current.Request.Url.ToString();
        ErrorLocation = ex.Message.ToString();

        try
        {
            string filepath = context.Current.Server.MapPath("~/LogFiles/");  //Text File Path

            if (!Directory.Exists(filepath))
            {
                Directory.CreateDirectory(filepath);

            }
            filepath = filepath + DateTime.Today.ToString("dd-MM-yy") + "-MobileService.txt";   //Text File Name
            if (!File.Exists(filepath))
            {
                File.Create(filepath).Dispose();
            }
            using (StreamWriter sw = File.AppendText(filepath))
            {
                string error = "Log Written Date:" + " " + DateTime.Now.ToString() + line + "Error Line No :" + " " + ErrorlineNo + line + "Error Message:" + " " + Errormsg + line + "Exception Type:" + " " + extype + line + "Error Location :" + " " + ErrorLocation + line + " Error Page Url:" + " " + exurl + line + "User Host IP:" + " " + hostIp + line;
                sw.WriteLine("-----------Exception Details on " + " " + DateTime.Now.ToString() + "-----------------");
                sw.WriteLine("-------------------------------------------------------------------------------------");
                sw.WriteLine(line);
                sw.WriteLine(error);
                sw.WriteLine("--------------------------------*End*------------------------------------------");
                sw.WriteLine(line);
                sw.Flush();
                sw.Close();

            }

        }
        catch (Exception e)
        {
            e.ToString();
        }
    }

    public static void SendLogToText(string ex)
    {
        try
        {
            string filepath = context.Current.Server.MapPath("~/LogFiles/");  //Text File Path

            if (!Directory.Exists(filepath))
            {
                Directory.CreateDirectory(filepath);

            }
            filepath = filepath + DateTime.Today.ToString("dd-MM-yy") + "-MobileService.txt";   //Text File Name
            if (!File.Exists(filepath))
            {
                File.Create(filepath).Dispose();
            }
            using (StreamWriter sw = File.AppendText(filepath))
            {
                string error = "Log Written Date:" + " " + DateTime.Now.ToString() + "Message: " + ex;
                sw.WriteLine(error);
                sw.Flush();
                sw.Close();

            }
        }
        catch (Exception e)
        {
            e.ToString();
        }
    }
}