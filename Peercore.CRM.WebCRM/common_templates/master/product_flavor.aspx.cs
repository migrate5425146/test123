﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using CRMServiceReference;

public partial class common_templates_master_product_flavor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
                //dtpDate.Text = DateTime.Today.ToString("dd-MMM-yyyy");
                //dtpDateTo.Text = DateTime.Today.ToString("dd-MMM-yyyy");

                //Load Products
                //LoadOptions();
            }
           
            //DropDownListProducts.SelectedIndex = 0;
            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Product Flavors ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        OriginatorString.Value = UserSession.Instance.OriginatorString;
    }
}