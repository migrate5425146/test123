﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true"
    CodeFile="rep_target.aspx.cs" Inherits="common_templates_transaction_rep_target" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="hfOriginator" runat="server" />
    <asp:HiddenField ID="hfOriginatorType" runat="server" />

    <script type="text/javascript" src="../../assets/scripts/jquery.validate.js"></script>
    <div id="div_main" class="divcontectmainforms">
        <div id="window" style="display: none; width: 500px;">
            <div style="width: 100%" id="contactentry">
                <div id="div_text">
                    <div id="div_message_popup" runat="server" style="display: none;">
                    </div>
                    <table border="0">
                        <tbody>
                            <tr>
                                <td class="textalignbottom">
                                    Territory Code
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" class="textboxwidth" id="txtRepCode"
                                        readonly="readonly" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">
                                    Name
                                </td>
                                <td colspan="3">
                                    <span class="wosub mand">&nbsp;<input type="text" style="width:281px" class="textboxwidth" id="txtName"
                                        readonly="readonly" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td class="textalignbottom">
                                    Target
                                </td>
                                <td>
                                    <span class="wosub mand">&nbsp;<input type="text" style="width:100px" class="textboxwidth input_numeric_target"
                                        id="txtTarget" />
                                        <span style="color: Red">*</span></span>
                                </td>
                                <td class="textalignbottom">
                                    &nbsp;Incentive
                                </td>
                                <td>
                                    <span class="wosub mand">&nbsp;&nbsp;&nbsp;<input type="text" style="width:100px" class="textboxwidth input_numeric_incentive"
                                        id="txtIncentive" />
                                        <span style="color: Red">*</span></span>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" class="textalignbottom">
                                    <button class="k-button" id="buttonSave" type="button">
                                        Save</button>
                                    <button class="k-button" id="buttonClear" style="display: none">
                                        Clear</button>
                                    <asp:HiddenField ID="hdnSelectedRepTargetId" runat="server" ClientIDMode="Static" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div class="addlinkdiv" style="float: left; display: none" id="div_address">
                        <a id="id_add_rep_target" clientidmode="Static" runat="server" href="#">Add Rep Target</a>
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
            <div class="back"></div></asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>
        <div id="div_message" runat="server" style="display: none;">
        </div>
        <div class="clearall">
        </div>
        <div class="formleft" style="display: none;">
            <div class="clearall">
            </div>
            <div class="formtextdiv">
                Division</div>
            <div class="formdetaildiv_right">
                :
                <asp:DropDownList ID="DropDownListDivision" runat="server" CssClass="input-large1" Width="150px">
                </asp:DropDownList>
            </div>
            <div class="clearall">
            </div>
        </div>
        <div class="clearall">
        </div>
        <div style="min-width: 200px; overflow: auto;padding:10px">
            <div style="float: left; width: 100%; overflow: auto;">
                <div id="divRepTargetsGrid">
                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        hideStatusDiv("MainContent_div_message");

        $(document).ready(function () {

            jQuery(function ($) {
                $('input.input_numeric_target').autoNumeric({ aSep: null, aDec: '.', mDec: 2 });
            });

            jQuery(function ($) {
                $('input.input_numeric_incentive').autoNumeric({ aSep: null, aDec: '.', mDec: 2 });
            });

            loadRepTargetsGrid();
            //$("#txtTarget").focus();

            $("#MainContent_DropDownListDivision").change(function () {
                loadRepTargetsGrid();
            });
        });

        $("#buttonSave").click(function () {
            RepTargetEntrySaveRepTarget();
        });

        function closepopupRepTarget() {
            var wnd = $("#window").kendoWindow({
                title: "Edit Territory Target",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            wnd.center().close();
            $("#window").css("display", "none");
        }

        /*Load Rep Target Grid*/
        function loadRepTargetsGrid() {
            var Originator = $("#<%= hfOriginator.ClientID %>").val();
            var OriginatorType = $("#<%= hfOriginatorType.ClientID %>").val();

            var take_grid = $("#MainContent_hfPageIndex").val();
            var divisionId = $("#MainContent_DropDownListDivision").val();
            var url = "javascript:updateRepTarget('#=RepTargetId#','#=RepCode#','#=RepName#','#=PrimaryTarAmt#','#=PrimaryIncAmt#');";

            var urlName = "<a href=\"" + url + "\">#=RepName#</a>";
            //            var urlCode = "<a href=\"" + url + "\">#=BrandCode#</a>";
            $("#div_loader").show();
            $("#divRepTargetsGrid").html("");
            $("#divRepTargetsGrid").kendoGrid({
                height: 410,
                columns: [
                    { field: "RepTargetId", title: "RepTargetId", width: "85px", hidden: true },
                    { field: "RepCode", title: "Territory Code", width: "100px" },
                    { template: urlName, field: "RepName", title: "Territory Name", width: "300px" },
                    { field: "PrimaryTarAmt", title: "Target Amount (Rs.)", width: "120px", hidden: false, format: "{0:n2}" },
                    { field: "PrimaryIncAmt", title: "Incentive Amount (Rs.)", width: "120px", hidden: false, format: "{0:n2}" },
                    { field: "Status", title: "Status", width: "105px", hidden: true }
                ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                filterable: true,
                selectable: "single",
                columnMenu: false,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 15,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",
                        model: { // define the model of the data source. Required for validation and property types.
                            //id: "SourceId",
                            fields: {
                                RepTargetId: { validation: { required: true} },
                                RepCode: { editable: false, nullable: true },
                                RepName: { editable: false, nullable: true },
                                PrimaryTarAmt: { type: "number", editable: false, nullable: true },
                                PrimaryIncAmt: { type: "number", editable: false, nullable: true },
                                Status: { validation: { required: true} }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/lead_customer/common.asmx/GetAllRepTargetsDataAndCount", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { Originator: Originator, OriginatorType: OriginatorType, pgindex: take_grid },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {
                                    $("#div_loader").hide();
                                    var entityGrid = $("#divRepTargetsGrid").data("kendoGrid");
                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));
                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }
                }
            });
        }

        /*Update Rep Target */
        function updateRepTarget(repTargetId, repCode, repName, tarAmt, incAmt) {
            $("#hdnSelectedRepTargetId").val(repTargetId);
            $("#txtRepCode").val(repCode);
            $("#txtName").val(repName);
            $("#txtTarget").val(tarAmt);
            $("#txtIncentive").val(incAmt);

            $("#window").css("display", "block");
            var wnd = $("#window").kendoWindow({
                title: "Edit Territory Target",
                modal: true,
                visible: false,
                resizable: false
            }).data("kendoWindow");
            $("#txtTarget").focus();

            wnd.center().open();
        }

        /*Save Rep Target */
        function RepTargetEntrySaveRepTarget() {
            var repCode = $("#txtRepCode").val();
            var repName = $("#txtName").val();
            var selectedRepTargetId = $("#hdnSelectedRepTargetId").val();
            var tar = $("#txtTarget").val();
            var inc = $("#txtIncentive").val();

            $("div#div_loader").show();

            var param = { 'repTargetId': selectedRepTargetId, 'repCode': repCode, 'target': tar, 'incentive': inc };

            $.ajax({
                type: "POST",
                url: ROOT_PATH + "service/lead_customer/common.asmx/SaveRepTarget",
                //data: JSON.stringify(param),
                data: JSON.stringify(param),
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (response) {
                    if (response.hasOwnProperty('d')) {
                        msg = response.d;
                    } else {
                        msg = response;
                    }
                    if (msg == "true") {
                        var sucessMsg = GetSuccesfullMessageDiv("Successfully Saved.", "MainContent_div_message");
                        closepopupRepTarget();
                        $('#MainContent_div_message').css('display', 'block');
                        $("div#div_loader").hide();
                        $("#MainContent_div_message").html(sucessMsg);

                        $("#MainContent_div_outlets").css("display", "none");
                        $("#MainContent_div_outlets_tme").css("display", "none");
                        $("#MainContent_div_routes_tme").css("display", "block");
                        $("#id_div_toolbar_container").css("display", "block");

                        loadRepTargetsGrid();
                        hideStatusDiv("MainContent_div_message");
                    }
                    else {
                        var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                        closepopupRepTarget();
                        $('#MainContent_div_message').css('display', 'block');
                        $("div#div_loader").hide();
                        $("#MainContent_div_message").html(errorMsg);

                        $("#MainContent_div_outlets").css("display", "none");
                        $("#MainContent_div_outlets_tme").css("display", "none");
                        $("#MainContent_div_routes_tme").css("display", "block");
                        $("#id_div_toolbar_container").css("display", "block");

                        loadRepTargetsGrid();
                        hideStatusDiv("MainContent_div_message");
                    }
                },
                error: function (response) {
                    var errorMsg = GetErrorMessageDiv("Error Occurred.", "MainContent_div_message");
                    closepopupRepTarget();

                    $("div#div_loader").hide();

                    $('#MainContent_div_message').css('display', 'block');
                    $("#MainContent_div_message").html(errorMsg);
                    hideStatusDiv("MainContent_div_message");

                }
            });
        }

    </script>
</asp:Content>
