﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using Peercore.CRM.Common;
using CRMServiceReference;

public partial class common_templates_transaction_rep_target : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
                //dtpDate.Text = DateTime.Today.ToString("dd-MMM-yyyy");
                //dtpDateTo.Text = DateTime.Today.ToString("dd-MMM-yyyy");
            }

            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("SR Targets", "#", "");
            LoadOptions();
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        hfOriginator.Value = UserSession.Instance.UserName;
        hfOriginatorType.Value = UserSession.Instance.OriginatorString;
    }

    public void LoadOptions()
    {
        ArgsDTO args = new ArgsDTO();

        args.StartIndex = ConfigUtil.StartIndex;
        args.RowCount = ConfigUtil.MaxRowCount;
        args.OrderBy = " division_name asc ";

        DiscountSchemeClient schemeClient = new DiscountSchemeClient();

        List<DivisionDTO> divisionList = (schemeClient.GetAllDivisions(args)).OrderBy(a=>a.DivisionId).ToList();
        divisionList.Insert(0, new DivisionDTO() { DivisionName = "ALL", DivisionId = 0 });

        DropDownListDivision.DataSource = divisionList;
        DropDownListDivision.DataTextField = "DivisionName";
        DropDownListDivision.DataValueField = "DivisionId";
        DropDownListDivision.DataBind();
    }
}