﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Common;
using Peercore.CRM.Entities;
using System.IO;
using System.Web.Services;
using System.Xml.Serialization;
using System.Configuration;
using CRMServiceReference;
using Peercore.CRM.Shared;

public partial class common_templates_transaction_product_divisions : PageBase
{
    CommonUtility commonUtility = new CommonUtility();

    protected void Page_Load(object sender, EventArgs e)
    {
        //Remove the session before setting the breadcrumb.
        Session.Remove(CommonUtility.BREADCRUMB);
        Master.SetBreadCrumb("Territory to Products ", "#", "");

        if (!IsPostBack)
        {
            //Set the Toolbar buttons
            buttonbar.VisibleSave(true);
            //buttonbar.VisibleDeactivate(true);
            buttonbar.onButtonSave = new usercontrols_buttonbar.ButtonSave(ButtonSave_Click);
            //buttonbar.onButtonClear = new usercontrols_buttonbar.ButtonClear(ButtonClear_Click);

            buttonbar.EnableSave(false);
            //buttonbar.EnableDeactivate(true);

            LoadOptions();
        }

        hfOriginator.Value = UserSession.Instance.UserName;
        hfOriginatorType.Value = UserSession.Instance.OriginatorString;
    }

    protected void ButtonSave_Click(object sender)
    {
        this.SaveProducts();
    }

    //protected void ButtonClear_Click(object sender)
    //{
    //    //// Clear();
    //    //txtSelectedDivisionName.Text = string.Empty;
    //    div_message.Attributes.Add("style", "display:none;padding:8px");
    //}

    private void SaveProducts()
    {
        try
        {
            if (Session[CommonUtility.DIVISION_PRODUCTS] == null)
            {
                div_message.Attributes.Add("style", "display:block;padding:2px");
                div_message.InnerHtml = commonUtility.GetErrorMessage("Please assign Products for selected division.");
                return;
            }

            int divisionId = 0;
            //if (!string.IsNullOrEmpty(HiddenFieldSelectedDivisionId.Value))
            //    divisionId = Int32.Parse(HiddenFieldSelectedDivisionId.Value);

            CommonClient commonClient = new CommonClient();
            List<ProductDTO> productList = (List<ProductDTO>)Session[CommonUtility.DIVISION_PRODUCTS];
            bool status = commonClient.SaveProductDivisions(divisionId,productList);

            if (status)
            {
                div_message.Attributes.Add("style", "display:block;padding:8px");
                div_message.InnerHtml = commonUtility.GetSucessfullMessage("Successfully Saved !");
                Session[CommonUtility.DIVISION_PRODUCTS] = null;
            }
        }
        catch (Exception ex)
        {
            div_message.Attributes.Add("style", "display:block;padding:2px");
            div_message.InnerHtml = commonUtility.GetErrorMessage("Error Occured.");
        }
    }

    private void LoadOptions()
    {
        #region Territory

        MasterClient masterlient = new MasterClient();
        ArgsModel args1 = new ArgsModel();
        args1.StartIndex = 1;
        args1.RowCount = 10000;
        args1.OrderBy = " territory_name asc";

        List<TerritoryModel> objTrrList = new List<TerritoryModel>();
        objTrrList = masterlient.GetAllTerritoryByOriginator(args1, UserSession.Instance.OriginatorString,
                                                                                UserSession.Instance.UserName);

        if (objTrrList.Count != 0)
        {
            ddlTerritory.Items.Clear();
            ddlTerritory.Items.Add(new ListItem("- Select Territory -", "0"));
            foreach (TerritoryModel item in objTrrList)
            {
                var strLength = (item.TerritoryCode == null) ? 0 : item.TerritoryCode.Length;
                string result = (item.TerritoryCode == null) ? "" : item.TerritoryCode.PadRight(strLength + 20).Substring(0, strLength + 20);

                ddlTerritory.Items.Add(new ListItem(item.TerritoryName + "  |  " + result, item.TerritoryId.ToString()));
            }

            ddlTerritory.Enabled = true;
        }

        #endregion
    }
}