﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using CRMServiceReference;

public partial class common_templates_transaction_product_prices : PageBase
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
                //dtpDate.Text = DateTime.Today.ToString("dd-MMM-yyyy");
                //dtpDateTo.Text = DateTime.Today.ToString("dd-MMM-yyyy");
            
                //Load Products
                //LoadOptions();
            }

            LoadOptions();
            DropDownListProducts.SelectedIndex = 0;
            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Product Prices ", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        OriginatorString.Value = UserSession.Instance.OriginatorString;
    }


    private void LoadOptions()
    {
        CommonClient commonClient = new CommonClient();

        ArgsDTO args2 = new ArgsDTO();
        args2.OrderBy = " name asc ";
        args2.AdditionalParams = "";
        args2.StartIndex = 1;
        args2.RowCount = 300;

        List<ProductDTO> productList = new List<ProductDTO>();
        productList = commonClient.GetAllProductsWithNoPrice(args2);

        //productList.Insert(0, new ProductDTO() { Name = "- Select -", ProductId = -1 });

        //DropDownListProducts.Items.Clear();
        //DropDownListProducts.Items.Add(new ListItem(" - Select -", "-1"));
        //DropDownListProducts.DataSource = productList;
        //DropDownListProducts.DataTextField = "Name";
        //DropDownListProducts.DataValueField = "ProductId";
        //DropDownListProducts.DataBind();
        if (productList.Count != 0)
        {
            DropDownListProducts.Items.Clear();
            DropDownListProducts.Items.Add(new ListItem("- Select Product -", "0"));
            foreach (ProductDTO item in productList)
            {
                DropDownListProducts.Items.Add(new ListItem(item.Name, item.ProductId.ToString()));

            }
            DropDownListProducts.Enabled = true;
        }
        else
        {
            DropDownListProducts.Items.Add(new ListItem("- Select Product -", 0.ToString()));
            DropDownListProducts.Enabled = false;
        }

        DropDownListProducts.SelectedIndex = 0;
    }
}