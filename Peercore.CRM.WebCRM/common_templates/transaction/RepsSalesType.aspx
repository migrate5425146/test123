﻿<%@ Page Title="mSales - Reps Sales Type" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="RepsSalesType.aspx.cs" Inherits="common_templates_transaction_RepsSalesType" %>

<%@ Register Src="~/usercontrols/buttonbar.ascx" TagPrefix="ucl" TagName="buttonbar" %>
<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" Runat="Server">
    <style>
        a.sprite_button, sprite_button, .savebtn, .add_options, .leadentry_title_bar .back, .k-button, .title_bar .back {
            height: 35px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" Runat="Server">
        <asp:HiddenField ID="hfPageIndex" runat="server" />

    <asp:HiddenField ID="hfOriginator" runat="server" />
    <asp:HiddenField ID="hfOriginatorType" runat="server" />

    <div id="repmodalWindow" style="display: none">
        <div id="div_repconfirm_message">
        </div>
        <div class="clearall" style="margin-bottom: 15px">
        </div>
        <button id="yes" class="k-button">
            Yes</button>
        <button id="no" class="k-button">
            No</button>
    </div>

    <div class="divcontectmainforms" style="color: black">
        <%--<div class="toolbar_container">
            <div class="toolbar_left" id="div_content">
                <div class="hoributton">
                    <div>
                        <ucl:buttonbar ID="buttonbar" runat="server" />
                    </div>
                </div>
            </div>
            <div class="toolbar_right" id="div3">
                <div class="leadentry_title_bar">
                    <div style="float: right; width: 65%;" align="right">
                        <asp:HyperLink ID="hlBack" runat="server" NavigateUrl="~/Default.aspx">
                        <div class="back"></div>

                        </asp:HyperLink>
                    </div>
                </div>
            </div>
        </div>
        <div class="clearall">
        </div>--%>
        <div id="div_message" runat="server" style="display: none">
        </div>
        <div class="clearall">
        </div>
        <div id="div_mainAssignedRoutes"><!--class="grid_container"-->
            <div id="grid">
            </div>
        </div>
    </div>
    <script type="text/javascript">

        var DistributerTerr = [];

        $(document).ready(function () {
            var todayDate = kendo.toString(kendo.parseDate(new Date()), 'dd-MMM-yyyy');
            var Originator = $("#<%= hfOriginator.ClientID %>").val();
            var OriginatorType = $("#<%= hfOriginatorType.ClientID %>").val();

            LoadSalesTypes();
            LoadAllSalesRepSalesTypeByOriginator(OriginatorType, Originator);
        });

        function LoadSalesTypes() {
            SalesTypes = [];

            var obj = { disId: 1, disName: 'Van-Sales' };
            SalesTypes.push(obj);
            obj = { disId: 2, disName: 'Pre-Sales' };
            SalesTypes.push(obj);
            
            return SalesTypes;
        }

        function LoadAllSalesRepSalesTypeByOriginator(OriginatorType, Originator) {

            var take_grid = $("#MainContent_hfPageIndex").val();

            //$("#div_loader").show();
            $("#grid").html("");
            $("#grid").kendoGrid({
                height: 600,
                columns: [
                    { field: "RepId", title: "Rep Id", width: "120px", hidden: true },
                    { command: "edit", width: "100px" },
                    {
                        title: "Sales Type",
                        field: "SalesType", // bound to the brandId field
                        width: "120px",
                        //template: GetSalesTypeName('#=SalesType#'),
                        template: function (e, field = "SalesType") {
                            //console.log("Field name:", field);
                            //console.log(e[field]);

                            for (var i = 0; i < SalesTypes.length; i++) {
                                var t = SalesTypes[i].disId;
                                if (SalesTypes[i].disId == e[field]) {
                                    if (t == 1) {
                                        return '<span style="color: green">' + SalesTypes[i].disName + '</span>';
                                    }
                                    else {
                                        return '<span style="color: red">' + SalesTypes[i].disName + '</span>';
                                    }
                                }                           
                            }
                        },
                        editor: function (container) { // use a dropdownlist as an editor
                            // create an input element with id and name set as the bound field (brandId)
                            var input = $('<input id="SalesType" name="SalesType">');

                            // append to the editor container
                            input.appendTo(container);

                            // initialize a dropdownlist
                            input.kendoDropDownList({
                                autoBind: false,
                                cascadeFrom: "Category",
                                dataTextField: "disName",
                                dataValueField: "disId",
                                //optionLabel: "none",
                                dataSource: SalesTypes // bind it to the brands array
                            }).appendTo(container);
                        }
                    },
                    { field: "RepCode", title: "Rep Code", width: "120px" },
                    { field: "RepName", title: "Rep Name", width: "200px" },
                    { field: "TerritoryName", title: "Territory", width: "200px" },
                    { field: "AsmName", title: "Asm Name", width: "250px" },
                    { field: "DistributerName", title: "DB Name", width: "320px" },
                ],
                editable: "inline",
                pageable: true,
                sortable: true,
                filterable: true,
                selectable: "single",
                columnMenu: false,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 15,
                    schema: {
                        data: "d.Data",
                        total: "d.Total",
                        model: {
                            id: "RepId",
                            fields: {
                                RepId: { validation: { required: true } },
                                RepCode: { editable: false, nullable: true },
                                RepName: { editable: false, nullable: true },
                                TerritoryCode: { editable: false, nullable: true },
                                TerritoryName: { editable: false, nullable: true },
                                AsmName: { editable: false, nullable: true },
                                DistributerCode: { editable: false, nullable: true },
                                DistributerName: { editable: false, nullable: true },
                                SalesType: { editable: true, nullable: true }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/master/master.asmx/GetAllSalesRepWithSalesType",
                            contentType: "application/json; charset=utf-8",
                            data: { pgindex: take_grid, OriginatorType: OriginatorType, Originator: Originator },
                            type: "POST",
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {
                                    $("#div_loader").hide();
                                    var entityGrid = $("#grid").data("kendoGrid");
                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));
                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        update: {
                            url: ROOT_PATH + "service/master/master.asmx/UpdateSRSalesType",
                            contentType: "application/json; charset=utf-8",
                            data: { Originator: Originator },
                            type: "POST",
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {
                                    $("#div_loader").hide();
                                    var entityGrid = $("#grid").data("kendoGrid");
                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));
                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            data = $.extend({ sort: null, filter: null }, data);
                            return JSON.stringify(data);
                        }
                    }
                }
            });
        }

    </script>
</asp:Content>

