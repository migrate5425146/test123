﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using CRMServiceReference;
using System.Configuration;
using Google.Authenticator;

public partial class auth : System.Web.UI.Page
{
    public string Key
    {
        get
        {
            string strQrKey = string.Empty;
            if (ConfigurationManager.AppSettings.Get("QRKey") != null)
            {
                strQrKey = ConfigurationManager.AppSettings.Get("QRKey").ToString();
            }
            return strQrKey;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.OriginalUserName))
        {
            if (!IsPostBack)
            {
                int userId = UserSession.Instance.OriginatorId;
                string tfaPin = DateTime.Now.ToString("yyMMddhhmmss");

                string userKey = string.Empty;

                if (userId > 0)
                {
                    userKey = userId.ToString() + tfaPin;

                    UserSession.Instance.TFAPin = tfaPin;

                    GenerateImage(userKey);
                }
            }
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    public void GenerateImage(string userKey)
    {
        TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
        //tfa.DefaultClockDriftTolerance = new TimeSpan(0, 1, 0);
        var setupInfo = tfa.GenerateSetupCode("Perfetti SFA", UserSession.Instance.OriginalUserName, Key + userKey, 300, 300);//the width and height of the Qr Code

        string qrCodeImageUrl = setupInfo.QrCodeSetupImageUrl; //  assigning the Qr code information + URL to string
        string manualEntrySetupCode = setupInfo.ManualEntryKey; // show the Manual Entry Key for the users that don't have app or phone
        imgQR.ImageUrl = qrCodeImageUrl;// showing the qr code on the page "linking the string to image element"
        //lblCode.Text = manualEntrySetupCode; // showing the manual Entry setup code for the users that can not use their phone

        lblUserKey.Text = Key + userKey;

        string strCurrentPin = tfa.GetCurrentPIN(Key + userKey);
        lblTFAPin.Text = strCurrentPin;
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        Response.Redirect("activateauth.aspx");
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/login.aspx");
    }
}