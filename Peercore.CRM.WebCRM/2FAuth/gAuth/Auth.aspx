﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Auth.aspx.cs" Inherits="auth" %>

<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>mSales v 2.0.1</title>

    <style type="text/css">
        .header-bar {
            background-color: royalblue;
            height: 90px;
            top: 0px;
            left: 0px;
            line-height: 90px;
            font-size: 30px;
            padding-left: 10px;
            color: #ffffff;
            font-family: sans-serif;
        }

        .tfa-auth-page {
            background-color: #ffffff;
        }

        .container {
            text-align: center;
        }
        .footer {
            background: #fbfcfc;
            text-align: left;
            border-top: 1px solid #ddd;
            width: 98%;
            position: absolute;
            bottom: 0px;
            height: 36px;
            padding-top: 30px;
            padding-bottom: 30px;
            padding-right: 10px;
        }

        .button {
            border: none;
            color: white;
            padding: 15px 32px;
            text-align: center;
            text-decoration: none;
            display: inline-block;
            font-size: 16px;
            margin: 4px 2px;
            cursor: pointer;
        }

        .button1 {background-color: lightgray;} /* Light Gray */
        .button2 {background-color: #008CBA;} /* Blue */
        
        .content-text{
            font-family: sans-serif;
        }

        .button1:hover {
            background-color: #bcbaba;
        }

        .button2:hover {
            background-color: #044a61;
        }

    </style>

    <script type="text/javascript" src="../../assets/scripts/jquery-1.8.2.min.js"></script>
    <script type="text/javascript" src="../../assets/scripts/common.js"></script>

</head>
<body class="tfa-auth-page">
    <div class="header-bar">Activate two factor authentication</div>
    <div class="content-text">
        <p>Google authenticator app allow you to generate security codes on your mobile device. If you do not already use google authenticator app, here is a <a href="https://play.google.com/store/apps/details?id=com.google.android.apps.authenticator2&hl=en" target="_blank">Google Authenticator App</a>.</p>
        <p>To configure the authenticator app</p>
        <ol>
            <li>Open your authenticator app and add a new time-based token.</li>
            <li>Scan the QR code below</li>
        </ol>
    </div>
    <div class="login">
        <form id="form1" runat="server">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <div class="thumbnail">
                            <asp:Label ID="lblUserKey" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblTFAPin" runat="server" Visible="false"></asp:Label>
                            <asp:Image ID="imgQR" runat="server" />
                        </div>
                    </div>
                </div>
            </div>
            <div class="footer">
                <asp:Button ID="btnCancel" runat="server" Text="Back" OnClick="btnCancel_Click" CssClass="button button1" style="float:right;"/>
                <asp:Button ID="btnNext" runat="server" Text="Next"  OnClick="btnNext_Click" CssClass="button button2" style="float: right;"/>
            </div>
        </form>
    </div>
</body>
</html>
