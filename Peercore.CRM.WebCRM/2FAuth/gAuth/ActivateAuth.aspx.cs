﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using CRMServiceReference;
using System.Configuration;
using Google.Authenticator;

public partial class activateauth : System.Web.UI.Page
{
    public string Key
    {
        get
        {
            string strQrKey = string.Empty;
            if (ConfigurationManager.AppSettings.Get("QRKey") != null)
            {
                strQrKey = ConfigurationManager.AppSettings.Get("QRKey").ToString();
            }
            return strQrKey;
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.OriginalUserName))
        {
            //int userId = UserSession.Instance.OriginatorId;
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }
    }

    protected void btnActivate_Click(object sender, EventArgs e)
    {
        OriginatorClient OriginatorClient = new OriginatorClient();
        OriginatorDTO OriginatorDTO = new OriginatorDTO();

        string originator = UserSession.Instance.OriginalUserName;
        int userId = UserSession.Instance.OriginatorId;
        string tfaPin = UserSession.Instance.TFAPin;

        string userKey = userId.ToString() + tfaPin;

        if (Verify(userKey))
        {
            //Update in database both IsAuthenticate and tfaPin
            OriginatorClient originatorClient = new OriginatorClient();
            bool b = originatorClient.UpdateOriginatorTFAAuthenticationSettings(originator, tfaPin, true);

            UserSession.Instance.UserName = UserSession.Instance.OriginalUserName;

            Response.Redirect("~/default.aspx");
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Response.Redirect("auth.aspx");
    }

    public bool Verify(string userKey)
    {
        bool ret = false;
        string user_enter = txtOtpCode.Text;

        if (user_enter == "")
        {
            divError.Text = "Please enter OTP key.";
            return false;
        }

        TwoFactorAuthenticator tfa = new TwoFactorAuthenticator();
        //tfa.DefaultClockDriftTolerance = new TimeSpan(0, 1, 0);
        //bool isCorrectPIN = tfa.ValidateTwoFactorPIN(Key + id, user_enter,new TimeSpan(0,1,0));

        lblUserKey.Text = Key + userKey;
        
        string strCurrentPin = tfa.GetCurrentPIN(Key + userKey);
        lblTFAPin.Text = strCurrentPin;

        
        if (strCurrentPin.Equals(user_enter))
        {
            ret = true;
        }
        else
        {
            divError.Text = "Invalid OTP key.";
        }

        return ret;
    }
}