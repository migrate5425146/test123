﻿<%@ Page Title="mSales - TFA User Assign" Language="C#" MasterPageFile="~/Site.master" AutoEventWireup="true" CodeFile="user_tfa_assign.aspx.cs" Inherits="_2FAuth_user_tfa_assign" %>

<%@ MasterType TypeName="SiteMaster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="HeadContent" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="Server">
    <asp:HiddenField ID="hfPageIndex" runat="server" />
    <asp:HiddenField ID="OriginatorString" runat="server" />
    <script type="text/javascript" src="../assets/scripts/jquery.validate.js"></script>
    <div id="div_main" class="divcontectmainforms">
        <div class="clearall">
        </div>

        <div id="div_message" runat="server" style="display: none;"></div>
        <div class="clearall">
        </div>

        <div style="min-width: 200px; overflow: auto">
            <div style="float: left; width: 100%; overflow: auto" >
                <div id="originator">
                </div>
            </div>
        </div>

    </div>

    <script type="text/javascript">

        $(document).ready(function () {
            loadOriginatorGrid();
        });

        function loadOriginatorGrid() {

            var take_grid = $("#MainContent_hfPageIndex").val();

            $("#originator").html("");
            $("#originator").kendoGrid({
                height: 800,
                columns: [
                    { field: "UserId", title: "UserId", width: "85px", hidden: true },
                    { field: "Originator", title: "Originator", width: "100px" },
                    { field: "UserName", title: "Name", width: "150px" },
                    { field: "designation", title: "Designation", width: "200px" },
                    {
                        field: "IsTFAUser",
                        template: "<input type=\"checkbox\" #= IsTFAUser ? checked='checked' : '' # class=\"option-input checkbox check_rowPT\"/>",
                        headerTemplate: '<input type=\"checkbox\" class=\" option-input checkbox checkAllPT\"/> TFA User ',
                        width: "100px",
                        sortable: false
                    },
                    {
                        field: "TFAIsAuthenticated",
                        template: "<input type=\"checkbox\" #= TFAIsAuthenticated ? checked='checked' : '' # class=\"option-input checkbox check_rowIsAuth\"/>",
                        headerTemplate: '<input type=\"checkbox\" class=\" option-input checkbox checkAllIsAuth\"/> Is TFA Auth ',
                        width: "100px",
                        sortable: false,
                        hidden: true
                    }
                ],
                editable: false, // enable editing
                pageable: true,
                sortable: true,
                filterable: true,
                selectable: "single",
                columnMenu: false,
                dataSource: {
                    serverSorting: true,
                    serverPaging: true,
                    serverFiltering: true,
                    pageSize: 20,
                    schema: {
                        data: "d.Data", // ASMX services return JSON in the following format { "d": <result> }. Specify how to get the result.
                        total: "d.Total",

                        model: { // define the model of the data source. Required for validation and property types.
                            fields: {
                                UserId: { validation: { required: true } },
                                Originator: { editable: false, nullable: true },
                                UserName: { editable: false, nullable: true },
                                designation: { editable: false, nullable: true },
                                IsTFAUser: { editable: false, nullable: true },
                                TFAIsAuthenticated: { editable: false, nullable: true }
                            }
                        }
                    },
                    transport: {
                        read: {
                            url: ROOT_PATH + "service/lead_customer/common.asmx/GetOriginatorsfor2FA", //specify the URL which data should return the records. This is the Read method of the Products.asmx service.
                            contentType: "application/json; charset=utf-8", // tells the web service to serialize JSON
                            data: { pgindex: take_grid },
                            type: "POST", //use HTTP POST request as the default GET is not allowed for ASMX
                            complete: function (jqXHR, textStatus) {
                                if (textStatus == "success") {

                                    $("#div_loader").hide();
                                    var entityGrid = $("#originator").data("kendoGrid");

                                    if ((take_grid != '') && (gridindex == '0')) {
                                        entityGrid.dataSource.page((take_grid - 1));

                                        gridindex = '1';
                                    }
                                }
                            }
                        },
                        parameterMap: function (data, operation) {
                            if (operation != "read") {
                                return JSON.stringify({ products: data.models })
                            } else {
                                data = $.extend({ sort: null, filter: null }, data);
                                return JSON.stringify(data);
                            }
                        }
                    }
                }
            });

            $("#originator .k-grid-content").on("change", "input.check_rowPT", function (e) {
                var Originator = $("#<%= OriginatorString.ClientID %>").val();
                var grid = $("#originator").data("kendoGrid");
                var checked = $(this).is(':checked');

                var col = $(this).closest('td');
                dataItem = grid.dataItem($(e.target).closest("tr"));
                dataItem.set(grid.columns[col.index()].field, checked);

                UpdateIsTFAUser(Originator, dataItem.Originator, checked);
            });

            $("#originator .k-grid-content").on("change", "input.check_rowIsAuth", function (e) {
                var Originator = $("#<%= OriginatorString.ClientID %>").val();
                var grid = $("#originator").data("kendoGrid");
                var checked = $(this).is(':checked');

                var col = $(this).closest('td');
                dataItem = grid.dataItem($(e.target).closest("tr"));
                dataItem.set(grid.columns[col.index()].field, checked);

                UpdateIsTFAAuth(Originator, dataItem.Originator, checked);
            });

            $("#originator .k-grid-header").on('change', "input.checkAllPT", function (e) {
                var Originator = $("#<%= OriginatorString.ClientID %>").val();
                var grid = $('#originator').data('kendoGrid');
                var checked = $(this).is(':checked');

                grid.table.find("tr").find("td:last input").attr("checked", checked);

                var items = $("#originator").data("kendoGrid").dataSource.data();
                for (i = 0; i < items.length; i++) {
                    var item = items[i];
                    item.SchemeTerritoryStatus = "A";

                    UpdateIsTFAUser(Originator, item.Originator, checked);
                }

                if (!checked) {
                    $("#originator").data("kendoGrid").clearSelection();
                }
            });

            $("#originator .k-grid-header").on('change', "input.checkAllIsAuth", function (e) {
                var Originator = $("#<%= OriginatorString.ClientID %>").val();
                var grid = $('#originator').data('kendoGrid');
                var checked = $(this).is(':checked');

                grid.table.find("tr").find("td:last input").attr("checked", checked);

                var items = $("#originator").data("kendoGrid").dataSource.data();
                for (i = 0; i < items.length; i++) {
                    var item = items[i];
                    item.SchemeTerritoryStatus = "A";

                    UpdateIsTFAAuth(Originator, item.Originator, checked);
                }

                if (!checked) {
                    $("#originator").data("kendoGrid").clearSelection();
                }
            });
        }

        function UpdateIsTFAUser(Originator, tfaUser, IsCheced) {
           
            var url = ROOT_PATH + "service/lead_customer/common.asmx/UpdateIsTFAUser"

            $.ajax({
                type: "POST",
                url: url,
                data: '{ originator: "' + Originator + '", ' +
                    ' tfa_user: "' + tfaUser + '", ' +
                    ' is_checked: "' + IsCheced + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d.Status == true) {
                        // var sucessMsg = GetSuccesfullMessageDiv("Region was successfully Saved.", "MainContent_div_message");
                        // $('#MainContent_div_message').css('display', 'block');
                        // $("#MainContent_div_message").html(sucessMsg);
                        // //closepopup();
                        //// LoadTerritoriesforDiscountScheme(0, Originator, ShemeHeaderId);
                        // hideStatusDiv("MainContent_div_message");
                    }
                    else {
                        var errorMsg = GetErrorMessageDiv("Error occurred!.", "MainContent_div_message");

                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);

                        hideStatusDiv("MainContent_div_message");
                    }
                },
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                    }
                }
            });
        }

        function UpdateIsTFAAuth(Originator, tfaUser, IsCheced) {
            var url = ROOT_PATH + "service/lead_customer/common.asmx/UpdateIsTFAAuth"

            $.ajax({
                type: "POST",
                url: url,
                data: '{ originator: "' + Originator + '", ' +
                    ' tfa_user: "' + tfaUser + '", ' +
                    ' is_checked: "' + IsCheced + '" }',
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (msg) {
                    if (msg.d.Status == true) {
                        // var sucessMsg = GetSuccesfullMessageDiv("Region was successfully Saved.", "MainContent_div_message");
                        // $('#MainContent_div_message').css('display', 'block');
                        // $("#MainContent_div_message").html(sucessMsg);
                        // //closepopup();
                        //// LoadTerritoriesforDiscountScheme(0, Originator, ShemeHeaderId);
                        // hideStatusDiv("MainContent_div_message");
                    }
                    else {
                        var errorMsg = GetErrorMessageDiv("Error occurred!.", "MainContent_div_message");

                        $('#MainContent_div_message').css('display', 'block');
                        $("#MainContent_div_message").html(errorMsg);

                        hideStatusDiv("MainContent_div_message");
                    }
                },
                error: function (msg, textStatus) {
                    if (textStatus == "error") {
                        var msg = msg;
                    }
                }
            });
        }

    </script>
</asp:Content>

