﻿using Peercore.CRM.Common;
using Peercore.CRM.Shared;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class _2FAuth_user_tfa_assign : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!string.IsNullOrWhiteSpace(UserSession.Instance.UserName))
        {
            if (!IsPostBack)
            {
                //dtpDate.Text = DateTime.Today.ToString("dd-MMM-yyyy");
                //dtpDateTo.Text = DateTime.Today.ToString("dd-MMM-yyyy");

                //Load Products
                //LoadOptions();
            }

            //DropDownListProducts.SelectedIndex = 0;
            //Remove the session before setting the breadcrumb.
            Session.Remove(CommonUtility.BREADCRUMB);
            Master.SetBreadCrumb("Users TFA Assign (Web)", "#", "");
        }
        else
        {
            Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
        }

        OriginatorString.Value = UserSession.Instance.UserName;
    }
}