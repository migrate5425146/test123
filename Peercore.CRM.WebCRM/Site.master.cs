﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Peercore.CRM.Shared;
using CRMServiceReference;
using System.Text;
using Peercore.CRM.Common;
using System.Text.RegularExpressions;
using System.Web.UI.HtmlControls;

public partial class SiteMaster : System.Web.UI.MasterPage
{
    #region Events
    protected void Page_Load(object sender, EventArgs e)
    {
        RegisterJavascript();
        SettingActiveModule();
        if (!IsPostBack)
        {
            if (UserSession.Instance.UserName != null && UserSession.Instance.UserName != string.Empty)
            {
                //LeadAndCustomers(true);
                if (UserSession.Instance.IsCheckManagerMode)
                {
                    ButtonDisableManagerMode.Visible = true;
                    btnManager.Visible = false;
                    drop_ChiildOriginators.Visible = false;
                }
                else
                {
                    ButtonDisableManagerMode.Visible = false;
                    btnManager.Visible = true;
                }

                LoadChiildOriginators();

                div_filter.Visible = true;
                drop_ChiildOriginators.Visible = true;

                lbtnSignOut.Visible = true;
            }
        }
    }

    protected void drop_ChiildOriginators_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //div_loader.Attributes.Add("style", "display:block");

            if (drop_ChiildOriginators.SelectedIndex != -1)
            {
                //if (drop_ChiildOriginators.SelectedValue.Equals("All"))
                string rep = "";
                if (drop_ChiildOriginators.SelectedIndex == 0)
                {
                    //UserSession.Instance.ManagerMode = true;
                    UserSession.Instance.ChildOriginators = new OriginatorClient().GetChildOriginators(UserSession.Instance.OriginalUserName,  UserSession.Instance.ManagerMode);
                    string childOriginatorsForRep = UserSession.Instance.ChildOriginators.Replace("originator", "r.originator");
                    UserSession.Instance.RepCodeList = new OriginatorClient().GetChildReps(UserSession.Instance.OriginalUserName, true, ref rep, childOriginatorsForRep);
                    UserSession.Instance.ChildReps = rep;
                    UserSession.Instance.FilterByUserName = "";
                    UserSession.Instance.FilterByRepType = "";
                    UserSession.Instance.FilterByRepOriginatorId = 0;
                    UserSession.Instance.UserName = UserSession.Instance.OriginalUserName;
                }
                else
                {
                    UserSession.Instance.ChildOriginators = new OriginatorClient().GetChildOriginators(drop_ChiildOriginators.SelectedValue, UserSession.Instance.ManagerMode);
                    string childOriginatorsForRep = UserSession.Instance.ChildOriginators.Replace("originator", "r.originator");
                    UserSession.Instance.RepCodeList = new OriginatorClient().GetChildReps(drop_ChiildOriginators.SelectedValue, false, ref rep, childOriginatorsForRep);
                    UserSession.Instance.ChildReps = rep;

                    UserSession.Instance.FilterByUserName = drop_ChiildOriginators.SelectedValue;

                    if (Session[CommonUtility.BDM_LIST] != null)
                    {
                        List<OriginatorDTO> originatorList = Session[CommonUtility.BDM_LIST] as List<OriginatorDTO>;
                        OriginatorDTO originatorDTO = originatorList.Find(a => a.UserName == UserSession.Instance.FilterByUserName);
                        if (originatorDTO != null)
                        {
                            UserSession.Instance.FilterByRepOriginatorId = originatorDTO.OriginatorId;
                            UserSession.Instance.FilterByRepType = originatorDTO.RepType;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            //commonUtility.ErrorMaintain(ex);
        }

        Response.Redirect(Request.Url.AbsoluteUri);
    }

    //Disable Manager Mode.
    protected void btnManager_Click(object sender, EventArgs e)
    {
        drop_ChiildOriginators.Visible = false;
        ButtonDisableManagerMode.Visible = true;
        UserSession.Instance.ManagerMode = false;
        UserSession.Instance.UserName = UserSession.Instance.OriginalUserName;
        UserSession.Instance.ChildOriginators = " (originator = '" + UserSession.Instance.UserName + "' )";
        UserSession.Instance.FilterByUserName = "";
        UserSession.Instance.IsCheckManagerMode = true;
        btnManager.Visible = false;

        Response.Redirect(Request.Url.AbsoluteUri);
    }

    //Enable Manager Mode.
    protected void ButtonDisableManagerMode_Click(object sender, EventArgs e)
    {
        try
        {
            btnManager.Visible = true;
            drop_ChiildOriginators.Visible = true;
            UserSession.Instance.ManagerMode = true;
            drop_ChiildOriginators.SelectedIndex = 0;
            UserSession.Instance.ChildOriginators = new OriginatorClient().GetChildOriginators(UserSession.Instance.OriginalUserName, UserSession.Instance.ManagerMode);
            ButtonDisableManagerMode.Visible = false;
            UserSession.Instance.IsCheckManagerMode = false;
        }
        catch (Exception ex)
        {
            //commonUtility.ErrorMaintain(ex);
        }
        Response.Redirect(Request.Url.AbsoluteUri);
    }

    protected void lbtnSignOut_Click(object sender, EventArgs e)
    {
        Session.RemoveAll();
        Response.Redirect(ConfigUtil.ApplicationPath + "login.aspx");
    }
    #endregion

    #region Methods
    private void RegisterJavascript()
    {
        //ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "jquery-1.8.2.min", "http://code.jquery.com/jquery-1.8.2.min.js");
        ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "jquery-1.8.2.min", string.Format("{0}{1}", ConfigUtil.ApplicationPath, "assets/scripts/jquery-1.8.2.min.js"));
        //ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "jquery-1.9.1.min", string.Format("{0}{1}", ConfigUtil.ApplicationPath, "assets/scripts/jquery.min.js"));
        //ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "kendo.web.min", string.Format("{0}{1}", ConfigUtil.ApplicationPath, "assets/scripts/telerik/kendo.all.min.js"));
        ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "kendo.web.min", string.Format("{0}{1}", ConfigUtil.ApplicationPath, "assets/scripts/telerik/kendo2016.all.min.js"));
        //ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "kendo.web.min", "http://cdn.kendostatic.com/2012.3.1114/js/kendo.all.min.js");
        
        //ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "jquery-ui.min", "http://code.jquery.com/ui/1.8.0/jquery-ui.min.js");
        //ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "jquery-ui.min", string.Format("{0}{1}", ConfigUtil.ApplicationPath, "assets/scripts/jquery-ui-1.8.0.min.js"));
        ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "jquery-ui.min", string.Format("{0}{1}", ConfigUtil.ApplicationPath, "assets/scripts/jquery-ui.js"));
        //ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "jquery-impromptu", string.Format("{0}{1}", ConfigUtil.ApplicationPath, "assets/scripts/jquery-impromptu.4.0.min.js"));
        
        ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "highcharts.js", string.Format("{0}{1}", ConfigUtil.ApplicationPath, "assets/scripts/highchart2016.js"));
        ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "selectbox-1.js", string.Format("{0}{1}", ConfigUtil.ApplicationPath, "assets/scripts/selectbox-1.js"));

        ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "common.js", string.Format("{0}{1}", ConfigUtil.ApplicationPath, "assets/scripts/common.js?version=1.0"));
        ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "master.js", string.Format("{0}{1}", ConfigUtil.ApplicationPath, "assets/scripts/custom/master.js?version=1.0"));
        ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "product.js", string.Format("{0}{1}", ConfigUtil.ApplicationPath, "assets/scripts/custom/product.js?version=1.0"));
        ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "call_cycle.js", string.Format("{0}{1}", ConfigUtil.ApplicationPath, "assets/scripts/custom/call_cycle.js?version=1.0"));
        ScriptManager.RegisterClientScriptInclude(this, this.GetType(), "discounts.js", string.Format("{0}{1}", ConfigUtil.ApplicationPath, "assets/scripts/custom/discounts.js?version=1.0"));

        if (UserSession.Instance.Originator != "")
        {
            logged_in.InnerHtml = "Welcome " + UserSession.Instance.Originator;
            //lbtnSignOut.Text = "Sign Out";
        }
    }

    public void SetCommonHeader(string menu)
    {
        //if (!string.IsNullOrWhiteSpace(menu))
        //{
        //    switch (menu)
        //    {
        //        case "lead":
        //            LeadAndCustomers(false);
        //            break;
        //        case "pipe":
        //            PipelineAndOppotunities(true);
        //            break;
        //        case "activity":
        //            ActivityPlanner();
        //            break;
        //        case "calendar":
        //            Calendar();
        //            break;
        //        case "dashboard":
        //            Dashboard();
        //            break;
        //        default:
        //            LeadAndCustomers(true);
        //            PipelineAndOppotunities(false);
        //            break;
        //    }
        //}
    }

    public void LeadAndCustomers(bool ishomePage)
    {
        try
        {
            //Load LeadStage Graphs.
            LeadStageClient leadStageClient = new LeadStageClient();

            ArgsDTO args = new ArgsDTO();
            if (UserSession.Instance != null)
            {
                args.Originator = UserSession.Instance.OriginatorCookers;
                args.ChildOriginators = UserSession.Instance.ChildOriginators;
                args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
                args.CRMAuthLevel = (!string.IsNullOrWhiteSpace(UserSession.Instance.FilterByUserName)) ? 0 : UserSession.Instance.CRMAuthLevel;
                args.RepCode = UserSession.Instance.RepCode;
                args.RepCodeList = UserSession.Instance.RepCodeList;
                args.ChildReps = UserSession.Instance.ChildReps;
                args.RepType = UserSession.Instance.RepType;
            }

            List<LeadStageDTO> leadList = leadStageClient.GetLeadStageChartData(args);
            div_topheader_content_two.InnerHtml = BindLeadStage(leadList);
            //div_topheader_two.InnerHtml = "<div class=\"homemsgboxtopic\">Leads & Customers</div>";
        }
        catch (Exception ex)
        {
            //commonUtility.ErrorMaintain(ex);
        }
    }

    private string BindLeadStage(List<LeadStageDTO> leadStageList)
    {
        StringBuilder sb = new StringBuilder();
        if (leadStageList != null)
        {
            if (leadStageList.Count != 0)
            {
                // Populate series data

                sb.Append("<div>");
                sb.Append("<script type='text/javascript'>");
                sb.Append("var chart;");

                //Setting Colors
                sb.Append(" Highcharts.setOptions({");
               // sb.Append("     colors: ['#FF0000', '#F79646', '#92D050', '#83B5F1', '#FFFF00' ]");
                sb.Append("     colors: ['#F79646', '#83B5F1' ]");
                sb.Append("  });");

                sb.Append("$(document).ready(function () {");
                sb.Append("var categoryLinks = {");
             //   sb.Append("     'Lead': ROOT_PATH + 'lead_customer/transaction/lead_contacts.aspx?cht=lead&ty=lead',");
                sb.Append("     'Customer': ROOT_PATH + 'lead_customer/transaction/lead_contacts.aspx?cht=lead&ty=customer',");
            //    sb.Append("     'Quote': ROOT_PATH + 'lead_customer/transaction/lead_contacts.aspx?cht=lead&ty=quote',");
            //    sb.Append("     'End User': ROOT_PATH + 'lead_customer/transaction/lead_contacts.aspx?cht=lead&ty=enduser',");
                sb.Append("     'Pending Approvals': ROOT_PATH + 'lead_customer/transaction/lead_contacts.aspx?cht=lead&ty=pendingcustomer'");
                sb.Append("};");
                sb.Append("var colors = Highcharts.getOptions().colors,");
                sb.Append(" chart = new Highcharts.Chart({");
                sb.Append("    chart: {");
                sb.Append("        renderTo: 'div_topheader_content_two',");
                sb.Append("       type: 'bar',");
                sb.Append("       backgroundColor:'transparent'");
                sb.Append("   },");

                //Setting Title
                sb.Append("   title: {");
                sb.Append("       text: 'Lead Staging'");
                sb.Append("   },");

                //Setting xAxis
                sb.Append("  xAxis: {");
                sb.Append("      categories:  [" + RenderYAxis(leadStageList) + "],");
                sb.Append("      labels: {");
                sb.Append("         formatter: function() {");
                sb.Append("             return '<a href=\"' + categoryLinks[this.value] + '\">' + this.value + '</a>';");
                sb.Append("         }");
                sb.Append("      },");
                sb.Append("      title: {");
                sb.Append("          text: null");
                sb.Append("      }");
                sb.Append("  },");

                //Setting yAxis
                sb.Append("  yAxis: {");
                sb.Append("  min: 10,");
                sb.Append("     title: {");
                sb.Append("         text: ''");
                sb.Append("    },");

                //Start stackLabels
                sb.Append("     stackLabels: {");
                sb.Append("        enabled: true,");
                sb.Append("       style: {");
                sb.Append("           fontWeight: 'bold',");
                sb.Append("           color: 'gray',");
                sb.Append("           backgroundColor: 'red'");
                sb.Append("       }");
                sb.Append("    }");
                sb.Append(" },");
                //End stackLabels

                //Start Legends
                sb.Append(" legend: {");
                sb.Append("     layout: 'vertical',");
                sb.Append("     align: 'right',");
                //sb.Append("      verticalAlign: 'top',");
                sb.Append("     x: -100,");
                sb.Append("     y: 100,");
                sb.Append("     floating: true,");
                sb.Append("     borderWidth: 1,");
                sb.Append("     backgroundColor: '#FFFFFF',");
                sb.Append("     shadow: false");
                sb.Append(" },");
                //End Legends

                //Start Tooltip
                sb.Append(" tooltip: {");
                sb.Append("    formatter: function () {");
                sb.Append("        return '' +");
                sb.Append("    this.x + ': ' + Highcharts.numberFormat(this.y, 0);");
                sb.Append("    }");
                sb.Append(" },");
                //End Tooltip

                sb.Append(" plotOptions: {");
                sb.Append(" series: {");
                sb.Append("     stacking: 'normal',");
                sb.Append("     pointWidth: 25,");
                sb.Append("     pointPadding: 0,");
                sb.Append("     cursor: 'pointer',");
                sb.Append("     point: {");
                sb.Append("         events: {");
                sb.Append("             click: function(e) { ");
                sb.Append("               LeadStagingChartLoad(this.category) ;  ");
                sb.Append("             }");
                sb.Append("         }");
                sb.Append("     },");
                sb.Append(" }");
                sb.Append("},");

                //Start Plotoption
                /*
                sb.Append("plotOptions: {");
                sb.Append("column: {");
                sb.Append("pointPadding: 0.2,");
                sb.Append("borderWidth: 0,");
                //sb.Append("cursor: 'pointer',");

                //Setting drill level function
                
                    //Setting cursor when drilldown.
                    sb.Append("cursor: 'pointer',");

                    sb.Append("point: {");
                    sb.Append("events: {");
                    sb.Append("click: function() {");
                    sb.Append("var drilldown = this.drilldown;");
                    sb.Append("alert(drilldown);");
                    sb.Append("}");
                    sb.Append("}");
                    sb.Append("},");

                //Start DataLabel.
                sb.Append("dataLabels: {");
                sb.Append("enabled: false,");
                sb.Append("rotation: -90,");
                //sb.Append("color: '#000000',");
                sb.Append("align: 'right',");
                sb.Append("x: -3,");
                sb.Append("y: 10,");
                sb.Append("formatter: function () {");
                sb.Append("return this.y;");
                sb.Append("},");
                sb.Append("style: {");
                sb.Append("fontSize: '13px',");
                sb.Append("fontFamily: 'Verdana, sans-serif'");
                sb.Append("}");
                sb.Append("}");
                //End Data Label

                //End Coloumn Property
                sb.Append("}");
                sb.Append("},");
                */

                //End Plotoption

                //Start Serious
                sb.Append("series: [{");
                sb.Append("     name: '',");
                sb.Append("     data: [" + RenderDataForBarChart(leadStageList) + "],");
                sb.Append("     dataLabels: {");
                sb.Append("         enabled: false,");
                sb.Append("         rotation: -90,");
                sb.Append("         color: '#FFFFFF',");
                sb.Append("         align: 'right',");
                sb.Append("         x: -3,");
                sb.Append("         y: 10,");
                sb.Append("         formatter: function () {");
                sb.Append("             return this.y;");
                sb.Append("         },");
                sb.Append("       style: {");
                sb.Append("          fontSize: '13px',");
                sb.Append("           fontFamily: 'Verdana, sans-serif'");
                sb.Append("       }");
                sb.Append("    }");
                sb.Append(" }]");
                sb.Append(" });");
                sb.Append("});");
                sb.Append("</script>");
                sb.Append("</div>");
            }
        }

        return sb.ToString();
    }

    string RenderDataForBarChart(List<LeadStageDTO> list)
    {
        LeadStageDTO lssto = new LeadStageDTO();
        //lssto.LeadStage = "Pending Approvals";
        //list.Add(lssto);
        StringBuilder stb = new StringBuilder();
        int index = 0;
        foreach (LeadStageDTO item in list)
        {
            if (item.LeadsCount != -1)
            {
                string s = item.LeadsCount.ToString();
                index++;
                if (index == list.Count){
                    if (item.LeadStage == "End User")
                    {
                        stb.Append("{y: " + s.ToString() + ",color: '#3a74bc'  }");
                    }
                    else if (item.LeadStage == "Customer")
                    {
                        stb.Append("{y: " + s.ToString() + ",color: '#d88227'  }");
                    }
                    else if (item.LeadStage == "Quote")
                    {
                        stb.Append("{y: " + s.ToString() + ",color: '#b1403d'  }");
                    }
                    else if (item.LeadStage == "Lead")
                    {
                        stb.Append("{y: " + s.ToString() + ",color: '#83a43d'  }");
                    }
                    else if (item.LeadStage == "Pending Approvals")
                    {
                        stb.Append("{y: " + s.ToString() + ",color: '#83B5F1'  }");
                    }

                }
                else
                {
                    if (item.LeadStage == "End User")
                    {
                        stb.Append("{y: " + s.ToString() + ",color: '#3a74bc'  },");
                    }
                    else if (item.LeadStage == "Customer")
                    {
                        stb.Append("{y: " + s.ToString() + ",color: '#d88227'  },");
                    }
                    else if (item.LeadStage == "Quote")
                    {
                        stb.Append("{y: " + s.ToString() + ",color: '#b1403d'  },");
                    }
                    else if (item.LeadStage == "Lead")
                    {
                        stb.Append("{y: " + s.ToString() + ",color: '#83a43d'  },");
                    }
                    else if (item.LeadStage == "Pending Approvals")
                    {
                        stb.Append("{y: " + s.ToString() + ",color: '#83B5F1'  }");
                    }
                }
               // stb.Append("{y: " + s.ToString() + ",color: '#83a43d'  }");
            }
        }
        return stb.ToString();
    }

    private string RenderYAxis(List<LeadStageDTO> list)
    {
        int index = 0;
        StringBuilder stb = new StringBuilder();
        foreach (LeadStageDTO item in list)
        {
            index++;
            //if (index == list.Count+1)
            //    stb.Append("'" + item.LeadStage.ToString() + "'");
            //else
            //    stb.Append("'" + item.LeadStage.ToString() + "',");
                stb.Append("'" + item.LeadStage.ToString() + "',");
        }
        stb.Append("'Pending Approvals'");
        return stb.ToString();
    }

    private void LoadChiildOriginators()
    {
        try
        {
            if (!IsPostBack)
            {
                drop_ChiildOriginators.Visible = true;
                CommonClient commonClient = new CommonClient();

                ArgsDTO args = new ArgsDTO();
                if (UserSession.Instance != null)
                {
                    args.Originator = UserSession.Instance.UserName;
                    args.ChildOriginators = UserSession.Instance.ChildOriginators;
                    args.DefaultDepartmentId = UserSession.Instance.DefaultDepartmentId;
                    args.ManagerMode = UserSession.Instance.ManagerMode;

                    //li_area_entry.Visible = false;
                    //li_repentry.Visible = true;
                    id_distributorentry.Visible = true;
                    a_discount_entry.Visible = false;
                    //li_aseentry.Visible = false;
                    id_idle_customers.Visible = false;
                    a_user_accounts.Visible = false;
                    a_user_tfa_accounts.Visible = false;
                    a_incentive_details.Visible = false;
                    id_distributorassign.Visible = false;
                    id_territoryassign.Visible = false;
                    a_order_details.Visible = false;
                    a_attendance_reason.Visible = false;
                    a_kpi_setting.Visible = false;

                    id_outlet_mt_category.Visible = false;
                    id_outlet_details_outlets.Visible = false;
                    id_outlet_details_outlets_asm_confirm.Visible = false;
                    id_outlet_details_outlets_sales_confirm.Visible = false;

                    asset_management.Visible = false;
                    media_management.Visible = false;
                    media_management_upload.Visible = false;
                    media_management_category.Visible = false;

                    id_asset_details.Visible = false;
                    id_transaction_history.Visible = false;

                    id_tradediscount_viwer.Visible = false;
                    id_tradediscount_report.Visible = false;
                    id_tradediscount_settings.Visible = false;
                    id_tradediscount_summary_report.Visible = false;

                    if (UserSession.Instance.OriginatorString == "ASE")
                    {
                        //a_pre_sales.Visible = false;
                        id_repsalestype.Visible = false;
                        //id_routeassign.Visible = false;
                        //li_area_entry.Visible = true;
                        a_order_details.Visible = true;

                        id_outlet_details_outlets.Visible = false;
                        id_outlet_details_outlets_asm_confirm.Visible = true;
                        id_outlet_details_outlets_sales_confirm.Visible = false;

                        media_management.Visible = true;
                        media_management_upload.Visible = true;

                        id_tradediscount_report.Visible = true;
                    }

                    
                    //if (UserSession.Instance.OriginatorString == "DR")
                    //{
                    //    li_repentry.Visible = false;
                    //}

                    
                    if (UserSession.Instance.OriginatorString == "DR" || UserSession.Instance.OriginatorString == "DIST")
                    {
                        id_distributorentry.Visible = false;
                    }

                    if (UserSession.Instance.OriginatorString == "DIST")
                    {
                        li_activity_planner.Visible = false;
                        li_reps_route_map.Visible = false;
                        a_route_details.Visible = false;
                        a_pre_sales.Visible = false;
                        a_outlets_details.Visible = false;
                        id_vehicle_subsidy.Visible = false;
                        id_upload_summary.Visible = false;

                        id_tradediscount_report.Visible = true;
                    }

                    if (UserSession.Instance.OriginatorString == "RSM")
                    {
                        a_discount_entry.Visible = true;
                        //li_aseentry.Visible = true;
                        //li_area_entry.Visible = true;
                        id_idle_customers.Visible = true;
                        a_user_accounts.Visible = true;
                        li_loading_stocks.Visible = true;
                        a_upload_details.Visible = true;
                        a_master_files.Visible = true;
                        id_holidayentery.Visible = true;
                        //id_routeentry.Visible = true;
                        //id_routeassign.Visible = true;
                        li_lead_customer.Visible = true;
                        a_invoice_details.Visible = true;
                        a_upload_details.Visible = true;
                        a_user_details.Visible = true;
                        a_settings.Visible = true;
                        a_incentive_details.Visible = true;

                        id_tradediscount_report.Visible = true;
                    }

                    if (UserSession.Instance.OriginatorString == "ADMIN")
                    {
                        a_discount_entry.Visible = true;
                        //li_aseentry.Visible = true;
                        //li_area_entry.Visible = true;
                        id_idle_customers.Visible = true;
                        a_user_accounts.Visible = true;
                        a_user_tfa_accounts.Visible = true;
                        li_loading_stocks.Visible = true;
                        a_upload_details.Visible = true;
                        a_master_files.Visible = true;
                        id_holidayentery.Visible = true;
                        //id_routeentry.Visible = true;
                        //id_routeassign.Visible = true;
                        li_lead_customer.Visible = true;
                        a_invoice_details.Visible = true;
                        a_upload_details.Visible = true;
                        a_user_details.Visible = true;
                        a_settings.Visible = true;
                        a_incentive_details.Visible = true;
                        id_distributorassign.Visible = true;
                        id_territoryassign.Visible = true;
                        a_order_details.Visible = true;
                        a_attendance_reason.Visible = true;

                        id_outlet_details_outlets.Visible = true;
                        id_outlet_details_outlets_asm_confirm.Visible = true;
                        id_outlet_details_outlets_sales_confirm.Visible = true;

                        asset_management.Visible = true;
                        media_management.Visible = true;
                        media_management_upload.Visible = true;
                        media_management_category.Visible = true;

                        a_kpi_setting.Visible = true;
                        id_asset_details.Visible = true;
                        id_transaction_history.Visible = true;
                        id_outlet_mt_category.Visible = true;

                        id_tradediscount_viwer.Visible = true;
                        id_tradediscount_report.Visible = true;
                        id_tradediscount_settings.Visible = true;
                        id_tradediscount_summary_report.Visible = true;
                    }

                    if (UserSession.Instance.OriginatorString == "REPORT")
                    {
                        a_discount_entry.Visible = false;
                        //li_aseentry.Visible = false;
                        //li_area_entry.Visible = false;
                        id_idle_customers.Visible = false;
                        li_lead_customer.Visible = false;
                        li_loading_stocks.Visible = false;
                        li_activity_planner.Visible = false;
                        a_route_details.Visible = false;
                        a_incentive_details.Visible = false;
                        a_invoice_details.Visible = false;
                        a_master_files.Visible = false;
                        a_upload_details.Visible = false;


                        id_asset_details.Visible = true;

                        id_tradediscount_report.Visible = true;
                        id_tradediscount_summary_report.Visible = true;
                    }

                    if (UserSession.Instance.OriginatorString == "MANAGER")
                    {
                        a_discount_entry.Visible = false;
                        //li_aseentry.Visible = false;
                        //li_area_entry.Visible = false;
                        id_idle_customers.Visible = false;
                        li_lead_customer.Visible = false;
                        li_loading_stocks.Visible = false;
                        li_activity_planner.Visible = false;
                        a_route_details.Visible = false;
                        a_incentive_details.Visible = false;
                        a_invoice_details.Visible = false;
                        a_master_files.Visible = false;
                        a_upload_details.Visible = false;

                        id_tradediscount_report.Visible = true;
                        id_tradediscount_summary_report.Visible = true;
                    }

                    if (UserSession.Instance.OriginatorString == "SLC") // Sales Cordinator
                    {
                        a_discount_entry.Visible = false;
                        //li_aseentry.Visible = true;
                        //li_area_entry.Visible = true;
                        id_idle_customers.Visible = true;
                        a_user_accounts.Visible = true;
                        li_loading_stocks.Visible = false;
                        a_upload_details.Visible = true;
                        a_master_files.Visible = false;
                        id_holidayentery.Visible = false;
                        //id_routeentry.Visible = true;
                        //id_routeassign.Visible = true;
                        li_lead_customer.Visible = true;
                        a_invoice_details.Visible = true;
                        lCustomerUpload.Visible = false;
                        a_user_details.Visible = false;
                        a_settings.Visible = true;
                        a_incentive_details.Visible = true;
                        id_distributorassign.Visible = false;
                        id_territoryassign.Visible = false;
                        a_order_details.Visible = true;
                        id_repsalestype.Visible = true;

                        id_outlet_details_outlets.Visible = true;
                        id_outlet_details_outlets_asm_confirm.Visible = false;
                        id_outlet_details_outlets_sales_confirm.Visible = true;

                        a_kpi_setting.Visible = true;
                        id_asset_details.Visible = true;

                        media_management.Visible = true;

                        id_tradediscount_viwer.Visible = true;
                        id_tradediscount_report.Visible = true;
                        id_tradediscount_summary_report.Visible = true;
                    }
                }

                OriginatorClient childOriginators = new OriginatorClient();
                List<OriginatorDTO> originatorList = childOriginators.GetChildOriginatorsList(args);

                if (originatorList.Count!=0)
                {
                    btnManager.Visible = true;
                }
                else
                {
                    btnManager.Visible = false;
                }

                originatorList.Insert(0, new OriginatorDTO() { Name = "All", UserName = UserSession.Instance.OriginalUserName });

                drop_ChiildOriginators.DataSource = originatorList;
                drop_ChiildOriginators.DataValueField = "UserName";
                drop_ChiildOriginators.DataTextField = "Name";
                drop_ChiildOriginators.DataBind();
                Session[CommonUtility.BDM_LIST] = originatorList;

                if (UserSession.Instance.FilterByUserName != null)
                    drop_ChiildOriginators.SelectedValue = UserSession.Instance.FilterByUserName.Trim();

                //Dispose Client.
                commonClient.Close();
                childOriginators.Close();
            }
        }
        catch (Exception ex)
        {
            //commonUtility.ErrorMaintain(ex);
        }
    }

    public void SetBreadCrumb(string pageName, string navigationUrl, string page)
    {
        string siteLanuage = string.Empty;
        if (!string.IsNullOrWhiteSpace(navigationUrl))
            id_breadcrumb.Add(pageName,  navigationUrl, page);
        else
            id_breadcrumb.Add("","","");
    }

    private void SettingActiveModule()
    {
        try
        {
            string currentUrl = Request.RawUrl.ToString();

            if (!string.IsNullOrWhiteSpace(currentUrl))
            {
                currentUrl = currentUrl.Split('?')[0].ToString();
                string[] urlarray = currentUrl.Split('/');

                var matchurl = urlarray.FirstOrDefault(a => ConfigUtil.CRMModules.Split(',').Any(b => a.IndexOf(b.ToString(), StringComparison.InvariantCultureIgnoreCase) >= 0));

                if (!string.IsNullOrWhiteSpace(matchurl))
                {
                    HtmlGenericControl lictrl = (HtmlGenericControl)FindControl("li_" + matchurl.ToLower().Replace(".aspx", ""));

                    if (lictrl != null)
                    {
                        lictrl.Attributes.Add("class", "active");
                    }
                }
            }
        }
        catch (Exception)
        {

        }


       /* string currentUrl = Request.RawUrl.ToString();

        Regex regex = new Regex(@"(\w+).aspx", RegexOptions.IgnoreCase);
        MatchCollection matches1 = regex.Matches(currentUrl);

        if (matches1.Count > 0)
        {
            string page = matches1[0].Groups[1].Value.Trim();
            if (!string.IsNullOrWhiteSpace(page))
            {
                switch (page.ToLower())
                {
                    case "default":
                        li_home.Attributes.Add("class", "active");
                        break;
                    case "opportunity":
                        li_pipeline.Attributes.Add("class", "active");
                        div_pipeline.Attributes.Add("class", "active_pipeline_icon");
                        break;
                    case "lead_contacts":
                        li_leadcustomer.Attributes.Add("class", "active");
                        break;
                    case "activity_scheduler":
                        li_activityplanne.Attributes.Add("class", "active");
                        break;
                    case "activityanalysis":
                        li_dashboard.Attributes.Add("class", "active");
                        break;
                    case "appointment_entry":
                        li_calendar.Attributes.Add("class", "active");
                        break;
                }
            }
        }*/
    }
    #endregion
}
