﻿using Peercore.CRM.BusinessRules;
using Peercore.CRM.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;


namespace Peercore.CRM.MobileService.MediaImageUpload
{
    public class MTImageUploder
    {
        public bool UploadImages(List<string> postedImages, string repCode, string custCode, string description)
        {
            bool status = false;
            string mediaName = "";
            //string fileExtention = "";
            string folder = "";
            string thumbPath = "";

            try
            {
                int le = postedImages.Count;
                foreach (var bytes in postedImages)
                {
                    string Location = ConfigurationManager.AppSettings["MTImagePath"];

                    folder = Location + "\\ModernTrade\\";

                    if (!Directory.Exists(folder))
                    {
                        Directory.CreateDirectory(folder);
                    }

                    //mediaName = mediaName.Substring(0, mediaName.IndexOf("."));

                    mediaName = custCode + "_" + repCode + "_" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".jpg";
                    string SaveLocation = folder + mediaName;

                    //postedFile.SaveAs(SaveLocation);
                    byte[] imageBytes = Convert.FromBase64String(bytes);
                    File.WriteAllBytes((SaveLocation), imageBytes);
                    //File.WriteAllBytes(HttpContext.Current.Server.MapPath(SaveLocation), bytes);

                    /** Genarate Thubnail  **/
                    thumbPath = copyImage(SaveLocation, mediaName, Location);

                    string imagePath = @"..\MediaFiles\";
                    imagePath = imagePath + (RelativePath(Location, SaveLocation).ToString());
                    string thumbImgPath = @"..\MediaFiles\ModernTradeThumbnails\" + mediaName + ".jpg";

                    SaveMediaDetail(mediaName, imagePath, thumbImgPath, custCode, repCode, description);

                    status = true;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return status;
        }

        private string copyImage(string SaveLocation, string imageName, string location)
        {
            string targetPath = location + "\\ModernTradeThumbnails\\"; //HttpContext.Current.Server.MapPath("~/MediaFiles/ModernTradeThumbnails/");
            string imgName = imageName;
            string imgPath = SaveLocation; //Server.MapPath("~/FILE/") + imgName;
            byte[] bytes = Convert.FromBase64String(GetThumbNail(imgPath).Split(',')[1]);
            System.Drawing.Image image;
            using (MemoryStream ms = new MemoryStream(bytes))
            {
                image = System.Drawing.Image.FromStream(ms);
            }
            //Session["imgName"] = DateTime.Now.ToString();

            if (!Directory.Exists(targetPath))
            {
                Directory.CreateDirectory(targetPath);
            }

            image.Save(targetPath + "\\" + imgName + ".jpg");

            return targetPath = String.Format("{0} {1} .jpg", targetPath, imgName); ;
        }

        //Convert Uploaded file to Thumbnail
        private string GetThumbNail(string url)
        {
            string path = url;
            System.Drawing.Image image = System.Drawing.Image.FromFile(path);
            using (System.Drawing.Image thumbnail = image.GetThumbnailImage(40, 50, new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback), IntPtr.Zero))
            {
                using (MemoryStream memoryStream = new MemoryStream())
                {
                    thumbnail.Save(memoryStream, ImageFormat.Png);
                    Byte[] bytes = new Byte[memoryStream.Length];
                    memoryStream.Position = 0;
                    memoryStream.Read(bytes, 0, (int)bytes.Length);
                    string base64String = Convert.ToBase64String(bytes, 0, bytes.Length);
                    return "data:image/png;base64," + base64String;
                }
            }
        }

        private bool ThumbnailCallback()
        {
            return false;
        }

        public string RelativePath(string absPath, string relTo)
        {
            string[] absDirs = absPath.Split('\\');
            string[] relDirs = relTo.Split('\\');

            // Get the shortest of the two paths
            int len = absDirs.Length < relDirs.Length ? absDirs.Length :
            relDirs.Length;

            // Use to determine where in the loop we exited
            int lastCommonRoot = -1;
            int index;

            // Find common root
            for (index = 0; index < len; index++)
            {
                if (absDirs[index] == relDirs[index]) lastCommonRoot = index;
                else break;
            }

            // If we didn't find a common prefix then throw
            if (lastCommonRoot == -1)
            {
                throw new ArgumentException("Paths do not have a common base");
            }

            // Build up the relative path
            StringBuilder relativePath = new StringBuilder();

            // Add on the ..
            for (index = lastCommonRoot + 1; index < absDirs.Length; index++)
            {
                if (absDirs[index].Length > 0) relativePath.Append("..\\");
            }

            // Add on the folders
            for (index = lastCommonRoot + 1; index < relDirs.Length - 1; index++)
            {
                relativePath.Append(relDirs[index] + "\\");
            }
            relativePath.Append(relDirs[relDirs.Length - 1]);

            return relativePath.ToString();
        }

        public void SaveMediaDetail(string mediaName, string saveLocation, string targetPath, string cust_Code, string repCode, string description)
        {
            try
            {
                UploadMediaModel uploadMediaModel = new UploadMediaModel();

                uploadMediaModel.CustCode = cust_Code;
                uploadMediaModel.Description = description;
                uploadMediaModel.RepCode = repCode;
                uploadMediaModel.Prefix = ConfigurationManager.AppSettings["MOD8"];
                uploadMediaModel.MediaFileName = mediaName;
                uploadMediaModel.FileExtention = Path.GetExtension(mediaName).ToLower(); ;
                uploadMediaModel.MediaFilePath = saveLocation;
                uploadMediaModel.ThumbnailPath = targetPath.ToString();
                uploadMediaModel.CreatedBy = repCode;
                uploadMediaModel.CreatedDate = DateTime.Now;

                MediaBR.Instance.SaveUpdateMediaFiles(uploadMediaModel);

            }
            catch (Exception)
            {

                throw;
            }
        }

    }
}