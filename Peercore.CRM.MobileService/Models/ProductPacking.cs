﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class ProductPacking
    {
        public int PackingID { get; set; }

        public string PackingName { get; set; }
    }
}