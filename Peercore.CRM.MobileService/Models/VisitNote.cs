﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class VisitNote
    {
        public string CustCode { get; set; }

        public string CustName { get; set; }

        public string Notes { get; set; }

        public string VisitDate { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public string RepCode { get; set; }

        public int routeId { get; set; }
    }
}