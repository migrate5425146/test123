﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class DiscountSchemeProductCondition
    {
        public bool IsExceptCondition { get; set; }

        public int? ProductID { get; set; }

        public int? BrandID { get; set; }

        public int? PackingID { get; set; }

        public int? CategoryID { get; set; }

        public int? FlavourID { get; set; }

        public bool? IsHighValueProduct { get; set; }

        public string ConditionType { get; set; }

        public string valueType { get; set; }

        public float value { get; set; }
    }
}