﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class ReturnInvoices
    {
        public int Id { get; set; }

        public string returnno { get; set; }

        public double returntotal { get; set; }

        public string returndate { get; set; }

        public bool canceled { get; set; }

        public int returntype { get; set; }

        public string remarks { get; set; }

        public double latitude { get; set; }

        public double longitude { get; set; }

        public List<ReturnInvoiceLine> ReturnInvoiceLine { get; set; }

        public List<ReturnInvoiceDetails> ReturnInvoiceDetails { get; set; }

        public string RepCode { get; set; }

        public int routeId { get; set; }

        public string routeName { get; set; }
    }

    public class ReturnInvoiceLine
    {
        public int Id { get; set; }

        public int returnid { get; set; }

        public string invoiceno { get; set; }

        public double return_val { get; set; }

        public double return_amt { get; set; }
    }

    public class ReturnInvoiceDetails
    {
        public int Id { get; set; }

        public int returnid { get; set; }

        public string invoiceno { get; set; }

        public int productid { get; set; }

        public double itemprice { get; set; }

        public double qty { get; set; }

        public double total { get; set; }

        public string usable { get; set; }
    }
}