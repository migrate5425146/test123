﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class CheckListType
    {
        public int CheckListTypeID { get; set; }

        public string Name { get; set; }
    }
}