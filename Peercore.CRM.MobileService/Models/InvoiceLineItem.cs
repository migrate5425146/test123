﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class InvoiceLineItem
    {
        public int ProductID { get; set; }

        public bool IsGift { get; set; }

        public double ItemPrice { get; set; }

        public double Qty { get; set; }

        public double Total { get; set; }
        public double QtyCasess { get; set; } // added by rangan

        public double QtyTonnage { get; set; } // added by rangan

    }
}