﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class ProductCategory
    {
        public int CategoryID { get; set; }

        public string CategoryName { get; set; }
    }

    public class ProductCategoryNew
    {
        public string id { get; set; }
        public string name { get; set; }
        public string categoryImage { get; set; }
    }
}