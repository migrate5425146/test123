﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class CheckList
    {
        public List<CheckListType> CheckListTypeCollection { get; set; }
        public List<CheckListMaster> CheckListCollection { get; set; }
        public List<CheckListImage> CheckListImageCollection { get; set; }
        public List<CheckListDetail> CheckListDetailCollection { get; set; }
        public List<ChecklistAllocation> CheckListCustomerCollection { get; set; }
    }
}