﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class CheckListImage
    {
        public int CheckListID { get; set; }
        public string ImageName { get; set; }
        public string Image { get; set; }
    }
}