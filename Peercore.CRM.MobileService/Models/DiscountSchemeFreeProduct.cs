﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class DiscountSchemeFreeProduct
    {
        public int ProductID { get; set; }

        public int Qty { get; set; }
    }
}