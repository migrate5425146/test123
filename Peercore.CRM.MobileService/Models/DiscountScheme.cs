﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class DiscountScheme
    {
        public int SchmeID { get; set; }

        public string SchemeName { get; set; }

        public List<DiscountSchemeDetail> SchemeDetailCollection { get; set; }
    }
}