﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public enum CreditSettlementType
    {
        Cash = 1,

        Cheque = 2
    }
}