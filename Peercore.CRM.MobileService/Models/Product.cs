﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class Product
    {
        public int BrandID { get; set; }

        public int CategoryID { get; set; }

        public int PackingID { get; set; }

        public bool IsHighValue { get; set; }

        public int FlavourID { get; set; }

        public int ProductID { get; set; }

        public string ProductName { get; set; }

        public string ProductCode { get; set; }

        public double Weight { get; set; }

        public string SKU { get; set; }

        public double RetailPrice { get; set; }

        public double ConsumerPrice { get; set; }

        public string ImageUrl { get; set; }

        public bool IsPromotion { get; set; }
        public bool IsHalfQtyItem { get; set; }

        public string Status { get; set; }

        public string SyncDate { get; set; }
    }

    public class ProductNew
    {
        public string id { get; set; }

        public string name { get; set; }

        public string description { get; set; }

        public bool inactive { get; set; }

        public string catalogCode { get; set; }

        public string barcode { get; set; }

        public string standardCode { get; set; }

        public string standardName { get; set; }

        public double unitWeight { get; set; }

        public string categoryId { get; set; }

        public string brandId { get; set; }

        public string taxCode { get; set; }

        public string productImage { get; set; }

        public List<ProductPriceNew> pricing { get; set; }
    }

    public class ProductPriceNew
    {
        public double minimumOrderQuantity { get; set; }

        public string customerReferenceId { get; set; }

        public string customerInternalId { get; set; }

        public double price { get; set; }

    }
}