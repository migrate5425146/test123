﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class StockTotal
    {
        public int ProductID { get; set; }

        public string ProductCode { get; set; }

        public double AllocationStock { get; set; }

        public float CaseConfiguration { get; set; }
        public float TonnageConfiguration { get; set; }
    }
}