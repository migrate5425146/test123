﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class DiscountSchemeCondition
    {
        public double? MinimumQty { get; set; }

        public double? MinimumValue { get; set; }

        public double? MaximumQty { get; set; }

        public double? MaximumValue { get; set; }

        public bool IsForEvery { get; set; }

        public int ConditionId { get; set; }
        
        public double? LineCount { get; set; }

        public List<DiscountSchemeProductCondition> ProductConditionCollection { get; set; }
    }
}