﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class CheckListMaster
    {
        public int CheckListID { get; set; }
        public int CheckListTypeID { get; set; }
        public string Task { get; set; }
        public string AddedOn { get; set; }
        public int Hide { get; set; }
        public string CustomerCode { get; set; }
        public bool Completed { get; set; }
    }
}