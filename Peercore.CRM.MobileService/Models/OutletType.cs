﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class OutletType
    {
        public int OutletTypeId { get; set; }

        public string OutletTypeName { get; set; }
    }
}