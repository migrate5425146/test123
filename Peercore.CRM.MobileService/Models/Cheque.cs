﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class Cheque
    {
        public double ChequeAmount { get; set; }

        public string ChequeNumber { get; set; }

        public string Bank { get; set; }

        public string ChequeDate { get; set; }
    }
}