﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public enum DiscountSchemeDetailType
    {
        Value = 1,

        Percentage = 2,

        Free = 3
    }
}