﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.Models
{
    public class UserLogin
    {
        public int UserID { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string AccessToken { get; set; }
        public string RepCode { get; set; }
        public int InvoiceCount { get; set; }
        public string UserType { get; set; }

        public int routeId { get; set; }
        public string routeName { get; set; }
        //public int territoryId { get; set; }
        //public string territoryName { get; set; }
        //public int areaId { get; set; }
        //public string areaName { get; set; }
        //public int regionId { get; set; }
        //public string regionName { get; set; }

        public int distributorId { get; set; }
        public string distributorname { get; set; }
        public string distributoraddress { get; set; }
        public string distributortel { get; set; }
        //public int asmId { get; set; }
        //public string asmname { get; set; }
        //public int rsmId { get; set; }
        //public string rsmname { get; set; }
        public bool IsTFA { get; set; }
    }

    public class UserRoute
    {
        public string routeName { get; set; }
        public int routeId { get; set; }
        public int territoryId { get; set; }
        public string territoryCode { get; set; }
        public string territoryName { get; set; }
        public int areaId { get; set; }
        public string areaCode { get; set; }
        public string areaName { get; set; }
        public int regionId { get; set; }
        public string regionCode { get; set; }
        public string regionName { get; set; }
        public int distributorId { get; set; }
        public string distributorcode { get; set; }
        public string distributorname { get; set; }
        public string distributoraddress { get; set; }
        public string distributordisplayname { get; set; }
        public string distributortel { get; set; }
        public int asmId { get; set; }
        public string asmCode { get; set; }
        public string asmname { get; set; }
        public int rsmId { get; set; }
        public string rsmCode { get; set; }
        public string rsmname { get; set; }
        public string last_inv_no { get; set; }
        public int last_inv_id { get; set; }
        public string last_order_no { get; set; }
        public int last_order_id { get; set; }
        public int salesType { get; set; }
        public int CanAddOutlet { get; set; }
        public bool IsAttendanceCheckIn { get; set; }
        public bool IsAttendanceCheckOut { get; set; }
        public bool IsGeoFenceEnable { get; set; } = false;
        public bool IsMTUser { get; set; } = false;
        public DynamicReport DashboardParam { get; set; }
    }

    public class Originator
    {
        public string originator { get; set; }
        public string Name { get; set; }
        public string AccessToken { get; set; }
    }

    public class DailyRoute
    {
        public string routeName { get; set; }
    }

    public class RepLocation
    {
        public DateTime DateTime { get; set; }
        public string CurrLat { get; set; }
        public string CurrLong { get; set; }
    }

    public class DynamicReport
    {
        public string reportValue { get; set; }
    }
}