﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Peercore.CRM.MobileService.DTO.Response;
using Peercore.CRM.Entities;
using Peercore.CRM.MobileService.Models;

namespace Peercore.CRM.MobileService.Converters
{
    public class DiscountSchemeConverters
    {
        #region SchemeHeader

        public DiscountSchemeResponse ConvertToDiscountSchemeResponse(SchemeHeaderEntity schemeHeaderEntity)
        {
            DiscountSchemeResponse discountSchemeResponse = new DiscountSchemeResponse();

            DiscountScheme discountScheme = new DiscountScheme();

            try
            {
                if (schemeHeaderEntity != null)
                {
                    discountScheme.SchmeID = schemeHeaderEntity.SchemeHeaderId;
                    discountScheme.SchemeName = string.IsNullOrEmpty(schemeHeaderEntity.SchemeName) ? "" : schemeHeaderEntity.SchemeName.Trim();
                    if (schemeHeaderEntity.SchemeDetailsList != null)
                        discountScheme.SchemeDetailCollection = ConvertToDiscountSchemeDetailList(schemeHeaderEntity.SchemeDetailsList);

                    discountSchemeResponse.Scheme = discountScheme;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return discountSchemeResponse;
        }

        public List<DiscountSchemeResponse> ConvertToDiscountSchemeResponseList(List<SchemeHeaderEntity> lstSchemeHeaderEntity)
        {
            DiscountSchemeResponse discountSchemeResponse = null;
            List<DiscountSchemeResponse> lstSchemeHeaderDTO = new List<DiscountSchemeResponse>();
            foreach (SchemeHeaderEntity item in lstSchemeHeaderEntity)
            {
                discountSchemeResponse = new DiscountSchemeResponse();
                discountSchemeResponse = ConvertToDiscountSchemeResponse(item);
                lstSchemeHeaderDTO.Add(discountSchemeResponse);
            }
            return lstSchemeHeaderDTO;
        }

        #endregion

        #region SchemeDetails

        public DiscountSchemeDetail ConvertToDiscountSchemeDetail(SchemeDetailsEntity schemeDetailsEntity)
        {
            DiscountSchemeDetail discountSchemeDetail = new DiscountSchemeDetail();

            try
            {
                if (schemeDetailsEntity != null)
                {
                    discountSchemeDetail.SchemeDetailType = (DiscountSchemeDetailType)schemeDetailsEntity.DetailsTypeId;
                    discountSchemeDetail.SchemeTypeId = schemeDetailsEntity.SchemeTypeId;
                    discountSchemeDetail.SchemeDetail = schemeDetailsEntity.SchemeDetails.Trim();
                    discountSchemeDetail.DiscountSchemeDetailID = schemeDetailsEntity.SchemeDetailsId;
                    discountSchemeDetail.DiscountPercentage = schemeDetailsEntity.DiscountPercentage;
                    discountSchemeDetail.DiscountValue = schemeDetailsEntity.DiscountValue;
                    discountSchemeDetail.IsRetail = schemeDetailsEntity.IsRetail;
                    discountSchemeDetail.FreeIssueCondition = schemeDetailsEntity.FreeIssueCondition;
                    if (schemeDetailsEntity.OptionLevelList != null)
                        discountSchemeDetail.ConditionCollection = ConvertToDiscountSchemeConditionList(schemeDetailsEntity.OptionLevelList);
                    if (schemeDetailsEntity.FreeProductList != null)
                        discountSchemeDetail.FreeProductCollection = ConvertToDiscountSchemeFreeProductList(schemeDetailsEntity.FreeProductList);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return discountSchemeDetail;
        }

        public List<DiscountSchemeDetail> ConvertToDiscountSchemeDetailList(List<SchemeDetailsEntity> lstSchemeDetailsEntity)
        {
            DiscountSchemeDetail discountSchemeDetail = null;
            List<DiscountSchemeDetail> lstSchemeDetailsDTO = new List<DiscountSchemeDetail>();
            foreach (SchemeDetailsEntity item in lstSchemeDetailsEntity)
            {
                discountSchemeDetail = new DiscountSchemeDetail();
                discountSchemeDetail = ConvertToDiscountSchemeDetail(item);
                lstSchemeDetailsDTO.Add(discountSchemeDetail);
            }
            return lstSchemeDetailsDTO;
        }

        #endregion

        #region OptionLevel

        public DiscountSchemeCondition ConvertToDiscountSchemeCondition(OptionLevelEntity optionLevelEntity)
        {
            DiscountSchemeCondition discountSchemeCondition = new DiscountSchemeCondition();

            try
            {
                if (discountSchemeCondition != null)
                {
                    discountSchemeCondition.MinimumQty = optionLevelEntity.MinQuantity;
                    discountSchemeCondition.MinimumValue = optionLevelEntity.MinValue;
                    discountSchemeCondition.MaximumQty = optionLevelEntity.MaxQuantity;
                    discountSchemeCondition.MaximumValue = optionLevelEntity.MaxValue;
                    discountSchemeCondition.IsForEvery = optionLevelEntity.IsForEvery;
                    discountSchemeCondition.ConditionId = optionLevelEntity.OptionLevelId;
                    discountSchemeCondition.LineCount = optionLevelEntity.LineCount;
                    if (optionLevelEntity.OptionLevelCombinationList != null)
                        discountSchemeCondition.ProductConditionCollection = ConvertToDiscountSchemeProductList(optionLevelEntity.OptionLevelCombinationList);
                }
            }
            catch (Exception)
            {

                throw;
            }
            return discountSchemeCondition;
        }

        public List<DiscountSchemeCondition> ConvertToDiscountSchemeConditionList(List<OptionLevelEntity> lstOptionLevelEntity)
        {
            DiscountSchemeCondition discountSchemeConditionDTO = null;
            List<DiscountSchemeCondition> lstOptionLevelDTO = new List<DiscountSchemeCondition>();
            if (lstOptionLevelEntity != null)
            {
                foreach (OptionLevelEntity item in lstOptionLevelEntity)
                {
                    discountSchemeConditionDTO = new DiscountSchemeCondition();
                    discountSchemeConditionDTO = ConvertToDiscountSchemeCondition(item);
                    lstOptionLevelDTO.Add(discountSchemeConditionDTO);
                }
            }
            return lstOptionLevelDTO;
        }

        #endregion

        #region OptionLevelCombination

        public static DiscountSchemeProductCondition ConvertToDiscountSchemeProductCondition(OptionLevelCombinationEntity optionLevelCombinationEntity)
        {
            DiscountSchemeProductCondition discountSchemeProductCondition = new DiscountSchemeProductCondition();

            try
            {
                if (discountSchemeProductCondition != null)
                {
                    discountSchemeProductCondition.ProductID = optionLevelCombinationEntity.ProductId;
                    discountSchemeProductCondition.CategoryID = optionLevelCombinationEntity.ProductCategoryId;
                    discountSchemeProductCondition.PackingID = optionLevelCombinationEntity.ProductPackingId;
                    discountSchemeProductCondition.BrandID = optionLevelCombinationEntity.BrandId;
                    discountSchemeProductCondition.IsExceptCondition = optionLevelCombinationEntity.IsExceptProduct;
                    discountSchemeProductCondition.IsHighValueProduct = optionLevelCombinationEntity.IsHvp;
                    discountSchemeProductCondition.FlavourID = optionLevelCombinationEntity.FlavorId;
                    discountSchemeProductCondition.ConditionType = optionLevelCombinationEntity.conditionType;
                    discountSchemeProductCondition.valueType = "qty";
                    discountSchemeProductCondition.value = 0;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return discountSchemeProductCondition;
        }

        public static List<DiscountSchemeProductCondition> ConvertToDiscountSchemeProductList(List<OptionLevelCombinationEntity> lstOptionLevelCombinationEntity)
        {
            DiscountSchemeProductCondition discountSchemeProductCondition = null;
            List<DiscountSchemeProductCondition> lstDiscountSchemeProductCondition = new List<DiscountSchemeProductCondition>();
            int index = 1;
            foreach (OptionLevelCombinationEntity item in lstOptionLevelCombinationEntity)
            {
                discountSchemeProductCondition = new DiscountSchemeProductCondition();
                discountSchemeProductCondition = ConvertToDiscountSchemeProductCondition(item);
                lstDiscountSchemeProductCondition.Add(discountSchemeProductCondition);
                index++;
            }
            return lstDiscountSchemeProductCondition;
        }

        #endregion

        #region FreeProduct

        public static DiscountSchemeFreeProduct ConvertToDiscountSchemeFreeProduct(FreeProductEntity objectIn)
        {
            DiscountSchemeFreeProduct objectOut = new DiscountSchemeFreeProduct();
            try
            {
                if (objectIn != null)
                {
                    objectOut.ProductID = objectIn.ProductId;
                    objectOut.Qty = (int)objectIn.Qty;
                }
            }
            catch (Exception)
            {

                throw;
            }
            return objectOut;
        }

        public static List<DiscountSchemeFreeProduct> ConvertToDiscountSchemeFreeProductList(List<FreeProductEntity> listIn)
        {
            List<DiscountSchemeFreeProduct> listOut = new List<DiscountSchemeFreeProduct>();
            try
            {
                foreach (FreeProductEntity itemIn in listIn)
                {
                    listOut.Add(ConvertToDiscountSchemeFreeProduct(itemIn));
                }
            }
            catch (Exception)
            {
                throw;
            }
            return listOut;
        }

        #endregion
    }
}