﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Peercore.CRM.MobileService.DTO.Response;
using Peercore.CRM.Entities;
using Peercore.CRM.MobileService.Models;
using Peercore.CRM.Model;

namespace Peercore.CRM.MobileService.Converters
{
    public class InvoiceConverters
    {
        #region InvoiceHeader

        public InvoiceCollection ConvertToInvoiceCollection(List<InvoiceHeaderEntity> invoiceHeaderList)
        {
            InvoiceCollection invoiceCollection = new InvoiceCollection();
            List<Invoice> lstInvoice = new List<Invoice>();
            Invoice invoice = new Invoice();
            CreditSettlement cr = new CreditSettlement();
            try
            {
                foreach (InvoiceHeaderEntity item in invoiceHeaderList)
                {
                    cr = new CreditSettlement();
                    invoice = new Invoice();
                    invoice.CustomerCode = item.CustCode;
                    invoice.InvoiceTotal = item.GrossTotal;
                    invoice.TotalDiscount = item.Discount;
                    invoice.InvoiceDate = item.IvceDate;
                    invoice.InvoiceNo = item.InvoiceNo;
                    invoice.InvoiceLineItemCollection = ConvertToInvoiceLineItemList(item.InvoiceDetailList);
                    invoice.DiscountSchemeGroupCollection = ConvertToInvoiceDiscountSchemeGroupList(item.InvoiceSchemeGroupList);
                    invoice.RouteName = item.RouteName;
                    invoice.routeId = item.RouteId;
                    invoice.DistributorName = item.DistributorName;
                    invoice.OrderNo = item.OrderNo;
                    //Comment by indunil.due to unnessary data population.
                    invoice.InvoiceCreditSettlementList = ConvertToCreditSettlementCollectionList(item.InvoiceSettlementList);
                    //invoice.DiscountSchemeCollection = ConvertToInvoiceDiscountSchemeList(item.InvoiceSchemeList);
                    invoice.ReturnInvoiceList = ConvertToReturnInvoicesList(item.ReturnInvoiceList);
                  //  invoice.QtyCasess = item.QtyCasess;
                   // invoice.QtyTonnage = item.QtyTonnage;


                    if (item.Status.Equals("D"))
                        invoice.Canceled = true;
                    else
                        invoice.Canceled = false;

                    lstInvoice.Add(invoice);
                }

                invoiceCollection.Invoices = lstInvoice;
            }
            catch (Exception)
            {

                throw;
            }

            return invoiceCollection;
        }

        public InvoiceCollection ConvertToInvoiceCollectionForASM(List<InvoiceHeaderEntity> invoiceHeaderList)
        {
            InvoiceCollection invoiceCollection = new InvoiceCollection();
            List<Invoice> lstInvoice = new List<Invoice>();
            Invoice invoice = new Invoice();
            CreditSettlement cr = new CreditSettlement();
            try
            {
                foreach (InvoiceHeaderEntity item in invoiceHeaderList)
                {
                    cr = new CreditSettlement();
                    invoice = new Invoice();
                    invoice.CustomerCode = item.CustCode;
                    invoice.CustomerName = item.CustomerName;
                    invoice.InvoiceTotal = item.GrossTotal;
                    invoice.TotalDiscount = item.Discount;
                    invoice.InvoiceDate = item.IvceDate;
                    invoice.InvoiceNo = item.InvoiceNo;
                    invoice.RouteName = item.RouteName;
                    invoice.DistributorName = item.DistributorName;
                    invoice.Latitude = item.Latitude;
                    invoice.Longitude = item.Longitude;
                    invoice.OrderNo = item.OrderNo;

                    if (item.Status.Equals("D"))
                        invoice.Canceled = true;
                    else
                        invoice.Canceled = false;

                    lstInvoice.Add(invoice);
                }

                invoiceCollection.Invoices = lstInvoice;
            }
            catch (Exception)
            {

                throw;
            }

            return invoiceCollection;
        }

        public VisitNoteCollection ConvertToVisitNoteCollectionForASM(List<VisitNoteEntity> invoiceHeaderList)
        {
            VisitNoteCollection invoiceCollection = new VisitNoteCollection();
            List<VisitNote> lstInvoice = new List<VisitNote>();
            VisitNote invoice = new VisitNote();
            try
            {
                foreach (VisitNoteEntity item in invoiceHeaderList)
                {
                    invoice = new VisitNote();
                    invoice.CustCode = item.CustomerCode;
                    invoice.CustName = item.CustomerName;
                    invoice.Notes = item.Notes;
                    invoice.VisitDate = item.VisitDate;
                    invoice.Latitude = item.Latitude;
                    invoice.Longitude = item.Longitude;
                    lstInvoice.Add(invoice);
                }

                invoiceCollection.VisitCollection = lstInvoice;
            }
            catch (Exception)
            {

                throw;
            }

            return invoiceCollection;
        }

        public ASMDashBoardResponse ConvertToDailyRepsSalesforASM(List<ASMDashboardEntity> asmDBList)
        {
            ASMDashBoardResponse invoiceCollection = new ASMDashBoardResponse();
            List<ASMDashboardDailyRepSales> lstInvoice = new List<ASMDashboardDailyRepSales>();
            ASMDashboardDailyRepSales invoice = null;
            try
            {
                foreach (ASMDashboardEntity item in asmDBList)
                {
                    invoice = new ASMDashboardDailyRepSales();
                    invoice.RepCode = item.RepCode;
                    invoice.RepName = item.RepName;
                    invoice.RouteName = item.RouteName;
                    invoice.AvailableOutlets = item.AvailableOutlets;
                    invoice.CoveredOutlets = item.PCCount + item.VCCount;
                    invoice.PCCount = item.PCCount;
                    invoice.SalesValue = item.GrossTotal - item.DiscountTotal;
                    invoice.GumSales = item.GumSales;
                    invoice.CandySales = item.CandySales;
                    invoice.JellySales = item.JellySales;
                    invoice.HVPSales = item.HVPSales;
                    invoice.Sales1300 = item.Sales1300;
                    invoice.SalesMLines = item.SalesMLines;
                    invoice.Qty1300 = item.Qty1300;
                    invoice.QtyMLines = item.QtyMLines;
                    invoice.FirstBillTime = item.FirstBillTime;
                    invoice.Echo = item.Echo;
                    lstInvoice.Add(invoice);
                }

                invoiceCollection.DailyRepSales = lstInvoice;
            }
            catch (Exception)
            {

                throw;
            }

            return invoiceCollection;
        }
        
        public ASMDashBoardResponse ConvertToDailyRepsSalesforASM(List<ASMDashboardModel> asmDBList)
        {
            ASMDashBoardResponse invoiceCollection = new ASMDashBoardResponse();
            List<ASMDashboardDailyRepSales> lstInvoice = new List<ASMDashboardDailyRepSales>();
            ASMDashboardDailyRepSales invoice = null;
            try
            {
                foreach (ASMDashboardModel item in asmDBList)
                {
                    invoice = new ASMDashboardDailyRepSales();
                    invoice.RepCode = item.RepCode;
                    invoice.RepName = item.RepName;
                    invoice.RouteName = item.RouteName;
                    invoice.AvailableOutlets = item.AvailableOutlets;
                    invoice.CoveredOutlets = item.PCCount + item.VCCount;
                    invoice.PCCount = item.PCCount;
                    invoice.SalesValue = item.GrossTotal - item.DiscountTotal;
                    invoice.GumSales = item.GumSales;
                    invoice.CandySales = item.CandySales;
                    invoice.JellySales = item.JellySales;
                    invoice.HVPSales = item.HVPSales;
                    invoice.Sales1300 = item.Sales1300;
                    invoice.SalesMLines = item.SalesMLines;
                    invoice.Qty1300 = item.Qty1300;
                    invoice.QtyMLines = item.QtyMLines;
                    invoice.FirstBillTime = item.FirstBillTime;
                    invoice.Echo = item.Echo;
                    lstInvoice.Add(invoice);
                }

                invoiceCollection.DailyRepSales = lstInvoice;
            }
            catch (Exception)
            {

                throw;
            }

            return invoiceCollection;
        }

        public InvoiceHeaderEntity ConvertToInvoiceCollection(Invoice invoice)
        {
            InvoiceHeaderEntity invoiceHeaderEntity = new InvoiceHeaderEntity();

            try
            {
                invoiceHeaderEntity.CustCode = invoice.CustomerCode;
                invoiceHeaderEntity.GrossTotal = invoice.InvoiceTotal;
                invoiceHeaderEntity.Discount = invoice.TotalDiscount;
                invoiceHeaderEntity.IvceDate = invoice.InvoiceDate;
                invoiceHeaderEntity.InvoiceNo = invoice.InvoiceNo;
                invoiceHeaderEntity.Remark = invoice.Remark;
                invoiceHeaderEntity.Longitude = invoice.Longitude;
                invoiceHeaderEntity.Latitude = invoice.Latitude;
                invoiceHeaderEntity.RepCode = invoice.RepCode;
                invoiceHeaderEntity.RouteId = invoice.routeId;

                invoiceHeaderEntity.TerritoryId = invoice.territoryId;
                invoiceHeaderEntity.AreaId = invoice.areaId;
                invoiceHeaderEntity.RegionId = invoice.regionId;
                invoiceHeaderEntity.DistributorId = invoice.distributorId;
                invoiceHeaderEntity.AsmId = invoice.asmId;
                invoiceHeaderEntity.RsmId = invoice.rsmId;
                invoiceHeaderEntity.OrderNo = invoice.OrderNo;

                invoiceHeaderEntity.InvoiceDetailList = ConvertToInvoiceDetailEntityList(invoice.InvoiceLineItemCollection);
                invoiceHeaderEntity.InvoiceSchemeGroupList = ConvertToInvoiceSchemeGroupEntityList(invoice.DiscountSchemeGroupCollection);
                invoiceHeaderEntity.InvoiceSettlement = ConvertToInvoiceSettlementEntity(invoice.InvoiceCreditSettlement);
                //Add By Irosh
                //invoiceHeaderEntity.InvoiceDetailWithDiscountsList = ConvertToInvoiceDetailWithDiscountsEntityList(invoice);

                if (invoice.Canceled)
                    invoiceHeaderEntity.Status = "D";
                else
                    invoiceHeaderEntity.Status = "A";
            }
            catch (Exception)
            {
                throw;
            }
            return invoiceHeaderEntity;
        }
        #endregion

        #region InvoiceDetail

        public List<InvoiceLineItem> ConvertToInvoiceLineItemList(List<InvoiceDetailEntity> invoiceHeaderList)
        {
            List<InvoiceLineItem> InvoiceLineItemList = new List<InvoiceLineItem>();
            InvoiceLineItem invoiceLineItem = new InvoiceLineItem();

            try
            {
                foreach (var item in invoiceHeaderList)
                {
                    invoiceLineItem = new InvoiceLineItem();
                    invoiceLineItem.ProductID = item.ProductId;
                    invoiceLineItem.ItemPrice = item.ItemPrice;
                    invoiceLineItem.Qty = item.Quantity;
                    invoiceLineItem.Total = item.SubTotal;
                    invoiceLineItem.QtyCasess = item.QtyCasess;//added by Rangan
                    invoiceLineItem.QtyTonnage = item.QtyTonnage;//added by Rangan

                    try
                    {
                        invoiceLineItem.IsGift = (item.ItemPrice < 2) ? true : false;
                    }
                    catch { }



                    InvoiceLineItemList.Add(invoiceLineItem);

                }
            }
            catch (Exception)
            {
                throw;
            }
            return InvoiceLineItemList;
        }

        public List<InvoiceDetailEntity> ConvertToInvoiceDetailEntityList(List<InvoiceLineItem> invoiceLineItemList)
        {
            List<InvoiceDetailEntity> InvoiceDetailEntityList = new List<InvoiceDetailEntity>();
            InvoiceDetailEntity invoiceDetailEntity = new InvoiceDetailEntity();

            try
            {
                foreach (var item in invoiceLineItemList)
                {
                    invoiceDetailEntity = new InvoiceDetailEntity();
                    invoiceDetailEntity.ProductId = item.ProductID;
                    invoiceDetailEntity.ItemPrice = item.ItemPrice;
                    invoiceDetailEntity.Quantity = item.Qty;
                    invoiceDetailEntity.SubTotal = item.Total;

                    InvoiceDetailEntityList.Add(invoiceDetailEntity);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return InvoiceDetailEntityList;
        }

        public List<InvoiceDetailWithDiscountsEntity> ConvertToInvoiceDetailWithDiscountsEntityList(Invoice invoice)
        {
            List<InvoiceDetailWithDiscountsEntity> InvoiceDetailEntityList = new List<InvoiceDetailWithDiscountsEntity>();
            InvoiceDetailEntity invoiceDetailEntity = new InvoiceDetailEntity();

            try
            {
                foreach (var item in invoice.InvoiceLineItemCollection)
                {
                    invoiceDetailEntity = new InvoiceDetailEntity();
                    invoiceDetailEntity.ProductId = item.ProductID;
                    invoiceDetailEntity.ItemPrice = item.ItemPrice;
                    invoiceDetailEntity.Quantity = (int)item.Qty;
                    invoiceDetailEntity.SubTotal = item.Total;
                    //InvoiceDetailEntityList.Add(invoiceDetailEntity);
                }
            }
            catch (Exception)
            {
                throw;
            }

            return InvoiceDetailEntityList;
        }

        #endregion

        #region InvoiceScheme

        public List<InvoiceDiscountScheme> ConvertToInvoiceDiscountSchemeList(List<InvoiceSchemeEntity> invoiceSchemeList)
        {
            List<InvoiceDiscountScheme> InvoiceDiscountSchemeList = new List<InvoiceDiscountScheme>();
            InvoiceDiscountScheme invoiceDiscountScheme = new InvoiceDiscountScheme();

            try
            {
                if (invoiceSchemeList != null && invoiceSchemeList.Count != 0)
                {
                    foreach (var item in invoiceSchemeList)
                    {
                        invoiceDiscountScheme = new InvoiceDiscountScheme();
                        invoiceDiscountScheme.ProductID = item.ProductId;
                        invoiceDiscountScheme.UsedQty = item.UsedQuantity;

                        InvoiceDiscountSchemeList.Add(invoiceDiscountScheme);
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
            return InvoiceDiscountSchemeList;
        }

        public List<InvoiceSchemeEntity> ConvertToInvoiceSchemeEntityList(List<InvoiceDiscountScheme> invoiceDiscountSchemeList)
        {
            List<InvoiceSchemeEntity> invoiceSchemeList = new List<InvoiceSchemeEntity>();
            InvoiceSchemeEntity invoiceSchemeEntity = new InvoiceSchemeEntity();

            try
            {
                foreach (var item in invoiceDiscountSchemeList)
                {
                    invoiceSchemeEntity = new InvoiceSchemeEntity();
                    invoiceSchemeEntity.ProductId = item.ProductID;
                    invoiceSchemeEntity.UsedQuantity = item.UsedQty;
                    invoiceSchemeEntity.DiscountVal = item.DiscountVal;
                    invoiceSchemeList.Add(invoiceSchemeEntity);
                }

            }
            catch (Exception)
            {

                throw;
            }
            return invoiceSchemeList;
        }
        #endregion

        #region InvoiceSchemeGroup

        public List<InvoiceDiscountSchemeGroup> ConvertToInvoiceDiscountSchemeGroupList(List<InvoiceSchemeGroupEntity> invoiceSchemeGroupList)
        {
            List<InvoiceDiscountSchemeGroup> InvoiceDiscountSchemeGroupList = new List<InvoiceDiscountSchemeGroup>();
            InvoiceDiscountSchemeGroup invoiceDiscountSchemeGroup = new InvoiceDiscountSchemeGroup();

            try
            {
                foreach (var item in invoiceSchemeGroupList)
                {
                    invoiceDiscountSchemeGroup = new InvoiceDiscountSchemeGroup();
                    invoiceDiscountSchemeGroup.DiscountSchemeGroupID = item.SchemeGroupId;
                    invoiceDiscountSchemeGroup.InvoiceID = item.InvoiceId;
                    invoiceDiscountSchemeGroup.DiscountSchemeDetailID = item.SchemeDetailsId;
                    invoiceDiscountSchemeGroup.DiscountValue = item.DiscountValue;
                    invoiceDiscountSchemeGroup.DiscountSchemeCollection = ConvertToInvoiceDiscountSchemeList(item.InvoiceSchemeList);

                    InvoiceDiscountSchemeGroupList.Add(invoiceDiscountSchemeGroup);
                }

            }
            catch (Exception)
            {

                throw;
            }
            return InvoiceDiscountSchemeGroupList;
        }

        public List<InvoiceSchemeGroupEntity> ConvertToInvoiceSchemeGroupEntityList(List<InvoiceDiscountSchemeGroup> invoiceDiscountSchemeGroupList)
        {
            List<InvoiceSchemeGroupEntity> invoiceSchemeGroupList = new List<InvoiceSchemeGroupEntity>();
            InvoiceSchemeGroupEntity invoiceSchemeGroupEntity = new InvoiceSchemeGroupEntity();

            try
            {
                foreach (var item in invoiceDiscountSchemeGroupList)
                {
                    invoiceSchemeGroupEntity = new InvoiceSchemeGroupEntity();
                    invoiceSchemeGroupEntity.InvoiceId = item.InvoiceID;
                    invoiceSchemeGroupEntity.SchemeGroupId = item.DiscountSchemeGroupID;
                    invoiceSchemeGroupEntity.SchemeDetailsId = item.DiscountSchemeDetailID;
                    invoiceSchemeGroupEntity.DiscountValue = item.DiscountValue;
                    invoiceSchemeGroupEntity.InvoiceSchemeList = ConvertToInvoiceSchemeEntityList(item.DiscountSchemeCollection);

                    invoiceSchemeGroupList.Add(invoiceSchemeGroupEntity);
                }

            }
            catch (Exception)
            {

                throw;
            }
            return invoiceSchemeGroupList;
        }
        #endregion

        #region Credit Settlement

        public CreditSettlement ConvertToCreditSettlementCollection(InvoiceSettlementEntity invoiceSettlement)
        {
            //CreditSettlementCollection invoiceCollection = new CreditSettlementCollection();
            // List<CreditSettlement> lstCreditSettlement = new List<CreditSettlement>();
            CreditSettlement creditSettlement = null;

            try
            {
                //foreach (var item in invoiceHeaderList)
                //{
                //creditSettlement = new CreditSettlement();
                if (invoiceSettlement != null)
                {
                    creditSettlement = new CreditSettlement();
                    //creditSettlement.CustCode = invoiceSettlement.CustCode;
                    //creditSettlement.CreditSettlementNo = invoiceSettlement.SettlementNo;
                    //creditSettlement.ChequeDate = item.ChequeDate;
                    //creditSettlement.Bank = item.ChequeBank;
                    //creditSettlement.ChequeNumber = item.ChequeNo;
                    //creditSettlement.ChequeAmount = item.ChequeAmount;
                    creditSettlement.CashAmount = invoiceSettlement.CashAmount;
                    creditSettlement.CreditAmount = 0;
                    creditSettlement.DueDate = invoiceSettlement.AddedOn;
                    creditSettlement.AddedOn = invoiceSettlement.AddedOn;
                    creditSettlement.InvoiceNo = invoiceSettlement.InvoiceNo;
                    if (invoiceSettlement.InvoiceSettlementChequeList.Count > 0)
                        creditSettlement.IsChequeReceived = true;
                    else
                        creditSettlement.IsChequeReceived = false;
                    //creditSettlement.CreditSettlementType = (CreditSettlementType)invoiceSettlement.SettlementTypeId;
                    creditSettlement.ChequeLineItemCollection = ConvertToInvoiceSettlementChequeList(invoiceSettlement.InvoiceSettlementChequeList);

                    creditSettlement.CreditSettlementDetailsCollection = ConvertToInvoiceSettlementDetailsList(invoiceSettlement.InvoiceSettlementDetailsList);
                }

                //lstCreditSettlement.Add(creditSettlement);
                //}
                //invoiceCollection.CreditSettlements = lstCreditSettlement;

            }
            catch (Exception)
            {

                throw;
            }
            return creditSettlement;
        }

        public List<CreditSettlement> ConvertToCreditSettlementCollectionList(List<InvoiceSettlementEntity> invoiceSettlementList)
        {
            //CreditSettlementCollection invoiceCollection = new CreditSettlementCollection();
            List<CreditSettlement> lstCreditSettlementList = new List<CreditSettlement>();
            CreditSettlement creditSettlement = null;

            try
            {
                if (invoiceSettlementList != null)
                {
                    foreach (var item in invoiceSettlementList)
                    {
                        //creditSettlement = new CreditSettlement();

                        creditSettlement = new CreditSettlement();
                        //creditSettlement.CustCode = invoiceSettlement.CustCode;
                        //creditSettlement.CreditSettlementNo = invoiceSettlement.SettlementNo;
                        //creditSettlement.ChequeDate = item.ChequeDate;
                        //creditSettlement.Bank = item.ChequeBank;
                        //creditSettlement.ChequeNumber = item.ChequeNo;
                        //creditSettlement.ChequeAmount = item.ChequeAmount;
                        creditSettlement.CashAmount = item.CashAmount;
                        creditSettlement.CreditAmount = 0;
                        creditSettlement.DueDate = item.DueDate;
                        creditSettlement.AddedOn = item.AddedOn;
                        creditSettlement.InvoiceNo = item.InvoiceNo;
                        creditSettlement.status_pay = item.status_pay;
                        creditSettlement.remarks = item.remarks;
                        creditSettlement.latitude = item.latitude;
                        creditSettlement.longitude = item.longitude;
                        creditSettlement.InvoiceNo = item.InvoiceNo;
                        creditSettlement.credit_settle_no = item.credit_settle_no;
                        creditSettlement.routeName = item.routeName;
                        creditSettlement.canceled = false;
                        if (item.IsCancel == "N")
                            creditSettlement.canceled = false;
                        else if (item.IsCancel == "Y")
                            creditSettlement.canceled = true;
                        if (item.InvoiceSettlementChequeList.Count > 0)
                            creditSettlement.IsChequeReceived = true;
                        else
                            creditSettlement.IsChequeReceived = false;
                        //creditSettlement.CreditSettlementType = (CreditSettlementType)invoiceSettlement.SettlementTypeId;
                        creditSettlement.ChequeLineItemCollection = ConvertToInvoiceSettlementChequeList(item.InvoiceSettlementChequeList);
                        creditSettlement.CreditSettlementDetailsCollection = ConvertToInvoiceSettlementDetailsList(item.InvoiceSettlementDetailsList);

                        lstCreditSettlementList.Add(creditSettlement);
                    }
                }

                //lstCreditSettlement.Add(creditSettlement);
                //}
                //invoiceCollection.CreditSettlements = lstCreditSettlement;

            }
            catch (Exception)
            {

                throw;
            }
            return lstCreditSettlementList;
        }

        public List<ReturnInvoices> ConvertToReturnInvoicesList(List<ReturnInvoiceEntity> returnInvoicesList)
        {
            //CreditSettlementCollection invoiceCollection = new CreditSettlementCollection();
            List<ReturnInvoices> lstReturnInvoicesList = new List<ReturnInvoices>();
            ReturnInvoices returnInvoices = null;

            try
            {
                if (returnInvoicesList != null)
                {
                    foreach (var item in returnInvoicesList)
                    {
                        returnInvoices = new ReturnInvoices();
                        returnInvoices.Id = item.Id;
                        returnInvoices.returndate = item.returndate;
                        returnInvoices.returnno = item.returnno;
                        returnInvoices.remarks = item.remarks;
                        returnInvoices.latitude = item.latitude;
                        returnInvoices.longitude = item.longitude;
                        returnInvoices.returntotal = item.returntotal;
                        returnInvoices.returntype = item.returntype;
                        returnInvoices.routeName = item.RouteName;
                        returnInvoices.canceled = false;
                        if (item.canceled == "N")
                            returnInvoices.canceled = false;
                        else if (item.canceled == "Y")
                            returnInvoices.canceled = true;
                        returnInvoices.ReturnInvoiceLine = ConvertToReturnInvoiceLineList(item.ReturnInvoiceLineList);
                        returnInvoices.ReturnInvoiceDetails = ConvertToReturnInvoiceDetailsList(item.InvoiceDetailList);

                        lstReturnInvoicesList.Add(returnInvoices);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstReturnInvoicesList;
        }

        public List<ReturnInvoiceDetails> ConvertToReturnInvoiceDetailsList(List<ReturnInvoiceDetailsEntity> returnInvoicesList)
        {
            List<ReturnInvoiceDetails> lstReturnInvoiceDetailsList = new List<ReturnInvoiceDetails>();
            ReturnInvoiceDetails returnInvoiceDetails = null;

            try
            {
                if (returnInvoicesList != null)
                {
                    foreach (var item in returnInvoicesList)
                    {
                        returnInvoiceDetails = new ReturnInvoiceDetails();
                        returnInvoiceDetails.Id = item.Id;
                        returnInvoiceDetails.invoiceno = item.invoiceno;
                        returnInvoiceDetails.itemprice = item.itemprice;
                        returnInvoiceDetails.productid = item.productid;
                        returnInvoiceDetails.qty = item.qty;
                        returnInvoiceDetails.returnid = item.returnid;
                        returnInvoiceDetails.total = item.total;
                        returnInvoiceDetails.usable = item.usable;

                        lstReturnInvoiceDetailsList.Add(returnInvoiceDetails);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstReturnInvoiceDetailsList;
        }

        public List<ReturnInvoiceLine> ConvertToReturnInvoiceLineList(List<ReturnInvoiceLineEntity> returnInvoicesList)
        {
            List<ReturnInvoiceLine> lstReturnInvoiceDetailsList = new List<ReturnInvoiceLine>();
            ReturnInvoiceLine returnInvoiceDetails = null;

            try
            {
                if (returnInvoicesList != null)
                {
                    foreach (var item in returnInvoicesList)
                    {
                        returnInvoiceDetails = new ReturnInvoiceLine();
                        returnInvoiceDetails.Id = item.Id;
                        returnInvoiceDetails.returnid = item.returnid;
                        returnInvoiceDetails.invoiceno = item.invoiceno;
                        returnInvoiceDetails.return_amt = item.return_amt;
                        returnInvoiceDetails.return_val = item.return_val;

                        lstReturnInvoiceDetailsList.Add(returnInvoiceDetails);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return lstReturnInvoiceDetailsList;
        }

        public CreditSettlementNew ConvertToCreditSettlementNewCollection(InvoiceSettlementEntity invoiceSettlement)
        {
            //CreditSettlementCollection invoiceCollection = new CreditSettlementCollection();
            // List<CreditSettlement> lstCreditSettlement = new List<CreditSettlement>();
            CreditSettlementNew creditSettlement = null;

            try
            {
                //foreach (var item in invoiceHeaderList)
                //{
                //creditSettlement = new CreditSettlement();
                if (invoiceSettlement != null)
                {
                    creditSettlement = new CreditSettlementNew();
                    //creditSettlement.CustCode = invoiceSettlement.CustCode;
                    //creditSettlement.CreditSettlementNo = invoiceSettlement.SettlementNo;
                    //creditSettlement.ChequeDate = item.ChequeDate;
                    //creditSettlement.Bank = item.ChequeBank;
                    //creditSettlement.ChequeNumber = item.ChequeNo;
                    //creditSettlement.ChequeAmount = item.ChequeAmount;
                    creditSettlement.CashAmount = invoiceSettlement.CashAmount;
                    //creditSettlement.CreditAmount = 0;
                    //creditSettlement.DueDate = invoiceSettlement.AddedOn;
                    creditSettlement.AddedOn = invoiceSettlement.AddedOn;
                    creditSettlement.InvoiceNo = invoiceSettlement.InvoiceNo;
                    //creditSettlement.CreditSettlementType = (CreditSettlementType)invoiceSettlement.SettlementTypeId;
                    //creditSettlement.ChequeLineItemCollection = ConvertToInvoiceSettlementChequeList(invoiceSettlement.InvoiceSettlementChequeList);
                }

                //lstCreditSettlement.Add(creditSettlement);
                //}
                //invoiceCollection.CreditSettlements = lstCreditSettlement;

            }
            catch (Exception)
            {

                throw;
            }
            return creditSettlement;
        }

        public InvoiceSettlementEntity ConvertToInvoiceSettlementEntity(CreditSettlement creditSettlement)
        {
            InvoiceSettlementEntity invoiceSettlementEntity = new InvoiceSettlementEntity();

            try
            {
                invoiceSettlementEntity.CustCode = creditSettlement.CustCode;
                //invoiceSettlementEntity.SettlementNo = creditSettlement.CreditSettlementNo;
                //invoiceSettlementEntity.ChequeDate = creditSettlement.ChequeDate;
                //invoiceSettlementEntity.ChequeBank = creditSettlement.Bank;
                //invoiceSettlementEntity.ChequeNo = creditSettlement.ChequeNumber;
                invoiceSettlementEntity.ChequeAmount = creditSettlement.ChequeAmount;
                invoiceSettlementEntity.CashAmount = creditSettlement.CashAmount;
                invoiceSettlementEntity.AddedOn = creditSettlement.AddedOn;
                //invoiceSettlementEntity.SettlementTypeId = (int)creditSettlement.CreditSettlementType;
                invoiceSettlementEntity.InvoiceSettlementChequeList = ConvertToInvoiceSettlementChequeEntityList(creditSettlement.ChequeLineItemCollection);
                invoiceSettlementEntity.InvoiceNo = creditSettlement.InvoiceNo;
                invoiceSettlementEntity.CreditAmount = creditSettlement.CreditAmount;
                invoiceSettlementEntity.DueDate = creditSettlement.DueDate;

                invoiceSettlementEntity.status_pay = creditSettlement.status_pay;
                invoiceSettlementEntity.latitude = creditSettlement.latitude;
                invoiceSettlementEntity.longitude = creditSettlement.longitude;
                invoiceSettlementEntity.remarks = creditSettlement.remarks;
                invoiceSettlementEntity.credit_settle_no = creditSettlement.credit_settle_no;
                invoiceSettlementEntity.IsCancel = (creditSettlement.canceled == true) ? "Y" : "N";
                invoiceSettlementEntity.RepCode = creditSettlement.RepCode;
                invoiceSettlementEntity.routeId = creditSettlement.routeId;
                invoiceSettlementEntity.InvoiceSettlementDetailsList = ConvertToInvoiceSettlementDetailsEntityList(creditSettlement.CreditSettlementDetailsCollection);
            }
            catch (Exception)
            {

                throw;
            }
            return invoiceSettlementEntity;
        }

        public List<InvoiceSettlementDetailsEntity> ConvertToInvoiceSettlementDetailsEntityList(List<CreditSettlementDetails> creditSettleList)
        {
            List<InvoiceSettlementDetailsEntity> invoiceSettlementDetailsList = new List<InvoiceSettlementDetailsEntity>();
            InvoiceSettlementDetailsEntity invoiceSettlementDetailsEntity = new InvoiceSettlementDetailsEntity();

            try
            {
                if (creditSettleList != null)
                {
                    foreach (var item in creditSettleList)
                    {
                        invoiceSettlementDetailsEntity = new InvoiceSettlementDetailsEntity();
                        invoiceSettlementDetailsEntity.CreditSettlementId = item.credit_settlement_id;
                        invoiceSettlementDetailsEntity.InvoiceId = item.invoice_id;
                        invoiceSettlementDetailsEntity.PayAmount = item.pay_amt;
                        invoiceSettlementDetailsEntity.InvoiceNo = item.InvoiceNo;

                        invoiceSettlementDetailsList.Add(invoiceSettlementDetailsEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return invoiceSettlementDetailsList;
        }

        #endregion

        #region Cheque

        public List<Cheque> ConvertToInvoiceSettlementChequeList(List<InvoiceSettlementChequeEntity> invoiceSettlementChequeList)
        {
            List<Cheque> chequeList = new List<Cheque>();
            Cheque cheque = new Cheque();

            try
            {
                foreach (var item in invoiceSettlementChequeList)
                {
                    cheque = new Cheque();
                    cheque.Bank = item.ChequeBank;
                    cheque.ChequeAmount = item.ChequeAmount;
                    cheque.ChequeDate = item.ChequeDate;
                    cheque.ChequeNumber = item.ChequeNo;

                    chequeList.Add(cheque);
                }

            }
            catch (Exception)
            {

                throw;
            }
            return chequeList;
        }

        public List<CreditSettlementDetails> ConvertToInvoiceSettlementDetailsList(List<InvoiceSettlementDetailsEntity> invoiceSettlementDetailsList)
        {
            List<CreditSettlementDetails> SettlementDetailsList = new List<CreditSettlementDetails>();
            CreditSettlementDetails SettlementDetails = new CreditSettlementDetails();

            try
            {
                foreach (var item in invoiceSettlementDetailsList)
                {
                    SettlementDetails = new CreditSettlementDetails();
                    SettlementDetails.id = item.Id;
                    SettlementDetails.invoice_id = item.InvoiceId;
                    SettlementDetails.outstand_amt = item.OutstandAmount;
                    SettlementDetails.pay_amt = item.PayAmount;
                    SettlementDetailsList.Add(SettlementDetails);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return SettlementDetailsList;
        }

        public List<InvoiceSettlementChequeEntity> ConvertToInvoiceSettlementChequeEntityList(List<Cheque> chequeList)
        {
            List<InvoiceSettlementChequeEntity> invoiceSettlementChequeList = new List<InvoiceSettlementChequeEntity>();
            InvoiceSettlementChequeEntity invoiceSettlementChequeEntity = new InvoiceSettlementChequeEntity();

            try
            {
                if (chequeList != null)
                {
                    foreach (var item in chequeList)
                    {
                        invoiceSettlementChequeEntity = new InvoiceSettlementChequeEntity();
                        invoiceSettlementChequeEntity.ChequeBank = item.Bank;
                        invoiceSettlementChequeEntity.ChequeAmount = item.ChequeAmount;
                        invoiceSettlementChequeEntity.ChequeDate = item.ChequeDate;
                        invoiceSettlementChequeEntity.ChequeNo = item.ChequeNumber;

                        invoiceSettlementChequeList.Add(invoiceSettlementChequeEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            return invoiceSettlementChequeList;
        }
        #endregion

        #region "Return Invoices"

        public ReturnInvoiceEntity ConvertToReturnInvoiceCollection(ReturnInvoices invoice)
        {
            ReturnInvoiceEntity invoiceHeaderEntity = new ReturnInvoiceEntity();

            try
            {
                invoiceHeaderEntity.Id = invoice.Id;
                invoiceHeaderEntity.latitude = invoice.latitude;
                invoiceHeaderEntity.longitude = invoice.longitude;
                invoiceHeaderEntity.remarks = invoice.remarks;
                invoiceHeaderEntity.returntotal = invoice.returntotal;
                invoiceHeaderEntity.remarks = invoice.remarks;
                invoiceHeaderEntity.returndate = invoice.returndate;
                invoiceHeaderEntity.returnno = invoice.returnno;
                invoiceHeaderEntity.returntype = invoice.returntype;
                invoiceHeaderEntity.canceled = "N";
                invoiceHeaderEntity.RepCode = invoice.RepCode;
                invoiceHeaderEntity.RouteId = invoice.routeId;
                if (invoice.canceled)
                    invoiceHeaderEntity.canceled = "Y";
                invoiceHeaderEntity.ReturnInvoiceLineList = ConvertToReturnInvoiceLineEntityList(invoice.ReturnInvoiceLine);
                invoiceHeaderEntity.InvoiceDetailList = ConvertToReturnInvoiceDetailEntityList(invoice.ReturnInvoiceDetails);
            }
            catch (Exception)
            {
                throw;
            }
            return invoiceHeaderEntity;
        }

        public List<ReturnInvoiceLineEntity> ConvertToReturnInvoiceLineEntityList(List<ReturnInvoiceLine> invoiceLineList)
        {
            List<ReturnInvoiceLineEntity> ReturnInvoiceLineEntityList = new List<ReturnInvoiceLineEntity>();
            ReturnInvoiceLineEntity ReturnInvoicLineEntity = new ReturnInvoiceLineEntity();

            try
            {
                foreach (var item in invoiceLineList)
                {
                    ReturnInvoicLineEntity = new ReturnInvoiceLineEntity();
                    ReturnInvoicLineEntity.Id = item.Id;
                    ReturnInvoicLineEntity.invoiceno = item.invoiceno;
                    ReturnInvoicLineEntity.returnid = item.returnid;
                    ReturnInvoicLineEntity.return_amt = item.return_amt;
                    ReturnInvoicLineEntity.return_val = item.return_val;
                    ReturnInvoiceLineEntityList.Add(ReturnInvoicLineEntity);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return ReturnInvoiceLineEntityList;
        }

        public List<ReturnInvoiceDetailsEntity> ConvertToReturnInvoiceDetailEntityList(List<ReturnInvoiceDetails> invoiceLineItemList)
        {
            List<ReturnInvoiceDetailsEntity> InvoiceDetailEntityList = new List<ReturnInvoiceDetailsEntity>();
            ReturnInvoiceDetailsEntity invoiceDetailEntity = new ReturnInvoiceDetailsEntity();

            try
            {
                foreach (var item in invoiceLineItemList)
                {
                    invoiceDetailEntity = new ReturnInvoiceDetailsEntity();
                    invoiceDetailEntity.Id = item.Id;
                    invoiceDetailEntity.invoiceno = item.invoiceno;
                    invoiceDetailEntity.itemprice = item.itemprice;
                    invoiceDetailEntity.productid = item.productid;
                    invoiceDetailEntity.qty = item.qty;
                    invoiceDetailEntity.returnid = item.returnid;
                    invoiceDetailEntity.total = item.total;
                    invoiceDetailEntity.usable = item.usable;
                    InvoiceDetailEntityList.Add(invoiceDetailEntity);
                }
            }
            catch (Exception)
            {
                throw;
            }
            return InvoiceDetailEntityList;
        }

        #endregion

        #region "ASM Dashboard"

        public RepsTrackingForASM ConvertToRepsTrackingForASM(List<RepsTrackingDetailsEntity> trackList)
        {
            RepsTrackingForASM trackCollection = new RepsTrackingForASM();
            List<RepsTrackingDetails> trackLst = new List<RepsTrackingDetails>();
            RepsTrackingDetails track = new RepsTrackingDetails();

            try
            {
                foreach (RepsTrackingDetailsEntity item in trackList)
                {
                    track = new RepsTrackingDetails();
                    track.CustomerCode = item.CustomerCode;
                    track.InvoiceDate = item.InvoiceDate;
                    track.InvoiceTotal = item.InvoiceTotal;
                    track.TotalDiscount = item.TotalDiscount;
                    track.Latitude = item.Latitude;
                    track.Longitude = item.Longitude;
                    track.PC = item.PC;
                    track.VC = item.VC;
                    track.DistributorName = item.DistributorName;
                    track.RepCode = item.RepCode;
                    track.RepName = item.RepName;
                    trackLst.Add(track);
                }

                trackCollection.RepsTrackingDetails = trackLst;
            }
            catch (Exception)
            {

                throw;
            }

            return trackCollection;
        }

        #endregion

        #region "Reps Report"

        public OutletWiseReportResponse ConvertToOutletWiseReport(List<RepsOutletWiseDetailsEntity> trackList)
        {
            OutletWiseReportResponse trackCollection = new OutletWiseReportResponse();
            List<RepsOutletWiseDetails> trackLst = new List<RepsOutletWiseDetails>();
            List<RepsOutletWiseDetailsEntity> rLst = new List<RepsOutletWiseDetailsEntity>();
            RepsOutletWiseDetails track = new RepsOutletWiseDetails();
            List<RepsOutletWiseProductDetails> trackLstProd = new List<RepsOutletWiseProductDetails>();
            RepsOutletWiseProductDetails trackProd = new RepsOutletWiseProductDetails();

            try
            {
                var groupedCustomerList = trackList
                    .GroupBy(u => u.CustomerCode)
                    .Select(grp => grp.ToList())
                    .ToList();

                foreach (var item in groupedCustomerList)
                {
                    rLst = new List<RepsOutletWiseDetailsEntity>();
                    rLst = item;

                    track = new RepsOutletWiseDetails();
                    track.CustomerCode = rLst[0].CustomerCode;
                    track.CustomerName = rLst[0].CustomerName;
                    track.PaymentTypeCredit = rLst[0].PaymentTypeCredit;
                    track.PaymentTypeCheque = rLst[0].PaymentTypeCheque;
                    track.PaymentTypeCash = rLst[0].PaymentTypeCash;
                    track.TotalDiscounts = rLst[0].DiscountTotal;
                    track.RouteName = rLst[0].RouteName;
                    track.RepName = rLst[0].RepName;

                    trackLstProd = new List<RepsOutletWiseProductDetails>();

                    foreach (RepsOutletWiseDetailsEntity item1 in rLst)
                    {
                        trackProd = new RepsOutletWiseProductDetails();
                        trackProd.ProductCode = item1.ProductCode;
                        trackProd.ProductName = item1.ProductName;
                        trackProd.ProductQty = item1.ProductQty;
                        trackProd.ProductAmount = item1.ProductAmount;
                        trackLstProd.Add(trackProd);
                    }

                    track.ProductDetails = trackLstProd;

                    trackLst.Add(track);
                }

                //foreach (RepsOutletWiseDetailsEntity item in groupedCustomerList)
                //{
                //    track = new RepsOutletWiseDetails();
                //    track.CustomerCode = item.CustomerCode;
                //    track.CustomerName = item.CustomerName;
                //    track.PaymentTypeCredit = item.PaymentTypeCredit;
                //    track.PaymentTypeCheque = item.PaymentTypeCheque;
                //    track.PaymentTypeCash = item.PaymentTypeCash;
                //    trackLst.Add(track);
                //}

                trackCollection.RepsOutletWiseDetails = trackLst;
            }
            catch (Exception)
            {

                throw;
            }

            return trackCollection;
        }

        public SKUWiseReportResponse ConvertToSKUWiseReport(List<RepsSKUMasterEntity> trackList)
        {
            SKUWiseReportResponse trackCollection = new SKUWiseReportResponse();
            List<RepsSKUMasterEntity> rLst = new List<RepsSKUMasterEntity>();
            List<RepsSKUMaster> trackLst = new List<RepsSKUMaster>();
            RepsSKUMaster track = new RepsSKUMaster();

            List<RepsSKUDetails> trackLstProd = new List<RepsSKUDetails>();
            RepsSKUDetails trackProd = new RepsSKUDetails();

            try
            {
                var groupedSKUList = trackList
                    .GroupBy(u => u.RouteName)
                    .Select(grp => grp.ToList())
                    .ToList();

                foreach (var item in groupedSKUList)
                {
                    rLst = new List<RepsSKUMasterEntity>();
                    rLst = item;

                    track = new RepsSKUMaster();
                    track.RepName = rLst[0].RepName;
                    track.RouteName = rLst[0].RouteName;

                    trackLstProd = new List<RepsSKUDetails>();

                    foreach (var item1 in rLst)
                    {
                        trackProd = new RepsSKUDetails();
                        trackProd.BrandName = item1.BrandName;
                        trackProd.CategoryName = item1.CategoryName;
                        trackProd.PackingName = item1.PackingName;
                        trackProd.IsHighValue = item1.IsHighValue;
                        trackProd.FlavourName = item1.FlavourName;
                        trackProd.ProductCode = item1.ProductCode;
                        trackProd.ProductFGCode = item1.ProductFGCode;
                        trackProd.ProductName = item1.ProductName;
                        trackProd.ProductQty = item1.ProductQty;
                        trackProd.ProductValue = item1.ProductValue;
                        trackProd.FreeIssues = item1.FreeIssues;
                        trackProd.DiscountValue = item1.DiscountValue;
                        trackLstProd.Add(trackProd);
                    }

                    track.SKUDetails = trackLstProd;

                    trackLst.Add(track);
                }

                trackCollection.RepsSKUWiseDetails = trackLst;
            }
            catch (Exception)
            {

                throw;
            }

            return trackCollection;
        }

        #endregion
        
    }
}