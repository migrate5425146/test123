﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Peercore.CRM.MobileService.DTO.Response;
using Peercore.CRM.Entities;
using Peercore.CRM.MobileService.Models;
using System.Drawing;
using System.IO;
using System.Configuration;
using System.Xml;
using System.Web.Configuration;

namespace Peercore.CRM.MobileService.Converters
{
    public class CommonConverters
    {
        public string ApplicationPath
        {
            get { return ConfigurationManager.AppSettings["AppPath"]; }
        }

        public string ApkVersion
        {
            get { return ConfigurationManager.AppSettings["ApkVersion"]; }
        }

        public string UpdateApkVersion(string version)
        {
            var configuration = WebConfigurationManager.OpenWebConfiguration("~");
            AppSettingsSection appSettingsSection = (AppSettingsSection)configuration.GetSection("appSettings");
            KeyValueConfigurationCollection settings = appSettingsSection.Settings;
            settings["ApkVersion"].Value = version;
            configuration.Save();

            return version;
        }

        #region UserLogin

        public UserLoginCollectionResponse ConvertToUserLoginCollection(OriginatorEntity originatorEntity)
        {
            UserLoginCollectionResponse userLoginCollectionResponse = new UserLoginCollectionResponse();
            List<UserLogin> UserLoginCollection = new List<UserLogin>();
            UserLogin userLogin = new UserLogin();

            try
            {
                if (originatorEntity != null)
                {
                    userLogin.UserID = originatorEntity.OriginatorId;
                    userLogin.UserName = string.IsNullOrEmpty(originatorEntity.Originator) ? "" : originatorEntity.Originator.Trim();
                    userLogin.Password = string.IsNullOrEmpty(originatorEntity.Password) ? "" :originatorEntity.Password.Trim();
                    userLogin.AccessToken = string.IsNullOrEmpty(originatorEntity.AccessToken) ? "" : originatorEntity.AccessToken.Trim();
                    userLogin.RepCode = string.IsNullOrEmpty(originatorEntity.RepCode) ? "" : originatorEntity.RepCode.Trim();
                    userLogin.InvoiceCount = originatorEntity.InvoiceCount;
                    userLogin.distributorname = string.IsNullOrEmpty(originatorEntity.distributorname) ? "" : originatorEntity.distributorname.Trim();
                    userLogin.distributoraddress = string.IsNullOrEmpty(originatorEntity.distributoraddress) ? "" : originatorEntity.distributoraddress.Trim();
                    userLogin.distributortel = string.IsNullOrEmpty(originatorEntity.distributortel) ? "" : originatorEntity.distributortel.Trim();
                    userLogin.routeId = string.IsNullOrEmpty(originatorEntity.RouteName) ? 0 : originatorEntity.RouteId;
                    userLogin.routeName = string.IsNullOrEmpty(originatorEntity.RouteName) ? "" : originatorEntity.RouteName.Trim();
                    userLogin.UserType = string.IsNullOrEmpty(originatorEntity.DeptString) ? "" : originatorEntity.DeptString.Trim();
                    userLogin.IsTFA = originatorEntity.IsTFAUser;

                    UserLoginCollection.Add(userLogin);

                    userLoginCollectionResponse.UserLoginCollection = UserLoginCollection;
                }
            }
            catch (Exception )
            {

                throw;
            }

            return userLoginCollectionResponse;
        }

        public UserRouteCollectionResponse ConvertToDailyRouteCollection(OriginatorEntity originatorEntity)
        {
            UserRouteCollectionResponse userLoginCollectionResponse = new UserRouteCollectionResponse();
            List<UserRoute> UserLoginCollection = new List<UserRoute>();
            UserRoute userLogin = new UserRoute();

            try
            {
                if (originatorEntity != null)
                {
                    userLogin.routeId = (originatorEntity.RouteId == 0) ? 0 : originatorEntity.RouteId;
                    userLogin.routeName = string.IsNullOrEmpty(originatorEntity.RouteName) ? "" : originatorEntity.RouteName.Trim();

                    userLogin.territoryId = originatorEntity.TerritoryId;
                    userLogin.territoryCode = string.IsNullOrEmpty(originatorEntity.TerritoryCode) ? "" : originatorEntity.TerritoryCode.Trim();
                    userLogin.territoryName = string.IsNullOrEmpty(originatorEntity.TerritoryName) ? "" : originatorEntity.TerritoryName.Trim();
                    
                    userLogin.areaId = originatorEntity.AreaId;
                    userLogin.areaCode = string.IsNullOrEmpty(originatorEntity.AreaCode) ? "" : originatorEntity.AreaCode.Trim();
                    userLogin.areaName = string.IsNullOrEmpty(originatorEntity.AreaName) ? "" : originatorEntity.AreaName.Trim();

                    userLogin.regionId = originatorEntity.RegionId;
                    userLogin.regionCode = string.IsNullOrEmpty(originatorEntity.RegionCode) ? "" : originatorEntity.RegionCode.Trim();
                    userLogin.regionName = string.IsNullOrEmpty(originatorEntity.RegionName) ? "" : originatorEntity.RegionName.Trim();

                    userLogin.distributorId = originatorEntity.distributorId;
                    userLogin.distributorcode = string.IsNullOrEmpty(originatorEntity.distributorcode) ? "" : originatorEntity.distributorcode.Trim();
                    userLogin.distributorname = string.IsNullOrEmpty(originatorEntity.distributorname) ? "" : originatorEntity.distributorname.Trim();
                    userLogin.distributoraddress = string.IsNullOrEmpty(originatorEntity.distributoraddress) ? "" : originatorEntity.distributoraddress.Trim();
                    userLogin.distributortel = string.IsNullOrEmpty(originatorEntity.distributortel) ? "" : originatorEntity.distributortel.Trim();
                    userLogin.distributordisplayname = string.IsNullOrEmpty(originatorEntity.distributorDisplayName) ? "" : originatorEntity.distributorDisplayName.Trim();


                    userLogin.asmId = originatorEntity.AsmId;
                    userLogin.asmname = string.IsNullOrEmpty(originatorEntity.Asmname) ? "" : originatorEntity.Asmname.Trim();

                    userLogin.rsmId = originatorEntity.RsmId;
                    userLogin.rsmCode = string.IsNullOrEmpty(originatorEntity.RsmCode) ? "" : originatorEntity.RsmCode.Trim();
                    userLogin.rsmname = string.IsNullOrEmpty(originatorEntity.Rsmname) ? "" : originatorEntity.Rsmname.Trim();

                    string lastInvNo = string.IsNullOrEmpty(originatorEntity.LastInvoiceNumber) ? "" : originatorEntity.LastInvoiceNumber.Trim();
                    string lastOrderNo = string.IsNullOrEmpty(originatorEntity.LastOrderNumber) ? "" : originatorEntity.LastOrderNumber.Trim();

                    userLogin.salesType = (originatorEntity.SalesType == 0) ? 0 : originatorEntity.SalesType;
                    userLogin.CanAddOutlet = (originatorEntity.CanAddOutlet == 0) ? 0 : originatorEntity.CanAddOutlet;
                    
                    userLogin.IsAttendanceCheckIn = originatorEntity.IsAttendanceCheckIn;
                    userLogin.IsAttendanceCheckOut = originatorEntity.IsAttendanceCheckOut;
                    userLogin.IsGeoFenceEnable = originatorEntity.IsGeoFenceEnable;
                    userLogin.IsMTUser = originatorEntity.IsMTUser;

                    DynamicReport dr = new DynamicReport();
                    dr.reportValue = originatorEntity.DashboardParam.reportValue;
                    userLogin.DashboardParam = dr;

                    try
                    {
                        userLogin.last_inv_no = lastInvNo;
                        string[] splitInv = lastInvNo.Split('/');
                        userLogin.last_inv_id = int.Parse(splitInv[splitInv.Length - 1]);
                    }
                    catch {
                        userLogin.last_inv_id = 0;
                    }

                    try
                    {
                        userLogin.last_order_no = lastOrderNo;
                        string[] splitOrder = lastOrderNo.Split('/');
                        userLogin.last_order_id = int.Parse(splitOrder[splitOrder.Length - 1]);
                    }
                    catch
                    {
                        userLogin.last_order_id = 0;
                    }

                    UserLoginCollection.Add(userLogin);

                    userLoginCollectionResponse.UserRouteCollection = UserLoginCollection;
                }
            }
            catch (Exception)
            {

                throw;
            }

            return userLoginCollectionResponse;
        }

        public DailyRoute ConvertToDailyRoute(OriginatorEntity originatorEntity)
        {
            DailyRoute dailyRoute = new DailyRoute();

            try
            {
                if (originatorEntity != null)
                {
                    dailyRoute.routeName = string.IsNullOrEmpty(originatorEntity.RouteName) ? "" : originatorEntity.RouteName.Trim();
                }
            }
            catch (Exception)
            {
                throw;
            }
            return dailyRoute;
        }

        #endregion

        #region Product
        /*
         */
        public ProductCollectionResponse ConvertToProductCollection(List<ProductEntity> productList)
        {
            ProductCollectionResponse productCollectionResponse = new ProductCollectionResponse();

            List<Product> ProductCollection = new List<Product>();
            Product product = new Product();

            try
            {
                foreach (var item in productList)
                {
                    product = new Product();
                    product.BrandID = item.BrandId;
                    product.CategoryID = item.CategoryId;
                    product.PackingID = item.PackingId;
                    product.IsHighValue = item.IsHighValue;
                    product.FlavourID = item.FlavourId;
                    product.ProductID = item.ProductId;

                    product.ProductName = item.Name.Trim();
                    product.ProductCode = item.Code.Trim();
                    product.Weight = item.Weight;
                    product.SKU = item.Sku.ToString();
                    product.RetailPrice = item.RetailPrice;
                    product.ConsumerPrice = item.OutletPrice;
                    product.IsPromotion = item.IsPromotion;
                    product.IsHalfQtyItem = item.IsHalfQtyItem;
                    product.Status = item.Status;
                    product.SyncDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");

                    string path = ApplicationPath + "image\\no_image.jpg";
                    //if (item.ImageContent != null)
                    //{
                    //    string imagePath = "image\\product\\" + item.Code.Trim()+".png";
                    //    path = ApplicationPath + imagePath;
                    //    byteArrayToImage(item.ImageContent, imagePath);
                    //}
                    //product.ImageUrl = path;
                    ////if (item.ImageContent != null)
                    ////{
                    ////    product.Image = item.ImageContent;
                    ////}
                    ////else
                    ////{
                    ////    string path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath + "image\\no_image.jpg";
                    ////    product.Image = System.IO.File.ReadAllBytes(path);
                    ////}

                    try
                    {
                        if (item.ImageContent != null)
                            product.ImageUrl = Convert.ToBase64String(item.ImageContent);
                        else
                        {
                            product.ImageUrl = "NoImage";
                        }
                    }
                    catch { product.ImageUrl = "NoImage"; }

                    ProductCollection.Add(product);
                }
                productCollectionResponse.ProductCollection = ProductCollection;

            }
            catch (Exception)
            {

                throw;
            }
            return productCollectionResponse;
        }

        public ProductListResponse ConvertToProductList(List<ProductEntity> productList)
        {
            ProductListResponse productCollectionResponse = new ProductListResponse();

            List<ProductNew> ProductCollection = new List<ProductNew>();
            ProductNew product = new ProductNew();
            List<ProductPriceNew> priceList = new List<ProductPriceNew>();
            try
            {
                foreach (var item in productList)
                {
                    product = new ProductNew();
                    product.id = item.ProductId.ToString();
                    product.name = item.Name;
                    product.description = item.Name;
                    //product.inactive = false;
                    product.catalogCode = item.Code;
                    product.barcode = "";
                    product.standardCode = item.Code;
                    product.standardName = item.Name;
                    try
                    {
                        product.unitWeight = item.Weight;
                    }
                    catch { }
                    product.categoryId = item.BrandId.ToString();
                    product.brandId = item.BrandId.ToString();
                    product.taxCode = "";
                    product.productImage = "";

                    try
                    {
                        ProductPriceNew price = new ProductPriceNew();
                        priceList = new List<ProductPriceNew>();

                        price.minimumOrderQuantity = 0;
                        price.customerInternalId = "";
                        price.customerReferenceId = "";
                        price.price = item.RetailPrice;

                        priceList.Add(price);

                        product.pricing = priceList;
                    }
                    catch { }

                    ProductCollection.Add(product);
                }
                productCollectionResponse.products = ProductCollection;

            }
            catch (Exception ex)
            {
                product = new ProductNew();
                product.name = ex.Message;

                ProductCollection.Add(product);

                productCollectionResponse.products = ProductCollection;

                throw;
            }

            return productCollectionResponse;
        }

        public string byteArrayToImage(byte[] byteArrayIn, string imagePath)
        {
            string path = "";
            try
            {
                MemoryStream ms = new MemoryStream(byteArrayIn);
                Image returnImage = Image.FromStream(ms);
                path = System.Web.Hosting.HostingEnvironment.ApplicationPhysicalPath;
                path = path + imagePath;
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
                returnImage.Save(path);

                return path;
            }
            catch (Exception ex)
            {
                throw;
            }
            return path;
        }

        #endregion

        #region Customer

        public CustomerCollectionResponse ConvertToCustomerCollection(List<CustomerEntity> customerList)
        {
            CustomerCollectionResponse customerCollectionResponse = new CustomerCollectionResponse();

            List<Customer> CustomerCollection = new List<Customer>();
            Customer customer = new Customer();

            try
            {
                foreach (var item in customerList)
                {
                    customer = new Customer();
                    customer.CustomerCode = item.CustomerCode.Trim();
                    customer.CustomerName = item.Name.Trim();
                    customer.OutletNo = item.CustomerCode.Trim();
                    customer.Address = string.IsNullOrEmpty(item.Address1) ? "" : item.Address1.Trim();
                    customer.Telephone = string.IsNullOrEmpty(item.Telephone) ? "" : item.Telephone.Trim();
                    customer.CreditLimit = item.CreditLimit;
                    customer.OutstandingCredit = item.OutstandingCredit;
                    customer.OutstandingBalance = item.OutstandingBalance;
                    customer.IsRetail = item.IsRetail;
                    customer.OutletTypeId = item.OutletTypeId;
                    customer.Longitude = item.Longitude;
                    customer.Latitude = item.Latitiude;
                    customer.CreatedOn = DateTime.Now.Date.ToString("yyyy-MM-dd HH:mm");
                    customer.custType = item.custType;
                    customer.dayOrder = item.dayOrder;
                    
                    //string path = ApplicationPath + "image\\no_image.jpg";

                    //customer.HasImage = false;
                    //if (item.ImageContent != null)
                    //{
                    //    string imagePath = "image\\customer\\" + item.CustomerCode.Trim() + ".png";
                    //    path = ApplicationPath + imagePath;
                    //    byteArrayToImage(item.ImageContent, imagePath);
                    //    customer.HasImage = true;
                    //}
                    //customer.ImageUrl = path;

                    try
                    {
                        if (item.ImageContent != null)
                        {
                            customer.ImageUrl = Convert.ToBase64String(item.ImageContent);
                            customer.HasImage = true;
                        }
                        else
                        {
                            customer.ImageUrl = "NoImage";
                            customer.HasImage = false;
                        }
                    }
                    catch { customer.ImageUrl = "NoImage"; customer.HasImage = false; }

                    customer.HasUnlimitedCredit = item.HasUnlimitedCredit;

                    CustomerCollection.Add(customer);
                }
                customerCollectionResponse.CustomerCollection = CustomerCollection;
            }
            catch (Exception)
            {

                throw;
            }
            return customerCollectionResponse;
        }

        public OriginatorCollectionResponse ConvertToRepsCollection(List<OriginatorEntity> repsList)
        {
            OriginatorCollectionResponse customerCollectionResponse = new OriginatorCollectionResponse();

            List<Originator> CustomerCollection = new List<Originator>();
            Originator customer = new Originator();

            try
            {
                foreach (var item in repsList)
                {
                    customer = new Originator();
                    customer.originator = item.Originator.Trim();
                    customer.Name = item.Name.Trim();
                    customer.AccessToken = item.AccessToken.Trim();

                    CustomerCollection.Add(customer);
                }
                customerCollectionResponse.RepsCollection = CustomerCollection;
            }
            catch (Exception)
            {

                throw;
            }
            return customerCollectionResponse;
        }

        public CustomerEntity ConvertToCustomerEntity(Customer customer)
        {
            CustomerEntity customerEntity = new CustomerEntity();


            try
            {
                    customerEntity.CustomerCode = customer.CustomerCode.Trim();
                    customerEntity.Name = customer.CustomerName.Trim();
                    customerEntity.OutletNo = customer.CustomerCode.Trim();
                    customerEntity.Address1 = string.IsNullOrEmpty(customer.Address) ? "" : customer.Address.Trim();
                    customerEntity.Telephone = string.IsNullOrEmpty(customer.Telephone) ? "" : customer.Telephone.Trim();                    
                    customerEntity.IsRetail = customer.IsRetail;
                    customerEntity.OutletTypeId = customer.OutletTypeId;
                    customerEntity.Longitude = customer.Longitude;
                    customerEntity.Latitiude = customer.Latitude;
                    customerEntity.CreatedOn = customer.CreatedOn;
                    customerEntity.RepCode = customer.RepCode;
                    customerEntity.RouteAssignId = customer.routeId;
                    customerEntity.HasUnlimitedCredit = customer.HasUnlimitedCredit;
                    customerEntity.CreditLimit = customer.CreditLimit;
                    customerEntity.ImageContent = Convert.FromBase64String(customer.imageByte);

                    return customerEntity;

            }
            catch (Exception)
            {

                throw;
            }
            return customerEntity;
        }

        public DocumentEntity ConvertToCustomerDocumentEntity(string custCode, byte[] image)
        {
            DocumentEntity documentEntity = new DocumentEntity();

            try
            {
                documentEntity.DocumentID = 0;
                documentEntity.LeadID = 0;
                documentEntity.DocumentName = custCode;
                documentEntity.Path = custCode + ".jpg";
                documentEntity.CustCode = custCode;
                documentEntity.AttachedDate = DateTime.Now;
                documentEntity.AttachedBy = "";
                documentEntity.LastModifiedBy = "";
                documentEntity.LastModifiedDate = DateTime.Now;
                documentEntity.EnduserCode = "";
                documentEntity.Content = image;

                return documentEntity;
            }
            catch (Exception)
            {

                throw;
            }
            return documentEntity;
        }

        #endregion

        #region Brand

        public ProductBrandCollectionResponse ConvertToProductBrandCollection(List<BrandEntity> brandList)
        {
            ProductBrandCollectionResponse productBrandCollectionResponse = new ProductBrandCollectionResponse();

            List<ProductBrand> brandCollection = new List<ProductBrand>();
            ProductBrand productBrand = new ProductBrand();

            try
            {
                foreach (var item in brandList)
                {
                    productBrand = new ProductBrand();
                    productBrand.BrandID = item.BrandId;
                    productBrand.BrandCode = item.BrandCode.Trim();
                    productBrand.BrandName = item.BrandName.Trim();
                    productBrand.Status = item.Status.Trim();
                    productBrand.SyncDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    //productBrand.Image = item.ImageContent;

                    //string path ="";
                    //if (item.ImageContent != null)
                    //{
                    //    string imagePath = "image\\brand\\" + item.BrandId.ToString() + ".png";
                    //    path = ApplicationPath + imagePath;
                    //    byteArrayToImage(item.ImageContent, imagePath);
                    //}
                    //productBrand.ImageUrl = path;

                    try
                    {
                        if (item.ImageContent != null)
                            productBrand.ImageUrl = Convert.ToBase64String(item.ImageContent);
                        else
                        {
                            productBrand.ImageUrl = "NoImage";
                        }
                    }
                    catch { productBrand.ImageUrl = "NoImage"; }

                    brandCollection.Add(productBrand);
                }
                productBrandCollectionResponse.BrandCollection = brandCollection;
            }
            catch (Exception)
            {

                throw;
            }
            return productBrandCollectionResponse;
        }

        #endregion

        #region ProductCategory

        public ProductCategoryCollectionResponse ConvertToCategoryCollection(List<ProductCategoryEntity> productCategoryList)
        {
            ProductCategoryCollectionResponse productCategoryCollectionResponse = new ProductCategoryCollectionResponse();

            List<ProductCategory> categoryCollection = new List<ProductCategory>();
            ProductCategory productCategory = new ProductCategory();

            try
            {
                foreach (var item in productCategoryList)
                {
                    productCategory = new ProductCategory();
                    productCategory.CategoryID = item.Id;
                    productCategory.CategoryName = item.Description.Trim();

                    categoryCollection.Add(productCategory);
                }
                productCategoryCollectionResponse.CategoryCollection = categoryCollection;

            }
            catch (Exception)
            {

                throw;
            }
            return productCategoryCollectionResponse;
        }

        public ProductCategoriesResponse ConvertToCategoryCollectionNew(List<ProductCategoryEntity> productCategoryList)
        {
            ProductCategoriesResponse productCategoryCollectionResponse = new ProductCategoriesResponse();

            List<ProductCategoryNew> categoryCollection = new List<ProductCategoryNew>();
            ProductCategoryNew productCategory = new ProductCategoryNew();

            try
            {
                foreach (var item in productCategoryList)
                {
                    productCategory = new ProductCategoryNew();
                    productCategory.id = item.Id.ToString();
                    productCategory.name = item.Description.Trim();
                    productCategory.categoryImage = "";

                    categoryCollection.Add(productCategory);
                }
                productCategoryCollectionResponse.productCategories = categoryCollection;

            }
            catch (Exception)
            {

                throw;
            }
            return productCategoryCollectionResponse;
        }

        #endregion

        #region ProductPacking

        public ProductPackingCollection ConvertToProductPackingCollection(List<ProductPackingEntity> packingList)
        {
            ProductPackingCollection productPackingCollection = new ProductPackingCollection();

            List<ProductPacking> packingCollection = new List<ProductPacking>();
            ProductPacking packingCategory = new ProductPacking();

            try
            {
                foreach (var item in packingList)
                {
                    packingCategory = new ProductPacking();
                    packingCategory.PackingID = item.PackingId;
                    packingCategory.PackingName = item.PackingName.Trim();

                    packingCollection.Add(packingCategory);
                }
                productPackingCollection.ProductPackings = packingCollection;

            }
            catch (Exception)
            {

                throw;
            }
            return productPackingCollection;
        }

        #endregion

        #region Flavor

        public ProductFlavourCollection ConvertToProductFlavourCollection(List<FlavorEntity> flavorList)
        {
            ProductFlavourCollection productFlavourCollection = new ProductFlavourCollection();

            List<ProductFlavour> flavourCollection = new List<ProductFlavour>();
            ProductFlavour productFlavour = new ProductFlavour();

            try
            {
                foreach (var item in flavorList)
                {
                    productFlavour = new ProductFlavour();
                    productFlavour.FlavourID = item.FlavorId;
                    productFlavour.FlavourName = item.FlavorName.Trim();

                    flavourCollection.Add(productFlavour);
                }
                productFlavourCollection.ProductFlavours = flavourCollection;

            }
            catch (Exception)
            {

                throw;
            }
            return productFlavourCollection;
        }

        #endregion

        #region OutletType

        public OutletTypeCollectionResponse ConvertToOutletTypeCollection(List<OutletTypeEntity> outletTypeList)
        {
            OutletTypeCollectionResponse outletTypeCollectionResponse = new OutletTypeCollectionResponse();

            List<OutletType> OutletTypeCollection = new List<OutletType>();
            OutletType outletType = new OutletType();

            try
            {
                foreach (var item in outletTypeList)
                {
                    outletType = new OutletType();
                    outletType.OutletTypeName = item.OutletTypeName;
                    outletType.OutletTypeId = item.OutletTypeId;

                    OutletTypeCollection.Add(outletType);
                }
                outletTypeCollectionResponse.OutletTypeCollection = OutletTypeCollection;

            }
            catch (Exception)
            {

                throw;
            }
            return outletTypeCollectionResponse;
        }

        #endregion

        #region visitNoteReason

        public VisitNoteReasonCollectionResponse ConvertToVisitNoteReasonCollection(List<VisitNoteReasonEntity> visitNoteReasonList)
        {
            VisitNoteReasonCollectionResponse visitNoteReasonCollectionResponse = new VisitNoteReasonCollectionResponse();

            List<VisitNoteReason> VisitNoteReasonCollection = new List<VisitNoteReason>();
            VisitNoteReason visitNoteReason = new VisitNoteReason();

            try
            {
                foreach (var item in visitNoteReasonList)
                {
                    visitNoteReason = new VisitNoteReason();
                    visitNoteReason.VisitNoteReasonId = item.VisitNoteReasonID;
                    visitNoteReason.Reason = item.Reason;

                    VisitNoteReasonCollection.Add(visitNoteReason);
                }
                visitNoteReasonCollectionResponse.VisitNoteReasonCollection = VisitNoteReasonCollection;

            }
            catch (Exception)
            {

                throw;
            }
            return visitNoteReasonCollectionResponse;
        }

        #endregion

        #region visitNote

        public VisitNoteEntity ConvertToVisitNoteEntity(VisitNote visitNote)
        {
            VisitNoteEntity visitNoteEntity = new VisitNoteEntity();

            try
            {
                visitNoteEntity.CustomerCode = visitNote.CustCode.Trim();
                visitNoteEntity.Notes = visitNote.Notes.Trim();
                visitNoteEntity.VisitDate = visitNote.VisitDate;
                visitNoteEntity.Latitude = visitNote.Latitude;
                visitNoteEntity.Longitude = visitNote.Longitude;
                visitNoteEntity.RepCode = visitNote.RepCode;
                visitNoteEntity.RouteMasterId = visitNote.routeId;
                return visitNoteEntity;
            }
            catch (Exception)
            {
                throw;
            }

            return visitNoteEntity;
        }

        #endregion

        #region TargetVsActual

        public TargetVsActualResponse ConvertToTargetVsActualResponse(List<RepTargetEntity> repTargetEntityList)
        {
            TargetVsActualResponse targetVsActualResponse = new TargetVsActualResponse();
            
            try
            {
                if (repTargetEntityList != null)
                {
                    double totTraget = 0;
                    double totActual = 0;
                    targetVsActualResponse.repTargetList = repTargetEntityList;

                    foreach (RepTargetEntity item in targetVsActualResponse.repTargetList)
                    {
                        totTraget += item.PrimaryTarAmt;
                        totActual += item.ActualAmt;
                    }

                    targetVsActualResponse.Actual = totActual;
                    targetVsActualResponse.Target = totTraget;
                    //targetVsActualResponse.WeekNo = repTargetEntity.WeekNo;
                }
            }
            catch (Exception)
            {
                throw;
            }
            return targetVsActualResponse;
        }

        #endregion

        #region StockTotal

        public StockTotalCollectionResponse ConvertToStockTotalCollection(List<StockTotalEntity> stockTotalList)
        {
            StockTotalCollectionResponse stockTotalCollectionResponse = new StockTotalCollectionResponse();

            List<StockTotal> StockTotalCollection = new List<StockTotal>();
            StockTotal stockTotal = new StockTotal();

            try
            {
                foreach (var item in stockTotalList)
                {
                    stockTotal = new StockTotal();
                    stockTotal.AllocationStock = item.BalanceQty;
                    stockTotal.ProductCode = item.CatalogCode;
                    stockTotal.ProductID = item.CatalogId;
                    stockTotal.CaseConfiguration = item.CaseConfiguration;
                    stockTotal.TonnageConfiguration = item.TonnageConfiguration;


                    StockTotalCollection.Add(stockTotal);
                }
                stockTotalCollectionResponse.StockTotalCollection = StockTotalCollection;

            }
            catch (Exception)
            {

                throw;
            }
            return stockTotalCollectionResponse;
        }

        #endregion

        #region CheckList

        public CheckListResponse ConvertToCheckList(List<ChecklistTypeEntity> checkListTypeEntityList, List<ChecklistMasterEntity> checklistMasterList, List<ChecklistImageEntity> checklistImageList, List<ChecklistDetailEntity> checklistDetailList, List<ChecklistCustomerEntity> checklistCustomers)
        {
            CheckListResponse checkListResponse = new CheckListResponse();
            checkListResponse.CheckList = new CheckList();
            List<CheckListType> CheckListTypeCollection = new List<CheckListType>();
            List<CheckListMaster> CheckListMasterCollection = new List<CheckListMaster>();
            List<CheckListImage> CheckListImageCollection = new List<CheckListImage>();
            List<CheckListDetail> CheckListDetailCollection = new List<CheckListDetail>();
            List<ChecklistAllocation> CheckListCustomerCollection = new List<ChecklistAllocation>();

            try
            {
                if (checkListTypeEntityList != null && checkListTypeEntityList.Count > 0)
                {
                    CheckListType checkListType = new CheckListType();
                    foreach (var item in checkListTypeEntityList)
                    {
                        checkListType = new CheckListType();
                        checkListType.CheckListTypeID = item.Id;
                        checkListType.Name = item.Name;

                        CheckListTypeCollection.Add(checkListType);
                    }
                }
                checkListResponse.CheckList.CheckListTypeCollection = CheckListTypeCollection;

                if (checklistMasterList != null && checklistMasterList.Count > 0)
                {
                    CheckListMaster checkListMaster = new CheckListMaster();
                    foreach (var item in checklistMasterList)
                    {
                        checkListMaster = new CheckListMaster();
                        checkListMaster.CheckListID = item.ChecklistId;
                        checkListMaster.CheckListTypeID = item.ChecklistTypeId;
                        checkListMaster.Task = item.Task;
                        checkListMaster.AddedOn = item.CreatedDate.Value.ToString("yyyy-MM-dd HH:mm");
                        checkListMaster.Hide = item.IsHidden;
                        checkListMaster.CustomerCode = item.CustomerCode;

                        CheckListMasterCollection.Add(checkListMaster);
                    }
                }
                checkListResponse.CheckList.CheckListCollection = CheckListMasterCollection;


                if (checklistImageList != null && checklistImageList.Count > 0)
                {
                    CheckListImage checkLisImage = new CheckListImage();
                    foreach (var item in checklistImageList)
                    {
                        checkLisImage = new CheckListImage();
                        checkLisImage.CheckListID = item.ChecklistId;
                        checkLisImage.ImageName = item.ImageName;
                        if (item.ImageContent != null)
                            checkLisImage.Image = Convert.ToBase64String(item.ImageContent);
                        else
                            checkLisImage.Image = "NoImage";

                        CheckListImageCollection.Add(checkLisImage);
                    }
                }
                checkListResponse.CheckList.CheckListImageCollection = CheckListImageCollection;


                if (checklistDetailList != null && checklistDetailList.Count > 0)
                {
                    CheckListDetail checkLisDetail = new CheckListDetail();
                    foreach (var item in checklistDetailList)
                    {
                        checkLisDetail = new CheckListDetail();
                        checkLisDetail.CheckListID = item.ChecklistId;
                        checkLisDetail.Remarks = item.Remarks;
                        checkLisDetail.CompletedOn = item.CreatedDate.Value.ToString("yyyy-MM-dd HH:mm");
                        checkLisDetail.CustomerCode = item.CustomerCode;

                        CheckListDetailCollection.Add(checkLisDetail);
                    }
                }
                checkListResponse.CheckList.CheckListDetailCollection = CheckListDetailCollection;

                if (checklistCustomers != null && checklistCustomers.Count > 0)
                {
                    ChecklistAllocation checkListAllocation = new ChecklistAllocation();
                    foreach (var item in checklistCustomers)
                    {
                        checkListAllocation = new ChecklistAllocation();
                        checkListAllocation.CheckListID = item.CheckListID;
                        checkListAllocation.CustomerCode = item.CustomerCode;

                        CheckListCustomerCollection.Add(checkListAllocation);
                    }
                }
                checkListResponse.CheckList.CheckListCustomerCollection = CheckListCustomerCollection;
            }
            catch (Exception ex)
            {
                throw;
            }
            return checkListResponse;
        }

        public ChecklistDetailEntity ConvertToChecklistDetailEntity(CheckListDetail checklistDetail)
        {
            ChecklistDetailEntity checklistDetailEntity = ChecklistDetailEntity.CreateObject();

            try
            {
                checklistDetailEntity.ChecklistId = checklistDetail.CheckListID;
                checklistDetailEntity.Remarks = checklistDetail.Remarks.Trim();
                checklistDetailEntity.CreatedDate = Convert.ToDateTime(checklistDetail.CompletedOn);
                checklistDetailEntity.CustomerCode = checklistDetail.CustomerCode.Trim();
                checklistDetailEntity.RepCode = checklistDetail.RepCode.Trim();
                checklistDetailEntity.RouteId = checklistDetail.routeId;
            }
            catch (Exception)
            {
                throw;
            }

            return checklistDetailEntity;
        }

        #endregion

        public RepLocationsResponse ConvertRepLocation(List<RepGeoLocation> repLocationList)
        {
            RepLocationsResponse RepLocationCollectionResponse = new RepLocationsResponse();
            List<RepLocation> RepLocationCollection = new List<RepLocation>();
            RepLocation RepLocation = new RepLocation();

            try
            {
                foreach (var item in repLocationList)
                {
                    RepLocation = new RepLocation();
                    RepLocation.DateTime = item.DateTime;
                    RepLocation.CurrLat = item.CurrLat;
                    RepLocation.CurrLong = item.CurrLong;
                    RepLocationCollection.Add(RepLocation);
                }

                var result = repLocationList.OrderByDescending(t => t.DateTime).First();
                RepLocationCollectionResponse.LastLat = result.CurrLat;
                RepLocationCollectionResponse.LastLong = result.CurrLong;

                RepLocationCollectionResponse.RepsLocations = RepLocationCollection;
            }
            catch (Exception)
            {
                throw;
            }

            return RepLocationCollectionResponse;
        }
    }
}