﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Peercore.CRM.MobileService.DTO.Response;
using Peercore.CRM.MobileService.Models;
using System.ServiceModel.Web;
using Peercore.CRM.Entities;
using Peercore.CRM.MobileService.Converters;
using System.Drawing;
using System.IO;
using System.Runtime.CompilerServices;
using Peercore.CRM.BusinessRules;
using Peercore.CRM.Model;
using Peercore.CRM.MobileService.MediaImageUpload;
using RestSharp;
using System.Configuration;






namespace Peercore.CRM.MobileService
{

    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "MsalesService" in code, svc and config file together.
    public class MsalesService : IMsalesService
    {
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "MobileVersion")]
        public string ApkVersion()
        {
            CommonConverters cc = new CommonConverters();
            string version = string.Empty;

            try
            {
                version = cc.ApkVersion;
            }
            catch (Exception e)
            {
                version = "VersionError";
            }

            return version;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "UpdateMobileVersion?version={version}")]
        public string UpdateApkVersion(string version)
        {
            CommonConverters cc = new CommonConverters();

            try
            {
                version = cc.UpdateApkVersion(version);
            }
            catch (Exception e)
            {
                version = "VersionError";
            }

            return version;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "newpin?accessToken={accessToken}&Latitude={Latitude}&Longitude={Longitude}")]
        public string NewPinCode(string accessToken, string Latitude = "", string Longitude = "")
        {
            string newPin = string.Empty;

            try
            {
                newPin = OriginatorBR.Instance.GetNewPinByAccessToken(accessToken);

                try
                {
                    SessionBR.Instance.CreateTransactionLogMobile(
                        accessToken,
                        DateTime.Now.ToString(),
                        "Select",
                        "NewPin",
                        "Request New PIN ",
                        Latitude.ToString(),
                        Longitude.ToString());
                }
                catch { }
            }
            catch (Exception e)
            {
                newPin = "PinError";
            }

            return newPin;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetUserLoginCollection?accessToken={accessToken}")]
        public UserLoginCollectionResponse GetUserLoginCollection(string accessToken)
        {
            var userLoginCollection = new UserLoginCollectionResponse();

            try
            {
                OriginatorEntity originator = new OriginatorEntity();
                CommonConverters commonConverters = new CommonConverters();
                originator = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);
                userLoginCollection = commonConverters.ConvertToUserLoginCollection(originator);
            }
            catch (Exception e)
            {
                userLoginCollection.IsError = true;
                userLoginCollection.ErrorMessage = e.Message;
            }

            return userLoginCollection;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetChecklistItemCollection?accessToken={accessToken}")]
        public CheckListResponse GetChecklistItemCollection(string accessToken)
        {
            var checkListObj = new CheckListResponse();
            try
            {
                int routeID = 0;
                CommonConverters commonConverters = new CommonConverters();
                List<ChecklistTypeEntity> checklistTypeList = ChecklistBR.Instance.GetChecklistTypeList();
                List<ChecklistMasterEntity> checklistMasterList = ChecklistBR.Instance.GetChecklistMaster(accessToken, ref routeID);
                List<ChecklistCustomerEntity> checklistCustomers = null;
                if (routeID != 0)
                    checklistCustomers = ChecklistBR.Instance.GetDailyChecklistCustomers(routeID);

                string strChecklistId = "";

                if (checklistMasterList != null && checklistMasterList.Count > 0)
                {
                    int i = 1;
                    foreach (ChecklistMasterEntity item in checklistMasterList)
                    {
                        if (i == checklistMasterList.Count)
                            strChecklistId = strChecklistId + item.ChecklistId;
                        else
                            strChecklistId = strChecklistId + (item.ChecklistId + ",");

                        i++;
                    }
                }

                List<ChecklistImageEntity> checklistImageList = new List<ChecklistImageEntity>();
                List<ChecklistDetailEntity> checklistDetailList = new List<ChecklistDetailEntity>();

                if (!string.IsNullOrEmpty(strChecklistId))
                {
                    checklistImageList = ChecklistBR.Instance.GetChecklistImageList(strChecklistId);
                    checklistDetailList = ChecklistBR.Instance.GetChecklistDetailList(strChecklistId);
                }

                checkListObj = commonConverters.ConvertToCheckList(checklistTypeList, checklistMasterList, checklistImageList, checklistDetailList, checklistCustomers);
            }
            catch (Exception e)
            {
                checkListObj.IsError = true;
                checkListObj.ErrorMessage = e.Message;
            }
            return checkListObj;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetDailyRoute?accessToken={accessToken}")]
        public UserRouteCollectionResponse GetDailyRoute(string accessToken)
        {
            var response = new UserRouteCollectionResponse();
            try
            {
                OriginatorEntity originator = new OriginatorEntity();
                CommonConverters commonConverters = new CommonConverters();
                originator = OriginatorBR.Instance.GetOriginatorRouteByAccessToken(accessToken);
                response = commonConverters.ConvertToDailyRouteCollection(originator);
            }
            catch (Exception e)
            {
                response.IsError = true;
                response.ErrorMessage = e.Message;
            }

            return response;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetCustomerCollection?accessToken={accessToken}")]
        public CustomerCollectionResponse GetCustomerCollection(string accessToken)
        {
            var customerCollection = new CustomerCollectionResponse();
            try
            {
                CommonConverters commonConverters = new CommonConverters();
                List<CustomerEntity> customerList = CustomerBR.Instance.GetRouteCustomerByAccessToken(accessToken);
                customerCollection = commonConverters.ConvertToCustomerCollection(customerList);
            }
            catch (Exception e)
            {
                customerCollection.IsError = true;
                customerCollection.ErrorMessage = e.Message;
            }
            return customerCollection;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetRepsCollection?accessToken={accessToken}")]
        public OriginatorCollectionResponse GetRepsCollection(string accessToken)
        {
            var customerCollection = new OriginatorCollectionResponse();

            try
            {
                CommonConverters commonConverters = new CommonConverters();
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);
                if (orgEntity != null)
                {
                    List<OriginatorEntity> repList = OriginatorBR.Instance.GetRepsByASEOriginator(orgEntity.Originator);
                    customerCollection = commonConverters.ConvertToRepsCollection(repList);
                }
                else
                {
                    customerCollection.IsError = true;
                    customerCollection.ErrorMessage = "Not Registered ASM";
                }
            }
            catch (Exception e)
            {
                customerCollection.IsError = true;
                customerCollection.ErrorMessage = e.Message;
            }

            return customerCollection;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetProductCollection?accessToken={accessToken}&syncDate={syncDate}")]
        public ProductCollectionResponse GetProductCollection(string accessToken, string syncDate)
        {
            var productCollection = new ProductCollectionResponse();
            try
            {
                CommonConverters commonConverters = new CommonConverters();
                List<ProductEntity> productList = ProductBR.Instance.GetProductsByAccessToken(accessToken, syncDate);

                productCollection = commonConverters.ConvertToProductCollection(productList);
            }
            catch (Exception e)
            {
                productCollection.IsError = true;
                productCollection.ErrorMessage = e.Message;
            }
            return productCollection;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "ProductList?accessToken={accessToken}&syncDate={syncDate}")]
        public ProductListResponse ProductList(string accessToken, string syncDate)
        {
            var productCollection = new ProductListResponse();
            try
            {
                CommonConverters commonConverters = new CommonConverters();
                List<ProductEntity> productList = ProductBR.Instance.GetProductsByAccessToken(accessToken, syncDate);
                productCollection = commonConverters.ConvertToProductList(productList);
            }
            catch (Exception e)
            {

            }
            return productCollection;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetProductImage?accessToken={accessToken}&productID={productID}")]
        public ProductImageResponse GetProductImage(string accessToken, int productID)
        {
            var productImageResponse = new ProductImageResponse();
            string strImage = string.Empty;

            try
            {
                byte[] image = ProductBR.Instance.GetProductsByAccessTokenAndId(accessToken, productID);
                try
                {
                    if (image != null)
                        strImage = Convert.ToBase64String(image);
                    else
                    {
                        strImage = "NoImage";
                    }
                }
                catch { strImage = "NoImage"; }

                productImageResponse.Image = strImage;
            }
            catch (Exception e)
            {
                productImageResponse.IsError = true;
                productImageResponse.ErrorMessage = "NoImage";
            }

            return productImageResponse;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetProductBrandCollection?accessToken={accessToken}&syncDate={syncDate}")]
        public ProductBrandCollectionResponse GetProductBrandCollection(string accessToken, string syncDate)
        {
            var productBrandCollection = new ProductBrandCollectionResponse();
            try
            {
                CommonConverters commonConverters = new CommonConverters();
                List<BrandEntity> brandList = BrandBR.Instance.GetAllBrands(accessToken, syncDate);
                productBrandCollection = commonConverters.ConvertToProductBrandCollection(brandList);
            }
            catch (Exception e)
            {
                productBrandCollection.IsError = true;
                productBrandCollection.ErrorMessage = e.Message;
            }

            return productBrandCollection;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetProductBrandImageResponse?accessToken={accessToken}&productBrandID={productBrandID}")]
        public ProductBrandImageResponse GetProductBrandImageResponse(string accessToken, int productBrandID)
        {
            var productBrandImageResponse = new ProductBrandImageResponse();
            try
            {
                byte[] image = BrandBR.Instance.GetBrandByAccessTokenAndId(accessToken, productBrandID);
                productBrandImageResponse.Image = image;
            }
            catch (Exception e)
            {
                productBrandImageResponse.IsError = true;
                productBrandImageResponse.ErrorMessage = e.Message;
            }
            return productBrandImageResponse;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetProductCategoryCollection?accessToken={accessToken}")]
        public ProductCategoryCollectionResponse GetProductCategoryCollection(string accessToken)
        {
            var productCategoryCollection = new ProductCategoryCollectionResponse();
            try
            {
                CommonConverters commonConverters = new CommonConverters();
                List<ProductCategoryEntity> productCategoryList = ProductBR.Instance.GetProductCategoriesByAccessToken(accessToken);
                productCategoryCollection = commonConverters.ConvertToCategoryCollection(productCategoryList);
            }
            catch (Exception e)
            {
                productCategoryCollection.IsError = true;
                productCategoryCollection.ErrorMessage = e.Message;
            }
            return productCategoryCollection;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "ProductCategories?accessToken={accessToken}")]
        public ProductCategoriesResponse ProductCategories(string accessToken)
        {
            var productCategoryCollection = new ProductCategoriesResponse();
            try
            {
                CommonConverters commonConverters = new CommonConverters();
                List<ProductCategoryEntity> productCategoryList = ProductBR.Instance.GetProductCategoriesByAccessToken(accessToken);
                productCategoryCollection = commonConverters.ConvertToCategoryCollectionNew(productCategoryList);
            }
            catch (Exception e)
            {

            }
            return productCategoryCollection;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetProductPackingCollection?accessToken={accessToken}")]
        public ProductPackingCollection GetProductPackingCollection(string accessToken)
        {
            var productPackingCollection = new ProductPackingCollection();
            try
            {
                CommonConverters commonConverters = new CommonConverters();
                List<ProductPackingEntity> packingList = ProductPackingBR.Instance.GetProductPackingByAccessToken(accessToken);
                productPackingCollection = commonConverters.ConvertToProductPackingCollection(packingList);
            }
            catch (Exception e)
            {
                productPackingCollection.IsError = true;
                productPackingCollection.ErrorMessage = e.Message;
            }
            return productPackingCollection;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetProductFlavourCollection?accessToken={accessToken}")]
        public ProductFlavourCollection GetProductFlavourCollection(string accessToken)
        {
            var productFlavourCollection = new ProductFlavourCollection();
            try
            {
                CommonConverters commonConverters = new CommonConverters();
                List<FlavorEntity> flavorList = FlavorBR.Instance.GetFlavorByAccessToken(accessToken);
                productFlavourCollection = commonConverters.ConvertToProductFlavourCollection(flavorList);
            }
            catch (Exception e)
            {
                productFlavourCollection.IsError = true;
                productFlavourCollection.ErrorMessage = e.Message;
            }
            return productFlavourCollection;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetInvoiceCollection?accessToken={accessToken}")]
        public InvoiceCollection GetInvoiceCollection(string accessToken)
        {
            var invoiceCollection = new InvoiceCollection();
            try
            {
                InvoiceConverters invoiceConverters = new InvoiceConverters();
                List<InvoiceHeaderEntity> invoiceHeaderList = new List<InvoiceHeaderEntity>();
                invoiceHeaderList = InvoiceHeaderBR.Instance.GetInvoicesByAccessToken(accessToken);
                invoiceCollection = invoiceConverters.ConvertToInvoiceCollection(invoiceHeaderList);
            }
            catch (Exception e)
            {
                invoiceCollection.IsError = true;
                invoiceCollection.ErrorMessage = e.Message;
            }

            return invoiceCollection;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetInvoiceCollectionforASM?accessToken={accessToken}&date={date}")]
        public InvoiceCollection GetInvoiceCollectionforASM(string accessToken, string date)
        {
            var invoiceCollection = new InvoiceCollection();
            try
            {
                InvoiceConverters invoiceConverters = new InvoiceConverters();
                List<InvoiceHeaderEntity> invoiceHeaderList = new List<InvoiceHeaderEntity>();
                invoiceHeaderList = InvoiceHeaderBR.Instance.GetInvoicesByAccessTokenforASM(accessToken, date);
                invoiceCollection = invoiceConverters.ConvertToInvoiceCollectionForASM(invoiceHeaderList);
            }
            catch (Exception e)
            {
                invoiceCollection.IsError = true;
                invoiceCollection.ErrorMessage = e.Message;
            }

            return invoiceCollection;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetRepsTrackingForASM?accessToken={accessToken}&date={date}")]
        public RepsTrackingForASM GetRepsTrackingForASM(string accessToken, string date)
        {
            var trackCollection = new RepsTrackingForASM();
            try
            {
                InvoiceConverters invoiceConverters = new InvoiceConverters();
                List<RepsTrackingDetailsEntity> invoiceHeaderList = new List<RepsTrackingDetailsEntity>();
                invoiceHeaderList = InvoiceHeaderBR.Instance.RepsTrackingDetailsforASM(accessToken, date);
                trackCollection = invoiceConverters.ConvertToRepsTrackingForASM(invoiceHeaderList);
            }
            catch (Exception e)
            {
                trackCollection.IsError = true;
                trackCollection.ErrorMessage = e.Message;
            }

            return trackCollection;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetVisitNoteCollectionforASM?accessToken={accessToken}&date={date}")]
        public VisitNoteCollection GetVisitNoteCollectionforASM(string accessToken, string date)
        {
            var invoiceCollection = new VisitNoteCollection();
            try
            {
                InvoiceConverters invoiceConverters = new InvoiceConverters();
                List<VisitNoteEntity> invoiceHeaderList = new List<VisitNoteEntity>();
                invoiceHeaderList = VisitNoteBR.Instance.GetVisitNotesByAccessTokenforASM(accessToken, date);
                invoiceCollection = invoiceConverters.ConvertToVisitNoteCollectionForASM(invoiceHeaderList);
            }
            catch (Exception e)
            {
                invoiceCollection.IsError = true;
                invoiceCollection.ErrorMessage = e.Message;
            }
            return invoiceCollection;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetDailyRepsSalesforASMOld?accessToken={accessToken}&date={date}")]
        public ASMDashBoardResponse GetDailyRepsSalesforASM(string accessToken, string date)
        {
            var salesCollection = new ASMDashBoardResponse();
            try
            {
                InvoiceConverters invoiceConverters = new InvoiceConverters();
                List<ASMDashboardEntity> salesListList = new List<ASMDashboardEntity>();
                salesListList = ASMDashBoardBR.Instance.GetDailyRepsSalesforASM(accessToken, date);
                salesCollection = invoiceConverters.ConvertToDailyRepsSalesforASM(salesListList);
            }
            catch (Exception e)
            {
                salesCollection.IsError = true;
                salesCollection.ErrorMessage = e.Message;
            }
            return salesCollection;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetDailyRepsSalesforASM?accessToken={accessToken}&date={date}")]
        public ASMDashBoardResponse GetDailyRepsSalesforASMNew(string accessToken, string date)
        {
            var salesCollection = new ASMDashBoardResponse();
            try
            {
                InvoiceConverters invoiceConverters = new InvoiceConverters();
                List<ASMDashboardModel> salesListList = new List<ASMDashboardModel>();
                salesListList = ASMDashBoardBR.Instance.GetDailyRepsSalesforASMNew(accessToken, date);
                salesCollection = invoiceConverters.ConvertToDailyRepsSalesforASM(salesListList);
            }
            catch (Exception e)
            {
                salesCollection.IsError = true;
                salesCollection.ErrorMessage = e.Message;
            }
            return salesCollection;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetVisitNoteCollection?accessToken={accessToken}&date={date}")]
        public VisitNoteCollection GetVisitNoteCollection(string accessToken, string date)
        {
            var invoiceCollection = new VisitNoteCollection();

            try
            {
                InvoiceConverters invoiceConverters = new InvoiceConverters();
                List<VisitNoteEntity> invoiceHeaderList = new List<VisitNoteEntity>();
                invoiceHeaderList = VisitNoteBR.Instance.GetVisitNotesByAccessToken(accessToken);
                invoiceCollection = invoiceConverters.ConvertToVisitNoteCollectionForASM(invoiceHeaderList);
            }
            catch (Exception e)
            {
                invoiceCollection.IsError = true;
                invoiceCollection.ErrorMessage = e.Message;
            }
            return invoiceCollection;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "AddInvoice")]
        public NewInvoiceResponse AddInvoice(string accessToken, Invoice newInvoice)
        {
            var newInvoiceResponse = new NewInvoiceResponse();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    InvoiceConverters invoiceConverters = new InvoiceConverters();

                    if (InvoiceHeaderBR.Instance.InsertInvoice(accessToken, invoiceConverters.ConvertToInvoiceCollection(newInvoice)))
                    {
                        newInvoiceResponse.IsError = false;
                        newInvoiceResponse.ErrorMessage = "Success";

                        try
                        {
                            if (newInvoice.Canceled == true)
                            {
                                SessionBR.Instance.CreateTransactionLogMobile(
                                    accessToken,
                                    DateTime.Now.ToString(),
                                    "Delete",
                                    "Invoice",
                                    "Delete invoice " + newInvoice.InvoiceNo,
                                    newInvoice.Latitude.ToString(),
                                    newInvoice.Longitude.ToString());
                            }
                            else
                            {
                                SessionBR.Instance.CreateTransactionLogMobile(
                                    accessToken,
                                    DateTime.Now.ToString(),
                                    "Insert",
                                    "Invoice",
                                    "Add invoice " + newInvoice.InvoiceNo,
                                    newInvoice.Latitude.ToString(),
                                    newInvoice.Longitude.ToString());
                            }
                        }
                        catch { }
                    }
                    else
                    {
                        newInvoiceResponse.IsError = true;
                        newInvoiceResponse.ErrorMessage = "Request Object Error";
                    }
                }
                else
                {
                    newInvoiceResponse.IsError = true;
                    newInvoiceResponse.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                newInvoiceResponse.IsError = true;
                newInvoiceResponse.ErrorMessage = e.Message;
            }

            return newInvoiceResponse;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "InsertCreditSettlement")]
        public NewCreditSettlementResponse InsertCreditSettlement(string accessToken, CreditSettlement creditSettlement)
        {
            var newCreditSettlementResponse = new NewCreditSettlementResponse();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    InvoiceConverters invoiceConverters = new InvoiceConverters();
                    if (InvoiceHeaderBR.Instance.InsertCreditSettlement(accessToken, invoiceConverters.ConvertToInvoiceSettlementEntity(creditSettlement)))
                    {
                        newCreditSettlementResponse.IsError = false;
                        newCreditSettlementResponse.ErrorMessage = "Success";

                        try
                        {
                            SessionBR.Instance.CreateTransactionLogMobile(
                                accessToken,
                                DateTime.Now.ToString(),
                                "Insert",
                                "CreditSettlement",
                                "Insert Credit Settlement " + creditSettlement.credit_settle_no,
                                creditSettlement.latitude.ToString(),
                                creditSettlement.longitude.ToString());
                        }
                        catch { }
                    }
                    else
                    {
                        newCreditSettlementResponse.IsError = true;
                        newCreditSettlementResponse.ErrorMessage = "Request Object Error";
                    }
                }
                else
                {
                    newCreditSettlementResponse.IsError = true;
                    newCreditSettlementResponse.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                newCreditSettlementResponse.IsError = true;
                newCreditSettlementResponse.ErrorMessage = e.Message;
            }

            return newCreditSettlementResponse;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetDiscountSchemeResponse?accessToken={accessToken}")]
        public DiscountSchemeResponse GetDiscountSchemeResponse(string accessToken)
        {
            var discountSchemeResponse = new DiscountSchemeResponse();

            try
            {
                DiscountSchemeConverters discountSchemeConverter = new DiscountSchemeConverters();
                SchemeHeaderEntity schemeHeader = new SchemeHeaderEntity();
                schemeHeader = SchemeHeaderBR.Instance.GetSchemeHeaderByAccessToken(accessToken);
                discountSchemeResponse = discountSchemeConverter.ConvertToDiscountSchemeResponse(schemeHeader);
            }
            catch (Exception e)
            {
                discountSchemeResponse.IsError = true;
                discountSchemeResponse.ErrorMessage = e.Message;
            }

            return discountSchemeResponse;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetDiscountSchemeForPreSalesResponse?accessToken={accessToken}")]
        public DiscountSchemeResponse GetDiscountSchemeForPreSalesResponse(string accessToken)
        {
            var discountSchemeResponse = new DiscountSchemeResponse();

            try
            {
                DiscountSchemeConverters discountSchemeConverter = new DiscountSchemeConverters();
                SchemeHeaderEntity schemeHeader = new SchemeHeaderEntity();
                schemeHeader = SchemeHeaderBR.Instance.GetDiscountSchemeForPreSalesResponse(accessToken);
                discountSchemeResponse = discountSchemeConverter.ConvertToDiscountSchemeResponse(schemeHeader);
            }
            catch (Exception e)
            {
                discountSchemeResponse.IsError = true;
                discountSchemeResponse.ErrorMessage = e.Message;
            }

            return discountSchemeResponse;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetOutletWiseReportforRep?accessToken={accessToken}&date={date}")]
        public OutletWiseReportResponse GetOutletWiseReportforRep(string accessToken, string date)
        {
            var outletwiserepResponse = new OutletWiseReportResponse();

            try
            {
                InvoiceConverters invoiceConverter = new InvoiceConverters();
                List<RepsOutletWiseDetailsEntity> schemeHeader = new List<RepsOutletWiseDetailsEntity>();
                schemeHeader = InvoiceHeaderBR.Instance.GetRepsOutletWiseDetailsByAccessToken(accessToken, date);
                outletwiserepResponse = invoiceConverter.ConvertToOutletWiseReport(schemeHeader);
            }
            catch (Exception e)
            {
                outletwiserepResponse.IsError = true;
                outletwiserepResponse.ErrorMessage = e.Message;
            }

            return outletwiserepResponse;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetOutletWiseOrderReportforRep?accessToken={accessToken}&date={date}")]
        public OutletWiseReportResponse GetOutletWiseOrderReportforRep(string accessToken, string date)
        {
            var outletwiserepResponse = new OutletWiseReportResponse();

            try
            {
                InvoiceConverters invoiceConverter = new InvoiceConverters();
                List<RepsOutletWiseDetailsEntity> schemeHeader = new List<RepsOutletWiseDetailsEntity>();
                schemeHeader = OrderHeaderBR.Instance.GetRepsOutletWiseOrderDetailsByAccessToken(accessToken, date);
                outletwiserepResponse = invoiceConverter.ConvertToOutletWiseReport(schemeHeader);
            }
            catch (Exception e)
            {
                outletwiserepResponse.IsError = true;
                outletwiserepResponse.ErrorMessage = e.Message;
            }

            return outletwiserepResponse;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetSKUWiseReportforRep?accessToken={accessToken}&date={date}")]
        public SKUWiseReportResponse GetSKUWiseReportforRep(string accessToken, string date)
        {
            var skuwiserepResponse = new SKUWiseReportResponse();

            try
            {
                InvoiceConverters invoiceConverter = new InvoiceConverters();
                List<RepsSKUMasterEntity> skuwiserepList = new List<RepsSKUMasterEntity>();
                skuwiserepList = InvoiceHeaderBR.Instance.GetRepsSKUWiseDetailsByAccessToken(accessToken, date);
                skuwiserepResponse = invoiceConverter.ConvertToSKUWiseReport(skuwiserepList);
            }
            catch (Exception e)
            {
                skuwiserepResponse.IsError = true;
                skuwiserepResponse.ErrorMessage = e.Message;
            }

            return skuwiserepResponse;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetSKUWiseOrderReportforRep?accessToken={accessToken}&date={date}")]
        public SKUWiseReportResponse GetSKUWiseOrderReportforRep(string accessToken, string date)
        {
            var skuwiserepResponse = new SKUWiseReportResponse();

            try
            {
                InvoiceConverters invoiceConverter = new InvoiceConverters();
                List<RepsSKUMasterEntity> skuwiserepList = new List<RepsSKUMasterEntity>();
                skuwiserepList = OrderHeaderBR.Instance.GetRepsSKUWiseOrderDetailsByAccessToken(accessToken, date);
                skuwiserepResponse = invoiceConverter.ConvertToSKUWiseReport(skuwiserepList);
            }
            catch (Exception e)
            {
                skuwiserepResponse.IsError = true;
                skuwiserepResponse.ErrorMessage = e.Message;
            }

            return skuwiserepResponse;
        }

        //[WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetCreditSettlementCollection?accessToken={accessToken}")]
        //public CreditSettlementCollection GetCreditSettlementCollection(string accessToken)
        //{
        //    var creditSettlementCollection = new CreditSettlementCollection();
        //    try
        //    {
        //        CommonConverters commonConverters = new CommonConverters();
        //        List<InvoiceSettlementEntity> invoiceSettlementList = new List<InvoiceSettlementEntity>();
        //        invoiceSettlementList = InvoiceHeaderBR.Instance.GetInvoiceSettlementByAccessToken(accessToken);
        //        creditSettlementCollection = commonConverters.ConvertToCreditSettlementCollection(invoiceSettlementList);
        //    }
        //    catch (Exception e)
        //    {
        //        creditSettlementCollection.IsError = true;
        //        creditSettlementCollection.ErrorMessage = e.Message;
        //    }
        //    return creditSettlementCollection;
        //}

        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "AddCreditSettlement")]
        //public NewCreditSettlementResponse AddCreditSettlement(string accessToken, CreditSettlement newCreditSettlement)
        //{
        //    var newCreditSettlementResponse = new NewCreditSettlementResponse();
        //    try
        //    {
        //        CommonConverters commonConverters = new CommonConverters();
        //        InvoiceHeaderBR.Instance.InvoiceSettlementSave(accessToken, commonConverters.ConvertToInvoiceSettlementEntity(newCreditSettlement));
        //    }
        //    catch (Exception e)
        //    {
        //        newCreditSettlementResponse.IsError = true;
        //        newCreditSettlementResponse.ErrorMessage = e.Message;
        //    }
        //    return newCreditSettlementResponse;
        //}

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "DeleteInvoiceHeader")]
        public InvoiceResponse DeleteInvoiceHeader(string accessToken, string invoiceNo, string Latitude = "", string Longitude = "")
        {
            var invoiceResponse = new InvoiceResponse();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    InvoiceConverters invoiceConverters = new InvoiceConverters();
                    InvoiceHeaderBR.Instance.DeleteInvoiceHeader(accessToken, invoiceNo);
                    invoiceResponse.ErrorMessage = "Success";

                    try
                    {
                        SessionBR.Instance.CreateTransactionLogMobile(
                            accessToken,
                            DateTime.Now.ToString(),
                            "Delete",
                            "Invoice",
                            "Delete Invoice no " + invoiceNo,
                            Latitude.ToString(),
                            Longitude.ToString());
                    }
                    catch { }
                }
                else
                {
                    invoiceResponse.IsError = true;
                    invoiceResponse.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                invoiceResponse.IsError = true;
                invoiceResponse.ErrorMessage = e.Message;
            }

            return invoiceResponse;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "DeleteOrderHeader")]
        public OrderResponse DeleteOrderHeader(string accessToken, string orderNo, string Latitude = "", string Longitude = "")
        {
            var ordereResponse = new OrderResponse();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    OrderHeaderBR.Instance.DeleteOrderHeader(orgEntity, accessToken, orderNo);
                    ordereResponse.ErrorMessage = "Success";

                    try
                    {
                        SessionBR.Instance.CreateTransactionLogMobile(
                            accessToken,
                            DateTime.Now.ToString(),
                            "Delete",
                            "Invoice",
                            "Delete order no " + orderNo,
                            Latitude.ToString(),
                            Longitude.ToString());
                    }
                    catch { }
                }
                else
                {
                    ordereResponse.IsError = true;
                    ordereResponse.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                ordereResponse.IsError = true;
                ordereResponse.ErrorMessage = e.Message;
            }

            return ordereResponse;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "InsertRepLocation")]
        public void InsertRepLocation(string accessToken, string CurrLat, string CurrLong)
        {

            try
            {
                SessionBR.Instance.CreateLog(accessToken, "Insert Rep Location : ", "Open");
            }
            catch { }

            string status = "Error";

            var invoiceResponse = new InvoiceResponse();
            try
            {
                OriginatorBR.Instance.InsertRepLocation(accessToken, CurrLat, CurrLong);
                status = "Success";
            }
            catch (Exception e)
            {
                status = "Error";
            }

            try
            {
                SessionBR.Instance.CreateLog(accessToken, "Insert Rep Location : ", status);
            }
            catch { }
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetRepLocations?accessToken={accessToken}")]
        public RepLocationsResponse GetRepLocations(string accessToken)
        {
            var repLocations = new RepLocationsResponse();
            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    CommonConverters commonConverters = new CommonConverters();
                    List<RepGeoLocation> repLocation = OriginatorBR.Instance.GetRepLocationsByOriginator(orgEntity.Originator);
                    repLocations = commonConverters.ConvertRepLocation(repLocation);
                }
            }
            catch (Exception e)
            {
                repLocations.IsError = true;
                repLocations.ErrorMessage = e.Message;
            }
            return repLocations;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetOutletTypeCollection?accessToken={accessToken}")]
        public OutletTypeCollectionResponse GetOutletTypeCollection(string accessToken)
        {
            var outletTypeCollection = new OutletTypeCollectionResponse();
            try
            {
                CommonConverters commonConverters = new CommonConverters();
                List<OutletTypeEntity> outletTypeList = OutletTypeBR.Instance.GetAllOutletTypeByAccessToken(accessToken);
                outletTypeCollection = commonConverters.ConvertToOutletTypeCollection(outletTypeList);
            }
            catch (Exception e)
            {
                outletTypeCollection.IsError = true;
                outletTypeCollection.ErrorMessage = e.Message;
            }
            return outletTypeCollection;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "AddCustomer")]
        public NewCustomerResponse AddCustomer(string accessToken, Customer newCustomer)
        {
            var newCustomerResponse = new NewCustomerResponse();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                CommonConverters invoiceConverters = new CommonConverters();

                if (orgEntity != null)
                {
                    if (newCustomer.CustomerCode.Contains("TEMP"))
                    {
                        newCustomerResponse.CustCode = CustomerBR.Instance.InsertCustomerByMob(accessToken, invoiceConverters.ConvertToCustomerEntity(newCustomer));
                    }
                    else
                    {
                        CustomerBR.Instance.UpdateCustomerByMob(accessToken, invoiceConverters.ConvertToCustomerEntity(newCustomer));
                        newCustomerResponse.CustCode = newCustomer.CustomerCode;
                        newCustomerResponse.ErrorMessage = "Success";

                        try
                        {
                            SessionBR.Instance.CreateTransactionLogMobile(
                                accessToken,
                                DateTime.Now.ToString(),
                                "Insert",
                                "Outlet",
                                "Insert new outlet " + newCustomerResponse.CustCode,
                                newCustomer.Latitude.ToString(),
                                newCustomer.Longitude.ToString());
                        }
                        catch { }
                    }
                }
                else
                {
                    newCustomerResponse.IsError = true;
                    newCustomerResponse.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                newCustomerResponse.IsError = true;
                newCustomerResponse.ErrorMessage = e.Message;
            }

            return newCustomerResponse;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetUserLoginDetails?userName={userName}&password={password}&emei={emei}")]
        public UserLoginCollectionResponse GetUserLoginDetails(string userName, string password, string emei)
        {
            var userLoginCollection = new UserLoginCollectionResponse();
            userLoginCollection.UserLoginCollection = new List<UserLogin>();

            try
            {
                OriginatorEntity originator = new OriginatorEntity();
                CommonConverters commonConverters = new CommonConverters();

                if (!OriginatorBR.Instance.IsNotEmeiUser(userName))
                {
                    if (OriginatorBR.Instance.IsEmeiExsistThisUser(userName, emei))
                    {
                        originator = OriginatorBR.Instance.GetLoginDetailsWithEMEI(userName, password, emei);

                        if (originator != null)
                        {
                            originator.Originator = userName;
                            originator.Password = password;
                            userLoginCollection = commonConverters.ConvertToUserLoginCollection(originator);

                            try
                            {
                                if (userLoginCollection.UserLoginCollection[0].IsTFA)
                                {
                                    OtpUser otpUser = new OtpUser();
                                    otpUser.userName = userLoginCollection.UserLoginCollection[0].UserName.Trim();

                                    var client = new RestClient(ConfigurationManager.AppSettings["OtpSmsUrl"].ToString());
                                    client.Timeout = -1;
                                    var request = new RestRequest(Method.POST);
                                    request.AddJsonBody(otpUser);

                                    IRestResponse response = client.Execute(request);
                                    Console.WriteLine(response.Content);
                                }
                            }
                            catch (Exception ex)
                            {

                                userLoginCollection.IsError = true;
                                userLoginCollection.ErrorMessage = ex.Message;
                            }
                        }
                        else
                        {
                            userLoginCollection.IsError = true;
                            //userLoginCollection.ErrorMessage = "Invalid Password";
                            userLoginCollection.ErrorMessage = "10202";
                        }
                    }
                    else
                    {
                        userLoginCollection.IsError = true;
                        userLoginCollection.ErrorMessage = "10201"; //EMEI Not Exsist in System
                    }
                }
                else
                {
                    originator = OriginatorBR.Instance.GetLoginDetailsWithEMEI(userName, password, emei);
                    if (originator.UserName != null)
                    {
                        originator.Originator = userName;
                        originator.Password = password;
                        userLoginCollection = commonConverters.ConvertToUserLoginCollection(originator);

                        try
                        {
                            if (userLoginCollection.UserLoginCollection[0].IsTFA)
                            {
                                OtpUser otpUser = new OtpUser();
                                otpUser.userName = userLoginCollection.UserLoginCollection[0].UserName.Trim();

                                var client = new RestClient(ConfigurationManager.AppSettings["OtpSmsUrl"].ToString());
                                client.Timeout = -1;
                                var request = new RestRequest(Method.POST);
                                request.AddJsonBody(otpUser);

                                IRestResponse response = client.Execute(request);
                                Console.WriteLine(response.Content);
                            }
                        }
                        catch (Exception ex)
                        {

                            userLoginCollection.IsError = true;
                            userLoginCollection.ErrorMessage = ex.Message;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                userLoginCollection.IsError = true;
                userLoginCollection.ErrorMessage = e.Message;
            }

            return userLoginCollection;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetUserEmeiStatus?userName={userName}&emei={emei}")]
        public UserLoginCollectionResponse GetUserEmeiStatus(string userName, string emei)
        {
            var userLoginCollection = new UserLoginCollectionResponse();
            userLoginCollection.UserLoginCollection = new List<UserLogin>();
            try
            {
                OriginatorEntity originator = new OriginatorEntity();
                CommonConverters commonConverters = new CommonConverters();

                if (!OriginatorBR.Instance.IsNotEmeiUser(userName))
                {
                    if (OriginatorBR.Instance.IsEmeiExsistThisUser(userName, emei))
                    {
                        userLoginCollection.IsError = false;
                        userLoginCollection.ErrorMessage = "Success"; //EMEI Exsist in System
                    }
                    else
                    {
                        userLoginCollection.IsError = true;
                        userLoginCollection.ErrorMessage = "10201"; //EMEI Not Exsist in System
                    }
                }
                else
                {
                    userLoginCollection.IsError = true;
                    userLoginCollection.ErrorMessage = "10201"; //EMEI Not Exsist in System
                }
            }
            catch (Exception e)
            {
                userLoginCollection.IsError = true;
                userLoginCollection.ErrorMessage = e.Message;
            }

            return userLoginCollection;
        }

        ////Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest,
        ////?accessToken={accessToken}&custCode={custCode}
        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "AddCustomerImage")]
        ////[WebInvoke(UriTemplate = "AddCustomerImage?accessToken={accessToken}&custCode={custCode}", Method = "POST")] 
        //public NewDocumentResponse AddCustomerImage( Stream image)
        //{
        //    byte[] buffer = new byte[32768];
        //    MemoryStream ms = new MemoryStream();
        //    int bytesRead, totalBytesRead = 0;
        //    do
        //    {
        //        bytesRead = image.Read(buffer, 0, buffer.Length);
        //        totalBytesRead += bytesRead;

        //        ms.Write(buffer, 0, bytesRead);
        //    } while (bytesRead > 0);

        //    // Save the photo on database. 

        //    ms.Close();
        //    //Console.WriteLine("Uploaded file {0} with {1} bytes", fileName, totalBytesRead); 

        //    var newDocumentResponse = new NewDocumentResponse();
        //    //try
        //    //{
        //    //    CommonConverters invoiceConverters = new CommonConverters();
        //    //    //DocumentBR.Instance.Save(invoiceConverters.ConvertToCustomerDocumentEntity(custCode, image));
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //    newDocumentResponse.IsError = true;
        //    //    newDocumentResponse.ErrorMessage = e.Message;
        //    //}
        //    return newDocumentResponse;
        //}

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetVisitNoteReasonCollection?accessToken={accessToken}")]
        public VisitNoteReasonCollectionResponse GetVisitNoteReasonCollection(string accessToken)
        {
            var visitNoteReasonCollection = new VisitNoteReasonCollectionResponse();
            try
            {
                CommonConverters commonConverters = new CommonConverters();
                List<VisitNoteReasonEntity> visitNoteReasonList = VisitNoteBR.Instance.GetVisitNoteReason();
                visitNoteReasonCollection = commonConverters.ConvertToVisitNoteReasonCollection(visitNoteReasonList);
            }
            catch (Exception e)
            {
                visitNoteReasonCollection.IsError = true;
                visitNoteReasonCollection.ErrorMessage = e.Message;
            }
            return visitNoteReasonCollection;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "AddVisitNote")]
        public NewVisitNoteResponse AddVisitNote(string accessToken, VisitNote newVisitNote)
        {
            var newVisitNoteResponse = new NewVisitNoteResponse();
            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    CommonConverters invoiceConverters = new CommonConverters();
                    VisitNoteBR.Instance.InsertVisitNote(accessToken, invoiceConverters.ConvertToVisitNoteEntity(newVisitNote));
                    newVisitNoteResponse.ErrorMessage = "Success";

                    try
                    {
                        SessionBR.Instance.CreateTransactionLogMobile(
                            accessToken,
                            DateTime.Now.ToString(),
                            "Insert",
                            "VisitNote",
                            "Insert new Visit Note for customer " + newVisitNote.CustCode,
                            newVisitNote.Latitude.ToString(),
                            newVisitNote.Longitude.ToString());
                    }
                    catch { }
                }
                else
                {
                    newVisitNoteResponse.IsError = true;
                    newVisitNoteResponse.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                newVisitNoteResponse.IsError = true;
                newVisitNoteResponse.ErrorMessage = e.Message;
            }

            return newVisitNoteResponse;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetRepTargetVsActual?accessToken={accessToken}")]
        public TargetVsActualResponse GetRepTargetVsActual(string accessToken)
        {
            var targetVsActualResponse = new TargetVsActualResponse();
            try
            {
                RepTargetEntity tepTarget = new RepTargetEntity();
                List<RepTargetEntity> repTargetList = new List<RepTargetEntity>();
                CommonConverters commonConverters = new CommonConverters();
                repTargetList = RepTargetBR.Instance.GetRepTargetVsActualByAccessToken(accessToken);
                targetVsActualResponse = commonConverters.ConvertToTargetVsActualResponse(repTargetList);
            }
            catch (Exception e)
            {
                targetVsActualResponse.IsError = true;
                targetVsActualResponse.ErrorMessage = e.Message;
            }

            return targetVsActualResponse;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetRepTargetVsActualNew?accessToken={accessToken}")]
        public TargetVsActualResponse GetRepTargetVsActualNew(string accessToken)
        {
            var targetVsActualResponse = new TargetVsActualResponse();
            try
            {
                RepTargetEntity tepTarget = new RepTargetEntity();
                List<RepTargetEntity> repTargetList = new List<RepTargetEntity>();
                CommonConverters commonConverters = new CommonConverters();
                repTargetList = RepTargetBR.Instance.GetRepTargetVsActualByAccessTokenNew(accessToken);
                targetVsActualResponse = commonConverters.ConvertToTargetVsActualResponse(repTargetList);
            }
            catch (Exception e)
            {
                targetVsActualResponse.IsError = true;
                targetVsActualResponse.ErrorMessage = e.Message;
            }

            return targetVsActualResponse;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetStockTotalCollection?accessToken={accessToken}")]
        public StockTotalCollectionResponse GetStockTotalCollection(string accessToken)
        {
            var stockTotalCollection = new StockTotalCollectionResponse();
            try
            {
                CommonConverters commonConverters = new CommonConverters();
                List<StockTotalEntity> stockTotalList = LoadStockBR.Instance.GetStockByAccessToken(accessToken);
                stockTotalCollection = commonConverters.ConvertToStockTotalCollection(stockTotalList);
            }
            catch (Exception e)
            {
                stockTotalCollection.IsError = true;
                stockTotalCollection.ErrorMessage = e.Message;
            }
            return stockTotalCollection;
        }

        //[WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "AddCustomerImage1")]
        ////[WebInvoke(UriTemplate = "AddCustomerImage?accessToken={accessToken}&custCode={custCode}", Method = "POST")] 
        ////public NewDocumentResponse AddCustomerImage1(string test,InvoiceTest image1)
        //public NewDocumentResponse AddCustomerImage1(string accessToken, InvoiceTest newCustomer)
        //{
        //    byte[] imageBytes = Encoding.Unicode.GetBytes(newCustomer.ImageByte);
        //    //byte[] buffer = new byte[32768];
        //    //MemoryStream ms = new MemoryStream();
        //    //int bytesRead, totalBytesRead = 0;
        //    //do
        //    //{
        //    //    bytesRead = image.image.Read(buffer, 0, buffer.Length);
        //    //    totalBytesRead += bytesRead;

        //    //    ms.Write(buffer, 0, bytesRead);
        //    //} while (bytesRead > 0);

        //    //// Save the photo on database. 


        //    //ms.Close();
        //    //Console.WriteLine("Uploaded file {0} with {1} bytes", fileName, totalBytesRead); 

        //    var newDocumentResponse = new NewDocumentResponse();
        //    //try
        //    //{
        //    //    CommonConverters invoiceConverters = new CommonConverters();
        //    //    //DocumentBR.Instance.Save(invoiceConverters.ConvertToCustomerDocumentEntity(custCode, image));
        //    //}
        //    //catch (Exception e)
        //    //{
        //    //    newDocumentResponse.IsError = true;
        //    //    newDocumentResponse.ErrorMessage = e.Message;
        //    //}
        //    return newDocumentResponse;
        //}

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "AddCustomerImage")]
        public NewDocumentResponse AddCustomerImage(string custCode, string accessToken, string imageByte)
        {
            try
            {
                SessionBR.Instance.CreateLog(accessToken, "Cutomer Image : " + custCode, "Open");
            }
            catch { }

            var newDocumentResponse = new NewDocumentResponse();
            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    byte[] imageBytes = Convert.FromBase64String(imageByte);
                    CommonConverters invoiceConverters = new CommonConverters();
                    DocumentBR.Instance.Save(invoiceConverters.ConvertToCustomerDocumentEntity(custCode, imageBytes));
                    newDocumentResponse.ErrorMessage = "Success";
                }
                else
                {
                    newDocumentResponse.IsError = true;
                    newDocumentResponse.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                newDocumentResponse.IsError = true;
                newDocumentResponse.ErrorMessage = e.Message;
            }

            try
            {
                SessionBR.Instance.CreateLog(accessToken, "Cutomer Image : " + custCode, newDocumentResponse.ErrorMessage);
            }
            catch { }

            return newDocumentResponse;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "CancelCreditSettlement")]
        public InvoiceResponse CancelCreditSettlement(string accessToken, string settlementNo, string Latitude = "", string Longitude = "")
        {
            var invoiceResponse = new InvoiceResponse();
            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    InvoiceConverters invoiceConverters = new InvoiceConverters();
                    InvoiceHeaderBR.Instance.CancelCreditSettlement(accessToken, settlementNo);
                    invoiceResponse.ErrorMessage = "Success";

                    try
                    {
                        SessionBR.Instance.CreateTransactionLogMobile(
                            accessToken,
                            DateTime.Now.ToString(),
                            "Delete",
                            "CreditSettlement",
                            "Cancel settlement no " + settlementNo,
                            Latitude.ToString(),
                            Longitude.ToString());
                    }
                    catch { }
                }
                else
                {
                    invoiceResponse.IsError = true;
                    invoiceResponse.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                invoiceResponse.IsError = true;
                invoiceResponse.ErrorMessage = e.Message;
            }

            return invoiceResponse;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "ReturnInvoices")]
        public ReturnInvoicesResponse ReturnInvoices(string accessToken, ReturnInvoices newReturnInvoice)
        {
            var returnInvoicesResponse = new ReturnInvoicesResponse();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    InvoiceConverters invoiceConverters = new InvoiceConverters();
                    InvoiceHeaderBR.Instance.ReturnInvoices(accessToken, invoiceConverters.ConvertToReturnInvoiceCollection(newReturnInvoice));
                    returnInvoicesResponse.ErrorMessage = "Success";

                    try
                    {
                        SessionBR.Instance.CreateTransactionLogMobile(
                            accessToken,
                            DateTime.Now.ToString(),
                            "Insert",
                            "ReturnInvoice",
                            "Insert Return Invoice " + newReturnInvoice.returnno,
                            newReturnInvoice.latitude.ToString(),
                            newReturnInvoice.longitude.ToString());
                    }
                    catch { }
                }
                else
                {
                    returnInvoicesResponse.IsError = true;
                    returnInvoicesResponse.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                returnInvoicesResponse.IsError = true;
                returnInvoicesResponse.ErrorMessage = e.Message;
            }

            return returnInvoicesResponse;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "UpdateEMEI")]
        public UpdateRepEMEIResponse UpdateRepEMEI(string accessToken, string emei, string pin)
        {
            try
            {
                SessionBR.Instance.CreateLog(accessToken, "Update EMEI : " + emei, "Open");
            }
            catch { }

            var returnResponse = new UpdateRepEMEIResponse();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    if (OriginatorBR.Instance.GetNewPinByAccessToken(accessToken) == pin)
                    {
                        OriginatorBR.Instance.UpdateRepEMEI(accessToken, emei);
                        returnResponse.IsError = false;
                        returnResponse.ErrorMessage = "Success";
                    }
                    else
                    {
                        returnResponse.IsError = true;
                        returnResponse.ErrorMessage = "10211"; //Pin Invalid
                    }
                }
                else
                {
                    returnResponse.IsError = true;
                    returnResponse.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                returnResponse.IsError = true;
                returnResponse.ErrorMessage = "Error";
            }

            try
            {
                SessionBR.Instance.CreateLog(accessToken, "Update EMEI : " + emei, returnResponse.ErrorMessage);
            }
            catch { }

            return returnResponse;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "UpdateRepsSyncedStatus")]
        public UpdateRepsSyncedStatusResponse UpdateRepsSyncedStatus(string RepCode, string emei, bool IsSync, string ApkVersion)
        {
            try
            {
                SessionBR.Instance.CreateLog(RepCode, "Update Sync Status : " + emei, "Open");
            }
            catch { }

            var returnResponse = new UpdateRepsSyncedStatusResponse();

            try
            {
                if (OriginatorBR.Instance.UpdateRepsSyncedStatus(RepCode, emei, IsSync, ApkVersion))
                {
                    returnResponse.IsError = false;
                    returnResponse.ErrorMessage = "Success";
                }
                else
                {
                    returnResponse.IsError = true;
                    returnResponse.ErrorMessage = "Error"; //Pin Invalid
                }
            }
            catch (Exception e)
            {
                returnResponse.IsError = true;
                returnResponse.ErrorMessage = "Error";
            }

            try
            {
                SessionBR.Instance.CreateLog(RepCode, "Update Sync Status : " + emei, returnResponse.ErrorMessage);
            }
            catch { }

            return returnResponse;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "UpdateDeviceIdByEmei")]
        public UpdateDeviceIdByEmei UpdateDeviceIdByEmei(string accessToken, string emei, string deviceId)
        {
            try
            {
                SessionBR.Instance.CreateLog(accessToken, "Update Device ID : " + emei, "Open");
            }
            catch { }

            var returnResponse = new UpdateDeviceIdByEmei();

            try
            {
                //OriginatorEntity orgEntity = new OriginatorEntity();
                //orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                //if (orgEntity != null)
                //{
                if (OriginatorBR.Instance.UpdateDeviceIdByEmei(accessToken, emei, deviceId))
                {
                    returnResponse.IsError = false;
                    returnResponse.ErrorMessage = "Success";
                }
                else
                {
                    returnResponse.IsError = true;
                    returnResponse.ErrorMessage = "Error";
                }
            }
            catch (Exception e)
            {
                returnResponse.IsError = true;
                returnResponse.ErrorMessage = "Error";
            }

            try
            {
                SessionBR.Instance.CreateLog(accessToken, "Update Device ID : " + emei, returnResponse.ErrorMessage);
            }
            catch { }

            return returnResponse;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "InsertRepEMEI")]
        public InsertRepEMEIResponse InsertRepEMEI(string userName, string emei, string pin)
        {
            try
            {
                SessionBR.Instance.CreateLog(userName, "Insert Rep EMEI : " + emei, "Open");
            }
            catch { }

            var returnResponse = new InsertRepEMEIResponse();

            try
            {
                if (userName == null || userName == "")
                {
                    returnResponse.IsError = true;
                    returnResponse.ErrorMessage = "10203";//User Empty
                    //returnResponse.ErrorDescription = "User Empty";
                }
                else if (emei == null || emei == "")
                {
                    returnResponse.IsError = true;
                    returnResponse.ErrorMessage = "10204";//Emei Empty
                    //returnResponse.ErrorDescription = "Emei Empty";
                }
                else
                {
                    if (OriginatorBR.Instance.GetNewPinByAccessToken("") == pin)
                    {
                        string ret = OriginatorBR.Instance.InsertRepEMEI(userName, emei);
                        if (ret == "UserNotExsist")
                        {
                            returnResponse.IsError = true;
                            returnResponse.ErrorMessage = "10202";//User Not Exsist
                            //returnResponse.ErrorDescription = "User Not Exsist";
                        }
                        else
                        {
                            returnResponse.IsError = false;
                            returnResponse.ErrorMessage = "Success";
                            //returnResponse.ErrorDescription = "Success";
                        }
                    }
                    else
                    {
                        returnResponse.IsError = true;
                        returnResponse.ErrorMessage = "10211";//Pin Empty
                        //returnResponse.ErrorDescription = "Pin Empty";
                    }
                }
            }
            catch (Exception e)
            {
                returnResponse.IsError = true;
                returnResponse.ErrorMessage = "10205";//API Error
                //returnResponse.ErrorDescription = "API Error";
            }

            try
            {
                SessionBR.Instance.CreateLog(userName, "Insert Rep EMEI : " + emei, returnResponse.ErrorMessage);
            }
            catch { }

            return returnResponse;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "CancelReturnInvoice")]
        public ReturnInvoicesResponse CancelReturnInvoice(string accessToken, string returnno, string Latitude = "", string Longitude = "")
        {
            var returnInvoicesResponse = new ReturnInvoicesResponse();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    InvoiceConverters invoiceConverters = new InvoiceConverters();
                    InvoiceHeaderBR.Instance.CancelReturnInvoices(accessToken, returnno);
                    returnInvoicesResponse.ErrorMessage = "Success";

                    try
                    {
                        SessionBR.Instance.CreateTransactionLogMobile(
                            accessToken,
                            DateTime.Now.ToString(),
                            "Delete",
                            "CreditSettlement",
                            "Cancel return no " + returnno,
                            Latitude.ToString(),
                            Longitude.ToString());
                    }
                    catch { }
                }
                else
                {
                    returnInvoicesResponse.IsError = true;
                    returnInvoicesResponse.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                returnInvoicesResponse.IsError = true;
                returnInvoicesResponse.ErrorMessage = e.Message;
            }

            return returnInvoicesResponse;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "AddCustomerChecklistDetails")]
        public CustomerChecklistResponse AddCustomerChecklistDetails(string accessToken, CheckListDetail newCheckList)
        {
            var customerChecklistResponse = new CustomerChecklistResponse();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    CommonConverters invoiceConverters = new CommonConverters();
                    ChecklistBR.Instance.SaveCustomerChecklists(invoiceConverters.ConvertToChecklistDetailEntity(newCheckList));
                    customerChecklistResponse.ErrorMessage = "Success";

                    try
                    {
                        SessionBR.Instance.CreateTransactionLogMobile(
                            accessToken,
                            DateTime.Now.ToString(),
                            "Insert",
                            "CheckList",
                            "Add new checklist by " + newCheckList.RepCode + " - " + newCheckList.Remarks,
                            newCheckList.Latitude.ToString(),
                            newCheckList.Longitude.ToString());
                    }
                    catch { }
                }
                else
                {
                    customerChecklistResponse.IsError = true;
                    customerChecklistResponse.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                customerChecklistResponse.IsError = true;
                customerChecklistResponse.ErrorMessage = e.Message;
            }

            return customerChecklistResponse;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "InsertDailyRepInvoiceCount")]
        public DailyRepInvoiceCountResponse InsertDailyRepInvoiceCount(string datetime, string repcode, string emei, int invoicecount, int delinvoicecount)
        {
            try
            {
                SessionBR.Instance.CreateLog(repcode, "Daily Rep Invoice Count : " + emei, "Open");
            }
            catch { }

            var DailyRepInvoiceCountResponse = new DailyRepInvoiceCountResponse();
            try
            {
                string ret = OriginatorBR.Instance.InsertDailyRepInvoiceCount(datetime, repcode, emei, invoicecount, delinvoicecount);
                if (ret == "successful")
                {
                    DailyRepInvoiceCountResponse.IsError = false;
                    DailyRepInvoiceCountResponse.ErrorMessage = "Success"; //LogOut Code
                }
                else
                {
                    DailyRepInvoiceCountResponse.IsError = true;
                    DailyRepInvoiceCountResponse.ErrorMessage = "Error"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                DailyRepInvoiceCountResponse.IsError = true;
                DailyRepInvoiceCountResponse.ErrorMessage = "Error"; //LogOut Code
            }

            try
            {
                SessionBR.Instance.CreateLog(repcode, "Daily Rep Invoice Count : " + emei, DailyRepInvoiceCountResponse.ErrorMessage);
            }
            catch { }

            return DailyRepInvoiceCountResponse;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "AddOrder")]
        //"GetOrderCollection?accessToken={accessToken}"
        public NewOrderResponse AddOrder(string accessToken, OrderModel newOrder)
        {
            var newOrderResponse = new NewOrderResponse();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    OrderModel oModel = OrderHeaderBR.Instance.GetOrdersByOrderNo(newOrder.OrderNo);

                    if (oModel != null)
                    {
                        newOrder.OrderId = oModel.OrderId;
                        if (OrderHeaderBR.Instance.UpdateOrder(accessToken, newOrder))
                        {
                            newOrderResponse.IsError = false;
                            newOrderResponse.ErrorMessage = "Success";

                            try
                            {
                                SessionBR.Instance.CreateTransactionLogMobile(
                                    accessToken,
                                    DateTime.Now.ToString(),
                                    "Update",
                                    "Order",
                                    "Update order " + newOrder.OrderNo,
                                    newOrder.Latitude.ToString(),
                                    newOrder.Longitude.ToString());
                            }
                            catch { }
                        }
                        else
                        {
                            newOrderResponse.IsError = true;
                            newOrderResponse.ErrorMessage = "Request Object Error";
                        }
                    }
                    else
                    {
                        if (OrderHeaderBR.Instance.InsertOrder(accessToken, newOrder))
                        {
                            newOrderResponse.IsError = false;
                            newOrderResponse.ErrorMessage = "Success";

                            try
                            {
                                SessionBR.Instance.CreateTransactionLogMobile(
                                    accessToken,
                                    DateTime.Now.ToString(),
                                    "Insert",
                                    "Order",
                                    "Insert new order " + newOrder.OrderNo,
                                    newOrder.Latitude.ToString(),
                                    newOrder.Longitude.ToString());
                            }
                            catch { }
                        }
                        else
                        {
                            newOrderResponse.IsError = true;
                            newOrderResponse.ErrorMessage = "Request Object Error";
                        }
                    }
                }
                else
                {
                    newOrderResponse.IsError = true;
                    newOrderResponse.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                newOrderResponse.IsError = true;
                newOrderResponse.ErrorMessage = e.Message;
            }

            return newOrderResponse;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "UpdateOrder")]
        public NewOrderResponse UpdateOrder(string accessToken, OrderModel newOrder)
        {
            var newOrderResponse = new NewOrderResponse();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    if (OrderHeaderBR.Instance.UpdateOrder(accessToken, newOrder))
                    {
                        newOrderResponse.IsError = false;
                        newOrderResponse.ErrorMessage = "Success";

                        try
                        {
                            SessionBR.Instance.CreateTransactionLogMobile(
                                accessToken,
                                DateTime.Now.ToString(),
                                "Update",
                                "Order",
                                "Update Order " + newOrder.OrderNo.ToString(),
                                newOrder.Latitude.ToString(),
                                newOrder.Longitude.ToString());
                        }
                        catch { }
                    }
                    else
                    {
                        newOrderResponse.IsError = true;
                        newOrderResponse.ErrorMessage = "Request Object Error";
                    }
                }
                else
                {
                    newOrderResponse.IsError = true;
                    newOrderResponse.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                newOrderResponse.IsError = true;
                newOrderResponse.ErrorMessage = e.Message;
            }

            return newOrderResponse;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetOrderCollection?accessToken={accessToken}")]
        public OrderCollection GetOrderCollection(string accessToken)
        {
            var orderCollection = new OrderCollection();
            try
            {
                List<OrderModel> orderList = new List<OrderModel>();
                orderList = OrderHeaderBR.Instance.GetOrdersByAccessToken(accessToken);

                orderCollection.Orders = orderList;
            }
            catch (Exception e)
            {
                orderCollection.IsError = true;
                orderCollection.ErrorMessage = e.Message;
            }

            return orderCollection;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "UpdateSRSyncDetail")]
        public SRSyncResponse UpdateSRSyncDetail(string accessToken, SalesRepSyncDetailModel srSyncDet)
        {
            var response = new SRSyncResponse();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    if (SyncBR.Instance.UpdateSRSyncDetail(accessToken, srSyncDet))
                    {
                        response.IsError = false;
                        response.ErrorMessage = "Success";
                    }
                    else
                    {
                        response.IsError = true;
                        response.ErrorMessage = "Request Object Error";
                    }
                }
                else
                {
                    response.IsError = true;
                    response.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                response.IsError = true;
                response.ErrorMessage = e.Message;
            }

            return response;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetDistributerList?accessToken={accessToken}")]
        public DistributerCollection GetDistributerList(string accessToken)
        {
            var dbCollection = new DistributerCollection();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                List<DistributerModel> dbList = new List<DistributerModel>();
                dbList = DistributorBR.Instance.GetAllDistributerByASM(orgEntity.Originator);

                dbCollection.DistributerList = dbList;
            }
            catch (Exception e)
            {
                dbCollection.IsError = true;
                dbCollection.ErrorMessage = e.Message;
            }

            return dbCollection;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetAllASMVisitsList?accessToken={accessToken}")]
        public ASMVisitCollection GetAllASMVisitsList(string accessToken)
        {
            var dbCollection = new ASMVisitCollection();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                List<OrigivatorVisitModel> visitList = new List<OrigivatorVisitModel>();
                visitList = OriginatorBR.Instance.GetAllASMVisits(orgEntity.Originator);

                dbCollection.ASMVisits = visitList;
            }
            catch (Exception e)
            {
                dbCollection.IsError = true;
                dbCollection.ErrorMessage = e.Message;
            }

            return dbCollection;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "UpdateOrigivatorVisitDetail")]
        public OrigivatorVisitResponse UpdateOrigivatorVisitDetail(string accessToken, OrigivatorVisitModel visitDet)
        {
            var response = new OrigivatorVisitResponse();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    if (OriginatorBR.Instance.UpdateOriginatorVisitDetail(accessToken, visitDet))
                    {
                        response.IsError = false;
                        response.ErrorMessage = "Success";

                        try
                        {
                            SessionBR.Instance.CreateTransactionLogMobile(
                                accessToken,
                                DateTime.Now.ToString(),
                                "Update",
                                "VisitDetail",
                                "Update Visit Detail ",
                                visitDet.cinLatitude.ToString(),
                                visitDet.cinLogitude.ToString());
                        }
                        catch { }
                    }
                    else
                    {
                        response.IsError = true;
                        response.ErrorMessage = "Request Object Error";
                    }
                }
                else
                {
                    response.IsError = true;
                    response.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                response.IsError = true;
                response.ErrorMessage = e.Message;
            }

            return response;
        }

        #region "3rd Phase"

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "ClockIn")]
        public OrigivatorAttendanceResponse UpdateOriginatorAttendanceCheckIn(string accessToken, OriginatorAttendance attendanceDet)
        {
            var response = new OrigivatorAttendanceResponse();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    if (OriginatorBR.Instance.UpdateOriginatorAttendanceCheckIn(accessToken, attendanceDet))
                    {
                        response.IsError = false;
                        response.ErrorMessage = "Success";

                        try
                        {
                            SessionBR.Instance.CreateTransactionLogMobile(
                                accessToken,
                                DateTime.Now.ToString(),
                                "Update",
                                "Attendance",
                                "Check-In",
                                attendanceDet.checkInLat.ToString(),
                                attendanceDet.checkInLng.ToString());
                        }
                        catch { }
                    }
                    else
                    {
                        response.IsError = true;
                        response.ErrorMessage = "Request Object Error";
                    }
                }
                else
                {
                    response.IsError = true;
                    response.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                response.IsError = true;
                response.ErrorMessage = e.Message;
            }

            return response;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "ClockOut")]
        public OrigivatorAttendanceResponse UpdateOriginatorAttendanceCheckOut(string accessToken, OriginatorAttendance attendanceDet)
        {
            var response = new OrigivatorAttendanceResponse();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    if (OriginatorBR.Instance.UpdateOriginatorAttendanceCheckOut(accessToken, attendanceDet))
                    {
                        response.IsError = false;
                        response.ErrorMessage = "Success";

                        try
                        {
                            SessionBR.Instance.CreateTransactionLogMobile(
                                accessToken,
                                DateTime.Now.ToString(),
                                "Update",
                                "Attendance",
                                "Check-Out",
                                attendanceDet.checkOutLat.ToString(),
                                attendanceDet.checkOutLng.ToString());
                        }
                        catch { }
                    }
                    else
                    {
                        response.IsError = true;
                        response.ErrorMessage = "Request Object Error";
                    }
                }
                else
                {
                    response.IsError = true;
                    response.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                response.IsError = true;
                response.ErrorMessage = e.Message;
            }

            return response;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetProductCollectionForModernTrade?accessToken={accessToken}&syncDate={syncDate}")]
        public ProductCollectionResponse GetProductCollectionForModernTrade(string accessToken, string syncDate)
        {
            var productCollection = new ProductCollectionResponse();
            try
            {
                CommonConverters commonConverters = new CommonConverters();
                List<ProductEntity> productList = ProductBR.Instance.GetProductsForModernTradeByAccessToken(accessToken, syncDate);
                productCollection = commonConverters.ConvertToProductCollection(productList);
            }
            catch (Exception e)
            {
                productCollection.IsError = true;
                productCollection.ErrorMessage = e.Message;
            }
            return productCollection;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "InsertModernTrade")]
        public ModernTradeInsertResponse InsertModernTrade(string accessToken, ModernTradeMasterModel request)
        {
            var newInvoiceResponse = new ModernTradeInsertResponse();
            newInvoiceResponse.ModernTradeId = 0;

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    ModernTradeMasterModel mtModel = ModernTradeBR.Instance.InsertModernTrade(accessToken, request);

                    if (mtModel.IsSuccess)
                    {
                        newInvoiceResponse.ModernTradeId = mtModel.ModernTradeId;
                        newInvoiceResponse.IsError = false;
                        newInvoiceResponse.ErrorMessage = "Success";

                        try
                        {
                            SessionBR.Instance.CreateTransactionLogMobile(
                                accessToken,
                                DateTime.Now.ToString(),
                                "Insert",
                                "ModernTrade",
                                "Insert Modern Trade " + request.ModernTradeCode,
                                request.Latitude.ToString(),
                                request.Longitude.ToString());
                        }
                        catch { }
                    }
                    else
                    {
                        newInvoiceResponse.IsError = true;
                        newInvoiceResponse.ErrorMessage = "Request Object Error";
                    }
                }
                else
                {
                    newInvoiceResponse.IsError = true;
                    newInvoiceResponse.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                newInvoiceResponse.IsError = true;
                newInvoiceResponse.ErrorMessage = e.Message;
            }

            return newInvoiceResponse;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "DeleteModernTradeHeader")]
        public InvoiceResponse DeleteModernTradeHeader(string accessToken, ModernTradeMasterModel request)
        {
            var invoiceResponse = new InvoiceResponse();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    InvoiceConverters invoiceConverters = new InvoiceConverters();
                    ModernTradeBR.Instance.DeleteModernTradeHeader(accessToken, request.ModernTradeId);
                    invoiceResponse.ErrorMessage = "Success";

                    try
                    {
                        SessionBR.Instance.CreateTransactionLogMobile(
                            accessToken,
                            DateTime.Now.ToString(),
                            "Delete",
                            "ModernTrade",
                            "Delete Modern Trade code " + request.ModernTradeCode,
                            request.Latitude.ToString(),
                            request.Longitude.ToString());
                    }
                    catch { }
                }
                else
                {
                    invoiceResponse.IsError = true;
                    invoiceResponse.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                invoiceResponse.IsError = true;
                invoiceResponse.ErrorMessage = e.Message;
            }

            return invoiceResponse;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetAllModernTradesByAccessToken?accessToken={accessToken}&date={date}")]
        public ModernTradeResponse GetAllModernTradesByAccessToken(string accessToken, string date)
        {
            var mtResponse = new ModernTradeResponse();

            try
            {
                InvoiceConverters invoiceConverters = new InvoiceConverters();
                List<ModernTradeMasterModel> mtList = new List<ModernTradeMasterModel>();
                mtList = ModernTradeBR.Instance.GetAllModernTradesByAccessToken(accessToken, date);

                mtResponse.IsError = false;
                mtResponse.modernTrades = mtList;
            }
            catch (Exception e)
            {
                mtResponse.IsError = true;
                mtResponse.ErrorMessage = e.Message;
            }

            return mtResponse;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetAllModernTradesByAccessTokenAndPeriod?accessToken={accessToken}&startDate={startDate}&endDate={endDate}")]
        public ModernTradeResponse GetAllModernTradesByAccessTokenAndPeriod(string accessToken, string startDate, string endDate)
        {
            var mtResponse = new ModernTradeResponse();

            try
            {
                InvoiceConverters invoiceConverters = new InvoiceConverters();
                List<ModernTradeMasterModel> mtList = new List<ModernTradeMasterModel>();
                mtList = ModernTradeBR.Instance.GetAllModernTradesByAccessTokenAndPeriod(accessToken, startDate, endDate);

                mtResponse.IsError = false;
                mtResponse.modernTrades = mtList;
            }
            catch (Exception e)
            {
                mtResponse.IsError = true;
                mtResponse.ErrorMessage = e.Message;
            }

            return mtResponse;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "InsertTransactionLogMobile")]
        public TransactionLogResponse CreateTransactionLogMobile(string accessToken, TransactionLogModel transLog)
        {
            var response = new TransactionLogResponse();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    SessionBR.Instance.CreateTransactionLogMobile(accessToken,
                                                                transLog.TransactionTime,
                                                                transLog.TransactionType,
                                                                transLog.TransactionModel,
                                                                transLog.TransactionDescription,
                                                                transLog.Latitude,
                                                                transLog.Longitude);
                }
                else
                {
                    response.IsError = true;
                    response.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                response.IsError = true;
                response.ErrorMessage = e.Message;
            }

            return response;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetAttendanceReasons?accessToken={accessToken}")]
        public OriginatorAttendanceReasonsResponse GetAttendanceReasons(string accessToken)
        {
            var mtResponse = new OriginatorAttendanceReasonsResponse();

            try
            {
                List<AttendanceReasons> mtList = new List<AttendanceReasons>();
                mtList = OriginatorBR.Instance.GetAttendanceReasons().FindAll(m => m.status == "A");

                mtResponse.IsError = false;
                mtResponse.reasons = mtList;
            }
            catch (Exception e)
            {
                mtResponse.IsError = true;
                mtResponse.ErrorMessage = e.Message;
            }

            return mtResponse;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "InsertOriginatorAttendanceReason")]
        public TransactionLogResponse InsertOriginatorAttendanceReason(string accessToken, OriginatorAttendanceReasons attenRequest)
        {
            var response = new TransactionLogResponse();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    attenRequest.originator = orgEntity.Originator;

                    if (OriginatorBR.Instance.InsertOriginatorAttendanceReason(attenRequest))
                    {
                        response.IsError = false;
                        response.ErrorMessage = "Success";

                        try
                        {
                            SessionBR.Instance.CreateTransactionLogMobile(
                                accessToken,
                                DateTime.Now.ToString(),
                                "Insert",
                                "AttendanceReason",
                                "Insert Attendance Reason " + attenRequest.reason,
                                attenRequest.Latitude.ToString(),
                                attenRequest.Longitude.ToString());
                        }
                        catch { }
                    }
                    else
                    {
                        response.IsError = true;
                        response.ErrorMessage = "Request Object Error";
                    }
                }
                else
                {
                    response.IsError = true;
                    response.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                response.IsError = true;
                response.ErrorMessage = e.Message;
            }

            return response;
        }

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "UpdateCustomerSequenceByRep")]
        public UpdateCustomerDayOrderResponse UpdateCustomerDayOrderByRep(string accessToken, int routeId, List<CustomerSequence> customerCollection, string Latitude = "", string Longitude = "")
        {
            var newResponse = new UpdateCustomerDayOrderResponse();

            try
            {
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    if (CustomerBR.Instance.UpdateCustomerDayOrderByRep(orgEntity.Originator, routeId, customerCollection))
                    {
                        newResponse.IsError = false;
                        newResponse.ErrorMessage = "Success";

                        try
                        {
                            SessionBR.Instance.CreateTransactionLogMobile(
                                accessToken,
                                DateTime.Now.ToString(),
                                "Update",
                                "CustomerSequence",
                                "Update Customer Sequence for Route Id " + routeId.ToString(),
                                Latitude.ToString(),
                                Longitude.ToString());
                        }
                        catch { }
                    }
                    else
                    {
                        newResponse.IsError = true;
                        newResponse.ErrorMessage = "Request Object Error";
                    }
                }
                else
                {
                    newResponse.IsError = true;
                    newResponse.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                newResponse.IsError = true;
                newResponse.ErrorMessage = e.Message;
            }

            return newResponse;
        }

        #endregion

        [WebInvoke(Method = "POST", ResponseFormat = WebMessageFormat.Json, BodyStyle = WebMessageBodyStyle.WrappedRequest, UriTemplate = "ImageUploadByMTCustomer?accessToken={accessToken}")]
        public MTCustomerImageUploadResponse ImageUploadedByMTCustomers(string accessToken, MTCustomerImages mtImages)
        {
            var response = new MTCustomerImageUploadResponse();
            try
            {
                //byte[] imageBytes = Convert.FromBase64String(imageByte);
                OriginatorEntity orgEntity = new OriginatorEntity();
                orgEntity = OriginatorBR.Instance.GetOriginatorByAccessToken(accessToken);

                if (orgEntity != null)
                {
                    //****** CODE ******
                    MTImageUploder mTImageUploder = new MTImageUploder();
                    mTImageUploder.UploadImages(mtImages.imgContent, orgEntity.Originator, mtImages.custCode, mtImages.description);


                    response.ErrorMessage = "Success";
                }
                else
                {
                    response.IsError = true;
                    response.ErrorMessage = "10210"; //LogOut Code
                }
            }
            catch (Exception e)
            {
                response.IsError = true;
                response.ErrorMessage = e.Message;
            }

            return response;
        }


        #region "Phase 4 Developement"
        //Added by Rangan 14/09/2023
        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetProductUOM?accessToken={accessToken}")]
        public ProductUOMCollectionResponse GetProductUOMCollection(string accessToken)
        {
            var productUOMCollection = new ProductUOMCollectionResponse();
            
            try
            {
                List<Model.ProductUOMViewModel> productUomList = ProductBR.Instance.GetProductUOMByAccessToken(accessToken);
                productUOMCollection.UOMCollection = productUomList;

            }
            catch (Exception e)
            {
                productUOMCollection.IsError = true;
                productUOMCollection.ErrorMessage = e.Message;
            }
            return productUOMCollection;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetCustomerECOCumulative?accessToken={accessToken}&strDate={strDate}&endDate={endDate}")]

        public CustomerECOCollectionResponse GetCustomerECOCumulative(string accessToken, string strDate, string endDate)
        {
            var customerECOCollection = new CustomerECOCollectionResponse();
            try
            {
                List<Model.CustomerECOModel> customerList = CustomerBR.Instance.GetCustomerECOCumulativeByAccessToken(accessToken, strDate, endDate);
                customerECOCollection.ECOCollection = customerList;
            }
            catch (Exception e)
            {
                customerECOCollection.IsError = true;
                customerECOCollection.ErrorMessage = e.Message;
            }
            return customerECOCollection;
        }


        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetBrandECOCumulative?accessToken={accessToken}&strDate={strDate}&endDate={endDate}")]

        public BrandECOCollectionResponse GetBrandECOCumulative(string accessToken, string strDate, string endDate)
        {
            var brandECOCollection = new BrandECOCollectionResponse();
            try
            {
                List<Model.BrandECOModel> brandList = BrandBR.Instance.GetBrandECOCumulativeByAccessToken(accessToken,strDate, endDate);
                brandECOCollection.ECOCollection = brandList;
            }
            catch (Exception e)
            {
                brandECOCollection.IsError = true;
                brandECOCollection.ErrorMessage = e.Message;
            }
            return brandECOCollection;
        }



        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetCategoryECOCumulative?accessToken={accessToken}&strDate={strDate}&endDate={endDate}")]

        public CategoryECOCollectionResponse GetCategoryECOCumulative(string accessToken, string strDate, string endDate)
        {
            var categoryECOCollection = new CategoryECOCollectionResponse();
            try
            {
                List<Model.CategoryECOModel> categoryList = ProductCategoryBR.Instance.GetCategoryECOCumulativeByAccessToken(accessToken, strDate, endDate);
                categoryECOCollection.ECOCollection = categoryList;
            }
            catch (Exception e)
            {
                categoryECOCollection.IsError = true;
                categoryECOCollection.ErrorMessage = e.Message;
            }
            return categoryECOCollection;
        }

        [WebInvoke(Method = "GET", ResponseFormat = WebMessageFormat.Json, UriTemplate = "GetProductECOCumulative?accessToken={accessToken}&strDate={strDate}&endDate={endDate}")]

        public ProductECOCollectionResponse GetProductECOCumulative(string accessToken, string strDate, string endDate)
        {
            var productECOCollection = new ProductECOCollectionResponse();
            try
            {
                List<Model.ProductECOModel> productList = ProductBR.Instance.GetProductECOCumulativeByAccessToken(accessToken, strDate, endDate);
                productECOCollection.ECOCollection = productList;
            }
            catch (Exception e)
            {
                productECOCollection.IsError = true;
                productECOCollection.ErrorMessage = e.Message;
            }
            return productECOCollection;
        }

        #endregion
    }

    public class OtpUser
    {
        public string userName { get; set; }
        public string otpNumber { get; set; }
    }
}


//10202 - //User Not Exsist
//10203 - //User Empty
//10204 - //Emei Empty
//10205 - //API Error
//10210 - //Logout
//10211 - //Pin Empty
