﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.DTO.Request
{
    public abstract class RequestBaseModel
    {
        public string AccessToken { get; set; }
    }
}