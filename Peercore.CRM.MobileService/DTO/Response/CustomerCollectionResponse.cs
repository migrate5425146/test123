﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Peercore.CRM.MobileService.Models;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class CustomerCollectionResponse : ResponseBaseModel
    {
        public List<Customer> CustomerCollection { get; set; }
    }

    public class UpdateCustomerDayOrderResponse : ResponseBaseModel
    {
        
    }

    
}