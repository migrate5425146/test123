﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Peercore.CRM.MobileService.Models;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class InvoiceResponse : ResponseBaseModel
    {
    }

    public class InvoiceCollection : ResponseBaseModel
    {
        public List<Invoice> Invoices { get; set; }
    }

    public class RepsTrackingForASM : ResponseBaseModel
    {
        public List<RepsTrackingDetails> RepsTrackingDetails { get; set; }
    }

    public class OutletWiseReportResponse : ResponseBaseModel
    {
        public List<RepsOutletWiseDetails> RepsOutletWiseDetails { get; set; }
    }
    
    public class SKUWiseReportResponse : ResponseBaseModel
    {
        public List<RepsSKUMaster> RepsSKUWiseDetails { get; set; }
    }
}