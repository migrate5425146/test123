﻿using Peercore.CRM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class OrigivatorVisitResponse : ResponseBaseModel
    {
    }

    public class OrigivatorAttendanceResponse : ResponseBaseModel
    {
    }

    public class ASMVisitCollection : ResponseBaseModel
    {
        public List<OrigivatorVisitModel> ASMVisits { get; set; }
    }
    
}