﻿using Peercore.CRM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class ModernTradeInsertResponse : ResponseBaseModel
    {
        public decimal ModernTradeId { get; set; }
    }

    public class ModernTradeResponse : ResponseBaseModel
    {
        public List<ModernTradeMasterModel> modernTrades { get; set; }
    }
}