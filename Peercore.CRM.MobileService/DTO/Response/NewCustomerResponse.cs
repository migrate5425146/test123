﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class NewCustomerResponse : ResponseBaseModel
    {
        public string CustCode { get; set; }
    }
}