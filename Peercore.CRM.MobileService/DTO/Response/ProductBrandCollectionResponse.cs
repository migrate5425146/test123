﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Peercore.CRM.MobileService.Models;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class ProductBrandCollectionResponse : ResponseBaseModel
    {
        public List<ProductBrand> BrandCollection { get; set; }
    }
}