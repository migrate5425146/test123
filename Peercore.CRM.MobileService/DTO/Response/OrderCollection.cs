﻿using Peercore.CRM.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class OrderResponse : ResponseBaseModel
    {
    }

    public class OrderCollection : ResponseBaseModel
    {
        public List<OrderModel> Orders { get; set; }
    }
    
    public class DistributerCollection : ResponseBaseModel
    {
        public List<DistributerModel> DistributerList { get; set; }
    }
}