﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Peercore.CRM.Model;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class CustomerECOCollectionResponse : ResponseBaseModel

    {
        public List<Model.CustomerECOModel> ECOCollection { get; set; }
    }
}