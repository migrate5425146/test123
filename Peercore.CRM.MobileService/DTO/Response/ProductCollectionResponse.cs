﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Peercore.CRM.MobileService.Models;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class ProductCollectionResponse : ResponseBaseModel
    {
        public List<Product> ProductCollection { get; set; }
    }

    public class ProductListResponse
    {
        public List<ProductNew> products { get; set; }
    }
}