﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Peercore.CRM.MobileService.Models;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class ProductCategoryCollectionResponse : ResponseBaseModel
    {
        public List<ProductCategory> CategoryCollection { get; set; }
    }

    public class ProductCategoriesResponse
    {
        public List<ProductCategoryNew> productCategories { get; set; }
    }
}