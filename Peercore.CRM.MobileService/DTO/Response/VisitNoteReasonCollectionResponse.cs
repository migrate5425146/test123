﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Peercore.CRM.MobileService.Models;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class VisitNoteReasonCollectionResponse : ResponseBaseModel
    {
        public List<VisitNoteReason> VisitNoteReasonCollection { get; set; }
    }

    public class VisitNoteCollection : ResponseBaseModel
    {
        public List<VisitNote> VisitCollection { get; set; }
    }
}