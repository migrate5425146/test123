﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class ProductBrandImageResponse : ResponseBaseModel
    {
        public byte[] Image { get; set; }
    }
}