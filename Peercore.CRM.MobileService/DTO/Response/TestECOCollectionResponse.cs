﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Peercore.CRM.Model;

namespace Peercore.CRM.MobileService.DTO.Response
{
    public class TestECOCollectionResponse : ResponseBaseModel

    {
        

        public class BrandECOCollectionResponse : ResponseBaseModel

        {
            public List<Model.BrandECOModel> ECOCollection { get; set; }
        }




        public class CategoryECOCollectionResponse : ResponseBaseModel

        {
            public List<Model.CategoryECOModel> ECOCollection { get; set; }
        }


        public class CustomerECOCollectionResponse : ResponseBaseModel

        {
            public List<Model.CustomerECOModel> ECOCollection { get; set; }
        }


        public class ProductECOCollectionResponse : ResponseBaseModel

        {
            public List<Model.ProductECOModel> ECOCollection { get; set; }
        }



    }



}