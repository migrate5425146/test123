﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
   public class ProductUOMModel
    {
        public int Id { get; set; }
        public int ProductId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public float Conversion { get; set; }
        public string Status { get; set; }
        public string FgCode { get; set; }
        public string Code { get; set; }
    }

    public class ProductUOMViewModel
    {
        public int ProductId { get; set; }
        public string FgCode { get; set; }
        public string Code { get; set; }
        public float CassesConversion { get; set; }
        public float TonnageConversion { get; set; }
        
    }
}
