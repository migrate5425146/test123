﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class TerritoryModel:BaseModel
    {
        public int TerritoryId { get; set; }
        public int AreaId { get; set; }
        public string AreaCode { get; set; }
        public string AreaName { get; set; }
        public string AreaStatus { get; set; }
        public string TerritoryCode { get; set; }
        public string TerritoryName { get; set; }
        public string RepCode { get; set; }
        public string RepName { get; set; }

        public string SchemeTerritoryStatus { get; set; }
        public bool CanAddOutlet { get; set; }
        public bool IsGeoFence { get; set; }
        public string ProductTerritoryStatus { get; set; }
    }

    public class DistributerTerritoryModel : BaseModel
    {
        public int TerritoryId { get; set; }
        public int AreaId { get; set; }
        public string AreaCode { get; set; }
        public string AreaName { get; set; }
        public string AreaStatus { get; set; }
        public string TerritoryCode { get; set; }
        public string TerritoryName { get; set; }
        public int AsmId { get; set; }
        public string AsmCode { get; set; }
        public string AsmName { get; set; }
        public string AsmStatus { get; set; }
        public int DistributerId { get; set; }
        public string DistributerCode { get; set; }
        public string DistributerName { get; set; }

    }
}
