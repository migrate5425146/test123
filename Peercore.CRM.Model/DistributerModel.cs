﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class DistributerModel : BaseModel
    {
        public int DistributorId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string DisplayName { get; set; }
        public string Originator { get; set; }
        public string Status { get; set; }
        public string PrefixCode { get; set; }
        public string ParentOriginator { get; set; }
        public string DeptString { get; set; }
        public string Password { get; set; }
        public string Adderess { get; set; }
        public string Mobile { get; set; }
        public string Telephone { get; set; }
        public string Email { get; set; }
        public bool IsNotEMEIUser { get; set; }
        public string NIC { get; set; }
    }
}
