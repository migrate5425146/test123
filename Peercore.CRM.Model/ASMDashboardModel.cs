﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class ASMDashboardModel
    {
        public string RepName { get; set; }
        public string RepCode { get; set; }
        public string RouteName { get; set; }
        public int AvailableOutlets { get; set; }
        public int CoveredOutlets { get; set; }
        public int PCCount { get; set; }
        public double SalesValue { get; set; }
        public double GumSales { get; set; }
        public double CandySales { get; set; }
        public double JellySales { get; set; }
        public double HVPSales { get; set; }
        public double Sales1300 { get; set; }
        public int Qty1300 { get; set; }
        public double SalesMLines { get; set; }
        public int QtyMLines { get; set; }
        public string FirstBillTime { get; set; }
        public string Echo { get; set; }
        public int VCCount { get; set; }
        public double GrossTotal { get; set; }
        public double DiscountTotal { get; set; }
    }
}
