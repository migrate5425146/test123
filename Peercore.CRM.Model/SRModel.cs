﻿using System;
using System.Collections.Generic;

namespace Peercore.CRM.Model
{
    public class SRModel : BaseModel
    {
        public int RepId { get; set; }
        public string RepCode { get; set; }
        public string RepName { get; set; }
        public int TerritoryId { get; set; }
        public string TerritoryStatus { get; set; }
        public string TerritoryCode { get; set; }
        public string TerritoryName { get; set; }
        public string UserName { get; set; }
        public string PrefixCode { get; set; }
        public string SupervisorCode { get; set; }
        public string State { get; set; }
        public int Include { get; set; }
        public string Telephone { get; set; }
        public string Fax { get; set; }
        public string Mobile { get; set; }
        public string Status { get; set; }
        public string AbnNo { get; set; }
        public int GstStatus { get; set; }
        public string CompanyName { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string Postcode { get; set; }
        public string PayMethod { get; set; }
        public string BrowseAvailable { get; set; }
        public string Originator { get; set; }
        public int DispInCrm { get; set; }
        public string RepType { get; set; }
        public int BusManagerFlag { get; set; }
        public int Version { get; set; }
        public double StickTarget { get; set; }
        public int TargetYear { get; set; }
        public int TargetMonth { get; set; }
        public string TargetMonthText { get; set; }
        public string Email { get; set; }
        public DateTime? JoinedDate { get; set; }
        public string GroupCode { get; set; }
        public bool IsNotEMEIUser { get; set; }
        public string DeptString { get; set; }
        public string Password { get; set; }
        public List<string> HolidaysList { get; set; }
        public string NIC { get; set; }
    }

    public class SalesRepSalesTypeModel : BaseModel
    {
        public int RepId { get; set; }
        public string RepCode { get; set; }
        public string RepName { get; set; }
        public string TerritoryCode { get; set; }
        public string TerritoryName { get; set; }
        public string AsmName { get; set; }
        public string DistributerCode { get; set; }
        public string DistributerName { get; set; }
        public int SalesType { get; set; }
    }

    public class SalesRepSyncModel : BaseModel
    {
        public bool IsRepSync { get; set; }
        public int RepId { get; set; }
        public string RepCode { get; set; }
        public string RepName { get; set; }
        public string TerritoryCode { get; set; }
        public string TerritoryName { get; set; }
        public string AsmName { get; set; }
        public string EmeiNo { get; set; }
        public string DeviceToken { get; set; }
        public bool DeliveryStatus { get; set; }
        public bool IsSynced { get; set; }
    }

    public class SalesRepSyncDetailModel : BaseModel
    {
        public int syncId { get; set; }
        public string repCode { get; set; }
        public string emeiNo { get; set; }
        public string mobileSyncTime { get; set; }
        public bool isAddCust { get; set; }
        public string descAddCust { get; set; }
        public bool isAddInv { get; set; }
        public string descAddInv { get; set; }
        public bool isAddVn { get; set; }
        public string descAddVn { get; set; }
        public bool isAddChkLst { get; set; }
        public string descChkLst { get; set; }
        public bool isAddOrder { get; set; }
        public string descOrder { get; set; }
        public bool isAddCredSettle { get; set; }
        public string descCredSettle { get; set; }
        public int territoryId { get; set; }
    }

    public class SalesRepRouteSequenceModel : BaseModel
    {
        public int SequenceId { get; set; }
        public string repCode { get; set; }
        public int invId { get; set; }
        public string invNo { get; set; }
        public double invTotal { get; set; }
        public string custCode { get; set; }      
        public string invLatitude { get; set; }
        public string invLongitude { get; set; }
        public string custName { get; set; }
        public string custTel { get; set; }
        public string custAddress { get; set; }
        public string custEmail { get; set; }
        public string custLatitude { get; set; }
        public string custLongitude { get; set; }
        public string invType { get; set; }

    }
}
