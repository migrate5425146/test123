﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class SyncModel
    {
    }

    public class NotificationResponse
    {
        public bool Status { get; set; }
        public string Message { get; set; }
        public Exception Error { get; set; }
    }

    public class SyncRequestModel
    {
        public string originator { get; set; }
        public string originatorType { get; set; }
        public string DeviceIMEI { get; set; }
    }
}
