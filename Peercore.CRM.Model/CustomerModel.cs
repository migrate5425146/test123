﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class CustomerModel : BaseModel
    {
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string OutletName { get; set; }

        /// <summary>
        /// Gets or sets the short name.
        /// </summary>
        /// <value>The short name.</value>
        public string ShortName { get; set; }

        /// <summary>
        /// Gets or sets the company.
        /// </summary>
        /// <value>The company.</value>
        public string Company { get; set; }

        /// <summary>
        /// Gets or sets the business.
        /// </summary>
        /// <value>The business.</value>
        public string Business { get; set; }

        /// <summary>
        /// Gets or sets the lead status.
        /// </summary>
        /// <value>The lead status.</value>
        public string LeadStatus { get; set; }

        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        /// <value>The state.</value>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the city.
        /// </summary>
        /// <value>The city.</value>
        public string City { get; set; }

        /// <summary>
        /// Gets or sets the address.
        /// </summary>
        /// <value>The address.</value>
        public string Address { get; set; }

        public string Address1 { get; set; }

        public string Address2 { get; set; }
        /// <summary>
        /// Gets or sets the postal code.
        /// </summary>
        /// <value>The postal code.</value>
        public string PostalCode { get; set; }

        /// <summary>
        /// Gets or sets the telephone.
        /// </summary>
        /// <value>The telephone.</value>
        public string Telephone { get; set; }

        /// <summary>
        /// Gets or sets the mobile.
        /// </summary>
        /// <value>The mobile.</value>
        public string Mobile { get; set; }

        /// <summary>
        /// Gets or sets the email.
        /// </summary>
        /// <value>The email.</value>
        public string Email { get; set; }

        /// <summary>
        /// Gets or sets the customer code.
        /// </summary>
        /// <value>The customer code.</value>
        public string OutletCode { get; set; }

        /// <summary>
        /// Gets or sets the fax.
        /// </summary>
        /// <value>The fax.</value>
        public string Fax { get; set; }

        /// <summary>
        /// Gets or sets the rep code.
        /// </summary>
        /// <value>The rep code.</value>
        public string RepCode { get; set; }

        public string PrimaryRepName { get; set; }
        public string SecondaryRepName { get; set; }


        // Additional Columns added to the Customer Table from Lead Table
        public string LeadSource { get; set; }
        public string Industry { get; set; }
        public string ReferredBy { get; set; }
        public int Rating { get; set; }
        public double Probability { get; set; }
        public string Website { get; set; }
        public int NoOfEmployees { get; set; }
        public double AnnualRevenue { get; set; }
        public string Description { get; set; }
        public string PreferredContact { get; set; }
        public string BusinessPotential { get; set; }
        public string Country { get; set; }
        public DateTime? LastCalledDate { get; set; }
        public string LitersBy { get; set; }
        public double PotentialLiters { get; set; }
        public string Status { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime? ModifiedDate { get; set; }
        public string CreditStatus { get; set; }
        public string EndUserCode { get; set; }
        public string Postcode { get; set; }
        public string Acn { get; set; }
        public string Abn { get; set; }
        public int GstOverride { get; set; }
        public string AreaCode { get; set; }
        public string DestArea { get; set; }
        public string InternetAdd { get; set; }
        public string PreTradCode { get; set; }
        public string PreTradName { get; set; }
        public string Contact { get; set; }
        public string Type { get; set; }
        public string ParRebateLvl { get; set; }
        public string PriceList { get; set; }
        public string Category { get; set; }
        public string PaymentMethod { get; set; }
        public double Discount { get; set; }
        public string RunNo { get; set; }
        public string RunCode { get; set; }
        public int RunOff { get; set; }
        public int DeliverySequence { get; set; }
        public string CustBusArea { get; set; }
        public string CustMarket { get; set; }
        public string CustGroup { get; set; }
        public string ArGroup { get; set; }
        public string CustSubGroup { get; set; }
        public string CustMarketSubGroup { get; set; }
        public string PriceGroup { get; set; }
        public string ParentCustomer { get; set; }
        public DateTime StartDate { get; set; }
        public string CarrierCode { get; set; }
        public string Freight { get; set; }
        public string BakeryRepCode { get; set; }
        public string TaxCert { get; set; }
        public string PrevCreditStatus { get; set; }
        public string PayeeNo { get; set; }
        public string CustData { get; set; }
        public string ForCur { get; set; }
        public string DelFlag { get; set; }
        public int Version { get; set; }
        public DateTime LastStmntDate { get; set; }
        public string PrintStmnt { get; set; }
        public int Coa { get; set; }
        public int EPeerKey { get; set; }
        public string AllocInProgress { get; set; }
        public string Bin1 { get; set; }
        public string Bin2 { get; set; }
        public string Dispatchmethod { get; set; }
        public string StandradFacilities { get; set; }
        public string ProductsServices { get; set; }
        public string Flag { get; set; }
        public string CreditStatusHistory { get; set; }
        public int SiteId { get; set; }
        public string BrowseAvailable { get; set; }
        public string Export { get; set; }
        public string MethodOfPay { get; set; }
        public string MapRef { get; set; }
        public string CopyExportDoc { get; set; }
        public int GracePeriod { get; set; }
        public string DefWarehouse { get; set; }
        public double BakRebPerc { get; set; }
        public string MixedPalletInd { get; set; }
        public string PalletType { get; set; }
        public string CustMarketSubGroupSection { get; set; }
        public string KeyAccount { get; set; }
        public string SplitPlistInd { get; set; }
        public DateTime RebateMonth { get; set; }
        public string BulkBinAcc1 { get; set; }
        public string BulkBinAcc2 { get; set; }
        public string BulkBinAcc3 { get; set; }
        public string MethodOfDelivery { get; set; }
        public string TradingTerm { get; set; }
        public int ReleaseOrderCount { get; set; }
        public string Correspondance { get; set; }
        public int SendToPayee { get; set; }
        public string DebtorEmail { get; set; }
        public int InvTerms { get; set; }
        public int MaxDeliveryTime { get; set; }
        public int IsRctiCustomer { get; set; }
        public string CustDesc { get; set; }
        public string PrefContact { get; set; }
        public string BusPotential { get; set; }
        public string CustType { get; set; }
        public string DeldocEmail { get; set; }

        public int RouteMasterId { get; set; }
        public string OutletNo { get; set; }
        public double OutstandingCredit { get; set; }

        public bool IsRetail { get; set; }

        public string CreatedOn { get; set; }

        public byte[] Document_Content { get; set; }

        public byte[] ImageContent { get; set; }

        public bool HasUnlimitedCredit { get; set; }

        public int OutletTypeId { get; set; }
        public string OutletTypeName { get; set; }
        public double Latitude { get; set; }
        public double Longitude { get; set; }
        public double OutStandingBalance { get; set; }
        public string GeoLocation { get; set; }
        public string RouteName { get; set; }
        public string DisCode { get; set; }
        public string DisName { get; set; }
        public string TerritoryName { get; set; }
        public string AreaName { get; set; }
        public string Asm { get; set; }
   

    }

    public class CustomerSequence
    {
        public string CustomerCode { get; set; }
        public int DayOrder { get; set; }
    }

    public class CustomerCategory: BaseModel
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
    }
}
