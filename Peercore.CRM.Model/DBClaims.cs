﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class DBClaimInvoiceHeaderModel
    {
        public decimal DBClaimInvoiceId { get; set; }
        public string DBClaimInvoiceNo { get; set; }
        public decimal TerritoryId { get; set; }
        public string TerritoryName { get; set; }
        public DateTime DBClaimInvoiceDate { get; set; }
        public DateTime DBClaimInvoiceCreatedDate { get; set; }
    }

    public class DBClaimInvoiceDetailsModel
    {
        public decimal DBClaimInvoiceId { get; set; }
        public  decimal ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public double ProductPrice { get; set; }
        public double ProductInvQty { get; set; }
        public double ProductInvValue { get; set; }
        public double ProductClaimQty { get; set; }
        public double ProductClaimValue { get; set; }
        public string Reason { get; set; }
    }
}
