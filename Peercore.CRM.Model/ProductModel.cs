﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class ProductModel : BaseModel
    {
        public int ProductId { get; set; }
        public string Code { get; set; }
        public string Name { get; set; }
        public string Status { get; set; }
        public string IsAvailable { get; set; }
        public string CustomerCode { get; set; }
        public string ProductStatus { get; set; }
        public double RetailPrice { get; set; }
        public double OutletPrice { get; set; }
        public int VanStock { get; set; }
        public double MaxDiscount { get; set; }
        public int Sku { get; set; }
        public int BrandId { get; set; }
        public int CategoryId { get; set; }
        public string BrandName { get; set; }
        public string CategoryName { get; set; }
        public int ProductPriceId { get; set; }
        public int DivisionId { get; set; }
        public string DivisionName { get; set; }
        public double Weight { get; set; }
        public bool IsHighValue { get; set; }
        public int FlavourId { get; set; }
        public int PackingId { get; set; }
        public byte[] ImageContent { get; set; }
        public double DistributorPrice { get; set; }
        public string FgCode { get; set; }
        public bool IsPromotion { get; set; }
        public int Qty { get; set; }
        public int ProductTerritoryId { get; set; }
        public string ProductTerritoryStatus { get; set; }
    }
}
