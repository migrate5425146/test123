﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class SalesRepAttendanceHoursModel
    {
        public string originator { get; set; }
        public string date { get; set; }
        public double AttendanceHours { get; set; }
        public double BillingHours { get; set; }
        public string AttendanceStart { get; set; }
        public string AttendanceEnd { get; set; }
        public string BillingStart { get; set; }
        public string BillingEnd { get; set; }
    }

    public class DashboardAttendanceByRepType
    {
        public string repType { get; set; }
        public string description { get; set; }
        public int attendanceCount { get; set; }
        public double attendancePercentage { get; set; }

    }

    public class DashboardAttendanceDetails
    {
        public string repCode { get; set; }
        public string repName { get; set; }
        public string salesType { get; set; }
        public string attendType { get; set; }
        public string checkInTime { get; set; }
        public string checkOutTime { get; set; }
        public string comment { get; set; }
        public string checkInLat { get; set; }
        public string checkInLng { get; set; }
        public string checkInAddress { get; set; }
        public string checkOutLat { get; set; }
        public string checkOutLng { get; set; }
        public string checkOutAddress { get; set; }
    }

    public class MobileRepsDashboardModel
    {
        public string originator { get; set; }
        public string BillVal1300 { get; set; }
    }
}