﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class OrderModel
    {
        public int OrderId { get; set; }
        public string OrderNo { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public double OrderTotal { get; set; }
        public double TotalDiscount { get; set; }
        public string OrderDate { get; set; }
        public string RequiredDate { get; set; }
        public bool Canceled { get; set; }
        public string Remark { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public string RouteName { get; set; }
        public string DistributorName { get; set; }
        public string RepCode { get; set; }
        public int RouteId { get; set; }
        public int TerritoryId { get; set; }
        public int AreaId { get; set; }
        public int RegionId { get; set; }
        public int DistributorId { get; set; }
        public int AsmId { get; set; }
        public int RsmId { get; set; }
        public string CreateBy { get; set; }
        public int DayCount { get; set; }
        public int RecordCount { get; set; }
        public bool IsInvoiceActivate { get; set; }
        public string payType { get; set; } = "";
        public List<OrderLineItem> OrderLineItemCollection { get; set; }
        public List<OrderDiscountSchemeGroup> DiscountSchemeGroupCollection { get; set; }
        public string SQuantitytype { get; set; }
    }

    public class OrderLineItem
    {
        public int OrderDetailId { get; set; }
        public int OrderId { get; set; }
        public int ProductID { get; set; }
        public bool IsGift { get; set; }
        public double ItemPrice { get; set; }
        public double Qty { get; set; }
        public double Total { get; set; }
        public double Discount { get; set; }
        public double DiscountValue { get; set; }
        public string ProductName { get; set; }
        public string Status { get; set; }
        public double QtyCasess { get; set; } // added by rangan

        public double QtyTonnage { get; set; } // added by rangan,

        public string SQuantitytype { get; set; }

    }

    public class OrderDiscountSchemeGroup
    {
        public int DiscountSchemeGroupID { get; set; }
        public int OrderId { get; set; }
        public double DiscountValue { get; set; }
        public int DiscountSchemeDetailID { get; set; }
        public string SchemeDetailName { get; set; }
        public List<OrderDiscountScheme> DiscountSchemeCollection { get; set; }
    }

    public class OrderDiscountScheme
    {
        public int OrderId { get; set; }
        public int ProductID { get; set; }
        public double UsedQty { get; set; }
        public double DiscountVal { get; set; }
        public int OrderSchemeId { get; set; }
        public int SchemeGroupId { get; set; }
        public string ProductName { get; set; }
    }

    public class OrderIsInvoiceActivate
    {
        public string originator { get; set; }
        public string originatorType { get; set; }
        public string orderId { get; set; }
        public bool isActive { get; set; }
    }
}
