﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class RouteMasterModel:BaseModel
    {
        public int RouteMasterId { get; set; }
        public string RouteMasterCode { get; set; }
        public string RouteMasterName { get; set; }
        public string Status { get; set; }
        public string RouteType { get; set; }
        public int TerritoryId { get; set; }
        public string TerritoryCode { get; set; }
        public string TerritoryName { get; set; }
        public string TerritoryStatus { get; set; }
        public int RepId { get; set; }
        public string RepCode { get; set; }
        public string RepName { get; set; }
        public string RepStatus { get; set; }

    }
}
