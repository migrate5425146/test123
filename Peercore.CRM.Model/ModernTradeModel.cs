﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class ModernTradeMasterModel
    {
        public decimal ModernTradeId { get; set; }
        public string ModernTradeCode { get; set; }
        public string ModernTradeDate { get; set; }
        public string RepCode { get; set; }
        public string CustomerCode { get; set; }
        public string CustomerName { get; set; }
        public bool Canceled { get; set; }
        public string Remark { get; set; }
        public double Longitude { get; set; }
        public double Latitude { get; set; }
        public int RouteId { get; set; }
        public string RouteName { get; set; }
        public int TerritoryId { get; set; }
        public int AreaId { get; set; }
        public int RegionId { get; set; }
        public string DistributorId { get; set; }
        public string AsmId { get; set; }
        public int RsmId { get; set; }
        public int RecordCount { get; set; }
        public bool IsSuccess { get; set; }
        public List<ModernTradeDetailsModel> ModernTradeItemCollection { get; set; }
    }

    public class ModernTradeDetailsModel
    {
        public decimal ModernTradeDetailId { get; set; }
        public decimal ModernTradeId { get; set; }
        public int ProductID { get; set; }
        public string ProductName { get; set; }
        public double Qty { get; set; }
        public string Status { get; set; }
    }

    public class ModernTradeListModel
    {
        public string AccessToken { get; set; }
        public ModernTradeMasterModel ModernTradeRequest { get; set; }
    }

}
