﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.Model
{
    public class TransactionLogModel
    {
        public string TransactionTime { get; set; }
        public string TransactionType { get; set; }
        public string TransactionModel { get; set; }
        public string TransactionDescription { get; set; }
        public string Latitude { get; set; } = "";
        public string Longitude { get; set; } = "";
    }
}
