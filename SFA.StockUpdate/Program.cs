﻿using Microsoft.Extensions.Configuration;
using SFA.StockUpdate.DAL;
using System;
using System.IO;

namespace SFA.StockUpdate
{
	class Program
	{
		private static IConfiguration _iconfiguration;

		static void Main(string[] args)
		{
			ReadAppSettings();
			UpdateStockOPBBF();
		}

		static void ReadAppSettings()
		{
			var builder = new ConfigurationBuilder()
								 .SetBasePath(Directory.GetCurrentDirectory())
								 .AddJsonFile("appSettings.json", optional: false, reloadOnChange: true);

			_iconfiguration = builder.Build();
		}

		static void UpdateStockOPBBF()
		{
			var stockDAL = new StockData(_iconfiguration);
			var updateStatus = stockDAL.UpdateStockOPBBF();
			
		}
	}
}
