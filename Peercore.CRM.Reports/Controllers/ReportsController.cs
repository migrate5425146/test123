﻿using Peercore.CRM.Reports.Models;
using Peercore.CRM.Reports.Models.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Peercore.CRM.Reports.Controllers
{
    public class ReportsController : Controller
    {
        // GET: Reports
        public ActionResult Index()
        {
            var model = new TerritoryModel();

            model.SelectOptions = new[]
            {
                new SelectListItem { Value = "1", Text = "item 1" },
                new SelectListItem { Value = "2", Text = "item 2" },
                new SelectListItem { Value = "3", Text = "item 3" },
            };

            return View(model);
        }

        public ActionResult AsmMaster()
        {
            return View();
        }




        public ReportViewModel LoadOption()
        {
            var p = new ReportViewModel();
            p.ReportTypeList = new List<ReportTypeModel>();// entities.ProductCategories.ToList();



            return p;
        }
    }
}