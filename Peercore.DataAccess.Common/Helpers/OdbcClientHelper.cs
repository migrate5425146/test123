﻿/// <summary>
///
/// Class Name  : OdbcClientHelper.cs
/// Project     : Peercore.DataAccess.Common
/// Author      : Kushan Fernando
/// Created On  : 6/19/2014 10:54:06 AM
///
/// Copyright (©) Peercore Information Technology.
///
/// All rights are reserved. Reproduction or transmission in whole or in part,
/// in any form or by any means, electronic, mechanical or otherwise,
/// is prohibited without the prior written consent of the copyright owner.
///
/// </summary>

#region - References -
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text; 
#endregion

namespace Peercore.DataAccess.Common.Helpers
{
    /// <summary>
    /// OdbcClientHelper
    /// </summary>
    internal class OdbcClientHelper : DbClientHelper
    {

    }
}
