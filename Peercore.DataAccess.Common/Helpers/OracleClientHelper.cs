﻿/// <summary>
///
/// Class Name  : OracleClientHelper.cs
/// Project     : Peercore.DataAccess.Common
/// Author      : Hirantha Gunawardana
/// Created On  : 1/28/2014 4:28:15 PM
///
/// Copyright (©) Peercore Information Technology.
///
/// All rights are reserved. Reproduction or transmission in whole or in part,
/// in any form or by any means, electronic, mechanical or otherwise,
/// is prohibited without the prior written consent of the copyright owner.
///
/// </summary>

#region - References -
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
//using Oracle.DataAccess.Client;
using Peercore.DataAccess.Common.Parameters; 
#endregion

namespace Peercore.DataAccess.Common.Helpers
{
    /// <summary>
    /// 
    /// </summary>
    internal class OracleClientHelper : DbClientHelper
    {
        /// <summary>
        /// Executes the query command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <param name="commandBehavior">The command behavior.</param>
        /// <returns></returns>
        public override DbDataReader ExecuteQueryCommand(DbCommand command, List<DbInputParameter> parameters, CommandBehavior commandBehavior = CommandBehavior.Default)
        {
            if (command.CommandType == CommandType.StoredProcedure)
            {
                OracleCommand oracleCommand = new OracleCommand(command.CommandText);
                oracleCommand.Connection = new OracleConnection(command.Connection.ConnectionString);
                oracleCommand.CommandType = command.CommandType;
                oracleCommand.CommandTimeout = command.CommandTimeout;

                if (oracleCommand.Connection.State == ConnectionState.Closed)
                    oracleCommand.Connection.Open();

                AttachParameters(command, parameters);

                OracleParameter oracleParameter = new OracleParameter();

                oracleParameter.ParameterName = "retval";
                oracleParameter.OracleDbType = OracleDbType.RefCursor;
                oracleParameter.Direction = ParameterDirection.Output;
                oracleCommand.Parameters.Add(oracleParameter);

                OracleDataReader oracleDataReader = oracleCommand.ExecuteReader(commandBehavior);

                return oracleDataReader;
            }
            else
            {
                return base.ExecuteQueryCommand(command, parameters, commandBehavior);
            }


        }

        /// <summary>
        /// Executes the non query command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public override int ExecuteNonQueryCommand(DbCommand command, List<DbInputParameter> parameters)
        {
            return base.ExecuteNonQueryCommand(command, parameters);
        }

        /// <summary>
        /// Executes the scalar command.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="parameters">The parameters.</param>
        /// <returns></returns>
        public override object ExecuteScalarCommand(DbCommand command, List<DbInputParameter> parameters)
        {
            return base.ExecuteScalarCommand(command, parameters);
        }

        /// <summary>
        /// Attaches the parameters.
        /// </summary>
        /// <param name="command">The command.</param>
        /// <param name="parameters">The parameters.</param>
        public override void AttachParameters(DbCommand command, List<Parameters.DbInputParameter> parameters)
        {
            if (parameters != null)
            {
                foreach (DbInputParameter parameter in parameters)
                {
                    DbParameter dbParameter = command.CreateParameter();
                    dbParameter.ParameterName = parameter.Name.Replace('@',':');
                    dbParameter.DbType = parameter.DataType;
                    dbParameter.Value = parameter.Value;
                    command.Parameters.Add(dbParameter);
                }
            }

        }

        /// <summary>
        /// Formats the database command.
        /// </summary>
        /// <param name="command">The command.</param>
        protected override void FormatDbCommand(DbCommand command)
        {
            command.CommandText = command.CommandText.Replace('@', ':');
        }
    }
}
