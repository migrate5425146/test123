﻿/// <summary>
///
/// Class Name  : DbInputParameter.cs
/// Project     : Peercore.DataAccess.Common
/// Author      : Hirantha Gunawardana
/// Created On  : 1/28/2014 4:31:33 PM
///
/// Copyright (©) Peercore Information Technology.
///
/// All rights are reserved. Reproduction or transmission in whole or in part,
/// in any form or by any means, electronic, mechanical or otherwise,
/// is prohibited without the prior written consent of the copyright owner.
///
/// </summary>

#region - References -
using System.Data;
using System; 
#endregion

namespace Peercore.DataAccess.Common.Parameters
{
    /// <summary>
    /// DbInputParameter
    /// </summary>
    public class DbInputParameter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DbInputParameter"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public DbInputParameter(string name, object value)
        {
            this.name = name;
            this.value = value ?? DBNull.Value;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DbInputParameter"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <param name="dataType">Type of the data.</param>
        public DbInputParameter(string name, DbType dataType,object value )
        {            
            this.name = name;
            this.value = value ?? DBNull.Value;
            this.dataType = dataType;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="DbInputParameter"/> class.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <param name="dataType">Type of the data.</param>
        /// <param name="parameterDirection">Parameter Direction.</param>
        public DbInputParameter(string name, DbType dataType, object value, ParameterDirection parameterDirection)
        {
            this.name = name;
            this.value = value ?? DBNull.Value;
            this.dataType = dataType;
            this.parameterDirection = parameterDirection;
        }

        private string name;
        /// <summary>
        /// Gets or sets the name.
        /// </summary>
        /// <value>The name.</value>
        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private object value;
        /// <summary>
        /// Gets or sets the value.
        /// </summary>
        /// <value>The value.</value>
        public object Value
        {
            get { return value; }
            set { this.value = value; }
        }

        private ParameterDirection parameterDirection;
        /// <summary>
        /// Gets or sets the ParameterDirection.
        /// </summary>
        /// <value>The type of the data.</value>
        public ParameterDirection ParameterDirection
        {
            get { return parameterDirection; }
            set { parameterDirection = value; }
        }

        private DbType dataType;
        /// <summary>
        /// Gets or sets the type of the data.
        /// </summary>
        /// <value>The type of the data.</value>
        public DbType DataType
        {
            get { return dataType; }
            set { dataType = value; }
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <param name="dataType">Type of the data.</param>
        /// <returns></returns>
        public static DbInputParameter GetInstance(string name, object value, DbType dataType)
        {
            return new DbInputParameter(name, dataType, value);
        }

        /// <summary>
        /// Gets the instance.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="dataType">Type of the data.</param>
        /// <param name="value">The value.</param>
        /// <returns></returns>
        public static DbInputParameter GetInstance(string name, DbType dataType,object value)
        {
            return new DbInputParameter(name, dataType, value);
        }
    }
}
