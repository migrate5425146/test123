﻿/// <summary>
///
/// Class Name  : DbInputParameterCollection.cs
/// Project     : Peercore.DataAccess.Common
/// Author      : Hirantha Gunawardana
/// Created On  : 1/28/2014 4:31:42 PM
///
/// Copyright (©) Peercore Information Technology.
///
/// All rights are reserved. Reproduction or transmission in whole or in part,
/// in any form or by any means, electronic, mechanical or otherwise,
/// is prohibited without the prior written consent of the copyright owner.
///
/// </summary>

#region - References -
using System.Data; 
#endregion

namespace Peercore.DataAccess.Common.Parameters
{
    /// <summary>
    /// DbInputParameterCollection
    /// </summary>
    public class DbInputParameterCollection : System.Collections.Generic.List<DbInputParameter>
    {
        /// <summary>
        /// Adds the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        public void Add(string name, object value)
        {
            this.Add(new DbInputParameter(name, value));
        }

        /// <summary>
        /// Adds the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <param name="dataType">Type of the data.</param>
        public void Add(string name, object value, DbType dataType)
        {
            this.Add(new DbInputParameter(name,dataType, value));
        }

        /// <summary>
        /// Adds the specified name.
        /// </summary>
        /// <param name="name">The name.</param>
        /// <param name="value">The value.</param>
        /// <param name="dataType">Type of the data.</param>
        public void Add(string name, object value, DbType dataType, ParameterDirection parameterDirection)
        {
            this.Add(new DbInputParameter(name, dataType, value, parameterDirection));
        }
    }
}
