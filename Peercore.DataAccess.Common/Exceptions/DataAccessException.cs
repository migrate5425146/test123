﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Peercore.DataAccess.Common.Exceptions
{
    public class ConnectionFiledException : Exception
    {
        public ConnectionFiledException() { }

        public ConnectionFiledException(string format, params object[] args) 
            : base(string.Format(format, args)) { }

        public ConnectionFiledException(string message, Exception innerException) 
            : base(message, innerException) { }


        public override string Message
        {
            get
            {
                return base.Message;
            }
        }

    }

    public class QueryExecutionException : Exception
    {
        public QueryExecutionException() { }

        public QueryExecutionException(string message) : base(message) { }

        public QueryExecutionException(string format, params object[] args) 
            : base(string.Format(format, args)) { }

        public QueryExecutionException(string message, Exception innerException) 
            : base(message, innerException) { }


        public override string Message
        {
            get
            {
                return base.Message;
            }
        }

    }

    public class DataProviderNotFoundException : Exception
    {
        public DataProviderNotFoundException() { }

        public DataProviderNotFoundException(string message) : base(message) { }

        public DataProviderNotFoundException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public DataProviderNotFoundException(string message, Exception innerException)
            : base(message, innerException) { }


        public override string Message
        {
            get
            {
                return base.Message;
            }
        }

    }

    public class XmlSqlStructureException : Exception
    {
        public XmlSqlStructureException() { }

        public XmlSqlStructureException(string message) : base(message) { }

        public XmlSqlStructureException(string format, params object[] args)
            : base(string.Format(format, args)) { }

        public XmlSqlStructureException(string message, Exception innerException)
            : base(message, innerException) { }


        public override string Message
        {
            get
            {
                return base.Message;
            }
        }

    }

}
