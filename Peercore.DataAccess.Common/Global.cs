﻿/// <summary>
///
/// Class Name  : Global.cs
/// Project     : Peercore.DataAccess.Common
/// Author      : Kushan Fernando
/// Created On  : 6/19/2014 11:24:15 AM
///
/// Copyright (©) Peercore Information Technology.
///
/// All rights are reserved. Reproduction or transmission in whole or in part,
/// in any form or by any means, electronic, mechanical or otherwise,
/// is prohibited without the prior written consent of the copyright owner.
///
/// </summary>

namespace Peercore.DataAccess.Common
{
    /// <summary>
    /// Global
    /// </summary>
    internal static class Global
    {
        /// <summary>
        /// The ingres client
        /// </summary>
        //public const string IngresClient = "Ingres.Client";

        /// <summary>
        /// The SQL client
        /// </summary>
        public const string SqlClient = "System.Data.SqlClient";

        /// <summary>
        /// The oracle client
        /// </summary>
        //public const string OracleClient = "System.Data.OracleClient";

        /// <summary>
        /// The SQL light client
        /// </summary>
        public const string SqlLightClient = "System.Data.SQLite";

        /// <summary>
        /// The ODBC client
        /// </summary>
        public const string OdbcClient = "System.Data.Odbc";
        
        /// <summary>
        /// The application version
        /// </summary>
        public const string ApplicationVersion = "2.0";
        
        /// <summary>
        /// The company name
        /// </summary>
        public const string CompanyName = "Peercore Information Technology";
        
        /// <summary>
        /// The copy right
        /// </summary>
        public const string CopyRight = "© 2014 Peercore Information Technology";

    }
}
