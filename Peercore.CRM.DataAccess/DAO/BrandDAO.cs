﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.CRM.Model;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public class BrandDAO:BaseDAO
    {
        private DbSqlAdapter BrandSql { get; set; }

        private void RegisterSql()
        {
            this.BrandSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.BrandSql.xml", ApplicationService.Instance.DbProvider);
        }

        public BrandDAO()
        {
            RegisterSql();
        }

        public BrandDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public BrandEntity CreateObject()
        {
            return BrandEntity.CreateObject();
        }

        public BrandEntity CreateObject(int entityId)
        {
            return BrandEntity.CreateObject(entityId);
        }

        public bool InsertBrand(BrandEntity brandEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drBrand = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Code", brandEntity.BrandCode, DbType.String);
                paramCollection.Add("@Name", brandEntity.BrandName, DbType.String);
                paramCollection.Add("@CreatedBy", brandEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(BrandSql["InsertBrand"], paramCollection);                           

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }

        public bool UpdateBrand(BrandEntity brandEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drBrand = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@BrandId", brandEntity.BrandId, DbType.Int32);
                paramCollection.Add("@Code", brandEntity.BrandCode, DbType.String);
                paramCollection.Add("@Name", brandEntity.BrandName, DbType.String);
                paramCollection.Add("@LastModifiedBy", brandEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(BrandSql["UpdateBrand"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }

        public List<BrandEntity> GetAllBrandsDataAndCount(ArgsEntity args)
        {
            DbDataReader drBrand = null;

            try
            {
                List<BrandEntity> brandEntityList = new List<BrandEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Int32);

                drBrand = this.DataAcessService.ExecuteQuery(BrandSql["GetAllBrandsDataAndCount"], paramCollection);

                if (drBrand != null && drBrand.HasRows)
                {
                    int idOrdinal = drBrand.GetOrdinal("id");
                    int codeOrdinal = drBrand.GetOrdinal("code");
                    int nameOrdinal = drBrand.GetOrdinal("name");
                    int statusOrdinal = drBrand.GetOrdinal("status");
                    int createdByOrdinal = drBrand.GetOrdinal("created_by");
                    int createdDateOrdinal = drBrand.GetOrdinal("created_date");
                    int lastModifiedByOrdinal = drBrand.GetOrdinal("last_modified_by");
                    int lastModifiedDateOrdinal = drBrand.GetOrdinal("last_modified_date");
                    int totalCountOrdinal = drBrand.GetOrdinal("total_count");

                    while (drBrand.Read())
                    {
                        BrandEntity brandEntity = CreateObject();

                        if (!drBrand.IsDBNull(idOrdinal)) brandEntity.BrandId = drBrand.GetInt32(idOrdinal);
                        if (!drBrand.IsDBNull(codeOrdinal)) brandEntity.BrandCode = drBrand.GetString(codeOrdinal);
                        if (!drBrand.IsDBNull(nameOrdinal)) brandEntity.BrandName = drBrand.GetString(nameOrdinal);
                        if (!drBrand.IsDBNull(statusOrdinal)) brandEntity.Status = drBrand.GetString(statusOrdinal);
                        if (!drBrand.IsDBNull(createdByOrdinal)) brandEntity.CreatedBy = drBrand.GetString(createdByOrdinal);
                        if (!drBrand.IsDBNull(createdDateOrdinal)) brandEntity.CreatedDate = drBrand.GetDateTime(createdDateOrdinal);
                        if (!drBrand.IsDBNull(lastModifiedByOrdinal)) brandEntity.LastModifiedBy = drBrand.GetString(lastModifiedByOrdinal);
                        if (!drBrand.IsDBNull(lastModifiedDateOrdinal)) brandEntity.LastModifiedDate = drBrand.GetDateTime(lastModifiedDateOrdinal);
                        if (!drBrand.IsDBNull(totalCountOrdinal)) brandEntity.TotalCount = drBrand.GetInt32(totalCountOrdinal);

                        brandEntityList.Add(brandEntity);
                    }
                }

                return brandEntityList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }

        public bool IsBrandCodeExists(BrandEntity brandEntity)
        {
            DbDataReader drBrand = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();


                paramCollection.Add("@BrandId", brandEntity.BrandId, DbType.Int32);
                paramCollection.Add("@BrandCode", brandEntity.BrandCode, DbType.String);

                drBrand = this.DataAcessService.ExecuteQuery(BrandSql["BrandCodeExists"], paramCollection);

                if (drBrand != null && drBrand.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }

        public bool IsBrandNameExists(BrandEntity brandEntity)
        {
            DbDataReader drBrand = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();


                paramCollection.Add("@BrandId", brandEntity.BrandId, DbType.Int32);
                paramCollection.Add("@BrandName", brandEntity.BrandName, DbType.String);

                drBrand = this.DataAcessService.ExecuteQuery(BrandSql["BrandNameExists"], paramCollection);

                if (drBrand != null && drBrand.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }

        public List<BrandEntity> GetAllBrands(string accessToken, string syncDate)
        {
            DbDataReader drBrand = null;

            try
            {
                List<BrandEntity> brandEntityList = new List<BrandEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@AccessToken", accessToken, DbType.String);
                paramCollection.Add("@SyncDate", syncDate, DbType.String);

                drBrand = this.DataAcessService.ExecuteQuery(BrandSql["GetAllBrands"], paramCollection);

                if (drBrand != null && drBrand.HasRows)
                {
                    int idOrdinal = drBrand.GetOrdinal("id");
                    int codeOrdinal = drBrand.GetOrdinal("code");
                    int nameOrdinal = drBrand.GetOrdinal("name");
                    int imageContentOrdinal = drBrand.GetOrdinal("image_content");
                    int statusOrdinal = drBrand.GetOrdinal("status");

                    while (drBrand.Read())
                    {
                        BrandEntity brandEntity = CreateObject();

                        if (!drBrand.IsDBNull(idOrdinal)) brandEntity.BrandId = drBrand.GetInt32(idOrdinal);
                        if (!drBrand.IsDBNull(codeOrdinal)) brandEntity.BrandCode = drBrand.GetString(codeOrdinal);
                        if (!drBrand.IsDBNull(nameOrdinal)) brandEntity.BrandName = drBrand.GetString(nameOrdinal);
                        if (!drBrand.IsDBNull(statusOrdinal)) brandEntity.Status = drBrand.GetString(statusOrdinal);
                        if (drBrand[imageContentOrdinal] != null)
                        {
                            if (!drBrand.IsDBNull(imageContentOrdinal)) brandEntity.ImageContent = (byte[])drBrand[imageContentOrdinal];
                        }

                        brandEntityList.Add(brandEntity);
                    }
                }

                return brandEntityList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }

        public byte[] GetBrandByAccessTokenAndId(string accessToken, int brandId)
        {
            DbDataReader drBrand = null;

            try
            {
                byte[] image = null;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@AccessToken", accessToken, DbType.String);
                paramCollection.Add("@BrandId", brandId, DbType.Int32);

                drBrand = this.DataAcessService.ExecuteQuery(BrandSql["GetBrandByAccessTokenAndId"], paramCollection);

                if (drBrand != null && drBrand.HasRows)
                {
                    int imageContentOrdinal = drBrand.GetOrdinal("image_content");

                    if (drBrand.Read())
                    {
                        if (drBrand[imageContentOrdinal] != null)
                        {
                            if (!drBrand.IsDBNull(imageContentOrdinal)) image = (byte[])drBrand[imageContentOrdinal];
                        }
                    }
                }

                return image;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }



        public bool update_brand_test(int id, byte[] content)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drBrand = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ID", id, DbType.Int32);
                paramCollection.Add("@documentContent", content, DbType.Binary);

                iNoRec = this.DataAcessService.ExecuteNonQuery(BrandSql["update_brand_test"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }

        public bool DeleteBrand(int brandId)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drBrand = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@BrandId", brandId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(BrandSql["DeleteBrand"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }




        public List<BrandECOModel> GetBrandECOCumulativeByAccessToken(string accessToken,string strDate, string endDate)
        {
            BrandECOModel brandEntity = null;
            DataTable objDS = new DataTable();
            List<BrandECOModel> BrandList = new List<BrandECOModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetBrandECOCumulative";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);
                    objCommand.Parameters.AddWithValue("@StartDate", strDate);
                    objCommand.Parameters.AddWithValue("@EndDate", endDate);



                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            brandEntity = new BrandECOModel();

                            if (item["BrnNm"] != DBNull.Value) brandEntity.BrandName = item["BrnNm"].ToString();

                            //if (item["ProNm"] != DBNull.Value) brandEntity.ProductName = item["ProNm"].ToString();

                            if (item["isPC"] != DBNull.Value)
                            {
                                string isPCValue = item["isPC"].ToString();
                                if (isPCValue == "1" || isPCValue.Equals("true", StringComparison.OrdinalIgnoreCase))
                                {
                                    brandEntity.isPC = true;
                                }
                                else if (isPCValue == "0" || isPCValue.Equals("false", StringComparison.OrdinalIgnoreCase))
                                {
                                    brandEntity.isPC = false;
                                }
                                else
                                {
                                    // Handle any other cases or set a default value
                                    brandEntity.isPC = false; // Or set a default value
                                }
                            }






                            BrandList.Add(brandEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return BrandList;
        }




    }
}
