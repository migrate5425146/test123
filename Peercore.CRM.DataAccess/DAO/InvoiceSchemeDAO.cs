﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public class InvoiceSchemeDAO :BaseDAO 
    {
        private DbSqlAdapter InvoiceSchemeSql { get; set; }

        private void RegisterSql()
        {
            this.InvoiceSchemeSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.InvoiceSchemeSql.xml", ApplicationService.Instance.DbProvider);
        }

        public InvoiceSchemeDAO()
        {
            RegisterSql();
        }

        public InvoiceSchemeDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public InvoiceSchemeEntity CreateObject()
        {
            return InvoiceSchemeEntity.CreateObject();
        }

        public List<InvoiceSchemeEntity> GetDiscountSchemeByInvoiceId(int schemeGroupId)
        {
            DbDataReader drInvoiceHeader = null;

            try
            {
                List<InvoiceSchemeEntity> InvoiceSchemeList = new List<InvoiceSchemeEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@SchemeGroupId", schemeGroupId, DbType.Int32);

                drInvoiceHeader = this.DataAcessService.ExecuteQuery(InvoiceSchemeSql["GetDiscountSchemeByInvoiceId"], paramCollection);

                if (drInvoiceHeader != null && drInvoiceHeader.HasRows)
                {
                    int idOrdinal = drInvoiceHeader.GetOrdinal("invoice_scheme_id");
                    //int invoiceIdOrdinal = drInvoiceHeader.GetOrdinal("invoice_id");
                    int schemeGroupIdOrdinal = drInvoiceHeader.GetOrdinal("scheme_group_id");
                    int productIdOrdinal = drInvoiceHeader.GetOrdinal("product_id");
                    //int schemeDetailsIdOrdinal = drInvoiceHeader.GetOrdinal("scheme_details_id");
                    int usedQtyOrdinal = drInvoiceHeader.GetOrdinal("used_qty");
                    int productNameOrdinal = drInvoiceHeader.GetOrdinal("name");

                    InvoiceSchemeEntity invoiceSchemeEntity = null;

                    while (drInvoiceHeader.Read())
                    {
                        invoiceSchemeEntity = CreateObject();

                        if (!drInvoiceHeader.IsDBNull(idOrdinal)) invoiceSchemeEntity.InvoiceSchemeId = drInvoiceHeader.GetInt32(idOrdinal);
                        //if (!drInvoiceHeader.IsDBNull(invoiceIdOrdinal)) invoiceSchemeEntity.InvoiceId = drInvoiceHeader.GetInt32(invoiceIdOrdinal);
                        if (!drInvoiceHeader.IsDBNull(schemeGroupIdOrdinal)) invoiceSchemeEntity.SchemeGroupId = drInvoiceHeader.GetInt32(schemeGroupIdOrdinal);
                        if (!drInvoiceHeader.IsDBNull(productIdOrdinal)) invoiceSchemeEntity.ProductId = drInvoiceHeader.GetInt32(productIdOrdinal);                        
                        if (!drInvoiceHeader.IsDBNull(usedQtyOrdinal)) invoiceSchemeEntity.UsedQuantity = drInvoiceHeader.GetDouble(usedQtyOrdinal);
                        if (!drInvoiceHeader.IsDBNull(productNameOrdinal)) invoiceSchemeEntity.ProductName = drInvoiceHeader.GetString(productNameOrdinal);

                        InvoiceSchemeList.Add(invoiceSchemeEntity);
                    }
                }

                return InvoiceSchemeList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drInvoiceHeader != null)
                    drInvoiceHeader.Close();
            }
        }

        public bool InsertInvoiceDiscountScheme(InvoiceSchemeEntity invoiceSchemeEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drBrand = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                //paramCollection.Add("@InvoiceId", invoiceSchemeEntity.InvoiceId, DbType.Int32);
                paramCollection.Add("@Productid", invoiceSchemeEntity.ProductId, DbType.Int32);
                paramCollection.Add("@SchemeGroupId", invoiceSchemeEntity.SchemeGroupId, DbType.Int32);
                paramCollection.Add("@DiscountVal", invoiceSchemeEntity.DiscountVal, DbType.Double);
                paramCollection.Add("@UsedQty", invoiceSchemeEntity.UsedQuantity, DbType.Double);

                iNoRec = this.DataAcessService.ExecuteNonQuery(InvoiceSchemeSql["InsertInvoiceDiscountScheme"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }

        public bool DeleteInvoiceDiscountScheme(int invoiceId)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@InvoiceId", invoiceId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(InvoiceSchemeSql["DeleteInvoiceDiscountScheme"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public List<InvoiceSchemeGroupEntity> GetInvoiceSchemeGroupByInvoiceId(int invoiceId)
        {
            //DbDataReader drInvoiceHeader = null;

            //try
            //{
            //    List<InvoiceSchemeGroupEntity> InvoiceSchemeGroupList = new List<InvoiceSchemeGroupEntity>();

            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@InvoiceId", invoiceId, DbType.Int32);

            //    drInvoiceHeader = this.DataAcessService.ExecuteQuery(InvoiceSchemeSql["GetInvoiceSchemeGroupByInvoiceId"], paramCollection);

            //    if (drInvoiceHeader != null && drInvoiceHeader.HasRows)
            //    {
            //        int schemeGroupIdOrdinal = drInvoiceHeader.GetOrdinal("scheme_group_id");
            //        int invoiceIdOrdinal = drInvoiceHeader.GetOrdinal("invoice_id");
            //        int schemeDetailsIdOrdinal = drInvoiceHeader.GetOrdinal("scheme_details_id");
            //        int discountValueOrdinal = drInvoiceHeader.GetOrdinal("discount_value");
            //        int schemeDetailsOrdinal = drInvoiceHeader.GetOrdinal("scheme_details");

            //        InvoiceSchemeGroupEntity invoiceSchemeGroupEntity = null;

            //        while (drInvoiceHeader.Read())
            //        {
            //            invoiceSchemeGroupEntity = InvoiceSchemeGroupEntity.CreateObject();

            //            if (!drInvoiceHeader.IsDBNull(schemeGroupIdOrdinal)) invoiceSchemeGroupEntity.SchemeGroupId = drInvoiceHeader.GetInt32(schemeGroupIdOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(invoiceIdOrdinal)) invoiceSchemeGroupEntity.InvoiceId = drInvoiceHeader.GetInt32(invoiceIdOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(schemeDetailsIdOrdinal)) invoiceSchemeGroupEntity.SchemeDetailsId = drInvoiceHeader.GetInt32(schemeDetailsIdOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(discountValueOrdinal)) invoiceSchemeGroupEntity.DiscountValue = drInvoiceHeader.GetDouble(discountValueOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(schemeDetailsOrdinal)) invoiceSchemeGroupEntity.SchemeDetailName = drInvoiceHeader.GetString(schemeDetailsOrdinal);
                        
            //            InvoiceSchemeGroupList.Add(invoiceSchemeGroupEntity);
            //        }
            //    }

            //    return InvoiceSchemeGroupList;
            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (drInvoiceHeader != null)
            //        drInvoiceHeader.Close();
            //}

            //*************************Add By Irosh Fernando 2018/10/3************************************

            InvoiceSchemeGroupEntity invoiceSchemeGroupEntity = null;
            DataTable objDS = new DataTable();
            List<InvoiceSchemeGroupEntity> InvoiceSchemeGroupList = new List<InvoiceSchemeGroupEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetInvoiceSchemeGroupByInvoiceId";

                    objCommand.Parameters.AddWithValue("@InvoiceId", invoiceId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            invoiceSchemeGroupEntity = InvoiceSchemeGroupEntity.CreateObject();
                            if (item["scheme_group_id"] != DBNull.Value) invoiceSchemeGroupEntity.SchemeGroupId = Convert.ToInt32(item["scheme_group_id"]);
                            if (item["invoice_id"] != DBNull.Value) invoiceSchemeGroupEntity.InvoiceId = Convert.ToInt32(item["invoice_id"]);
                            if (item["scheme_details_id"] != DBNull.Value) invoiceSchemeGroupEntity.SchemeDetailsId = Convert.ToInt32(item["scheme_details_id"]);
                            if (item["discount_value"] != DBNull.Value) invoiceSchemeGroupEntity.DiscountValue = Convert.ToDouble(item["discount_value"]);
                            if (item["scheme_details"] != DBNull.Value) invoiceSchemeGroupEntity.SchemeDetailName = item["scheme_details"].ToString();
                            InvoiceSchemeGroupList.Add(invoiceSchemeGroupEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return InvoiceSchemeGroupList;

            //*************************Add By Irosh Fernando 2018/10/3************************************
        }

        public List<InvoiceSchemeGroupEntity> GetInvoiceSchemeTotDiscountByInvoiceId(int invoiceId)
        {
            DbDataReader drInvoiceHeader = null;

            try
            {
                List<InvoiceSchemeGroupEntity> InvoiceSchemeGroupList = new List<InvoiceSchemeGroupEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@InvoiceId", invoiceId, DbType.Int32);

                drInvoiceHeader = this.DataAcessService.ExecuteQuery(InvoiceSchemeSql["GetInvoiceSchemeTotDiscountByInvoiceId"], paramCollection);

                if (drInvoiceHeader != null && drInvoiceHeader.HasRows)
                {
                    int schemeGroupIdOrdinal = drInvoiceHeader.GetOrdinal("scheme_group_id");
                    int invoiceIdOrdinal = drInvoiceHeader.GetOrdinal("invoice_id");
                    int schemeDetailsIdOrdinal = drInvoiceHeader.GetOrdinal("scheme_details_id");
                    int discountValueOrdinal = drInvoiceHeader.GetOrdinal("discount_value");
                    int schemeDetailsOrdinal = drInvoiceHeader.GetOrdinal("scheme_details");

                    InvoiceSchemeGroupEntity invoiceSchemeGroupEntity = null;

                    while (drInvoiceHeader.Read())
                    {
                        invoiceSchemeGroupEntity = InvoiceSchemeGroupEntity.CreateObject();

                        if (!drInvoiceHeader.IsDBNull(schemeGroupIdOrdinal)) invoiceSchemeGroupEntity.SchemeGroupId = drInvoiceHeader.GetInt32(schemeGroupIdOrdinal);
                        if (!drInvoiceHeader.IsDBNull(invoiceIdOrdinal)) invoiceSchemeGroupEntity.InvoiceId = drInvoiceHeader.GetInt32(invoiceIdOrdinal);
                        if (!drInvoiceHeader.IsDBNull(schemeDetailsIdOrdinal)) invoiceSchemeGroupEntity.SchemeDetailsId = drInvoiceHeader.GetInt32(schemeDetailsIdOrdinal);
                        if (!drInvoiceHeader.IsDBNull(discountValueOrdinal)) invoiceSchemeGroupEntity.DiscountValue = drInvoiceHeader.GetDouble(discountValueOrdinal);
                        if (!drInvoiceHeader.IsDBNull(schemeDetailsOrdinal)) invoiceSchemeGroupEntity.SchemeDetailName = drInvoiceHeader.GetString(schemeDetailsOrdinal);

                        InvoiceSchemeGroupList.Add(invoiceSchemeGroupEntity);
                    }
                }

                return InvoiceSchemeGroupList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drInvoiceHeader != null)
                    drInvoiceHeader.Close();
            }
        }

        public bool InsertInvoiceDiscountSchemeGroup(out int schemeGroupId, InvoiceSchemeGroupEntity invoiceSchemeGroupEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drBrand = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@InvoiceId", invoiceSchemeGroupEntity.InvoiceId, DbType.Int32);
                paramCollection.Add("@SchemeDetailsId", invoiceSchemeGroupEntity.SchemeDetailsId, DbType.Int32);
                paramCollection.Add("@DiscountValue", invoiceSchemeGroupEntity.DiscountValue, DbType.Double);
                paramCollection.Add("@SchemeGroupId", null, DbType.Int32, ParameterDirection.Output);

                schemeGroupId = this.DataAcessService.ExecuteNonQuery(InvoiceSchemeSql["InsertInvoiceDiscountSchemeGroup"], paramCollection, true, "@SchemeGroupId");

                if (schemeGroupId > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }
        
    }
}
