﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class ActivityTypeDAO : BaseDAO
    {
        private DbSqlAdapter ActivityTypeSql { get; set; }

        public ActivityTypeDAO()
        {
            this.ActivityTypeSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.ActivityTypeSql.xml", ApplicationService.Instance.DbProvider);
        }

        public ActivityTypeEntity CreateObject()
        {
            return ActivityTypeEntity.CreateObject();
        }

        public ActivityTypeEntity CreateObject(int activityTypeId)
        {
            return ActivityTypeEntity.CreateObject(activityTypeId);
        }

        public List<ActivityTypeEntity> GetAllActivityTypes(string defaultDeptID, bool IsSchedular = false)
        {
            DbDataReader drActivityType = null;
            try
            {
                List<ActivityTypeEntity> activityTypeCollection = new List<ActivityTypeEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@DefaultDeptID", defaultDeptID, DbType.String);
                //paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                if (IsSchedular == true)
                    drActivityType = this.DataAcessService.ExecuteQuery(ActivityTypeSql["GetActivityTypesForSchedular"], paramCollection);
                else
                    drActivityType = this.DataAcessService.ExecuteQuery(ActivityTypeSql["GetAllActivityTypes"], paramCollection);


                if (drActivityType != null && drActivityType.HasRows)
                {
                    int typeCodeOrdinal = drActivityType.GetOrdinal("type_code");
                    int typeDescriptionOrdinal = drActivityType.GetOrdinal("type_description");
                    int emailAddressOrdinal = drActivityType.GetOrdinal("email_address");
                    int emailTemplateOrdinal = drActivityType.GetOrdinal("email_template");
                    int colourOrdinal = drActivityType.GetOrdinal("colour");
                    int isColdCallOrdinal = drActivityType.GetOrdinal("is_cold_call");

                    while (drActivityType.Read())
                    {
                        ActivityTypeEntity activityType = new ActivityTypeEntity();

                        if (!drActivityType.IsDBNull(typeCodeOrdinal)) activityType.Code = drActivityType.GetString(typeCodeOrdinal);
                        if (!drActivityType.IsDBNull(typeDescriptionOrdinal)) activityType.Description = drActivityType.GetString(typeDescriptionOrdinal);
                        if (!drActivityType.IsDBNull(emailAddressOrdinal)) activityType.EmailAddresses = drActivityType.GetString(emailAddressOrdinal);
                        if (!drActivityType.IsDBNull(emailTemplateOrdinal)) activityType.EmailTemplate = drActivityType.GetString(emailTemplateOrdinal);
                        if (!drActivityType.IsDBNull(colourOrdinal)) activityType.ActivityTypeColour = drActivityType.GetString(colourOrdinal);
                        if (!drActivityType.IsDBNull(isColdCallOrdinal)) activityType.ShowInPlanner = drActivityType.GetString(isColdCallOrdinal);

                        activityTypeCollection.Add(activityType);
                    }
                }

                return activityTypeCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drActivityType != null)
                    drActivityType.Close();
            }
        }

        public ActivityTypeEntity GetActivityType(string code,ArgsEntity args)
        {
            DbDataReader drActivityType = null;
            ActivityTypeEntity activityType = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Code", code, DbType.String);
                paramCollection.Add("@DefaultDeptID", args.DefaultDepartmentId, DbType.String);
                drActivityType = this.DataAcessService.ExecuteQuery(ActivityTypeSql["GetActivityType"], paramCollection);

                if (drActivityType != null && drActivityType.HasRows)
                {
                    int typeCodeOrdinal = drActivityType.GetOrdinal("type_code");
                    int typeDescriptionOrdinal = drActivityType.GetOrdinal("type_description");
                    int emailAddressOrdinal = drActivityType.GetOrdinal("email_address");
                    int emailTemplateOrdinal = drActivityType.GetOrdinal("email_template");
                    int color = drActivityType.GetOrdinal("colour");

                    if (drActivityType.Read())
                    {
                        activityType = new ActivityTypeEntity();

                        if (!drActivityType.IsDBNull(typeCodeOrdinal)) activityType.Code = drActivityType.GetString(typeCodeOrdinal);
                        if (!drActivityType.IsDBNull(typeDescriptionOrdinal)) activityType.Description = drActivityType.GetString(typeDescriptionOrdinal);
                        if (!drActivityType.IsDBNull(emailAddressOrdinal)) activityType.EmailAddresses = drActivityType.GetString(emailAddressOrdinal);
                        if (!drActivityType.IsDBNull(emailTemplateOrdinal)) activityType.EmailTemplate = drActivityType.GetString(emailTemplateOrdinal);
                        if (!drActivityType.IsDBNull(color)) activityType.ActivityTypeColour = drActivityType.GetString(color);
                    }
                }

                return activityType;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drActivityType != null)
                    drActivityType.Close();
            }
        }

        public List<ActivityTypeEntity> GetAllFixedActivityTypes(bool bShownInPlanner = false)
        {
            DbDataReader drActivityType = null;

            try
            {
                List<ActivityTypeEntity> activityTypeCollection = new List<ActivityTypeEntity>();

                //DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                //paramCollection.Add("@IsShownInPlanner", bShownInPlanner, DbType.Boolean);
                //paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                drActivityType = this.DataAcessService.ExecuteQuery(ActivityTypeSql["GetAllFixedActivityTypes"]);
                
                if (drActivityType != null && drActivityType.HasRows)
                {
                    int typeCodeOrdinal = drActivityType.GetOrdinal("type_code");
                    int typeDescriptionOrdinal = drActivityType.GetOrdinal("type_description");
                    int emailAddressOrdinal = drActivityType.GetOrdinal("email_address");
                    int emailTemplateOrdinal = drActivityType.GetOrdinal("email_template");
                    int colourOrdinal = drActivityType.GetOrdinal("colour");

                    while (drActivityType.Read())
                    {
                        ActivityTypeEntity activityType = new ActivityTypeEntity();

                        if (!drActivityType.IsDBNull(typeCodeOrdinal)) activityType.Code = drActivityType.GetString(typeCodeOrdinal);
                        if (!drActivityType.IsDBNull(typeDescriptionOrdinal)) activityType.Description = drActivityType.GetString(typeDescriptionOrdinal);
                        if (!drActivityType.IsDBNull(emailAddressOrdinal)) activityType.EmailAddresses = drActivityType.GetString(emailAddressOrdinal);
                        if (!drActivityType.IsDBNull(emailTemplateOrdinal)) activityType.EmailTemplate = drActivityType.GetString(emailTemplateOrdinal);
                        if (!drActivityType.IsDBNull(colourOrdinal)) activityType.ActivityTypeColour = drActivityType.GetString(colourOrdinal);

                        activityTypeCollection.Add(activityType);
                    }
                }

                return activityTypeCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drActivityType != null)
                    drActivityType.Close();
            }
        }

        public List<ActivityTypeEntity> GetColdCallActyvityTypes()
        {
            DbDataReader drActivityType = null;

            try
            {
                List<ActivityTypeEntity> activityTypeCollection = new List<ActivityTypeEntity>();
                
                drActivityType = this.DataAcessService.ExecuteQuery(ActivityTypeSql["GetColdCallActivityTypes"]);

                if (drActivityType != null && drActivityType.HasRows)
                {

                    int typeCodeOrdinal = drActivityType.GetOrdinal("type_code");
                    int typeDescriptionOrdinal = drActivityType.GetOrdinal("type_description");
                    int emailAddressOrdinal = drActivityType.GetOrdinal("email_address");
                    int emailTemplateOrdinal = drActivityType.GetOrdinal("email_template");

                    int defaultDeptIdOrdinal = drActivityType.GetOrdinal("default_dept_id");
                    int showInPlannerOrdinal = drActivityType.GetOrdinal("show_in_planner");
                    int defaultValueOrdinal = drActivityType.GetOrdinal("default_value");
                    int colourOrdinal = drActivityType.GetOrdinal("colour");
                    int isColdCallOrdinal = drActivityType.GetOrdinal("is_cold_call");

                    while (drActivityType.Read())
                    {
                        ActivityTypeEntity activityType = new ActivityTypeEntity();

                        if (!drActivityType.IsDBNull(typeCodeOrdinal)) activityType.Code = drActivityType.GetString(typeCodeOrdinal);
                        if (!drActivityType.IsDBNull(typeDescriptionOrdinal)) activityType.Description = drActivityType.GetString(typeDescriptionOrdinal);
                        if (!drActivityType.IsDBNull(emailAddressOrdinal)) activityType.EmailAddresses = drActivityType.GetString(emailAddressOrdinal);
                        if (!drActivityType.IsDBNull(emailTemplateOrdinal)) activityType.EmailTemplate = drActivityType.GetString(emailTemplateOrdinal);

                        if (!drActivityType.IsDBNull(showInPlannerOrdinal)) activityType.ShowInPlanner = drActivityType.GetString(showInPlannerOrdinal);
                        if (!drActivityType.IsDBNull(defaultValueOrdinal)) activityType.SetAsDefault = drActivityType.GetString(defaultValueOrdinal);
                        if (!drActivityType.IsDBNull(colourOrdinal)) activityType.ActivityTypeColour = drActivityType.GetString(colourOrdinal);
                        if (!drActivityType.IsDBNull(isColdCallOrdinal)) activityType.ColdCallActivity = drActivityType.GetString(isColdCallOrdinal);

                        activityTypeCollection.Add(activityType);
                    }
                }

                return activityTypeCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drActivityType != null)
                    drActivityType.Close();
            }
        }
    }
}
