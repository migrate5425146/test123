﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class ImportMailDAO : BaseDAO
    {
        private DbSqlAdapter ImportMailSql { get; set; }

        private void RegisterSql()
        {
            this.ImportMailSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.ImportMailSql.xml", ApplicationService.Instance.DbProvider);
        }

        public ImportMailDAO()
        {
            RegisterSql();
        }

        public ImportMailDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public ImportMailEntity CreateObject()
        {
            return ImportMailEntity.CreateObject();
        }

        public ImportMailEntity CreateObject(int entityId)
        {
            return ImportMailEntity.CreateObject(entityId);
        }

        public List<ImportMailEntity> GetLeadEmails(ArgsEntity args)
        {
            DbDataReader drEmail = null;

            try
            {
                // Check whether the Lead already exists
                List<ImportMailEntity> importMailList = new List<ImportMailEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@LeadId", args.LeadId, DbType.Int32);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                drEmail = this.DataAcessService.ExecuteQuery(ImportMailSql["GetLeadEmails"], paramCollection);

                if (drEmail != null && drEmail.HasRows)
                {
                    int mailIdOrdinal = drEmail.GetOrdinal("mail_id");
                    int mailSenderOrdinal = drEmail.GetOrdinal("mail_sender");
                    int mailSubjectOrdinal = drEmail.GetOrdinal("mail_subject");
                    int mailDateOrdinal = drEmail.GetOrdinal("mail_date");
                    int mailContentOrdinal = drEmail.GetOrdinal("mail_content");
                    int mailFileNameOrdinal = drEmail.GetOrdinal("mail_attachement_filename");
                    int totalCountOrdinal = drEmail.GetOrdinal("total_count");

                    while (drEmail.Read())
                    {
                        ImportMailEntity oImportMail = ImportMailEntity.CreateObject();
                        if (!drEmail.IsDBNull(mailIdOrdinal)) oImportMail.ImportMailID = drEmail.GetInt32(mailIdOrdinal);
                        if (!drEmail.IsDBNull(mailSenderOrdinal)) oImportMail.From = drEmail.GetString(mailSenderOrdinal);
                        if (!drEmail.IsDBNull(mailSubjectOrdinal)) oImportMail.Subject = drEmail.GetString(mailSubjectOrdinal);
                        if (!drEmail.IsDBNull(mailDateOrdinal)) oImportMail.Date = drEmail.GetDateTime(mailDateOrdinal).ToLocalTime();
                        if (!drEmail.IsDBNull(mailContentOrdinal)) oImportMail.Content = drEmail.GetString(mailContentOrdinal);
                        if (!drEmail.IsDBNull(mailFileNameOrdinal)) oImportMail.FileName = drEmail.GetString(mailFileNameOrdinal);
                        if (!drEmail.IsDBNull(totalCountOrdinal)) oImportMail.TotalCount = drEmail.GetInt32(totalCountOrdinal);

                        importMailList.Add(oImportMail);
                    }
                }

                if (drEmail != null)
                    drEmail.Close();

                foreach (ImportMailEntity oImportMail in importMailList)
                {
                    if (oImportMail != null)
                    {
                        paramCollection = new DbInputParameterCollection();
                        paramCollection.Add("@MailId", oImportMail.ImportMailID, DbType.Int32);
                        oImportMail.FileContent = (byte[])DataAcessService.GetOneValue(ImportMailSql["GetLeadEmailContent"], paramCollection);
                    }
                }

                return importMailList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drEmail != null && (!drEmail.IsClosed))
                    drEmail.Close();
            }
        }

        public List<ImportMailEntity> GetCustEmails(ArgsEntity args)
        {
            DbDataReader drEmail = null;

            try
            {
                // Check whether the Lead already exists
                List<ImportMailEntity> importMailList = new List<ImportMailEntity>();
                
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@CustCode", args.CustomerCode, DbType.String);
                //paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);

                drEmail = this.DataAcessService.ExecuteQuery(ImportMailSql["GetCustEmails"], paramCollection);


                if (drEmail != null && drEmail.HasRows)
                {
                    int mailIdOrdinal = drEmail.GetOrdinal("mail_id");
                    int mailSenderOrdinal = drEmail.GetOrdinal("mail_sender");
                    int mailSubjectOrdinal = drEmail.GetOrdinal("mail_subject");
                    int mailDateOrdinal = drEmail.GetOrdinal("mail_date");
                    int mailContentOrdinal = drEmail.GetOrdinal("mail_content");
                    int mailFileNameOrdinal = drEmail.GetOrdinal("mail_attachement_filename");
                    int totcountOrdinal = drEmail.GetOrdinal("mail_attachement_filename");

                    while (drEmail.Read())
                    {
                        ImportMailEntity oImportMail = ImportMailEntity.CreateObject();
                        if (!drEmail.IsDBNull(mailIdOrdinal)) oImportMail.ImportMailID = drEmail.GetInt32(mailIdOrdinal);
                        if (!drEmail.IsDBNull(mailSenderOrdinal)) oImportMail.From = drEmail.GetString(mailSenderOrdinal);
                        if (!drEmail.IsDBNull(mailSubjectOrdinal)) oImportMail.Subject = drEmail.GetString(mailSubjectOrdinal);
                        if (!drEmail.IsDBNull(mailDateOrdinal)) oImportMail.Date = drEmail.GetDateTime(mailDateOrdinal).ToLocalTime();
                        if (!drEmail.IsDBNull(mailContentOrdinal)) oImportMail.Content = drEmail.GetString(mailContentOrdinal);
                        if (!drEmail.IsDBNull(mailFileNameOrdinal)) oImportMail.FileName = drEmail.GetString(mailFileNameOrdinal);
                        if (!drEmail.IsDBNull(totcountOrdinal)) oImportMail.TotalCount = drEmail.GetInt32(totcountOrdinal);
                        importMailList.Add(oImportMail);
                    }
                }

                if (drEmail != null)
                    drEmail.Close();

                return importMailList;
            }
            catch
            {
                throw;
            }
            finally
            {
            }
        }

        public ImportMailEntity GetEmailbyId(int mailId)
        {
            DbDataReader drEmail = null;
            ImportMailEntity oImportMail = ImportMailEntity.CreateObject();

            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@MailId", mailId, DbType.Int32);

                drEmail = this.DataAcessService.ExecuteQuery(ImportMailSql["GetEmailbyId"], paramCollection);

                if (drEmail != null && drEmail.HasRows)
                {
                    int mailIdOrdinal = drEmail.GetOrdinal("mail_id");
                    int mailSenderOrdinal = drEmail.GetOrdinal("mail_sender");
                    int mailSubjectOrdinal = drEmail.GetOrdinal("mail_subject");
                    int mailDateOrdinal = drEmail.GetOrdinal("mail_date");
                    int mailContentOrdinal = drEmail.GetOrdinal("mail_content");
                    int mailFileNameOrdinal = drEmail.GetOrdinal("mail_attachement_filename");

                    if (drEmail.Read())
                    {

                        if (!drEmail.IsDBNull(mailIdOrdinal)) oImportMail.ImportMailID = drEmail.GetInt32(mailIdOrdinal);
                        if (!drEmail.IsDBNull(mailSenderOrdinal)) oImportMail.From = drEmail.GetString(mailSenderOrdinal);
                        if (!drEmail.IsDBNull(mailSubjectOrdinal)) oImportMail.Subject = drEmail.GetString(mailSubjectOrdinal);
                        if (!drEmail.IsDBNull(mailDateOrdinal)) oImportMail.Date = drEmail.GetDateTime(mailDateOrdinal).ToLocalTime();
                        if (!drEmail.IsDBNull(mailContentOrdinal)) oImportMail.Content = drEmail.GetString(mailContentOrdinal);
                        if (!drEmail.IsDBNull(mailFileNameOrdinal)) oImportMail.FileName = drEmail.GetString(mailFileNameOrdinal);
                    }

                    if (oImportMail != null)
                    {
                        if (drEmail != null && !drEmail.IsClosed)
                            drEmail.Close();
                        oImportMail.FileContent = (byte[])this.DataAcessService.GetOneValue(ImportMailSql["GetLeadEmailContent"], paramCollection);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (drEmail != null && !drEmail.IsClosed)
                    drEmail.Close();
            }

            return oImportMail;
        }
    }
}
