﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Entities;
using System.Data.Common;
using System.Collections;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class DocumentRepositoryDAO : BaseDAO
    {
        private DbSqlAdapter DocumentRepositorySql { get; set; }

        //public DocumentRepositoryDAO()
        //{
        //    this.DocumentRepositorySql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.DocumentRepositorySql.xml");
        //}

        private void RegisterSql()
        {
            this.DocumentRepositorySql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.DocumentRepositorySql.xml", ApplicationService.Instance.DbProvider);
        }

        public DocumentRepositoryDAO()
        {
            RegisterSql();
        }

        public DocumentRepositoryDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public DocumentRepositoryEntity CreateObject()
        {
            return DocumentRepositoryEntity.CreateObject();
        }

        public DocumentRepositoryEntity CreateObject(int entityId)
        {
            return DocumentRepositoryEntity.CreateObject(entityId);
        }

        public bool InsertDocumentRepository(DocumentEntity documentRepositoryEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@repositoryID", documentRepositoryEntity.DocumentID, DbType.Int32);
                paramCollection.Add("@documentName", documentRepositoryEntity.DocumentName, DbType.String);
                paramCollection.Add("@path", documentRepositoryEntity.Path, DbType.String);
                paramCollection.Add("@attachedDate", documentRepositoryEntity.AttachedDate, DbType.DateTime);
                paramCollection.Add("@attachedBy", documentRepositoryEntity.AttachedBy, DbType.String);
                //paramCollection.Add("@documentContent", documentRepositoryEntity.FileContent, DbType.Byte);


                iNoRec = this.DataAcessService.ExecuteNonQuery(DocumentRepositorySql["InsertDocumentRepository"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateDocumentRepository(DocumentEntity documentRepositoryEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@documentName", documentRepositoryEntity.DocumentName, DbType.Int32);
                paramCollection.Add("@path", documentRepositoryEntity.Path, DbType.Int32);
                //paramCollection.Add("@documentContent", documentRepositoryEntity.FileContent, DbType.Byte);
                paramCollection.Add("@repositoryID", documentRepositoryEntity.DocumentID, DbType.Int32);
                
                iNoRec = this.DataAcessService.ExecuteNonQuery(DocumentRepositorySql["UpdateDocumentRepository"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public int GetNextDocumentRepositoryID()
        {
            try
            {
                return System.Convert.ToInt32(this.DataAcessService.GetOneValue(DocumentRepositorySql["GetNextDocumentRepositoryID"]));   // Get Next ID               
            }
            catch
            {
                throw;
            }
        }

        public List<DocumentEntity> GetDocumentDetails()
        {
            DbDataReader drDocumentRepository = null;
            List<DocumentEntity> documentRepositoryCollection = new List<DocumentEntity>();

            try
            {             
                drDocumentRepository = this.DataAcessService.ExecuteQuery(DocumentRepositorySql["GetAllDocumentRepositories"]);

                if (drDocumentRepository != null && drDocumentRepository.HasRows)
                {
                    int documentRespositoryIDOrdinal = drDocumentRepository.GetOrdinal("repository_id");
                    int documentNameOrdinal = drDocumentRepository.GetOrdinal("document_name");
                    int pathOrdinal = drDocumentRepository.GetOrdinal("path");
                    int dateTimeOrdinal = drDocumentRepository.GetOrdinal("attached_date");
                    int originatorOrdinal = drDocumentRepository.GetOrdinal("attached_by");

                    while (drDocumentRepository.Read())
                    {
                        DocumentEntity documentRepository = DocumentEntity.CreateObject();
                        if (!drDocumentRepository.IsDBNull(documentRespositoryIDOrdinal)) documentRepository.DocumentID = drDocumentRepository.GetInt32(documentRespositoryIDOrdinal);
                        if (!drDocumentRepository.IsDBNull(documentNameOrdinal)) documentRepository.DocumentName = drDocumentRepository.GetString(documentNameOrdinal);
                        if (!drDocumentRepository.IsDBNull(pathOrdinal)) documentRepository.Path = drDocumentRepository.GetString(pathOrdinal);
                        if (!drDocumentRepository.IsDBNull(dateTimeOrdinal)) documentRepository.AttachedDate = drDocumentRepository.GetDateTime(dateTimeOrdinal).ToLocalTime();
                        if (!drDocumentRepository.IsDBNull(originatorOrdinal)) documentRepository.AttachedBy = drDocumentRepository.GetString(originatorOrdinal);

                        documentRepositoryCollection.Add(documentRepository);
                    }
                }

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (drDocumentRepository != null)
                    drDocumentRepository.Close();
            }

            return documentRepositoryCollection;
        }

        public DocumentEntity GetDocumentDetails(int repositoryId)
        {
            DbDataReader drDocumentRepository = null;
            List<DocumentEntity> documentRepositoryCollection = new List<DocumentEntity>();

            try
            {
                DocumentEntity documentRepository = null;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@repositoryID", repositoryId, DbType.Int32);

                drDocumentRepository = this.DataAcessService.ExecuteQuery(DocumentRepositorySql["GetDocumentRepository"], paramCollection);

                if (drDocumentRepository != null && drDocumentRepository.HasRows)
                {
                    int documentRespositoryIDOrdinal = drDocumentRepository.GetOrdinal("repository_id");
                    int documentNameOrdinal = drDocumentRepository.GetOrdinal("document_name");
                    int pathOrdinal = drDocumentRepository.GetOrdinal("path");
                    int dateTimeOrdinal = drDocumentRepository.GetOrdinal("attached_date");
                    int originatorOrdinal = drDocumentRepository.GetOrdinal("attached_by");
                    int documentContentOrdinal = drDocumentRepository.GetOrdinal("document_content");
                    

                    if (drDocumentRepository.Read())
                    {
                        documentRepository = DocumentEntity.CreateObject();
                        if (!drDocumentRepository.IsDBNull(documentRespositoryIDOrdinal)) documentRepository.DocumentID = drDocumentRepository.GetInt32(documentRespositoryIDOrdinal);
                        if (!drDocumentRepository.IsDBNull(documentNameOrdinal)) documentRepository.DocumentName = drDocumentRepository.GetString(documentNameOrdinal);
                        if (!drDocumentRepository.IsDBNull(pathOrdinal)) documentRepository.Path = drDocumentRepository.GetString(pathOrdinal);
                        if (!drDocumentRepository.IsDBNull(dateTimeOrdinal)) documentRepository.AttachedDate = drDocumentRepository.GetDateTime(dateTimeOrdinal).ToLocalTime();
                        if (!drDocumentRepository.IsDBNull(originatorOrdinal)) documentRepository.AttachedBy = drDocumentRepository.GetString(originatorOrdinal);

                        //if (drDocumentRepository[documentContentOrdinal] != null)
                        //{
                        //    if (!drDocumentRepository.IsDBNull(documentContentOrdinal)) documentRepository.FileContent = (byte[])drDocumentRepository[documentContentOrdinal];
                        //}
                    }
                }
                
                return documentRepository;

            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (drDocumentRepository != null && (!drDocumentRepository.IsClosed))
                    drDocumentRepository.Close();
            }

        }

        public bool DeleDocumentRepository(int repositoryID)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@repositoryID", repositoryID, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(DocumentRepositorySql["DeleDocumentRepository"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }
    }
}
