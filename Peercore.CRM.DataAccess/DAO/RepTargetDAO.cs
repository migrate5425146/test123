﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.CRM.Shared;
using Peercore.DataAccess.Common.Utilties;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using System.Data.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class RepTargetDAO:BaseDAO
    {
        private DbSqlAdapter RepTargetSql { get; set; }

        private void RegisterSql()
        {
            this.RepTargetSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.RepTargetSql.xml", ApplicationService.Instance.DbProvider);
        }

        public RepTargetDAO()
        {
            RegisterSql();
        }

        public RepTargetDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public RepTargetEntity CreateObject()
        {
            return RepTargetEntity.CreateObject();
        }

        public RepTargetEntity CreateObject(int entityId)
        {
            return RepTargetEntity.CreateObject(entityId);
        }

        public bool InsertRepTarget(RepTargetEntity repTargetEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@rep_code", repTargetEntity.RepCode, DbType.String);
                paramCollection.Add("@primary_tar_amt", repTargetEntity.PrimaryTarAmt, DbType.Double);
                paramCollection.Add("@primary_inc_amt", repTargetEntity.PrimaryIncAmt, DbType.Double);
                paramCollection.Add("@created_by", repTargetEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(RepTargetSql["InsertRepTarget"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool DeactivateRepTarget(RepTargetEntity repTargetEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@rep_target_id", repTargetEntity.RepTargetId, DbType.Int32);
                paramCollection.Add("@last_modified_by", repTargetEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(RepTargetSql["DeactivateRepTarget"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public List<RepTargetEntity> GetAllRepTargets(string Originator, string OriginatorType, ArgsEntity args)
        {
            DbDataReader drRepTarget = null;

            try
            {
                List<RepTargetEntity> repTargetList = new List<RepTargetEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", Originator, DbType.String);
                paramCollection.Add("@OriginatorType", OriginatorType, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Int32);

                drRepTarget = this.DataAcessService.ExecuteQuery(RepTargetSql["GetAllRepTargets"], paramCollection);

                if (drRepTarget != null && drRepTarget.HasRows)
                {
                    int idOrdinal = drRepTarget.GetOrdinal("rep_target_id");
                    int repCodeOrdinal = drRepTarget.GetOrdinal("rep_code");
                    int repNameOrdinal = drRepTarget.GetOrdinal("name");
                    int primaryTarAmountOrdinal = drRepTarget.GetOrdinal("primary_tar_amt");
                    int primaryIncAmountOrdinal = drRepTarget.GetOrdinal("primary_inc_amt");
                    int effStartDateOrdinal = drRepTarget.GetOrdinal("eff_start_date");
                    int effEndDateOrdinal = drRepTarget.GetOrdinal("eff_end_date");
                    int statusOrdinal = drRepTarget.GetOrdinal("status");
                    int createdByOrdinal = drRepTarget.GetOrdinal("created_by");
                    int createdDateOrdinal = drRepTarget.GetOrdinal("created_date");
                    int lastModifiedByOrdinal = drRepTarget.GetOrdinal("last_modified_by");
                    int lastModifiedDateOrdinal = drRepTarget.GetOrdinal("last_modified_date");
                    int totalCountOrdinal = drRepTarget.GetOrdinal("total_count");

                    while (drRepTarget.Read())
                    {
                        RepTargetEntity repTargetEntity = CreateObject();

                        if (!drRepTarget.IsDBNull(idOrdinal)) repTargetEntity.RepTargetId = drRepTarget.GetInt32(idOrdinal);
                        if (!drRepTarget.IsDBNull(repCodeOrdinal)) repTargetEntity.RepCode = drRepTarget.GetString(repCodeOrdinal);
                        if (!drRepTarget.IsDBNull(repNameOrdinal)) repTargetEntity.RepName = drRepTarget.GetString(repNameOrdinal);
                        if (!drRepTarget.IsDBNull(primaryTarAmountOrdinal)) repTargetEntity.PrimaryTarAmt = drRepTarget.GetDouble(primaryTarAmountOrdinal);
                        if (!drRepTarget.IsDBNull(primaryIncAmountOrdinal)) repTargetEntity.PrimaryIncAmt = drRepTarget.GetDouble(primaryIncAmountOrdinal);
                        if (!drRepTarget.IsDBNull(effStartDateOrdinal)) repTargetEntity.EffStartDate = drRepTarget.GetDateTime(effStartDateOrdinal);
                        if (!drRepTarget.IsDBNull(effEndDateOrdinal)) repTargetEntity.EffEndDate = drRepTarget.GetDateTime(effEndDateOrdinal);
                        if (!drRepTarget.IsDBNull(statusOrdinal)) repTargetEntity.Status = drRepTarget.GetString(statusOrdinal);
                        if (!drRepTarget.IsDBNull(createdByOrdinal)) repTargetEntity.CreatedBy = drRepTarget.GetString(createdByOrdinal);
                        if (!drRepTarget.IsDBNull(createdDateOrdinal)) repTargetEntity.CreatedDate = drRepTarget.GetDateTime(createdDateOrdinal);
                        if (!drRepTarget.IsDBNull(lastModifiedByOrdinal)) repTargetEntity.LastModifiedBy = drRepTarget.GetString(lastModifiedByOrdinal);
                        if (!drRepTarget.IsDBNull(lastModifiedDateOrdinal)) repTargetEntity.LastModifiedDate = drRepTarget.GetDateTime(lastModifiedDateOrdinal);
                        if (!drRepTarget.IsDBNull(totalCountOrdinal)) repTargetEntity.TotalCount = drRepTarget.GetInt32(totalCountOrdinal);

                        repTargetList.Add(repTargetEntity);
                    }
                }

                return repTargetList;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (drRepTarget != null)
                    drRepTarget.Close();
            }
        }

        public List<RepTargetEntity> GetRepTargetVsActualByAccessToken(string accessToken)
        {
            DbDataReader drProduct = null;

            try
            {
                RepTargetEntity repTargetEntity = null;
                List<RepTargetEntity> repTargetList= new List<RepTargetEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AccessToken", accessToken, DbType.String);

                drProduct = this.DataAcessService.ExecuteQuery(RepTargetSql["GetRepTargetVsActualByAccessToken"], paramCollection);

                if (drProduct != null && drProduct.HasRows)
                {
                    int actualOrdinal = drProduct.GetOrdinal("actual");
                    int targetOrdinal = drProduct.GetOrdinal("target");
                    int weeknoOrdinal = drProduct.GetOrdinal("week_no");
                    //int descXOrdinal = drProduct.GetOrdinal("DescX");

                    Double cumActual = 0;
                    Double cumTarget = 0;

                    while (drProduct.Read())
                    {
                        repTargetEntity = new RepTargetEntity();
                        if (!drProduct.IsDBNull(weeknoOrdinal)) repTargetEntity.WeekNo = drProduct.GetInt32(weeknoOrdinal);

                        //Double actual = Convert.ToDouble(drProduct.GetFloat(actualOrdinal));

                        //if (!drProduct.IsDBNull(actualOrdinal)) repTargetEntity.ActualAmt = Double.Parse(actual.ToString());
                        if (!drProduct.IsDBNull(actualOrdinal)) repTargetEntity.ActualAmt = drProduct.GetDouble(actualOrdinal);
                        if (!drProduct.IsDBNull(targetOrdinal)) repTargetEntity.PrimaryTarAmt = drProduct.GetDouble(targetOrdinal);
                        //if (!drProduct.IsDBNull(descXOrdinal)) repTargetEntity.DescriptionforX = drProduct.GetString(descXOrdinal);

                        cumActual += repTargetEntity.ActualAmt;
                        cumTarget += repTargetEntity.PrimaryTarAmt;
                        repTargetEntity.CumActualAmt = cumActual;
                        repTargetEntity.CumPrimaryTarAmt = cumTarget;

                        repTargetList.Add(repTargetEntity);
                    }
                }

                return repTargetList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }

        public List<RepTargetEntity> GetRepTargetVsActualByAccessTokenNew(string accessToken)
        {
            DbDataReader drProduct = null;

            try
            {
                RepTargetEntity repTargetEntity = null;
                List<RepTargetEntity> repTargetList = new List<RepTargetEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AccessToken", accessToken, DbType.String);

                drProduct = this.DataAcessService.ExecuteQuery(RepTargetSql["GetRepTargetVsActualByAccessToken_new"], paramCollection);

                if (drProduct != null && drProduct.HasRows)
                {
                    int actualOrdinal = drProduct.GetOrdinal("actual");
                    int targetOrdinal = drProduct.GetOrdinal("target");
                    int weeknoOrdinal = drProduct.GetOrdinal("week_no");
                    int descXOrdinal = drProduct.GetOrdinal("DescX");

                    Double cumActual = 0;
                    Double cumTarget = 0;

                    while (drProduct.Read())
                    {
                        repTargetEntity = new RepTargetEntity();
                        if (!drProduct.IsDBNull(weeknoOrdinal)) repTargetEntity.WeekNo = drProduct.GetInt32(weeknoOrdinal);

                        //Double actual = Convert.ToDouble(drProduct.GetFloat(actualOrdinal));

                        //if (!drProduct.IsDBNull(actualOrdinal)) repTargetEntity.ActualAmt = Double.Parse(actual.ToString());
                        if (!drProduct.IsDBNull(actualOrdinal)) repTargetEntity.ActualAmt = drProduct.GetDouble(actualOrdinal);
                        if (!drProduct.IsDBNull(targetOrdinal)) repTargetEntity.PrimaryTarAmt = drProduct.GetDouble(targetOrdinal);
                        if (!drProduct.IsDBNull(descXOrdinal)) repTargetEntity.DescriptionforX = drProduct.GetString(descXOrdinal);

                        cumActual += repTargetEntity.ActualAmt;
                        cumTarget += repTargetEntity.PrimaryTarAmt;
                        repTargetEntity.CumActualAmt = cumActual;
                        repTargetEntity.CumPrimaryTarAmt = cumTarget;

                        repTargetList.Add(repTargetEntity);
                    }
                }

                return repTargetList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProduct != null)
                    drProduct.Close();
            }
        }
    }
}
