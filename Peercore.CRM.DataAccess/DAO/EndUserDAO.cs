﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class EndUserDAO : BaseDAO
    {
        private DbSqlAdapter EndUserSql { get; set; }

        private void RegisterSql()
        {
            this.EndUserSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.EndUserSql.xml", ApplicationService.Instance.DbProvider);
        }

        public EndUserDAO()
        {
            RegisterSql();
        }

        public EndUserDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public EndUserEntity CreateObject()
        {
            return EndUserEntity.CreateObject();
        }

        public EndUserEntity CreateObject(string entityCode)
        {
            return EndUserEntity.CreateObject(entityCode);
        }

        public bool UpdateEndUser(EndUserEntity endUser)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;
            DbDataReader drEndUser = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@EnduserCode", ValidateString(endUser.EndUserCode), DbType.String);
                paramCollection.Add("@CustCode", ValidateString(endUser.CustomerCode), DbType.String);
                if (endUser.PrimaryDistributor == "Y")                {
                    
                    UpdateSuccessful = this.DataAcessService.ExecuteNonQuery(EndUserSql["UpdatePrimaryDistributor"], paramCollection) > 0;
                }
                else if (endUser.PrimaryDistributor != "Y")
                {
                    drEndUser = this.DataAcessService.ExecuteQuery(EndUserSql["GetPrimaryDistributor"], paramCollection);
                    if (drEndUser == null || !drEndUser.HasRows)
                        endUser.PrimaryDistributor = "Y";

                    if (drEndUser != null)
                        drEndUser.Close();
                }

                //if (UpdateSuccessful)
                //{
                    //paramCollection.Add("@CustCode", ValidateString(endUser.CustomerCode), DbType.String);
                    //paramCollection.Add("@EnduserCode", ValidateString(endUser.EndUserCode), DbType.String);
                    paramCollection.Add("@Name", ValidateString(endUser.Name), DbType.String);
                    paramCollection.Add("@Address1", ValidateString(endUser.Address1), DbType.String);
                    paramCollection.Add("@Address2", ValidateString(endUser.Address2), DbType.String);
                    paramCollection.Add("@City", ValidateString(endUser.City), DbType.String);
                    paramCollection.Add("@State", ValidateString(endUser.State), DbType.String);
                    paramCollection.Add("@Postcode", ValidateString(endUser.PostCode), DbType.String);
                    paramCollection.Add("@Telephone", ValidateString(endUser.Telephone), DbType.String);
                    paramCollection.Add("@Contact", ValidateString(endUser.Contact), DbType.String);
                    paramCollection.Add("@RepCode", ValidateString(endUser.RepCode), DbType.String);
                    paramCollection.Add("@EmailAddress", ValidateString(endUser.EmailAddress), DbType.String);
                    paramCollection.Add("@Version", endUser.Version, DbType.Int32);
                    paramCollection.Add("@PrimaryDist", ValidateString(endUser.PrimaryDistributor), DbType.String);
                    paramCollection.Add("@Rating", endUser.Rating, DbType.Int32);
                    paramCollection.Add("@EnduserType", endUser.EndUserType, DbType.String);


                    iNoRec = this.DataAcessService.ExecuteNonQuery(EndUserSql["UpdateEndUser"], paramCollection);

                    if (iNoRec > 0)
                        UpdateSuccessful = true;
                //}
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (drEndUser != null && (!drEndUser.IsClosed))
                    drEndUser.Close();
            }
            return UpdateSuccessful;
        }

        public bool InsertEndUser(EndUserEntity endUser)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;
            DbDataReader drEndUser = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@EnduserCode", ValidateString(endUser.EndUserCode), DbType.String);
                paramCollection.Add("@CustCode", ValidateString(endUser.CustomerCode), DbType.String);

                if (endUser.PrimaryDistributor == "Y")
                {
                    UpdateSuccessful = this.DataAcessService.ExecuteNonQuery(EndUserSql["UpdatePrimaryDistributor"], paramCollection) > 0;
                }
                else if (endUser.PrimaryDistributor != "Y")
                {
                    drEndUser = this.DataAcessService.ExecuteQuery(EndUserSql["GetPrimaryDistributor"], paramCollection);
                    if (drEndUser == null || !drEndUser.HasRows)
                        endUser.PrimaryDistributor = "Y";

                    if (drEndUser != null)
                        drEndUser.Close();
                }

                //if (UpdateSuccessful)
                //{
                //    paramCollection.Add("@CustCode", ValidateString(endUser.CustomerCode), DbType.String);
                //    paramCollection.Add("@EnduserCode", ValidateString(endUser.EndUserCode), DbType.String);
                    paramCollection.Add("@Name", ValidateString(endUser.Name), DbType.String);
                    paramCollection.Add("@Address1", ValidateString(endUser.Address1), DbType.String);
                    paramCollection.Add("@Address2", ValidateString(endUser.Address2), DbType.String);
                    paramCollection.Add("@City", ValidateString(endUser.City), DbType.String);
                    paramCollection.Add("@State", ValidateString(endUser.State), DbType.String);
                    paramCollection.Add("@Postcode", ValidateString(endUser.PostCode), DbType.String);
                    paramCollection.Add("@Telephone", ValidateString(endUser.Telephone), DbType.String);
                    paramCollection.Add("@Contact", ValidateString(endUser.Contact), DbType.String);
                    paramCollection.Add("@RepCode", ValidateString(endUser.RepCode), DbType.String);
                    paramCollection.Add("@EmailAddress", ValidateString(endUser.EmailAddress), DbType.String);
                    paramCollection.Add("@Version", endUser.Version, DbType.Int32);
                    paramCollection.Add("@PrimaryDist", ValidateString(endUser.PrimaryDistributor), DbType.String);
                    paramCollection.Add("@Rating", endUser.Rating, DbType.Int32);
                    paramCollection.Add("@EnduserType", endUser.EndUserType, DbType.String);
                    //paramCollection.Add("@MarketId", endUser.MarketId, DbType.Int32);
                    //paramCollection.Add("@CategoryId", endUser.Category, DbType.Int32);
                    //paramCollection.Add("@VolumeId", endUser.VolumeId, DbType.Int32);

                    iNoRec = this.DataAcessService.ExecuteNonQuery(EndUserSql["InsertEndUser"], paramCollection);

                    if (iNoRec > 0)
                        UpdateSuccessful = true;
                //}
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (drEndUser != null && (!drEndUser.IsClosed))
                    drEndUser.Close();
            }
            return UpdateSuccessful;
        }

        public EndUserEntity GetEndUser(ArgsEntity args)
        {

            DbDataReader drEndUser = null;

            try
            {
                EndUserEntity endUser = null;
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@EnduserCode", ValidateString(args.EnduserCode), DbType.String);
                paramCollection.Add("@CustCode", ValidateString(args.CustomerCode), DbType.String);

                drEndUser = this.DataAcessService.ExecuteQuery(EndUserSql["GetEndUser"], paramCollection);

                if (drEndUser != null && drEndUser.HasRows)
                {
                    endUser = EndUserEntity.CreateObject();
                    int custCodeOrdinal = drEndUser.GetOrdinal("cust_code");
                    int enduserCodeOrdinal = drEndUser.GetOrdinal("enduser_code");
                    int nameOrdinal = drEndUser.GetOrdinal("name");
                    int address1Ordinal = drEndUser.GetOrdinal("address_1");
                    int address2Ordinal = drEndUser.GetOrdinal("address_2");

                    int cityOrdinal = drEndUser.GetOrdinal("city");
                    int stateOrdinal = drEndUser.GetOrdinal("state");
                    int postcodeOrdinal = drEndUser.GetOrdinal("postcode");
                    int telephoneOrdinal = drEndUser.GetOrdinal("telephone");
                    int contactOrdinal = drEndUser.GetOrdinal("contact");

                    int repCodeOrdinal = drEndUser.GetOrdinal("rep_code");
                    int emailAddressOrdinal = drEndUser.GetOrdinal("email_address");
                    int versionOrdinal = drEndUser.GetOrdinal("version");
                    int delFlagOrdinal = drEndUser.GetOrdinal("del_flag");
                    int primaryDistOrdinal = drEndUser.GetOrdinal("primary_dist");
                    int ratingOrdinal = drEndUser.GetOrdinal("rating");
                    int primaryRepNameOrdinal = drEndUser.GetOrdinal("PrimaryRep");
                    int secondaryRepNameOrdinal = drEndUser.GetOrdinal("SecondaryRep");
                    int endUserTypeOrdinal = drEndUser.GetOrdinal("enduser_type");
                    int customernameOrdinal = drEndUser.GetOrdinal("customername");

                    if (drEndUser.Read())
                    {
                        if (!drEndUser.IsDBNull(custCodeOrdinal)) endUser.CustomerCode = drEndUser.GetString(custCodeOrdinal);
                        if (!drEndUser.IsDBNull(enduserCodeOrdinal)) endUser.EndUserCode = drEndUser.GetString(enduserCodeOrdinal);
                        if (!drEndUser.IsDBNull(nameOrdinal)) endUser.Name = drEndUser.GetString(nameOrdinal);
                        if (!drEndUser.IsDBNull(address1Ordinal)) endUser.Address1 = drEndUser.GetString(address1Ordinal);
                        if (!drEndUser.IsDBNull(address2Ordinal)) endUser.Address2 = drEndUser.GetString(address2Ordinal);

                        if (!drEndUser.IsDBNull(cityOrdinal)) endUser.City = drEndUser.GetString(cityOrdinal);
                        if (!drEndUser.IsDBNull(stateOrdinal)) endUser.State = drEndUser.GetString(stateOrdinal);
                        if (!drEndUser.IsDBNull(postcodeOrdinal)) endUser.PostCode = drEndUser.GetString(postcodeOrdinal);
                        if (!drEndUser.IsDBNull(telephoneOrdinal)) endUser.Telephone = drEndUser.GetString(telephoneOrdinal);
                        if (!drEndUser.IsDBNull(contactOrdinal)) endUser.Contact = drEndUser.GetString(contactOrdinal);

                        if (!drEndUser.IsDBNull(repCodeOrdinal)) endUser.RepCode = drEndUser.GetString(repCodeOrdinal);
                        if (!drEndUser.IsDBNull(emailAddressOrdinal)) endUser.EmailAddress = drEndUser.GetString(emailAddressOrdinal);
                        if (!drEndUser.IsDBNull(versionOrdinal)) endUser.Version = drEndUser.GetInt32(versionOrdinal);
                        if (!drEndUser.IsDBNull(delFlagOrdinal)) endUser.IsActive = drEndUser.GetString(delFlagOrdinal) != "Y";
                        if (!drEndUser.IsDBNull(primaryDistOrdinal)) endUser.PrimaryDistributor = drEndUser.GetString(primaryDistOrdinal);
                        if (!drEndUser.IsDBNull(ratingOrdinal)) endUser.Rating = drEndUser.GetInt32(ratingOrdinal);

                        if (!drEndUser.IsDBNull(primaryRepNameOrdinal)) endUser.PrimaryRepName = drEndUser.GetString(primaryRepNameOrdinal);
                        if (!drEndUser.IsDBNull(secondaryRepNameOrdinal)) endUser.SecondaryRepName = drEndUser.GetString(secondaryRepNameOrdinal);
                        if (!drEndUser.IsDBNull(endUserTypeOrdinal)) endUser.EndUserType = drEndUser.GetString(endUserTypeOrdinal);
                        if (!drEndUser.IsDBNull(customernameOrdinal)) endUser.CustomerName = drEndUser.GetString(customernameOrdinal);

                    }
                }

                return endUser;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drEndUser != null)
                    drEndUser.Close();
            }
        }

        public List<EndUserEntity> GetEndUsersByCode(string enduserCodes)
        {
            DbDataReader drEndUser = null;

            try
            {
                List<EndUserEntity> endUsersList = new List<EndUserEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@EnduserCodes", enduserCodes, DbType.String);


                drEndUser = this.DataAcessService.ExecuteQuery(EndUserSql["GetEndUsersByCode"], paramCollection);

                if (drEndUser != null && drEndUser.HasRows)
                {
                    int custCodeOrdinal = drEndUser.GetOrdinal("cust_code");
                    int enduserCodeOrdinal = drEndUser.GetOrdinal("enduser_code");
                    int nameOrdinal = drEndUser.GetOrdinal("name");
                    int address1Ordinal = drEndUser.GetOrdinal("address_1");
                    int address2Ordinal = drEndUser.GetOrdinal("address_2");

                    int cityOrdinal = drEndUser.GetOrdinal("city");
                    int stateOrdinal = drEndUser.GetOrdinal("state");
                    int postcodeOrdinal = drEndUser.GetOrdinal("postcode");
                    int telephoneOrdinal = drEndUser.GetOrdinal("telephone");
                    int contactOrdinal = drEndUser.GetOrdinal("contact");

                    int repCodeOrdinal = drEndUser.GetOrdinal("rep_code");
                    int emailAddressOrdinal = drEndUser.GetOrdinal("email_address");
                    int versionOrdinal = drEndUser.GetOrdinal("version");
                    int delFlagOrdinal = drEndUser.GetOrdinal("del_flag");
                    int primaryDistOrdinal = drEndUser.GetOrdinal("primary_dist");
                    int ratingOrdinal = drEndUser.GetOrdinal("rating");
                    int primaryRepNameOrdinal = drEndUser.GetOrdinal("PrimaryRep");
                    //int secondaryRepNameOrdinal = drEndUser.GetOrdinal("SecondaryRep");
                    int endUserTypeOrdinal = drEndUser.GetOrdinal("enduser_type");
                    int customernameOrdinal = drEndUser.GetOrdinal("customername");


                    while (drEndUser.Read())
                    {
                        EndUserEntity endUser = EndUserEntity.CreateObject();

                        if (!drEndUser.IsDBNull(custCodeOrdinal)) endUser.CustomerCode = drEndUser.GetString(custCodeOrdinal);
                        if (!drEndUser.IsDBNull(enduserCodeOrdinal)) endUser.EndUserCode = drEndUser.GetString(enduserCodeOrdinal);
                        if (!drEndUser.IsDBNull(nameOrdinal)) endUser.Name = drEndUser.GetString(nameOrdinal);
                        if (!drEndUser.IsDBNull(address1Ordinal)) endUser.Address1 = drEndUser.GetString(address1Ordinal);
                        if (!drEndUser.IsDBNull(address2Ordinal)) endUser.Address2 = drEndUser.GetString(address2Ordinal);

                        if (!drEndUser.IsDBNull(cityOrdinal)) endUser.City = drEndUser.GetString(cityOrdinal);
                        if (!drEndUser.IsDBNull(stateOrdinal)) endUser.State = drEndUser.GetString(stateOrdinal);
                        if (!drEndUser.IsDBNull(postcodeOrdinal)) endUser.PostCode = drEndUser.GetString(postcodeOrdinal);
                        if (!drEndUser.IsDBNull(telephoneOrdinal)) endUser.Telephone = drEndUser.GetString(telephoneOrdinal);
                        if (!drEndUser.IsDBNull(contactOrdinal)) endUser.Contact = drEndUser.GetString(contactOrdinal);

                        if (!drEndUser.IsDBNull(repCodeOrdinal)) endUser.RepCode = drEndUser.GetString(repCodeOrdinal);
                        if (!drEndUser.IsDBNull(emailAddressOrdinal)) endUser.EmailAddress = drEndUser.GetString(emailAddressOrdinal);
                        if (!drEndUser.IsDBNull(versionOrdinal)) endUser.Version = drEndUser.GetInt32(versionOrdinal);
                        if (!drEndUser.IsDBNull(delFlagOrdinal)) endUser.IsActive = drEndUser.GetString(delFlagOrdinal) != "Y";
                        if (!drEndUser.IsDBNull(primaryDistOrdinal)) endUser.PrimaryDistributor = drEndUser.GetString(primaryDistOrdinal);
                        if (!drEndUser.IsDBNull(ratingOrdinal)) endUser.Rating = drEndUser.GetInt32(ratingOrdinal);

                        if (!drEndUser.IsDBNull(primaryRepNameOrdinal)) endUser.PrimaryRepName = drEndUser.GetString(primaryRepNameOrdinal);
                        //if (!drEndUser.IsDBNull(secondaryRepNameOrdinal)) endUser.SecondaryRepName = drEndUser.GetString(secondaryRepNameOrdinal);
                        if (!drEndUser.IsDBNull(endUserTypeOrdinal)) endUser.EndUserType = drEndUser.GetString(endUserTypeOrdinal);
                        if (!drEndUser.IsDBNull(customernameOrdinal)) endUser.CustomerName = drEndUser.GetString(customernameOrdinal);

                        endUsersList.Add(endUser);
                    }
                }

                return endUsersList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drEndUser != null)
                    drEndUser.Close();
            }
        }

        public List<EndUserEntity> GetEndUsers(ArgsEntity args, string sStatus)
        {
            DbDataReader drEndUser = null;
            
            try
            {
                List<EndUserEntity> endUsersList = new List<EndUserEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Status", sStatus, DbType.String);
                paramCollection.Add("@CustCode", ValidateString(args.CustomerCode), DbType.String);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);

                drEndUser = this.DataAcessService.ExecuteQuery(EndUserSql["GetEndUserByCustomerCode"], paramCollection);

                if (drEndUser != null && drEndUser.HasRows)
                {
                    int custCodeOrdinal = drEndUser.GetOrdinal("cust_code");
                    int enduserCodeOrdinal = drEndUser.GetOrdinal("enduser_code");
                    int nameOrdinal = drEndUser.GetOrdinal("name");
                    int address1Ordinal = drEndUser.GetOrdinal("address_1");
                    int address2Ordinal = drEndUser.GetOrdinal("address_2");

                    int cityOrdinal = drEndUser.GetOrdinal("city");
                    int stateOrdinal = drEndUser.GetOrdinal("state");
                    int postcodeOrdinal = drEndUser.GetOrdinal("postcode");
                    int telephoneOrdinal = drEndUser.GetOrdinal("telephone");
                    int contactOrdinal = drEndUser.GetOrdinal("contact");

                    int repCodeOrdinal = drEndUser.GetOrdinal("rep_code");
                    int emailAddressOrdinal = drEndUser.GetOrdinal("email_address");
                    int versionOrdinal = drEndUser.GetOrdinal("version");
                    int delFlagOrdinal = drEndUser.GetOrdinal("del_flag");
                    int totcountOrdinal = drEndUser.GetOrdinal("totcount");
                    

                    while (drEndUser.Read())
                    {
                        EndUserEntity endUser = EndUserEntity.CreateObject();

                        if (!drEndUser.IsDBNull(custCodeOrdinal)) endUser.CustomerCode = drEndUser.GetString(custCodeOrdinal);
                        if (!drEndUser.IsDBNull(enduserCodeOrdinal)) endUser.EndUserCode = drEndUser.GetString(enduserCodeOrdinal);
                        if (!drEndUser.IsDBNull(nameOrdinal)) endUser.Name = drEndUser.GetString(nameOrdinal);
                        if (!drEndUser.IsDBNull(address1Ordinal)) endUser.Address1 = drEndUser.GetString(address1Ordinal);
                        if (!drEndUser.IsDBNull(address2Ordinal)) endUser.Address2 = drEndUser.GetString(address2Ordinal);

                        if (!drEndUser.IsDBNull(cityOrdinal)) endUser.City = drEndUser.GetString(cityOrdinal);
                        if (!drEndUser.IsDBNull(stateOrdinal)) endUser.State = drEndUser.GetString(stateOrdinal);
                        if (!drEndUser.IsDBNull(postcodeOrdinal)) endUser.PostCode = drEndUser.GetString(postcodeOrdinal);
                        if (!drEndUser.IsDBNull(telephoneOrdinal)) endUser.Telephone = drEndUser.GetString(telephoneOrdinal);
                        if (!drEndUser.IsDBNull(contactOrdinal)) endUser.Contact = drEndUser.GetString(contactOrdinal);

                        if (!drEndUser.IsDBNull(repCodeOrdinal)) endUser.RepCode = drEndUser.GetString(repCodeOrdinal);
                        if (!drEndUser.IsDBNull(emailAddressOrdinal)) endUser.EmailAddress = drEndUser.GetString(emailAddressOrdinal);
                        if (!drEndUser.IsDBNull(versionOrdinal)) endUser.Version = drEndUser.GetInt32(versionOrdinal);
                        if (!drEndUser.IsDBNull(delFlagOrdinal)) endUser.IsActive = drEndUser.GetString(delFlagOrdinal) != "Y";
                        if (!drEndUser.IsDBNull(totcountOrdinal)) endUser.TotalCount = drEndUser.GetInt32(totcountOrdinal);
                        
                        endUsersList.Add(endUser);
                    }
                }

                return endUsersList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drEndUser != null)
                    drEndUser.Close();
            }
        }

        public int SetActiveStatus(ArgsEntity args, bool isActive)
        {
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@EnduserCode", ValidateString(args.EnduserCode), DbType.String);
                paramCollection.Add("@CustCode", ValidateString(args.CustomerCode), DbType.String);
                paramCollection.Add("@DelFlag", isActive ? "N" : "Y", DbType.String);
                paramCollection.Add("@PrimaryDist", "primary_dist", DbType.String);
                return this.DataAcessService.ExecuteNonQuery(EndUserSql["UpdateEndUserStatus"], paramCollection);
            }
            catch
            {
                throw;
            }
            finally
            {
            }

        }

        public List<EndUserEntity> GetAllEndUsers(ArgsEntity args)
        {

            DbDataReader drEndUser = null;
            string sWhereCls = string.Empty;

            try
            {
                //string sRetrieveInactive = bRetrieveInactive ? " = '" + "Y'" : " != '" + "Y'";
                //sWhereCls += " de.del_flag " + sRetrieveInactive;
                //sWhereCls = !string.IsNullOrWhiteSpace(sWhereCls) ? " WHERE " + sWhereCls : "";

                List<EndUserEntity> endUsersList = new List<EndUserEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@RetrieveInactive", args.ActiveInactiveChecked, DbType.Boolean);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@ChildReps", string.IsNullOrEmpty(args.ChildReps) ? "" : args.ChildReps, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                drEndUser = this.DataAcessService.ExecuteQuery(EndUserSql["GetAllEndUsers"], paramCollection);
                
                if (drEndUser != null && drEndUser.HasRows)
                {
                    int custCodeOrdinal = drEndUser.GetOrdinal("cust_code");
                    int enduserCodeOrdinal = drEndUser.GetOrdinal("enduser_code");
                    int nameOrdinal = drEndUser.GetOrdinal("name");
                    int address1Ordinal = drEndUser.GetOrdinal("address_1");
                    int address2Ordinal = drEndUser.GetOrdinal("address_2");

                    int cityOrdinal = drEndUser.GetOrdinal("city");
                    int stateOrdinal = drEndUser.GetOrdinal("state");
                    int postcodeOrdinal = drEndUser.GetOrdinal("postcode");
                    int telephoneOrdinal = drEndUser.GetOrdinal("telephone");
                    int contactOrdinal = drEndUser.GetOrdinal("contact");

                    int repCodeOrdinal = drEndUser.GetOrdinal("rep_code");
                    int emailAddressOrdinal = drEndUser.GetOrdinal("email_address");
                    int versionOrdinal = drEndUser.GetOrdinal("version");
                    int delFlagOrdinal = drEndUser.GetOrdinal("del_flag");
                    int custNameOrdinal = drEndUser.GetOrdinal("cust_name");
                    int totalCountOrdinal = drEndUser.GetOrdinal("total_count");
                    int viewCountOrdinal = drEndUser.GetOrdinal("view_count");

                    while (drEndUser.Read())
                    {
                        EndUserEntity endUser = EndUserEntity.CreateObject();

                        if (!drEndUser.IsDBNull(custCodeOrdinal)) endUser.CustomerCode = drEndUser.GetString(custCodeOrdinal);
                        if (!drEndUser.IsDBNull(enduserCodeOrdinal)) endUser.EndUserCode = drEndUser.GetString(enduserCodeOrdinal);
                        if (!drEndUser.IsDBNull(nameOrdinal)) endUser.Name = drEndUser.GetString(nameOrdinal);
                        if (!drEndUser.IsDBNull(address1Ordinal)) endUser.Address1 = drEndUser.GetString(address1Ordinal);
                        if (!drEndUser.IsDBNull(address2Ordinal)) endUser.Address2 = drEndUser.GetString(address2Ordinal);

                        if (!drEndUser.IsDBNull(cityOrdinal)) endUser.City = drEndUser.GetString(cityOrdinal);
                        if (!drEndUser.IsDBNull(stateOrdinal)) endUser.State = drEndUser.GetString(stateOrdinal);
                        if (!drEndUser.IsDBNull(postcodeOrdinal)) endUser.PostCode = drEndUser.GetString(postcodeOrdinal);
                        if (!drEndUser.IsDBNull(telephoneOrdinal)) endUser.Telephone = drEndUser.GetString(telephoneOrdinal);
                        if (!drEndUser.IsDBNull(contactOrdinal)) endUser.Contact = drEndUser.GetString(contactOrdinal);

                        if (!drEndUser.IsDBNull(repCodeOrdinal)) endUser.RepCode = drEndUser.GetString(repCodeOrdinal);
                        if (!drEndUser.IsDBNull(emailAddressOrdinal)) endUser.EmailAddress = drEndUser.GetString(emailAddressOrdinal);
                        if (!drEndUser.IsDBNull(versionOrdinal)) endUser.Version = drEndUser.GetInt32(versionOrdinal);
                        if (!drEndUser.IsDBNull(delFlagOrdinal)) endUser.IsActive = drEndUser.GetString(delFlagOrdinal) != "Y";
                        if (!drEndUser.IsDBNull(custNameOrdinal)) endUser.CustomerName = drEndUser.GetString(custNameOrdinal);
                        if (!drEndUser.IsDBNull(totalCountOrdinal)) endUser.TotalCount = drEndUser.GetInt32(totalCountOrdinal);
                        if (!drEndUser.IsDBNull(viewCountOrdinal)) endUser.ViewRowCount = drEndUser.GetInt32(viewCountOrdinal);
                        endUsersList.Add(endUser);
                    }
                }

                return endUsersList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drEndUser != null)
                    drEndUser.Close();
            }
        }

        public EndUserEntity IsEndUserExistForCustomer(ArgsEntity args)
        {
            DbDataReader drEndUser = null;
            EndUserEntity oEndUser = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@EnduserCode", ValidateString(args.EnduserCode), DbType.String);
                paramCollection.Add("@CustCode", ValidateString(args.CustomerCode), DbType.String);
                paramCollection.Add("@Command", "=", DbType.String);

                drEndUser = this.DataAcessService.ExecuteQuery(EndUserSql["GetEndUserExistForCustomerCode"], paramCollection);

                if (drEndUser != null && drEndUser.HasRows)
                {
                    if (drEndUser.Read())
                    {
                        oEndUser = EndUserEntity.CreateObject();
                        if (!drEndUser.IsDBNull(drEndUser.GetOrdinal("name"))) oEndUser.Name = drEndUser.GetString(drEndUser.GetOrdinal("name"));
                    }
                }

                return oEndUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (drEndUser != null)
                    drEndUser.Close();
            }
        }

        public List<EndUserEntity> IsEndUserExistForOtherCustomers(ArgsEntity args)
        {
            DbDataReader drEndUser = null;
            List<EndUserEntity> listEndUser = null;
            EndUserEntity oEndUser = null;

            try
            {
                listEndUser = new List<EndUserEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@EnduserCode", ValidateString(args.EnduserCode), DbType.String);
                paramCollection.Add("@CustCode", ValidateString(args.CustomerCode), DbType.String);
                paramCollection.Add("@Command", "!=", DbType.String);

                drEndUser = this.DataAcessService.ExecuteQuery(EndUserSql["GetEndUserExistForCustomerCode"], paramCollection);

                if (drEndUser != null && drEndUser.HasRows)
                {
                    while (drEndUser.Read())
                    {
                        oEndUser = EndUserEntity.CreateObject();
                        if (!drEndUser.IsDBNull(drEndUser.GetOrdinal("name"))) oEndUser.Name = drEndUser.GetString(drEndUser.GetOrdinal("name"));
                        if (!drEndUser.IsDBNull(drEndUser.GetOrdinal("customer_name"))) oEndUser.CustomerName = drEndUser.GetString(drEndUser.GetOrdinal("customer_name"));

                        listEndUser.Add(oEndUser);
                    }
                }

                return listEndUser;
            }
            catch (Exception ex)
            {
                throw ex;
            }
            finally
            {
                if (drEndUser != null)
                    drEndUser.Close();
            }
        }

        public List<EndUserPriceEntity> GetEndUserPricing(ArgsEntity args, string effectiveDate)
        {
            DbDataReader drEndUserPricing = null;
            List<EndUserPriceEntity> lEndUserPrice = new List<EndUserPriceEntity>();

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@EnduserCode", ValidateString(args.EnduserCode), DbType.String);
                paramCollection.Add("@CustCode", ValidateString(args.CustomerCode), DbType.String);
                paramCollection.Add("@EffectiveDate", effectiveDate, DbType.String);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);

                drEndUserPricing = this.DataAcessService.ExecuteQuery(EndUserSql["GetEndUserPricing"], paramCollection);
                
                if (drEndUserPricing != null && drEndUserPricing.HasRows)
                {
                    int catlogCodeOrdinal = drEndUserPricing.GetOrdinal("catlog_code");
                    int descriptionOrdinal = drEndUserPricing.GetOrdinal("description");
                    int custCodeOrdinal = drEndUserPricing.GetOrdinal("cust_code");
                    int enduserCodeOrdinal = drEndUserPricing.GetOrdinal("enduser_code");
                    int effectiveDateOrdinal = drEndUserPricing.GetOrdinal("effective_date");

                    int lastUpdatedOrdinal = drEndUserPricing.GetOrdinal("last_updated");
                    int originatorOrdinal = drEndUserPricing.GetOrdinal("originator");
                    int priceOrdinal = drEndUserPricing.GetOrdinal("price");
                    int versionOrdinal = drEndUserPricing.GetOrdinal("version");
                    int total_countOrdinal = drEndUserPricing.GetOrdinal("total_count");
                    

                    while (drEndUserPricing.Read())
                    {
                        EndUserPriceEntity oEndUserPrice = EndUserPriceEntity.CreateObject();

                        if (!drEndUserPricing.IsDBNull(catlogCodeOrdinal)) oEndUserPrice.CatlogCode = drEndUserPricing.GetString(catlogCodeOrdinal);
                        if (!drEndUserPricing.IsDBNull(descriptionOrdinal)) oEndUserPrice.Description = drEndUserPricing.GetString(descriptionOrdinal);
                        if (!drEndUserPricing.IsDBNull(custCodeOrdinal)) oEndUserPrice.CustCode = drEndUserPricing.GetString(custCodeOrdinal);
                        if (!drEndUserPricing.IsDBNull(enduserCodeOrdinal)) oEndUserPrice.EndUserCode = drEndUserPricing.GetString(enduserCodeOrdinal);
                        if (!drEndUserPricing.IsDBNull(effectiveDateOrdinal)) oEndUserPrice.EffectiveDateString = drEndUserPricing.GetString(effectiveDateOrdinal);

                        if (!drEndUserPricing.IsDBNull(lastUpdatedOrdinal)) oEndUserPrice.LastUpdatedString = drEndUserPricing.GetString(lastUpdatedOrdinal);

                        if (!drEndUserPricing.IsDBNull(originatorOrdinal)) oEndUserPrice.Originator = drEndUserPricing.GetString(originatorOrdinal);
                        if (!drEndUserPricing.IsDBNull(priceOrdinal)) oEndUserPrice.Price = drEndUserPricing.GetDouble(priceOrdinal);
                        if (!drEndUserPricing.IsDBNull(versionOrdinal)) oEndUserPrice.Version = drEndUserPricing.GetInt32(versionOrdinal);
                        if (!drEndUserPricing.IsDBNull(total_countOrdinal)) oEndUserPrice.TotalCount = drEndUserPricing.GetInt32(total_countOrdinal);

                        if (!string.IsNullOrEmpty(oEndUserPrice.EffectiveDateString))
                            oEndUserPrice.EffectiveDate = Convert.ToDateTime(oEndUserPrice.EffectiveDateString);
                        else
                            oEndUserPrice.EffectiveDate = new DateTime(1900, 1, 1);

                        if (!string.IsNullOrEmpty(oEndUserPrice.LastUpdatedString))
                            oEndUserPrice.LastUpdated = Convert.ToDateTime(oEndUserPrice.LastUpdatedString);
                        else
                            oEndUserPrice.LastUpdated = new DateTime(1900, 1, 1);


                        lEndUserPrice.Add(oEndUserPrice);
                    }
                }

                return lEndUserPrice;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drEndUserPricing != null)
                    drEndUserPricing.Close();
            }
        }

        public List<EnduserEnquiryEntity> GetEnduserEnquiry(ArgsEntity args)
        {

            DbDataReader drEndUser = null;
            string sWhereCls = string.Empty;
            string sOrderCls = string.Empty;

            try
            {
                //if (!string.IsNullOrWhiteSpace(repCode))
                //{
                //    sWhereCls += " AND de.rep_code ='" + repCode + "'";
                //}
                //if (!string.IsNullOrWhiteSpace(custCode))
                //{
                //    sWhereCls += " AND b.cust_code ='" + custCode + "'";
                //}
                //if (!string.IsNullOrWhiteSpace(enduserCode))
                //{
                //    sWhereCls += " AND b.enduser_code ='" + enduserCode + "'";
                //}

                //if (!string.IsNullOrWhiteSpace(enduserCode))
                //    sOrderCls += " ORDER BY b.enduser_code,b.cust_code,b.catlog_code ";
                //else if (!string.IsNullOrWhiteSpace(custCode))
                //    sOrderCls += " ORDER BY b.cust_code,b.enduser_code,b.catlog_code ";

                List<EnduserEnquiryEntity> enduserEnquiryList = new List<EnduserEnquiryEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@EnduserCode", ValidateString(args.EnduserCode), DbType.String);
                paramCollection.Add("@CustCode", ValidateString(args.CustomerCode), DbType.String);
                paramCollection.Add("@RepCode", ValidateString(args.RepCode), DbType.String);
                paramCollection.Add("@StartDate", args.DtStartDate.ToString("dd-MMM-yyyy") + " 00:00:00", DbType.String);
                paramCollection.Add("@EndDate", args.DtEndDate.ToString("dd-MMM-yyyy") + " 23:59:59", DbType.String);

                drEndUser = this.DataAcessService.ExecuteQuery(EndUserSql["GetEnduserEnquiry"], paramCollection);

                if (drEndUser != null && drEndUser.HasRows)
                {
                    int custCodeOrdinal = drEndUser.GetOrdinal("cust_code");
                    int enduserCodeOrdinal = drEndUser.GetOrdinal("enduser_code");
                    int customerNameOrdinal = drEndUser.GetOrdinal("customer_name");
                    int catlogCodeOrdinal = drEndUser.GetOrdinal("catlog_code");
                    int descriptionOrdinal = drEndUser.GetOrdinal("description");
                    int priceOrdinal = drEndUser.GetOrdinal("price");
                    int casesSuppliedOrdinal = drEndUser.GetOrdinal("cases_supplied");
                    int rebateMonthOrdinal = drEndUser.GetOrdinal("rebate_month");
                    int rebPercOrdinal = drEndUser.GetOrdinal("bak_reb_perc");
                    int enduserNameOrdinal = drEndUser.GetOrdinal("enduser_name");
                    int repCodeOrdinal = drEndUser.GetOrdinal("rep_code");
                    int repNameOrdinal = drEndUser.GetOrdinal("rep_name");


                    while (drEndUser.Read())
                    {
                        EnduserEnquiryEntity endUser = EnduserEnquiryEntity.CreateObject();

                        if (!drEndUser.IsDBNull(custCodeOrdinal)) endUser.CustomerCode = drEndUser.GetString(custCodeOrdinal);
                        if (!drEndUser.IsDBNull(enduserCodeOrdinal)) endUser.EndUserCode = drEndUser.GetString(enduserCodeOrdinal);
                        if (!drEndUser.IsDBNull(customerNameOrdinal)) endUser.CustomerName = drEndUser.GetString(customerNameOrdinal);
                        if (!drEndUser.IsDBNull(catlogCodeOrdinal)) endUser.CatlogCode = drEndUser.GetString(catlogCodeOrdinal);
                        if (!drEndUser.IsDBNull(descriptionOrdinal)) endUser.Description = drEndUser.GetString(descriptionOrdinal);
                        if (!drEndUser.IsDBNull(priceOrdinal)) endUser.Price = drEndUser.GetDouble(priceOrdinal);
                        if (!drEndUser.IsDBNull(casesSuppliedOrdinal)) endUser.CasesSupplied = drEndUser.GetDouble(casesSuppliedOrdinal);
                        if (!drEndUser.IsDBNull(rebateMonthOrdinal)) endUser.RebateMonth = drEndUser.GetDateTime(rebateMonthOrdinal);
                        if (!drEndUser.IsDBNull(rebPercOrdinal)) endUser.RebatePerc = drEndUser.GetDouble(rebPercOrdinal);
                        if (!drEndUser.IsDBNull(enduserNameOrdinal)) endUser.EndUserName = drEndUser.GetString(enduserNameOrdinal);
                        if (!drEndUser.IsDBNull(repCodeOrdinal)) endUser.RepCode = drEndUser.GetString(repCodeOrdinal);
                        if (!drEndUser.IsDBNull(repNameOrdinal)) endUser.RepName = drEndUser.GetString(repNameOrdinal);

                        enduserEnquiryList.Add(endUser);
                    }
                }

                return enduserEnquiryList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drEndUser != null)
                    drEndUser.Close();
            }
        }

        public List<EnduserEnquiryEntity> GetEnduserSalesWithAllProducts1(ArgsEntity args, bool bIncludeProductsWithNoSales, bool bActiveOnly)
        {

            DbDataReader drEndUser = null;
            string sWhereCls = string.Empty;
            string sOuterWhereCls = string.Empty;
            string sOrderCls = string.Empty;

            try
            {
                List<EnduserEnquiryEntity> enduserEnquiryList = new List<EnduserEnquiryEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@EnduserCode", ValidateString(args.EnduserCode), DbType.String);
                paramCollection.Add("@CustCode", ValidateString(args.CustomerCode), DbType.String);
                paramCollection.Add("@RepCode", ValidateString(args.RepCode), DbType.String);
                paramCollection.Add("@StartDate", args.DtStartDate.ToString("dd-MMM-yyyy") + " 00:00:00", DbType.String);
                paramCollection.Add("@EndDate", args.DtEndDate.ToString("dd-MMM-yyyy") + " 23:59:59", DbType.String);
                paramCollection.Add("@ProductsWithNoSales", bIncludeProductsWithNoSales ?1:0, DbType.Boolean);
                paramCollection.Add("@ActiveOnly", bActiveOnly ? 1 : 0, DbType.Boolean);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);

                drEndUser = this.DataAcessService.ExecuteQuery(EndUserSql["GetEndUserSalesWithAllProducts"], paramCollection);

                if (drEndUser != null && drEndUser.HasRows)
                {
                    //int custCodeOrdinal = drEndUser.GetOrdinal("cust_code");
                    //int enduserCodeOrdinal = drEndUser.GetOrdinal("enduser_code");
                    //int customerNameOrdinal = drEndUser.GetOrdinal("customer_name");
                    int catlogCodeOrdinal = drEndUser.GetOrdinal("catlog_code");
                    int descriptionOrdinal = drEndUser.GetOrdinal("description");
                    int priceOrdinal = drEndUser.GetOrdinal("price");
                    int casesSuppliedOrdinal = drEndUser.GetOrdinal("cases_supplied");
                    int rebateMonthOrdinal = drEndUser.GetOrdinal("rebate_month");
                    int rebPercOrdinal = drEndUser.GetOrdinal("bak_reb_perc");
                    //int enduserNameOrdinal = drEndUser.GetOrdinal("enduser_name");
                   // int repCodeOrdinal = drEndUser.GetOrdinal("rep_code");
                    //int repNameOrdinal = drEndUser.GetOrdinal("rep_name");
                    int curPriceOrdinal = drEndUser.GetOrdinal("cur_price");
                    int totalcasesOrdinal = drEndUser.GetOrdinal("totalcases");

                    while (drEndUser.Read())
                    {
                        EnduserEnquiryEntity endUser = EnduserEnquiryEntity.CreateObject();

                        //if (!drEndUser.IsDBNull(custCodeOrdinal)) endUser.CustomerCode = drEndUser.GetString(custCodeOrdinal);
                        //if (!drEndUser.IsDBNull(enduserCodeOrdinal)) endUser.EndUserCode = drEndUser.GetString(enduserCodeOrdinal);
                        //if (!drEndUser.IsDBNull(customerNameOrdinal)) endUser.CustomerName = drEndUser.GetString(customerNameOrdinal);
                        if (!drEndUser.IsDBNull(catlogCodeOrdinal)) endUser.CatlogCode = drEndUser.GetString(catlogCodeOrdinal);
                        if (!drEndUser.IsDBNull(descriptionOrdinal)) endUser.Description = drEndUser.GetString(descriptionOrdinal);
                        if (!drEndUser.IsDBNull(priceOrdinal)) endUser.Price = drEndUser.GetDouble(priceOrdinal);
                        if (!drEndUser.IsDBNull(casesSuppliedOrdinal)) endUser.CasesSupplied = drEndUser.GetDouble(casesSuppliedOrdinal);
                        if (!drEndUser.IsDBNull(rebateMonthOrdinal)) endUser.RebateMonth = drEndUser.GetDateTime(rebateMonthOrdinal);
                        if (!drEndUser.IsDBNull(rebPercOrdinal)) endUser.RebatePerc = drEndUser.GetDouble(rebPercOrdinal);
                        //if (!drEndUser.IsDBNull(enduserNameOrdinal)) endUser.EndUserName = drEndUser.GetString(enduserNameOrdinal);
                        //if (!drEndUser.IsDBNull(repCodeOrdinal)) endUser.RepCode = drEndUser.GetString(repCodeOrdinal);
                        //if (!drEndUser.IsDBNull(repNameOrdinal)) endUser.RepName = drEndUser.GetString(repNameOrdinal);
                        if (!drEndUser.IsDBNull(curPriceOrdinal)) endUser.CurPrice = drEndUser.GetDouble(curPriceOrdinal);
                        if (!drEndUser.IsDBNull(totalcasesOrdinal)) endUser.TotalCases = drEndUser.GetDouble(totalcasesOrdinal);

                        enduserEnquiryList.Add(endUser);
                    }
                }

                return enduserEnquiryList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drEndUser != null)
                    drEndUser.Close();
            }
        }

        public List<EnduserEnquiryEntity> GetEnduserSalesWithAllProducts(ArgsEntity args, bool bIncludeProductsWithNoSales, bool bActiveOnly)
        {

            DbDataReader drEndUser = null;
            string sWhereCls = string.Empty;
            string sOuterWhereCls = string.Empty;
            string sOrderCls = string.Empty;

            try
            {
                List<EnduserEnquiryEntity> enduserEnquiryList = new List<EnduserEnquiryEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@EnduserCode", ValidateString(args.EnduserCode), DbType.String);
                paramCollection.Add("@CustCode", ValidateString(args.CustomerCode), DbType.String);
                paramCollection.Add("@RepCode", ValidateString(args.RepCode), DbType.String);
                //paramCollection.Add("@StartDate", args.DtStartDate.ToString("dd-MMM-yyyy") + " 00:00:00", DbType.String);
                //paramCollection.Add("@EndDate", args.DtEndDate.ToString("dd-MMM-yyyy") + " 23:59:59", DbType.String);
                paramCollection.Add("@ProdWithNoSales", bIncludeProductsWithNoSales ? 1 : 0, DbType.Boolean);
                paramCollection.Add("@ActiveOnly", bActiveOnly ? 1 : 0, DbType.Boolean);

                paramCollection.Add("@Month", args.SMonth, DbType.Int32);
                paramCollection.Add("@Year", args.SYear, DbType.Int32);

                //paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                //paramCollection.Add("@ShowCount", 0, DbType.Int32);
		
		
                drEndUser = this.DataAcessService.ExecuteQuery(EndUserSql["GetEndUserSalesWithAllProducts"], paramCollection);

                if (drEndUser != null && drEndUser.HasRows)
                {
                    int custCodeOrdinal = drEndUser.GetOrdinal("cust_code");
                    int enduserCodeOrdinal = drEndUser.GetOrdinal("enduser_code");
                    int customerNameOrdinal = drEndUser.GetOrdinal("customer_name");
                    int catlogCodeOrdinal = drEndUser.GetOrdinal("catlog_code");
                    int descriptionOrdinal = drEndUser.GetOrdinal("description");
                    int priceOrdinal = drEndUser.GetOrdinal("price");
                   // int casesSuppliedOrdinal = drEndUser.GetOrdinal("cases_supplied");
                    //int rebateMonthOrdinal = drEndUser.GetOrdinal("rebate_month");
                    int rebPercOrdinal = drEndUser.GetOrdinal("bak_reb_perc");
                    int enduserNameOrdinal = drEndUser.GetOrdinal("enduser_name");
                    int repCodeOrdinal = drEndUser.GetOrdinal("rep_code");
                    int repNameOrdinal = drEndUser.GetOrdinal("rep_name");
                    int curPriceOrdinal = drEndUser.GetOrdinal("cur_price");
                    int totalcasesOrdinal = drEndUser.GetOrdinal("year_count");

                    int month1Ordinal = drEndUser.GetOrdinal("month1");
                    int month2Ordinal = drEndUser.GetOrdinal("month2");
                    int month3Ordinal = drEndUser.GetOrdinal("month3");
                    int month4Ordinal = drEndUser.GetOrdinal("month4");
                    int month5Ordinal = drEndUser.GetOrdinal("month5");
                    int month6Ordinal = drEndUser.GetOrdinal("month6");
                    int month7Ordinal = drEndUser.GetOrdinal("month7");
                    int month8Ordinal = drEndUser.GetOrdinal("month8");
                    int month9Ordinal = drEndUser.GetOrdinal("month9");
                    int month10Ordinal = drEndUser.GetOrdinal("month10");
                    int month11Ordinal = drEndUser.GetOrdinal("month11");
                    int month12Ordinal = drEndUser.GetOrdinal("month12");

                    int totalcountOrdinal = drEndUser.GetOrdinal("total_count");

                    while (drEndUser.Read())
                    {
                        EnduserEnquiryEntity endUser = EnduserEnquiryEntity.CreateObject();

                        if (!drEndUser.IsDBNull(custCodeOrdinal)) endUser.CustomerCode = drEndUser.GetString(custCodeOrdinal);
                        if (!drEndUser.IsDBNull(enduserCodeOrdinal)) endUser.EndUserCode = drEndUser.GetString(enduserCodeOrdinal);
                        if (!drEndUser.IsDBNull(customerNameOrdinal)) endUser.CustomerName = drEndUser.GetString(customerNameOrdinal);
                        if (!drEndUser.IsDBNull(catlogCodeOrdinal)) endUser.CatlogCode = drEndUser.GetString(catlogCodeOrdinal);
                        if (!drEndUser.IsDBNull(descriptionOrdinal)) endUser.Description = drEndUser.GetString(descriptionOrdinal);
                        if (!drEndUser.IsDBNull(priceOrdinal)) endUser.Price = drEndUser.GetDouble(priceOrdinal);
                        //if (!drEndUser.IsDBNull(casesSuppliedOrdinal)) endUser.CasesSupplied = drEndUser.GetDouble(casesSuppliedOrdinal);
                        //if (!drEndUser.IsDBNull(rebateMonthOrdinal)) endUser.RebateMonth = drEndUser.GetDateTime(rebateMonthOrdinal);
                        if (!drEndUser.IsDBNull(rebPercOrdinal)) endUser.RebatePerc = drEndUser.GetDouble(rebPercOrdinal);
                        if (!drEndUser.IsDBNull(enduserNameOrdinal)) endUser.EndUserName = drEndUser.GetString(enduserNameOrdinal);
                        if (!drEndUser.IsDBNull(repCodeOrdinal)) endUser.RepCode = drEndUser.GetString(repCodeOrdinal);
                        if (!drEndUser.IsDBNull(repNameOrdinal)) endUser.RepName = drEndUser.GetString(repNameOrdinal);
                        if (!drEndUser.IsDBNull(curPriceOrdinal)) endUser.CurPrice = drEndUser.GetDouble(curPriceOrdinal);
                        if (!drEndUser.IsDBNull(totalcasesOrdinal)) endUser.TotalCases = drEndUser.GetDouble(totalcasesOrdinal);

                        if (!drEndUser.IsDBNull(month1Ordinal)) endUser.month1 = drEndUser.GetDouble(month1Ordinal);
                        if (!drEndUser.IsDBNull(month2Ordinal)) endUser.month2 = drEndUser.GetDouble(month2Ordinal);
                        if (!drEndUser.IsDBNull(month3Ordinal)) endUser.month3 = drEndUser.GetDouble(month3Ordinal);
                        if (!drEndUser.IsDBNull(month4Ordinal)) endUser.month4 = drEndUser.GetDouble(month4Ordinal);
                        if (!drEndUser.IsDBNull(month5Ordinal)) endUser.month5 = drEndUser.GetDouble(month5Ordinal);
                        if (!drEndUser.IsDBNull(month6Ordinal)) endUser.month6 = drEndUser.GetDouble(month6Ordinal);
                        if (!drEndUser.IsDBNull(month7Ordinal)) endUser.month7 = drEndUser.GetDouble(month7Ordinal);
                        if (!drEndUser.IsDBNull(month8Ordinal)) endUser.month8 = drEndUser.GetDouble(month8Ordinal);
                        if (!drEndUser.IsDBNull(month9Ordinal)) endUser.month9 = drEndUser.GetDouble(month9Ordinal);
                        if (!drEndUser.IsDBNull(month10Ordinal)) endUser.month10 = drEndUser.GetDouble(month10Ordinal);
                        if (!drEndUser.IsDBNull(month11Ordinal)) endUser.month11 = drEndUser.GetDouble(month11Ordinal);
                        if (!drEndUser.IsDBNull(month12Ordinal)) endUser.month12 = drEndUser.GetDouble(month12Ordinal);

                        if (!drEndUser.IsDBNull(totalcountOrdinal)) endUser.TotalCount = drEndUser.GetInt32(totalcountOrdinal);
                        enduserEnquiryList.Add(endUser);
                    }
                }

                return enduserEnquiryList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drEndUser != null)
                    drEndUser.Close();
            }
        }

        public List<EndUserSalesEntity> GetEnduserSales(ArgsEntity args, bool bActiveOnly)
        {
            DbDataReader drEndUser = null;
            string sWhereCls = string.Empty;

            try
            {
                //if (!string.IsNullOrWhiteSpace(repCode))
                //    sWhereCls += " AND cr.rep_code = '" + repCode + "'";
                //else if (!string.IsNullOrWhiteSpace(custCode))
                //    sWhereCls += " AND b.cust_code = '" + custCode + "'";

                //if (bActiveOnly)
                //    sWhereCls += " AND de.del_flag != 'Y'";

                List<EndUserSalesEntity> endUserSalesList = new List<EndUserSalesEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", ValidateString(args.CustomerCode), DbType.String);
                paramCollection.Add("@RepCode", ValidateString(args.RepCode), DbType.String);
                paramCollection.Add("@StartDate", args.DtStartDate.ToString("dd-MMM-yyyy") + " 00:00:00", DbType.String);
                paramCollection.Add("@EndDate", args.DtEndDate.ToString("dd-MMM-yyyy") + " 23:59:59", DbType.String);
                paramCollection.Add("@ActiveOnly", bActiveOnly ? 1 : 0, DbType.Boolean);

                drEndUser = this.DataAcessService.ExecuteQuery(EndUserSql["GetEndUserSales"], paramCollection);

                if (drEndUser != null && drEndUser.HasRows)
                {
                    int marketOrdinal = drEndUser.GetOrdinal("Market");
                    int costYearOrdinal = drEndUser.GetOrdinal("CostYear");
                    int costMonthOrdinal = drEndUser.GetOrdinal("CostMonth");
                    int casesOrdinal = drEndUser.GetOrdinal("Cases");
                    int tonnesOrdinal = drEndUser.GetOrdinal("Tonnes");

                    while (drEndUser.Read())
                    {
                        EndUserSalesEntity endUserSales = EndUserSalesEntity.CreateObject();

                        if (!drEndUser.IsDBNull(marketOrdinal)) endUserSales.Market = drEndUser.GetString(marketOrdinal);
                        if (!drEndUser.IsDBNull(costYearOrdinal)) endUserSales.Year = drEndUser.GetInt32(costYearOrdinal);
                        if (!drEndUser.IsDBNull(costMonthOrdinal)) endUserSales.Month = drEndUser.GetInt32(costMonthOrdinal);
                        if (!drEndUser.IsDBNull(casesOrdinal)) endUserSales.Cases = drEndUser.GetDouble(casesOrdinal);
                        if (!drEndUser.IsDBNull(tonnesOrdinal)) endUserSales.Tonnes1 = drEndUser.GetFloat(tonnesOrdinal);

                        endUserSalesList.Add(endUserSales);
                    }
                }

                return endUserSalesList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drEndUser != null)
                    drEndUser.Close();
            }
        }

        public List<CustomerEntity> GetEndUserCustomers(ArgsEntity args)
        {

            DbDataReader drEndUserCustomers = null;

            try
            {
                List<CustomerEntity> customerList = new List<CustomerEntity>();
                CustomerEntity customer;
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@EnduserCode", ValidateString(args.EnduserCode), DbType.String);
              //  paramCollection.Add("@CustCode", ValidateString(args.CustomerCode), DbType.String);

                drEndUserCustomers = this.DataAcessService.ExecuteQuery(EndUserSql["GetEndUserCustomers"], paramCollection);

                if (drEndUserCustomers != null && drEndUserCustomers.HasRows)
                {
                    int custCodeOrdinal = drEndUserCustomers.GetOrdinal("cust_code");
                    int enduserCodeOrdinal = drEndUserCustomers.GetOrdinal("enduser_code");
                    int nameOrdinal = drEndUserCustomers.GetOrdinal("name");
                    int totcount1Ordinal = drEndUserCustomers.GetOrdinal("totcount");
                    int primarydistOrdinal = drEndUserCustomers.GetOrdinal("primary_dist");
                    //int address2Ordinal = drEndUser.GetOrdinal("address_2");

                    //int cityOrdinal = drEndUser.GetOrdinal("city");
                    //int stateOrdinal = drEndUser.GetOrdinal("state");
                    //int postcodeOrdinal = drEndUser.GetOrdinal("postcode");
                    //int telephoneOrdinal = drEndUser.GetOrdinal("telephone");
                    //int contactOrdinal = drEndUser.GetOrdinal("contact");

                    //int repCodeOrdinal = drEndUser.GetOrdinal("rep_code");
                    //int emailAddressOrdinal = drEndUser.GetOrdinal("email_address");
                    //int versionOrdinal = drEndUser.GetOrdinal("version");
                    //int delFlagOrdinal = drEndUser.GetOrdinal("del_flag");


                    while (drEndUserCustomers.Read())
                    {
                        customer = CustomerEntity.CreateObject();
                        if (!drEndUserCustomers.IsDBNull(custCodeOrdinal)) customer.CustomerCode = drEndUserCustomers.GetString(custCodeOrdinal);
                        if (!drEndUserCustomers.IsDBNull(enduserCodeOrdinal)) customer.EndUserCode = drEndUserCustomers.GetString(enduserCodeOrdinal);
                        if (!drEndUserCustomers.IsDBNull(nameOrdinal)) customer.Name = drEndUserCustomers.GetString(nameOrdinal);
                        if (!drEndUserCustomers.IsDBNull(totcount1Ordinal)) customer.TotalCount = drEndUserCustomers.GetInt32(totcount1Ordinal);
                        if (!drEndUserCustomers.IsDBNull(primarydistOrdinal)) customer.PrimaryDist = drEndUserCustomers.GetString(primarydistOrdinal);
                        //if (!drEndUser.IsDBNull(address2Ordinal)) endUser.Address2 = drEndUser.GetString(address2Ordinal);

                        //if (!drEndUser.IsDBNull(cityOrdinal)) endUser.City = drEndUser.GetString(cityOrdinal);
                        //if (!drEndUser.IsDBNull(stateOrdinal)) endUser.State = drEndUser.GetString(stateOrdinal);
                        //if (!drEndUser.IsDBNull(postcodeOrdinal)) endUser.PostCode = drEndUser.GetString(postcodeOrdinal);
                        //if (!drEndUser.IsDBNull(telephoneOrdinal)) endUser.Telephone = drEndUser.GetString(telephoneOrdinal);
                        //if (!drEndUser.IsDBNull(contactOrdinal)) endUser.Contact = drEndUser.GetString(contactOrdinal);

                        //if (!drEndUser.IsDBNull(repCodeOrdinal)) endUser.RepCode = drEndUser.GetString(repCodeOrdinal);
                        //if (!drEndUser.IsDBNull(emailAddressOrdinal)) endUser.EmailAddress = drEndUser.GetString(emailAddressOrdinal);
                        //if (!drEndUser.IsDBNull(versionOrdinal)) endUser.Version = drEndUser.GetInt32(versionOrdinal);
                        //if (!drEndUser.IsDBNull(delFlagOrdinal)) endUser.IsActive = drEndUser.GetString(delFlagOrdinal) != "Y";
                        customerList.Add(customer);
                    }
                }

                return customerList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drEndUserCustomers != null)
                    drEndUserCustomers.Close();
            }
        }

        public bool Transfer(EndUserEntity endUser, ArgsEntity args)
        {
            bool isSuccess = true;
            int? iCount = 0;
            DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            try
            {

                paramCollection.Add("@EnduserCode", ValidateString(endUser.EndUserCode), DbType.String);
                paramCollection.Add("@CustCode", ValidateString(endUser.CustomerCode), DbType.String);

                iCount = (int)this.DataAcessService.GetOneValue(EndUserSql["GetEndUserPriceCount"], paramCollection);

                // Transfer End User Price
                if (iCount == null || iCount == 0)
                {
                    paramCollection = new DbInputParameterCollection();
                    paramCollection.Add("@EnduserCode", ValidateString(endUser.EndUserCode), DbType.String);
                    paramCollection.Add("@CustCode", ValidateString(endUser.CustomerCode), DbType.String);
                    paramCollection.Add("@OldCustCode", ValidateString(args.CustomerCode), DbType.String);

                    isSuccess = this.DataAcessService.ExecuteNonQuery(EndUserSql["TransferEndUserPrice"], paramCollection) > 0;

                }

                // Check whether the Old Customer is the Primary Distributor of given End User, if so set the new Customer the Primary Distributor
                paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@EnduserCode", ValidateString(endUser.EndUserCode), DbType.String);
                paramCollection.Add("@OldCustCode", ValidateString(args.CustomerCode), DbType.String);
                string sIsPrimaryDist = (string)this.DataAcessService.GetOneValue(EndUserSql["IsPrimaryDistributor"], paramCollection);

                //Comment by indunil,17/07/2013, handling by transaction scope
                //if (isSuccess)
                //{
                    // Deactivate the End User for the Old Customer
                    paramCollection = new DbInputParameterCollection();
                    paramCollection.Add("@EnduserCode", ValidateString(endUser.EndUserCode), DbType.String);
                    paramCollection.Add("@CustCode", ValidateString(args.CustomerCode), DbType.String);
                    paramCollection.Add("@DelFlag", "Y", DbType.String);
                    paramCollection.Add("@PrimaryDist", "N", DbType.String);
                    isSuccess = this.DataAcessService.ExecuteNonQuery(EndUserSql["UpdateEndUserStatus"], paramCollection) > 0;
                //}

                //if (!isSuccess)
                //    return isSuccess;

                paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@EnduserCode", ValidateString(endUser.EndUserCode), DbType.String);
                paramCollection.Add("@CustCode", ValidateString(endUser.CustomerCode), DbType.String);
                iCount = (int)this.DataAcessService.GetOneValue(EndUserSql["GetEndUserCount"], paramCollection);

                if (iCount == null || iCount == 0)
                {
                    paramCollection = new DbInputParameterCollection();
                    paramCollection.Add("@EnduserCode", ValidateString(endUser.EndUserCode), DbType.String);
                    paramCollection.Add("@CustCode", ValidateString(endUser.CustomerCode), DbType.String);
                    paramCollection.Add("@OldCustCode", ValidateString(args.CustomerCode), DbType.String);
                    paramCollection.Add("@PrimaryDist", (sIsPrimaryDist == "Y" ? "Y" : "N"), DbType.String);

                    isSuccess = this.DataAcessService.ExecuteNonQuery(EndUserSql["TransferEndUser"], paramCollection) > 0;
                }
                else
                {
                    paramCollection = new DbInputParameterCollection();
                    paramCollection.Add("@EnduserCode", ValidateString(endUser.EndUserCode), DbType.String);
                    paramCollection.Add("@CustCode", ValidateString(endUser.CustomerCode), DbType.String);
                    paramCollection.Add("@DelFlag", "", DbType.String);
                    paramCollection.Add("@PrimaryDist", (sIsPrimaryDist == "Y" ? "Y" : "N"), DbType.String);

                    isSuccess = this.DataAcessService.ExecuteNonQuery(EndUserSql["UpdateEndUserStatus"], paramCollection) > 0;
                }

                //Comment by indunil,17/07/2013, handling by transaction scope
                // Write to Log
                //if (isSuccess)
                //{
                    paramCollection = new DbInputParameterCollection();
                    paramCollection.Add("@EnduserCode", ValidateString(endUser.EndUserCode), DbType.String);
                    paramCollection.Add("@OldCustCode", ValidateString(args.CustomerCode), DbType.String);
                    paramCollection.Add("@Originator", args.Originator, DbType.String);
                    paramCollection.Add("@TransferredOn", DateTime.Now.ToUniversalTime(), DbType.DateTime);

                    isSuccess = this.DataAcessService.ExecuteNonQuery(EndUserSql["InsertEndUserTransferLog"], paramCollection) > 0;
                //}
            }
            catch
            {
                isSuccess = false;
            }
            return isSuccess;
        }

        public EndUserPriceEntity GetEndUserPrice(ArgsEntity args, DateTime effectiveDate, string catalogCode)
        {
            DbDataReader drEndUserPrice = null;

            try
            {
                EndUserPriceEntity endUserPriceEntity = new EndUserPriceEntity();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@EnduserCode", ValidateString(args.EnduserCode), DbType.String);
                paramCollection.Add("@CustCode", ValidateString(args.CustomerCode), DbType.String);
                paramCollection.Add("@EffectiveDate", effectiveDate.ToString("dd-MMM-yyyy"), DbType.String);
                paramCollection.Add("@CatalogCode", catalogCode, DbType.String);

                drEndUserPrice = this.DataAcessService.ExecuteQuery(EndUserSql["GetEndUserPrice"], paramCollection);

                //drEndUserPrice = (IngresDataReader)oDataHandle.ExecuteSelect(string.Format(CRMSQLs.GetEndUserPrice,
                //    sCustCode,
                //    sEndUserCode,
                //    effectiveDate.ToString("dd-MMM-yyyy"),
                //    catalogCode));

                if (drEndUserPrice != null && drEndUserPrice.HasRows)
                {
                    int custCodeOrdinal = drEndUserPrice.GetOrdinal("cust_code");
                    int enduserCodeOrdinal = drEndUserPrice.GetOrdinal("enduser_code");
                    int effectiveDateOrdinal = drEndUserPrice.GetOrdinal("effective_date");
                    int lastUpdatedOrdinal = drEndUserPrice.GetOrdinal("last_updated");
                    int originatorOrdinal = drEndUserPrice.GetOrdinal("originator");

                    int catlogCodeOrdinal = drEndUserPrice.GetOrdinal("catlog_code");
                    int priceOrdinal = drEndUserPrice.GetOrdinal("price");
                    int versionOrdinal = drEndUserPrice.GetOrdinal("version");
                    int custNameOrdinal = drEndUserPrice.GetOrdinal("cust_name");
                    int enduserNameOrdinal = drEndUserPrice.GetOrdinal("enduser_name");


                    if (drEndUserPrice.Read())
                    {
                        if (!drEndUserPrice.IsDBNull(custCodeOrdinal)) endUserPriceEntity.CustCode = drEndUserPrice.GetString(custCodeOrdinal);
                        if (!drEndUserPrice.IsDBNull(enduserCodeOrdinal)) endUserPriceEntity.EndUserCode = drEndUserPrice.GetString(enduserCodeOrdinal);
                        if (!drEndUserPrice.IsDBNull(effectiveDateOrdinal)) endUserPriceEntity.EffectiveDate = drEndUserPrice.GetDateTime(effectiveDateOrdinal).ToLocalTime();
                        if (!drEndUserPrice.IsDBNull(lastUpdatedOrdinal)) endUserPriceEntity.LastUpdated = drEndUserPrice.GetDateTime(lastUpdatedOrdinal).ToLocalTime();
                        if (!drEndUserPrice.IsDBNull(originatorOrdinal)) endUserPriceEntity.Originator = drEndUserPrice.GetString(originatorOrdinal);

                        if (!drEndUserPrice.IsDBNull(catlogCodeOrdinal)) endUserPriceEntity.CatlogCode = drEndUserPrice.GetString(catlogCodeOrdinal);
                        if (!drEndUserPrice.IsDBNull(priceOrdinal)) endUserPriceEntity.Price = drEndUserPrice.GetDouble(priceOrdinal);
                        if (!drEndUserPrice.IsDBNull(versionOrdinal)) endUserPriceEntity.Version = drEndUserPrice.GetInt32(versionOrdinal);
                        if (!drEndUserPrice.IsDBNull(custNameOrdinal)) endUserPriceEntity.CustomerName = drEndUserPrice.GetString(custNameOrdinal);
                        if (!drEndUserPrice.IsDBNull(enduserNameOrdinal)) endUserPriceEntity.EndUserName = drEndUserPrice.GetString(enduserNameOrdinal);
                    }
                }

                return endUserPriceEntity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drEndUserPrice != null)
                    drEndUserPrice.Close();
            }
        }

        public bool SavePriceList(ArgsEntity args, DateTime dtmEffectiveDate, List<EndUserPriceEntity> listEndUserPrice)
        {
            DbInputParameterCollection paramCollection = new DbInputParameterCollection();
   
            try
            {
                // Delete all records for given Customer, End User & Effective Date  
                paramCollection.Add("@EnduserCode", ValidateString(args.EnduserCode), DbType.String);
                paramCollection.Add("@CustCode", ValidateString(args.CustomerCode), DbType.String);
                paramCollection.Add("@EffectiveDate", dtmEffectiveDate.ToString("dd-MMM-yyyy"), DbType.String);

                this.DataAcessService.ExecuteNonQuery(EndUserSql["DeleteEndUserPricelist"], paramCollection);
                bool isSucc = true;
                foreach (EndUserPriceEntity price in listEndUserPrice)
                {
                    paramCollection = new DbInputParameterCollection();
                    if (price.CatlogCode != "" )
                    {
                        paramCollection.Add("@EnduserCode", ValidateString(args.EnduserCode), DbType.String);
                        paramCollection.Add("@CustCode", ValidateString(args.CustomerCode), DbType.String);
                        paramCollection.Add("@EffectiveDate", dtmEffectiveDate.ToString("dd-MMM-yyyy"), DbType.String);
                        paramCollection.Add("@LastUpdated", DateTime.Today.ToString("dd-MMM-yyyy"), DbType.String);
                        paramCollection.Add("@Originator", ValidateString(args.Originator), DbType.String);
                        paramCollection.Add("@CatlogCode", ValidateString(price.CatlogCode), DbType.String);
                        paramCollection.Add("@Price", price.Price, DbType.String);
                        paramCollection.Add("@Version", args.Status , DbType.String);

                        isSucc = this.DataAcessService.ExecuteNonQuery(EndUserSql["InsertEnduserPriceList"], paramCollection) > 0;

                        if (!isSucc)
                            break;
                    }
                }

                return isSucc;

            }
            catch (Exception)
            {
                throw;
            }
        }

        #region CTC mobile DAO methods

        //public EndUserEntity GetEndUserByMarketId(ArgsEntity args)
        //{

        //    DbDataReader drEndUser = null;

        //    try
        //    {
        //        EndUserEntity endUser = null;
        //        DbInputParameterCollection paramCollection = new DbInputParameterCollection();
        //        paramCollection.Add("@EnduserCode", ValidateString(args.EnduserCode), DbType.String);
        //        paramCollection.Add("@MarketId", args.MarketId, DbType.Int32);

        //        drEndUser = this.DataAcessService.ExecuteQuery(EndUserSql["GetEndUserByMarketId"], paramCollection);

        //        if (drEndUser != null && drEndUser.HasRows)
        //        {
        //            endUser = EndUserEntity.CreateObject();
        //            int enduserCodeOrdinal = drEndUser.GetOrdinal("enduser_code");
        //            int nameOrdinal = drEndUser.GetOrdinal("name");
        //            int address1Ordinal = drEndUser.GetOrdinal("address_1");
        //            int address2Ordinal = drEndUser.GetOrdinal("address_2");
        //            int cityOrdinal = drEndUser.GetOrdinal("city");
        //            int stateOrdinal = drEndUser.GetOrdinal("state");
        //            int telephoneOrdinal = drEndUser.GetOrdinal("telephone");

        //            int pheroheryOrdinal = drEndUser.GetOrdinal("pherohery");
        //            int outleOrdinal = drEndUser.GetOrdinal("email_address");
        //            int versionOrdinal = drEndUser.GetOrdinal("version"); 
        //            int delFlagOrdinal = drEndUser.GetOrdinal("del_flag");
        //            int primaryDistOrdinal = drEndUser.GetOrdinal("primary_dist");
        //            int ratingOrdinal = drEndUser.GetOrdinal("rating");
        //            int primaryRepNameOrdinal = drEndUser.GetOrdinal("PrimaryRep");
        //            int secondaryRepNameOrdinal = drEndUser.GetOrdinal("SecondaryRep");
        //            int endUserTypeOrdinal = drEndUser.GetOrdinal("enduser_type");
        //            int customernameOrdinal = drEndUser.GetOrdinal("customername");

        //            if (drEndUser.Read())
        //            {
        //                if (!drEndUser.IsDBNull(enduserCodeOrdinal)) endUser.EndUserCode = drEndUser.GetString(enduserCodeOrdinal);
        //                if (!drEndUser.IsDBNull(nameOrdinal)) endUser.Name = drEndUser.GetString(nameOrdinal);
        //                if (!drEndUser.IsDBNull(address1Ordinal)) endUser.Address1 = drEndUser.GetString(address1Ordinal);
        //                if (!drEndUser.IsDBNull(address2Ordinal)) endUser.Address2 = drEndUser.GetString(address2Ordinal);

        //                if (!drEndUser.IsDBNull(cityOrdinal)) endUser.City = drEndUser.GetString(cityOrdinal);
        //                if (!drEndUser.IsDBNull(stateOrdinal)) endUser.State = drEndUser.GetString(stateOrdinal);
        //                if (!drEndUser.IsDBNull(telephoneOrdinal)) endUser.Telephone = drEndUser.GetString(telephoneOrdinal);
        //                if (!drEndUser.IsDBNull(contactOrdinal)) endUser.Contact = drEndUser.GetString(contactOrdinal);

        //                if (!drEndUser.IsDBNull(repCodeOrdinal)) endUser.RepCode = drEndUser.GetString(repCodeOrdinal);
        //                if (!drEndUser.IsDBNull(emailAddressOrdinal)) endUser.EmailAddress = drEndUser.GetString(emailAddressOrdinal);
        //                if (!drEndUser.IsDBNull(versionOrdinal)) endUser.Version = drEndUser.GetInt32(versionOrdinal);
        //                if (!drEndUser.IsDBNull(delFlagOrdinal)) endUser.IsActive = drEndUser.GetString(delFlagOrdinal) != "Y";
        //                if (!drEndUser.IsDBNull(primaryDistOrdinal)) endUser.PrimaryDistributor = drEndUser.GetString(primaryDistOrdinal);
        //                if (!drEndUser.IsDBNull(ratingOrdinal)) endUser.Rating = drEndUser.GetInt32(ratingOrdinal);

        //                if (!drEndUser.IsDBNull(primaryRepNameOrdinal)) endUser.PrimaryRepName = drEndUser.GetString(primaryRepNameOrdinal);
        //                if (!drEndUser.IsDBNull(secondaryRepNameOrdinal)) endUser.SecondaryRepName = drEndUser.GetString(secondaryRepNameOrdinal);
        //                if (!drEndUser.IsDBNull(endUserTypeOrdinal)) endUser.EndUserType = drEndUser.GetString(endUserTypeOrdinal);
        //                if (!drEndUser.IsDBNull(customernameOrdinal)) endUser.CustomerName = drEndUser.GetString(customernameOrdinal);

        //            }
        //        }

        //        return endUser;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if (drEndUser != null)
        //            drEndUser.Close();
        //    }
        //}
        #endregion

    }
}
