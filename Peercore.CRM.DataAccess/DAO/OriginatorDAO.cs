﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.DataAccess.Common;
using Peercore.Workflow.Common;
using System.Data.SqlClient;
using Peercore.CRM.Model;

namespace Peercore.CRM.DataAccess.DAO
{
    public class OriginatorDAO : BaseDAO
    {
        string sChildOriginators = "";
        OriginatorEntity oOriginator;

        List<string> lRepCodes;
        string sChildRepCodes = "";

        private DbSqlAdapter originatorSql { get; set; }

        private void RegisterSql()
        {
            this.originatorSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.OriginatorSql.xml", ApplicationService.Instance.DbProvider);
        }

        public OriginatorDAO()
        {
            RegisterSql();
        }

        public OriginatorDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }


        public OriginatorEntity CreateObject()
        {
            return OriginatorEntity.CreateObject();
        }

        public OriginatorEntity CreateObject(int entityId)
        {
            return OriginatorEntity.CreateObject(entityId);
        }

        //public List<OriginatorEntity> GetReps()
        //{
        //    DbDataReader drOriginator = null;

        //    List<OriginatorEntity> RepList = null;
        //    OriginatorEntity originator = CreateObject();
        //    try
        //    {
        //        drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetReps"]);
        //        if (drOriginator != null && drOriginator.HasRows)
        //        {
        //            RepList = new List<OriginatorEntity>();

        //            int originatorOrdinal = drOriginator.GetOrdinal("originator");
        //            int countOrdinal = drOriginator.GetOrdinal("count");

        //            while (drOriginator.Read())
        //            {
        //                originator = CreateObject();
        //                if (!drOriginator.IsDBNull(countOrdinal)) originator.TotalCount = drOriginator.GetInt32(countOrdinal);
        //                if (!drOriginator.IsDBNull(originatorOrdinal)) originator.Name = drOriginator.GetString(originatorOrdinal);

        //                RepList.Add(originator);
        //            }
        //        }
        //        return RepList;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if (drOriginator != null)
        //            drOriginator.Close();
        //    }
        //}

        public List<OriginatorEntity> GetChildOriginatorsList(ArgsEntity args)
        {
            List<OriginatorEntity> lOriginators = new List<OriginatorEntity>();

            try
            {
                lOriginators.Clear();

                if (args.Originator != "")
                {
                    if (args.ManagerMode)
                        lOriginators = GetChildOriginatorList(args.Originator, true);
                }

                return lOriginators;
            }
            catch
            {
                throw;
            }
        }

        //private List<OriginatorEntity> GetChildren(string sParent, string departmentId, bool bIsList = false)
        //{
        //    DataTable dtChildOrig = new DataTable();
        //    DbDataReader idrOriginator = null;
        //    string sOriginator = "";
        //    string sChildOriginators = "";
        //    OriginatorEntity oOriginator;
        //    List<OriginatorEntity> lOriginators = new List<OriginatorEntity>();
        //    try
        //    {
        //        DbInputParameterCollection paramCollection = new DbInputParameterCollection();
        //        paramCollection.Add("@deptId", departmentId, DbType.String);
        //        paramCollection.Add("@originator", sParent, DbType.String);
        //        idrOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetCompanyOriginators"], paramCollection);

        //        if (idrOriginator != null && idrOriginator.HasRows)
        //        {
        //            int originatorIdOrdinal = idrOriginator.GetOrdinal("originator_id");
        //            int originatorOrdinal = idrOriginator.GetOrdinal("originator");
        //            int nameOrdinal = idrOriginator.GetOrdinal("name");
        //            int internetAddOrdinal = idrOriginator.GetOrdinal("internet_add");

        //            while (idrOriginator.Read())
        //            {
        //                if (!bIsList)   // Form the String for Where Clause
        //                {
        //                    if (!idrOriginator.IsDBNull(originatorOrdinal)) sOriginator = idrOriginator.GetString(originatorOrdinal);

        //                    sChildOriginators += "'" + sOriginator + "' ,";
        //                }
        //                else            // Populate a List with child Originators
        //                {
        //                    oOriginator = CreateObject();
        //                    if (!idrOriginator.IsDBNull(originatorIdOrdinal)) oOriginator.OriginatorId = idrOriginator.GetInt32(originatorIdOrdinal);
        //                    if (!idrOriginator.IsDBNull(originatorOrdinal)) oOriginator.UserName = idrOriginator.GetString(originatorOrdinal);
        //                    if (!idrOriginator.IsDBNull(nameOrdinal)) oOriginator.Name = idrOriginator.GetString(nameOrdinal);
        //                    if (!idrOriginator.IsDBNull(internetAddOrdinal)) oOriginator.Email = idrOriginator.GetString(internetAddOrdinal);
        //                    lOriginators.Add(oOriginator);
        //                }
        //            }
        //        }
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if (idrOriginator != null)
        //            idrOriginator.Close();
        //    }
        //    return lOriginators;
        //}

        private List<OriginatorEntity> GetChildOriginatorList(string parent, bool bIsList = false)
        {
            //DataTable dtChildOrig = new DataTable();
            //List<OriginatorEntity> originatorlist = new List<OriginatorEntity>();
            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@Originator", parent, DbType.String);

            //    dtChildOrig = this.DataAcessService.ExecuteDisconnected(originatorSql["GetChildOriginators"], paramCollection);

            //    if (dtChildOrig != null)
            //    {
            //        for (int i = 0; i < dtChildOrig.Rows.Count; i++)
            //        {
            //            if (!bIsList)
            //            {
            //                sChildOriginators += "'" + dtChildOrig.Rows[i]["originator"].ToString() + "' ,";
            //            }
            //            else
            //            {
            //                oOriginator = OriginatorEntity.CreateObject();
            //                oOriginator.OriginatorId = int.Parse(dtChildOrig.Rows[i]["originator_id"].ToString());
            //                oOriginator.UserName = dtChildOrig.Rows[i]["originator"].ToString().Trim();
            //                oOriginator.Name = dtChildOrig.Rows[i]["name"].ToString();
            //                oOriginator.Email = dtChildOrig.Rows[i]["internet_add"].ToString();
            //                oOriginator.RepType = dtChildOrig.Rows[i]["rep_type"].ToString();
            //                oOriginator.ClientType = dtChildOrig.Rows[i]["client_type"].ToString();
            //                oOriginator.RepCode = dtChildOrig.Rows[i]["rep_code"].ToString();
            //                originatorlist.Add(oOriginator);
            //            }
            //            originatorlist = originatorlist.Concat(GetChildOriginatorList(dtChildOrig.Rows[i]["originator"].ToString(), bIsList)).ToList();   // Recursive Call
            //        }
            //    }
            //}
            //catch
            //{
            //    throw;
            //}
            //return originatorlist;

            DataTable objDS = new DataTable();
            OriginatorEntity oOriginator = null;
            List<OriginatorEntity> originatorlist = new List<OriginatorEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetChildOriginators";

                    objCommand.Parameters.AddWithValue("@Originator", parent);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            oOriginator = new OriginatorEntity();
                            oOriginator.Name = item["name"].ToString();
                            oOriginator.OriginatorId = Convert.ToInt32(item["originator_id"]);
                            oOriginator.UserName = item["originator"].ToString();
                            oOriginator.Email = item["internet_add"].ToString();
                            oOriginator.RepType = item["rep_type"].ToString();
                            oOriginator.ClientType = item["client_type"].ToString();
                            oOriginator.RepCode = item["rep_code"].ToString();
                            originatorlist.Add(oOriginator);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }


            return originatorlist;
        }

        public string GetChildOriginators(string parent, bool bIsList = false, string text = "")
        {
            DataTable dtChildOrig = new DataTable();
            List<OriginatorEntity> lOriginators = new List<OriginatorEntity>();
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@originator", parent, DbType.String);

                dtChildOrig = this.DataAcessService.ExecuteDisconnected(originatorSql["GetChildOriginators"], paramCollection);

                if (dtChildOrig != null)
                {
                    for (int i = 0; i < dtChildOrig.Rows.Count; i++)
                    {
                        if (!bIsList)   // Form the String for Where Clause
                        {
                            text += "'" + dtChildOrig.Rows[i]["originator"].ToString() + "' ,";
                        }
                        else            // Populate a List with child Originators
                        {
                            oOriginator = OriginatorEntity.CreateObject();
                            oOriginator.OriginatorId = int.Parse(dtChildOrig.Rows[i]["originator_id"].ToString());
                            oOriginator.UserName = dtChildOrig.Rows[i]["originator"].ToString().Trim();
                            oOriginator.Name = dtChildOrig.Rows[i]["name"].ToString();
                            oOriginator.Email = dtChildOrig.Rows[i]["internet_add"].ToString();
                            oOriginator.RepType = dtChildOrig.Rows[i]["rep_type"].ToString();
                            oOriginator.ClientType = dtChildOrig.Rows[i]["client_type"].ToString();
                            lOriginators.Add(oOriginator);
                        }

                        text = GetChildOriginators(dtChildOrig.Rows[i]["originator"].ToString(), bIsList, text);   // Recursive Call
                    }
                }
            }
            catch
            {
                throw;
            }

            return text;
        }

        public List<string> GetChildReps(string originator, bool managerMode, ref string sWhereCls, string childOriginators = null)
        {
            string sRepCode = "";
            DbDataReader idrRep;

            try
            {
                string sChildRepCodes = "";
                List<string> lRepCodes = new List<string>();

                if (originator != "")
                {
                    sRepCode = "";

                    // Get the Rep Codes of this Originator

                    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                    paramCollection.Add("@originator", originator, DbType.String);

                    idrRep = this.DataAcessService.ExecuteQuery(originatorSql["GetAllRepCodes"], paramCollection);

                    if (idrRep != null && idrRep.HasRows)
                    {
                        while (idrRep.Read())
                        {
                            if (!idrRep.IsDBNull(idrRep.GetOrdinal("rep_code")))
                            {
                                sRepCode += "'" + idrRep.GetString(idrRep.GetOrdinal("rep_code")) + "',";
                                lRepCodes.Add(idrRep.GetString(idrRep.GetOrdinal("rep_code")));
                            }
                        }
                    }

                    if (idrRep != null)
                        idrRep.Close();

                    if (sRepCode != "")
                        sRepCode = sRepCode.Remove(sRepCode.Length - 1, 1);  // Remove the Last Comma


                    if (managerMode == true)    // Quinn - 16-Mar-2011
                    {
                        GetChildRepCodes(originator, ref lRepCodes, ref sChildRepCodes, childOriginators);
                        if (sChildRepCodes != "")
                        {
                            if (sRepCode != "")
                                sChildRepCodes += sRepCode;
                            else
                                sChildRepCodes = sChildRepCodes.Remove(sChildRepCodes.Length - 1, 1);  // Remove the Last Comma
                            sWhereCls = " rep_code IN (" + sChildRepCodes + ")";
                        }
                        else
                            sWhereCls = " rep_code IN (" + sRepCode + ")";
                    }
                    else
                        sWhereCls = " rep_code IN (" + sRepCode + ")";
                }

                return lRepCodes;
            }
            catch
            {
                throw;
            }
        }

        private void GetChildRepCodes(string sParent, ref List<string> lisRepCodes, ref string childRepCode, string childOriginators = null)
        {
            DataTable dtChildOrig = new DataTable();

            try
            {
                if (childOriginators == null)
                {
                    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                    paramCollection.Add("@originator", sParent, DbType.String);

                    dtChildOrig = this.DataAcessService.ExecuteDisconnected(originatorSql["GetChildRepCodes"], paramCollection);

                    if (dtChildOrig != null)
                    {
                        for (int i = 0; i < dtChildOrig.Rows.Count; i++)
                        {
                            childRepCode += "'" + dtChildOrig.Rows[i]["rep_code"].ToString() + "' ,";

                            lisRepCodes.Add(dtChildOrig.Rows[i]["rep_code"].ToString());

                            GetChildRepCodes(dtChildOrig.Rows[i]["originator"].ToString(), ref lisRepCodes, ref childRepCode, childOriginators);   // Recursive Call
                        }
                    }
                }
                else
                {

                    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                    paramCollection.Add("@ChildOriginators", childOriginators, DbType.String);

                    dtChildOrig = this.DataAcessService.ExecuteDisconnected(originatorSql["GetChildRepCodesNew"], paramCollection);
                    //   dtChildOrig = this.DataAcessService.ExecuteDisconnected(originatorSql["GetChildRepCodesNew"].Format(childOriginators));

                    if (dtChildOrig != null)
                    {
                        for (int i = 0; i < dtChildOrig.Rows.Count; i++)
                        {
                            childRepCode += "'" + dtChildOrig.Rows[i]["rep_code"].ToString() + "' ,";

                            lisRepCodes.Add(dtChildOrig.Rows[i]["rep_code"].ToString());

                            GetChildRepCodes(dtChildOrig.Rows[i]["originator"].ToString(), ref lisRepCodes, ref childRepCode);   // Recursive Call
                        }
                    }

                }
            }
            catch
            {
                throw;
            }
        }

        public OriginatorEntity GetLoginDetails(string userName, string password)
        {
            //DbDataReader idrOriginator = null;
            //OriginatorEntity oOriginator = OriginatorEntity.CreateObject();

            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@originator", userName, DbType.String);
            //    paramCollection.Add("@password", password, DbType.String);

            //    idrOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetLoginDetails"], paramCollection);

            //    if (idrOriginator != null && idrOriginator.HasRows)
            //    {
            //        int originatorIdOrdinal = idrOriginator.GetOrdinal("originator_id");
            //        int nameOrdinal = idrOriginator.GetOrdinal("name");
            //        int deptIdOrdinal = idrOriginator.GetOrdinal("default_dept_id");
            //        int clientTypeOrdinal = idrOriginator.GetOrdinal("client_type");
            //        int repCodeOrdinal = idrOriginator.GetOrdinal("rep_code");
            //        int repTypeOrdinal = idrOriginator.GetOrdinal("rep_type");
            //        int intAddOrdinal = idrOriginator.GetOrdinal("internet_add");
            //        int routeIdOrdinal = idrOriginator.GetOrdinal("route_id");
            //        int routeNameOrdinal = idrOriginator.GetOrdinal("route_name");
            //        int deptStringOrdinal = idrOriginator.GetOrdinal("dept_string");
            //        int accessTokenOrdinal = idrOriginator.GetOrdinal("access_token");
            //        int distributornameOrdinal = idrOriginator.GetOrdinal("distributorname");
            //        int distributoraddressOrdinal = idrOriginator.GetOrdinal("distributoraddress");
            //        int distributortelOrdinal = idrOriginator.GetOrdinal("distributortel");
            //        int invoiceNoOrdinal = idrOriginator.GetOrdinal("invoice_no");

            //        while (idrOriginator.Read())
            //        {
            //            if (!idrOriginator.IsDBNull(nameOrdinal)) oOriginator.Name = idrOriginator.GetString(nameOrdinal);
            //            if (!idrOriginator.IsDBNull(originatorIdOrdinal)) oOriginator.OriginatorId = idrOriginator.GetInt32(originatorIdOrdinal);
            //            oOriginator.UserName = userName;
            //            if (!idrOriginator.IsDBNull(deptIdOrdinal)) oOriginator.DefaultDepartmentId = idrOriginator.GetString(deptIdOrdinal);
            //            if (!idrOriginator.IsDBNull(clientTypeOrdinal)) oOriginator.ClientType = idrOriginator.GetString(clientTypeOrdinal);
            //            if (!idrOriginator.IsDBNull(repCodeOrdinal)) oOriginator.RepCode = idrOriginator.GetString(repCodeOrdinal);
            //            if (!idrOriginator.IsDBNull(repTypeOrdinal)) oOriginator.RepType = idrOriginator.GetString(repTypeOrdinal);
            //            if (!idrOriginator.IsDBNull(intAddOrdinal)) oOriginator.Email = idrOriginator.GetString(intAddOrdinal);
            //            if (!idrOriginator.IsDBNull(routeIdOrdinal)) oOriginator.RouteId = idrOriginator.GetInt32(routeIdOrdinal);
            //            if (!idrOriginator.IsDBNull(routeNameOrdinal)) oOriginator.RouteName = idrOriginator.GetString(routeNameOrdinal);
            //            if (!idrOriginator.IsDBNull(deptStringOrdinal)) oOriginator.DeptString = idrOriginator.GetString(deptStringOrdinal);
            //            if (!idrOriginator.IsDBNull(accessTokenOrdinal)) oOriginator.AccessToken = idrOriginator.GetString(accessTokenOrdinal);
            //            if (!idrOriginator.IsDBNull(distributornameOrdinal)) oOriginator.distributorname = idrOriginator.GetString(distributornameOrdinal);
            //            if (!idrOriginator.IsDBNull(distributoraddressOrdinal)) oOriginator.distributoraddress = idrOriginator.GetString(distributoraddressOrdinal);
            //            if (!idrOriginator.IsDBNull(distributortelOrdinal)) oOriginator.distributortel = idrOriginator.GetString(distributortelOrdinal);
            //            if (!idrOriginator.IsDBNull(invoiceNoOrdinal)) oOriginator.InvoiceCount = Convert.ToInt32(idrOriginator.GetString(invoiceNoOrdinal));
            //        }
            //    }
            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (idrOriginator != null)
            //        idrOriginator.Close();
            //}

            //return oOriginator;

            DataTable objDS = new DataTable();
            OriginatorEntity oOriginator = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetLoginDetails";

                    objCommand.Parameters.AddWithValue("@originator", userName);
                    objCommand.Parameters.AddWithValue("@password", password);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            oOriginator = new OriginatorEntity();
                            oOriginator.Name = objDS.Rows[0]["name"].ToString();
                            oOriginator.OriginatorId = Convert.ToInt32(objDS.Rows[0]["originator_id"]);
                            oOriginator.UserName = userName;
                            oOriginator.DefaultDepartmentId = objDS.Rows[0]["default_dept_id"].ToString();
                            oOriginator.ClientType = objDS.Rows[0]["client_type"].ToString();
                            oOriginator.RepCode = objDS.Rows[0]["rep_code"].ToString();
                            oOriginator.RepType = objDS.Rows[0]["rep_type"].ToString();
                            oOriginator.Email = objDS.Rows[0]["internet_add"].ToString();
                            oOriginator.RouteId = Convert.ToInt32(objDS.Rows[0]["route_id"]);
                            oOriginator.RouteName = objDS.Rows[0]["route_name"].ToString();
                            oOriginator.DeptString = objDS.Rows[0]["dept_string"].ToString();
                            oOriginator.AccessToken = objDS.Rows[0]["access_token"].ToString();
                            oOriginator.distributorname = objDS.Rows[0]["distributorname"].ToString();
                            oOriginator.distributoraddress = objDS.Rows[0]["distributoraddress"].ToString();
                            oOriginator.distributortel = objDS.Rows[0]["distributortel"].ToString();
                            oOriginator.InvoiceCount = Convert.ToInt32(objDS.Rows[0]["invoice_no"]);
                            oOriginator.TFAPin = objDS.Rows[0]["tfa_pin"].ToString();
                            oOriginator.IsTFAUser = Convert.ToBoolean(objDS.Rows[0]["tfa_user"]);
                            oOriginator.TFAIsAuthenticated = Convert.ToBoolean(objDS.Rows[0]["tfa_is_authenticated"]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return oOriginator;
        }

        public bool IsNotEmeiUser(string userName)
        {
            bool ret = false;

            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "IsNotEmeiUser";

                    objCommand.Parameters.AddWithValue("@originator", userName);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        ret = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = false;
            }

            return ret;
        }

        public bool IsEmeiExsistThisUser(string userName, string emei)
        {
            bool ret = false;

            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "IsEmeiExsistThisUser";

                    objCommand.Parameters.AddWithValue("@originator", userName);
                    objCommand.Parameters.AddWithValue("@emei", emei);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        ret = true;
                    }
                }
            }
            catch (Exception ex)
            {
                ret = false;
            }

            return ret;
        }

        public OriginatorEntity GetLoginDetailsWithEMEI(string userName, string password, string emei)
        {
            DataTable objDS = new DataTable();
            OriginatorEntity oOriginator = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetLoginDetailsWithEMEI";

                    objCommand.Parameters.AddWithValue("@originator", userName);
                    objCommand.Parameters.AddWithValue("@password", password);
                    objCommand.Parameters.AddWithValue("@emei", emei);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            oOriginator = new OriginatorEntity();
                            oOriginator.Name = objDS.Rows[0]["name"].ToString();
                            oOriginator.OriginatorId = Convert.ToInt32(objDS.Rows[0]["originator_id"]);
                            oOriginator.UserName = userName;
                            oOriginator.DefaultDepartmentId = objDS.Rows[0]["default_dept_id"].ToString();
                            oOriginator.ClientType = objDS.Rows[0]["client_type"].ToString();
                            oOriginator.RepCode = objDS.Rows[0]["rep_code"].ToString();
                            oOriginator.RepType = objDS.Rows[0]["rep_type"].ToString();
                            oOriginator.Email = objDS.Rows[0]["internet_add"].ToString();
                            oOriginator.DeptString = objDS.Rows[0]["dept_string"].ToString();
                            oOriginator.AccessToken = objDS.Rows[0]["access_token"].ToString();
                            //oOriginator.InvoiceCount = Convert.ToInt32(objDS.Rows[0]["invoice_no"]);

                            oOriginator.RouteId = Convert.ToInt32(objDS.Rows[0]["route_id"]);
                            oOriginator.RouteName = objDS.Rows[0]["route_name"].ToString();
                            //oOriginator.TerritoryId = Convert.ToInt32(objDS.Rows[0]["territory_id"]);
                            //oOriginator.TerritoryName = objDS.Rows[0]["territory_name"].ToString();
                            //oOriginator.AreaId = Convert.ToInt32(objDS.Rows[0]["area_id"]);
                            //oOriginator.AreaName = objDS.Rows[0]["area_name"].ToString();
                            //oOriginator.RegionId = Convert.ToInt32(objDS.Rows[0]["region_id"]);
                            //oOriginator.RegionName = objDS.Rows[0]["region_name"].ToString();

                            oOriginator.distributorname = objDS.Rows[0]["distributorname"].ToString();
                            oOriginator.distributoraddress = objDS.Rows[0]["distributoraddress"].ToString();
                            oOriginator.distributortel = objDS.Rows[0]["distributortel"].ToString();
                            //oOriginator.AsmId = Convert.ToInt32(objDS.Rows[0]["asm_id"]);
                            //oOriginator.Asmname = objDS.Rows[0]["asm_name"].ToString();
                            //oOriginator.RsmId = Convert.ToInt32(objDS.Rows[0]["rsm_id"]);
                            //oOriginator.Rsmname = objDS.Rows[0]["rsm_name"].ToString();
                            oOriginator.IsTFAUser = Convert.ToBoolean(objDS.Rows[0]["tfa_user"]);
                            oOriginator.OtpMobile = objDS.Rows[0]["otp_mobile"].ToString();
                            try
                            {
                                oOriginator.distributorId = Convert.ToInt32(objDS.Rows[0]["distributorId"]);
                            }
                            catch { }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return oOriginator;
        }

        public OriginatorEntity GetOriginator(string sOriginator)
        {
            DbDataReader drOriginator = null;

            OriginatorEntity originator = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@originator", sOriginator, DbType.String);

                drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetOriginator"], paramCollection);

                if (drOriginator != null && drOriginator.HasRows)
                {
                    int nameOrdinal = drOriginator.GetOrdinal("name");
                    int emailOrdinal = drOriginator.GetOrdinal("internet_add");
                    int clientTypeOrdinal = drOriginator.GetOrdinal("client_type");
                    int idOrdinal = drOriginator.GetOrdinal("originator_id");

                    if (drOriginator.Read())
                    {
                        originator = OriginatorEntity.CreateObject();
                        if (!drOriginator.IsDBNull(nameOrdinal)) originator.Name = drOriginator.GetString(nameOrdinal);
                        if (!drOriginator.IsDBNull(emailOrdinal)) originator.Email = drOriginator.GetString(emailOrdinal);
                        if (!drOriginator.IsDBNull(clientTypeOrdinal)) originator.ClientType = drOriginator.GetString(clientTypeOrdinal);
                        if (!drOriginator.IsDBNull(idOrdinal)) originator.OriginatorId = drOriginator.GetInt32(idOrdinal);
                    }
                }
                return originator;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOriginator != null)
                    drOriginator.Close();
            }
        }

        public string GetRepCode(string originator)
        {
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@originator", originator, DbType.String);

                return System.Convert.ToString(this.DataAcessService.GetOneValue(originatorSql["GetLoggedInReps"], paramCollection));
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorEntity> GetBDMList()
        {
            DbDataReader drOriginator = null;

            List<OriginatorEntity> BDMList = new List<OriginatorEntity>();
            OriginatorEntity originator = null;
            try
            {
                drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetBDMs"]);

                if (drOriginator != null && drOriginator.HasRows)
                {
                    int nameOrdinal = drOriginator.GetOrdinal("name");
                    int emailOrdinal = drOriginator.GetOrdinal("internet_add");
                    int clientTypeOrdinal = drOriginator.GetOrdinal("client_type");
                    int idOrdinal = drOriginator.GetOrdinal("originator_id");
                    int originatorOrdinal = drOriginator.GetOrdinal("originator");

                    while (drOriginator.Read())
                    {
                        originator = OriginatorEntity.CreateObject();
                        if (!drOriginator.IsDBNull(nameOrdinal)) originator.Name = drOriginator.GetString(nameOrdinal);
                        if (!drOriginator.IsDBNull(emailOrdinal)) originator.Email = drOriginator.GetString(emailOrdinal);
                        if (!drOriginator.IsDBNull(clientTypeOrdinal)) originator.ClientType = drOriginator.GetString(clientTypeOrdinal);
                        if (!drOriginator.IsDBNull(idOrdinal)) originator.OriginatorId = drOriginator.GetInt32(idOrdinal);
                        if (!drOriginator.IsDBNull(originatorOrdinal)) originator.UserName = drOriginator.GetString(originatorOrdinal);
                        BDMList.Add(originator);
                    }
                }
                return BDMList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOriginator != null)
                    drOriginator.Close();
            }
        }

        public List<KeyValuePair<string, string>> GetChildRepsList(string childOriginators)
        {
            DbDataReader drOriginator = null;
            string sWhereCls = "";
            List<KeyValuePair<string, string>> repsList = new List<KeyValuePair<string, string>>();

            try
            {
                sWhereCls = childOriginators;
                sWhereCls = sWhereCls.Replace("originator", "r.originator");

                drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetChildReps"].Format(sWhereCls));

                if (drOriginator != null && drOriginator.HasRows)
                {
                    int nameOrdinal = drOriginator.GetOrdinal("name");
                    int codeOrdinal = drOriginator.GetOrdinal("rep_code");

                    while (drOriginator.Read())
                    {
                        string name = !drOriginator.IsDBNull(nameOrdinal) ? drOriginator.GetString(nameOrdinal) : string.Empty;

                        string code = !drOriginator.IsDBNull(codeOrdinal) ? drOriginator.GetString(codeOrdinal) : string.Empty;

                        KeyValuePair<string, string> rep = new KeyValuePair<string, string>(code, name);
                        repsList.Add(rep);
                    }
                }
                return repsList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOriginator != null)
                    drOriginator.Close();
            }
        }

        public List<OriginatorEntity> GetAllRepCodes(string sOriginator)
        {
            DbDataReader idrRep = null;

            List<OriginatorEntity> RepList = null;
            OriginatorEntity originator = OriginatorEntity.CreateObject();

            try
            {
                RepList = new List<OriginatorEntity>();

                // Get the Rep Codes of this Originator
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@originator", sOriginator, DbType.String);

                idrRep = this.DataAcessService.ExecuteQuery(originatorSql["GetAllRepCodes"], paramCollection);

                if (idrRep != null && idrRep.HasRows)
                {
                    while (idrRep.Read())
                    {
                        originator = OriginatorEntity.CreateObject();

                        if (!idrRep.IsDBNull(idrRep.GetOrdinal("rep_code")))
                            originator.RepCode = idrRep.GetString(idrRep.GetOrdinal("rep_code"));

                        RepList.Add(originator);
                    }
                }

                return RepList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrRep != null)
                    idrRep.Close();
            }
        }

        public List<StateEntity> GetDistinctRepStates()
        {
            DbDataReader idrState = null;

            List<StateEntity> StateList = null;
            StateEntity state = StateEntity.CreateObject();
            try
            {
                idrState = this.DataAcessService.ExecuteQuery(originatorSql["GetDistinctRepStates"]);

                if (idrState != null && idrState.HasRows)
                {
                    StateList = new List<StateEntity>();

                    int stateOrdinal = idrState.GetOrdinal("state");

                    while (idrState.Read())
                    {
                        state = StateEntity.CreateObject();
                        if (!idrState.IsDBNull(stateOrdinal)) state.StateName = idrState.GetString(stateOrdinal).Trim();
                        StateList.Add(state);
                    }
                }
                return StateList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrState != null)
                    idrState.Close();
            }
        }

        public List<OriginatorEntity> GetRepGroupOriginators(string originator)
        {
            DbDataReader idrRep = null;
            List<OriginatorEntity> listOriginator;
            OriginatorEntity originatorEntity;

            try
            {
                listOriginator = new List<OriginatorEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@originator", originator, DbType.String);
                idrRep = this.DataAcessService.ExecuteQuery(originatorSql["GetRepGroupOriginators"], paramCollection);

                if (idrRep != null && idrRep.HasRows)
                {
                    while (idrRep.Read())
                    {
                        originatorEntity = OriginatorEntity.CreateObject();
                        if (!idrRep.IsDBNull(idrRep.GetOrdinal("originator")))
                            originatorEntity.UserName = idrRep.GetString(idrRep.GetOrdinal("originator"));

                        listOriginator.Add(originatorEntity);
                    }
                }

                return listOriginator;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrRep != null)
                    idrRep.Close();
            }
        }

        public List<OriginatorEntity> GetCRMReps()
        {
            List<OriginatorEntity> repList = null;
            DbDataReader drOriginator = null;

            try
            {
                drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetCRMReps"]);

                if (drOriginator.HasRows)
                {
                    OriginatorEntity rep = null;
                    repList = new List<OriginatorEntity>();

                    int originatorOrdinal = drOriginator.GetOrdinal("originator");
                    int nameOrdinal = drOriginator.GetOrdinal("name");

                    while (drOriginator.Read())
                    {
                        rep = OriginatorEntity.CreateObject();
                        if (!drOriginator.IsDBNull(originatorOrdinal)) rep.UserName = drOriginator.GetString(originatorOrdinal).Trim();
                        if (!drOriginator.IsDBNull(nameOrdinal)) rep.Name = drOriginator.GetString(nameOrdinal);
                        repList.Add(rep);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (drOriginator != null && (!drOriginator.IsClosed))
                    drOriginator.Close();
            }

            return repList;
        }

        public List<OriginatorEntity> GetChildOriginatorsList(string sOriginator, bool managerMode = false)
        {
            List<OriginatorEntity> lOriginators = new List<OriginatorEntity>();

            try
            {
                lOriginators.Clear();

                if (sOriginator != "")
                {
                    if (managerMode)
                    {

                    }
                        lOriginators = GetChildOriginatorList(sOriginator, true);
                }

                return lOriginators;
            }
            catch
            {
                throw;
            }
        }

        public List<OriginatorEntity> GetRepForKeyAccounts(string distributor)
        {
            DbDataReader idrRep = null;
            List<OriginatorEntity> listOriginator;
            OriginatorEntity originatorEntity;

            try
            {
                listOriginator = new List<OriginatorEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Distributor", distributor, DbType.String);
                idrRep = this.DataAcessService.ExecuteQuery(originatorSql["GetRepForKeyAccounts"], paramCollection);

                if (idrRep != null && idrRep.HasRows)
                {
                    int repcodeOrdinal = idrRep.GetOrdinal("rep_code");
                    int nameOrdinal = idrRep.GetOrdinal("name");
                    //int originatorOrdinal = idrRep.GetOrdinal("originator");
                    //int originatornameOrdinal = idrRep.GetOrdinal("originatorname");

                    while (idrRep.Read())
                    {
                        originatorEntity = OriginatorEntity.CreateObject();

                        if (!idrRep.IsDBNull(repcodeOrdinal)) originatorEntity.RepCode = idrRep.GetString(repcodeOrdinal);
                        if (!idrRep.IsDBNull(nameOrdinal)) originatorEntity.Name = idrRep.GetString(nameOrdinal);
                        //if (!idrRep.IsDBNull(originatorOrdinal)) originatorEntity.UserName = idrRep.GetString(originatorOrdinal);
                        //if (!idrRep.IsDBNull(originatornameOrdinal)) originatorEntity.FullName = idrRep.GetString(originatornameOrdinal);

                        listOriginator.Add(originatorEntity);
                    }
                }

                return listOriginator;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrRep != null)
                    idrRep.Close();
            }
        }

        public string GetPayMethod(string sRepCode)
        {
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@RepCode", sRepCode, DbType.String);
                return Convert.ToString(DataAcessService.GetOneValue(originatorSql["GetPayMethodByRepCode"], paramCollection));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string GetUserName(string sRepCode)
        {
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@RepCode", sRepCode, DbType.String);
                return Convert.ToString(DataAcessService.GetOneValue(originatorSql["GetUsernameByRepCode"], paramCollection));
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OriginatorEntity> GetOriginatorsByCatergory(ArgsEntity args)
        {
            DataTable objDS = new DataTable();
            OriginatorEntity oOriginator = null;
            List<OriginatorEntity> originatorlist = new List<OriginatorEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetOriginatorsByCatergory";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AdditionalParams", args.AdditionalParams);
                    //if (args.ChildOriginators != null)
                    //{
                    //    objCommand.Parameters.AddWithValue("@ChildOriginators", args.ChildOriginators.Replace("originator", "ori.parent_originator"));
                    //}
                    //else
                    //{
                    //    objCommand.Parameters.AddWithValue("@ChildOriginators", "");
                    //}
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@Originator", args.Originator);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            oOriginator = new OriginatorEntity();
                            oOriginator.Name = item["name"].ToString();
                            oOriginator.OriginatorId = Convert.ToInt32(item["originator_id"]);
                            oOriginator.UserName = item["originator"].ToString();
                            oOriginator.TotalCount = Convert.ToInt32(item["total_count"]);
                            originatorlist.Add(oOriginator);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }


            return originatorlist;
        }
        
        public List<OriginatorEntity> GetDistributersByASMOriginator(ArgsEntity args)
        {
            DataTable objDS = new DataTable();
            OriginatorEntity oOriginator = null;
            List<OriginatorEntity> originatorlist = new List<OriginatorEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetDistributersByASMOriginator";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AdditionalParams", args.AdditionalParams);
                    //if (args.ChildOriginators != null)
                    //{
                    //    objCommand.Parameters.AddWithValue("@ChildOriginators", args.ChildOriginators.Replace("originator", "ori.parent_originator"));
                    //}
                    //else
                    //{
                    //    objCommand.Parameters.AddWithValue("@ChildOriginators", "");
                    //}
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@Originator", args.Originator);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            oOriginator = new OriginatorEntity();
                            oOriginator.Name = item["name"].ToString();
                            oOriginator.OriginatorId = Convert.ToInt32(item["originator_id"]);
                            oOriginator.UserName = item["originator"].ToString();
                            oOriginator.TotalCount = Convert.ToInt32(item["total_count"]);
                            originatorlist.Add(oOriginator);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }


            return originatorlist;
        }
        
        public List<OriginatorEntity> GetAllSRByOriginator(ArgsEntity args)
        {
            DataTable objDS = new DataTable();
            OriginatorEntity oOriginator = null;
            List<OriginatorEntity> originatorlist = new List<OriginatorEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllSRByOriginator";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AdditionalParams", args.AdditionalParams);
                    //if (args.ChildOriginators != null)
                    //{
                    //    objCommand.Parameters.AddWithValue("@ChildOriginators", args.ChildOriginators.Replace("originator", "ori.parent_originator"));
                    //}
                    //else
                    //{
                    //    objCommand.Parameters.AddWithValue("@ChildOriginators", "");
                    //}
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@Originator", args.Originator);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            oOriginator = new OriginatorEntity();
                            oOriginator.Name = item["name"].ToString();
                            oOriginator.OriginatorId = Convert.ToInt32(item["originator_id"]);
                            oOriginator.UserName = item["originator"].ToString();
                            oOriginator.TotalCount = Convert.ToInt32(item["total_count"]);
                            originatorlist.Add(oOriginator);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }


            return originatorlist;
        }

        public List<OriginatorEntity> GetAllDistributorList(ArgsEntity args)
        {
            DbDataReader drOriginator = null;

            try
            {
                List<OriginatorEntity> originatorlist = new List<OriginatorEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", args.AdditionalParams, DbType.String);
                if (args.ChildOriginators != null)
                {
                    paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "ori.parent_originator"), DbType.String);
                }
                else
                {
                    paramCollection.Add("@ChildOriginators", "", DbType.String);
                }
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@Originator", args.Originator, DbType.String);

                drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetAllDistributorList"], paramCollection);

                if (drOriginator != null && drOriginator.HasRows)
                {
                    int originatorIdOrdinal = drOriginator.GetOrdinal("originator_id");
                    int originatorOrdinal = drOriginator.GetOrdinal("originator");
                    int nameOrdinal = drOriginator.GetOrdinal("name");
                    int totalCountOrdinal = drOriginator.GetOrdinal("total_count");

                    while (drOriginator.Read())
                    {
                        OriginatorEntity originatorEntity = CreateObject();

                        if (!drOriginator.IsDBNull(originatorIdOrdinal)) originatorEntity.OriginatorId = drOriginator.GetInt32(originatorIdOrdinal);
                        if (!drOriginator.IsDBNull(originatorOrdinal)) originatorEntity.UserName = drOriginator.GetString(originatorOrdinal);
                        if (!drOriginator.IsDBNull(nameOrdinal)) originatorEntity.Name = drOriginator.GetString(nameOrdinal);
                        if (!drOriginator.IsDBNull(totalCountOrdinal)) originatorEntity.TotalCount = drOriginator.GetInt32(totalCountOrdinal);

                        originatorlist.Add(originatorEntity);
                    }
                }

                return originatorlist;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (drOriginator != null)
                    drOriginator.Close();
            }
        }

        public List<OriginatorEntity> GetAllOriginatorsByCatergory(ArgsEntity args)
        {
            DbDataReader drOriginator = null;

            try
            {
                List<OriginatorEntity> originatorlist = new List<OriginatorEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", args.AdditionalParams, DbType.String);
                if (args.ChildOriginators != null)
                {
                    paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "ori.parent_originator"), DbType.String);
                }
                else
                {
                    paramCollection.Add("@ChildOriginators", "", DbType.String);
                }

                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@Originator", args.Originator, DbType.String);

                drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetAllOriginatorsByCatergory"], paramCollection);

                if (drOriginator != null && drOriginator.HasRows)
                {
                    int originatorIdOrdinal = drOriginator.GetOrdinal("originator_id");
                    int originatorOrdinal = drOriginator.GetOrdinal("originator");
                    int nameOrdinal = drOriginator.GetOrdinal("name");
                    int totalCountOrdinal = drOriginator.GetOrdinal("total_count");

                    while (drOriginator.Read())
                    {
                        OriginatorEntity originatorEntity = CreateObject();

                        if (!drOriginator.IsDBNull(originatorIdOrdinal)) originatorEntity.OriginatorId = drOriginator.GetInt32(originatorIdOrdinal);
                        if (!drOriginator.IsDBNull(originatorOrdinal)) originatorEntity.UserName = drOriginator.GetString(originatorOrdinal);
                        if (!drOriginator.IsDBNull(nameOrdinal)) originatorEntity.Name = drOriginator.GetString(nameOrdinal);
                        if (!drOriginator.IsDBNull(totalCountOrdinal)) originatorEntity.TotalCount = drOriginator.GetInt32(totalCountOrdinal);

                        originatorlist.Add(originatorEntity);
                    }
                }

                return originatorlist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOriginator != null)
                    drOriginator.Close();
            }
        }

        public List<OriginatorEntity> GetASEsByDateForReport(ArgsEntity args, DateTime startDate, DateTime endDate)
        {
            DbDataReader drOriginator = null;

            try
            {
                List<OriginatorEntity> originatorlist = new List<OriginatorEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@StartDate", startDate, DbType.DateTime);
                paramCollection.Add("@EndDate", endDate, DbType.DateTime);

                drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetASEsByDateForReport"], paramCollection);

                if (drOriginator != null && drOriginator.HasRows)
                {
                    int originatorIdOrdinal = drOriginator.GetOrdinal("originator_id");
                    int originatorOrdinal = drOriginator.GetOrdinal("originator");
                    int nameOrdinal = drOriginator.GetOrdinal("name");
                    int totalCountOrdinal = drOriginator.GetOrdinal("total_count");

                    while (drOriginator.Read())
                    {
                        OriginatorEntity originatorEntity = CreateObject();

                        if (!drOriginator.IsDBNull(originatorIdOrdinal)) originatorEntity.OriginatorId = drOriginator.GetInt32(originatorIdOrdinal);
                        if (!drOriginator.IsDBNull(originatorOrdinal)) originatorEntity.UserName = drOriginator.GetString(originatorOrdinal);
                        if (!drOriginator.IsDBNull(nameOrdinal)) originatorEntity.Name = drOriginator.GetString(nameOrdinal);
                        if (!drOriginator.IsDBNull(totalCountOrdinal)) originatorEntity.TotalCount = drOriginator.GetInt32(totalCountOrdinal);

                        originatorlist.Add(originatorEntity);
                    }
                }

                return originatorlist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOriginator != null)
                    drOriginator.Close();
            }
        }

        public List<OriginatorEntity> GetDistributorsByCatergoryWithNameAndCode(ArgsEntity args)
        {
            DbDataReader drOriginator = null;

            try
            {
                List<OriginatorEntity> originatorlist = new List<OriginatorEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", args.AdditionalParams, DbType.String);
                if (args.ChildOriginators != null)
                {
                    paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "ori.parent_originator"), DbType.String);
                }
                else
                {
                    paramCollection.Add("@ChildOriginators", "", DbType.String);
                }
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@Originator", args.Originator, DbType.String);

                drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetOriginatorsByCatergory"], paramCollection);

                if (drOriginator != null && drOriginator.HasRows)
                {
                    int originatorIdOrdinal = drOriginator.GetOrdinal("originator_id");
                    int originatorOrdinal = drOriginator.GetOrdinal("originator");
                    int nameOrdinal = drOriginator.GetOrdinal("name");
                    int totalCountOrdinal = drOriginator.GetOrdinal("total_count");

                    while (drOriginator.Read())
                    {
                        OriginatorEntity originatorEntity = CreateObject();

                        if (!drOriginator.IsDBNull(originatorIdOrdinal)) originatorEntity.OriginatorId = drOriginator.GetInt32(originatorIdOrdinal);
                        if (!drOriginator.IsDBNull(originatorOrdinal)) originatorEntity.UserName = drOriginator.GetString(originatorOrdinal);
                        if (!drOriginator.IsDBNull(nameOrdinal)) originatorEntity.Name = drOriginator.GetString(nameOrdinal) + " / " + drOriginator.GetString(originatorOrdinal);
                        if (!drOriginator.IsDBNull(totalCountOrdinal)) originatorEntity.TotalCount = drOriginator.GetInt32(totalCountOrdinal);

                        originatorlist.Add(originatorEntity);
                    }
                }

                return originatorlist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOriginator != null)
                    drOriginator.Close();
            }
        }

        public List<OriginatorEntity> GetOriginatorsByCatergory_For_Route_Assign(ArgsEntity args)
        {
            DataTable objDS = new DataTable();
            OriginatorEntity oOriginator = null;
            List<OriginatorEntity> originatorlist = new List<OriginatorEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetOriginatorsByCatergory_For_Route_Assign";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AdditionalParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OriginatorType",  args.OriginatorType);
                    objCommand.Parameters.AddWithValue("@Originator",  args.Originator);
                    //if (args.ChildOriginators != null)
                    //{
                    //    objCommand.Parameters.AddWithValue("@ChildOriginators", args.ChildOriginators.Replace("originator", "ori.parent_originator"));
                    //}
                    //else
                    //{
                    //    objCommand.Parameters.AddWithValue("@ChildOriginators", "");
                    //}
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            oOriginator = new OriginatorEntity();
                            oOriginator.Name = item["name"].ToString();
                            oOriginator.OriginatorId = Convert.ToInt32(item["originator_id"]);
                            oOriginator.UserName = item["originator"].ToString();
                            oOriginator.TotalCount = Convert.ToInt32(item["total_count"]);
                            oOriginator.RepCode = item["rep_code"].ToString();
                            originatorlist.Add(oOriginator);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }


            return originatorlist;
        }

        public List<OriginatorEntity> GetRepsByOriginator(string rep, string username)
        {
            DbDataReader drOriginator = null;
            List<OriginatorEntity> replist = new List<OriginatorEntity>();

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", username, DbType.String);
                paramCollection.Add("@RepName", rep, DbType.String);

                drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetRepsByOriginator"], paramCollection);

                if (drOriginator != null && drOriginator.HasRows)
                {
                    //   int originatorIdOrdinal = drOriginator.GetOrdinal("originator_id");
                    int originatorOrdinal = drOriginator.GetOrdinal("originator");
                    int nameOrdinal = drOriginator.GetOrdinal("name");
                    int repCodeOrdinal = drOriginator.GetOrdinal("prefix_code");


                    while (drOriginator.Read())
                    {
                        OriginatorEntity originatorEntity = CreateObject();

                        // if (!drOriginator.IsDBNull(originatorIdOrdinal)) originatorEntity.OriginatorId = drOriginator.GetInt32(originatorIdOrdinal);
                        if (!drOriginator.IsDBNull(originatorOrdinal)) originatorEntity.Originator = drOriginator.GetString(originatorOrdinal);
                        if (!drOriginator.IsDBNull(nameOrdinal)) originatorEntity.Name = drOriginator.GetString(nameOrdinal);
                        if (!drOriginator.IsDBNull(repCodeOrdinal)) originatorEntity.RepCode = drOriginator.GetString(repCodeOrdinal);

                        replist.Add(originatorEntity);
                    }
                }

                return replist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOriginator != null)
                    drOriginator.Close();
            }
        }

        public List<OriginatorEntity> GetRepsByASEOriginator(string ASE)
        {
            DbDataReader drOriginator = null;
            List<OriginatorEntity> replist = new List<OriginatorEntity>();

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@ASEOriginator", ASE, DbType.String);

                drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetRepsByASEOriginator"], paramCollection);

                if (drOriginator != null && drOriginator.HasRows)
                {
                    //   int originatorIdOrdinal = drOriginator.GetOrdinal("originator_id");
                    int originatorOrdinal = drOriginator.GetOrdinal("originator");
                    int nameOrdinal = drOriginator.GetOrdinal("name");
                    int repCodeOrdinal = drOriginator.GetOrdinal("prefix_code");
                    int access_tokenOrdinal = drOriginator.GetOrdinal("access_token");

                    while (drOriginator.Read())
                    {
                        OriginatorEntity originatorEntity = CreateObject();

                        // if (!drOriginator.IsDBNull(originatorIdOrdinal)) originatorEntity.OriginatorId = drOriginator.GetInt32(originatorIdOrdinal);
                        if (!drOriginator.IsDBNull(originatorOrdinal)) originatorEntity.Originator = drOriginator.GetString(originatorOrdinal);
                        if (!drOriginator.IsDBNull(nameOrdinal)) originatorEntity.Name = drOriginator.GetString(nameOrdinal);
                        if (!drOriginator.IsDBNull(repCodeOrdinal)) originatorEntity.RepCode = drOriginator.GetString(repCodeOrdinal);
                        if (!drOriginator.IsDBNull(access_tokenOrdinal)) originatorEntity.AccessToken = drOriginator.GetString(access_tokenOrdinal);

                        replist.Add(originatorEntity);
                    }
                }

                return replist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOriginator != null)
                    drOriginator.Close();
            }
        }

        public List<OriginatorEntity> GetAllOriginators(string originator)
        {
            DbDataReader drOriginator = null;
            List<OriginatorEntity> originatorlist = new List<OriginatorEntity>();

            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", originator, DbType.String);

                drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetAllOriginators"], paramCollection);

                if (drOriginator != null && drOriginator.HasRows)
                {
                    int originatorIdOrdinal = drOriginator.GetOrdinal("originator_id");
                    int originatorOrdinal = drOriginator.GetOrdinal("originator");
                    int nameOrdinal = drOriginator.GetOrdinal("name");

                    while (drOriginator.Read())
                    {
                        OriginatorEntity originatorEntity = CreateObject();

                        if (!drOriginator.IsDBNull(originatorIdOrdinal)) originatorEntity.OriginatorId = drOriginator.GetInt32(originatorIdOrdinal);
                        if (!drOriginator.IsDBNull(originatorOrdinal)) originatorEntity.Originator = drOriginator.GetString(originatorOrdinal);
                        if (!drOriginator.IsDBNull(nameOrdinal)) originatorEntity.Name = drOriginator.GetString(nameOrdinal);

                        originatorlist.Add(originatorEntity);
                    }
                }

                return originatorlist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOriginator != null)
                    drOriginator.Close();
            }
        }

        public List<OriginatorEntity> GetDistributorsByLikeName(string name)
        {
            DbDataReader drOriginator = null;
            List<OriginatorEntity> originatorlist = new List<OriginatorEntity>();

            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Name", name, DbType.String);

                drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetDistributorsByLikeName"], paramCollection);

                if (drOriginator != null && drOriginator.HasRows)
                {
                    int originatorIdOrdinal = drOriginator.GetOrdinal("id");
                    int originatorOrdinal = drOriginator.GetOrdinal("originator");
                    int nameOrdinal = drOriginator.GetOrdinal("name");

                    while (drOriginator.Read())
                    {
                        OriginatorEntity originatorEntity = CreateObject();

                        if (!drOriginator.IsDBNull(originatorIdOrdinal)) originatorEntity.OriginatorId = drOriginator.GetInt32(originatorIdOrdinal);
                        if (!drOriginator.IsDBNull(originatorOrdinal)) originatorEntity.UserName = drOriginator.GetString(originatorOrdinal);
                        if (!drOriginator.IsDBNull(nameOrdinal)) originatorEntity.Name = drOriginator.GetString(nameOrdinal);

                        originatorlist.Add(originatorEntity);
                    }
                }

                return originatorlist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOriginator != null)
                    drOriginator.Close();
            }
        }

        //public List<RepEntity> GetRepTargetsByDistributor(string username)
        //{
        //    DbDataReader drOriginator = null;
        //    List<RepEntity> replist = new List<RepEntity>();

        //    try
        //    {

        //        DbInputParameterCollection paramCollection = new DbInputParameterCollection();
        //        paramCollection.Add("@Distributor", username, DbType.String);

        //        drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetRepTargetsByDistributor"], paramCollection);

        //        if (drOriginator != null && drOriginator.HasRows)
        //        {
        //            int repCodeOrdinal = drOriginator.GetOrdinal("rep_code");
        //            int nameOrdinal = drOriginator.GetOrdinal("name");
        //            int stickTargetOrdinal = drOriginator.GetOrdinal("stick_target");
        //            int monthOrdinal = drOriginator.GetOrdinal("month");
        //            int yearOrdinal = drOriginator.GetOrdinal("year");
        //            int monthtextOrdinal = drOriginator.GetOrdinal("monthtext");

        //            while (drOriginator.Read())
        //            {
        //                RepEntity repEntity = new RepEntity();

        //                if (!drOriginator.IsDBNull(repCodeOrdinal)) repEntity.RepCode = drOriginator.GetString(repCodeOrdinal);
        //                if (!drOriginator.IsDBNull(nameOrdinal)) repEntity.Name = drOriginator.GetString(nameOrdinal);
        //                if (!drOriginator.IsDBNull(stickTargetOrdinal)) repEntity.StickTarget = Convert.ToDouble(drOriginator.GetInt32(stickTargetOrdinal));
        //                if (!drOriginator.IsDBNull(monthOrdinal)) repEntity.TargetMonth = drOriginator.GetInt32(monthOrdinal);
        //                if (!drOriginator.IsDBNull(yearOrdinal)) repEntity.TargetYear = drOriginator.GetInt32(yearOrdinal);
        //                if (!drOriginator.IsDBNull(monthtextOrdinal)) repEntity.TargetMonthText = drOriginator.GetString(monthtextOrdinal);

        //                replist.Add(repEntity);
        //            }
        //        }

        //        return replist;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if (drOriginator != null)
        //            drOriginator.Close();
        //    }
        //}


        public List<TargetInfoEntity> GetRepMonthleyTargetsByDistributor(SalesInfoDetailSearchCriteriaEntity busSalesEnqSrc, ArgsEntity args)
        {
            DbDataReader drOriginator = null;
            List<TargetInfoEntity> replist = new List<TargetInfoEntity>();

            try
            {
                double displayOption = 1;
                if (busSalesEnqSrc.cDisplayOption == 'M')
                {
                    displayOption = 1000;
                }

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Distributor", args.Originator, DbType.String);
                paramCollection.Add("@DetailType", busSalesEnqSrc.DetailType, DbType.String);
                paramCollection.Add("@CustCode", busSalesEnqSrc.CustomerCode, DbType.String);
                paramCollection.Add("@BrandCode", busSalesEnqSrc.Brand, DbType.String);
                paramCollection.Add("@RepCode", busSalesEnqSrc.RepCode, DbType.String);
                paramCollection.Add("@ProductCode", "", DbType.String);

                paramCollection.Add("@RouteId", busSalesEnqSrc.Route, DbType.String);
                paramCollection.Add("@MarketId", busSalesEnqSrc.Market, DbType.String);

                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);




                drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetDRActualorDistributor"], paramCollection);

                if (drOriginator != null && drOriginator.HasRows)
                {
                    int repCodeOrdinal = drOriginator.GetOrdinal("code");
                    int nameOrdinal = drOriginator.GetOrdinal("name");


                    int m1Ordinal = drOriginator.GetOrdinal("M101");
                    int m2Ordinal = drOriginator.GetOrdinal("M102");
                    int m3Ordinal = drOriginator.GetOrdinal("M103");
                    int m4Ordinal = drOriginator.GetOrdinal("M104");
                    int m5Ordinal = drOriginator.GetOrdinal("M105");
                    int m6Ordinal = drOriginator.GetOrdinal("M106");
                    int m7Ordinal = drOriginator.GetOrdinal("M107");
                    int m8Ordinal = drOriginator.GetOrdinal("M108");
                    int m9Ordinal = drOriginator.GetOrdinal("M109");
                    int m10Ordinal = drOriginator.GetOrdinal("M110");
                    int m11Ordinal = drOriginator.GetOrdinal("M111");
                    int m12Ordinal = drOriginator.GetOrdinal("M112");

                    int tytdOrdinal = drOriginator.GetOrdinal("tytd_target");
                    int lytdOrdinal = drOriginator.GetOrdinal("lytd_target");
                    int year1Ordinal = drOriginator.GetOrdinal("year1_target");
                    int year2Ordinal = drOriginator.GetOrdinal("year2_target");

                    int actual1Ordinal = drOriginator.GetOrdinal("M1");
                    int actual2Ordinal = drOriginator.GetOrdinal("M2");
                    int actual3Ordinal = drOriginator.GetOrdinal("M3");
                    int actual4Ordinal = drOriginator.GetOrdinal("M4");
                    int actual5Ordinal = drOriginator.GetOrdinal("M5");
                    int actual6Ordinal = drOriginator.GetOrdinal("M6");
                    int actual7Ordinal = drOriginator.GetOrdinal("M7");
                    int actual8Ordinal = drOriginator.GetOrdinal("M8");
                    int actual9Ordinal = drOriginator.GetOrdinal("M9");
                    int actual10Ordinal = drOriginator.GetOrdinal("M10");
                    int actual11Ordinal = drOriginator.GetOrdinal("M11");
                    int actual12Ordinal = drOriginator.GetOrdinal("M12");

                    int actualtytdOrdinal = drOriginator.GetOrdinal("tytd_actual");
                    int actuallytdOrdinal = drOriginator.GetOrdinal("lytd_actual");
                    int actualyear1Ordinal = drOriginator.GetOrdinal("year1_actual");
                    int actualyear2Ordinal = drOriginator.GetOrdinal("year2_actual");

                    int m1totOrdinal = drOriginator.GetOrdinal("M101_target_tot");
                    int m2totOrdinal = drOriginator.GetOrdinal("M102_target_tot");
                    int m3totOrdinal = drOriginator.GetOrdinal("M103_target_tot");
                    int m4totOrdinal = drOriginator.GetOrdinal("M104_target_tot");
                    int m5totOrdinal = drOriginator.GetOrdinal("M105_target_tot");
                    int m6totOrdinal = drOriginator.GetOrdinal("M106_target_tot");
                    int m7totOrdinal = drOriginator.GetOrdinal("M107_target_tot");
                    int m8totOrdinal = drOriginator.GetOrdinal("M108_target_tot");
                    int m9totOrdinal = drOriginator.GetOrdinal("M109_target_tot");
                    int m10totOrdinal = drOriginator.GetOrdinal("M110_target_tot");
                    int m11totOrdinal = drOriginator.GetOrdinal("M111_target_tot");
                    int m12totOrdinal = drOriginator.GetOrdinal("M112_target_tot");

                    int tytdTotOrdinal = drOriginator.GetOrdinal("tytd_target_tot");
                    int lytdTotOrdinal = drOriginator.GetOrdinal("lytd_target_tot");
                    int year1TotOrdinal = drOriginator.GetOrdinal("year1_target_tot");
                    int year2TotOrdinal = drOriginator.GetOrdinal("year2_target_tot");

                    int actual1TotOrdinal = drOriginator.GetOrdinal("m1_actual_tot");
                    int actual2TotOrdinal = drOriginator.GetOrdinal("m2_actual_tot");
                    int actual3TotOrdinal = drOriginator.GetOrdinal("m3_actual_tot");
                    int actual4TotOrdinal = drOriginator.GetOrdinal("m4_actual_tot");
                    int actual5TotOrdinal = drOriginator.GetOrdinal("m5_actual_tot");
                    int actual6TotOrdinal = drOriginator.GetOrdinal("m6_actual_tot");
                    int actual7TotOrdinal = drOriginator.GetOrdinal("m7_actual_tot");
                    int actual8TotOrdinal = drOriginator.GetOrdinal("m8_actual_tot");
                    int actual9TotOrdinal = drOriginator.GetOrdinal("m9_actual_tot");
                    int actual10TotOrdinal = drOriginator.GetOrdinal("m10_actual_tot");
                    int actual11TotOrdinal = drOriginator.GetOrdinal("m11_actual_tot");
                    int actual12TotOrdinal = drOriginator.GetOrdinal("m12_actual_tot");

                    int actualtytdTotOrdinal = drOriginator.GetOrdinal("tytd_actual_tot");
                    int actuallytdTotOrdinal = drOriginator.GetOrdinal("lytd_actual_tot");
                    int actualyear1TotOrdinal = drOriginator.GetOrdinal("year1_actual_tot");
                    int actualyear2TotOrdinal = drOriginator.GetOrdinal("year2_actual_tot");

                    int totCountOrdinal = drOriginator.GetOrdinal("totcount");

                    while (drOriginator.Read())
                    {
                        TargetInfoEntity repEntity = new TargetInfoEntity();

                        if (!drOriginator.IsDBNull(repCodeOrdinal)) repEntity.Code = drOriginator.GetString(repCodeOrdinal);
                        if (!drOriginator.IsDBNull(nameOrdinal)) repEntity.Name = drOriginator.GetString(nameOrdinal);

                        if (!drOriginator.IsDBNull(m1Ordinal)) repEntity.TargetMonth1 = Math.Round(drOriginator.GetInt32(m1Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m2Ordinal)) repEntity.TargetMonth2 = Math.Round(drOriginator.GetInt32(m2Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m3Ordinal)) repEntity.TargetMonth3 = Math.Round(drOriginator.GetInt32(m3Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m4Ordinal)) repEntity.TargetMonth4 = Math.Round(drOriginator.GetInt32(m4Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m5Ordinal)) repEntity.TargetMonth5 = Math.Round(drOriginator.GetInt32(m5Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m6Ordinal)) repEntity.TargetMonth6 = Math.Round(drOriginator.GetInt32(m6Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m7Ordinal)) repEntity.TargetMonth7 = Math.Round(drOriginator.GetInt32(m7Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m8Ordinal)) repEntity.TargetMonth8 = Math.Round(drOriginator.GetInt32(m8Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m9Ordinal)) repEntity.TargetMonth9 = Math.Round(drOriginator.GetInt32(m9Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m10Ordinal)) repEntity.TargetMonth10 = Math.Round(drOriginator.GetInt32(m10Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m11Ordinal)) repEntity.TargetMonth11 = Math.Round(drOriginator.GetInt32(m11Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m12Ordinal)) repEntity.TargetMonth12 = Math.Round(drOriginator.GetInt32(m12Ordinal) / displayOption, 2);

                        if (!drOriginator.IsDBNull(tytdOrdinal)) repEntity.TargetTYTD = Math.Round(drOriginator.GetInt32(tytdOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(lytdOrdinal)) repEntity.TargetLYTD = Math.Round(drOriginator.GetInt32(lytdOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(year1Ordinal)) repEntity.TargetYear1 = Math.Round(drOriginator.GetInt32(year1Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(year2Ordinal)) repEntity.TargetYear2 = Math.Round(drOriginator.GetInt32(year2Ordinal) / displayOption, 2);

                        if (!drOriginator.IsDBNull(actual1Ordinal)) repEntity.ActualMonth1 = Math.Round(drOriginator.GetInt32(actual1Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual2Ordinal)) repEntity.ActualMonth2 = Math.Round(drOriginator.GetInt32(actual2Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual3Ordinal)) repEntity.ActualMonth3 = Math.Round(drOriginator.GetInt32(actual3Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual4Ordinal)) repEntity.ActualMonth4 = Math.Round(drOriginator.GetInt32(actual4Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual5Ordinal)) repEntity.ActualMonth5 = Math.Round(drOriginator.GetInt32(actual5Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual6Ordinal)) repEntity.ActualMonth6 = Math.Round(drOriginator.GetInt32(actual6Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual7Ordinal)) repEntity.ActualMonth7 = Math.Round(drOriginator.GetInt32(actual7Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual8Ordinal)) repEntity.ActualMonth8 = Math.Round(drOriginator.GetInt32(actual8Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual9Ordinal)) repEntity.ActualMonth9 = Math.Round(drOriginator.GetInt32(actual9Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual10Ordinal)) repEntity.ActualMonth10 = Math.Round(drOriginator.GetInt32(actual10Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual11Ordinal)) repEntity.ActualMonth11 = Math.Round(drOriginator.GetInt32(actual11Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual12Ordinal)) repEntity.ActualMonth12 = Math.Round(drOriginator.GetInt32(actual12Ordinal) / displayOption, 2);

                        if (!drOriginator.IsDBNull(actualtytdOrdinal)) repEntity.ActualTYTD = Math.Round(drOriginator.GetInt32(actualtytdOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actuallytdOrdinal)) repEntity.ActualLYTD = Math.Round(drOriginator.GetInt32(actuallytdOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actualyear1Ordinal)) repEntity.ActualYear1 = Math.Round(drOriginator.GetInt32(actualyear1Ordinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actualyear2Ordinal)) repEntity.ActualYear2 = Math.Round(drOriginator.GetInt32(actualyear2Ordinal) / displayOption, 2);


                        if (!drOriginator.IsDBNull(m1totOrdinal)) repEntity.TargetMonth1Tot = Math.Round(drOriginator.GetInt32(m1totOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m2totOrdinal)) repEntity.TargetMonth2Tot = Math.Round(drOriginator.GetInt32(m2totOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m3totOrdinal)) repEntity.TargetMonth3Tot = Math.Round(drOriginator.GetInt32(m3totOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m4totOrdinal)) repEntity.TargetMonth4Tot = Math.Round(drOriginator.GetInt32(m4totOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m5totOrdinal)) repEntity.TargetMonth5Tot = Math.Round(drOriginator.GetInt32(m5totOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m6totOrdinal)) repEntity.TargetMonth6Tot = Math.Round(drOriginator.GetInt32(m6totOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m7totOrdinal)) repEntity.TargetMonth7Tot = Math.Round(drOriginator.GetInt32(m7totOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m8totOrdinal)) repEntity.TargetMonth8Tot = Math.Round(drOriginator.GetInt32(m8totOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m9totOrdinal)) repEntity.TargetMonth9Tot = Math.Round(drOriginator.GetInt32(m9totOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m10totOrdinal)) repEntity.TargetMonth10Tot = Math.Round(drOriginator.GetInt32(m10totOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m11totOrdinal)) repEntity.TargetMonth11Tot = Math.Round(drOriginator.GetInt32(m11totOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(m12totOrdinal)) repEntity.TargetMonth12Tot = Math.Round(drOriginator.GetInt32(m12totOrdinal) / displayOption, 2);

                        if (!drOriginator.IsDBNull(tytdTotOrdinal)) repEntity.TargetTYTDTot = Math.Round(drOriginator.GetInt32(tytdTotOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(lytdTotOrdinal)) repEntity.TargetLYTDTot = Math.Round(drOriginator.GetInt32(lytdTotOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(year1TotOrdinal)) repEntity.TargetYear1Tot = Math.Round(drOriginator.GetInt32(year1TotOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(year2TotOrdinal)) repEntity.TargetYear2Tot = Math.Round(drOriginator.GetInt32(year2TotOrdinal) / displayOption, 2);

                        if (!drOriginator.IsDBNull(actual1TotOrdinal)) repEntity.ActualMonth1Tot = Math.Round(drOriginator.GetInt32(actual1TotOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual2TotOrdinal)) repEntity.ActualMonth2Tot = Math.Round(drOriginator.GetInt32(actual2TotOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual3TotOrdinal)) repEntity.ActualMonth3Tot = Math.Round(drOriginator.GetInt32(actual3TotOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual4TotOrdinal)) repEntity.ActualMonth4Tot = Math.Round(drOriginator.GetInt32(actual4TotOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual5TotOrdinal)) repEntity.ActualMonth5Tot = Math.Round(drOriginator.GetInt32(actual5TotOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual6TotOrdinal)) repEntity.ActualMonth6Tot = Math.Round(drOriginator.GetInt32(actual6TotOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual7TotOrdinal)) repEntity.ActualMonth7Tot = Math.Round(drOriginator.GetInt32(actual7TotOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual8TotOrdinal)) repEntity.ActualMonth8Tot = Math.Round(drOriginator.GetInt32(actual8TotOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual9TotOrdinal)) repEntity.ActualMonth9Tot = Math.Round(drOriginator.GetInt32(actual9TotOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual10TotOrdinal)) repEntity.ActualMonth10Tot = Math.Round(drOriginator.GetInt32(actual10TotOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual11TotOrdinal)) repEntity.ActualMonth11Tot = Math.Round(drOriginator.GetInt32(actual11TotOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actual12TotOrdinal)) repEntity.ActualMonth12Tot = Math.Round(drOriginator.GetInt32(actual12TotOrdinal) / displayOption, 2);

                        if (!drOriginator.IsDBNull(actualtytdTotOrdinal)) repEntity.ActualTYTDTot = Math.Round(drOriginator.GetInt32(actualtytdTotOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actuallytdTotOrdinal)) repEntity.ActualLYTDTot = Math.Round(drOriginator.GetInt32(actuallytdTotOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actualyear1TotOrdinal)) repEntity.ActualYear1Tot = Math.Round(drOriginator.GetInt32(actualyear1TotOrdinal) / displayOption, 2);
                        if (!drOriginator.IsDBNull(actualyear2TotOrdinal)) repEntity.ActualYear2Tot = Math.Round(drOriginator.GetInt32(actualyear2TotOrdinal) / displayOption, 2);

                        if (!drOriginator.IsDBNull(totCountOrdinal)) repEntity.RowCount = Convert.ToInt32(drOriginator.GetInt32(totCountOrdinal));


                        replist.Add(repEntity);
                    }
                }

                return replist;
            }
            catch
            {
                return replist;
            }
            finally
            {
                if (drOriginator != null)
                    drOriginator.Close();
            }
        }

        public List<TargetInfoEntity> GetTodaySalesDrilldown(SalesInfoDetailSearchCriteriaEntity busSalesEnqSrc, ArgsEntity args)
        {
            TargetInfoEntity objEntity = null;
            DataTable objDS = new DataTable();
            List<TargetInfoEntity> objList = new List<TargetInfoEntity>();

            try
            {
                double displayOption = 1;
                if (busSalesEnqSrc.cDisplayOption == 'M')
                {
                    displayOption = 1000;
                }

                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetSalesDetailsForDashBoard";

                    objCommand.Parameters.AddWithValue("@Distributor", args.Originator);
                    objCommand.Parameters.AddWithValue("@DetailType", busSalesEnqSrc.DetailType);
                    objCommand.Parameters.AddWithValue("@CustCode", busSalesEnqSrc.CustomerCode);
                    objCommand.Parameters.AddWithValue("@BrandCode", busSalesEnqSrc.Brand);
                    objCommand.Parameters.AddWithValue("@RepCode", busSalesEnqSrc.RepCode);
                    objCommand.Parameters.AddWithValue("@ProductCode", "");
                    objCommand.Parameters.AddWithValue("@RouteId", busSalesEnqSrc.Route);
                    objCommand.Parameters.AddWithValue("@MarketId", busSalesEnqSrc.Market);
                    objCommand.Parameters.AddWithValue("@IsCMP", busSalesEnqSrc.IsCMP);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objEntity = new TargetInfoEntity();

                            if (item["code"] != DBNull.Value) objEntity.Code = item["code"].ToString();
                            if (item["name"] != DBNull.Value) objEntity.Name = item["name"].ToString();
                            if (item["ty_today_actual"] != DBNull.Value) objEntity.TyTodayActual = Math.Round(Convert.ToInt32(item["ty_today_actual"]) / displayOption, 2);
                            if (item["ty_tmtd_actual"] != DBNull.Value) objEntity.TyTtmtdActual = Math.Round(Convert.ToInt32(item["ty_tmtd_actual"]) / displayOption, 2);
                            if (item["ty_lmtd_actual"] != DBNull.Value) objEntity.TyLmtdActual = Math.Round(Convert.ToInt32(item["ty_lmtd_actual"]) / displayOption, 2);
                            if (item["ly_tmtd_actual"] != DBNull.Value) objEntity.LyTmtdActual = Math.Round(Convert.ToInt32(item["ly_tmtd_actual"]) / displayOption, 2);
                            if (item["ty_today_actual_tot"] != DBNull.Value) objEntity.TyTodayActualTot = Math.Round(Convert.ToInt32(item["ty_today_actual_tot"]) / displayOption, 2);
                            if (item["ty_tmtd_actual_tot"] != DBNull.Value) objEntity.TyTtmtdActualTot = Math.Round(Convert.ToInt32(item["ty_tmtd_actual_tot"]) / displayOption, 2);
                            if (item["ty_lmtd_actual_tot"] != DBNull.Value) objEntity.TyLmtdActualTot = Math.Round(Convert.ToInt32(item["ty_lmtd_actual_tot"]) / displayOption, 2);
                            if (item["ly_tmtd_actual_tot"] != DBNull.Value) objEntity.LyTmtdActualTot = Math.Round(Convert.ToInt32(item["ly_tmtd_actual_tot"]) / displayOption, 2);
                            if (item["totcount"] != DBNull.Value) objEntity.RowCount = Convert.ToInt32(item["totcount"]);

                            objList.Add(objEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public List<OriginatorEntity> GetParentOriginators(string sOriginator)
        {
            DbDataReader drOriginator = null;

            try
            {
                List<OriginatorEntity> originatorlist = new List<OriginatorEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", sOriginator, DbType.String);
                drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetParentOriginators"], paramCollection);

                if (drOriginator != null && drOriginator.HasRows)
                {
                    int originatorIdOrdinal = drOriginator.GetOrdinal("originator_id");
                    int originatorOrdinal = drOriginator.GetOrdinal("originator");
                    int nameOrdinal = drOriginator.GetOrdinal("name");

                    while (drOriginator.Read())
                    {
                        OriginatorEntity originatorEntity = CreateObject();

                        if (!drOriginator.IsDBNull(originatorIdOrdinal)) originatorEntity.OriginatorId = drOriginator.GetInt32(originatorIdOrdinal);
                        if (!drOriginator.IsDBNull(originatorOrdinal)) originatorEntity.UserName = drOriginator.GetString(originatorOrdinal);
                        if (!drOriginator.IsDBNull(originatorOrdinal)) originatorEntity.Originator = drOriginator.GetString(originatorOrdinal);
                        if (!drOriginator.IsDBNull(nameOrdinal)) originatorEntity.Name = drOriginator.GetString(nameOrdinal);

                        originatorlist.Add(originatorEntity);
                    }
                }

                return originatorlist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOriginator != null)
                    drOriginator.Close();
            }
        }

        public OriginatorEntity GetOriginatorByAccessToken(string accessToken)
        {
            //*************************Add By Irosh Fernando 2018/10/3************************************

            OriginatorEntity originator = null;
            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetOriginatorByAccessToken";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            originator = OriginatorEntity.CreateObject();
                            if (item["originator_id"] != DBNull.Value) originator.OriginatorId = Convert.ToInt32(item["originator_id"]);
                            if (item["originator"] != DBNull.Value) originator.Originator = item["originator"].ToString();
                            if (item["password"] != DBNull.Value) originator.Password = "";
                            if (item["access_token"] != DBNull.Value) originator.AccessToken = item["access_token"].ToString();
                            if (item["rep_code"] != DBNull.Value) originator.RepCode = item["rep_code"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return originator;

            //*************************Add By Irosh Fernando 2018/10/3************************************
        }

        public string GetNewPinByAccessToken(string accessToken)
        {
            DbDataReader drOriginator = null;

            string newpin = string.Empty;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AccessToken", accessToken, DbType.String);

                drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetNewPinByAccessToken"], paramCollection);

                if (drOriginator != null && drOriginator.HasRows)
                {
                    int idPinNumber = drOriginator.GetOrdinal("pin_no");

                    if (drOriginator.Read())
                    {
                        if (!drOriginator.IsDBNull(idPinNumber)) newpin = drOriginator.GetString(idPinNumber);
                    }
                }

                return newpin;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOriginator != null)
                    drOriginator.Close();
            }
        }

        public OriginatorEntity GetOriginatorRouteByAccessToken(string accessToken)
        {
            OriginatorEntity originator = null;
            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetOriginatorRouteByAccessToken";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            originator = new OriginatorEntity();

                            if (item["route_id"] != DBNull.Value) originator.RouteId = Convert.ToInt32(item["route_id"]);
                            if (item["route_name"] != DBNull.Value) originator.RouteName = item["route_name"].ToString();
                            if (item["territory_id"] != DBNull.Value) originator.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_code"] != DBNull.Value) originator.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) originator.TerritoryName = item["territory_name"].ToString();
                            if (item["area_id"] != DBNull.Value) originator.AreaId = Convert.ToInt32(item["area_id"]);
                            if (item["area_code"] != DBNull.Value) originator.AreaCode = item["area_code"].ToString();
                            if (item["area_name"] != DBNull.Value) originator.AreaName = item["area_name"].ToString();
                            if (item["region_id"] != DBNull.Value) originator.RegionId = Convert.ToInt32(item["region_id"]);
                            if (item["region_code"] != DBNull.Value) originator.RegionCode = item["region_code"].ToString();
                            if (item["region_name"] != DBNull.Value) originator.RegionName = item["region_name"].ToString();
                            if (item["rsm_id"] != DBNull.Value) originator.RsmId = Convert.ToInt32(item["rsm_id"]);
                            if (item["rsm_code"] != DBNull.Value) originator.RsmCode = item["rsm_code"].ToString();
                            if (item["rsm_name"] != DBNull.Value) originator.Rsmname = item["rsm_name"].ToString();
                            if (item["asm_id"] != DBNull.Value) originator.AsmId = Convert.ToInt32(item["asm_id"]);
                            if (item["asm_code"] != DBNull.Value) originator.AsmCode = item["asm_code"].ToString();
                            if (item["asm_name"] != DBNull.Value) originator.Asmname = item["asm_name"].ToString();
                            if (item["dis_id"] != DBNull.Value) originator.distributorId = Convert.ToInt32(item["dis_id"]);
                            if (item["dis_code"] != DBNull.Value) originator.distributorcode = item["dis_code"].ToString();
                            if (item["dis_name"] != DBNull.Value) originator.distributorname = item["dis_name"].ToString();
                            if (item["dis_name_display"] != DBNull.Value) originator.distributorDisplayName = item["dis_name_display"].ToString();
                            if (item["dis_tel"] != DBNull.Value) originator.distributortel = item["dis_tel"].ToString();
                            if (item["last_inv_no"] != DBNull.Value) originator.LastInvoiceNumber = item["last_inv_no"].ToString();
                            if (item["dis_address"] != DBNull.Value) originator.distributoraddress = item["dis_address"].ToString();

                            if (item["last_order_no"] != DBNull.Value) originator.LastOrderNumber = item["last_order_no"].ToString();
                            if (item["sales_type"] != DBNull.Value) originator.SalesType = Convert.ToInt32(item["sales_type"]);
                            if (item["can_addoutlet"] != DBNull.Value) originator.CanAddOutlet = Convert.ToInt32(item["can_addoutlet"]);

                            try
                            {
                                if (item["is_checkin"] != DBNull.Value) originator.IsAttendanceCheckIn = Convert.ToBoolean(item["is_checkin"]);
                                if (item["is_checkout"] != DBNull.Value) originator.IsAttendanceCheckOut = Convert.ToBoolean(item["is_checkout"]);
                                if (item["is_geofence"] != DBNull.Value) originator.IsGeoFenceEnable = Convert.ToBoolean(item["is_geofence"]);
                                if (item["is_mtuser"] != DBNull.Value) originator.IsMTUser = Convert.ToBoolean(item["is_mtuser"]);
                            }
                            catch {
                                originator.IsAttendanceCheckIn = false;
                                originator.IsAttendanceCheckOut = false;
                                originator.IsGeoFenceEnable = false;
                            }

                            DynamicReportEntity dReportEntity = new DynamicReportEntity();
                            try
                            {
                                if (item["dynamicReport_report_vales"] != DBNull.Value) dReportEntity.reportValue = item["dynamicReport_report_vales"].ToString();
                            }
                            catch {

                                dReportEntity.reportValue = "1300";
                            }

                            originator.DashboardParam = dReportEntity;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return originator;
        }

        public bool PushNotifyGetSyncDetails()
        {
            bool successful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                iNoRec = this.DataAcessService.ExecuteNonQuery(originatorSql["PushNotifyGetSyncDetails"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public List<OriginatorEntity> GetLevelThreeParentOriginators(string originator)
        {
            DbDataReader drOriginator = null;
            List<OriginatorEntity> originatorlist = new List<OriginatorEntity>();

            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", originator, DbType.String);

                drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetLevelThreeParentOriginators"], paramCollection);

                if (drOriginator != null && drOriginator.HasRows)
                {
                    int originatorIdOrdinal = drOriginator.GetOrdinal("originator_id");
                    int originatorOrdinal = drOriginator.GetOrdinal("originator");
                    int nameOrdinal = drOriginator.GetOrdinal("name");
                    int emailOrdinal = drOriginator.GetOrdinal("internet_add");

                    while (drOriginator.Read())
                    {
                        OriginatorEntity originatorEntity = CreateObject();

                        if (!drOriginator.IsDBNull(originatorIdOrdinal)) originatorEntity.OriginatorId = drOriginator.GetInt32(originatorIdOrdinal);
                        if (!drOriginator.IsDBNull(originatorOrdinal)) originatorEntity.Originator = drOriginator.GetString(originatorOrdinal);
                        if (!drOriginator.IsDBNull(originatorOrdinal)) originatorEntity.UserName = drOriginator.GetString(originatorOrdinal);
                        if (!drOriginator.IsDBNull(nameOrdinal)) originatorEntity.Name = drOriginator.GetString(nameOrdinal);
                        if (!drOriginator.IsDBNull(emailOrdinal)) originatorEntity.Email = drOriginator.GetString(emailOrdinal);

                        originatorlist.Add(originatorEntity);
                    }
                }

                return originatorlist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOriginator != null)
                    drOriginator.Close();
            }
        }

        public List<RepGeoLocation> GetRepLocationsByOriginator(string originator)
        {
            DbDataReader drOriginator = null;
            List<RepGeoLocation> repLocationlist = new List<RepGeoLocation>();
            RepGeoLocation repLocation = new RepGeoLocation();

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", originator, DbType.String);

                drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetRepLocations"], paramCollection);

                if (drOriginator != null && drOriginator.HasRows)
                {
                    int datetimeOrdinal = drOriginator.GetOrdinal("originator_id");
                    int originatorOrdinal = drOriginator.GetOrdinal("originator");
                    int currLatOrdinal = drOriginator.GetOrdinal("name");
                    int currLongOrdinal = drOriginator.GetOrdinal("internet_add");

                    while (drOriginator.Read())
                    {
                        repLocation = new RepGeoLocation();
                        if (!drOriginator.IsDBNull(datetimeOrdinal)) repLocation.DateTime = drOriginator.GetDateTime(datetimeOrdinal);
                        if (!drOriginator.IsDBNull(originatorOrdinal)) repLocation.Originator = drOriginator.GetString(originatorOrdinal);
                        if (!drOriginator.IsDBNull(currLatOrdinal)) repLocation.CurrLat = drOriginator.GetString(currLatOrdinal);
                        if (!drOriginator.IsDBNull(currLongOrdinal)) repLocation.CurrLong = drOriginator.GetString(currLongOrdinal);
                        repLocationlist.Add(repLocation);
                    }
                }

                return repLocationlist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOriginator != null)
                    drOriginator.Close();
            }
        }

        public bool UpdatePassword(string originator, string password)
        {
            bool successful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", originator, DbType.String);
                paramCollection.Add("@Password", password, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(originatorSql["UpdatePassword"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateMobilePin()
        {
            bool successful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", "", DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(originatorSql["UpdateMobilePin"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool ClearDeviceEMEIFromOriginator(string originator, string emei_no)
        {
            bool successful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", originator, DbType.String);
                paramCollection.Add("@emei_no", emei_no, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(originatorSql["ClearDeviceEMEIFromOriginator"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateRepEMEI(string originator, string emei)
        {
            bool successful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", originator, DbType.String);
                paramCollection.Add("@emei", emei, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(originatorSql["UpdateRepEMEI"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateRepsSyncedStatus(string RepCode, string emei, bool IsSync, string ApkVersion)
        {
            bool successful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@RepCode", RepCode, DbType.String);
                paramCollection.Add("@emei", emei, DbType.String);
                paramCollection.Add("@IsSync", IsSync, DbType.Boolean);
                paramCollection.Add("@ApkVersion", ApkVersion, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(originatorSql["UpdateRepsSyncedStatus"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public bool UpdateDeviceIdByEmei(string originator, string emei, string DeviceId)
        {
            //bool successful = false;
            //int iNoRec = 0;
            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            //    paramCollection.Add("@Originator", originator, DbType.String);
            //    paramCollection.Add("@emei", emei, DbType.String);
            //    paramCollection.Add("@DeviceId", DeviceId, DbType.String);

            //    iNoRec = this.DataAcessService.ExecuteNonQuery(originatorSql["UpdateDeviceIdByEmei"], paramCollection);

            //    if (iNoRec > 0)
            //        successful = true;

            //    return successful;
            //}
            //catch (Exception)
            //{
            //    throw;
            //}

            int strResult = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateDeviceIdByEmei";

                    objCommand.Parameters.AddWithValue("@Originator", originator);
                    objCommand.Parameters.AddWithValue("@emei", emei);
                    objCommand.Parameters.AddWithValue("@DeviceId", DeviceId);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    strResult = objCommand.ExecuteNonQuery();
                    objCommand.Connection.Close();

                    if (strResult > 0)
                        return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }

            return false;
        }

        public bool InsertRepLocation(string originator, string CurrLat, string CurrLong)
        {
            bool successful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", originator, DbType.String);
                paramCollection.Add("@CurrLat", CurrLat, DbType.String);
                paramCollection.Add("@CurrLong", CurrLong, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(originatorSql["InsertRepLocation"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
        }

        public string InsertRepEMEI(string userName, string emei)
        {
            bool successful = false;
            int iNoRec = 0;
            string ret;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@userName", userName, DbType.String);
                paramCollection.Add("@emei", emei, DbType.String);

                //iNoRec = this.DataAcessService.ExecuteNonQuery(originatorSql["InsertRepEMEI"], paramCollection);

                //if (iNoRec > 0)
                //    successful = true;

                //return successful;

                ret = System.Convert.ToString(this.DataAcessService.GetOneValue(originatorSql["InsertRepEMEI"], paramCollection));
            }
            catch (Exception)
            {
                throw;
            }

            return ret;
        }

        public List<OriginatorEMEIEntity> GetAllOriginatorEMEINos(string originator)
        {
            DbDataReader drOriginator = null;

            try
            {
                List<OriginatorEMEIEntity> originatorlist = new List<OriginatorEMEIEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", originator, DbType.String);
                drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["GetAllOriginatorEMEINos"], paramCollection);

                if (drOriginator != null && drOriginator.HasRows)
                {
                    int originatorOrdinal = drOriginator.GetOrdinal("originator");
                    int mobile_noOrdinal = drOriginator.GetOrdinal("mobile_no");
                    int emei_noOrdinal = drOriginator.GetOrdinal("emei_no");

                    while (drOriginator.Read())
                    {
                        OriginatorEMEIEntity originatorEntity = new OriginatorEMEIEntity();
                        if (!drOriginator.IsDBNull(originatorOrdinal)) originatorEntity.originator = drOriginator.GetString(originatorOrdinal);
                        if (!drOriginator.IsDBNull(mobile_noOrdinal)) originatorEntity.mobile_no = drOriginator.GetString(mobile_noOrdinal);
                        if (!drOriginator.IsDBNull(emei_noOrdinal)) originatorEntity.emei_no = drOriginator.GetString(emei_noOrdinal);

                        originatorlist.Add(originatorEntity);
                    }
                }

                return originatorlist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOriginator != null)
                    drOriginator.Close();
            }
        }

        //for originators
        public List<OriginatorEntity> GetOriginators(ArgsEntity args)
        {
            OriginatorEntity objEntity = null;
            DataTable objDS = new DataTable();
            List<OriginatorEntity> objList = new List<OriginatorEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetOriginatorDataForGrid";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    if (args.AdditionalParams == null)
                    {
                        objCommand.Parameters.AddWithValue("@AddParams", "");
                    }
                    else
                    {
                        objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    }

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objEntity = new OriginatorEntity();
                            if (item["originator"] != DBNull.Value) objEntity.Originator = item["originator"].ToString();
                            if (item["name"] != DBNull.Value) objEntity.UserName = item["name"].ToString();
                            if (item["designation"] != DBNull.Value) objEntity.designation = item["designation"].ToString();
                            if (item["user_type"] != DBNull.Value) objEntity.user_type = item["user_type"].ToString();
                            if (item["type_des"] != DBNull.Value) objEntity.type_des = item["type_des"].ToString();
                            if (item["otp_mobile"] != DBNull.Value) objEntity.Mobile = item["otp_mobile"].ToString();
                            if (item["originator_id"] != DBNull.Value) objEntity.UserId = Convert.ToInt32(item["originator_id"]);
                            if (item["total_count"] != DBNull.Value) objEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public List<OriginatorEntity> GetOriginatorsFor2FA(ArgsEntity args)
        {
            OriginatorEntity objEntity = null;
            DataTable objDS = new DataTable();
            List<OriginatorEntity> objList = new List<OriginatorEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "sp_GetAll_Originators_for_2FA";

                    objCommand.Parameters.AddWithValue("@OrderBy", "originator");
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    if (args.AdditionalParams == null)
                    {
                        objCommand.Parameters.AddWithValue("@AddParams", "");
                    }
                    else
                    {
                        objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    }

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objEntity = new OriginatorEntity();
                            if (item["originator"] != DBNull.Value) objEntity.Originator = item["originator"].ToString();
                            if (item["name"] != DBNull.Value) objEntity.UserName = item["name"].ToString();
                            if (item["designation"] != DBNull.Value) objEntity.designation = item["designation"].ToString();
                            if (item["tfa_is_authenticated"] != DBNull.Value) objEntity.TFAIsAuthenticated = Convert.ToBoolean(item["tfa_is_authenticated"]);
                            if (item["tfa_user"] != DBNull.Value) objEntity.IsTFAUser = Convert.ToBoolean(item["tfa_user"]);
                            if (item["originator_id"] != DBNull.Value) objEntity.OriginatorId = Convert.ToInt32(item["originator_id"]);
                            if (item["total_count"] != DBNull.Value) objEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public bool UpdateIsTFAUser(string originator, bool isTFA)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;

                    //objCommand.Transaction = trans;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateOriginatorTFA_IsTFAUser";

                    objCommand.Parameters.AddWithValue("@originator", originator);
                    objCommand.Parameters.AddWithValue("@IsTFAUser", isTFA);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    iNoRec = objCommand.ExecuteNonQuery();
                    objCommand.Connection.Close();

                    if (iNoRec > 0)
                        successful = true;

                    return successful;
                }
            }
            catch (Exception ex)
            {
                return successful;
            }
        }

        public bool UpdateIsTFAAuth(string originator, bool isTFAAuth)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;

                    //objCommand.Transaction = trans;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateOriginatorTFA_IsTFAAuthenticated";

                    objCommand.Parameters.AddWithValue("@originator", originator);
                    objCommand.Parameters.AddWithValue("@IsAuthenticated", isTFAAuth);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    iNoRec = objCommand.ExecuteNonQuery();
                    objCommand.Connection.Close();

                    if (iNoRec > 0)
                        successful = true;

                    return successful;
                }
            }
            catch (Exception ex)
            {
                return successful;
            }
        }

        //for adding new user
        public string addNewUser(string originator, string userName, string designation, string password, string userType, string mobile)
        {
            string successful = null;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@originator", originator, DbType.String);
                paramCollection.Add("@userName", userName, DbType.String);
                paramCollection.Add("@designation", designation, DbType.String);
                paramCollection.Add("@password", password, DbType.String);
                paramCollection.Add("@dept_string", userType, DbType.String);
                paramCollection.Add("@auth_level", 30);
                paramCollection.Add("@mobile", mobile, DbType.String);


                iNoRec = this.DataAcessService.ExecuteNonQuery(originatorSql["addNewUser"], paramCollection);

                if (iNoRec > 0)
                    successful = "successful";

                return successful;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public string InsertDailyRepInvoiceCount(string datetime, string repcode, string emei, int invoicecount, int delinvoicecount)
        {
            //string successful = null;
            //int iNoRec = 0;
            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            //    paramCollection.Add("@datetime", datetime, DbType.String);
            //    paramCollection.Add("@repcode", repcode, DbType.String);
            //    paramCollection.Add("@emei", emei, DbType.String);
            //    paramCollection.Add("@invoicecount", invoicecount, DbType.Int32);
            //    paramCollection.Add("@delinvoicecount", delinvoicecount, DbType.Int32);

            //    iNoRec = this.DataAcessService.ExecuteNonQuery(originatorSql["InsertDailyRepInvoiceCount"], paramCollection);

            //    if (iNoRec > 0)
            //        successful = "successful";

            //    return successful;
            //}
            //catch (Exception)
            //{
            //    throw;
            //}

            string successful = null;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;

                    //objCommand.Transaction = trans;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertDailyRepInvoiceCount";

                    objCommand.Parameters.AddWithValue("@datetime", datetime);
                    objCommand.Parameters.AddWithValue("@repcode", repcode);
                    objCommand.Parameters.AddWithValue("@emei", emei);
                    objCommand.Parameters.AddWithValue("@invoicecount", invoicecount);
                    objCommand.Parameters.AddWithValue("@delinvoicecount", delinvoicecount);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    iNoRec = objCommand.ExecuteNonQuery();
                    objCommand.Connection.Close();

                    if (iNoRec > 0)
                        successful = "successful";

                    return successful;
                }
            }
            catch (Exception ex)
            {
                return successful;
            }
        }
        
        public bool UpdateOriginatorVisitDetail(string originator, OrigivatorVisitModel visitDet)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;

                    //objCommand.Transaction = trans;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateOriginatorVisitDetail";

                    //objCommand.Parameters.AddWithValue("@datetime", originator);
                    objCommand.Parameters.AddWithValue("@visitType", visitDet.visitType);
                    objCommand.Parameters.AddWithValue("@fromOriginator", visitDet.fromOriginator);
                    objCommand.Parameters.AddWithValue("@toOriginator", visitDet.toOriginator);
                    objCommand.Parameters.AddWithValue("@cinLatitude", visitDet.cinLatitude);
                    objCommand.Parameters.AddWithValue("@cinLogitude", visitDet.cinLogitude);
                    objCommand.Parameters.AddWithValue("@cinVisitTime", visitDet.cinVisitTime);
                    objCommand.Parameters.AddWithValue("@coutLatitude", visitDet.coutLatitude);
                    objCommand.Parameters.AddWithValue("@coutLogitude", visitDet.coutLogitude);
                    objCommand.Parameters.AddWithValue("@coutVisitTime", visitDet.coutVisitTime);
                    objCommand.Parameters.AddWithValue("@visitNote", (string.IsNullOrEmpty(visitDet.visitNote) == true) ? "" : visitDet.visitNote);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    iNoRec = objCommand.ExecuteNonQuery();
                    objCommand.Connection.Close();

                    if (iNoRec > 0)
                        successful = true;

                    return successful;
                }
            }
            catch (Exception ex)
            {
                return successful;
            }
        }

        public List<OrigivatorVisitModel> GetAllASMVisits(string originator)
        {
            OrigivatorVisitModel objEntity = null;
            DataTable objDS = new DataTable();
            List<OrigivatorVisitModel> objList = new List<OrigivatorVisitModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllASMVisitsByOriginator";

                    objCommand.Parameters.AddWithValue("@Originator", originator);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objEntity = new OrigivatorVisitModel();
                            if (item["visitType"] != DBNull.Value) objEntity.visitType = item["visitType"].ToString();
                            if (item["fromOriginator"] != DBNull.Value) objEntity.fromOriginator = item["fromOriginator"].ToString();
                            if (item["toOriginator"] != DBNull.Value) objEntity.toOriginator = item["toOriginator"].ToString();
                            if (item["cin_latitude"] != DBNull.Value) objEntity.cinLatitude = item["cin_latitude"].ToString();
                            if (item["cin_logitude"] != DBNull.Value) objEntity.cinLogitude = item["cin_logitude"].ToString();
                            if (item["cin_visitTime"] != DBNull.Value) objEntity.cinVisitTime = item["cin_visitTime"].ToString();
                            if (item["cout_latitude"] != DBNull.Value) objEntity.coutLatitude = item["cout_latitude"].ToString();
                            if (item["cout_logitude"] != DBNull.Value) objEntity.coutLogitude = item["cout_logitude"].ToString();
                            if (item["cout_visitTime"] != DBNull.Value) objEntity.coutVisitTime = item["cout_visitTime"].ToString();

                            objList.Add(objEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                objList = new List<OrigivatorVisitModel>();
            }

            return objList;
        }

        //for update user
        public string updateUser(string originator, string userName, string designation, string password, string userType, int UserID, string mobile)
        {
            string successful = null;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@originator", originator, DbType.String);
                paramCollection.Add("@userName", userName, DbType.String);
                paramCollection.Add("@designation", designation, DbType.String);
                paramCollection.Add("@password", password, DbType.String);
                paramCollection.Add("@dept_string", userType, DbType.String);
                paramCollection.Add("@mobile", mobile, DbType.String);
                paramCollection.Add("@originator_id", UserID);

                iNoRec = this.DataAcessService.ExecuteNonQuery(originatorSql["updateUser"], paramCollection);

                if (iNoRec > 0)
                    successful = "successful";

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
        }



        //for deleting user
        public string deleteUser(int UserId)
        {
            string successful = null;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@UserId", UserId);

                iNoRec = this.DataAcessService.ExecuteNonQuery(originatorSql["deleteUser"], paramCollection);

                if (iNoRec > 0)
                    successful = "successful";

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
        }

        // originator types for dropdown
        public List<OriginatorEntity> getAllOriginatorTypesForDropdown()
        {
            OriginatorEntity objEntity = null;
            DataTable objDS = new DataTable();
            List<OriginatorEntity> objList = new List<OriginatorEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "getAllOriginatorTypesForDropdown";

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objEntity = new OriginatorEntity();
                            if (item["user_type_id"] != DBNull.Value) objEntity.user_type_id = Convert.ToInt32(item["user_type_id"].ToString());
                            if (item["user_type"] != DBNull.Value) objEntity.user_type = item["user_type"].ToString();
                            if (item["type_des"] != DBNull.Value) objEntity.type_des = item["type_des"].ToString();

                            objList.Add(objEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        // originator types for dropdown
        public List<SalesDataEntity> getRepsLiveSales(string originator)
        {
            DbDataReader drOriginator = null;

            try
            {
                List<SalesDataEntity> originatorList = new List<SalesDataEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@originator", originator);
                drOriginator = this.DataAcessService.ExecuteQuery(originatorSql["getRepsLiveSales"], paramCollection);

                if (drOriginator != null && drOriginator.HasRows)
                {
                    int rep_codeOrdinal = drOriginator.GetOrdinal("rep_code");
                    int sales_valOrdinal = drOriginator.GetOrdinal("sales_val");

                    SalesDataEntity originatorEntity;

                    while (drOriginator.Read())
                    {
                        originatorEntity = new SalesDataEntity();
                        if (!drOriginator.IsDBNull(rep_codeOrdinal)) originatorEntity.Originator = drOriginator.GetString(rep_codeOrdinal);
                        if (!drOriginator.IsDBNull(sales_valOrdinal)) originatorEntity.sales = drOriginator.GetDouble(sales_valOrdinal);
                        originatorList.Add(originatorEntity);
                    }
                }

                return originatorList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOriginator != null)
                    drOriginator.Close();
            }
        }

        //#region "Perfetti 2nd Phase"

        public List<OriginatorImeiModel> GetAllOriginatorImeiByOriginator(ArgsModel args, string Originator)
        {
            OriginatorImeiModel objEntity = null;
            DataTable objDS = new DataTable();
            List<OriginatorImeiModel> objList = new List<OriginatorImeiModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllOriginatorImeiByOriginator";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@Originator", Originator);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objEntity = new OriginatorImeiModel();
                            if (item["originator"] != DBNull.Value) objEntity.originator = item["originator"].ToString();
                            if (item["emei_no"] != DBNull.Value) objEntity.emei_no = item["emei_no"].ToString();
                            if (item["mobile_no"] != DBNull.Value) objEntity.mobile_no = item["mobile_no"].ToString();
                            if (item["status"] != DBNull.Value) objEntity.status = item["status"].ToString();
                            if (item["last_sync_date"] != DBNull.Value) objEntity.last_sync_date = item["last_sync_date"].ToString();
                            if (item["apk_ver"] != DBNull.Value) objEntity.apk_version = item["apk_ver"].ToString();
                            if (item["total_count"] != DBNull.Value) objEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public bool UpdateOriginatorIsIMEIByOriginator(string originator, bool isImei)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateOriginatorIsIMEIByOriginator";

                    objCommand.Parameters.AddWithValue("@Originator", originator);
                    objCommand.Parameters.AddWithValue("@IsNotImei", !isImei);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }

                    iNoRec = objCommand.ExecuteNonQuery();

                    if (iNoRec > 0)
                        successful = true;
                }
            }
            catch (Exception ex)
            {
                successful = false;
            }

            return successful;
        }
        
        public bool UpdateOriginatorPassword(string originator, string password)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateOriginatorPassword";

                    objCommand.Parameters.AddWithValue("@Originator", originator);
                    objCommand.Parameters.AddWithValue("@Password", password);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }

                    iNoRec = objCommand.ExecuteNonQuery();

                    if (iNoRec > 0)
                        successful = true;
                }
            }
            catch (Exception ex)
            {
                successful = false;
            }

            return successful;
        }

        public bool UpdateOriginatorTFAAuthenticationSettings(string originator, string tfaPin, bool isAuthenticated)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateOriginatorTFAAuthenticationSettings";

                    objCommand.Parameters.AddWithValue("@Originator", originator);
                    objCommand.Parameters.AddWithValue("@TfaPin", tfaPin);
                    objCommand.Parameters.AddWithValue("@IsAuthenticated", isAuthenticated);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }

                    iNoRec = objCommand.ExecuteNonQuery();

                    if (iNoRec > 0)
                        successful = true;
                }
            }
            catch (Exception ex)
            {
                successful = false;
            }

            return successful;
        }

        public bool DeleteEMEIFromOriginator(string originator, string emei_no)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteEMEIFromOriginator";

                    objCommand.Parameters.AddWithValue("@Originator", originator);
                    objCommand.Parameters.AddWithValue("@emei_no", emei_no);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }

                    iNoRec = objCommand.ExecuteNonQuery();

                    if (iNoRec > 0)
                        successful = true;
                }
            }
            catch (Exception ex)
            {
                successful = false;
            }

            return successful;
        }

        //#endregion

        #region "3rd Phase"

        public List<AttendanceReasons> GetAttendanceReasons()
        {
            AttendanceReasons objEntity = null;
            DataTable objDS = new DataTable();
            List<AttendanceReasons> objList = new List<AttendanceReasons>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAttendanceReasons";

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objEntity = new AttendanceReasons();
                            if (item["Id"] != DBNull.Value) objEntity.id = Convert.ToInt32(item["Id"].ToString());
                            if (item["Reason"] != DBNull.Value) objEntity.reason = item["Reason"].ToString();
                            if (item["Description"] != DBNull.Value) objEntity.description = item["Description"].ToString();
                            if (item["Status"] != DBNull.Value) objEntity.status = item["Status"].ToString();

                            objList.Add(objEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public List<SalesRepAttendanceHoursModel> GetRepsAttendanceHours(string originator, string date)
        {
            SalesRepAttendanceHoursModel objEntity = null;
            DataTable objDS = new DataTable();
            List<SalesRepAttendanceHoursModel> objList = new List<SalesRepAttendanceHoursModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllRepsAttendanceHoursByDate";

                    objCommand.Parameters.AddWithValue("@originator", originator);
                    objCommand.Parameters.AddWithValue("@date", date);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objEntity = new SalesRepAttendanceHoursModel();
                            if (item["originator"] != DBNull.Value) objEntity.originator = item["originator"].ToString();
                            if (item["date"] != DBNull.Value) objEntity.date = item["date"].ToString();
                            if (item["attendance_hours"] != DBNull.Value) objEntity.AttendanceHours = Convert.ToDouble(item["attendance_hours"].ToString());
                            if (item["billing_hours"] != DBNull.Value) objEntity.BillingHours = Convert.ToDouble(item["billing_hours"].ToString());
                            if (item["attendance_start"] != DBNull.Value) objEntity.AttendanceStart = item["attendance_start"].ToString();
                            if (item["attendance_end"] != DBNull.Value) objEntity.AttendanceEnd = item["attendance_end"].ToString();
                            if (item["billing_start"] != DBNull.Value) objEntity.BillingStart = item["billing_start"].ToString();
                            if (item["billing_end"] != DBNull.Value) objEntity.BillingEnd = item["billing_end"].ToString();

                            objList.Add(objEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public bool UpdateOriginatorAttendanceCheckIn(OriginatorAttendance attendanceDet)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;

                    //objCommand.Transaction = trans;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateOriginatorAttendanceCheckIn";

                    //objCommand.Parameters.AddWithValue("@datetime", originator);
                    objCommand.Parameters.AddWithValue("@originator", attendanceDet.originator);
                    objCommand.Parameters.AddWithValue("@isCheckIn", true);
                    objCommand.Parameters.AddWithValue("@checkInTime", attendanceDet.checkInTime);
                    objCommand.Parameters.AddWithValue("@checkInLat", attendanceDet.checkInLat);
                    objCommand.Parameters.AddWithValue("@checkInLng", attendanceDet.checkInLng);
                    objCommand.Parameters.AddWithValue("@checkInAddress", attendanceDet.checkInAddress);
                    objCommand.Parameters.AddWithValue("@checkInRouteId", attendanceDet.checkInRouteId);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    iNoRec = objCommand.ExecuteNonQuery();
                    objCommand.Connection.Close();

                    if (iNoRec > 0)
                        successful = true;

                    return successful;
                }
            }
            catch (Exception ex)
            {
                return successful;
            }
        }

        public bool UpdateOriginatorAttendanceCheckOut(OriginatorAttendance attendanceDet)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;

                    //objCommand.Transaction = trans;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateOriginatorAttendanceCheckOut";

                    //objCommand.Parameters.AddWithValue("@datetime", originator);
                    objCommand.Parameters.AddWithValue("@originator", attendanceDet.originator);
                    objCommand.Parameters.AddWithValue("@isCheckOut", true);
                    objCommand.Parameters.AddWithValue("@checkOutTime", attendanceDet.checkOutTime);
                    objCommand.Parameters.AddWithValue("@checkOutLat", attendanceDet.checkOutLat);
                    objCommand.Parameters.AddWithValue("@checkOutLng", attendanceDet.checkOutLng);
                    objCommand.Parameters.AddWithValue("@checkOutAddress", attendanceDet.checkOutAddress);
                    objCommand.Parameters.AddWithValue("@checkOutRouteId", attendanceDet.checkOutRouteId);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    iNoRec = objCommand.ExecuteNonQuery();
                    objCommand.Connection.Close();

                    if (iNoRec > 0)
                        successful = true;

                    return successful;
                }
            }
            catch (Exception ex)
            {
                return successful;
            }
        }

        public bool InsertAttendanceReason(AttendanceReasons attendanceDet)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;

                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertAttendanceReason";

                    objCommand.Parameters.AddWithValue("@Reason", attendanceDet.reason);
                    objCommand.Parameters.AddWithValue("@Description", attendanceDet.description);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    iNoRec = objCommand.ExecuteNonQuery();
                    objCommand.Connection.Close();

                    if (iNoRec > 0)
                        successful = true;

                    return successful;
                }
            }
            catch (Exception ex)
            {
                return successful;
            }
        }

        public bool UpdateAttendanceReason(AttendanceReasons attendanceDet)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;

                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateAttendanceReason";

                    objCommand.Parameters.AddWithValue("@Id", attendanceDet.id);
                    objCommand.Parameters.AddWithValue("@Reason", attendanceDet.reason);
                    objCommand.Parameters.AddWithValue("@Description", attendanceDet.description);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    iNoRec = objCommand.ExecuteNonQuery();
                    objCommand.Connection.Close();

                    if (iNoRec > 0)
                        successful = true;

                    return successful;
                }
            }
            catch (Exception ex)
            {
                return successful;
            }
        }

        public bool InsertOriginatorAttendanceReason(OriginatorAttendanceReasons attendanceDet)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;

                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertOriginatorAttendanceReason";

                    objCommand.Parameters.AddWithValue("@Originator", attendanceDet.originator);
                    objCommand.Parameters.AddWithValue("@Time", attendanceDet.time);
                    objCommand.Parameters.AddWithValue("@Reason", attendanceDet.reason);
                    objCommand.Parameters.AddWithValue("@Description", attendanceDet.description);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    iNoRec = objCommand.ExecuteNonQuery();
                    objCommand.Connection.Close();

                    if (iNoRec > 0)
                        successful = true;

                    return successful;
                }
            }
            catch (Exception ex)
            {
                return successful;
            }
        }

        public bool deleteAttendanceReason(string originator, int id)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;

                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteAttendanceReason";

                    objCommand.Parameters.AddWithValue("@ReasonId", id);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    iNoRec = objCommand.ExecuteNonQuery();
                    objCommand.Connection.Close();

                    if (iNoRec > 0)
                        successful = true;

                    return successful;
                }
            }
            catch (Exception ex)
            {
                return successful;
            }
        }

        public List<DashboardAttendanceByRepType> GetDashboardAttendanceByRepType(string originator, string date)
        {
            DashboardAttendanceByRepType objEntity = null;
            DataTable objDS = new DataTable();
            List<DashboardAttendanceByRepType> objList = new List<DashboardAttendanceByRepType>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetDashboardAttendanceByRepType";

                    objCommand.Parameters.AddWithValue("@Originator", originator);
                    objCommand.Parameters.AddWithValue("@Date", date);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objEntity = new DashboardAttendanceByRepType();
                            if (item["RepType"] != DBNull.Value) objEntity.repType = item["RepType"].ToString();
                            if (item["Description"] != DBNull.Value) objEntity.description = item["Description"].ToString();
                            if (item["AttendanceCount"] != DBNull.Value) objEntity.attendanceCount = Convert.ToInt32(item["AttendanceCount"].ToString());
                            if (item["AttendancePercentage"] != DBNull.Value) objEntity.attendancePercentage = Convert.ToDouble(item["AttendancePercentage"].ToString());

                            objList.Add(objEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public List<DashboardAttendanceDetails> GetDashboardAttendanceDetails(string originator, string date, string salesType, string attendType)
        {
            DashboardAttendanceDetails objEntity = null;
            DataTable objDS = new DataTable();
            List<DashboardAttendanceDetails> objList = new List<DashboardAttendanceDetails>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetDashboardAttendanceDetails";

                    objCommand.Parameters.AddWithValue("@Originator", originator);
                    objCommand.Parameters.AddWithValue("@Date", date);
                    objCommand.Parameters.AddWithValue("@SalesType", salesType);
                    objCommand.Parameters.AddWithValue("@AttendType", attendType);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objEntity = new DashboardAttendanceDetails();
                            if (item["rep_code"] != DBNull.Value) objEntity.repCode = item["rep_code"].ToString();
                            if (item["name"] != DBNull.Value) objEntity.repName = item["name"].ToString();
                            if (item["sales_type"] != DBNull.Value)
                            {
                                if (item["sales_type"].ToString() == "1")
                                {
                                    objEntity.salesType = "Van-Sales";
                                }
                                else if (item["sales_type"].ToString() == "2")
                                {
                                    objEntity.salesType = "Pre-Sales";
                                }                                    
                            }

                            if (item["attend_type"] != DBNull.Value) objEntity.attendType = item["attend_type"].ToString();
                            if (item["checkin_time"] != DBNull.Value) objEntity.checkInTime = Convert.ToDateTime(item["checkin_time"].ToString()).ToString("yyyy/MM/dd HH:mm");
                            if (item["checkout_time"] != DBNull.Value) objEntity.checkOutTime = Convert.ToDateTime(item["checkout_time"].ToString()).ToString("yyyy/MM/dd HH:mm");
                            if (item["comment"] != DBNull.Value) objEntity.comment = item["comment"].ToString();
                            if (item["checkInLat"] != DBNull.Value) objEntity.checkInLat = item["checkInLat"].ToString();
                            if (item["checkInLng"] != DBNull.Value) objEntity.checkInLng = item["checkInLng"].ToString();
                            if (item["checkInAddress"] != DBNull.Value) objEntity.checkInAddress = item["checkInAddress"].ToString();
                            if (item["checkOutLat"] != DBNull.Value) objEntity.checkOutLat = item["checkOutLat"].ToString();
                            if (item["checkOutLng"] != DBNull.Value) objEntity.checkOutLng = item["checkOutLng"].ToString();
                            if (item["checkOutAddress"] != DBNull.Value) objEntity.checkOutAddress = item["checkOutAddress"].ToString();

                            objList.Add(objEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        #endregion
    }
}
