﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Entities;
using System.Data.Common;
using System.Collections;
using Peercore.DataAccess.Common.Parameters;
using System.Data;

namespace Peercore.CRM.DataAccess.DAO
{
    public class RepWizeActivityAnalysisDAO:BaseDAO
    {
        private DbSqlAdapter RepWizeActivityAnalysisSql { get; set; }

        public RepWizeActivityAnalysisDAO()
        {
            this.RepWizeActivityAnalysisSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.RepWizeActivityAnalysisSql.xml");
        }

        public RepWizeActivityAnalysisEntity CreateObject()
        {
            return RepWizeActivityAnalysisEntity.CreateObject();
        }

        public RepWizeActivityAnalysisEntity CreateObject(int entityId)
        {
            return RepWizeActivityAnalysisEntity.CreateObject(entityId);
        }

        public List<RepWizeActivityAnalysisEntity> GetRepWideActivity(DateTime FromDate, DateTime ToDate, List<LeadStageEntity> LeadStageList, ArgsEntity argsEntity, List<OriginatorEntity> OriginatorList)
        {
            DbDataReader drReader = null;
            List<RepWizeActivityAnalysisEntity> RepWizeActivityAnalysisList = null;
            List<LeadStageEntity> lsList = new List<LeadStageEntity>();

            foreach (LeadStageEntity ls in LeadStageList)
            {
                lsList.Add(ls);
            }
          
            try
            {
                string SQLLead = null;
                string SQLStages = null;
                string SQLActComp = null;
                string SQLActNotComp = null;
                string SQLPipeLine = null;
                LeadStageEntity lsLead = null;
                string fromDate = DateTime.Parse(FromDate.ToString("dd-MMM-yyyy 00:00:00")).ToUniversalTime().ToString();
                string toDate = DateTime.Parse(ToDate.ToString("dd-MMM-yyyy 23:59:59")).ToUniversalTime().ToString();

                /* FIRST LEAD STAGE*/
                foreach (LeadStageEntity ls in lsList)
                {
                    if (lsLead == null && ls.StageName != "Customer")
                        lsLead = ls;
                    else
                    {
                        if (lsLead.StageOrder > ls.StageOrder && ls.StageName != "Customer")
                            lsLead = ls;
                    }
                }

                SQLLead = RepWizeActivityAnalysisSql["RepWizeActivityAnalysisLead"].Format(lsLead.StageName.Trim(), lsLead.StageId, toDate, argsEntity.DefaultDepartmentId).ToString();
               
                /*LEAD STAGES*/
                lsList.Remove(lsLead);

                foreach (LeadStageEntity ls in lsList)
                {
                    if (SQLStages == null)
                    {
                        SQLStages = RepWizeActivityAnalysisSql["RepWizeActivityAnalysisStages"].Format(lsLead.StageName.Trim(), lsLead.StageId, toDate, argsEntity.DefaultDepartmentId).ToString();                       
                    }
                    else
                    {
                        SQLStages += " UNION ALL " + RepWizeActivityAnalysisSql["RepWizeActivityAnalysisStages"].Format(lsLead.StageName.Trim(), lsLead.StageId, toDate, argsEntity.DefaultDepartmentId).ToString();                       
                    }
                }

                /*PIPELINE*/

                SQLPipeLine = RepWizeActivityAnalysisSql["RepWizeActivityAnalysisPipeLine"].Format(toDate, "", argsEntity.DefaultDepartmentId).ToString();                       
                              
                //if (FromDate.DayOfWeek.ToString() == "Sunday")
                //    fromDate = DateTime.Parse(FromDate.AddDays(1).ToString()).ToUniversalTime().ToString("dd-MMM-yyyy 00:00:00");

                //if (FromDate.AddDays(7).Equals(ToDate))
                //    toDate = DateTime.Parse(ToDate.AddDays(-2).ToString()).ToUniversalTime().ToString("dd-MMM-yyyy 23:59:59");
                //else if (FromDate.AddDays(6).Equals(ToDate))
                //    toDate = DateTime.Parse(ToDate.AddDays(-1).ToString()).ToUniversalTime().ToString("dd-MMM-yyyy 23:59:59");
                //else
                //    toDate = DateTime.Parse(ToDate.ToString()).ToUniversalTime().ToString("dd-MMM-yyyy 23:59:59");
                /*ACTIVITY COMPLETED*/

                SQLActComp = RepWizeActivityAnalysisSql["RepWizeActivityAnalysisActComp"].Format(fromDate, toDate, argsEntity.DefaultDepartmentId).ToString();     

              
                /*ACTIVITY NOT COMPLETED*/
                //RepWizeActivityAnalysisActNotComp
                SQLActNotComp = RepWizeActivityAnalysisSql["RepWizeActivityAnalysisActNotComp"].Format(fromDate, toDate, argsEntity.DefaultDepartmentId).ToString();  
               
                List<Stages> stagesList = new List<Stages>();
                string sSqlStages = " SELECT b.* FROM ( " + SQLLead + " UNION ALL " + SQLStages + " ) as b WHERE b.originator IN (" + argsEntity.BDMList + ")";

                
                drReader = this.DataAcessService.ExecuteQuery(RepWizeActivityAnalysisSql["sql"].Format(sSqlStages));
               
                if (drReader != null && drReader.HasRows)
                {
                    int countOrdinal = drReader.GetOrdinal("count");
                    int LeadStageOrdinal = drReader.GetOrdinal("LeadStageEntity");
                    int originatorOrdinal = drReader.GetOrdinal("originator");

                    while (drReader.Read())
                    {
                        Stages stages = new Stages();
                        if (!drReader.IsDBNull(countOrdinal)) stages.Count = drReader.GetInt32(countOrdinal);
                        if (!drReader.IsDBNull(LeadStageOrdinal)) stages.StageName = drReader.GetString(LeadStageOrdinal);
                        if (!drReader.IsDBNull(originatorOrdinal)) stages.RepName = drReader.GetString(originatorOrdinal);

                        stagesList.Add(stages);
                    }
                }
                if (drReader != null)
                    drReader.Close();

                List<RepWizeActivityAnalysisEntity> RepWideList = new List<RepWizeActivityAnalysisEntity>();

                string sSql = "SELECT originator,SUM(ACCount) as ACCount, SUM(ANCCount) as ANCCount,SUM(PLCount) as PLCount from(" +
                    SQLActComp + " UNION ALL " + SQLActNotComp + " UNION ALL " + SQLPipeLine +
                    ") as b WHERE b.originator IN (" + argsEntity.BDMList + ") GROUP BY b.originator";

                drReader = this.DataAcessService.ExecuteQuery(RepWizeActivityAnalysisSql["sql"].Format(sSql));
              
                if (drReader != null && drReader.HasRows)
                {
                    int ACCountOrdinal = drReader.GetOrdinal("ACCount");
                    int ANCCountOrdinal = drReader.GetOrdinal("ANCCount");
                    int PLCountOrdinal = drReader.GetOrdinal("PLCount");
                    int originatorOrdinal = drReader.GetOrdinal("originator");

                    while (drReader.Read())
                    {
                        RepWizeActivityAnalysisEntity repWide = RepWizeActivityAnalysisEntity.CreateObject();
                        if (!drReader.IsDBNull(originatorOrdinal)) repWide.RepName = drReader.GetString(originatorOrdinal);
                        if (!drReader.IsDBNull(ANCCountOrdinal)) repWide.ActivityCountNotCompleted = drReader.GetInt64(ANCCountOrdinal);
                        if (!drReader.IsDBNull(ACCountOrdinal)) repWide.ActivityCount = drReader.GetInt64(ACCountOrdinal);
                        if (!drReader.IsDBNull(PLCountOrdinal)) repWide.OppotunityCount = drReader.GetInt64(PLCountOrdinal);

                        RepWideList.Add(repWide);
                    }
                }
                if (drReader != null)
                    drReader.Close();

                               
                RepWizeActivityAnalysisEntity RepWideAnalysis = null;
                List<RepWizeActivityAnalysisEntity> rLst;

                List<Stages> stglst;
                RepWizeActivityAnalysisList = new List<RepWizeActivityAnalysisEntity>();
                foreach (OriginatorEntity o in OriginatorList)
                {
                    var lstNonStage =
                    from c in RepWideList
                    where c.RepName == o.UserName
                    select c;

                    rLst = lstNonStage.ToList();

                    var lstStage =
                    from s in stagesList
                    where s.RepName == o.UserName
                    select s;

                    stglst = lstStage.ToList();

                    if (rLst.Count > 0 || stglst.Count > 0)
                    {
                        if (stglst.Count < LeadStageList.Count)
                        {
                            foreach (LeadStageEntity ls in LeadStageList)
                            {
                                Stages l = stglst.FirstOrDefault(x => x.StageName == ls.StageName);
                                if (l == null)
                                {
                                    l = new Stages()
                                    {
                                        StageName = ls.StageName,
                                        RepName = o.Name,
                                        Count = 0
                                    };
                                    stglst.Add(l);
                                    l = null;
                                }
                            }
                        }

                        if (rLst.Count > 0)
                        {
                            RepWideAnalysis = RepWizeActivityAnalysisEntity.CreateObject();
                            RepWideAnalysis.RepName = o.Name;
                            RepWideAnalysis.ActivityCount = rLst[0].ActivityCount;
                            RepWideAnalysis.ActivityCountNotCompleted = rLst[0].ActivityCountNotCompleted;
                            RepWideAnalysis.OppotunityCount = rLst[0].OppotunityCount;

                        }
                        if (stglst.Count > 0)
                        {
                            if (RepWideAnalysis == null)
                            {
                                RepWideAnalysis = RepWizeActivityAnalysisEntity.CreateObject();
                                RepWideAnalysis.RepName = o.Name;
                                RepWideAnalysis.ActivityCount = 0;
                                RepWideAnalysis.ActivityCountNotCompleted = 0;
                                RepWideAnalysis.OppotunityCount = 0;
                                RepWideAnalysis.StagesList = stglst;
                            }
                            else
                            {
                                RepWideAnalysis.StagesList = stglst;
                            }
                        }
                        RepWizeActivityAnalysisList.Add(RepWideAnalysis);
                        RepWideAnalysis = null;
                    }
                }



                return RepWizeActivityAnalysisList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drReader != null)
                    drReader.Close();
            }
        }
        
    }
}
