﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class SchemeTypeDAO : BaseDAO
    {
        private DbSqlAdapter SchemeTypeSql { get; set; }

        private void RegisterSql()
        {
            this.SchemeTypeSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.SchemeTypeSql.xml", ApplicationService.Instance.DbProvider);
        }

        public SchemeTypeDAO()
        {
            RegisterSql();
        }

        public SchemeTypeDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public SchemeTypeEntity CreateObject()
        {
            return SchemeTypeEntity.CreateObject();
        }

        public SchemeTypeEntity CreateObject(int entityId)
        {
            return SchemeTypeEntity.CreateObject(entityId);
        }

        public List<SchemeTypeEntity> GetAllSchemeType()
        {
            DbDataReader drSchemeType = null;
            try
            {
                List<SchemeTypeEntity> schemeTypeCollection = new List<SchemeTypeEntity>();

                drSchemeType = this.DataAcessService.ExecuteQuery(SchemeTypeSql["GetAllSchemeType"]);
                if (drSchemeType != null && drSchemeType.HasRows)
                {
                    int schemeTypeIdOrdinal = drSchemeType.GetOrdinal("scheme_type_id");
                    int schemeTypeOrdinal = drSchemeType.GetOrdinal("scheme_type");
                    int descriptionOrdinal = drSchemeType.GetOrdinal("description");

                    while (drSchemeType.Read())
                    {
                        SchemeTypeEntity schemeType = SchemeTypeEntity.CreateObject();

                        if (!drSchemeType.IsDBNull(schemeTypeIdOrdinal)) schemeType.SchemeTypeId = drSchemeType.GetInt32(schemeTypeIdOrdinal);
                        if (!drSchemeType.IsDBNull(schemeTypeOrdinal)) schemeType.SchemeType = drSchemeType.GetString(schemeTypeOrdinal);
                        if (!drSchemeType.IsDBNull(descriptionOrdinal)) schemeType.Description = drSchemeType.GetString(descriptionOrdinal);
                        schemeTypeCollection.Add(schemeType);

                    }
                }

                return schemeTypeCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drSchemeType != null)
                    drSchemeType.Close();
            }
        }
    }
}
