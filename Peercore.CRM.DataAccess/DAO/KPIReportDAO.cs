﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Peercore.CRM.Model;
using Peercore.CRM.Shared;
using Peercore.DataAccess.Common.Parameters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Peercore.CRM.DataAccess.DAO
{
    public class KPIReportDAO : BaseDAO
    {
        #region - KPIReport - 

        public string GetAllEmailRecipients(ArgsModel args)
        {
            string emailRecipients = string.Empty;

            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllEmailRecipients";

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            emailRecipients = Convert.ToString(item["email_recipients"]);
                        }
                    }
                }
            }
            catch (Exception)
            {
                emailRecipients = string.Empty;
            }

            return emailRecipients;
        }

        public List<KPIReportModel> GetAllKPIReports(ArgsModel args, int month, int year)
        {
            DataTable objDS = new DataTable();
            List<KPIReportModel> reportList = new List<KPIReportModel>();
            KPIReportModel reportEntity = null;
            month = month + 1;
            string monthStr = month.ToString();
            string yearStr = year.ToString();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetAllKPIReports";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@month", (monthStr));
                    objCommand.Parameters.AddWithValue("@year", (yearStr));

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        reportEntity = new KPIReportModel();

                        //if (item["id"] != DBNull.Value) reportEntity.Id = Convert.ToInt32(item["id"]);
                        if (item["batch_id"] != DBNull.Value) reportEntity.BatchId = Convert.ToInt32(item["batch_id"]);
                        if (item["area_id"] != DBNull.Value) reportEntity.AreaId = Convert.ToInt32(item["area_id"]);
                        if (item["area_name"] != DBNull.Value) reportEntity.AreaName = Convert.ToString(item["area_name"]);
                        if (item["target_date"] != DBNull.Value) reportEntity.TargetDate = Convert.ToDateTime(item["target_date"]);
                        if (item["ta_ready_stock_val"] != DBNull.Value) reportEntity.TotReadyStockVal = Convert.ToInt32(item["ta_ready_stock_val"]);
                        if (item["ta_pre_invoice_val"] != DBNull.Value) reportEntity.TotPreInvoiceVal = Convert.ToInt32(item["ta_pre_invoice_val"]);
                        if (item["ta_ready_stock_pc"] != DBNull.Value) reportEntity.TotReadyStockPc = Convert.ToInt32(item["ta_ready_stock_pc"]);
                        if (item["ta_pre_invoice_pc"] != DBNull.Value) reportEntity.TotPreInvoicePc = Convert.ToInt32(item["ta_pre_invoice_pc"]);
                        if (item["ta_bill_count_1500"] != DBNull.Value) reportEntity.TotBillCount = Convert.ToInt32(item["ta_bill_count_1500"]);
                        if (item["ta_bill_value_1500"] != DBNull.Value) reportEntity.TotBillValue = Convert.ToInt32(item["ta_bill_value_1500"]);
                        if (item["ta_wh_sale_bill_count"] != DBNull.Value) reportEntity.TotWhSalesBillCount = Convert.ToInt32(item["ta_wh_sale_bill_count"]);

                        if (item["ta_brand1_bill_count"] != DBNull.Value) reportEntity.TotBrand1BillCount = Convert.ToInt32(item["ta_brand1_bill_count"]);
                        if (item["ta_brand1_bill_val"] != DBNull.Value) reportEntity.TotBrand1BillVal = Convert.ToInt32(item["ta_brand1_bill_val"]);
                        if (item["ta_brand1_outlet_count"] != DBNull.Value) reportEntity.TotBrand1OutletCount = Convert.ToInt32(item["ta_brand1_outlet_count"]);
                        if (item["ta_brand2_bill_count"] != DBNull.Value) reportEntity.TotBrand2BillCount = Convert.ToInt32(item["ta_brand2_bill_count"]);
                        if (item["ta_brand2_bill_val"] != DBNull.Value) reportEntity.TotBrand2BillVal = Convert.ToInt32(item["ta_brand2_bill_val"]);
                        if (item["ta_brand2_outlet_count"] != DBNull.Value) reportEntity.TotBrand2OutletCount = Convert.ToInt32(item["ta_brand2_outlet_count"]);
                        if (item["ta_brand3_bill_count"] != DBNull.Value) reportEntity.TotBrand3BillCount = Convert.ToInt32(item["ta_brand3_bill_count"]);
                        if (item["ta_brand3_bill_val"] != DBNull.Value) reportEntity.TotBrand3BillVal = Convert.ToInt32(item["ta_brand3_bill_val"]);
                        if (item["ta_brand3_outlet_count"] != DBNull.Value) reportEntity.TotBrand3OutletCount = Convert.ToInt32(item["ta_brand3_outlet_count"]);
                        if (item["ta_brand4_bill_count"] != DBNull.Value) reportEntity.TotBrand4BillCount = Convert.ToInt32(item["ta_brand4_bill_count"]);
                        if (item["ta_brand4_bill_val"] != DBNull.Value) reportEntity.TotBrand4BillVal = Convert.ToInt32(item["ta_brand4_bill_val"]);
                        if (item["ta_brand4_outlet_count"] != DBNull.Value) reportEntity.TotBrand4OutletCount = Convert.ToInt32(item["ta_brand4_outlet_count"]);
                        if (item["ta_brand5_bill_count"] != DBNull.Value) reportEntity.TotBrand5BillCount = Convert.ToInt32(item["ta_brand5_bill_count"]);
                        if (item["ta_brand5_bill_val"] != DBNull.Value) reportEntity.TotBrand5BillVal = Convert.ToInt32(item["ta_brand5_bill_val"]);
                        if (item["ta_brand5_outlet_count"] != DBNull.Value) reportEntity.TotBrand5OutletCount = Convert.ToInt32(item["ta_brand5_outlet_count"]);
                        if (item["ta_multi_lines_bills"] != DBNull.Value) reportEntity.TotMultiLineBill = Convert.ToInt32(item["ta_multi_lines_bills"]);

                        if (item["ta_ready_stock_mandays"] != DBNull.Value) reportEntity.TotReadyStockMandays = Convert.ToInt32(item["ta_ready_stock_mandays"]);
                        if (item["ta_pre_invoice_mandays"] != DBNull.Value) reportEntity.TotPreInvMandays = Convert.ToInt32(item["ta_pre_invoice_mandays"]);
                        if (item["ta_eco"] != DBNull.Value) reportEntity.TotECO = Convert.ToInt32(item["ta_eco"]);
                        if (item["ta_no_sales_account"] != DBNull.Value) reportEntity.TotNoSalesAcc = Convert.ToInt32(item["ta_no_sales_account"]);
                        if (item["ta_non_visit_outlets"] != DBNull.Value) reportEntity.TotNonVisitedOutlets = Convert.ToInt32(item["ta_non_visit_outlets"]);
                        if (item["ta_mss_complience"] != DBNull.Value) reportEntity.TotMSSComplience = Convert.ToInt32(item["ta_mss_complience"]);

                        if (item["avg_ready_stock_val"] != DBNull.Value) reportEntity.AvgReadyStockVal = Convert.ToInt32(item["avg_ready_stock_val"]);
                        if (item["avg_pre_invoice_val"] != DBNull.Value) reportEntity.AvgPreInvoiceVal = Convert.ToInt32(item["avg_pre_invoice_val"]);

                        if (item["avg_ready_stock_pc"] != DBNull.Value) reportEntity.AvgReadyStockPc = Convert.ToInt32(item["avg_ready_stock_pc"]);
                        if (item["avg_pre_invoice_pc"] != DBNull.Value) reportEntity.AvgPreInvoicePc = Convert.ToInt32(item["avg_pre_invoice_pc"]);
                        if (item["avg_bill_count_1500"] != DBNull.Value) reportEntity.AvgBillCount = Convert.ToInt32(item["avg_bill_count_1500"]);
                        if (item["avg_bill_value_1500"] != DBNull.Value) reportEntity.AvgBillValue = Convert.ToInt32(item["avg_bill_value_1500"]);
                        if (item["avg_wh_sale_bill_count"] != DBNull.Value) reportEntity.AvgWhSalesBillCount = Convert.ToInt32(item["avg_wh_sale_bill_count"]);
                        if (item["avg_brand1_bill_count"] != DBNull.Value) reportEntity.AvgBrand1BillCount = Convert.ToInt32(item["avg_brand1_bill_count"]);
                        if (item["avg_brand1_bill_val"] != DBNull.Value) reportEntity.AvgBrand1BillVal = Convert.ToInt32(item["avg_brand1_bill_val"]);
                        if (item["avg_brand1_outlet_count"] != DBNull.Value) reportEntity.AvgBrand1OutletCount = Convert.ToInt32(item["avg_brand1_outlet_count"]);
                        if (item["avg_brand2_bill_count"] != DBNull.Value) reportEntity.AvgBrand2BillCount = Convert.ToInt32(item["avg_brand2_bill_count"]);
                        if (item["avg_brand2_bill_val"] != DBNull.Value) reportEntity.AvgBrand2BillVal = Convert.ToInt32(item["avg_brand2_bill_val"]);
                        if (item["avg_brand2_outlet_count"] != DBNull.Value) reportEntity.AvgBrand2OutletCount = Convert.ToInt32(item["avg_brand2_outlet_count"]);
                        if (item["avg_brand3_bill_count"] != DBNull.Value) reportEntity.AvgBrand3BillCount = Convert.ToInt32(item["avg_brand3_bill_count"]);
                        if (item["avg_brand3_bill_val"] != DBNull.Value) reportEntity.AvgBrand3BillVal = Convert.ToInt32(item["avg_brand3_bill_val"]);
                        if (item["avg_brand3_outlet_count"] != DBNull.Value) reportEntity.AvgBrand3OutletCount = Convert.ToInt32(item["avg_brand3_outlet_count"]);
                        if (item["avg_brand4_bill_count"] != DBNull.Value) reportEntity.AvgBrand4BillCount = Convert.ToInt32(item["avg_brand4_bill_count"]);
                        if (item["avg_brand4_bill_val"] != DBNull.Value) reportEntity.AvgBrand4BillVal = Convert.ToInt32(item["avg_brand4_bill_val"]);
                        if (item["avg_brand4_outlet_count"] != DBNull.Value) reportEntity.AvgBrand4OutletCount = Convert.ToInt32(item["avg_brand4_outlet_count"]);
                        if (item["avg_brand5_bill_count"] != DBNull.Value) reportEntity.AvgBrand5BillCount = Convert.ToInt32(item["avg_brand5_bill_count"]);
                        if (item["avg_brand5_bill_val"] != DBNull.Value) reportEntity.AvgBrand5BillVal = Convert.ToInt32(item["avg_brand5_bill_val"]);
                        if (item["avg_brand5_outlet_count"] != DBNull.Value) reportEntity.AvgBrand5OutletCount = Convert.ToInt32(item["avg_brand5_outlet_count"]);
                        if (item["avg_multi_lines_bills"] != DBNull.Value) reportEntity.AvgMultiLineBill = Convert.ToInt32(item["avg_multi_lines_bills"]);
                        if (item["avg_ready_stock_mandays"] != DBNull.Value) reportEntity.AvgReadyStockMandays = Convert.ToInt32(item["avg_ready_stock_mandays"]);
                        if (item["avg_pre_invoice_mandays"] != DBNull.Value) reportEntity.AvgPreInvMandays = Convert.ToInt32(item["avg_pre_invoice_mandays"]);
                        if (item["avg_eco"] != DBNull.Value) reportEntity.AvgECO = Convert.ToInt32(item["avg_eco"]);
                        if (item["avg_no_sales_account"] != DBNull.Value) reportEntity.AvgNoSalesAcc = Convert.ToInt32(item["avg_no_sales_account"]);
                        if (item["avg_non_visit_outlets"] != DBNull.Value) reportEntity.AvgNonVisitedOutlets = Convert.ToInt32(item["avg_non_visit_outlets"]);
                        if (item["avg_mss_complience"] != DBNull.Value) reportEntity.AvgMSSComplience = Convert.ToInt32(item["avg_mss_complience"]);

                        if (item["created_by"] != DBNull.Value) reportEntity.CreatedBy = item["created_by"].ToString();
                        if (item["total_count"] != DBNull.Value) reportEntity.TotalCount = Convert.ToInt32(item["total_count"]);
                        reportList.Add(reportEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return reportList;
        }

        public List<KPIReportModel> GetAllKPIReportWeeklyTargetsByMonth(ArgsModel args, int month, int year)
        {
            DataTable objDS = new DataTable();
            List<KPIReportModel> reportList = new List<KPIReportModel>();
            KPIReportModel reportEntity = null;
            month = month + 1;
            string monthStr = month.ToString();
            string yearStr = year.ToString();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetAllKPIReportWeeklyTargetsByMonth";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@month", (monthStr));
                    objCommand.Parameters.AddWithValue("@year", (yearStr));

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        reportEntity = new KPIReportModel();

                        //if (item["id"] != DBNull.Value) reportEntity.Id = Convert.ToInt32(item["id"]);
                        if (item["batch_id"] != DBNull.Value) reportEntity.BatchId = Convert.ToInt32(item["batch_id"]);
                        if (item["week_id"] != DBNull.Value) reportEntity.WeekId = Convert.ToInt32(item["week_id"]);
                        if (item["week_name"] != DBNull.Value) reportEntity.WeekName = Convert.ToString(item["week_name"]);
                        if (item["target_date"] != DBNull.Value) reportEntity.TargetDate = Convert.ToDateTime(item["target_date"]);
                        if (item["ta_ready_stock_val"] != DBNull.Value) reportEntity.TotReadyStockVal = Convert.ToInt32(item["ta_ready_stock_val"]);
                        if (item["ta_pre_invoice_val"] != DBNull.Value) reportEntity.TotPreInvoiceVal = Convert.ToInt32(item["ta_pre_invoice_val"]);
                        if (item["ta_ready_stock_pc"] != DBNull.Value) reportEntity.TotReadyStockPc = Convert.ToInt32(item["ta_ready_stock_pc"]);
                        if (item["ta_pre_invoice_pc"] != DBNull.Value) reportEntity.TotPreInvoicePc = Convert.ToInt32(item["ta_pre_invoice_pc"]);
                        if (item["ta_bill_count_1500"] != DBNull.Value) reportEntity.TotBillCount = Convert.ToInt32(item["ta_bill_count_1500"]);
                        if (item["ta_bill_value_1500"] != DBNull.Value) reportEntity.TotBillValue = Convert.ToInt32(item["ta_bill_value_1500"]);
                        if (item["ta_wh_sale_bill_count"] != DBNull.Value) reportEntity.TotWhSalesBillCount = Convert.ToInt32(item["ta_wh_sale_bill_count"]);

                        if (item["ta_brand1_bill_count"] != DBNull.Value) reportEntity.TotBrand1BillCount = Convert.ToInt32(item["ta_brand1_bill_count"]);
                        if (item["ta_brand1_bill_val"] != DBNull.Value) reportEntity.TotBrand1BillVal = Convert.ToInt32(item["ta_brand1_bill_val"]);
                        if (item["ta_brand1_outlet_count"] != DBNull.Value) reportEntity.TotBrand1OutletCount = Convert.ToInt32(item["ta_brand1_outlet_count"]);
                        if (item["ta_brand2_bill_count"] != DBNull.Value) reportEntity.TotBrand2BillCount = Convert.ToInt32(item["ta_brand2_bill_count"]);
                        if (item["ta_brand2_bill_val"] != DBNull.Value) reportEntity.TotBrand2BillVal = Convert.ToInt32(item["ta_brand2_bill_val"]);
                        if (item["ta_brand2_outlet_count"] != DBNull.Value) reportEntity.TotBrand2OutletCount = Convert.ToInt32(item["ta_brand2_outlet_count"]);
                        if (item["ta_brand3_bill_count"] != DBNull.Value) reportEntity.TotBrand3BillCount = Convert.ToInt32(item["ta_brand3_bill_count"]);
                        if (item["ta_brand3_bill_val"] != DBNull.Value) reportEntity.TotBrand3BillVal = Convert.ToInt32(item["ta_brand3_bill_val"]);
                        if (item["ta_brand3_outlet_count"] != DBNull.Value) reportEntity.TotBrand3OutletCount = Convert.ToInt32(item["ta_brand3_outlet_count"]);
                        if (item["ta_brand4_bill_count"] != DBNull.Value) reportEntity.TotBrand4BillCount = Convert.ToInt32(item["ta_brand4_bill_count"]);
                        if (item["ta_brand4_bill_val"] != DBNull.Value) reportEntity.TotBrand4BillVal = Convert.ToInt32(item["ta_brand4_bill_val"]);
                        if (item["ta_brand4_outlet_count"] != DBNull.Value) reportEntity.TotBrand4OutletCount = Convert.ToInt32(item["ta_brand4_outlet_count"]);
                        if (item["ta_brand5_bill_count"] != DBNull.Value) reportEntity.TotBrand5BillCount = Convert.ToInt32(item["ta_brand5_bill_count"]);
                        if (item["ta_brand5_bill_val"] != DBNull.Value) reportEntity.TotBrand5BillVal = Convert.ToInt32(item["ta_brand5_bill_val"]);
                        if (item["ta_brand5_outlet_count"] != DBNull.Value) reportEntity.TotBrand5OutletCount = Convert.ToInt32(item["ta_brand5_outlet_count"]);
                        if (item["ta_multi_lines_bills"] != DBNull.Value) reportEntity.TotMultiLineBill = Convert.ToInt32(item["ta_multi_lines_bills"]);

                        if (item["ta_ready_stock_mandays"] != DBNull.Value) reportEntity.TotReadyStockMandays = Convert.ToInt32(item["ta_ready_stock_mandays"]);
                        if (item["ta_pre_invoice_mandays"] != DBNull.Value) reportEntity.TotPreInvMandays = Convert.ToInt32(item["ta_pre_invoice_mandays"]);
                        if (item["ta_eco"] != DBNull.Value) reportEntity.TotECO = Convert.ToInt32(item["ta_eco"]);
                        if (item["ta_no_sales_account"] != DBNull.Value) reportEntity.TotNoSalesAcc = Convert.ToInt32(item["ta_no_sales_account"]);
                        if (item["ta_non_visit_outlets"] != DBNull.Value) reportEntity.TotNonVisitedOutlets = Convert.ToInt32(item["ta_non_visit_outlets"]);
                        if (item["ta_mss_complience"] != DBNull.Value) reportEntity.TotMSSComplience = Convert.ToInt32(item["ta_mss_complience"]);

                        if (item["avg_ready_stock_val"] != DBNull.Value) reportEntity.AvgReadyStockVal = Convert.ToInt32(item["avg_ready_stock_val"]);
                        if (item["avg_pre_invoice_val"] != DBNull.Value) reportEntity.AvgPreInvoiceVal = Convert.ToInt32(item["avg_pre_invoice_val"]);

                        if (item["avg_ready_stock_pc"] != DBNull.Value) reportEntity.AvgReadyStockPc = Convert.ToInt32(item["avg_ready_stock_pc"]);
                        if (item["avg_pre_invoice_pc"] != DBNull.Value) reportEntity.AvgPreInvoicePc = Convert.ToInt32(item["avg_pre_invoice_pc"]);
                        if (item["avg_bill_count_1500"] != DBNull.Value) reportEntity.AvgBillCount = Convert.ToInt32(item["avg_bill_count_1500"]);
                        if (item["avg_bill_value_1500"] != DBNull.Value) reportEntity.AvgBillValue = Convert.ToInt32(item["avg_bill_value_1500"]);
                        if (item["avg_wh_sale_bill_count"] != DBNull.Value) reportEntity.AvgWhSalesBillCount = Convert.ToInt32(item["avg_wh_sale_bill_count"]);
                        if (item["avg_brand1_bill_count"] != DBNull.Value) reportEntity.AvgBrand1BillCount = Convert.ToInt32(item["avg_brand1_bill_count"]);
                        if (item["avg_brand1_bill_val"] != DBNull.Value) reportEntity.AvgBrand1BillVal = Convert.ToInt32(item["avg_brand1_bill_val"]);
                        if (item["avg_brand1_outlet_count"] != DBNull.Value) reportEntity.AvgBrand1OutletCount = Convert.ToInt32(item["avg_brand1_outlet_count"]);
                        if (item["avg_brand2_bill_count"] != DBNull.Value) reportEntity.AvgBrand2BillCount = Convert.ToInt32(item["avg_brand2_bill_count"]);
                        if (item["avg_brand2_bill_val"] != DBNull.Value) reportEntity.AvgBrand2BillVal = Convert.ToInt32(item["avg_brand2_bill_val"]);
                        if (item["avg_brand2_outlet_count"] != DBNull.Value) reportEntity.AvgBrand2OutletCount = Convert.ToInt32(item["avg_brand2_outlet_count"]);
                        if (item["avg_brand3_bill_count"] != DBNull.Value) reportEntity.AvgBrand3BillCount = Convert.ToInt32(item["avg_brand3_bill_count"]);
                        if (item["avg_brand3_bill_val"] != DBNull.Value) reportEntity.AvgBrand3BillVal = Convert.ToInt32(item["avg_brand3_bill_val"]);
                        if (item["avg_brand3_outlet_count"] != DBNull.Value) reportEntity.AvgBrand3OutletCount = Convert.ToInt32(item["avg_brand3_outlet_count"]);
                        if (item["avg_brand4_bill_count"] != DBNull.Value) reportEntity.AvgBrand4BillCount = Convert.ToInt32(item["avg_brand4_bill_count"]);
                        if (item["avg_brand4_bill_val"] != DBNull.Value) reportEntity.AvgBrand4BillVal = Convert.ToInt32(item["avg_brand4_bill_val"]);
                        if (item["avg_brand4_outlet_count"] != DBNull.Value) reportEntity.AvgBrand4OutletCount = Convert.ToInt32(item["avg_brand4_outlet_count"]);
                        if (item["avg_brand5_bill_count"] != DBNull.Value) reportEntity.AvgBrand5BillCount = Convert.ToInt32(item["avg_brand5_bill_count"]);
                        if (item["avg_brand5_bill_val"] != DBNull.Value) reportEntity.AvgBrand5BillVal = Convert.ToInt32(item["avg_brand5_bill_val"]);
                        if (item["avg_brand5_outlet_count"] != DBNull.Value) reportEntity.AvgBrand5OutletCount = Convert.ToInt32(item["avg_brand5_outlet_count"]);
                        if (item["avg_multi_lines_bills"] != DBNull.Value) reportEntity.AvgMultiLineBill = Convert.ToInt32(item["avg_multi_lines_bills"]);
                        if (item["avg_ready_stock_mandays"] != DBNull.Value) reportEntity.AvgReadyStockMandays = Convert.ToInt32(item["avg_ready_stock_mandays"]);
                        if (item["avg_pre_invoice_mandays"] != DBNull.Value) reportEntity.AvgPreInvMandays = Convert.ToInt32(item["avg_pre_invoice_mandays"]);
                        if (item["avg_eco"] != DBNull.Value) reportEntity.AvgECO = Convert.ToInt32(item["avg_eco"]);
                        if (item["avg_no_sales_account"] != DBNull.Value) reportEntity.AvgNoSalesAcc = Convert.ToInt32(item["avg_no_sales_account"]);
                        if (item["avg_non_visit_outlets"] != DBNull.Value) reportEntity.AvgNonVisitedOutlets = Convert.ToInt32(item["avg_non_visit_outlets"]);
                        if (item["avg_mss_complience"] != DBNull.Value) reportEntity.AvgMSSComplience = Convert.ToInt32(item["avg_mss_complience"]);

                        if (item["created_by"] != DBNull.Value) reportEntity.CreatedBy = item["created_by"].ToString();
                        if (item["total_count"] != DBNull.Value) reportEntity.TotalCount = Convert.ToInt32(item["total_count"]);
                        reportList.Add(reportEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return reportList;
        }

        public bool UploadPayments(string FilePath, string UserName, string key, DateTime targetDate)
        {
            try
            {
                bool status = false;
                DataTable dt = new DataTable();
                DataTable objDS = new DataTable();

                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(FilePath, false))
                {
                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;
                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    IEnumerable<Row> rows = sheetData.Descendants<Row>();

                    foreach (Cell cell in rows.ElementAt(0))
                    {
                        dt.Columns.Add(GetCellValue(spreadSheetDocument, cell).Trim());
                    }


                    foreach (Row row in rows)
                    {
                        DataRow tempRow = dt.NewRow();
                        try
                        {
                            int columnIndex = 0;
                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
                                cellColumnIndex--;
                                if (columnIndex < cellColumnIndex)
                                {
                                    do
                                    {
                                        tempRow[columnIndex] = "";
                                        columnIndex++;
                                    }
                                    while (columnIndex < cellColumnIndex);
                                }
                                tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);

                                columnIndex++;
                            }

                            if (tempRow[0].ToString() != "")
                                dt.Rows.Add(tempRow);
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }

                dt.Rows.RemoveAt(0);
                dt.AcceptChanges();
                // Add columns
                dt.Columns.Add(new DataColumn("UserID", typeof(System.String)));
                dt.Columns.Add(new DataColumn("AreaId", typeof(System.String)));
                dt.Columns.Add(new DataColumn("KPIDate", typeof(System.DateTime)));

                // Assign values
                foreach (DataRow dr in dt.Rows)
                {
                    dr["UserID"] = UserName;

                    if (!string.IsNullOrEmpty(dr["Area"].ToString()))
                    {
                        ////If AllIsland(total) row then areanId =0
                        //if (key == dr["Area"].ToString())
                        //{
                        //    dr["AreaId"] = 0;
                        //}
                        //else
                        //{
                            dr["AreaId"] = GetAreaIdFromName(dr["Area"].ToString());
                        //}
                    }
                    dr["KPIDate"] = targetDate;
                }

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                if (dt.Rows.Count > 0)
                {
                    using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                    {
                        SqlCommand objCommand = new SqlCommand();
                        objCommand.Connection = conn;
                        objCommand.CommandType = CommandType.StoredProcedure;
                        objCommand.CommandText = "spUploadKPIReport";

                        objCommand.Parameters.AddWithValue("@tblKPIReport", dt);

                        //Get list of data which alredy exist in the table
                        SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                        objAdap.Fill(objDS);
                    }
                    status = true;
                }

                return status;
            }
            catch (Exception)
            {

                throw;
            }
        }
        
        public bool UploadKPIReportTargetWeekly(string FilePath, string UserName, string key, DateTime targetDate)
        {
            try
            {
                bool status = false;
                DataTable dt = new DataTable();
                DataTable objDS = new DataTable();

                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(FilePath, false))
                {
                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;
                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    IEnumerable<Row> rows = sheetData.Descendants<Row>();

                    foreach (Cell cell in rows.ElementAt(0))
                    {
                        dt.Columns.Add(GetCellValue(spreadSheetDocument, cell).Trim());
                    }

                    foreach (Row row in rows)
                    {
                        DataRow tempRow = dt.NewRow();
                        try
                        {
                            int columnIndex = 0;
                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
                                cellColumnIndex--;
                                if (columnIndex < cellColumnIndex)
                                {
                                    do
                                    {
                                        tempRow[columnIndex] = "";
                                        columnIndex++;
                                    }
                                    while (columnIndex < cellColumnIndex);
                                }
                                tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);

                                columnIndex++;
                            }

                            if (tempRow[0].ToString() != "")
                                dt.Rows.Add(tempRow);
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }

                dt.Rows.RemoveAt(0);
                dt.AcceptChanges();

                // Add columns
                dt.Columns.Add(new DataColumn("UserID", typeof(System.String)));
                dt.Columns.Add(new DataColumn("WeekId", typeof(System.String)));
                dt.Columns.Add(new DataColumn("KPIDate", typeof(System.DateTime)));

                // Assign values
                foreach (DataRow dr in dt.Rows)
                {
                    dr["UserID"] = UserName;

                    if (!string.IsNullOrEmpty(dr["Week"].ToString()))
                    {
                        ////If AllIsland(total) row then areanId =0
                        //if (key == dr["Area"].ToString())
                        //{
                        //    dr["AreaId"] = 0;
                        //}
                        //else
                        //{
                        dr["WeekId"] = GetWeekIdFromName(dr["Week"].ToString());
                        //}
                    }

                    dr["KPIDate"] = targetDate;
                }

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                if (dt.Rows.Count > 0)
                {
                    using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                    {
                        SqlCommand objCommand = new SqlCommand();
                        objCommand.Connection = conn;
                        objCommand.CommandType = CommandType.StoredProcedure;
                        objCommand.CommandText = "spUploadKPIReportTargetWeekly";

                        objCommand.Parameters.AddWithValue("@tblKPIReport", dt);

                        //Get list of data which alredy exist in the table
                        SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                        objAdap.Fill(objDS);
                    }
                    status = true;
                }

                return status;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public int GetAreaIdFromName(string areaName)
        {
            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetAreaId";

                    objCommand.Parameters.AddWithValue("@areaName", areaName);

                    var returnParameter = objCommand.Parameters.Add("@ReturnVal", SqlDbType.VarChar, 20);
                    returnParameter.Direction = ParameterDirection.Output;

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    objCommand.ExecuteNonQuery();
                    int rtn = Convert.ToInt32(objCommand.Parameters["@ReturnVal"].Value);

                    return rtn;
                }
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public int GetWeekIdFromName(string weekName)
        {
            int weekId = 0;
            switch (weekName)
            {
                case "Week 1":
                    weekId = 1;
                    break;
                case "Week 2":
                    weekId = 2;
                    break;
                case "Week 3":
                    weekId = 3;
                    break;
                case "Week 4":
                    weekId = 4;
                    break;
                case "Week 5":
                    weekId = 5;
                    break;
                default:
                    weekId = 0;
                    break;
            }

            return weekId;
        }

        #endregion

        #region KPIReportSettings - Brands

        public bool UpdateKPIReportBrands(KPIReportSettingsModel brandsDetail)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spUpdateKPIBrands";

                    objCommand.Parameters.AddWithValue("@focus_brand1_id", brandsDetail.Brand1);
                    objCommand.Parameters.AddWithValue("@focus_brand2_id", brandsDetail.Brand2);
                    objCommand.Parameters.AddWithValue("@focus_brand3_id", brandsDetail.Brand3);
                    objCommand.Parameters.AddWithValue("@focus_brand4_id", brandsDetail.Brand4);
                    objCommand.Parameters.AddWithValue("@focus_brand5_id", brandsDetail.Brand5);
                    objCommand.Parameters.AddWithValue("@created_by", brandsDetail.CreatedBy);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        public bool UpdateKPIEmailRecipients(string emailRecipints)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spUpdateKPIEmailRecipients";

                    objCommand.Parameters.AddWithValue("@EmailRecipints", emailRecipints);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;
        }

        public List<KPIReportSettingsModel> GetKPIReportBrands(ArgsModel args)
        {
            DataTable objDS = new DataTable();
            List<KPIReportSettingsModel> brandList = new List<KPIReportSettingsModel>();
            KPIReportSettingsModel brandsEntity = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetKPIReportBrands";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        brandsEntity = new KPIReportSettingsModel();

                        if (item["id"] != DBNull.Value) brandsEntity.Id = Convert.ToInt32(item["id"]);
                        if (item["focus_brand1_id"] != DBNull.Value) brandsEntity.Brand1 = Convert.ToInt32(item["focus_brand1_id"]);
                        if (item["focus_brand2_id"] != DBNull.Value) brandsEntity.Brand2 = Convert.ToInt32(item["focus_brand2_id"]);
                        if (item["focus_brand3_id"] != DBNull.Value) brandsEntity.Brand3 = Convert.ToInt32(item["focus_brand3_id"]);
                        if (item["focus_brand4_id"] != DBNull.Value) brandsEntity.Brand4 = Convert.ToInt32(item["focus_brand4_id"]);
                        if (item["focus_brand5_id"] != DBNull.Value) brandsEntity.Brand5 = Convert.ToInt32(item["focus_brand5_id"]);

                        if (item["created_date"] != DBNull.Value) brandsEntity.CreatedDate = Convert.ToDateTime(item["created_date"].ToString());
                        if (item["created_by"] != DBNull.Value) brandsEntity.CreatedBy = item["created_by"].ToString();
                        if (item["total_count"] != DBNull.Value) brandsEntity.TotalCount = Convert.ToInt32(item["total_count"]);
                        brandList.Add(brandsEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return brandList;
        }

        public List<KPIReportBrandItemModel> GetKPIBrandItemsByBrandId(ArgsModel args, int kpiBrandId)
        {
            DataTable objDS = new DataTable();
            List<KPIReportBrandItemModel> brandList = new List<KPIReportBrandItemModel>();
            KPIReportBrandItemModel brandsEntity = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetKPIBrandItemsByBrandId";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@KpiBrandId", kpiBrandId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        brandsEntity = new KPIReportBrandItemModel();

						if (item["kpi_brand_id"] != DBNull.Value) brandsEntity.KpiBrandId = Convert.ToInt32(item["kpi_brand_id"]);
						if (item["product_id"] != DBNull.Value) brandsEntity.ProductId = Convert.ToInt32(item["product_id"]);
						if (item["fg_code"] != DBNull.Value) brandsEntity.ProductFgsCode = item["fg_code"].ToString();
						if (item["product_code"] != DBNull.Value) brandsEntity.ProductCode = item["product_code"].ToString();
						if (item["product_name"] != DBNull.Value) brandsEntity.ProductName = item["product_name"].ToString();
						if (item["brand_id"] != DBNull.Value) brandsEntity.BrandId = Convert.ToInt32(item["brand_id"]);
                        if (item["brand_name"] != DBNull.Value) brandsEntity.BrandName = item["brand_name"].ToString();
                        if (item["category_id"] != DBNull.Value) brandsEntity.CategoryId = Convert.ToInt32(item["category_id"]);
                        if (item["category_name"] != DBNull.Value) brandsEntity.CategoryName = item["category_name"].ToString();
                        if (item["flavor_id"] != DBNull.Value) brandsEntity.FlavourId = Convert.ToInt32(item["flavor_id"]);
                        if (item["flavor_name"] != DBNull.Value) brandsEntity.FlavourName = item["flavor_name"].ToString();
                        if (item["packing_id"] != DBNull.Value) brandsEntity.PackingId = Convert.ToInt32(item["packing_id"]);
                        if (item["packing_name"] != DBNull.Value) brandsEntity.PackingName = item["packing_name"].ToString();
                        if (item["is_high_value"] != DBNull.Value) brandsEntity.IsHighValue = Convert.ToBoolean(item["is_high_value"]);
                        if (item["is_assigned"] != DBNull.Value) brandsEntity.IsAssigned = Convert.ToBoolean(item["is_assigned"]);
						if (item["total_count"] != DBNull.Value) brandsEntity.TotalCount = Convert.ToInt32(item["total_count"]);

						brandList.Add(brandsEntity);
                    }
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return brandList;
        }

        public bool AssignKPIBrandItems(string originator,
                                        string kpi_brand_id,
                                        string product_id,
                                        string is_checked)
        { 
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateAssignKPIBrandItems";

                    objCommand.Parameters.AddWithValue("@originator", originator);
                    objCommand.Parameters.AddWithValue("@kpi_brand_id", kpi_brand_id);
                    objCommand.Parameters.AddWithValue("@product_id", product_id);
                    objCommand.Parameters.AddWithValue("@is_checked", is_checked);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;
        }

        public bool UpdateKPIBrandDisplayName(string kpi_brand_id, string displayName)
        { 
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateKPIBrandDisplayName";

                    objCommand.Parameters.AddWithValue("@kpi_brand_id", kpi_brand_id);
                    objCommand.Parameters.AddWithValue("@displayName", displayName);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;
        }

        public KPIReportBrandsModel GetKPIReportBrandByBrandId(string kpi_brand_id)
        {
            DataTable objDS = new DataTable();
            KPIReportBrandsModel brandsEntity = new KPIReportBrandsModel();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetKPIReportBrandByBrandId";

                    objCommand.Parameters.AddWithValue("@KpiBrandId", kpi_brand_id);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    if (objDS.Rows[0]["kpi_brand_id"] != DBNull.Value) brandsEntity.BrandId = Convert.ToInt32(objDS.Rows[0]["kpi_brand_id"]);
                    if (objDS.Rows[0]["kpi_brand_name"] != DBNull.Value) brandsEntity.BrandName = objDS.Rows[0]["kpi_brand_name"].ToString();
                    if (objDS.Rows[0]["kpi_brand_display_name"] != DBNull.Value) brandsEntity.BrandDisplayName = objDS.Rows[0]["kpi_brand_display_name"].ToString();
                }
            }
            catch (Exception ex)
            {

                throw;
            }

            return brandsEntity;
        }

        public List<KPIReportSettingsModel> KPIReportBrands()
        {
            DataTable objDS = new DataTable();
            List<KPIReportSettingsModel> brandList = new List<KPIReportSettingsModel>();
            KPIReportSettingsModel brandsEntity = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spKPIReportBrands";

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        brandsEntity = new KPIReportSettingsModel();

                        if (item["id"] != DBNull.Value) brandsEntity.Id = Convert.ToInt32(item["id"]);
                        if (item["focus_brand1_id"] != DBNull.Value) brandsEntity.Brand1 = Convert.ToInt32(item["focus_brand1_id"]);
                        if (item["focus_brand2_id"] != DBNull.Value) brandsEntity.Brand2 = Convert.ToInt32(item["focus_brand2_id"]);
                        if (item["focus_brand3_id"] != DBNull.Value) brandsEntity.Brand3 = Convert.ToInt32(item["focus_brand3_id"]);
                        if (item["focus_brand4_id"] != DBNull.Value) brandsEntity.Brand4 = Convert.ToInt32(item["focus_brand4_id"]);
                        if (item["focus_brand5_id"] != DBNull.Value) brandsEntity.Brand5 = Convert.ToInt32(item["focus_brand5_id"]);
                        if (item["created_date"] != DBNull.Value) brandsEntity.CreatedDate = Convert.ToDateTime(item["created_date"].ToString());
                        if (item["created_by"] != DBNull.Value) brandsEntity.CreatedBy = item["created_by"].ToString();
                        brandList.Add(brandsEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return brandList;
        }

        public List<KPIReportBrandsModel> GetAllKPIReportsBrands()
        {
            DataTable objDS = new DataTable();
            List<KPIReportBrandsModel> brandList = new List<KPIReportBrandsModel>();
            KPIReportBrandsModel brandsEntity = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllReportsKPIBrands";

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        brandsEntity = new KPIReportBrandsModel();

						if (item["kpi_brand_id"] != DBNull.Value) brandsEntity.BrandId = Convert.ToInt32(item["kpi_brand_id"]);
						if (item["kpi_brand_name"] != DBNull.Value) brandsEntity.BrandName = item["kpi_brand_name"].ToString();
						if (item["kpi_brand_display_name"] != DBNull.Value) brandsEntity.BrandDisplayName = item["kpi_brand_display_name"].ToString();

						brandList.Add(brandsEntity);
                    }
                }
            }
            catch (Exception)
            {
                brandList = new List<KPIReportBrandsModel>();
            }

            return brandList;
        }

        #endregion


        #region - Common Methods -

        //--using openXml
        public static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            string value = cell.CellValue.InnerXml;
            if (cell.CellValue == null)
            {
                return "";
            }
            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }
        }

        public static int? GetColumnIndexFromName(string columnName)
        {
            //return columnIndex;
            string name = columnName;
            int number = 0;
            int pow = 1;
            for (int i = name.Length - 1; i >= 0; i--)
            {
                number += (name[i] - 'A' + 1) * pow;
                pow *= 26;
            }
            return number;
        }

        public static string GetColumnName(string cellReference)
        {
            // Create a regular expression to match the column name portion of the cell name.
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellReference);
            return match.Value;
        }

        #endregion

    }
}
