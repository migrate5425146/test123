﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class GroupRepDAO : BaseDAO
    {
        private DbSqlAdapter GroupRepSql { get; set; }

        private void RegisterSql()
        {
            this.GroupRepSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.GroupRepSql.xml", ApplicationService.Instance.DbProvider);
        }

        public GroupRepDAO()
        {
            RegisterSql();
        }

        public GroupRepDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public GroupRepEntity CreateObject()
        {
            return GroupRepEntity.CreateObject();
        }

        public GroupRepEntity CreateObject(int entityId)
        {
            return GroupRepEntity.CreateObject(entityId);
        }


        public List<GroupRepEntity> GetAllGroupsReps(int RepGroupId)
        {
            GroupRepEntity oGroupRep = null;
            DbDataReader idrGroupRep = null;

            try
            {
                List<GroupRepEntity> cGroupRep = new List<GroupRepEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@RepGroupId", RepGroupId, DbType.Int32);

                idrGroupRep = this.DataAcessService.ExecuteQuery(GroupRepSql["GetGroupRepsByGroup"], paramCollection);

                if (idrGroupRep != null && idrGroupRep.HasRows)
                {
                    while (idrGroupRep.Read())
                    {
                        oGroupRep = GroupRepEntity.CreateObject();
                        if (!idrGroupRep.IsDBNull(idrGroupRep.GetOrdinal("rep_group_id"))) oGroupRep.RepGroupId = idrGroupRep.GetInt32(idrGroupRep.GetOrdinal("rep_group_id"));
                        if (!idrGroupRep.IsDBNull(idrGroupRep.GetOrdinal("originator"))) oGroupRep.Originator = idrGroupRep.GetString(idrGroupRep.GetOrdinal("originator"));
                        cGroupRep.Add(oGroupRep);
                    }
                }

                return cGroupRep;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrGroupRep != null)
                    idrGroupRep.Close();
            }
        }
    }
}
