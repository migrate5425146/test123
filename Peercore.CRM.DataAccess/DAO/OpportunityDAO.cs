﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using System.Data.Common;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class OpportunityDAO : BaseDAO
    {
        private DbSqlAdapter OpportunitySql { get; set; }

        private void RegisterSql()
        {
            this.OpportunitySql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.OpportunitySql.xml", ApplicationService.Instance.DbProvider);
        }

        public OpportunityDAO()
        {
            RegisterSql();
        }

        public OpportunityDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public OpportunityEntity CreateObject()
        {
            return OpportunityEntity.CreateObject();
        }

        public OpportunityEntity CreateObject(int entityId)
        {
            return OpportunityEntity.CreateObject(entityId);
        }

        #region Old Methods
        /*
        public OpportunityCompositeEntity CreateObject()
        {
            return OpportunityCompositeEntity.CreateObject();
        }

        public OpportunityCompositeEntity CreateObject(int entityId)
        {
            return OpportunityCompositeEntity.CreateObject(entityId);
        }

        public List<OpportunityEntity> GetOpportunities(Guid leadId, Guid Originator)
        {
            List<OpportunityEntity> lstOpportunity = null;
            DbDataReader dataReader = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@originator_id", Originator.ToString(), DbType.String);

                dataReader = this.DataAcessService.ExecuteQuery(OpportunitySql["GetOpportunitiesByOriginator"], paramCollection);

                if (dataReader.HasRows)
                {
                    lstOpportunity = new List<OpportunityEntity>();

                    int delFlag = dataReader.GetOrdinal("del_flag");
                    int opportunityId = dataReader.GetOrdinal("opportunity_id");
                    int originatorId = dataReader.GetOrdinal("originator_id");
                    int name = dataReader.GetOrdinal("name");
                    int closeDate = dataReader.GetOrdinal("close_date");
                    int pipelineStageId = dataReader.GetOrdinal("pipeline_stage_id");
                    int probability = dataReader.GetOrdinal("probability");
                    int amount = dataReader.GetOrdinal("amount");
                    int description = dataReader.GetOrdinal("description");
                    int createdBy = dataReader.GetOrdinal("created_by");
                    int createdDate = dataReader.GetOrdinal("created_date");
                    int lastModifiedBy = dataReader.GetOrdinal("last_modified_by");
                    int lastModifieDate = dataReader.GetOrdinal("last_modified_date");
                    int units = dataReader.GetOrdinal("units");
                    int pipelineStage = dataReader.GetOrdinal("pipeline_stage");

                    while (dataReader.Read())
                    {
                        OpportunityEntity opportunityEntity = new OpportunityEntity();
                        if (!dataReader.IsDBNull(delFlag)) opportunityEntity.Status = dataReader.GetString(delFlag);
                        if (!dataReader.IsDBNull(opportunityId)) opportunityEntity.OpportunityID = dataReader.GetGuid(opportunityId);
                        if (!dataReader.IsDBNull(originatorId)) opportunityEntity.OriginatorId = dataReader.GetGuid(originatorId);
                        if (!dataReader.IsDBNull(name)) opportunityEntity.Name = dataReader.GetString(name);
                        if (!dataReader.IsDBNull(closeDate)) opportunityEntity.CloseDate = dataReader.GetDateTime(closeDate);
                        if (!dataReader.IsDBNull(pipelineStageId)) opportunityEntity.PipelineStageId = dataReader.GetGuid(pipelineStageId);
                        if (!dataReader.IsDBNull(probability)) opportunityEntity.Probability = dataReader.GetDouble(probability);
                        if (!dataReader.IsDBNull(amount)) opportunityEntity.Amount = dataReader.GetDouble(amount);
                        if (!dataReader.IsDBNull(description)) opportunityEntity.Description = dataReader.GetString(description);
                        if (!dataReader.IsDBNull(createdBy)) opportunityEntity.CreatedBy = dataReader.GetGuid(createdBy);
                        if (!dataReader.IsDBNull(createdDate)) opportunityEntity.CreatedDate = dataReader.GetDateTime(createdDate);
                        if (!dataReader.IsDBNull(lastModifiedBy)) opportunityEntity.LastModifiedBy = dataReader.GetGuid(lastModifiedBy);
                        if (!dataReader.IsDBNull(lastModifieDate)) opportunityEntity.LastModifiedDate = dataReader.GetDateTime(lastModifieDate);
                        if (!dataReader.IsDBNull(units)) opportunityEntity.Units = dataReader.GetDouble(units);
                        if (!dataReader.IsDBNull(pipelineStage)) opportunityEntity.PipelineStage = dataReader.GetString(pipelineStage);

                        opportunityEntity.IsDirty = false;
                        lstOpportunity.Add(opportunityEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
            return lstOpportunity;
        }

        public bool InsertOpportunities(OpportunityEntity opportunity)
        {
            bool saveSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                //paramCollection.Add("@opportunity_id", opportunity.OpportunityID.ToString(), DbType.String);
                paramCollection.Add("@source_id", opportunity.SourceId.ToString(), DbType.String);
                paramCollection.Add("@originator_id", opportunity.OriginatorId.ToString(), DbType.String);
                paramCollection.Add("@name", opportunity.Name, DbType.String);
                paramCollection.Add("@close_date", opportunity.CloseDate, DbType.DateTime);
                paramCollection.Add("@probability", opportunity.Probability, DbType.Double);
                paramCollection.Add("@amount", opportunity.Amount, DbType.Double);
                paramCollection.Add("@description", opportunity.Description, DbType.String);
                paramCollection.Add("@created_by", opportunity.CreatedBy.ToString(), DbType.String);
                paramCollection.Add("@created_date", opportunity.CreatedDate, DbType.DateTime);
                paramCollection.Add("@last_modified_by", opportunity.LastModifiedBy.ToString(), DbType.String);
                paramCollection.Add("@last_modified_date", opportunity.LastModifiedDate, DbType.DateTime);
                paramCollection.Add("@cust_code", opportunity.CustCode, DbType.String);
                paramCollection.Add("@units", opportunity.Units, DbType.Double);
                paramCollection.Add("@rep_group_id", opportunity.RepGroupID.ToString(), DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(OpportunitySql["InsertOpportunity"], paramCollection);

                if (iNoRec > 0)
                    saveSuccessful = true;

            }
            catch (Exception)
            {

                throw;
            }
            return saveSuccessful;
        }

        public bool UpdateOpportunities(OpportunityEntity opportunity)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@opportunity_id", opportunity.OpportunityID.ToString(), DbType.String);
                paramCollection.Add("@name", opportunity.Name, DbType.String);
                paramCollection.Add("@close_date", opportunity.CloseDate, DbType.DateTime);
                paramCollection.Add("@probability", opportunity.Probability, DbType.Double);
                paramCollection.Add("@amount", opportunity.Amount, DbType.Double);
                paramCollection.Add("@description", opportunity.Description, DbType.String);
                paramCollection.Add("@last_modified_by", opportunity.LastModifiedBy.ToString(), DbType.String);
                paramCollection.Add("@last_modified_date", opportunity.LastModifiedDate, DbType.DateTime);
                paramCollection.Add("@cust_code", opportunity.CustCode, DbType.String);
                paramCollection.Add("@units", opportunity.Units, DbType.Double);


                iNoRec = this.DataAcessService.ExecuteNonQuery(OpportunitySql["UpdateOpportunity"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return UpdateSuccessful;
        }

        public OpportunityCompositeEntity GetOpportunity(string OpportunityId)
        {
            DbDataReader dataReader = null;
            OpportunityCompositeEntity opportunityEntity = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@opportunity_id", OpportunityId, DbType.String);

                dataReader = this.DataAcessService.ExecuteQuery(OpportunitySql["GetOpportunityDetails"], paramCollection);

                if (dataReader.HasRows)
                {
                    int delFlag = dataReader.GetOrdinal("del_flag");
                    int opportunityId = dataReader.GetOrdinal("opportunity_id");
                    int originatorId = dataReader.GetOrdinal("originator_id");
                    int name = dataReader.GetOrdinal("name");
                    int closeDate = dataReader.GetOrdinal("close_date");
                    int pipelineStageId = dataReader.GetOrdinal("pipeline_stage_id");
                    int probability = dataReader.GetOrdinal("probability");
                    int amount = dataReader.GetOrdinal("amount");
                    int description = dataReader.GetOrdinal("description");
                    int createdBy = dataReader.GetOrdinal("created_by");
                    int createdDate = dataReader.GetOrdinal("created_date");
                    int lastModifiedBy = dataReader.GetOrdinal("last_modified_by");
                    int lastModifieDate = dataReader.GetOrdinal("last_modified_date");
                    int custcode = dataReader.GetOrdinal("cust_code");
                    int units = dataReader.GetOrdinal("units");
                    int repgroupid = dataReader.GetOrdinal("rep_group_id");
                    int repgroupname = dataReader.GetOrdinal("rep_group_name");

                    while (dataReader.Read())
                    {
                        opportunityEntity = new OpportunityCompositeEntity();

                        if (!dataReader.IsDBNull(delFlag)) opportunityEntity.Status = dataReader.GetString(delFlag);
                        if (!dataReader.IsDBNull(opportunityId)) opportunityEntity.OpportunityID = (Guid)dataReader.GetValue(opportunityId);
                        if (!dataReader.IsDBNull(originatorId)) opportunityEntity.OriginatorId = dataReader.GetGuid(originatorId);
                        if (!dataReader.IsDBNull(name)) opportunityEntity.Name = dataReader.GetString(name);
                        if (!dataReader.IsDBNull(closeDate)) opportunityEntity.CloseDate = dataReader.GetDateTime(closeDate);
                        if (!dataReader.IsDBNull(pipelineStageId)) opportunityEntity.PipelineStageId = dataReader.GetGuid(pipelineStageId);
                        if (!dataReader.IsDBNull(probability)) opportunityEntity.Probability = dataReader.GetDouble(probability);
                        if (!dataReader.IsDBNull(amount)) opportunityEntity.Amount = dataReader.GetDouble(amount);
                        if (!dataReader.IsDBNull(description)) opportunityEntity.Description = dataReader.GetString(description);
                        if (!dataReader.IsDBNull(createdBy)) opportunityEntity.CreatedBy = dataReader.GetString(createdBy);
                        if (!dataReader.IsDBNull(createdDate)) opportunityEntity.CreatedDate = dataReader.GetDateTime(createdDate);
                        if (!dataReader.IsDBNull(lastModifiedBy)) opportunityEntity.LastModifiedBy = dataReader.GetString(lastModifiedBy);
                        if (!dataReader.IsDBNull(lastModifieDate)) opportunityEntity.LastModifiedDate = dataReader.GetDateTime(lastModifieDate);
                        if (!dataReader.IsDBNull(custcode)) opportunityEntity.CustCode = dataReader.GetString(custcode);
                        if (!dataReader.IsDBNull(units)) opportunityEntity.Units = dataReader.GetDouble(units);
                        if (!dataReader.IsDBNull(repgroupid)) opportunityEntity.RepGroupID = dataReader.GetInt32(repgroupid);
                        if (!dataReader.IsDBNull(repgroupname)) opportunityEntity.RepgroupName = dataReader.GetString(repgroupname);

                        opportunityEntity.IsDirty = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
            return opportunityEntity;
        }

        public List<OpportunityCompositeEntity> GetContactOpportunities(int pipelineStageID, string Originator, DateTime dtmCloseDate, string orderby)
        {
            DbDataReader dataReader = null;
            List<OpportunityCompositeEntity> lstopportunity = null;

            string sWhereCls1 = "";
            string sWhereCls2 = "";

            try
            {
                if (!Originator.Equals(""))
                {
                    sWhereCls1 = sWhereCls2 = "(originator = '" + UserSession.Instance.UserName + "' OR originator IN(" + Originator + "))";
                    sWhereCls1 = sWhereCls2 = "( " + sWhereCls1.Replace("originator", "co.originator");
                }
                sWhereCls1 = sWhereCls2 += " )";

                if (pipelineStageID > 0)
                {
                    sWhereCls1 = sWhereCls1 != "" ? sWhereCls1 + " AND pipeline_stage_id = " + pipelineStageID.ToString() : " pipeline_stage_id = " + pipelineStageID.ToString();
                    sWhereCls2 = sWhereCls2 != "" ? sWhereCls2 + " AND pipeline_stage_id = " + pipelineStageID.ToString() : " pipeline_stage_id = " + pipelineStageID.ToString();
                }

                if (!dtmCloseDate.Equals(null))
                {
                    sWhereCls1 = sWhereCls1 != "" ? sWhereCls1 + " AND " : sWhereCls1;
                    sWhereCls2 = sWhereCls2 != "" ? sWhereCls2 + " AND " : sWhereCls2;

                    //sWhereCls1 += " close_date >= '" + Convert.ToDateTime(dtmCloseDate).ToString("yyyy-MM-dd")
                    //    + "' AND close_date <= '" + Convert.ToDateTime(dtmCloseDate).AddDays(7).ToString("yyyy-MM-dd") + "' ";
                    //sWhereCls2 += " close_date >= '" + Convert.ToDateTime(dtmCloseDate).ToString("yyyy-MM-dd")
                    //    + "' AND close_date <= '" + Convert.ToDateTime(dtmCloseDate).AddDays(7).ToString("yyyy-MM-dd") + "' ";

                    sWhereCls1 += " close_date >= '" + Convert.ToDateTime(dtmCloseDate).ToString("yyyy-MM-dd") + "' ";
                    sWhereCls2 += " close_date >= '" + Convert.ToDateTime(dtmCloseDate).ToString("yyyy-MM-dd") + "' ";
                }

                if (!string.IsNullOrWhiteSpace(sWhereCls1))
                    sWhereCls1 = sWhereCls1.Insert(0, " AND ");
                if (!string.IsNullOrWhiteSpace(sWhereCls2))
                    sWhereCls2 = sWhereCls2.Insert(0, " AND ");

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@default_dept_id", Convert.ToString(UserSession.Instance.DefaultDepartmentId), DbType.String);

                dataReader = this.DataAcessService.ExecuteQuery(OpportunitySql["GetContactOpportunities"].Format(sWhereCls1, sWhereCls2, orderby), paramCollection);

                if (dataReader.HasRows)
                {
                    lstopportunity = new List<OpportunityCompositeEntity>();

                    int opportunityId = dataReader.GetOrdinal("opportunity_id");
                    int leadId = dataReader.GetOrdinal("lead_id");
                    int originator = dataReader.GetOrdinal("Originator");
                    int name = dataReader.GetOrdinal("name");
                    int closeDate = dataReader.GetOrdinal("close_date");
                    int Stage = dataReader.GetOrdinal("pipeline_stage_id");
                    int probability = dataReader.GetOrdinal("probability");
                    int amount = dataReader.GetOrdinal("amount");
                    int units = dataReader.GetOrdinal("units");
                    int LeadName = dataReader.GetOrdinal("ContactName");
                    int description = dataReader.GetOrdinal("description");
                    int Business = dataReader.GetOrdinal("business");
                    int Industry = dataReader.GetOrdinal("industry");
                    int Address = dataReader.GetOrdinal("address");
                    int City = dataReader.GetOrdinal("city");
                    int State = dataReader.GetOrdinal("state");
                    int PostCode = dataReader.GetOrdinal("postcode");
                    int LeadStage = dataReader.GetOrdinal("lead_stage");
                    int IndustryDescription = dataReader.GetOrdinal("industry_description");
                    int CustCode = dataReader.GetOrdinal("cust_code");
                    int Status = dataReader.GetOrdinal("state");


                    while (dataReader.Read())
                    {
                        OpportunityCompositeEntity opportunityComposite = new OpportunityCompositeEntity();

                        if (!dataReader.IsDBNull(opportunityId)) opportunityComposite.OpportunityID = dataReader.GetGuid(opportunityId);
                        if (!dataReader.IsDBNull(leadId)) opportunityComposite.LeadID = dataReader.GetGuid(leadId);
                        if (!dataReader.IsDBNull(originator)) opportunityComposite.Originator = dataReader.GetString(originator);
                        if (!dataReader.IsDBNull(name)) opportunityComposite.Name = dataReader.GetString(name);
                        if (!dataReader.IsDBNull(closeDate)) opportunityComposite.CloseDate = dataReader.GetDateTime(closeDate);
                        if (!dataReader.IsDBNull(Stage)) opportunityComposite.PipelineStageId = dataReader.GetGuid(Stage);
                        if (!dataReader.IsDBNull(probability)) opportunityComposite.Probability = dataReader.GetDouble(probability);
                        if (!dataReader.IsDBNull(amount)) opportunityComposite.Amount = dataReader.GetDouble(amount);
                        if (!dataReader.IsDBNull(units)) opportunityComposite.Units = dataReader.GetDouble(units);
                        if (!dataReader.IsDBNull(LeadName)) opportunityComposite.Name = dataReader.GetString(LeadName);
                        if (!dataReader.IsDBNull(description)) opportunityComposite.Description = dataReader.GetString(description);
                        if (!dataReader.IsDBNull(Business)) opportunityComposite.Business = dataReader.GetString(Business);
                        if (!dataReader.IsDBNull(Industry)) opportunityComposite.Industry = dataReader.GetString(Industry);
                        if (!dataReader.IsDBNull(Address)) opportunityComposite.Address = dataReader.GetString(Address);
                        if (!dataReader.IsDBNull(City)) opportunityComposite.City = dataReader.GetString(City);
                        if (!dataReader.IsDBNull(State)) opportunityComposite.State = dataReader.GetString(State);
                        if (!dataReader.IsDBNull(PostCode)) opportunityComposite.PostCode = dataReader.GetString(PostCode);
                        if (!dataReader.IsDBNull(LeadStage)) opportunityComposite.LeadStage = dataReader.GetString(LeadStage);
                        if (!dataReader.IsDBNull(IndustryDescription)) opportunityComposite.IndustryDescription = dataReader.GetString(IndustryDescription);
                        if (!dataReader.IsDBNull(CustCode)) opportunityComposite.CustCode = dataReader.GetString(CustCode);
                        if (!dataReader.IsDBNull(Status)) opportunityComposite.Status = dataReader.GetString(Status);

                        opportunityComposite.IsDirty = false;
                        lstopportunity.Add(opportunityComposite);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return lstopportunity;
        } 
        */
        #endregion

        #region New Methods
        public List<OpportunityEntity> GetOpportunities(ArgsEntity args, int contactID)
        {
            DbDataReader drOpportunity = null;
            try
            {
                List<OpportunityEntity> opportunityCollection = new List<OpportunityEntity>();

                //Comment by Indunil,11/01/2013,Show all the opportunity.
                //sWhereCls = childOriginators;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@leadID", contactID, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                drOpportunity = this.DataAcessService.ExecuteQuery(OpportunitySql["GetOpportunitiesByOriginator"], paramCollection);


                if (drOpportunity != null && drOpportunity.HasRows)
                {
                    int opportunityIdOrdinal = drOpportunity.GetOrdinal("opportunity_id");
                    int leadIdOrdinal = drOpportunity.GetOrdinal("lead_id");
                    int originatorOrdinal = drOpportunity.GetOrdinal("originator");
                    int nameOrdinal = drOpportunity.GetOrdinal("name");
                    int closeDateOrdinal = drOpportunity.GetOrdinal("close_date");
                    int pipelineStageIdOrdinal = drOpportunity.GetOrdinal("pipeline_stage_id");
                    int probabilityOrdinal = drOpportunity.GetOrdinal("probability");
                    int amountOrdinal = drOpportunity.GetOrdinal("amount");
                    int descriptionOrdinal = drOpportunity.GetOrdinal("description");
                    int createdByOrdinal = drOpportunity.GetOrdinal("created_by");
                    int createdDateOrdinal = drOpportunity.GetOrdinal("created_date");
                    int lastModifiedByOrdinal = drOpportunity.GetOrdinal("last_modified_by");
                    int lastModifiedDateOrdinal = drOpportunity.GetOrdinal("last_modified_date");
                    int unitsOrdinal = drOpportunity.GetOrdinal("units");
                    int pipelineStageOrdinal = drOpportunity.GetOrdinal("pipeline_stage");
                    int totalCountOrdinal = drOpportunity.GetOrdinal("total_count");

                    while (drOpportunity.Read())
                    {
                        OpportunityEntity opportunity = CreateObject();

                        if (!drOpportunity.IsDBNull(opportunityIdOrdinal)) opportunity.OpportunityID = drOpportunity.GetInt32(opportunityIdOrdinal);
                        if (!drOpportunity.IsDBNull(leadIdOrdinal)) opportunity.LeadID = drOpportunity.GetInt32(leadIdOrdinal);
                        if (!drOpportunity.IsDBNull(originatorOrdinal)) opportunity.Originator = drOpportunity.GetString(originatorOrdinal);
                        if (!drOpportunity.IsDBNull(nameOrdinal)) opportunity.Name = drOpportunity.GetString(nameOrdinal);
                        if (!drOpportunity.IsDBNull(closeDateOrdinal)) opportunity.CloseDate = drOpportunity.GetDateTime(closeDateOrdinal).ToLocalTime();
                        if (!drOpportunity.IsDBNull(pipelineStageIdOrdinal)) opportunity.Stage = drOpportunity.GetInt32(pipelineStageIdOrdinal);
                        if (!drOpportunity.IsDBNull(probabilityOrdinal)) opportunity.Probability = drOpportunity.GetDouble(probabilityOrdinal);
                        if (!drOpportunity.IsDBNull(amountOrdinal)) opportunity.Amount = drOpportunity.GetDouble(amountOrdinal);
                        if (!drOpportunity.IsDBNull(unitsOrdinal)) opportunity.Units = drOpportunity.GetDouble(unitsOrdinal);
                        if (!drOpportunity.IsDBNull(descriptionOrdinal)) opportunity.Description = drOpportunity.GetString(descriptionOrdinal);
                        if (!drOpportunity.IsDBNull(createdByOrdinal)) opportunity.CreatedBy = drOpportunity.GetString(createdByOrdinal);
                        if (!drOpportunity.IsDBNull(createdDateOrdinal)) opportunity.CreatedDate = drOpportunity.GetDateTime(createdDateOrdinal);
                        if (!drOpportunity.IsDBNull(lastModifiedByOrdinal)) opportunity.LastModifiedBy = drOpportunity.GetString(lastModifiedByOrdinal);
                        if (!drOpportunity.IsDBNull(lastModifiedDateOrdinal)) opportunity.LastModifiedDate = drOpportunity.GetDateTime(lastModifiedDateOrdinal);
                        if (!drOpportunity.IsDBNull(pipelineStageOrdinal)) opportunity.PipelineStage = drOpportunity.GetString(pipelineStageOrdinal);
                        if (!drOpportunity.IsDBNull(totalCountOrdinal)) opportunity.TotalCount = drOpportunity.GetInt32(totalCountOrdinal);

                        opportunityCollection.Add(opportunity);

                    }
                }

                return opportunityCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOpportunity != null)
                    drOpportunity.Close();
            }
        }

        public bool Insert(OpportunityEntity opportunity, out int newopportunity)
        {
            bool saveSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                opportunity.OpportunityID = Convert.ToInt32(DataAcessService.GetOneValue(OpportunitySql["GetNextOpportunityID"]));   // Get Next ID
                newopportunity = opportunity.OpportunityID;

                paramCollection.Add("@OpportunityID", opportunity.OpportunityID, DbType.Int32);
                paramCollection.Add("@LeadID", opportunity.LeadID, DbType.Int32);
                paramCollection.Add("@Originator", opportunity.Originator, DbType.String);
                paramCollection.Add("@Name", opportunity.Name, DbType.String);
                paramCollection.Add("@CloseDate", opportunity.CloseDate.ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                paramCollection.Add("@Stage", opportunity.Stage, DbType.Int32);
                paramCollection.Add("@Probability", opportunity.Probability, DbType.Double);
                paramCollection.Add("@Amount", opportunity.Amount, DbType.Double);
                paramCollection.Add("@Description", string.IsNullOrEmpty(opportunity.Description)? "": opportunity.Description, DbType.String);
                paramCollection.Add("@LastModifiedBy", opportunity.LastModifiedBy.ToString(), DbType.String);
                if (opportunity.LastModifiedDate.HasValue)
                paramCollection.Add("@LastModifiedDate", opportunity.LastModifiedDate.Value.ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                paramCollection.Add("@CustCode", string.IsNullOrEmpty(opportunity.CustCode) ? "" : opportunity.CustCode, DbType.String);
                paramCollection.Add("@Units", opportunity.Units, DbType.Double);
                paramCollection.Add("@RepGroupID", opportunity.RepGroupID.ToString(), DbType.Int32);
                paramCollection.Add("@EndUserCode", string.IsNullOrEmpty(opportunity.EndUserCode) ? "" : opportunity.EndUserCode, DbType.String);
                paramCollection.Add("@Tonnes", opportunity.Tonnes, DbType.Double);
                    
                iNoRec = this.DataAcessService.ExecuteNonQuery(OpportunitySql["InsertOpportunity"], paramCollection);

                if (iNoRec > 0)
                    saveSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return saveSuccessful;
        }

        public bool Update(OpportunityEntity opportunity)
        {
            bool saveSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@OpportunityID", opportunity.OpportunityID, DbType.Int32);
                paramCollection.Add("@LeadID", opportunity.LeadID, DbType.Int32);
                paramCollection.Add("@Originator", opportunity.Originator, DbType.String);
                paramCollection.Add("@Name", opportunity.Name, DbType.String);
                paramCollection.Add("@CloseDate", opportunity.CloseDate.ToString("dd-MMM-yyyy HH:mm:ss"), DbType.DateTime);
                paramCollection.Add("@Stage", opportunity.Stage, DbType.Int32);
                paramCollection.Add("@Probability", opportunity.Probability, DbType.Double);
                paramCollection.Add("@Amount", opportunity.Amount, DbType.Double);
                paramCollection.Add("@Description", string.IsNullOrEmpty(opportunity.Description) ? "" : opportunity.Description, DbType.String);
                paramCollection.Add("@LastModifiedBy", opportunity.LastModifiedBy.ToString(), DbType.String);
                if(opportunity.LastModifiedDate.HasValue)
                paramCollection.Add("@LastModifiedDate", opportunity.LastModifiedDate.Value.ToString("dd-MMM-yyyy HH:mm:ss"), DbType.DateTime);
                paramCollection.Add("@CustCode", string.IsNullOrEmpty(opportunity.CustCode) ? "" : opportunity.CustCode, DbType.String);
                paramCollection.Add("@Units", opportunity.Units, DbType.Double);
                paramCollection.Add("@RepGroupID", opportunity.RepGroupID.ToString(), DbType.Int32);
                paramCollection.Add("@EndUserCode", string.IsNullOrEmpty(opportunity.EndUserCode) ? "" : opportunity.EndUserCode, DbType.String);
                paramCollection.Add("@Tonnes", opportunity.Tonnes, DbType.Double);

                iNoRec = this.DataAcessService.ExecuteNonQuery(OpportunitySql["UpdateOpportunity"], paramCollection);

                if (iNoRec > 0)
                    saveSuccessful = true;

            }
            catch (Exception)
            {

                throw;
            }
            return saveSuccessful;
        }

        public List<OpportunityEntity> GetOpportunitiesForCustomer(string cutomerCode, ArgsEntity args)
        {
            OpportunityEntity oOpportunity = null;
            DbDataReader idrOpportunity = null;

            try
            {
                List<OpportunityEntity> Opportunities = new List<OpportunityEntity>();
               
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", cutomerCode, DbType.String);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);

                idrOpportunity = this.DataAcessService.ExecuteQuery(OpportunitySql["GetOpportunitiesForCustomer"],paramCollection);

                if (idrOpportunity != null)
                {
                    while (idrOpportunity.Read())
                    {
                        oOpportunity = CreateObject();

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("opportunity_id")))
                            oOpportunity.OpportunityID = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("opportunity_id"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("lead_id")))
                            oOpportunity.LeadID = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("lead_id"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("originator")))
                            oOpportunity.Originator = idrOpportunity.GetString(idrOpportunity.GetOrdinal("originator"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("name")))
                            oOpportunity.Name = idrOpportunity.GetString(idrOpportunity.GetOrdinal("name"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("close_date")))
                            oOpportunity.CloseDate = idrOpportunity.GetDateTime(idrOpportunity.GetOrdinal("close_date")).ToLocalTime();
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("pipeline_stage_id")))
                            oOpportunity.Stage = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("pipeline_stage_id"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("probability")))
                            oOpportunity.Probability = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("probability"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("amount")))
                            oOpportunity.Amount = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("amount"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("units")))
                            oOpportunity.Units = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("units"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("description")))
                            oOpportunity.Description = idrOpportunity.GetString(idrOpportunity.GetOrdinal("description"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("cust_code")))
                            oOpportunity.CustCode = idrOpportunity.GetString(idrOpportunity.GetOrdinal("cust_code"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("created_by")))
                            oOpportunity.CreatedBy = idrOpportunity.GetString(idrOpportunity.GetOrdinal("created_by"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("created_date")))
                            oOpportunity.CreatedDate = idrOpportunity.GetDateTime(idrOpportunity.GetOrdinal("created_date"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("last_modified_by")))
                            oOpportunity.LastModifiedBy = idrOpportunity.GetString(idrOpportunity.GetOrdinal("last_modified_by"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("last_modified_date")))
                            oOpportunity.LastModifiedDate = idrOpportunity.GetDateTime(idrOpportunity.GetOrdinal("last_modified_date"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("pipeline_stage")))
                            oOpportunity.PipelineStage = idrOpportunity.GetString(idrOpportunity.GetOrdinal("pipeline_stage"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("tonnes")))
                            oOpportunity.Tonnes = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("Tonnes"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("totcount")))
                            oOpportunity.TotalCount = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("totcount"));

                        Opportunities.Add(oOpportunity);
                    }
                }

                return Opportunities;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrOpportunity != null)
                    idrOpportunity.Close();
            }
        }

        public int GetOpportunityCountForHome(int iPipelineStageID, ArgsEntity args)
        {
            int totalcount = 0;
            DbDataReader idrOpportunity = null;

            try
            {
                List<OpportunityEntity> Opportunities = new List<OpportunityEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                if (iPipelineStageID>0) 
                    paramCollection.Add("@PipelineStageID", iPipelineStageID, DbType.Int32);
                else
                    paramCollection.Add("@PipelineStageID", null, DbType.Int32);

               // paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "O.originator"), DbType.String);
                paramCollection.Add("@StartDate", args.SStartDate, DbType.String);
                paramCollection.Add("@DefaultDepartmentId", args.DefaultDepartmentId, DbType.String);

                idrOpportunity = this.DataAcessService.ExecuteQuery(OpportunitySql["GetOpportunityCountForHome"], paramCollection);

                if (idrOpportunity != null)
                {
                    //while (idrOpportunity.Read())
                    //{

                        int totalcountOrdinal = idrOpportunity.GetOrdinal("TotalCount");


                        if (idrOpportunity.Read())
                        {
                            if (!idrOpportunity.IsDBNull(totalcountOrdinal)) totalcount = idrOpportunity.GetInt32(totalcountOrdinal);
                        }
                    //}
                }

                return totalcount;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrOpportunity != null)
                    idrOpportunity.Close();
            }
        }

        public List<CustomerOpportunityEntity> GetContactOpportunities(int iPipelineStageID, ArgsEntity args)
        {
            CustomerOpportunityEntity oOpportunity = null;
            DbDataReader idrOpportunity = null;

            try
            {
                List<CustomerOpportunityEntity> Opportunities = new List<CustomerOpportunityEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@PipelineStageID", iPipelineStageID, DbType.Int32);
                //paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "O.Originator"), DbType.String);
                paramCollection.Add("@StartDate", args.SStartDate, DbType.String);
                paramCollection.Add("@DefaultDepartmentId", args.DefaultDepartmentId, DbType.String);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);


                idrOpportunity = this.DataAcessService.ExecuteQuery(OpportunitySql["GetContactOpportunities"], paramCollection);

                if (idrOpportunity != null)
                {
                    while (idrOpportunity.Read())
                    {
                        oOpportunity = CustomerOpportunityEntity.CreateObject();

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("opportunity_id")))
                            oOpportunity.OpportunityID = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("opportunity_id"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("lead_id")))
                            oOpportunity.LeadID = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("lead_id"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("Originator")))
                            oOpportunity.Originator = idrOpportunity.GetString(idrOpportunity.GetOrdinal("Originator"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("name")))
                            oOpportunity.Name = idrOpportunity.GetString(idrOpportunity.GetOrdinal("name"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("close_date")))
                            oOpportunity.CloseDate = idrOpportunity.GetDateTime(idrOpportunity.GetOrdinal("close_date"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("pipeline_stage_id")))
                            oOpportunity.Stage = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("pipeline_stage_id"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("probability")))
                            oOpportunity.Probability = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("probability"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("amount")))
                            oOpportunity.Amount = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("amount"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("units")))
                            oOpportunity.Units = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("units"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("ContactName")))
                            oOpportunity.LeadName = idrOpportunity.GetString(idrOpportunity.GetOrdinal("ContactName"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("description")))
                            oOpportunity.Description = idrOpportunity.GetString(idrOpportunity.GetOrdinal("description"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("business")))
                            oOpportunity.Business = idrOpportunity.GetString(idrOpportunity.GetOrdinal("business"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("industry")))
                            oOpportunity.Industry = idrOpportunity.GetString(idrOpportunity.GetOrdinal("industry"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("address")))
                            oOpportunity.Address = idrOpportunity.GetString(idrOpportunity.GetOrdinal("address"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("city")))
                            oOpportunity.City = idrOpportunity.GetString(idrOpportunity.GetOrdinal("city"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("state")))
                            oOpportunity.State = idrOpportunity.GetString(idrOpportunity.GetOrdinal("state"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("postcode")))
                            oOpportunity.PostCode = idrOpportunity.GetString(idrOpportunity.GetOrdinal("postcode"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("lead_stage")))
                            oOpportunity.LeadStage = idrOpportunity.GetString(idrOpportunity.GetOrdinal("lead_stage"));

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("industry_description")))
                            oOpportunity.IndustryDescription = idrOpportunity.GetString(idrOpportunity.GetOrdinal("industry_description"));

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("cust_code")))
                            oOpportunity.CustCode = idrOpportunity.GetString(idrOpportunity.GetOrdinal("cust_code"));
                        //oOpportunity.CreatedBy = idrOpportunity.GetString(idrOpportunity.GetOrdinal("created_by"));
                        //oOpportunity.CreatedDate = idrOpportunity.GetDateTime(idrOpportunity.GetOrdinal("created_date"));
                        //oOpportunity.LastModifiedBy = idrOpportunity.GetString(idrOpportunity.GetOrdinal("last_modified_by"));
                        //oOpportunity.LastModifiedDate = idrOpportunity.GetDateTime(idrOpportunity.GetOrdinal("last_modified_date"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("enduser_code")))
                            oOpportunity.EndUserCode = idrOpportunity.GetString(idrOpportunity.GetOrdinal("enduser_code"));

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("tonnes")))
                            oOpportunity.Tonnes = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("tonnes"));

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("total_count")))
                            oOpportunity.TotalCount = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("total_count"));

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("amount_sum")))
                            oOpportunity.AmountSum = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("amount_sum"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("units_sum")))
                            oOpportunity.UnitsSum = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("units_sum"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("tonnes_sum")))
                            oOpportunity.TonnesSum = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("tonnes_sum"));

                        Opportunities.Add(oOpportunity);
                    }
                }

                return Opportunities;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrOpportunity != null)
                    idrOpportunity.Close();
            }
        }

        public List<ClientOpportunityEntity> GetAllClientOpportunity(int iPipelineStageID, ArgsEntity args)
        {
            ClientOpportunityEntity oOpportunity = null;
            DbDataReader idrOpportunity = null;

            try
            {
                List<ClientOpportunityEntity> Opportunities = new List<ClientOpportunityEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@PipelineStageID", iPipelineStageID, DbType.Int32);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "O.Originator"), DbType.String);
                paramCollection.Add("@StartDate", args.SStartDate, DbType.String);
                paramCollection.Add("@DefaultDepartmentId", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                idrOpportunity = this.DataAcessService.ExecuteQuery(OpportunitySql["GetAllClientOpportunity"], paramCollection);

                if (idrOpportunity != null)
                {
                    while (idrOpportunity.Read())
                    {
                        oOpportunity = ClientOpportunityEntity.CreateObject();

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("lead_stage")))
                            oOpportunity.OpportunityOwner = idrOpportunity.GetString(idrOpportunity.GetOrdinal("lead_stage"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("row_count")))
                            oOpportunity.Count = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("row_count"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("amount_sum")))
                            oOpportunity.Value = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("amount_sum"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("units_sum")))
                            oOpportunity.Units = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("units_sum"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("tonnes_sum")))
                            oOpportunity.Tonnes = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("tonnes_sum"));

                        Opportunities.Add(oOpportunity);
                    }
                }

                return Opportunities;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrOpportunity != null)
                    idrOpportunity.Close();
            }
        }

        public bool Delete(int opportunityId)
        {
            try
            {
                bool successful = false;
                int iNoRec = 0;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OpportunityId", opportunityId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(OpportunitySql["DeleteOpportunity"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch
            {
                throw;
            }
        }

        public List<CatalogEntity> GetOppProducts(int opportunityId, ArgsEntity args)
        {
            DbDataReader drCatalog = null;

            try
            {
                List<CatalogEntity> Cataloglist = new List<CatalogEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OpportunityId", opportunityId, DbType.Int32);

                //paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                //paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                //paramCollection.Add("@RawCount", args.RowCount, DbType.Int32);

                drCatalog = this.DataAcessService.ExecuteQuery(OpportunitySql["GetOpportunityProducts"], paramCollection);

                if (drCatalog != null && drCatalog.HasRows)
                {
                    int codeOrdinal = drCatalog.GetOrdinal("catlog_code");
                    int descriptioneOrdinal = drCatalog.GetOrdinal("description");
                    int priceOrdinal = drCatalog.GetOrdinal("price");

                    while (drCatalog.Read())
                    {
                        CatalogEntity catalogEntity = CatalogEntity.CreateObject();

                        if (!drCatalog.IsDBNull(codeOrdinal)) catalogEntity.CatlogCode = drCatalog.GetString(codeOrdinal);
                        if (!drCatalog.IsDBNull(descriptioneOrdinal)) catalogEntity.Description = drCatalog.GetString(descriptioneOrdinal);
                        if (!drCatalog.IsDBNull(priceOrdinal)) catalogEntity.Price = drCatalog.GetDouble(priceOrdinal);

                        Cataloglist.Add(catalogEntity);
                    }
                }

                return Cataloglist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drCatalog != null)
                    drCatalog.Close();
            }
        }

        

        public CustomerOpportunityEntity GetOpportunity(int iOpportunityID)
        {
            DbDataReader idrOpportunity = null;

            try
            {
                CustomerOpportunityEntity oOpportunity = null;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@opportunityID", iOpportunityID, DbType.Int32);

                idrOpportunity = this.DataAcessService.ExecuteQuery(OpportunitySql["GetOpportunityDetails"], paramCollection);

                //idrOpportunity = (IngresDataReader)oDataHandle.ExecuteSelect(string.Format(CRMSQLs.GetOpportunityDetails, iOpportunityID));

                if (idrOpportunity != null && idrOpportunity.HasRows)
                {
                    if (idrOpportunity.Read())
                    {
                        oOpportunity = CustomerOpportunityEntity.CreateObject();

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("opportunity_id")))
                            oOpportunity.OpportunityID = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("opportunity_id"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("lead_id")))
                            oOpportunity.LeadID = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("lead_id"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("originator")))
                            oOpportunity.Originator = idrOpportunity.GetString(idrOpportunity.GetOrdinal("originator"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("name")))
                            oOpportunity.Name = idrOpportunity.GetString(idrOpportunity.GetOrdinal("name"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("close_date")))
                            oOpportunity.CloseDate = idrOpportunity.GetDateTime(idrOpportunity.GetOrdinal("close_date")).ToLocalTime();
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("pipeline_stage_id")))
                            oOpportunity.Stage = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("pipeline_stage_id"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("probability")))
                            oOpportunity.Probability = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("probability"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("amount")))
                            oOpportunity.Amount = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("amount"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("units")))
                            oOpportunity.Units = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("units"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("description")))
                            oOpportunity.Description = idrOpportunity.GetString(idrOpportunity.GetOrdinal("description"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("cust_code")))
                            oOpportunity.CustCode = idrOpportunity.GetString(idrOpportunity.GetOrdinal("cust_code"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("created_by")))
                            oOpportunity.CreatedBy = idrOpportunity.GetString(idrOpportunity.GetOrdinal("created_by"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("created_date")))
                            oOpportunity.CreatedDate = idrOpportunity.GetDateTime(idrOpportunity.GetOrdinal("created_date"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("last_modified_by")))
                            oOpportunity.LastModifiedBy = idrOpportunity.GetString(idrOpportunity.GetOrdinal("last_modified_by"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("last_modified_date")))
                            oOpportunity.LastModifiedDate = idrOpportunity.GetDateTime(idrOpportunity.GetOrdinal("last_modified_date"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("rep_group_id")))
                            oOpportunity.RepGroupID = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("rep_group_id"));
                        //if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("pipeline_stage")))
                        //    oOpportunity.PipelineStage = idrOpportunity.GetString(idrOpportunity.GetOrdinal("pipeline_stage"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("rep_group_name")))
                            oOpportunity.RepGroupName = idrOpportunity.GetString(idrOpportunity.GetOrdinal("rep_group_name"));

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("lead_name")))
                            oOpportunity.LeadName = idrOpportunity.GetString(idrOpportunity.GetOrdinal("lead_name"));

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("enduser_code")))
                            oOpportunity.EndUserCode = idrOpportunity.GetString(idrOpportunity.GetOrdinal("enduser_code"));

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("tonnes")))
                            oOpportunity.Tonnes = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("tonnes"));
                    }
                }

                return oOpportunity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrOpportunity != null)
                    idrOpportunity.Close();
            }
        }

        /*
        public List<OpportunityEntity> GetCustomerOpportunities(int pipelineStageID, string repGroups , ArgsEntity args)
        {
            //args.StartIndex, args.RowCount, args.ChiildOriginators, args.AdditionalParams, args.OrderBy

            OpportunityEntity oOpportunity = null;
            DbDataReader idrOpportunity = null;

            string sWhereCls = "";
                       
            try
            {
                List<OpportunityEntity> Opportunities = new List<OpportunityEntity>();

                if (args.Originator != "")
                {                   
                    //sWhereCls = oOriginator.GetChildOriginators(sOriginator); //Cannot use global child users since its filtered in the UI itself

                    sWhereCls = " WHERE (" + args.ChildOriginators.Replace("originator", "o.originator");

                    
                    if (repGroups != "")
                    {
                        repGroups = repGroups.Remove(repGroups.Length - 1, 1);  // Remove the Last Comma
                        sWhereCls += " OR o.rep_group_id IN (" + repGroups + ")";
                    }
                    //sWhereCls += " )";
                    sWhereCls += "  AND o.del_flag !='Y' )";
                }

                if (pipelineStageID > 0)
                {
                    sWhereCls = sWhereCls != "" ? sWhereCls + " AND O.pipeline_stage_id = " + pipelineStageID.ToString() : " O.pipeline_stage_id = " + pipelineStageID.ToString();
                }

                if (!string.IsNullOrEmpty(args.AdditionalParams))
                {
                    sWhereCls += " AND " + args.AdditionalParams.Replace("L.lead_name", "C.name");
                }

                if (!string.IsNullOrEmpty(args.OrderBy))
                    sWhereCls += " ORDER BY " + args.OrderBy.Replace("L.lead_name", "C.name");

                //string SQL = string.Format(CRMSQLs.GetCustomerOpportunities,
                //    sWhereCls + " AND o.del_flag !='Y' ", defaultDeptID);
               
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@defaultDeptID", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@startIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@rowCount", args.RowCount, DbType.Int32);

                idrOpportunity = this.DataAcessService.ExecuteQuery(OpportunitySql["GetCustomerOpportunities"].Format(sWhereCls), paramCollection);

                if (idrOpportunity != null)
                {
                    while (idrOpportunity.Read())
                    {
                        oOpportunity = new OpportunityEntity();

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("opportunity_id")))
                            oOpportunity.OpportunityID = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("opportunity_id"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("Originator")))
                            oOpportunity.Originator = idrOpportunity.GetString(idrOpportunity.GetOrdinal("Originator"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("name")))
                            oOpportunity.Name = idrOpportunity.GetString(idrOpportunity.GetOrdinal("name"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("close_date")))
                            oOpportunity.CloseDate = idrOpportunity.GetDateTime(idrOpportunity.GetOrdinal("close_date")).ToLocalTime();
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("pipeline_stage_id")))
                            oOpportunity.Stage = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("pipeline_stage_id"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("probability")))
                            oOpportunity.Probability = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("probability"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("amount")))
                            oOpportunity.Amount = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("amount"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("units")))
                            oOpportunity.Units = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("units"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("ContactName")))
                            oOpportunity.LeadName = idrOpportunity.GetString(idrOpportunity.GetOrdinal("ContactName"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("description")))
                            oOpportunity.Description = idrOpportunity.GetString(idrOpportunity.GetOrdinal("description"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("business")))
                            oOpportunity.Business = idrOpportunity.GetString(idrOpportunity.GetOrdinal("business"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("industry")))
                            oOpportunity.Industry = idrOpportunity.GetString(idrOpportunity.GetOrdinal("industry"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("address")))
                            oOpportunity.Address = idrOpportunity.GetString(idrOpportunity.GetOrdinal("address"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("city")))
                            oOpportunity.City = idrOpportunity.GetString(idrOpportunity.GetOrdinal("city"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("state")))
                            oOpportunity.State = idrOpportunity.GetString(idrOpportunity.GetOrdinal("state"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("postcode")))
                            oOpportunity.PostCode = idrOpportunity.GetString(idrOpportunity.GetOrdinal("postcode"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("lead_stage")))
                            oOpportunity.LeadStage = idrOpportunity.GetString(idrOpportunity.GetOrdinal("lead_stage"));

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("industry_description")))
                            oOpportunity.IndustryDescription = idrOpportunity.GetString(idrOpportunity.GetOrdinal("industry_description"));

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("cust_code")))
                            oOpportunity.CustCode = idrOpportunity.GetString(idrOpportunity.GetOrdinal("cust_code"));

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("pipeline_stage")))
                            oOpportunity.PipelineStage = idrOpportunity.GetString(idrOpportunity.GetOrdinal("pipeline_stage"));         // Bramen 01-Sep-2011

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("last_called_date")))
                            oOpportunity.LastActivityDate = idrOpportunity.GetDateTime(idrOpportunity.GetOrdinal("last_called_date"));

                        Opportunities.Add(oOpportunity);
                    }
                }

                return Opportunities;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrOpportunity != null)
                    idrOpportunity.Close();
            }
        }

        public List<OpportunityEntity> GetContactOpportunitiesCount(string originator, string defaultDepartmentID)
        {
            OpportunityEntity oOpportunity = null;
            DbDataReader idrOpportunity = null;

            string sWhereCls = "";
          
            try
            {
                List<OpportunityEntity> Opportunities = new List<OpportunityEntity>();

                if (originator != "")
                {                  
                    sWhereCls = originator;
                    sWhereCls = "( " + sWhereCls.Replace("originator", "O.originator") + " )";

                }


                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@defaultDeptID", defaultDepartmentID, DbType.String);

                idrOpportunity = this.DataAcessService.ExecuteQuery(OpportunitySql["GetOpportunitiesForLeadCount"].Format(sWhereCls), paramCollection);

                
                if (idrOpportunity != null)
                {
                    while (idrOpportunity.Read())
                    {
                        oOpportunity = new OpportunityEntity();
                        //O.pipeline_stage_id, count(O.pipeline_stage_id) c 
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("pipeline_stage_id")))
                            oOpportunity.Stage = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("pipeline_stage_id"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("c")))
                            oOpportunity.PipelineStageCount = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("c"));

                        Opportunities.Add(oOpportunity);
                    }
                }

                return Opportunities;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrOpportunity != null)
                    idrOpportunity.Close();
            }
        }

        public List<OpportunityEntity> GetOpportunitiesForCustomerCount(string originator, string defaultDepartmentID, string childOriginators, string repGroups)
        {
            OpportunityEntity oOpportunity = null;
            DbDataReader idrOpportunity = null;

            string sWhereCls = "";
                    
            try
            {
                List<OpportunityEntity> Opportunities = new List<OpportunityEntity>();
                if (originator != "")
                {                   
                    sWhereCls = childOriginators;

                    sWhereCls = "(" + sWhereCls.Replace("originator", "o.originator");

                   
                    if (repGroups != "")
                    {
                        repGroups = repGroups.Remove(repGroups.Length - 1, 1);  // Remove the Last Comma
                        sWhereCls += " OR o.rep_group_id IN (" + repGroups + ")";
                    }
                    sWhereCls += " )";
                }

                sWhereCls = sWhereCls + " AND o.del_flag != 'Y' ";

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@defaultDeptID", defaultDepartmentID, DbType.String);

                idrOpportunity = this.DataAcessService.ExecuteQuery(OpportunitySql["GetOpportunitiesForCustomerCount"].Format(sWhereCls), paramCollection);
               
                if (idrOpportunity != null)
                {
                    while (idrOpportunity.Read())
                    {
                        oOpportunity = new OpportunityEntity();

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("pipeline_stage_id")))
                            oOpportunity.PipelineStageId = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("pipeline_stage_id"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("c")))
                            oOpportunity.PipelineStageCount = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("c"));

                        Opportunities.Add(oOpportunity);
                    }
                }

                return Opportunities;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrOpportunity != null)
                    idrOpportunity.Close();
            }
        }

        public int GetOpportunityForWindow(ArgsEntity  args , string sRepGroups , DateTime? dtmCloseDate = null)
        {
            int count = 0;
            DbDataReader idrOpportunity = null;

            string sWhereCls1 = "";
           
            string sWhereCls2 = "";
           
            try
            {
                if (args.Originator != "")
                {
                    sWhereCls1 = args.ChildOriginators;
                    sWhereCls1 = "( " + sWhereCls1.Replace("originator", "O.originator");

                    sWhereCls2 = sWhereCls1;
                   
                    if (sRepGroups != "")
                    {
                        sRepGroups = sRepGroups.Remove(sRepGroups.Length - 1, 1);  // Remove the Last Comma
                        sWhereCls1 = sWhereCls2 += " OR O.rep_group_id IN (" + sRepGroups + ")";
                    }
                    sWhereCls1 = sWhereCls2 += " )";
                }

                if (dtmCloseDate != null)
                {
                    sWhereCls1 = sWhereCls1 != "" ? sWhereCls1 + " AND " : sWhereCls1;
                    sWhereCls2 = sWhereCls2 != "" ? sWhereCls2 + " AND " : sWhereCls2;

                    sWhereCls1 += " close_date >= '" + Convert.ToDateTime(dtmCloseDate).ToString("dd-MMM-yyyy HH:mm:ss")
                        + "' AND close_date <= '" + Convert.ToDateTime(dtmCloseDate).AddDays(7).ToString("dd-MMM-yyyy HH:mm:ss") + "' ";
                    sWhereCls2 += " close_date >= '" + Convert.ToDateTime(dtmCloseDate).ToString("dd-MMM-yyyy HH:mm:ss")
                        + "' AND close_date <= '" + Convert.ToDateTime(dtmCloseDate).AddDays(7).ToString("dd-MMM-yyyy HH:mm:ss") + "' ";
                }

                if (!string.IsNullOrWhiteSpace(sWhereCls1))
                    sWhereCls1 = sWhereCls1.Insert(0, " AND ");
                if (!string.IsNullOrWhiteSpace(sWhereCls2))
                    sWhereCls2 = sWhereCls2.Insert(0, " AND ");

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@defaultDeptID", args.DefaultDepartmentId, DbType.String);

                idrOpportunity = this.DataAcessService.ExecuteQuery(OpportunitySql["GetOpportunityForWindow"].Format(sWhereCls1, sWhereCls2), paramCollection);
                               
                if (idrOpportunity != null)
                {
                    if (idrOpportunity.Read())
                    {
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("count")))
                        {
                            if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("count")))
                                count = int.Parse(idrOpportunity.GetInt64(idrOpportunity.GetOrdinal("count")).ToString());
                            //var item = idrOpportunity.GetInt64(idrOpportunity.GetOrdinal("count"));
                            //if (item != 0)
                            //{
                            //    count =int.Parse(idrOpportunity.GetInt64(idrOpportunity.GetOrdinal("count")).ToString());
                            //}
                            //else
                            //{
                            //    count = 0;
                            //}
                        }

                    }
                }
                return count;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (idrOpportunity != null)
                {
                    idrOpportunity.Close();

                }
            }
        }

        public double GetOpportunitySum(ArgsEntity args)
        {
            DbDataReader drOpportunity = null;
            double sum = 0;
            try
            {

                string Originators = " AND ( " + args.ChildOriginators.Replace("originator", "O.originator");

                if (!string.IsNullOrEmpty(args.AdditionalParams))
                    Originators = Originators + args.AdditionalParams;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@defaultDeptID", args.DefaultDepartmentId, DbType.String);

                drOpportunity = this.DataAcessService.ExecuteQuery(OpportunitySql["GetOpportunitySum"].Format(Originators), paramCollection);
               
                if (drOpportunity.HasRows)
                {
                    if (drOpportunity.Read())
                    {
                        if (!drOpportunity.IsDBNull(drOpportunity.GetOrdinal("opportunitysum")))
                            sum = drOpportunity.GetDouble(drOpportunity.GetOrdinal("opportunitysum"));
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (drOpportunity != null)
                    drOpportunity.Close();
            }
            return sum;
        }

        
        
        

        public void GetAllProducts(ref DataTable dtCatalog)
        {
            try
            {            
                dtCatalog = this.DataAcessService.ExecuteDisconnected(OpportunitySql["GetAllProducts"]);
            }
            catch
            {
                throw;
            }
        }

        public int GetNextOpportunityID()
        {
            try
            {
                int opportunityID = 0;
                opportunityID = Convert.ToInt32(this.DataAcessService.GetOneValue(OpportunitySql["GetNextOpportunityID"]));
                return opportunityID;
            }
            catch 
            {                
                throw;
            }
        }
         */
        #endregion
    }
}
