﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.CRM.Entities;
using System.Data.Common;
using System.Data;

namespace Peercore.CRM.DataAccess.DAO
{
    public class FreeProductDAO:BaseDAO
    {
        private DbSqlAdapter FreeProductSql { get; set; }

        private void RegisterSql()
        {
            this.FreeProductSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.FreeProductSql.xml", ApplicationService.Instance.DbProvider);
        }

        public FreeProductDAO()
        {
            RegisterSql();
        }

        public FreeProductDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public FreeProductEntity CreateObject()
        {
            return FreeProductEntity.CreateObject();
        }

        public FreeProductEntity CreateObject(int entityId)
        {
            return FreeProductEntity.CreateObject(entityId);
        }        

        public bool InsertFreeProduct(out int freeProductId, FreeProductEntity freeProductEntity)
        {
            bool successful = false;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@ProductId", freeProductEntity.ProductId, DbType.Int32);
                paramCollection.Add("@Qty", freeProductEntity.Qty, DbType.Double);
                paramCollection.Add("@ProductPackingId", freeProductEntity.ProductPackingId, DbType.Int32);
                paramCollection.Add("@SchemeDetailsId", freeProductEntity.SchemeDetailsId, DbType.Int32);
                paramCollection.Add("@CreatedBy", freeProductEntity.CreatedBy, DbType.String);
                paramCollection.Add("@FreeProductId", null, DbType.Int32, ParameterDirection.Output);

                freeProductId = this.DataAcessService.ExecuteNonQuery(FreeProductSql["InsertFreeProduct"], paramCollection, true, "@FreeProductId");
                if (freeProductId > 0)
                    successful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return successful;
        }

        public bool UpdateFreeProduct(FreeProductEntity freeProductEntity)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@FreeProductId", freeProductEntity.FreeProductId, DbType.Int32);
                paramCollection.Add("@ProductId", freeProductEntity.ProductId, DbType.Int32);
                paramCollection.Add("@Qty", freeProductEntity.Qty, DbType.Double);
                paramCollection.Add("@ProductPackingId", freeProductEntity.ProductPackingId, DbType.Int32);
                paramCollection.Add("@SchemeDetailsId", freeProductEntity.SchemeDetailsId, DbType.Int32);
                paramCollection.Add("@LastModifiedBy", freeProductEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(FreeProductSql["UpdateFreeProduct"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return UpdateSuccessful;
        }

        public List<FreeProductEntity> GetAllFreeProducts(ArgsEntity args)
        {
            DbDataReader drfreeProduct = null;
            try
            {
                List<FreeProductEntity> freeProductCollection = new List<FreeProductEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                drfreeProduct = this.DataAcessService.ExecuteQuery(FreeProductSql["GetAllFreeProducts"], paramCollection);

                if (drfreeProduct != null && drfreeProduct.HasRows)
                {
                    int freeProductIdOrdinal = drfreeProduct.GetOrdinal("free_product_id");
                    int productIdOrdinal = drfreeProduct.GetOrdinal("product_id");
                    int qtyOrdinal = drfreeProduct.GetOrdinal("qty");
                    int productPackingIdOrdinal = drfreeProduct.GetOrdinal("product_packing_id");
                    int schemeDetailsIdOrdinal = drfreeProduct.GetOrdinal("scheme_details_id");
                    int totalCountOrdinal = drfreeProduct.GetOrdinal("total_count");

                    FreeProductEntity freeProductEntity = null;
                    while (drfreeProduct.Read())
                    {
                        freeProductEntity = CreateObject();

                        if (!drfreeProduct.IsDBNull(freeProductIdOrdinal)) freeProductEntity.FreeProductId = drfreeProduct.GetInt32(freeProductIdOrdinal);
                        if (!drfreeProduct.IsDBNull(productIdOrdinal)) freeProductEntity.ProductId = drfreeProduct.GetInt32(productIdOrdinal);
                        if (!drfreeProduct.IsDBNull(qtyOrdinal)) freeProductEntity.Qty = drfreeProduct.GetDouble(qtyOrdinal);
                        if (!drfreeProduct.IsDBNull(productPackingIdOrdinal)) freeProductEntity.ProductPackingId = drfreeProduct.GetInt32(productPackingIdOrdinal);
                        if (!drfreeProduct.IsDBNull(schemeDetailsIdOrdinal)) freeProductEntity.SchemeDetailsId = drfreeProduct.GetInt32(schemeDetailsIdOrdinal);
                        if (!drfreeProduct.IsDBNull(totalCountOrdinal)) freeProductEntity.TotalCount = drfreeProduct.GetInt32(totalCountOrdinal);                       

                        freeProductCollection.Add(freeProductEntity);

                    }
                }

                return freeProductCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drfreeProduct != null)
                    drfreeProduct.Close();
            }
        }

        public FreeProductEntity GetFreeProductById(int freeProductId)
        {
            DbDataReader drfreeProduct = null;

            try
            {
                FreeProductEntity freeProductEntity = CreateObject();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@FreeProductId", freeProductId, DbType.Int32);

                drfreeProduct = this.DataAcessService.ExecuteQuery(FreeProductSql["GetFreeProductById"], paramCollection);

                if (drfreeProduct != null && drfreeProduct.HasRows)
                {
                    int freeProductIdOrdinal = drfreeProduct.GetOrdinal("free_product_id");
                    int productIdOrdinal = drfreeProduct.GetOrdinal("product_id");
                    int qtyOrdinal = drfreeProduct.GetOrdinal("qty");
                    int productPackingIdOrdinal = drfreeProduct.GetOrdinal("product_packing_id");
                    int schemeDetailsIdOrdinal = drfreeProduct.GetOrdinal("scheme_details_id");
                    int createdByOrdinal = drfreeProduct.GetOrdinal("created_by");
                    int createdDateOrdinal = drfreeProduct.GetOrdinal("created_date");
                    int lastModifiedByOrdinal = drfreeProduct.GetOrdinal("last_modified_by");
                    int lastModifiedDateOrdinal = drfreeProduct.GetOrdinal("last_modified_datek");                    

                    if (drfreeProduct.Read())
                    {
                        if (!drfreeProduct.IsDBNull(freeProductIdOrdinal)) freeProductEntity.FreeProductId = drfreeProduct.GetInt32(freeProductIdOrdinal);
                        if (!drfreeProduct.IsDBNull(productIdOrdinal)) freeProductEntity.ProductId = drfreeProduct.GetInt32(productIdOrdinal);
                        if (!drfreeProduct.IsDBNull(qtyOrdinal)) freeProductEntity.Qty = drfreeProduct.GetDouble(qtyOrdinal);
                        if (!drfreeProduct.IsDBNull(productPackingIdOrdinal)) freeProductEntity.ProductPackingId = drfreeProduct.GetInt32(productPackingIdOrdinal);
                        if (!drfreeProduct.IsDBNull(schemeDetailsIdOrdinal)) freeProductEntity.SchemeDetailsId = drfreeProduct.GetInt32(schemeDetailsIdOrdinal);
                        if (!drfreeProduct.IsDBNull(createdByOrdinal)) freeProductEntity.CreatedBy = drfreeProduct.GetString(createdByOrdinal);
                        if (!drfreeProduct.IsDBNull(createdDateOrdinal)) freeProductEntity.CreatedDate = drfreeProduct.GetDateTime(createdDateOrdinal);
                        if (!drfreeProduct.IsDBNull(lastModifiedByOrdinal)) freeProductEntity.LastModifiedBy = drfreeProduct.GetString(lastModifiedByOrdinal);
                        if (!drfreeProduct.IsDBNull(lastModifiedDateOrdinal)) freeProductEntity.LastModifiedDate = drfreeProduct.GetDateTime(lastModifiedDateOrdinal);  
                    }
                }

                return freeProductEntity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drfreeProduct != null)
                    drfreeProduct.Close();
            }
        }

        public bool UpdateFreeProductInActive(int schemeHeaderId)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@SchemeHeaderId", schemeHeaderId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(FreeProductSql["UpdateFreeProductInActive"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return UpdateSuccessful;
        }

        public List<FreeProductEntity> GeFreeProductsByDetailsId(int schemeDetailsId)
        {
            DbDataReader drfreeProduct = null;
            try
            {
                List<FreeProductEntity> freeProductCollection = new List<FreeProductEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@DetailsId", schemeDetailsId, DbType.Int32);

                drfreeProduct = this.DataAcessService.ExecuteQuery(FreeProductSql["GeFreeProductsByDetailsId"], paramCollection);

                if (drfreeProduct != null && drfreeProduct.HasRows)
                {
                    int freeProductIdOrdinal = drfreeProduct.GetOrdinal("free_product_id");
                    int productIdOrdinal = drfreeProduct.GetOrdinal("product_id");
                    int qtyOrdinal = drfreeProduct.GetOrdinal("qty");
                    int productPackingIdOrdinal = drfreeProduct.GetOrdinal("product_packing_id");
                    int schemeDetailsIdOrdinal = drfreeProduct.GetOrdinal("scheme_details_id");
                    int packingNameOrdinal = drfreeProduct.GetOrdinal("packing_name");
                    int productNameOrdinal = drfreeProduct.GetOrdinal("product_name");

                    FreeProductEntity freeProductEntity = null;
                    while (drfreeProduct.Read())
                    {
                        freeProductEntity = CreateObject();

                        if (!drfreeProduct.IsDBNull(freeProductIdOrdinal)) freeProductEntity.FreeProductId = drfreeProduct.GetInt32(freeProductIdOrdinal);
                        if (!drfreeProduct.IsDBNull(productIdOrdinal)) freeProductEntity.ProductId = drfreeProduct.GetInt32(productIdOrdinal);
                        if (!drfreeProduct.IsDBNull(qtyOrdinal)) freeProductEntity.Qty = drfreeProduct.GetDouble(qtyOrdinal);
                        if (!drfreeProduct.IsDBNull(productPackingIdOrdinal)) freeProductEntity.ProductPackingId = drfreeProduct.GetInt32(productPackingIdOrdinal);
                        if (!drfreeProduct.IsDBNull(schemeDetailsIdOrdinal)) freeProductEntity.SchemeDetailsId = drfreeProduct.GetInt32(schemeDetailsIdOrdinal);
                        if (!drfreeProduct.IsDBNull(packingNameOrdinal)) freeProductEntity.ProductPackingName = drfreeProduct.GetString(packingNameOrdinal);
                        if (!drfreeProduct.IsDBNull(productNameOrdinal)) freeProductEntity.ProductName = drfreeProduct.GetString(productNameOrdinal);

                        freeProductCollection.Add(freeProductEntity);

                    }
                }

                return freeProductCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drfreeProduct != null)
                    drfreeProduct.Close();
            }
        }

        public List<FreeProductEntity> GetFreeProductsByDivisionId(int divisionId)
        {
            DbDataReader drfreeProduct = null;
            try
            {
                List<FreeProductEntity> freeProductCollection = new List<FreeProductEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@DivisionId", divisionId, DbType.Int32);

                drfreeProduct = this.DataAcessService.ExecuteQuery(FreeProductSql["GetFreeProductsByDivisionId"], paramCollection);

                if (drfreeProduct != null && drfreeProduct.HasRows)
                {
                    int freeProductIdOrdinal = drfreeProduct.GetOrdinal("free_product_id");
                    int productIdOrdinal = drfreeProduct.GetOrdinal("product_id");
                    int qtyOrdinal = drfreeProduct.GetOrdinal("qty");
                    int productPackingIdOrdinal = drfreeProduct.GetOrdinal("product_packing_id");
                    int schemeDetailsIdOrdinal = drfreeProduct.GetOrdinal("scheme_details_id");
                   
                    FreeProductEntity freeProductEntity = null;
                    while (drfreeProduct.Read())
                    {
                        freeProductEntity = CreateObject();

                        if (!drfreeProduct.IsDBNull(freeProductIdOrdinal)) freeProductEntity.FreeProductId = drfreeProduct.GetInt32(freeProductIdOrdinal);
                        if (!drfreeProduct.IsDBNull(productIdOrdinal)) freeProductEntity.ProductId = drfreeProduct.GetInt32(productIdOrdinal);
                        if (!drfreeProduct.IsDBNull(qtyOrdinal)) freeProductEntity.Qty = drfreeProduct.GetDouble(qtyOrdinal);
                        if (!drfreeProduct.IsDBNull(productPackingIdOrdinal)) freeProductEntity.ProductPackingId = drfreeProduct.GetInt32(productPackingIdOrdinal);
                        if (!drfreeProduct.IsDBNull(schemeDetailsIdOrdinal)) freeProductEntity.SchemeDetailsId = drfreeProduct.GetInt32(schemeDetailsIdOrdinal);

                        freeProductCollection.Add(freeProductEntity);
                    }
                }

                return freeProductCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drfreeProduct != null)
                    drfreeProduct.Close();
            }
        }
    }
}
