﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;

namespace Peercore.CRM.DataAccess.DAO
{
    public class SurveyDAO : BaseDAO 
    {
        private DbSqlAdapter CommonSql { get; set; }

        private void RegisterSql()
        {
            this.CommonSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.CommonSql.xml", ApplicationService.Instance.DbProvider);
        }

        public SurveyDAO()
        {
            RegisterSql();
        }

        public SurveyDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public QuestionEntity CreateObject()
        {
            return QuestionEntity.CreateObject();
        }

        public List<QuestionEntity> GetAllSurveyQuestions()
        {
            DbDataReader dataReader = null;
            List<QuestionEntity> questionList= new List<QuestionEntity>();
            QuestionEntity questionEntity = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                dataReader = this.DataAcessService.ExecuteQuery(CommonSql["GetCustomerSurveyQuestions"]);

                if (dataReader != null && dataReader.HasRows)
                {
                    int idOrdinal = dataReader.GetOrdinal("id");
                    int codeOrdinal = dataReader.GetOrdinal("code");
                    int questionOrdinal = dataReader.GetOrdinal("name");

                    while (dataReader.Read())
                    {
                        questionEntity = CreateObject();

                        if (!dataReader.IsDBNull(idOrdinal)) questionEntity.Id = dataReader.GetInt32(idOrdinal);
                        if (!dataReader.IsDBNull(codeOrdinal)) questionEntity.Code = dataReader.GetString(codeOrdinal);
                        if (!dataReader.IsDBNull(questionOrdinal)) questionEntity.Quesion = dataReader.GetString(questionOrdinal);

                        questionList.Add(questionEntity);
                    }
                }

                return questionList;
            }

            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }

        }


    }
}
