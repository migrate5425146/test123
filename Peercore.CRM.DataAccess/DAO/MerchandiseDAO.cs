﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Entities;
using System.Data.Common;
using System.Collections;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;


namespace Peercore.CRM.DataAccess.DAO
{
    public class MerchandiseDAO  : BaseDAO
    {
        private DbSqlAdapter MerchandiseSql { get; set; }

        private void RegisterSql()
        {
            this.MerchandiseSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.MerchandiseSql.xml", ApplicationService.Instance.DbProvider);
        }

        public MerchandiseDAO()
        {
            RegisterSql();
        }

        public MerchandiseDAO(DbWorkflowScope scope)
            : base(scope)
        {
           RegisterSql();
        }

        public MerchandiseEntity CreateObject()
        {
            return MerchandiseEntity.CreateObject();
        }

        public MerchandiseEntity CreateObject(int entityId)
        {
            return MerchandiseEntity.CreateObject(entityId);
        }

        public List<MerchandiseEntity> GetAllMerchandise(ArgsEntity args)
        {
            DbDataReader drMerchandise = null;

            try
            {
                List<MerchandiseEntity> merchandiseList = new List<MerchandiseEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                drMerchandise = this.DataAcessService.ExecuteQuery(MerchandiseSql["GetAllMerchandise"], paramCollection);

                if (drMerchandise != null && drMerchandise.HasRows)
                {
                    int idOrdinal = drMerchandise.GetOrdinal("id");
                    int codeOrdinal = drMerchandise.GetOrdinal("code");
                    int descriptionOrdinal = drMerchandise.GetOrdinal("description");
                    int statusOrdinal = drMerchandise.GetOrdinal("status");
                    int createdbyOrdinal = drMerchandise.GetOrdinal("created_by");
                    int createddateOrdinal = drMerchandise.GetOrdinal("created_date");
                    int lastmodifiedbyOrdinal = drMerchandise.GetOrdinal("last_modified_by");
                    int lastmodifieddateOrdinal = drMerchandise.GetOrdinal("last_modified_date");
                    int totalCountOrdinal = drMerchandise.GetOrdinal("total_count");

                    while (drMerchandise.Read())
                    {
                        MerchandiseEntity merchandiseEntity = CreateObject();

                        if (!drMerchandise.IsDBNull(idOrdinal)) merchandiseEntity.MerchandiseId = drMerchandise.GetInt32(idOrdinal);
                        if (!drMerchandise.IsDBNull(codeOrdinal)) merchandiseEntity.Code = drMerchandise.GetString(codeOrdinal);
                        if (!drMerchandise.IsDBNull(descriptionOrdinal)) merchandiseEntity.Description = drMerchandise.GetString(descriptionOrdinal);
                        if (!drMerchandise.IsDBNull(statusOrdinal)) merchandiseEntity.Status = drMerchandise.GetString(statusOrdinal);
                        if (!drMerchandise.IsDBNull(createdbyOrdinal)) merchandiseEntity.CreatedBy = drMerchandise.GetString(createdbyOrdinal);
                        if (!drMerchandise.IsDBNull(createddateOrdinal)) merchandiseEntity.CreatedDate = drMerchandise.GetDateTime(createddateOrdinal);
                        if (!drMerchandise.IsDBNull(lastmodifiedbyOrdinal)) merchandiseEntity.LastModifiedBy = drMerchandise.GetString(lastmodifiedbyOrdinal);
                        if (!drMerchandise.IsDBNull(lastmodifieddateOrdinal)) merchandiseEntity.LastModifiedDate = drMerchandise.GetDateTime(lastmodifieddateOrdinal);

                        merchandiseList.Add(merchandiseEntity);
                    }
                }

                return merchandiseList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drMerchandise != null)
                    drMerchandise.Close();
            }
        }

        public List<MerchandiseEntity> GetCustomerMerchandise(ArgsEntity args)
        {
            DbDataReader drMerchandise = null;

            try
            {
                List<MerchandiseEntity> merchandiseList = new List<MerchandiseEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                drMerchandise = this.DataAcessService.ExecuteQuery(MerchandiseSql["GetCustomerMerchandise"], paramCollection);

                if (drMerchandise != null && drMerchandise.HasRows)
                {
                    int idOrdinal = drMerchandise.GetOrdinal("id");
                    int codeOrdinal = drMerchandise.GetOrdinal("code");
                    int descriptionOrdinal = drMerchandise.GetOrdinal("description");
                    int statusOrdinal = drMerchandise.GetOrdinal("status");
                    int createdbyOrdinal = drMerchandise.GetOrdinal("created_by");
                    int createddateOrdinal = drMerchandise.GetOrdinal("created_date");
                    int lastmodifiedbyOrdinal = drMerchandise.GetOrdinal("last_modified_by");
                    int lastmodifieddateOrdinal = drMerchandise.GetOrdinal("last_modified_date");
                    int custCodeOrdinal = drMerchandise.GetOrdinal("cust_code");
                    int totalCountOrdinal = drMerchandise.GetOrdinal("total_count");

                    while (drMerchandise.Read())
                    {
                        MerchandiseEntity merchandiseEntity = CreateObject();

                        if (!drMerchandise.IsDBNull(idOrdinal)) merchandiseEntity.MerchandiseId = drMerchandise.GetInt32(idOrdinal);
                        if (!drMerchandise.IsDBNull(codeOrdinal)) merchandiseEntity.Code = drMerchandise.GetString(codeOrdinal);
                        if (!drMerchandise.IsDBNull(descriptionOrdinal)) merchandiseEntity.Description = drMerchandise.GetString(descriptionOrdinal);
                        if (!drMerchandise.IsDBNull(statusOrdinal)) merchandiseEntity.Status = drMerchandise.GetString(statusOrdinal);
                        if (!drMerchandise.IsDBNull(createdbyOrdinal)) merchandiseEntity.CreatedBy = drMerchandise.GetString(createdbyOrdinal);
                        if (!drMerchandise.IsDBNull(createddateOrdinal)) merchandiseEntity.CreatedDate = drMerchandise.GetDateTime(createddateOrdinal);
                        if (!drMerchandise.IsDBNull(lastmodifiedbyOrdinal)) merchandiseEntity.LastModifiedBy = drMerchandise.GetString(lastmodifiedbyOrdinal);

                        if (!drMerchandise.IsDBNull(custCodeOrdinal))
                        { merchandiseEntity.CustomerCode = drMerchandise.GetString(custCodeOrdinal); }
                        else
                        { merchandiseEntity.CustomerCode = string.Empty; }

                        if (!drMerchandise.IsDBNull(lastmodifieddateOrdinal)) merchandiseEntity.LastModifiedDate = drMerchandise.GetDateTime(lastmodifieddateOrdinal);

                        merchandiseList.Add(merchandiseEntity);
                    }
                }

                return merchandiseList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drMerchandise != null)
                    drMerchandise.Close();
            }
        }

        public bool InsertCustomerMerchandises(MobileCustomerMerchandisingEntity mobileCustomerMerchandisingEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", mobileCustomerMerchandisingEntity.CustomerCode, DbType.String);
                paramCollection.Add("@MerchandiseId", mobileCustomerMerchandisingEntity.MerchandisingId, DbType.Int32);
                paramCollection.Add("@CreatedBy", mobileCustomerMerchandisingEntity.CreatedBy, DbType.String);
                paramCollection.Add("@Display", mobileCustomerMerchandisingEntity.Display, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(MerchandiseSql["InsertCustomerMerchandising"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        /// <summary>
        /// Updates All the records For today, as Deactivated For the customer
        /// (only Todays record for the relevent customer)
        /// </summary>
        /// <param name="objectiveEntity"></param>
        /// <returns></returns>
        public bool DeleteAllCustomerMerchandisesForToday(MobileCustomerMerchandisingEntity mobileCustomerMerchandisingEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", mobileCustomerMerchandisingEntity.CustomerCode, DbType.String);
                paramCollection.Add("@LastModifiedBy", mobileCustomerMerchandisingEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(MerchandiseSql["DeleteAllCustomerMerchandisesForToday"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }
    }
}
