﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;
using Peercore.CRM.Model;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public class AreaDAO : BaseDAO
    {
        private DbSqlAdapter AreaSql { get; set; }

        private void RegisterSql()
        {
            this.AreaSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.AreaSql.xml", ApplicationService.Instance.DbProvider);
        }

        public AreaDAO()
        {
            RegisterSql();
        }

        public AreaDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public AreaEntity CreateObject()
        {
            return AreaEntity.CreateObject();
        }

        public AreaEntity CreateObject(int entityId)
        {
            return AreaEntity.CreateObject(entityId);
        }

        public List<CityAreaEntity> GetAllAreas(ArgsEntity args)
        {
            //CityAreaEntity oArea = null;
            DbDataReader idrArea = null;

            try
            {
                List<CityAreaEntity> cCityArea = new List<CityAreaEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);

                idrArea = this.DataAcessService.ExecuteQuery(AreaSql["GetAllCityAreas"], paramCollection);

                if (idrArea != null && idrArea.HasRows)
                {
                    int area_idOrdinal = idrArea.GetOrdinal("area_id");
                    int area_nameOrdinal = idrArea.GetOrdinal("area_name");
                    int totalCountOrdinal = idrArea.GetOrdinal("total_count");

                    while (idrArea.Read())
                    {
                        CityAreaEntity oArea = CityAreaEntity.CreateObject();

                        if (!idrArea.IsDBNull(area_idOrdinal)) oArea.AreaID = idrArea.GetInt32(area_idOrdinal);
                        if (!idrArea.IsDBNull(area_nameOrdinal)) oArea.AreaCode = idrArea.GetString(area_nameOrdinal).Trim();
                        if (!idrArea.IsDBNull(totalCountOrdinal)) oArea.TotalCount = idrArea.GetInt32(totalCountOrdinal);


                        cCityArea.Add(oArea);
                    }
                }

                return cCityArea;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrArea != null)
                    idrArea.Close();
            }
        }

        public List<CityAreaEntity> GetAllAreaCodes()
        {
            CityAreaEntity oArea = null;
            DbDataReader idrArea = null;

            try
            {
                List<CityAreaEntity> cCityArea = new List<CityAreaEntity>();

                idrArea = this.DataAcessService.ExecuteQuery(AreaSql["GetAllAreaCodes"]);

                if (idrArea != null && idrArea.HasRows)
                {
                    while (idrArea.Read())
                    {
                        oArea = CityAreaEntity.CreateObject();
                        if (!idrArea.IsDBNull(idrArea.GetOrdinal("area_code"))) oArea.AreaCode = idrArea.GetString(idrArea.GetOrdinal("area_code")).Trim();

                        cCityArea.Add(oArea);
                    }
                }

                return cCityArea;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrArea != null)
                    idrArea.Close();
            }
        }

        public bool IsAreaNameExists(CityAreaEntity areaEntity)
        {
            DbDataReader drArea = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();


                paramCollection.Add("@AreaId", areaEntity.AreaID, DbType.Int32);
                paramCollection.Add("@AreaName", areaEntity.AreaName, DbType.String);

                drArea = this.DataAcessService.ExecuteQuery(AreaSql["AreaNameExists"], paramCollection);

                if (drArea != null && drArea.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drArea != null)
                    drArea.Close();
            }
        }

        public bool InsertArea(CityAreaEntity areaEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drArea = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                //paramCollection.Add("@Code", brandEntity.BrandCode, DbType.String);
                paramCollection.Add("@AreaName", areaEntity.AreaName, DbType.String);
                //paramCollection.Add("@CreatedBy", brandEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(AreaSql["InsertArea"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drArea != null)
                    drArea.Close();
            }
        }

        public bool UpdateArea(CityAreaEntity areaEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drArea = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@AreaId", areaEntity.AreaID, DbType.Int32);
                paramCollection.Add("@AreaName", areaEntity.AreaName, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(AreaSql["UpdateArea"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drArea != null)
                    drArea.Close();
            }
        }

        public List<AreaModel> GetAllAreaNew(ArgsModel args)
        {
            AreaModel areaEntity = null;
            DataTable objDS = new DataTable();
            List<AreaModel> AreaList = new List<AreaModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllAreas";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            areaEntity = new AreaModel();
                            if (item["area_id"] != DBNull.Value) areaEntity.AreaId = Convert.ToInt32(item["area_id"]);
                            if (item["region_id"] != DBNull.Value) areaEntity.RegionId = Convert.ToInt32(item["region_id"]);
                            if (item["region_code"] != DBNull.Value) areaEntity.RegionCode = item["region_code"].ToString();
                            if (item["region_name"] != DBNull.Value) areaEntity.RegionName = item["region_name"].ToString();
                            if (item["region_status"] != DBNull.Value) areaEntity.RegionStatus = item["region_status"].ToString();
                            if (item["area_code"] != DBNull.Value) areaEntity.AreaCode = item["area_code"].ToString();
                            if (item["area_name"] != DBNull.Value) areaEntity.AreaName = item["area_name"].ToString();
                            if (item["total_count"] != DBNull.Value) areaEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                            AreaList.Add(areaEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return AreaList;
        }

        public List<AreaModel> GetAllAreaByAreaCode(ArgsModel args, string AreaCode)
        {
            RegionDAO regionObj = null;
            AreaModel areaEntity = null;
            DataTable objDS = new DataTable();
            List<AreaModel> AreaList = new List<AreaModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllAreaByAreaCode";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@AreaCode", AreaCode);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        areaEntity = new AreaModel();
                        if (item["area_id"] != DBNull.Value) areaEntity.AreaId = Convert.ToInt32(item["area_id"]);
                        if (item["region_id"] != DBNull.Value) areaEntity.RegionId = Convert.ToInt32(item["region_id"]);
                        if (item["region_code"] != DBNull.Value) areaEntity.RegionCode = item["region_code"].ToString();
                        if (item["region_name"] != DBNull.Value) areaEntity.RegionName = item["region_name"].ToString();
                        if (item["region_status"] != DBNull.Value) areaEntity.RegionStatus = item["region_status"].ToString();
                        if (item["area_code"] != DBNull.Value) areaEntity.AreaCode = item["area_code"].ToString();
                        if (item["area_name"] != DBNull.Value) areaEntity.AreaName = item["area_name"].ToString();
                        if (item["total_count"] != DBNull.Value) areaEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                        AreaList.Add(areaEntity);
                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }

            return AreaList;
        }
        
        public List<AreaModel> GetAllAreaByAreaName(ArgsModel args, string AreaName)
        {
            RegionDAO regionObj = null;
            AreaModel areaEntity = null;
            DataTable objDS = new DataTable();
            List<AreaModel> AreaList = new List<AreaModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllAreaByAreaName";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@AreaName", AreaName);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        areaEntity = new AreaModel();
                        if (item["area_id"] != DBNull.Value) areaEntity.AreaId = Convert.ToInt32(item["area_id"]);
                        if (item["region_id"] != DBNull.Value) areaEntity.RegionId = Convert.ToInt32(item["region_id"]);
                        if (item["region_code"] != DBNull.Value) areaEntity.RegionCode = item["region_code"].ToString();
                        if (item["region_name"] != DBNull.Value) areaEntity.RegionName = item["region_name"].ToString();
                        if (item["region_status"] != DBNull.Value) areaEntity.RegionStatus = item["region_status"].ToString();
                        if (item["area_code"] != DBNull.Value) areaEntity.AreaCode = item["area_code"].ToString();
                        if (item["area_name"] != DBNull.Value) areaEntity.AreaName = item["area_name"].ToString();
                        if (item["total_count"] != DBNull.Value) areaEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                        AreaList.Add(areaEntity);
                    }
                }

            }
            catch (Exception ex)
            {
                throw;
            }

            return AreaList;
        }

        public List<AreaModel> GetAllAreaByRegionId(ArgsModel args, string RegionId)
        {
            AreaModel areaEntity = null;
            DataTable objDS = new DataTable();
            List<AreaModel> AreaList = new List<AreaModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllAreaByRegionId";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@RegionId", RegionId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            areaEntity = new AreaModel();
                            if (item["area_id"] != DBNull.Value) areaEntity.AreaId = Convert.ToInt32(item["area_id"]);
                            if (item["region_id"] != DBNull.Value) areaEntity.RegionId = Convert.ToInt32(item["region_id"]);
                            if (item["region_code"] != DBNull.Value) areaEntity.RegionCode = item["region_code"].ToString();
                            if (item["region_name"] != DBNull.Value) areaEntity.RegionName = item["region_name"].ToString();
                            if (item["region_status"] != DBNull.Value) areaEntity.RegionStatus = item["region_status"].ToString();
                            if (item["area_code"] != DBNull.Value) areaEntity.AreaCode = item["area_code"].ToString();
                            if (item["area_name"] != DBNull.Value) areaEntity.AreaName = item["area_name"].ToString();
                            if (item["total_count"] != DBNull.Value) areaEntity.TotalCount = Convert.ToInt32(item["total_count"]);
                            AreaList.Add(areaEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return AreaList;
        }

        public bool DeleteAreaFromRegionByAreaId(int areaId)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteAreaFromRegionByAreaId";

                    objCommand.Parameters.AddWithValue("@AreaId", areaId);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }

                    iNoRec = objCommand.ExecuteNonQuery();

                    if (iNoRec > 0)
                        successful = true;
                }
            }
            catch (Exception ex)
            {
                successful = false;
            }

            return successful;
        }
        
        public bool InsertUpdateArea(string region_id, string area_id, string area_code, string area_name)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertUpdateArea";

                    objCommand.Parameters.AddWithValue("@RegionId", region_id);
                    objCommand.Parameters.AddWithValue("@AreaId", area_id);
                    objCommand.Parameters.AddWithValue("@AreaCode", area_code);
                    objCommand.Parameters.AddWithValue("@AreaName", area_name);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }

                    iNoRec = objCommand.ExecuteNonQuery();

                    if (iNoRec > 0)
                        successful = true;
                }
            }
            catch(Exception ex)
            {
                successful = false;
            }
            
            return successful;
        }

        public bool DeleteArea(int areaId)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteArea";

                    objCommand.Parameters.AddWithValue("@AreaId", areaId);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }

                    iNoRec = objCommand.ExecuteNonQuery();

                    if (iNoRec > 0)
                        successful = true;
                }
            }
            catch (Exception ex)
            {
                successful = false;
            }

            return successful;
        }
    }
}
