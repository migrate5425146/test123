﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Entities;
using Peercore.CRM.Shared;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common;
using Peercore.Workflow.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class MessageEntryDAO : BaseDAO 
    {
        private DbSqlAdapter MessageEntrySql { get; set; }

        private void RegisterSql()
        {
            this.MessageEntrySql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.MessageEntrySql.xml", ApplicationService.Instance.DbProvider);
        }

        public MessageEntryDAO()
        {
            RegisterSql();
        }

        public MessageEntryDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public MessagesEntity CreateObject()
        {
            return MessagesEntity.CreateObject();
        }

        public MessagesEntity CreateObject(int entityId)
        {
            return MessagesEntity.CreateObject(entityId);
        }

        public bool Insert(MessagesEntity message, out int newrecordid)
        {
            bool saveSuccessful = false;
            DbInputParameterCollection paramCollection = null;

            try
            {
                paramCollection = new DbInputParameterCollection();
                message.MessageID = Convert.ToInt32(DataAcessService.GetOneValue(MessageEntrySql["GetNextMessageID"]));   // Get Next ID
                newrecordid = message.MessageID;

                paramCollection.Add("@message_id", message.MessageID.ToString(), DbType.String);
                paramCollection.Add("@message", message.Message, DbType.String);
                paramCollection.Add("@originator", message.Originator, DbType.String);
                paramCollection.Add("@message_date", message.MessageDate, DbType.DateTime);
                paramCollection.Add("@del_flag", message.DelFlag, DbType.String);

                saveSuccessful = this.DataAcessService.ExecuteNonQuery(MessageEntrySql["InsertMessage"], paramCollection)>0;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                
            }
            return saveSuccessful;
        }

        public bool Update(MessagesEntity message)
        {
            bool saveSuccessful = false;
            DbInputParameterCollection paramCollection = null;

            try
            {
                paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@message_id", message.MessageID.ToString(), DbType.String);
                paramCollection.Add("@message", message.Message, DbType.String);
                paramCollection.Add("@originator", message.Originator, DbType.String);
                paramCollection.Add("@message_date", message.MessageDate, DbType.DateTime);
                paramCollection.Add("@del_flag", message.DelFlag, DbType.String);

                saveSuccessful = this.DataAcessService.ExecuteNonQuery(MessageEntrySql["UpdateMessage"], paramCollection)>0;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
            return saveSuccessful;
        }

        public bool InsertMessageRep(MessagesEntity message, OriginatorEntity originator)
        {
            bool saveSuccessful = false;
            DbInputParameterCollection paramCollection = null;

            try
            {
                paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@message_id", message.MessageID.ToString(), DbType.String);

                this.DataAcessService.ExecuteNonQuery(MessageEntrySql["DeleteMessageRep"], paramCollection);

                paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@message_id", message.MessageID.ToString(), DbType.String);
                paramCollection.Add("@originator_id", originator.OriginatorId.ToString(), DbType.String);

                saveSuccessful = this.DataAcessService.ExecuteNonQuery(MessageEntrySql["InsertMessageRep"], paramCollection) > 0;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
            return saveSuccessful;
        }

        public List<MessagesEntity> GetMessageByOriginator(string originator)
        {
            DbDataReader dataReader = null;
            List<MessagesEntity> lstMessages = new List<MessagesEntity>();
            MessagesEntity MessagesEntity = null;
            
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@originator", originator, DbType.String);

                dataReader = this.DataAcessService.ExecuteQuery(MessageEntrySql["GetMessageByOriginator"], paramCollection);

                while (dataReader.Read())
                {
                    MessagesEntity = MessagesEntity.CreateObject();
                    MessagesEntity.Message = dataReader.GetString("message");
                    MessagesEntity.MessageID = dataReader.GetInt32("message_id");
                    MessagesEntity.MessageDate = dataReader.GetDateTime("message_date");
                    MessagesEntity.Originator = originator;
                    lstMessages.Add(MessagesEntity);
                }
            }

            catch (Exception)
            {
                
                throw;
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }
            return lstMessages;
        }

        public MessagesEntity GetMessageCount(string originator, string datestring)
        {
            DbDataReader dataReader = null;
           // List<MessagesEntity> lstMessages = new List<MessagesEntity>();
            MessagesEntity messagesEntity = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", originator, DbType.String);
                paramCollection.Add("@DateString", datestring, DbType.String);

                dataReader = this.DataAcessService.ExecuteQuery(MessageEntrySql["GetMessagesCount"], paramCollection);

                while (dataReader.Read())
                {
                    messagesEntity = MessagesEntity.CreateObject();
                    messagesEntity.TotalCount = dataReader.GetInt32("TotalCount");
                    messagesEntity.Message = dataReader.GetString("message");
                    messagesEntity.MessageID = dataReader.GetInt32("message_id");
                    messagesEntity.MessageDate = dataReader.GetDateTime("message_date");
                    messagesEntity.Originator = originator;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }
            return messagesEntity;
        }

        public MessagesEntity GetMessage(int messageId)
        {
            DbDataReader dataReader = null;
            MessagesEntity MessagesEntity = null;
            

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@message_id", messageId.ToString(), DbType.String);

                dataReader = this.DataAcessService.ExecuteQuery(MessageEntrySql["GetMessage"], paramCollection);

                if (dataReader.Read())
                {
                    MessagesEntity = MessagesEntity.CreateObject();
                    MessagesEntity.Message = dataReader.GetString("message");
                    MessagesEntity.MessageDate = dataReader.GetDateTime("message_date");
                }

            }
            catch (Exception)
            {
                
                throw;
            }
            finally
            {
                if (dataReader != null)
                {
                    dataReader.Close();
                }
            }

            return MessagesEntity;
        }

        public bool DeleteMessage(int messageId)
        {
            bool deleteSuccessful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@message_id", messageId.ToString(), DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(MessageEntrySql["DeleteMessageRep"], paramCollection);

                if (iNoRec > 0)
                {
                    iNoRec = this.DataAcessService.ExecuteNonQuery(MessageEntrySql["DeleteMessage"], paramCollection);
                }

                if (iNoRec > 0)
                {
                    deleteSuccessful = true;
                }
            }
            catch (Exception)
            {
                throw;
            }
            //if (iNoRec > 0)
            //{
            //    DataAcessService.Commit();
            //}
            //else
            //{
            //    DataAcessService.Rollback();
            //}
            return deleteSuccessful;
        }

        public bool UpdateMessageRep(string username)
        {
            bool status = false;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@IsNew", "N", DbType.String);
                paramCollection.Add("@Originator", username, DbType.String);

                int iNoRecs = this.DataAcessService.ExecuteNonQuery(MessageEntrySql["UpdateMessageRep"], paramCollection);
                if (iNoRecs > 0)
                    status = true;
            }
            catch (Exception)
            {

                throw;
            }
            return status;
        }
    }
}
