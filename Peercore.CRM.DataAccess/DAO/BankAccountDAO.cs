﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.CRM.Shared;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;

namespace Peercore.CRM.DataAccess.DAO
{
    public class BankAccountDAO: BaseDAO
    {
        private DbSqlAdapter BankAccountSql { get; set; }

        private void RegisterSql()
        {
            this.BankAccountSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.BankAccountSql.xml", ApplicationService.Instance.DbProvider);
        }

        public BankAccountDAO()
        {
            RegisterSql();
        }

        public BankAccountDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public BankAccountEntity CreateObject()
        {
            return BankAccountEntity.CreateObject();
        }

        public BankAccountEntity CreateObject(int entityId)
        {
            return BankAccountEntity.CreateObject(entityId);
        }

        public List<BankAccountEntity> GetBankAccountsForUser(string sourceTypeText, string originator)
        {
            DbDataReader drBankAcoount = null;

            try
            {
                List<BankAccountEntity> bankAccountList = new List<BankAccountEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@SourceTypeText", sourceTypeText, DbType.String);
                paramCollection.Add("@Originator", originator, DbType.String);

                drBankAcoount = this.DataAcessService.ExecuteQuery(BankAccountSql["GetBankAccountsForUser"], paramCollection);

                if (drBankAcoount != null && drBankAcoount.HasRows)
                {
                    int idOrdinal = drBankAcoount.GetOrdinal("id");
                    int accountNumberOrdinal = drBankAcoount.GetOrdinal("account_number");
                    int bankOrdinal = drBankAcoount.GetOrdinal("bank");
                    int isDefaultOrdinal = drBankAcoount.GetOrdinal("is_default");
                    int branchOrdinal = drBankAcoount.GetOrdinal("branch"); 

                    while (drBankAcoount.Read())
                    {
                        BankAccountEntity bankAccountEntity = CreateObject();

                        if (!drBankAcoount.IsDBNull(idOrdinal)) bankAccountEntity.BankAccountId = drBankAcoount.GetInt32(idOrdinal);
                        if (!drBankAcoount.IsDBNull(accountNumberOrdinal)) bankAccountEntity.AccountNumber = drBankAcoount.GetString(accountNumberOrdinal);
                        if (!drBankAcoount.IsDBNull(bankOrdinal)) bankAccountEntity.Bank = drBankAcoount.GetString(bankOrdinal);
                        if (!drBankAcoount.IsDBNull(isDefaultOrdinal)) bankAccountEntity.isDefault = drBankAcoount.GetBoolean(isDefaultOrdinal);
                        if (!drBankAcoount.IsDBNull(branchOrdinal)) bankAccountEntity.Branch = drBankAcoount.GetString(branchOrdinal);

                        bankAccountList.Add(bankAccountEntity);
                    }
                }

                return bankAccountList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drBankAcoount != null)
                    drBankAcoount.Close();
            }
        }

        public bool SaveBankAccounts(BankAccountEntity account)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@SourceId", account.SourceId, DbType.Int32);
                paramCollection.Add("@SourceTypeText", account.SourceTypeText, DbType.String);
                paramCollection.Add("@AccountNo", account.AccountNumber, DbType.String);
                paramCollection.Add("@Bank", account.Bank, DbType.String);
                paramCollection.Add("@branch", account.Branch, DbType.String);
                paramCollection.Add("@IsDefault", account.isDefault, DbType.Boolean);
                paramCollection.Add("@Originator", account.CreatedBy, DbType.String);



                iNoRec = this.DataAcessService.ExecuteNonQuery(BankAccountSql["InsertBankAccounts"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }
    }
}
