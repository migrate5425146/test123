﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;
using System.Data.SqlClient;
using Peercore.CRM.Model;

namespace Peercore.CRM.DataAccess.DAO
{
    public class ActivityDAO:BaseDAO
    {
        private DbSqlAdapter ActivitySql { get; set; }

        private void RegisterSql()
        {
            this.ActivitySql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.ActivitySql.xml", ApplicationService.Instance.DbProvider);
        }

        public ActivityDAO()
        {
            RegisterSql();
        }

        public ActivityDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public ActivityEntity CreateObject()
        {
            return ActivityEntity.CreateObject();
        }

        public ActivityEntity CreateObject(int entityId)
        {
            return ActivityEntity.CreateObject(entityId);
        }
        
        public bool UpdateActivity(ActivityEntity activity)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Subject", ValidateString(activity.Subject), DbType.String);
                paramCollection.Add("@StartDate", activity.StartDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.DateTime);
                paramCollection.Add("@Status", activity.Status, DbType.String);
                paramCollection.Add("@AssignedTo", ValidateString(activity.AssignedTo), DbType.String);
                paramCollection.Add("@LeadId", activity.LeadID, DbType.Int32);
                paramCollection.Add("@Comments", ValidateString(activity.Comments), DbType.String);
                paramCollection.Add("@SentMail", activity.SentMail, DbType.String);
                paramCollection.Add("@Priority", activity.Priority, DbType.String);
                paramCollection.Add("@SendReminder", activity.SendReminder, DbType.String);
                paramCollection.Add("@ReminderDate", activity.ReminderDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.DateTime);
                paramCollection.Add("@LastModifiedBy", ValidateString(activity.LastModifiedBy), DbType.String);
                if (activity.LastModifiedDate.HasValue)
                    paramCollection.Add("@LastModifiedDate", activity.LastModifiedDate.Value.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.DateTime);
                paramCollection.Add("@ActivityType", activity.ActivityType, DbType.String);
                paramCollection.Add("@EndDate", activity.EndDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.DateTime);
                paramCollection.Add("@CustCode", ValidateString(activity.CustomerCode), DbType.String);
                paramCollection.Add("@RepGroupId", activity.RepGroupID, DbType.Int32);
                paramCollection.Add("@ActivityId", activity.ActivityID, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ActivitySql["UpdateActivity"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return UpdateSuccessful;
        }

        public bool InsertActivity(ActivityEntity activity, ref int iReminderActivityID)
        {
            bool UpdateSuccessful = true;
            int iNoRec = 0;

            try
            {
                activity.ActivityID = Convert.ToInt32(DataAcessService.GetOneValue(ActivitySql["GetNextActivityId"]));   // Get Next ID
                iReminderActivityID = activity.ActivityID;
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
               
                paramCollection.Add("@ActivityId", activity.ActivityID, DbType.Int32);
                paramCollection.Add("@Subject", ValidateString(activity.Subject), DbType.String);
                paramCollection.Add("@StartDate", activity.StartDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.DateTime);
                paramCollection.Add("@Status", activity.Status, DbType.String);
                paramCollection.Add("@AssignedTo", ValidateString(activity.AssignedTo), DbType.String);
                paramCollection.Add("@LeadId", activity.LeadID, DbType.Int32);
                paramCollection.Add("@Comments", ValidateString(activity.Comments), DbType.String);
                paramCollection.Add("@SentMail", string.IsNullOrEmpty(activity.SentMail) ? "N" : activity.SentMail, DbType.String);
                paramCollection.Add("@Priority", activity.Priority, DbType.String);
                paramCollection.Add("@SendReminder", activity.SendReminder, DbType.String);
                paramCollection.Add("@ReminderDate", activity.ReminderDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                paramCollection.Add("@CreatedBy", ValidateString(activity.CreatedBy), DbType.String);
                if (activity.CreatedDate.HasValue) 
                    paramCollection.Add("@CreatedDate", activity.CreatedDate.Value.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.DateTime);
                paramCollection.Add("@ActivityType", activity.ActivityType, DbType.String);
                paramCollection.Add("@EndDate", activity.EndDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.DateTime);
                paramCollection.Add("@CustCode", string.IsNullOrEmpty(activity.CustomerCode) ? "" : ValidateString(activity.CustomerCode), DbType.String);
                paramCollection.Add("@RepGroupId", activity.RepGroupID, DbType.Int32);
                paramCollection.Add("@LeadStageId", activity.LeadStageID, DbType.Int32);
                paramCollection.Add("@ParentActivityId", 0, DbType.Int32);
                paramCollection.Add("@EnduserCode", string.IsNullOrEmpty(activity.EndUserCode) ? "" : ValidateString(activity.EndUserCode), DbType.String);
                paramCollection.Add("@CallcycleId", 0, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ActivitySql["InsertActivity"], paramCollection);
                        
                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return UpdateSuccessful;
        }

        public bool InsertCustomerActivity(ActivityEntity customerActivityEntity, string originator, out int appointmentID)
        {
            try
            {
                bool isSuccess = true;
                customerActivityEntity.AppointmentID = System.Convert.ToInt32(DataAcessService.GetOneValue(ActivitySql["GetNextAppointmentID"]));   // Get Next ID
                appointmentID = customerActivityEntity.AppointmentID;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AppointmentId", customerActivityEntity.AppointmentID, DbType.Int32);
                paramCollection.Add("@Subject", ValidateString(customerActivityEntity.Subject), DbType.String);
                paramCollection.Add("@Body", ValidateString(customerActivityEntity.Comments), DbType.String);
                paramCollection.Add("@StartTime", customerActivityEntity.StartDate.ToUniversalTime(), DbType.DateTime);
                paramCollection.Add("@EndTime", customerActivityEntity.EndDate.ToUniversalTime(), DbType.DateTime);
                paramCollection.Add("@Category", ValidateString(customerActivityEntity.ActivityType), DbType.String);
                paramCollection.Add("@CreatedBy", ValidateString(originator), DbType.String);

                isSuccess = this.DataAcessService.ExecuteNonQuery(ActivitySql["InsertAppointment"], paramCollection) > 0;

                return isSuccess;
            }
            catch
            {
                throw;
            }
        }

        public bool UpdateActivityWithAppointment(ActivityEntity customerActivityEntity)
        {
            bool isSuccess = true;

            DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            paramCollection.Add("@AppointmentId", customerActivityEntity.AppointmentID, DbType.Int32);
            paramCollection.Add("@ActivityId", customerActivityEntity.ActivityID, DbType.Int32);
            isSuccess = this.DataAcessService.ExecuteNonQuery(ActivitySql["UpdateActivityWithAppointment"], paramCollection) > 0;

            return isSuccess;
        }

        public bool UpdateLeadCustomer(ActivityEntity activity)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                if (activity.LastModifiedDate.HasValue)
                paramCollection.Add("@LastCalledDate", activity.LastModifiedDate.Value.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.DateTime);



                if (activity.CustomerCode != "" && activity.LeadID == 0)
                {
                    paramCollection.Add("@CustCode", ValidateString(activity.CustomerCode), DbType.String);
                    iNoRec = this.DataAcessService.ExecuteNonQuery(ActivitySql["UpdateCustomerLastCalledDate"], paramCollection);                    
                }
                else
                {
                    paramCollection.Add("@LeadId", activity.LeadID, DbType.Int32);
                    iNoRec = this.DataAcessService.ExecuteNonQuery(ActivitySql["UpdateLeadLastCalledDate"], paramCollection);
                }

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return UpdateSuccessful;
        }

        public string InsertConversionQuery(ActivityEntity activity)
        {
            try
            {
                activity.ActivityID = Convert.ToInt32(DataAcessService.GetOneValue(ActivitySql["GetNextActivityId"]));   // Get Next ID
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                //paramCollection.Add("@ActivityId", activity.ActivityID, DbType.String);
                //paramCollection.Add("@Subject", ValidateString(activity.Subject), DbType.String);
                //paramCollection.Add("@StartDate", activity.StartDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                //paramCollection.Add("@Status", activity.Status, DbType.String);
                //paramCollection.Add("@AssignedTo", ValidateString(activity.AssignedTo), DbType.String);
                //paramCollection.Add("@LeadId", activity.LeadID, DbType.String);
                //paramCollection.Add("@Comments", ValidateString(activity.Comments), DbType.String);
                //paramCollection.Add("@SentMail", activity.SentMail, DbType.String);
                //paramCollection.Add("@Priority", activity.Priority, DbType.String);
                //paramCollection.Add("@SendReminder", activity.SendReminder, DbType.String);
                //paramCollection.Add("@ReminderDate", activity.ReminderDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                //paramCollection.Add("@CreatedBy", ValidateString(activity.CreatedBy), DbType.String);
                //paramCollection.Add("@CreatedDate", activity.LastModifiedDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                //paramCollection.Add("@ActivityType", activity.ActivityType, DbType.String);
                //paramCollection.Add("@EndDate", activity.EndDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                //paramCollection.Add("@CustCode", ValidateString(activity.CustomerCode), DbType.String);
                //paramCollection.Add("@RepGroupId", activity.RepGroupID, DbType.String);
                //paramCollection.Add("@ParentActivityId", 0, DbType.String);
                //paramCollection.Add("@EnduserCode", ValidateString(activity.EndUserCode), DbType.String);
                //paramCollection.Add("@CallcycleId", 0, DbType.String);

                paramCollection.Add("@ActivityId", activity.ActivityID, DbType.Int32);
                paramCollection.Add("@Subject", ValidateString(activity.Subject), DbType.String);
                paramCollection.Add("@StartDate", activity.StartDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                paramCollection.Add("@Status", activity.Status, DbType.String);
                paramCollection.Add("@AssignedTo", ValidateString(activity.AssignedTo), DbType.String);
                paramCollection.Add("@LeadId", activity.LeadID, DbType.Int32);
                paramCollection.Add("@Comments", ValidateString(activity.Comments), DbType.String);
                paramCollection.Add("@SentMail", string.IsNullOrEmpty(activity.SentMail) ? "N" : activity.SentMail, DbType.String);
                paramCollection.Add("@Priority", activity.Priority, DbType.String);
                paramCollection.Add("@SendReminder", activity.SendReminder, DbType.String);
                paramCollection.Add("@ReminderDate", activity.ReminderDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                paramCollection.Add("@CreatedBy", ValidateString(activity.CreatedBy), DbType.String);
                if (activity.LastModifiedDate.HasValue)
                paramCollection.Add("@CreatedDate", activity.LastModifiedDate.Value.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                paramCollection.Add("@ActivityType", activity.ActivityType, DbType.String);
                paramCollection.Add("@EndDate", activity.EndDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                paramCollection.Add("@CustCode", string.IsNullOrEmpty(activity.CustomerCode) ? "" : ValidateString(activity.CustomerCode), DbType.String);
                paramCollection.Add("@RepGroupId", activity.RepGroupID, DbType.Int32);
                paramCollection.Add("@LeadStageId", activity.LeadStageID, DbType.Int32);
                paramCollection.Add("@ParentActivityId", 0, DbType.Int32);
                paramCollection.Add("@EnduserCode", string.IsNullOrEmpty(activity.EndUserCode) ? "" : ValidateString(activity.EndUserCode), DbType.String);
                paramCollection.Add("@CallcycleId", 0, DbType.Int32);

                //paramCollection.Add("@sql", "", DbType.String);

                //this.DataAcessService.ExecuteQuery(LeadSql["UpdatePipelineStageID"], paramCollection);

                //return paramCollection.Find(s => s.Name == "@sql").Value.ToString();



                return this.DataAcessService.GetOneValue(ActivitySql["InsertActivityString"], paramCollection).ToString();
            }
            catch
            {
                throw;
            }
        }

        public List<ActivityEntity> GetRelatedActivity(ArgsEntity args)
        {
            DbDataReader drActivity = null;
            try
            {
                List<ActivityEntity> activityCollection = new List<ActivityEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@LeadId", args.LeadId, DbType.Int32);
                paramCollection.Add("@DepartmentId", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "assigned_to"), DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                drActivity = this.DataAcessService.ExecuteQuery(ActivitySql["GetContactActivityByAssignedTo"], paramCollection);

                if (drActivity != null && drActivity.HasRows)
                {
                    int activityIdOrdinal = drActivity.GetOrdinal("activity_id");
                    int subjectOrdinal = drActivity.GetOrdinal("subject");
                    int dueDateOrdinal = drActivity.GetOrdinal("start_date");
                    int endDateOrdinal = drActivity.GetOrdinal("end_date");
                    int statusOrdinal = drActivity.GetOrdinal("status");
                    int assignedToOrdinal = drActivity.GetOrdinal("assigned_to");
                    int leadIdOrdinal = drActivity.GetOrdinal("lead_id");
                    int commentsOrdinal = drActivity.GetOrdinal("comments");
                    int sentMailOrdinal = drActivity.GetOrdinal("sent_mail");
                    int priorityOrdinal = drActivity.GetOrdinal("priority");
                    int sendReminderOrdinal = drActivity.GetOrdinal("send_reminder");
                    int reminderDateOrdinal = drActivity.GetOrdinal("reminder_date");
                    int activityTypeOrdinal = drActivity.GetOrdinal("activity_type");
                    int statusDescriptionOrdinal = drActivity.GetOrdinal("status_description");
                    int priorityDescriptionOrdinal = drActivity.GetOrdinal("priority_description");
                    int typeDescriptionOrdinal = drActivity.GetOrdinal("type_description");
                    int appointmentIDOrdinal = drActivity.GetOrdinal("appointment_id");
                    int totalCountOrdinal = drActivity.GetOrdinal("total_count");

                    while (drActivity.Read())
                    {
                        ActivityEntity activity = ActivityEntity.CreateObject();

                        if (!drActivity.IsDBNull(activityIdOrdinal)) activity.ActivityID = drActivity.GetInt32(activityIdOrdinal);
                        if (!drActivity.IsDBNull(subjectOrdinal)) activity.Subject = drActivity.GetString(subjectOrdinal);
                        if (!drActivity.IsDBNull(dueDateOrdinal)) activity.StartDate = drActivity.GetDateTime(dueDateOrdinal).ToLocalTime();
                        if (!drActivity.IsDBNull(statusOrdinal)) activity.Status = drActivity.GetString(statusOrdinal);
                        if (!drActivity.IsDBNull(assignedToOrdinal)) activity.AssignedTo = drActivity.GetString(assignedToOrdinal);
                        if (!drActivity.IsDBNull(leadIdOrdinal)) activity.LeadID = drActivity.GetInt32(leadIdOrdinal);
                        if (!drActivity.IsDBNull(commentsOrdinal)) activity.Comments = drActivity.GetString(commentsOrdinal);
                        if (!drActivity.IsDBNull(sentMailOrdinal)) activity.SentMail = drActivity.GetString(sentMailOrdinal);
                        if (!drActivity.IsDBNull(priorityOrdinal)) activity.Priority = drActivity.GetString(priorityOrdinal);
                        if (!drActivity.IsDBNull(sendReminderOrdinal)) activity.SendReminder = drActivity.GetString(sendReminderOrdinal);
                        if (!drActivity.IsDBNull(reminderDateOrdinal)) activity.ReminderDate = drActivity.GetDateTime(reminderDateOrdinal).ToLocalTime();
                        if (!drActivity.IsDBNull(activityTypeOrdinal)) activity.ActivityType = drActivity.GetString(activityTypeOrdinal);
                        if (!drActivity.IsDBNull(statusDescriptionOrdinal)) activity.StatusDescription = drActivity.GetString(statusDescriptionOrdinal);
                        if (!drActivity.IsDBNull(priorityDescriptionOrdinal)) activity.PriorityDescription = drActivity.GetString(priorityDescriptionOrdinal);
                        if (!drActivity.IsDBNull(typeDescriptionOrdinal)) activity.ActivityTypeDescription = drActivity.GetString(typeDescriptionOrdinal);
                        if (!drActivity.IsDBNull(endDateOrdinal)) activity.EndDate = drActivity.GetDateTime(endDateOrdinal).ToLocalTime();
                        if (!drActivity.IsDBNull(appointmentIDOrdinal)) activity.AppointmentID = drActivity.GetInt32(appointmentIDOrdinal);

                        if (!drActivity.IsDBNull(totalCountOrdinal)) activity.TotalCount = drActivity.GetInt32(totalCountOrdinal);
                        
                        activityCollection.Add(activity);

                    }
                }

                return activityCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drActivity != null)
                    drActivity.Close();
            }
        }

        public ActivityEntity GetActivity(int iActivityID)
        {
            DbDataReader drActivity = null;

            try
            {
                ActivityEntity activity = ActivityEntity.CreateObject();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@ActivityId", iActivityID, DbType.Int32);

                drActivity = this.DataAcessService.ExecuteQuery(ActivitySql["GetActivityDetails"], paramCollection);

                if (drActivity != null && drActivity.HasRows)
                {
                    int activityIdOrdinal = drActivity.GetOrdinal("activity_id");
                    int subjectOrdinal = drActivity.GetOrdinal("subject");
                    int startDateOrdinal = drActivity.GetOrdinal("start_date");
                    int statusOrdinal = drActivity.GetOrdinal("status");
                    int assignedToOrdinal = drActivity.GetOrdinal("assigned_to");
                    int leadIdOrdinal = drActivity.GetOrdinal("lead_id");
                    int custCodeOrdinal = drActivity.GetOrdinal("cust_code");

                    int commentsOrdinal = drActivity.GetOrdinal("comments");
                    int sentMailOrdinal = drActivity.GetOrdinal("sent_mail");
                    int priorityOrdinal = drActivity.GetOrdinal("priority");
                    int sendReminderOrdinal = drActivity.GetOrdinal("send_reminder");
                    int reminderDateOrdinal = drActivity.GetOrdinal("reminder_date");

                    int createdByOrdinal = drActivity.GetOrdinal("created_by");
                    int createdDateOrdinal = drActivity.GetOrdinal("created_date");
                    int lastModifiedByOrdinal = drActivity.GetOrdinal("last_modified_by");
                    int lastModifiedDateOrdinal = drActivity.GetOrdinal("last_modified_date");
                    int activityTypeOrdinal = drActivity.GetOrdinal("activity_type");
                    int endDateOrdinal = drActivity.GetOrdinal("end_date");
                    int repGroupIdOrdinal = drActivity.GetOrdinal("rep_group_id");
                    int repGroupNameOrdinal = drActivity.GetOrdinal("rep_group_name");
                    int appointmentIDOrdinal = drActivity.GetOrdinal("appointment_id");
                    int parentActivityIDOrdinal = drActivity.GetOrdinal("parent_activity_id");
                    int enduserCodeOrdinal = drActivity.GetOrdinal("enduser_code");

                    if (drActivity.Read())
                    {
                        if (!drActivity.IsDBNull(activityIdOrdinal)) activity.ActivityID = drActivity.GetInt32(activityIdOrdinal);
                        if (!drActivity.IsDBNull(subjectOrdinal)) activity.Subject = drActivity.GetString(subjectOrdinal);
                        if (!drActivity.IsDBNull(startDateOrdinal)) activity.StartDate = drActivity.GetDateTime(startDateOrdinal).ToLocalTime();
                        if (!drActivity.IsDBNull(statusOrdinal)) activity.Status = drActivity.GetString(statusOrdinal);
                        if (!drActivity.IsDBNull(assignedToOrdinal)) activity.AssignedTo = drActivity.GetString(assignedToOrdinal);
                        if (!drActivity.IsDBNull(leadIdOrdinal)) activity.LeadID = drActivity.GetInt32(leadIdOrdinal);
                        if (!drActivity.IsDBNull(commentsOrdinal)) activity.Comments = drActivity.GetString(commentsOrdinal);
                        if (!drActivity.IsDBNull(sentMailOrdinal)) activity.SentMail = drActivity.GetString(sentMailOrdinal);
                        if (!drActivity.IsDBNull(priorityOrdinal)) activity.Priority = drActivity.GetString(priorityOrdinal);
                        if (!drActivity.IsDBNull(sendReminderOrdinal)) activity.SendReminder = drActivity.GetString(sendReminderOrdinal);
                        if (!drActivity.IsDBNull(reminderDateOrdinal)) activity.ReminderDate = drActivity.GetDateTime(reminderDateOrdinal).ToLocalTime();

                        if (activity.SendReminder == "Y")
                            activity.ReminderCreated = true;

                        if (!drActivity.IsDBNull(custCodeOrdinal)) activity.CustomerCode = drActivity.GetString(custCodeOrdinal);
                        if (!drActivity.IsDBNull(createdByOrdinal)) activity.CreatedBy = drActivity.GetString(createdByOrdinal);
                        if (!drActivity.IsDBNull(createdDateOrdinal)) activity.CreatedDate = drActivity.GetDateTime(createdDateOrdinal);
                        if (!drActivity.IsDBNull(lastModifiedByOrdinal)) activity.LastModifiedBy = drActivity.GetString(lastModifiedByOrdinal);
                        if (!drActivity.IsDBNull(lastModifiedDateOrdinal)) activity.LastModifiedDate = drActivity.GetDateTime(lastModifiedDateOrdinal);
                        if (!drActivity.IsDBNull(activityTypeOrdinal)) activity.ActivityType = drActivity.GetString(activityTypeOrdinal);
                        if (!drActivity.IsDBNull(endDateOrdinal)) activity.EndDate = drActivity.GetDateTime(endDateOrdinal).ToLocalTime();
                        if (!drActivity.IsDBNull(repGroupIdOrdinal)) activity.RepGroupID = drActivity.GetInt32(repGroupIdOrdinal);
                        if (!drActivity.IsDBNull(repGroupNameOrdinal)) activity.RepGroupName = drActivity.GetString(repGroupNameOrdinal);
                        if (!drActivity.IsDBNull(appointmentIDOrdinal)) activity.AppointmentID = drActivity.GetInt32(appointmentIDOrdinal);
                        if (!drActivity.IsDBNull(parentActivityIDOrdinal)) activity.ParentActivityID = drActivity.GetInt32(parentActivityIDOrdinal);
                        if (!drActivity.IsDBNull(enduserCodeOrdinal)) activity.EndUserCode = drActivity.GetString(enduserCodeOrdinal);

                    }
                }

                return activity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drActivity != null)
                    drActivity.Close();
            }
        }

        public List<ActivityEntity> GetActivitiesForCustomer(ArgsEntity args)
        {
            DbDataReader drActivity = null;

            try
            {
                List<ActivityEntity> activityCollection = new List<ActivityEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", args.CustomerCode, DbType.String);
                paramCollection.Add("@DepartmentId", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                drActivity = this.DataAcessService.ExecuteQuery(ActivitySql["GetContactActivityForCustomer"], paramCollection);

                if (drActivity != null && drActivity.HasRows)
                {
                    int activityIdOrdinal = drActivity.GetOrdinal("activity_id");
                    int subjectOrdinal = drActivity.GetOrdinal("subject");
                    int dueDateOrdinal = drActivity.GetOrdinal("start_date");
                    int statusOrdinal = drActivity.GetOrdinal("status");
                    int assignedToOrdinal = drActivity.GetOrdinal("assigned_to");
                    int leadIdOrdinal = drActivity.GetOrdinal("lead_id");
                    int commentsOrdinal = drActivity.GetOrdinal("comments");
                    int sentMailOrdinal = drActivity.GetOrdinal("sent_mail");
                    int priorityOrdinal = drActivity.GetOrdinal("priority");
                    int sendReminderOrdinal = drActivity.GetOrdinal("send_reminder");
                    int reminderDateOrdinal = drActivity.GetOrdinal("reminder_date");
                    int statusDescriptionOrdinal = drActivity.GetOrdinal("status_description");
                    int priorityDescriptionOrdinal = drActivity.GetOrdinal("priority_description");
                    int typeDescriptionOrdinal = drActivity.GetOrdinal("type_description");
                    int endDateOrdinal = drActivity.GetOrdinal("end_date");
                    int appointmentIdOrdinal = drActivity.GetOrdinal("appointment_id");
                    int totcountOrdinal = drActivity.GetOrdinal("totcount");

                    while (drActivity.Read())
                    {
                        ActivityEntity activity = ActivityEntity.CreateObject();

                        if (!drActivity.IsDBNull(activityIdOrdinal)) activity.ActivityID = drActivity.GetInt32(activityIdOrdinal);
                        if (!drActivity.IsDBNull(subjectOrdinal)) activity.Subject = drActivity.GetString(subjectOrdinal);
                        if (!drActivity.IsDBNull(dueDateOrdinal)) activity.StartDate = drActivity.GetDateTime(dueDateOrdinal);
                        if (!drActivity.IsDBNull(statusOrdinal)) activity.Status = drActivity.GetString(statusOrdinal);
                        if (!drActivity.IsDBNull(assignedToOrdinal)) activity.AssignedTo = drActivity.GetString(assignedToOrdinal);
                        if (!drActivity.IsDBNull(leadIdOrdinal)) activity.LeadID = drActivity.GetInt32(leadIdOrdinal);
                        if (!drActivity.IsDBNull(commentsOrdinal)) activity.Comments = drActivity.GetString(commentsOrdinal);
                        if (!drActivity.IsDBNull(sentMailOrdinal)) activity.SentMail = drActivity.GetString(sentMailOrdinal);
                        if (!drActivity.IsDBNull(priorityOrdinal)) activity.Priority = drActivity.GetString(priorityOrdinal);
                        if (!drActivity.IsDBNull(sendReminderOrdinal)) activity.SendReminder = drActivity.GetString(sendReminderOrdinal);
                        if (!drActivity.IsDBNull(reminderDateOrdinal)) activity.ReminderDate = drActivity.GetDateTime(reminderDateOrdinal);
                        if (!drActivity.IsDBNull(statusDescriptionOrdinal)) activity.StatusDescription = drActivity.GetString(statusDescriptionOrdinal);
                        if (!drActivity.IsDBNull(priorityDescriptionOrdinal)) activity.PriorityDescription = drActivity.GetString(priorityDescriptionOrdinal);
                        if (!drActivity.IsDBNull(typeDescriptionOrdinal)) activity.ActivityTypeDescription = drActivity.GetString(typeDescriptionOrdinal);
                        if (!drActivity.IsDBNull(endDateOrdinal)) activity.EndDate = drActivity.GetDateTime(endDateOrdinal);
                        if (!drActivity.IsDBNull(appointmentIdOrdinal)) activity.AppointmentID = drActivity.GetInt32(appointmentIdOrdinal);
                        if (!drActivity.IsDBNull(totcountOrdinal)) activity.TotalCount = drActivity.GetInt32(totcountOrdinal);

                        activityCollection.Add(activity);

                    }
                }

                return activityCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drActivity != null)
                    drActivity.Close();
            }
        }

        public List<CustomerActivityEntity> GetAllActivities(ArgsEntity args, string repTerritory = "")
        {
            //CustomerActivityEntity activity = new CustomerActivityEntity();
            //DbDataReader idrActivity = null;
            //try
            //{
            //    List<CustomerActivityEntity> Activities = new List<CustomerActivityEntity>();

            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@Originator", args.Originator, DbType.String);
            //    paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "a.created_by"), DbType.String);
            //    //paramCollection.Add("@repGroups", sRepGroups, DbType.String);
            //    paramCollection.Add("@today", args.SToday, DbType.String);
            //    paramCollection.Add("@todaystart", DateTime.Today.ToString("yyyy/MM/dd") + " 00:00:00", DbType.String);
            //    paramCollection.Add("@todayend", DateTime.Today.ToString("yyyy/MM/dd") + " 23:59:59", DbType.String);

            //    paramCollection.Add("@startdate", string.IsNullOrEmpty(args.SStartDate) ? "" : DateTime.Parse(args.SStartDate).ToString("yyyy/MM/dd") + " 00:00:00", DbType.String);
            //    paramCollection.Add("@enddate", string.IsNullOrEmpty(args.SEndDate) ? "" : DateTime.Parse(args.SEndDate).ToString("yyyy/MM/dd") + " 23:59:59", DbType.String);
     
            //    paramCollection.Add("@source", args.SSource, DbType.String);
            //    paramCollection.Add("@floor", string.IsNullOrEmpty(args.Floor) ? "" : args.Floor, DbType.String);
            //    paramCollection.Add("@status", args.Status, DbType.String);

            //    paramCollection.Add("@defaultDeptID", args.DefaultDepartmentId, DbType.String);
            //    paramCollection.Add("@territory", repTerritory, DbType.String);
            //    paramCollection.Add("@ColumnsToGet", "b.*", DbType.String);
            //    paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
            //    paramCollection.Add("@RepCode", args.RepCode, DbType.String);

            //    idrActivity = this.DataAcessService.ExecuteQuery(ActivitySql["GetAllActivitiesRepTerritory"], paramCollection);
            //    if (idrActivity != null && idrActivity.HasRows)
            //    {
            //        int activityIdOrdinal = idrActivity.GetOrdinal("activity_id");
            //        int subjectOrdinal = idrActivity.GetOrdinal("subject");
            //        int startDateOrdinal = idrActivity.GetOrdinal("start_date");
            //        int endDateOrdinal = idrActivity.GetOrdinal("end_date");
            //        int statusOrdinal = idrActivity.GetOrdinal("status");
            //        int assignedToOrdinal = idrActivity.GetOrdinal("assigned_to");
            //        int leadIdOrdinal = idrActivity.GetOrdinal("lead_id");
            //        int commentsOrdinal = idrActivity.GetOrdinal("comments");
            //        int sentMailOrdinal = idrActivity.GetOrdinal("sent_mail");
            //        int priorityOrdinal = idrActivity.GetOrdinal("priority");
            //        int sendReminderOrdinal = idrActivity.GetOrdinal("send_reminder");
            //        int reminderDateOrdinal = idrActivity.GetOrdinal("reminder_date");
            //        int activityTypeOrdinal = idrActivity.GetOrdinal("activity_type");
            //    //    int leadNameOrdinal = idrActivity.GetOrdinal("lead_name");
            //        int statusDescOrdinal = idrActivity.GetOrdinal("status_desc");
            //        int typeDescOrdinal = idrActivity.GetOrdinal("type_desc");
            //      //  int leadStageOrdinal = idrActivity.GetOrdinal("lead_stage");
            //      //  int custCodeOrdinal = idrActivity.GetOrdinal("cust_code");
            //        int appoinmentidOrdinal = idrActivity.GetOrdinal("appointment_id");
            //    //    int enduserCodeOrdinal = idrActivity.GetOrdinal("enduser_code");
            //        int repCodeOrdinal = idrActivity.GetOrdinal("rep_code");
            //        int repNameOrdinal = idrActivity.GetOrdinal("rep_name");
            //        int routeIdOrdinal = idrActivity.GetOrdinal("route_id");

            //        while (idrActivity.Read())
            //        {
            //            activity = CustomerActivityEntity.CreateObject();

            //            if (!idrActivity.IsDBNull(activityIdOrdinal)) activity.ActivityID = idrActivity.GetInt32(activityIdOrdinal);
            //            if (!idrActivity.IsDBNull(subjectOrdinal)) activity.Subject = idrActivity.GetString(subjectOrdinal);
            //            if (!idrActivity.IsDBNull(startDateOrdinal)) activity.StartDate = idrActivity.GetDateTime(startDateOrdinal);
            //            if (!idrActivity.IsDBNull(endDateOrdinal)) activity.EndDate = idrActivity.GetDateTime(endDateOrdinal);
            //            if (!idrActivity.IsDBNull(statusOrdinal)) activity.Status = idrActivity.GetString(statusOrdinal);
            //            if (!idrActivity.IsDBNull(assignedToOrdinal)) activity.AssignedTo = idrActivity.GetString(assignedToOrdinal);
            //            if (!idrActivity.IsDBNull(leadIdOrdinal)) activity.LeadId = idrActivity.GetInt32(leadIdOrdinal);
            //            if (!idrActivity.IsDBNull(commentsOrdinal)) activity.Comments = idrActivity.GetString(commentsOrdinal);
            //            if (!idrActivity.IsDBNull(sentMailOrdinal)) activity.SentMail = idrActivity.GetString(sentMailOrdinal);
            //            if (!idrActivity.IsDBNull(priorityOrdinal)) activity.Priority = idrActivity.GetString(priorityOrdinal);
            //            if (!idrActivity.IsDBNull(sendReminderOrdinal)) activity.SendReminder = idrActivity.GetString(sendReminderOrdinal);
            //            if (!idrActivity.IsDBNull(reminderDateOrdinal)) activity.ReminderDate = idrActivity.GetDateTime(reminderDateOrdinal);
            //            if (!idrActivity.IsDBNull(activityTypeOrdinal)) activity.ActivityType = idrActivity.GetString(activityTypeOrdinal);
            //            if (!idrActivity.IsDBNull(statusDescOrdinal)) activity.StatusDesc = idrActivity.GetString(statusDescOrdinal);
            //            if (!idrActivity.IsDBNull(typeDescOrdinal)) activity.TypeDesc = idrActivity.GetString(typeDescOrdinal);
            //            if (!idrActivity.IsDBNull(appoinmentidOrdinal)) activity.AppointmentId = idrActivity.GetInt32(appoinmentidOrdinal);  
            //            if (!idrActivity.IsDBNull(repCodeOrdinal)) activity.RepCode = idrActivity.GetString(repCodeOrdinal);
            //            if (!idrActivity.IsDBNull(repNameOrdinal)) activity.RepName = idrActivity.GetString(repNameOrdinal);
            //            if (!idrActivity.IsDBNull(routeIdOrdinal)) activity.routeId = idrActivity.GetInt32(routeIdOrdinal);

            //            Activities.Add(activity);
            //        }
            //    }

            //    return Activities;
            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (idrActivity != null)
            //        idrActivity.Close();
            //}




            CustomerActivityEntity objModel = null;
            DataTable objDS = new DataTable();
            List<CustomerActivityEntity> objList = new List<CustomerActivityEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllActivitiesRepTerritoryNew";

                    objCommand.Parameters.AddWithValue("@Originator", args.Originator);
                    objCommand.Parameters.AddWithValue("@ChildOriginators", args.ChildOriginators.Replace("originator", "a.created_by"));
                    objCommand.Parameters.AddWithValue("@today", args.SToday);
                    objCommand.Parameters.AddWithValue("@todaystart", DateTime.Today.ToString("yyyy/MM/dd") + " 00:00:00");
                    objCommand.Parameters.AddWithValue("@todayend", DateTime.Today.ToString("yyyy/MM/dd") + " 23:59:59");
                    objCommand.Parameters.AddWithValue("@startdate", string.IsNullOrEmpty(args.SStartDate) ? "" : DateTime.Parse(args.SStartDate).ToString("yyyy/MM/dd") + " 00:00:00");
                    objCommand.Parameters.AddWithValue("@enddate", string.IsNullOrEmpty(args.SEndDate) ? "" : DateTime.Parse(args.SEndDate).ToString("yyyy/MM/dd") + " 23:59:59");
                    objCommand.Parameters.AddWithValue("@source", args.SSource);
                    objCommand.Parameters.AddWithValue("@floor", string.IsNullOrEmpty(args.Floor) ? "" : args.Floor);
                    objCommand.Parameters.AddWithValue("@status", args.Status);
                    objCommand.Parameters.AddWithValue("@defaultDeptID", args.DefaultDepartmentId);
                    objCommand.Parameters.AddWithValue("@territory", repTerritory);
                    objCommand.Parameters.AddWithValue("@ColumnsToGet", "b.*");
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@RepCode", args.RepCode);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new CustomerActivityEntity();
                            if (item["activity_id"] != DBNull.Value) objModel.ActivityID = Convert.ToInt32(item["activity_id"]);
                            if (item["subject"] != DBNull.Value) objModel.Subject = item["subject"].ToString();
                            if (item["start_date"] != DBNull.Value) objModel.StartDate = Convert.ToDateTime(item["start_date"]);
                            if (item["end_date"] != DBNull.Value) objModel.EndDate = Convert.ToDateTime(item["end_date"]);
                            if (item["status"] != DBNull.Value) objModel.Status = item["status"].ToString();
                            if (item["assigned_to"] != DBNull.Value) objModel.AssignedTo = item["assigned_to"].ToString();
                            if (item["lead_id"] != DBNull.Value) objModel.LeadId = Convert.ToInt32(item["lead_id"]);
                            if (item["comments"] != DBNull.Value) objModel.Comments = item["comments"].ToString();
                            if (item["sent_mail"] != DBNull.Value) objModel.SentMail = item["sent_mail"].ToString();
                            if (item["priority"] != DBNull.Value) objModel.Priority = item["priority"].ToString();
                            if (item["send_reminder"] != DBNull.Value) objModel.SendReminder = item["send_reminder"].ToString();
                            if (item["reminder_date"] != DBNull.Value) objModel.ReminderDate = Convert.ToDateTime(item["reminder_date"]);
                            if (item["activity_type"] != DBNull.Value) objModel.ActivityType = item["activity_type"].ToString();
                            if (item["status_desc"] != DBNull.Value) objModel.StatusDesc = item["status_desc"].ToString();
                            if (item["type_desc"] != DBNull.Value) objModel.TypeDesc = item["type_desc"].ToString();
                            if (item["appointment_id"] != DBNull.Value) objModel.AppointmentId = Convert.ToInt32(item["appointment_id"]);
                            if (item["rep_code"] != DBNull.Value) objModel.RepCode = item["rep_code"].ToString();
                            if (item["rep_name"] != DBNull.Value) objModel.RepName = item["rep_name"].ToString();
                            if (item["route_id"] != DBNull.Value) objModel.routeId = Convert.ToInt32(item["route_id"]);

                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public List<CustomerActivityModel> GetAllActivitiesNew(ArgsModel args, string repTerritory = "")
        {
            CustomerActivityModel objModel = null;
            DataTable objDS = new DataTable();
            List<CustomerActivityModel> objList = new List<CustomerActivityModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllActivitiesRepTerritoryNew";

                    objCommand.Parameters.AddWithValue("@Originator", args.Originator);
                    objCommand.Parameters.AddWithValue("@ChildOriginators", args.ChildOriginators.Replace("originator", "a.created_by"));
                    objCommand.Parameters.AddWithValue("@today", args.SToday);
                    objCommand.Parameters.AddWithValue("@todaystart", DateTime.Today.ToString("yyyy/MM/dd") + " 00:00:00");
                    objCommand.Parameters.AddWithValue("@todayend", DateTime.Today.ToString("yyyy/MM/dd") + " 23:59:59");
                    objCommand.Parameters.AddWithValue("@startdate", string.IsNullOrEmpty(args.SStartDate) ? "" : DateTime.Parse(args.SStartDate).ToString("yyyy/MM/dd") + " 00:00:00");
                    objCommand.Parameters.AddWithValue("@enddate", string.IsNullOrEmpty(args.SEndDate) ? "" : DateTime.Parse(args.SEndDate).ToString("yyyy/MM/dd") + " 23:59:59");
                    objCommand.Parameters.AddWithValue("@source", args.SSource);
                    objCommand.Parameters.AddWithValue("@floor", string.IsNullOrEmpty(args.Floor) ? "" : args.Floor);
                    objCommand.Parameters.AddWithValue("@status", args.Status);
                    objCommand.Parameters.AddWithValue("@defaultDeptID", args.DefaultDepartmentId);
                    objCommand.Parameters.AddWithValue("@territory", repTerritory);
                    objCommand.Parameters.AddWithValue("@ColumnsToGet", "b.*");
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@RepCode", args.RepCode);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new CustomerActivityModel();
                            if (item["activity_id"] != DBNull.Value) objModel.ActivityID = Convert.ToInt32(item["activity_id"]);
                            if (item["subject"] != DBNull.Value) objModel.Subject = item["subject"].ToString();
                            if (item["start_date"] != DBNull.Value) objModel.StartDate = Convert.ToDateTime(item["start_date"]);
                            if (item["end_date"] != DBNull.Value) objModel.EndDate = Convert.ToDateTime(item["end_date"]);
                            if (item["status"] != DBNull.Value) objModel.Status = item["status"].ToString();
                            if (item["assigned_to"] != DBNull.Value) objModel.AssignedTo = item["assigned_to"].ToString();
                            if (item["lead_id"] != DBNull.Value) objModel.LeadId = Convert.ToInt32(item["lead_id"]);
                            if (item["comments"] != DBNull.Value) objModel.Comments = item["comments"].ToString();
                            if (item["sent_mail"] != DBNull.Value) objModel.SentMail = item["sent_mail"].ToString();
                            if (item["priority"] != DBNull.Value) objModel.Priority = item["priority"].ToString();
                            if (item["send_reminder"] != DBNull.Value) objModel.SendReminder = item["send_reminder"].ToString();
                            if (item["reminder_date"] != DBNull.Value) objModel.ReminderDate = Convert.ToDateTime(item["reminder_date"]);
                            if (item["activity_type"] != DBNull.Value) objModel.ActivityType = item["activity_type"].ToString();
                            if (item["status_desc"] != DBNull.Value) objModel.StatusDesc = item["status_desc"].ToString();
                            if (item["type_desc"] != DBNull.Value) objModel.TypeDesc = item["type_desc"].ToString();
                            if (item["appointment_id"] != DBNull.Value) objModel.AppointmentId = Convert.ToInt32(item["appointment_id"]);
                            if (item["rep_code"] != DBNull.Value) objModel.RepCode = item["rep_code"].ToString();
                            if (item["rep_name"] != DBNull.Value) objModel.RepName = item["rep_name"].ToString();
                            if (item["route_id"] != DBNull.Value) objModel.routeId = Convert.ToInt32(item["route_id"]);

                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public List<CustomerActivityEntity> GetActivityCountByType(ArgsEntity args)
        {
            CustomerActivityEntity activity = null;
            DbDataReader idrActivity = null;

            try
            {
                List<CustomerActivityEntity> Activities = new List<CustomerActivityEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "assigned_to"), DbType.String);
                

                paramCollection.Add("@startdate", string.IsNullOrEmpty(args.SStartDate) ? "" : DateTime.Parse(args.SStartDate).ToString("yyyy/MM/dd") + " 00:00:00", DbType.String);
                paramCollection.Add("@enddate", string.IsNullOrEmpty(args.SEndDate) ? "" : DateTime.Parse(args.SEndDate).ToString("yyyy/MM/dd") + " 23:59:59", DbType.String);

                paramCollection.Add("@status", args.Status, DbType.String);

                paramCollection.Add("@defaultDeptID", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@ColumnsToGet", "b.activity_type, IFNULL(b.type_desc, '') AS type_desc, COUNT(*) AS ActCount", DbType.String);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                idrActivity = this.DataAcessService.ExecuteQuery(ActivitySql["GetActivityCountByType"], paramCollection);

                if (idrActivity != null && idrActivity.HasRows)
                {
                    int activityTypeOrdinal = idrActivity.GetOrdinal("activity_type");
                    int typeDescOrdinal = idrActivity.GetOrdinal("type_desc");
                    int actCountOrdinal = idrActivity.GetOrdinal("ActCount");

                    while (idrActivity.Read())
                    {
                        activity = CustomerActivityEntity.CreateObject();

                        //if (!idrActivity.IsDBNull(activityTypeOrdinal))
                        //{
                        //    if (idrActivity.GetString(activityTypeOrdinal) == "")
                        //        activity.ActivityType = "COLD CALL";
                        //    else
                        //        activity.ActivityType = idrActivity.GetString(activityTypeOrdinal);
                        //}

                        //if (!idrActivity.IsDBNull(typeDescOrdinal))
                        //{
                        //    if (idrActivity.GetString(typeDescOrdinal) == "")
                        //        activity.TypeDesc = "Cold Call";
                        //    else
                        //        activity.TypeDesc = idrActivity.GetString(typeDescOrdinal);
                        //}

                        if (!idrActivity.IsDBNull(activityTypeOrdinal)) activity.ActivityType = idrActivity.GetString(activityTypeOrdinal);
                        if (!idrActivity.IsDBNull(typeDescOrdinal)) activity.TypeDesc = idrActivity.GetString(typeDescOrdinal);
                        if (!idrActivity.IsDBNull(actCountOrdinal)) activity.NoOfActivities = idrActivity.GetInt32(actCountOrdinal);

                        Activities.Add(activity);
                    }
                }

                return Activities;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrActivity != null)
                    idrActivity.Close();
            }
        }

        public List<CustomerActivityEntity> GetAllActivitiesByType(ArgsEntity args , string sActivityType = "")
        {
            CustomerActivityEntity activity = null;
            DbDataReader idrActivity = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "assigned_to"), DbType.String);
                paramCollection.Add("@defaultDeptID", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@startdate", string.IsNullOrEmpty(args.SStartDate) ? "" : DateTime.Parse(args.SStartDate).ToString("dd-MMM-yyyy") + " 00:00:00", DbType.String);
                paramCollection.Add("@enddate", string.IsNullOrEmpty(args.SEndDate) ? "" : DateTime.Parse(args.SEndDate).ToString("dd-MMM-yyyy") + " 23:59:59", DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@ActivityStatus", args.ActivityStatus, DbType.String);
                paramCollection.Add("@Sector", args.Sector, DbType.String);
                paramCollection.Add("@ActivityType", sActivityType, DbType.String);            
                paramCollection.Add("@ColumnsToGet", "b.*", DbType.String);

                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                List<CustomerActivityEntity> Activities = new List<CustomerActivityEntity>();
                idrActivity = this.DataAcessService.ExecuteQuery(ActivitySql["GetAllActivitiesByTypeData"], paramCollection);

                if (idrActivity != null && idrActivity.HasRows)
                {
                    int activityIdOrdinal = idrActivity.GetOrdinal("activity_id");
                    int subjectOrdinal = idrActivity.GetOrdinal("subject");
                    int startDateOrdinal = idrActivity.GetOrdinal("start_date");
                    int endDateOrdinal = idrActivity.GetOrdinal("end_date");
                    int statusOrdinal = idrActivity.GetOrdinal("status");
                    int assignedToOrdinal = idrActivity.GetOrdinal("assigned_to");
                    int leadIdOrdinal = idrActivity.GetOrdinal("lead_id");
                    int commentsOrdinal = idrActivity.GetOrdinal("comments");
                    int sentMailOrdinal = idrActivity.GetOrdinal("sent_mail");
                    int priorityOrdinal = idrActivity.GetOrdinal("priority");
                    int sendReminderOrdinal = idrActivity.GetOrdinal("send_reminder");
                    int reminderDateOrdinal = idrActivity.GetOrdinal("reminder_date");
                    int activityTypeOrdinal = idrActivity.GetOrdinal("activity_type");
                    int leadNameOrdinal = idrActivity.GetOrdinal("lead_name");
                    int statusDescOrdinal = idrActivity.GetOrdinal("status_desc");
                    int typeDescOrdinal = idrActivity.GetOrdinal("type_desc");
                    int leadStageOrdinal = idrActivity.GetOrdinal("lead_stage");
                    int custCodeOrdinal = idrActivity.GetOrdinal("cust_code");

                    int endUserCodeOrdinal = idrActivity.GetOrdinal("enduser_code");
                    int totalCountOrdinal = idrActivity.GetOrdinal("total_count");

                    while (idrActivity.Read())
                    {
                        activity = CustomerActivityEntity.CreateObject();

                        if (!idrActivity.IsDBNull(activityIdOrdinal)) activity.ActivityID = idrActivity.GetInt32(activityIdOrdinal);
                        if (!idrActivity.IsDBNull(subjectOrdinal)) activity.Subject = idrActivity.GetString(subjectOrdinal);
                        if (!idrActivity.IsDBNull(startDateOrdinal)) activity.StartDate = idrActivity.GetDateTime(startDateOrdinal).ToLocalTime();
                        if (!idrActivity.IsDBNull(endDateOrdinal)) activity.EndDate = idrActivity.GetDateTime(endDateOrdinal).ToLocalTime();
                        if (!idrActivity.IsDBNull(statusOrdinal)) activity.Status = idrActivity.GetString(statusOrdinal);
                        if (!idrActivity.IsDBNull(assignedToOrdinal)) activity.AssignedTo = idrActivity.GetString(assignedToOrdinal);
                        if (!idrActivity.IsDBNull(leadIdOrdinal)) activity.LeadId = idrActivity.GetInt32(leadIdOrdinal);
                        if (!idrActivity.IsDBNull(commentsOrdinal)) activity.Comments = idrActivity.GetString(commentsOrdinal);
                        if (!idrActivity.IsDBNull(sentMailOrdinal)) activity.SentMail = idrActivity.GetString(sentMailOrdinal);
                        if (!idrActivity.IsDBNull(priorityOrdinal)) activity.Priority = idrActivity.GetString(priorityOrdinal);
                        if (!idrActivity.IsDBNull(sendReminderOrdinal)) activity.SendReminder = idrActivity.GetString(sendReminderOrdinal);
                        if (!idrActivity.IsDBNull(reminderDateOrdinal)) activity.ReminderDate = idrActivity.GetDateTime(reminderDateOrdinal).ToLocalTime();
                        //if (!idrActivity.IsDBNull(activityTypeOrdinal))
                        //{
                        //    if (idrActivity.GetString(activityTypeOrdinal) == "")
                        //        activity.ActivityType = "COLD CALL";
                        //    else
                        //        activity.ActivityType = idrActivity.GetString(activityTypeOrdinal);
                        //}
                        if (!idrActivity.IsDBNull(activityTypeOrdinal)) activity.ActivityType = idrActivity.GetString(activityTypeOrdinal);
                        if (!idrActivity.IsDBNull(leadNameOrdinal)) activity.LeadName = idrActivity.GetString(leadNameOrdinal);
                        if (!idrActivity.IsDBNull(statusDescOrdinal)) activity.StatusDesc = idrActivity.GetString(statusDescOrdinal);
                        //if (!idrActivity.IsDBNull(typeDescOrdinal))
                        //    activity.TypeDesc = idrActivity.GetString(typeDescOrdinal);
                        //else
                        //    activity.TypeDesc = "Cold Call";
                        if (!idrActivity.IsDBNull(typeDescOrdinal)) activity.TypeDesc = idrActivity.GetString(typeDescOrdinal);

                        if (!idrActivity.IsDBNull(leadStageOrdinal)) activity.LeadStage = idrActivity.GetString(leadStageOrdinal);
                        if (!idrActivity.IsDBNull(custCodeOrdinal)) activity.CustCode = idrActivity.GetString(custCodeOrdinal);

                        if (!idrActivity.IsDBNull(endUserCodeOrdinal)) activity.EndUserCode = idrActivity.GetString(endUserCodeOrdinal);
                        if (!idrActivity.IsDBNull(totalCountOrdinal)) activity.TotalCount = idrActivity.GetInt32(totalCountOrdinal);

                        Activities.Add(activity);
                    }
                }

                return Activities;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrActivity != null)
                    idrActivity.Close();
            }
        }

        public int ChangeActivityDate(ActivityEntity activity, bool UpdateFromCalendar = false)
        {
            int iNoRec = 0;
            try
            {
                if (UpdateFromCalendar == false)
                {
                    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                    paramCollection.Add("@StartDate", activity.StartDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                    paramCollection.Add("@EndDate", activity.EndDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                    paramCollection.Add("@LastModifiedBy", ValidateString(activity.LastModifiedBy), DbType.String);
                    if (activity.LastModifiedDate.HasValue)
                    paramCollection.Add("@LastModifiedDate", activity.LastModifiedDate.Value.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                 //   paramCollection.Add("@ActivityID", activity.ActivityID, DbType.Int32);
                    paramCollection.Add("@AppointmentId", activity.AppointmentID, DbType.Int32); 
                    paramCollection.Add("@activityType", activity.ActivityType, DbType.String);
                    

                    iNoRec = this.DataAcessService.ExecuteNonQuery(ActivitySql["ChangeActivityDate"], paramCollection);
                }
                else if (activity.AppointmentID > 0 && UpdateFromCalendar == true)
                {
                    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                    paramCollection.Add("@Comments", activity.Comments, DbType.String);
                    paramCollection.Add("@LastModifiedBy", ValidateString(activity.LastModifiedBy), DbType.String);
                    if (activity.LastModifiedDate.HasValue)
                    paramCollection.Add("@LastModifiedDate", activity.LastModifiedDate.Value.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.DateTime);
                    paramCollection.Add("@AppointmentID", activity.AppointmentID, DbType.Int32); ;

                    iNoRec = this.DataAcessService.ExecuteNonQuery(ActivitySql["UpdateActivityComments"], paramCollection);
                }
                return iNoRec;
            }
            catch
            {
                throw;
            }
        }

        public List<LeadCustomerViewEntity> GetNotCalledCustomers(ArgsEntity args)
        {
            //string sWhereCls1 = "";
            //string sWhereCls2 = "";
            //clsOriginator oOriginator;
            DbDataReader idrCall = null;
            DateTime dtEndDate;

            DateTime dtStartDate = new DateTime(args.SYear, args.SMonth, 1);

            if (DateTime.Today.Year == args.SYear && DateTime.Today.Month == args.SMonth)
                dtEndDate = DateTime.Today;
            else
                dtEndDate = dtStartDate.AddMonths(1).AddDays(-1);

            //sWhereCls2 = " AND a.start_date >= '" + DateTime.Parse(dtStartDate.ToString("dd-MMM-yyyy 00:00:00")).ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss")
            //            + "' and a.start_date <= '" + DateTime.Parse(dtEndDate.ToString("dd-MMM-yyyy 23:59:59")).ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss") + "'";

            //if (args.Originator != "")
            //{
            //    sWhereCls1 += " AND " + args.ChildOriginators;
            //}

            try
            {
                List<LeadCustomerViewEntity> leadCustomers = new List<LeadCustomerViewEntity>();

                LeadCustomerViewEntity oLeadCustomer = null;

                //idrCall = (IngresDataReader)oDataHandle.ExecuteSelect(string.Format(CRMSQLs.GetNotCalledCustomers,
                //    sWhereCls1, sWhereCls2, DefaultDeptID));
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@FromDate", DateTime.Parse(dtStartDate.ToString("dd-MMM-yyyy 00:00:00")).ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                paramCollection.Add("@ToDate", DateTime.Parse(dtEndDate.ToString("dd-MMM-yyyy 23:59:59")).ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                paramCollection.Add("@ChildOriginators", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@DepartmentId", args.DefaultDepartmentId, DbType.String);
                idrCall = this.DataAcessService.ExecuteQuery(ActivitySql["GetNotCalledCustomers"], paramCollection);

                if (idrCall != null)
                {
                    while (idrCall.Read())
                    {
                        oLeadCustomer = LeadCustomerViewEntity.CreateObject();

                        //if (!idrCall.IsDBNull(idrCall.GetOrdinal("lead_id")))
                        //    oLeadCustomer.SourceId = idrCall.GetInt32(idrCall.GetOrdinal("lead_id"));
                        //if (!idrCall.IsDBNull(idrCall.GetOrdinal("Originator")))
                        //    oLeadCustomer.Originator = idrCall.GetString(idrCall.GetOrdinal("Originator"));
                        if (!idrCall.IsDBNull(idrCall.GetOrdinal("ContactName")))
                            oLeadCustomer.Name = idrCall.GetString(idrCall.GetOrdinal("ContactName"));
                        if (!idrCall.IsDBNull(idrCall.GetOrdinal("business")))
                            oLeadCustomer.Business = idrCall.GetString(idrCall.GetOrdinal("business"));
                        if (!idrCall.IsDBNull(idrCall.GetOrdinal("industry")))
                            oLeadCustomer.Industry = idrCall.GetString(idrCall.GetOrdinal("industry"));
                        if (!idrCall.IsDBNull(idrCall.GetOrdinal("address")))
                            oLeadCustomer.Address = idrCall.GetString(idrCall.GetOrdinal("address"));
                        if (!idrCall.IsDBNull(idrCall.GetOrdinal("city")))
                            oLeadCustomer.City = idrCall.GetString(idrCall.GetOrdinal("city"));
                        if (!idrCall.IsDBNull(idrCall.GetOrdinal("state")))
                            oLeadCustomer.State = idrCall.GetString(idrCall.GetOrdinal("state"));
                        if (!idrCall.IsDBNull(idrCall.GetOrdinal("postcode")))
                            oLeadCustomer.PostalCode = idrCall.GetString(idrCall.GetOrdinal("postcode"));
                        if (!idrCall.IsDBNull(idrCall.GetOrdinal("lead_stage")))
                            oLeadCustomer.LeadStage = idrCall.GetString(idrCall.GetOrdinal("lead_stage"));

                        if (!idrCall.IsDBNull(idrCall.GetOrdinal("industry_description")))
                            oLeadCustomer.IndustryDescription = idrCall.GetString(idrCall.GetOrdinal("industry_description"));

                        if (!idrCall.IsDBNull(idrCall.GetOrdinal("cust_code")))
                            oLeadCustomer.CustomerCode = idrCall.GetString(idrCall.GetOrdinal("cust_code"));

                        leadCustomers.Add(oLeadCustomer);
                    }
                }

                return leadCustomers;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrCall != null)
                    idrCall.Close();
            }
        }

        public List<LeadCustomerViewEntity> GetCalledCustomers(ArgsEntity args)
        {
            //string sWhereCls1 = "";
            //string sWhereCls2 = "";
            //clsOriginator oOriginator;
            DbDataReader idrCall = null;
            DateTime dtEndDate;

            DateTime dtStartDate = new DateTime(args.SYear, args.SMonth, 1);

            if (DateTime.Today.Year == args.SYear && DateTime.Today.Month == args.SMonth)
                dtEndDate = DateTime.Today;
            else
                dtEndDate = dtStartDate.AddMonths(1).AddDays(-1);

            //sWhereCls2 = " AND a.start_date >= '" + DateTime.Parse(dtStartDate.ToString("dd-MMM-yyyy 00:00:00")).ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss")
            //            + "' and a.start_date <= '" + DateTime.Parse(dtEndDate.ToString("dd-MMM-yyyy 23:59:59")).ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss") + "'";

            //if (sOriginator != "")
            //{
            //    sWhereCls1 += " AND " + ChildOriginators;
            //}

            try
            {
                List<LeadCustomerViewEntity> leadCustomers = new List<LeadCustomerViewEntity>();

                LeadCustomerViewEntity oLeadCustomer = null;

                //idrCall = (IngresDataReader)oDataHandle.ExecuteSelect(string.Format(CRMSQLs.GetCalledCustomers,
                //    sWhereCls1, sWhereCls2, DefaultDeptID));
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@FromDate", DateTime.Parse(dtStartDate.ToString("dd-MMM-yyyy 00:00:00")).ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                paramCollection.Add("@ToDate", DateTime.Parse(dtEndDate.ToString("dd-MMM-yyyy 23:59:59")).ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                paramCollection.Add("@ChildOriginators", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@DepartmentId", args.DefaultDepartmentId, DbType.String);
                idrCall = this.DataAcessService.ExecuteQuery(ActivitySql["GetCalledCustomers"], paramCollection);

                if (idrCall != null)
                {
                    while (idrCall.Read())
                    {
                        oLeadCustomer = LeadCustomerViewEntity.CreateObject();

                        //if (!idrCall.IsDBNull(idrCall.GetOrdinal("lead_id")))
                        //    oLeadCustomer.SourceId = idrCall.GetInt32(idrCall.GetOrdinal("lead_id"));
                        //if (!idrCall.IsDBNull(idrCall.GetOrdinal("Originator")))
                        //    oLeadCustomer.Originator = idrCall.GetString(idrCall.GetOrdinal("Originator"));
                        if (!idrCall.IsDBNull(idrCall.GetOrdinal("ContactName")))
                            oLeadCustomer.Name = idrCall.GetString(idrCall.GetOrdinal("ContactName"));
                        if (!idrCall.IsDBNull(idrCall.GetOrdinal("business")))
                            oLeadCustomer.Business = idrCall.GetString(idrCall.GetOrdinal("business"));
                        if (!idrCall.IsDBNull(idrCall.GetOrdinal("industry")))
                            oLeadCustomer.Industry = idrCall.GetString(idrCall.GetOrdinal("industry"));
                        if (!idrCall.IsDBNull(idrCall.GetOrdinal("address")))
                            oLeadCustomer.Address = idrCall.GetString(idrCall.GetOrdinal("address"));
                        if (!idrCall.IsDBNull(idrCall.GetOrdinal("city")))
                            oLeadCustomer.City = idrCall.GetString(idrCall.GetOrdinal("city"));
                        if (!idrCall.IsDBNull(idrCall.GetOrdinal("state")))
                            oLeadCustomer.State = idrCall.GetString(idrCall.GetOrdinal("state"));
                        if (!idrCall.IsDBNull(idrCall.GetOrdinal("postcode")))
                            oLeadCustomer.PostalCode = idrCall.GetString(idrCall.GetOrdinal("postcode"));
                        if (!idrCall.IsDBNull(idrCall.GetOrdinal("lead_stage")))
                            oLeadCustomer.LeadStage = idrCall.GetString(idrCall.GetOrdinal("lead_stage"));

                        if (!idrCall.IsDBNull(idrCall.GetOrdinal("industry_description")))
                            oLeadCustomer.IndustryDescription = idrCall.GetString(idrCall.GetOrdinal("industry_description"));

                        if (!idrCall.IsDBNull(idrCall.GetOrdinal("cust_code")))
                            oLeadCustomer.CustomerCode = idrCall.GetString(idrCall.GetOrdinal("cust_code"));

                        leadCustomers.Add(oLeadCustomer);
                    }
                }

                return leadCustomers;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrCall != null)
                    idrCall.Close();
            }
        }

        public CustomerActivityEntity GetActivityAppoitmentId(int iActivityId)
        {
            DbDataReader drActivity = null;

            try
            {
                CustomerActivityEntity activityCollection = CustomerActivityEntity.CreateObject();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@ActivityId", iActivityId, DbType.Int32);

                drActivity = this.DataAcessService.ExecuteQuery(ActivitySql["GetActivitiesByAppoitmentId"], paramCollection);

                if (drActivity != null && drActivity.HasRows)
                {
                    int activityIdOrdinal = drActivity.GetOrdinal("appointment_id");
                    //int subjectOrdinal = drActivity.GetOrdinal("subject");
                    //int startDateOrdinal = drActivity.GetOrdinal("start_date");
                    //int endDateOrdinal = drActivity.GetOrdinal("end_date");
                    while (drActivity.Read())
                    {
                        //CustomerActivities oActivityView = new CustomerActivities();
                        if (!drActivity.IsDBNull(activityIdOrdinal)) activityCollection.ActivityID = iActivityId;
                        if (!drActivity.IsDBNull(activityIdOrdinal)) activityCollection.AppointmentId = drActivity.GetInt32(activityIdOrdinal);
                        //if (!drActivity.IsDBNull(subjectOrdinal)) oActivityView.Subject = drActivity.GetString(subjectOrdinal);
                        //if (!drActivity.IsDBNull(startDateOrdinal)) oActivityView.StartDate = drActivity.GetDateTime(startDateOrdinal).ToLocalTime();
                        //if (!drActivity.IsDBNull(endDateOrdinal)) oActivityView.EndDate = drActivity.GetDateTime(endDateOrdinal).ToLocalTime();
                        //activityCollection.Add(oActivityView);
                        break;
                    }
                }

                return activityCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drActivity != null)
                    drActivity.Close();
            }
        }

        public ActivityEntity GetActivityByAppoitmentId(int iAppointmentId)
        {
            DbDataReader drActivity = null;

            try
            {
                ActivityEntity activity = ActivityEntity.CreateObject();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AppointmentId", iAppointmentId, DbType.Int32);

                drActivity = this.DataAcessService.ExecuteQuery(ActivitySql["GetActivityByAppointmentId"], paramCollection);

                if (drActivity != null && drActivity.HasRows)
                {
                    int activityIdOrdinal = drActivity.GetOrdinal("activity_id");
                    int leadIdOrdinal = drActivity.GetOrdinal("lead_id");
                    int custCodeOrdinal = drActivity.GetOrdinal("cust_code");
                    int enduserCodeOrdinal = drActivity.GetOrdinal("enduser_code");

                    while (drActivity.Read())
                    {
                        if (!drActivity.IsDBNull(activityIdOrdinal)) activity.AppointmentID = iAppointmentId;
                        if (!drActivity.IsDBNull(activityIdOrdinal)) activity.ActivityID = drActivity.GetInt32(activityIdOrdinal);
                        if (!drActivity.IsDBNull(leadIdOrdinal)) activity.LeadID = drActivity.GetInt32(leadIdOrdinal);
                        if (!drActivity.IsDBNull(custCodeOrdinal)) activity.CustomerCode = drActivity.GetString(custCodeOrdinal);
                        if (!drActivity.IsDBNull(enduserCodeOrdinal)) activity.EndUserCode = drActivity.GetString(enduserCodeOrdinal);
                        break;
                    }
                }

                return activity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drActivity != null)
                    drActivity.Close();
            }
        }

        public int ResetAppointment(ActivityEntity activity)
        {
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@ActivityId", activity.ActivityID, DbType.Int32);

                return this.DataAcessService.ExecuteNonQuery(ActivitySql["ResetAppointmentIdInActivity"], paramCollection);
            }
            catch
            {
                throw;
            }
        }

        public int Delete(ActivityEntity activity)
        {
            try
            {
                int isSuccess = 0;
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@ActivityId", activity.ActivityID, DbType.Int32);
                isSuccess = this.DataAcessService.ExecuteNonQuery(ActivitySql["DeleteActivity"], paramCollection) ;
                return isSuccess;// this.DataAcessService.ExecuteNonQuery(ActivitySql["DeleteActivity"], paramCollection);
            }
            catch
            {
                throw;
            }
        }

        public List<ActivityEntity> GetActivitiesForEndUser(ArgsEntity args)
        {
            DbDataReader drActivity = null;

            try
            {
                List<ActivityEntity> activityCollection = new List<ActivityEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@DepartmentId", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@CustCode", args.CustomerCode, DbType.String);
                paramCollection.Add("@EnduserCode", args.EnduserCode, DbType.String);

                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);

                drActivity = this.DataAcessService.ExecuteQuery(ActivitySql["GetActivityForEndUser"], paramCollection);

                if (drActivity != null && drActivity.HasRows)
                {
                    int activityIdOrdinal = drActivity.GetOrdinal("activity_id");
                    int subjectOrdinal = drActivity.GetOrdinal("subject");
                    int dueDateOrdinal = drActivity.GetOrdinal("start_date");
                    int statusOrdinal = drActivity.GetOrdinal("status");
                    int assignedToOrdinal = drActivity.GetOrdinal("assigned_to");
                    int leadIdOrdinal = drActivity.GetOrdinal("lead_id");
                    int commentsOrdinal = drActivity.GetOrdinal("comments");
                    int sentMailOrdinal = drActivity.GetOrdinal("sent_mail");
                    int priorityOrdinal = drActivity.GetOrdinal("priority");
                    int sendReminderOrdinal = drActivity.GetOrdinal("send_reminder");
                    int reminderDateOrdinal = drActivity.GetOrdinal("reminder_date");
                    int statusDescriptionOrdinal = drActivity.GetOrdinal("status_description");
                    int priorityDescriptionOrdinal = drActivity.GetOrdinal("priority_description");
                    int typeDescriptionOrdinal = drActivity.GetOrdinal("type_description");
                    int endDateOrdinal = drActivity.GetOrdinal("end_date");
                    int appointmentIdOrdinal = drActivity.GetOrdinal("appointment_id");
                    int enduserCodeOrdinal = drActivity.GetOrdinal("enduser_code");
                    int custCodeOrdinal = drActivity.GetOrdinal("cust_code");
                    int total_countOrdinal = drActivity.GetOrdinal("total_count");

                    while (drActivity.Read())
                    {
                        ActivityEntity activity = ActivityEntity.CreateObject();

                        if (!drActivity.IsDBNull(activityIdOrdinal)) activity.ActivityID = drActivity.GetInt32(activityIdOrdinal);
                        if (!drActivity.IsDBNull(subjectOrdinal)) activity.Subject = drActivity.GetString(subjectOrdinal);
                        if (!drActivity.IsDBNull(dueDateOrdinal)) activity.StartDate = drActivity.GetDateTime(dueDateOrdinal).ToLocalTime();
                        if (!drActivity.IsDBNull(statusOrdinal)) activity.Status = drActivity.GetString(statusOrdinal);
                        if (!drActivity.IsDBNull(assignedToOrdinal)) activity.AssignedTo = drActivity.GetString(assignedToOrdinal);
                        if (!drActivity.IsDBNull(leadIdOrdinal)) activity.LeadID = drActivity.GetInt32(leadIdOrdinal);
                        if (!drActivity.IsDBNull(commentsOrdinal)) activity.Comments = drActivity.GetString(commentsOrdinal);
                        if (!drActivity.IsDBNull(sentMailOrdinal)) activity.SentMail = drActivity.GetString(sentMailOrdinal);
                        if (!drActivity.IsDBNull(priorityOrdinal)) activity.Priority = drActivity.GetString(priorityOrdinal);
                        if (!drActivity.IsDBNull(sendReminderOrdinal)) activity.SendReminder = drActivity.GetString(sendReminderOrdinal);
                        if (!drActivity.IsDBNull(reminderDateOrdinal)) activity.ReminderDate = drActivity.GetDateTime(reminderDateOrdinal).ToLocalTime();
                        if (!drActivity.IsDBNull(statusDescriptionOrdinal)) activity.StatusDescription = drActivity.GetString(statusDescriptionOrdinal);
                        if (!drActivity.IsDBNull(priorityDescriptionOrdinal)) activity.PriorityDescription = drActivity.GetString(priorityDescriptionOrdinal);
                        if (!drActivity.IsDBNull(typeDescriptionOrdinal)) activity.ActivityTypeDescription = drActivity.GetString(typeDescriptionOrdinal);
                        if (!drActivity.IsDBNull(endDateOrdinal)) activity.EndDate = drActivity.GetDateTime(endDateOrdinal).ToLocalTime();
                        if (!drActivity.IsDBNull(appointmentIdOrdinal)) activity.AppointmentID = drActivity.GetInt32(appointmentIdOrdinal);
                        if (!drActivity.IsDBNull(enduserCodeOrdinal)) activity.EndUserCode = drActivity.GetString(enduserCodeOrdinal);
                        if (!drActivity.IsDBNull(enduserCodeOrdinal)) activity.CustomerCode = drActivity.GetString(custCodeOrdinal);
                        if (!drActivity.IsDBNull(total_countOrdinal)) activity.TotalCount = drActivity.GetInt32(total_countOrdinal);

                        activityCollection.Add(activity);
                    }
                }

                return activityCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drActivity != null)
                    drActivity.Close();
            }
        }

        public List<CustomerActivityEntity> GetOutstandingActivities(ArgsEntity args)
        {
            CustomerActivityEntity activity = null;
            DbDataReader idrActivity = null;

            try
            {
                List<CustomerActivityEntity> Activities = new List<CustomerActivityEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "assigned_to"), DbType.String);
                paramCollection.Add("@DepartmentId", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
               

                idrActivity = this.DataAcessService.ExecuteQuery(ActivitySql["GetOutstandingActivities"], paramCollection);

                if (idrActivity != null && idrActivity.HasRows)
                {
                    int activityIdOrdinal = idrActivity.GetOrdinal("activity_id");
                    int subjectOrdinal = idrActivity.GetOrdinal("subject");
                    int startDateOrdinal = idrActivity.GetOrdinal("start_date");
                    int endDateOrdinal = idrActivity.GetOrdinal("end_date");
                    int statusOrdinal = idrActivity.GetOrdinal("status");
                    int assignedToOrdinal = idrActivity.GetOrdinal("assigned_to");
                    int leadIdOrdinal = idrActivity.GetOrdinal("lead_id");
                    int commentsOrdinal = idrActivity.GetOrdinal("comments");
                    int sentMailOrdinal = idrActivity.GetOrdinal("sent_mail");
                    int priorityOrdinal = idrActivity.GetOrdinal("priority");
                    int sendReminderOrdinal = idrActivity.GetOrdinal("send_reminder");
                    int reminderDateOrdinal = idrActivity.GetOrdinal("reminder_date");
                    int activityTypeOrdinal = idrActivity.GetOrdinal("activity_type");
                    int leadNameOrdinal = idrActivity.GetOrdinal("lead_name");
                    int statusDescOrdinal = idrActivity.GetOrdinal("status_desc");
                    int typeDescOrdinal = idrActivity.GetOrdinal("type_desc");
                    int leadStageOrdinal = idrActivity.GetOrdinal("lead_stage");
                    int custCodeOrdinal = idrActivity.GetOrdinal("cust_code");
                    int appoinmentidOrdinal = idrActivity.GetOrdinal("appointment_id");
                    int totalcountOrdinal = idrActivity.GetOrdinal("total_count");

                    while (idrActivity.Read())
                    {
                        activity = CustomerActivityEntity.CreateObject();

                        if (!idrActivity.IsDBNull(activityIdOrdinal)) activity.ActivityID = idrActivity.GetInt32(activityIdOrdinal);
                        if (!idrActivity.IsDBNull(subjectOrdinal)) activity.Subject = idrActivity.GetString(subjectOrdinal);
                        if (!idrActivity.IsDBNull(startDateOrdinal)) activity.StartDate = idrActivity.GetDateTime(startDateOrdinal).ToLocalTime();
                        if (!idrActivity.IsDBNull(endDateOrdinal)) activity.EndDate = idrActivity.GetDateTime(endDateOrdinal).ToLocalTime();
                        if (!idrActivity.IsDBNull(statusOrdinal)) activity.Status = idrActivity.GetString(statusOrdinal);
                        if (!idrActivity.IsDBNull(assignedToOrdinal)) activity.AssignedTo = idrActivity.GetString(assignedToOrdinal);
                        if (!idrActivity.IsDBNull(leadIdOrdinal)) activity.LeadId = idrActivity.GetInt32(leadIdOrdinal);
                        if (!idrActivity.IsDBNull(commentsOrdinal)) activity.Comments = idrActivity.GetString(commentsOrdinal);
                        if (!idrActivity.IsDBNull(sentMailOrdinal)) activity.SentMail = idrActivity.GetString(sentMailOrdinal);
                        if (!idrActivity.IsDBNull(priorityOrdinal)) activity.Priority = idrActivity.GetString(priorityOrdinal);
                        if (!idrActivity.IsDBNull(sendReminderOrdinal)) activity.SendReminder = idrActivity.GetString(sendReminderOrdinal);
                        if (!idrActivity.IsDBNull(reminderDateOrdinal)) activity.ReminderDate = idrActivity.GetDateTime(reminderDateOrdinal).ToLocalTime();
                        if (!idrActivity.IsDBNull(activityTypeOrdinal)) activity.ActivityType = idrActivity.GetString(activityTypeOrdinal);
                        if (!idrActivity.IsDBNull(leadNameOrdinal)) activity.LeadName = idrActivity.GetString(leadNameOrdinal);
                        if (!idrActivity.IsDBNull(statusDescOrdinal)) activity.StatusDesc = idrActivity.GetString(statusDescOrdinal);
                        if (!idrActivity.IsDBNull(typeDescOrdinal)) activity.TypeDesc = idrActivity.GetString(typeDescOrdinal);
                        if (!idrActivity.IsDBNull(leadStageOrdinal)) activity.LeadStage = idrActivity.GetString(leadStageOrdinal);
                        if (!idrActivity.IsDBNull(custCodeOrdinal)) activity.CustCode = idrActivity.GetString(custCodeOrdinal);
                        if (!idrActivity.IsDBNull(appoinmentidOrdinal)) activity.AppointmentId = idrActivity.GetInt32(appoinmentidOrdinal);
                        if (!idrActivity.IsDBNull(totalcountOrdinal)) activity.TotalCount = idrActivity.GetInt32(totalcountOrdinal);

                        Activities.Add(activity);
                    }
                }

                return Activities;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrActivity != null)
                    idrActivity.Close();
            }
        }

        public int UpdateActivityFromPlanner(ActivityEntity activity)
        {
            try
            {
                //Commented by kavisha for CTC reuirements
                //DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                //paramCollection.Add("@Subject", ValidateString(activity.Subject), DbType.String);
                //paramCollection.Add("@Comments", ValidateString(activity.Comments), DbType.String);
                //paramCollection.Add("@StartDate", activity.StartDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.DateTime);
                //paramCollection.Add("@EndDate", activity.EndDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.DateTime);
                //paramCollection.Add("@ActivityType", ValidateString(activity.ActivityType), DbType.String);
                //paramCollection.Add("@LastModifiedBy", ValidateString(activity.LastModifiedBy), DbType.String);
                //if (activity.LastModifiedDate.HasValue)
                //paramCollection.Add("@LastModifiedDate", activity.LastModifiedDate.Value.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.DateTime);
                //paramCollection.Add("@ActivityId", activity.ActivityID, DbType.Int32);

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@RepCode", activity.RepCode, DbType.String);
                paramCollection.Add("@RouteId",activity.RouteID , DbType.Int32);
                paramCollection.Add("@StartDate", activity.StartDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.DateTime);
                paramCollection.Add("@ActivityId", activity.ActivityID, DbType.Int32);


                return this.DataAcessService.ExecuteNonQuery(ActivitySql["UpdateActivityFromPlanner"], paramCollection);

            }
            catch
            {
                throw;
            }
        }


        public int UpdateTMEActivityFromPlanner(ActivityEntity activity)
        {
            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@StartDate", activity.StartDate.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.DateTime);
                paramCollection.Add("@ActivityId", activity.ActivityID, DbType.Int32);


                return this.DataAcessService.ExecuteNonQuery(ActivitySql["UpdateTMEActivityFromPlanner"], paramCollection);

            }
            catch
            {
                throw;
            }
        }

        public int GetAllActivitiesForHomeCount(ArgsEntity args)
        {
            DbDataReader idrActivity = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                //paramCollection.Add("@WhereLeadContact", sWhereClsLeadContact, DbType.String);
                paramCollection.Add("@ActivityDay", args.SToday, DbType.String);
                paramCollection.Add("@ActivityDayStartDate", args.ActivityDayStartDate, DbType.String);
                paramCollection.Add("@ActivityDayEndDate", args.ActivityDayEndDate, DbType.String);
                paramCollection.Add("@Source", args.SSource, DbType.String);
                paramCollection.Add("@DefaultDepartmentId", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@Status", args.Status, DbType.String);
                paramCollection.Add("@Childoriginator", args.ChildOriginators.Replace("originator", "assigned_to"), DbType.String);
                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@RepTerritory", args.RepTerritory, DbType.String);
                paramCollection.Add("@StartDate", args.SStartDate, DbType.String);
                paramCollection.Add("@EndDate", args.SEndDate, DbType.String);


                idrActivity = this.DataAcessService.ExecuteQuery(ActivitySql["GetAllActivitiesForHomeCount"], paramCollection);

                int count = 0;

                if (idrActivity != null && idrActivity.HasRows)
                {
                    int activityIdOrdinal = idrActivity.GetOrdinal("count");
                    while (idrActivity.Read())
                    {
                        if (!idrActivity.IsDBNull(activityIdOrdinal)) count = idrActivity.GetInt32(activityIdOrdinal);
                    }
                }

                return count;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrActivity != null)
                    idrActivity.Close();
            }
        }

        public int GetOutstandingActivitiesCount(ArgsEntity args)
        {
            DbDataReader idrActivity = null;

            try
            {
                List<CustomerActivityEntity> Activities = new List<CustomerActivityEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "assigned_to"), DbType.String);
                paramCollection.Add("@DepartmentId", args.DefaultDepartmentId, DbType.String);

                idrActivity = this.DataAcessService.ExecuteQuery(ActivitySql["GetOutstandingActivitiesCount"], paramCollection);

                int count = 0;

                if (idrActivity != null && idrActivity.HasRows)
                {
                    int activityIdOrdinal = idrActivity.GetOrdinal("count");
                    while (idrActivity.Read())
                    {
                        if (!idrActivity.IsDBNull(activityIdOrdinal)) count = idrActivity.GetInt32(activityIdOrdinal);
                    }
                }

                return count;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrActivity != null)
                    idrActivity.Close();
            }
        }

        public AppointmentCountViewEntity GetAllActivitiesByTypeCount(ArgsEntity args)
        {
            DbDataReader idrActivity = null;
            AppointmentCountViewEntity appointment =null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "assigned_to"), DbType.String);
                paramCollection.Add("@DefaultDepId", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@FromDate", string.IsNullOrEmpty(args.SStartDate) ? "" : DateTime.Parse(args.SStartDate).ToString("dd-MMM-yyyy") + " 00:00:00", DbType.String);
                paramCollection.Add("@ToDate", string.IsNullOrEmpty(args.SEndDate) ? "" : DateTime.Parse(args.SEndDate).ToString("dd-MMM-yyyy") + " 23:59:59", DbType.String);
                paramCollection.Add("@Sector", args.Sector, DbType.String);

                //List<AppointmentCountViewEntity> appointmentCollection = new List<AppointmentCountViewEntity>();
                idrActivity = this.DataAcessService.ExecuteQuery(ActivitySql["GetAllActivitiesByTypeTable"], paramCollection);

                if (idrActivity != null && idrActivity.HasRows)
                {
                    
                    //int categoryOrdinal = idrActivity.GetOrdinal("category");
                    //int colorOrdinal = idrActivity.GetOrdinal("color");
                    int week1Ordinal = idrActivity.GetOrdinal("week1");
                    int week2Ordinal = idrActivity.GetOrdinal("week2");
                    int week3Ordinal = idrActivity.GetOrdinal("week3");
                    int week4Ordinal = idrActivity.GetOrdinal("week4");
                    int week5Ordinal = idrActivity.GetOrdinal("week5");
                    int month1Ordinal = idrActivity.GetOrdinal("month1");
                    int month2Ordinal = idrActivity.GetOrdinal("month2");
                    int month3Ordinal = idrActivity.GetOrdinal("month3");

                    //while (idrActivity.Read())
                    //{
                    idrActivity.Read();
                    appointment = AppointmentCountViewEntity.CreateObject();

                        //if (!idrActivity.IsDBNull(categoryOrdinal)) appointment.Category = idrActivity.GetString(categoryOrdinal);
                        //if (!idrActivity.IsDBNull(colorOrdinal)) appointment.Color = idrActivity.GetString(colorOrdinal);
                        if (!idrActivity.IsDBNull(week1Ordinal)) appointment.Week1 = idrActivity.GetInt32(week1Ordinal);
                        if (!idrActivity.IsDBNull(week2Ordinal)) appointment.Week2 = idrActivity.GetInt32(week2Ordinal);
                        if (!idrActivity.IsDBNull(week3Ordinal)) appointment.Week3 = idrActivity.GetInt32(week3Ordinal);
                        if (!idrActivity.IsDBNull(week4Ordinal)) appointment.Week4 = idrActivity.GetInt32(week4Ordinal);
                        if (!idrActivity.IsDBNull(week5Ordinal)) appointment.Week5 = idrActivity.GetInt32(week5Ordinal);

                        if (!idrActivity.IsDBNull(month1Ordinal)) appointment.Month1 = idrActivity.GetInt32(month1Ordinal);
                        if (!idrActivity.IsDBNull(month2Ordinal)) appointment.Month2 = idrActivity.GetInt32(month2Ordinal);
                        if (!idrActivity.IsDBNull(month3Ordinal)) appointment.Month3 = idrActivity.GetInt32(month3Ordinal);

                       // appointmentCollection.Add(appointment);
                    //}
                }
                return appointment;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrActivity != null)
                    idrActivity.Close();
            }
        }

        public List<CustomerActivityEntity> GetAllActivitiesForHome(ArgsEntity args)
        {
            CustomerActivityEntity activity = null;
            DbDataReader idrActivity = null;
            try
            {
                List<CustomerActivityEntity> Activities = new List<CustomerActivityEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "assigned_to"), DbType.String);
                //paramCollection.Add("@repGroups", sRepGroups, DbType.String);
                paramCollection.Add("@today", args.SToday, DbType.String);
                paramCollection.Add("@todaystart", DateTime.Today.ToString("yyyy/MM/dd") + " 00:00:00", DbType.String);
                paramCollection.Add("@todayend", DateTime.Today.ToString("yyyy/MM/dd") + " 23:59:59", DbType.String);

                paramCollection.Add("@startdate", string.IsNullOrEmpty(args.SStartDate) ? "" : DateTime.Parse(args.SStartDate).ToString("yyyy/MM/dd") + " 00:00:00", DbType.String);
                paramCollection.Add("@enddate", string.IsNullOrEmpty(args.SEndDate) ? "" : DateTime.Parse(args.SEndDate).ToString("yyyy/MM/dd") + " 23:59:59", DbType.String);

                paramCollection.Add("@source", args.SSource, DbType.String);
                paramCollection.Add("@floor", string.IsNullOrEmpty(args.Floor) ? "" : args.Floor, DbType.String);
                paramCollection.Add("@status", args.Status, DbType.String);

                paramCollection.Add("@defaultDeptID", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@territory", "", DbType.String);
                paramCollection.Add("@ColumnsToGet", "b.*,count(*) over (partition by null) as total_count", DbType.String);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@RepCode", args.RepCode, DbType.String);

                idrActivity = this.DataAcessService.ExecuteQuery(ActivitySql["GetAllActivitiesRepTerritory"], paramCollection);
                if (idrActivity != null && idrActivity.HasRows)
                {
                    int activityIdOrdinal = idrActivity.GetOrdinal("activity_id");
                    int subjectOrdinal = idrActivity.GetOrdinal("subject");
                    int startDateOrdinal = idrActivity.GetOrdinal("start_date");
                    int endDateOrdinal = idrActivity.GetOrdinal("end_date");
                    int statusOrdinal = idrActivity.GetOrdinal("status");
                    int assignedToOrdinal = idrActivity.GetOrdinal("assigned_to");
                    int leadIdOrdinal = idrActivity.GetOrdinal("lead_id");
                    int commentsOrdinal = idrActivity.GetOrdinal("comments");
                    int sentMailOrdinal = idrActivity.GetOrdinal("sent_mail");
                    int priorityOrdinal = idrActivity.GetOrdinal("priority");
                    int sendReminderOrdinal = idrActivity.GetOrdinal("send_reminder");
                    int reminderDateOrdinal = idrActivity.GetOrdinal("reminder_date");
                    int activityTypeOrdinal = idrActivity.GetOrdinal("activity_type");
                    int leadNameOrdinal = idrActivity.GetOrdinal("lead_name");
                    int statusDescOrdinal = idrActivity.GetOrdinal("status_desc");
                    int typeDescOrdinal = idrActivity.GetOrdinal("type_desc");
                    int leadStageOrdinal = idrActivity.GetOrdinal("lead_stage");
                    int custCodeOrdinal = idrActivity.GetOrdinal("cust_code");
                    int appoinmentidOrdinal = idrActivity.GetOrdinal("appointment_id");
                    int enduserCodeOrdinal = idrActivity.GetOrdinal("enduser_code");
                    int totalCountOrdinal = idrActivity.GetOrdinal("total_count");

                    while (idrActivity.Read())
                    {
                        activity = CustomerActivityEntity.CreateObject();

                        if (!idrActivity.IsDBNull(activityIdOrdinal)) activity.ActivityID = idrActivity.GetInt32(activityIdOrdinal);
                        if (!idrActivity.IsDBNull(subjectOrdinal)) activity.Subject = idrActivity.GetString(subjectOrdinal);
                        if (!idrActivity.IsDBNull(startDateOrdinal)) activity.StartDate = idrActivity.GetDateTime(startDateOrdinal).ToLocalTime();
                        if (!idrActivity.IsDBNull(endDateOrdinal)) activity.EndDate = idrActivity.GetDateTime(endDateOrdinal).ToLocalTime();
                        if (!idrActivity.IsDBNull(statusOrdinal)) activity.Status = idrActivity.GetString(statusOrdinal);
                        if (!idrActivity.IsDBNull(assignedToOrdinal)) activity.AssignedTo = idrActivity.GetString(assignedToOrdinal);
                        if (!idrActivity.IsDBNull(leadIdOrdinal)) activity.LeadId = idrActivity.GetInt32(leadIdOrdinal);
                        if (!idrActivity.IsDBNull(commentsOrdinal)) activity.Comments = idrActivity.GetString(commentsOrdinal);
                        if (!idrActivity.IsDBNull(sentMailOrdinal)) activity.SentMail = idrActivity.GetString(sentMailOrdinal);
                        if (!idrActivity.IsDBNull(priorityOrdinal)) activity.Priority = idrActivity.GetString(priorityOrdinal);
                        if (!idrActivity.IsDBNull(sendReminderOrdinal)) activity.SendReminder = idrActivity.GetString(sendReminderOrdinal);
                        if (!idrActivity.IsDBNull(reminderDateOrdinal)) activity.ReminderDate = idrActivity.GetDateTime(reminderDateOrdinal).ToLocalTime();
                        if (!idrActivity.IsDBNull(activityTypeOrdinal)) activity.ActivityType = idrActivity.GetString(activityTypeOrdinal);
                        if (!idrActivity.IsDBNull(leadNameOrdinal)) activity.LeadName = idrActivity.GetString(leadNameOrdinal);
                        if (!idrActivity.IsDBNull(statusDescOrdinal)) activity.StatusDesc = idrActivity.GetString(statusDescOrdinal);
                        if (!idrActivity.IsDBNull(typeDescOrdinal)) activity.TypeDesc = idrActivity.GetString(typeDescOrdinal);
                        if (!idrActivity.IsDBNull(leadStageOrdinal)) activity.LeadStage = idrActivity.GetString(leadStageOrdinal);
                        if (!idrActivity.IsDBNull(custCodeOrdinal)) activity.CustCode = idrActivity.GetString(custCodeOrdinal);
                        if (!idrActivity.IsDBNull(appoinmentidOrdinal)) activity.AppointmentId = idrActivity.GetInt32(appoinmentidOrdinal);
                        if (!idrActivity.IsDBNull(enduserCodeOrdinal)) activity.EndUserCode = idrActivity.GetString(enduserCodeOrdinal);
                        if (!idrActivity.IsDBNull(totalCountOrdinal)) activity.TotalCount = idrActivity.GetInt32(totalCountOrdinal);
                        

                        Activities.Add(activity);
                    }
                }

                return Activities;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrActivity != null)
                    idrActivity.Close();
            }
        }

        public int GetRemindersForHomeCount(ArgsEntity args)
        {
            //CustomerActivityEntity activity = null;
            //DbDataReader idrActivity = null;
            try
            {
                //List<CustomerActivityEntity> Activities = new List<CustomerActivityEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "assigned_to"), DbType.String);
                //paramCollection.Add("@repGroups", sRepGroups, DbType.String);
                paramCollection.Add("@today", args.SToday, DbType.String);
                paramCollection.Add("@todaystart", DateTime.Today.ToString("yyyy/MM/dd") + " 00:00:00", DbType.String);
                paramCollection.Add("@todayend", DateTime.Today.ToString("yyyy/MM/dd") + " 23:59:59", DbType.String);

                paramCollection.Add("@startdate", string.IsNullOrEmpty(args.SStartDate) ? "" : DateTime.Parse(args.SStartDate).ToString("yyyy/MM/dd") + " 00:00:00", DbType.String);
                paramCollection.Add("@enddate", string.IsNullOrEmpty(args.SEndDate) ? "" : DateTime.Parse(args.SEndDate).ToString("yyyy/MM/dd") + " 23:59:59", DbType.String);

                paramCollection.Add("@source", args.SSource, DbType.String);
                paramCollection.Add("@floor", string.IsNullOrEmpty(args.Floor) ? "" : args.Floor, DbType.String);
                paramCollection.Add("@status", args.Status, DbType.String);

                paramCollection.Add("@defaultDeptID", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@territory", "", DbType.String);
                paramCollection.Add("@ColumnsToGet", " count(*) ", DbType.String);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@RepCode", args.RepCode, DbType.String);

                //idrActivity = this.DataAcessService.ExecuteQuery(ActivitySql["GetAllActivitiesRepTerritory"], paramCollection);
                int count = (int) this.DataAcessService.GetOneValue(ActivitySql["GetAllActivitiesRepTerritory"], paramCollection);
                
                return count;
            }
            catch
            {
                throw;
            }
            finally
            {
                //if (idrActivity != null)
                 //   idrActivity.Close();
            }
        }

        public List<CallCycleContactEntity> GetAllCallCyclesCountById(ArgsEntity args)
        {
            DbDataReader drCallCycleLeads = null;
            List<CallCycleContactEntity> callCycleContactCollection = new List<CallCycleContactEntity>();
            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@FromDate", args.SStartDate + " 00:00:00", DbType.String);
                paramCollection.Add("@Enddate", args.SEndDate + " 23:59:59", DbType.String);
                paramCollection.Add("@RepType", args.RepType, DbType.String);
                paramCollection.Add("@InActive", args.IsIncludeContact, DbType.String);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "C.created_by"), DbType.String);
                paramCollection.Add("@CallCycleID", 0, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowCount", 1, DbType.Boolean);

                drCallCycleLeads = this.DataAcessService.ExecuteQuery(ActivitySql["GetAllCallCycles"], paramCollection);

               

                if (drCallCycleLeads != null && drCallCycleLeads.HasRows)
                {
                    int callcycleIdOrdinal = drCallCycleLeads.GetOrdinal("callcycle_id");
                    int descriptionOrdinal = drCallCycleLeads.GetOrdinal("description");
                    int countOrdinal = drCallCycleLeads.GetOrdinal("CCCount");


                    while (drCallCycleLeads.Read())
                    {
                        CallCycleContactEntity callCycleContact = CallCycleContactEntity.CreateObject();

                        if (!drCallCycleLeads.IsDBNull(callcycleIdOrdinal)) callCycleContact.CallCycle.CallCycleID = drCallCycleLeads.GetInt32(callcycleIdOrdinal);
                        if (!drCallCycleLeads.IsDBNull(descriptionOrdinal)) callCycleContact.CallCycle.Description = drCallCycleLeads.GetString(descriptionOrdinal);
                        if (!drCallCycleLeads.IsDBNull(countOrdinal)) callCycleContact.CallCycle.NoOfLeadCustomers = drCallCycleLeads.GetInt32(countOrdinal);

                        //if (!drCallCycleLeads.IsDBNull(callcycleIdOrdinal)) callCycleContact.ActivityID = drCallCycleLeads.GetInt32(callcycleIdOrdinal);
                        //if (!drCallCycleLeads.IsDBNull(descriptionOrdinal)) callCycleContact.Comments = drCallCycleLeads.GetString(descriptionOrdinal);
                        //if (!drCallCycleLeads.IsDBNull(countOrdinal)) callCycleContact.TotalCount = drCallCycleLeads.GetInt32(countOrdinal);

                        callCycleContactCollection.Add(callCycleContact);

                    }
                }

                return callCycleContactCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drCallCycleLeads != null)
                    drCallCycleLeads.Close();
            }
        }

        public List<CallCycleContactEntity> GetAllCallCycles(ArgsEntity args)
        {
            DbDataReader drCallCycleLeads = null;
            List<CallCycleContactEntity> callCycleContactCollection = new List<CallCycleContactEntity>();
            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@FromDate", args.SStartDate + " 00:00:00", DbType.String);
                paramCollection.Add("@Enddate", args.SEndDate + " 23:59:59", DbType.String);
                paramCollection.Add("@RepType", args.RepType, DbType.String);
                paramCollection.Add("@InActive", args.IsIncludeContact, DbType.String);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators.Replace("originator", "C.created_by"), DbType.String);
                paramCollection.Add("@CallCycleID", args.LeadId, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowCount", 0, DbType.Boolean);

                drCallCycleLeads = this.DataAcessService.ExecuteQuery(ActivitySql["GetAllCallCycles"], paramCollection);
                
                if (drCallCycleLeads != null && drCallCycleLeads.HasRows)
                {
                    int callcycleIdOrdinal = drCallCycleLeads.GetOrdinal("callcycle_id");
                    int leadIdOrdinal = drCallCycleLeads.GetOrdinal("lead_id");
                    int descriptionOrdinal = drCallCycleLeads.GetOrdinal("description");
                    int dueonOrdinal = drCallCycleLeads.GetOrdinal("dueon");
                    //int originatorOrdinal = drCallCycleLeads.GetOrdinal("originator");
                    int leadNameOrdinal = drCallCycleLeads.GetOrdinal("lead_name");
                    int cityOrdinal = drCallCycleLeads.GetOrdinal("city");
                    int postcodeOrdinal = drCallCycleLeads.GetOrdinal("postcode");
                    int addressOrdinal = drCallCycleLeads.GetOrdinal("address");
                    int stateOrdinal = drCallCycleLeads.GetOrdinal("State");
                    int leadStageOrdinal = drCallCycleLeads.GetOrdinal("lead_Stage");
                    int custCodeOrdinal = drCallCycleLeads.GetOrdinal("cust_code");
                    int createdByOrdinal = drCallCycleLeads.GetOrdinal("created_by");
                    int createdDateOrdinal = drCallCycleLeads.GetOrdinal("created_date");
                    int commentsOrdinal = drCallCycleLeads.GetOrdinal("comments");

                    int endUserCodeOrdinal = drCallCycleLeads.GetOrdinal("enduser_code");
                    int weekDayOrdinal = drCallCycleLeads.GetOrdinal("week_day");
                    //int prefixCodeOrdinal = drCallCycleLeads.GetOrdinal("prefix_code");
                    int dayOrderOrdinal = drCallCycleLeads.GetOrdinal("day_order");
                    int totalcountOrdinal = drCallCycleLeads.GetOrdinal("total_count");

                    while (drCallCycleLeads.Read())
                    {
                        CallCycleContactEntity callCycleContact = CallCycleContactEntity.CreateObject();

                        if (!drCallCycleLeads.IsDBNull(callcycleIdOrdinal)) callCycleContact.CallCycle.CallCycleID = drCallCycleLeads.GetInt32(callcycleIdOrdinal);
                        if (!drCallCycleLeads.IsDBNull(dueonOrdinal)) callCycleContact.CallCycle.DueOn = drCallCycleLeads.GetDateTime(dueonOrdinal).ToLocalTime();
                        if (!drCallCycleLeads.IsDBNull(descriptionOrdinal)) callCycleContact.CallCycle.Description = drCallCycleLeads.GetString(descriptionOrdinal);

                        if (!drCallCycleLeads.IsDBNull(leadIdOrdinal)) callCycleContact.Contact.SourceId = drCallCycleLeads.GetInt32(leadIdOrdinal);
                        if (!drCallCycleLeads.IsDBNull(leadNameOrdinal)) callCycleContact.Contact.Name = drCallCycleLeads.GetString(leadNameOrdinal);
                        if (!drCallCycleLeads.IsDBNull(cityOrdinal)) callCycleContact.Contact.City = drCallCycleLeads.GetString(cityOrdinal);
                        if (!drCallCycleLeads.IsDBNull(postcodeOrdinal)) callCycleContact.Contact.PostalCode = drCallCycleLeads.GetString(postcodeOrdinal);
                        if (!drCallCycleLeads.IsDBNull(addressOrdinal)) callCycleContact.Contact.Address = drCallCycleLeads.GetString(addressOrdinal);
                        if (!drCallCycleLeads.IsDBNull(stateOrdinal)) callCycleContact.Contact.State = drCallCycleLeads.GetString(stateOrdinal);


                        //if (!drCallCycleLeads.IsDBNull(originatorOrdinal)) callCycleContact.Originator = drCallCycleLeads.GetString(originatorOrdinal);
                        if (!drCallCycleLeads.IsDBNull(createdByOrdinal)) callCycleContact.Originator = drCallCycleLeads.GetString(createdByOrdinal);

                        //callCycleContact.Contact.LeadCustomerType = LeadCustomerTypes.Lead;
                        if (!drCallCycleLeads.IsDBNull(leadStageOrdinal))
                        {
                            callCycleContact.Contact.LeadStage = drCallCycleLeads.GetString(leadStageOrdinal);
                            callCycleContact.LeadStage.StageName = drCallCycleLeads.GetString(leadStageOrdinal);
                        }
                        if (!drCallCycleLeads.IsDBNull(custCodeOrdinal)) callCycleContact.Contact.CustomerCode = drCallCycleLeads.GetString(custCodeOrdinal);
                        if (!drCallCycleLeads.IsDBNull(createdByOrdinal)) callCycleContact.CallCycle.CreatedBy = drCallCycleLeads.GetString(createdByOrdinal);
                        if (!drCallCycleLeads.IsDBNull(createdDateOrdinal)) callCycleContact.CallCycle.CreatedDate = drCallCycleLeads.GetDateTime(createdDateOrdinal);
                        if (!drCallCycleLeads.IsDBNull(commentsOrdinal)) callCycleContact.CallCycle.Comments = drCallCycleLeads.GetString(commentsOrdinal);

                        if (!drCallCycleLeads.IsDBNull(custCodeOrdinal)) callCycleContact.Contact.EndUserCode = drCallCycleLeads.GetString(endUserCodeOrdinal);
                        if (!drCallCycleLeads.IsDBNull(weekDayOrdinal)) callCycleContact.WeekDayId = drCallCycleLeads.GetInt32(weekDayOrdinal);
                        //if (!drCallCycleLeads.IsDBNull(prefixCodeOrdinal)) callCycleContact.PrefixCode = drCallCycleLeads.GetString(prefixCodeOrdinal);
                        if (!drCallCycleLeads.IsDBNull(dayOrderOrdinal)) callCycleContact.DayOrder = drCallCycleLeads.GetInt32(dayOrderOrdinal);



                        if (callCycleContact.Contact.CustomerCode != "" && callCycleContact.Contact.EndUserCode != "")
                        {
                            callCycleContact.LeadStage.StageName = "End User";
                        }
                        else if (callCycleContact.Contact.CustomerCode != "")
                        {
                            callCycleContact.LeadStage.StageName = "Customer";
                        }
                        //else
                        //{
                        //    callCycleContact.LeadStage.StageName = "Lead";
                        //}

                        if (!drCallCycleLeads.IsDBNull(totalcountOrdinal)) callCycleContact.TotalCount = drCallCycleLeads.GetInt32(totalcountOrdinal);

                        callCycleContactCollection.Add(callCycleContact);

                    }
                }

                return callCycleContactCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drCallCycleLeads != null)
                    drCallCycleLeads.Close();
            }
        }

        public int UpdateActivityStatusForTME(ActivityEntity activity)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@DelFlag", activity.Status, DbType.String);
                paramCollection.Add("@StartDate", activity.StartDate.Date.ToString("yyyy-MM-dd"), DbType.String);
                paramCollection.Add("@EndDate", activity.EndDate.Date.ToString("yyyy-MM-dd"), DbType.String);
                paramCollection.Add("@Tme", activity.LastModifiedBy, DbType.String);
                paramCollection.Add("@LastModifiedBy", activity.LastModifiedBy, DbType.String);
                paramCollection.Add("@ID", null, DbType.Int32, ParameterDirection.Output);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ActivitySql["UpdateActivityStatusForTME"], paramCollection, true, "@ID");

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return iNoRec;
        }
    }
}
