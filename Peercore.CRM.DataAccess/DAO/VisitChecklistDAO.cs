﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;

namespace Peercore.CRM.DataAccess.DAO
{
    public class VisitChecklistDAO:BaseDAO
    {
        private DbSqlAdapter VisitChecklistSql { get; set; }

        private void RegisterSql()
        {
            this.VisitChecklistSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.VisitChecklistSql.xml", ApplicationService.Instance.DbProvider);
        }

        public VisitChecklistDAO()
        {
            RegisterSql();
        }

        public VisitChecklistDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public VisitChecklistEntity CreateObject()
        {
            return VisitChecklistEntity.CreateObject();
        }

        public VisitChecklistEntity CreateObject(int entityId)
        {
            return VisitChecklistEntity.CreateObject(entityId);
        }

        public List<VisitChecklistEntity> GetVisitChecklist(int routeMasterId)
        {
            DbDataReader drObjective = null;

            try
            {
                List<VisitChecklistEntity> objectiveList = new List<VisitChecklistEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@routeMasterId", routeMasterId, DbType.Int32);

                drObjective = this.DataAcessService.ExecuteQuery(VisitChecklistSql["GetVisitChecklist"], paramCollection);

                if (drObjective != null && drObjective.HasRows)
                {
                    int hasCarStockOrdinal = drObjective.GetOrdinal("has_car_stock");
                    int hasMerchandiseOrdinal = drObjective.GetOrdinal("has_merchandise");
                    int isVehicleCheckOrdinal = drObjective.GetOrdinal("is_vehicle_check");

                    while (drObjective.Read())
                    {
                        VisitChecklistEntity objectiveEntity = CreateObject();

                        if (!drObjective.IsDBNull(hasCarStockOrdinal)) objectiveEntity.HasCarStock = drObjective.GetBoolean(hasCarStockOrdinal);
                        if (!drObjective.IsDBNull(hasMerchandiseOrdinal)) objectiveEntity.HasMerchandise = drObjective.GetBoolean(hasMerchandiseOrdinal);
                        if (!drObjective.IsDBNull(isVehicleCheckOrdinal)) objectiveEntity.IsVehicleCheck = drObjective.GetBoolean(isVehicleCheckOrdinal);

                        objectiveList.Add(objectiveEntity);
                    }
                }

                return objectiveList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drObjective != null)
                    drObjective.Close();
            }
        }

        
        public List<VisitChecklistMasterEntity> GetAllVisitCheckListMaster(ArgsEntity args)
        {
            DbDataReader drVisitChecklist = null;

            try
            {
                List<VisitChecklistMasterEntity> visitChecklistMasterEntityList = new List<VisitChecklistMasterEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                drVisitChecklist = this.DataAcessService.ExecuteQuery(VisitChecklistSql["GetAllVisitCheckListMaster"], paramCollection);

                if (drVisitChecklist != null && drVisitChecklist.HasRows)
                {
                    int idOrdinal = drVisitChecklist.GetOrdinal("id");
                    int nameOrdinal = drVisitChecklist.GetOrdinal("name");
                    int descriptionOrdinal = drVisitChecklist.GetOrdinal("description");
                    int effectiveStartDateOrdinal = drVisitChecklist.GetOrdinal("effective_start_date");
                    int effectiveEndDateOrdinal = drVisitChecklist.GetOrdinal("effective_end_date");
                    int statusOrdinal = drVisitChecklist.GetOrdinal("status");
                    int createdbyOrdinal = drVisitChecklist.GetOrdinal("created_by");
                    int createddateOrdinal = drVisitChecklist.GetOrdinal("created_date");
                    int lastmodifiedbyOrdinal = drVisitChecklist.GetOrdinal("last_modified_by");
                    int lastmodifieddateOrdinal = drVisitChecklist.GetOrdinal("last_modified_date");
                    int totalCountOrdinal = drVisitChecklist.GetOrdinal("total_count");

                    while (drVisitChecklist.Read())
                    {
                        VisitChecklistMasterEntity objectiveEntity = VisitChecklistMasterEntity.CreateObject();

                        if (!drVisitChecklist.IsDBNull(idOrdinal)) objectiveEntity.VisitChecklistMasterId = drVisitChecklist.GetInt32(idOrdinal);
                        if (!drVisitChecklist.IsDBNull(nameOrdinal)) objectiveEntity.Name = drVisitChecklist.GetString(nameOrdinal);
                        if (!drVisitChecklist.IsDBNull(descriptionOrdinal)) objectiveEntity.Description = drVisitChecklist.GetString(descriptionOrdinal);
                        if (!drVisitChecklist.IsDBNull(effectiveStartDateOrdinal)) objectiveEntity.EffectiveStartDate = drVisitChecklist.GetDateTime(effectiveStartDateOrdinal);
                        if (!drVisitChecklist.IsDBNull(effectiveEndDateOrdinal)) objectiveEntity.EffectiveStartDate = drVisitChecklist.GetDateTime(effectiveEndDateOrdinal);
                        if (!drVisitChecklist.IsDBNull(statusOrdinal)) objectiveEntity.Status = drVisitChecklist.GetString(statusOrdinal);
                        if (!drVisitChecklist.IsDBNull(createdbyOrdinal)) objectiveEntity.CreatedBy = drVisitChecklist.GetString(createdbyOrdinal);
                        if (!drVisitChecklist.IsDBNull(createddateOrdinal)) objectiveEntity.CreatedDate = drVisitChecklist.GetDateTime(createddateOrdinal);
                        if (!drVisitChecklist.IsDBNull(lastmodifiedbyOrdinal)) objectiveEntity.LastModifiedBy = drVisitChecklist.GetString(lastmodifiedbyOrdinal);
                        if (!drVisitChecklist.IsDBNull(lastmodifieddateOrdinal)) objectiveEntity.LastModifiedDate = drVisitChecklist.GetDateTime(lastmodifieddateOrdinal);
                        if (!drVisitChecklist.IsDBNull(totalCountOrdinal)) objectiveEntity.TotalCount = drVisitChecklist.GetInt32(totalCountOrdinal);

                        visitChecklistMasterEntityList.Add(objectiveEntity);
                    }
                }

                return visitChecklistMasterEntityList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drVisitChecklist != null)
                    drVisitChecklist.Close();
            }
        }

        public bool InsertVisitCheckListMaster(VisitChecklistMasterEntity visitChecklistMasterEntity)
        {
            bool successful = false;
            int iNo = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Name", visitChecklistMasterEntity.Name, DbType.String);
                paramCollection.Add("@Description", visitChecklistMasterEntity.Description, DbType.String);
                paramCollection.Add("@EffectiveStartDate", visitChecklistMasterEntity.EffectiveStartDate, DbType.DateTime);
                paramCollection.Add("@EffectiveEndDate", visitChecklistMasterEntity.EffectiveEndDate, DbType.DateTime);
                paramCollection.Add("@CreatedBy", visitChecklistMasterEntity.CreatedBy, DbType.String);

                iNo = this.DataAcessService.ExecuteNonQuery(VisitChecklistSql["InsertVisitCheckListMaster"], paramCollection);
                if (iNo > 0)
                    successful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return successful;
        }

        public bool UpdateVisitCheckListMaster(VisitChecklistMasterEntity visitChecklistMasterEntity)
        {
            bool successful = false;
            int iNo = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@VisitCheckListMasterId", visitChecklistMasterEntity.VisitChecklistMasterId, DbType.Int32);
                paramCollection.Add("@Name", visitChecklistMasterEntity.Name, DbType.String);
                paramCollection.Add("@Description", visitChecklistMasterEntity.Description, DbType.String);
                paramCollection.Add("@EffectiveStartDate", visitChecklistMasterEntity.EffectiveStartDate, DbType.DateTime);
                paramCollection.Add("@EffectiveEndDate", visitChecklistMasterEntity.EffectiveEndDate, DbType.DateTime);
                paramCollection.Add("@LastModifiedBy", visitChecklistMasterEntity.LastModifiedBy, DbType.String);

                iNo = this.DataAcessService.ExecuteNonQuery(VisitChecklistSql["UpdateVisitCheckListMaster"], paramCollection);
                if (iNo > 0)
                    successful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return successful;
        }

        public bool DeleteVisitCheckListMaster(VisitChecklistMasterEntity visitChecklistMasterEntity)
        {
            bool successful = false;
            int iNo = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@VisitCheckListMasterId", visitChecklistMasterEntity.VisitChecklistMasterId, DbType.Int32);
                paramCollection.Add("@LastModifiedBy", visitChecklistMasterEntity.LastModifiedBy, DbType.String);

                iNo = this.DataAcessService.ExecuteNonQuery(VisitChecklistSql["DeleteVisitCheckListMaster"], paramCollection);
                if (iNo > 0)
                    successful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return successful;
        }

    }
}
