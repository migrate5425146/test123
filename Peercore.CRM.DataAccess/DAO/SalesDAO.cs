﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Entities;
using System.Data.Common;
using System.Collections;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;
using DocumentFormat.OpenXml.Spreadsheet;
using DocumentFormat.OpenXml.Packaging;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public class SalesDAO : BaseDAO
    {
        private DbSqlAdapter SalesSql { get; set; }

        public SalesDAO()
        {
            RegisterSql();
        }

        private void RegisterSql()
        {
            this.SalesSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.SalesSql.xml", ApplicationService.Instance.DbProvider);
        }

        public SalesDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public EndUserSalesEntity CreateObject()
        {
            return EndUserSalesEntity.CreateObject();
        }

        public EndUserSalesEntity GetCurrentCostPeriod()
        {
            DbDataReader idrCostPeriod = null;
            EndUserSalesEntity endUserSalesEntity = null;
            try
            {
                idrCostPeriod = this.DataAcessService.ExecuteQuery(SalesSql["GetCurrentCostPeriod"]);

                if (idrCostPeriod != null && idrCostPeriod.HasRows)
                {
                    int currentyearOrdinal = idrCostPeriod.GetOrdinal("current_year");
                    int currentperiodOrdinal = idrCostPeriod.GetOrdinal("current_period");

                    if (idrCostPeriod.Read())
                    {
                        endUserSalesEntity = CreateObject();

                        if (!idrCostPeriod.IsDBNull(currentyearOrdinal)) endUserSalesEntity.Year = idrCostPeriod.GetInt32(currentyearOrdinal);
                        if (!idrCostPeriod.IsDBNull(currentperiodOrdinal)) endUserSalesEntity.Period = idrCostPeriod.GetInt16(currentperiodOrdinal);
                    }
                }

                return endUserSalesEntity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrCostPeriod != null && (!idrCostPeriod.IsClosed))
                    idrCostPeriod.Close();
            }
        }

        public EndUserSalesEntity GetCostPeriod(string dtDate)
        {
            DbDataReader idrCostPeriod = null;
            EndUserSalesEntity endUserSalesEntity = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                //paramCollection.Add("@Date", dtDate.ToUniversalTime().ToString("dd-MMM-yyyy"), DbType.DateTime);
                paramCollection.Add("@Date", dtDate, DbType.String);

                idrCostPeriod = this.DataAcessService.ExecuteQuery(SalesSql["GetCostPeriod"], paramCollection);

                if (idrCostPeriod != null && idrCostPeriod.HasRows)
                {
                    int currentyearOrdinal = idrCostPeriod.GetOrdinal("cost_year");
                    int currentperiodOrdinal = idrCostPeriod.GetOrdinal("cost_period");

                    if (idrCostPeriod.Read())
                    {
                        endUserSalesEntity = CreateObject();

                        if (!idrCostPeriod.IsDBNull(currentyearOrdinal)) endUserSalesEntity.Year = idrCostPeriod.GetInt32(currentyearOrdinal);
                        if (!idrCostPeriod.IsDBNull(currentperiodOrdinal)) endUserSalesEntity.Period = idrCostPeriod.GetInt32(currentperiodOrdinal);
                    }
                }
                return endUserSalesEntity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrCostPeriod != null && (!idrCostPeriod.IsClosed))
                    idrCostPeriod.Close();
            }
        }

        public List<EndUserSalesEntity> GetCostPeriod(ArgsEntity args)
        {
            DbDataReader idrCostPeriod = null;
            EndUserSalesEntity endUserSalesEntity = null;
            List<EndUserSalesEntity> endUserSaleslist = new List<EndUserSalesEntity>();
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@DefaultDepartmentId", args.DefaultDepartmentId, DbType.String);

                idrCostPeriod = this.DataAcessService.ExecuteQuery(SalesSql["GetCostPeriodForDepartment"], paramCollection);

                if (idrCostPeriod != null && idrCostPeriod.HasRows)
                {
                    int monthOrdinal = idrCostPeriod.GetOrdinal("month_abbri");
                    int periodOrdinal = idrCostPeriod.GetOrdinal("cost_period");

                    while (idrCostPeriod.Read())
                    {
                        endUserSalesEntity = CreateObject();

                        if (!idrCostPeriod.IsDBNull(periodOrdinal)) endUserSalesEntity.Period = idrCostPeriod.GetInt32(periodOrdinal);
                        if ((!idrCostPeriod.IsDBNull(monthOrdinal)) && (!idrCostPeriod.IsDBNull(periodOrdinal)))
                            endUserSalesEntity.Description = idrCostPeriod.GetInt32(periodOrdinal) + " - " + idrCostPeriod.GetString(monthOrdinal);

                        endUserSaleslist.Add(endUserSalesEntity);
                    }
                }
                return endUserSaleslist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrCostPeriod != null && (!idrCostPeriod.IsClosed))
                    idrCostPeriod.Close();
            }
        }

        public string GetMonthForCostPeriod(int costYear, int costPeriod)
        {
            DbDataReader idrCostPeriod = null;
            string month = string.Empty;
            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CostYear", costYear, DbType.Int32);
                paramCollection.Add("@CostPeriod", costPeriod, DbType.String);

                idrCostPeriod = this.DataAcessService.ExecuteQuery(SalesSql["GetMonthOfCostPeriod"], paramCollection);

                if (idrCostPeriod != null && idrCostPeriod.HasRows)
                {
                    if (idrCostPeriod.Read())
                    {
                        if (!idrCostPeriod.IsDBNull(idrCostPeriod.GetOrdinal("month_abbri")))
                            month = idrCostPeriod.GetString(idrCostPeriod.GetOrdinal("month_abbri"));
                    }
                }
                return month;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrCostPeriod != null && (!idrCostPeriod.IsClosed))
                    idrCostPeriod.Close();
            }
        }

        public EndUserSalesEntity GetSalesData(string sCustCode, int iCostPeriod, string[] sArrSalesMonths)
        {
            DbDataReader idrCostPeriod = null;
            EndUserSalesEntity endUserSalesEntity = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();


                string sSum1 = "", sSum2 = "", sSum3 = "", sSum4 = "";

                sSum1 = "p" + iCostPeriod.ToString();

                if (iCostPeriod == 1)
                    sSum2 = "pp12";
                else
                    sSum2 = "p" + (iCostPeriod - 1).ToString();

                if (iCostPeriod == 1)
                    sSum3 = "pp11";
                else if (iCostPeriod == 2)
                    sSum3 = "pp12";
                else
                    sSum3 = "p" + (iCostPeriod - 2).ToString();

                if (iCostPeriod == 1)
                    sSum4 = "pp10";
                else if (iCostPeriod == 2)
                    sSum4 = "pp11";
                else if (iCostPeriod == 3)
                    sSum4 = "pp12";
                else
                    sSum4 = "p" + (iCostPeriod - 3).ToString();

                paramCollection.Add("@CostPeriod", iCostPeriod, DbType.Int32);
                paramCollection.Add("@CustCode", sCustCode, DbType.String);
                paramCollection.Add("@Sum2", sSum2, DbType.String);
                paramCollection.Add("@Sum3", sSum3, DbType.String);
                paramCollection.Add("@Sum4", sSum4, DbType.String);

                //idrCostPeriod = this.DataAcessService.ExecuteQuery(SalesSql["GetSalesData"], paramCollection);

                if (idrCostPeriod != null && idrCostPeriod.HasRows)
                {
                    int tonnes1Ordinal = idrCostPeriod.GetOrdinal("tonnes1");
                    int tonnes2Ordinal = idrCostPeriod.GetOrdinal("tonnes2");
                    int tonnes3Ordinal = idrCostPeriod.GetOrdinal("tonnes3");
                    int dollar1Ordinal = idrCostPeriod.GetOrdinal("dollar1");
                    int dollar2Ordinal = idrCostPeriod.GetOrdinal("dollar2");
                    int dollar3Ordinal = idrCostPeriod.GetOrdinal("dollar3");

                    if (idrCostPeriod.Read())
                    {
                        endUserSalesEntity = CreateObject();

                        if (!idrCostPeriod.IsDBNull(tonnes1Ordinal)) endUserSalesEntity.Tonnes1 = idrCostPeriod.GetDouble(tonnes1Ordinal);
                        endUserSalesEntity.XLabel_FirstMonth = sArrSalesMonths[2];
                        if (!idrCostPeriod.IsDBNull(tonnes2Ordinal)) endUserSalesEntity.Tonnes2 = idrCostPeriod.GetDouble(tonnes2Ordinal);
                        endUserSalesEntity.XLabel_SecondMonth = sArrSalesMonths[1];
                        if (!idrCostPeriod.IsDBNull(tonnes3Ordinal)) endUserSalesEntity.Tonnes3 = idrCostPeriod.GetDouble(tonnes3Ordinal);
                        endUserSalesEntity.XLabel_ThirdMonth = sArrSalesMonths[0];

                        if (!idrCostPeriod.IsDBNull(dollar1Ordinal)) endUserSalesEntity.Dollar1 = idrCostPeriod.GetDecimal(dollar1Ordinal);
                        if (!idrCostPeriod.IsDBNull(dollar2Ordinal)) endUserSalesEntity.Dollar2 = idrCostPeriod.GetDecimal(dollar2Ordinal);
                        if (!idrCostPeriod.IsDBNull(dollar3Ordinal)) endUserSalesEntity.Dollar3 = idrCostPeriod.GetDecimal(dollar3Ordinal);

                        endUserSalesEntity.TotalSales += endUserSalesEntity.Tonnes3;

                        endUserSalesEntity.SalesSummary = (endUserSalesEntity.TotalSales / 13);
                    }
                }
                return endUserSalesEntity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrCostPeriod != null && (!idrCostPeriod.IsClosed))
                    idrCostPeriod.Close();
            }
        }

        public List<EndUserSalesEntity> GetDebtorsDetails(string sCustCode)
        {
            DbDataReader idrCostPeriod = null;
            EndUserSalesEntity endUserSalesEntity = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", sCustCode, DbType.String);
                List<EndUserSalesEntity> endUserSaleslist = new List<EndUserSalesEntity>();
                idrCostPeriod = this.DataAcessService.ExecuteQuery(SalesSql["GetDebtorsDetails"], paramCollection);

                if (idrCostPeriod != null && idrCostPeriod.HasRows)
                {
                    while (idrCostPeriod.Read())
                    {
                        endUserSalesEntity = CreateObject();

                        endUserSalesEntity.GracePeriod = ExtensionMethods.GetInt32(idrCostPeriod, "grace_period");
                        endUserSalesEntity.Documentdate = ExtensionMethods.GetDateTime(idrCostPeriod, "doc_date");
                        endUserSalesEntity.DueAmount = ExtensionMethods.GetDecimal(idrCostPeriod, "amount_due");

                        endUserSalesEntity.TransType = ExtensionMethods.GetInt32(idrCostPeriod, "trans_type");
                        endUserSalesEntity.Status = ExtensionMethods.GetString(idrCostPeriod, "status");
                        endUserSalesEntity.Discount = ExtensionMethods.GetDecimal(idrCostPeriod, "discount");

                        endUserSaleslist.Add(endUserSalesEntity);
                    }
                }
                return endUserSaleslist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrCostPeriod != null && (!idrCostPeriod.IsClosed))
                    idrCostPeriod.Close();
            }
        }

        public List<EndUserSalesEntity> GetCostYears()
        {
            DbDataReader idrCostPeriod = null;
            EndUserSalesEntity endUserSalesEntity = null;
            try
            {
                List<EndUserSalesEntity> endUserSaleslist = new List<EndUserSalesEntity>();
                idrCostPeriod = this.DataAcessService.ExecuteQuery(SalesSql["GetCostYears"]);

                if (idrCostPeriod != null && idrCostPeriod.HasRows)
                {
                    while (idrCostPeriod.Read())
                    {
                        endUserSalesEntity = CreateObject();

                        endUserSalesEntity.Year = ExtensionMethods.GetInt32(idrCostPeriod, "cost_year");
                        endUserSaleslist.Add(endUserSalesEntity);
                    }
                }
                return endUserSaleslist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrCostPeriod != null && (!idrCostPeriod.IsClosed))
                    idrCostPeriod.Close();
            }
        }

        public void LoadCostPeriods(ref List<string> lstCostPeriods, int iCostYear)
        {
            DbDataReader idrCostPeriod = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@cost_year", iCostYear, DbType.String);
                idrCostPeriod = this.DataAcessService.ExecuteQuery(SalesSql["GetCostPeriodsByCostYear"], paramCollection);

                if (idrCostPeriod != null && idrCostPeriod.HasRows)
                {
                    int monthOrdinal = idrCostPeriod.GetOrdinal("month_abbri");
                    while (idrCostPeriod.Read())
                    {
                        string month = "";
                        if (!idrCostPeriod.IsDBNull(monthOrdinal)) month = idrCostPeriod.GetString(monthOrdinal);

                        lstCostPeriods.Add(month);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (idrCostPeriod != null && (!idrCostPeriod.IsClosed))
                    idrCostPeriod.Close();
            }
        }

        public string GetDescription(string sGroupType, string sCatalogGrouping)
        {
            DbDataReader idrCostPeriod = null;
            string description = string.Empty;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@GroupType", sGroupType, DbType.String);
                paramCollection.Add("@CatalogGrouping", sCatalogGrouping, DbType.String);

                idrCostPeriod = this.DataAcessService.ExecuteQuery(SalesSql["GetCatalogueGroupDescription"], paramCollection);

                if (idrCostPeriod != null && idrCostPeriod.HasRows)
                {
                    int descriptionOrdinal = idrCostPeriod.GetOrdinal("description");
                    while (idrCostPeriod.Read())
                    {
                        if (!idrCostPeriod.IsDBNull(descriptionOrdinal)) description = idrCostPeriod.GetString(descriptionOrdinal);
                    }
                }
                return description;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (idrCostPeriod != null && (!idrCostPeriod.IsClosed))
                    idrCostPeriod.Close();
            }
        }

        public int UploadSalesTargets(string FilePath, string Extension, string UserName)
        {
            try
            {
                DataTable dt = new DataTable();

                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(FilePath, false))
                {
                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;
                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    IEnumerable<Row> rows = sheetData.Descendants<Row>();

                    foreach (Cell cell in rows.ElementAt(0))
                    {
                        dt.Columns.Add(GetCellValue(spreadSheetDocument, cell).Trim());
                    }

                    foreach (Row row in rows) //this will also include your header row...
                    {
                        DataRow tempRow = dt.NewRow();
                        try
                        {
                            for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
                            {
                                try
                                {
                                    tempRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));
                                }
                                catch { }
                            }

                            if (tempRow[0].ToString() != "")
                                dt.Rows.Add(tempRow);
                        }
                        catch { }
                    }
                }

                dt.Rows.RemoveAt(0);
                dt.AcceptChanges();

                DbInputParameterCollection paramCollection;

                int retResult = 0;
                bool retStatus = false;

                foreach (DataRow dr in dt.Rows)
                {
                    //paramCollection = new DbInputParameterCollection();
                    //paramCollection.Add("@rep_code", dr["RepCode"].ToString(), DbType.String);
                    //this.DataAcessService.ExecuteNonQuery(SalesSql["sp_Delete_SalesRep_Targets"], paramCollection);

                    //paramCollection.Add("@created_by", UserName, DbType.String);
                    //paramCollection.Add("@primary_tar_amt", dr["TargetAmount"].ToString(), DbType.String);
                    //paramCollection.Add("@primary_inc_amt", dr["IncentiveAmount"].ToString(), DbType.String);
                    //this.DataAcessService.ExecuteNonQuery(SalesSql["InsertRepTarget"], paramCollection);

                    using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                    {
                        SqlCommand objCommand = new SqlCommand();
                        objCommand.Connection = conn;
                        objCommand.CommandType = CommandType.StoredProcedure;
                        objCommand.CommandText = "InsertRepTarget";

                        objCommand.Parameters.AddWithValue("@territory_code", dr["TerritoryCode"].ToString());
                        objCommand.Parameters.AddWithValue("@primary_tar_amt", float.Parse(dr["TargetAmount"].ToString()));
                        objCommand.Parameters.AddWithValue("@primary_inc_amt", float.Parse(dr["IncentiveAmount"].ToString()));
                        objCommand.Parameters.AddWithValue("@created_by", UserName);

                        if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                        if (objCommand.ExecuteNonQuery() > 0)
                        {
                            retStatus = true;
                        }
                        else
                        {
                            retStatus = false;
                        }
                    }

                    retResult++;
                }

                return retResult;
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            string value = cell.CellValue.InnerXml;

            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }
        }


        # region business Details
        /*
        public BusSalesEntity GetBusSalesDetails(SalesInfoDetailSearchCriteriaEntity SearchCriteria)
        {
            BusSalesEntity bussales = new BusSalesEntity();

            DbDataReader drBusDetails = null;
            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@busarea", SearchCriteria.BusinessArea, DbType.String);
                paramCollection.Add("@catlogtype", SearchCriteria.CatalogType, DbType.String);
                paramCollection.Add("@market", SearchCriteria.Market, DbType.String);
                paramCollection.Add("@brand", SearchCriteria.Brand, DbType.String);
                paramCollection.Add("@catgroup", SearchCriteria.CategoryGroup, DbType.String);
                paramCollection.Add("@catsubgroup", SearchCriteria.CategorySubGroup, DbType.String);
                paramCollection.Add("@catlogcode", SearchCriteria.CatalogCode, DbType.String);
                paramCollection.Add("@custcode", SearchCriteria.CustomerCode, DbType.String);
                paramCollection.Add("@state", SearchCriteria.State, DbType.String);
                paramCollection.Add("@rep", SearchCriteria.RepCode, DbType.String);
                paramCollection.Add("@subparent", SearchCriteria.CustomerSubGroup, DbType.String);
                paramCollection.Add("@custgroup", SearchCriteria.CustomerGroup, DbType.String);
                

                drBusDetails = this.DataAcessService.ExecuteQuery(SalesSql["GetBusSalesDetails"], paramCollection);


                if (drBusDetails != null && drBusDetails.HasRows)
                {
                    int SalesVolP1Ordinal = drBusDetails.GetOrdinal("sales_vol_p1");
                    int SalesVolP2Ordinal = drBusDetails.GetOrdinal("sales_vol_p2");
                    int SalesVolP3Ordinal = drBusDetails.GetOrdinal("sales_vol_p3");
                    int SalesVolP4Ordinal = drBusDetails.GetOrdinal("sales_vol_p4");
                    int SalesVolP5Ordinal = drBusDetails.GetOrdinal("sales_vol_p5");
                    int SalesVolP6Ordinal = drBusDetails.GetOrdinal("sales_vol_p6");
                    int SalesVolP7Ordinal = drBusDetails.GetOrdinal("sales_vol_p7");
                    int SalesVolP8Ordinal = drBusDetails.GetOrdinal("sales_vol_p8");
                    int SalesVolP9Ordinal = drBusDetails.GetOrdinal("sales_vol_p9");
                    int SalesVolP10Ordinal = drBusDetails.GetOrdinal("sales_vol_p10");
                    int SalesVolP11Ordinal = drBusDetails.GetOrdinal("sales_vol_p11");
                    int SalesVolP12Ordinal = drBusDetails.GetOrdinal("sales_vol_p12");

                    int SalesVolPp1Ordinal = drBusDetails.GetOrdinal("sales_vol_pp1");
                    int SalesVolPp2Ordinal = drBusDetails.GetOrdinal("sales_vol_pp2");
                    int SalesVolPp3Ordinal = drBusDetails.GetOrdinal("sales_vol_pp3");
                    int SalesVolPp4Ordinal = drBusDetails.GetOrdinal("sales_vol_pp4");
                    int SalesVolPp5Ordinal = drBusDetails.GetOrdinal("sales_vol_pp5");
                    int SalesVolPp6Ordinal = drBusDetails.GetOrdinal("sales_vol_pp6");
                    int SalesVolPp7Ordinal = drBusDetails.GetOrdinal("sales_vol_pp7");
                    int SalesVolPp8Ordinal = drBusDetails.GetOrdinal("sales_vol_pp8");
                    int SalesVolPp9Ordinal = drBusDetails.GetOrdinal("sales_vol_pp9");
                    int SalesVolPp10Ordinal = drBusDetails.GetOrdinal("sales_vol_pp10");
                    int SalesVolPp11Ordinal = drBusDetails.GetOrdinal("sales_vol_pp11");
                    int SalesVolPp12Ordinal = drBusDetails.GetOrdinal("sales_vol_pp12");

                    int SalesValP1Ordinal = drBusDetails.GetOrdinal("sales_val_p1");
                    int SalesValP2Ordinal = drBusDetails.GetOrdinal("sales_val_p2");
                    int SalesValP3Ordinal = drBusDetails.GetOrdinal("sales_val_p3");
                    int SalesValP4Ordinal = drBusDetails.GetOrdinal("sales_val_p4");
                    int SalesValP5Ordinal = drBusDetails.GetOrdinal("sales_val_p5");
                    int SalesValP6Ordinal = drBusDetails.GetOrdinal("sales_val_p6");
                    int SalesValP7Ordinal = drBusDetails.GetOrdinal("sales_val_p7");
                    int SalesValP8Ordinal = drBusDetails.GetOrdinal("sales_val_p8");
                    int SalesValP9Ordinal = drBusDetails.GetOrdinal("sales_val_p9");
                    int SalesValP10Ordinal = drBusDetails.GetOrdinal("sales_val_p10");
                    int SalesValP11Ordinal = drBusDetails.GetOrdinal("sales_val_p11");
                    int SalesValP12Ordinal = drBusDetails.GetOrdinal("sales_val_p12");

                    int SalesValPp1Ordinal = drBusDetails.GetOrdinal("sales_val_pp1");
                    int SalesValPp2Ordinal = drBusDetails.GetOrdinal("sales_val_pp2");
                    int SalesValPp3Ordinal = drBusDetails.GetOrdinal("sales_val_pp3");
                    int SalesValPp4Ordinal = drBusDetails.GetOrdinal("sales_val_pp4");
                    int SalesValPp5Ordinal = drBusDetails.GetOrdinal("sales_val_pp5");
                    int SalesValPp6Ordinal = drBusDetails.GetOrdinal("sales_val_pp6");
                    int SalesValPp7Ordinal = drBusDetails.GetOrdinal("sales_val_pp7");
                    int SalesValPp8Ordinal = drBusDetails.GetOrdinal("sales_val_pp8");
                    int SalesValPp9Ordinal = drBusDetails.GetOrdinal("sales_val_pp9");
                    int SalesValPp10Ordinal = drBusDetails.GetOrdinal("sales_val_pp10");
                    int SalesValPp11Ordinal = drBusDetails.GetOrdinal("sales_val_pp11");
                    int SalesValPp12Ordinal = drBusDetails.GetOrdinal("sales_val_pp12");

                    int SalesVolYr1Ordinal = drBusDetails.GetOrdinal("sales_vol_yr1");
                    int SalesVolYr2Ordinal = drBusDetails.GetOrdinal("sales_vol_yr2");
                    int SalesVolYr3Ordinal = drBusDetails.GetOrdinal("sales_vol_yr3");
                    int SalesVolYr4Ordinal = drBusDetails.GetOrdinal("sales_vol_yr4");

                    int SalesValYr1Ordinal = drBusDetails.GetOrdinal("sales_val_yr1");
                    int SalesValYr2Ordinal = drBusDetails.GetOrdinal("sales_val_yr2");
                    int SalesValYr3Ordinal = drBusDetails.GetOrdinal("sales_val_yr3");
                    int SalesValYr4Ordinal = drBusDetails.GetOrdinal("sales_val_yr4");

                    if (drBusDetails.Read())
                    {
                        if (!drBusDetails.IsDBNull(SalesVolP1Ordinal)) bussales.SalesVolP1 = drBusDetails.GetDouble(SalesVolP1Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolP2Ordinal)) bussales.SalesVolP2 = drBusDetails.GetDouble(SalesVolP2Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolP3Ordinal)) bussales.SalesVolP3 = drBusDetails.GetDouble(SalesVolP3Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolP4Ordinal)) bussales.SalesVolP4 = drBusDetails.GetDouble(SalesVolP4Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolP5Ordinal)) bussales.SalesVolP5 = drBusDetails.GetDouble(SalesVolP5Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolP6Ordinal)) bussales.SalesVolP6 = drBusDetails.GetDouble(SalesVolP6Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolP7Ordinal)) bussales.SalesVolP7 = drBusDetails.GetDouble(SalesVolP7Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolP8Ordinal)) bussales.SalesVolP8 = drBusDetails.GetDouble(SalesVolP8Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolP9Ordinal)) bussales.SalesVolP9 = drBusDetails.GetDouble(SalesVolP9Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolP10Ordinal)) bussales.SalesVolP10 = drBusDetails.GetDouble(SalesVolP10Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolP11Ordinal)) bussales.SalesVolP11 = drBusDetails.GetDouble(SalesVolP11Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolP12Ordinal)) bussales.SalesVolP12 = drBusDetails.GetDouble(SalesVolP12Ordinal);

                        if (!drBusDetails.IsDBNull(SalesVolPp1Ordinal)) bussales.SalesVolPp1 = drBusDetails.GetDouble(SalesVolPp1Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolPp2Ordinal)) bussales.SalesVolPp2 = drBusDetails.GetDouble(SalesVolPp2Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolPp3Ordinal)) bussales.SalesVolPp3 = drBusDetails.GetDouble(SalesVolPp3Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolPp4Ordinal)) bussales.SalesVolPp4 = drBusDetails.GetDouble(SalesVolPp4Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolPp5Ordinal)) bussales.SalesVolPp5 = drBusDetails.GetDouble(SalesVolPp5Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolPp6Ordinal)) bussales.SalesVolPp6 = drBusDetails.GetDouble(SalesVolPp6Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolPp7Ordinal)) bussales.SalesVolPp7 = drBusDetails.GetDouble(SalesVolPp7Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolPp8Ordinal)) bussales.SalesVolPp8 = drBusDetails.GetDouble(SalesVolPp8Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolPp9Ordinal)) bussales.SalesVolPp9 = drBusDetails.GetDouble(SalesVolPp9Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolPp10Ordinal)) bussales.SalesVolPp10 = drBusDetails.GetDouble(SalesVolPp10Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolPp11Ordinal)) bussales.SalesVolPp11 = drBusDetails.GetDouble(SalesVolPp11Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolPp12Ordinal)) bussales.SalesVolPp12 = drBusDetails.GetDouble(SalesVolPp12Ordinal);

                        if (!drBusDetails.IsDBNull(SalesValP1Ordinal)) bussales.SalesValP1 = drBusDetails.GetDouble(SalesValP1Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValP2Ordinal)) bussales.SalesValP2 = drBusDetails.GetDouble(SalesValP2Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValP3Ordinal)) bussales.SalesValP3 = drBusDetails.GetDouble(SalesValP3Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValP4Ordinal)) bussales.SalesValP4 = drBusDetails.GetDouble(SalesValP4Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValP5Ordinal)) bussales.SalesValP5 = drBusDetails.GetDouble(SalesValP5Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValP6Ordinal)) bussales.SalesValP6 = drBusDetails.GetDouble(SalesValP6Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValP7Ordinal)) bussales.SalesValP7 = drBusDetails.GetDouble(SalesValP7Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValP8Ordinal)) bussales.SalesValP8 = drBusDetails.GetDouble(SalesValP8Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValP9Ordinal)) bussales.SalesValP9 = drBusDetails.GetDouble(SalesValP9Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValP10Ordinal)) bussales.SalesValP10 = drBusDetails.GetDouble(SalesValP10Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValP11Ordinal)) bussales.SalesValP11 = drBusDetails.GetDouble(SalesValP11Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValP12Ordinal)) bussales.SalesValP12 = drBusDetails.GetDouble(SalesValP12Ordinal);

                        if (!drBusDetails.IsDBNull(SalesValPp1Ordinal)) bussales.SalesValPp1 = drBusDetails.GetDouble(SalesValPp1Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValPp2Ordinal)) bussales.SalesValPp2 = drBusDetails.GetDouble(SalesValPp2Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValPp3Ordinal)) bussales.SalesValPp3 = drBusDetails.GetDouble(SalesValPp3Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValPp4Ordinal)) bussales.SalesValPp4 = drBusDetails.GetDouble(SalesValPp4Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValPp5Ordinal)) bussales.SalesValPp5 = drBusDetails.GetDouble(SalesValPp5Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValPp6Ordinal)) bussales.SalesValPp6 = drBusDetails.GetDouble(SalesValPp6Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValPp7Ordinal)) bussales.SalesValPp7 = drBusDetails.GetDouble(SalesValPp7Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValPp8Ordinal)) bussales.SalesValPp8 = drBusDetails.GetDouble(SalesValPp8Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValPp9Ordinal)) bussales.SalesValPp9 = drBusDetails.GetDouble(SalesValPp9Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValPp10Ordinal)) bussales.SalesValPp10 = drBusDetails.GetDouble(SalesValPp10Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValPp11Ordinal)) bussales.SalesValPp11 = drBusDetails.GetDouble(SalesValPp11Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValPp12Ordinal)) bussales.SalesValPp12 = drBusDetails.GetDouble(SalesValPp12Ordinal);

                        if (!drBusDetails.IsDBNull(SalesVolYr1Ordinal)) bussales.SalesVolYr1 = drBusDetails.GetDouble(SalesVolYr1Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolYr2Ordinal)) bussales.SalesVolYr2 = drBusDetails.GetDouble(SalesVolYr2Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolYr3Ordinal)) bussales.SalesVolYr3 = drBusDetails.GetDouble(SalesVolYr3Ordinal);
                        if (!drBusDetails.IsDBNull(SalesVolYr4Ordinal)) bussales.SalesVolYr4 = drBusDetails.GetDouble(SalesVolYr4Ordinal);

                        if (!drBusDetails.IsDBNull(SalesValYr1Ordinal)) bussales.SalesValYr1 = drBusDetails.GetDouble(SalesValYr1Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValYr2Ordinal)) bussales.SalesValYr2 = drBusDetails.GetDouble(SalesValYr2Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValYr3Ordinal)) bussales.SalesValYr3 = drBusDetails.GetDouble(SalesValYr3Ordinal);
                        if (!drBusDetails.IsDBNull(SalesValYr4Ordinal)) bussales.SalesValYr4 = drBusDetails.GetDouble(SalesValYr4Ordinal);
                    }

                }
                return bussales;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drBusDetails != null)
                    drBusDetails.Close();
            }

        }

        public BusSalesEntity GetBusBudgetDetails(SalesInfoDetailSearchCriteriaEntity SearchCriteria)
        {
            BusSalesEntity busbudget = new BusSalesEntity();

            DbDataReader drBusDetails = null;
            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@busarea", SearchCriteria.BusinessArea, DbType.String);
                paramCollection.Add("@catlogtype", SearchCriteria.CatalogType, DbType.String);
                paramCollection.Add("@market", SearchCriteria.Market, DbType.String);
                paramCollection.Add("@brand", SearchCriteria.Brand, DbType.String);
                paramCollection.Add("@catgroup", SearchCriteria.CategoryGroup, DbType.String);
                paramCollection.Add("@catsubgroup", SearchCriteria.CategorySubGroup, DbType.String);
                paramCollection.Add("@catlogcode", SearchCriteria.CatalogCode, DbType.String);
                paramCollection.Add("@custcode", SearchCriteria.CustomerCode, DbType.String);
                paramCollection.Add("@state", SearchCriteria.State, DbType.String);
                paramCollection.Add("@rep", SearchCriteria.RepCode, DbType.String);
                paramCollection.Add("@subparent", SearchCriteria.CustomerSubGroup, DbType.String);
                paramCollection.Add("@custgroup", SearchCriteria.CustomerGroup, DbType.String);

                drBusDetails = this.DataAcessService.ExecuteQuery(SalesSql["GetBusBudgetDetails"], paramCollection);


                if (drBusDetails != null && drBusDetails.HasRows)
                {
                    int BudgetVolP1Ordinal = drBusDetails.GetOrdinal("budget_vol_p1");
                    int BudgetVolP2Ordinal = drBusDetails.GetOrdinal("budget_vol_p2");
                    int BudgetVolP3Ordinal = drBusDetails.GetOrdinal("budget_vol_p3");
                    int BudgetVolP4Ordinal = drBusDetails.GetOrdinal("budget_vol_p4");
                    int BudgetVolP5Ordinal = drBusDetails.GetOrdinal("budget_vol_p5");
                    int BudgetVolP6Ordinal = drBusDetails.GetOrdinal("budget_vol_p6");
                    int BudgetVolP7Ordinal = drBusDetails.GetOrdinal("budget_vol_p7");
                    int BudgetVolP8Ordinal = drBusDetails.GetOrdinal("budget_vol_p8");
                    int BudgetVolP9Ordinal = drBusDetails.GetOrdinal("budget_vol_p9");
                    int BudgetVolP10Ordinal = drBusDetails.GetOrdinal("budget_vol_p10");
                    int BudgetVolP11Ordinal = drBusDetails.GetOrdinal("budget_vol_p11");
                    int BudgetVolP12Ordinal = drBusDetails.GetOrdinal("budget_vol_p12");

                    int BudgetVolPp1Ordinal = drBusDetails.GetOrdinal("budget_vol_pp1");
                    int BudgetVolPp2Ordinal = drBusDetails.GetOrdinal("budget_vol_pp2");
                    int BudgetVolPp3Ordinal = drBusDetails.GetOrdinal("budget_vol_pp3");
                    int BudgetVolPp4Ordinal = drBusDetails.GetOrdinal("budget_vol_pp4");
                    int BudgetVolPp5Ordinal = drBusDetails.GetOrdinal("budget_vol_pp5");
                    int BudgetVolPp6Ordinal = drBusDetails.GetOrdinal("budget_vol_pp6");
                    int BudgetVolPp7Ordinal = drBusDetails.GetOrdinal("budget_vol_pp7");
                    int BudgetVolPp8Ordinal = drBusDetails.GetOrdinal("budget_vol_pp8");
                    int BudgetVolPp9Ordinal = drBusDetails.GetOrdinal("budget_vol_pp9");
                    int BudgetVolPp10Ordinal = drBusDetails.GetOrdinal("budget_vol_pp10");
                    int BudgetVolPp11Ordinal = drBusDetails.GetOrdinal("budget_vol_pp11");
                    int BudgetVolPp12Ordinal = drBusDetails.GetOrdinal("budget_vol_pp12");

                    int BudgetValP1Ordinal = drBusDetails.GetOrdinal("budget_val_p1");
                    int BudgetValP2Ordinal = drBusDetails.GetOrdinal("budget_val_p2");
                    int BudgetValP3Ordinal = drBusDetails.GetOrdinal("budget_val_p3");
                    int BudgetValP4Ordinal = drBusDetails.GetOrdinal("budget_val_p4");
                    int BudgetValP5Ordinal = drBusDetails.GetOrdinal("budget_val_p5");
                    int BudgetValP6Ordinal = drBusDetails.GetOrdinal("budget_val_p6");
                    int BudgetValP7Ordinal = drBusDetails.GetOrdinal("budget_val_p7");
                    int BudgetValP8Ordinal = drBusDetails.GetOrdinal("budget_val_p8");
                    int BudgetValP9Ordinal = drBusDetails.GetOrdinal("budget_val_p9");
                    int BudgetValP10Ordinal = drBusDetails.GetOrdinal("budget_val_p10");
                    int BudgetValP11Ordinal = drBusDetails.GetOrdinal("budget_val_p11");
                    int BudgetValP12Ordinal = drBusDetails.GetOrdinal("budget_val_p12");

                    int BudgetValPp1Ordinal = drBusDetails.GetOrdinal("budget_val_pp1");
                    int BudgetValPp2Ordinal = drBusDetails.GetOrdinal("budget_val_pp2");
                    int BudgetValPp3Ordinal = drBusDetails.GetOrdinal("budget_val_pp3");
                    int BudgetValPp4Ordinal = drBusDetails.GetOrdinal("budget_val_pp4");
                    int BudgetValPp5Ordinal = drBusDetails.GetOrdinal("budget_val_pp5");
                    int BudgetValPp6Ordinal = drBusDetails.GetOrdinal("budget_val_pp6");
                    int BudgetValPp7Ordinal = drBusDetails.GetOrdinal("budget_val_pp7");
                    int BudgetValPp8Ordinal = drBusDetails.GetOrdinal("budget_val_pp8");
                    int BudgetValPp9Ordinal = drBusDetails.GetOrdinal("budget_val_pp9");
                    int BudgetValPp10Ordinal = drBusDetails.GetOrdinal("budget_val_pp10");
                    int BudgetValPp11Ordinal = drBusDetails.GetOrdinal("budget_val_pp11");
                    int BudgetValPp12Ordinal = drBusDetails.GetOrdinal("budget_val_pp12");

                    int BudgetVolYr1Ordinal = drBusDetails.GetOrdinal("budget_vol_yr1");
                    int BudgetVolYr2Ordinal = drBusDetails.GetOrdinal("budget_vol_yr2");
                    int BudgetVolYr3Ordinal = drBusDetails.GetOrdinal("budget_vol_yr3");
                    int BudgetVolYr4Ordinal = drBusDetails.GetOrdinal("budget_vol_yr4");

                    int BudgetValYr1Ordinal = drBusDetails.GetOrdinal("budget_val_yr1");
                    int BudgetValYr2Ordinal = drBusDetails.GetOrdinal("budget_val_yr2");
                    int BudgetValYr3Ordinal = drBusDetails.GetOrdinal("budget_val_yr3");
                    int BudgetValYr4Ordinal = drBusDetails.GetOrdinal("budget_val_yr4");

                    if (drBusDetails.Read())
                    {
                        if (!drBusDetails.IsDBNull(BudgetVolP1Ordinal)) busbudget.SalesVolP1 = drBusDetails.GetDouble(BudgetVolP1Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolP2Ordinal)) busbudget.SalesVolP2 = drBusDetails.GetDouble(BudgetVolP2Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolP3Ordinal)) busbudget.SalesVolP3 = drBusDetails.GetDouble(BudgetVolP3Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolP4Ordinal)) busbudget.SalesVolP4 = drBusDetails.GetDouble(BudgetVolP4Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolP5Ordinal)) busbudget.SalesVolP5 = drBusDetails.GetDouble(BudgetVolP5Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolP6Ordinal)) busbudget.SalesVolP6 = drBusDetails.GetDouble(BudgetVolP6Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolP7Ordinal)) busbudget.SalesVolP7 = drBusDetails.GetDouble(BudgetVolP7Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolP8Ordinal)) busbudget.SalesVolP8 = drBusDetails.GetDouble(BudgetVolP8Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolP9Ordinal)) busbudget.SalesVolP9 = drBusDetails.GetDouble(BudgetVolP9Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolP10Ordinal)) busbudget.SalesVolP10 = drBusDetails.GetDouble(BudgetVolP10Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolP11Ordinal)) busbudget.SalesVolP11 = drBusDetails.GetDouble(BudgetVolP11Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolP12Ordinal)) busbudget.SalesVolP12 = drBusDetails.GetDouble(BudgetVolP12Ordinal);

                        if (!drBusDetails.IsDBNull(BudgetVolPp1Ordinal)) busbudget.SalesVolPp1 = drBusDetails.GetDouble(BudgetVolPp1Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolPp2Ordinal)) busbudget.SalesVolPp2 = drBusDetails.GetDouble(BudgetVolPp2Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolPp3Ordinal)) busbudget.SalesVolPp3 = drBusDetails.GetDouble(BudgetVolPp3Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolPp4Ordinal)) busbudget.SalesVolPp4 = drBusDetails.GetDouble(BudgetVolPp4Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolPp5Ordinal)) busbudget.SalesVolPp5 = drBusDetails.GetDouble(BudgetVolPp5Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolPp6Ordinal)) busbudget.SalesVolPp6 = drBusDetails.GetDouble(BudgetVolPp6Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolPp7Ordinal)) busbudget.SalesVolPp7 = drBusDetails.GetDouble(BudgetVolPp7Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolPp8Ordinal)) busbudget.SalesVolPp8 = drBusDetails.GetDouble(BudgetVolPp8Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolPp9Ordinal)) busbudget.SalesVolPp9 = drBusDetails.GetDouble(BudgetVolPp9Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolPp10Ordinal)) busbudget.SalesVolPp10 = drBusDetails.GetDouble(BudgetVolPp10Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolPp11Ordinal)) busbudget.SalesVolPp11 = drBusDetails.GetDouble(BudgetVolPp11Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolPp12Ordinal)) busbudget.SalesVolPp12 = drBusDetails.GetDouble(BudgetVolPp12Ordinal);

                        if (!drBusDetails.IsDBNull(BudgetValP1Ordinal)) busbudget.SalesValP1 = drBusDetails.GetDouble(BudgetValP1Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValP2Ordinal)) busbudget.SalesValP2 = drBusDetails.GetDouble(BudgetValP2Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValP3Ordinal)) busbudget.SalesValP3 = drBusDetails.GetDouble(BudgetValP3Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValP4Ordinal)) busbudget.SalesValP4 = drBusDetails.GetDouble(BudgetValP4Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValP5Ordinal)) busbudget.SalesValP5 = drBusDetails.GetDouble(BudgetValP5Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValP6Ordinal)) busbudget.SalesValP6 = drBusDetails.GetDouble(BudgetValP6Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValP7Ordinal)) busbudget.SalesValP7 = drBusDetails.GetDouble(BudgetValP7Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValP8Ordinal)) busbudget.SalesValP8 = drBusDetails.GetDouble(BudgetValP8Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValP9Ordinal)) busbudget.SalesValP9 = drBusDetails.GetDouble(BudgetValP9Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValP10Ordinal)) busbudget.SalesValP10 = drBusDetails.GetDouble(BudgetValP10Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValP11Ordinal)) busbudget.SalesValP11 = drBusDetails.GetDouble(BudgetValP11Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValP12Ordinal)) busbudget.SalesValP12 = drBusDetails.GetDouble(BudgetValP12Ordinal);

                        if (!drBusDetails.IsDBNull(BudgetValPp1Ordinal)) busbudget.SalesValPp1 = drBusDetails.GetDouble(BudgetValPp1Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValPp2Ordinal)) busbudget.SalesValPp2 = drBusDetails.GetDouble(BudgetValPp2Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValPp3Ordinal)) busbudget.SalesValPp3 = drBusDetails.GetDouble(BudgetValPp3Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValPp4Ordinal)) busbudget.SalesValPp4 = drBusDetails.GetDouble(BudgetValPp4Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValPp5Ordinal)) busbudget.SalesValPp5 = drBusDetails.GetDouble(BudgetValPp5Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValPp6Ordinal)) busbudget.SalesValPp6 = drBusDetails.GetDouble(BudgetValPp6Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValPp7Ordinal)) busbudget.SalesValPp7 = drBusDetails.GetDouble(BudgetValPp7Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValPp8Ordinal)) busbudget.SalesValPp8 = drBusDetails.GetDouble(BudgetValPp8Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValPp9Ordinal)) busbudget.SalesValPp9 = drBusDetails.GetDouble(BudgetValPp9Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValPp10Ordinal)) busbudget.SalesValPp10 = drBusDetails.GetDouble(BudgetValPp10Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValPp11Ordinal)) busbudget.SalesValPp11 = drBusDetails.GetDouble(BudgetValPp11Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValPp12Ordinal)) busbudget.SalesValPp12 = drBusDetails.GetDouble(BudgetValPp12Ordinal);

                        if (!drBusDetails.IsDBNull(BudgetVolYr1Ordinal)) busbudget.SalesVolYr1 = drBusDetails.GetDouble(BudgetVolYr1Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolYr2Ordinal)) busbudget.SalesVolYr2 = drBusDetails.GetDouble(BudgetVolYr2Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolYr3Ordinal)) busbudget.SalesVolYr3 = drBusDetails.GetDouble(BudgetVolYr3Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetVolYr4Ordinal)) busbudget.SalesVolYr4 = drBusDetails.GetDouble(BudgetVolYr4Ordinal);

                        if (!drBusDetails.IsDBNull(BudgetValYr1Ordinal)) busbudget.SalesValYr1 = drBusDetails.GetDouble(BudgetValYr1Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValYr2Ordinal)) busbudget.SalesValYr2 = drBusDetails.GetDouble(BudgetValYr2Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValYr3Ordinal)) busbudget.SalesValYr3 = drBusDetails.GetDouble(BudgetValYr3Ordinal);
                        if (!drBusDetails.IsDBNull(BudgetValYr4Ordinal)) busbudget.SalesValYr4 = drBusDetails.GetDouble(BudgetValYr4Ordinal);
                    
                    }

                }
                return busbudget;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drBusDetails != null)
                    drBusDetails.Close();
            }

        }

        public BusSalesEntity GetBusProfitDetails(SalesInfoDetailSearchCriteriaEntity SearchCriteria)
        {
            BusSalesEntity busProfit = new BusSalesEntity();

            DbDataReader drBusDetails = null;
            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@busarea", SearchCriteria.BusinessArea, DbType.String);
                paramCollection.Add("@catlogtype", SearchCriteria.CatalogType, DbType.String);
                paramCollection.Add("@market", SearchCriteria.Market, DbType.String);
                paramCollection.Add("@brand", SearchCriteria.Brand, DbType.String);
                paramCollection.Add("@catgroup", SearchCriteria.CategoryGroup, DbType.String);
                paramCollection.Add("@catsubgroup", SearchCriteria.CategorySubGroup, DbType.String);
                paramCollection.Add("@catlogcode", SearchCriteria.CatalogCode, DbType.String);
                paramCollection.Add("@custcode", SearchCriteria.CustomerCode, DbType.String);
                paramCollection.Add("@state", SearchCriteria.State, DbType.String);
                paramCollection.Add("@rep", SearchCriteria.RepCode, DbType.String);
                paramCollection.Add("@subparent", SearchCriteria.CustomerSubGroup, DbType.String);

                drBusDetails = this.DataAcessService.ExecuteQuery(SalesSql["GetBusProfitDetails"], paramCollection);


                if (drBusDetails != null && drBusDetails.HasRows)
                {
                    int SalesProfitP1Ordinal = drBusDetails.GetOrdinal("sales_profit_p1");
                    int SalesProfitP2Ordinal = drBusDetails.GetOrdinal("sales_profit_p2");
                    int SalesProfitP3Ordinal = drBusDetails.GetOrdinal("sales_profit_p3");
                    int SalesProfitP4Ordinal = drBusDetails.GetOrdinal("sales_profit_p4");
                    int SalesProfitP5Ordinal = drBusDetails.GetOrdinal("sales_profit_p5");
                    int SalesProfitP6Ordinal = drBusDetails.GetOrdinal("sales_profit_p6");
                    int SalesProfitP7Ordinal = drBusDetails.GetOrdinal("sales_profit_p7");
                    int SalesProfitP8Ordinal = drBusDetails.GetOrdinal("sales_profit_p8");
                    int SalesProfitP9Ordinal = drBusDetails.GetOrdinal("sales_profit_p9");
                    int SalesProfitP10Ordinal = drBusDetails.GetOrdinal("sales_profit_p10");
                    int SalesProfitP11Ordinal = drBusDetails.GetOrdinal("sales_profit_p11");
                    int SalesProfitP12Ordinal = drBusDetails.GetOrdinal("sales_profit_p12");

                    int SalesProfitPp1Ordinal = drBusDetails.GetOrdinal("sales_profit_pp1");
                    int SalesProfitPp2Ordinal = drBusDetails.GetOrdinal("sales_profit_pp2");
                    int SalesProfitPp3Ordinal = drBusDetails.GetOrdinal("sales_profit_pp3");
                    int SalesProfitPp4Ordinal = drBusDetails.GetOrdinal("sales_profit_pp4");
                    int SalesProfitPp5Ordinal = drBusDetails.GetOrdinal("sales_profit_pp5");
                    int SalesProfitPp6Ordinal = drBusDetails.GetOrdinal("sales_profit_pp6");
                    int SalesProfitPp7Ordinal = drBusDetails.GetOrdinal("sales_profit_pp7");
                    int SalesProfitPp8Ordinal = drBusDetails.GetOrdinal("sales_profit_pp8");
                    int SalesProfitPp9Ordinal = drBusDetails.GetOrdinal("sales_profit_pp9");
                    int SalesProfitPp10Ordinal = drBusDetails.GetOrdinal("sales_profit_pp10");
                    int SalesProfitPp11Ordinal = drBusDetails.GetOrdinal("sales_profit_pp11");
                    int SalesProfitPp12Ordinal = drBusDetails.GetOrdinal("sales_profit_pp12");


                    int SalesProfit1Ordinal = drBusDetails.GetOrdinal("sales_profit_yr1");
                    int SalesProfit2Ordinal = drBusDetails.GetOrdinal("sales_profit_yr2");
                    int SalesProfit3Ordinal = drBusDetails.GetOrdinal("sales_profit_yr3");
                    int SalesProfit4Ordinal = drBusDetails.GetOrdinal("sales_profit_yr4");


                    if (drBusDetails.Read())
                    {
                        if (!drBusDetails.IsDBNull(SalesProfitP1Ordinal)) busProfit.SalesValP1 = drBusDetails.GetDouble(SalesProfitP1Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitP2Ordinal)) busProfit.SalesValP2 = drBusDetails.GetDouble(SalesProfitP2Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitP3Ordinal)) busProfit.SalesValP3 = drBusDetails.GetDouble(SalesProfitP3Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitP4Ordinal)) busProfit.SalesValP4 = drBusDetails.GetDouble(SalesProfitP4Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitP5Ordinal)) busProfit.SalesValP5 = drBusDetails.GetDouble(SalesProfitP5Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitP6Ordinal)) busProfit.SalesValP6 = drBusDetails.GetDouble(SalesProfitP6Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitP7Ordinal)) busProfit.SalesValP7 = drBusDetails.GetDouble(SalesProfitP7Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitP8Ordinal)) busProfit.SalesValP8 = drBusDetails.GetDouble(SalesProfitP8Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitP9Ordinal)) busProfit.SalesValP9 = drBusDetails.GetDouble(SalesProfitP9Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitP10Ordinal)) busProfit.SalesValP10 = drBusDetails.GetDouble(SalesProfitP10Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitP11Ordinal)) busProfit.SalesValP11 = drBusDetails.GetDouble(SalesProfitP11Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitP12Ordinal)) busProfit.SalesValP12 = drBusDetails.GetDouble(SalesProfitP12Ordinal);

                        if (!drBusDetails.IsDBNull(SalesProfitPp1Ordinal)) busProfit.SalesValPp1 = drBusDetails.GetDouble(SalesProfitPp1Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitPp2Ordinal)) busProfit.SalesValPp2 = drBusDetails.GetDouble(SalesProfitPp2Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitPp3Ordinal)) busProfit.SalesValPp3 = drBusDetails.GetDouble(SalesProfitPp3Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitPp4Ordinal)) busProfit.SalesValPp4 = drBusDetails.GetDouble(SalesProfitPp4Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitPp5Ordinal)) busProfit.SalesValPp5 = drBusDetails.GetDouble(SalesProfitPp5Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitPp6Ordinal)) busProfit.SalesValPp6 = drBusDetails.GetDouble(SalesProfitPp6Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitPp7Ordinal)) busProfit.SalesValPp7 = drBusDetails.GetDouble(SalesProfitPp7Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitPp8Ordinal)) busProfit.SalesValPp8 = drBusDetails.GetDouble(SalesProfitPp8Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitPp9Ordinal)) busProfit.SalesValPp9 = drBusDetails.GetDouble(SalesProfitPp9Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitPp10Ordinal)) busProfit.SalesValPp10 = drBusDetails.GetDouble(SalesProfitPp10Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitPp11Ordinal)) busProfit.SalesValPp11 = drBusDetails.GetDouble(SalesProfitPp11Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfitPp12Ordinal)) busProfit.SalesValPp12 = drBusDetails.GetDouble(SalesProfitPp12Ordinal);



                        if (!drBusDetails.IsDBNull(SalesProfit1Ordinal)) busProfit.SalesValYr1 = drBusDetails.GetDouble(SalesProfit1Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfit2Ordinal)) busProfit.SalesValYr2 = drBusDetails.GetDouble(SalesProfit2Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfit3Ordinal)) busProfit.SalesValYr3 = drBusDetails.GetDouble(SalesProfit3Ordinal);
                        if (!drBusDetails.IsDBNull(SalesProfit4Ordinal)) busProfit.SalesValYr4 = drBusDetails.GetDouble(SalesProfit4Ordinal);

                    }

                }
                return busProfit;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drBusDetails != null)
                    drBusDetails.Close();
            }

        }
        */
       #endregion
    }
}
