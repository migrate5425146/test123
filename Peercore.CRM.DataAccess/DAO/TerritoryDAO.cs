﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;
using Peercore.CRM.Model;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public class TerritoryDAO : BaseDAO
    {
        #region "Territory"
        public List<TerritoryModel> GetAllTerritory(ArgsModel args)
        {
            TerritoryModel objModel = null;
            DataTable objDS = new DataTable();
            List<TerritoryModel> objList = new List<TerritoryModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllTerritory";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new TerritoryModel();
                            if (item["area_id"] != DBNull.Value) objModel.AreaId = Convert.ToInt32(item["area_id"]);
                            if (item["area_code"] != DBNull.Value) objModel.AreaCode = item["area_code"].ToString();
                            if (item["area_name"] != DBNull.Value) objModel.AreaName = item["area_name"].ToString();
                            if (item["area_status"] != DBNull.Value) objModel.AreaStatus = item["area_status"].ToString();
                            if (item["territory_id"] != DBNull.Value) objModel.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_code"] != DBNull.Value) objModel.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objModel.TerritoryName = item["territory_name"].ToString();
                            if (item["total_count"] != DBNull.Value) objModel.TotalCount = Convert.ToInt32(item["total_count"]);
                            if (item["can_addoutlet"] != DBNull.Value) objModel.CanAddOutlet = Convert.ToBoolean(item["can_addoutlet"]);
                            if (item["is_geofence"] != DBNull.Value) objModel.IsGeoFence = Convert.ToBoolean(item["is_geofence"]);

                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public List<TerritoryModel> GetAllTerritoryByTerritoryCode(ArgsModel args, string TerritoryCode)
        {
            TerritoryModel objModel = null;
            DataTable objDS = new DataTable();
            List<TerritoryModel> objList = new List<TerritoryModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllTerritoryByTerritoryCode";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@TerritoryCode", TerritoryCode);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new TerritoryModel();
                            if (item["area_id"] != DBNull.Value) objModel.AreaId = Convert.ToInt32(item["area_id"]);
                            if (item["territory_id"] != DBNull.Value) objModel.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_code"] != DBNull.Value) objModel.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objModel.TerritoryName = item["territory_name"].ToString();
                            if (item["total_count"] != DBNull.Value) objModel.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public List<TerritoryModel> GetAllTerritoryByTerritoryName(ArgsModel args, string TerritoryName)
        {
            TerritoryModel objModel = null;
            DataTable objDS = new DataTable();
            List<TerritoryModel> objList = new List<TerritoryModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllTerritoryByTerritoryName";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@TerritoryName", TerritoryName);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new TerritoryModel();
                            if (item["area_id"] != DBNull.Value) objModel.AreaId = Convert.ToInt32(item["area_id"]);
                            if (item["territory_id"] != DBNull.Value) objModel.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_code"] != DBNull.Value) objModel.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objModel.TerritoryName = item["territory_name"].ToString();
                            if (item["total_count"] != DBNull.Value) objModel.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }
        
        public TerritoryModel GetTerritoryByTerritoryCode(string TerritoryCode)
        {
            TerritoryModel objModel = null;
            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetTerritoryByTerritoryCode";

                    objCommand.Parameters.AddWithValue("@TerritoryCode", TerritoryCode);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new TerritoryModel();
                            if (item["area_id"] != DBNull.Value) objModel.AreaId = Convert.ToInt32(item["area_id"]);
                            if (item["territory_id"] != DBNull.Value) objModel.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_code"] != DBNull.Value) objModel.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objModel.TerritoryName = item["territory_name"].ToString();
                            //if (item["total_count"] != DBNull.Value) objModel.TotalCount = Convert.ToInt32(item["total_count"]);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objModel;
        }
        
        public List<TerritoryModel> GetAllTerritoryByOriginator(ArgsModel args, string OriginatorType, string Originator)
        {
            TerritoryModel objModel = null;
            DataTable objDS = new DataTable();
            List<TerritoryModel> objList = new List<TerritoryModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllTerritoryByOriginator";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@OriginatorType", OriginatorType);
                    objCommand.Parameters.AddWithValue("@Originator", Originator);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new TerritoryModel();
                            if (item["area_id"] != DBNull.Value) objModel.AreaId = Convert.ToInt32(item["area_id"]);
                            if (item["territory_id"] != DBNull.Value) objModel.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_code"] != DBNull.Value) objModel.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objModel.TerritoryName = item["territory_name"].ToString();
                            if (item["total_count"] != DBNull.Value) objModel.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public List<TerritoryModel> GetAllTerritoryByDistributerId(ArgsModel args, string DistributerId)
        {
            TerritoryModel objModel = null;
            DataTable objDS = new DataTable();
            List<TerritoryModel> objList = new List<TerritoryModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllTerritoryByDistributerId";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@DistributerId", DistributerId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new TerritoryModel();
                            if (item["area_id"] != DBNull.Value) objModel.AreaId = Convert.ToInt32(item["area_id"]);
                            if (item["area_code"] != DBNull.Value) objModel.AreaCode = item["area_code"].ToString();
                            if (item["area_name"] != DBNull.Value) objModel.AreaName = item["area_name"].ToString();
                            if (item["territory_id"] != DBNull.Value) objModel.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_code"] != DBNull.Value) objModel.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objModel.TerritoryName = item["territory_name"].ToString();
                            if (item["total_count"] != DBNull.Value) objModel.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }
        
        public List<TerritoryModel> GetAllTerritoryByDiscountScheme(ArgsModel args, string SchemeHeaderId)
        {
            TerritoryModel objModel = null;
            DataTable objDS = new DataTable();
            List<TerritoryModel> objList = new List<TerritoryModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllTerritoryByDiscountSchemeHeaderId";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@SchemeHeaderId", SchemeHeaderId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new TerritoryModel();
                            if (item["area_id"] != DBNull.Value) objModel.AreaId = Convert.ToInt32(item["area_id"]);
                            if (item["area_code"] != DBNull.Value) objModel.AreaCode = item["area_code"].ToString();
                            if (item["area_name"] != DBNull.Value) objModel.AreaName = item["area_name"].ToString();
                            if (item["territory_id"] != DBNull.Value) objModel.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_code"] != DBNull.Value) objModel.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objModel.TerritoryName = item["territory_name"].ToString();
                            if (item["discount_territory_status"] != DBNull.Value) objModel.SchemeTerritoryStatus = item["discount_territory_status"].ToString();
                            if (item["total_count"] != DBNull.Value) objModel.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public List<TerritoryModel> GetAllTerritoryByTerritoryId(ArgsModel args, string TerritoryId)
        {
            TerritoryModel objModel = null;
            DataTable objDS = new DataTable();
            List<TerritoryModel> objList = new List<TerritoryModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllTerritoryByTerritoryId";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@TerritoryId", TerritoryId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new TerritoryModel();
                            if (item["area_id"] != DBNull.Value) objModel.AreaId = Convert.ToInt32(item["area_id"]);
                            if (item["territory_id"] != DBNull.Value) objModel.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_code"] != DBNull.Value) objModel.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objModel.TerritoryName = item["territory_name"].ToString();
                            if (item["total_count"] != DBNull.Value) objModel.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public List<TerritoryModel> GetAllTerritoryByAreaId(ArgsModel args, string AreaId)
        {
            TerritoryModel objModel = null;
            DataTable objDS = new DataTable();
            List<TerritoryModel> objList = new List<TerritoryModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllTerritoryByAreaId";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@AreaId", AreaId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new TerritoryModel();
                            if (item["area_id"] != DBNull.Value) objModel.AreaId = Convert.ToInt32(item["area_id"]);
                            if (item["territory_id"] != DBNull.Value) objModel.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_code"] != DBNull.Value) objModel.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objModel.TerritoryName = item["territory_name"].ToString();
                            if (item["total_count"] != DBNull.Value) objModel.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public bool DeleteTerritory(string territory_id)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteTerritoryByTerritoryId";

                    objCommand.Parameters.AddWithValue("@TerritoryId", territory_id);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }
        
        public bool DeleteTerritoryFromAreaByTerritoryId(string territory_id)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteTerritoryFromAreaByTerritoryId";

                    objCommand.Parameters.AddWithValue("@TerritoryId", territory_id);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool IsTerritoryAssigned(string territoryId, out string RepCode)
        {
            bool retStatus = false;
            DataTable objDS = new DataTable();
            RepCode = "";
            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetRepDetailsByTerritoryId";

                    objCommand.Parameters.AddWithValue("@TerritoryId", territoryId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        RepCode = objDS.Rows[0]["rep_code"].ToString();
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }
        
        public bool DeleteConfirmSRFromTerritory(string territoryId, string repId)
        {
            bool retStatus = false;
            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteConfirmSRFromTerritory";

                    objCommand.Parameters.AddWithValue("@TerritoryId", territoryId);
                    objCommand.Parameters.AddWithValue("@RepId", repId);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool SaveTerritory(string area_id, string territory_id, string territory_code, string territory_name, string created_by)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertUpdateTerritory";

                    objCommand.Parameters.AddWithValue("@AreaId", area_id);
                    objCommand.Parameters.AddWithValue("@TerritoryId", territory_id);
                    objCommand.Parameters.AddWithValue("@TerritoryCode", territory_code);
                    objCommand.Parameters.AddWithValue("@TerritoryName", territory_name);
                    objCommand.Parameters.AddWithValue("@CreatedBy", created_by);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }
        
        public bool UpdateTerritoryCanAddOutlet(string territory_id, bool can_add_outlet)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateTerritoryCanAddOutlet";

                    //objCommand.Parameters.AddWithValue("@AreaId", area_id);
                    objCommand.Parameters.AddWithValue("@TerritoryId", territory_id);
                    objCommand.Parameters.AddWithValue("@CanAddOutlet", can_add_outlet);
                    //objCommand.Parameters.AddWithValue("@TerritoryName", territory_name);
                    //objCommand.Parameters.AddWithValue("@CreatedBy", created_by);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool UpdateTerritoryGeoFence(string territory_id, bool is_geofence)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateTerritoryIsGeoFence";

                    //objCommand.Parameters.AddWithValue("@AreaId", area_id);
                    objCommand.Parameters.AddWithValue("@TerritoryId", territory_id);
                    objCommand.Parameters.AddWithValue("@IsGeoFence", is_geofence);
                    //objCommand.Parameters.AddWithValue("@TerritoryName", territory_name);
                    //objCommand.Parameters.AddWithValue("@CreatedBy", created_by);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        #endregion

        public List<DistributerTerritoryModel> GetAllTerritoriesForDistributer(ArgsModel args, string OriginatorType, string Originator)
        {
            DistributerTerritoryModel objModel = null;
            DataTable objDS = new DataTable();
            List<DistributerTerritoryModel> objList = new List<DistributerTerritoryModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllTerritoriesForDistributer";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@OriginatorType", OriginatorType);
                    objCommand.Parameters.AddWithValue("@Originator", Originator);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new DistributerTerritoryModel();
                            
                            if (item["territory_id"] != DBNull.Value) objModel.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["territory_code"] != DBNull.Value) objModel.TerritoryCode = item["territory_code"].ToString();
                            if (item["territory_name"] != DBNull.Value) objModel.TerritoryName = item["territory_name"].ToString();
                            if (item["area_id"] != DBNull.Value) objModel.AreaId = Convert.ToInt32(item["area_id"]);
                            if (item["area_status"] != DBNull.Value) objModel.AreaStatus = item["area_status"].ToString();
                            if (item["area_code"] != DBNull.Value) objModel.AreaCode = item["area_code"].ToString(); 
                            if (item["area_name"] != DBNull.Value) objModel.AreaName = item["area_name"].ToString();
                            if (item["asm_id"] != DBNull.Value) objModel.AsmId = Convert.ToInt32(item["asm_id"]);
                            if (item["asm_status"] != DBNull.Value) objModel.AsmStatus = item["asm_status"].ToString();
                            if (item["asm_code"] != DBNull.Value) objModel.AsmCode = item["asm_code"].ToString();
                            if (item["asm_name"] != DBNull.Value) objModel.AsmName = item["asm_name"].ToString();
                            if (item["db_id"] != DBNull.Value) objModel.DistributerId = Convert.ToInt32(item["db_id"]);
                            if (item["db_code"] != DBNull.Value) objModel.DistributerCode = item["db_code"].ToString();
                            if (item["db_name"] != DBNull.Value) objModel.DistributerName = item["db_name"].ToString();
                            if (item["total_count"] != DBNull.Value) objModel.TotalCount = Convert.ToInt32(item["total_count"]);

                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

    }
}
