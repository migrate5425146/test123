﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using System.Data.Common;
using Peercore.Workflow.Common;
using System.Data.SqlClient;
using Peercore.CRM.Model;

namespace Peercore.CRM.DataAccess.DAO
{
    public class LoadStockDAO : BaseDAO
    {
        private DbSqlAdapter LoadStockSql { get; set; }

        private void RegisterSql()
        {
            this.LoadStockSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.LoadStockSql.xml", ApplicationService.Instance.DbProvider);
        }

        public LoadStockDAO()
        {
            RegisterSql();
        }

        public LoadStockDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public LoadStockEntity CreateObject()
        {
            return LoadStockEntity.CreateObject();
        }

        public LoadStockDetailEntity CreateLoadStockDetailEntityObject()
        {
            return LoadStockDetailEntity.CreateObject();
        }

        public RouteEntity CreateRouteEntityObject()
        {
            return RouteEntity.CreateObject();
        }

        //public LoadStockEntity CreateObject(int entityId)
        //{
        //    return LoadStockEntity.CreateObject(entityId);
        //}

        public bool InsertStockLoaddedMovement(int stockId, LoadStockDetailEntity objLoadStockDetailEntity, string movementType, DateTime allocDate)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramLoadStock = new DbInputParameterCollection();
                paramLoadStock.Add("@StockId", stockId, DbType.Int32);
                paramLoadStock.Add("@CatlogCode", objLoadStockDetailEntity.CatalogCode, DbType.String);
                paramLoadStock.Add("@AllocationDate", allocDate, DbType.DateTime);
                paramLoadStock.Add("@AllocationQty", objLoadStockDetailEntity.AllocQty, DbType.Double);
                paramLoadStock.Add("@LoadedQty", objLoadStockDetailEntity.LoadQty, DbType.Double);
                paramLoadStock.Add("@BalanceQty", objLoadStockDetailEntity.BalanceQty, DbType.Double);
                paramLoadStock.Add("@ReturnQty", objLoadStockDetailEntity.ReturningQty, DbType.Double);
                paramLoadStock.Add("@ReturnType", objLoadStockDetailEntity.ReturnReason, DbType.String);
                paramLoadStock.Add("@MovtType", movementType, DbType.String);
                paramLoadStock.Add("@VehicleBalanceQty", objLoadStockDetailEntity.VehicleBalanceQty, DbType.Double);
                paramLoadStock.Add("@VehicleReturnQty", objLoadStockDetailEntity.VehicleReturningQty, DbType.Double);


                iNoRec = this.DataAcessService.ExecuteNonQuery(LoadStockSql["InsertStockMovement"], paramLoadStock);
                if (iNoRec > 0)
                    successful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return successful;
        }

        public bool InsertStockAllocMovement(int stockId, LoadStockDetailEntity objLoadStockDetailEntity, string movementType, DateTime allocDate)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramLoadStock = new DbInputParameterCollection();

                paramLoadStock.Add("@StockId", stockId, DbType.Int32);
                paramLoadStock.Add("@CatlogCode", objLoadStockDetailEntity.CatalogCode, DbType.String);
                paramLoadStock.Add("@AllocationDate", allocDate, DbType.DateTime);
                paramLoadStock.Add("@AllocationQty", objLoadStockDetailEntity.AllocQty, DbType.Double);
                paramLoadStock.Add("@MovtType", movementType, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(LoadStockSql["InsertStockMovement"], paramLoadStock);
                if (iNoRec > 0)
                    successful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return successful;
        }


        public bool DeleteStockDetail(LoadStockEntity lstLoadStock)
        {
            bool successful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AllocDate", lstLoadStock.AllocDate, DbType.DateTime);
                paramCollection.Add("@RouteAssignID", lstLoadStock.RouteAssignID, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(LoadStockSql["DeleteProducts"], paramCollection);

                if (iNoRec > 0)
                    successful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return successful;
        }

        public LoadStockEntity GetProductForStockAdjustment(DateTime allocDate, int distributorId, int divisionId, ArgsEntity args)
        {
            DbDataReader drLoadStockDetail = null;
            LoadStockEntity lstLoadStockEntity = LoadStockEntity.CreateObject();
            List<LoadStockDetailEntity> lstLoadStockDetail = new List<LoadStockDetailEntity>();
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AllocDate", allocDate, DbType.DateTime);
                paramCollection.Add("@DistributorID", distributorId, DbType.Int32);
                paramCollection.Add("@DivisionID", divisionId, DbType.Int32);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);

                drLoadStockDetail = this.DataAcessService.ExecuteQuery(LoadStockSql["GetAllProductsForStockLoad"], paramCollection);
                if (drLoadStockDetail != null && drLoadStockDetail.HasRows)
                {
                    int stockHeaderIdOrdinal = drLoadStockDetail.GetOrdinal("stock_header_id");
                    int codeOrdinal = drLoadStockDetail.GetOrdinal("code");
                    int nameOrdinal = drLoadStockDetail.GetOrdinal("name");
                    int allocationQtyOrdinal = drLoadStockDetail.GetOrdinal("allocation_qty");
                    int loadedQtyOrdinal = drLoadStockDetail.GetOrdinal("loaded_qty");
                    int balanceQtyOrdinal = drLoadStockDetail.GetOrdinal("balance_qty");
                    int returnQtyOrdinal = drLoadStockDetail.GetOrdinal("return_qty");
                    int skuOrdinal = drLoadStockDetail.GetOrdinal("sku");
                    int stockDetailIdOrdinal = drLoadStockDetail.GetOrdinal("stock_detail_id");

                    int vehicleBalanceQtyOrdinal = drLoadStockDetail.GetOrdinal("vehicle_balance_qty");
                    int vehicleReturnQtyOrdinal = drLoadStockDetail.GetOrdinal("vehicle_return_qty");
                    int totalCountOrdinal = drLoadStockDetail.GetOrdinal("total_count");

                    int AllocQtyTotalOrdinal = drLoadStockDetail.GetOrdinal("allocation_qty_total");
                    int stockTotalIdOrdinal = drLoadStockDetail.GetOrdinal("stock_total_id");
                    int productIdOrdinal = drLoadStockDetail.GetOrdinal("product_id");
                    int RecordIDOrdinal = drLoadStockDetail.GetOrdinal("RId");

                    while (drLoadStockDetail.Read())
                    {
                        LoadStockDetailEntity LoadStockDetailEntity = CreateLoadStockDetailEntityObject();
                        if (!drLoadStockDetail.IsDBNull(stockHeaderIdOrdinal)) LoadStockDetailEntity.StockHeaderId = drLoadStockDetail.GetInt32(stockHeaderIdOrdinal);
                        if (!drLoadStockDetail.IsDBNull(codeOrdinal)) LoadStockDetailEntity.CatalogCode = drLoadStockDetail.GetString(codeOrdinal);
                        if (!drLoadStockDetail.IsDBNull(nameOrdinal)) LoadStockDetailEntity.CatalogName = drLoadStockDetail.GetString(nameOrdinal);
                        try
                        {
                            if (!drLoadStockDetail.IsDBNull(allocationQtyOrdinal)) LoadStockDetailEntity.AllocQty = drLoadStockDetail.GetDouble(AllocQtyTotalOrdinal);
                        }
                        catch
                        {
                            LoadStockDetailEntity.AllocQty = 0;
                        }
                        if (!drLoadStockDetail.IsDBNull(loadedQtyOrdinal)) LoadStockDetailEntity.LoadQty = drLoadStockDetail.GetDouble(loadedQtyOrdinal);
                        if (!drLoadStockDetail.IsDBNull(balanceQtyOrdinal)) LoadStockDetailEntity.BalanceQty = drLoadStockDetail.GetDouble(balanceQtyOrdinal);
                        if (!drLoadStockDetail.IsDBNull(returnQtyOrdinal)) LoadStockDetailEntity.ReturnQty = drLoadStockDetail.GetDouble(returnQtyOrdinal);
                        if (!drLoadStockDetail.IsDBNull(stockDetailIdOrdinal)) LoadStockDetailEntity.StockDetailId = drLoadStockDetail.GetInt32(stockDetailIdOrdinal);
                        if (!drLoadStockDetail.IsDBNull(skuOrdinal)) LoadStockDetailEntity.Sku = drLoadStockDetail.GetInt32(skuOrdinal);

                        if (!drLoadStockDetail.IsDBNull(vehicleBalanceQtyOrdinal)) LoadStockDetailEntity.VehicleBalanceQty = drLoadStockDetail.GetDouble(vehicleBalanceQtyOrdinal);
                        if (!drLoadStockDetail.IsDBNull(vehicleReturnQtyOrdinal)) LoadStockDetailEntity.VehicleReturnQty = drLoadStockDetail.GetDouble(vehicleReturnQtyOrdinal);
                        if (!drLoadStockDetail.IsDBNull(totalCountOrdinal)) LoadStockDetailEntity.TotalCount = drLoadStockDetail.GetInt32(totalCountOrdinal);

                        if (!drLoadStockDetail.IsDBNull(AllocQtyTotalOrdinal)) LoadStockDetailEntity.AllocationQtyTotal = drLoadStockDetail.GetDouble(AllocQtyTotalOrdinal);
                        if (!drLoadStockDetail.IsDBNull(stockTotalIdOrdinal)) LoadStockDetailEntity.StockTotalId = drLoadStockDetail.GetInt32(stockTotalIdOrdinal);
                        if (!drLoadStockDetail.IsDBNull(productIdOrdinal)) LoadStockDetailEntity.CatalogId = drLoadStockDetail.GetInt32(productIdOrdinal);

                        if (!drLoadStockDetail.IsDBNull(productIdOrdinal)) LoadStockDetailEntity.RecordId = drLoadStockDetail.GetInt32(RecordIDOrdinal);

                        LoadStockDetailEntity.LoadQtyOld = LoadStockDetailEntity.LoadQty;

                        lstLoadStockDetail.Add(LoadStockDetailEntity);
                    }
                    lstLoadStockEntity.lstLoadStockDetail = lstLoadStockDetail;
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drLoadStockDetail != null)
                    drLoadStockDetail.Close();
            }
            return lstLoadStockEntity;
        }

        public List<RouteEntity> GetAllRoutes(string name)
        {
            DbDataReader drRouteEntity = null;
            List<RouteEntity> lstRouteEntity = new List<RouteEntity>();

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@RouteName", name, DbType.String);

                drRouteEntity = this.DataAcessService.ExecuteQuery(LoadStockSql["GetAllRoutes"], paramCollection);
                if (drRouteEntity != null && drRouteEntity.HasRows)
                {
                    int idOrdinal = drRouteEntity.GetOrdinal("id");
                    int routeMasterIdOrdinal = drRouteEntity.GetOrdinal("route_master_id");
                    int routeNameOrdinal = drRouteEntity.GetOrdinal("route_name");
                    int repCodeOrdinal = drRouteEntity.GetOrdinal("rep_code");
                    int createdbyOrdinal = drRouteEntity.GetOrdinal("created_by");
                    int createdDateOrdinal = drRouteEntity.GetOrdinal("created_date");
                    int lastModifiedByOrdinal = drRouteEntity.GetOrdinal("last_modified_by");
                    int lastModifiedDateOrdinal = drRouteEntity.GetOrdinal("last_modified_date");

                    while (drRouteEntity.Read())
                    {
                        RouteEntity RouteEntity = CreateRouteEntityObject();
                        if (!drRouteEntity.IsDBNull(idOrdinal)) RouteEntity.RouteId = drRouteEntity.GetInt32(idOrdinal);
                        if (!drRouteEntity.IsDBNull(routeMasterIdOrdinal)) RouteEntity.RouteMasterId = drRouteEntity.GetInt32(routeMasterIdOrdinal);
                        if (!drRouteEntity.IsDBNull(routeNameOrdinal)) RouteEntity.RouteName = drRouteEntity.GetString(routeNameOrdinal);
                        if (!drRouteEntity.IsDBNull(repCodeOrdinal)) RouteEntity.RepCode = drRouteEntity.GetString(repCodeOrdinal);
                        if (!drRouteEntity.IsDBNull(createdbyOrdinal)) RouteEntity.CreatedBy = drRouteEntity.GetString(createdbyOrdinal);
                        if (!drRouteEntity.IsDBNull(createdDateOrdinal)) RouteEntity.CreatedDate = drRouteEntity.GetDateTime(createdDateOrdinal);
                        if (!drRouteEntity.IsDBNull(lastModifiedByOrdinal)) RouteEntity.LastModifiedBy = drRouteEntity.GetString(lastModifiedByOrdinal);
                        if (!drRouteEntity.IsDBNull(lastModifiedDateOrdinal)) RouteEntity.LastModifiedDate = drRouteEntity.GetDateTime(lastModifiedDateOrdinal);
                        lstRouteEntity.Add(RouteEntity);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drRouteEntity != null)
                    drRouteEntity.Close();
            }
            return lstRouteEntity;
        }

        public List<OriginatorEntity> GetRepsByRouteMasterId(string name, int routeMasterId)
        {
            DbDataReader drReps = null;
            List<OriginatorEntity> replist = new List<OriginatorEntity>();

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@RouteMasterID", routeMasterId, DbType.Int32);
                paramCollection.Add("@RepName", name, DbType.String);

                drReps = this.DataAcessService.ExecuteQuery(LoadStockSql["GetRepsByRouteMasterId"], paramCollection);

                if (drReps != null && drReps.HasRows)
                {
                    int repCodeOrdinal = drReps.GetOrdinal("rep_code");
                    int nameOrdinal = drReps.GetOrdinal("name");

                    while (drReps.Read())
                    {
                        OriginatorEntity originatorEntity = OriginatorEntity.CreateObject();

                        if (!drReps.IsDBNull(nameOrdinal)) originatorEntity.Name = drReps.GetString(nameOrdinal);
                        if (!drReps.IsDBNull(repCodeOrdinal)) originatorEntity.RepCode = drReps.GetString(repCodeOrdinal);

                        replist.Add(originatorEntity);
                    }
                }

                return replist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drReps != null)
                    drReps.Close();
            }
        }



        public List<LoadStockDetailEntity> GetStockUpdateForRep(string repCode)
        {
            DbDataReader drLoadStockDetail = null;
            List<LoadStockDetailEntity> loadStockEntityList = new List<LoadStockDetailEntity>();
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@RepCode", repCode, DbType.String);

                drLoadStockDetail = this.DataAcessService.ExecuteQuery(LoadStockSql["GetStockUpdateForRep"], paramCollection);
                if (drLoadStockDetail != null && drLoadStockDetail.HasRows)
                {
                    int productidOrdinal = drLoadStockDetail.GetOrdinal("id");
                    int productCodeOrdinal = drLoadStockDetail.GetOrdinal("code");
                    int balanceQtyOrdinal = drLoadStockDetail.GetOrdinal("balance_qty");

                    while (drLoadStockDetail.Read())
                    {
                        LoadStockDetailEntity loadStockEntity = LoadStockDetailEntity.CreateObject();

                        if (!drLoadStockDetail.IsDBNull(productidOrdinal)) loadStockEntity.StockHeaderId = drLoadStockDetail.GetInt32(productidOrdinal);
                        if (!drLoadStockDetail.IsDBNull(productCodeOrdinal)) loadStockEntity.CatalogCode = drLoadStockDetail.GetString(productCodeOrdinal);
                        if (!drLoadStockDetail.IsDBNull(balanceQtyOrdinal)) loadStockEntity.BalanceQty = drLoadStockDetail.GetDouble(balanceQtyOrdinal);

                        loadStockEntityList.Add(loadStockEntity);

                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drLoadStockDetail != null)
                    drLoadStockDetail.Close();
            }
            return loadStockEntityList;
        }

        public List<RouteEntity> GetRouteRepLikeRouteName(string name, int isTemp, string allocationDate)
        {
            DbDataReader drRouteEntity = null;
            List<RouteEntity> lstRouteEntity = new List<RouteEntity>();

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@RouteName", name, DbType.String);
                paramCollection.Add("@IsTemp", isTemp, DbType.Int32); //0 or 1
                paramCollection.Add("@AllocationDate", allocationDate, DbType.String);

                drRouteEntity = this.DataAcessService.ExecuteQuery(LoadStockSql["GetRouteRepLikeRoute"], paramCollection);
                if (drRouteEntity != null && drRouteEntity.HasRows)
                {
                    int idOrdinal = drRouteEntity.GetOrdinal("id");
                    int routeNameOrdinal = drRouteEntity.GetOrdinal("name");
                    int tempRepNameOrdinal = drRouteEntity.GetOrdinal("tempRepName");
                    int originalRepOrdinal = drRouteEntity.GetOrdinal("originalRep");

                    while (drRouteEntity.Read())
                    {
                        RouteEntity RouteEntity = CreateRouteEntityObject();
                        if (!drRouteEntity.IsDBNull(idOrdinal)) RouteEntity.RouteId = drRouteEntity.GetInt32(idOrdinal);
                        if (!drRouteEntity.IsDBNull(routeNameOrdinal)) RouteEntity.RouteName = drRouteEntity.GetString(routeNameOrdinal);
                        if (!drRouteEntity.IsDBNull(tempRepNameOrdinal)) RouteEntity.TempRepName = drRouteEntity.GetString(tempRepNameOrdinal);
                        if (!drRouteEntity.IsDBNull(originalRepOrdinal)) RouteEntity.RepCode = drRouteEntity.GetString(originalRepOrdinal);
                        lstRouteEntity.Add(RouteEntity);
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drRouteEntity != null)
                    drRouteEntity.Close();
            }
            return lstRouteEntity;
        }





        public List<StockTotalEntity> GetStockByAccessToken(string accessToken)
        {
            //DbDataReader drLoadStockDetail = null;
            //List<StockTotalEntity> loadStockEntityList = new List<StockTotalEntity>();

            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@AccessToken", accessToken, DbType.String);

            //    drLoadStockDetail = this.DataAcessService.ExecuteQuery(LoadStockSql["GetStockByAccessToken"], paramCollection);
            //    if (drLoadStockDetail != null && drLoadStockDetail.HasRows)
            //    {
            //        int productidOrdinal = drLoadStockDetail.GetOrdinal("product_id");
            //        int productCodeOrdinal = drLoadStockDetail.GetOrdinal("code");
            //        int balanceQtyOrdinal = drLoadStockDetail.GetOrdinal("allocation_qty");

            //        while (drLoadStockDetail.Read())
            //        {
            //            StockTotalEntity loadStockEntity = StockTotalEntity.CreateObject();

            //            if (!drLoadStockDetail.IsDBNull(productidOrdinal)) loadStockEntity.CatalogId = drLoadStockDetail.GetInt32(productidOrdinal);
            //            if (!drLoadStockDetail.IsDBNull(productCodeOrdinal)) loadStockEntity.CatalogCode = drLoadStockDetail.GetString(productCodeOrdinal);
            //            if (!drLoadStockDetail.IsDBNull(balanceQtyOrdinal)) loadStockEntity.BalanceQty = drLoadStockDetail.GetDouble(balanceQtyOrdinal);

            //            loadStockEntityList.Add(loadStockEntity);
            //        }
            //    }
            //}
            //catch (Exception)
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (drLoadStockDetail != null)
            //        drLoadStockDetail.Close();
            //}

            //return loadStockEntityList;

            //*************************Add By Irosh Fernando 2018/10/3************************************

            DataTable objDS = new DataTable();
            List<StockTotalEntity> loadStockEntityList = new List<StockTotalEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetStockByAccessToken";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            StockTotalEntity loadStockEntity = StockTotalEntity.CreateObject();
                            if (item["product_id"] != DBNull.Value) loadStockEntity.CatalogId = Convert.ToInt32(item["product_id"]);
                            if (item["code"] != DBNull.Value) loadStockEntity.CatalogCode = item["code"].ToString();
                            if (item["allocation_qty"] != DBNull.Value) loadStockEntity.BalanceQty = Convert.ToDouble(item["allocation_qty"]);
                            if (item["case_config"] != DBNull.Value)
                            {
                                float floatValue;
                                if (float.TryParse(item["case_config"].ToString(), out floatValue))
                                {
                                    loadStockEntity.CaseConfiguration = floatValue;
                                }
                            }
                            if (item["tonnage_config"] != DBNull.Value)
                            {
                                float floatValue;
                                if (float.TryParse(item["tonnage_config"].ToString(), out floatValue))
                                {
                                    loadStockEntity.TonnageConfiguration = floatValue;
                                }
                                
                            }




                            loadStockEntityList.Add(loadStockEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return loadStockEntityList;

            //*************************Add By Irosh Fernando 2018/10/3************************************
        }

        public bool UpdateStockTotalClear(int distributorId)
        {
            bool successful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@DistributorId", distributorId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(LoadStockSql["UpdateStockTotalClear"], paramCollection);
                if (iNoRec > 0)
                    successful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return successful;
        }


        //public LoadStockEntity GetAllProductStockByTerritoryId(ArgsModel args, string allocDate, int territoryId)
        //{
        //    DbDataReader drLoadStockDetail = null;
        //    LoadStockEntity lstLoadStockEntity = LoadStockEntity.CreateObject();
        //    List<LoadStockDetailEntity> lstLoadStockDetail = new List<LoadStockDetailEntity>();
        //    try
        //    {
        //        DbInputParameterCollection paramCollection = new DbInputParameterCollection();
        //        paramCollection.Add("@AllocDate", allocDate, DbType.DateTime);
        //        paramCollection.Add("@DistributorID", distributorId, DbType.Int32);
        //        paramCollection.Add("@DivisionID", divisionId, DbType.Int32);
        //        paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
        //        paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);

        //        drLoadStockDetail = this.DataAcessService.ExecuteQuery(LoadStockSql["GetAllProductsForStockLoad"], paramCollection);
        //        if (drLoadStockDetail != null && drLoadStockDetail.HasRows)
        //        {
        //            int stockHeaderIdOrdinal = drLoadStockDetail.GetOrdinal("stock_header_id");
        //            int codeOrdinal = drLoadStockDetail.GetOrdinal("code");
        //            int nameOrdinal = drLoadStockDetail.GetOrdinal("name");
        //            int allocationQtyOrdinal = drLoadStockDetail.GetOrdinal("allocation_qty");
        //            int loadedQtyOrdinal = drLoadStockDetail.GetOrdinal("loaded_qty");
        //            int balanceQtyOrdinal = drLoadStockDetail.GetOrdinal("balance_qty");
        //            int returnQtyOrdinal = drLoadStockDetail.GetOrdinal("return_qty");
        //            int skuOrdinal = drLoadStockDetail.GetOrdinal("sku");
        //            int stockDetailIdOrdinal = drLoadStockDetail.GetOrdinal("stock_detail_id");

        //            int vehicleBalanceQtyOrdinal = drLoadStockDetail.GetOrdinal("vehicle_balance_qty");
        //            int vehicleReturnQtyOrdinal = drLoadStockDetail.GetOrdinal("vehicle_return_qty");
        //            int totalCountOrdinal = drLoadStockDetail.GetOrdinal("total_count");

        //            int AllocQtyTotalOrdinal = drLoadStockDetail.GetOrdinal("allocation_qty_total");
        //            int stockTotalIdOrdinal = drLoadStockDetail.GetOrdinal("stock_total_id");
        //            int productIdOrdinal = drLoadStockDetail.GetOrdinal("product_id");
        //            int RecordIDOrdinal = drLoadStockDetail.GetOrdinal("RId");

        //            while (drLoadStockDetail.Read())
        //            {
        //                LoadStockDetailEntity LoadStockDetailEntity = CreateLoadStockDetailEntityObject();
        //                if (!drLoadStockDetail.IsDBNull(stockHeaderIdOrdinal)) LoadStockDetailEntity.StockHeaderId = drLoadStockDetail.GetInt32(stockHeaderIdOrdinal);
        //                if (!drLoadStockDetail.IsDBNull(codeOrdinal)) LoadStockDetailEntity.CatalogCode = drLoadStockDetail.GetString(codeOrdinal);
        //                if (!drLoadStockDetail.IsDBNull(nameOrdinal)) LoadStockDetailEntity.CatalogName = drLoadStockDetail.GetString(nameOrdinal);
        //                if (!drLoadStockDetail.IsDBNull(allocationQtyOrdinal)) LoadStockDetailEntity.AllocQty = drLoadStockDetail.GetDouble(allocationQtyOrdinal);
        //                if (!drLoadStockDetail.IsDBNull(loadedQtyOrdinal)) LoadStockDetailEntity.LoadQty = drLoadStockDetail.GetDouble(loadedQtyOrdinal);
        //                if (!drLoadStockDetail.IsDBNull(balanceQtyOrdinal)) LoadStockDetailEntity.BalanceQty = drLoadStockDetail.GetDouble(balanceQtyOrdinal);
        //                if (!drLoadStockDetail.IsDBNull(returnQtyOrdinal)) LoadStockDetailEntity.ReturnQty = drLoadStockDetail.GetDouble(returnQtyOrdinal);
        //                if (!drLoadStockDetail.IsDBNull(stockDetailIdOrdinal)) LoadStockDetailEntity.StockDetailId = drLoadStockDetail.GetInt32(stockDetailIdOrdinal);
        //                if (!drLoadStockDetail.IsDBNull(skuOrdinal)) LoadStockDetailEntity.Sku = drLoadStockDetail.GetInt32(skuOrdinal);

        //                if (!drLoadStockDetail.IsDBNull(vehicleBalanceQtyOrdinal)) LoadStockDetailEntity.VehicleBalanceQty = drLoadStockDetail.GetDouble(vehicleBalanceQtyOrdinal);
        //                if (!drLoadStockDetail.IsDBNull(vehicleReturnQtyOrdinal)) LoadStockDetailEntity.VehicleReturnQty = drLoadStockDetail.GetDouble(vehicleReturnQtyOrdinal);
        //                if (!drLoadStockDetail.IsDBNull(totalCountOrdinal)) LoadStockDetailEntity.TotalCount = drLoadStockDetail.GetInt32(totalCountOrdinal);

        //                if (!drLoadStockDetail.IsDBNull(AllocQtyTotalOrdinal)) LoadStockDetailEntity.AllocationQtyTotal = drLoadStockDetail.GetDouble(AllocQtyTotalOrdinal);
        //                if (!drLoadStockDetail.IsDBNull(stockTotalIdOrdinal)) LoadStockDetailEntity.StockTotalId = drLoadStockDetail.GetInt32(stockTotalIdOrdinal);
        //                if (!drLoadStockDetail.IsDBNull(productIdOrdinal)) LoadStockDetailEntity.CatalogId = drLoadStockDetail.GetInt32(productIdOrdinal);

        //                if (!drLoadStockDetail.IsDBNull(productIdOrdinal)) LoadStockDetailEntity.RecordId = drLoadStockDetail.GetInt32(RecordIDOrdinal);

        //                LoadStockDetailEntity.LoadQtyOld = LoadStockDetailEntity.LoadQty;

        //                lstLoadStockDetail.Add(LoadStockDetailEntity);
        //            }
        //            lstLoadStockEntity.lstLoadStockDetail = lstLoadStockDetail;
        //        }
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if (drLoadStockDetail != null)
        //            drLoadStockDetail.Close();
        //    }
        //    return lstLoadStockEntity;
        //}



        // For 2nd Phase Development

        public ProductStockModel GetAllProductStockByTerritoryId(ArgsModel args, DateTime allocDate, int territoryId)
        {
            ProductStockModel psEntity = new ProductStockModel();
            DataTable objDS = new DataTable();
            List<ProductStockDetailModel> psDetailList = new List<ProductStockDetailModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllProductStockByTerritoryId";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);

                    objCommand.Parameters.AddWithValue("@AllocDate", allocDate);
                    objCommand.Parameters.AddWithValue("@TerritoryId", territoryId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            ProductStockDetailModel psDetailEntity = new ProductStockDetailModel();
                            if (item["product_id"] != DBNull.Value) psDetailEntity.CatalogId = Convert.ToInt32(item["product_id"]);
                            if (item["code"] != DBNull.Value) psDetailEntity.CatalogCode = item["code"].ToString();
                            if (item["name"] != DBNull.Value) psDetailEntity.CatalogName = item["name"].ToString();
                            if (item["territory_id"] != DBNull.Value) psDetailEntity.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["allocation_qty_total"] != DBNull.Value) psDetailEntity.AllocationQtyTotal = Convert.ToDouble(item["allocation_qty_total"]);
                            if (item["RecordID"] != DBNull.Value) psDetailEntity.RecordId = Convert.ToInt32(item["RecordID"]);
                            if (item["total_count"] != DBNull.Value) psDetailEntity.TotalCount = Convert.ToInt32(item["total_count"]);
                            psDetailEntity.LoadQtyOld = psDetailEntity.LoadQty;

                            psDetailList.Add(psDetailEntity);
                        }

                        psEntity.LstLoadStockDetail = psDetailList;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return psEntity;
        }

        public ProductStockModel GetStockHeaderIdByTerritoryId(DateTime allocDate, int territoryId)
        {
            ProductStockModel psEntity = new ProductStockModel();
            DataTable objDS = new DataTable();
            //List<ProductStockDetailModel> psDetailList = new List<ProductStockDetailModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetStockHeaderByTerritoryId";

                    objCommand.Parameters.AddWithValue("@AllocDate", allocDate);
                    objCommand.Parameters.AddWithValue("@TerritoryId", territoryId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        if (objDS.Rows[0]["stock_header_id"] != DBNull.Value) psEntity.StockId = Convert.ToInt32(objDS.Rows[0]["stock_header_id"]);
                        //if (objDS.Rows[0]["code"] != DBNull.Value) psEntity.CatalogCode = objDS.Rows[0]["code"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return psEntity;
        }

        public bool SaveStockHeader(out int stockId, ProductStockModel lstLoadStock)
        {
            bool retStatus = false;
            stockId = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertStockHeader";

                    objCommand.Parameters.AddWithValue("@StockAllocDate", lstLoadStock.AllocDate);
                    objCommand.Parameters.AddWithValue("@RouteAssignID", lstLoadStock.RouteAssignID);
                    objCommand.Parameters.AddWithValue("@RepCode", lstLoadStock.RepCode);
                    objCommand.Parameters.AddWithValue("@TerritoryId", lstLoadStock.TerritoryId);
                    objCommand.Parameters.AddWithValue("@StockAllocType", lstLoadStock.StockAllocType);

                    SqlParameter p = objCommand.Parameters.Add("@ID", SqlDbType.Int);
                    p.Direction = ParameterDirection.Output;

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        stockId = (int)objCommand.Parameters["@ID"].Value;
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool InsertStockDetail(int stockId, ProductStockDetailModel objLoadStockEntity)
        {
            bool retStatus = false;
            stockId = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertStockDetail";

                    objCommand.Parameters.AddWithValue("@StockId", objLoadStockEntity.StockHeaderId);
                    objCommand.Parameters.AddWithValue("@CatlogCode", objLoadStockEntity.CatalogCode);
                    objCommand.Parameters.AddWithValue("@AllocationQty", objLoadStockEntity.AllocQty);
                    objCommand.Parameters.AddWithValue("@LoadedQty", objLoadStockEntity.LoadQty);
                    objCommand.Parameters.AddWithValue("@BalanceQty", objLoadStockEntity.BalanceQty);
                    objCommand.Parameters.AddWithValue("@ReturnQty", objLoadStockEntity.ReturnQty);
                    objCommand.Parameters.AddWithValue("@VehicleBalanceQty", objLoadStockEntity.VehicleBalanceQty);
                    objCommand.Parameters.AddWithValue("@VehicleReturnQty", objLoadStockEntity.VehicleReturnQty);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool InsertStockTotal(StockTotalModel stockTotalEntity)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertStockTotal";

                    objCommand.Parameters.AddWithValue("@TerritoryId", stockTotalEntity.TerritoryId);
                    objCommand.Parameters.AddWithValue("@ProductId", stockTotalEntity.CatalogId);
                    objCommand.Parameters.AddWithValue("@AllocationQty", stockTotalEntity.AllocQty);
                    objCommand.Parameters.AddWithValue("@LoadedQty", stockTotalEntity.LoadQty);
                    objCommand.Parameters.AddWithValue("@BalanceQty", stockTotalEntity.BalanceQty);
                    objCommand.Parameters.AddWithValue("@VehicleBalanceQty", stockTotalEntity.VehicleBalanceQty);
                    objCommand.Parameters.AddWithValue("@ReturnQty", stockTotalEntity.ReturnQty);
                    objCommand.Parameters.AddWithValue("@VehicleReturnQty", stockTotalEntity.VehicleReturnQty);
                    objCommand.Parameters.AddWithValue("@CreatedBy", stockTotalEntity.CreatedBy);


                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool UpdateStockTotal(StockTotalModel stockTotalEntity)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateStockTotal";

                    objCommand.Parameters.AddWithValue("@StockTotalId", stockTotalEntity.StockTotalId);
                    objCommand.Parameters.AddWithValue("@TerritoryId", stockTotalEntity.TerritoryId);
                    objCommand.Parameters.AddWithValue("@ProductId", stockTotalEntity.CatalogId);
                    objCommand.Parameters.AddWithValue("@AllocationQty", stockTotalEntity.AllocQty);
                    objCommand.Parameters.AddWithValue("@LoadedQty", stockTotalEntity.LoadQty);
                    objCommand.Parameters.AddWithValue("@BalanceQty", stockTotalEntity.BalanceQty);
                    objCommand.Parameters.AddWithValue("@VehicleBalanceQty", stockTotalEntity.VehicleBalanceQty);
                    objCommand.Parameters.AddWithValue("@ReturnQty", stockTotalEntity.ReturnQty);
                    objCommand.Parameters.AddWithValue("@VehicleReturnQty", stockTotalEntity.VehicleReturnQty);
                    objCommand.Parameters.AddWithValue("@LastModifiedBy", stockTotalEntity.LastModifiedBy);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool UpdateStockDetail(ProductStockDetailModel loadStockDetailEntity)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateStockDetail";

                    objCommand.Parameters.AddWithValue("@StockId", loadStockDetailEntity.StockHeaderId);
                    objCommand.Parameters.AddWithValue("@CatlogCode", loadStockDetailEntity.CatalogCode);
                    objCommand.Parameters.AddWithValue("@AllocationQty", loadStockDetailEntity.AllocQty);
                    objCommand.Parameters.AddWithValue("@LoadedQty", loadStockDetailEntity.LoadQty);
                    objCommand.Parameters.AddWithValue("@BalanceQty", loadStockDetailEntity.BalanceQty);
                    objCommand.Parameters.AddWithValue("@ReturnQty", loadStockDetailEntity.ReturnQty);
                    objCommand.Parameters.AddWithValue("@VehicleBalanceQty", loadStockDetailEntity.VehicleBalanceQty);
                    objCommand.Parameters.AddWithValue("@VehicleReturnQty", loadStockDetailEntity.VehicleReturnQty);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool InsertStockMovement(StockMovementModel stockTotalEntity)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertStockMovement";

                    objCommand.Parameters.AddWithValue("@TerritoryId", stockTotalEntity.TerritoryId);
                    objCommand.Parameters.AddWithValue("@CatalogId", stockTotalEntity.CatalogId);
                    objCommand.Parameters.AddWithValue("@AllocQty", stockTotalEntity.AllocQty);
                    objCommand.Parameters.AddWithValue("@CatalogPrice", stockTotalEntity.CatalogPrice);
                    objCommand.Parameters.AddWithValue("@TransRefId", stockTotalEntity.TransRefId);
                    objCommand.Parameters.AddWithValue("@AllocDate", stockTotalEntity.AllocDate);
                    objCommand.Parameters.AddWithValue("@TransType", stockTotalEntity.TransType);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public StockTotalModel GetStockTotalByTerritoryIdAndProductId(int territoryId, int productId)
        {
            StockTotalModel stEntity = new StockTotalModel();
            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetStockTotalByTerritoryIdAndProductId";

                    objCommand.Parameters.AddWithValue("@ProductId", productId);
                    objCommand.Parameters.AddWithValue("@TerritoryId", territoryId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        if (objDS.Rows[0]["id"] != DBNull.Value) stEntity.StockTotalId = Convert.ToInt32(objDS.Rows[0]["id"]);
                        if (objDS.Rows[0]["allocation_qty"] != DBNull.Value) stEntity.AllocQty = Convert.ToDouble(objDS.Rows[0]["allocation_qty"]);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return stEntity;
        }

        public List<DBClaimInvoiceDetailsModel> GetAllDBClaimInvoiceDetailsByInvoiceId(ArgsModel args, string invoiceId)
        {
            DBClaimInvoiceDetailsModel psEntity = new DBClaimInvoiceDetailsModel();
            DataTable objDS = new DataTable();
            List<DBClaimInvoiceDetailsModel> psDetailList = new List<DBClaimInvoiceDetailsModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllDBClaimInvoiceDetailsByInvoiceId";

                    //objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    //objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    //objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    //objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    //objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@InvoiceId", invoiceId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            DBClaimInvoiceDetailsModel psDetailEntity = new DBClaimInvoiceDetailsModel();
                            if (item["claim_invoice_id"] != DBNull.Value) psDetailEntity.DBClaimInvoiceId = Convert.ToDecimal(item["claim_invoice_id"]);
                            if (item["product_id"] != DBNull.Value) psDetailEntity.ProductId = Convert.ToDecimal(item["product_id"]);
                            if (item["product_code"] != DBNull.Value) psDetailEntity.ProductCode = item["product_code"].ToString();
                            if (item["product_name"] != DBNull.Value) psDetailEntity.ProductName = item["product_name"].ToString();
                            if (item["product_price"] != DBNull.Value) psDetailEntity.ProductPrice = Convert.ToDouble(item["product_price"]);
                            if (item["product_inv_qty"] != DBNull.Value) psDetailEntity.ProductInvQty = Convert.ToDouble(item["product_inv_qty"]);
                            if (item["product_inv_value"] != DBNull.Value) psDetailEntity.ProductInvValue = Convert.ToDouble(item["product_inv_value"]);
                            if (item["product_claim_qty"] != DBNull.Value) psDetailEntity.ProductClaimQty = Convert.ToDouble(item["product_claim_qty"]);
                            if (item["product_claim_value"] != DBNull.Value) psDetailEntity.ProductClaimValue = Convert.ToDouble(item["product_claim_value"]);
                            if (item["reason"] != DBNull.Value) psDetailEntity.Reason = item["reason"].ToString();

                            psDetailList.Add(psDetailEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return psDetailList;
        }
    }
}
