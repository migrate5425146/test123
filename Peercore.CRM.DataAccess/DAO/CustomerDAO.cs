﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Entities;
using System.Data.Common;
using System.Collections;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.CRM.DataAccess.datasets;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using System.Data.SqlClient;
using System.Text.RegularExpressions;
using Peercore.CRM.Model;

namespace Peercore.CRM.DataAccess.DAO
{
    public class CustomerDAO:BaseDAO
    {
        private DbSqlAdapter CustomerSql { get; set; }

        //public CustomerDAO()
        //{
        //    this.CustomerSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.CustomerSql.xml");
        //}

        private void RegisterSql()
        {
            this.CustomerSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.CustomerSql.xml", ApplicationService.Instance.DbProvider);
        }

        public CustomerDAO()
        {
            RegisterSql();
        }

        public CustomerDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public CustomerEntity CreateObject()
        {
            return CustomerEntity.CreateObject();
        }

        public CustomerEntity CreateObject(int entityId)
        {
            return CustomerEntity.CreateObject(entityId);
        }

        public CustomerEntity GetCustomer(string customerCode)
        {
            DbDataReader customerReader = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", customerCode, DbType.String);

                customerReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetCustomerDetails"], paramCollection);

                CustomerEntity customer = null;

                if (customerReader.HasRows)
                {
                    int nameOrdinal = customerReader.GetOrdinal("name");
                    int businessOrdinal = customerReader.GetOrdinal("business");
                    int stateOrdinal = customerReader.GetOrdinal("state");
                    int custCodeOrdinal = customerReader.GetOrdinal("cust_code");
                    int telephoneOrdinal = customerReader.GetOrdinal("telephone");
                    int faxOrdinal = customerReader.GetOrdinal("fax");
                    int mobileOrdinal = customerReader.GetOrdinal("mobile");
                    int emailAddressOrdinal = customerReader.GetOrdinal("internet_add");
                    int address1Ordinal = customerReader.GetOrdinal("address_1");
                    int address2Ordinal = customerReader.GetOrdinal("address_2");
                    int cityOrdinal = customerReader.GetOrdinal("city");
                    int postcodeOrdinal = customerReader.GetOrdinal("postcode");
                    int shortNameOrdinal = customerReader.GetOrdinal("short_name");

                    int descriptionOrdinal = customerReader.GetOrdinal("cust_desc");
                    int countryOrdinal = customerReader.GetOrdinal("country");
                    int websiteOrdinal = customerReader.GetOrdinal("website");
                    int prefContactOrdinal = customerReader.GetOrdinal("pref_contact");
                    int companyOrdinal = customerReader.GetOrdinal("company");
                    int lastCalledDateOrdinal = customerReader.GetOrdinal("last_called_date");
                    int litersByOrdinal = customerReader.GetOrdinal("liters_by");
                    int potentialLitersOrdinal = customerReader.GetOrdinal("potential_liters");

                    int ratingOrdinal = customerReader.GetOrdinal("rating");
                    int annualRevenueOrdinal = customerReader.GetOrdinal("annual_revenue");
                    int noofEmployeesOrdinal = customerReader.GetOrdinal("no_of_employees");
                    int businessPotentialOrdinal = customerReader.GetOrdinal("bus_potential");
                    int referredByOrdinal = customerReader.GetOrdinal("referred_by");
                    int sourceOrdinal = customerReader.GetOrdinal("lead_source");
                    int industryOrdinal = customerReader.GetOrdinal("industry");
                    //int assignTo = customerReader.GetOrdinal("potential_liters");
                    int delFlagOrdinal = customerReader.GetOrdinal("del_flag");
                    int creaditStatusOrdinal = customerReader.GetOrdinal("credit_status");
                    int primaryRepNameOrdinal = customerReader.GetOrdinal("PrimaryRep");
                    int secondaryRepNameOrdinal = customerReader.GetOrdinal("SecondaryRep");

                    //int peripheryOrdinal = customerReader.GetOrdinal("periphery_name");
                    //int volumeOrdinal = customerReader.GetOrdinal("volume");
                    //int outlettypeOrdinal = customerReader.GetOrdinal("outlettype_name");
                    //int categoryOrdinal = customerReader.GetOrdinal("category_name");
                    //int MarketOrdinal = customerReader.GetOrdinal("market_name");
                    int peripheryIdOrdinal = customerReader.GetOrdinal("periphery_id");
                    int volumeIdOrdinal = customerReader.GetOrdinal("volume_id");
                    int outlettypeIdOrdinal = customerReader.GetOrdinal("outlet_typeid");
                    int categoryIdOrdinal = customerReader.GetOrdinal("category_id");
                    int MarketIdOrdinal = customerReader.GetOrdinal("market_id");

                    int istlpOrdinal = customerReader.GetOrdinal("istlp");
                    int LongitudeIdOrdinal = customerReader.GetOrdinal("longitude");
                    int LatitudeIdOrdinal = customerReader.GetOrdinal("latitude");
                    int repCodeOrdinal = customerReader.GetOrdinal("rep_code");
                    int routeAssigneIdOrdinal = customerReader.GetOrdinal("route_assign_id");
                    int assignedRouteNameOrdinal = customerReader.GetOrdinal("assigned_route_name");
                    int marketNameOrdinal = customerReader.GetOrdinal("market_name");

                    int documentContentOrdinal = customerReader.GetOrdinal("document_content");
                    int isretail = customerReader.GetOrdinal("is_retail");

                    int outlet_user_codeOrdinal = customerReader.GetOrdinal("outlet_user_code");

                    if (customerReader.Read())
                    {
                        customer = CustomerEntity.CreateObject();

                        if (!customerReader.IsDBNull(nameOrdinal)) customer.Name = customerReader.GetString(nameOrdinal);
                        if (!customerReader.IsDBNull(businessOrdinal)) customer.Business = customerReader.GetString(businessOrdinal).Trim();
                        if (!customerReader.IsDBNull(stateOrdinal)) customer.State = customerReader.GetString(stateOrdinal).Trim();

                        if (!customerReader.IsDBNull(address1Ordinal))
                            customer.Address = customer.Address1 = customerReader.GetString(address1Ordinal);

                        if (!customerReader.IsDBNull(address2Ordinal))
                        {
                            customer.Address2 = customerReader.GetString(address2Ordinal);
                            customer.Address += (!string.IsNullOrWhiteSpace(customerReader.GetString(address2Ordinal)) ?
                           ", " + customerReader.GetString(address2Ordinal) : string.Empty);
                        }

                        if (!customerReader.IsDBNull(cityOrdinal)) customer.City = customerReader.GetString(cityOrdinal);
                        if (!customerReader.IsDBNull(emailAddressOrdinal)) customer.Email = customerReader.GetString(emailAddressOrdinal);
                        if (!customerReader.IsDBNull(mobileOrdinal)) customer.Mobile = customerReader.GetString(mobileOrdinal);
                        if (!customerReader.IsDBNull(postcodeOrdinal)) customer.PostalCode = customerReader.GetString(postcodeOrdinal);
                        if (!customerReader.IsDBNull(telephoneOrdinal)) customer.Telephone = customerReader.GetString(telephoneOrdinal);
                        if (!customerReader.IsDBNull(custCodeOrdinal)) customer.CustomerCode = customerReader.GetString(custCodeOrdinal);
                        if (!customerReader.IsDBNull(shortNameOrdinal)) customer.ShortName = customerReader.GetString(shortNameOrdinal);
                        if (!customerReader.IsDBNull(faxOrdinal)) customer.Fax = customerReader.GetString(faxOrdinal);

                        if (!customerReader.IsDBNull(descriptionOrdinal)) customer.Description = customerReader.GetString(descriptionOrdinal);
                        if (!customerReader.IsDBNull(countryOrdinal)) customer.Country = customerReader.GetString(countryOrdinal);
                        if (!customerReader.IsDBNull(websiteOrdinal)) customer.Website = customerReader.GetString(websiteOrdinal);
                        if (!customerReader.IsDBNull(prefContactOrdinal)) customer.PreferredContact = customerReader.GetString(prefContactOrdinal);
                        if (!customerReader.IsDBNull(companyOrdinal)) customer.Company = customerReader.GetString(companyOrdinal);
                        if (!customerReader.IsDBNull(lastCalledDateOrdinal)) customer.LastCalledDate = customerReader.GetDateTime(lastCalledDateOrdinal);
                        if (!customerReader.IsDBNull(litersByOrdinal)) customer.LitersBy = customerReader.GetString(litersByOrdinal);
                        if (!customerReader.IsDBNull(potentialLitersOrdinal)) customer.PotentialLiters = customerReader.GetDouble(potentialLitersOrdinal);

                        if (!customerReader.IsDBNull(ratingOrdinal)) customer.Rating = customerReader.GetInt32(ratingOrdinal);
                        if (!customerReader.IsDBNull(annualRevenueOrdinal)) customer.AnnualRevenue = customerReader.GetDouble(annualRevenueOrdinal);
                        if (!customerReader.IsDBNull(noofEmployeesOrdinal)) customer.NoOfEmployees = customerReader.GetInt32(noofEmployeesOrdinal);
                        if (!customerReader.IsDBNull(businessPotentialOrdinal)) customer.BusinessPotential = customerReader.GetString(businessPotentialOrdinal);
                        if (!customerReader.IsDBNull(referredByOrdinal)) customer.ReferredBy = customerReader.GetString(referredByOrdinal);
                        if (!customerReader.IsDBNull(sourceOrdinal)) customer.LeadSource = customerReader.GetString(sourceOrdinal);
                        if (!customerReader.IsDBNull(industryOrdinal)) customer.Industry = customerReader.GetString(industryOrdinal);
                        if (!customerReader.IsDBNull(delFlagOrdinal)) customer.Status = customerReader.GetString(delFlagOrdinal);

                        if (!customerReader.IsDBNull(creaditStatusOrdinal)) customer.CreditStatus = customerReader.GetString(creaditStatusOrdinal);
                        if (!customerReader.IsDBNull(primaryRepNameOrdinal)) customer.PrimaryRepName = customerReader.GetString(primaryRepNameOrdinal);
                        if (!customerReader.IsDBNull(secondaryRepNameOrdinal)) customer.SecondaryRepName = customerReader.GetString(secondaryRepNameOrdinal);


                        if (!customerReader.IsDBNull(istlpOrdinal)) customer.isTLP = customerReader.GetBoolean(istlpOrdinal);
                        if (!customerReader.IsDBNull(LongitudeIdOrdinal)) customer.Longitude = customerReader.GetDouble(LongitudeIdOrdinal);
                        if (!customerReader.IsDBNull(LatitudeIdOrdinal)) customer.Latitiude = customerReader.GetDouble(LatitudeIdOrdinal);
                        //if (!customerReader.IsDBNull(peripheryOrdinal)) customer.Periphery = customerReader.GetString(peripheryOrdinal);
                        //if (!customerReader.IsDBNull(outlettypeOrdinal)) customer.OutletType = customerReader.GetString(outlettypeOrdinal);
                        //if (!customerReader.IsDBNull(categoryOrdinal)) customer.Category = customerReader.GetString(categoryOrdinal);
                        //if (!customerReader.IsDBNull(MarketOrdinal)) customer.MarketName = customerReader.GetString(MarketOrdinal);
                        //if (!customerReader.IsDBNull(volumeOrdinal)) customer.Volume = customerReader.GetString(volumeOrdinal);

                        if (!customerReader.IsDBNull(peripheryIdOrdinal)) customer.PeripheryId = customerReader.GetInt32(peripheryIdOrdinal);
                        if (!customerReader.IsDBNull(outlettypeIdOrdinal)) customer.OutletTypeId = customerReader.GetInt32(outlettypeIdOrdinal);
                        if (!customerReader.IsDBNull(categoryIdOrdinal)) customer.CategoryId = customerReader.GetInt32(categoryIdOrdinal);
                        if (!customerReader.IsDBNull(MarketIdOrdinal)) customer.MarketId = customerReader.GetInt32(MarketIdOrdinal);
                        if (!customerReader.IsDBNull(volumeIdOrdinal)) customer.VolumeId = customerReader.GetInt32(volumeIdOrdinal);
                        if (!customerReader.IsDBNull(repCodeOrdinal)) customer.RepCode = customerReader.GetString(repCodeOrdinal);
                        if (!customerReader.IsDBNull(assignedRouteNameOrdinal)) customer.AssignedRouteName = customerReader.GetString(assignedRouteNameOrdinal);
                        if (!customerReader.IsDBNull(routeAssigneIdOrdinal)) customer.RouteAssignId = customerReader.GetInt32(routeAssigneIdOrdinal);
                        if (!customerReader.IsDBNull(marketNameOrdinal)) customer.MarketName = customerReader.GetString(marketNameOrdinal);
                        if (!customerReader.IsDBNull(isretail)) customer.IsRetail = customerReader.GetBoolean(isretail);
                        if (customerReader[documentContentOrdinal] != null)
                        {
                            if (!customerReader.IsDBNull(documentContentOrdinal)) customer.ImageContent = (byte[])customerReader[documentContentOrdinal];
                        }
                        if (!customerReader.IsDBNull(outlet_user_codeOrdinal)) customer.OutletUserCode = customerReader.GetString(outlet_user_codeOrdinal);

                    }

                }

                return customer;

            }
            catch
            {

                throw;
            }
            finally
            {
                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();
            }
        }

        public List<LeadCustomerViewEntity> GetPendingCustomerCollection(ArgsEntity args)
        {
            DbDataReader customerReader = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                paramCollection.Add("@ChildOriginatorsCust", args.ChildOriginators.Replace("originator", "r.originator"), DbType.String);//cust
                paramCollection.Add("@DefaultDepId", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@LeadStageId", args.LeadStageId, DbType.Int32);
                paramCollection.Add("@RetreiveActive", args.ActiveInactiveChecked, DbType.Boolean);
                paramCollection.Add("@IsReqSentVisible", args.ReqSentIsChecked, DbType.Boolean);
                paramCollection.Add("@RepType", args.RepType, DbType.String);//cust
                paramCollection.Add("@LeadStage", args.LeadStage, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                List<LeadCustomerViewEntity> leadCollection = new List<LeadCustomerViewEntity>();
                customerReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetPendingCustomers"], paramCollection);

                if (customerReader.HasRows)
                {
                    int nameOrdinal = customerReader.GetOrdinal("name");
                    int stateOrdinal = customerReader.GetOrdinal("state");
                    int custCodeOrdinal = customerReader.GetOrdinal("cust_code");
                    int telephoneOrdinal = customerReader.GetOrdinal("telephone");
                    int address1Ordinal = customerReader.GetOrdinal("address_1");
                    int address2Ordinal = customerReader.GetOrdinal("address_2");
                    int cityOrdinal = customerReader.GetOrdinal("city");
                    int leadSourceOrdinal = customerReader.GetOrdinal("lead_source");
                    int delFlag = customerReader.GetOrdinal("del_flag");
                    int leadIdOrdinal = customerReader.GetOrdinal("lead_id");
                    int leadStageIdOrdinal = customerReader.GetOrdinal("lead_stage_id");
                    int leadStageOrdinal = customerReader.GetOrdinal("lead_stage");
                    int leadStatusOrdinal = customerReader.GetOrdinal("lead_status");
                    int totalCountOrdinal = customerReader.GetOrdinal("total_count");
                    int peripheryOrdinal = customerReader.GetOrdinal("periphery_name");
                    int volumeOrdinal = customerReader.GetOrdinal("volume");
                    int outlettypeOrdinal = customerReader.GetOrdinal("outlettype_name");
                    int categoryOrdinal = customerReader.GetOrdinal("category_name");
                    int MarketOrdinal = customerReader.GetOrdinal("market_name");
                    int istlpOrdinal = customerReader.GetOrdinal("istlp");
                    int LongitudeIdOrdinal = customerReader.GetOrdinal("longitude");
                    int LatitudeIdOrdinal = customerReader.GetOrdinal("latitude");

                    while (customerReader.Read())
                    {
                        LeadCustomerViewEntity leadCustomerView = LeadCustomerViewEntity.CreateObject();

                        if (!customerReader.IsDBNull(nameOrdinal)) leadCustomerView.Name = customerReader.GetString(nameOrdinal);
                        if (!customerReader.IsDBNull(stateOrdinal)) leadCustomerView.LeadStatus = customerReader.GetString(stateOrdinal);
                        leadCustomerView.LeadCustomerType = LeadCustomerTypes.Customer;

                        if (!customerReader.IsDBNull(stateOrdinal)) leadCustomerView.State = customerReader.GetString(stateOrdinal).Trim();

                        if (!customerReader.IsDBNull(address1Ordinal))
                            leadCustomerView.Address = leadCustomerView.Address1 = customerReader.GetString(address1Ordinal);

                        if (!customerReader.IsDBNull(address2Ordinal))
                        {
                            leadCustomerView.Address2 = customerReader.GetString(address2Ordinal);
                            leadCustomerView.Address += (!string.IsNullOrWhiteSpace(customerReader.GetString(address2Ordinal)) ?
                           ", " + customerReader.GetString(address2Ordinal) : string.Empty);
                        }

                        if (!customerReader.IsDBNull(cityOrdinal)) leadCustomerView.City = customerReader.GetString(cityOrdinal).Trim();
                        if (!customerReader.IsDBNull(telephoneOrdinal)) leadCustomerView.Telephone = customerReader.GetString(telephoneOrdinal);
                        if (!customerReader.IsDBNull(custCodeOrdinal)) leadCustomerView.CustomerCode = customerReader.GetString(custCodeOrdinal).Trim();
                        if (!customerReader.IsDBNull(istlpOrdinal)) leadCustomerView.isTLP = customerReader.GetBoolean(istlpOrdinal);
                        if (!customerReader.IsDBNull(LongitudeIdOrdinal)) leadCustomerView.Longitude = customerReader.GetDouble(LongitudeIdOrdinal);
                        if (!customerReader.IsDBNull(LatitudeIdOrdinal)) leadCustomerView.Latitiude = customerReader.GetDouble(LatitudeIdOrdinal);
                        if (!customerReader.IsDBNull(peripheryOrdinal)) leadCustomerView.Periphery = customerReader.GetString(peripheryOrdinal);
                        if (!customerReader.IsDBNull(outlettypeOrdinal)) leadCustomerView.OutletType = customerReader.GetString(outlettypeOrdinal);
                        if (!customerReader.IsDBNull(categoryOrdinal)) leadCustomerView.Category = customerReader.GetString(categoryOrdinal);
                        if (!customerReader.IsDBNull(MarketOrdinal)) leadCustomerView.MarketName = customerReader.GetString(MarketOrdinal);
                        if (!customerReader.IsDBNull(volumeOrdinal)) leadCustomerView.Volume = customerReader.GetString(volumeOrdinal);
                        if (!customerReader.IsDBNull(leadSourceOrdinal)) leadCustomerView.LeadSource = customerReader.GetString(leadSourceOrdinal).Trim();
                        if (!customerReader.IsDBNull(delFlag)) leadCustomerView.IsDeleted = customerReader.GetString(delFlag);
                        if (!customerReader.IsDBNull(leadIdOrdinal)) leadCustomerView.SourceId = customerReader.GetInt32(leadIdOrdinal);
                        if (!customerReader.IsDBNull(leadStageOrdinal)) leadCustomerView.LeadStage = customerReader.GetString(leadStageOrdinal);
                        if (!customerReader.IsDBNull(leadStatusOrdinal)) leadCustomerView.LeadStatus = customerReader.GetString(leadStatusOrdinal);
                        if (!customerReader.IsDBNull(totalCountOrdinal)) leadCustomerView.RowCount = customerReader.GetInt32(totalCountOrdinal);

                        leadCollection.Add(leadCustomerView);
                    }
                }
                return leadCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();
            }
        }

        public List<LeadAddressEntity> GetCustomerAddresses(string custCode,ArgsEntity args)
        {
            DbDataReader idrContactDetails = null;

            try
            {
                List<LeadAddressEntity> leadAddresses = new List<LeadAddressEntity>();

            
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AssigneeCode", custCode, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", args.AdditionalParams, DbType.String);

                idrContactDetails = this.DataAcessService.ExecuteQuery(CustomerSql["GetCustomerAddresses"], paramCollection);


                if (idrContactDetails != null && idrContactDetails.HasRows)
                {
                    int assigneeNumberOrdinal = idrContactDetails.GetOrdinal("assignee_no");
                    int assigneeCodeOrdinal = idrContactDetails.GetOrdinal("assignee_code");
                    int address1Ordinal = idrContactDetails.GetOrdinal("address_1");
                    int address2Ordinal = idrContactDetails.GetOrdinal("address_2");
                    int cityOrdinal = idrContactDetails.GetOrdinal("city");
                    int stateOrdinal = idrContactDetails.GetOrdinal("state");
                    int postcodeOrdinal = idrContactDetails.GetOrdinal("postcode");
                    int areaCodeOrdinal = idrContactDetails.GetOrdinal("area_code");
                    int countryOrdinal = idrContactDetails.GetOrdinal("country");
                    int nameOrdinal = idrContactDetails.GetOrdinal("name");
                    int contactOrdinal = idrContactDetails.GetOrdinal("contact");
                    int telephoneOrdinal = idrContactDetails.GetOrdinal("telephone");
                    int faxOrdinal = idrContactDetails.GetOrdinal("fax");
                    int addressTypeOrdinal = idrContactDetails.GetOrdinal("address_type");
                    int assignToOrdinal = idrContactDetails.GetOrdinal("assign_to");
                    int modifiedByOrdinal = idrContactDetails.GetOrdinal("modified_by");

                    while (idrContactDetails.Read())
                    {
                        LeadAddressEntity leadAddress = LeadAddressEntity.CreateObject();

                        if (!idrContactDetails.IsDBNull(assigneeNumberOrdinal)) leadAddress.AssigneeNo = System.Convert.ToInt32(idrContactDetails[assigneeNumberOrdinal]);
                        if (!idrContactDetails.IsDBNull(assigneeCodeOrdinal)) leadAddress.CustCode = idrContactDetails.GetString(assigneeCodeOrdinal);
                        if (!idrContactDetails.IsDBNull(address1Ordinal)) leadAddress.Address1 = idrContactDetails.GetString(address1Ordinal);
                        if (!idrContactDetails.IsDBNull(address2Ordinal)) leadAddress.Address2 = idrContactDetails.GetString(address2Ordinal);
                        if (!idrContactDetails.IsDBNull(cityOrdinal)) leadAddress.City = idrContactDetails.GetString(cityOrdinal);
                        if (!idrContactDetails.IsDBNull(stateOrdinal)) leadAddress.State = idrContactDetails.GetString(stateOrdinal);
                        if (!idrContactDetails.IsDBNull(postcodeOrdinal)) leadAddress.PostCode = idrContactDetails.GetString(postcodeOrdinal);
                        if (!idrContactDetails.IsDBNull(areaCodeOrdinal)) leadAddress.AreaCode = idrContactDetails.GetString(areaCodeOrdinal);
                        if (!idrContactDetails.IsDBNull(countryOrdinal)) leadAddress.Country = idrContactDetails.GetString(countryOrdinal);
                        if (!idrContactDetails.IsDBNull(nameOrdinal)) leadAddress.Name = idrContactDetails.GetString(nameOrdinal);
                        if (!idrContactDetails.IsDBNull(contactOrdinal)) leadAddress.Contact = idrContactDetails.GetString(contactOrdinal);
                        if (!idrContactDetails.IsDBNull(telephoneOrdinal)) leadAddress.Telephone = idrContactDetails.GetString(telephoneOrdinal);
                        if (!idrContactDetails.IsDBNull(faxOrdinal)) leadAddress.Fax = idrContactDetails.GetString(faxOrdinal);
                        if (!idrContactDetails.IsDBNull(addressTypeOrdinal)) leadAddress.AddressType = idrContactDetails.GetString(addressTypeOrdinal);
                        if (!idrContactDetails.IsDBNull(assignToOrdinal)) leadAddress.AssignedTo = idrContactDetails.GetString(assignToOrdinal);
                        if (!idrContactDetails.IsDBNull(modifiedByOrdinal)) leadAddress.LastModifiedBy = idrContactDetails.GetString(modifiedByOrdinal);

                        leadAddresses.Add(leadAddress);
                    }
                }

                return leadAddresses;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrContactDetails != null)
                    idrContactDetails.Close();
            }
        }

        public List<RepCustomerViewEntity> GetNewCustomerCount(ArgsEntity args)
        {
            DbDataReader repReader = null;

            try
            {
                List<RepCustomerViewEntity> repCollection = new List<RepCustomerViewEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Year", args.SMonth, DbType.Int32);
                paramCollection.Add("@Month", args.SMonth, DbType.Int32);
                paramCollection.Add("@DefaultDeptID", args.DefaultDepartmentId, DbType.String);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);

                repReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetNewCustomersByRep"], paramCollection);


                RepCustomerViewEntity rep = null;

                if (repReader.HasRows)
                {
                    int stateOrdinal = repReader.GetOrdinal("state");
                    //int createdByOrdinal = repReader.GetOrdinal("created_by");
                    int nameOrdinal = repReader.GetOrdinal("name");
                    int newCustomersOrdinal = repReader.GetOrdinal("NewCustomers");

                    while (repReader.Read())
                    {
                        rep = RepCustomerViewEntity.CreateObject();

                        if (!repReader.IsDBNull(stateOrdinal)) rep.State = repReader.GetString(stateOrdinal);
                        //if (!repReader.IsDBNull(createdByOrdinal)) rep.Originator = repReader.GetString(createdByOrdinal);
                        if (!repReader.IsDBNull(nameOrdinal)) rep.Name = repReader.GetString(nameOrdinal);
                        if (!repReader.IsDBNull(newCustomersOrdinal)) rep.NewCustomers = repReader.GetInt32(newCustomersOrdinal);

                        repCollection.Add(rep);
                    }

                }

                return repCollection;

            }
            catch
            {
                throw;
            }
            finally
            {
                if (repReader != null)
                    repReader.Close();
            }
        }

        public LeadCustomerViewEntity GetCustomerView(string customerCode, string defaultDeptID)
        {                      
            DbDataReader customerReader = null;

            try
            {
                LeadCustomerViewEntity leadCustomerView = LeadCustomerViewEntity.CreateObject();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@defaultDeptID", defaultDeptID, DbType.String);
                paramCollection.Add("@custCode", customerCode, DbType.String);

                customerReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetCustomers"], paramCollection);
               
                if (customerReader.HasRows)
                {
                    int nameOrdinal = customerReader.GetOrdinal("name");
                    int businessOrdinal = customerReader.GetOrdinal("business");
                    int stateOrdinal = customerReader.GetOrdinal("state");
                    int custCodeOrdinal = customerReader.GetOrdinal("cust_code");

                    int telephoneOrdinal = customerReader.GetOrdinal("telephone");
                    int faxOrdinal = customerReader.GetOrdinal("fax");
                    int mobileOrdinal = customerReader.GetOrdinal("mobile");
                    int emailAddressOrdinal = customerReader.GetOrdinal("internet_add");
                    int address1Ordinal = customerReader.GetOrdinal("address_1");
                    int address2Ordinal = customerReader.GetOrdinal("address_2");

                    int cityOrdinal = customerReader.GetOrdinal("city");

                    int postcodeOrdinal = customerReader.GetOrdinal("postcode");
                    int originatorOrdinal = customerReader.GetOrdinal("originator");

                    int leadSourceOrdinal = customerReader.GetOrdinal("lead_source");
                    int industryOrdinal = customerReader.GetOrdinal("industry");
                    int referredByOrdinal = customerReader.GetOrdinal("referred_by");
                    int ratingOrdinal = customerReader.GetOrdinal("rating");
                    int probabilityOrdinal = customerReader.GetOrdinal("probability");
                    int noOfEmployeesOrdinal = customerReader.GetOrdinal("no_of_employees");
                    int annualRevenueOrdinal = customerReader.GetOrdinal("annual_revenue");
                    int descriptionOrdinal = customerReader.GetOrdinal("cust_desc");
                    int countryOrdinal = customerReader.GetOrdinal("country");
                    int websiteOrdinal = customerReader.GetOrdinal("website");
                    int prefContactOrdinal = customerReader.GetOrdinal("pref_contact");
                    int busPotentialOrdinal = customerReader.GetOrdinal("bus_potential");
                    int companyOrdinal = customerReader.GetOrdinal("company");

                    int industryDescriptionOrdinal = customerReader.GetOrdinal("industry_description");
                    int prefContactDescriptionOrdinal = customerReader.GetOrdinal("pref_contact_description");
                    int channelDescriptionOrdinal = customerReader.GetOrdinal("channel_description");

                    while (customerReader.Read())
                    {

                        if (!customerReader.IsDBNull(nameOrdinal)) leadCustomerView.Name = customerReader.GetString(nameOrdinal);
                        if (!customerReader.IsDBNull(businessOrdinal)) leadCustomerView.Business = customerReader.GetString(businessOrdinal);
                        if (!customerReader.IsDBNull(stateOrdinal)) leadCustomerView.LeadStatus = customerReader.GetString(stateOrdinal);
                        leadCustomerView.LeadCustomerType = LeadCustomerTypes.Customer;

                        if (!customerReader.IsDBNull(stateOrdinal)) leadCustomerView.State = customerReader.GetString(stateOrdinal).Trim();

                        if (!customerReader.IsDBNull(address1Ordinal))
                            leadCustomerView.Address = customerReader.GetString(address1Ordinal);

                        if (!customerReader.IsDBNull(address2Ordinal))
                            leadCustomerView.Address += (!string.IsNullOrWhiteSpace(customerReader.GetString(address2Ordinal)) ?
                           ", " + customerReader.GetString(address2Ordinal) : string.Empty);

                        if (!customerReader.IsDBNull(cityOrdinal)) leadCustomerView.City = customerReader.GetString(cityOrdinal).Trim();
                        if (!customerReader.IsDBNull(emailAddressOrdinal)) leadCustomerView.Email = customerReader.GetString(emailAddressOrdinal);
                        if (!customerReader.IsDBNull(mobileOrdinal)) leadCustomerView.Mobile = customerReader.GetString(mobileOrdinal);
                        if (!customerReader.IsDBNull(postcodeOrdinal)) leadCustomerView.PostalCode = customerReader.GetString(postcodeOrdinal);
                        if (!customerReader.IsDBNull(telephoneOrdinal)) leadCustomerView.Telephone = customerReader.GetString(telephoneOrdinal);
                        if (!customerReader.IsDBNull(custCodeOrdinal)) leadCustomerView.CustomerCode = customerReader.GetString(custCodeOrdinal).Trim();
                        if (!customerReader.IsDBNull(originatorOrdinal)) leadCustomerView.Originator = customerReader.GetString(originatorOrdinal);

                        if (!customerReader.IsDBNull(leadSourceOrdinal)) leadCustomerView.LeadSource = customerReader.GetString(leadSourceOrdinal).Trim();
                        if (!customerReader.IsDBNull(industryOrdinal)) leadCustomerView.Industry = customerReader.GetString(industryOrdinal);
                        if (!customerReader.IsDBNull(referredByOrdinal)) leadCustomerView.ReferredBy = customerReader.GetString(referredByOrdinal);
                        if (!customerReader.IsDBNull(ratingOrdinal)) leadCustomerView.Rating = customerReader.GetInt32(ratingOrdinal);
                        //if (!customerReader.IsDBNull(probabilityOrdinal)) leadCustomerView.pro = customerReader.GetString(probabilityOrdinal);
                        if (!customerReader.IsDBNull(websiteOrdinal)) leadCustomerView.Webiste = customerReader.GetString(websiteOrdinal);
                        if (!customerReader.IsDBNull(noOfEmployeesOrdinal)) leadCustomerView.NoOfEmployees = customerReader.GetInt32(noOfEmployeesOrdinal);
                        if (!customerReader.IsDBNull(annualRevenueOrdinal)) leadCustomerView.AnnualRevenue = customerReader.GetDouble(annualRevenueOrdinal);
                        if (!customerReader.IsDBNull(descriptionOrdinal)) leadCustomerView.Description = customerReader.GetString(descriptionOrdinal);
                        if (!customerReader.IsDBNull(prefContactOrdinal)) leadCustomerView.PreferredContact = customerReader.GetString(prefContactOrdinal);
                        if (!customerReader.IsDBNull(busPotentialOrdinal)) leadCustomerView.BusinessPotential = customerReader.GetString(busPotentialOrdinal);
                        if (!customerReader.IsDBNull(countryOrdinal)) leadCustomerView.Country = customerReader.GetString(countryOrdinal);
                        if (!customerReader.IsDBNull(companyOrdinal)) leadCustomerView.Company = customerReader.GetString(companyOrdinal);

                        if (!customerReader.IsDBNull(industryDescriptionOrdinal)) leadCustomerView.IndustryDescription = customerReader.GetString(industryDescriptionOrdinal);
                        if (!customerReader.IsDBNull(prefContactDescriptionOrdinal)) leadCustomerView.PreferredContactDescription = customerReader.GetString(prefContactDescriptionOrdinal);
                        if (!customerReader.IsDBNull(channelDescriptionOrdinal)) leadCustomerView.ChannelDescription = customerReader.GetString(channelDescriptionOrdinal);
                        leadCustomerView.LeadStage = "Customer";


                    }
                }

                return leadCustomerView;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();
            }
        }
       
        public bool InsertCustomerAddress(LeadAddressEntity customerAddress)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@assignTo", customerAddress.AssignedTo, DbType.String);
                paramCollection.Add("@assigneeNo", customerAddress.AssigneeNo, DbType.Int32);
                paramCollection.Add("@assigneeCode", customerAddress.CustCode, DbType.String);
                paramCollection.Add("@addressType", customerAddress.AddressType, DbType.String);
                paramCollection.Add("@name", customerAddress.Name, DbType.String);
                paramCollection.Add("@address1", customerAddress.Address1, DbType.String);
                paramCollection.Add("@address2", customerAddress.Address2, DbType.String);
                paramCollection.Add("@city", customerAddress.City, DbType.String);
                paramCollection.Add("@state", customerAddress.State, DbType.String);
                paramCollection.Add("@postcode", customerAddress.PostCode, DbType.String);
                paramCollection.Add("@areacode", string.IsNullOrEmpty(customerAddress.AreaCode) ? "" : customerAddress.AreaCode, DbType.String);
                paramCollection.Add("@telephone", customerAddress.Telephone, DbType.String);
                paramCollection.Add("@fax", customerAddress.Fax, DbType.String);
                paramCollection.Add("@contact", customerAddress.Contact, DbType.String);
                paramCollection.Add("@country", customerAddress.Country, DbType.String);
                paramCollection.Add("@modifiedby", customerAddress.CreatedBy, DbType.String);
                

                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["TransformAddressToCustomer"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateCustomerAddress(LeadAddressEntity customerAddress)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                  paramCollection.Add("@addressType", customerAddress.AddressType, DbType.String);
                  paramCollection.Add("@name", customerAddress.Name, DbType.String);
                  paramCollection.Add("@address1", customerAddress.Address1, DbType.String);
                  paramCollection.Add("@address2", customerAddress.Address2, DbType.String);
                  paramCollection.Add("@city", customerAddress.City, DbType.String);
                  paramCollection.Add("@state", customerAddress.State, DbType.String);
                  paramCollection.Add("@postcode", customerAddress.PostCode, DbType.String);
                  paramCollection.Add("@areaCode", string.IsNullOrEmpty(customerAddress.AreaCode) ? "" : customerAddress.AreaCode, DbType.String);
                  paramCollection.Add("@telephone", customerAddress.Telephone, DbType.String);
                  paramCollection.Add("@fax", customerAddress.Fax, DbType.String);
                  paramCollection.Add("@contact", customerAddress.Contact, DbType.String);
                  paramCollection.Add("@country", customerAddress.Country, DbType.String);
                  paramCollection.Add("@assigneeCode", customerAddress.CustCode, DbType.String);
                  paramCollection.Add("@assigneeNo", customerAddress.AssigneeNo, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["UpdateCustomerAddress"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public int GetNextCustomerAssigneeNo(LeadAddressEntity customerAddress)
        {
            try
            {
           
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@assigneeCode", customerAddress.CustCode, DbType.String);
                paramCollection.Add("@assignTo", customerAddress.AssignedTo, DbType.String);
                paramCollection.Add("@addressType", customerAddress.AddressType, DbType.String);

                return System.Convert.ToInt32(this.DataAcessService.GetOneValue(CustomerSql["GetNextCustomerAssigneeNo"],paramCollection));   // Get Next ID               
            }
            catch
            {
                throw;
            }
        }

        public LeadAddressEntity GetCustomerTableAddresses(string custCode)
        {
            DbDataReader idrContactDetails = null;
            LeadAddressEntity leadAddress = LeadAddressEntity.CreateObject();

            try
            {               
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@custCode", custCode, DbType.String);

                idrContactDetails = this.DataAcessService.ExecuteQuery(CustomerSql["GetCustomerTableAddress"], paramCollection);

                if (idrContactDetails != null && idrContactDetails.HasRows)
                {
                    int custCodeOrdinal = idrContactDetails.GetOrdinal("cust_code");
                    int address1Ordinal = idrContactDetails.GetOrdinal("address_1");
                    int cityOrdinal = idrContactDetails.GetOrdinal("city");
                    int stateOrdinal = idrContactDetails.GetOrdinal("state");
                    int postcodeOrdinal = idrContactDetails.GetOrdinal("postcode");


                    while (idrContactDetails.Read())
                    {
                        if (!idrContactDetails.IsDBNull(address1Ordinal)) leadAddress.Address1 = idrContactDetails.GetString(address1Ordinal);
                        if (!idrContactDetails.IsDBNull(custCodeOrdinal)) leadAddress.CustCode = idrContactDetails.GetString(custCodeOrdinal);
                        if (!idrContactDetails.IsDBNull(cityOrdinal)) leadAddress.City = idrContactDetails.GetString(cityOrdinal);
                        if (!idrContactDetails.IsDBNull(stateOrdinal)) leadAddress.State = idrContactDetails.GetString(stateOrdinal);
                        if (!idrContactDetails.IsDBNull(postcodeOrdinal)) leadAddress.PostCode = idrContactDetails.GetString(postcodeOrdinal);
                    }
                }

                return leadAddress;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrContactDetails != null)
                    idrContactDetails.Close();
            }
        }

        public List<SalesOrderEntity> GetOutstandingDeliveries(string custCode)
        {
            DbDataReader idrOrder = null;
            List<SalesOrderEntity> listSalesOrder;
            SalesOrderEntity salesOrder;

            try
            {
                listSalesOrder = new List<SalesOrderEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", custCode, DbType.String);

                idrOrder = this.DataAcessService.ExecuteQuery(CustomerSql["GetOutstandingDeliveries"], paramCollection);


                if (idrOrder.HasRows)
                {
                    int SOrderNoOrdinal = idrOrder.GetOrdinal("sorder_no");
                    int PListNoOrdinal = idrOrder.GetOrdinal("plist_no");
                    int DateRequiredOrdinal = idrOrder.GetOrdinal("new_date_required");
                    int CatlogCodeOrdinal = idrOrder.GetOrdinal("catlog_code");
                    int DescriptionOrdinal = idrOrder.GetOrdinal("description");
                    int OrderDateOrdinal = idrOrder.GetOrdinal("order_date");
                    int ReqdQtyOrdinal = idrOrder.GetOrdinal("reqd_qty");
                    int PriceOrdinal = idrOrder.GetOrdinal("price");

                    while (idrOrder.Read())
                    {
                        salesOrder = SalesOrderEntity.CreateObject();

                        if (!idrOrder.IsDBNull(SOrderNoOrdinal)) salesOrder.SOrderNo = idrOrder.GetInt32(SOrderNoOrdinal);
                        if (!idrOrder.IsDBNull(PListNoOrdinal)) salesOrder.PListNo = idrOrder.GetInt32(PListNoOrdinal);
                        if (!idrOrder.IsDBNull(DateRequiredOrdinal)) salesOrder.DateRequired = idrOrder.GetDateTime(DateRequiredOrdinal);
                        if (!idrOrder.IsDBNull(CatlogCodeOrdinal)) salesOrder.CatlogCode = idrOrder.GetString(CatlogCodeOrdinal).Trim();
                        if (!idrOrder.IsDBNull(DescriptionOrdinal)) salesOrder.Description = idrOrder.GetString(DescriptionOrdinal);
                        if (!idrOrder.IsDBNull(OrderDateOrdinal)) salesOrder.OrderDate = idrOrder.GetDateTime(OrderDateOrdinal);
                        if (!idrOrder.IsDBNull(ReqdQtyOrdinal)) salesOrder.RequiredQty = idrOrder.GetDouble(ReqdQtyOrdinal);
                        if (!idrOrder.IsDBNull(PriceOrdinal)) salesOrder.Price = idrOrder.GetDouble(PriceOrdinal);

                        listSalesOrder.Add(salesOrder);
                    }
                }

                return listSalesOrder;
            }
            catch (Exception)
            {

                throw;
            }
            finally
            {
                if (idrOrder != null)
                    idrOrder.Close();
            }
        }

        public int GetOutstandingDeliveriesCount(string custCode)
        {
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", custCode, DbType.String);
                return System.Convert.ToInt32(this.DataAcessService.GetOneValue(CustomerSql["GetOutstandingDeliveriesCount"], paramCollection)); 

            }
            catch (Exception)
            {

                throw;
            }
        }

        public DateTime GetLastDelivery(string custCode)
        {
            try
            {                
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@custCode", custCode, DbType.String);

                return System.Convert.ToDateTime(this.DataAcessService.GetOneValue(CustomerSql["GetLastDelivery"], paramCollection));                  
            }
            catch (Exception)
            {

                throw;
            }
        }

        public List<LeadCustomerViewEntity> GetCustomerCollection(ArgsEntity args)
        {

            //public ObservableCollection<LeadCustomerView> GetCustomerCollection(string sOriginator, bool retreiveActive=true,
            //    string sRepType = "", string defaultDeptID = "", string childOriginators="")
            //{
            //clsOriginator oOriginator = new clsOriginator();
            string sOriginators = "";
            string whereCls = "";
            DbDataReader customerReader = null;

            try
            {
                //if (retreiveActive)
                //    whereCls = " c.del_flag != 'Y' ";
                //else
                //    whereCls = " c.del_flag = 'Y' ";

                //if (sRepType != "")
                //    whereCls += " AND (c.cust_type = '" + sRepType + "' OR c.cust_type = 'A')";

                List<LeadCustomerViewEntity> customerReaderCollection = new List<LeadCustomerViewEntity>();

                ////sOriginators = oOriginator.GetChildOriginators(sOriginator);
                //sOriginators = childOriginators;

                //if (!string.IsNullOrWhiteSpace(sOriginators))
                //{
                //    whereCls = !string.IsNullOrWhiteSpace(whereCls) ? whereCls + " AND " + sOriginators : sOriginators;
                //    whereCls = whereCls.Replace("originator", "r.originator");
                //}

                //string sSql = "";

                //sSql = string.Format(CRMSQLs.GetCustomers, whereCls, defaultDeptID);

                //if (!string.IsNullOrEmpty(orderBy))
                //{
                //    sSql = "SELECT d.* FROM (" + sSql + ")d " + additionalParams + "ORDER BY d." + orderBy;
                //}
                ////whereCls = !string.IsNullOrWhiteSpace(whereCls) ? " WHERE " + whereCls : string.Empty;


                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@childOriginators", args.ChildOriginators.Replace("originator", "r.originator"), DbType.String);
                paramCollection.Add("@defaultDepId", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@AddressType", args.AddressType, DbType.String);
                //paramCollection.Add("@LeadStageId", args.LeadStageId, DbType.Int32);
                paramCollection.Add("@RetreiveActive", args.ActiveInactiveChecked, DbType.Boolean);
                //paramCollection.Add("@IsReqSentVisible", isReqSentVisible, DbType.Boolean);
                paramCollection.Add("@RepType", args.RepType, DbType.String);

                customerReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetCustomerCollection"], paramCollection);

                if (customerReader.HasRows)
                {
                    int nameOrdinal = customerReader.GetOrdinal("name");
                    int businessOrdinal = customerReader.GetOrdinal("business");
                    int stateOrdinal = customerReader.GetOrdinal("state");
                    int custCodeOrdinal = customerReader.GetOrdinal("cust_code");

                    int telephoneOrdinal = customerReader.GetOrdinal("telephone");
                    int faxOrdinal = customerReader.GetOrdinal("fax");
                    int mobileOrdinal = customerReader.GetOrdinal("mobile");
                    int emailAddressOrdinal = customerReader.GetOrdinal("internet_add");
                    int address1Ordinal = customerReader.GetOrdinal("address_1");
                    int address2Ordinal = customerReader.GetOrdinal("address_2");

                    int cityOrdinal = customerReader.GetOrdinal("city");

                    int postcodeOrdinal = customerReader.GetOrdinal("postcode");
                    int originatorOrdinal = customerReader.GetOrdinal("primary_rep");

                    int leadSourceOrdinal = customerReader.GetOrdinal("lead_source");
                    int industryOrdinal = customerReader.GetOrdinal("industry");
                    int referredByOrdinal = customerReader.GetOrdinal("referred_by");
                    int ratingOrdinal = customerReader.GetOrdinal("rating");
                    int probabilityOrdinal = customerReader.GetOrdinal("probability");
                    int noOfEmployeesOrdinal = customerReader.GetOrdinal("no_of_employees");
                    int annualRevenueOrdinal = customerReader.GetOrdinal("annual_revenue");
                    int descriptionOrdinal = customerReader.GetOrdinal("cust_desc");
                    int countryOrdinal = customerReader.GetOrdinal("country");
                    int websiteOrdinal = customerReader.GetOrdinal("website");
                    int prefContactOrdinal = customerReader.GetOrdinal("pref_contact");
                    int busPotentialOrdinal = customerReader.GetOrdinal("bus_potential");
                    int companyOrdinal = customerReader.GetOrdinal("company");

                    int peripheryIdOrdinal = customerReader.GetOrdinal("periphery_id");
                    int volumeIdOrdinal = customerReader.GetOrdinal("volume_id");
                    int outlettypeIdOrdinal = customerReader.GetOrdinal("outlet_typeid");
                    int categoryIdOrdinal = customerReader.GetOrdinal("category_id");
                    int istlpOrdinal = customerReader.GetOrdinal("istlp");
                    int MarketIdOrdinal = customerReader.GetOrdinal("market_id");
                    int LongitudeIdOrdinal = customerReader.GetOrdinal("longitude");
                    int LatitudeIdOrdinal = customerReader.GetOrdinal("latitude");

                    int industryDescriptionOrdinal = customerReader.GetOrdinal("industry_description");
                    int prefContactDescriptionOrdinal = customerReader.GetOrdinal("pref_contact_description");
                    int channelDescriptionOrdinal = customerReader.GetOrdinal("channel_description");
                    int delFlag = customerReader.GetOrdinal("del_flag");
                    int secondaryRepOrdinal = customerReader.GetOrdinal("secondary_rep");

                    int peripheryOrdinal = customerReader.GetOrdinal("periphery_name");
                    int volumeOrdinal = customerReader.GetOrdinal("volume");
                    int outlettypeOrdinal = customerReader.GetOrdinal("outlettype_name");
                    int categoryOrdinal = customerReader.GetOrdinal("category_name");
                    int MarketOrdinal = customerReader.GetOrdinal("market_name");


                    while (customerReader.Read())
                    {
                        LeadCustomerViewEntity leadCustomerView = LeadCustomerViewEntity.CreateObject();

                        if (!customerReader.IsDBNull(nameOrdinal)) leadCustomerView.Name = customerReader.GetString(nameOrdinal);
                        if (!customerReader.IsDBNull(businessOrdinal)) leadCustomerView.Business = customerReader.GetString(businessOrdinal);
                        if (!customerReader.IsDBNull(stateOrdinal)) leadCustomerView.LeadStatus = customerReader.GetString(stateOrdinal);
                        leadCustomerView.LeadCustomerType = LeadCustomerTypes.Customer;

                        if (!customerReader.IsDBNull(stateOrdinal)) leadCustomerView.State = customerReader.GetString(stateOrdinal).Trim();

                        if (!customerReader.IsDBNull(address1Ordinal))
                            leadCustomerView.Address = leadCustomerView.Address1 = customerReader.GetString(address1Ordinal);

                        if (!customerReader.IsDBNull(address2Ordinal))
                        {
                            leadCustomerView.Address2 = customerReader.GetString(address2Ordinal);
                            leadCustomerView.Address += (!string.IsNullOrWhiteSpace(customerReader.GetString(address2Ordinal)) ?
                           ", " + customerReader.GetString(address2Ordinal) : string.Empty);
                        }

                        if (!customerReader.IsDBNull(cityOrdinal)) leadCustomerView.City = customerReader.GetString(cityOrdinal).Trim();
                        if (!customerReader.IsDBNull(emailAddressOrdinal)) leadCustomerView.Email = customerReader.GetString(emailAddressOrdinal);
                        if (!customerReader.IsDBNull(mobileOrdinal)) leadCustomerView.Mobile = customerReader.GetString(mobileOrdinal);
                        if (!customerReader.IsDBNull(postcodeOrdinal)) leadCustomerView.PostalCode = customerReader.GetString(postcodeOrdinal);
                        if (!customerReader.IsDBNull(telephoneOrdinal)) leadCustomerView.Telephone = customerReader.GetString(telephoneOrdinal);
                        if (!customerReader.IsDBNull(custCodeOrdinal)) leadCustomerView.CustomerCode = customerReader.GetString(custCodeOrdinal).Trim();
                        if (!customerReader.IsDBNull(originatorOrdinal)) leadCustomerView.Originator = customerReader.GetString(originatorOrdinal);


                        if (!customerReader.IsDBNull(istlpOrdinal)) leadCustomerView.isTLP = customerReader.GetBoolean(istlpOrdinal);
                        if (!customerReader.IsDBNull(LongitudeIdOrdinal)) leadCustomerView.Longitude = customerReader.GetDouble(LongitudeIdOrdinal);
                        if (!customerReader.IsDBNull(LatitudeIdOrdinal)) leadCustomerView.Latitiude = customerReader.GetDouble(LatitudeIdOrdinal);
                        if (!customerReader.IsDBNull(peripheryOrdinal)) leadCustomerView.Periphery = customerReader.GetString(peripheryOrdinal);
                        if (!customerReader.IsDBNull(outlettypeOrdinal)) leadCustomerView.OutletType = customerReader.GetString(outlettypeOrdinal);
                        if (!customerReader.IsDBNull(categoryOrdinal)) leadCustomerView.Category = customerReader.GetString(categoryOrdinal);
                        if (!customerReader.IsDBNull(MarketOrdinal)) leadCustomerView.MarketName = customerReader.GetString(MarketOrdinal);

                        if (!customerReader.IsDBNull(leadSourceOrdinal)) leadCustomerView.LeadSource = customerReader.GetString(leadSourceOrdinal).Trim();
                        if (!customerReader.IsDBNull(industryOrdinal)) leadCustomerView.Industry = customerReader.GetString(industryOrdinal);
                        if (!customerReader.IsDBNull(referredByOrdinal)) leadCustomerView.ReferredBy = customerReader.GetString(referredByOrdinal);
                        if (!customerReader.IsDBNull(ratingOrdinal)) leadCustomerView.Rating = customerReader.GetInt32(ratingOrdinal);
                        //if (!customerReader.IsDBNull(probabilityOrdinal)) leadCustomerView.pro = customerReader.GetString(probabilityOrdinal);
                        if (!customerReader.IsDBNull(websiteOrdinal)) leadCustomerView.Webiste = customerReader.GetString(websiteOrdinal);
                        if (!customerReader.IsDBNull(noOfEmployeesOrdinal)) leadCustomerView.NoOfEmployees = customerReader.GetInt32(noOfEmployeesOrdinal);
                        if (!customerReader.IsDBNull(annualRevenueOrdinal)) leadCustomerView.AnnualRevenue = customerReader.GetDouble(annualRevenueOrdinal);
                        if (!customerReader.IsDBNull(descriptionOrdinal)) leadCustomerView.Description = customerReader.GetString(descriptionOrdinal);
                        if (!customerReader.IsDBNull(prefContactOrdinal)) leadCustomerView.PreferredContact = customerReader.GetString(prefContactOrdinal);
                        if (!customerReader.IsDBNull(busPotentialOrdinal)) leadCustomerView.BusinessPotential = customerReader.GetString(busPotentialOrdinal);
                        if (!customerReader.IsDBNull(countryOrdinal)) leadCustomerView.Country = customerReader.GetString(countryOrdinal);
                        if (!customerReader.IsDBNull(companyOrdinal)) leadCustomerView.Company = customerReader.GetString(companyOrdinal);

                        if (!customerReader.IsDBNull(industryDescriptionOrdinal)) leadCustomerView.IndustryDescription = customerReader.GetString(industryDescriptionOrdinal);
                        if (!customerReader.IsDBNull(prefContactDescriptionOrdinal)) leadCustomerView.PreferredContactDescription = customerReader.GetString(prefContactDescriptionOrdinal);
                        if (!customerReader.IsDBNull(channelDescriptionOrdinal)) leadCustomerView.ChannelDescription = customerReader.GetString(channelDescriptionOrdinal);
                        leadCustomerView.LeadStage = "Customer";
                        if (!customerReader.IsDBNull(delFlag)) leadCustomerView.IsDeleted = customerReader.GetString(delFlag);
                        if (!customerReader.IsDBNull(secondaryRepOrdinal)) leadCustomerView.SecondaryRepCode = customerReader.GetString(secondaryRepOrdinal);

                        customerReaderCollection.Add(leadCustomerView);
                    }
                }

                return customerReaderCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();
            }
        }

        public List<LeadCustomerViewEntity> GetCustomerCollectionByState(ArgsEntity args)
        {
            string sOriginators = "";
            string customerWhereCls = "";

            DbDataReader customerReader = null;
            try
            {
                args.SStartDate = DateTime.Parse((DateTime.Parse(args.SStartDate)).ToString("dd-MMM-yyyy 00:00:00")).ToUniversalTime().ToString();
                args.SEndDate = DateTime.Parse((DateTime.Parse(args.SEndDate)).ToString("dd-MMM-yyyy 23:59:59")).ToUniversalTime().ToString();


                List<LeadCustomerViewEntity> customerReaderCollection = new List<LeadCustomerViewEntity>();

                //sOriginators = oOriginator.GetChildOriginators(sOriginator);
               
                /*sOriginators = childOriginators;

                if (!string.IsNullOrWhiteSpace(sOriginators))
                {
                    customerWhereCls = !string.IsNullOrWhiteSpace(customerWhereCls) ? customerWhereCls + " AND " + sOriginators : sOriginators;
                }
                customerWhereCls = customerWhereCls.Replace("originator", "modified_by");

                string activityWhereCls = " ";

                if (!string.IsNullOrEmpty(additionalParams))
                {
                    activityWhereCls = "WHERE " + additionalParams;
                }

                if (!string.IsNullOrEmpty(orderBy))
                {
                    activityWhereCls = " " + orderBy;
                }
                */
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                //paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                //paramCollection.Add("@LeadStageId", args.LeadStageId, DbType.Int32);
                paramCollection.Add("@FromDate", args.SStartDate, DbType.String);
                paramCollection.Add("@EndDate", args.SEndDate, DbType.String);

                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                customerReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetCustomersAll"],paramCollection);
                //customerReader = (IngresDataReader)oDataHandle.ExecuteSelect(string.Format(CRMSQLs.GetCustomersAll, sFromDate,
                //    sEndDate, " AND " + customerWhereCls, "b.*", activityWhereCls));

                if (customerReader.HasRows)
                {
                    int nameOrdinal = customerReader.GetOrdinal("name");
                    int businessOrdinal = customerReader.GetOrdinal("business");
                    int stateOrdinal = customerReader.GetOrdinal("state");
                    int custCodeOrdinal = customerReader.GetOrdinal("cust_code");

                    int telephoneOrdinal = customerReader.GetOrdinal("telephone");
                    int faxOrdinal = customerReader.GetOrdinal("fax");
                    int mobileOrdinal = customerReader.GetOrdinal("mobile");
                    int emailAddressOrdinal = customerReader.GetOrdinal("internet_add");
                    int address1Ordinal = customerReader.GetOrdinal("address_1");
                    int address2Ordinal = customerReader.GetOrdinal("address_2");

                    int cityOrdinal = customerReader.GetOrdinal("city");

                    int postcodeOrdinal = customerReader.GetOrdinal("postcode");
                    int originatorOrdinal = customerReader.GetOrdinal("originator");

                    int leadSourceOrdinal = customerReader.GetOrdinal("lead_source");
                    int industryOrdinal = customerReader.GetOrdinal("industry");
                    int referredByOrdinal = customerReader.GetOrdinal("referred_by");
                    int ratingOrdinal = customerReader.GetOrdinal("rating");
                    int probabilityOrdinal = customerReader.GetOrdinal("probability");
                    int noOfEmployeesOrdinal = customerReader.GetOrdinal("no_of_employees");
                    int annualRevenueOrdinal = customerReader.GetOrdinal("annual_revenue");
                    int descriptionOrdinal = customerReader.GetOrdinal("cust_desc");
                    int countryOrdinal = customerReader.GetOrdinal("country");
                    int websiteOrdinal = customerReader.GetOrdinal("website");
                    int prefContactOrdinal = customerReader.GetOrdinal("pref_contact");
                    int busPotentialOrdinal = customerReader.GetOrdinal("bus_potential");
                    int companyOrdinal = customerReader.GetOrdinal("company");

                    int industryDescriptionOrdinal = customerReader.GetOrdinal("industry_description");
                    int prefContactDescriptionOrdinal = customerReader.GetOrdinal("pref_contact_description");
                    int channelDescriptionOrdinal = customerReader.GetOrdinal("channel_description");

                    while (customerReader.Read())
                    {
                        LeadCustomerViewEntity leadCustomerView = LeadCustomerViewEntity.CreateObject();

                        if (!customerReader.IsDBNull(nameOrdinal)) leadCustomerView.Name = customerReader.GetString(nameOrdinal);
                        if (!customerReader.IsDBNull(businessOrdinal)) leadCustomerView.Business = customerReader.GetString(businessOrdinal);
                        if (!customerReader.IsDBNull(stateOrdinal)) leadCustomerView.LeadStatus = customerReader.GetString(stateOrdinal);
                        leadCustomerView.LeadCustomerType = LeadCustomerTypes.Customer;

                        if (!customerReader.IsDBNull(stateOrdinal)) leadCustomerView.State = customerReader.GetString(stateOrdinal).Trim();

                        if (!customerReader.IsDBNull(address1Ordinal))
                            leadCustomerView.Address = customerReader.GetString(address1Ordinal);

                        if (!customerReader.IsDBNull(address2Ordinal))
                            leadCustomerView.Address += (!string.IsNullOrWhiteSpace(customerReader.GetString(address2Ordinal)) ?
                           ", " + customerReader.GetString(address2Ordinal) : string.Empty);

                        if (!customerReader.IsDBNull(cityOrdinal)) leadCustomerView.City = customerReader.GetString(cityOrdinal).Trim();
                        if (!customerReader.IsDBNull(emailAddressOrdinal)) leadCustomerView.Email = customerReader.GetString(emailAddressOrdinal);
                        if (!customerReader.IsDBNull(mobileOrdinal)) leadCustomerView.Mobile = customerReader.GetString(mobileOrdinal);
                        if (!customerReader.IsDBNull(postcodeOrdinal)) leadCustomerView.PostalCode = customerReader.GetString(postcodeOrdinal);
                        if (!customerReader.IsDBNull(telephoneOrdinal)) leadCustomerView.Telephone = customerReader.GetString(telephoneOrdinal);
                        if (!customerReader.IsDBNull(custCodeOrdinal)) leadCustomerView.CustomerCode = customerReader.GetString(custCodeOrdinal).Trim();
                        if (!customerReader.IsDBNull(originatorOrdinal)) leadCustomerView.Originator = customerReader.GetString(originatorOrdinal);

                        if (!customerReader.IsDBNull(leadSourceOrdinal)) leadCustomerView.LeadSource = customerReader.GetString(leadSourceOrdinal).Trim();
                        if (!customerReader.IsDBNull(industryOrdinal)) leadCustomerView.Industry = customerReader.GetString(industryOrdinal);
                        if (!customerReader.IsDBNull(referredByOrdinal)) leadCustomerView.ReferredBy = customerReader.GetString(referredByOrdinal);
                        if (!customerReader.IsDBNull(ratingOrdinal)) leadCustomerView.Rating = customerReader.GetInt32(ratingOrdinal);
                        //if (!customerReader.IsDBNull(probabilityOrdinal)) leadCustomerView.pro = customerReader.GetString(probabilityOrdinal);
                        if (!customerReader.IsDBNull(websiteOrdinal)) leadCustomerView.Webiste = customerReader.GetString(websiteOrdinal);
                        if (!customerReader.IsDBNull(noOfEmployeesOrdinal)) leadCustomerView.NoOfEmployees = customerReader.GetInt32(noOfEmployeesOrdinal);
                        if (!customerReader.IsDBNull(annualRevenueOrdinal)) leadCustomerView.AnnualRevenue = customerReader.GetDouble(annualRevenueOrdinal);
                        if (!customerReader.IsDBNull(descriptionOrdinal)) leadCustomerView.Description = customerReader.GetString(descriptionOrdinal);
                        if (!customerReader.IsDBNull(prefContactOrdinal)) leadCustomerView.PreferredContact = customerReader.GetString(prefContactOrdinal);
                        if (!customerReader.IsDBNull(busPotentialOrdinal)) leadCustomerView.BusinessPotential = customerReader.GetString(busPotentialOrdinal);
                        if (!customerReader.IsDBNull(countryOrdinal)) leadCustomerView.Country = customerReader.GetString(countryOrdinal);
                        if (!customerReader.IsDBNull(companyOrdinal)) leadCustomerView.Company = customerReader.GetString(companyOrdinal);

                        if (!customerReader.IsDBNull(industryDescriptionOrdinal)) leadCustomerView.IndustryDescription = customerReader.GetString(industryDescriptionOrdinal);
                        if (!customerReader.IsDBNull(prefContactDescriptionOrdinal)) leadCustomerView.PreferredContactDescription = customerReader.GetString(prefContactDescriptionOrdinal);
                        if (!customerReader.IsDBNull(channelDescriptionOrdinal)) leadCustomerView.ChannelDescription = customerReader.GetString(channelDescriptionOrdinal);
                        leadCustomerView.LeadStage = "Customer";

                        customerReaderCollection.Add(leadCustomerView);
                    }
                }

                return customerReaderCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();
            }
        }

        public bool InserColumnSetting(ColumnSettingsEntity objColumnSettings)
        {
            bool Inserted = true;
            int iNoRowsSetting = 0;
            int RecordId = 0;

            try
            {
                if (objColumnSettings != null)
                {
                    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                    RecordId = DeleteRecord(objColumnSettings.Originator, objColumnSettings.GridName);

                    if (RecordId > 0)
                    {
                        paramCollection.Add("@IdNo", RecordId, DbType.Int32);
                        Inserted = this.DataAcessService.ExecuteNonQuery(CustomerSql["DeleteOldRecordHeader"], paramCollection)>0;

                        if (Inserted)
                        {
                            paramCollection = new DbInputParameterCollection();
                            paramCollection.Add("@IdNo", RecordId, DbType.Int32);
                            Inserted = this.DataAcessService.ExecuteNonQuery(CustomerSql["DeleteOldRecordDetails"], paramCollection) > 0;
                        }
                        
                    }

                    objColumnSettings.IdNo = Convert.ToInt32(this.DataAcessService.GetOneValue(CustomerSql["GetNextHeaderId"]));
                    if (RecordId == 0)
                        RecordId = objColumnSettings.IdNo;
                    int iNoRows = 0;
                    if (Inserted)
                    {
                        paramCollection = new DbInputParameterCollection();
                        paramCollection.Add("@IdNo", RecordId, DbType.Int32);
                        paramCollection.Add("@Originator", objColumnSettings.Originator, DbType.String);
                        paramCollection.Add("@UserControl", objColumnSettings.UserControl, DbType.String);
                        paramCollection.Add("@GridName", objColumnSettings.GridName, DbType.String);
                        paramCollection.Add("@FilterName", "", DbType.String);
                        iNoRows = this.DataAcessService.ExecuteNonQuery(CustomerSql["InserStructHeader"], paramCollection);
                    }
                    if (iNoRows > 0)
                    {
                        List<ColumnSettingsEntity> lstSettings = objColumnSettings.LstColumnSetting;
                        if (lstSettings.Count > 0)
                        {
                            foreach (ColumnSettingsEntity objSettings in lstSettings)
                            {
                                paramCollection = new DbInputParameterCollection();
                                paramCollection.Add("@IdNo", RecordId, DbType.Int32);
                                paramCollection.Add("@PropertyName", objSettings.PropertyName, DbType.String);
                                paramCollection.Add("@Isvisible", objSettings.Isvisible, DbType.String);
                                paramCollection.Add("@Width", 0, DbType.String);
                                paramCollection.Add("@ColumnOrder", 0, DbType.String);
                                paramCollection.Add("@SortType", "", DbType.String);
                                iNoRowsSetting = this.DataAcessService.ExecuteNonQuery(CustomerSql["InserStructDetail"], paramCollection);
                            }
                        }
                    }

                    if (iNoRowsSetting > 0)
                    {
                        if (objColumnSettings.SortType != null && objColumnSettings.SortName != null)
                        {
                            paramCollection = new DbInputParameterCollection();
                            paramCollection.Add("@PropertyName", objColumnSettings.SortName, DbType.String);
                            paramCollection.Add("@Originator", objColumnSettings.Originator, DbType.String);
                            paramCollection.Add("@SortType", objColumnSettings.SortType, DbType.String);
                            Inserted = this.DataAcessService.ExecuteNonQuery(CustomerSql["UpdateSorting"], paramCollection) > 0;
                        }
                        Inserted = true;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return Inserted;
        }

        public int DeleteRecord(string originator, string gridName)
        {
            DbDataReader idrCustomer = null;
            int RecordId = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", originator, DbType.String);
                paramCollection.Add("@GridName", gridName, DbType.String);

                idrCustomer = this.DataAcessService.ExecuteQuery(CustomerSql["GetIdNo"], paramCollection);
                if (idrCustomer.Read())
                {
                    if (!idrCustomer.IsDBNull(idrCustomer.GetOrdinal("id_no")))
                        RecordId = idrCustomer.GetInt32(idrCustomer.GetOrdinal("id_no"));
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (idrCustomer != null)
                {
                    idrCustomer.Close();
                }
            }
            return RecordId;
        }

        public ColumnSettingsEntity GetColumnSetting(string originator, string gridName)
        {
            ColumnSettingsEntity ColumnSettings = null;
            List<ColumnSettingsEntity> lstColumnSettings = new List<ColumnSettingsEntity>();
            DbDataReader idrCustomer = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", originator, DbType.String);
                paramCollection.Add("@GridName", gridName, DbType.String);

                idrCustomer = this.DataAcessService.ExecuteQuery(CustomerSql["GetStructHeader"], paramCollection);
                if (idrCustomer.HasRows)
                {
                    while (idrCustomer.Read())
                    {
                        ColumnSettings = ColumnSettingsEntity.CreateObject();
                        if (!idrCustomer.IsDBNull(idrCustomer.GetOrdinal("property_name")))
                            ColumnSettings.PropertyName = idrCustomer.GetString(idrCustomer.GetOrdinal("property_name"));
                        if (!idrCustomer.IsDBNull(idrCustomer.GetOrdinal("isvisible")))
                            ColumnSettings.Isvisible = idrCustomer.GetString(idrCustomer.GetOrdinal("isvisible"));
                        lstColumnSettings.Add(ColumnSettings);
                    }
                    ColumnSettings.LstColumnSetting = lstColumnSettings.ToList();
                }
                else
                {
                    ColumnSettings = ColumnSettingsEntity.CreateObject();
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (idrCustomer != null)
                {
                    idrCustomer.Close();
                }
            }
            return ColumnSettings;
        }

        public ColumnSettingsEntity GetSortItems(string originator, string gridName)
        {
            //DataTable objDS = new DataTable();
            //List<CustomerEntity> customerList = new List<CustomerEntity>();

            //try
            //{
            //    using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
            //    {
            //        SqlCommand objCommand = new SqlCommand();
            //        objCommand.Connection = conn;
            //        objCommand.CommandType = CommandType.StoredProcedure;
            //        objCommand.CommandText = "GetSortItem";

            //        objCommand.Parameters.AddWithValue("@AccessToken", accessToken);

            //        SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
            //        objAdap.Fill(objDS);

            //        if (objDS.Rows.Count > 0)
            //        {
            //            foreach (DataRow item in objDS.Rows)
            //            {
            //                CustomerEntity customerObj = CustomerEntity.CreateObject();
            //                if (item["cust_code"] != DBNull.Value) customerObj.CustomerCode = item["cust_code"].ToString();
            //                if (item["name"] != DBNull.Value) customerObj.Name = item["name"].ToString();
            //                if (item["outlet_typeid"] != DBNull.Value) customerObj.OutletTypeId = Convert.ToInt32(item["outlet_typeid"]);
            //                if (item["address_1"] != DBNull.Value) customerObj.Address1 = item["address_1"].ToString();
            //                if (item["telephone"] != DBNull.Value) customerObj.Telephone = item["telephone"].ToString();
            //                if (item["outstanding_balance"] != DBNull.Value) customerObj.OutstandingBalance = Convert.ToDouble(item["outstanding_balance"]);
            //                if (item["credit_limit"] != DBNull.Value) customerObj.CreditLimit = Convert.ToDouble(item["credit_limit"]);
            //                if (item["is_retail"] != DBNull.Value) customerObj.IsRetail = Convert.ToBoolean(item["is_retail"]);
            //                if (item["latitude"] != DBNull.Value) customerObj.Latitiude = Convert.ToDouble(item["latitude"]);
            //                if (item["longitude"] != DBNull.Value) customerObj.Longitude = Convert.ToDouble(item["longitude"]);
            //                if (item["has_unlimited_credit"] != DBNull.Value) customerObj.HasUnlimitedCredit = Convert.ToBoolean(item["has_unlimited_credit"]);

            //                if (item["document_content"] != null)
            //                {
            //                    if (item["document_content"] != DBNull.Value) customerObj.ImageContent = (byte[])item["document_content"];
            //                }

            //                customerList.Add(customerObj);
            //            }
            //        }
            //    }
            //}
            //catch (Exception ex)
            //{
            //    throw;
            //}

            //return customerList;


            DbDataReader idrCustomer = null;
            ColumnSettingsEntity ColumnSettings =  ColumnSettingsEntity.CreateObject();
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Originator", originator, DbType.String);
                paramCollection.Add("@GridName", gridName, DbType.String);

                idrCustomer = this.DataAcessService.ExecuteQuery(CustomerSql["GetSortItem"], paramCollection);
                if (idrCustomer.HasRows)
                {
                    if (idrCustomer.Read())
                    {
                        if (!idrCustomer.IsDBNull(idrCustomer.GetOrdinal("id_no")))
                            ColumnSettings.IdNo = idrCustomer.GetInt32(idrCustomer.GetOrdinal("id_no"));
                        if (!idrCustomer.IsDBNull(idrCustomer.GetOrdinal("property_name")))
                            ColumnSettings.SortName = idrCustomer.GetString(idrCustomer.GetOrdinal("property_name"));
                        if (!idrCustomer.IsDBNull(idrCustomer.GetOrdinal("sort_type")))
                            ColumnSettings.SortType = idrCustomer.GetString(idrCustomer.GetOrdinal("sort_type"));
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (idrCustomer != null)
                {
                    idrCustomer.Close();
                }
            }
            return ColumnSettings;
        }        

        #region - CTC DAO -

        //public bool InsertCustomer(CustomerEntity customerEntity)
        //{
        //    bool successful = false;
        //    int iNoRec = 0;

        //    try
        //    {
        //        DbInputParameterCollection paramCollection = new DbInputParameterCollection();

        //        paramCollection.Add("@CustCode", customerEntity.CustomerCode, DbType.String);
        //        paramCollection.Add("@Name", customerEntity.Name, DbType.String);
        //     //   paramCollection.Add("@RepCode", customerEntity.RepCode, DbType.String);
        //        paramCollection.Add("@Address", customerEntity.Address1, DbType.String);
        //    //    paramCollection.Add("@City", customerEntity.City, DbType.String);
        //        paramCollection.Add("@Telephone", customerEntity.Telephone, DbType.String);
        //       // paramCollection.Add("@Pherohery", customerEntity.Periphery, DbType.String);
        //       // paramCollection.Add("@OutletType", customerEntity.OutletType, DbType.String);
        //        paramCollection.Add("@VolumeId", customerEntity.Volume, DbType.Int32);
        //        paramCollection.Add("@MarketId", customerEntity.MarketId, DbType.Int32);
        //        paramCollection.Add("@PheroheryId", customerEntity.PeripheryId, DbType.Int32);
        //        paramCollection.Add("@OutletTypeId", customerEntity.OutletTypeId, DbType.Int32);
        //        paramCollection.Add("@CategoryId", customerEntity.CategoryId, DbType.Int32);
        //       // paramCollection.Add("@Category", customerEntity.Category, DbType.Int32);
        //        paramCollection.Add("@IsTLP", customerEntity.isTLP, DbType.Boolean);
        //       // paramCollection.Add("@PromotionId", customerEntity.PromotionId, DbType.Int32);+
        //        paramCollection.Add("@Longitude", customerEntity.Longitude, DbType.Double);
        //        paramCollection.Add("@Latitude", customerEntity.Latitiude, DbType.Double);
        //        paramCollection.Add("@Cdr", customerEntity.Cdr, DbType.String);
        //        paramCollection.Add("@Display", customerEntity.Display, DbType.String);
        //        paramCollection.Add("@Remarks", customerEntity.Remarks, DbType.String);
        //        paramCollection.Add("@Rdf", customerEntity.RdfStatus, DbType.String);
        //        paramCollection.Add("@RepCode", customerEntity.RepCode, DbType.String);
        //        paramCollection.Add("@RouteAssignId", customerEntity.RouteAssignId, DbType.Int32);

        //        iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertCustomer"], paramCollection);

        //        if (iNoRec > 0)
        //            successful = true;

        //        return successful;
        //    }
        //    catch (Exception)
        //    {
        //        throw;
        //    }
        //    finally
        //    {

        //    }
        //}

        public int InsertCustomer(CustomerEntity customerEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            int cust_code = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CustCode", customerEntity.CustomerCode, DbType.String);
                paramCollection.Add("@Name", customerEntity.Name, DbType.String);
                //   paramCollection.Add("@RepCode", customerEntity.RepCode, DbType.String);
                paramCollection.Add("@Address", customerEntity.Address1, DbType.String);
                //    paramCollection.Add("@City", customerEntity.City, DbType.String);
                paramCollection.Add("@Telephone", customerEntity.Telephone, DbType.String);
                // paramCollection.Add("@Pherohery", customerEntity.Periphery, DbType.String);
                // paramCollection.Add("@OutletType", customerEntity.OutletType, DbType.String);
                paramCollection.Add("@VolumeId", customerEntity.Volume, DbType.Int32);
                paramCollection.Add("@MarketId", customerEntity.MarketId, DbType.Int32);
                paramCollection.Add("@PheroheryId", customerEntity.PeripheryId, DbType.Int32);
                paramCollection.Add("@OutletTypeId", customerEntity.OutletTypeId, DbType.Int32);
                paramCollection.Add("@CategoryId", customerEntity.CategoryId, DbType.Int32);
                // paramCollection.Add("@Category", customerEntity.Category, DbType.Int32);
                paramCollection.Add("@IsTLP", customerEntity.isTLP, DbType.Boolean);
                // paramCollection.Add("@PromotionId", customerEntity.PromotionId, DbType.Int32);+
                paramCollection.Add("@Longitude", customerEntity.Longitude, DbType.Double);
                paramCollection.Add("@Latitude", customerEntity.Latitiude, DbType.Double);
                paramCollection.Add("@Cdr", customerEntity.Cdr, DbType.String);
                paramCollection.Add("@Display", customerEntity.Display, DbType.String);
                paramCollection.Add("@Remarks", customerEntity.Remarks, DbType.String);
                paramCollection.Add("@Rdf", customerEntity.RdfStatus, DbType.String);
                paramCollection.Add("@RepCode", customerEntity.RepCode, DbType.String);
                paramCollection.Add("@RouteAssignId", customerEntity.RouteAssignId, DbType.Int32);
                paramCollection.Add("@IsRetail", customerEntity.IsRetail, DbType.Boolean);
                paramCollection.Add("@OutletUserCode", customerEntity.OutletUserCode, DbType.String);

                paramCollection.Add("@cust_code", null, DbType.Int32, ParameterDirection.Output);

                cust_code = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertCustomer"], paramCollection, true, "@cust_code");

                if (cust_code > 0)
                    successful = true;

                return cust_code;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateCustomer(CustomerEntity customerEntity)
        {
            bool successful = false;

            int iNoRec = 0;
            
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CustCode", customerEntity.CustomerCode, DbType.String);
                paramCollection.Add("@Name", customerEntity.Name, DbType.String);
                paramCollection.Add("@Address", customerEntity.Address1, DbType.String);
                paramCollection.Add("@Telephone", customerEntity.Telephone, DbType.String);
                paramCollection.Add("@VolumeId", customerEntity.VolumeId, DbType.Int32);
                paramCollection.Add("@MarketId", customerEntity.MarketId, DbType.Int32);
                paramCollection.Add("@PheroheryId", customerEntity.PeripheryId, DbType.Int32);
                paramCollection.Add("@OutletTypeId", customerEntity.OutletTypeId, DbType.Int32);
                paramCollection.Add("@CategoryId", customerEntity.CategoryId, DbType.Int32);
                paramCollection.Add("@IsTLP", customerEntity.isTLP, DbType.Boolean);
                paramCollection.Add("@Longitude", customerEntity.Longitude, DbType.Double);
                paramCollection.Add("@Latitude", customerEntity.Latitiude, DbType.Double);
                paramCollection.Add("@RepCode", customerEntity.RepCode, DbType.String);
                paramCollection.Add("@RouteAssignId", customerEntity.RouteAssignId, DbType.Int32);
                paramCollection.Add("@IsRetail", customerEntity.IsRetail, DbType.Boolean);
                paramCollection.Add("@OutletUserCode", customerEntity.OutletUserCode, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["UpdateCustomer"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public List<TraderCustomerEntity> GetCustomersForTME(string tmeid)
        {
            DbDataReader customerReader = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@tmeid", tmeid , DbType.String);

                List<TraderCustomerEntity> customerList = new List<TraderCustomerEntity>();
                customerReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetCustomersForTME"], paramCollection);

                TraderCustomerEntity customer = null;

                if (customerReader != null && customerReader.HasRows)
                {

                    //int custCodeOrdinal = customerReader.GetOrdinal("cust_code");
                    //int nameOrdinal = customerReader.GetOrdinal("name");
                    //int address1Ordinal = customerReader.GetOrdinal("address_1");
                    //int address2Ordinal = customerReader.GetOrdinal("address_2");
                    //int cityOrdinal = customerReader.GetOrdinal("city");
                    //int telephoneOrdinal = customerReader.GetOrdinal("telephone");

                    //int repCodeOrdinal = customerReader.GetOrdinal("rep_code");
                    //int peripheryOrdinal = customerReader.GetOrdinal("periphery_name");
                    //int outlettypeOrdinal = customerReader.GetOrdinal("outlettype_name");
                    //int promotionOrdinal = customerReader.GetOrdinal("promotion_name");
                    //int marketidOrdinal = customerReader.GetOrdinal("market_id");
                    //int istlpOrdinal = customerReader.GetOrdinal("istlp");

                    //int custCodeOrdinal = customerReader.GetOrdinal("cust_code");
                    //int nameOrdinal = customerReader.GetOrdinal("name");
                    //int volumeIdOrdinal = customerReader.GetOrdinal("volume_id");
                    //int outlettypeIdOrdinal = customerReader.GetOrdinal("outlet_typeid");
                    //int MarketIdOrdinal = customerReader.GetOrdinal("market_id");

                    //int outletOrdinal = customerReader.GetOrdinal("outlettype_name");
                    //int marketOrdinal = customerReader.GetOrdinal("market_name");
                    //int volumeOrdinal = customerReader.GetOrdinal("volume");

                    int custCodeOrdinal = customerReader.GetOrdinal("cust_code");
                    int nameOrdinal = customerReader.GetOrdinal("name");
                    int address1Ordinal = customerReader.GetOrdinal("address_1");
                    int telephoneOrdinal = customerReader.GetOrdinal("telephone");
                    int peripheryIdOrdinal = customerReader.GetOrdinal("periphery_id");
                    int volumeIdOrdinal = customerReader.GetOrdinal("volume_id");
                    int outlettypeIdOrdinal = customerReader.GetOrdinal("outlet_typeid");
                    int categoryIdOrdinal = customerReader.GetOrdinal("category_id");
                    int istlpOrdinal = customerReader.GetOrdinal("istlp");
                    int MarketIdOrdinal = customerReader.GetOrdinal("market_id");
                    int LongitudeIdOrdinal = customerReader.GetOrdinal("longitude");
                    int LatitudeIdOrdinal = customerReader.GetOrdinal("latitude");
                    int ContactPersonNameOrdinal = customerReader.GetOrdinal("contactpersonname");
                   // int peripheryCodeOrdinal = customerReader.GetOrdinal("periphery_code");

                    while (customerReader.Read())
                    {
                        TraderCustomerEntity customerfortme = TraderCustomerEntity.CreateObject();

                        if (!customerReader.IsDBNull(custCodeOrdinal)) customerfortme.CustomerCode = customerReader.GetString(custCodeOrdinal);
                        if (!customerReader.IsDBNull(nameOrdinal)) customerfortme.CustomerName = customerReader.GetString(nameOrdinal);
                        if (!customerReader.IsDBNull(volumeIdOrdinal)) customerfortme.VolumeId = customerReader.GetInt32(volumeIdOrdinal);
                        if (!customerReader.IsDBNull(address1Ordinal)) customerfortme.Address1 = customerReader.GetString(address1Ordinal);
                        if (!customerReader.IsDBNull(telephoneOrdinal)) customerfortme.PhoneNo = customerReader.GetString(telephoneOrdinal);

                        if (!customerReader.IsDBNull(peripheryIdOrdinal)) customerfortme.PheriheryId = customerReader.GetInt32(peripheryIdOrdinal);
                       // if (!customerReader.IsDBNull(peripheryCodeOrdinal)) customerfortme.PheripheryCode = customerReader.GetString(peripheryCodeOrdinal);
                        if (!customerReader.IsDBNull(outlettypeIdOrdinal)) customerfortme.OutletTypeId = customerReader.GetInt32(outlettypeIdOrdinal);
                        if (!customerReader.IsDBNull(categoryIdOrdinal)) customerfortme.CatrgoryId = customerReader.GetInt32(categoryIdOrdinal);
                        if (!customerReader.IsDBNull(MarketIdOrdinal)) customerfortme.MarketId = customerReader.GetInt32(MarketIdOrdinal);
                        if (!customerReader.IsDBNull(istlpOrdinal)) customerfortme.Istlp = customerReader.GetBoolean(istlpOrdinal);

                        if (!customerReader.IsDBNull(LongitudeIdOrdinal)) customerfortme.CustomerLongitude = customerReader.GetDouble(LongitudeIdOrdinal);
                        if (!customerReader.IsDBNull(LatitudeIdOrdinal)) customerfortme.CustomerLatitude = customerReader.GetDouble(LatitudeIdOrdinal);
                        if (!customerReader.IsDBNull(ContactPersonNameOrdinal)) customerfortme.ContactPersonName = customerReader.GetString(ContactPersonNameOrdinal);

                        //if (!customerReader.IsDBNull(custCodeOrdinal)) customerfortme.CustomerCode = customerReader.GetString(custCodeOrdinal);
                        //if (!customerReader.IsDBNull(outlettypeIdOrdinal)) customerfortme.OutletTypeId = customerReader.GetInt32(outlettypeIdOrdinal);
                        //if (!customerReader.IsDBNull(volumeIdOrdinal)) customerfortme.VolumeId = customerReader.GetInt32(volumeIdOrdinal);
                        //if (!customerReader.IsDBNull(MarketIdOrdinal)) customerfortme.MarketId = customerReader.GetInt32(MarketIdOrdinal);
                        //if (!customerReader.IsDBNull(nameOrdinal)) customerfortme.CustomerName = customerReader.GetString(nameOrdinal);
                        //if (!customerReader.IsDBNull(marketOrdinal)) customerfortme.MarketName = customerReader.GetString(marketOrdinal);
                        //if (!customerReader.IsDBNull(volumeOrdinal)) customerfortme.CustomerVolume = customerReader.GetString(volumeOrdinal);

                        //if (!customerReader.IsDBNull(custCodeOrdinal)) customer.CustCode = customerReader.GetString(custCodeOrdinal);
                        //if (!customerReader.IsDBNull(nameOrdinal)) customer.Name = customerReader.GetString(nameOrdinal);
                        //if (!customerReader.IsDBNull(address1Ordinal)) customer.Address1 = customerReader.GetString(address1Ordinal);
                        //if (!customerReader.IsDBNull(address2Ordinal)) customer.Address2 = customerReader.GetString(address2Ordinal);
                        //if (!customerReader.IsDBNull(cityOrdinal)) customer.City = customerReader.GetString(cityOrdinal);
                        //if (!customerReader.IsDBNull(telephoneOrdinal)) customer.Telephone = customerReader.GetString(telephoneOrdinal);

                        //if (!customerReader.IsDBNull(repCodeOrdinal)) customer.RepCode = customerReader.GetString(repCodeOrdinal);
                        //if (!customerReader.IsDBNull(peripheryOrdinal)) customer.Periphery = customerReader.GetString(peripheryOrdinal);
                        //if (!customerReader.IsDBNull(outlettypeOrdinal)) customer.OutletType = customerReader.GetString(outlettypeOrdinal);
                        //if (!customerReader.IsDBNull(promotionOrdinal)) customer.PromotionName = customerReader.GetString(promotionOrdinal);
                        //if (!customerReader.IsDBNull(marketidOrdinal)) customer.MarketId = customerReader.GetInt32(marketidOrdinal);
                        //if (!customerReader.IsDBNull(istlpOrdinal)) customer.OutletType = customerReader.GetString(istlpOrdinal);

                        customerList.Add(customerfortme);
                    }
                }

                return customerList;
            }
            catch
            {
                throw;
            }
            finally 
            {
                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();
            }
        }

        public TraderCustomerEntity GetCustomerForMobileDetails(string custCode)
        {
            DbDataReader customerReader = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", custCode, DbType.String);

                List<TraderCustomerEntity> customerList = new List<TraderCustomerEntity>();
                customerReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetCustomerForMobileDetails"], paramCollection);

              //  TraderCustomerEntity customer = null;
                TraderCustomerEntity customer = TraderCustomerEntity.CreateObject();
                if (customerReader != null && customerReader.HasRows)
                {

                    //int custCodeOrdinal = customerReader.GetOrdinal("cust_code");
                    //int nameOrdinal = customerReader.GetOrdinal("name");
                    //int address1Ordinal = customerReader.GetOrdinal("address_1");
                    //int address2Ordinal = customerReader.GetOrdinal("address_2");
                    //int cityOrdinal = customerReader.GetOrdinal("city");
                    //int telephoneOrdinal = customerReader.GetOrdinal("telephone");
                    //int repCodeOrdinal = customerReader.GetOrdinal("rep_code"); 
                    //int marketidOrdinal = customerReader.GetOrdinal("market_id");
                    //int istlpOrdinal = customerReader.GetOrdinal("istlp");

                    int custCodeOrdinal = customerReader.GetOrdinal("cust_code");
                    int nameOrdinal = customerReader.GetOrdinal("name");
                    int address1Ordinal = customerReader.GetOrdinal("address_1");
                    int telephoneOrdinal = customerReader.GetOrdinal("telephone");
                    int peripheryIdOrdinal = customerReader.GetOrdinal("periphery_id");
                    int volumeIdOrdinal = customerReader.GetOrdinal("volume_id");
                    int outlettypeIdOrdinal = customerReader.GetOrdinal("outlet_typeid");
                    int categoryIdOrdinal = customerReader.GetOrdinal("category_id");
                    int istlpOrdinal = customerReader.GetOrdinal("istlp");
                    int MarketIdOrdinal = customerReader.GetOrdinal("market_id");
                    int LongitudeIdOrdinal = customerReader.GetOrdinal("longitude");
                    int LatitudeIdOrdinal = customerReader.GetOrdinal("latitude");
                    int ContactPersonNameOrdinal = customerReader.GetOrdinal("contactpersonname");

                    int remarksOrdinal = customerReader.GetOrdinal("remarks");
                    int cdrOrdinal = customerReader.GetOrdinal("cdr");
                    int displayOrdinal = customerReader.GetOrdinal("display");
                    int rdfOrdinal = customerReader.GetOrdinal("rdf_status");
                //   int promotionOrdinal = customerReader.GetOrdinal("promotion_name");
                //   int outletOrdinal = customerReader.GetOrdinal("outlettype_name");                                    
                //   int address2Ordinal = customerReader.GetOrdinal("address_2");
                //   int cityOrdinal = customerReader.GetOrdinal("city");                   
                //   int repCodeOrdinal = customerReader.GetOrdinal("rep_code");
                //   int peripheryOrdinal = customerReader.GetOrdinal("periphery_name");


                    while (customerReader.Read())
                    {
                        

                        if (!customerReader.IsDBNull(custCodeOrdinal)) customer.CustomerCode = customerReader.GetString(custCodeOrdinal);
                        if (!customerReader.IsDBNull(nameOrdinal)) customer.CustomerName = customerReader.GetString(nameOrdinal);
                        if (!customerReader.IsDBNull(volumeIdOrdinal)) customer.VolumeId = customerReader.GetInt32(volumeIdOrdinal);
                        if (!customerReader.IsDBNull(address1Ordinal)) customer.Address1 = customerReader.GetString(address1Ordinal);
                        if (!customerReader.IsDBNull(telephoneOrdinal)) customer.PhoneNo = customerReader.GetString(telephoneOrdinal);

                        if (!customerReader.IsDBNull(peripheryIdOrdinal)) customer.PheriheryId = customerReader.GetInt32(peripheryIdOrdinal);
                        if (!customerReader.IsDBNull(outlettypeIdOrdinal)) customer.OutletTypeId = customerReader.GetInt32(outlettypeIdOrdinal);
                        if (!customerReader.IsDBNull(categoryIdOrdinal)) customer.CatrgoryId = customerReader.GetInt32(categoryIdOrdinal);
                        if (!customerReader.IsDBNull(MarketIdOrdinal)) customer.MarketId = customerReader.GetInt32(MarketIdOrdinal);
                        if (!customerReader.IsDBNull(istlpOrdinal)) customer.Istlp = customerReader.GetBoolean(istlpOrdinal);

                        if (!customerReader.IsDBNull(LongitudeIdOrdinal)) customer.CustomerLongitude = customerReader.GetDouble(LongitudeIdOrdinal);
                        if (!customerReader.IsDBNull(LatitudeIdOrdinal)) customer.CustomerLatitude = customerReader.GetDouble(LatitudeIdOrdinal);
                        if (!customerReader.IsDBNull(ContactPersonNameOrdinal)) customer.ContactPersonName = customerReader.GetString(ContactPersonNameOrdinal);

                        if (!customerReader.IsDBNull(remarksOrdinal)) customer.Remarks = customerReader.GetString(remarksOrdinal);
                        if (!customerReader.IsDBNull(cdrOrdinal)) customer.CDR = customerReader.GetString(cdrOrdinal);
                        if (!customerReader.IsDBNull(displayOrdinal)) customer.Display = customerReader.GetString(displayOrdinal);
                        if (!customerReader.IsDBNull(rdfOrdinal)) customer.RDF = customerReader.GetString(rdfOrdinal);

                     //   if (!customerReader.IsDBNull(address2Ordinal)) customer.Address2 = customerReader.GetString(address2Ordinal);
                      //  if (!customerReader.IsDBNull(cityOrdinal)) customer.Town = customerReader.GetString(cityOrdinal);                       
                      //  if (!customerReader.IsDBNull(repCodeOrdinal)) customer.Rep = customerReader.GetString(repCodeOrdinal);
                        //if (!customerReader.IsDBNull(peripheryOrdinal)) customer.Pherihery = customerReader.GetString(peripheryOrdinal);
                        //if (!customerReader.IsDBNull(outletOrdinal)) customer.OutletType = customerReader.GetString(outletOrdinal);
                        //if (!customerReader.IsDBNull(custCodeOrdinal)) customer.CustCode = customerReader.GetString(custCodeOrdinal);
                        //if (!customerReader.IsDBNull(nameOrdinal)) customer.Name = customerReader.GetString(nameOrdinal);
                        //if (!customerReader.IsDBNull(address1Ordinal)) customer.Address1 = customerReader.GetString(address1Ordinal);
                        //if (!customerReader.IsDBNull(address2Ordinal)) customer.Address2 = customerReader.GetString(address2Ordinal);
                        //if (!customerReader.IsDBNull(cityOrdinal)) customer.City = customerReader.GetString(cityOrdinal);
                        //if (!customerReader.IsDBNull(telephoneOrdinal)) customer.Telephone = customerReader.GetString(telephoneOrdinal);
                        //if (!customerReader.IsDBNull(repCodeOrdinal)) customer.RepCode = customerReader.GetString(repCodeOrdinal);
                        //if (!customerReader.IsDBNull(peripheryOrdinal)) customer.Periphery = customerReader.GetString(peripheryOrdinal);
                        //if (!customerReader.IsDBNull(outlettypeOrdinal)) customer.OutletType = customerReader.GetString(outlettypeOrdinal);
                        //if (!customerReader.IsDBNull(promotionOrdinal)) customer.PromotionName = customerReader.GetString(promotionOrdinal);
                        //if (!customerReader.IsDBNull(marketidOrdinal)) customer.MarketId = customerReader.GetInt32(marketidOrdinal);
                        //if (!customerReader.IsDBNull(istlpOrdinal)) customer.OutletType = customerReader.GetString(istlpOrdinal);

                    //    customerList.Add(customer);
                    }
                }

                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();

                List<ProductEntity> productList = new List<ProductEntity>();
                customerReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetCurrentProductsForCustomer"], paramCollection);

                if (customerReader != null && customerReader.HasRows)
                {
                    int idOrdinal = customerReader.GetOrdinal("id");
                    int codeOrdinal = customerReader.GetOrdinal("code");
                    int nameOrdinal = customerReader.GetOrdinal("name");
                    int statusOrdinal = customerReader.GetOrdinal("status");
                    int createdbyOrdinal = customerReader.GetOrdinal("created_by");
                    int createddateOrdinal = customerReader.GetOrdinal("created_date");
                    int lastmodifiedbyOrdinal = customerReader.GetOrdinal("last_modified_by");
                    int lastmodifieddateOrdinal = customerReader.GetOrdinal("last_modified_date");
                    int custCodeOrdinal = customerReader.GetOrdinal("cust_code");
                  //  int totalCountOrdinal = customerReader.GetOrdinal("total_count");

                    while (customerReader.Read())
                    {
                        ProductEntity productEntity = new ProductEntity();

                        if (!customerReader.IsDBNull(idOrdinal)) productEntity.ProductId = customerReader.GetInt32(idOrdinal);
                        if (!customerReader.IsDBNull(codeOrdinal)) productEntity.Code = customerReader.GetString(codeOrdinal);
                        if (!customerReader.IsDBNull(nameOrdinal)) productEntity.Name = customerReader.GetString(nameOrdinal);
                        if (!customerReader.IsDBNull(statusOrdinal)) productEntity.Status = customerReader.GetString(statusOrdinal);
                        if (!customerReader.IsDBNull(createdbyOrdinal)) productEntity.CreatedBy = customerReader.GetString(createdbyOrdinal);
                        if (!customerReader.IsDBNull(createddateOrdinal)) productEntity.CreatedDate = customerReader.GetDateTime(createddateOrdinal);
                        if (!customerReader.IsDBNull(lastmodifiedbyOrdinal)) productEntity.LastModifiedBy = customerReader.GetString(lastmodifiedbyOrdinal);

                        if (!customerReader.IsDBNull(custCodeOrdinal))
                        { productEntity.CustomerCode = customerReader.GetString(custCodeOrdinal); }
                        else
                        { productEntity.CustomerCode = string.Empty; }

                        if (!customerReader.IsDBNull(lastmodifieddateOrdinal)) productEntity.LastModifiedDate = customerReader.GetDateTime(lastmodifieddateOrdinal);

                        productList.Add(productEntity);
                    }
                }

                customer.ProductList = productList;

                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();

                List<MerchandiseEntity> merchandiseList = new List<MerchandiseEntity>();
                customerReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetCurrentMerchandicesForCustomer"], paramCollection);

                if (customerReader != null && customerReader.HasRows)
                {
                    int idOrdinal = customerReader.GetOrdinal("id");
                    int codeOrdinal = customerReader.GetOrdinal("code");
                    int descriptionOrdinal = customerReader.GetOrdinal("description");
                    int statusOrdinal = customerReader.GetOrdinal("status");
                    int createdbyOrdinal = customerReader.GetOrdinal("created_by");
                    int createddateOrdinal = customerReader.GetOrdinal("created_date");
                    int lastmodifiedbyOrdinal = customerReader.GetOrdinal("last_modified_by");
                    int lastmodifieddateOrdinal = customerReader.GetOrdinal("last_modified_date");
                  //  int totalCountOrdinal = customerReader.GetOrdinal("total_count");

                    while (customerReader.Read())
                    {
                        MerchandiseEntity merchandiseEntity = new MerchandiseEntity();

                        if (!customerReader.IsDBNull(idOrdinal)) merchandiseEntity.MerchandiseId = customerReader.GetInt32(idOrdinal);
                        if (!customerReader.IsDBNull(codeOrdinal)) merchandiseEntity.Code = customerReader.GetString(codeOrdinal);
                        if (!customerReader.IsDBNull(descriptionOrdinal)) merchandiseEntity.Description = customerReader.GetString(descriptionOrdinal);
                        if (!customerReader.IsDBNull(statusOrdinal)) merchandiseEntity.Status = customerReader.GetString(statusOrdinal);
                        if (!customerReader.IsDBNull(createdbyOrdinal)) merchandiseEntity.CreatedBy = customerReader.GetString(createdbyOrdinal);
                        if (!customerReader.IsDBNull(createddateOrdinal)) merchandiseEntity.CreatedDate = customerReader.GetDateTime(createddateOrdinal);
                        if (!customerReader.IsDBNull(lastmodifiedbyOrdinal)) merchandiseEntity.LastModifiedBy = customerReader.GetString(lastmodifiedbyOrdinal);
                        if (!customerReader.IsDBNull(lastmodifieddateOrdinal)) merchandiseEntity.LastModifiedDate = customerReader.GetDateTime(lastmodifieddateOrdinal);

                        merchandiseList.Add(merchandiseEntity);
                    }
                }

                customer.MerchadiseList = merchandiseList;

                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();

                List<ObjectiveEntity> objectiveList = new List<ObjectiveEntity>();
                customerReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetCurrentObjectivesForCustomer"], paramCollection);

                if (customerReader != null && customerReader.HasRows)
                {
                   // int idOrdinal = customerReader.GetOrdinal("id");
                    int codeOrdinal = customerReader.GetOrdinal("code");
                    int descriptionOrdinal = customerReader.GetOrdinal("description");
                    int statusOrdinal = customerReader.GetOrdinal("status");
                    int createdbyOrdinal = customerReader.GetOrdinal("created_by");
                    int createddateOrdinal = customerReader.GetOrdinal("created_date");
                    int lastmodifiedbyOrdinal = customerReader.GetOrdinal("last_modified_by");
                    int lastmodifieddateOrdinal = customerReader.GetOrdinal("last_modified_date");
               //     int totalCountOrdinal = customerReader.GetOrdinal("total_count");

                    while (customerReader.Read())
                    {
                        ObjectiveEntity objectiveEntity = new ObjectiveEntity();

                      //  if (!customerReader.IsDBNull(idOrdinal)) objectiveEntity.ObjectiveId = customerReader.GetInt32(idOrdinal);
                        if (!customerReader.IsDBNull(codeOrdinal)) objectiveEntity.Code = customerReader.GetString(codeOrdinal);
                        if (!customerReader.IsDBNull(descriptionOrdinal)) objectiveEntity.Description = customerReader.GetString(descriptionOrdinal);
                        if (!customerReader.IsDBNull(statusOrdinal)) objectiveEntity.Status = customerReader.GetString(statusOrdinal);
                        if (!customerReader.IsDBNull(createdbyOrdinal)) objectiveEntity.CreatedBy = customerReader.GetString(createdbyOrdinal);
                        if (!customerReader.IsDBNull(createddateOrdinal)) objectiveEntity.CreatedDate = customerReader.GetDateTime(createddateOrdinal);
                        if (!customerReader.IsDBNull(lastmodifiedbyOrdinal)) objectiveEntity.LastModifiedBy = customerReader.GetString(lastmodifiedbyOrdinal);
                        if (!customerReader.IsDBNull(lastmodifieddateOrdinal)) objectiveEntity.LastModifiedDate = customerReader.GetDateTime(lastmodifieddateOrdinal);

                        objectiveList.Add(objectiveEntity);
                    }
                }

                customer.ObjectiveList = objectiveList;

                return customer;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();
            }
        }

        public List<PromotionEntity> GetCustomerPromotions(string custCode)
        {
            DbDataReader promotionReader = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", custCode, DbType.String);

                List<PromotionEntity> promotionList = new List<PromotionEntity>();
                promotionReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetCustomerPromotions"], paramCollection);

               // TraderCustomerEntity customer = null;

                if (promotionReader != null && promotionReader.HasRows)
                {

                    int promoCodeOrdinal = promotionReader.GetOrdinal("promo_id");
                    int promonameOrdinal = promotionReader.GetOrdinal("name");


                    while (promotionReader.Read())
                    {
                        PromotionEntity promotions = PromotionEntity.CreateObject();

                        if (!promotionReader.IsDBNull(promoCodeOrdinal)) promotions.PromotionId = promotionReader.GetInt32(promoCodeOrdinal);
                        if (!promotionReader.IsDBNull(promonameOrdinal)) promotions.PromotionName = promotionReader.GetString(promonameOrdinal);


                        promotionList.Add(promotions);
                    }
                }

                return promotionList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (promotionReader != null && (!promotionReader.IsClosed))
                    promotionReader.Close();
            }
        }

        public List<CustomerEntity> GetDailyEffectiveReportData(string fromDate,string todate)
        {
            DbDataReader customerReader = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@fromdate", fromDate, DbType.String);
                paramCollection.Add("@todate", todate, DbType.String);

                List<CustomerEntity> customerList = new List<CustomerEntity>();
                customerReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetDailyEffectiveReportData"], paramCollection);

                if (customerReader != null && customerReader.HasRows)
                {            
                    int custCodeOrdinal = customerReader.GetOrdinal("cust_code");
                    int nameOrdinal = customerReader.GetOrdinal("name");
                    int shortnameOrdinal = customerReader.GetOrdinal("short_name");
                    int address1Ordinal = customerReader.GetOrdinal("address_1");
                    int istlpOrdinal = customerReader.GetOrdinal("istlp");

                    int rdfstatusOrdinal = customerReader.GetOrdinal("rdf_status");
                    int cdrOrdinal = customerReader.GetOrdinal("cdr");
                    int displayOrdinal = customerReader.GetOrdinal("display");
                    int remarksOrdinal = customerReader.GetOrdinal("remarks");


                    while (customerReader.Read())
                    {
                        CustomerEntity customerObj = CustomerEntity.CreateObject();

                        if (!customerReader.IsDBNull(custCodeOrdinal)) customerObj.CustomerCode = customerReader.GetString(custCodeOrdinal);
                        if (!customerReader.IsDBNull(nameOrdinal)) customerObj.Name = customerReader.GetString(nameOrdinal);
                        if (!customerReader.IsDBNull(shortnameOrdinal)) customerObj.ShortName = customerReader.GetString(shortnameOrdinal);
                        if (!customerReader.IsDBNull(address1Ordinal)) customerObj.Address1 = customerReader.GetString(address1Ordinal);
                        if (!customerReader.IsDBNull(istlpOrdinal)) customerObj.isTLP = customerReader.GetBoolean(istlpOrdinal);

                        if (!customerReader.IsDBNull(rdfstatusOrdinal)) customerObj.RdfStatus = customerReader.GetString(rdfstatusOrdinal);
                        if (!customerReader.IsDBNull(cdrOrdinal)) customerObj.Cdr = customerReader.GetString(cdrOrdinal);
                        if (!customerReader.IsDBNull(displayOrdinal)) customerObj.Display = customerReader.GetString(displayOrdinal);
                        if (!customerReader.IsDBNull(remarksOrdinal)) customerObj.Remarks = customerReader.GetString(remarksOrdinal);

                        customerList.Add(customerObj);
                    }
                }

                return customerList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();
            }
        }

        public bool SaveDERData(DailyEffectivenessEntity DEREntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CustCode", DEREntity.CustomerCode, DbType.String);
                paramCollection.Add("@CDR", DEREntity.CDR, DbType.Boolean);
                paramCollection.Add("@Display", DEREntity.Display, DbType.String);
                paramCollection.Add("@Remarks", DEREntity.Remark, DbType.String);
                paramCollection.Add("@RDFStatus", DEREntity.RDF, DbType.String);
                paramCollection.Add("@IsTLP", DEREntity.IsTLP, DbType.Boolean);
                paramCollection.Add("@CreatedDate", DEREntity.CreatedDate, DbType.DateTime);
                paramCollection.Add("@LastModifiedDate", DEREntity.LastModifiedDate, DbType.DateTime);
                paramCollection.Add("@CreatedBy", DEREntity.CreatedBy, DbType.String);
                paramCollection.Add("@LastModifiedBy", DEREntity.LastModifiedBy, DbType.String);
                paramCollection.Add("@Status", DEREntity.Status, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertDailyEffectiveReportData"], paramCollection);
                
                List<ProductEntity> ProductList = new List<ProductEntity>();
                ProductList = DEREntity.ProductList;
                DbInputParameterCollection paramCustCode = new DbInputParameterCollection();
                paramCustCode.Add("@CustCode", DEREntity.CustomerCode, DbType.String);
              //  int count = 0;
                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["UpdateCustomerProductslog"], paramCustCode);

                if (DEREntity.ProductList != null)
                {
                    foreach (ProductEntity product in ProductList)
                    {
                            paramCollection = new DbInputParameterCollection();
                        
                            paramCollection.Add("@CustCode", product.CustomerCode, DbType.String);
                            paramCollection.Add("@ProductId", product.ProductId, DbType.Int32);
                            //paramCollection.Add("@CreatedDate", DEREntity.CreatedDate, DbType.DateTime);
                            //paramCollection.Add("@LastModifiedDate", DEREntity.LastModifiedDate, DbType.DateTime);
                            paramCollection.Add("@CreatedBy", DEREntity.CreatedBy, DbType.String);
                           // paramCollection.Add("@Index", count, DbType.Int32);
                            //paramCollection.Add("@LastModifiedBy", DEREntity.LastModifiedBy, DbType.String);
                            //paramCollection.Add("@Status", DEREntity.Status, DbType.String);
                           // paramCollection.Add("@ProductStatus", product.ProductStatus, DbType.String);

                            iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertCustomerProductslog"], paramCollection);
                            //count++;
                            if (iNoRec <= 0)
                                break; ;
                        }
                    
                }

               
                List<MerchandiseEntity> MerchandisesList = new List<MerchandiseEntity>();
                MerchandisesList = DEREntity.MerchadiseList;
                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["UpdateCustomerMerchandises"], paramCustCode);
                if (DEREntity.MerchadiseList != null)
                {
                 // int count = 0;
                    foreach (MerchandiseEntity merchandise in MerchandisesList)
                    {
                        paramCollection = new DbInputParameterCollection();
                        if (merchandise.CustomerCode != "" && merchandise.MerchandiseId != null)
                        {
                            paramCollection.Add("@CustCode", merchandise.CustomerCode, DbType.String);
                            paramCollection.Add("@MerchandiseId", merchandise.MerchandiseId, DbType.Int32);
                            //       paramCollection.Add("@CreatedDate", DEREntity.CreatedDate, DbType.DateTime);
                            //        paramCollection.Add("@LastModifiedDate", DEREntity.LastModifiedDate, DbType.DateTime);
                            paramCollection.Add("@CreatedBy", DEREntity.CreatedBy, DbType.String);
                            //paramCollection.Add("@Index", count, DbType.Int32);
                            //         paramCollection.Add("@LastModifiedBy", DEREntity.LastModifiedBy, DbType.String);
                            //          paramCollection.Add("@Status", DEREntity.Status, DbType.String);

                            iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertCustomerMerchandises"], paramCollection);
                         //   count++;
                            if (iNoRec <= 0)
                                break; ;
                        }
                    }
                }

                List<ObjectiveEntity> ObjectivesList = new List<ObjectiveEntity>();
                ObjectivesList = DEREntity.ObjectiveList;
                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["UpdateCustomerObjectives"], paramCustCode);
                if (DEREntity.ObjectiveList != null)
                {

                  //  int count = 0;
                    foreach (ObjectiveEntity objective in ObjectivesList)
                    {
                        paramCollection = new DbInputParameterCollection();
                        if (objective.CustomerCode != "" && objective.ObjectiveId != null)
                        {
                            paramCollection.Add("@CustCode", objective.CustomerCode, DbType.String);
                            paramCollection.Add("@ObjectiveId", objective.ObjectiveId, DbType.Int32);
                            //paramCollection.Add("@CreatedDate", DEREntity.CreatedDate, DbType.DateTime);
                            //paramCollection.Add("@LastModifiedDate", DEREntity.LastModifiedDate, DbType.DateTime);
                            paramCollection.Add("@CreatedBy", DEREntity.CreatedBy, DbType.String);
                          //  paramCollection.Add("@Index", count, DbType.Int32);
                            //paramCollection.Add("@LastModifiedBy", DEREntity.LastModifiedBy, DbType.String);
                            //paramCollection.Add("@Status", DEREntity.Status, DbType.String);

                            iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertCustomerObjectives"], paramCollection);
                           // count++;
                            if (iNoRec <= 0)
                                break; ;
                        }
                    }
                }

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }

        }

        public List<ProductEntity> GetCustomerProductStatus(string custCode) {
            DbDataReader productReader = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", custCode, DbType.String);

                List<ProductEntity> ProductListList = new List<ProductEntity>();
                productReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetProductStatusByCustomer"], paramCollection);

                // TraderCustomerEntity customer = null;

                if (productReader != null && productReader.HasRows)
                {
                    int prodIdOrdinal = productReader.GetOrdinal("id");
                    int prodCodeOrdinal = productReader.GetOrdinal("code");
                    int prodNameOrdinal = productReader.GetOrdinal("name");
                    int prodstatusOrdinal = productReader.GetOrdinal("product_status");


                    while (productReader.Read())
                    {
                        ProductEntity product = ProductEntity.CreateObject();

                        if (!productReader.IsDBNull(prodIdOrdinal)) product.ProductId = productReader.GetInt32(prodIdOrdinal);
                        if (!productReader.IsDBNull(prodCodeOrdinal)) product.Code = productReader.GetString(prodCodeOrdinal);
                        if (!productReader.IsDBNull(prodNameOrdinal)) product.Name = productReader.GetString(prodNameOrdinal);
                        if (!productReader.IsDBNull(prodstatusOrdinal)) product.ProductStatus = productReader.GetString(prodstatusOrdinal);
                        ProductListList.Add(product);
                    }
                }

                return ProductListList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (productReader != null && (!productReader.IsClosed))
                    productReader.Close();
            }
        }


        public bool isCustomerExist(CustomerEntity customerEntity)
        {
            DbDataReader drCustomer = null;
            bool isExist = false;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@custCode", customerEntity.CustomerCode, DbType.String);
                //paramCollection.Add("@contactType", contactPersonEntity.ContactType, DbType.String);
                //paramCollection.Add("@origin", contactPersonEntity.Origin, DbType.String);

                drCustomer = this.DataAcessService.ExecuteQuery(CustomerSql["GetCustomerNewDetails"], paramCollection);

                if (drCustomer.HasRows)
                {
                    isExist = true;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drCustomer != null)
                    drCustomer.Close();
            }

            return isExist;
        }

        public bool isPendingCustomerExist(CustomerEntity customerEntity)
        {
            DbDataReader drCustomer = null;
            bool isExist = false;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@custCode", customerEntity.CustCode, DbType.String);

                drCustomer = this.DataAcessService.ExecuteQuery(CustomerSql["GetCustomerNewDetails"], paramCollection);

                if (drCustomer.HasRows)
                {
                    isExist = true;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drCustomer != null)
                    drCustomer.Close();
            }

            return isExist;
        }


        public bool DeleteCustomer(string custCode)
        {

            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CustCode", custCode, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["DeleteCustomer"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool DeleteCustomerFromRoute(string custCode, int routeAssignId)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CustCode", custCode, DbType.String);
                paramCollection.Add("@RouteAssignId", routeAssignId, DbType.Int32);
                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["DeleteCustomerFromRoute"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool DeleteAddCustomerRoute(string custCode)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CustCode", custCode, DbType.String);
                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["DeleteAddCustomerRoute"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool DeduceOutstandingBalance(string custCode, double reducingAmount, string modifiedBy)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@custCode", custCode, DbType.String);
                paramCollection.Add("@reducingAmount", reducingAmount, DbType.Double);
                paramCollection.Add("@modifiedBy", modifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["ReduceOutstandingBalance"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }


        public List<InvoiceHeaderEntity> GetCustomerUnsettledInvoices(string custCode)
        {
            DbDataReader ivceReader = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", custCode, DbType.String);

                List<InvoiceHeaderEntity> invoiceList = new List<InvoiceHeaderEntity>();
                ivceReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetUnsettledInvoicesForCustomer"], paramCollection);

                // TraderCustomerEntity customer = null;

                if (ivceReader != null && ivceReader.HasRows)
                {

                    int ivceIdOrdinal = ivceReader.GetOrdinal("id");
                    int ivceNoOrdinal = ivceReader.GetOrdinal("invoice_no");
                    int dateOrdinal = ivceReader.GetOrdinal("invoice_date");
                    int grandTotalOrdinal = ivceReader.GetOrdinal("grand_total");

                    while (ivceReader.Read())
                    {
                        InvoiceHeaderEntity invoice = InvoiceHeaderEntity.CreateObject();

                        if (!ivceReader.IsDBNull(ivceIdOrdinal)) invoice.IvceId = ivceReader.GetInt32(ivceIdOrdinal);
                        if (!ivceReader.IsDBNull(ivceNoOrdinal)) invoice.IvceNo = ivceReader.GetInt32(ivceNoOrdinal);
                        if (!ivceReader.IsDBNull(dateOrdinal)) invoice.IvceDate = ivceReader.GetDateTime(dateOrdinal).ToString("yyyy-MM-dd HH:mm");
                        if (!ivceReader.IsDBNull(grandTotalOrdinal)) invoice.Grandtotal = ivceReader.GetDouble(grandTotalOrdinal);

                        invoiceList.Add(invoice);
                    }
                }

                return invoiceList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (ivceReader != null && (!ivceReader.IsClosed))
                    ivceReader.Close();
            }
        }

        public List<TraderCustomerEntity> GetCustomersForInvoiceByRoute(int routeId)
        {
            DbDataReader customerReader = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@RouteId", routeId, DbType.Int32);

                List<TraderCustomerEntity> customerList = new List<TraderCustomerEntity>();
                customerReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetCustomersForInvoiceByRoute"], paramCollection);

                TraderCustomerEntity customer = null;

                if (customerReader != null && customerReader.HasRows)
                {
                    int custCodeOrdinal = customerReader.GetOrdinal("cust_code");
                    int nameOrdinal = customerReader.GetOrdinal("name");
                    int ContactPersonNameOrdinal = customerReader.GetOrdinal("first_name");
                    int cOutstandingOrdinal = customerReader.GetOrdinal("outstanding_balance");
                    int cLimitOrdinal = customerReader.GetOrdinal("credit_limit");

                    while (customerReader.Read())
                    {
                        TraderCustomerEntity customerforroute= TraderCustomerEntity.CreateObject();

                        if (!customerReader.IsDBNull(custCodeOrdinal)) customerforroute.CustomerCode = customerReader.GetString(custCodeOrdinal);
                        if (!customerReader.IsDBNull(nameOrdinal)) customerforroute.CustomerName = customerReader.GetString(nameOrdinal);
                        if (!customerReader.IsDBNull(ContactPersonNameOrdinal)) customerforroute.ContactPersonName = customerReader.GetString(ContactPersonNameOrdinal);
                        if (!customerReader.IsDBNull(cOutstandingOrdinal)) customerforroute.OutstandingBalance = customerReader.GetDouble(cOutstandingOrdinal);
                        if (!customerReader.IsDBNull(cLimitOrdinal)) customerforroute.CreditLimit = customerReader.GetDouble(cLimitOrdinal);


                        customerList.Add(customerforroute);
                    }
                }

                return customerList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();
            }
        }

        public TraderCustomerEntity GetCustomerCreditDetails(string customerCode)
        {
            DbDataReader customerReader = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", customerCode, DbType.String);

                customerReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetCustomerCreditDetails"], paramCollection);

                TraderCustomerEntity customer = null;

                if (customerReader.HasRows)
                {
                    int nameOrdinal = customerReader.GetOrdinal("name");
                    int cLimiOrdinal = customerReader.GetOrdinal("credit_limit");
                    int outBalanceOrdinal = customerReader.GetOrdinal("outstanding_balance");
                    int cBalanceOrdinal = customerReader.GetOrdinal("credit_balance");


                    if (customerReader.Read())
                    {
                        customer = TraderCustomerEntity.CreateObject();

                        if (!customerReader.IsDBNull(nameOrdinal)) customer.CustomerName = customerReader.GetString(nameOrdinal);
                        if (!customerReader.IsDBNull(cLimiOrdinal)) customer.CreditLimit = customerReader.GetDouble(cLimiOrdinal);
                        if (!customerReader.IsDBNull(outBalanceOrdinal)) customer.OutstandingBalance = customerReader.GetDouble(outBalanceOrdinal);
                        if (!customerReader.IsDBNull(cBalanceOrdinal)) customer.CashLimit = customerReader.GetDouble(cBalanceOrdinal);

                      
                    }

                }

                return customer;

            }
            catch
            {

                throw;
            }
            finally
            {
                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();
            }
        }

        public List<TraderCustomerEntity> GetCustomersByRoute(int routeId)
        {
            DbDataReader customerReader = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@RouteId", routeId, DbType.Int32);

                List<TraderCustomerEntity> customerList = new List<TraderCustomerEntity>();
                customerReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetCustomersByRoute"], paramCollection);

                TraderCustomerEntity customer = null;

                if (customerReader != null && customerReader.HasRows)
                {
                    int custCodeOrdinal = customerReader.GetOrdinal("cust_code");
                    int nameOrdinal = customerReader.GetOrdinal("name");


                    while (customerReader.Read())
                    {
                        customer = TraderCustomerEntity.CreateObject();

                        if (!customerReader.IsDBNull(custCodeOrdinal)) customer.CustomerCode = customerReader.GetString(custCodeOrdinal);
                        if (!customerReader.IsDBNull(nameOrdinal)) customer.CustomerName = customerReader.GetString(nameOrdinal);

                        customerList.Add(customer);
                    }
                }

                return customerList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();
            }
        }

        public List<SalesForceCustomerEntity> GetAllCoveredOutletsForRoute(int routeId, DateTime startDate, DateTime endDate)
        {
            DbDataReader customerReader = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Route_ID", routeId, DbType.Int32);
                paramCollection.Add("@StartDate", startDate.Date.ToString("yyyy-MM-dd"), DbType.String);
                paramCollection.Add("@EndDate", endDate.Date.ToString("yyyy-MM-dd"), DbType.String);

                List<SalesForceCustomerEntity> customerList = new List<SalesForceCustomerEntity>();
                customerReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetAllCoveredOutletsForRoute"], paramCollection);

                if (customerReader != null && customerReader.HasRows)
                {
                    int custCodeOrdinal = customerReader.GetOrdinal("cust_code");
                    int nameOrdinal = customerReader.GetOrdinal("name");

                    SalesForceCustomerEntity customerObj = null;
                    while (customerReader.Read())
                    {
                        customerObj = SalesForceCustomerEntity.CreateObject();

                        if (!customerReader.IsDBNull(custCodeOrdinal)) customerObj.CustomerCode = customerReader.GetString(custCodeOrdinal);
                        if (!customerReader.IsDBNull(nameOrdinal)) customerObj.Name = customerReader.GetString(nameOrdinal);

                        customerList.Add(customerObj);
                    }
                }
                return customerList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();
            }
        }

        public List<SalesForceCustomerEntity> GetAllOutletsForProgram(int programId)
        {
            DbDataReader customerReader = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@ProgramID", programId, DbType.Int32);

                List<SalesForceCustomerEntity> customerList = new List<SalesForceCustomerEntity>();
                customerReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetAllOutletsForProgram"], paramCollection);

                if (customerReader != null && customerReader.HasRows)
                {
                    int custCodeOrdinal = customerReader.GetOrdinal("cust_code");
                    int nameOrdinal = customerReader.GetOrdinal("name");

                    SalesForceCustomerEntity customerObj = null;
                    while (customerReader.Read())
                    {
                        customerObj = SalesForceCustomerEntity.CreateObject();

                        if (!customerReader.IsDBNull(custCodeOrdinal)) customerObj.CustomerCode = customerReader.GetString(custCodeOrdinal);
                        if (!customerReader.IsDBNull(nameOrdinal)) customerObj.Name = customerReader.GetString(nameOrdinal);

                        customerList.Add(customerObj);
                    }
                }
                return customerList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();
            }
        }

        public List<TargetInfoEntity> GetCustomerMonthleyTargetsByDistributor(string username)
        {
            DbDataReader drCustomer = null;
            List<TargetInfoEntity> customerlist = new List<TargetInfoEntity>();

            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Distributor", username, DbType.String);

                drCustomer = this.DataAcessService.ExecuteQuery(CustomerSql["GetDRActualorDistributor"], paramCollection);

                if (drCustomer != null && drCustomer.HasRows)
                {
                    int custCodeOrdinal = drCustomer.GetOrdinal("cust_code");
                    int nameOrdinal = drCustomer.GetOrdinal("name");

                    int m1Ordinal = drCustomer.GetOrdinal("1");
                    int m2Ordinal = drCustomer.GetOrdinal("2");
                    int m3Ordinal = drCustomer.GetOrdinal("3");
                    int m4Ordinal = drCustomer.GetOrdinal("4");
                    int m5Ordinal = drCustomer.GetOrdinal("5");
                    int m6Ordinal = drCustomer.GetOrdinal("6");
                    int m7Ordinal = drCustomer.GetOrdinal("7");
                    int m8Ordinal = drCustomer.GetOrdinal("8");
                    int m9Ordinal = drCustomer.GetOrdinal("9");
                    int m10Ordinal = drCustomer.GetOrdinal("10");
                    int m11Ordinal = drCustomer.GetOrdinal("11");
                    int m12Ordinal = drCustomer.GetOrdinal("12");

                    int tytdOrdinal = drCustomer.GetOrdinal("tytd");
                    int lytdOrdinal = drCustomer.GetOrdinal("lytd");
                    int year1Ordinal = drCustomer.GetOrdinal("year1");
                    int year2Ordinal = drCustomer.GetOrdinal("year2");
                    

                    while (drCustomer.Read())
                    {
                        TargetInfoEntity infoEntity = new TargetInfoEntity();

                        if (!drCustomer.IsDBNull(custCodeOrdinal)) infoEntity.CustCode = drCustomer.GetString(custCodeOrdinal);
                        if (!drCustomer.IsDBNull(nameOrdinal)) infoEntity.RepName = drCustomer.GetString(nameOrdinal);

                        if (!drCustomer.IsDBNull(m1Ordinal)) infoEntity.TargetMonth1 = Convert.ToInt32(drCustomer.GetDouble(m1Ordinal));
                        if (!drCustomer.IsDBNull(m2Ordinal)) infoEntity.TargetMonth2 = Convert.ToInt32(drCustomer.GetDouble(m2Ordinal));
                        if (!drCustomer.IsDBNull(m3Ordinal)) infoEntity.TargetMonth3 = Convert.ToInt32(drCustomer.GetDouble(m3Ordinal));
                        if (!drCustomer.IsDBNull(m4Ordinal)) infoEntity.TargetMonth4 = Convert.ToInt32(drCustomer.GetDouble(m4Ordinal));
                        if (!drCustomer.IsDBNull(m5Ordinal)) infoEntity.TargetMonth5 = Convert.ToInt32(drCustomer.GetDouble(m5Ordinal));
                        if (!drCustomer.IsDBNull(m6Ordinal)) infoEntity.TargetMonth6 = Convert.ToInt32(drCustomer.GetDouble(m6Ordinal));
                        if (!drCustomer.IsDBNull(m7Ordinal)) infoEntity.TargetMonth7 = Convert.ToInt32(drCustomer.GetDouble(m7Ordinal));
                        if (!drCustomer.IsDBNull(m8Ordinal)) infoEntity.TargetMonth8 = Convert.ToInt32(drCustomer.GetDouble(m8Ordinal));
                        if (!drCustomer.IsDBNull(m9Ordinal)) infoEntity.TargetMonth9 = Convert.ToInt32(drCustomer.GetDouble(m9Ordinal));
                        if (!drCustomer.IsDBNull(m10Ordinal)) infoEntity.TargetMonth10 = Convert.ToInt32(drCustomer.GetDouble(m10Ordinal));
                        if (!drCustomer.IsDBNull(m11Ordinal)) infoEntity.TargetMonth11 = Convert.ToInt32(drCustomer.GetDouble(m11Ordinal));
                        if (!drCustomer.IsDBNull(m12Ordinal)) infoEntity.TargetMonth12 = Convert.ToInt32(drCustomer.GetDouble(m12Ordinal));

                        if (!drCustomer.IsDBNull(tytdOrdinal)) infoEntity.TargetTYTD = Convert.ToInt32(drCustomer.GetInt32(tytdOrdinal));
                        if (!drCustomer.IsDBNull(lytdOrdinal)) infoEntity.TargetLYTD = Convert.ToInt32(drCustomer.GetDouble(lytdOrdinal));
                        if (!drCustomer.IsDBNull(year1Ordinal)) infoEntity.TargetYear1 = Convert.ToInt32(drCustomer.GetDouble(year1Ordinal));
                        if (!drCustomer.IsDBNull(year2Ordinal)) infoEntity.TargetYear2 = Convert.ToInt32(drCustomer.GetDouble(year2Ordinal));

                        customerlist.Add(infoEntity);
                    }
                }

                return customerlist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drCustomer != null)
                    drCustomer.Close();
            }
        }

        public List<CustomerEntity> GetCustomerTargetsByRouteId(int routeMasterId, ArgsEntity args)
        {
            List<CustomerEntity> customerTargets = new List<CustomerEntity>();
            DbDataReader dataReader = null;

            DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            //paramCollection.Add("@EffectiveStartDate", args.DtStartDate.ToString("yyyy-MM-dd"), DbType.String);
            //paramCollection.Add("@EffectiveEndDate", args.DtEndDate.ToString("yyyy-MM-dd"), DbType.String);
            paramCollection.Add("@RouteID", routeMasterId, DbType.Int32);
            paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
            paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
            paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
            paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
            paramCollection.Add("@ShowSQL", 0, DbType.Int32);

            dataReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetCustomerTargetsByRouteId"], paramCollection);

            try
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    int cusCodeOrdinal = dataReader.GetOrdinal("cust_code");
                    int targetIdOrdinal = dataReader.GetOrdinal("id");
                    int targetOrdinal = dataReader.GetOrdinal("stick_target");
                    int nameOrdinal = dataReader.GetOrdinal("Name");
                    int totalCountOrdinal = dataReader.GetOrdinal("total_count");

                    CustomerEntity customerEntity = null;
                    while (dataReader.Read())
                    {
                        customerEntity = CreateObject();

                        if (!dataReader.IsDBNull(cusCodeOrdinal)) customerEntity.CustCode = dataReader.GetString(cusCodeOrdinal);
                        if (!dataReader.IsDBNull(cusCodeOrdinal)) customerEntity.CustomerCode = dataReader.GetString(cusCodeOrdinal);
                        if (!dataReader.IsDBNull(targetIdOrdinal)) customerEntity.TargetId = dataReader.GetInt32(targetIdOrdinal);
                        if (!dataReader.IsDBNull(targetOrdinal)) customerEntity.StickTarget = dataReader.GetInt32(targetOrdinal);
                        if (!dataReader.IsDBNull(nameOrdinal)) customerEntity.Name = dataReader.GetString(nameOrdinal);
                        if (!dataReader.IsDBNull(totalCountOrdinal)) customerEntity.TotalCount = dataReader.GetInt32(totalCountOrdinal);

                        customerTargets.Add(customerEntity);
                    }
                }
                return customerTargets;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public bool InsertCustomerTarget(CustomerEntity customerEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CusCode", customerEntity.CustomerCode, DbType.String);
                paramCollection.Add("@StickTarget", customerEntity.StickTarget, DbType.Int32);
                paramCollection.Add("@EffStartDate", customerEntity.EffStartDate, DbType.DateTime);
                paramCollection.Add("@EffEndDate", customerEntity.EffEndDate, DbType.DateTime);
                paramCollection.Add("@CreatedBy", customerEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertCustomerTarget"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool InsertCustomerCategory(CustomerCategory custCategory)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertCustomerCategory";

                    objCommand.Parameters.AddWithValue("@CategoryName", custCategory.CategoryName);
                    objCommand.Parameters.AddWithValue("@ModifiedBy", custCategory.LastModifiedBy);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool UpdateCustomerCategory(CustomerCategory custCategory)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateCustomerCategory";

                    objCommand.Parameters.AddWithValue("@CategoryId", custCategory.CategoryId);
                    objCommand.Parameters.AddWithValue("@CategoryName", custCategory.CategoryName);
                    objCommand.Parameters.AddWithValue("@ModifiedBy", custCategory.LastModifiedBy);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool DeleteCustomerCategory(CustomerCategory custCategory)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteCustomerCategory";

                    objCommand.Parameters.AddWithValue("@CategoryId", custCategory.CategoryId);
                    objCommand.Parameters.AddWithValue("@ModifiedBy", custCategory.LastModifiedBy);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool UpdateCustomerTarget(CustomerEntity customerEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@TargetID", customerEntity.TargetId, DbType.Int32);
                paramCollection.Add("@StickTarget", customerEntity.StickTarget, DbType.Int32);
                paramCollection.Add("@LastModifiedBy", customerEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["UpdateCustomerTarget"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateCustomerRemarks(CustomerEntity customerEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@cust_code", customerEntity.CustomerCode, DbType.String);
                paramCollection.Add("@remarks", customerEntity.Remarks, DbType.String);
                paramCollection.Add("@last_modified_by", customerEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["UpdateCustomerRemarks"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public CustomerEntity GetCustomerPending(string customerCode)
        {
            DbDataReader customerReader = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", customerCode, DbType.String);

                customerReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetCustomerDetailsForNewEntry"], paramCollection);

                CustomerEntity customer = null;

                if (customerReader.HasRows)
                {
                    int nameOrdinal = customerReader.GetOrdinal("name");
                    int businessOrdinal = customerReader.GetOrdinal("business");
                    int stateOrdinal = customerReader.GetOrdinal("state");
                    int custCodeOrdinal = customerReader.GetOrdinal("cust_code");
                    int telephoneOrdinal = customerReader.GetOrdinal("telephone");
                    int faxOrdinal = customerReader.GetOrdinal("fax");
                    int mobileOrdinal = customerReader.GetOrdinal("mobile");
                    int emailAddressOrdinal = customerReader.GetOrdinal("internet_add");
                    int address1Ordinal = customerReader.GetOrdinal("address_1");
                    int address2Ordinal = customerReader.GetOrdinal("address_2");
                    int cityOrdinal = customerReader.GetOrdinal("city");
                    int postcodeOrdinal = customerReader.GetOrdinal("postcode");
                    int shortNameOrdinal = customerReader.GetOrdinal("short_name");

                    int descriptionOrdinal = customerReader.GetOrdinal("cust_desc");
                    int countryOrdinal = customerReader.GetOrdinal("country");
                    int websiteOrdinal = customerReader.GetOrdinal("website");
                    int prefContactOrdinal = customerReader.GetOrdinal("pref_contact");
                    int companyOrdinal = customerReader.GetOrdinal("company");
                    int lastCalledDateOrdinal = customerReader.GetOrdinal("last_called_date");
                    int litersByOrdinal = customerReader.GetOrdinal("liters_by");
                    int potentialLitersOrdinal = customerReader.GetOrdinal("potential_liters");

                    int ratingOrdinal = customerReader.GetOrdinal("rating");
                    int annualRevenueOrdinal = customerReader.GetOrdinal("annual_revenue");
                    int noofEmployeesOrdinal = customerReader.GetOrdinal("no_of_employees");
                    int businessPotentialOrdinal = customerReader.GetOrdinal("bus_potential");
                    int referredByOrdinal = customerReader.GetOrdinal("referred_by");
                    int sourceOrdinal = customerReader.GetOrdinal("lead_source");
                    int industryOrdinal = customerReader.GetOrdinal("industry");
                    //int assignTo = customerReader.GetOrdinal("potential_liters");
                    int delFlagOrdinal = customerReader.GetOrdinal("del_flag");
                    int creaditStatusOrdinal = customerReader.GetOrdinal("credit_status");
                    int primaryRepNameOrdinal = customerReader.GetOrdinal("PrimaryRep");
                    int secondaryRepNameOrdinal = customerReader.GetOrdinal("SecondaryRep");

                    //int peripheryOrdinal = customerReader.GetOrdinal("periphery_name");
                    //int volumeOrdinal = customerReader.GetOrdinal("volume");
                    //int outlettypeOrdinal = customerReader.GetOrdinal("outlettype_name");
                    //int categoryOrdinal = customerReader.GetOrdinal("category_name");
                    //int MarketOrdinal = customerReader.GetOrdinal("market_name");
                    int peripheryIdOrdinal = customerReader.GetOrdinal("periphery_id");
                    int volumeIdOrdinal = customerReader.GetOrdinal("volume_id");
                    int outlettypeIdOrdinal = customerReader.GetOrdinal("outlet_typeid");
                    int categoryIdOrdinal = customerReader.GetOrdinal("category_id");
                    int MarketIdOrdinal = customerReader.GetOrdinal("market_id");

                    int istlpOrdinal = customerReader.GetOrdinal("istlp");
                    int LongitudeIdOrdinal = customerReader.GetOrdinal("longitude");
                    int LatitudeIdOrdinal = customerReader.GetOrdinal("latitude");
                    int repCodeOrdinal = customerReader.GetOrdinal("rep_code");
                    int routeAssigneIdOrdinal = customerReader.GetOrdinal("route_assign_id");
                    int assignedRouteNameOrdinal = customerReader.GetOrdinal("assigned_route_name");
                    int marketNameOrdinal = customerReader.GetOrdinal("market_name");

                    if (customerReader.Read())
                    {
                        customer = CustomerEntity.CreateObject();

                        if (!customerReader.IsDBNull(nameOrdinal)) customer.Name = customerReader.GetString(nameOrdinal);
                        if (!customerReader.IsDBNull(businessOrdinal)) customer.Business = customerReader.GetString(businessOrdinal).Trim();
                        if (!customerReader.IsDBNull(stateOrdinal)) customer.State = customerReader.GetString(stateOrdinal).Trim();

                        if (!customerReader.IsDBNull(address1Ordinal))
                            customer.Address = customer.Address1 = customerReader.GetString(address1Ordinal);

                        if (!customerReader.IsDBNull(address2Ordinal))
                        {
                            customer.Address2 = customerReader.GetString(address2Ordinal);
                            customer.Address += (!string.IsNullOrWhiteSpace(customerReader.GetString(address2Ordinal)) ?
                           ", " + customerReader.GetString(address2Ordinal) : string.Empty);
                        }

                        if (!customerReader.IsDBNull(cityOrdinal)) customer.City = customerReader.GetString(cityOrdinal);
                        if (!customerReader.IsDBNull(emailAddressOrdinal)) customer.Email = customerReader.GetString(emailAddressOrdinal);
                        if (!customerReader.IsDBNull(mobileOrdinal)) customer.Mobile = customerReader.GetString(mobileOrdinal);
                        if (!customerReader.IsDBNull(postcodeOrdinal)) customer.PostalCode = customerReader.GetString(postcodeOrdinal);
                        if (!customerReader.IsDBNull(telephoneOrdinal)) customer.Telephone = customerReader.GetString(telephoneOrdinal);
                        if (!customerReader.IsDBNull(custCodeOrdinal)) customer.CustomerCode = customerReader.GetString(custCodeOrdinal);
                        if (!customerReader.IsDBNull(shortNameOrdinal)) customer.ShortName = customerReader.GetString(shortNameOrdinal);
                        if (!customerReader.IsDBNull(faxOrdinal)) customer.Fax = customerReader.GetString(faxOrdinal);

                        if (!customerReader.IsDBNull(descriptionOrdinal)) customer.Description = customerReader.GetString(descriptionOrdinal);
                        if (!customerReader.IsDBNull(countryOrdinal)) customer.Country = customerReader.GetString(countryOrdinal);
                        if (!customerReader.IsDBNull(websiteOrdinal)) customer.Website = customerReader.GetString(websiteOrdinal);
                        if (!customerReader.IsDBNull(prefContactOrdinal)) customer.PreferredContact = customerReader.GetString(prefContactOrdinal);
                        if (!customerReader.IsDBNull(companyOrdinal)) customer.Company = customerReader.GetString(companyOrdinal);
                        if (!customerReader.IsDBNull(lastCalledDateOrdinal)) customer.LastCalledDate = customerReader.GetDateTime(lastCalledDateOrdinal);
                        if (!customerReader.IsDBNull(litersByOrdinal)) customer.LitersBy = customerReader.GetString(litersByOrdinal);
                        if (!customerReader.IsDBNull(potentialLitersOrdinal)) customer.PotentialLiters = customerReader.GetDouble(potentialLitersOrdinal);

                        if (!customerReader.IsDBNull(ratingOrdinal)) customer.Rating = customerReader.GetInt32(ratingOrdinal);
                        if (!customerReader.IsDBNull(annualRevenueOrdinal)) customer.AnnualRevenue = customerReader.GetDouble(annualRevenueOrdinal);
                        if (!customerReader.IsDBNull(noofEmployeesOrdinal)) customer.NoOfEmployees = customerReader.GetInt32(noofEmployeesOrdinal);
                        if (!customerReader.IsDBNull(businessPotentialOrdinal)) customer.BusinessPotential = customerReader.GetString(businessPotentialOrdinal);
                        if (!customerReader.IsDBNull(referredByOrdinal)) customer.ReferredBy = customerReader.GetString(referredByOrdinal);
                        if (!customerReader.IsDBNull(sourceOrdinal)) customer.LeadSource = customerReader.GetString(sourceOrdinal);
                        if (!customerReader.IsDBNull(industryOrdinal)) customer.Industry = customerReader.GetString(industryOrdinal);
                        if (!customerReader.IsDBNull(delFlagOrdinal)) customer.Status = customerReader.GetString(delFlagOrdinal);

                        if (!customerReader.IsDBNull(creaditStatusOrdinal)) customer.CreditStatus = customerReader.GetString(creaditStatusOrdinal);
                        if (!customerReader.IsDBNull(primaryRepNameOrdinal)) customer.PrimaryRepName = customerReader.GetString(primaryRepNameOrdinal);
                        if (!customerReader.IsDBNull(secondaryRepNameOrdinal)) customer.SecondaryRepName = customerReader.GetString(secondaryRepNameOrdinal);


                        if (!customerReader.IsDBNull(istlpOrdinal)) customer.isTLP = customerReader.GetBoolean(istlpOrdinal);
                        if (!customerReader.IsDBNull(LongitudeIdOrdinal)) customer.Longitude = customerReader.GetDouble(LongitudeIdOrdinal);
                        if (!customerReader.IsDBNull(LatitudeIdOrdinal)) customer.Latitiude = customerReader.GetDouble(LatitudeIdOrdinal);
                        //if (!customerReader.IsDBNull(peripheryOrdinal)) customer.Periphery = customerReader.GetString(peripheryOrdinal);
                        //if (!customerReader.IsDBNull(outlettypeOrdinal)) customer.OutletType = customerReader.GetString(outlettypeOrdinal);
                        //if (!customerReader.IsDBNull(categoryOrdinal)) customer.Category = customerReader.GetString(categoryOrdinal);
                        //if (!customerReader.IsDBNull(MarketOrdinal)) customer.MarketName = customerReader.GetString(MarketOrdinal);
                        //if (!customerReader.IsDBNull(volumeOrdinal)) customer.Volume = customerReader.GetString(volumeOrdinal);

                        if (!customerReader.IsDBNull(peripheryIdOrdinal)) customer.PeripheryId = customerReader.GetInt32(peripheryIdOrdinal);
                        if (!customerReader.IsDBNull(outlettypeIdOrdinal)) customer.OutletTypeId = customerReader.GetInt32(outlettypeIdOrdinal);
                        if (!customerReader.IsDBNull(categoryIdOrdinal)) customer.CategoryId = customerReader.GetInt32(categoryIdOrdinal);
                        if (!customerReader.IsDBNull(MarketIdOrdinal)) customer.MarketId = customerReader.GetInt32(MarketIdOrdinal);
                        if (!customerReader.IsDBNull(volumeIdOrdinal)) customer.VolumeId = customerReader.GetInt32(volumeIdOrdinal);
                        if (!customerReader.IsDBNull(repCodeOrdinal)) customer.RepCode = customerReader.GetString(repCodeOrdinal);
                        if (!customerReader.IsDBNull(assignedRouteNameOrdinal)) customer.AssignedRouteName = customerReader.GetString(assignedRouteNameOrdinal);
                        if (!customerReader.IsDBNull(routeAssigneIdOrdinal)) customer.RouteAssignId = customerReader.GetInt32(routeAssigneIdOrdinal);
                        if (!customerReader.IsDBNull(marketNameOrdinal)) customer.MarketName = customerReader.GetString(marketNameOrdinal);
                    }

                }

                return customer;

            }
            catch
            {

                throw;
            }
            finally
            {
                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();
            }
        }

        public bool UpdateCustomerNewEntry(CustomerEntity customerEntity)
        {
            bool successful = false;
            bool successCust = false;
            int iNoRec = 0;
            int iCustRec = 0;

            //if (customerEntity.CustomerTypeOfApprove == "pendingcustomer")
            //{
                try
                {
                    DbInputParameterCollection paramCustCode = new DbInputParameterCollection();
                    paramCustCode.Add("@CustCode", customerEntity.CustomerCode, DbType.String);
                    iCustRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["UpdateCustomerChangeLog"], paramCustCode);

                    if (iCustRec > 0)
                        successCust = true;

                    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                    paramCollection.Add("@CustCode", customerEntity.CustomerCode, DbType.String);
                    paramCollection.Add("@Name", customerEntity.Name, DbType.String);
                    //   paramCollection.Add("@RepCode", customerEntity.RepCode, DbType.String);
                    paramCollection.Add("@Address", customerEntity.Address1, DbType.String);
                    //    paramCollection.Add("@City", customerEntity.City, DbType.String);
                    paramCollection.Add("@Telephone", customerEntity.Telephone, DbType.String);
                    // paramCollection.Add("@Pherohery", customerEntity.Periphery, DbType.String);
                    // paramCollection.Add("@OutletType", customerEntity.OutletType, DbType.String);
                    paramCollection.Add("@VolumeId", customerEntity.VolumeId, DbType.Int32);
                    paramCollection.Add("@MarketId", customerEntity.MarketId, DbType.Int32);
                    paramCollection.Add("@PheroheryId", customerEntity.PeripheryId, DbType.Int32);
                    paramCollection.Add("@OutletTypeId", customerEntity.OutletTypeId, DbType.Int32);
                    paramCollection.Add("@CategoryId", customerEntity.CategoryId, DbType.Int32);
                    // paramCollection.Add("@Category", customerEntity.Category, DbType.Int32);
                    paramCollection.Add("@IsTLP", customerEntity.isTLP, DbType.Boolean);
                    // paramCollection.Add("@PromotionId", customerEntity.PromotionId, DbType.Int32);+
                    paramCollection.Add("@Longitude", customerEntity.Longitude, DbType.Double);
                    paramCollection.Add("@Latitude", customerEntity.Latitiude, DbType.Double);
                    paramCollection.Add("@Cdr", customerEntity.Cdr, DbType.String);
                    paramCollection.Add("@Display", customerEntity.Display, DbType.String);
                    paramCollection.Add("@Remarks", customerEntity.Remarks, DbType.String);
                    paramCollection.Add("@Rdf", customerEntity.RdfStatus, DbType.String);
                    paramCollection.Add("@RepCode", customerEntity.RepCode, DbType.String);
                    paramCollection.Add("@RouteAssignId", customerEntity.RouteAssignId, DbType.Int32);

                    iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["UpdateCustomerNewEntry"], paramCollection);

                    if (iNoRec > 0)
                        successful = true;

                    return successful;
                }
                catch (Exception)
                {
                    throw;
                }
                finally
                {

                }
            //}
            //else
            //{
                //try
                //{

                //    //DbInputParameterCollection paramCustCode = new DbInputParameterCollection();
                //    //paramCustCode.Add("@CustCode", customerEntity.CustomerCode, DbType.String);
                //    //paramCustCode.Add("@Name", customerEntity.Name, DbType.String);
                //    //paramCustCode.Add("@Address", customerEntity.Address1, DbType.String);
                //    //paramCustCode.Add("@Telephone", customerEntity.Telephone, DbType.String);
                //    //paramCustCode.Add("@VolumeId", customerEntity.VolumeId, DbType.Int32);
                //    //paramCustCode.Add("@MarketId", customerEntity.MarketId, DbType.Int32);
                //    //paramCustCode.Add("@PheroheryId", customerEntity.PeripheryId, DbType.Int32);
                //    //paramCustCode.Add("@OutletTypeId", customerEntity.OutletTypeId, DbType.Int32);
                //    //paramCustCode.Add("@CategoryId", customerEntity.CategoryId, DbType.Int32);
                //    //paramCustCode.Add("@IsTLP", customerEntity.isTLP, DbType.Boolean);
                //    //paramCustCode.Add("@Longitude", customerEntity.Longitude, DbType.Double);
                //    //paramCustCode.Add("@Latitude", customerEntity.Latitiude, DbType.Double);
                //    //iCustRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertCustomerChangeLog"], paramCustCode);

                //    //if (iCustRec > 0)
                //    //    successCust = true;

                //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                //    paramCollection.Add("@CustCode", customerEntity.CustomerCode, DbType.String);
                //    paramCollection.Add("@Name", customerEntity.Name, DbType.String);
                //    //   paramCollection.Add("@RepCode", customerEntity.RepCode, DbType.String);
                //    paramCollection.Add("@Address", customerEntity.Address1, DbType.String);
                //    //    paramCollection.Add("@City", customerEntity.City, DbType.String);
                //    paramCollection.Add("@Telephone", customerEntity.Telephone, DbType.String);
                //    // paramCollection.Add("@Pherohery", customerEntity.Periphery, DbType.String);
                //    // paramCollection.Add("@OutletType", customerEntity.OutletType, DbType.String);
                //    paramCollection.Add("@VolumeId", customerEntity.VolumeId, DbType.Int32);
                //    paramCollection.Add("@MarketId", customerEntity.MarketId, DbType.Int32);
                //    paramCollection.Add("@PheroheryId", customerEntity.PeripheryId, DbType.Int32);
                //    paramCollection.Add("@OutletTypeId", customerEntity.OutletTypeId, DbType.Int32);
                //    paramCollection.Add("@CategoryId", customerEntity.CategoryId, DbType.Int32);
                //    // paramCollection.Add("@Category", customerEntity.Category, DbType.Int32);
                //    paramCollection.Add("@IsTLP", customerEntity.isTLP, DbType.Boolean);
                //    // paramCollection.Add("@PromotionId", customerEntity.PromotionId, DbType.Int32);+
                //    paramCollection.Add("@Longitude", customerEntity.Longitude, DbType.Double);
                //    paramCollection.Add("@Latitude", customerEntity.Latitiude, DbType.Double);
                //    paramCollection.Add("@RepCode", customerEntity.RepCode, DbType.String);
                //    paramCollection.Add("@RouteAssignId", customerEntity.RouteAssignId, DbType.Int32);

                //    iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["UpdateCustomer"], paramCollection);

                //    if (iNoRec > 0)
                //        successful = true;

                //    return successful;
                //}
                //catch (Exception)
                //{
                //    throw;
                //}
                //finally
                //{

                //}
            //}
        }

        public bool isCustomerExistInNewEntryAndPending(CustomerEntity customerEntity)
        {
            bool isExist = true;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@custCode", customerEntity.CustomerCode, DbType.String);
                //paramCollection.Add("@contactType", contactPersonEntity.ContactType, DbType.String);
                //paramCollection.Add("@origin", contactPersonEntity.Origin, DbType.String);

                CustomerEntity cusEntity = null;
                cusEntity = this.GetCustomerPending(customerEntity.CustomerCode);
                if (cusEntity != null)
                {
                    if (cusEntity.CustomerCode != "" && cusEntity.CustomerCode != null)
                    {
                        isExist = true;
                    }
                    else
                    {
                        isExist = false;
                    }
                }
                else
                {
                    isExist = false;
                }
            }
            catch
            {
                throw;
            }

            return isExist;
        }

        public bool InsertCustomerMaster(CustomerEntity customerEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CustCode", customerEntity.CustomerCode, DbType.String);
                paramCollection.Add("@Name", customerEntity.Name, DbType.String);
                paramCollection.Add("@Address", customerEntity.Address1, DbType.String);
                paramCollection.Add("@Telephone", customerEntity.Telephone, DbType.String);
                paramCollection.Add("@VolumeId", customerEntity.Volume, DbType.Int32);
                paramCollection.Add("@MarketId", customerEntity.MarketId, DbType.Int32);
                paramCollection.Add("@PheroheryId", customerEntity.PeripheryId, DbType.Int32);
                paramCollection.Add("@OutletTypeId", customerEntity.OutletTypeId, DbType.Int32);
                paramCollection.Add("@CategoryId", customerEntity.CategoryId, DbType.Int32);
                paramCollection.Add("@IsTLP", customerEntity.isTLP, DbType.Boolean);
                paramCollection.Add("@Longitude", customerEntity.Longitude, DbType.Double);
                paramCollection.Add("@Latitude", customerEntity.Latitiude, DbType.Double);
                paramCollection.Add("@Cdr", customerEntity.Cdr, DbType.String);
                paramCollection.Add("@Display", customerEntity.Display, DbType.String);
                paramCollection.Add("@Remarks", customerEntity.Remarks, DbType.String);
                paramCollection.Add("@Rdf", customerEntity.RdfStatus, DbType.String);
                paramCollection.Add("@RepCode", customerEntity.RepCode, DbType.String);
                paramCollection.Add("@RouteAssignId", customerEntity.RouteAssignId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertCustomerMaster"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        #region - RCS Form - 

        public bool SaveRCSCustomerDetails(TraderCustomerEntity tCustEntity) { 
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CustCode", tCustEntity.CustomerCode, DbType.String);
                paramCollection.Add("@Name", tCustEntity.CustomerName, DbType.String);
                paramCollection.Add("@Address", tCustEntity.Address1, DbType.String);
                paramCollection.Add("@Telephone", tCustEntity.Telephone, DbType.String);
                paramCollection.Add("@Mobile", tCustEntity.Mobile, DbType.String);
                paramCollection.Add("@Province", tCustEntity.Province, DbType.String);
                paramCollection.Add("@GeoLocation", tCustEntity.GeoLocation, DbType.String);
                paramCollection.Add("@Status", tCustEntity.Status, DbType.String);
                paramCollection.Add("@ProductsHandled", tCustEntity.ProductsHandled, DbType.String);
                paramCollection.Add("@PeripheryCode", tCustEntity.PeripheryCode, DbType.String);
                paramCollection.Add("@CanvassingFrequency", tCustEntity.CanvassingFrequency, DbType.String);
                paramCollection.Add("@Aus30Share", tCustEntity.AsuShare, DbType.String);
                paramCollection.Add("@AuthorityAssesment", tCustEntity.AuthorityAssessment, DbType.String);
            //    paramCollection.Add("@VisitReason", tCustEntity.VisitReason, DbType.String);
           //     paramCollection.Add("@SpentTime", tCustEntity.SpentTime, DbType.String);
                paramCollection.Add("@CreatedBy", tCustEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertRCSCustomerDetails"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }

            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveRCSCoantactDetails(ContactPersonEntity contactPersonEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                if (contactPersonEntity != null)
                {
                    paramCollection.Add("@CustCode", contactPersonEntity.CustCode, DbType.String);
                    paramCollection.Add("@Title", contactPersonEntity.Title, DbType.String);
                    paramCollection.Add("@FirstName", contactPersonEntity.FirstName, DbType.String);
                    paramCollection.Add("@Ethnicity", contactPersonEntity.Ethnicity, DbType.String);
                }

                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertRCSContactDetails"], paramCollection);

                if (contactPersonEntity.Language != null)
                {
                    DbInputParameterCollection paramContactCollection = null;
                    foreach (String language in contactPersonEntity.Language)
                    {
                        paramContactCollection = new DbInputParameterCollection();

                         paramContactCollection.Add("@CustCode", contactPersonEntity.CustCode, DbType.String);
                        paramContactCollection.Add("@LanguageCode", language, DbType.String);
                        paramContactCollection.Add("@CreatedBy", contactPersonEntity.CreatedBy, DbType.String);

                        iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertCustomerLanguages"], paramContactCollection);
                        if (iNoRec <= 0)
                            break; ;
                    }
                }

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }

            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveRCSMerchandiseDetails(List<MerchandiseEntity> merchandiseList)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                if (merchandiseList != null)
                {
                    int count = 0;
                    foreach (MerchandiseEntity merchandise in merchandiseList)
                    {
                        DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                        if (merchandise.CustomerCode != "")
                        {
                            paramCollection.Add("@CustCode", merchandise.CustomerCode, DbType.String);
                            paramCollection.Add("@RCSMerchandiseCode", merchandise.Code, DbType.String);
                            paramCollection.Add("@Description", merchandise.Description, DbType.String);
                            paramCollection.Add("@CreatedBy", merchandise.CreatedBy, DbType.String);

                            iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertRCSMerchandises"], paramCollection);
                            count++;
                            if (iNoRec <= 0)
                                break; ;
                        }
                    }
                }

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }

            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveRCSOutletDetails(List<OutletEntity> outletList)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                if (outletList != null)
                {
                    int count = 0;
                    foreach (OutletEntity outlet in outletList)
                    {
                        DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                        if (outlet.CustomerCode != "")
                        {
                            paramCollection.Add("@CustCode", outlet.CustomerCode, DbType.String);
                            paramCollection.Add("@OutletId", outlet.OutletId, DbType.Int32);
                            paramCollection.Add("@Specify", outlet.Specify, DbType.String);
                            paramCollection.Add("@CreatedBy", outlet.CreatedBy, DbType.String);

                            iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertCustomerOutlets"], paramCollection);
                            count++;
                            if (iNoRec <= 0)
                                break; ;
                        }
                    }
                }

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }

            catch (Exception)
            {
                throw;
            }
        }

        public bool SaveRCSVolumeAssesmentDetails(List<VolumeAssesmentEntity> volumeAssesmentList)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                if (volumeAssesmentList != null)
                {
                    int count = 0;
                    foreach (VolumeAssesmentEntity volume in volumeAssesmentList)
                    {
                        DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                        if (volume.CustCode != "" && volume.ProductId != null)
                        {
                            paramCollection.Add("@CustCode", volume.CustCode, DbType.String);
                            paramCollection.Add("@ProductId", volume.ProductId, DbType.Int32);
                            paramCollection.Add("@Value", volume.Value, DbType.Int32);
                            paramCollection.Add("@CreatedBy", volume.CreatedBy, DbType.String);

                            iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertVolumeAssesments"], paramCollection);
                            count++;
                            if (iNoRec <= 0)
                                break; ;
                        }
                    }
                }

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }

            catch (Exception)
            {
                throw;
            }
        }


        public bool SaveRCSCollectionData(TraderCustomerEntity tCustEntity, ContactPersonEntity contactPersonEntity, List<MerchandiseEntity> merchandiseList, List<OutletEntity> outletList ,PurchaseBehaviourEntity behaviourEntity,List<VolumeAssesmentEntity> volumeAssesmentList)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CustCode", tCustEntity.CustomerCode, DbType.String);
                paramCollection.Add("@Name", tCustEntity.CustomerName, DbType.String);
                paramCollection.Add("@Address", tCustEntity.Address1, DbType.String);
                paramCollection.Add("@Telephone", tCustEntity.Telephone, DbType.String);
                paramCollection.Add("@Mobile", tCustEntity.Mobile, DbType.String);
                paramCollection.Add("@Province", tCustEntity.Province, DbType.String);
                paramCollection.Add("@GeoLocation", tCustEntity.GeoLocation, DbType.String);
                paramCollection.Add("@Status", tCustEntity.Status, DbType.String);
                paramCollection.Add("@ProductsHandled", tCustEntity.ProductsHandled, DbType.String);
                paramCollection.Add("@PeripheryCode", tCustEntity.PeripheryCode, DbType.String);
                paramCollection.Add("@CanvassingFrequency", tCustEntity.CanvassingFrequency, DbType.String);
                paramCollection.Add("@Aus30Share", tCustEntity.AsuShare, DbType.String);
                paramCollection.Add("@AuthorityAssesment", tCustEntity.AuthorityAssessment, DbType.String);
                paramCollection.Add("@VisitReason", tCustEntity.VisitReason, DbType.String);
                paramCollection.Add("@SpentTime", tCustEntity.SpentTime, DbType.String);
                paramCollection.Add("@CreatedBy", tCustEntity.CreatedBy, DbType.String);
                if (contactPersonEntity != null)
                {
                    paramCollection.Add("@Title", contactPersonEntity.Title, DbType.String);
                    paramCollection.Add("@FirstName", contactPersonEntity.FirstName, DbType.String);
                    paramCollection.Add("@Ethnicity", contactPersonEntity.Ethnicity, DbType.String);
                }
                if (behaviourEntity != null)
                {
                    paramCollection.Add("@BehaviourId", behaviourEntity.BehaviourId, DbType.Int32);
                    paramCollection.Add("@BehaviourValue", behaviourEntity.Value, DbType.Int32);
                }

                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertRCSData"], paramCollection);

                DbInputParameterCollection paramCustCode = new DbInputParameterCollection();
                paramCustCode.Add("@CustCode", tCustEntity.CustomerCode, DbType.String);
                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["UpdateCustomerLanguages"], paramCustCode);

                if (contactPersonEntity.Language != null) {
                    DbInputParameterCollection paramContactCollection = null;
                    foreach (String language in contactPersonEntity.Language)
                    {
                        paramContactCollection = new DbInputParameterCollection();

                        paramContactCollection.Add("@CustCode", tCustEntity.CustomerCode, DbType.String);
                        paramContactCollection.Add("@LanguageCode", tCustEntity.CustomerCode, DbType.String);
                        paramContactCollection.Add("@CreatedBy", tCustEntity.CreatedBy, DbType.String);

                        iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertCustomerLanguages"], paramContactCollection);
                        if (iNoRec <= 0)
                            break; ;
                    }
                }

                if (merchandiseList != null)
                {
                    int count = 0;
                    foreach (MerchandiseEntity merchandise in merchandiseList)
                    {
                        paramCollection = new DbInputParameterCollection();
                        if (merchandise.CustomerCode != "" && merchandise.MerchandiseId != null)
                        {
                            paramCollection.Add("@CustCode", merchandise.CustomerCode, DbType.String);
                            paramCollection.Add("@MerchandiseId", merchandise.MerchandiseId, DbType.Int32);
                            paramCollection.Add("@MerchCode", merchandise.Code, DbType.String);
                            

                            iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertCustomerMerchandises"], paramCollection);
                            count++;
                            if (iNoRec <= 0)
                                break; ;
                        }
                    }
                }

                if (outletList != null)
                {
                    int count = 0;
                    foreach (OutletEntity outlet in outletList)
                    {
                        paramCollection = new DbInputParameterCollection();
                        if (outlet.CustomerCode != "")
                        {
                            paramCollection.Add("@CustCode", outlet.CustomerCode, DbType.String);
                            paramCollection.Add("@OutletId", outlet.OutletId, DbType.Int32);
                            paramCollection.Add("@Specify", outlet.Specify, DbType.String);


                            iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertCustomerOutlets"], paramCollection);
                            count++;
                            if (iNoRec <= 0)
                                break; ;
                        }
                    }
                }


                if (volumeAssesmentList != null)
                {
                    int count = 0;
                    foreach (VolumeAssesmentEntity volume in volumeAssesmentList)
                    {
                        paramCollection = new DbInputParameterCollection();
                        if (volume.CustCode != "" && volume.ProductId != null)
                        {
                            paramCollection.Add("@CustCode", volume.CustCode, DbType.String);
                            paramCollection.Add("@ProductId", volume.ProductId, DbType.Int32);
                            paramCollection.Add("@Value", volume.Value, DbType.Int32);
                            paramCollection.Add("@CreatedBy", volume.CreatedBy, DbType.String);

                            iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertVolumeAssesments"], paramCollection);
                            count++;
                            if (iNoRec <= 0)
                                break; ;
                        }
                    }
                }

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }

        }
        
        #endregion

        #endregion

        #region - Report Methods -

        public DailyEffectiveDataSet GetDailyEffectiveReportData(ArgsEntity args)
        {
            DbDataAdapter dapDailyEffective = null;

            try
            {
                //Instantiate report specific data set.
                DailyEffectiveDataSet dsDailyEffective = new DailyEffectiveDataSet();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@fromdate", args.SStartDate, DbType.String);
                paramCollection.Add("@todate", args.SEndDate, DbType.String);

                //Retreive the data adapter to fill the report data
                dapDailyEffective = this.DataAcessService.ExecuteReportQuery(CustomerSql["GetDailyEffectiveReportData"], paramCollection);

                //Fill the report specific data set
                dapDailyEffective.Fill(dsDailyEffective,dsDailyEffective.DailyEffectiveGeneralDetail.TableName);

                return dsDailyEffective;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dapDailyEffective != null)
                    dapDailyEffective.Dispose();
            }
        }

        public bool InsertVolumePerformanceData(VolumePerformanceEntity volumeperformanceEntity) { 
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@No", volumeperformanceEntity.No, DbType.Int32);
                paramCollection.Add("@Region", volumeperformanceEntity.Region, DbType.String);
                paramCollection.Add("@Market", volumeperformanceEntity.Market, DbType.String);
                paramCollection.Add("@Originator", volumeperformanceEntity.Originator, DbType.String);
                paramCollection.Add("@Route", volumeperformanceEntity.Route, DbType.String);
                paramCollection.Add("@CustName", volumeperformanceEntity.CustName, DbType.String);
                paramCollection.Add("@Tier", volumeperformanceEntity.Tier, DbType.Int32);
                paramCollection.Add("@CustCode", volumeperformanceEntity.CustCode, DbType.String);
                paramCollection.Add("@ProductType", volumeperformanceEntity.ProductType, DbType.String);
                paramCollection.Add("@MonthlyTarget", volumeperformanceEntity.MonthleyTarget, DbType.Double);
                paramCollection.Add("@MonthlyActual", volumeperformanceEntity.MonthleyActual, DbType.Double);
                paramCollection.Add("@TLPTarget", volumeperformanceEntity.TLPTarget, DbType.Double);
                paramCollection.Add("@AdditionalTarget", volumeperformanceEntity.AdditionalTarget, DbType.Double);
                paramCollection.Add("@TotalTarget", volumeperformanceEntity.TotalTarget, DbType.Double);
                paramCollection.Add("@TotalActual", volumeperformanceEntity.TotalActual, DbType.Double);
                paramCollection.Add("@TLPAbishekaVarPerc", volumeperformanceEntity.TLPAbishekaVariable, DbType.Int32);
                paramCollection.Add("@TLPIncentive", volumeperformanceEntity.TLPIncentive, DbType.Int32);
                paramCollection.Add("@AbishekaIncentive", volumeperformanceEntity.AbishekaIncentive, DbType.Int32);
                paramCollection.Add("@TotalIncentive", volumeperformanceEntity.TotalIncentive, DbType.Int32);


                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["InsertVolumePerformanceData"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
        }

        #endregion

        public List<KeyValuePair<int, string>> GetAllOutletTypes()
        {
            List<KeyValuePair<int, string>> outletTypes = new List<KeyValuePair<int, string>>();
            DbDataReader idroutletTypes = null;

            DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            idroutletTypes = this.DataAcessService.ExecuteQuery(CustomerSql["GetAllOutletTypes"]);

            try
            {
                if (idroutletTypes != null && idroutletTypes.HasRows)
                {
                    int idOrdinal = idroutletTypes.GetOrdinal("outlettype_id");
                    int nameOrdinal = idroutletTypes.GetOrdinal("outlettype_name");

                    int id = 0;
                    string name = "";
                    while (idroutletTypes.Read())
                    {
                        if (!idroutletTypes.IsDBNull(idOrdinal)) id = idroutletTypes.GetInt32(idOrdinal);
                        if (!idroutletTypes.IsDBNull(nameOrdinal)) name = idroutletTypes.GetString(nameOrdinal);

                        outletTypes.Add(new KeyValuePair<int, string>(id, name));
                    }
                }
                return outletTypes;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idroutletTypes != null)
                    idroutletTypes.Close();
            }
        }

        public List<KeyValuePair<string, string>> GetAllTowns()
        {
            List<KeyValuePair<string, string>> towns = new List<KeyValuePair<string, string>>();
            DbDataReader idroutletTypes = null;

            DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            idroutletTypes = this.DataAcessService.ExecuteQuery(CustomerSql["GetAllTowns"]);

            try
            {
                if (idroutletTypes != null && idroutletTypes.HasRows)
                {
                    int cityOrdinal = idroutletTypes.GetOrdinal("city");
                    string city = "";
                    while (idroutletTypes.Read())
                    {
                        if (!idroutletTypes.IsDBNull(cityOrdinal)) city = idroutletTypes.GetString(cityOrdinal);

                        towns.Add(new KeyValuePair<string, string>(city, city));
                    }
                }
                return towns;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idroutletTypes != null)
                    idroutletTypes.Close();
            }
        }

        //public DataTable ReviewExcelfile(string FilePath, string Extension, string UserName)
        //{
        //    try
        //    {
        //        DataTable dt = new DataTable();

        //        using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(FilePath, false))
        //        {

        //            WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
        //            IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
        //            string relationshipId = sheets.First().Id.Value;
        //            WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
        //            Worksheet workSheet = worksheetPart.Worksheet;
        //            SheetData sheetData = workSheet.GetFirstChild<SheetData>();
        //            IEnumerable<Row> rows = sheetData.Descendants<Row>();

        //            foreach (Cell cell in rows.ElementAt(0))
        //            {
        //                dt.Columns.Add(GetCellValue(spreadSheetDocument, cell).Trim());
        //            }

        //            foreach (Row row in rows) //this will also include your header row...
        //            {
        //                DataRow tempRow = dt.NewRow();
        //                try
        //                {
        //                    for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
        //                    {
        //                        try
        //                        {
        //                            tempRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));
        //                        }
        //                        catch { }
        //                    }

        //                    if (tempRow[0].ToString() != "")
        //                        dt.Rows.Add(tempRow);
        //                }
        //                catch { }
        //            }

        //        }

        //        dt.Rows.RemoveAt(0);
        //        dt.AcceptChanges();

        //        return dt;
        //    }
        //    catch (Exception)
        //    {

        //        throw;
        //    }
        //}


        //public bool UploadExcelfile(string FilePath, string Extension, string UserName, string ASEOriginator, string DistOriginator, string RepOriginator)
        //{
        //    try
        //    {
        //        DataTable dt = new DataTable();

        //        using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(FilePath, false))
        //        {
        //            WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
        //            IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
        //            string relationshipId = sheets.First().Id.Value;
        //            WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
        //            Worksheet workSheet = worksheetPart.Worksheet;
        //            SheetData sheetData = workSheet.GetFirstChild<SheetData>();
        //            IEnumerable<Row> rows = sheetData.Descendants<Row>();

        //            foreach (Cell cell in rows.ElementAt(0))
        //            {
        //                dt.Columns.Add(GetCellValue(spreadSheetDocument, cell).Trim());
        //            }

        //            foreach (Row row in rows) //this will also include your header row...
        //            {
        //                DataRow tempRow = dt.NewRow();
        //                try
        //                {
        //                    //for (int i = 0; i < row.Descendants<Cell>().Count(); i++)
        //                    //{
        //                    //    try
        //                    //    {
        //                    //        tempRow[i] = GetCellValue(spreadSheetDocument, row.Descendants<Cell>().ElementAt(i));
        //                    //    }
        //                    //    catch { }
        //                    //}

        //                    int columnIndex = 0;
        //                    foreach (Cell cell in row.Descendants<Cell>())
        //                    {
        //                        // Gets the column index of the cell with data
        //                        int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
        //                        cellColumnIndex--; //zero based index
        //                        if (columnIndex < cellColumnIndex)
        //                        {
        //                            do
        //                            {
        //                                tempRow[columnIndex] = ""; //Insert blank data here;
        //                                columnIndex++;
        //                            }
        //                            while (columnIndex < cellColumnIndex);
        //                        }
        //                        tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);

        //                        columnIndex++;
        //                    }

        //                    if (tempRow[0].ToString() != "")
        //                        dt.Rows.Add(tempRow);
        //                }
        //                catch { }
        //            }

        //        }

        //        dt.Rows.RemoveAt(0);
        //        dt.AcceptChanges();

        //        //Call Cycle Insert
        //        bool successful = false;
        //        int iNoRec = 0;

        //        DbInputParameterCollection paramCollection = new DbInputParameterCollection();

        //        paramCollection.Add("@ase", ASEOriginator, DbType.String);
        //        paramCollection.Add("@distributor", DistOriginator, DbType.String);
        //        paramCollection.Add("@rep", RepOriginator, DbType.String);

        //        iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["ExcelUpload_CallCycle"], paramCollection);

        //        if (iNoRec > 0)
        //            successful = true;

        //        //Outlet Type Insert
        //        var distinctOutletTypes = dt.AsEnumerable()
        //                            .Select(s => new
        //                            {
        //                                outlet_type_name = s.Field<string>("Outlettype"),
        //                            })
        //                            .Distinct().ToList();

        //        foreach (var str in distinctOutletTypes)
        //        {
        //            if (str.outlet_type_name != null)
        //            {
        //                iNoRec = 0;
        //                paramCollection = new DbInputParameterCollection();
        //                paramCollection.Add("@outlet_type", str.outlet_type_name.ToString(), DbType.String);
        //                this.DataAcessService.ExecuteNonQuery(CustomerSql["ExcelUpload_OutletTypes"], paramCollection);
        //            }
        //        }

        //        //Route Master Insert
        //        var distinctRoutes = dt.AsEnumerable()
        //                           .Select(s => new
        //                           {
        //                               route_name = s.Field<string>("Route"),
        //                           })
        //                           .Distinct().ToList();

        //        foreach (var str in distinctRoutes)
        //        {
        //            if (str.route_name != null)
        //            {
        //                iNoRec = 0;
        //                paramCollection = new DbInputParameterCollection();
        //                paramCollection.Add("@ase", ASEOriginator, DbType.String);
        //                paramCollection.Add("@rep", RepOriginator, DbType.String);
        //                paramCollection.Add("@route_name", str.route_name.ToString(), DbType.String);
        //                this.DataAcessService.ExecuteNonQuery(CustomerSql["ExcelUpload_RouteMaster"], paramCollection);
        //            }
        //        }

        //        this.DataAcessService.ExecuteNonQuery(CustomerSql["ExcelUpload_Customer_TempTable_Truncate"]);

        //        foreach (DataRow dr in dt.Rows)
        //        {
        //            paramCollection = new DbInputParameterCollection();
        //            paramCollection.Add("@no", dr["No"].ToString(), DbType.Int32);
        //            paramCollection.Add("@name", dr["Name"].ToString(), DbType.String);
        //            paramCollection.Add("@address", dr["Address"].ToString(), DbType.String);
        //            paramCollection.Add("@phone", dr["phone"].ToString(), DbType.String);
        //            paramCollection.Add("@outlettype", dr["outlettype"].ToString(), DbType.String);
        //            paramCollection.Add("@area", dr["Route"].ToString(), DbType.String);
        //            this.DataAcessService.ExecuteNonQuery(CustomerSql["ExcelUpload_Customer_TempTable"], paramCollection);
        //        }

        //        paramCollection = new DbInputParameterCollection();
        //        paramCollection.Add("@rep", RepOriginator, DbType.String);
        //        this.DataAcessService.ExecuteNonQuery(CustomerSql["ExcelUpload_Customer_Cursor"], paramCollection);

        //        return true;
        //    }
        //    catch (Exception ex)
        //    {

        //        throw;
        //    }
        //}

        public bool UploadExcelfile(string FilePath, string Extension, string UserName, string TerritoryId)
        {
            try
            {
                DataTable dt = new DataTable();

                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(FilePath, false))
                {
                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;
                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    IEnumerable<Row> rows = sheetData.Descendants<Row>();

                    foreach (Cell cell in rows.ElementAt(0))
                    {
                        dt.Columns.Add(GetCellValue(spreadSheetDocument, cell).Trim());
                    }

                    foreach (Row row in rows)
                    {
                        DataRow tempRow = dt.NewRow();
                        try
                        {
                            int columnIndex = 0;
                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
                                cellColumnIndex--;
                                if (columnIndex < cellColumnIndex)
                                {
                                    do
                                    {
                                        tempRow[columnIndex] = "";
                                        columnIndex++;
                                    }
                                    while (columnIndex < cellColumnIndex);
                                }
                                tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);

                                columnIndex++;
                            }

                            if (tempRow[0].ToString() != "")
                                dt.Rows.Add(tempRow);
                        }
                        catch { }
                    }
                }

                dt.Rows.RemoveAt(0);
                dt.AcceptChanges();
                
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                if (dt.Rows.Count > 0)
                {
                    #region Call Cycle Insert
                    //paramCollection.Add("@ase", ASEOriginator, DbType.String);
                    //paramCollection.Add("@distributor", DistOriginator, DbType.String);
                    //paramCollection.Add("@rep", RepOriginator, DbType.String);

                    //iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["ExcelUpload_CallCycle"], paramCollection);

                    //if (iNoRec > 0)
                    //    successful = true;
                    #endregion

                    //Outlet Type Insert
                    var distinctOutletTypes = dt.AsEnumerable()
                                        .Select(s => new
                                        {
                                            outlet_type_name = s.Field<string>("Outlettype"),
                                        })
                                        .Distinct().ToList();

                    foreach (var str in distinctOutletTypes)
                    {
                        if (str.outlet_type_name != null)
                        {
                            paramCollection = new DbInputParameterCollection();
                            paramCollection.Add("@outlet_type", str.outlet_type_name.ToString(), DbType.String);
                            this.DataAcessService.ExecuteNonQuery(CustomerSql["ExcelUpload_OutletTypes"], paramCollection);
                        }
                    }
                    
                    //Outlet MT Category Insert
                    var distinctMTCategory = dt.AsEnumerable()
                                        .Select(s => new
                                        {
                                            outlet_mt_category = s.Field<string>("OutletMTCategory"),
                                        })
                                        .Distinct().ToList();

                    foreach (var str in distinctMTCategory)
                    {
                        if (str.outlet_mt_category != null)
                        {
                            paramCollection = new DbInputParameterCollection();
                            paramCollection.Add("@outlet_mt_category", str.outlet_mt_category.ToString(), DbType.String);
                            this.DataAcessService.ExecuteNonQuery(CustomerSql["ExcelUpload_OutletMTCategory"], paramCollection);
                        }
                    }

                    //Route Master Insert
                    var distinctRoutes = dt.AsEnumerable()
                                       .Select(s => new
                                       {
                                           route_name = s.Field<string>("Route"),
                                       })
                                       .Distinct().ToList();

                    foreach (var str in distinctRoutes)
                    {
                        if (str.route_name != null)
                        {
                            paramCollection = new DbInputParameterCollection();
                            paramCollection.Add("@originator", UserName, DbType.String);
                            paramCollection.Add("@territory_id", TerritoryId, DbType.Int32);
                            paramCollection.Add("@route_name", str.route_name.ToString(), DbType.String);
                            this.DataAcessService.ExecuteNonQuery(CustomerSql["ExcelUpload_RouteMaster"], paramCollection);
                        }
                    }

                    this.DataAcessService.ExecuteNonQuery(CustomerSql["ExcelUpload_Customer_TempTable_Truncate"]);

                    foreach (DataRow dr in dt.Rows)
                    {
                        paramCollection = new DbInputParameterCollection();
                        paramCollection.Add("@no", dr["No"].ToString(), DbType.Int32);
                        paramCollection.Add("@name", dr["Name"].ToString(), DbType.String);
                        paramCollection.Add("@address", dr["Address"].ToString(), DbType.String);
                        paramCollection.Add("@phone", dr["phone"].ToString(), DbType.String);
                        paramCollection.Add("@outlettype", dr["outlettype"].ToString(), DbType.String);
                        paramCollection.Add("@area", dr["Route"].ToString(), DbType.String);
                        try
                        {
                            paramCollection.Add("@mtCode", dr["MTCode"].ToString(), DbType.String);
                        }
                        catch { }
                        try
                        {
                            paramCollection.Add("@outletMtCode", dr["OutletMTCategory"].ToString(), DbType.String);
                        }
                        catch { }
                        this.DataAcessService.ExecuteNonQuery(CustomerSql["ExcelUpload_Customer_TempTable"], paramCollection);
                    }

                    paramCollection = new DbInputParameterCollection();
                    paramCollection.Add("@originator", UserName, DbType.String);
                    paramCollection.Add("@territory_id", TerritoryId, DbType.Int32);
                    this.DataAcessService.ExecuteNonQuery(CustomerSql["ExcelUpload_Customer_Cursor"], paramCollection);

                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static string GetColumnName(string cellReference)
        {
            // Create a regular expression to match the column name portion of the cell name.
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellReference);
            return match.Value;
        }

        public static int? GetColumnIndexFromName(string columnName)
        {

            //return columnIndex;
            string name = columnName;
            int number = 0;
            int pow = 1;
            for (int i = name.Length - 1; i >= 0; i--)
            {
                number += (name[i] - 'A' + 1) * pow;
                pow *= 26;
            }
            return number;
        }

        //--using openXml
        public static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            string value = cell.CellValue.InnerXml;
            if (cell.CellValue == null)
            {
                return "";
            }
            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }
        }

        public List<CustomerEntity> GetRouteCustomerByAccessToken(string accessToken)
        {
            //DbDataReader customerReader = null;
            //try
            //{
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@AccessToken", accessToken, DbType.String);

            //    List<CustomerEntity> customerList = new List<CustomerEntity>();
            //    customerReader = this.DataAcessService.ExecuteQuery(CustomerSql["GetRouteCustomerByAccessToken"], paramCollection);

            //    if (customerReader != null && customerReader.HasRows)
            //    {
            //        int custCodeOrdinal = customerReader.GetOrdinal("cust_code");
            //        int nameOrdinal = customerReader.GetOrdinal("name");
            //        int outletTypeIdOrdinal = customerReader.GetOrdinal("outlet_typeid");
            //        int address1Ordinal = customerReader.GetOrdinal("address_1");
            //        int telephoneOrdinal = customerReader.GetOrdinal("telephone");
            //        int outstandingBalanceOrdinal = customerReader.GetOrdinal("outstanding_balance");
            //        int creditLimitOrdinal = customerReader.GetOrdinal("credit_limit");
            //        int isRetailOrdinal = customerReader.GetOrdinal("is_retail");
            //        int document_contentOrdinal = customerReader.GetOrdinal("document_content");
            //        int has_unlimited_creditOrdinal = customerReader.GetOrdinal("has_unlimited_credit");
            //        int latitudeOrdinal = customerReader.GetOrdinal("latitude");
            //        int longitudeOrdinal = customerReader.GetOrdinal("longitude");

            //        while (customerReader.Read())
            //        {
            //            CustomerEntity customerObj = CustomerEntity.CreateObject();

            //            if (!customerReader.IsDBNull(custCodeOrdinal)) customerObj.CustomerCode = customerReader.GetString(custCodeOrdinal);
            //            if (!customerReader.IsDBNull(nameOrdinal)) customerObj.Name = customerReader.GetString(nameOrdinal);
            //            if (!customerReader.IsDBNull(outletTypeIdOrdinal)) customerObj.OutletTypeId = customerReader.GetInt32(outletTypeIdOrdinal);
            //            if (!customerReader.IsDBNull(address1Ordinal)) customerObj.Address1 = customerReader.GetString(address1Ordinal);
            //            if (!customerReader.IsDBNull(telephoneOrdinal)) customerObj.Telephone = customerReader.GetString(telephoneOrdinal);

            //            if (!customerReader.IsDBNull(outstandingBalanceOrdinal)) customerObj.OutstandingBalance = customerReader.GetDouble(outstandingBalanceOrdinal);
            //            if (!customerReader.IsDBNull(creditLimitOrdinal)) customerObj.CreditLimit = customerReader.GetDouble(creditLimitOrdinal);
            //            if (!customerReader.IsDBNull(isRetailOrdinal)) customerObj.IsRetail = customerReader.GetBoolean(isRetailOrdinal);

            //            if (!customerReader.IsDBNull(latitudeOrdinal)) customerObj.Latitiude = customerReader.GetDouble(latitudeOrdinal);
            //            if (!customerReader.IsDBNull(longitudeOrdinal)) customerObj.Longitude = customerReader.GetDouble(longitudeOrdinal);

            //            if (!customerReader.IsDBNull(has_unlimited_creditOrdinal)) customerObj.HasUnlimitedCredit = customerReader.GetBoolean(has_unlimited_creditOrdinal);

            //            if (customerReader[document_contentOrdinal] != null)
            //            {
            //                if (!customerReader.IsDBNull(document_contentOrdinal)) customerObj.ImageContent = (byte[])customerReader[document_contentOrdinal];
            //            }
                        
            //            customerList.Add(customerObj);
            //        }
            //    }

            //    return customerList;
            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (customerReader != null && (!customerReader.IsClosed))
            //        customerReader.Close();
            //}

            //*************************Add By Irosh Fernando 2018/10/3************************************

            DataTable objDS = new DataTable();
            List<CustomerEntity> customerList = new List<CustomerEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetRouteCustomerByAccessToken";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            CustomerEntity customerObj = CustomerEntity.CreateObject();
                            if (item["cust_code"] != DBNull.Value) customerObj.CustomerCode = item["cust_code"].ToString();
                            if (item["name"] != DBNull.Value) customerObj.Name = item["name"].ToString();
                            if (item["outlet_typeid"] != DBNull.Value) customerObj.OutletTypeId = Convert.ToInt32(item["outlet_typeid"]);
                            if (item["address_1"] != DBNull.Value) customerObj.Address1 = item["address_1"].ToString();
                            if (item["telephone"] != DBNull.Value) customerObj.Telephone = item["telephone"].ToString();
                            if (item["outstanding_balance"] != DBNull.Value) customerObj.OutstandingBalance = Convert.ToDouble(item["outstanding_balance"]);
                            if (item["credit_limit"] != DBNull.Value) customerObj.CreditLimit = Convert.ToDouble(item["credit_limit"]);
                            if (item["is_retail"] != DBNull.Value) customerObj.IsRetail = Convert.ToBoolean(item["is_retail"]);
                            if (item["latitude"] != DBNull.Value) customerObj.Latitiude = Convert.ToDouble(item["latitude"]);
                            if (item["longitude"] != DBNull.Value) customerObj.Longitude = Convert.ToDouble(item["longitude"]);
                            if (item["has_unlimited_credit"] != DBNull.Value) customerObj.HasUnlimitedCredit = Convert.ToBoolean(item["has_unlimited_credit"]);
                            if (item["cust_type"] != DBNull.Value) customerObj.custType = Convert.ToInt32(item["cust_type"]);
                            if (item["day_order"] != DBNull.Value) customerObj.dayOrder = Convert.ToInt32(item["day_order"]);

                            if (item["document_content"] != null)
                            {
                                if (item["document_content"] != DBNull.Value) customerObj.ImageContent = (byte[])item["document_content"];
                            }

                            customerList.Add(customerObj);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return customerList;

            //*************************Add By Irosh Fernando 2018/10/3************************************
        }

        public string InsertCustomerByMob(string accessToken, CustomerEntity customerEntity)
        {
            //int iNoRec = 0;
            //DbDataReader customerCodeReader = null;

            //try
            //{
            //    //string CustomerCode = this.DataAcessService.GetOneValue(CustomerSql["GetNextCustCode"]).ToString();
            //    string CustomerCode = "";

            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            //    paramCollection.Add("@CustCode", CustomerCode, DbType.String);
            //    paramCollection.Add("@CustCodeTmp", customerEntity.CustomerCode, DbType.String);
            //    paramCollection.Add("@Name", customerEntity.Name, DbType.String);
            //    paramCollection.Add("@Address", customerEntity.Address1, DbType.String);
            //    paramCollection.Add("@Telephone", customerEntity.Telephone, DbType.String);
            //    paramCollection.Add("@OutletTypeId", customerEntity.OutletTypeId, DbType.Int32);
            //    paramCollection.Add("@RouteId", customerEntity.RouteAssignId, DbType.Int32);
            //    paramCollection.Add("@IsRetail", customerEntity.IsRetail, DbType.Int32);
            //    paramCollection.Add("@RepCode", customerEntity.RepCode, DbType.String);
            //    paramCollection.Add("@Longitude", customerEntity.Longitude, DbType.Double);
            //    paramCollection.Add("@Latitude", customerEntity.Latitiude, DbType.Double);
            //    paramCollection.Add("@CreatedOn", customerEntity.CreatedOn, DbType.DateTime);
            //    //Add By Irosh 
            //    paramCollection.Add("@has_unlimited_credit", customerEntity.HasUnlimitedCredit, DbType.Boolean);
            //    paramCollection.Add("@credit_limit", customerEntity.CreditLimit, DbType.Double);
            //    paramCollection.Add("@ImageContent", customerEntity.ImageContent, DbType.Binary);

            //    //iNoRec = this.DataAcessService.ExecuteQuery(CustomerSql["InsertCustomerByRep"], paramCollection);
            //    customerCodeReader = this.DataAcessService.ExecuteQuery(CustomerSql["InsertCustomerByRep"], paramCollection);

            //    if (customerCodeReader != null && customerCodeReader.HasRows)
            //    {
            //        int custCodeOrdinal = customerCodeReader.GetOrdinal("new_cust_code");
            //        while (customerCodeReader.Read())
            //        {
            //            CustomerEntity customerObj = CustomerEntity.CreateObject();

            //            if (!customerCodeReader.IsDBNull(custCodeOrdinal)) CustomerCode = customerCodeReader.GetString(custCodeOrdinal);
            //        }
            //    }
            //    //if (iNoRec == 0)
            //    //    CustomerCode = "";

            //    return CustomerCode;
            //}
            //catch (Exception)
            //{
            //    throw;
            //}
            //finally
            //{

            //}

            string CustomerCode = "";

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;

                    //objCommand.Transaction = trans;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertCustomerByRep";

                    objCommand.Parameters.AddWithValue("@CustCode", CustomerCode);
                    objCommand.Parameters.AddWithValue("@CustCodeTmp", customerEntity.CustomerCode);
                    objCommand.Parameters.AddWithValue("@Name", customerEntity.Name);
                    objCommand.Parameters.AddWithValue("@Address", customerEntity.Address1);
                    objCommand.Parameters.AddWithValue("@Telephone", customerEntity.Telephone);
                    objCommand.Parameters.AddWithValue("@OutletTypeId", customerEntity.OutletTypeId);
                    objCommand.Parameters.AddWithValue("@RouteId", customerEntity.RouteAssignId);
                    objCommand.Parameters.AddWithValue("@IsRetail", customerEntity.IsRetail);
                    objCommand.Parameters.AddWithValue("@RepCode", customerEntity.RepCode);
                    objCommand.Parameters.AddWithValue("@Longitude", customerEntity.Longitude);
                    objCommand.Parameters.AddWithValue("@Latitude", customerEntity.Latitiude);
                    objCommand.Parameters.AddWithValue("@CreatedOn", customerEntity.CreatedOn);
                    objCommand.Parameters.AddWithValue("@has_unlimited_credit", customerEntity.HasUnlimitedCredit);
                    objCommand.Parameters.AddWithValue("@credit_limit", customerEntity.CreditLimit);
                    objCommand.Parameters.AddWithValue("@ImageContent", customerEntity.ImageContent);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    object customerCodeReader = objCommand.ExecuteScalar();

                    if (customerCodeReader != null)
                    {
                        CustomerCode = customerCodeReader.ToString();
                    }
                    

                    //if (customerCodeReader != null && customerCodeReader.HasRows)
                    //{
                    //    while (customerCodeReader.Read())
                    //    {
                    //        CustomerCode = customerCodeReader["new_cust_code"].ToString();
                    //    }
                    //}

                    objCommand.Connection.Close();

                    return CustomerCode;
                }
            }
            catch (Exception ex)
            {
                return CustomerCode;
            }
        }

        public bool UpdateCustomerByMob(string accessToken, CustomerEntity customerEntity)
        {
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CustCode", customerEntity.CustomerCode, DbType.String);
                paramCollection.Add("@Name", customerEntity.Name, DbType.String);
                paramCollection.Add("@Address", customerEntity.Address1, DbType.String);
                paramCollection.Add("@Telephone", customerEntity.Telephone, DbType.String);
                paramCollection.Add("@OutletTypeId", customerEntity.OutletTypeId, DbType.Int32);
                //paramCollection.Add("@RouteAssignId", customerEntity.RouteAssignId, DbType.Int32);
                paramCollection.Add("@IsRetail", customerEntity.IsRetail, DbType.Int32);
                paramCollection.Add("@RepCode", customerEntity.RepCode, DbType.String);

                paramCollection.Add("@Longitude", customerEntity.Longitude, DbType.Double);
                paramCollection.Add("@Latitude", customerEntity.Latitiude, DbType.Double);
                //Add By Irosh
                paramCollection.Add("@has_unlimited_credit", customerEntity.HasUnlimitedCredit, DbType.Boolean);
                paramCollection.Add("@credit_limit", customerEntity.CreditLimit, DbType.Double);

                paramCollection.Add("@ImageContent", customerEntity.ImageContent, DbType.Binary);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["UpdateCustomerByMob"], paramCollection);

                return iNoRec > 0;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        
        public string GetCustomerCodeByTempCustCode(string tempCustCode)
        {
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCodeTmp", tempCustCode, DbType.String);

                string CustomerCode = this.DataAcessService.GetOneValue(CustomerSql["GetCustomerCodeByTempCustCode"], paramCollection).ToString();
                return CustomerCode;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool DeleteCustomerFromSystem(string custCode)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CustCode", custCode, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CustomerSql["DeleteCustomerFromSystem"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        #region "Perfetti 2nd Phase Development" 
        
        public List<CustomerModel> GetAllPendingCustomers(ArgsModel args, string IsReject, string Originator, string Asm, string Territory)
        {
            DataTable objDS = new DataTable();
            List<CustomerModel> Customerlist = new List<CustomerModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllPendingCustomers";

                    objCommand.Parameters.AddWithValue("@Originator", Originator);
                    objCommand.Parameters.AddWithValue("@IsReject", bool.Parse(IsReject));
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", (args.AdditionalParams == null) ? "" : args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@Asm", Asm);
                    objCommand.Parameters.AddWithValue("@Territory", Territory);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            CustomerModel customerModel = new CustomerModel();

                            if (item["cust_code"] != DBNull.Value) customerModel.OutletCode = item["cust_code"].ToString();
                            if (item["name"] != DBNull.Value) customerModel.OutletName = item["name"].ToString();
                            if (item["address_1"] != DBNull.Value) customerModel.Address = item["address_1"].ToString();
                            if (item["del_flag"] != DBNull.Value) customerModel.Status = item["del_flag"].ToString();
                            if (item["outlet_typeid"] != DBNull.Value) customerModel.OutletTypeId = Convert.ToInt32(item["outlet_typeid"]);
                            if (item["outlettype_name"] != DBNull.Value) customerModel.OutletTypeName = item["outlettype_name"].ToString();
                            if (item["latitude"] != DBNull.Value) customerModel.Latitude = Convert.ToDouble(item["latitude"]);
                            if (item["longitude"] != DBNull.Value) customerModel.Longitude = Convert.ToDouble(item["longitude"]);
                            if (item["is_retail"] != DBNull.Value) customerModel.IsRetail = Convert.ToBoolean(item["is_retail"]);
                            if (item["telephone"] != DBNull.Value) customerModel.Telephone = item["telephone"].ToString();
                            if (item["outstanding_balance"] != DBNull.Value) customerModel.OutStandingBalance = Convert.ToDouble(item["outstanding_balance"]);
                            if (item["geo_location"] != DBNull.Value) customerModel.GeoLocation = item["geo_location"].ToString();

                            if (item["route_name"] != DBNull.Value) customerModel.RouteName = item["route_name"].ToString();
                            if (item["dis_code"] != DBNull.Value) customerModel.DisCode = item["dis_code"].ToString();
                            if (item["dis_name"] != DBNull.Value) customerModel.DisName = item["dis_name"].ToString();
                            if (item["territory_name"] != DBNull.Value) customerModel.TerritoryName = item["territory_name"].ToString();
                            if (item["area_name"] != DBNull.Value) customerModel.AreaName = item["area_name"].ToString();
                            if (item["asm_name"] != DBNull.Value) customerModel.Asm = item["asm_name"].ToString();



                            if (item["total_count"] != DBNull.Value) customerModel.TotalCount = Convert.ToInt32(item["total_count"]);

                            Customerlist.Add(customerModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return Customerlist;
        }
        
        public List<CustomerModel> GetAllPendingCustomersForSalesConfirm(ArgsModel args, string IsReject, string Originator, string Asm, string Territory)
        {
            DataTable objDS = new DataTable();
            List<CustomerModel> Customerlist = new List<CustomerModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllPendingCustomersForSalesConfirm";

                    objCommand.Parameters.AddWithValue("@Originator", Originator);
                    objCommand.Parameters.AddWithValue("@IsReject", bool.Parse(IsReject));
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", (args.AdditionalParams == null) ? "" : args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@Asm", Asm);
                    objCommand.Parameters.AddWithValue("@Territory", Territory);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            CustomerModel customerModel = new CustomerModel();

                            if (item["cust_code"] != DBNull.Value) customerModel.OutletCode = item["cust_code"].ToString();
                            if (item["name"] != DBNull.Value) customerModel.OutletName = item["name"].ToString();
                            if (item["address_1"] != DBNull.Value) customerModel.Address = item["address_1"].ToString();
                            if (item["del_flag"] != DBNull.Value) customerModel.Status = item["del_flag"].ToString();
                            if (item["outlet_typeid"] != DBNull.Value) customerModel.OutletTypeId = Convert.ToInt32(item["outlet_typeid"]);
                            if (item["outlettype_name"] != DBNull.Value) customerModel.OutletTypeName = item["outlettype_name"].ToString();
                            if (item["latitude"] != DBNull.Value) customerModel.Latitude = Convert.ToDouble(item["latitude"]);
                            if (item["longitude"] != DBNull.Value) customerModel.Longitude = Convert.ToDouble(item["longitude"]);
                            if (item["is_retail"] != DBNull.Value) customerModel.IsRetail = Convert.ToBoolean(item["is_retail"]);
                            if (item["telephone"] != DBNull.Value) customerModel.Telephone = item["telephone"].ToString();
                            if (item["outstanding_balance"] != DBNull.Value) customerModel.OutStandingBalance = Convert.ToDouble(item["outstanding_balance"]);
                            if (item["geo_location"] != DBNull.Value) customerModel.GeoLocation = item["geo_location"].ToString();

                            if (item["route_name"] != DBNull.Value) customerModel.RouteName = item["route_name"].ToString();
                            if (item["dis_code"] != DBNull.Value) customerModel.DisCode = item["dis_code"].ToString();
                            if (item["dis_name"] != DBNull.Value) customerModel.DisName = item["dis_name"].ToString();
                            if (item["territory_name"] != DBNull.Value) customerModel.TerritoryName = item["territory_name"].ToString();
                            if (item["area_name"] != DBNull.Value) customerModel.AreaName = item["area_name"].ToString();
                            if (item["asm_name"] != DBNull.Value) customerModel.Asm = item["asm_name"].ToString();

                            if (item["total_count"] != DBNull.Value) customerModel.TotalCount = Convert.ToInt32(item["total_count"]);

                            Customerlist.Add(customerModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return Customerlist;
        }

        public List<CustomerModel> GetAllAllCustomerByRouteMasterId(ArgsModel args, string RouteMasterId)
        {
            DataTable objDS = new DataTable();
            List<CustomerModel> Customerlist = new List<CustomerModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllAllCustomerByRouteMasterId";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@RouteMasterId", RouteMasterId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            CustomerModel customerModel = new CustomerModel();

                            customerModel.RouteMasterId = Convert.ToInt32(RouteMasterId);
                            if (item["cust_code"] != DBNull.Value) customerModel.OutletCode = item["cust_code"].ToString();
                            if (item["name"] != DBNull.Value) customerModel.OutletName = item["name"].ToString();
                            if (item["address_1"] != DBNull.Value) customerModel.Address = item["address_1"].ToString();
                            if (item["del_flag"] != DBNull.Value) customerModel.Status = item["del_flag"].ToString();
                            if (item["outlet_typeid"] != DBNull.Value) customerModel.OutletTypeId = Convert.ToInt32(item["outlet_typeid"]);
                            if (item["outlettype_name"] != DBNull.Value) customerModel.OutletTypeName = item["outlettype_name"].ToString();
                            if (item["latitude"] != DBNull.Value) customerModel.Latitude = Convert.ToDouble(item["latitude"]);
                            if (item["longitude"] != DBNull.Value) customerModel.Longitude = Convert.ToDouble(item["longitude"]);
                            if (item["is_retail"] != DBNull.Value) customerModel.IsRetail = Convert.ToBoolean(item["is_retail"]);
                            if (item["telephone"] != DBNull.Value) customerModel.Telephone = item["telephone"].ToString();
                            if (item["outstanding_balance"] != DBNull.Value) customerModel.OutStandingBalance = Convert.ToDouble(item["outstanding_balance"]);
                            if (item["geo_location"] != DBNull.Value) customerModel.GeoLocation = item["geo_location"].ToString();
                            if (item["total_count"] != DBNull.Value) customerModel.TotalCount = Convert.ToInt32(item["total_count"]);

                            Customerlist.Add(customerModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return Customerlist;
        }

        public bool DeleteBulkCust(string userType, string userName, string custList)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteBulkCustomer";

                    objCommand.Parameters.AddWithValue("@userType", userType);
                    objCommand.Parameters.AddWithValue("@userName", userName);
                    objCommand.Parameters.AddWithValue("@custList", custList);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }
        
        public bool AcceptRejectCustomer(string Originator, string CustCode, bool IsReject)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "AcceptRejectCustomer";

                    objCommand.Parameters.AddWithValue("@Originator", Originator);
                    objCommand.Parameters.AddWithValue("@CustCode", CustCode);
                    objCommand.Parameters.AddWithValue("@IsReject", IsReject);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }
        
        public bool AcceptSalesRejectCustomer(string Originator, string CustCode, bool IsReject)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "AcceptSalesRejectCustomer";

                    objCommand.Parameters.AddWithValue("@Originator", Originator);
                    objCommand.Parameters.AddWithValue("@CustCode", CustCode);
                    objCommand.Parameters.AddWithValue("@IsReject", IsReject);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool UpdateCustomerDayOrderByRep(string originator, int routeId, string customerCode, int dayOrder)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "UpdateCustomerDayOrderByRep";

                    objCommand.Parameters.AddWithValue("@Originator", originator);
                    objCommand.Parameters.AddWithValue("@RouteId", routeId);
                    objCommand.Parameters.AddWithValue("@CustCode", customerCode);
                    objCommand.Parameters.AddWithValue("@DayOrder", dayOrder);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        #endregion
        public List<CustomerECOModel> GetCustomerECOCumulativeByAccessToken(string accessToken, string strDate, string endDate)
        {
            CustomerECOModel customerEntity = null;
            DataTable objDS = new DataTable();
            List<CustomerECOModel> CustomerList = new List<CustomerECOModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetCustomerECOCumulative";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);
                    objCommand.Parameters.AddWithValue("@StartDate", strDate);
                    objCommand.Parameters.AddWithValue("@EndDate", endDate);



                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            customerEntity = new CustomerECOModel();

                            if (item["CustomerCode"] != DBNull.Value) customerEntity.CustomerCode = item["CustomerCode"].ToString();

                            if (item["CustomerName"] != DBNull.Value) customerEntity.CustomerName = item["CustomerName"].ToString();

                            if (item["isPC"] != DBNull.Value)
                            {
                                string isPCValue = item["isPC"].ToString();
                                if (isPCValue == "1" || isPCValue.Equals("true", StringComparison.OrdinalIgnoreCase))
                                {
                                    customerEntity.isPC = true;
                                }
                                else if (isPCValue == "0" || isPCValue.Equals("false", StringComparison.OrdinalIgnoreCase))
                                {
                                    customerEntity.isPC = false;
                                }
                                else
                                {
                                    // Handle any other cases or set a default value
                                    customerEntity.isPC = false; // Or set a default value
                                }
                            }






                            CustomerList.Add(customerEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return CustomerList;
        }

    }
}
