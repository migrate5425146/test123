﻿using Peercore.CRM.Model;
using Peercore.CRM.Shared;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.Workflow.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Common;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public class ModernTradeDAO : BaseDAO
    {
        private DbSqlAdapter ModernTradeSql { get; set; }

        private void RegisterSql()
        {
            this.ModernTradeSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.ModernTradeSql.xml", ApplicationService.Instance.DbProvider);
        }

        public ModernTradeDAO()
        {
            RegisterSql();
        }

        public ModernTradeDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public bool InsertModernTradeHeader(out int modernTradeId, ModernTradeMasterModel modernTrade)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertModernTradeHeader";

                    objCommand.Parameters.AddWithValue("@ModernTradeId", modernTrade.ModernTradeId);
                    objCommand.Parameters.AddWithValue("@ModernTradeCode", modernTrade.ModernTradeCode);
                    objCommand.Parameters.AddWithValue("@ModernTradeDate", modernTrade.ModernTradeDate);
                    objCommand.Parameters.AddWithValue("@RepCode", modernTrade.RepCode);
                    objCommand.Parameters.AddWithValue("@CustomerCode", modernTrade.CustomerCode);
                    objCommand.Parameters.AddWithValue("@Status", (modernTrade.Canceled) ? "D" : "A");
                    objCommand.Parameters.AddWithValue("@Remark", (modernTrade.Remark == null) ? "" : modernTrade.Remark);
                    objCommand.Parameters.AddWithValue("@Longitude", modernTrade.Longitude);
                    objCommand.Parameters.AddWithValue("@Latitude", modernTrade.Latitude);
                    objCommand.Parameters.AddWithValue("@RouteId", modernTrade.RouteId);
                    objCommand.Parameters.AddWithValue("@TerritoryId", modernTrade.TerritoryId);
                    objCommand.Parameters.AddWithValue("@AreaId", modernTrade.AreaId);
                    objCommand.Parameters.AddWithValue("@RegionId", modernTrade.RegionId);
                    objCommand.Parameters.AddWithValue("@DistributorId", modernTrade.DistributorId);
                    objCommand.Parameters.AddWithValue("@AsmId", modernTrade.AsmId);
                    objCommand.Parameters.AddWithValue("@RsmId", modernTrade.RsmId);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    modernTradeId = int.Parse(objCommand.ExecuteScalar().ToString());
                    if (modernTradeId > 0)
                    {
                        retStatus = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool DeleteModernTradeHeader(string deleteBy, decimal modernTradeId)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteModernTradeHeader";

                    objCommand.Parameters.AddWithValue("@ModernTradeId", modernTradeId);
                    objCommand.Parameters.AddWithValue("@DeletedBy", deleteBy);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    modernTradeId = int.Parse(objCommand.ExecuteScalar().ToString());
                    if (modernTradeId > 0)
                    {
                        retStatus = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool DeleteModernTradeDetails(decimal modernTradeId)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteModernTradeDetails";

                    objCommand.Parameters.AddWithValue("@ModernTradeId", modernTradeId);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    modernTradeId = int.Parse(objCommand.ExecuteScalar().ToString());
                    if (modernTradeId > 0)
                    {
                        retStatus = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool InsertModernTradeDetails(ModernTradeDetailsModel modernTradeDetail)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertModernTradeDetail";

                    objCommand.Parameters.AddWithValue("@ModernTradeId", modernTradeDetail.ModernTradeId);
                    objCommand.Parameters.AddWithValue("@ProductId", modernTradeDetail.ProductID);
                    objCommand.Parameters.AddWithValue("@Qty", modernTradeDetail.Qty);
                    objCommand.Parameters.AddWithValue("@Status", modernTradeDetail.Status);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    int count = objCommand.ExecuteNonQuery();
                    if (count > 0)
                    {
                        retStatus = true;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public List<ModernTradeMasterModel> GetAllModernTradesByAccessToken(string accessToken, string date)
        {
            ModernTradeMasterModel mtModel = null;
            DataTable objDS = new DataTable();
            List<ModernTradeMasterModel> MTList = new List<ModernTradeMasterModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllModernTradesByAccessToken";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);
                    objCommand.Parameters.AddWithValue("@SelectDate", date);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            mtModel = new ModernTradeMasterModel();
                            if (item["id"] != DBNull.Value) mtModel.ModernTradeId = Convert.ToDecimal(item["id"]);
                            if (item["mt_code"] != DBNull.Value) mtModel.ModernTradeCode = item["mt_code"].ToString();
                            if (item["mt_date"] != DBNull.Value) mtModel.ModernTradeDate = Convert.ToDateTime(item["mt_date"]).ToString("yyyy-MM-dd HH:mm");
                            if (item["cust_code"] != DBNull.Value) mtModel.CustomerCode = item["cust_code"].ToString();
                            if (item["cust_name"] != DBNull.Value) mtModel.CustomerName = item["cust_name"].ToString();
                            if (item["latitude"] != DBNull.Value) mtModel.Latitude = Convert.ToDouble(item["latitude"]);
                            if (item["longitude"] != DBNull.Value) mtModel.Longitude = Convert.ToDouble(item["longitude"]);
                            if (item["remark"] != DBNull.Value) mtModel.Remark = item["remark"].ToString();
                            if (item["route_id"] != DBNull.Value) mtModel.RouteId = Convert.ToInt32(item["route_id"]);
                            if (item["route_name"] != DBNull.Value) mtModel.RouteName = item["route_name"].ToString();
                            if (item["territory_id"] != DBNull.Value) mtModel.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["area_id"] != DBNull.Value) mtModel.AreaId = Convert.ToInt32(item["area_id"]);
                            if (item["region_id"] != DBNull.Value) mtModel.RegionId = Convert.ToInt32(item["region_id"]);
                            if (item["rep_code"] != DBNull.Value) mtModel.RepCode = item["rep_code"].ToString();
                            if (item["dis_id"] != DBNull.Value) mtModel.DistributorId = item["dis_id"].ToString();
                            if (item["asm_id"] != DBNull.Value) mtModel.AsmId = item["asm_id"].ToString();
                            if (item["status"] != DBNull.Value) mtModel.Canceled = (item["status"].ToString() == "A") ? false : true;

                            try
                            {
                                List<ModernTradeDetailsModel> mtDetails = GetAllModernTradeDetailsByModernTradeId(mtModel.ModernTradeId);
                                mtModel.ModernTradeItemCollection = mtDetails;
                            }
                            catch { }

                            MTList.Add(mtModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return MTList;
        }
        
        public List<ModernTradeMasterModel> GetAllModernTradesByAccessTokenAndPeriod(string accessToken, string startDate, string endDate)
        {
            ModernTradeMasterModel mtModel = null;
            DataTable objDS = new DataTable();
            List<ModernTradeMasterModel> MTList = new List<ModernTradeMasterModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllModernTradesByAccessTokenAndPeriod";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);
                    objCommand.Parameters.AddWithValue("@StartDate", startDate);
                    objCommand.Parameters.AddWithValue("@EndDate", endDate);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            mtModel = new ModernTradeMasterModel();
                            if (item["id"] != DBNull.Value) mtModel.ModernTradeId = Convert.ToDecimal(item["id"]);
                            if (item["mt_code"] != DBNull.Value) mtModel.ModernTradeCode = item["mt_code"].ToString();
                            if (item["mt_date"] != DBNull.Value) mtModel.ModernTradeDate = Convert.ToDateTime(item["mt_date"]).ToString("yyyy-MM-dd HH:mm");
                            if (item["cust_code"] != DBNull.Value) mtModel.CustomerCode = item["cust_code"].ToString();
                            if (item["cust_name"] != DBNull.Value) mtModel.CustomerName = item["cust_name"].ToString();
                            if (item["latitude"] != DBNull.Value) mtModel.Latitude = Convert.ToDouble(item["latitude"]);
                            if (item["longitude"] != DBNull.Value) mtModel.Longitude = Convert.ToDouble(item["longitude"]);
                            if (item["remark"] != DBNull.Value) mtModel.Remark = item["remark"].ToString();
                            if (item["route_id"] != DBNull.Value) mtModel.RouteId = Convert.ToInt32(item["route_id"]);
                            if (item["route_name"] != DBNull.Value) mtModel.RouteName = item["route_name"].ToString();
                            if (item["territory_id"] != DBNull.Value) mtModel.TerritoryId = Convert.ToInt32(item["territory_id"]);
                            if (item["area_id"] != DBNull.Value) mtModel.AreaId = Convert.ToInt32(item["area_id"]);
                            if (item["region_id"] != DBNull.Value) mtModel.RegionId = Convert.ToInt32(item["region_id"]);
                            if (item["rep_code"] != DBNull.Value) mtModel.RepCode = item["rep_code"].ToString();
                            if (item["dis_id"] != DBNull.Value) mtModel.DistributorId = item["dis_id"].ToString();
                            if (item["asm_id"] != DBNull.Value) mtModel.AsmId = item["asm_id"].ToString();
                            if (item["status"] != DBNull.Value) mtModel.Canceled = (item["status"].ToString() == "A") ? false : true;

                            try
                            {
                                List<ModernTradeDetailsModel> mtDetails = GetAllModernTradeDetailsByModernTradeId(mtModel.ModernTradeId);
                                mtModel.ModernTradeItemCollection = mtDetails;
                            }
                            catch { }

                            MTList.Add(mtModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return MTList;
        }

        public List<ModernTradeDetailsModel> GetAllModernTradeDetailsByModernTradeId(decimal modernTradeId)
        {
            ModernTradeDetailsModel mtModel = null;
            DataTable objDS = new DataTable();
            List<ModernTradeDetailsModel> MTList = new List<ModernTradeDetailsModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllModernTradeDetailsByModernTradeId";

                    objCommand.Parameters.AddWithValue("@ModernTradeId", modernTradeId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            mtModel = new ModernTradeDetailsModel();
                            if (item["id"] != DBNull.Value) mtModel.ModernTradeDetailId = Convert.ToDecimal(item["id"]);
                            if (item["mt_id"] != DBNull.Value) mtModel.ModernTradeId = Convert.ToDecimal(item["mt_id"]);
                            if (item["product_id"] != DBNull.Value) mtModel.ProductID = Convert.ToInt32(item["product_id"]);
                            if (item["product_name"] != DBNull.Value) mtModel.ProductName = item["product_name"].ToString();
                            if (item["quantity"] != DBNull.Value) mtModel.Qty = Convert.ToDouble(item["quantity"]);
                            
                            MTList.Add(mtModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return MTList;
        }
    }
}
