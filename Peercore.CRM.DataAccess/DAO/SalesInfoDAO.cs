﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public class SalesInfoDAO : BaseDAO
    {
        private DbSqlAdapter SalesInfoSql { get; set; }

        private void RegisterSql()
        {
            this.SalesInfoSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.SalesSql.xml", ApplicationService.Instance.DbProvider);
        }

        public SalesInfoDAO()
        {
            RegisterSql();
        }

        public SalesInfoDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public SalesInfoDetailSearchCriteriaEntity CreateObject()
        {
            return SalesInfoDetailSearchCriteriaEntity.CreateObject();
        }
        /*
        public List<BePieChartEntity> GetSalesInfoPieChart(SalesInfoDetailSearchCriteriaEntity searchCriteria, ArgsEntity args)
        {
            DbDataReader drSalesInfo = null;
            List<BePieChartEntity> SalesInfoList = new List<BePieChartEntity>();
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@DetailType", searchCriteria.DetailType, DbType.String);
                paramCollection.Add("@BusinessAreaList", searchCriteria.BusinessAreaList.Count == 0 ? "" : string.Join("','", searchCriteria.BusinessAreaList), DbType.String);
                paramCollection.Add("@LcSumTable", searchCriteria.sLcSumTable, DbType.String);

                paramCollection.Add("@CatalogType", searchCriteria.CatalogType, DbType.String);
                paramCollection.Add("@BusinessArea", searchCriteria.BusinessArea, DbType.String);
                paramCollection.Add("@Market", searchCriteria.Market, DbType.String);
                paramCollection.Add("@Brand", searchCriteria.Brand, DbType.String);
                paramCollection.Add("@CategoryGroup", searchCriteria.CategoryGroup, DbType.String);

                paramCollection.Add("@CategorySubGroup", searchCriteria.CategorySubGroup, DbType.String);
                paramCollection.Add("@State", searchCriteria.State, DbType.String);

                string sWhrCls = "";
                if (searchCriteria.ChildReps != null)
                    sWhrCls = searchCriteria.ChildReps.Replace("rep_code", "r.rep_code");
                //if (searchCriteria.RepCodes != null && searchCriteria.RepCodes.Count > 0)
                //{                    
                //    string testRepCode = (string)searchCriteria.RepCodes[0];

                //    if (testRepCode.Length == 2)
                //    {
                //        sWhrCls = " AND r.rep_code IN (";

                //        foreach (string repCode in searchCriteria.RepCodes)
                //            sWhrCls += "'" + repCode + "',";

                //        sWhrCls = sWhrCls.Substring(0, sWhrCls.Length - 1);
                //        sWhrCls += ") ";
                //    }
                //    else
                //    {
                //        sWhrCls = " AND sh.state_code IN (";

                //        foreach (string repCode in searchCriteria.RepCodes)
                //            sWhrCls += "'" + repCode + "',";

                //        sWhrCls = sWhrCls.Substring(0, sWhrCls.Length - 1);
                //        sWhrCls += ") ";
                //    }

                //}

                paramCollection.Add("@RepCodes", sWhrCls, DbType.String);


                paramCollection.Add("@CustomerCode", searchCriteria.CustomerCode, DbType.String);
                paramCollection.Add("@CustomerGroup", searchCriteria.CustomerGroup, DbType.String);
                paramCollection.Add("@CustomerSubGroup", searchCriteria.CustomerSubGroup, DbType.String);

                paramCollection.Add("@RepType", searchCriteria.RepType, DbType.String);
                paramCollection.Add("@PCDescription", searchCriteria.parentCustomerDescription, DbType.String);
                paramCollection.Add("@CatalogCode", searchCriteria.CatalogCode, DbType.String);
                paramCollection.Add("@ParamValChar", "", DbType.String);//setting.ParamValChar
                paramCollection.Add("@BackeryOption", searchCriteria.BackeryOption, DbType.Int32);
                paramCollection.Add("@CostPeriod", searchCriteria.iCostPeriod, DbType.Int32);
                paramCollection.Add("@SortField", searchCriteria.SortField, DbType.String);
                //paramCollection.Add("@DisplayOption", searchCriteria.cDisplayOption, DbType.String);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);


                drSalesInfo = this.DataAcessService.ExecuteQuery(SalesInfoSql["GetSalesInfoPieChart"], paramCollection);
                if (drSalesInfo != null && drSalesInfo.HasRows)
                {
                    int sum1Ordinal = drSalesInfo.GetOrdinal("Sum1");
                    int sum2Ordinal = drSalesInfo.GetOrdinal("Sum2");
                    

                    int codeOrdinal = drSalesInfo.GetOrdinal("code");
                    int mdOrdinal = drSalesInfo.GetOrdinal("description");
                    

                    while (drSalesInfo.Read())
                    {
                        BePieChartEntity salesInfo = BePieChartEntity.CreateObject();
                        if (searchCriteria.cDisplayOption.ToString() == "D")
                        {
                            if (!drSalesInfo.IsDBNull(sum1Ordinal)) salesInfo.Value = drSalesInfo.GetDouble(sum1Ordinal);
                        }
                        else
                        {
                            if (!drSalesInfo.IsDBNull(sum2Ordinal)) salesInfo.Value = drSalesInfo.GetDouble(sum2Ordinal);
                        }

                        if (!drSalesInfo.IsDBNull(mdOrdinal)) salesInfo.Name = drSalesInfo.GetString(mdOrdinal);
                        

                        SalesInfoList.Add(salesInfo);
                    }
                }
                return SalesInfoList.OrderBy(c => c.Value).ToList();
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drSalesInfo != null)
                    drSalesInfo.Close();
            }
        }

        public List<SalesInfoEntity> GetSalesInfoDetails(SalesInfoDetailSearchCriteriaEntity searchCriteria,ArgsEntity args)
        {
            DbDataReader drSalesInfo = null;
            List<SalesInfoEntity> SalesInfoList = new List<SalesInfoEntity>();
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@DetailType", searchCriteria.DetailType, DbType.String);
                paramCollection.Add("@BusinessAreaList", searchCriteria.BusinessAreaList.Count == 0 ? "" : string.Join("','", searchCriteria.BusinessAreaList), DbType.String);
                paramCollection.Add("@LcSumTable", searchCriteria.sLcSumTable, DbType.String);

                paramCollection.Add("@CatalogType", searchCriteria.CatalogType, DbType.String);
                paramCollection.Add("@BusinessArea", searchCriteria.BusinessArea, DbType.String);
                paramCollection.Add("@Market", searchCriteria.Market, DbType.String);
                paramCollection.Add("@Brand", searchCriteria.Brand, DbType.String);
                paramCollection.Add("@CategoryGroup", searchCriteria.CategoryGroup, DbType.String);

                paramCollection.Add("@CategorySubGroup", searchCriteria.CategorySubGroup, DbType.String);
                paramCollection.Add("@State", searchCriteria.State, DbType.String);

                string sWhrCls = "";
                if (searchCriteria.ChildReps != null)
                    sWhrCls = searchCriteria.ChildReps.Replace("rep_code", "r.rep_code");
                //if (searchCriteria.RepCodes != null && searchCriteria.RepCodes.Count > 0)
                //{                    
                //    string testRepCode = (string)searchCriteria.RepCodes[0];

                //    if (testRepCode.Length == 2)
                //    {
                //        sWhrCls = " AND r.rep_code IN (";

                //        foreach (string repCode in searchCriteria.RepCodes)
                //            sWhrCls += "'" + repCode + "',";

                //        sWhrCls = sWhrCls.Substring(0, sWhrCls.Length - 1);
                //        sWhrCls += ") ";
                //    }
                //    else
                //    {
                //        sWhrCls = " AND sh.state_code IN (";

                //        foreach (string repCode in searchCriteria.RepCodes)
                //            sWhrCls += "'" + repCode + "',";

                //        sWhrCls = sWhrCls.Substring(0, sWhrCls.Length - 1);
                //        sWhrCls += ") ";
                //    }

                //}

                paramCollection.Add("@RepCodes", sWhrCls, DbType.String);


                paramCollection.Add("@CustomerCode", searchCriteria.CustomerCode, DbType.String);
                paramCollection.Add("@CustomerGroup", searchCriteria.CustomerGroup, DbType.String);
                paramCollection.Add("@CustomerSubGroup", searchCriteria.CustomerSubGroup, DbType.String);

                paramCollection.Add("@RepType", searchCriteria.RepType, DbType.String);
                paramCollection.Add("@PCDescription", searchCriteria.parentCustomerDescription, DbType.String);
                paramCollection.Add("@CatalogCode", searchCriteria.CatalogCode, DbType.String);
                paramCollection.Add("@ParamValChar", "", DbType.String);//setting.ParamValChar
                paramCollection.Add("@BackeryOption", searchCriteria.BackeryOption, DbType.Int32);
                paramCollection.Add("@CostPeriod", searchCriteria.iCostPeriod, DbType.Int32);
                paramCollection.Add("@SortField", searchCriteria.SortField, DbType.String);
                //paramCollection.Add("@DisplayOption", searchCriteria.cDisplayOption, DbType.String);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
               

                //if (searchCriteria.BackeryOption == 1)
                //{
                //    Setting setting = (new clsSettings()).GetSettings("BAKERY_MARKETS");
                //    if (setting != null)
                //    {
                //        sFrom.Append(" ,catalog ca ");
                //        sWhereCls.AppendFormat(" AND sh.catlog_code = ca.catlog_code AND ca.market IN {0}", setting.ParamValChar);
                //    }
                //}




                drSalesInfo = this.DataAcessService.ExecuteQuery(SalesInfoSql["GetSalesInfoDetails"], paramCollection);
                if (drSalesInfo != null && drSalesInfo.HasRows)
                {
                    int sum1Ordinal = drSalesInfo.GetOrdinal("Sum1");
                    int sum2Ordinal = drSalesInfo.GetOrdinal("Sum2");
                    int sum3Ordinal = drSalesInfo.GetOrdinal("Sum3");
                    int sum4Ordinal = drSalesInfo.GetOrdinal("Sum4");
                    int sum5Ordinal = drSalesInfo.GetOrdinal("Sum5");
                    int sum6Ordinal = drSalesInfo.GetOrdinal("Sum6");
                    int sum7Ordinal = drSalesInfo.GetOrdinal("Sum7");
                    int sum8Ordinal = drSalesInfo.GetOrdinal("Sum8");
                    int sum9Ordinal = drSalesInfo.GetOrdinal("Sum9");
                    int sum10Ordinal = drSalesInfo.GetOrdinal("Sum10");
                    int sum11Ordinal = drSalesInfo.GetOrdinal("Sum11");
                    int sum12Ordinal = drSalesInfo.GetOrdinal("Sum12");
                    int sum13Ordinal = drSalesInfo.GetOrdinal("Sum13");
                    int sum14Ordinal = drSalesInfo.GetOrdinal("Sum14");
                    int sum15Ordinal = drSalesInfo.GetOrdinal("Sum15");
                    int sum16Ordinal = drSalesInfo.GetOrdinal("Sum16");

                    int volYr1Ordinal = drSalesInfo.GetOrdinal("vol_yr1");
                    int volYr2Ordinal = drSalesInfo.GetOrdinal("vol_yr2");
                    int volYr3Ordinal = drSalesInfo.GetOrdinal("vol_yr3");
                    int valYr1Ordinal = drSalesInfo.GetOrdinal("val_yr1");
                    int valYr2Ordinal = drSalesInfo.GetOrdinal("val_yr2");
                    int valYr3Ordinal = drSalesInfo.GetOrdinal("val_yr3");

                    int codeOrdinal = drSalesInfo.GetOrdinal("code");
                    int mdOrdinal = drSalesInfo.GetOrdinal("description");
                    int conversionOrdinal = -1;


                    int totcountOrdinal = drSalesInfo.GetOrdinal("totcount");

                    int sum1TotOrdinal = drSalesInfo.GetOrdinal("Sum1Tot");
                    int sum2TotOrdinal = drSalesInfo.GetOrdinal("Sum2Tot");
                    int sum3TotOrdinal = drSalesInfo.GetOrdinal("Sum3Tot");
                    int sum4TotOrdinal = drSalesInfo.GetOrdinal("Sum4Tot");
                    int sum5TotOrdinal = drSalesInfo.GetOrdinal("Sum5Tot");
                    int sum6TotOrdinal = drSalesInfo.GetOrdinal("Sum6Tot");
                    int sum7TotOrdinal = drSalesInfo.GetOrdinal("Sum7Tot");
                    int sum8TotOrdinal = drSalesInfo.GetOrdinal("Sum8Tot");
                    int sum9TotOrdinal = drSalesInfo.GetOrdinal("Sum9Tot");
                    int sum10TotOrdinal = drSalesInfo.GetOrdinal("Sum10Tot");
                    int sum11TotOrdinal = drSalesInfo.GetOrdinal("Sum11Tot");
                    int sum12TotOrdinal = drSalesInfo.GetOrdinal("Sum12Tot");
                    int sum13TotOrdinal = drSalesInfo.GetOrdinal("Sum13Tot");
                    int sum14TotOrdinal = drSalesInfo.GetOrdinal("Sum14Tot");
                    int sum15TotOrdinal = drSalesInfo.GetOrdinal("Sum15Tot");
                    int sum16TotOrdinal = drSalesInfo.GetOrdinal("Sum16Tot");

                    int volYr1TotOrdinal = drSalesInfo.GetOrdinal("vol_yr1Tot");
                    int volYr2TotOrdinal = drSalesInfo.GetOrdinal("vol_yr2Tot");
                    int volYr3TotOrdinal = drSalesInfo.GetOrdinal("vol_yr3Tot");
                    int valYr1TotOrdinal = drSalesInfo.GetOrdinal("val_yr1Tot");
                    int valYr2TotOrdinal = drSalesInfo.GetOrdinal("val_yr2Tot");
                    int valYr3TotOrdinal = drSalesInfo.GetOrdinal("val_yr3Tot");

                    while (drSalesInfo.Read())
                    {
                        SalesInfoEntity salesInfo = SalesInfoEntity.CreateObject();
                        salesInfo.DisplayOption = searchCriteria.cDisplayOption.ToString();

                        if (!drSalesInfo.IsDBNull(sum1Ordinal)) salesInfo.Sum1 = drSalesInfo.GetDouble(sum1Ordinal);
                        if (!drSalesInfo.IsDBNull(sum2Ordinal)) salesInfo.Sum2 = drSalesInfo.GetDouble(sum2Ordinal);
                        if (!drSalesInfo.IsDBNull(sum3Ordinal)) salesInfo.Sum3 = drSalesInfo.GetDouble(sum3Ordinal);
                        if (!drSalesInfo.IsDBNull(sum4Ordinal)) salesInfo.Sum4 = drSalesInfo.GetDouble(sum4Ordinal);
                        if (!drSalesInfo.IsDBNull(sum5Ordinal)) salesInfo.Sum5 = drSalesInfo.GetDouble(sum5Ordinal);
                        if (!drSalesInfo.IsDBNull(sum6Ordinal)) salesInfo.Sum6 = drSalesInfo.GetDouble(sum6Ordinal);
                        if (!drSalesInfo.IsDBNull(sum7Ordinal)) salesInfo.Sum7 = drSalesInfo.GetDouble(sum7Ordinal);
                        if (!drSalesInfo.IsDBNull(sum8Ordinal)) salesInfo.Sum8 = drSalesInfo.GetDouble(sum8Ordinal);
                        if (!drSalesInfo.IsDBNull(sum9Ordinal)) salesInfo.Sum9 = drSalesInfo.GetDouble(sum9Ordinal);
                        if (!drSalesInfo.IsDBNull(sum10Ordinal)) salesInfo.Sum10 = drSalesInfo.GetDouble(sum10Ordinal);
                        if (!drSalesInfo.IsDBNull(sum11Ordinal)) salesInfo.Sum11 = drSalesInfo.GetDouble(sum11Ordinal);
                        if (!drSalesInfo.IsDBNull(sum12Ordinal)) salesInfo.Sum12 = drSalesInfo.GetDouble(sum12Ordinal);
                        if (!drSalesInfo.IsDBNull(sum13Ordinal)) salesInfo.Sum13 = drSalesInfo.GetDouble(sum13Ordinal);
                        if (!drSalesInfo.IsDBNull(sum14Ordinal)) salesInfo.Sum14 = drSalesInfo.GetDouble(sum14Ordinal);
                        if (!drSalesInfo.IsDBNull(sum15Ordinal)) salesInfo.Sum15 = drSalesInfo.GetDouble(sum15Ordinal);
                        if (!drSalesInfo.IsDBNull(sum16Ordinal)) salesInfo.Sum16 = drSalesInfo.GetDouble(sum16Ordinal);

                        if (!drSalesInfo.IsDBNull(volYr1Ordinal)) salesInfo.VolYear1 = drSalesInfo.GetDouble(volYr1Ordinal);
                        if (!drSalesInfo.IsDBNull(volYr2Ordinal)) salesInfo.VolYear2 = drSalesInfo.GetDouble(volYr2Ordinal);
                        if (!drSalesInfo.IsDBNull(volYr3Ordinal)) salesInfo.VolYear3 = drSalesInfo.GetDouble(volYr3Ordinal);

                        if (!drSalesInfo.IsDBNull(valYr1Ordinal)) salesInfo.ValYear1 = drSalesInfo.GetDouble(valYr1Ordinal);
                        if (!drSalesInfo.IsDBNull(valYr2Ordinal)) salesInfo.ValYear2 = drSalesInfo.GetDouble(valYr2Ordinal);
                        if (!drSalesInfo.IsDBNull(valYr3Ordinal)) salesInfo.ValYear3 = drSalesInfo.GetDouble(valYr3Ordinal);

                 
                        if (!drSalesInfo.IsDBNull(mdOrdinal)) salesInfo.MD = drSalesInfo.GetString(mdOrdinal);
                        if (!drSalesInfo.IsDBNull(codeOrdinal)) salesInfo.Code = drSalesInfo.GetString(codeOrdinal);

                        if (conversionOrdinal != -1 && !drSalesInfo.IsDBNull(conversionOrdinal)) salesInfo.Conversion = drSalesInfo.GetDouble(conversionOrdinal).ToString();
                        if (!drSalesInfo.IsDBNull(totcountOrdinal)) salesInfo.TotalCount = drSalesInfo.GetInt32(totcountOrdinal);


                        if (!drSalesInfo.IsDBNull(sum1TotOrdinal)) salesInfo.Sum1Tot = drSalesInfo.GetDouble(sum1TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum2TotOrdinal)) salesInfo.Sum2Tot = drSalesInfo.GetDouble(sum2TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum3TotOrdinal)) salesInfo.Sum3Tot = drSalesInfo.GetDouble(sum3TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum4TotOrdinal)) salesInfo.Sum4Tot = drSalesInfo.GetDouble(sum4TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum5TotOrdinal)) salesInfo.Sum5Tot = drSalesInfo.GetDouble(sum5TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum6TotOrdinal)) salesInfo.Sum6Tot = drSalesInfo.GetDouble(sum6TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum7TotOrdinal)) salesInfo.Sum7Tot = drSalesInfo.GetDouble(sum7TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum8TotOrdinal)) salesInfo.Sum8Tot = drSalesInfo.GetDouble(sum8TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum9TotOrdinal)) salesInfo.Sum9Tot = drSalesInfo.GetDouble(sum9TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum10TotOrdinal)) salesInfo.Sum10Tot = drSalesInfo.GetDouble(sum10TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum11TotOrdinal)) salesInfo.Sum11Tot = drSalesInfo.GetDouble(sum11TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum12TotOrdinal)) salesInfo.Sum12Tot = drSalesInfo.GetDouble(sum12TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum13TotOrdinal)) salesInfo.Sum13Tot = drSalesInfo.GetDouble(sum13TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum14TotOrdinal)) salesInfo.Sum14Tot = drSalesInfo.GetDouble(sum14TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum15TotOrdinal)) salesInfo.Sum15Tot = drSalesInfo.GetDouble(sum15TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum16TotOrdinal)) salesInfo.Sum16Tot = drSalesInfo.GetDouble(sum16TotOrdinal);

                        if (!drSalesInfo.IsDBNull(volYr1TotOrdinal)) salesInfo.VolYear1Tot = drSalesInfo.GetDouble(volYr1TotOrdinal);
                        if (!drSalesInfo.IsDBNull(volYr2TotOrdinal)) salesInfo.VolYear2Tot = drSalesInfo.GetDouble(volYr2TotOrdinal);
                        if (!drSalesInfo.IsDBNull(volYr3TotOrdinal)) salesInfo.VolYear3Tot = drSalesInfo.GetDouble(volYr3TotOrdinal);

                        if (!drSalesInfo.IsDBNull(valYr1TotOrdinal)) salesInfo.ValYear1Tot = drSalesInfo.GetDouble(valYr1TotOrdinal);
                        if (!drSalesInfo.IsDBNull(valYr2TotOrdinal)) salesInfo.ValYear2Tot = drSalesInfo.GetDouble(valYr2TotOrdinal);
                        if (!drSalesInfo.IsDBNull(valYr3TotOrdinal)) salesInfo.ValYear3Tot = drSalesInfo.GetDouble(valYr3TotOrdinal);

                        SalesInfoList.Add(salesInfo);
                    }
                }

                return SalesInfoList;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drSalesInfo != null)
                    drSalesInfo.Close();
            }
        }
       
        public void GetTrendData(ref List<AtblEntity> lstTrendData, ref List<string> lstCostPeriods, SalesInfoDetailSearchCriteriaEntity sidSrcCriteria, string sCode, string history_type = "sales")
        {
            DbDataReader drSalesInfo = null;
            try
            {
                //cost
                //clsCostPeriods opCostPeriods = new clsCostPeriods();
                EndUserSalesEntity endUserSalesEntity = new SalesDAO().GetCurrentCostPeriod();
                int iCostYear = endUserSalesEntity.Year;

                new SalesDAO().LoadCostPeriods(ref lstCostPeriods, iCostYear);

                lstTrendData = (from L in lstCostPeriods select new AtblEntity { period = L }).ToList<AtblEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@LastFlag", sidSrcCriteria.sLastFlag, DbType.String);
                paramCollection.Add("@HistoryType", history_type, DbType.String);

                paramCollection.Add("@BusinessArea", sidSrcCriteria.BusinessArea, DbType.String);
                paramCollection.Add("@CatalogType", sidSrcCriteria.CatalogType, DbType.String);
                paramCollection.Add("@Market", sidSrcCriteria.Market, DbType.String);
                paramCollection.Add("@Brand", sidSrcCriteria.Brand, DbType.String);
                paramCollection.Add("@CatalogCode", sidSrcCriteria.CatalogCode, DbType.String);
                paramCollection.Add("@CustomerCode", sidSrcCriteria.CustomerCode, DbType.String);

                paramCollection.Add("@State", sidSrcCriteria.State, DbType.String);
                paramCollection.Add("@RepCode", sidSrcCriteria.RepCode, DbType.String);
                paramCollection.Add("@PCustCode", sidSrcCriteria.ParentCustomerCode, DbType.String);
                paramCollection.Add("@SPCustCode", sidSrcCriteria.SubParentCustomerCode, DbType.String);
                paramCollection.Add("@DetailType", sidSrcCriteria.DetailType, DbType.String);
                paramCollection.Add("@Code", sCode, DbType.String);

                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);


                drSalesInfo = this.DataAcessService.ExecuteQuery(SalesInfoSql["GetTrendData"], paramCollection);
                
                //IngresDataReader ingDataReader = (IngresDataReader)oDataHandle.ExecuteSelect(string.Format(BSESqls.GetTrendData, sFrom.ToString(), sWhereCls.ToString()));

                double sales_vol_yr1 = 0;
                double sales_vol_yr2 = 0;
                double sales_vol_yr3 = 0;
                double sales_vol_yr4 = 0;
                double sales_val_yr1 = 0;
                double sales_val_yr2 = 0;
                double sales_val_yr3 = 0;
                double sales_val_yr4 = 0;

                if (drSalesInfo != null && drSalesInfo.HasRows)
                {
                    int sumVolP1Ordinal = drSalesInfo.GetOrdinal("SumVolP1");
                    int sumVolP2Ordinal = drSalesInfo.GetOrdinal("SumVolP2");
                    int sumVolP3Ordinal = drSalesInfo.GetOrdinal("SumVolP3");
                    int sumVolP4Ordinal = drSalesInfo.GetOrdinal("SumVolP4");
                    int sumVolP5Ordinal = drSalesInfo.GetOrdinal("SumVolP5");
                    int sumVolP6Ordinal = drSalesInfo.GetOrdinal("SumVolP6");
                    int sumVolP7Ordinal = drSalesInfo.GetOrdinal("SumVolP7");
                    int sumVolP8Ordinal = drSalesInfo.GetOrdinal("SumVolP8");
                    int sumVolP9Ordinal = drSalesInfo.GetOrdinal("SumVolP9");
                    int sumVolP10Ordinal = drSalesInfo.GetOrdinal("SumVolP10");
                    int sumVolP11Ordinal = drSalesInfo.GetOrdinal("SumVolP11");
                    int sumVolP12Ordinal = drSalesInfo.GetOrdinal("SumVolP12");

                    int sumVolPp1Ordinal = drSalesInfo.GetOrdinal("SumVolPp1");
                    int sumVolPp2Ordinal = drSalesInfo.GetOrdinal("SumVolPp2");
                    int sumVolPp3Ordinal = drSalesInfo.GetOrdinal("SumVolPp3");
                    int sumVolPp4Ordinal = drSalesInfo.GetOrdinal("SumVolPp4");
                    int sumVolPp5Ordinal = drSalesInfo.GetOrdinal("SumVolPp5");
                    int sumVolPp6Ordinal = drSalesInfo.GetOrdinal("SumVolPp6");
                    int sumVolPp7Ordinal = drSalesInfo.GetOrdinal("SumVolPp7");
                    int sumVolPp8Ordinal = drSalesInfo.GetOrdinal("SumVolPp8");
                    int sumVolPp9Ordinal = drSalesInfo.GetOrdinal("SumVolPp9");
                    int sumVolPp10Ordinal = drSalesInfo.GetOrdinal("SumVolPp10");
                    int sumVolPp11Ordinal = drSalesInfo.GetOrdinal("SumVolPp11");
                    int sumVolPp12Ordinal = drSalesInfo.GetOrdinal("SumVolPp12");

                    int sumValP1Ordinal = drSalesInfo.GetOrdinal("SumValP1");
                    int sumValP2Ordinal = drSalesInfo.GetOrdinal("SumValP2");
                    int sumValP3Ordinal = drSalesInfo.GetOrdinal("SumValP3");
                    int sumValP4Ordinal = drSalesInfo.GetOrdinal("SumValP4");
                    int sumValP5Ordinal = drSalesInfo.GetOrdinal("SumValP5");
                    int sumValP6Ordinal = drSalesInfo.GetOrdinal("SumValP6");
                    int sumValP7Ordinal = drSalesInfo.GetOrdinal("SumValP7");
                    int sumValP8Ordinal = drSalesInfo.GetOrdinal("SumValP8");
                    int sumValP9Ordinal = drSalesInfo.GetOrdinal("SumValP9");
                    int sumValP10Ordinal = drSalesInfo.GetOrdinal("SumValP10");
                    int sumValP11Ordinal = drSalesInfo.GetOrdinal("SumValP11");
                    int sumValP12Ordinal = drSalesInfo.GetOrdinal("SumValP12");

                    int sumValPp1Ordinal = drSalesInfo.GetOrdinal("SumValPp1");
                    int sumValPp2Ordinal = drSalesInfo.GetOrdinal("SumValPp2");
                    int sumValPp3Ordinal = drSalesInfo.GetOrdinal("SumValPp3");
                    int sumValPp4Ordinal = drSalesInfo.GetOrdinal("SumValPp4");
                    int sumValPp5Ordinal = drSalesInfo.GetOrdinal("SumValPp5");
                    int sumValPp6Ordinal = drSalesInfo.GetOrdinal("SumValPp6");
                    int sumValPp7Ordinal = drSalesInfo.GetOrdinal("SumValPp7");
                    int sumValPp8Ordinal = drSalesInfo.GetOrdinal("SumValPp8");
                    int sumValPp9Ordinal = drSalesInfo.GetOrdinal("SumValPp9");
                    int sumValPp10Ordinal = drSalesInfo.GetOrdinal("SumValPp10");
                    int sumValPp11Ordinal = drSalesInfo.GetOrdinal("SumValPp11");
                    int sumValPp12Ordinal = drSalesInfo.GetOrdinal("SumValPp12");

                    int volYr1Ordinal = drSalesInfo.GetOrdinal("vol_yr1");
                    int volYr2Ordinal = drSalesInfo.GetOrdinal("vol_yr2");
                    int volYr3Ordinal = drSalesInfo.GetOrdinal("vol_yr3");
                    int volYr4Ordinal = drSalesInfo.GetOrdinal("vol_yr4");

                    int valYr1Ordinal = drSalesInfo.GetOrdinal("val_yr1");
                    int valYr2Ordinal = drSalesInfo.GetOrdinal("val_yr2");
                    int valYr3Ordinal = drSalesInfo.GetOrdinal("val_yr3");
                    int valYr4Ordinal = drSalesInfo.GetOrdinal("val_yr4");

                    if (drSalesInfo.Read())
                    {
                        if (!drSalesInfo.IsDBNull(sumVolP1Ordinal)) lstTrendData[0].sales_vol_p = drSalesInfo.GetDouble(sumVolP1Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolP2Ordinal)) lstTrendData[1].sales_vol_p = drSalesInfo.GetDouble(sumVolP2Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolP3Ordinal)) lstTrendData[2].sales_vol_p = drSalesInfo.GetDouble(sumVolP3Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolP4Ordinal)) lstTrendData[3].sales_vol_p = drSalesInfo.GetDouble(sumVolP4Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolP5Ordinal)) lstTrendData[4].sales_vol_p = drSalesInfo.GetDouble(sumVolP5Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolP6Ordinal)) lstTrendData[5].sales_vol_p = drSalesInfo.GetDouble(sumVolP6Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolP7Ordinal)) lstTrendData[6].sales_vol_p = drSalesInfo.GetDouble(sumVolP7Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolP8Ordinal)) lstTrendData[7].sales_vol_p = drSalesInfo.GetDouble(sumVolP8Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolP9Ordinal)) lstTrendData[8].sales_vol_p = drSalesInfo.GetDouble(sumVolP9Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolP10Ordinal)) lstTrendData[9].sales_vol_p = drSalesInfo.GetDouble(sumVolP10Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolP11Ordinal)) lstTrendData[10].sales_vol_p = drSalesInfo.GetDouble(sumVolP11Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolP12Ordinal)) lstTrendData[11].sales_vol_p = drSalesInfo.GetDouble(sumVolP12Ordinal);

                        if (!drSalesInfo.IsDBNull(sumVolPp1Ordinal)) lstTrendData[0].sales_vol_pp = drSalesInfo.GetDouble(sumVolPp1Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolPp2Ordinal)) lstTrendData[1].sales_vol_pp = drSalesInfo.GetDouble(sumVolPp2Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolPp3Ordinal)) lstTrendData[2].sales_vol_pp = drSalesInfo.GetDouble(sumVolPp3Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolPp4Ordinal)) lstTrendData[3].sales_vol_pp = drSalesInfo.GetDouble(sumVolPp4Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolPp5Ordinal)) lstTrendData[4].sales_vol_pp = drSalesInfo.GetDouble(sumVolPp5Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolPp6Ordinal)) lstTrendData[5].sales_vol_pp = drSalesInfo.GetDouble(sumVolPp6Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolPp7Ordinal)) lstTrendData[6].sales_vol_pp = drSalesInfo.GetDouble(sumVolPp7Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolPp8Ordinal)) lstTrendData[7].sales_vol_pp = drSalesInfo.GetDouble(sumVolPp8Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolPp9Ordinal)) lstTrendData[8].sales_vol_pp = drSalesInfo.GetDouble(sumVolPp9Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolPp10Ordinal)) lstTrendData[9].sales_vol_pp = drSalesInfo.GetDouble(sumVolPp10Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolPp11Ordinal)) lstTrendData[10].sales_vol_pp = drSalesInfo.GetDouble(sumVolPp11Ordinal);
                        if (!drSalesInfo.IsDBNull(sumVolPp12Ordinal)) lstTrendData[11].sales_vol_pp = drSalesInfo.GetDouble(sumVolPp12Ordinal);

                        if (!drSalesInfo.IsDBNull(sumValP1Ordinal)) lstTrendData[0].sales_val_p = drSalesInfo.GetDouble(sumValP1Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValP2Ordinal)) lstTrendData[1].sales_val_p = drSalesInfo.GetDouble(sumValP2Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValP3Ordinal)) lstTrendData[2].sales_val_p = drSalesInfo.GetDouble(sumValP3Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValP4Ordinal)) lstTrendData[3].sales_val_p = drSalesInfo.GetDouble(sumValP4Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValP5Ordinal)) lstTrendData[4].sales_val_p = drSalesInfo.GetDouble(sumValP5Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValP6Ordinal)) lstTrendData[5].sales_val_p = drSalesInfo.GetDouble(sumValP6Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValP7Ordinal)) lstTrendData[6].sales_val_p = drSalesInfo.GetDouble(sumValP7Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValP8Ordinal)) lstTrendData[7].sales_val_p = drSalesInfo.GetDouble(sumValP8Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValP9Ordinal)) lstTrendData[8].sales_val_p = drSalesInfo.GetDouble(sumValP9Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValP10Ordinal)) lstTrendData[9].sales_val_p = drSalesInfo.GetDouble(sumValP10Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValP11Ordinal)) lstTrendData[10].sales_val_p = drSalesInfo.GetDouble(sumValP11Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValP12Ordinal)) lstTrendData[11].sales_val_p = drSalesInfo.GetDouble(sumValP12Ordinal);

                        if (!drSalesInfo.IsDBNull(sumValPp1Ordinal)) lstTrendData[0].sales_val_pp = drSalesInfo.GetDouble(sumValPp1Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValPp2Ordinal)) lstTrendData[1].sales_val_pp = drSalesInfo.GetDouble(sumValPp2Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValPp3Ordinal)) lstTrendData[2].sales_val_pp = drSalesInfo.GetDouble(sumValPp3Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValPp4Ordinal)) lstTrendData[3].sales_val_pp = drSalesInfo.GetDouble(sumValPp4Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValPp5Ordinal)) lstTrendData[4].sales_val_pp = drSalesInfo.GetDouble(sumValPp5Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValPp6Ordinal)) lstTrendData[5].sales_val_pp = drSalesInfo.GetDouble(sumValPp6Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValPp7Ordinal)) lstTrendData[6].sales_val_pp = drSalesInfo.GetDouble(sumValPp7Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValPp8Ordinal)) lstTrendData[7].sales_val_pp = drSalesInfo.GetDouble(sumValPp8Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValPp9Ordinal)) lstTrendData[8].sales_val_pp = drSalesInfo.GetDouble(sumValPp9Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValPp10Ordinal)) lstTrendData[9].sales_val_pp = drSalesInfo.GetDouble(sumValPp10Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValPp11Ordinal)) lstTrendData[10].sales_val_pp = drSalesInfo.GetDouble(sumValPp11Ordinal);
                        if (!drSalesInfo.IsDBNull(sumValPp12Ordinal)) lstTrendData[11].sales_val_pp = drSalesInfo.GetDouble(sumValPp12Ordinal);

                        if (!drSalesInfo.IsDBNull(volYr1Ordinal)) sales_vol_yr1 = drSalesInfo.GetDouble(volYr1Ordinal);
                        if (!drSalesInfo.IsDBNull(volYr2Ordinal)) sales_vol_yr2 = drSalesInfo.GetDouble(volYr2Ordinal);
                        if (!drSalesInfo.IsDBNull(volYr3Ordinal)) sales_vol_yr3 = drSalesInfo.GetDouble(volYr3Ordinal);
                        if (!drSalesInfo.IsDBNull(volYr4Ordinal)) sales_vol_yr4 = drSalesInfo.GetDouble(volYr4Ordinal);

                        if (!drSalesInfo.IsDBNull(valYr1Ordinal)) sales_val_yr1 = drSalesInfo.GetDouble(valYr1Ordinal);
                        if (!drSalesInfo.IsDBNull(valYr2Ordinal)) sales_val_yr2 = drSalesInfo.GetDouble(valYr2Ordinal);
                        if (!drSalesInfo.IsDBNull(valYr3Ordinal)) sales_val_yr3 = drSalesInfo.GetDouble(valYr3Ordinal);
                        if (!drSalesInfo.IsDBNull(valYr4Ordinal)) sales_val_yr4 = drSalesInfo.GetDouble(valYr4Ordinal);

                    }
                }


                double sales_vol_lastytd = 0;
                double sales_val_lastytd = 0;

                double sales_profit_lastytd = 0;
                double sales_profit_yr = 0;

                double budget_vol_lastytd = 0;
                double budget_val_lastytd = 0;
                double budget_val_yr = 0;
                double budget_vol_yr = 0;
                double sales_vol_yr = 0;
                double sales_val_yr = 0;

                for (int x = 1; x <= 12; x++)
                {
                    if (x <= sidSrcCriteria.iCostPeriod)
                    {
                        sales_vol_lastytd += lstTrendData[x - 1].sales_vol_pp;
                        sales_val_lastytd += lstTrendData[x - 1].sales_val_pp;
                        sales_profit_lastytd += lstTrendData[x - 1].sales_profit_pp;
                        budget_val_lastytd += lstTrendData[x - 1].budget_val_pp;
                        budget_vol_lastytd += lstTrendData[x - 1].budget_vol_pp;
                        budget_val_yr += lstTrendData[x - 1].budget_val_p;
                        budget_vol_yr += lstTrendData[x - 1].budget_vol_p;
                    }

                    if (lstTrendData[x - 1].sales_vol_p != 0)
                    {
                        lstTrendData[x - 1].sales_val_tonval_p = lstTrendData[x - 1].sales_val_p / lstTrendData[x - 1].sales_vol_p;
                        lstTrendData[x - 1].sales_profit_tonval_p = lstTrendData[x - 1].sales_profit_p / lstTrendData[x - 1].sales_vol_p;
                    }

                    if (lstTrendData[x - 1].sales_val_p != 0)
                    {
                        lstTrendData[x - 1].sales_profit_perc_p = lstTrendData[x - 1].sales_profit_p * 100 / lstTrendData[x - 1].sales_val_p;
                    }

                    if (lstTrendData[x - 1].sales_vol_pp != 0)
                    {
                        lstTrendData[x - 1].sales_val_tonval_pp = lstTrendData[x - 1].sales_val_pp / lstTrendData[x - 1].sales_vol_pp;
                        lstTrendData[x - 1].sales_profit_tonval_pp = lstTrendData[x - 1].sales_profit_pp / lstTrendData[x - 1].sales_vol_pp;
                    }

                    if (lstTrendData[x - 1].sales_val_pp != 0)
                    {
                        lstTrendData[x - 1].sales_profit_perc_pp = (lstTrendData[x - 1].sales_profit_pp * 100) / lstTrendData[x - 1].sales_val_pp;
                    }

                    sales_vol_yr = sales_vol_yr + lstTrendData[x - 1].sales_vol_p;
                    sales_val_yr = sales_val_yr + lstTrendData[x - 1].sales_val_p;
                    sales_profit_yr = sales_profit_yr + lstTrendData[x - 1].sales_profit_p;
                }

            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drSalesInfo != null)
                    drSalesInfo.Close();
            }
        }
        */


        public List<SalesDataEntity> GetAllSales(ArgsEntity args, string IsASMOrProduct, int ProductId)
        {
            //DbDataReader salesReader = null;

            //try
            //{
            //    List<SalesDataEntity> salesDataCollection = new List<SalesDataEntity>();

            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@Originator", args.Originator, DbType.String);
            //    paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
            //    paramCollection.Add("@IsASMOrProduct", IsASMOrProduct, DbType.String);
            //    paramCollection.Add("@ProductId", ProductId, DbType.Int32);
                
            //    salesReader = this.DataAcessService.ExecuteQuery(SalesInfoSql["GetAllSales"], paramCollection);


            //    SalesDataEntity salesInfo = null;

            //    if (salesReader.HasRows)
            //    {
            //        while (salesReader.Read())
            //        {
            //            salesInfo = SalesDataEntity.CreateObject();

            //            salesInfo.MonthName = salesReader["month"].ToString();
            //            salesInfo.sales = double.Parse(salesReader["total"].ToString());

            //            salesDataCollection.Add(salesInfo);
            //        }

            //    }

            //    return salesDataCollection;

            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (salesReader != null)
            //        salesReader.Close();
            //}

            DataTable objDS = new DataTable();
            SalesDataEntity oSales = null;
            List<SalesDataEntity> salesDataCollection = new List<SalesDataEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllSales";

                    objCommand.Parameters.AddWithValue("@Originator", args.Originator);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@IsASMOrProduct", IsASMOrProduct);
                    objCommand.Parameters.AddWithValue("@ProductId", ProductId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            oSales = new SalesDataEntity();
                            oSales.MonthName = item["month"].ToString();
                            oSales.sales = Convert.ToDouble(item["total"]);
                            salesDataCollection.Add(oSales);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }


            return salesDataCollection;
        }

        public List<SalesDataEntity> getASMSalesData(ArgsEntity args)
        {
            //DbDataReader salesReader = null;

            //try
            //{
            //    List<SalesDataEntity> salesDataCollection = new List<SalesDataEntity>();

            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@Originator", args.Originator, DbType.String);
            //    paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);

            //    salesReader = this.DataAcessService.ExecuteQuery(SalesInfoSql["getASMSalesForDashboard"], paramCollection);


            //    SalesDataEntity salesInfo = null;

            //    if (salesReader.HasRows)
            //    {
            //        while (salesReader.Read())
            //        {
            //            salesInfo = SalesDataEntity.CreateObject();

            //            salesInfo.MonthName = salesReader["asm"].ToString();
            //            salesInfo.sales = double.Parse(salesReader["total"].ToString());

            //            salesDataCollection.Add(salesInfo);
            //        }

            //    }

            //    return salesDataCollection;

            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (salesReader != null)
            //        salesReader.Close();
            //}

            DataTable objDS = new DataTable();
            SalesDataEntity oSales = null;
            List<SalesDataEntity> salesDataCollection = new List<SalesDataEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "getASMSalesForDashboard";

                    objCommand.Parameters.AddWithValue("@Originator", args.Originator);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            oSales = new SalesDataEntity();
                            oSales.MonthName = item["asm"].ToString();
                            oSales.sales = Convert.ToDouble(item["total"]);
                            salesDataCollection.Add(oSales);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }


            return salesDataCollection;
        }

        public List<SalesDataEntity> GetRepSales(ArgsEntity args)
        {
            //DbDataReader salesReader = null;

            //try
            //{
            //    List<SalesDataEntity> salesDataCollection = new List<SalesDataEntity>();

            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@Originator", args.Originator, DbType.String);
            //    paramCollection.Add("@Date", args.SStartDate, DbType.String);

            //    salesReader = this.DataAcessService.ExecuteQuery(SalesInfoSql["GetRepSalesForDashboard"], paramCollection);


            //    SalesDataEntity salesInfo = null;

            //    if (salesReader.HasRows)
            //    {
            //        while (salesReader.Read())
            //        {
            //            salesInfo = SalesDataEntity.CreateObject();

            //            salesInfo.Originator = salesReader["rep_name"].ToString();
            //            salesInfo.sales = double.Parse(salesReader["gross_total"].ToString());
            //            salesInfo.discount = double.Parse(salesReader["discount_total"].ToString());

            //            salesDataCollection.Add(salesInfo);
            //        }

            //    }

            //    return salesDataCollection;

            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (salesReader != null)
            //        salesReader.Close();
            //}


            DataTable objDS = new DataTable();
            SalesDataEntity oSales = null;
            List<SalesDataEntity> salesDataCollection = new List<SalesDataEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetRepSalesForDashboard";

                    objCommand.Parameters.AddWithValue("@Originator", args.Originator);
                    objCommand.Parameters.AddWithValue("@Date", args.SStartDate);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            oSales = new SalesDataEntity();
                            oSales.Originator = item["rep_name"].ToString();
                            oSales.sales = Convert.ToDouble(item["gross_total"]);
                            oSales.discount = Convert.ToDouble(item["discount_total"].ToString());
                            salesDataCollection.Add(oSales);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
            }


            return salesDataCollection;
        }
    }
}
