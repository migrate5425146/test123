﻿using Peercore.CRM.Entities;
using Peercore.CRM.Shared;
using Peercore.DataAccess.Common.Utilties;
using Peercore.Workflow.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.DataAccess.DAO
{
    public class CompanyDAO : BaseDAO
    {
        private DbSqlAdapter ActivitySql { get; set; }

        private void RegisterSql()
        {
            this.ActivitySql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.CommonSql.xml", ApplicationService.Instance.DbProvider);
        }

        public CompanyDAO()
        {
            RegisterSql();
        }

        public CompanyDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public CompanyEntity CreateObject()
        {
            return CompanyEntity.CreateObject();
        }

        public CompanyEntity CreateObject(int entityId)
        {
            return CompanyEntity.CreateObject(entityId);
        }

        public List<CompanyEntity> GetAllCompany()
        {
            CompanyEntity companyEntity = null;
            DataTable objTable = new DataTable();
            List<CompanyEntity> CompanyList = new List<CompanyEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllCompany";

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objTable);

                    if (objTable.Rows.Count > 0)
                    {
                        foreach (DataRow item in objTable.Rows)
                        {
                            companyEntity = CompanyEntity.CreateObject();
                            if (item["comp_id"] != DBNull.Value) companyEntity.CompId = decimal.Parse(item["comp_id"].ToString());
                            if (item["comp_code"] != DBNull.Value) companyEntity.CompCode = item["comp_code"].ToString();
                            if (item["comp_name"] != DBNull.Value) companyEntity.CompName = item["comp_name"].ToString();
                            CompanyList.Add(companyEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return CompanyList;
        }
    }
}
