﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;

namespace Peercore.CRM.DataAccess.DAO
{
    public class EndUserSalesInfoDetailsDAO:BaseDAO
    {
        private DbSqlAdapter SalesInfoSql { get; set; }

        private void RegisterSql()
        {
            this.SalesInfoSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.SalesSql.xml", ApplicationService.Instance.DbProvider);
        }

        public EndUserSalesInfoDetailsDAO()
        {
            RegisterSql();
        }

        public EndUserSalesInfoDetailsDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public SalesInfoDetailSearchCriteriaEntity CreateObject()
        {
            return SalesInfoDetailSearchCriteriaEntity.CreateObject();
        }

        /*
        public List<SalesInfoEntity> GetEndUserSalesInfoDetails(SalesInfoDetailSearchCriteriaEntity searchCriteria,ArgsEntity args)
        {
            DbDataReader drSalesInfo = null;
            try
            {
                List<SalesInfoEntity> salesInfoList = new List<SalesInfoEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@DetailType", searchCriteria.DetailType, DbType.String);
                paramCollection.Add("@EndUserTable", searchCriteria.sEndUserTable, DbType.String);
                paramCollection.Add("@Brand", searchCriteria.Brand, DbType.String);
                paramCollection.Add("@State", searchCriteria.State, DbType.String);

                string sWhrCls = "";
                if (searchCriteria.ChildReps != null)
                    sWhrCls = searchCriteria.ChildReps.Replace("rep_code", "r.rep_code");

                paramCollection.Add("@AdditionalWhere", sWhrCls, DbType.String);

                //if (searchCriteria.RepCode != null && searchCriteria.RepCodes.Count > 0)
                //{
                //    string sWhrCls = "";
                //    string testRepCode = (string)searchCriteria.RepCodes[0];

                //    if (testRepCode.Length == 2)
                //    {
                //        sWhrCls = " AND r.rep_code IN (";

                //        foreach (string repCode in searchCriteria.RepCodes)
                //            sWhrCls += "'" + repCode + "',";

                //        sWhrCls = sWhrCls.Substring(0, sWhrCls.Length - 1);
                //        sWhrCls += ") ";
                //    }
                //    else
                //    {
                //        sWhrCls = " AND sh.state_code IN (";

                //        foreach (string repCode in searchCriteria.RepCodes)
                //            sWhrCls += "'" + repCode + "',";

                //        sWhrCls = sWhrCls.Substring(0, sWhrCls.Length - 1);
                //        sWhrCls += ") ";
                //    }

                //    paramCollection.Add("@AdditionalWhere", sWhrCls, DbType.String);
                //}
                //else
                //{
                //    paramCollection.Add("@AdditionalWhere", null, DbType.String);
                //}

                
                paramCollection.Add("@CustomerCode", searchCriteria.CustomerCode, DbType.String);
                paramCollection.Add("@CatalogCode", searchCriteria.CatalogCode, DbType.String);
                paramCollection.Add("@CostPeriod", searchCriteria.iCostPeriod.ToString(), DbType.String);
                paramCollection.Add("@RepType", searchCriteria.RepType, DbType.String);

                string sum1 = "", sum2 = "", sum3 = "", sum4 = "", sum5 = "", sum6 = "";

                switch (searchCriteria.iCostPeriod)
                {
                    case 1: sum1 = "p1"; sum2 = "pp12"; sum3 = "pp11"; sum4 = "pp10"; sum5 = "pp9"; sum6 = "pp8"; break;
                    case 2: sum1 = "p2"; sum2 = "p1"; sum3 = "pp12"; sum4 = "pp11"; sum5 = "pp10"; sum6 = "pp8"; break;
                    case 3: sum1 = "p3"; sum2 = "p2"; sum3 = "p1"; sum4 = "pp12"; sum5 = "pp11"; sum6 = "pp10"; break;
                    case 4: sum1 = "p4"; sum2 = "p3"; sum3 = "p2"; sum4 = "p1"; sum5 = "pp12"; sum6 = "pp11"; break;
                    case 5: sum1 = "p5"; sum2 = "p4"; sum3 = "p3"; sum4 = "p2"; sum5 = "p1"; sum6 = "pp12"; break;
                    case 6: sum1 = "p6"; sum2 = "p5"; sum3 = "p4"; sum4 = "p3"; sum5 = "p2"; sum6 = "p1"; break;
                    case 7: sum1 = "p7"; sum2 = "p6"; sum3 = "p5"; sum4 = "p4"; sum5 = "p3"; sum6 = "p2"; break;
                    case 8: sum1 = "p8"; sum2 = "p7"; sum3 = "p6"; sum4 = "p5"; sum5 = "p4"; sum6 = "p3"; break;
                    case 9: sum1 = "p9"; sum2 = "p8"; sum3 = "p7"; sum4 = "p6"; sum5 = "p5"; sum6 = "p4"; break;
                    case 10: sum1 = "p10"; sum2 = "p9"; sum3 = "p8"; sum4 = "p7"; sum5 = "p6"; sum6 = "p5"; break;
                    case 11: sum1 = "p11"; sum2 = "p10"; sum3 = "p9"; sum4 = "p8"; sum5 = "p7"; sum6 = "p6"; break;
                    case 12: sum1 = "p12"; sum2 = "p11"; sum3 = "p10"; sum4 = "p9"; sum5 = "p8"; sum6 = "p7"; break;
                    default: break;
                }
                paramCollection.Add("@Sum1", sum1, DbType.String);
                paramCollection.Add("@Sum2", sum2, DbType.String);
                paramCollection.Add("@Sum3", sum3, DbType.String);
                paramCollection.Add("@Sum4", sum4, DbType.String);
                paramCollection.Add("@Sum5", sum5, DbType.String);
                paramCollection.Add("@Sum6", sum6, DbType.String);
                //paramCollection.Add("@OrderBy", searchCriteria.SortField, DbType.String);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                drSalesInfo = this.DataAcessService.ExecuteQuery(SalesInfoSql["GetEndUserSalesInfoDetails"], paramCollection);

                if (drSalesInfo != null && drSalesInfo.HasRows)
                {
                    int sum1Ordinal = drSalesInfo.GetOrdinal("Sum1");
                    int sum2Ordinal = drSalesInfo.GetOrdinal("Sum2");
                    int sum3Ordinal = drSalesInfo.GetOrdinal("Sum3");
                    int sum4Ordinal = drSalesInfo.GetOrdinal("Sum4");
                    int sum5Ordinal = drSalesInfo.GetOrdinal("Sum5");
                    int sum6Ordinal = drSalesInfo.GetOrdinal("Sum6");
                    int sum7Ordinal = drSalesInfo.GetOrdinal("Sum7");
                    int sum8Ordinal = drSalesInfo.GetOrdinal("Sum8");
                    int sum9Ordinal = drSalesInfo.GetOrdinal("Sum9");
                    int sum10Ordinal = drSalesInfo.GetOrdinal("Sum10");
                    int sum11Ordinal = drSalesInfo.GetOrdinal("Sum11");
                    int sum12Ordinal = drSalesInfo.GetOrdinal("Sum12");
                    

                    int codeOrdinal = drSalesInfo.GetOrdinal("code");
                    int mdOrdinal = drSalesInfo.GetOrdinal("description");
                    int conversionOrdinal = -1;


                    int totcountOrdinal = drSalesInfo.GetOrdinal("totcount");

                    int sum1TotOrdinal = drSalesInfo.GetOrdinal("Sum1Tot");
                    int sum2TotOrdinal = drSalesInfo.GetOrdinal("Sum2Tot");
                    int sum3TotOrdinal = drSalesInfo.GetOrdinal("Sum3Tot");
                    int sum4TotOrdinal = drSalesInfo.GetOrdinal("Sum4Tot");
                    int sum5TotOrdinal = drSalesInfo.GetOrdinal("Sum5Tot");
                    int sum6TotOrdinal = drSalesInfo.GetOrdinal("Sum6Tot");
                    int sum7TotOrdinal = drSalesInfo.GetOrdinal("Sum7Tot");
                    int sum8TotOrdinal = drSalesInfo.GetOrdinal("Sum8Tot");
                    int sum9TotOrdinal = drSalesInfo.GetOrdinal("Sum9Tot");
                    int sum10TotOrdinal = drSalesInfo.GetOrdinal("Sum10Tot");
                    int sum11TotOrdinal = drSalesInfo.GetOrdinal("Sum11Tot");
                    int sum12TotOrdinal = drSalesInfo.GetOrdinal("Sum12Tot");
                    //int sum13TotOrdinal = drSalesInfo.GetOrdinal("Sum13Tot");
                    //int sum14TotOrdinal = drSalesInfo.GetOrdinal("Sum14Tot");
                    //int sum15TotOrdinal = drSalesInfo.GetOrdinal("Sum15Tot");
                    //int sum16TotOrdinal = drSalesInfo.GetOrdinal("Sum16Tot");

                    //int volYr1TotOrdinal = drSalesInfo.GetOrdinal("vol_yr1Tot");
                    //int volYr2TotOrdinal = drSalesInfo.GetOrdinal("vol_yr2Tot");
                    //int volYr3TotOrdinal = drSalesInfo.GetOrdinal("vol_yr3Tot");
                    //int valYr1TotOrdinal = drSalesInfo.GetOrdinal("val_yr1Tot");
                    //int valYr2TotOrdinal = drSalesInfo.GetOrdinal("val_yr2Tot");
                    //int valYr3TotOrdinal = drSalesInfo.GetOrdinal("val_yr3Tot");

                    while (drSalesInfo.Read())
                    {
                        SalesInfoEntity salesInfo = SalesInfoEntity.CreateObject();
                        salesInfo.DisplayOption = searchCriteria.cDisplayOption.ToString();

                        if (!drSalesInfo.IsDBNull(sum1Ordinal)) salesInfo.Sum1 = drSalesInfo.GetDouble(sum1Ordinal);
                        if (!drSalesInfo.IsDBNull(sum2Ordinal)) salesInfo.Sum2 = drSalesInfo.GetDouble(sum2Ordinal);
                        if (!drSalesInfo.IsDBNull(sum3Ordinal)) salesInfo.Sum3 = drSalesInfo.GetDouble(sum3Ordinal);
                        if (!drSalesInfo.IsDBNull(sum4Ordinal)) salesInfo.Sum4 = drSalesInfo.GetDouble(sum4Ordinal);
                        if (!drSalesInfo.IsDBNull(sum5Ordinal)) salesInfo.Sum5 = drSalesInfo.GetDouble(sum5Ordinal);
                        if (!drSalesInfo.IsDBNull(sum6Ordinal)) salesInfo.Sum6 = drSalesInfo.GetDouble(sum6Ordinal);
                        if (!drSalesInfo.IsDBNull(sum7Ordinal)) salesInfo.Sum7 = drSalesInfo.GetDouble(sum7Ordinal);
                        if (!drSalesInfo.IsDBNull(sum8Ordinal)) salesInfo.Sum8 = drSalesInfo.GetDouble(sum8Ordinal);
                        if (!drSalesInfo.IsDBNull(sum9Ordinal)) salesInfo.Sum9 = drSalesInfo.GetDouble(sum9Ordinal);
                        if (!drSalesInfo.IsDBNull(sum10Ordinal)) salesInfo.Sum10 = drSalesInfo.GetDouble(sum10Ordinal);
                        if (!drSalesInfo.IsDBNull(sum11Ordinal)) salesInfo.Sum11 = drSalesInfo.GetDouble(sum11Ordinal);
                        if (!drSalesInfo.IsDBNull(sum12Ordinal)) salesInfo.Sum12 = drSalesInfo.GetDouble(sum12Ordinal);
                        
                        salesInfo.Sum13 = 0;
                        salesInfo.Sum14 = 0;
                        salesInfo.Sum15 = 0;
                        salesInfo.Sum16 = 0;

                        salesInfo.VolYear1 = 0;
                        salesInfo.VolYear2 = 0;
                        salesInfo.VolYear3 = 0;

                        salesInfo.ValYear1 = 0;
                        salesInfo.ValYear2 = 0;
                        salesInfo.ValYear3 = 0;


                        if (!drSalesInfo.IsDBNull(mdOrdinal)) salesInfo.MD = drSalesInfo.GetString(mdOrdinal);
                        if (!drSalesInfo.IsDBNull(codeOrdinal)) salesInfo.Code = drSalesInfo.GetString(codeOrdinal);

                        if (conversionOrdinal != -1 && !drSalesInfo.IsDBNull(conversionOrdinal)) salesInfo.Conversion = drSalesInfo.GetDouble(conversionOrdinal).ToString();
                        if (!drSalesInfo.IsDBNull(totcountOrdinal)) salesInfo.TotalCount = drSalesInfo.GetInt32(totcountOrdinal);


                        if (!drSalesInfo.IsDBNull(sum1TotOrdinal)) salesInfo.Sum1Tot = drSalesInfo.GetDouble(sum1TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum2TotOrdinal)) salesInfo.Sum2Tot = drSalesInfo.GetDouble(sum2TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum3TotOrdinal)) salesInfo.Sum3Tot = drSalesInfo.GetDouble(sum3TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum4TotOrdinal)) salesInfo.Sum4Tot = drSalesInfo.GetDouble(sum4TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum5TotOrdinal)) salesInfo.Sum5Tot = drSalesInfo.GetDouble(sum5TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum6TotOrdinal)) salesInfo.Sum6Tot = drSalesInfo.GetDouble(sum6TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum7TotOrdinal)) salesInfo.Sum7Tot = drSalesInfo.GetDouble(sum7TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum8TotOrdinal)) salesInfo.Sum8Tot = drSalesInfo.GetDouble(sum8TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum9TotOrdinal)) salesInfo.Sum9Tot = drSalesInfo.GetDouble(sum9TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum10TotOrdinal)) salesInfo.Sum10Tot = drSalesInfo.GetDouble(sum10TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum11TotOrdinal)) salesInfo.Sum11Tot = drSalesInfo.GetDouble(sum11TotOrdinal);
                        if (!drSalesInfo.IsDBNull(sum12TotOrdinal)) salesInfo.Sum12Tot = drSalesInfo.GetDouble(sum12TotOrdinal);
                        //if (!drSalesInfo.IsDBNull(sum13TotOrdinal)) salesInfo.Sum13Tot = drSalesInfo.GetDouble(sum13TotOrdinal);
                        //if (!drSalesInfo.IsDBNull(sum14TotOrdinal)) salesInfo.Sum14Tot = drSalesInfo.GetDouble(sum14TotOrdinal);
                        //if (!drSalesInfo.IsDBNull(sum15TotOrdinal)) salesInfo.Sum15Tot = drSalesInfo.GetDouble(sum15TotOrdinal);
                        //if (!drSalesInfo.IsDBNull(sum16TotOrdinal)) salesInfo.Sum16Tot = drSalesInfo.GetDouble(sum16TotOrdinal);

                        //if (!drSalesInfo.IsDBNull(volYr1TotOrdinal)) salesInfo.VolYear1Tot = drSalesInfo.GetDouble(volYr1TotOrdinal);
                        //if (!drSalesInfo.IsDBNull(volYr2TotOrdinal)) salesInfo.VolYear2Tot = drSalesInfo.GetDouble(volYr2TotOrdinal);
                        //if (!drSalesInfo.IsDBNull(volYr3TotOrdinal)) salesInfo.VolYear3Tot = drSalesInfo.GetDouble(volYr3TotOrdinal);

                        //if (!drSalesInfo.IsDBNull(valYr1TotOrdinal)) salesInfo.ValYear1Tot = drSalesInfo.GetDouble(valYr1TotOrdinal);
                        //if (!drSalesInfo.IsDBNull(valYr2TotOrdinal)) salesInfo.ValYear2Tot = drSalesInfo.GetDouble(valYr2TotOrdinal);
                        //if (!drSalesInfo.IsDBNull(valYr3TotOrdinal)) salesInfo.ValYear3Tot = drSalesInfo.GetDouble(valYr3TotOrdinal);

                        salesInfoList.Add(salesInfo);
                    }
                }

                return salesInfoList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drSalesInfo != null)
                    drSalesInfo.Close();
            }
        }

        public List<BePieChartEntity> GetEndUserSalesInfoPieChart(SalesInfoDetailSearchCriteriaEntity searchCriteria, ArgsEntity args)
        {
            DbDataReader drSalesInfo = null;
            try
            {
                List<BePieChartEntity> SalesInfoList = new List<BePieChartEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@DetailType", searchCriteria.DetailType, DbType.String);
                paramCollection.Add("@EndUserTable", searchCriteria.sEndUserTable, DbType.String);
                paramCollection.Add("@Brand", searchCriteria.Brand, DbType.String);
                paramCollection.Add("@State", searchCriteria.State, DbType.String);

                string sWhrCls = "";
                if (searchCriteria.ChildReps != null)
                    sWhrCls = searchCriteria.ChildReps.Replace("rep_code", "r.rep_code");

                paramCollection.Add("@AdditionalWhere", sWhrCls, DbType.String);
                paramCollection.Add("@CustomerCode", searchCriteria.CustomerCode, DbType.String);
                paramCollection.Add("@CatalogCode", searchCriteria.CatalogCode, DbType.String);
                paramCollection.Add("@CostPeriod", searchCriteria.iCostPeriod.ToString(), DbType.String);
                paramCollection.Add("@RepType", searchCriteria.RepType, DbType.String);

                string sum1 = "", sum2 = "", sum3 = "", sum4 = "", sum5 = "", sum6 = "";

                switch (searchCriteria.iCostPeriod)
                {
                    case 1: sum1 = "p1"; sum2 = "pp12"; sum3 = "pp11"; sum4 = "pp10"; sum5 = "pp9"; sum6 = "pp8"; break;
                    case 2: sum1 = "p2"; sum2 = "p1"; sum3 = "pp12"; sum4 = "pp11"; sum5 = "pp10"; sum6 = "pp8"; break;
                    case 3: sum1 = "p3"; sum2 = "p2"; sum3 = "p1"; sum4 = "pp12"; sum5 = "pp11"; sum6 = "pp10"; break;
                    case 4: sum1 = "p4"; sum2 = "p3"; sum3 = "p2"; sum4 = "p1"; sum5 = "pp12"; sum6 = "pp11"; break;
                    case 5: sum1 = "p5"; sum2 = "p4"; sum3 = "p3"; sum4 = "p2"; sum5 = "p1"; sum6 = "pp12"; break;
                    case 6: sum1 = "p6"; sum2 = "p5"; sum3 = "p4"; sum4 = "p3"; sum5 = "p2"; sum6 = "p1"; break;
                    case 7: sum1 = "p7"; sum2 = "p6"; sum3 = "p5"; sum4 = "p4"; sum5 = "p3"; sum6 = "p2"; break;
                    case 8: sum1 = "p8"; sum2 = "p7"; sum3 = "p6"; sum4 = "p5"; sum5 = "p4"; sum6 = "p3"; break;
                    case 9: sum1 = "p9"; sum2 = "p8"; sum3 = "p7"; sum4 = "p6"; sum5 = "p5"; sum6 = "p4"; break;
                    case 10: sum1 = "p10"; sum2 = "p9"; sum3 = "p8"; sum4 = "p7"; sum5 = "p6"; sum6 = "p5"; break;
                    case 11: sum1 = "p11"; sum2 = "p10"; sum3 = "p9"; sum4 = "p8"; sum5 = "p7"; sum6 = "p6"; break;
                    case 12: sum1 = "p12"; sum2 = "p11"; sum3 = "p10"; sum4 = "p9"; sum5 = "p8"; sum6 = "p7"; break;
                    default: break;
                }
                paramCollection.Add("@Sum1", sum1, DbType.String);
                paramCollection.Add("@Sum2", sum2, DbType.String);
                paramCollection.Add("@Sum3", sum3, DbType.String);
                paramCollection.Add("@Sum4", sum4, DbType.String);
                paramCollection.Add("@Sum5", sum5, DbType.String);
                paramCollection.Add("@Sum6", sum6, DbType.String);
                //paramCollection.Add("@OrderBy", searchCriteria.SortField, DbType.String);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                
                drSalesInfo = this.DataAcessService.ExecuteQuery(SalesInfoSql["GetEndUserSalesInfoPieChart"], paramCollection);
                if (drSalesInfo != null && drSalesInfo.HasRows)
                {
                    int sum1Ordinal = drSalesInfo.GetOrdinal("Sum1");
                    int sum2Ordinal = drSalesInfo.GetOrdinal("Sum2");


                    int codeOrdinal = drSalesInfo.GetOrdinal("code");
                    int mdOrdinal = drSalesInfo.GetOrdinal("description");


                    while (drSalesInfo.Read())
                    {
                        BePieChartEntity salesInfo = BePieChartEntity.CreateObject();
                        if (searchCriteria.cDisplayOption.ToString() == "D")
                        {
                            if (!drSalesInfo.IsDBNull(sum1Ordinal)) salesInfo.Value = drSalesInfo.GetDouble(sum1Ordinal);
                        }
                        else
                        {
                            if (!drSalesInfo.IsDBNull(sum2Ordinal)) salesInfo.Value = drSalesInfo.GetDouble(sum2Ordinal);
                        }

                        if (!drSalesInfo.IsDBNull(mdOrdinal)) salesInfo.Name = drSalesInfo.GetString(mdOrdinal);

                        SalesInfoList.Add(salesInfo);
                    }
                }
                return SalesInfoList.OrderBy(c => c.Value).ToList();
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drSalesInfo != null)
                    drSalesInfo.Close();
            }
        }
        */
        /*
        public List<SalesInfoEntity> GetEndUserSalesInfoDetails(SalesInfoDetailSearchCriteriaEntity searchCriteria)
        {
            DbDataReader drSalesInfo = null;
            try
            {
                List<SalesInfoEntity> salesInfoList = new List<SalesInfoEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@DetailType", searchCriteria.DetailType, DbType.String);
                paramCollection.Add("@EndUserTable", searchCriteria.sEndUserTable, DbType.String);
                paramCollection.Add("@Brand", searchCriteria.Brand, DbType.String);
                paramCollection.Add("@State", searchCriteria.State, DbType.String);

                if (searchCriteria.RepCode != null && searchCriteria.RepCodes.Count > 0)
                {
                    string sWhrCls = "";
                    string testRepCode = (string)searchCriteria.RepCodes[0];

                    if (testRepCode.Length == 2)
                    {
                        sWhrCls = " AND r.rep_code IN (";

                        foreach (string repCode in searchCriteria.RepCodes)
                            sWhrCls += "'" + repCode + "',";

                        sWhrCls = sWhrCls.Substring(0, sWhrCls.Length - 1);
                        sWhrCls += ") ";
                    }
                    else
                    {
                        sWhrCls = " AND sh.state_code IN (";

                        foreach (string repCode in searchCriteria.RepCodes)
                            sWhrCls += "'" + repCode + "',";

                        sWhrCls = sWhrCls.Substring(0, sWhrCls.Length - 1);
                        sWhrCls += ") ";
                    }

                    paramCollection.Add("@AdditionalWhere", sWhrCls, DbType.String);
                }
                else
                {
                    paramCollection.Add("@AdditionalWhere", null, DbType.String);
                }


                paramCollection.Add("@CustomerCode", searchCriteria.CustomerCode, DbType.String);
                paramCollection.Add("@CatalogCode", searchCriteria.CatalogCode, DbType.String);
                paramCollection.Add("@CostPeriod", searchCriteria.iCostPeriod.ToString(), DbType.String);
                paramCollection.Add("@RepType", searchCriteria.RepType.ToString(), DbType.String);

                string sum1 = "", sum2 = "", sum3 = "", sum4 = "", sum5 = "", sum6 = "";

                switch (searchCriteria.iCostPeriod)
                {
                    case 1: sum1 = "p1"; sum2 = "pp12"; sum3 = "pp11"; sum4 = "pp10"; sum5 = "pp9"; sum6 = "pp8"; break;
                    case 2: sum1 = "p2"; sum2 = "p1"; sum3 = "pp12"; sum4 = "pp11"; sum5 = "pp10"; sum6 = "pp8"; break;
                    case 3: sum1 = "p3"; sum2 = "p2"; sum3 = "p1"; sum4 = "pp12"; sum5 = "pp11"; sum6 = "pp10"; break;
                    case 4: sum1 = "p4"; sum2 = "p3"; sum3 = "p2"; sum4 = "p1"; sum5 = "pp12"; sum6 = "pp11"; break;
                    case 5: sum1 = "p5"; sum2 = "p4"; sum3 = "p3"; sum4 = "p2"; sum5 = "p1"; sum6 = "pp12"; break;
                    case 6: sum1 = "p6"; sum2 = "p5"; sum3 = "p4"; sum4 = "p3"; sum5 = "p2"; sum6 = "p1"; break;
                    case 7: sum1 = "p7"; sum2 = "p6"; sum3 = "p5"; sum4 = "p4"; sum5 = "p3"; sum6 = "p2"; break;
                    case 8: sum1 = "p8"; sum2 = "p7"; sum3 = "p6"; sum4 = "p5"; sum5 = "p4"; sum6 = "p3"; break;
                    case 9: sum1 = "p9"; sum2 = "p8"; sum3 = "p7"; sum4 = "p6"; sum5 = "p5"; sum6 = "p4"; break;
                    case 10: sum1 = "p10"; sum2 = "p9"; sum3 = "p8"; sum4 = "p7"; sum5 = "p6"; sum6 = "p5"; break;
                    case 11: sum1 = "p11"; sum2 = "p10"; sum3 = "p9"; sum4 = "p8"; sum5 = "p7"; sum6 = "p6"; break;
                    case 12: sum1 = "p12"; sum2 = "p11"; sum3 = "p10"; sum4 = "p9"; sum5 = "p8"; sum6 = "p7"; break;
                    default: break;
                }
                paramCollection.Add("@Sum1", sum1, DbType.String);
                paramCollection.Add("@Sum2", sum2, DbType.String);
                paramCollection.Add("@Sum3", sum3, DbType.String);
                paramCollection.Add("@Sum4", sum4, DbType.String);
                paramCollection.Add("@Sum5", sum5, DbType.String);
                paramCollection.Add("@Sum6", sum6, DbType.String);
                paramCollection.Add("@OrderBy", searchCriteria.SortField, DbType.String);

                drSalesInfo = this.DataAcessService.ExecuteQuery(SalesInfoSql["GetEndUserSalesInfoDetails"], paramCollection);

                if (drSalesInfo != null && drSalesInfo.HasRows)
                {
                    int stateCodeOrdinal = drSalesInfo.GetOrdinal("state_code");
                    int dollar1Ordinal = drSalesInfo.GetOrdinal("dollar1");
                    int dollar2Ordinal = drSalesInfo.GetOrdinal("dollar2");
                    int dollar3Ordinal = drSalesInfo.GetOrdinal("dollar3");
                    int dollar4Ordinal = drSalesInfo.GetOrdinal("dollar4");
                    int dollar5Ordinal = drSalesInfo.GetOrdinal("dollar5");
                    int dollar6Ordinal = drSalesInfo.GetOrdinal("dollar6");
                    int tonnes1Ordinal = drSalesInfo.GetOrdinal("tonnes1");
                    int tonnes2Ordinal = drSalesInfo.GetOrdinal("tonnes2");
                    int tonnes3Ordinal = drSalesInfo.GetOrdinal("tonnes3");
                    int tonnes4Ordinal = drSalesInfo.GetOrdinal("tonnes4");
                    int tonnes5Ordinal = drSalesInfo.GetOrdinal("tonnes5");
                    int tonnes6Ordinal = drSalesInfo.GetOrdinal("tonnes6");

                    int Col1Ordinal = drSalesInfo.GetOrdinal("Col1");
                    int Col2Ordinal = drSalesInfo.GetOrdinal("Col2");
                    int Col3Ordinal = drSalesInfo.GetOrdinal("Col3");
                    int Col4Ordinal = drSalesInfo.GetOrdinal("Col4");
                    int Col5Ordinal = drSalesInfo.GetOrdinal("Col5");
                    int Col6Ordinal = drSalesInfo.GetOrdinal("Col6");
                    int Col7Ordinal = drSalesInfo.GetOrdinal("Col7");
                    int Col8Ordinal = drSalesInfo.GetOrdinal("Col8");
                    int Col9Ordinal = drSalesInfo.GetOrdinal("Col9");
                    int Col10Ordinal = drSalesInfo.GetOrdinal("Col10");

                    while (drSalesInfo.Read())
                    {
                        SalesInfoEntity salesInfoEntity = SalesInfoEntity.CreateObject();

                        if (!drSalesInfo.IsDBNull(stateCodeOrdinal)) salesInfoEntity.StateCode = drSalesInfo.GetString(stateCodeOrdinal);
                        if (!drSalesInfo.IsDBNull(dollar1Ordinal)) salesInfoEntity.Dollar1 = drSalesInfo.GetDouble(dollar1Ordinal);
                        if (!drSalesInfo.IsDBNull(dollar2Ordinal)) salesInfoEntity.Dollar2 = drSalesInfo.GetDouble(dollar2Ordinal);
                        if (!drSalesInfo.IsDBNull(dollar3Ordinal)) salesInfoEntity.Dollar3 = drSalesInfo.GetDouble(dollar3Ordinal);
                        if (!drSalesInfo.IsDBNull(dollar4Ordinal)) salesInfoEntity.Dollar4 = drSalesInfo.GetDouble(dollar4Ordinal);
                        if (!drSalesInfo.IsDBNull(dollar5Ordinal)) salesInfoEntity.Dollar5 = drSalesInfo.GetDouble(dollar5Ordinal);
                        if (!drSalesInfo.IsDBNull(dollar6Ordinal)) salesInfoEntity.Dollar6 = drSalesInfo.GetDouble(dollar6Ordinal);

                        if (!drSalesInfo.IsDBNull(tonnes1Ordinal)) salesInfoEntity.Tonnes1 = drSalesInfo.GetDouble(tonnes1Ordinal);
                        if (!drSalesInfo.IsDBNull(tonnes2Ordinal)) salesInfoEntity.Tonnes2 = drSalesInfo.GetDouble(tonnes2Ordinal);
                        if (!drSalesInfo.IsDBNull(tonnes3Ordinal)) salesInfoEntity.Tonnes3 = drSalesInfo.GetDouble(tonnes3Ordinal);
                        if (!drSalesInfo.IsDBNull(tonnes4Ordinal)) salesInfoEntity.Tonnes4 = drSalesInfo.GetDouble(tonnes4Ordinal);
                        if (!drSalesInfo.IsDBNull(tonnes5Ordinal)) salesInfoEntity.Tonnes5 = drSalesInfo.GetDouble(tonnes5Ordinal);
                        if (!drSalesInfo.IsDBNull(tonnes6Ordinal)) salesInfoEntity.Tonnes6 = drSalesInfo.GetDouble(tonnes6Ordinal);

                        if (!drSalesInfo.IsDBNull(Col1Ordinal)) salesInfoEntity.Col1 = drSalesInfo.GetString(Col1Ordinal);
                        if (!drSalesInfo.IsDBNull(Col2Ordinal)) salesInfoEntity.Col2 = drSalesInfo.GetString(Col2Ordinal);
                        if (!drSalesInfo.IsDBNull(Col3Ordinal)) salesInfoEntity.Col3 = drSalesInfo.GetString(Col3Ordinal);
                        if (!drSalesInfo.IsDBNull(Col4Ordinal)) salesInfoEntity.Col4 = drSalesInfo.GetString(Col4Ordinal);
                        if (!drSalesInfo.IsDBNull(Col5Ordinal)) salesInfoEntity.Col5 = drSalesInfo.GetString(Col5Ordinal);
                        if (!drSalesInfo.IsDBNull(Col6Ordinal)) salesInfoEntity.Col6 = drSalesInfo.GetString(Col6Ordinal);
                        if (!drSalesInfo.IsDBNull(Col7Ordinal)) salesInfoEntity.Col7 = drSalesInfo.GetString(Col7Ordinal);
                        if (!drSalesInfo.IsDBNull(Col8Ordinal)) salesInfoEntity.Col8 = drSalesInfo.GetString(Col8Ordinal);
                        if (!drSalesInfo.IsDBNull(Col9Ordinal)) salesInfoEntity.Col9 = drSalesInfo.GetString(Col9Ordinal);
                        if (!drSalesInfo.IsDBNull(Col10Ordinal)) salesInfoEntity.Col10 = drSalesInfo.GetString(Col10Ordinal);

                        salesInfoList.Add(salesInfoEntity);
                    }
                }

                return salesInfoList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drSalesInfo != null)
                    drSalesInfo.Close();
            }
        }
        */
        //public List<SalesTrendEntity> GetEndUserTrendData(ref List<string> lstCostPeriods, SalesInfoDetailSearchCriteriaEntity searchCriteria, string sCode, string history_type = "sales")
        //{
        //    DbDataReader drSalesInfo = null;
        //    try
        //    {
        //        List<SalesTrendEntity> salesTrendInfoList = new List<SalesTrendEntity>();

        //        SalesDAO salesDao=new SalesDAO();
        //        EndUserSalesEntity endUserSalesEntity = salesDao.GetCurrentCostPeriod();
        //        if (endUserSalesEntity != null)
        //        {

        //        }

        //        salesTrendInfoList = (from L in lstCostPeriods select new SalesTrendEntity { Period= L }).ToList<SalesTrendEntity>();

        //        DbInputParameterCollection paramCollection = new DbInputParameterCollection();
        //        paramCollection.Add("@DetailType", searchCriteria.DetailType, DbType.String);
        //        paramCollection.Add("@EndUserTable", searchCriteria.sEndUserTable, DbType.String);
        //        paramCollection.Add("@Brand", searchCriteria.Brand, DbType.String);
        //        paramCollection.Add("@State", searchCriteria.State, DbType.String);

        //        if (searchCriteria.RepCode != null && searchCriteria.RepCodes.Count > 0)
        //        {
        //            string sWhrCls = "";
        //            string testRepCode = (string)searchCriteria.RepCodes[0];

        //            if (testRepCode.Length == 2)
        //            {
        //                sWhrCls = " AND r.rep_code IN (";

        //                foreach (string repCode in searchCriteria.RepCodes)
        //                    sWhrCls += "'" + repCode + "',";

        //                sWhrCls = sWhrCls.Substring(0, sWhrCls.Length - 1);
        //                sWhrCls += ") ";
        //            }
        //            else
        //            {
        //                sWhrCls = " AND sh.state_code IN (";

        //                foreach (string repCode in searchCriteria.RepCodes)
        //                    sWhrCls += "'" + repCode + "',";

        //                sWhrCls = sWhrCls.Substring(0, sWhrCls.Length - 1);
        //                sWhrCls += ") ";
        //            }

        //            paramCollection.Add("@AdditionalWhere", sWhrCls, DbType.String);
        //        }
        //        else
        //        {
        //            paramCollection.Add("@AdditionalWhere", null, DbType.String);
        //        }


        //        paramCollection.Add("@CustomerCode", searchCriteria.CustomerCode, DbType.String);
        //        paramCollection.Add("@CatalogCode", searchCriteria.CatalogCode, DbType.String);
        //        paramCollection.Add("@CostPeriod", searchCriteria.iCostPeriod.ToString(), DbType.String);
        //        paramCollection.Add("@RepType", searchCriteria.RepType.ToString(), DbType.String);

        //        string sum1 = "", sum2 = "", sum3 = "", sum4 = "", sum5 = "", sum6 = "";

        //        switch (searchCriteria.iCostPeriod)
        //        {
        //            case 1: sum1 = "p1"; sum2 = "pp12"; sum3 = "pp11"; sum4 = "pp10"; sum5 = "pp9"; sum6 = "pp8"; break;
        //            case 2: sum1 = "p2"; sum2 = "p1"; sum3 = "pp12"; sum4 = "pp11"; sum5 = "pp10"; sum6 = "pp8"; break;
        //            case 3: sum1 = "p3"; sum2 = "p2"; sum3 = "p1"; sum4 = "pp12"; sum5 = "pp11"; sum6 = "pp10"; break;
        //            case 4: sum1 = "p4"; sum2 = "p3"; sum3 = "p2"; sum4 = "p1"; sum5 = "pp12"; sum6 = "pp11"; break;
        //            case 5: sum1 = "p5"; sum2 = "p4"; sum3 = "p3"; sum4 = "p2"; sum5 = "p1"; sum6 = "pp12"; break;
        //            case 6: sum1 = "p6"; sum2 = "p5"; sum3 = "p4"; sum4 = "p3"; sum5 = "p2"; sum6 = "p1"; break;
        //            case 7: sum1 = "p7"; sum2 = "p6"; sum3 = "p5"; sum4 = "p4"; sum5 = "p3"; sum6 = "p2"; break;
        //            case 8: sum1 = "p8"; sum2 = "p7"; sum3 = "p6"; sum4 = "p5"; sum5 = "p4"; sum6 = "p3"; break;
        //            case 9: sum1 = "p9"; sum2 = "p8"; sum3 = "p7"; sum4 = "p6"; sum5 = "p5"; sum6 = "p4"; break;
        //            case 10: sum1 = "p10"; sum2 = "p9"; sum3 = "p8"; sum4 = "p7"; sum5 = "p6"; sum6 = "p5"; break;
        //            case 11: sum1 = "p11"; sum2 = "p10"; sum3 = "p9"; sum4 = "p8"; sum5 = "p7"; sum6 = "p6"; break;
        //            case 12: sum1 = "p12"; sum2 = "p11"; sum3 = "p10"; sum4 = "p9"; sum5 = "p8"; sum6 = "p7"; break;
        //            default: break;
        //        }
        //        paramCollection.Add("@Sum1", sum1, DbType.String);
        //        paramCollection.Add("@Sum2", sum2, DbType.String);
        //        paramCollection.Add("@Sum3", sum3, DbType.String);
        //        paramCollection.Add("@Sum4", sum4, DbType.String);
        //        paramCollection.Add("@Sum5", sum5, DbType.String);
        //        paramCollection.Add("@Sum6", sum6, DbType.String);
        //        paramCollection.Add("@OrderBy", searchCriteria.SortField, DbType.String);

        //        drSalesInfo = this.DataAcessService.ExecuteQuery(SalesInfoSql["GetEndUserTrendData"], paramCollection);

        //        if (drSalesInfo != null && drSalesInfo.HasRows)
        //        {
        //            int stateCodeOrdinal = drSalesInfo.GetOrdinal("state_code");
        //            int dollar1Ordinal = drSalesInfo.GetOrdinal("dollar1");
        //            int dollar2Ordinal = drSalesInfo.GetOrdinal("dollar2");
        //            int dollar3Ordinal = drSalesInfo.GetOrdinal("dollar3");
        //            int dollar4Ordinal = drSalesInfo.GetOrdinal("dollar4");
        //            int dollar5Ordinal = drSalesInfo.GetOrdinal("dollar5");
        //            int dollar6Ordinal = drSalesInfo.GetOrdinal("dollar6");
        //            int tonnes1Ordinal = drSalesInfo.GetOrdinal("tonnes1");
        //            int tonnes2Ordinal = drSalesInfo.GetOrdinal("tonnes2");
        //            int tonnes3Ordinal = drSalesInfo.GetOrdinal("tonnes3");
        //            int tonnes4Ordinal = drSalesInfo.GetOrdinal("tonnes4");
        //            int tonnes5Ordinal = drSalesInfo.GetOrdinal("tonnes5");
        //            int tonnes6Ordinal = drSalesInfo.GetOrdinal("tonnes6");

        //            int Col1Ordinal = drSalesInfo.GetOrdinal("Col1");
        //            int Col2Ordinal = drSalesInfo.GetOrdinal("Col2");
        //            int Col3Ordinal = drSalesInfo.GetOrdinal("Col3");
        //            int Col4Ordinal = drSalesInfo.GetOrdinal("Col4");
        //            int Col5Ordinal = drSalesInfo.GetOrdinal("Col5");
        //            int Col6Ordinal = drSalesInfo.GetOrdinal("Col6");
        //            int Col7Ordinal = drSalesInfo.GetOrdinal("Col7");
        //            int Col8Ordinal = drSalesInfo.GetOrdinal("Col8");
        //            int Col9Ordinal = drSalesInfo.GetOrdinal("Col9");
        //            int Col10Ordinal = drSalesInfo.GetOrdinal("Col10");

        //            while (drSalesInfo.Read())
        //            {
        //                SalesTrendEntity salesInfoEntity = SalesTrendEntity.CreateObject();

        //                //if (!drSalesInfo.IsDBNull(stateCodeOrdinal)) salesInfoEntity.StateCode = drSalesInfo.GetString(stateCodeOrdinal);
        //                //if (!drSalesInfo.IsDBNull(dollar1Ordinal)) salesInfoEntity.Dollar1 = drSalesInfo.GetDouble(dollar1Ordinal);
        //                //if (!drSalesInfo.IsDBNull(dollar2Ordinal)) salesInfoEntity.Dollar2 = drSalesInfo.GetDouble(dollar2Ordinal);
        //                //if (!drSalesInfo.IsDBNull(dollar3Ordinal)) salesInfoEntity.Dollar3 = drSalesInfo.GetDouble(dollar3Ordinal);
        //                //if (!drSalesInfo.IsDBNull(dollar4Ordinal)) salesInfoEntity.Dollar4 = drSalesInfo.GetDouble(dollar4Ordinal);
        //                //if (!drSalesInfo.IsDBNull(dollar5Ordinal)) salesInfoEntity.Dollar5 = drSalesInfo.GetDouble(dollar5Ordinal);
        //                //if (!drSalesInfo.IsDBNull(dollar6Ordinal)) salesInfoEntity.Dollar6 = drSalesInfo.GetDouble(dollar6Ordinal);

        //                //if (!drSalesInfo.IsDBNull(tonnes1Ordinal)) salesInfoEntity.Tonnes1 = drSalesInfo.GetDouble(tonnes1Ordinal);
        //                //if (!drSalesInfo.IsDBNull(tonnes2Ordinal)) salesInfoEntity.Tonnes2 = drSalesInfo.GetDouble(tonnes2Ordinal);
        //                //if (!drSalesInfo.IsDBNull(tonnes3Ordinal)) salesInfoEntity.Tonnes3 = drSalesInfo.GetDouble(tonnes3Ordinal);
        //                //if (!drSalesInfo.IsDBNull(tonnes4Ordinal)) salesInfoEntity.Tonnes4 = drSalesInfo.GetDouble(tonnes4Ordinal);
        //                //if (!drSalesInfo.IsDBNull(tonnes5Ordinal)) salesInfoEntity.Tonnes5 = drSalesInfo.GetDouble(tonnes5Ordinal);
        //                //if (!drSalesInfo.IsDBNull(tonnes6Ordinal)) salesInfoEntity.Tonnes6 = drSalesInfo.GetDouble(tonnes6Ordinal);

        //                //if (!drSalesInfo.IsDBNull(Col1Ordinal)) salesInfoEntity.Col1 = drSalesInfo.GetString(Col1Ordinal);
        //                //if (!drSalesInfo.IsDBNull(Col2Ordinal)) salesInfoEntity.Col2 = drSalesInfo.GetString(Col2Ordinal);
        //                //if (!drSalesInfo.IsDBNull(Col3Ordinal)) salesInfoEntity.Col3 = drSalesInfo.GetString(Col3Ordinal);
        //                //if (!drSalesInfo.IsDBNull(Col4Ordinal)) salesInfoEntity.Col4 = drSalesInfo.GetString(Col4Ordinal);
        //                //if (!drSalesInfo.IsDBNull(Col5Ordinal)) salesInfoEntity.Col5 = drSalesInfo.GetString(Col5Ordinal);
        //                //if (!drSalesInfo.IsDBNull(Col6Ordinal)) salesInfoEntity.Col6 = drSalesInfo.GetString(Col6Ordinal);
        //                //if (!drSalesInfo.IsDBNull(Col7Ordinal)) salesInfoEntity.Col7 = drSalesInfo.GetString(Col7Ordinal);
        //                //if (!drSalesInfo.IsDBNull(Col8Ordinal)) salesInfoEntity.Col8 = drSalesInfo.GetString(Col8Ordinal);
        //                //if (!drSalesInfo.IsDBNull(Col9Ordinal)) salesInfoEntity.Col9 = drSalesInfo.GetString(Col9Ordinal);
        //                //if (!drSalesInfo.IsDBNull(Col10Ordinal)) salesInfoEntity.Col10 = drSalesInfo.GetString(Col10Ordinal);

        //                salesTrendInfoList.Add(salesInfoEntity);
        //            }
        //        }

        //        return salesTrendInfoList;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if (drSalesInfo != null)
        //            drSalesInfo.Close();
        //    }
        //}
    }
}
