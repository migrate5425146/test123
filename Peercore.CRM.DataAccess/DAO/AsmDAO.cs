﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;
using Peercore.CRM.Model;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public class AsmDAO : BaseDAO
    {
        public List<AsmModel> GetAllAsm(ArgsModel args)
        {
            AsmModel objModel = null;
            DataTable objDS = new DataTable();
            List<AsmModel> objList = new List<AsmModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllAsm";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    //objCommand.Parameters.AddWithValue("@AreaId", AreaId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new AsmModel();
                            if (item["asm_id"] != DBNull.Value) objModel.AsmId = Convert.ToInt32(item["asm_id"]);
                            if (item["area_id"] != DBNull.Value) objModel.AreaId = Convert.ToInt32(item["area_id"]);
                            if (item["area_status"] != DBNull.Value) objModel.AreaStatus = item["area_status"].ToString();
                            if (item["area_code"] != DBNull.Value) objModel.AreaCode = item["area_code"].ToString();
                            if (item["area_name"] != DBNull.Value) objModel.AreaName = item["area_name"].ToString();
                            if (item["asm_code"] != DBNull.Value) objModel.AsmCode = item["asm_code"].ToString();
                            if (item["asm_name"] != DBNull.Value) objModel.AsmName = item["asm_name"].ToString();
                            if (item["asm_user_name"] != DBNull.Value) objModel.AsmUserName = item["asm_user_name"].ToString();
                            if (item["asm_tel1"] != DBNull.Value) objModel.AsmTel1 = item["asm_tel1"].ToString();
                            if (item["asm_tel2"] != DBNull.Value) objModel.AsmTel2 = item["asm_tel2"].ToString();
                            if (item["asm_email"] != DBNull.Value) objModel.AsmEmail = item["asm_email"].ToString();
                            if (item["IsNotEMEIUser"] != DBNull.Value) objModel.IsNotIMEIuser = Convert.ToBoolean(item["IsNotEMEIUser"].ToString());
                            if (item["total_count"] != DBNull.Value) objModel.TotalCount = Convert.ToInt32(item["total_count"]);
                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        //public List<AsmModel> GetAllAreaByAreaCode(ArgsModel args, string AsmCode)
        //{
        //    AsmModel objModel = null;
        //    DataTable objDS = new DataTable();
        //    List<AsmModel> objList = new List<AsmModel>();

        //    try
        //    {
        //        using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
        //        {
        //            SqlCommand objCommand = new SqlCommand();
        //            objCommand.Connection = conn;
        //            objCommand.CommandType = CommandType.StoredProcedure;
        //            objCommand.CommandText = "GetAllAsmByAsmCode";

        //            objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
        //            objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
        //            objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
        //            objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
        //            objCommand.Parameters.AddWithValue("@ShowSQL", 0);
        //            objCommand.Parameters.AddWithValue("@AsmCode", AsmCode);

        //            SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
        //            objAdap.Fill(objDS);

        //            if (objDS.Rows.Count > 0)
        //            {
        //                foreach (DataRow item in objDS.Rows)
        //                {
        //                    objModel = new AsmModel();
        //                    if (item["asm_id"] != DBNull.Value) objModel.AsmId = Convert.ToInt32(item["asm_id"]);
        //                    if (item["area_id"] != DBNull.Value) objModel.AreaId = Convert.ToInt32(item["area_id"]);
        //                    if (item["asm_code"] != DBNull.Value) objModel.AsmCode = item["asm_code"].ToString();
        //                    if (item["asm_name"] != DBNull.Value) objModel.AsmName = item["asm_name"].ToString();
        //                    if (item["total_count"] != DBNull.Value) objModel.TotalCount = Convert.ToInt32(item["total_count"]);
        //                    objList.Add(objModel);
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw;
        //    }

        //    return objList;
        //}

        public List<AsmModel> GetAllAsmByAreaId(ArgsModel args, string AreaId)
        {
            AsmModel objModel = null;
            DataTable objDS = new DataTable();
            List<AsmModel> objList = new List<AsmModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllAsmByAreaId";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@AreaId", AreaId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            objModel = new AsmModel();
                            if (item["asm_id"] != DBNull.Value) objModel.AsmId = Convert.ToInt32(item["asm_id"]);
                            if (item["area_id"] != DBNull.Value) objModel.AreaId = Convert.ToInt32(item["area_id"]);
                            if (item["asm_code"] != DBNull.Value) objModel.AsmCode = item["asm_code"].ToString();
                            if (item["asm_name"] != DBNull.Value) objModel.AsmName = item["asm_name"].ToString();
                            if (item["total_count"] != DBNull.Value) objModel.TotalCount = Convert.ToInt32(item["total_count"]);
                            objList.Add(objModel);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public bool DeleteAsmFromAreaByAsmId(int asmId)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteAsmFromAreaByAsmId";

                    objCommand.Parameters.AddWithValue("@AsmId", asmId);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }

                    iNoRec = objCommand.ExecuteNonQuery();

                    if (iNoRec > 0)
                        successful = true;
                }
            }
            catch (Exception ex)
            {
                successful = false;
            }

            return successful;
        }

        public bool InsertUpdateAsm(string area_id,
                            string asm_id,
                            string asm_code,
                            string asm_name,
                            string asm_username,
                            string asm_tel1,
                            string asm_email,
                            string created_by)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertUpdateASM";

                    objCommand.Parameters.AddWithValue("@AreaId", area_id);
                    objCommand.Parameters.AddWithValue("@AsmId", asm_id);
                    objCommand.Parameters.AddWithValue("@AsmCode", asm_code);
                    objCommand.Parameters.AddWithValue("@AsmName", asm_name);
                    objCommand.Parameters.AddWithValue("@AsmUserName", asm_username);
                    objCommand.Parameters.AddWithValue("@AsmTel1", asm_tel1);
                    objCommand.Parameters.AddWithValue("@AsmEmail", asm_email);
                    objCommand.Parameters.AddWithValue("@CreatedBy", created_by);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool DeleteAsmByAsmId(string asmId)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteAsmByAsmId";

                    objCommand.Parameters.AddWithValue("@AsmId", asmId);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }

                    iNoRec = objCommand.ExecuteNonQuery();

                    if (iNoRec > 0)
                        successful = true;
                }
            }
            catch (Exception ex)
            {
                successful = false;
            }

            return successful;
        }

        public AsmModel GetAsmByAsmId(string AsmId)
        {
            AsmModel objModel = new AsmModel();
            DataTable objDS = new DataTable();
            List<AsmModel> objList = new List<AsmModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAsmByAsmId";

                    objCommand.Parameters.AddWithValue("@AsmId", AsmId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            if (item["asm_id"] != DBNull.Value) objModel.AsmId = Convert.ToInt32(item["asm_id"]);
                            if (item["area_id"] != DBNull.Value) objModel.AreaId = Convert.ToInt32(item["area_id"]);
                            if (item["asm_code"] != DBNull.Value) objModel.AsmCode = item["asm_code"].ToString();
                            if (item["asm_name"] != DBNull.Value) objModel.AsmName = item["asm_name"].ToString();
                            if (item["originator"] != DBNull.Value) objModel.AsmUserName = item["originator"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objModel;
        }
    }
}
