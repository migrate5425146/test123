﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class CustomerOpportunityDAO : BaseDAO
    {
        private DbSqlAdapter CustomerOpportunitySql { get; set; }

        private void RegisterSql()
        {
            this.CustomerOpportunitySql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.OpportunitySql.xml", ApplicationService.Instance.DbProvider);
        }

        public CustomerOpportunityDAO()
        {
            RegisterSql();
        }

        public CustomerOpportunityDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public CustomerOpportunityEntity CreateObject()
        {
            return CustomerOpportunityEntity.CreateObject();
        }

        public CustomerOpportunityEntity CreateObject(int entityId)
        {
            return CustomerOpportunityEntity.CreateObject(entityId);
        }

        public List<CustomerOpportunityEntity> GetOpportunitiesForEndUser(ArgsEntity args)
        {
            CustomerOpportunityEntity oOpportunity = null;
            DbDataReader idrOpportunity = null;

            try
            {
                List<CustomerOpportunityEntity> Opportunities = new List<CustomerOpportunityEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CustCode", args.CustomerCode, DbType.String);
                paramCollection.Add("@EnduserCode", args.EnduserCode, DbType.String);

                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);

                idrOpportunity = this.DataAcessService.ExecuteQuery(CustomerOpportunitySql["GetOpportunitiesForEndUser"], paramCollection);

                if (idrOpportunity != null)
                {
                    int opportunityIdOrdinal = idrOpportunity.GetOrdinal("opportunity_id");
                    int leadIdOrdinal = idrOpportunity.GetOrdinal("lead_id");
                    int originatorOrdinal = idrOpportunity.GetOrdinal("originator");
                    int nameOrdinal = idrOpportunity.GetOrdinal("name");
                    int closeDateOrdinal = idrOpportunity.GetOrdinal("close_date");
                    int pipelineStageIdOrdinal = idrOpportunity.GetOrdinal("pipeline_stage_id");
                    int probabilityOrdinal = idrOpportunity.GetOrdinal("probability");
                    int amountOrdinal = idrOpportunity.GetOrdinal("amount");
                    int unitsOrdinal = idrOpportunity.GetOrdinal("units");
                    int descriptionOrdinal = idrOpportunity.GetOrdinal("description");
                    int custcodeOrdinal = idrOpportunity.GetOrdinal("cust_code");

                    int createdByOrdinal = idrOpportunity.GetOrdinal("created_by");
                    int createdDateOrdinal = idrOpportunity.GetOrdinal("created_date");
                    int lastModifiedByOrdinal = idrOpportunity.GetOrdinal("last_modified_by");
                    int lastModifiedDateOrdinal = idrOpportunity.GetOrdinal("last_modified_date");

                    int pipelineStageOrdinal = idrOpportunity.GetOrdinal("pipeline_stage");
                    int totalCountOrdinal = idrOpportunity.GetOrdinal("total_count");
                    int enduserCodeOrdinal = idrOpportunity.GetOrdinal("enduser_code");

                    while (idrOpportunity.Read())
                    {
                        CustomerOpportunityEntity opportunity = CreateObject();

                        if (!idrOpportunity.IsDBNull(opportunityIdOrdinal)) opportunity.OpportunityID = idrOpportunity.GetInt32(opportunityIdOrdinal);
                        if (!idrOpportunity.IsDBNull(leadIdOrdinal)) opportunity.LeadID = idrOpportunity.GetInt32(leadIdOrdinal);
                        if (!idrOpportunity.IsDBNull(originatorOrdinal)) opportunity.Originator = idrOpportunity.GetString(originatorOrdinal);
                        if (!idrOpportunity.IsDBNull(nameOrdinal)) opportunity.Name = idrOpportunity.GetString(nameOrdinal);
                        if (!idrOpportunity.IsDBNull(closeDateOrdinal)) opportunity.CloseDate = idrOpportunity.GetDateTime(closeDateOrdinal).ToLocalTime();
                        if (!idrOpportunity.IsDBNull(pipelineStageIdOrdinal)) opportunity.Stage = idrOpportunity.GetInt32(pipelineStageIdOrdinal);
                        if (!idrOpportunity.IsDBNull(probabilityOrdinal)) opportunity.Probability = idrOpportunity.GetDouble(probabilityOrdinal);
                        if (!idrOpportunity.IsDBNull(amountOrdinal)) opportunity.Amount = idrOpportunity.GetDouble(amountOrdinal);
                        if (!idrOpportunity.IsDBNull(unitsOrdinal)) opportunity.Units = idrOpportunity.GetDouble(unitsOrdinal);
                        if (!idrOpportunity.IsDBNull(descriptionOrdinal)) opportunity.Description = idrOpportunity.GetString(descriptionOrdinal);
                        if (!idrOpportunity.IsDBNull(custcodeOrdinal)) opportunity.CustCode = idrOpportunity.GetString(custcodeOrdinal);

                        if (!idrOpportunity.IsDBNull(createdByOrdinal)) opportunity.CreatedBy = idrOpportunity.GetString(createdByOrdinal);
                        if (!idrOpportunity.IsDBNull(createdDateOrdinal)) opportunity.CreatedDate = idrOpportunity.GetDateTime(createdDateOrdinal);
                        if (!idrOpportunity.IsDBNull(lastModifiedByOrdinal)) opportunity.LastModifiedBy = idrOpportunity.GetString(lastModifiedByOrdinal);
                        if (!idrOpportunity.IsDBNull(lastModifiedDateOrdinal)) opportunity.LastModifiedDate = idrOpportunity.GetDateTime(lastModifiedDateOrdinal);

                        if (!idrOpportunity.IsDBNull(pipelineStageOrdinal)) opportunity.PipelineStage = idrOpportunity.GetString(pipelineStageOrdinal);
                        if (!idrOpportunity.IsDBNull(totalCountOrdinal)) opportunity.TotalCount = idrOpportunity.GetInt32(totalCountOrdinal);
                        if (!idrOpportunity.IsDBNull(enduserCodeOrdinal)) opportunity.EndUserCode = idrOpportunity.GetString(enduserCodeOrdinal);

                        Opportunities.Add(opportunity);
                    }
                }

                return Opportunities;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrOpportunity != null)
                    idrOpportunity.Close();
            }
        }

        public List<CustomerOpportunityEntity> GetContactOpportunities(int pipelineStageID, ArgsEntity args, string repGroups)
        {
            CustomerOpportunityEntity oOpportunity;
            DbDataReader idrOpportunity = null;

            string sWhereCls1 = "";
            string sWhereCls2 = "";

            int i = 0;

            try
            {
                List<CustomerOpportunityEntity> Opportunities = new List<CustomerOpportunityEntity>();

                /*if (args.Originator != "")
                {
                    sWhereCls1 = args.ChildOriginators;
                    sWhereCls1 = "( " + sWhereCls1.Replace("originator", "O.originator");

                    sWhereCls2 = sWhereCls1;
                    
                    if (repGroups != "")
                    {
                        repGroups = repGroups.Remove(repGroups.Length - 1, 1);  // Remove the Last Comma
                        sWhereCls1 = sWhereCls2 += " OR O.rep_group_id IN (" + repGroups + ")";
                    }
                    sWhereCls1 = sWhereCls2 += " )";
                }
                */

                /*if (pipelineStageID > 0)
                {
                    sWhereCls1 = sWhereCls1 != "" ? sWhereCls1 + " AND O.pipeline_stage_id = " + pipelineStageID.ToString() : " O.pipeline_stage_id = " + pipelineStageID.ToString();
                    sWhereCls2 = sWhereCls2 != "" ? sWhereCls2 + " AND O.pipeline_stage_id = " + pipelineStageID.ToString() : " O.pipeline_stage_id = " + pipelineStageID.ToString();
                }
                */

                /*if (args.DtEndDate != null)
                {
                    sWhereCls1 += " close_date >= '" + Convert.ToDateTime(dtmCloseDate).ToString("dd-MMM-yyyy HH:mm:ss")
                        + "' AND close_date <= '" + Convert.ToDateTime(dtmCloseDate).AddDays(7).ToString("dd-MMM-yyyy HH:mm:ss") + "' ";
                    
                    sWhereCls2 += " close_date >= '" + Convert.ToDateTime(dtmCloseDate).ToString("dd-MMM-yyyy HH:mm:ss")
                        + "' AND close_date <= '" + Convert.ToDateTime(dtmCloseDate).AddDays(7).ToString("dd-MMM-yyyy HH:mm:ss") + "' ";
                }
                
                if (!string.IsNullOrWhiteSpace(sWhereCls1))
                    sWhereCls1 = sWhereCls1.Insert(0, " AND ");
                
                string sdditionalParams = string.Empty;
                if (!string.IsNullOrWhiteSpace(args.AdditionalParams))
                    sdditionalParams = "and " + args.AdditionalParams;
                 */

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@DefaultDeptID", args.DefaultDepartmentId, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@Originator", args.Originator, DbType.String);
                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                paramCollection.Add("@RepGroups", repGroups, DbType.String);
                paramCollection.Add("@PipelineStageID", pipelineStageID.ToString(), DbType.String);
                paramCollection.Add("@StartDate", args.SStartDate, DbType.String);
                paramCollection.Add("@EndDate", args.DtEndDate, DbType.String);

                idrOpportunity = this.DataAcessService.ExecuteQuery(CustomerOpportunitySql["GetContactOpportunities"], paramCollection);

                if (idrOpportunity != null)
                {
                    while (idrOpportunity.Read())
                    {
                        oOpportunity = CreateObject();

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("opportunity_id")))
                            oOpportunity.OpportunityID = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("opportunity_id"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("lead_id")))
                            oOpportunity.LeadID = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("lead_id"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("Originator")))
                            oOpportunity.Originator = idrOpportunity.GetString(idrOpportunity.GetOrdinal("Originator"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("name")))
                            oOpportunity.Name = idrOpportunity.GetString(idrOpportunity.GetOrdinal("name"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("close_date")))
                            oOpportunity.CloseDate = idrOpportunity.GetDateTime(idrOpportunity.GetOrdinal("close_date")).ToLocalTime();
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("pipeline_stage_id")))
                            oOpportunity.Stage = idrOpportunity.GetInt32(idrOpportunity.GetOrdinal("pipeline_stage_id"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("probability")))
                            oOpportunity.Probability = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("probability"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("amount")))
                            oOpportunity.Amount = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("amount"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("units")))
                            oOpportunity.Units = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("units"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("ContactName")))
                            oOpportunity.LeadName = idrOpportunity.GetString(idrOpportunity.GetOrdinal("ContactName"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("description")))
                            oOpportunity.Description = idrOpportunity.GetString(idrOpportunity.GetOrdinal("description"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("business")))
                            oOpportunity.Business = idrOpportunity.GetString(idrOpportunity.GetOrdinal("business"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("industry")))
                            oOpportunity.Industry = idrOpportunity.GetString(idrOpportunity.GetOrdinal("industry"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("address")))
                            oOpportunity.Address = idrOpportunity.GetString(idrOpportunity.GetOrdinal("address"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("city")))
                            oOpportunity.City = idrOpportunity.GetString(idrOpportunity.GetOrdinal("city"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("state")))
                            oOpportunity.State = idrOpportunity.GetString(idrOpportunity.GetOrdinal("state"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("postcode")))
                            oOpportunity.PostCode = idrOpportunity.GetString(idrOpportunity.GetOrdinal("postcode"));
                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("lead_stage")))
                            oOpportunity.LeadStage = idrOpportunity.GetString(idrOpportunity.GetOrdinal("lead_stage"));

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("industry_description")))
                            oOpportunity.IndustryDescription = idrOpportunity.GetString(idrOpportunity.GetOrdinal("industry_description"));

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("cust_code")))
                            oOpportunity.CustCode = idrOpportunity.GetString(idrOpportunity.GetOrdinal("cust_code"));

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("enduser_code")))
                            oOpportunity.EndUserCode = idrOpportunity.GetString(idrOpportunity.GetOrdinal("enduser_code"));

                        if (!idrOpportunity.IsDBNull(idrOpportunity.GetOrdinal("tonnes")))
                            oOpportunity.Tonnes = idrOpportunity.GetDouble(idrOpportunity.GetOrdinal("tonnes"));

                        Opportunities.Add(oOpportunity);
                    }
                }

                return Opportunities;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrOpportunity != null)
                    idrOpportunity.Close();
            }
        }
    }
}
