﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;
using System.Data;

namespace Peercore.CRM.DataAccess.DAO
{
    public class HolidayDAO:BaseDAO
    {
        private DbSqlAdapter CommonSql { get; set; }

        private void RegisterSql()
        {
            this.CommonSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.CommonSql.xml", ApplicationService.Instance.DbProvider);
        }

        public HolidayDAO()
        {
            RegisterSql();
        }

        public HolidayEntity CreateObject()
        {
            return HolidayEntity.CreateObject();
        }

        //public HolidayEntity CreateObject(int entityId)
        //{
        //    return HolidayEntity.CreateObject(entityId);
        //}

        public bool InsertHoliday(HolidayEntity holidayentity)
        {
            bool saveSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Date", holidayentity.Date.ToShortDateString(), DbType.Date);
                paramCollection.Add("@Description", holidayentity.Title, DbType.String);
                paramCollection.Add("@HolidayType", holidayentity.HolidayType, DbType.String);
                paramCollection.Add("@CreatedBy", holidayentity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CommonSql["InsertHolidays"], paramCollection);

                if (iNoRec > 0)
                    saveSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return saveSuccessful;
        }

        public bool UpdateHoliday(HolidayEntity holidayentity)
        {
            bool saveSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@HolidayId", holidayentity.HolidayId, DbType.Int32);
                paramCollection.Add("@Date", holidayentity.Date.ToShortDateString(), DbType.Date);
                paramCollection.Add("@Description", holidayentity.Title, DbType.String);
                paramCollection.Add("@HolidayType", holidayentity.HolidayType, DbType.String);
                paramCollection.Add("@CreatedBy", holidayentity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CommonSql["UpdateHoliday"], paramCollection);

                if (iNoRec > 0)
                    saveSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return saveSuccessful;
        }

        public bool DeleteHoliday(int holidayId)
        {
            bool saveSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@HolidayId", holidayId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CommonSql["DeleteHoliday"], paramCollection);

                if (iNoRec > 0)
                    saveSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return saveSuccessful;
        }

        public List<HolidayEntity>  GetAllHolidays(ArgsEntity args)
        {
            List<HolidayEntity> holidaylist = new List<HolidayEntity>();
            DbDataReader dataReader = null;

            DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            paramCollection.Add("@StartDate", args.SStartDate, DbType.Date);
            paramCollection.Add("@EndDate", args.SEndDate, DbType.Date);
            paramCollection.Add("@Originator", args.Originator, DbType.String);

            dataReader = this.DataAcessService.ExecuteQuery(CommonSql["GetAllHolidays"],paramCollection);

            try
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    int idOrdinal = dataReader.GetOrdinal("id");
                    int dateOrdinal = dataReader.GetOrdinal("date");
                    int descriptionOrdinal = dataReader.GetOrdinal("description");
                    int holidaytypeOrdinal = dataReader.GetOrdinal("holiday_type");
                    int holidaytypeDescOrdinal = dataReader.GetOrdinal("type_desctiption");
                    int colorOrdinal = dataReader.GetOrdinal("color");
                    
                    while (dataReader.Read())
                    {
                        HolidayEntity holiday = CreateObject();

                        if (!dataReader.IsDBNull(idOrdinal)) holiday.HolidayId = dataReader.GetInt32(idOrdinal);
                        if (!dataReader.IsDBNull(dateOrdinal)) holiday.Date = dataReader.GetDateTime(dateOrdinal).AddHours(1);
                        if (!dataReader.IsDBNull(dateOrdinal)) holiday.EndDate = dataReader.GetDateTime(dateOrdinal).AddHours(12);
                        if (!dataReader.IsDBNull(descriptionOrdinal)) holiday.Title = dataReader.GetString(descriptionOrdinal);
                        if (!dataReader.IsDBNull(holidaytypeOrdinal)) holiday.HolidayType = dataReader.GetString(holidaytypeOrdinal);
                        if (!dataReader.IsDBNull(holidaytypeDescOrdinal)) holiday.TypeDescription = dataReader.GetString(holidaytypeDescOrdinal);
                        if (!dataReader.IsDBNull(colorOrdinal)) holiday.TypeColor = dataReader.GetString(colorOrdinal);
                        //holiday.Title = holiday.TypeDescription + " - " + holiday.Description;
                        holiday.IsAllDay = true;

                        holidaylist.Add(holiday);
                    }
                }
                return holidaylist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public HolidayEntity GetHolidayTypeByCode(string holidayType)
        {
            DbDataReader dataReader = null;
            HolidayEntity holiday = CreateObject();

            DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            paramCollection.Add("@HolidayType", holidayType, DbType.String);
            dataReader = this.DataAcessService.ExecuteQuery(CommonSql["GetHolidayTypeByCode"], paramCollection);

            try
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    int typeOrdinal = dataReader.GetOrdinal("holiday_type");
                    int descriptionOrdinal = dataReader.GetOrdinal("description");
                    int colorOrdinal = dataReader.GetOrdinal("color");

                    while (dataReader.Read())
                    {

                        if (!dataReader.IsDBNull(typeOrdinal)) holiday.HolidayType = dataReader.GetString(typeOrdinal);
                        if (!dataReader.IsDBNull(descriptionOrdinal)) holiday.TypeDescription = dataReader.GetString(descriptionOrdinal);
                        if (!dataReader.IsDBNull(colorOrdinal)) holiday.TypeColor = dataReader.GetString(colorOrdinal);
                    }
                }
                return holiday;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public List<HolidayEntity> GetAllHolidaytypes()
        {
            List<HolidayEntity> holidaylist = new List<HolidayEntity>();
            DbDataReader dataReader = null;

            dataReader = this.DataAcessService.ExecuteQuery(CommonSql["GetAllHolidayTypes"]);

            try
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    int idOrdinal = dataReader.GetOrdinal("id");
                    int holidaytypeOrdinal = dataReader.GetOrdinal("holiday_type");
                    int holidaytypeDescOrdinal = dataReader.GetOrdinal("type_description");
                    int colorOrdinal = dataReader.GetOrdinal("color");

                    while (dataReader.Read())
                    {
                        HolidayEntity holiday = CreateObject();

                        if (!dataReader.IsDBNull(idOrdinal)) holiday.HolidayId = dataReader.GetInt32(idOrdinal);
                        if (!dataReader.IsDBNull(holidaytypeOrdinal)) holiday.HolidayType = dataReader.GetString(holidaytypeOrdinal);
                        if (!dataReader.IsDBNull(holidaytypeDescOrdinal)) holiday.TypeDescription = dataReader.GetString(holidaytypeDescOrdinal);
                        if (!dataReader.IsDBNull(colorOrdinal)) holiday.TypeColor = dataReader.GetString(colorOrdinal);

                        holidaylist.Add(holiday);
                    }
                }
                return holidaylist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }        

        public bool IsHolidayExistsForTheDay(string createdBy, DateTime effDate)
        {
            DbDataReader drHolidays = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Date", effDate.ToString("yyyy-MM-dd"), DbType.String);
                paramCollection.Add("@CreatedBy", createdBy, DbType.String);

                drHolidays = this.DataAcessService.ExecuteQuery(CommonSql["HolidayExistsForTheDay"], paramCollection);

                if (drHolidays != null && drHolidays.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drHolidays != null)
                    drHolidays.Close();
            }
        }

        public bool UpdateHolidayLastChangedDate(HolidayEntity holidayentity)
        {
            bool saveSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@HolidayId", holidayentity.HolidayId, DbType.Int32);
                paramCollection.Add("@LastChangedDate", holidayentity.Date.ToShortDateString(), DbType.Date);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CommonSql["UpdateHolidayLastChangedDate"], paramCollection);

                if (iNoRec > 0)
                    saveSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return saveSuccessful;
        }

        public bool UpdateHolidayDate(HolidayEntity holidayentity)
        {
            bool saveSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@HolidayId", holidayentity.HolidayId, DbType.Int32);
                paramCollection.Add("@Date", holidayentity.Date.ToShortDateString(), DbType.Date);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CommonSql["UpdateHolidayDate"], paramCollection);

                if (iNoRec > 0)
                    saveSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return saveSuccessful;
        }

        public int InsertHolidayReturnId(HolidayEntity holidayentity)
        {
            bool saveSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Date", holidayentity.Date.ToShortDateString(), DbType.Date);
                paramCollection.Add("@Description", holidayentity.Title, DbType.String);
                paramCollection.Add("@HolidayType", holidayentity.HolidayType, DbType.String);
                paramCollection.Add("@CreatedBy", holidayentity.CreatedBy, DbType.String);
                paramCollection.Add("@ID", null, DbType.Int32, ParameterDirection.Output);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CommonSql["InsertHolidayReturnId"], paramCollection, true, "@ID");

                if (iNoRec > 0)
                    saveSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return iNoRec;
        }
    }
}
