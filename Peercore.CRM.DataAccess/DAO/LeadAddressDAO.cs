﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class LeadAddressDAO : BaseDAO
    {
        private DbSqlAdapter LeadAddressSql { get; set; }

        private void RegisterSql()
        {
            this.LeadAddressSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.LeadAddressSql.xml", ApplicationService.Instance.DbProvider);
        }

        public LeadAddressDAO()
        {
            RegisterSql();
        }

        public LeadAddressDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public LeadAddressEntity CreateObject()
        {
            return LeadAddressEntity.CreateObject();
        }

        public LeadAddressEntity CreateObject(int entityId)
        {
            return LeadAddressEntity.CreateObject(entityId);
        }

        public List<LeadAddressEntity> GetCustomerAddresses(int leadId)
        {
            DbDataReader idrContactDetails = null;

            try
            {
                List<LeadAddressEntity> leadAddresses = new List<LeadAddressEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@LeadId", leadId, DbType.Int32);

                idrContactDetails = this.DataAcessService.ExecuteQuery(LeadAddressSql["GetLeadAddress"], paramCollection);

                if (idrContactDetails != null && idrContactDetails.HasRows)
                {
                    int leadAddressIdOrdinal = idrContactDetails.GetOrdinal("lead_address_id");
                    int leadIdOrdinal = idrContactDetails.GetOrdinal("lead_id");
                    int address1Ordinal = idrContactDetails.GetOrdinal("address1");
                    int address2Ordinal = idrContactDetails.GetOrdinal("address2");
                    int cityOrdinal = idrContactDetails.GetOrdinal("city");
                    int stateOrdinal = idrContactDetails.GetOrdinal("state");
                    int postcodeOrdinal = idrContactDetails.GetOrdinal("postcode");
                    int countryOrdinal = idrContactDetails.GetOrdinal("country");

                    int assignToOrdinal = idrContactDetails.GetOrdinal("assign_to");
                    int assigneeNoOrdinal = idrContactDetails.GetOrdinal("assignee_no");
                    int addressTypeOrdinal = idrContactDetails.GetOrdinal("address_type");
                    int nameOrdinal = idrContactDetails.GetOrdinal("name");
                    int areaCodeOrdinal = idrContactDetails.GetOrdinal("area_code");
                    int telephoneOrdinal = idrContactDetails.GetOrdinal("telephone");
                    int faxOrdinal = idrContactDetails.GetOrdinal("fax");
                    int contactOrdinal = idrContactDetails.GetOrdinal("contact");

                    while (idrContactDetails.Read())
                    {
                        LeadAddressEntity leadAddress = LeadAddressEntity.CreateObject();

                        if (!idrContactDetails.IsDBNull(leadAddressIdOrdinal)) leadAddress.LeadAddressID = idrContactDetails.GetInt32(leadAddressIdOrdinal);
                        if (!idrContactDetails.IsDBNull(leadIdOrdinal)) leadAddress.LeadID = idrContactDetails.GetInt32(leadIdOrdinal);
                        if (!idrContactDetails.IsDBNull(address1Ordinal)) leadAddress.Address1 = idrContactDetails.GetString(address1Ordinal);
                        if (!idrContactDetails.IsDBNull(address2Ordinal)) leadAddress.Address2 = idrContactDetails.GetString(address2Ordinal);
                        if (!idrContactDetails.IsDBNull(cityOrdinal)) leadAddress.City = idrContactDetails.GetString(cityOrdinal);
                        if (!idrContactDetails.IsDBNull(stateOrdinal)) leadAddress.State = idrContactDetails.GetString(stateOrdinal);
                        if (!idrContactDetails.IsDBNull(postcodeOrdinal)) leadAddress.PostCode = idrContactDetails.GetString(postcodeOrdinal);
                        if (!idrContactDetails.IsDBNull(countryOrdinal)) leadAddress.Country = idrContactDetails.GetString(countryOrdinal);

                        if (!idrContactDetails.IsDBNull(assignToOrdinal)) leadAddress.AssignedTo = idrContactDetails.GetString(assignToOrdinal);
                        if (!idrContactDetails.IsDBNull(assigneeNoOrdinal)) leadAddress.AssigneeNo = idrContactDetails.GetInt32(assigneeNoOrdinal);
                        if (!idrContactDetails.IsDBNull(addressTypeOrdinal)) leadAddress.AddressType = idrContactDetails.GetString(addressTypeOrdinal);
                        if (!idrContactDetails.IsDBNull(nameOrdinal)) leadAddress.Name = idrContactDetails.GetString(nameOrdinal);
                        if (!idrContactDetails.IsDBNull(areaCodeOrdinal)) leadAddress.AreaCode = idrContactDetails.GetString(areaCodeOrdinal);
                        if (!idrContactDetails.IsDBNull(telephoneOrdinal)) leadAddress.Telephone = idrContactDetails.GetString(telephoneOrdinal);
                        if (!idrContactDetails.IsDBNull(faxOrdinal)) leadAddress.Fax = idrContactDetails.GetString(faxOrdinal);
                        if (!idrContactDetails.IsDBNull(contactOrdinal)) leadAddress.Contact = idrContactDetails.GetString(contactOrdinal);

                        leadAddresses.Add(leadAddress);
                    }
                }

                return leadAddresses;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrContactDetails != null)
                    idrContactDetails.Close();
            }
        }

        public LeadAddressEntity GetLeadTableAddress(int leadId)
        {
            DbDataReader idrContactDetails = null;
            LeadAddressEntity leadAddress = LeadAddressEntity.CreateObject();

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@LeadId", leadId, DbType.Int32);

                idrContactDetails = this.DataAcessService.ExecuteQuery(LeadAddressSql["GetLeadTableAddress"], paramCollection);

                if (idrContactDetails != null && idrContactDetails.HasRows)
                {
                    int leadIdOrdinal = idrContactDetails.GetOrdinal("lead_id");
                    int addressOrdinal = idrContactDetails.GetOrdinal("address");
                    int cityOrdinal = idrContactDetails.GetOrdinal("city");
                    int stateOrdinal = idrContactDetails.GetOrdinal("state");
                    int postcodeOrdinal = idrContactDetails.GetOrdinal("postcode");

                    while (idrContactDetails.Read())
                    {
                        if (!idrContactDetails.IsDBNull(leadIdOrdinal)) leadAddress.LeadID = idrContactDetails.GetInt32(leadIdOrdinal);
                        if (!idrContactDetails.IsDBNull(addressOrdinal)) leadAddress.Address1 = idrContactDetails.GetString(addressOrdinal);
                        if (!idrContactDetails.IsDBNull(cityOrdinal)) leadAddress.City = idrContactDetails.GetString(cityOrdinal);
                        if (!idrContactDetails.IsDBNull(stateOrdinal)) leadAddress.State = idrContactDetails.GetString(stateOrdinal);
                        if (!idrContactDetails.IsDBNull(postcodeOrdinal)) leadAddress.PostCode = idrContactDetails.GetString(postcodeOrdinal);
                    }
                }
                return leadAddress;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrContactDetails != null)
                    idrContactDetails.Close();
            }
        }

        public bool UpdateLeadAddress(LeadAddressEntity leadAddress)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@LeadId", leadAddress.LeadID, DbType.Int32);
                paramCollection.Add("@Address1", ValidateString(leadAddress.Address1), DbType.String);
                paramCollection.Add("@Address2", ValidateString(leadAddress.Address2), DbType.String);
                paramCollection.Add("@City", ValidateString(leadAddress.City), DbType.String);
                paramCollection.Add("@Country", ValidateString(leadAddress.Country), DbType.String);
                paramCollection.Add("@Postcode", ValidateString(leadAddress.PostCode), DbType.String);

                paramCollection.Add("@State", ValidateString(leadAddress.State), DbType.String);
                paramCollection.Add("@LastModifiedBy", ValidateString(leadAddress.LastModifiedBy), DbType.String);
                paramCollection.Add("@LastModifiedDate", leadAddress.CreatedDate, DbType.DateTime);
                paramCollection.Add("@Name", ValidateString(leadAddress.Name), DbType.String);
                paramCollection.Add("@AreaCode", ValidateString(leadAddress.AreaCode), DbType.String);

                paramCollection.Add("@Telephone", ValidateString(leadAddress.Telephone), DbType.String);
                paramCollection.Add("@Fax", ValidateString(leadAddress.Fax), DbType.String);
                paramCollection.Add("@Contact", ValidateString(leadAddress.Contact), DbType.String);
                paramCollection.Add("@AssignTo", leadAddress.AssignedTo, DbType.String);
                paramCollection.Add("@AddressType", ValidateString(leadAddress.AddressType), DbType.String);
                paramCollection.Add("@LeadAddressId", leadAddress.LeadAddressID, DbType.Int32);
                paramCollection.Add("@AssigneeNo", leadAddress.AssigneeNo, DbType.Int32);
                iNoRec = this.DataAcessService.ExecuteNonQuery(LeadAddressSql["UpdateLeadAddress"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return UpdateSuccessful;
        }

        public bool InsertLeadAddress(LeadAddressEntity leadAddress, ref int leadAddressId)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                leadAddress.LeadAddressID = Convert.ToInt32(DataAcessService.GetOneValue(LeadAddressSql["GetNextLeadAddressID"]));
                // Get Next ID

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@LeadId", leadAddress.LeadID, DbType.Int32);                
                leadAddress.AssigneeNo = Convert.ToInt32(DataAcessService.GetOneValue(LeadAddressSql["GetNextAssigneeNo"], paramCollection));
                string dddd = leadAddress.CreatedDate.ToString("dd-MMM-yyyy HH:mm:ss");

                paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@LeadId", leadAddress.LeadID, DbType.Int32);
                paramCollection.Add("@Address1", ValidateString(leadAddress.Address1), DbType.String);
                paramCollection.Add("@Address2", ValidateString(leadAddress.Address2), DbType.String);
                paramCollection.Add("@City", ValidateString(leadAddress.City), DbType.String);
                paramCollection.Add("@Country", ValidateString(leadAddress.Country), DbType.String);
                paramCollection.Add("@Postcode", ValidateString(leadAddress.PostCode), DbType.String);

                paramCollection.Add("@State", ValidateString(leadAddress.State), DbType.String);
                paramCollection.Add("@CreatedBy", ValidateString(leadAddress.CreatedBy), DbType.String);
                paramCollection.Add("@CreatedDate", leadAddress.CreatedDate.ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);
                paramCollection.Add("@Name", ValidateString(leadAddress.Name), DbType.String);
                paramCollection.Add("@AreaCode", ValidateString(leadAddress.AreaCode), DbType.String);

                paramCollection.Add("@Telephone", ValidateString(leadAddress.Telephone), DbType.String);
                paramCollection.Add("@Fax", ValidateString(leadAddress.Fax), DbType.String);
                paramCollection.Add("@Contact", ValidateString(leadAddress.Contact), DbType.String);
                paramCollection.Add("@AssignTo", leadAddress.AssignedTo, DbType.String);
                paramCollection.Add("@AddressType", ValidateString(leadAddress.AddressType), DbType.String);
                paramCollection.Add("@LeadAddressId", leadAddress.LeadAddressID, DbType.Int32);

                paramCollection.Add("@AssigneeNo", leadAddress.AssigneeNo, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(LeadAddressSql["InsertLeadAddress"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return UpdateSuccessful;
        }

        public bool TransferAddressToCustomer(List<LeadAddressEntity> customerAddressList)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;
            try
            {
                if (customerAddressList.Count > 0)
                {
                    string CustCode = customerAddressList[0].CustCode;

                    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                    paramCollection.Add("@AssigneeCode", customerAddressList[0].CustCode, DbType.String);
                    paramCollection.Add("@AssignedTo", customerAddressList[0].AssignedTo, DbType.String);
                    paramCollection.Add("@AddressType", customerAddressList[0].AddressType, DbType.String); 

                    int AssigneeNo = Convert.ToInt32(DataAcessService.GetOneValue(LeadAddressSql["GetNextCustomerAssigneeNo"],paramCollection));

                    foreach (LeadAddressEntity customerAddress in customerAddressList)
                    {
                        paramCollection = new DbInputParameterCollection();
                        paramCollection.Add("@AssignTo", ValidateString(customerAddress.AssignedTo), DbType.String);
                        paramCollection.Add("@AssigneeNo", AssigneeNo, DbType.String);
                        paramCollection.Add("@AssigneeCode", ValidateString(customerAddress.CustCode), DbType.String);
                        paramCollection.Add("@AddressType", ValidateString(customerAddress.AddressType), DbType.String);
                        paramCollection.Add("@Name", ValidateString(customerAddress.Name), DbType.String);
                        paramCollection.Add("@Address1", ValidateString(customerAddress.Address1), DbType.String);

                        paramCollection.Add("@Address2", ValidateString(customerAddress.Address2), DbType.String);
                        paramCollection.Add("@City", ValidateString(customerAddress.City), DbType.String);
                        paramCollection.Add("@State", ValidateString(customerAddress.State), DbType.String);
                        paramCollection.Add("@Postcode", ValidateString(customerAddress.PostCode), DbType.String);
                        paramCollection.Add("@AreaCode", ValidateString(customerAddress.AreaCode), DbType.String);

                        paramCollection.Add("@Telephone", customerAddress.Telephone, DbType.String);
                        paramCollection.Add("@Fax", customerAddress.Fax, DbType.String);
                        paramCollection.Add("@Contact", ValidateString(customerAddress.Contact), DbType.String);
                        paramCollection.Add("@Country", ValidateString(customerAddress.Country), DbType.String);
                        paramCollection.Add("@ModifiedBy", customerAddress.CreatedBy, DbType.String);

                        iNoRec = this.DataAcessService.ExecuteNonQuery(LeadAddressSql["TransformAddressToCustomer"], paramCollection);

                        if (iNoRec > 0)
                            UpdateSuccessful = true;

                        if (!UpdateSuccessful)
                            break;

                        AssigneeNo += 1;
                    }

                }
                return UpdateSuccessful;

            }
            catch
            {
                throw;
            }
        }

    }
}
