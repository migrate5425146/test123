﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;

namespace Peercore.CRM.DataAccess.DAO
{
    public class OptionLevelDAO : BaseDAO
    {
        private DbSqlAdapter OptionLevelSql { get; set; }

        private void RegisterSql()
        {
            this.OptionLevelSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.OptionLevelSql.xml", ApplicationService.Instance.DbProvider);
        }

        public OptionLevelDAO()
        {
            RegisterSql();
        }

        public OptionLevelDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public OptionLevelEntity CreateObject()
        {
            return OptionLevelEntity.CreateObject();
        }

        public OptionLevelEntity CreateObject(int entityId)
        {
            return OptionLevelEntity.CreateObject(entityId);
        }

        public bool InsertOptionLevel(out int optionLevelId, OptionLevelEntity optionLevelEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drOption = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                //paramCollection.Add("@OptionLevelId", optionLevelEntity.OptionLevelId, DbType.Int32);
                paramCollection.Add("@SchemeDetailsId", optionLevelEntity.SchemeDetailId, DbType.Int32);
                paramCollection.Add("@ProductCatagoryId", optionLevelEntity.ProductCategoryId, DbType.Int32);
                paramCollection.Add("@ProductPackingId", optionLevelEntity.ProductPackingId, DbType.Int32);
                paramCollection.Add("@ProductId", optionLevelEntity.ProductId, DbType.Int32);
                paramCollection.Add("@BrandId", optionLevelEntity.BrandId, DbType.Int32);
                //paramCollection.Add("@ItemTypeId", optionLevelEntity.ItemTypeId, DbType.Int32);
                paramCollection.Add("@MinQty", optionLevelEntity.MinQuantity, DbType.Double);
                paramCollection.Add("@minValue", optionLevelEntity.MinValue, DbType.Double);
                paramCollection.Add("@MaxQty", optionLevelEntity.MaxQuantity, DbType.Double);
                paramCollection.Add("@MaxValue", optionLevelEntity.MaxValue, DbType.Double);
                paramCollection.Add("@CreatedBy", optionLevelEntity.CreatedBy, DbType.String);
                
                paramCollection.Add("@Ishvp", optionLevelEntity.IsHvp, DbType.Boolean);
                paramCollection.Add("@OperatorId", optionLevelEntity.OperatorId, DbType.Int32);
                paramCollection.Add("@IsBill", optionLevelEntity.IsBill, DbType.Boolean);
                paramCollection.Add("@CatlogId", optionLevelEntity.CatalogId, DbType.Int32);
                paramCollection.Add("@IsActive", true, DbType.Boolean);
                paramCollection.Add("@IsFor", optionLevelEntity.IsForEvery, DbType.Boolean);

                paramCollection.Add("@LineCount", optionLevelEntity.LineCount, DbType.Double);

                paramCollection.Add("@OptionLevelId", null, DbType.Int32, ParameterDirection.Output);
                optionLevelId = this.DataAcessService.ExecuteNonQuery(OptionLevelSql["InsertOptionLevel"], paramCollection, true, "@OptionLevelId");
                if (optionLevelId > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drOption != null)
                    drOption.Close();
            }
        }

        public bool UpdateOptionLevel(OptionLevelEntity optionLevelEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drOption = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@SchemeDetailsId", optionLevelEntity.SchemeDetailId, DbType.Int32);
                paramCollection.Add("@ProductCatagoryId", optionLevelEntity.ProductCategoryId, DbType.Int32);
                paramCollection.Add("@ProductPackingId", optionLevelEntity.ProductPackingId, DbType.Int32);
                paramCollection.Add("@ProductId", optionLevelEntity.ProductId, DbType.Int32);
                paramCollection.Add("@BrandId", optionLevelEntity.BrandId, DbType.Int32);
                //paramCollection.Add("@ItemTypeId", optionLevelEntity.ItemTypeId, DbType.Int32);
                paramCollection.Add("@MinQty", optionLevelEntity.MinQuantity, DbType.Double);
                paramCollection.Add("@minValue", optionLevelEntity.MinValue, DbType.Double);
                paramCollection.Add("@MaxQty", optionLevelEntity.MaxQuantity, DbType.Double);
                paramCollection.Add("@MaxValue", optionLevelEntity.MaxValue, DbType.Double);
                paramCollection.Add("@LastModifiedBy", optionLevelEntity.CreatedBy, DbType.String);
                paramCollection.Add("@OptionLevelId", optionLevelEntity.OptionLevelId, DbType.Int32);
                paramCollection.Add("@IsFor", optionLevelEntity.IsForEvery, DbType.Boolean);

                iNoRec = this.DataAcessService.ExecuteNonQuery(OptionLevelSql["UpdateOptionLevel"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drOption != null)
                    drOption.Close();
            }
        }

        public List<OptionLevelEntity> GetOptionLevelByDetailsId(int schemeDetailsId)
        {
            DbDataReader drOptionLevel = null;
            try
            {
                List<OptionLevelEntity> OptionLevelCollection = new List<OptionLevelEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@DetailsId", schemeDetailsId, DbType.Int32);

                drOptionLevel = this.DataAcessService.ExecuteQuery(OptionLevelSql["GetOptionLevelByDetailsId"], paramCollection);
                if (drOptionLevel != null && drOptionLevel.HasRows)
                {
                    int optionLevelIdOrdinal = drOptionLevel.GetOrdinal("option_level_id");
                    int schemeDetailsIdOrdinal = drOptionLevel.GetOrdinal("scheme_details_id");
                    int productCatagoryIdOrdinal = drOptionLevel.GetOrdinal("product_catagory_id");
                    int productPackingIdOrdinal = drOptionLevel.GetOrdinal("product_packing_id");
                    int productIdOrdinal = drOptionLevel.GetOrdinal("product_id");
                    int brandIdOrdinal = drOptionLevel.GetOrdinal("brand_id");
                    int catalogIdOrdinal = drOptionLevel.GetOrdinal("catalog_id");
                    int minQtyOrdinal = drOptionLevel.GetOrdinal("min_qty");
                    int minValueOrdinal = drOptionLevel.GetOrdinal("min_value");
                    int maxQtyOrdinal = drOptionLevel.GetOrdinal("max_qty");
                    int maxValueOrdinal = drOptionLevel.GetOrdinal("max_value");

                    int ishvpOrdinal = drOptionLevel.GetOrdinal("ishvp");
                    int productCategoryOrdinal = drOptionLevel.GetOrdinal("product_category_name");
                    int brandNameOrdinal = drOptionLevel.GetOrdinal("brand_name");
                    int packingNameOrdinal = drOptionLevel.GetOrdinal("packing_name");
                    int catalogNameOrdinal = drOptionLevel.GetOrdinal("catalog_name");
                    int isBillOrdinal = drOptionLevel.GetOrdinal("is_bill");
                    int operatorIdOrdinal = drOptionLevel.GetOrdinal("operator_id");
                    int isForEveryOrdinal = drOptionLevel.GetOrdinal("is_for_every");

                    int lineCountOrdinal = drOptionLevel.GetOrdinal("line_count");

                    while (drOptionLevel.Read())
                    {
                        OptionLevelEntity optionLevelEntity = OptionLevelEntity.CreateObject();

                        if (!drOptionLevel.IsDBNull(optionLevelIdOrdinal)) optionLevelEntity.OptionLevelId = drOptionLevel.GetInt32(optionLevelIdOrdinal);
                        if (!drOptionLevel.IsDBNull(schemeDetailsIdOrdinal)) optionLevelEntity.SchemeDetailId = drOptionLevel.GetInt32(schemeDetailsIdOrdinal);
                        if (!drOptionLevel.IsDBNull(productCatagoryIdOrdinal)) optionLevelEntity.ProductCategoryId = drOptionLevel.GetInt32(productCatagoryIdOrdinal);
                        if (!drOptionLevel.IsDBNull(productPackingIdOrdinal)) optionLevelEntity.ProductPackingId = drOptionLevel.GetInt32(productPackingIdOrdinal);
                        if (!drOptionLevel.IsDBNull(brandIdOrdinal)) optionLevelEntity.BrandId = drOptionLevel.GetInt32(brandIdOrdinal);
                        if (!drOptionLevel.IsDBNull(catalogIdOrdinal)) optionLevelEntity.CatalogId = drOptionLevel.GetInt32(catalogIdOrdinal);
                        if (!drOptionLevel.IsDBNull(productIdOrdinal)) optionLevelEntity.ProductId = drOptionLevel.GetInt32(productIdOrdinal);
                        if (!drOptionLevel.IsDBNull(minQtyOrdinal)) optionLevelEntity.MinQuantity = drOptionLevel.GetDouble(minQtyOrdinal);
                        if (!drOptionLevel.IsDBNull(minValueOrdinal)) optionLevelEntity.MinValue = drOptionLevel.GetDouble(minValueOrdinal);
                        if (!drOptionLevel.IsDBNull(maxQtyOrdinal)) optionLevelEntity.MaxQuantity = drOptionLevel.GetDouble(maxQtyOrdinal);
                        if (!drOptionLevel.IsDBNull(maxValueOrdinal)) optionLevelEntity.MaxValue = drOptionLevel.GetDouble(maxValueOrdinal);

                        if (!drOptionLevel.IsDBNull(ishvpOrdinal)) optionLevelEntity.IsHvp = drOptionLevel.GetBoolean(ishvpOrdinal);
                        if (!drOptionLevel.IsDBNull(productCategoryOrdinal)) optionLevelEntity.ProductCatagoryName = drOptionLevel.GetString(productCategoryOrdinal);
                        if (!drOptionLevel.IsDBNull(brandNameOrdinal)) optionLevelEntity.BrandName = drOptionLevel.GetString(brandNameOrdinal);
                        if (!drOptionLevel.IsDBNull(packingNameOrdinal)) optionLevelEntity.ProductPackingName = drOptionLevel.GetString(packingNameOrdinal);
                        if (!drOptionLevel.IsDBNull(catalogNameOrdinal)) optionLevelEntity.CatlogName = drOptionLevel.GetString(catalogNameOrdinal);

                        if (!drOptionLevel.IsDBNull(isBillOrdinal)) optionLevelEntity.IsBill = drOptionLevel.GetBoolean(isBillOrdinal);
                        if (!drOptionLevel.IsDBNull(operatorIdOrdinal)) optionLevelEntity.OperatorId = drOptionLevel.GetInt32(operatorIdOrdinal);

                        if (!drOptionLevel.IsDBNull(isForEveryOrdinal)) optionLevelEntity.IsForEvery = drOptionLevel.GetBoolean(isForEveryOrdinal);

                        if (!drOptionLevel.IsDBNull(lineCountOrdinal)) optionLevelEntity.LineCount = drOptionLevel.GetDouble(lineCountOrdinal);

                        OptionLevelCollection.Add(optionLevelEntity);
                    }
                }

                return OptionLevelCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOptionLevel != null)
                    drOptionLevel.Close();
            }
        }

        public bool UpdateOptionLevelInActive(int schemeHeaderId)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@SchemeHeaderId", schemeHeaderId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(OptionLevelSql["UpdateOptionLevelInActive"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return UpdateSuccessful;
        }

        public List<OptionLevelEntity> GetOptionLevelByDivisionId(int divisionId)
        {
            DbDataReader drOptionLevel = null;
            try
            {
                List<OptionLevelEntity> OptionLevelCollection = new List<OptionLevelEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@DivisionId", divisionId, DbType.Int32);

                drOptionLevel = this.DataAcessService.ExecuteQuery(OptionLevelSql["GetOptionLevelByDivisionId"], paramCollection);
                if (drOptionLevel != null && drOptionLevel.HasRows)
                {
                    int optionLevelIdOrdinal = drOptionLevel.GetOrdinal("option_level_id");
                    int schemeDetailsIdOrdinal = drOptionLevel.GetOrdinal("scheme_details_id");
                    int productCatagoryIdOrdinal = drOptionLevel.GetOrdinal("product_catagory_id");
                    int productPackingIdOrdinal = drOptionLevel.GetOrdinal("product_packing_id");
                    int productIdOrdinal = drOptionLevel.GetOrdinal("product_id");
                    int brandIdOrdinal = drOptionLevel.GetOrdinal("brand_id");
                    int catalogIdOrdinal = drOptionLevel.GetOrdinal("catalog_id");
                    int minQtyOrdinal = drOptionLevel.GetOrdinal("min_qty");
                    int minValueOrdinal = drOptionLevel.GetOrdinal("min_value");
                    int maxQtyOrdinal = drOptionLevel.GetOrdinal("max_qty");
                    int maxValueOrdinal = drOptionLevel.GetOrdinal("max_value");

                    int ishvpOrdinal = drOptionLevel.GetOrdinal("ishvp");
                    //int productCategoryOrdinal = drOptionLevel.GetOrdinal("product_category_name");
                    //int brandNameOrdinal = drOptionLevel.GetOrdinal("brand_name");
                    //int packingNameOrdinal = drOptionLevel.GetOrdinal("packing_name");
                    //int catalogNameOrdinal = drOptionLevel.GetOrdinal("catalog_name");
                    int isBillOrdinal = drOptionLevel.GetOrdinal("is_bill");
                    int operatorIdOrdinal = drOptionLevel.GetOrdinal("operator_id");
                    int isForEveryOrdinal = drOptionLevel.GetOrdinal("is_for_every");

                    while (drOptionLevel.Read())
                    {
                        OptionLevelEntity optionLevelEntity = OptionLevelEntity.CreateObject();

                        if (!drOptionLevel.IsDBNull(optionLevelIdOrdinal)) optionLevelEntity.OptionLevelId = drOptionLevel.GetInt32(optionLevelIdOrdinal);
                        if (!drOptionLevel.IsDBNull(schemeDetailsIdOrdinal)) optionLevelEntity.SchemeDetailId = drOptionLevel.GetInt32(schemeDetailsIdOrdinal);
                        if (!drOptionLevel.IsDBNull(productCatagoryIdOrdinal)) optionLevelEntity.ProductCategoryId = drOptionLevel.GetInt32(productCatagoryIdOrdinal);
                        if (!drOptionLevel.IsDBNull(productPackingIdOrdinal)) optionLevelEntity.ProductPackingId = drOptionLevel.GetInt32(productPackingIdOrdinal);
                        if (!drOptionLevel.IsDBNull(brandIdOrdinal)) optionLevelEntity.BrandId = drOptionLevel.GetInt32(brandIdOrdinal);
                        if (!drOptionLevel.IsDBNull(catalogIdOrdinal)) optionLevelEntity.CatalogId = drOptionLevel.GetInt32(catalogIdOrdinal);
                        if (!drOptionLevel.IsDBNull(productIdOrdinal)) optionLevelEntity.ProductId = drOptionLevel.GetInt32(productIdOrdinal);
                        if (!drOptionLevel.IsDBNull(minQtyOrdinal)) optionLevelEntity.MinQuantity = drOptionLevel.GetDouble(minQtyOrdinal);
                        if (!drOptionLevel.IsDBNull(minValueOrdinal)) optionLevelEntity.MinValue = drOptionLevel.GetDouble(minValueOrdinal);
                        if (!drOptionLevel.IsDBNull(maxQtyOrdinal)) optionLevelEntity.MaxQuantity = drOptionLevel.GetDouble(maxQtyOrdinal);
                        if (!drOptionLevel.IsDBNull(maxValueOrdinal)) optionLevelEntity.MaxValue = drOptionLevel.GetDouble(maxValueOrdinal);

                        if (!drOptionLevel.IsDBNull(ishvpOrdinal)) optionLevelEntity.IsHvp = drOptionLevel.GetBoolean(ishvpOrdinal);
                        //if (!drOptionLevel.IsDBNull(productCategoryOrdinal)) optionLevelEntity.ProductCatagoryName = drOptionLevel.GetString(productCategoryOrdinal);
                        //if (!drOptionLevel.IsDBNull(brandNameOrdinal)) optionLevelEntity.BrandName = drOptionLevel.GetString(brandNameOrdinal);
                        //if (!drOptionLevel.IsDBNull(packingNameOrdinal)) optionLevelEntity.ProductPackingName = drOptionLevel.GetString(packingNameOrdinal);
                        //if (!drOptionLevel.IsDBNull(catalogNameOrdinal)) optionLevelEntity.CatlogName = drOptionLevel.GetString(catalogNameOrdinal);

                        if (!drOptionLevel.IsDBNull(isBillOrdinal)) optionLevelEntity.IsBill = drOptionLevel.GetBoolean(isBillOrdinal);
                        if (!drOptionLevel.IsDBNull(operatorIdOrdinal)) optionLevelEntity.OperatorId = drOptionLevel.GetInt32(operatorIdOrdinal);

                        if (!drOptionLevel.IsDBNull(isForEveryOrdinal)) optionLevelEntity.IsForEvery = drOptionLevel.GetBoolean(isForEveryOrdinal);

                        OptionLevelCollection.Add(optionLevelEntity);
                    }
                }

                return OptionLevelCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOptionLevel != null)
                    drOptionLevel.Close();
            }
        }
    }
}
