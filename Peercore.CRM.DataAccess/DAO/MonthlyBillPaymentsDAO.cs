﻿using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Spreadsheet;
using Peercore.CRM.Model;
using Peercore.CRM.Shared;
using Peercore.DataAccess.Common.Parameters;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Peercore.CRM.DataAccess.DAO
{
    public class MonthlyBillPaymentsDAO : BaseDAO
    {
        #region - Monthly Bill Payments Upload -

        public List<PaymentModel> GetAllPayments(ArgsModel args)
        {
            DataTable objDS = new DataTable();
            List<PaymentModel> paymentList = new List<PaymentModel>();
            PaymentModel paymentEntity = null;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spGetAllPayments";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        paymentEntity = new PaymentModel();

                        if (item["payment_id"] != DBNull.Value) paymentEntity.PaymentId = Convert.ToInt32(item["payment_id"]);
                        if (item["rep_code"] != DBNull.Value) paymentEntity.RepCode = item["rep_code"].ToString();
                        if (item["payment_year"] != DBNull.Value) paymentEntity.PayYear = Convert.ToInt32(item["payment_year"]);
                        if (item["payment_month"] != DBNull.Value) paymentEntity.PayMonth = Convert.ToInt32(item["payment_month"]);
                        if (item["payment_month"] != DBNull.Value) paymentEntity.PayMonthName = CultureInfo.CurrentCulture.DateTimeFormat.GetMonthName(Convert.ToInt32(item["payment_month"])); ;
                        if (item["payment_date"] != DBNull.Value) paymentEntity.PaymentDate = Convert.ToDateTime(item["payment_date"]);
                        if (item["mobile_bill_no"] != DBNull.Value) paymentEntity.BillNo = Convert.ToInt32(item["mobile_bill_no"]);
                        if (item["bill_amount"] != DBNull.Value) paymentEntity.BillAmount = Convert.ToDecimal(item["bill_amount"]);
                        if (item["allowance"] != DBNull.Value) paymentEntity.Allowance = Convert.ToDecimal(item["allowance"]);
                        if (item["remark"] != DBNull.Value) paymentEntity.Remark = item["remark"].ToString();

                        //if (item["created_by"] != DBNull.Value) paymentEntity.CreatedBy = item["created_by"].ToString();
                        if (item["total_count"] != DBNull.Value) paymentEntity.TotalCount = Convert.ToInt32(item["total_count"]);
                        paymentList.Add(paymentEntity);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }

            return paymentList;
        }

        public bool DeletePayment(int paymentId, string originator)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spDeletePayment";

                    objCommand.Parameters.AddWithValue("@paymentId", paymentId);
                    objCommand.Parameters.AddWithValue("@modified_by", originator);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        public bool SavePayments(PaymentModel paymentDetail)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spSavePayment"; //No procedure available

                    objCommand.Parameters.AddWithValue("@rep_code", paymentDetail.RepCode);
                    objCommand.Parameters.AddWithValue("@payment_month", paymentDetail.PayMonth);
                    objCommand.Parameters.AddWithValue("@payment_date", paymentDetail.PaymentDate);
                    objCommand.Parameters.AddWithValue("@mobile_bill_no", paymentDetail.BillNo);
                    objCommand.Parameters.AddWithValue("@bill_amount", paymentDetail.BillAmount);
                    objCommand.Parameters.AddWithValue("@allowance", paymentDetail.Allowance);
                    objCommand.Parameters.AddWithValue("@status", paymentDetail.Status);
                    objCommand.Parameters.AddWithValue("@remark", paymentDetail.Remark);
                    objCommand.Parameters.AddWithValue("@created_by", paymentDetail.CreatedBy);


                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        public bool UpdatePayment(PaymentModel paymentDetail)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "spUpdatePayment";

                    objCommand.Parameters.AddWithValue("@payment_id", paymentDetail.PaymentId);

                    int monthNo = DateTime.ParseExact(paymentDetail.PayMonthName, "MMMM", CultureInfo.InvariantCulture).Month;

                    objCommand.Parameters.AddWithValue("@rep_code", paymentDetail.RepCode);
                    objCommand.Parameters.AddWithValue("@payment_year", paymentDetail.PayYear);
                    objCommand.Parameters.AddWithValue("@payment_month", monthNo);
                    objCommand.Parameters.AddWithValue("@payment_date", paymentDetail.PaymentDate);
                    objCommand.Parameters.AddWithValue("@mobile_bill_no", paymentDetail.BillNo);
                    objCommand.Parameters.AddWithValue("@bill_amount", paymentDetail.BillAmount);
                    objCommand.Parameters.AddWithValue("@allowance", paymentDetail.Allowance);
                    //objCommand.Parameters.AddWithValue("@status", paymentDetail.Status);
                    objCommand.Parameters.AddWithValue("@remark", paymentDetail.Remark);
                    objCommand.Parameters.AddWithValue("@modified_by", paymentDetail.LastModifiedBy);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }

            return retStatus;

        }

        public bool UploadPayments(string FilePath, string UserName)
        {
            try
            {
                bool status = false;
                DataTable dt = new DataTable();
                DataTable objDS = new DataTable();

                using (SpreadsheetDocument spreadSheetDocument = SpreadsheetDocument.Open(FilePath, false))
                {
                    WorkbookPart workbookPart = spreadSheetDocument.WorkbookPart;
                    IEnumerable<Sheet> sheets = spreadSheetDocument.WorkbookPart.Workbook.GetFirstChild<Sheets>().Elements<Sheet>();
                    string relationshipId = sheets.First().Id.Value;
                    WorksheetPart worksheetPart = (WorksheetPart)spreadSheetDocument.WorkbookPart.GetPartById(relationshipId);
                    Worksheet workSheet = worksheetPart.Worksheet;
                    SheetData sheetData = workSheet.GetFirstChild<SheetData>();
                    IEnumerable<Row> rows = sheetData.Descendants<Row>();

                    foreach (Cell cell in rows.ElementAt(0))
                    {
                        dt.Columns.Add(GetCellValue(spreadSheetDocument, cell).Trim());
                    }


                    foreach (Row row in rows)
                    {
                        DataRow tempRow = dt.NewRow();
                        try
                        {
                            int columnIndex = 0;
                            foreach (Cell cell in row.Descendants<Cell>())
                            {
                                int cellColumnIndex = (int)GetColumnIndexFromName(GetColumnName(cell.CellReference));
                                cellColumnIndex--;
                                if (columnIndex < cellColumnIndex)
                                {
                                    do
                                    {
                                        tempRow[columnIndex] = "";
                                        columnIndex++;
                                    }
                                    while (columnIndex < cellColumnIndex);
                                }
                                tempRow[columnIndex] = GetCellValue(spreadSheetDocument, cell);

                                columnIndex++;
                            }

                            if (tempRow[0].ToString() != "")
                                dt.Rows.Add(tempRow);
                        }
                        catch (Exception)
                        {
                            throw;
                        }
                    }
                }

                dt.Rows.RemoveAt(0);
                dt.AcceptChanges();
                // Add columns
                dt.Columns.Add(new DataColumn("UserID", typeof(System.String)));
                dt.Columns.Add(new DataColumn("MonthId", typeof(System.Int16)));
                dt.Columns.Add(new DataColumn("RepID", typeof(System.String)));
                //dt.Columns.Add(new DataColumn("PayDate", typeof(System.String)));

                // Assign values
                foreach (DataRow dr in dt.Rows)
                {
                    dr["UserID"] = UserName;
                    dr["MonthId"] = DateTime.ParseExact(dr["PaymentMonth"].ToString(), "MMMM", CultureInfo.InvariantCulture).Month;
                    dr["RepID"] = dr["RepCode"].ToString().PadLeft(5, '0'); //String.Format("{0:00000}", dr["RepCode"]); 

                    string b = dr["PaymentDate"].ToString();
                    double d = double.Parse(b);
                    DateTime conv = DateTime.FromOADate(d);
                    dr["PaymentDate"] = conv;

                    //DateTime xx = DateTime.ParseExact(dr["PaymentDate"].ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture);  //DateTime.Parse(dr["PaymentDate"].ToString());
                }

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                if (dt.Rows.Count > 0)
                {
                    using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                    {
                        SqlCommand objCommand = new SqlCommand();
                        objCommand.Connection = conn;
                        objCommand.CommandType = CommandType.StoredProcedure;
                        objCommand.CommandText = "spUploadMonthlyPayments";

                        objCommand.Parameters.AddWithValue("@tblPatments", dt);

                        //Get list of data which alredy exist in the table
                        SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                        objAdap.Fill(objDS);
                        WriteMonthlyPaymentsLog(objDS);
                    }
                    status = true;
                }

                return status;
            }
            catch (Exception)
            {

                throw;
            }
        }

        public static void WriteMonthlyPaymentsLog(DataTable strLog)
        {
            try
            {
                string logFilePath = @"C:\Logs\Log-" + System.DateTime.Today.ToString("MM-dd-yyyy") + "." + "txt";
                FileInfo logFileInfo = new FileInfo(logFilePath);
                DirectoryInfo logDirInfo = new DirectoryInfo(logFileInfo.DirectoryName);
                if (!logDirInfo.Exists) logDirInfo.Create();
                File.WriteAllText(logFilePath, String.Empty);

                using (FileStream fileStream = new FileStream(logFilePath, FileMode.Append))
                {
                    using (StreamWriter log = new StreamWriter(fileStream))
                    {
                        log.Write("Log Entry : monthly Payments Detail ");
                        log.WriteLine("{0} {1}", DateTime.Now.ToLongTimeString(),
                            DateTime.Now.ToLongDateString());
                        log.WriteLine("-------------------------------");
                        log.WriteLine(strLog.Rows.Count + " Records Exists.");
                        foreach (DataRow row in strLog.Rows)
                        {
                            string repCode = row["RepCode"].ToString();
                            string payMonth = row["PaymentMonth"].ToString();
                            string mobileBillNo = row["MobileBillNo"].ToString();
                            string billAmount = row["BillAmount"].ToString();
                            string allowance = row["Allowance"].ToString();
                            log.WriteLine("{0} | {1} | {2} | {3} | {4}", repCode, payMonth, mobileBillNo, billAmount, allowance);
                        }
                        log.WriteLine("-----------END--------------");

                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }


        #endregion

        #region - Common Methods -

        //--using openXml
        public static string GetCellValue(SpreadsheetDocument document, Cell cell)
        {
            SharedStringTablePart stringTablePart = document.WorkbookPart.SharedStringTablePart;
            string value = cell.CellValue.InnerXml;
            if (cell.CellValue == null)
            {
                return "";
            }
            if (cell.DataType != null && cell.DataType.Value == CellValues.SharedString)
            {
                return stringTablePart.SharedStringTable.ChildElements[Int32.Parse(value)].InnerText;
            }
            else
            {
                return value;
            }
        }

        public static int? GetColumnIndexFromName(string columnName)
        {
            //return columnIndex;
            string name = columnName;
            int number = 0;
            int pow = 1;
            for (int i = name.Length - 1; i >= 0; i--)
            {
                number += (name[i] - 'A' + 1) * pow;
                pow *= 26;
            }
            return number;
        }

        public static string GetColumnName(string cellReference)
        {
            // Create a regular expression to match the column name portion of the cell name.
            Regex regex = new Regex("[A-Za-z]+");
            Match match = regex.Match(cellReference);
            return match.Value;
        }

        #endregion
    }
}
