﻿using Peercore.CRM.Model;
using Peercore.CRM.Shared;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.Workflow.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.DataAccess.DAO
{
    public class OrderDetailsDAO : BaseDAO
    {
        private DbSqlAdapter OrderDetailSql { get; set; }

        private void RegisterSql()
        {
            this.OrderDetailSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.OrderHeaderSql.xml", ApplicationService.Instance.DbProvider);
        }

        public OrderDetailsDAO()
        {
            RegisterSql();
        }

        public OrderDetailsDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public bool DeleteOrderDetailsByOrderId(int orderId)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drBrand = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@OrderId", orderId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(OrderDetailSql["DeleteOrderDetailsByOrderId"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }

        public bool InsertOrderDetails(OrderLineItem orderDetailLine)
        {
            bool successful = false; 
            int iNoRec = 0;
            DbDataReader drBrand = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@OrderId", orderDetailLine.OrderId, DbType.Int32);
                paramCollection.Add("@Productid", orderDetailLine.ProductID, DbType.Int32);
                paramCollection.Add("@Quantity", orderDetailLine.Qty, DbType.Double);

                paramCollection.Add("@Discount", orderDetailLine.Discount, DbType.Double);
                paramCollection.Add("@DiscountValue", orderDetailLine.DiscountValue, DbType.String);
                paramCollection.Add("@SubTotal", orderDetailLine.Total, DbType.Double);
                
                paramCollection.Add("@ItemPrice", orderDetailLine.ItemPrice, DbType.String);
                paramCollection.Add("@Status", 'A', DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(OrderDetailSql["InsertOrderDetails"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }

        public List<OrderLineItem> GetOrderDetailsByOrderId(int orderId, string SQuantitytype)
        {
            OrderLineItem orderDetailEntity = null;
            DataTable objDS = new DataTable();
            List<OrderLineItem> OrderDetailList = new List<OrderLineItem>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetOrderDetailsByOrderId";

                    objCommand.Parameters.AddWithValue("@OrderId", orderId);
                    objCommand.Parameters.AddWithValue("@QuantityType", SQuantitytype);
                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            orderDetailEntity = new OrderLineItem();
                            if (item["id"] != DBNull.Value) orderDetailEntity.OrderDetailId = Convert.ToInt32(item["id"]);
                            if (item["order_id"] != DBNull.Value) orderDetailEntity.OrderId = Convert.ToInt32(item["order_id"]);
                            if (item["product_id"] != DBNull.Value) orderDetailEntity.ProductID = Convert.ToInt32(item["product_id"]);
                            if (item["quantity"] != DBNull.Value) orderDetailEntity.Qty = Convert.ToDouble(item["quantity"]);
                            if (item["discount"] != DBNull.Value) orderDetailEntity.Discount = Convert.ToDouble(item["discount"]);
                            if (item["discount_value"] != DBNull.Value) orderDetailEntity.DiscountValue = Convert.ToDouble(item["discount_value"]);
                            if (item["sub_total"] != DBNull.Value) orderDetailEntity.Total = Convert.ToDouble(item["sub_total"]);
                            if (item["status"] != DBNull.Value) orderDetailEntity.Status = item["status"].ToString();
                            if (item["item_price"] != DBNull.Value) orderDetailEntity.ItemPrice = Convert.ToDouble(item["item_price"]);
                            if (item["name"] != DBNull.Value) orderDetailEntity.ProductName = item["name"].ToString();
                            //if (item["qty_casses"] != DBNull.Value) orderDetailEntity.QtyCasess = Convert.ToDouble(item["qty_casses"]);

                            //if (item["qty_tonnage"] != DBNull.Value) orderDetailEntity.QtyTonnage = Convert.ToDouble(item["qty_tonnage"]);


                            OrderDetailList.Add(orderDetailEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return OrderDetailList;
        }
    }
}
