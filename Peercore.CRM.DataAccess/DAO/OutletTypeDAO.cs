﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class OutletTypeDAO : BaseDAO
    {
        private DbSqlAdapter CommonSql { get; set; }

        private void RegisterSql()
        {
            this.CommonSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.CommonSql.xml", ApplicationService.Instance.DbProvider);
        }

        public OutletTypeDAO()
        {
            RegisterSql();
        }

        public OutletTypeDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public OutletTypeEntity CreateObject()
        {
            return OutletTypeEntity.CreateObject();
        }

        public OutletTypeEntity CreateObject(int entityId)
        {
            return OutletTypeEntity.CreateObject(entityId);
        }

        public List<OutletTypeEntity> GetAllOutletTypeByAccessToken(string accessToken)
        {
            DbDataReader customerReader = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                //paramCollection.Add("@AccessToken", accessToken, DbType.String);

                List<OutletTypeEntity> customerList = new List<OutletTypeEntity>();
                customerReader = this.DataAcessService.ExecuteQuery(CommonSql["GetAllOutletTypes"], paramCollection);

                if (customerReader != null && customerReader.HasRows)
                {
                    int outlettypeIdOrdinal = customerReader.GetOrdinal("outlettype_id");
                    int outlettypeNameOrdinal = customerReader.GetOrdinal("outlettype_name");


                    while (customerReader.Read())
                    {
                        OutletTypeEntity customerObj = OutletTypeEntity.CreateObject();

                        if (!customerReader.IsDBNull(outlettypeIdOrdinal)) customerObj.OutletTypeId = customerReader.GetInt32(outlettypeIdOrdinal);
                        if (!customerReader.IsDBNull(outlettypeNameOrdinal)) customerObj.OutletTypeName = customerReader.GetString(outlettypeNameOrdinal);
                        

                        customerList.Add(customerObj);
                    }
                }

                return customerList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (customerReader != null && (!customerReader.IsClosed))
                    customerReader.Close();
            }
        }

        public List<OutletTypeEntity> GetAllOutletType()
        {
            OutletTypeEntity oOutletType = null;
            DbDataReader idrOutletType = null;

            try
            {
                List<OutletTypeEntity> cOutletType = new List<OutletTypeEntity>();

                idrOutletType = this.DataAcessService.ExecuteQuery(CommonSql["GetAllOutletType"]);

                if (idrOutletType != null && idrOutletType.HasRows)
                {
                    int outlettype_idOrdinal = idrOutletType.GetOrdinal("outlettype_id");
                    int outlettype_nameOrdinal = idrOutletType.GetOrdinal("outlettype_name");

                    while (idrOutletType.Read())
                    {
                        oOutletType = OutletTypeEntity.CreateObject();
                        if (!idrOutletType.IsDBNull(outlettype_idOrdinal)) oOutletType.OutletTypeId = idrOutletType.GetInt32(outlettype_idOrdinal);
                        if (!idrOutletType.IsDBNull(outlettype_nameOrdinal)) oOutletType.OutletTypeName = idrOutletType.GetString(outlettype_nameOrdinal).Trim();

                        cOutletType.Add(oOutletType);
                    }
                }

                return cOutletType;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (idrOutletType != null)
                    idrOutletType.Close();
            }
        }

        public bool IsOutletTypeExists(OutletTypeEntity outlettypeEntity)
        {
            DbDataReader drOutletType = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@outlettype_id", outlettypeEntity.OutletTypeId, DbType.Int32);
                paramCollection.Add("@outlettype_name", outlettypeEntity.OutletTypeName, DbType.String);

                drOutletType = this.DataAcessService.ExecuteQuery(CommonSql["OutletTypeExists"], paramCollection);

                if (drOutletType != null && drOutletType.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drOutletType != null)
                    drOutletType.Close();
            }
        }

        public bool UpdateOutletType(OutletTypeEntity outlettypeEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drOutletType = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@outlettype_id", outlettypeEntity.OutletTypeId, DbType.Int32);
                paramCollection.Add("@outlettype_name", outlettypeEntity.OutletTypeName, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CommonSql["UpdateOutletType"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drOutletType != null)
                    drOutletType.Close();
            }
        }

        public bool InsertOutletType(OutletTypeEntity outlettypeEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drOutletType = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                //paramCollection.Add("@Code", brandEntity.BrandCode, DbType.String);
                paramCollection.Add("@outlettype_name", outlettypeEntity.OutletTypeName, DbType.String);
                //paramCollection.Add("@CreatedBy", brandEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CommonSql["InsertOutletType"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drOutletType != null)
                    drOutletType.Close();
            }
        }

        public bool DeleteOutletType(int outletTypeId)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drFlavor = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@OutletTypeId", outletTypeId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(CommonSql["DeleteOutletType"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drFlavor != null)
                    drFlavor.Close();
            }
        }
    }
}
