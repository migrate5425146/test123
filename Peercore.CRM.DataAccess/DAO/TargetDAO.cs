﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;

namespace Peercore.CRM.DataAccess.DAO
{
    public class TargetDAO : BaseDAO
    {
        private DbSqlAdapter Salessql { get; set; }

        private void RegisterSql()
        {
            this.Salessql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.SalesSql.xml", ApplicationService.Instance.DbProvider);
        }

        public TargetDAO()
        {
            RegisterSql();
        }

        public TargetEntity CreateObject()
        {
            return TargetEntity.CreateObject();
        }

        public TargetEntity CreateObject(int entityId)
        {
            return TargetEntity.CreateObject(entityId);
        }

        public TargetEntity GetDRTargetActualsByRoute(int routeId)
        {
            TargetEntity targets = new TargetEntity();
            DbDataReader dataReader = null;

            DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            paramCollection.Add("@RouteId", routeId, DbType.Int32);

            dataReader = this.DataAcessService.ExecuteQuery(Salessql["GetDRTargetActualsByRoute"], paramCollection);

            try
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    int repCodeOrdinal = dataReader.GetOrdinal("rep_code");
                    int stickActualOrdinal = dataReader.GetOrdinal("stick_actual");
                    int amountActualOrdinal = dataReader.GetOrdinal("amount_actual");
                    int stickTargetOrdinal = dataReader.GetOrdinal("stick_target");
                    int amountTargetOrdinal = dataReader.GetOrdinal("amount_target");

                    while (dataReader.Read())
                    {
                        if (!dataReader.IsDBNull(repCodeOrdinal)) targets.RepCode = dataReader.GetString(repCodeOrdinal);
                        if (!dataReader.IsDBNull(stickActualOrdinal)) targets.Actual = Convert.ToDouble(dataReader.GetInt32(stickActualOrdinal));
                        if (!dataReader.IsDBNull(amountActualOrdinal)) targets.AmountActual = Convert.ToDouble(dataReader.GetDecimal(amountActualOrdinal));
                        if (!dataReader.IsDBNull(stickTargetOrdinal)) targets.Target = Convert.ToDouble(dataReader.GetInt32(stickTargetOrdinal));
                        if (!dataReader.IsDBNull(amountTargetOrdinal)) targets.AmountTarget = Convert.ToDouble(dataReader.GetDecimal(amountTargetOrdinal));

                        targets.RouteId = routeId;
                    }
                }
                return targets;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }       

        public List<TargetEntity> GetTargetsVsActualsBrandWise(int programID, DateTime fromDate, DateTime toDate)
        {
            List<TargetEntity> targets = new List<TargetEntity>();
            DbDataReader dataReader = null;

            DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            paramCollection.Add("@ProgramID", programID, DbType.Int32);
            paramCollection.Add("@FromDate", fromDate.ToString("yyyy-MM-dd"), DbType.String);
            paramCollection.Add("@ToDate", toDate.ToString("yyyy-MM-dd"), DbType.String);

            dataReader = this.DataAcessService.ExecuteQuery(Salessql["GetTargetsVsActualsBrandWise"], paramCollection);

            try
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    int brandCodeOrdinal = dataReader.GetOrdinal("brand_code");
                    int brandIdOrdinal = dataReader.GetOrdinal("brand_id");
                    int actualOrdinal = dataReader.GetOrdinal("stick_actual");
                    int targetOrdinal = dataReader.GetOrdinal("stick_target");

                    TargetEntity targetEntity = null;
                    while (dataReader.Read())
                    {
                        targetEntity = CreateObject();

                        if (!dataReader.IsDBNull(brandCodeOrdinal)) targetEntity.BrandCode = dataReader.GetString(brandCodeOrdinal);
                        if (!dataReader.IsDBNull(targetOrdinal)) targetEntity.BrandId = dataReader.GetInt32(targetOrdinal);
                        if (!dataReader.IsDBNull(targetOrdinal)) targetEntity.Target = dataReader.GetInt32(targetOrdinal);
                        if (!dataReader.IsDBNull(actualOrdinal)) targetEntity.Actual = dataReader.GetInt32(actualOrdinal);

                        targets.Add(targetEntity);
                    }
                }
                return targets;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public TargetEntity GetTargetsVsActualsForOutlet(string custCode, DateTime fromDate, DateTime todate)
        {
            TargetEntity target = CreateObject();
            
            DbDataReader dataReader = null;

            DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            paramCollection.Add("@CustCode", custCode, DbType.String);
            paramCollection.Add("@StartDate", fromDate.ToString("yyyy-MM-dd"), DbType.String);
            paramCollection.Add("@EndDate", todate.ToString("yyyy-MM-dd"), DbType.String);
            

            dataReader = this.DataAcessService.ExecuteQuery(Salessql["GetTargetsVsActualsForOutlet"], paramCollection);

            try
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    int custCodeOrdinal = dataReader.GetOrdinal("cust_code");
                    int actualOrdinal = dataReader.GetOrdinal("stick_actual");
                    int targetOrdinal = dataReader.GetOrdinal("stick_target");

                    while (dataReader.Read())
                    {
                        if (!dataReader.IsDBNull(custCodeOrdinal)) target.CustomerCode = dataReader.GetString(custCodeOrdinal);                            
                        if (!dataReader.IsDBNull(targetOrdinal)) target.Target = dataReader.GetInt32(targetOrdinal);
                        if (!dataReader.IsDBNull(actualOrdinal)) target.Actual = dataReader.GetInt32(actualOrdinal);
                    }
                }

                if (target.CustomerCode == null) target.CustomerCode = custCode;
                return target;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }


        public List<TargetEntity> GetTargetsVsActualsOfRoutes(DateTime fromDate, DateTime toDate)
        {            
            List<TargetEntity> targets = new List<TargetEntity>();

            DbDataReader dataReader = null;

            DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            paramCollection.Add("@FromDate", fromDate.ToString("yyyy-MM-dd"), DbType.String);
            paramCollection.Add("@ToDate", toDate.ToString("yyyy-MM-dd"), DbType.String);

            dataReader = this.DataAcessService.ExecuteQuery(Salessql["GetTargetsVsActualsOfRoutes"], paramCollection);

            try
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    int routeMasterIdOrdinal = dataReader.GetOrdinal("route_master_id");
                    int routeNameOrdinal = dataReader.GetOrdinal("route_name");
                    int actualOrdinal = dataReader.GetOrdinal("stick_actual");
                    int targetOrdinal = dataReader.GetOrdinal("stick_target");

                    TargetEntity target = null;
                    while (dataReader.Read())
                    {
                        target = CreateObject();

                        if (!dataReader.IsDBNull(routeMasterIdOrdinal)) target.RouteId = dataReader.GetInt32(routeMasterIdOrdinal);
                        if (!dataReader.IsDBNull(routeMasterIdOrdinal)) target.RouteName = dataReader.GetString(routeNameOrdinal);
                        if (!dataReader.IsDBNull(targetOrdinal)) target.Target = dataReader.GetInt32(targetOrdinal);
                        if (!dataReader.IsDBNull(actualOrdinal)) target.Actual = dataReader.GetInt32(actualOrdinal);

                        targets.Add(target);
                    }
                }

                return targets;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public List<TargetEntity> GetTargetsVsActualsOfOutletsByRep(DateTime fromDate, DateTime toDate, string repcode)
        {
            List<TargetEntity> targets = new List<TargetEntity>();

            DbDataReader dataReader = null;

            DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            paramCollection.Add("@RepCode", repcode, DbType.String);
            paramCollection.Add("@StartDate", fromDate.ToString("yyyy-MM-dd"), DbType.String);
            paramCollection.Add("@EndDate", toDate.ToString("yyyy-MM-dd"), DbType.String);

            dataReader = this.DataAcessService.ExecuteQuery(Salessql["GetTargetsVsActualsOfOutlets"], paramCollection);

            try
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    int repCodeOrdinal = dataReader.GetOrdinal("cust_code");
                    int actualOrdinal = dataReader.GetOrdinal("stick_actual");
                    int targetOrdinal = dataReader.GetOrdinal("stick_target");
                    int percentageOrdinal = dataReader.GetOrdinal("percentage");

                    TargetEntity target = null;
                    while (dataReader.Read())
                    {
                        target = CreateObject();

                        if (!dataReader.IsDBNull(repCodeOrdinal)) target.CustomerCode = dataReader.GetString(repCodeOrdinal);
                        if (!dataReader.IsDBNull(percentageOrdinal)) target.TargetActualPercentage = Convert.ToDouble(dataReader.GetDecimal(percentageOrdinal));
                        if (!dataReader.IsDBNull(targetOrdinal)) target.Target = dataReader.GetInt32(targetOrdinal);
                        if (!dataReader.IsDBNull(actualOrdinal)) target.Actual = dataReader.GetInt32(actualOrdinal);

                        targets.Add(target);
                    }
                }

                return targets;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public List<TargetEntity> GetRouteSummaryBrandwise(int routeId)
        {
            List<TargetEntity> targets = new List<TargetEntity>();

            DbDataReader dataReader = null;

            DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            paramCollection.Add("@RouteId", routeId, DbType.Int32);

            dataReader = this.DataAcessService.ExecuteQuery(Salessql["GetRouteSummaryBrandwise"], paramCollection);

            try
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    int brandCodeOrdinal = dataReader.GetOrdinal("brand_code");
                    int brandIdOrdinal = dataReader.GetOrdinal("brand_id");
                    int brandNameOrdinal = dataReader.GetOrdinal("brand_name");
                    int achivementOrdinal = dataReader.GetOrdinal("achivement");
                    int targetOrdinal = dataReader.GetOrdinal("route_target");
                    int averageOrdinal = dataReader.GetOrdinal("average_sale");

                    TargetEntity target = null;
                    while (dataReader.Read())
                    {
                        target = CreateObject();

                        if (!dataReader.IsDBNull(brandCodeOrdinal)) target.BrandCode = dataReader.GetString(brandCodeOrdinal);
                        if (!dataReader.IsDBNull(brandIdOrdinal)) target.BrandId = dataReader.GetInt32(brandIdOrdinal);
                        if (!dataReader.IsDBNull(brandNameOrdinal)) target.BrandName = dataReader.GetString(brandNameOrdinal);
                        if (!dataReader.IsDBNull(achivementOrdinal)) target.Actual = dataReader.GetInt32(achivementOrdinal);
                        if (!dataReader.IsDBNull(targetOrdinal)) target.Target = dataReader.GetInt32(targetOrdinal);
                        if (!dataReader.IsDBNull(averageOrdinal)) target.AverageSale = Convert.ToDouble(dataReader.GetDecimal(averageOrdinal));
                        targets.Add(target);
                    }
                }

                return targets;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }


        public List<MobileAbishekaSummaryEntity> GetAbishekaRouteSummaryByRoute(int routeId)
        {
            List<MobileAbishekaSummaryEntity> targets = new List<MobileAbishekaSummaryEntity>();
            DbDataReader dataReader = null;

            DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            paramCollection.Add("@RouteMasterId", routeId, DbType.Int32);

            dataReader = this.DataAcessService.ExecuteQuery(Salessql["GetAbishekaRouteSummaryByRoute"], paramCollection);

            try
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    int volumeOrdinal = dataReader.GetOrdinal("volume");
                    int actualOrdinal = dataReader.GetOrdinal("actual");
                    int targetOrdinal = dataReader.GetOrdinal("target");
                    int achiPercOrdinal = dataReader.GetOrdinal("achivement_perc");
                    int achiCountOrdinal = dataReader.GetOrdinal("achived_count");
                    int outCountOrdinal = dataReader.GetOrdinal("outlet_count");
                    int investmentOrdinal = dataReader.GetOrdinal("investment");
                    int milleCostOrdinal = dataReader.GetOrdinal("mille_cost");

                    int actualTotOrdinal = dataReader.GetOrdinal("actual_tot");
                    int targetTotOrdinal = dataReader.GetOrdinal("target_tot");
                    int achiPercTotOrdinal = dataReader.GetOrdinal("achivement_perc_tot");
                    int achiCountTotOrdinal = dataReader.GetOrdinal("achived_count_tot");
                    int outCountTotOrdinal = dataReader.GetOrdinal("outlet_count_tot");
                    int investmentTotOrdinal = dataReader.GetOrdinal("investment_tot");
                    int milleCostTotOrdinal = dataReader.GetOrdinal("mille_cost_tot");

                    MobileAbishekaSummaryEntity target = null;
                    while (dataReader.Read())
                    {
                        target = new MobileAbishekaSummaryEntity();

                        if (!dataReader.IsDBNull(volumeOrdinal)) target.Volume = dataReader.GetInt32(volumeOrdinal);

                        if (!dataReader.IsDBNull(actualOrdinal)) target.Actual = dataReader.GetInt32(actualOrdinal);
                        if (!dataReader.IsDBNull(targetOrdinal)) target.Target = Convert.ToInt32(dataReader.GetDouble(targetOrdinal));
                        if (!dataReader.IsDBNull(achiPercOrdinal)) target.AchievementPerc = Convert.ToDouble( dataReader.GetDecimal(achiPercOrdinal));
                        if (!dataReader.IsDBNull(achiCountOrdinal)) target.TargetAchivers = dataReader.GetInt32(achiCountOrdinal);
                        if (!dataReader.IsDBNull(outCountOrdinal)) target.Outlets = dataReader.GetInt32(outCountOrdinal);
                        if (!dataReader.IsDBNull(investmentOrdinal)) target.Investment = Convert.ToDouble(dataReader.GetDecimal(investmentOrdinal));
                        if (!dataReader.IsDBNull(milleCostOrdinal)) target.CostPerMille = Convert.ToDouble(dataReader.GetDecimal(milleCostOrdinal));

                        if (!dataReader.IsDBNull(actualTotOrdinal)) target.ActualTotal = dataReader.GetInt32(actualTotOrdinal);
                        if (!dataReader.IsDBNull(targetTotOrdinal)) target.TargetTotal = Convert.ToInt32(dataReader.GetDouble(targetTotOrdinal));
                        if (!dataReader.IsDBNull(achiPercTotOrdinal)) target.AchievementPercTotal = Convert.ToDouble(dataReader.GetDecimal(achiPercTotOrdinal));
                        if (!dataReader.IsDBNull(achiCountTotOrdinal)) target.TargetAchiversTotal = dataReader.GetInt32(achiCountTotOrdinal);
                        if (!dataReader.IsDBNull(outCountTotOrdinal)) target.OutletsTotal = dataReader.GetInt32(outCountTotOrdinal);
                        if (!dataReader.IsDBNull(investmentTotOrdinal)) target.InvestmentTotal = Convert.ToDouble(dataReader.GetDecimal(investmentTotOrdinal));
                        if (!dataReader.IsDBNull(milleCostTotOrdinal)) target.CostPerMilleTotal = Convert.ToDouble(dataReader.GetDecimal(milleCostTotOrdinal));

                        targets.Add(target);
                    }
                }

                return targets;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public List<MobileAbishekaSummaryEntity> GetAllVolumeRouteSummaryByRoute(int routeId)
        {
            List<MobileAbishekaSummaryEntity> targets = new List<MobileAbishekaSummaryEntity>();
            DbDataReader dataReader = null;

            DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            paramCollection.Add("@RouteMasterId", routeId, DbType.Int32);

            dataReader = this.DataAcessService.ExecuteQuery(Salessql["GetAllVolumeRouteSummaryByRoute"], paramCollection);

            try
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    int volumeOrdinal = dataReader.GetOrdinal("volume");
                    int actualOrdinal = dataReader.GetOrdinal("actual");
                    int targetOrdinal = dataReader.GetOrdinal("target");
                    int achiPercOrdinal = dataReader.GetOrdinal("achivement_perc");
                    int achiCountOrdinal = dataReader.GetOrdinal("achived_count");
                    int outCountOrdinal = dataReader.GetOrdinal("outlet_count");
                    int investmentOrdinal = dataReader.GetOrdinal("investment");
                    int milleCostOrdinal = dataReader.GetOrdinal("mille_cost");

                    int actualTotOrdinal = dataReader.GetOrdinal("actual_tot");
                    int targetTotOrdinal = dataReader.GetOrdinal("target_tot");
                    int achiPercTotOrdinal = dataReader.GetOrdinal("achivement_perc_tot");
                    int achiCountTotOrdinal = dataReader.GetOrdinal("achived_count_tot");
                    int outCountTotOrdinal = dataReader.GetOrdinal("outlet_count_tot");
                    int investmentTotOrdinal = dataReader.GetOrdinal("investment_tot");
                    int milleCostTotOrdinal = dataReader.GetOrdinal("mille_cost_tot");

                    MobileAbishekaSummaryEntity target = null;
                    while (dataReader.Read())
                    {
                        target = new MobileAbishekaSummaryEntity();

                        if (!dataReader.IsDBNull(volumeOrdinal)) target.Volume = dataReader.GetInt32(volumeOrdinal);

                        if (!dataReader.IsDBNull(actualOrdinal)) target.Actual = dataReader.GetInt32(actualOrdinal);
                        if (!dataReader.IsDBNull(targetOrdinal)) target.Target =dataReader.GetInt32(targetOrdinal);
                        if (!dataReader.IsDBNull(achiPercOrdinal)) target.AchievementPerc = Convert.ToDouble(dataReader.GetDecimal(achiPercOrdinal));
                        if (!dataReader.IsDBNull(achiCountOrdinal)) target.TargetAchivers = dataReader.GetInt32(achiCountOrdinal);
                        if (!dataReader.IsDBNull(outCountOrdinal)) target.Outlets = dataReader.GetInt32(outCountOrdinal);
                        if (!dataReader.IsDBNull(investmentOrdinal)) target.Investment = Convert.ToDouble(dataReader.GetDecimal(investmentOrdinal));
                        if (!dataReader.IsDBNull(milleCostOrdinal)) target.CostPerMille = Convert.ToDouble(dataReader.GetDecimal(milleCostOrdinal));

                        if (!dataReader.IsDBNull(actualTotOrdinal)) target.ActualTotal = dataReader.GetInt32(actualTotOrdinal);
                        if (!dataReader.IsDBNull(targetTotOrdinal)) target.TargetTotal = Convert.ToInt32(dataReader.GetDouble(targetTotOrdinal));
                        if (!dataReader.IsDBNull(achiPercTotOrdinal)) target.AchievementPercTotal = Convert.ToDouble(dataReader.GetDecimal(achiPercTotOrdinal));
                        if (!dataReader.IsDBNull(achiCountTotOrdinal)) target.TargetAchiversTotal = dataReader.GetInt32(achiCountTotOrdinal);
                        if (!dataReader.IsDBNull(outCountTotOrdinal)) target.OutletsTotal = dataReader.GetInt32(outCountTotOrdinal);
                        if (!dataReader.IsDBNull(investmentTotOrdinal)) target.InvestmentTotal = Convert.ToDouble(dataReader.GetDecimal(investmentTotOrdinal));
                        if (!dataReader.IsDBNull(milleCostTotOrdinal)) target.CostPerMilleTotal = Convert.ToDouble(dataReader.GetDecimal(milleCostTotOrdinal));

                        targets.Add(target);
                    }
                }

                return targets;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public List<MobileCustomerPerformanceEntity> GetCustomerVolumePerformance(string custCode)
        {
            List<MobileCustomerPerformanceEntity> targets = new List<MobileCustomerPerformanceEntity>();
            DbDataReader dataReader = null;
            DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            paramCollection.Add("@CustCode", custCode, DbType.String);
            dataReader = this.DataAcessService.ExecuteQuery(Salessql["GetCustomerAllVolumePerformance"], paramCollection);

            try
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    int custCodeOrdinal = dataReader.GetOrdinal("cust_code");
                    int brandCodeOrdinal = dataReader.GetOrdinal("brand_code");
                    int actualOrdinal = dataReader.GetOrdinal("actual");
                    int targetOrdinal = dataReader.GetOrdinal("target");
                    int achiPercOrdinal = dataReader.GetOrdinal("achivement_perc");
                    int csrOrdinal = dataReader.GetOrdinal("csr");
                    int rsrOrdinal = dataReader.GetOrdinal("rsr");
                    int actualTotOrdinal = dataReader.GetOrdinal("actual_total");
                    int targetTotOrdinal = dataReader.GetOrdinal("target_total");
                    int achiPercTotOrdinal = dataReader.GetOrdinal("achivement_perc_total");
                    int csrTotOrdinal = dataReader.GetOrdinal("csr_total");
                    int rsrTotOrdinal = dataReader.GetOrdinal("rsr_total");

                    MobileCustomerPerformanceEntity target = null;
                    while (dataReader.Read())
                    {
                        target = new MobileCustomerPerformanceEntity();

                        if (!dataReader.IsDBNull(custCodeOrdinal)) target.CustCode = dataReader.GetString(custCodeOrdinal);
                        if (!dataReader.IsDBNull(brandCodeOrdinal)) target.BrandCode = dataReader.GetString(brandCodeOrdinal);
                        if (!dataReader.IsDBNull(actualOrdinal)) target.Actual = dataReader.GetInt32(actualOrdinal);
                        if (!dataReader.IsDBNull(targetOrdinal)) target.Target = dataReader.GetInt32(targetOrdinal);
                        if (!dataReader.IsDBNull(achiPercOrdinal)) target.AchievementPerc = Convert.ToDouble(dataReader.GetDecimal(achiPercOrdinal));
                        if (!dataReader.IsDBNull(csrOrdinal)) target.CurrentStrikeRate = Convert.ToDouble(dataReader.GetDecimal(csrOrdinal));
                        if (!dataReader.IsDBNull(rsrOrdinal)) target.ReqiredStrikeRate = Convert.ToDouble(dataReader.GetDecimal(rsrOrdinal));
                        if (!dataReader.IsDBNull(actualTotOrdinal)) target.Actual = dataReader.GetInt32(actualTotOrdinal);
                        if (!dataReader.IsDBNull(targetOrdinal)) target.Target = dataReader.GetInt32(targetOrdinal);
                        if (!dataReader.IsDBNull(achiPercTotOrdinal)) target.AchievementPerc = Convert.ToDouble(dataReader.GetDecimal(achiPercTotOrdinal));
                        if (!dataReader.IsDBNull(csrTotOrdinal)) target.CurrentStrikeRate = Convert.ToDouble(dataReader.GetDecimal(csrTotOrdinal));
                        if (!dataReader.IsDBNull(rsrTotOrdinal)) target.ReqiredStrikeRate = Convert.ToDouble(dataReader.GetDecimal(rsrTotOrdinal));
                        targets.Add(target);
                    }
                }

                return targets;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public List<MobileCustomerPerformanceEntity> GetAbishekaCustomerPerformance(string custCode)
        {
            List<MobileCustomerPerformanceEntity> targets = new List<MobileCustomerPerformanceEntity>();
            DbDataReader dataReader = null;
            DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            paramCollection.Add("@CustCode", custCode, DbType.String);
            dataReader = this.DataAcessService.ExecuteQuery(Salessql["GetAbishekaCustomerPerformance"], paramCollection);

            try
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    int custCodeOrdinal = dataReader.GetOrdinal("cust_code");
                    int brandCodeOrdinal = dataReader.GetOrdinal("brand_code");
                    int actualOrdinal = dataReader.GetOrdinal("actual");
                    int targetOrdinal = dataReader.GetOrdinal("target");
                    int achiPercOrdinal = dataReader.GetOrdinal("achivement_perc");
                    int csrOrdinal = dataReader.GetOrdinal("csr");
                    int rsrOrdinal = dataReader.GetOrdinal("rsr");
                    int actualTotOrdinal = dataReader.GetOrdinal("actual_total");
                    int targetTotOrdinal = dataReader.GetOrdinal("target_total");
                    int achiPercTotOrdinal = dataReader.GetOrdinal("achivement_perc_total");
                    int csrTotOrdinal = dataReader.GetOrdinal("csr_total");
                    int rsrTotOrdinal = dataReader.GetOrdinal("rsr_total");

                    MobileCustomerPerformanceEntity target = null;
                    while (dataReader.Read())
                    {
                        target = new MobileCustomerPerformanceEntity();

                        if (!dataReader.IsDBNull(custCodeOrdinal)) target.CustCode = dataReader.GetString(custCodeOrdinal);
                        if (!dataReader.IsDBNull(brandCodeOrdinal)) target.BrandCode = dataReader.GetString(brandCodeOrdinal);
                        if (!dataReader.IsDBNull(actualOrdinal)) target.Actual = dataReader.GetInt32(actualOrdinal);
                        if (!dataReader.IsDBNull(targetOrdinal)) target.Target = dataReader.GetInt32(targetOrdinal);
                        if (!dataReader.IsDBNull(achiPercOrdinal)) target.AchievementPerc = Convert.ToDouble(dataReader.GetDecimal(achiPercOrdinal));
                        if (!dataReader.IsDBNull(csrOrdinal)) target.CurrentStrikeRate = Convert.ToDouble(dataReader.GetDecimal(csrOrdinal));
                        if (!dataReader.IsDBNull(rsrOrdinal)) target.ReqiredStrikeRate = Convert.ToDouble(dataReader.GetDecimal(rsrOrdinal));
                        if (!dataReader.IsDBNull(actualTotOrdinal)) target.Actual = dataReader.GetInt32(actualTotOrdinal);
                        if (!dataReader.IsDBNull(targetOrdinal)) target.Target = dataReader.GetInt32(targetOrdinal);
                        if (!dataReader.IsDBNull(achiPercTotOrdinal)) target.AchievementPerc = Convert.ToDouble(dataReader.GetDecimal(achiPercTotOrdinal));
                        if (!dataReader.IsDBNull(csrTotOrdinal)) target.CurrentStrikeRate = Convert.ToDouble(dataReader.GetDecimal(csrTotOrdinal));
                        if (!dataReader.IsDBNull(rsrTotOrdinal)) target.ReqiredStrikeRate = Convert.ToDouble(dataReader.GetDecimal(rsrTotOrdinal));
                        targets.Add(target);
                    }
                }

                return targets;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public List<MobileMonthlyActualsAndEarnings> GetCustomerTargetAchievedAndEarningsMonthly(string custCode)
        {
            List<MobileMonthlyActualsAndEarnings> entityList = new List<MobileMonthlyActualsAndEarnings>();
            DbDataReader dataReader = null;
            DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            paramCollection.Add("@CustCode", custCode, DbType.String);
            
            try
            {
                dataReader = this.DataAcessService.ExecuteQuery(Salessql["GetCustomerTargetAchievedAndEarningsMonthly"], paramCollection);

                if (dataReader != null && dataReader.HasRows)
                {
                    int custCodeOrdinal = dataReader.GetOrdinal("cust_code");
                    int nameOrdinal = dataReader.GetOrdinal("name");

                    int AMonth1Ordinal = dataReader.GetOrdinal("AMonth1");
                    int AMonth2Ordinal = dataReader.GetOrdinal("AMonth2");
                    int AMonth3Ordinal = dataReader.GetOrdinal("AMonth3");
                    int AMonth4Ordinal = dataReader.GetOrdinal("AMonth4");
                    int AMonth5Ordinal = dataReader.GetOrdinal("AMonth5");
                    int AMonth6Ordinal = dataReader.GetOrdinal("AMonth6");
                    int AMonth7Ordinal = dataReader.GetOrdinal("AMonth7");
                    int AMonth8Ordinal = dataReader.GetOrdinal("AMonth8");
                    int AMonth9Ordinal = dataReader.GetOrdinal("AMonth9");
                    int AMonth10Ordinal = dataReader.GetOrdinal("AMonth10");
                    int AMonth11Ordinal = dataReader.GetOrdinal("AMonth11");
                    int AMonth12Ordinal = dataReader.GetOrdinal("AMonth12");

                    int EMonth1Ordinal = dataReader.GetOrdinal("EMonth1");
                    int EMonth2Ordinal = dataReader.GetOrdinal("EMonth2");
                    int EMonth3Ordinal = dataReader.GetOrdinal("EMonth3");
                    int EMonth4Ordinal = dataReader.GetOrdinal("EMonth4");
                    int EMonth5Ordinal = dataReader.GetOrdinal("EMonth5");
                    int EMonth6Ordinal = dataReader.GetOrdinal("EMonth6");
                    int EMonth7Ordinal = dataReader.GetOrdinal("EMonth7");
                    int EMonth8Ordinal = dataReader.GetOrdinal("EMonth8");
                    int EMonth9Ordinal = dataReader.GetOrdinal("EMonth9");
                    int EMonth10Ordinal = dataReader.GetOrdinal("EMonth10");
                    int EMonth11Ordinal = dataReader.GetOrdinal("EMonth11");
                    int EMonth12Ordinal = dataReader.GetOrdinal("EMonth12");

                    MobileMonthlyActualsAndEarnings monthlyEntity = null;
                    while (dataReader.Read())
                    {
                        monthlyEntity = MobileMonthlyActualsAndEarnings.CreateObject();

                        if (!dataReader.IsDBNull(custCodeOrdinal)) monthlyEntity.CustomerCode = dataReader.GetString(custCodeOrdinal);
                        if (!dataReader.IsDBNull(nameOrdinal)) monthlyEntity.CustomerName = dataReader.GetString(nameOrdinal);

                        if (!dataReader.IsDBNull(AMonth1Ordinal)) monthlyEntity.AchievedJanuary = Convert.ToDouble(dataReader.GetDecimal(AMonth1Ordinal));
                        if (!dataReader.IsDBNull(AMonth2Ordinal)) monthlyEntity.AchievedFebruary = Convert.ToDouble(dataReader.GetDecimal(AMonth2Ordinal));
                        if (!dataReader.IsDBNull(AMonth3Ordinal)) monthlyEntity.AchievedMarch = Convert.ToDouble(dataReader.GetDecimal(AMonth3Ordinal));
                        if (!dataReader.IsDBNull(AMonth4Ordinal)) monthlyEntity.AchievedApril = Convert.ToDouble(dataReader.GetDecimal(AMonth4Ordinal));
                        if (!dataReader.IsDBNull(AMonth5Ordinal)) monthlyEntity.AchievedMay = Convert.ToDouble(dataReader.GetDecimal(AMonth5Ordinal));
                        if (!dataReader.IsDBNull(AMonth6Ordinal)) monthlyEntity.AchievedJune = Convert.ToDouble(dataReader.GetDecimal(AMonth6Ordinal));
                        if (!dataReader.IsDBNull(AMonth7Ordinal)) monthlyEntity.AchievedJuly = Convert.ToDouble(dataReader.GetDecimal(AMonth7Ordinal));
                        if (!dataReader.IsDBNull(AMonth8Ordinal)) monthlyEntity.AchievedAugust = Convert.ToDouble(dataReader.GetDecimal(AMonth8Ordinal));
                        if (!dataReader.IsDBNull(AMonth9Ordinal)) monthlyEntity.AchievedSeptember = Convert.ToDouble(dataReader.GetDecimal(AMonth9Ordinal));
                        if (!dataReader.IsDBNull(AMonth10Ordinal)) monthlyEntity.AchievedOctober = Convert.ToDouble(dataReader.GetDecimal(AMonth10Ordinal));
                        if (!dataReader.IsDBNull(AMonth11Ordinal)) monthlyEntity.AchievedNovember = Convert.ToDouble(dataReader.GetDecimal(AMonth11Ordinal));
                        if (!dataReader.IsDBNull(AMonth12Ordinal)) monthlyEntity.AchievedDecember = Convert.ToDouble(dataReader.GetDecimal(AMonth12Ordinal));

                        if (!dataReader.IsDBNull(EMonth1Ordinal)) monthlyEntity.EarningsJanuary = Convert.ToDouble(dataReader.GetDecimal(EMonth1Ordinal));
                        if (!dataReader.IsDBNull(EMonth2Ordinal)) monthlyEntity.EarningsFebruary = Convert.ToDouble(dataReader.GetDecimal(EMonth2Ordinal));
                        if (!dataReader.IsDBNull(EMonth3Ordinal)) monthlyEntity.EarningsMarch = Convert.ToDouble(dataReader.GetDecimal(EMonth3Ordinal));
                        if (!dataReader.IsDBNull(EMonth4Ordinal)) monthlyEntity.EarningsApril = Convert.ToDouble(dataReader.GetDecimal(EMonth4Ordinal));
                        if (!dataReader.IsDBNull(EMonth5Ordinal)) monthlyEntity.EarningsMay = Convert.ToDouble(dataReader.GetDecimal(EMonth5Ordinal));
                        if (!dataReader.IsDBNull(EMonth6Ordinal)) monthlyEntity.EarningsJune = Convert.ToDouble(dataReader.GetDecimal(EMonth6Ordinal));
                        if (!dataReader.IsDBNull(EMonth7Ordinal)) monthlyEntity.EarningsJuly = Convert.ToDouble(dataReader.GetDecimal(EMonth7Ordinal));
                        if (!dataReader.IsDBNull(EMonth8Ordinal)) monthlyEntity.EarningsAugust = Convert.ToDouble(dataReader.GetDecimal(EMonth8Ordinal));
                        if (!dataReader.IsDBNull(EMonth9Ordinal)) monthlyEntity.EarningsSeptember = Convert.ToDouble(dataReader.GetDecimal(EMonth9Ordinal));
                        if (!dataReader.IsDBNull(EMonth10Ordinal)) monthlyEntity.EarningsOctober = Convert.ToDouble(dataReader.GetDecimal(EMonth10Ordinal));
                        if (!dataReader.IsDBNull(EMonth11Ordinal)) monthlyEntity.EarningsNovember = Convert.ToDouble(dataReader.GetDecimal(EMonth11Ordinal));
                        if (!dataReader.IsDBNull(EMonth12Ordinal)) monthlyEntity.EarningsDecember = Convert.ToDouble(dataReader.GetDecimal(EMonth12Ordinal));

                        entityList.Add(monthlyEntity);
                    }
                }

                return entityList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }


        public List<MobileAbishekaSummaryEntity> GetAbishekaBonusAndTargetByRoute(int routeId)
        {
            List<MobileAbishekaSummaryEntity> targets = new List<MobileAbishekaSummaryEntity>();
            DbDataReader dataReader = null;

            DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            paramCollection.Add("@RouteMasterId", routeId, DbType.Int32);

            dataReader = this.DataAcessService.ExecuteQuery(Salessql["GetAbishekaBonusAndTargetByRoute"], paramCollection);

            try
            {
                if (dataReader != null && dataReader.HasRows)
                {

                    int actualOrdinal = dataReader.GetOrdinal("abisheka_actual");
                    int targetOrdinal = dataReader.GetOrdinal("abisheka_target");
                    int actualBonusOrdinal = dataReader.GetOrdinal("bonus_actual");
                    int targetBonusOrdinal = dataReader.GetOrdinal("bonus_target");


                    MobileAbishekaSummaryEntity target = null;
                    while (dataReader.Read())
                    {
                        target = new MobileAbishekaSummaryEntity();

                        if (!dataReader.IsDBNull(actualOrdinal)) target.Actual = Convert.ToInt32(dataReader.GetInt32(actualOrdinal));
                        if (!dataReader.IsDBNull(targetOrdinal)) target.Target = Convert.ToInt32(dataReader.GetDouble(targetOrdinal));
                        if (!dataReader.IsDBNull(actualBonusOrdinal)) target.ActualBonus = Convert.ToInt32(dataReader.GetInt32(actualBonusOrdinal));
                        if (!dataReader.IsDBNull(targetBonusOrdinal)) target.TargetBonus = Convert.ToInt32(dataReader.GetDouble(targetBonusOrdinal));

                        targets.Add(target);
                    }
                }

                return targets;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }
           
        
    }
}
