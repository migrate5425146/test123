﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;
using Peercore.CRM.DataAccess.datasets;
using System.Data.SqlClient;
using Peercore.CRM.Model;

namespace Peercore.CRM.DataAccess.DAO
{
    public class SchemeHeaderDAO : BaseDAO
    {
        private DbSqlAdapter SchemeHeaderSql { get; set; }

        private void RegisterSql()
        {
            this.SchemeHeaderSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.SchemeHeaderSql.xml", ApplicationService.Instance.DbProvider);
        }

        public SchemeHeaderDAO()
        {
            RegisterSql();
        }

        public SchemeHeaderDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public SchemeHeaderEntity CreateObject()
        {
            return SchemeHeaderEntity.CreateObject();
        }

        public SchemeHeaderEntity CreateObject(int entityId)
        {
            return SchemeHeaderEntity.CreateObject(entityId);
        }

        public bool UpdateSchemeHeader(SchemeHeaderEntity schemeHeader)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@SchemeHeaderId", schemeHeader.SchemeHeaderId, DbType.Int32);
                paramCollection.Add("@SchemeName", schemeHeader.SchemeName, DbType.String);
                paramCollection.Add("@IsActive", schemeHeader.IsActive, DbType.Boolean);
                paramCollection.Add("@StartDate", schemeHeader.StartDate, DbType.DateTime);
                paramCollection.Add("@EndDate", schemeHeader.EndDate, DbType.DateTime);
                paramCollection.Add("@DivisionId", schemeHeader.DivisionId, DbType.Int32);
                paramCollection.Add("@LastModifiedBy", schemeHeader.LastModifiedBy, DbType.String);                

                iNoRec = this.DataAcessService.ExecuteNonQuery(SchemeHeaderSql["UpdateSchemeHeader"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return UpdateSuccessful;
        }

        public bool InsertSchemeHeader(out int schemeHeaderId, SchemeHeaderEntity schemeHeader)
        {
            bool successful = false;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@SchemeName", schemeHeader.SchemeName, DbType.String);
                paramCollection.Add("@IsActive", schemeHeader.IsActive, DbType.Boolean);
                paramCollection.Add("@StartDate", schemeHeader.StartDate, DbType.DateTime);
                paramCollection.Add("@EndDate", schemeHeader.EndDate, DbType.DateTime);
                paramCollection.Add("@DivisionId", schemeHeader.DivisionId, DbType.Int32);
                paramCollection.Add("@CreatedBy", schemeHeader.CreatedBy, DbType.String);
                paramCollection.Add("@SchemeHeaderId", null, DbType.Int32, ParameterDirection.Output);

                schemeHeaderId = this.DataAcessService.ExecuteNonQuery(SchemeHeaderSql["InsertSchemeHeader"], paramCollection, true, "@SchemeHeaderId");
                if (schemeHeaderId > 0)
                    successful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return successful;
        }

        public List<SchemeHeaderEntity> GetAllSchemeHeader(ArgsEntity args)
        {
            DbDataReader drSchemeHeader = null;
            try
            {
                List<SchemeHeaderEntity> schemeHeaderCollection = new List<SchemeHeaderEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@StartDate", args.SStartDate, DbType.String);
                paramCollection.Add("@EndDate", args.SEndDate, DbType.String);
                paramCollection.Add("@IsActive", !args.ActiveInactiveChecked, DbType.Boolean);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                drSchemeHeader = this.DataAcessService.ExecuteQuery(SchemeHeaderSql["GetAllSchemeHeader"], paramCollection);

                if (drSchemeHeader != null && drSchemeHeader.HasRows)
                {
                    int schemeHeaderIdOrdinal = drSchemeHeader.GetOrdinal("scheme_header_id");
                    int schemeNameOrdinal = drSchemeHeader.GetOrdinal("scheme_name");
                    int isActiveOrdinal = drSchemeHeader.GetOrdinal("is_active");
                    int startDateOrdinal = drSchemeHeader.GetOrdinal("start_date");
                    int endDateOrdinal = drSchemeHeader.GetOrdinal("end_date");
                    int divisionIdOrdinal = drSchemeHeader.GetOrdinal("division_id");
                    int divisionNameOrdinal = drSchemeHeader.GetOrdinal("division_name");
                    int totalCountOrdinal = drSchemeHeader.GetOrdinal("total_count");

                    while (drSchemeHeader.Read())
                    {
                        SchemeHeaderEntity schemeHeader = SchemeHeaderEntity.CreateObject();

                        if (!drSchemeHeader.IsDBNull(schemeHeaderIdOrdinal)) schemeHeader.SchemeHeaderId = drSchemeHeader.GetInt32(schemeHeaderIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(schemeNameOrdinal)) schemeHeader.SchemeName = drSchemeHeader.GetString(schemeNameOrdinal);
                        if (!drSchemeHeader.IsDBNull(isActiveOrdinal)) schemeHeader.IsActive = drSchemeHeader.GetBoolean(isActiveOrdinal);
                        if (!drSchemeHeader.IsDBNull(startDateOrdinal)) schemeHeader.StartDate = drSchemeHeader.GetDateTime(startDateOrdinal);
                        if (!drSchemeHeader.IsDBNull(endDateOrdinal)) schemeHeader.EndDate = drSchemeHeader.GetDateTime(endDateOrdinal);
                        if (!drSchemeHeader.IsDBNull(divisionIdOrdinal)) schemeHeader.DivisionId = drSchemeHeader.GetInt32(divisionIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(divisionNameOrdinal)) schemeHeader.DivisionName = drSchemeHeader.GetString(divisionNameOrdinal);
                        if (!drSchemeHeader.IsDBNull(totalCountOrdinal)) schemeHeader.TotalCount = drSchemeHeader.GetInt32(totalCountOrdinal);

                        schemeHeaderCollection.Add(schemeHeader);

                    }
                }

                return schemeHeaderCollection;
            }
            catch(Exception ex)
            {
                throw;
            }
            finally
            {
                if (drSchemeHeader != null)
                    drSchemeHeader.Close();
            }
        }

        public List<SchemeHeaderEntity> GetAllSchemeHeader()
        {
            DbDataReader drSchemeHeader = null;
            try
            {
                List<SchemeHeaderEntity> schemeHeaderCollection = new List<SchemeHeaderEntity>();

                drSchemeHeader = this.DataAcessService.ExecuteQuery(SchemeHeaderSql["GetAllSchemeHeaderForCopy"]);

                if (drSchemeHeader != null && drSchemeHeader.HasRows)
                {
                    int schemeHeaderIdOrdinal = drSchemeHeader.GetOrdinal("scheme_header_id");
                    int schemeNameOrdinal = drSchemeHeader.GetOrdinal("scheme_name");
                    int isActiveOrdinal = drSchemeHeader.GetOrdinal("is_active");
                    int startDateOrdinal = drSchemeHeader.GetOrdinal("start_date");
                    int endDateOrdinal = drSchemeHeader.GetOrdinal("end_date");
                    int divisionIdOrdinal = drSchemeHeader.GetOrdinal("division_id");
                    int divisionNameOrdinal = drSchemeHeader.GetOrdinal("division_name");
                    int totalCountOrdinal = drSchemeHeader.GetOrdinal("total_count");

                    while (drSchemeHeader.Read())
                    {
                        SchemeHeaderEntity schemeHeader = SchemeHeaderEntity.CreateObject();

                        if (!drSchemeHeader.IsDBNull(schemeHeaderIdOrdinal)) schemeHeader.SchemeHeaderId = drSchemeHeader.GetInt32(schemeHeaderIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(schemeNameOrdinal)) schemeHeader.SchemeName = drSchemeHeader.GetString(schemeNameOrdinal);
                        if (!drSchemeHeader.IsDBNull(isActiveOrdinal)) schemeHeader.IsActive = drSchemeHeader.GetBoolean(isActiveOrdinal);
                        if (!drSchemeHeader.IsDBNull(startDateOrdinal)) schemeHeader.StartDate = drSchemeHeader.GetDateTime(startDateOrdinal);
                        if (!drSchemeHeader.IsDBNull(endDateOrdinal)) schemeHeader.EndDate = drSchemeHeader.GetDateTime(endDateOrdinal);
                        if (!drSchemeHeader.IsDBNull(divisionIdOrdinal)) schemeHeader.DivisionId = drSchemeHeader.GetInt32(divisionIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(divisionNameOrdinal)) schemeHeader.DivisionName = drSchemeHeader.GetString(divisionNameOrdinal);
                        if (!drSchemeHeader.IsDBNull(totalCountOrdinal)) schemeHeader.TotalCount = drSchemeHeader.GetInt32(totalCountOrdinal);

                        schemeHeaderCollection.Add(schemeHeader);

                    }
                }

                return schemeHeaderCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drSchemeHeader != null)
                    drSchemeHeader.Close();
            }
        }

        public SchemeHeaderEntity GetSchemeHeaderById(int schemeHeaderId)
        {
            DbDataReader drSchemeHeader = null;
           

            try
            {
                SchemeHeaderEntity schemeHeader = SchemeHeaderEntity.CreateObject(); ;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@SchemeHeaderId", schemeHeaderId, DbType.Int32);

                drSchemeHeader = this.DataAcessService.ExecuteQuery(SchemeHeaderSql["GetSchemeHeaderById"], paramCollection);

                if (drSchemeHeader != null && drSchemeHeader.HasRows)
                {
                    int schemeHeaderIdOrdinal = drSchemeHeader.GetOrdinal("scheme_header_id");
                    int schemeNameOrdinal = drSchemeHeader.GetOrdinal("scheme_name");
                    int startDateOrdinal = drSchemeHeader.GetOrdinal("start_date");
                    int endDateOrdinal = drSchemeHeader.GetOrdinal("end_date");
                    int divisionIdOrdinal = drSchemeHeader.GetOrdinal("division_id");
                    int divisionNameOrdinal = drSchemeHeader.GetOrdinal("division_name");
                    int isActiveOrdinal = drSchemeHeader.GetOrdinal("is_active");  

                    while (drSchemeHeader.Read())
                    {
                        if (!drSchemeHeader.IsDBNull(schemeHeaderIdOrdinal)) schemeHeader.SchemeHeaderId = drSchemeHeader.GetInt32(schemeHeaderIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(schemeNameOrdinal)) schemeHeader.SchemeName = drSchemeHeader.GetString(schemeNameOrdinal);                        
                        if (!drSchemeHeader.IsDBNull(isActiveOrdinal)) schemeHeader.IsActive = drSchemeHeader.GetBoolean(isActiveOrdinal);
                        if (!drSchemeHeader.IsDBNull(startDateOrdinal)) schemeHeader.StartDate = drSchemeHeader.GetDateTime(startDateOrdinal);
                        if (!drSchemeHeader.IsDBNull(divisionIdOrdinal)) schemeHeader.DivisionId = drSchemeHeader.GetInt32(divisionIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(endDateOrdinal)) schemeHeader.EndDate = drSchemeHeader.GetDateTime(endDateOrdinal);
                        if (!drSchemeHeader.IsDBNull(divisionNameOrdinal)) schemeHeader.DivisionName = drSchemeHeader.GetString(divisionNameOrdinal);
                    }
                }

                return schemeHeader;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drSchemeHeader != null)
                    drSchemeHeader.Close();
            }
        }

        public List<SchemeHeaderEntity> GetSchemeHeaderByName(string name,bool isActive)
        {
            DbDataReader drSchemeHeader = null;
            List<SchemeHeaderEntity> schemaList = new List<SchemeHeaderEntity>();

            try
            {
                SchemeHeaderEntity schemeHeader = null;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Name", name, DbType.String);
                paramCollection.Add("@IsActive", isActive, DbType.Boolean);

                drSchemeHeader = this.DataAcessService.ExecuteQuery(SchemeHeaderSql["GetSchemeHeaderByName"], paramCollection);

                if (drSchemeHeader != null && drSchemeHeader.HasRows)
                {
                    int schemeHeaderIdOrdinal = drSchemeHeader.GetOrdinal("scheme_header_id");
                    int schemeNameOrdinal = drSchemeHeader.GetOrdinal("scheme_name");
                    int startDateOrdinal = drSchemeHeader.GetOrdinal("start_date");
                    int endDateOrdinal = drSchemeHeader.GetOrdinal("end_date");
                    int divisionIdOrdinal = drSchemeHeader.GetOrdinal("division_id");
                    int isActiveOrdinal = drSchemeHeader.GetOrdinal("is_active");  

                    while (drSchemeHeader.Read())
                    {
                        schemeHeader = SchemeHeaderEntity.CreateObject();

                        if (!drSchemeHeader.IsDBNull(schemeHeaderIdOrdinal)) schemeHeader.SchemeHeaderId = drSchemeHeader.GetInt32(schemeHeaderIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(schemeNameOrdinal)) schemeHeader.SchemeName = drSchemeHeader.GetString(schemeNameOrdinal);                        
                        if (!drSchemeHeader.IsDBNull(isActiveOrdinal)) schemeHeader.IsActive = drSchemeHeader.GetBoolean(isActiveOrdinal);
                        if (!drSchemeHeader.IsDBNull(startDateOrdinal)) schemeHeader.StartDate = drSchemeHeader.GetDateTime(startDateOrdinal);
                        if (!drSchemeHeader.IsDBNull(divisionIdOrdinal)) schemeHeader.DivisionId = drSchemeHeader.GetInt32(divisionIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(endDateOrdinal)) schemeHeader.EndDate = drSchemeHeader.GetDateTime(endDateOrdinal);

                        schemaList.Add(schemeHeader);

                    }
                }

                return schemaList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drSchemeHeader != null)
                    drSchemeHeader.Close();
            }
        }

        
        

        public bool IsSchemeHeaderNameExists(SchemeHeaderEntity schemeHeaderEntity)
        {
            DbDataReader drScheme = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();


                paramCollection.Add("@SchemeHeaderId", schemeHeaderEntity.SchemeHeaderId, DbType.Int32);
                paramCollection.Add("@SchemeHeaderName", schemeHeaderEntity.SchemeName, DbType.String);

                drScheme = this.DataAcessService.ExecuteQuery(SchemeHeaderSql["SchemeHeaderNameExists"], paramCollection);

                if (drScheme != null && drScheme.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drScheme != null)
                    drScheme.Close();
            }
        }

        public bool UpdateSchemeHeaderIsActive(SchemeHeaderEntity schemeHeader)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@SchemeHeaderId", schemeHeader.SchemeHeaderId, DbType.Int32);
                paramCollection.Add("@IsActive", schemeHeader.IsActive, DbType.Boolean);
                paramCollection.Add("@LastModifiedBy", schemeHeader.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(SchemeHeaderSql["UpdateSchemeHeaderIsActive"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return UpdateSuccessful;
        }


        public SchemeHeaderEntity GetAllSchemeHeaderByDivisionId(int divisionId)
        {
            DbDataReader drSchemeHeader = null;


            try
            {
                SchemeHeaderEntity schemeHeader = SchemeHeaderEntity.CreateObject(); ;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@DivisionId", divisionId, DbType.Int32);

                drSchemeHeader = this.DataAcessService.ExecuteQuery(SchemeHeaderSql["GetAllSchemeHeaderByDivisionId"], paramCollection);

                if (drSchemeHeader != null && drSchemeHeader.HasRows)
                {
                    int schemeHeaderIdOrdinal = drSchemeHeader.GetOrdinal("scheme_header_id");
                    int schemeNameOrdinal = drSchemeHeader.GetOrdinal("scheme_name");
                    int startDateOrdinal = drSchemeHeader.GetOrdinal("start_date");
                    int endDateOrdinal = drSchemeHeader.GetOrdinal("end_date");
                    int divisionIdOrdinal = drSchemeHeader.GetOrdinal("division_id");
                    int isActiveOrdinal = drSchemeHeader.GetOrdinal("is_active");

                    while (drSchemeHeader.Read())
                    {
                        if (!drSchemeHeader.IsDBNull(schemeHeaderIdOrdinal)) schemeHeader.SchemeHeaderId = drSchemeHeader.GetInt32(schemeHeaderIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(schemeNameOrdinal)) schemeHeader.SchemeName = drSchemeHeader.GetString(schemeNameOrdinal);
                        if (!drSchemeHeader.IsDBNull(isActiveOrdinal)) schemeHeader.IsActive = drSchemeHeader.GetBoolean(isActiveOrdinal);
                        if (!drSchemeHeader.IsDBNull(startDateOrdinal)) schemeHeader.StartDate = drSchemeHeader.GetDateTime(startDateOrdinal);
                        if (!drSchemeHeader.IsDBNull(divisionIdOrdinal)) schemeHeader.DivisionId = drSchemeHeader.GetInt32(divisionIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(endDateOrdinal)) schemeHeader.EndDate = drSchemeHeader.GetDateTime(endDateOrdinal);
                    }
                }

                return schemeHeader;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drSchemeHeader != null)
                    drSchemeHeader.Close();
            }
        }

        public SchemeHeaderEntity GetSchemeHeaderByAccessToken(string accessToken)
        {
            //DbDataReader drSchemeHeader = null;

            //try
            //{
            //    SchemeHeaderEntity schemeHeader = SchemeHeaderEntity.CreateObject(); ;

            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@AccessToken", accessToken, DbType.String);

            //    drSchemeHeader = this.DataAcessService.ExecuteQuery(SchemeHeaderSql["GetSchemeHeaderByAccessToken"], paramCollection);

            //    if (drSchemeHeader != null && drSchemeHeader.HasRows)
            //    {
            //        int schemeHeaderIdOrdinal = drSchemeHeader.GetOrdinal("scheme_header_id");
            //        int schemeNameOrdinal = drSchemeHeader.GetOrdinal("scheme_name");
            //        int startDateOrdinal = drSchemeHeader.GetOrdinal("start_date");
            //        int endDateOrdinal = drSchemeHeader.GetOrdinal("end_date");
            //        int divisionIdOrdinal = drSchemeHeader.GetOrdinal("division_id");
            //        int divisionNameOrdinal = drSchemeHeader.GetOrdinal("division_name");
            //        int isActiveOrdinal = drSchemeHeader.GetOrdinal("is_active");

            //        while (drSchemeHeader.Read())
            //        {
            //            if (!drSchemeHeader.IsDBNull(schemeHeaderIdOrdinal)) schemeHeader.SchemeHeaderId = drSchemeHeader.GetInt32(schemeHeaderIdOrdinal);
            //            if (!drSchemeHeader.IsDBNull(schemeNameOrdinal)) schemeHeader.SchemeName = drSchemeHeader.GetString(schemeNameOrdinal);
            //            if (!drSchemeHeader.IsDBNull(isActiveOrdinal)) schemeHeader.IsActive = drSchemeHeader.GetBoolean(isActiveOrdinal);
            //            if (!drSchemeHeader.IsDBNull(startDateOrdinal)) schemeHeader.StartDate = drSchemeHeader.GetDateTime(startDateOrdinal);
            //            if (!drSchemeHeader.IsDBNull(divisionIdOrdinal)) schemeHeader.DivisionId = drSchemeHeader.GetInt32(divisionIdOrdinal);
            //            if (!drSchemeHeader.IsDBNull(endDateOrdinal)) schemeHeader.EndDate = drSchemeHeader.GetDateTime(endDateOrdinal);
            //            if (!drSchemeHeader.IsDBNull(divisionNameOrdinal)) schemeHeader.DivisionName = drSchemeHeader.GetString(divisionNameOrdinal);
            //        }
            //    }

            //    return schemeHeader;
            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (drSchemeHeader != null)
            //        drSchemeHeader.Close();
            //}

            //*************************Add By Irosh Fernando 2018/10/3************************************

            SchemeHeaderEntity schemeHeader = SchemeHeaderEntity.CreateObject();
            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetSchemeHeaderByAccessToken";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        if (objDS.Rows[0]["scheme_header_id"] != DBNull.Value) schemeHeader.SchemeHeaderId = Convert.ToInt32(objDS.Rows[0]["scheme_header_id"]);
                        if (objDS.Rows[0]["scheme_name"] != DBNull.Value) schemeHeader.SchemeName = objDS.Rows[0]["scheme_name"].ToString();
                        if (objDS.Rows[0]["is_active"] != DBNull.Value) schemeHeader.IsActive = Convert.ToBoolean(objDS.Rows[0]["is_active"]);
                        if (objDS.Rows[0]["start_date"] != DBNull.Value) schemeHeader.StartDate = Convert.ToDateTime(objDS.Rows[0]["start_date"]);
                        if (objDS.Rows[0]["division_id"] != DBNull.Value) schemeHeader.DivisionId = Convert.ToInt32(objDS.Rows[0]["division_id"]);
                        if (objDS.Rows[0]["end_date"] != DBNull.Value) schemeHeader.EndDate = Convert.ToDateTime(objDS.Rows[0]["end_date"]);
                        if (objDS.Rows[0]["division_name"] != DBNull.Value) schemeHeader.DivisionName = objDS.Rows[0]["division_name"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return schemeHeader;

            //*************************Add By Irosh Fernando 2018/10/3************************************
        }

        public SchemeHeaderEntity GetSchemeHeaderForPreSalesByAccessToken(string accessToken)
        {
            SchemeHeaderEntity schemeHeader = SchemeHeaderEntity.CreateObject();
            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetSchemeHeaderForPreSalesByAccessToken";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        if (objDS.Rows[0]["scheme_header_id"] != DBNull.Value) schemeHeader.SchemeHeaderId = Convert.ToInt32(objDS.Rows[0]["scheme_header_id"]);
                        if (objDS.Rows[0]["scheme_name"] != DBNull.Value) schemeHeader.SchemeName = objDS.Rows[0]["scheme_name"].ToString();
                        if (objDS.Rows[0]["is_active"] != DBNull.Value) schemeHeader.IsActive = Convert.ToBoolean(objDS.Rows[0]["is_active"]);
                        if (objDS.Rows[0]["start_date"] != DBNull.Value) schemeHeader.StartDate = Convert.ToDateTime(objDS.Rows[0]["start_date"]);
                        if (objDS.Rows[0]["division_id"] != DBNull.Value) schemeHeader.DivisionId = Convert.ToInt32(objDS.Rows[0]["division_id"]);
                        if (objDS.Rows[0]["end_date"] != DBNull.Value) schemeHeader.EndDate = Convert.ToDateTime(objDS.Rows[0]["end_date"]);
                        if (objDS.Rows[0]["division_name"] != DBNull.Value) schemeHeader.DivisionName = objDS.Rows[0]["division_name"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return schemeHeader;
        }

        //public List<SchemeHeaderEntity> GetSchemeHeaderByAccessToken(string accessToken)
        //{
        //    DbDataReader drSchemeHeader = null;


        //    try
        //    {
        //        List<SchemeHeaderEntity> schemeHeaderList = new List<SchemeHeaderEntity>();
        //        SchemeHeaderEntity schemeHeader = SchemeHeaderEntity.CreateObject(); 

        //        DbInputParameterCollection paramCollection = new DbInputParameterCollection();
        //        paramCollection.Add("@AccessToken", accessToken, DbType.String);

        //        drSchemeHeader = this.DataAcessService.ExecuteQuery(SchemeHeaderSql["GetSchemeHeaderByAccessToken"], paramCollection);

        //        if (drSchemeHeader != null && drSchemeHeader.HasRows)
        //        {
        //            int schemeHeaderIdOrdinal = drSchemeHeader.GetOrdinal("scheme_header_id");
        //            int schemeNameOrdinal = drSchemeHeader.GetOrdinal("scheme_name");
        //            int startDateOrdinal = drSchemeHeader.GetOrdinal("start_date");
        //            int endDateOrdinal = drSchemeHeader.GetOrdinal("end_date");
        //            int divisionIdOrdinal = drSchemeHeader.GetOrdinal("division_id");
        //            int divisionNameOrdinal = drSchemeHeader.GetOrdinal("division_name");
        //            int isActiveOrdinal = drSchemeHeader.GetOrdinal("is_active");

        //            while (drSchemeHeader.Read())
        //            {
        //                if (!drSchemeHeader.IsDBNull(schemeHeaderIdOrdinal)) schemeHeader.SchemeHeaderId = drSchemeHeader.GetInt32(schemeHeaderIdOrdinal);
        //                if (!drSchemeHeader.IsDBNull(schemeNameOrdinal)) schemeHeader.SchemeName = drSchemeHeader.GetString(schemeNameOrdinal);
        //                if (!drSchemeHeader.IsDBNull(isActiveOrdinal)) schemeHeader.IsActive = drSchemeHeader.GetBoolean(isActiveOrdinal);
        //                if (!drSchemeHeader.IsDBNull(startDateOrdinal)) schemeHeader.StartDate = drSchemeHeader.GetDateTime(startDateOrdinal);
        //                if (!drSchemeHeader.IsDBNull(divisionIdOrdinal)) schemeHeader.DivisionId = drSchemeHeader.GetInt32(divisionIdOrdinal);
        //                if (!drSchemeHeader.IsDBNull(endDateOrdinal)) schemeHeader.EndDate = drSchemeHeader.GetDateTime(endDateOrdinal);
        //                if (!drSchemeHeader.IsDBNull(divisionNameOrdinal)) schemeHeader.DivisionName = drSchemeHeader.GetString(divisionNameOrdinal);

        //                schemeHeaderList.Add(schemeHeader);
        //            }
        //        }

        //        return schemeHeaderList;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    finally
        //    {
        //        if (drSchemeHeader != null)
        //            drSchemeHeader.Close();
        //    }
        //}


        #region - Report Methods -
        public TradeLoadDataSet GetTradeLoadSummary(ArgsEntity args)
        {
            DbDataAdapter dapAppointment = null;

            try
            {

                //Instantiate report specific data set.
                TradeLoadDataSet dsTradeLoad = new TradeLoadDataSet();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@StartDate", args.DtStartDate, DbType.DateTime);
                paramCollection.Add("@EndDate", args.DtEndDate, DbType.DateTime);

                paramCollection.Add("@BrandId", args.BrandId, DbType.Int32);
                paramCollection.Add("@PackingId", args.PackingId, DbType.Int32);
                paramCollection.Add("@FlavorId", args.FlavorId, DbType.Int32);
                paramCollection.Add("@CategoryId", args.CategoryId, DbType.Int32);
                paramCollection.Add("@Hvp", args.Hvp, DbType.Boolean);

                paramCollection.Add("@GroupCol", args.@GroupCol, DbType.String);
                paramCollection.Add("@RepCodeList", args.ChildOriginators.Replace("originator", "rep_code"), DbType.String);

                


                //Retreive the data adapter to fill the report data
                dapAppointment = this.DataAcessService.ExecuteReportQuery(SchemeHeaderSql["GetTradeLoadSummary"], paramCollection);

                //Fill the report specific data set
                dapAppointment.Fill(dsTradeLoad, dsTradeLoad.TradeLoadSummary.TableName);

                return dsTradeLoad;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dapAppointment != null)
                    dapAppointment.Dispose();
            }
        }
        #endregion


        public List<SchemeHeaderModel> GetAllDiscountSchemeHeader(ArgsModel args)
        {
            DbDataReader drSchemeHeader = null;
            try
            {
                List<SchemeHeaderModel> schemeHeaderCollection = new List<SchemeHeaderModel>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@StartDate", args.SStartDate, DbType.String);
                paramCollection.Add("@EndDate", args.SEndDate, DbType.String);
                paramCollection.Add("@IsActive", !args.ActiveInactiveChecked, DbType.Boolean);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                drSchemeHeader = this.DataAcessService.ExecuteQuery(SchemeHeaderSql["GetAllSchemeHeader"], paramCollection);

                if (drSchemeHeader != null && drSchemeHeader.HasRows)
                {
                    int schemeHeaderIdOrdinal = drSchemeHeader.GetOrdinal("scheme_header_id");
                    int schemeNameOrdinal = drSchemeHeader.GetOrdinal("scheme_name");
                    int isActiveOrdinal = drSchemeHeader.GetOrdinal("is_active");
                    int startDateOrdinal = drSchemeHeader.GetOrdinal("start_date");
                    int endDateOrdinal = drSchemeHeader.GetOrdinal("end_date");
                    //int divisionIdOrdinal = drSchemeHeader.GetOrdinal("te_id");
                    //int divisionNameOrdinal = drSchemeHeader.GetOrdinal("division_name");
                    int totalCountOrdinal = drSchemeHeader.GetOrdinal("total_count");

                    while (drSchemeHeader.Read())
                    {
                        SchemeHeaderModel schemeHeader = new SchemeHeaderModel();

                        if (!drSchemeHeader.IsDBNull(schemeHeaderIdOrdinal)) schemeHeader.SchemeHeaderId = drSchemeHeader.GetInt32(schemeHeaderIdOrdinal);
                        if (!drSchemeHeader.IsDBNull(schemeNameOrdinal)) schemeHeader.SchemeName = drSchemeHeader.GetString(schemeNameOrdinal);
                        if (!drSchemeHeader.IsDBNull(isActiveOrdinal)) schemeHeader.IsActive = drSchemeHeader.GetBoolean(isActiveOrdinal);
                        if (!drSchemeHeader.IsDBNull(startDateOrdinal)) schemeHeader.StartDate = drSchemeHeader.GetDateTime(startDateOrdinal);
                        if (!drSchemeHeader.IsDBNull(endDateOrdinal)) schemeHeader.EndDate = drSchemeHeader.GetDateTime(endDateOrdinal);
                        //if (!drSchemeHeader.IsDBNull(divisionIdOrdinal)) schemeHeader.TerritoryId = drSchemeHeader.GetInt32(divisionIdOrdinal);
                        //if (!drSchemeHeader.IsDBNull(divisionNameOrdinal)) schemeHeader.TerritoryName = drSchemeHeader.GetString(divisionNameOrdinal);
                        if (!drSchemeHeader.IsDBNull(totalCountOrdinal)) schemeHeader.TotalCount = drSchemeHeader.GetInt32(totalCountOrdinal);

                        schemeHeaderCollection.Add(schemeHeader);

                    }
                }

                return schemeHeaderCollection;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                if (drSchemeHeader != null)
                    drSchemeHeader.Close();
            }
        }

        public bool SaveUpdateDiscountTerritory(string originator,
                                                       string scheme_header_id,
                                                       string territory_id,
                                                       string is_checked)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertUpdateDiscountTerritory";

                    objCommand.Parameters.AddWithValue("@Originator", originator);
                    objCommand.Parameters.AddWithValue("@SchemeHeaderId", scheme_header_id);
                    objCommand.Parameters.AddWithValue("@TerritoryId", territory_id);
                    objCommand.Parameters.AddWithValue("@IsChecked", is_checked);

                    if (territory_id != "0" || territory_id != "")
                    {
                        if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                        if (objCommand.ExecuteNonQuery() > 0)
                        {
                            retStatus = true;
                        }
                        else
                        {
                            retStatus = false;
                        }
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }
    }
}
