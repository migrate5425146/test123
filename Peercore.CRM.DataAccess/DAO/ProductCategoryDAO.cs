﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Entities;
using System.Data.Common;
using System.Collections;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.CRM.Model;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public class ProductCategoryDAO : BaseDAO
    {
        private DbSqlAdapter ProductCategorySql { get; set; }

        private void RegisterSql()
        {
            this.ProductCategorySql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.ProductCategorySql.xml", ApplicationService.Instance.DbProvider);
        }

        public ProductCategoryDAO()
        {
            RegisterSql();
        }

        public ProductCategoryDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public ProductCategoryEntity CreateObject()
        {
            return ProductCategoryEntity.CreateObject();
        }

        public ProductCategoryEntity CreateObject(int entityId)
        {
            return ProductCategoryEntity.CreateObject(entityId);
        }

        public List<ProductCategoryEntity> GetAllProductCategory(ArgsEntity args)
        {
            DbDataReader drProductCategory = null;

            try
            {
                List<ProductCategoryEntity> productcategoryList = new List<ProductCategoryEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", (args.AdditionalParams == null) ? "": args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                drProductCategory = this.DataAcessService.ExecuteQuery(ProductCategorySql["GetAllProductsCategory"], paramCollection);

                if (drProductCategory != null && drProductCategory.HasRows)
                {
                    int idOrdinal = drProductCategory.GetOrdinal("id");
                    int catcodeOrdinal = drProductCategory.GetOrdinal("category_code");
                    int descriptionOrdinal = drProductCategory.GetOrdinal("description");

                    while (drProductCategory.Read())
                    {
                        ProductCategoryEntity productcategoryEntity = CreateObject();

                        if (!drProductCategory.IsDBNull(idOrdinal)) productcategoryEntity.Id = drProductCategory.GetInt32(idOrdinal);
                        if (!drProductCategory.IsDBNull(catcodeOrdinal)) productcategoryEntity.Code = drProductCategory.GetString(catcodeOrdinal);
                        if (!drProductCategory.IsDBNull(descriptionOrdinal)) productcategoryEntity.Description = drProductCategory.GetString(descriptionOrdinal);

                        productcategoryList.Add(productcategoryEntity);
                    }
                }

                return productcategoryList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProductCategory != null)
                    drProductCategory.Close();
            }
        }

        public bool IsProductCategoryCodeExists(ProductCategoryEntity productcategoryEntity)
        {
            DbDataReader drProductCategory = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();


                paramCollection.Add("@Id", productcategoryEntity.Id, DbType.Int32);
                paramCollection.Add("@Code", productcategoryEntity.Code, DbType.String);

                drProductCategory = this.DataAcessService.ExecuteQuery(ProductCategorySql["ProductCategoryCodeExists"], paramCollection);

                if (drProductCategory != null && drProductCategory.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProductCategory != null)
                    drProductCategory.Close();
            }
        }

        public bool IsProductCategoryNameExists(ProductCategoryEntity productcategoryEntity)
        {
            DbDataReader drProductCategory = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Id", productcategoryEntity.Id, DbType.Int32);
                paramCollection.Add("@Description", productcategoryEntity.Description, DbType.String);

                drProductCategory = this.DataAcessService.ExecuteQuery(ProductCategorySql["ProductCategoryNameExists"], paramCollection);

                if (drProductCategory != null && drProductCategory.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProductCategory != null)
                    drProductCategory.Close();
            }
        }

        public bool InsertProductCategory(ProductCategoryEntity productcategoryEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drProductCategory = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Code", productcategoryEntity.Code, DbType.String);
                paramCollection.Add("@Description", productcategoryEntity.Description, DbType.String);
                //paramCollection.Add("@CreatedBy", brandEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ProductCategorySql["InsertProductCategory"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drProductCategory != null)
                    drProductCategory.Close();
            }
        }

        public bool UpdateProductCategory(ProductCategoryEntity productcategoryEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drProductCategory = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Id", productcategoryEntity.Id, DbType.Int32);
                paramCollection.Add("@Code", productcategoryEntity.Code, DbType.String);
                paramCollection.Add("@Description", productcategoryEntity.Description, DbType.String);
                //paramCollection.Add("@LastModifiedBy", brandEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ProductCategorySql["UpdateProductCategory"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drProductCategory != null)
                    drProductCategory.Close();
            }
        }

        public bool DeleteProductCategory(ProductCategoryEntity productcategoryEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drProductCategory = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Id", productcategoryEntity.Id, DbType.Int32);
                
                iNoRec = this.DataAcessService.ExecuteNonQuery(ProductCategorySql["DeleteProductCategory"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drProductCategory != null)
                    drProductCategory.Close();
            }
        }



        public List<CategoryECOModel> GetCategoryECOCumulativeByAccessToken(string accessToken, string strDate, string endDate)
        {
            CategoryECOModel categoryEntity = null;
            DataTable objDS = new DataTable();
            List<CategoryECOModel> CategoryList = new List<CategoryECOModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetCategoryECOCumulative";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);
                    objCommand.Parameters.AddWithValue("@StartDate", strDate);
                    objCommand.Parameters.AddWithValue("@EndDate", endDate);



                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            categoryEntity = new CategoryECOModel();

                            if (item["CatCd"] != DBNull.Value) categoryEntity.CategoryCode = item["CatCd"].ToString();

                            if (item["CatNm"] != DBNull.Value) categoryEntity.CategoryName = item["CatNm"].ToString();

                            if (item["isPC"] != DBNull.Value)
                            {
                                string isPCValue = item["isPC"].ToString();
                                if (isPCValue == "1" || isPCValue.Equals("true", StringComparison.OrdinalIgnoreCase))
                                {
                                    categoryEntity.isPC = true;
                                }
                                else if (isPCValue == "0" || isPCValue.Equals("false", StringComparison.OrdinalIgnoreCase))
                                {
                                    categoryEntity.isPC = false;
                                }
                                else
                                {
                                    // Handle any other cases or set a default value
                                    categoryEntity.isPC = false; // Or set a default value
                                }
                            }






                            CategoryList.Add(categoryEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return CategoryList;
        }







    }
}
