﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public class InvoiceHeaderDAO:BaseDAO
    {
        private DbSqlAdapter InvoiceHeaderSql { get; set; }

        private void RegisterSql()
        {
            this.InvoiceHeaderSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.InvoiceHeaderSql.xml", ApplicationService.Instance.DbProvider);
        }

        public InvoiceHeaderDAO()
        {
            RegisterSql();
        }

        public InvoiceHeaderDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public InvoiceHeaderEntity CreateObject()
        {
            return InvoiceHeaderEntity.CreateObject();
        }

        public List<InvoiceHeaderEntity> GetOutstandingInvoicesForCustomer(string custCode, DateTime fromDate, DateTime toDate)
        {
            DbDataReader drInvoiceHeader = null;

            try
            {
                List<InvoiceHeaderEntity> InvoiceHeaderList = new List<InvoiceHeaderEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CustCode", custCode, DbType.String);
                paramCollection.Add("@FromDate", fromDate.Date.ToString("yyyy-MM-dd"), DbType.String);
                paramCollection.Add("@ToDate", toDate.Date.ToString("yyyy-MM-dd"), DbType.String);
                
                drInvoiceHeader = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["GetOutstandingInvoicesForCustomer"], paramCollection);

                if (drInvoiceHeader != null && drInvoiceHeader.HasRows)
                {
                    int ivceNoOrdinal = drInvoiceHeader.GetOrdinal("invoice_no");
                    int ivceIdOrdinal = drInvoiceHeader.GetOrdinal("id");
                    int custCodeOrdinal = drInvoiceHeader.GetOrdinal("cust_code");
                    int ivceDateOrdinal = drInvoiceHeader.GetOrdinal("invoice_date");
                    int grossOrdinal = drInvoiceHeader.GetOrdinal("gross_total");
                    int discountOrdinal = drInvoiceHeader.GetOrdinal("discount_total");
                    int returnsTotalOrdinal = drInvoiceHeader.GetOrdinal("returns_total");
                    int grandTotalOrdinal = drInvoiceHeader.GetOrdinal("grand_total");
                    int statusOrdinal = drInvoiceHeader.GetOrdinal("status");
                    InvoiceHeaderEntity invoiceHeaderEntity = null;

                    while (drInvoiceHeader.Read())
                    {
                        invoiceHeaderEntity = CreateObject();
                        if (!drInvoiceHeader.IsDBNull(ivceNoOrdinal)) invoiceHeaderEntity.IvceNo = drInvoiceHeader.GetInt32(ivceNoOrdinal);
                        if (!drInvoiceHeader.IsDBNull(ivceIdOrdinal)) invoiceHeaderEntity.IvceId = drInvoiceHeader.GetInt32(ivceIdOrdinal);
                        if (!drInvoiceHeader.IsDBNull(custCodeOrdinal)) invoiceHeaderEntity.CustCode = drInvoiceHeader.GetString(custCodeOrdinal);                       
                        if (!drInvoiceHeader.IsDBNull(ivceDateOrdinal)) invoiceHeaderEntity.IvceDate = drInvoiceHeader.GetDateTime(ivceDateOrdinal).ToString("yyyy-MM-dd HH:mm");
                        if (!drInvoiceHeader.IsDBNull(grossOrdinal)) invoiceHeaderEntity.GrossTotal = drInvoiceHeader.GetDouble(grossOrdinal);
                        if (!drInvoiceHeader.IsDBNull(discountOrdinal)) invoiceHeaderEntity.Discount = drInvoiceHeader.GetDouble(discountOrdinal);
                        if (!drInvoiceHeader.IsDBNull(returnsTotalOrdinal)) invoiceHeaderEntity.ReturnsTotal = drInvoiceHeader.GetDouble(returnsTotalOrdinal);
                        if (!drInvoiceHeader.IsDBNull(grandTotalOrdinal)) invoiceHeaderEntity.Grandtotal = drInvoiceHeader.GetDouble(grandTotalOrdinal);
                        if (!drInvoiceHeader.IsDBNull(statusOrdinal)) invoiceHeaderEntity.Status = drInvoiceHeader.GetString(statusOrdinal);
                        InvoiceHeaderList.Add(invoiceHeaderEntity);
                    }
                }

                return InvoiceHeaderList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drInvoiceHeader != null)
                    drInvoiceHeader.Close();
            }
        }

        public List<InvoiceHeaderEntity> GetInvoicesByAccessToken(string accessToken)
        {
            //DbDataReader drInvoiceHeader = null;

            //try
            //{
            //    List<InvoiceHeaderEntity> InvoiceHeaderList = new List<InvoiceHeaderEntity>();
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@AccessToken", accessToken, DbType.String);

            //    drInvoiceHeader = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["GetInvoicesByAccessToken"], paramCollection);

            //    if (drInvoiceHeader != null && drInvoiceHeader.HasRows)
            //    {
            //        int ivceNoOrdinal = drInvoiceHeader.GetOrdinal("invoice_no");
            //        int ivceIdOrdinal = drInvoiceHeader.GetOrdinal("id");
            //        int custCodeOrdinal = drInvoiceHeader.GetOrdinal("cust_code");
            //        int ivceDateOrdinal = drInvoiceHeader.GetOrdinal("invoice_date");
            //        int grossOrdinal = drInvoiceHeader.GetOrdinal("gross_total");
            //        int discountOrdinal = drInvoiceHeader.GetOrdinal("discount_total");
            //        int returnsTotalOrdinal = drInvoiceHeader.GetOrdinal("returns_total");
            //        int grandTotalOrdinal = drInvoiceHeader.GetOrdinal("grand_total");
            //        int route_nameOrdinal = drInvoiceHeader.GetOrdinal("route_name");
            //        int distributornameOrdinal = drInvoiceHeader.GetOrdinal("distributorname");
            //        int statusOrdinal = drInvoiceHeader.GetOrdinal("status");
            //        int route_idOrdinal = drInvoiceHeader.GetOrdinal("route_id");

            //        InvoiceHeaderEntity invoiceHeaderEntity = null;

            //        while (drInvoiceHeader.Read())
            //        {
            //            invoiceHeaderEntity = CreateObject();
            //            if (!drInvoiceHeader.IsDBNull(ivceNoOrdinal)) invoiceHeaderEntity.InvoiceNo = drInvoiceHeader.GetString(ivceNoOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(ivceIdOrdinal)) invoiceHeaderEntity.IvceId = drInvoiceHeader.GetInt32(ivceIdOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(custCodeOrdinal)) invoiceHeaderEntity.CustCode = drInvoiceHeader.GetString(custCodeOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(ivceDateOrdinal)) invoiceHeaderEntity.IvceDate = drInvoiceHeader.GetDateTime(ivceDateOrdinal).ToString("yyyy-MM-dd HH:mm");
            //            if (!drInvoiceHeader.IsDBNull(grossOrdinal)) invoiceHeaderEntity.GrossTotal = (double)drInvoiceHeader.GetDecimal(grossOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(discountOrdinal)) invoiceHeaderEntity.Discount = (double)drInvoiceHeader.GetDecimal(discountOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(returnsTotalOrdinal)) invoiceHeaderEntity.ReturnsTotal = (double)drInvoiceHeader.GetDecimal(returnsTotalOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(grandTotalOrdinal)) invoiceHeaderEntity.Grandtotal = (double)drInvoiceHeader.GetDecimal(grandTotalOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(route_nameOrdinal)) invoiceHeaderEntity.RouteName = drInvoiceHeader.GetString(route_nameOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(distributornameOrdinal)) invoiceHeaderEntity.DistributorName = drInvoiceHeader.GetString(distributornameOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(statusOrdinal)) invoiceHeaderEntity.Status = drInvoiceHeader.GetString(statusOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(route_idOrdinal)) invoiceHeaderEntity.RouteId = drInvoiceHeader.GetInt32(route_idOrdinal);
            //            InvoiceHeaderList.Add(invoiceHeaderEntity);
            //        }
            //    }

            //    return InvoiceHeaderList;
            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (drInvoiceHeader != null)
            //        drInvoiceHeader.Close();
            //}

            //*************************Add By Irosh Fernando 2018/10/3************************************

            InvoiceHeaderEntity invoiceHeaderEntity = null;
            DataTable objDS = new DataTable();
            List<InvoiceHeaderEntity> InvoiceHeaderList = new List<InvoiceHeaderEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetInvoicesByAccessToken";

                    objCommand.Parameters.AddWithValue("@AccessToken", accessToken);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            invoiceHeaderEntity = CreateObject();
                            if (item["invoice_no"] != DBNull.Value) invoiceHeaderEntity.InvoiceNo = item["invoice_no"].ToString();
                            if (item["id"] != DBNull.Value) invoiceHeaderEntity.IvceId = Convert.ToInt32(item["id"]);
                            if (item["cust_code"] != DBNull.Value) invoiceHeaderEntity.CustCode = item["cust_code"].ToString();
                            if (item["invoice_date"] != DBNull.Value) invoiceHeaderEntity.IvceDate = Convert.ToDateTime(item["invoice_date"]).ToString("yyyy-MM-dd HH:mm");
                            if (item["gross_total"] != DBNull.Value) invoiceHeaderEntity.GrossTotal = Convert.ToDouble(item["gross_total"]);
                            if (item["discount_total"] != DBNull.Value) invoiceHeaderEntity.Discount = Convert.ToDouble(item["discount_total"]);
                            if (item["returns_total"] != DBNull.Value) invoiceHeaderEntity.ReturnsTotal = Convert.ToDouble(item["returns_total"]);
                            if (item["grand_total"] != DBNull.Value) invoiceHeaderEntity.Grandtotal = Convert.ToDouble(item["grand_total"]);
                            if (item["route_name"] != DBNull.Value) invoiceHeaderEntity.RouteName = item["route_name"].ToString();
                            if (item["distributorname"] != DBNull.Value) invoiceHeaderEntity.DistributorName = item["distributorname"].ToString();
                            if (item["status"] != DBNull.Value) invoiceHeaderEntity.Status = item["status"].ToString();
                            if (item["route_id"] != DBNull.Value) invoiceHeaderEntity.RouteId = Convert.ToInt32(item["route_id"]);
                            if (item["order_no"] != DBNull.Value) invoiceHeaderEntity.OrderNo = item["order_no"].ToString();
                            InvoiceHeaderList.Add(invoiceHeaderEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return InvoiceHeaderList;

            //*************************Add By Irosh Fernando 2018/10/3************************************
        }

        public List<InvoiceHeaderEntity> GetInvoicesByAccessTokenForASM(string accessToken, string date)
        {
            DbDataReader drInvoiceHeader = null;

            try
            {
                List<InvoiceHeaderEntity> InvoiceHeaderList = new List<InvoiceHeaderEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AccessToken", accessToken, DbType.String);
                paramCollection.Add("@date", date, DbType.String);

                drInvoiceHeader = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["GetInvoicesByAccessTokenForASM"], paramCollection);

                if (drInvoiceHeader != null && drInvoiceHeader.HasRows)
                {
                    int ivceNoOrdinal = drInvoiceHeader.GetOrdinal("invoice_no");
                    int ivceIdOrdinal = drInvoiceHeader.GetOrdinal("id");
                    int custCodeOrdinal = drInvoiceHeader.GetOrdinal("cust_code");
                    int custNameOrdinal = drInvoiceHeader.GetOrdinal("cust_name");
                    int ivceDateOrdinal = drInvoiceHeader.GetOrdinal("invoice_date");
                    int grossOrdinal = drInvoiceHeader.GetOrdinal("gross_total");
                    int discountOrdinal = drInvoiceHeader.GetOrdinal("discount_total");
                    int returnsTotalOrdinal = drInvoiceHeader.GetOrdinal("returns_total");
                    int grandTotalOrdinal = drInvoiceHeader.GetOrdinal("grand_total");
                    int route_nameOrdinal = drInvoiceHeader.GetOrdinal("route_name");
                    int distributornameOrdinal = drInvoiceHeader.GetOrdinal("distributorname");
                    int latitudeOrdinal = drInvoiceHeader.GetOrdinal("latitude");
                    int longitudeOrdinal = drInvoiceHeader.GetOrdinal("longitude");
                    int statusOrdinal = drInvoiceHeader.GetOrdinal("status");
                    int order_noOrdinal = drInvoiceHeader.GetOrdinal("order_no");
                    InvoiceHeaderEntity invoiceHeaderEntity = null;

                    while (drInvoiceHeader.Read())
                    {
                        invoiceHeaderEntity = CreateObject();
                        if (!drInvoiceHeader.IsDBNull(ivceNoOrdinal)) invoiceHeaderEntity.InvoiceNo = drInvoiceHeader.GetString(ivceNoOrdinal);
                        if (!drInvoiceHeader.IsDBNull(ivceIdOrdinal)) invoiceHeaderEntity.IvceId = drInvoiceHeader.GetInt32(ivceIdOrdinal);
                        if (!drInvoiceHeader.IsDBNull(custCodeOrdinal)) invoiceHeaderEntity.CustCode = drInvoiceHeader.GetString(custCodeOrdinal);
                        if (!drInvoiceHeader.IsDBNull(custNameOrdinal)) invoiceHeaderEntity.CustomerName = drInvoiceHeader.GetString(custNameOrdinal);
                        if (!drInvoiceHeader.IsDBNull(ivceDateOrdinal)) invoiceHeaderEntity.IvceDate = drInvoiceHeader.GetDateTime(ivceDateOrdinal).ToString("yyyy-MM-dd HH:mm"); ;
                        if (!drInvoiceHeader.IsDBNull(grossOrdinal)) invoiceHeaderEntity.GrossTotal = (double)drInvoiceHeader.GetDecimal(grossOrdinal);
                        if (!drInvoiceHeader.IsDBNull(discountOrdinal)) invoiceHeaderEntity.Discount = (double)drInvoiceHeader.GetDecimal(discountOrdinal);
                        if (!drInvoiceHeader.IsDBNull(returnsTotalOrdinal)) invoiceHeaderEntity.ReturnsTotal = (double)drInvoiceHeader.GetDecimal(returnsTotalOrdinal);
                        if (!drInvoiceHeader.IsDBNull(grandTotalOrdinal)) invoiceHeaderEntity.Grandtotal = (double)drInvoiceHeader.GetDecimal(grandTotalOrdinal);
                        if (!drInvoiceHeader.IsDBNull(route_nameOrdinal)) invoiceHeaderEntity.RouteName = drInvoiceHeader.GetString(route_nameOrdinal);
                        if (!drInvoiceHeader.IsDBNull(distributornameOrdinal)) invoiceHeaderEntity.DistributorName = drInvoiceHeader.GetString(distributornameOrdinal);
                        if (!drInvoiceHeader.IsDBNull(latitudeOrdinal)) invoiceHeaderEntity.Latitude = drInvoiceHeader.GetDouble(latitudeOrdinal);
                        if (!drInvoiceHeader.IsDBNull(longitudeOrdinal)) invoiceHeaderEntity.Longitude = drInvoiceHeader.GetDouble(longitudeOrdinal);
                        if (!drInvoiceHeader.IsDBNull(statusOrdinal)) invoiceHeaderEntity.Status = drInvoiceHeader.GetString(statusOrdinal);
                        if (!drInvoiceHeader.IsDBNull(order_noOrdinal)) invoiceHeaderEntity.OrderNo = drInvoiceHeader.GetString(order_noOrdinal);
                        InvoiceHeaderList.Add(invoiceHeaderEntity);
                    }
                }

                return InvoiceHeaderList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drInvoiceHeader != null)
                    drInvoiceHeader.Close();
            }
        }

        public List<RepsTrackingDetailsEntity> RepsTrackingDetailsforASM(string accessToken, string date)
        {
            DbDataReader drInvoiceHeader = null;

            try
            {
                List<RepsTrackingDetailsEntity> InvoiceHeaderList = new List<RepsTrackingDetailsEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AccessToken", accessToken, DbType.String);
                paramCollection.Add("@date", date, DbType.String);

                drInvoiceHeader = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["GetAllRepsTrackingForASM"], paramCollection);

                if (drInvoiceHeader != null && drInvoiceHeader.HasRows)
                {
                    int cust_codeOrdinal = drInvoiceHeader.GetOrdinal("cust_code");
                    int invoice_dateOrdinal = drInvoiceHeader.GetOrdinal("invoice_date");
                    int gross_totalOrdinal = drInvoiceHeader.GetOrdinal("gross_total");
                    int discount_totalOrdinal = drInvoiceHeader.GetOrdinal("discount_total");
                    int longitudeOrdinal = drInvoiceHeader.GetOrdinal("longitude");
                    int latitudeOrdinal = drInvoiceHeader.GetOrdinal("latitude");
                    int rep_codeOrdinal = drInvoiceHeader.GetOrdinal("rep_code");
                    int pcOrdinal = drInvoiceHeader.GetOrdinal("pc");
                    int vcOrdinal = drInvoiceHeader.GetOrdinal("vc");
                    int rep_nameOrdinal = drInvoiceHeader.GetOrdinal("name");
                    int dis_nameOrdinal = drInvoiceHeader.GetOrdinal("dis_name");

                    RepsTrackingDetailsEntity invoiceHeaderEntity = null;

                    while (drInvoiceHeader.Read())
                    {
                        invoiceHeaderEntity = new RepsTrackingDetailsEntity();
                        if (!drInvoiceHeader.IsDBNull(cust_codeOrdinal)) invoiceHeaderEntity.CustomerCode = drInvoiceHeader.GetString(cust_codeOrdinal);
                        if (!drInvoiceHeader.IsDBNull(invoice_dateOrdinal)) invoiceHeaderEntity.InvoiceDate = drInvoiceHeader.GetDateTime(invoice_dateOrdinal).ToString("yyyy-MM-dd hh:mm");
                        if (!drInvoiceHeader.IsDBNull(gross_totalOrdinal)) invoiceHeaderEntity.InvoiceTotal = (double)drInvoiceHeader.GetDecimal(gross_totalOrdinal);
                        if (!drInvoiceHeader.IsDBNull(discount_totalOrdinal)) invoiceHeaderEntity.TotalDiscount = (double)drInvoiceHeader.GetDecimal(discount_totalOrdinal);
                        if (!drInvoiceHeader.IsDBNull(longitudeOrdinal)) invoiceHeaderEntity.Longitude = drInvoiceHeader.GetDouble(longitudeOrdinal);
                        if (!drInvoiceHeader.IsDBNull(latitudeOrdinal)) invoiceHeaderEntity.Latitude = drInvoiceHeader.GetDouble(latitudeOrdinal);
                        if (!drInvoiceHeader.IsDBNull(rep_codeOrdinal)) invoiceHeaderEntity.RepCode = drInvoiceHeader.GetString(rep_codeOrdinal);
                        if (!drInvoiceHeader.IsDBNull(pcOrdinal)) invoiceHeaderEntity.PC = drInvoiceHeader.GetInt32(pcOrdinal);
                        if (!drInvoiceHeader.IsDBNull(vcOrdinal)) invoiceHeaderEntity.VC = drInvoiceHeader.GetInt32(vcOrdinal);
                        if (!drInvoiceHeader.IsDBNull(rep_nameOrdinal)) invoiceHeaderEntity.RepName = drInvoiceHeader.GetString(rep_nameOrdinal);
                        if (!drInvoiceHeader.IsDBNull(dis_nameOrdinal)) invoiceHeaderEntity.DistributorName = drInvoiceHeader.GetString(dis_nameOrdinal);
                        InvoiceHeaderList.Add(invoiceHeaderEntity);
                    }
                }

                return InvoiceHeaderList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drInvoiceHeader != null)
                    drInvoiceHeader.Close();
            }
        }


        public bool InsertInvoiceHeader(out int invoiceId, InvoiceHeaderEntity invoiceHeaderEntity)
        {
            bool successful = false;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CustCode", invoiceHeaderEntity.CustCode, DbType.String);
                paramCollection.Add("@InvoiceNo", invoiceHeaderEntity.InvoiceNo, DbType.String);
                paramCollection.Add("@InvoiceDate", invoiceHeaderEntity.IvceDate, DbType.DateTime);
                paramCollection.Add("@GrossTotal", invoiceHeaderEntity.GrossTotal, DbType.Double);
                paramCollection.Add("@DiscountTotal", invoiceHeaderEntity.Discount, DbType.String);
                paramCollection.Add("@ReturnsTotal", invoiceHeaderEntity.ReturnsTotal, DbType.Double);
                paramCollection.Add("@RepCode", invoiceHeaderEntity.RepCode, DbType.String);
                paramCollection.Add("@GrandTotal", invoiceHeaderEntity.Grandtotal, DbType.Double);
                paramCollection.Add("@Status", invoiceHeaderEntity.Status, DbType.String);
                paramCollection.Add("@Remark", invoiceHeaderEntity.Remark, DbType.String);
                paramCollection.Add("@Longitude", invoiceHeaderEntity.Longitude, DbType.Double);
                paramCollection.Add("@Latitude", invoiceHeaderEntity.Latitude, DbType.Double);
                paramCollection.Add("@RouteId", invoiceHeaderEntity.RouteId, DbType.Int32);
                paramCollection.Add("@TerritoryId", invoiceHeaderEntity.TerritoryId, DbType.Int32);
                paramCollection.Add("@AreaId", invoiceHeaderEntity.AreaId, DbType.Int32);
                paramCollection.Add("@RegionId", invoiceHeaderEntity.RegionId, DbType.Int32);
                paramCollection.Add("@DistributorId", invoiceHeaderEntity.DistributorId, DbType.Int32);
                paramCollection.Add("@AsmId", invoiceHeaderEntity.AsmId, DbType.Int32);
                paramCollection.Add("@RsmId", invoiceHeaderEntity.RsmId, DbType.Int32);
                paramCollection.Add("@OrderNo", invoiceHeaderEntity.OrderNo, DbType.String);

                paramCollection.Add("@InvoiceId", null, DbType.Int32, ParameterDirection.Output);

                invoiceId = this.DataAcessService.ExecuteNonQuery(InvoiceHeaderSql["InsertInvoiceHeader"], paramCollection, true, "@InvoiceId");

                if (invoiceId > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public bool InsertInvoiceProductDiscount(int invoiceId)
        {
            //bool successful = false;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@InvoiceId", invoiceId, DbType.Int32);

                this.DataAcessService.ExecuteNonQuery(InvoiceHeaderSql["InsertInvoiceDetails_with_Discounts"], paramCollection);

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool UpdateInvoiceHeader(out int invoiceId, InvoiceHeaderEntity invoiceHeaderEntity)
        {
            bool successful = false;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CustCode", invoiceHeaderEntity.CustCode, DbType.String);
                paramCollection.Add("@InvoiceNo", invoiceHeaderEntity.InvoiceNo, DbType.String);
                paramCollection.Add("@InvoiceDate", invoiceHeaderEntity.IvceDate, DbType.DateTime);

                paramCollection.Add("@GrossTotal", invoiceHeaderEntity.GrossTotal, DbType.Double);
                paramCollection.Add("@DiscountTotal", invoiceHeaderEntity.Discount, DbType.String);
                paramCollection.Add("@ReturnsTotal", invoiceHeaderEntity.ReturnsTotal, DbType.Double);
                paramCollection.Add("@RepCode", invoiceHeaderEntity.RepCode, DbType.String);
                paramCollection.Add("@GrandTotal", invoiceHeaderEntity.Grandtotal, DbType.String);
                paramCollection.Add("@Status", invoiceHeaderEntity.Status, DbType.String);

                paramCollection.Add("@InvoiceId", null, DbType.Int32, ParameterDirection.Output);

                invoiceId = this.DataAcessService.ExecuteNonQuery(InvoiceHeaderSql["UpdateInvoiceHeader"], paramCollection, true, "@InvoiceId");
                if (invoiceId > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public bool IsInvoiceExist(string invoiceNo)
        {
            DbDataReader drScheme = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@InvoiceNo", invoiceNo, DbType.String);
                drScheme = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["IsInvoiceExist"], paramCollection);

                if (drScheme != null && drScheme.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drScheme != null)
                    drScheme.Close();
            }
        }

        public List<InvoiceHeaderEntity> GetAllInvoicesDataAndCount(ArgsEntity args)
        {
            DbDataReader drInvoiceHeader = null;

            try
            {
                List<InvoiceHeaderEntity> InvoiceHeaderList = new List<InvoiceHeaderEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@EndDate", args.SEndDate +" 23:59:59", DbType.String);
                paramCollection.Add("@StartDate", args.SStartDate +" 00:00:00", DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);

                drInvoiceHeader = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["GetAllInvoicesDataAndCount"], paramCollection);

                if (drInvoiceHeader != null && drInvoiceHeader.HasRows)
                {
                    int ivceNoOrdinal = drInvoiceHeader.GetOrdinal("invoice_no");
                    int ivceIdOrdinal = drInvoiceHeader.GetOrdinal("id");
                    int custCodeOrdinal = drInvoiceHeader.GetOrdinal("cust_code");
                    int ivceDateOrdinal = drInvoiceHeader.GetOrdinal("invoice_date");
                    int grossOrdinal = drInvoiceHeader.GetOrdinal("gross_total");
                    int discountOrdinal = drInvoiceHeader.GetOrdinal("discount_total");
                    int returnsTotalOrdinal = drInvoiceHeader.GetOrdinal("returns_total");
                    int grandTotalOrdinal = drInvoiceHeader.GetOrdinal("grand_total");
                    int statusOrdinal = drInvoiceHeader.GetOrdinal("status");
                    int nameOrdinal = drInvoiceHeader.GetOrdinal("name");
                    int repCodeOrdinal = drInvoiceHeader.GetOrdinal("rep_code");
                    int remarkOrdinal = drInvoiceHeader.GetOrdinal("remark");
                    int totalCountOrdinal = drInvoiceHeader.GetOrdinal("total_count");

                    InvoiceHeaderEntity invoiceHeaderEntity = null;

                    while (drInvoiceHeader.Read())
                    {
                        invoiceHeaderEntity = CreateObject();

                        if (!drInvoiceHeader.IsDBNull(ivceNoOrdinal)) invoiceHeaderEntity.InvoiceNo = drInvoiceHeader.GetString(ivceNoOrdinal);
                        if (!drInvoiceHeader.IsDBNull(ivceIdOrdinal)) invoiceHeaderEntity.IvceId = drInvoiceHeader.GetInt32(ivceIdOrdinal);
                        if (!drInvoiceHeader.IsDBNull(custCodeOrdinal)) invoiceHeaderEntity.CustCode = drInvoiceHeader.GetString(custCodeOrdinal);
                        if (!drInvoiceHeader.IsDBNull(ivceDateOrdinal)) invoiceHeaderEntity.IvceDate = drInvoiceHeader.GetDateTime(ivceDateOrdinal).ToString("yyyy-MM-dd HH:mm"); ;
                        if (!drInvoiceHeader.IsDBNull(grossOrdinal)) invoiceHeaderEntity.GrossTotal = (double)drInvoiceHeader.GetDecimal(grossOrdinal);
                        if (!drInvoiceHeader.IsDBNull(discountOrdinal)) invoiceHeaderEntity.Discount = (double)drInvoiceHeader.GetDecimal(discountOrdinal);
                        if (!drInvoiceHeader.IsDBNull(returnsTotalOrdinal)) invoiceHeaderEntity.ReturnsTotal = (double)drInvoiceHeader.GetDecimal(returnsTotalOrdinal);
                        if (!drInvoiceHeader.IsDBNull(grandTotalOrdinal)) invoiceHeaderEntity.Grandtotal = (double)drInvoiceHeader.GetDecimal(grandTotalOrdinal);
                        if (!drInvoiceHeader.IsDBNull(statusOrdinal)) invoiceHeaderEntity.Status = drInvoiceHeader.GetString(statusOrdinal);
                        if (!drInvoiceHeader.IsDBNull(totalCountOrdinal)) invoiceHeaderEntity.TotalCount = drInvoiceHeader.GetInt32(totalCountOrdinal);
                        if (!drInvoiceHeader.IsDBNull(nameOrdinal)) invoiceHeaderEntity.CustomerName = drInvoiceHeader.GetString(nameOrdinal);
                        if (!drInvoiceHeader.IsDBNull(repCodeOrdinal)) invoiceHeaderEntity.RepCode = drInvoiceHeader.GetString(repCodeOrdinal);
                        if (!drInvoiceHeader.IsDBNull(remarkOrdinal)) invoiceHeaderEntity.Remark = drInvoiceHeader.GetString(remarkOrdinal);
                        

                        InvoiceHeaderList.Add(invoiceHeaderEntity);
                    }
                }

                return InvoiceHeaderList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drInvoiceHeader != null)
                    drInvoiceHeader.Close();
            }
        }

        public bool InsertInvoiceSettlement(out int settlementId, InvoiceSettlementEntity invoiceSettlementEntity)
        {
            bool successful = false;
            //int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                //Add By Irosh for Add Features from Generic Vertion to Perfetti 2017/02/22
                ////////////////////////////////////////////////////////////////////////////
                //paramCollection.Add("@Date", invoiceSettlementEntity.AddedOn, DbType.DateTime);
                //paramCollection.Add("@CashAmount", invoiceSettlementEntity.CashAmount, DbType.Double);
                ////paramCollection.Add("@ChequeAmount", invoiceSettlementEntity.ChequeAmount, DbType.Double);
                ////paramCollection.Add("@ChequeNo", invoiceSettlementEntity.ChequeNo, DbType.String);
                ////paramCollection.Add("@ChequeBank", invoiceSettlementEntity.ChequeBank, DbType.String);
                ////paramCollection.Add("@ChequeDate", invoiceSettlementEntity.ChequeDate, DbType.DateTime);
                //paramCollection.Add("@CustCode", invoiceSettlementEntity.CustCode, DbType.String);
                //paramCollection.Add("@InvoiceId", invoiceSettlementEntity.InvoiceId, DbType.Int32);
                //paramCollection.Add("@SettlementNo", invoiceSettlementEntity.SettlementNo, DbType.String);
                //paramCollection.Add("@SettlementTypeId", invoiceSettlementEntity.SettlementTypeId, DbType.Int32);
                //paramCollection.Add("@DueDate", invoiceSettlementEntity.DueDate, DbType.DateTime);
                //paramCollection.Add("@CreditAmount", invoiceSettlementEntity.CreditAmount, DbType.Double);

                paramCollection.Add("@Date", invoiceSettlementEntity.AddedOn, DbType.DateTime);
                paramCollection.Add("@CashAmount", invoiceSettlementEntity.CashAmount, DbType.Double);
                paramCollection.Add("@ChequeAmount", invoiceSettlementEntity.ChequeAmount, DbType.Double);
                //paramCollection.Add("@ChequeNo", invoiceSettlementEntity.ChequeNo, DbType.String);
                //paramCollection.Add("@ChequeBank", invoiceSettlementEntity.ChequeBank, DbType.String);
                //paramCollection.Add("@ChequeDate", invoiceSettlementEntity.ChequeDate, DbType.DateTime);
                paramCollection.Add("@CustCode", invoiceSettlementEntity.CustCode, DbType.String);
                paramCollection.Add("@InvoiceId", invoiceSettlementEntity.InvoiceId, DbType.Int32);
                paramCollection.Add("@SettlementNo", invoiceSettlementEntity.SettlementNo, DbType.String);
                paramCollection.Add("@SettlementTypeId", invoiceSettlementEntity.SettlementTypeId, DbType.Int32);
                paramCollection.Add("@DueDate", invoiceSettlementEntity.DueDate, DbType.DateTime);
                paramCollection.Add("@CreditAmount", invoiceSettlementEntity.CreditAmount, DbType.Double);

                //Add By Irosh 2015/11/13
                paramCollection.Add("@status_pay", invoiceSettlementEntity.status_pay, DbType.String);
                paramCollection.Add("@latitude", invoiceSettlementEntity.latitude, DbType.String);
                paramCollection.Add("@longitude", invoiceSettlementEntity.longitude, DbType.String);
                paramCollection.Add("@remarks", invoiceSettlementEntity.remarks, DbType.String);
                paramCollection.Add("@credit_settlement_no", invoiceSettlementEntity.credit_settle_no, DbType.String);
                paramCollection.Add("@IsCancel", invoiceSettlementEntity.IsCancel, DbType.String);

                paramCollection.Add("@RepCode", invoiceSettlementEntity.RepCode, DbType.String);

                paramCollection.Add("@RouteId", invoiceSettlementEntity.routeId, DbType.Int32);

                //iNoRec = this.DataAcessService.ExecuteNonQuery(InvoiceHeaderSql["InsertInvoiceSettlement"], paramCollection);

                //if (iNoRec > 0)
                //    successful = true;

                paramCollection.Add("@SettlementId", null, DbType.Int32, ParameterDirection.Output);

                settlementId = this.DataAcessService.ExecuteNonQuery(InvoiceHeaderSql["InsertInvoiceSettlement"], paramCollection, true, "@SettlementId");
                if (settlementId > 0)
                    successful = true;
                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public bool UpdateInvoiceSettlement(InvoiceSettlementEntity invoiceSettlementEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Date", invoiceSettlementEntity.AddedOn, DbType.DateTime);
                paramCollection.Add("@CashAmount", invoiceSettlementEntity.CashAmount, DbType.Double);
                //paramCollection.Add("@ChequeAmount", invoiceSettlementEntity.ChequeAmount, DbType.Double);
                //paramCollection.Add("@ChequeNo", invoiceSettlementEntity.ChequeNo, DbType.String);
                //paramCollection.Add("@ChequeBank", invoiceSettlementEntity.ChequeBank, DbType.String);
                //paramCollection.Add("@ChequeDate", invoiceSettlementEntity.ChequeDate, DbType.DateTime);
                paramCollection.Add("@CustCode", invoiceSettlementEntity.CustCode, DbType.String);
                paramCollection.Add("@InvoiceId", invoiceSettlementEntity.InvoiceId, DbType.Int32);
                paramCollection.Add("@SettlementNo", invoiceSettlementEntity.SettlementNo, DbType.String);
                paramCollection.Add("@SettlementTypeId", invoiceSettlementEntity.SettlementTypeId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(InvoiceHeaderSql["UpdateInvoiceSettlement"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public bool IsInvoiceSettlementExist(string settlementNo)
        {
            DbDataReader drScheme = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@SettlementNo", settlementNo, DbType.String);
                drScheme = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["IsInvoiceSettlementExist"], paramCollection);

                if (drScheme != null && drScheme.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drScheme != null)
                    drScheme.Close();
            }
        }

        public List<InvoiceSettlementEntity> GetInvoiceSettlementByAccessToken(string accessToken)
        {
            DbDataReader drInvoiceHeader = null;

            try
            {
                List<InvoiceSettlementEntity> InvoiceSettlementList = new List<InvoiceSettlementEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AccessToken", accessToken, DbType.String);

                drInvoiceHeader = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["GetInvoiceSettlementByAccessToken"], paramCollection);

                if (drInvoiceHeader != null && drInvoiceHeader.HasRows)
                {
                    int idOrdinal = drInvoiceHeader.GetOrdinal("id");
                    int dateOrdinal = drInvoiceHeader.GetOrdinal("date");
                    int cashAmountOrdinal = drInvoiceHeader.GetOrdinal("cash_amount");
                    //int chequeAmountOrdinal = drInvoiceHeader.GetOrdinal("cheque_amount");
                    //int chequeNoOrdinal = drInvoiceHeader.GetOrdinal("cheque_no");
                    //int chequeBankOrdinal = drInvoiceHeader.GetOrdinal("cheque_bank");
                    //int chequedateOrdinal = drInvoiceHeader.GetOrdinal("cheque_date");
                    int custCodeOrdinal = drInvoiceHeader.GetOrdinal("cust_code");
                    int invoiceIdOrdinal = drInvoiceHeader.GetOrdinal("invoice_id");
                    int settlementNoOrdinal = drInvoiceHeader.GetOrdinal("settlement_no");
                    int settlementTypeIdOrdinal = drInvoiceHeader.GetOrdinal("settlement_type_id");

                    InvoiceSettlementEntity invoiceSettlementEntity = null;

                    while (drInvoiceHeader.Read())
                    {
                        invoiceSettlementEntity = InvoiceSettlementEntity.CreateObject();
                        if (!drInvoiceHeader.IsDBNull(idOrdinal)) invoiceSettlementEntity.InvoiceSettlementId = drInvoiceHeader.GetInt32(idOrdinal);
                        if (!drInvoiceHeader.IsDBNull(dateOrdinal)) invoiceSettlementEntity.AddedOn = drInvoiceHeader.GetDateTime(dateOrdinal).ToString("yyyy-MM-dd HH:mm");
                        if (!drInvoiceHeader.IsDBNull(cashAmountOrdinal)) invoiceSettlementEntity.CashAmount = drInvoiceHeader.GetDouble(cashAmountOrdinal);
                        //if (!drInvoiceHeader.IsDBNull(chequeAmountOrdinal)) invoiceSettlementEntity.ChequeAmount = drInvoiceHeader.GetDouble(chequeAmountOrdinal);
                        //if (!drInvoiceHeader.IsDBNull(chequeNoOrdinal)) invoiceSettlementEntity.ChequeNo = drInvoiceHeader.GetString(chequeNoOrdinal);
                        //if (!drInvoiceHeader.IsDBNull(chequeBankOrdinal)) invoiceSettlementEntity.ChequeBank = drInvoiceHeader.GetString(chequeBankOrdinal);
                        //if (!drInvoiceHeader.IsDBNull(chequedateOrdinal)) invoiceSettlementEntity.ChequeDate = drInvoiceHeader.GetDateTime(chequedateOrdinal);
                        if (!drInvoiceHeader.IsDBNull(custCodeOrdinal)) invoiceSettlementEntity.CustCode = drInvoiceHeader.GetString(custCodeOrdinal);
                        if (!drInvoiceHeader.IsDBNull(invoiceIdOrdinal)) invoiceSettlementEntity.InvoiceId = drInvoiceHeader.GetInt32(invoiceIdOrdinal);
                        if (!drInvoiceHeader.IsDBNull(settlementNoOrdinal)) invoiceSettlementEntity.SettlementNo = drInvoiceHeader.GetString(settlementNoOrdinal);
                        if (!drInvoiceHeader.IsDBNull(settlementTypeIdOrdinal)) invoiceSettlementEntity.SettlementTypeId = drInvoiceHeader.GetInt32(settlementTypeIdOrdinal);
                        InvoiceSettlementList.Add(invoiceSettlementEntity);
                    }
                }

                return InvoiceSettlementList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drInvoiceHeader != null)
                    drInvoiceHeader.Close();
            }
        }

        public InvoiceSettlementEntity GetInvoiceSettlementByInvoiceId(int invoiceId)
        {
            DbDataReader drInvoiceHeader = null;

            try
            {
                //List<InvoiceSettlementEntity> InvoiceSettlementList = new List<InvoiceSettlementEntity>();
                InvoiceSettlementEntity invoiceSettlementEntity = null;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@InvoiceId", invoiceId, DbType.String);

                drInvoiceHeader = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["GetInvoiceSettlementByInvoiceId"], paramCollection);

                if (drInvoiceHeader != null && drInvoiceHeader.HasRows)
                {
                    int idOrdinal = drInvoiceHeader.GetOrdinal("id");
                    int dateOrdinal = drInvoiceHeader.GetOrdinal("date");
                    int cashAmountOrdinal = drInvoiceHeader.GetOrdinal("cash_amount");
                    //int chequeAmountOrdinal = drInvoiceHeader.GetOrdinal("cheque_amount");
                    //int chequeNoOrdinal = drInvoiceHeader.GetOrdinal("cheque_no");
                    //int chequeBankOrdinal = drInvoiceHeader.GetOrdinal("cheque_bank");
                    //int chequedateOrdinal = drInvoiceHeader.GetOrdinal("cheque_date");
                    int custCodeOrdinal = drInvoiceHeader.GetOrdinal("cust_code");
                    int invoiceIdOrdinal = drInvoiceHeader.GetOrdinal("invoice_id");
                    int settlementNoOrdinal = drInvoiceHeader.GetOrdinal("settlement_no");
                    int settlementTypeIdOrdinal = drInvoiceHeader.GetOrdinal("settlement_type_id");
                    
                    while (drInvoiceHeader.Read())
                    {
                        invoiceSettlementEntity = InvoiceSettlementEntity.CreateObject();

                        if (!drInvoiceHeader.IsDBNull(idOrdinal)) invoiceSettlementEntity.InvoiceSettlementId = drInvoiceHeader.GetInt32(idOrdinal);
                        if (!drInvoiceHeader.IsDBNull(dateOrdinal)) invoiceSettlementEntity.AddedOn = drInvoiceHeader.GetDateTime(dateOrdinal).ToString("yyyy-MM-dd HH:mm");
                        if (!drInvoiceHeader.IsDBNull(cashAmountOrdinal)) invoiceSettlementEntity.CashAmount = drInvoiceHeader.GetDouble(cashAmountOrdinal);
                        //if (!drInvoiceHeader.IsDBNull(chequeAmountOrdinal)) invoiceSettlementEntity.ChequeAmount = drInvoiceHeader.GetDouble(chequeAmountOrdinal);
                        //if (!drInvoiceHeader.IsDBNull(chequeNoOrdinal)) invoiceSettlementEntity.ChequeNo = drInvoiceHeader.GetString(chequeNoOrdinal);
                        //if (!drInvoiceHeader.IsDBNull(chequeBankOrdinal)) invoiceSettlementEntity.ChequeBank = drInvoiceHeader.GetString(chequeBankOrdinal);
                        //if (!drInvoiceHeader.IsDBNull(chequedateOrdinal)) invoiceSettlementEntity.ChequeDate = drInvoiceHeader.GetDateTime(chequedateOrdinal);
                        if (!drInvoiceHeader.IsDBNull(custCodeOrdinal)) invoiceSettlementEntity.CustCode = drInvoiceHeader.GetString(custCodeOrdinal);
                        if (!drInvoiceHeader.IsDBNull(invoiceIdOrdinal)) invoiceSettlementEntity.InvoiceId = drInvoiceHeader.GetInt32(invoiceIdOrdinal);
                        if (!drInvoiceHeader.IsDBNull(settlementNoOrdinal)) invoiceSettlementEntity.SettlementNo = drInvoiceHeader.GetString(settlementNoOrdinal);
                        if (!drInvoiceHeader.IsDBNull(settlementTypeIdOrdinal)) invoiceSettlementEntity.SettlementTypeId = drInvoiceHeader.GetInt32(settlementTypeIdOrdinal);
                        //InvoiceSettlementList.Add(invoiceSettlementEntity);
                    }
                }

                return invoiceSettlementEntity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drInvoiceHeader != null)
                    drInvoiceHeader.Close();
            }
        }

        public List<InvoiceSettlementChequeEntity> GetInvoiceSettlementChequeBySettlementId(int settlementId)
        {
            //DbDataReader drInvoiceHeader = null;

            //try
            //{
            //    List<InvoiceSettlementChequeEntity> InvoiceSettlementChequeList = new List<InvoiceSettlementChequeEntity>();

            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@InvoiceSettlementId", settlementId, DbType.String);

            //    drInvoiceHeader = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["GetInvoiceSettlementChequeBySettlementId"], paramCollection);

            //    if (drInvoiceHeader != null && drInvoiceHeader.HasRows)
            //    {
            //        int idOrdinal = drInvoiceHeader.GetOrdinal("id");
            //        int invoiceSettlementIdOrdinal = drInvoiceHeader.GetOrdinal("invoice_settlement_id");
            //        int chequeAmountOrdinal = drInvoiceHeader.GetOrdinal("cheque_amount");
            //        int chequeNoOrdinal = drInvoiceHeader.GetOrdinal("cheque_no");
            //        int chequeBankOrdinal = drInvoiceHeader.GetOrdinal("cheque_bank");
            //        int chequedateOrdinal = drInvoiceHeader.GetOrdinal("cheque_date");

            //        InvoiceSettlementChequeEntity invoiceSettlementEntity = null;

            //        while (drInvoiceHeader.Read())
            //        {
            //            invoiceSettlementEntity = InvoiceSettlementChequeEntity.CreateObject();

            //            if (!drInvoiceHeader.IsDBNull(idOrdinal)) invoiceSettlementEntity.InvoiceSettlementChequeId = drInvoiceHeader.GetInt32(idOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(invoiceSettlementIdOrdinal)) invoiceSettlementEntity.InvoiceSettlementId = drInvoiceHeader.GetInt32(invoiceSettlementIdOrdinal);

            //            if (!drInvoiceHeader.IsDBNull(chequeAmountOrdinal)) invoiceSettlementEntity.ChequeAmount = drInvoiceHeader.GetDouble(chequeAmountOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(chequeNoOrdinal)) invoiceSettlementEntity.ChequeNo = drInvoiceHeader.GetString(chequeNoOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(chequeBankOrdinal)) invoiceSettlementEntity.ChequeBank = drInvoiceHeader.GetString(chequeBankOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(chequedateOrdinal)) invoiceSettlementEntity.ChequeDate = drInvoiceHeader.GetDateTime(chequedateOrdinal).ToString("yyyy-MM-dd HH:mm");


            //            InvoiceSettlementChequeList.Add(invoiceSettlementEntity);
            //        }
            //    }

            //    return InvoiceSettlementChequeList;
            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (drInvoiceHeader != null)
            //        drInvoiceHeader.Close();
            //}

            //*************************Add By Irosh Fernando 2018/10/3************************************

            InvoiceSettlementChequeEntity invoiceSettlementEntity = null;
            DataTable objDS = new DataTable();
            List<InvoiceSettlementChequeEntity> InvoiceSettlementChequeList = new List<InvoiceSettlementChequeEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetInvoiceSettlementChequeBySettlementId";

                    objCommand.Parameters.AddWithValue("@InvoiceSettlementId", settlementId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            invoiceSettlementEntity = InvoiceSettlementChequeEntity.CreateObject();
                            if (item["id"] != DBNull.Value) invoiceSettlementEntity.InvoiceSettlementChequeId = Convert.ToInt32(item["id"]);
                            if (item["invoice_settlement_id"] != DBNull.Value) invoiceSettlementEntity.InvoiceSettlementId = Convert.ToInt32(item["invoice_settlement_id"]);
                            if (item["cheque_amount"] != DBNull.Value) invoiceSettlementEntity.ChequeAmount = Convert.ToDouble(item["cheque_amount"]);
                            if (item["cheque_no"] != DBNull.Value) invoiceSettlementEntity.ChequeNo = item["cheque_no"].ToString();
                            if (item["cheque_bank"] != DBNull.Value) invoiceSettlementEntity.ChequeBank = item["cheque_bank"].ToString();
                            if (item["cheque_date"] != DBNull.Value) invoiceSettlementEntity.ChequeDate = Convert.ToDateTime(item["cheque_date"]).ToString("yyyy-MM-dd HH:mm");
                            InvoiceSettlementChequeList.Add(invoiceSettlementEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return InvoiceSettlementChequeList;

            //*************************Add By Irosh Fernando 2018/10/3************************************
        }

        public bool InsertInvoiceSettlementCheque(InvoiceSettlementChequeEntity invoiceSettlementChequeEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@InvoiceSettlementId", invoiceSettlementChequeEntity.InvoiceSettlementId, DbType.Int32);
                paramCollection.Add("@ChequeAmount", invoiceSettlementChequeEntity.ChequeAmount, DbType.Double);
                paramCollection.Add("@ChequeNo", invoiceSettlementChequeEntity.ChequeNo, DbType.String);
                paramCollection.Add("@ChequeBank", invoiceSettlementChequeEntity.ChequeBank, DbType.String);
                paramCollection.Add("@ChequeDate", invoiceSettlementChequeEntity.ChequeDate, DbType.DateTime);

                iNoRec = this.DataAcessService.ExecuteNonQuery(InvoiceHeaderSql["InsertInvoiceSettlementCheque"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public bool InsertInvoiceSettlementDetails(InvoiceSettlementDetailsEntity invoiceSettlementDetailsEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@credit_settlement_id", invoiceSettlementDetailsEntity.CreditSettlementId, DbType.Int32);
                paramCollection.Add("@invoice_no", invoiceSettlementDetailsEntity.InvoiceNo, DbType.String);
                paramCollection.Add("@invoice_id", invoiceSettlementDetailsEntity.InvoiceId, DbType.Double);
                paramCollection.Add("@outstand_amt", 0, DbType.String);
                paramCollection.Add("@pay_amt", invoiceSettlementDetailsEntity.PayAmount, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(InvoiceHeaderSql["InsertInvoiceSettlementDetails"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public bool UpdateOutstandingForInvoiceSettlement(int settlementId)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@SettlementId", settlementId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(InvoiceHeaderSql["UpdateOutstandingForInvoiceSettlement"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public bool UpdateOutstandingForCancelInvoiceSettlement(int settlementId)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@SettlementId", settlementId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(InvoiceHeaderSql["UpdateOutstandingForCancelInvoiceSettlement"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public bool UpdateOutstandingForInvoice(int InvoiceId)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@InvoiceId", InvoiceId, DbType.Int32);
                iNoRec = this.DataAcessService.ExecuteNonQuery(InvoiceHeaderSql["UpdateOutstandingForInvoice"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }
        
        public bool UpdateOutstandingForDeleteInvoice(int InvoiceId)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@InvoiceId", InvoiceId, DbType.Int32);
                iNoRec = this.DataAcessService.ExecuteNonQuery(InvoiceHeaderSql["UpdateOutstandingForDeleteInvoice"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public InvoiceHeaderEntity GetInvoiceHeaderByInvoiceNo(string invoiceNo)
        {
            DbDataReader drInvoiceHeader = null;
            InvoiceHeaderEntity invoiceHeaderEntity = null;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@InvoiceNo", invoiceNo, DbType.String);

                drInvoiceHeader = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["GetInvoiceHeaderByInvoiceNo"], paramCollection);

                if (drInvoiceHeader != null && drInvoiceHeader.HasRows)
                {
                    int ivceNoOrdinal = drInvoiceHeader.GetOrdinal("invoice_no");
                    int ivceIdOrdinal = drInvoiceHeader.GetOrdinal("id");
                    int custCodeOrdinal = drInvoiceHeader.GetOrdinal("cust_code");
                    int ivceDateOrdinal = drInvoiceHeader.GetOrdinal("invoice_date");
                    int grossOrdinal = drInvoiceHeader.GetOrdinal("gross_total");
                    int discountOrdinal = drInvoiceHeader.GetOrdinal("discount_total");
                    int returnsTotalOrdinal = drInvoiceHeader.GetOrdinal("returns_total");
                    int grandTotalOrdinal = drInvoiceHeader.GetOrdinal("grand_total");

                    int statusOrdinal = drInvoiceHeader.GetOrdinal("status");

                    while (drInvoiceHeader.Read())
                    {
                        invoiceHeaderEntity = CreateObject();

                        if (!drInvoiceHeader.IsDBNull(ivceNoOrdinal)) invoiceHeaderEntity.InvoiceNo = drInvoiceHeader.GetString(ivceNoOrdinal);
                        if (!drInvoiceHeader.IsDBNull(ivceIdOrdinal)) invoiceHeaderEntity.IvceId = drInvoiceHeader.GetInt32(ivceIdOrdinal);
                        if (!drInvoiceHeader.IsDBNull(custCodeOrdinal)) invoiceHeaderEntity.CustCode = drInvoiceHeader.GetString(custCodeOrdinal);
                        if (!drInvoiceHeader.IsDBNull(ivceDateOrdinal)) invoiceHeaderEntity.IvceDate = drInvoiceHeader.GetDateTime(ivceDateOrdinal).ToString("yyyy-MM-dd HH:mm"); ;

                        if (!drInvoiceHeader.IsDBNull(grossOrdinal)) invoiceHeaderEntity.GrossTotal = (double)drInvoiceHeader.GetDecimal(grossOrdinal);
                        if (!drInvoiceHeader.IsDBNull(discountOrdinal)) invoiceHeaderEntity.Discount = (double)drInvoiceHeader.GetDecimal(discountOrdinal);
                        if (!drInvoiceHeader.IsDBNull(returnsTotalOrdinal)) invoiceHeaderEntity.ReturnsTotal = (double)drInvoiceHeader.GetDecimal(returnsTotalOrdinal);
                        if (!drInvoiceHeader.IsDBNull(grandTotalOrdinal)) invoiceHeaderEntity.Grandtotal = (double)drInvoiceHeader.GetDecimal(grandTotalOrdinal);

                        if (!drInvoiceHeader.IsDBNull(statusOrdinal)) invoiceHeaderEntity.Status = drInvoiceHeader.GetString(statusOrdinal);
                    }
                }

                return invoiceHeaderEntity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drInvoiceHeader != null)
                    drInvoiceHeader.Close();
            }
        }

        public bool DeleteInvoiceHeader(InvoiceHeaderEntity invoiceHeaderEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@InvoiceId", invoiceHeaderEntity.IvceId, DbType.Int32);
                paramCollection.Add("@LastModifiedBy", invoiceHeaderEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(InvoiceHeaderSql["DeleteInvoiceHeader"], paramCollection);
                
                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
            }
        }

        //Add By Irosh for Add Features from Generic Vertion to Perfetti 2017/02/22
        ////////////////////////////////////////////////////////////////////////////
        public InvoiceSettlementEntity GetInvoiceSettlementBySettlementNo(string settlementNo)
        {
            DbDataReader drInvoiceHeader = null;
            InvoiceSettlementEntity invoiceSettlementEntity = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@InvoiceSettlementNo", settlementNo, DbType.String);

                drInvoiceHeader = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["GetInvoiceSettlementBySettlementNo"], paramCollection);

                if (drInvoiceHeader != null && drInvoiceHeader.HasRows)
                {
                    int idOrdinal = drInvoiceHeader.GetOrdinal("id");
                    //int dateOrdinal = drInvoiceHeader.GetOrdinal("date");
                    //int cash_amountOrdinal = drInvoiceHeader.GetOrdinal("cash_amount");
                    //int due_dateOrdinal = drInvoiceHeader.GetOrdinal("due_date");
                    //int status_payOrdinal = drInvoiceHeader.GetOrdinal("status_pay");
                    //int latitudeOrdinal = drInvoiceHeader.GetOrdinal("latitude");
                    //int longitudeOrdinal = drInvoiceHeader.GetOrdinal("longitude");
                    int credit_settlement_noOrdinal = drInvoiceHeader.GetOrdinal("credit_settlement_no");

                    while (drInvoiceHeader.Read())
                    {
                        invoiceSettlementEntity = InvoiceSettlementEntity.CreateObject();

                        if (!drInvoiceHeader.IsDBNull(idOrdinal)) invoiceSettlementEntity.Id = drInvoiceHeader.GetInt32(idOrdinal);
                        if (!drInvoiceHeader.IsDBNull(credit_settlement_noOrdinal)) invoiceSettlementEntity.credit_settle_no = drInvoiceHeader.GetString(credit_settlement_noOrdinal);

                        //if (!drInvoiceHeader.IsDBNull(outstand_amtOrdinal)) invoiceSettlementEntity.OutstandAmount = drInvoiceHeader.GetDouble(outstand_amtOrdinal);
                        //if (!drInvoiceHeader.IsDBNull(pay_amtOrdinal)) invoiceSettlementEntity.PayAmount = drInvoiceHeader.GetDouble(pay_amtOrdinal);

                        //InvoiceSettlementChequeList.Add(invoiceSettlementEntity);
                    }
                }

                return invoiceSettlementEntity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drInvoiceHeader != null)
                    drInvoiceHeader.Close();
            }
        }

        public bool CancelInvoiceSettlementNo(InvoiceSettlementEntity invoiceHeaderEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@SettlementNo", invoiceHeaderEntity.SettlementNo, DbType.String);
                //paramCollection.Add("@LastModifiedBy", invoiceHeaderEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(InvoiceHeaderSql["CancelInvoiceSettlementNo"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public bool ReturnInvoiceHeader(out int returnId, ReturnInvoiceEntity invoiceHeaderEntity)
        {
            bool successful = false;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@returnno", invoiceHeaderEntity.returnno, DbType.String);
                paramCollection.Add("@returntotal", invoiceHeaderEntity.returntotal, DbType.Double);
                paramCollection.Add("@returndate", invoiceHeaderEntity.returndate, DbType.DateTime);

                paramCollection.Add("@canceled", invoiceHeaderEntity.canceled, DbType.String);
                paramCollection.Add("@returntype", invoiceHeaderEntity.returntype, DbType.Int32);
                paramCollection.Add("@remarks", invoiceHeaderEntity.remarks, DbType.String);
                paramCollection.Add("@latitude", invoiceHeaderEntity.latitude, DbType.Double);
                paramCollection.Add("@longitude", invoiceHeaderEntity.longitude, DbType.Double);
                paramCollection.Add("@RepCode", invoiceHeaderEntity.RepCode, DbType.String);
                paramCollection.Add("@RouteId", invoiceHeaderEntity.RouteId, DbType.String);
                paramCollection.Add("@ReturnId", null, DbType.Int32, ParameterDirection.Output);

                returnId = this.DataAcessService.ExecuteNonQuery(InvoiceHeaderSql["InsertReturnInvoiceHeader"], paramCollection, true, "@ReturnId");
                if (returnId > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public bool CancelReturnInvoiceHeader(string ReturnInvoiceNo)
        {
            bool successful = false;
            int returnId = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@returnNo", ReturnInvoiceNo, DbType.String);
                returnId = this.DataAcessService.ExecuteNonQuery(InvoiceHeaderSql["CancelReturnInvoiceHeader"], paramCollection);

                if (returnId > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public List<ReturnInvoiceEntity> GetReturnInvoiceByInvoiceId(int invoiceId)
        {
            DbDataReader drReturnInvoice = null;

            try
            {
                List<ReturnInvoiceEntity> ReturnInvoiceList = new List<ReturnInvoiceEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@InvoiceId", invoiceId, DbType.Int32);

                drReturnInvoice = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["GetReturnInvoiceHeaderByInvoiceId"], paramCollection);

                if (drReturnInvoice != null && drReturnInvoice.HasRows)
                {
                    int returnidOrdinal = drReturnInvoice.GetOrdinal("returnid");
                    int returnnoOrdinal = drReturnInvoice.GetOrdinal("returnno");
                    int returntotalOrdinal = drReturnInvoice.GetOrdinal("returntotal");
                    int returndateOrdinal = drReturnInvoice.GetOrdinal("returndate");
                    int canceledOrdinal = drReturnInvoice.GetOrdinal("canceled");
                    int returntypeOrdinal = drReturnInvoice.GetOrdinal("returntype");
                    int remarksOrdinal = drReturnInvoice.GetOrdinal("remarks");
                    int latitudeOrdinal = drReturnInvoice.GetOrdinal("latitude");
                    int longitudeOrdinal = drReturnInvoice.GetOrdinal("longitude");
                    int routeNameOrdinal = drReturnInvoice.GetOrdinal("route_name");

                    ReturnInvoiceEntity returninvoiceEntity = null;

                    while (drReturnInvoice.Read())
                    {
                        returninvoiceEntity = ReturnInvoiceEntity.CreateObject();

                        if (!drReturnInvoice.IsDBNull(returnidOrdinal)) returninvoiceEntity.Id = drReturnInvoice.GetInt32(returnidOrdinal);
                        if (!drReturnInvoice.IsDBNull(returnnoOrdinal)) returninvoiceEntity.returnno = drReturnInvoice.GetString(returnnoOrdinal);
                        if (!drReturnInvoice.IsDBNull(returntotalOrdinal)) returninvoiceEntity.returntotal = drReturnInvoice.GetDouble(returntotalOrdinal);
                        if (!drReturnInvoice.IsDBNull(returndateOrdinal)) returninvoiceEntity.returndate = drReturnInvoice.GetDateTime(returndateOrdinal).ToString("yyyy-MM-dd hh:mm");
                        if (!drReturnInvoice.IsDBNull(canceledOrdinal)) returninvoiceEntity.canceled = drReturnInvoice.GetString(canceledOrdinal);
                        if (!drReturnInvoice.IsDBNull(returntypeOrdinal)) returninvoiceEntity.returntype = drReturnInvoice.GetInt32(returntypeOrdinal);
                        if (!drReturnInvoice.IsDBNull(remarksOrdinal)) returninvoiceEntity.remarks = drReturnInvoice.GetString(remarksOrdinal);
                        if (!drReturnInvoice.IsDBNull(latitudeOrdinal)) returninvoiceEntity.latitude = drReturnInvoice.GetDouble(latitudeOrdinal);
                        if (!drReturnInvoice.IsDBNull(longitudeOrdinal)) returninvoiceEntity.longitude = drReturnInvoice.GetDouble(longitudeOrdinal);
                        if (!drReturnInvoice.IsDBNull(longitudeOrdinal)) returninvoiceEntity.RouteName = drReturnInvoice.GetString(routeNameOrdinal);
                        ReturnInvoiceList.Add(returninvoiceEntity);
                    }
                }

                return ReturnInvoiceList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drReturnInvoice != null)
                    drReturnInvoice.Close();
            }
        }

        public List<InvoiceSettlementEntity> GetInvoiceSettlementListByInvoiceId(int invoiceId)
        {
            //DbDataReader drInvoiceHeader = null;

            //try
            //{
            //    List<InvoiceSettlementEntity> InvoiceSettlementList = new List<InvoiceSettlementEntity>();
            //    InvoiceSettlementEntity invoiceSettlementEntity = null;

            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@InvoiceId", invoiceId, DbType.String);

            //    drInvoiceHeader = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["GetInvoiceSettlementByInvoiceId"], paramCollection);

            //    if (drInvoiceHeader != null && drInvoiceHeader.HasRows)
            //    {
            //        int idOrdinal = drInvoiceHeader.GetOrdinal("id");
            //        int dateOrdinal = drInvoiceHeader.GetOrdinal("date");
            //        int duedateOrdinal = drInvoiceHeader.GetOrdinal("due_date");
            //        int cashAmountOrdinal = drInvoiceHeader.GetOrdinal("cash_amount");
            //        //int chequeAmountOrdinal = drInvoiceHeader.GetOrdinal("cheque_amount");
            //        //int chequeNoOrdinal = drInvoiceHeader.GetOrdinal("cheque_no");
            //        //int chequeBankOrdinal = drInvoiceHeader.GetOrdinal("cheque_bank");
            //        //int chequedateOrdinal = drInvoiceHeader.GetOrdinal("cheque_date");
            //        int custCodeOrdinal = drInvoiceHeader.GetOrdinal("cust_code");
            //        int invoiceIdOrdinal = drInvoiceHeader.GetOrdinal("invoice_id");
            //        int settlementNoOrdinal = drInvoiceHeader.GetOrdinal("settlement_no");
            //        int settlementTypeIdOrdinal = drInvoiceHeader.GetOrdinal("settlement_type_id");
            //        int status_payOrdinal = drInvoiceHeader.GetOrdinal("status_pay");
            //        int latitudeOrdinal = drInvoiceHeader.GetOrdinal("latitude");
            //        int longitudeOrdinal = drInvoiceHeader.GetOrdinal("longitude");
            //        int remarksOrdinal = drInvoiceHeader.GetOrdinal("remarks");
            //        int credit_settlement_noOrdinal = drInvoiceHeader.GetOrdinal("credit_settlement_no");
            //        int isCancelOrdinal = drInvoiceHeader.GetOrdinal("isCancel");
            //        int routeNameOrdinal = drInvoiceHeader.GetOrdinal("route_name");

            //        while (drInvoiceHeader.Read())
            //        {
            //            invoiceSettlementEntity = InvoiceSettlementEntity.CreateObject();
            //            if (!drInvoiceHeader.IsDBNull(idOrdinal)) invoiceSettlementEntity.InvoiceSettlementId = drInvoiceHeader.GetInt32(idOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(dateOrdinal)) invoiceSettlementEntity.AddedOn = drInvoiceHeader.GetDateTime(dateOrdinal).ToString("yyyy-MM-dd HH:mm");
            //            if (!drInvoiceHeader.IsDBNull(duedateOrdinal)) invoiceSettlementEntity.DueDate = drInvoiceHeader.GetDateTime(duedateOrdinal).ToString("yyyy-MM-dd HH:mm");
            //            if (!drInvoiceHeader.IsDBNull(cashAmountOrdinal)) invoiceSettlementEntity.CashAmount = drInvoiceHeader.GetDouble(cashAmountOrdinal);
            //            //if (!drInvoiceHeader.IsDBNull(chequeAmountOrdinal)) invoiceSettlementEntity.ChequeAmount = drInvoiceHeader.GetDouble(chequeAmountOrdinal);
            //            //if (!drInvoiceHeader.IsDBNull(chequeNoOrdinal)) invoiceSettlementEntity.ChequeNo = drInvoiceHeader.GetString(chequeNoOrdinal);
            //            //if (!drInvoiceHeader.IsDBNull(chequeBankOrdinal)) invoiceSettlementEntity.ChequeBank = drInvoiceHeader.GetString(chequeBankOrdinal);
            //            //if (!drInvoiceHeader.IsDBNull(chequedateOrdinal)) invoiceSettlementEntity.ChequeDate = drInvoiceHeader.GetDateTime(chequedateOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(custCodeOrdinal)) invoiceSettlementEntity.CustCode = drInvoiceHeader.GetString(custCodeOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(invoiceIdOrdinal)) invoiceSettlementEntity.InvoiceId = drInvoiceHeader.GetInt32(invoiceIdOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(settlementNoOrdinal)) invoiceSettlementEntity.SettlementNo = drInvoiceHeader.GetString(settlementNoOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(settlementTypeIdOrdinal)) invoiceSettlementEntity.SettlementTypeId = drInvoiceHeader.GetInt32(settlementTypeIdOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(status_payOrdinal)) invoiceSettlementEntity.status_pay = drInvoiceHeader.GetString(status_payOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(latitudeOrdinal)) invoiceSettlementEntity.latitude = drInvoiceHeader.GetString(latitudeOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(longitudeOrdinal)) invoiceSettlementEntity.longitude = drInvoiceHeader.GetString(longitudeOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(remarksOrdinal)) invoiceSettlementEntity.remarks = drInvoiceHeader.GetString(remarksOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(credit_settlement_noOrdinal)) invoiceSettlementEntity.credit_settle_no = drInvoiceHeader.GetString(credit_settlement_noOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(isCancelOrdinal)) invoiceSettlementEntity.IsCancel = drInvoiceHeader.GetString(isCancelOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(isCancelOrdinal)) invoiceSettlementEntity.routeName = drInvoiceHeader.GetString(routeNameOrdinal);
            //            InvoiceSettlementList.Add(invoiceSettlementEntity);
            //        }
            //    }

            //    //return invoiceSettlementEntity;
            //    return InvoiceSettlementList;
            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (drInvoiceHeader != null)
            //        drInvoiceHeader.Close();
            //}

            //*************************Add By Irosh Fernando 2018/10/3************************************

            InvoiceSettlementEntity invoiceSettlementEntity = null;
            DataTable objDS = new DataTable();
            List<InvoiceSettlementEntity> InvoiceSettlementList = new List<InvoiceSettlementEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetInvoiceSettlementByInvoiceId";

                    objCommand.Parameters.AddWithValue("@InvoiceId", invoiceId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            invoiceSettlementEntity = InvoiceSettlementEntity.CreateObject();
                            if (item["id"] != DBNull.Value) invoiceSettlementEntity.InvoiceSettlementId = Convert.ToInt32(item["id"]);
                            if (item["date"] != DBNull.Value) invoiceSettlementEntity.AddedOn = Convert.ToDateTime(item["date"]).ToString("yyyy-MM-dd HH:mm");
                            if (item["due_date"] != DBNull.Value) invoiceSettlementEntity.DueDate = Convert.ToDateTime(item["due_date"]).ToString("yyyy-MM-dd HH:mm");
                            if (item["cash_amount"] != DBNull.Value) invoiceSettlementEntity.CashAmount = Convert.ToDouble(item["cash_amount"]);
                            if (item["cust_code"] != DBNull.Value) invoiceSettlementEntity.CustCode = item["cust_code"].ToString();
                            if (item["invoice_id"] != DBNull.Value) invoiceSettlementEntity.InvoiceId = Convert.ToInt32(item["invoice_id"]);
                            if (item["settlement_no"] != DBNull.Value) invoiceSettlementEntity.SettlementNo = item["settlement_no"].ToString();
                            if (item["settlement_type_id"] != DBNull.Value) invoiceSettlementEntity.SettlementTypeId = Convert.ToInt32(item["settlement_type_id"]);
                            if (item["status_pay"] != DBNull.Value) invoiceSettlementEntity.status_pay = item["status_pay"].ToString();
                            if (item["latitude"] != DBNull.Value) invoiceSettlementEntity.latitude = item["latitude"].ToString();
                            if (item["longitude"] != DBNull.Value) invoiceSettlementEntity.longitude = item["longitude"].ToString();
                            if (item["remarks"] != DBNull.Value) invoiceSettlementEntity.remarks = item["remarks"].ToString();
                            if (item["credit_settlement_no"] != DBNull.Value) invoiceSettlementEntity.credit_settle_no = item["credit_settlement_no"].ToString();
                            if (item["isCancel"] != DBNull.Value) invoiceSettlementEntity.IsCancel = item["isCancel"].ToString();
                            if (item["route_name"] != DBNull.Value) invoiceSettlementEntity.routeName = item["route_name"].ToString();
                            InvoiceSettlementList.Add(invoiceSettlementEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return InvoiceSettlementList;

            //*************************Add By Irosh Fernando 2018/10/3************************************
        }


        public List<InvoiceSettlementDetailsEntity> GetInvoiceSettlementDetailsBySettlementId(int settlementId)
        {
            DbDataReader drInvoiceHeader = null;

            try
            {
                List<InvoiceSettlementDetailsEntity> InvoiceSettlementChequeList = new List<InvoiceSettlementDetailsEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@InvoiceSettlementId", settlementId, DbType.String);

                drInvoiceHeader = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["GetInvoiceSettlementDetailsBySettlementId"], paramCollection);

                if (drInvoiceHeader != null && drInvoiceHeader.HasRows)
                {
                    int idOrdinal = drInvoiceHeader.GetOrdinal("id");
                    int invoice_idOrdinal = drInvoiceHeader.GetOrdinal("invoice_id");
                    int outstand_amtOrdinal = drInvoiceHeader.GetOrdinal("outstand_amt");
                    int pay_amtOrdinal = drInvoiceHeader.GetOrdinal("pay_amt");

                    InvoiceSettlementDetailsEntity invoiceSettlementEntity = null;

                    while (drInvoiceHeader.Read())
                    {
                        invoiceSettlementEntity = InvoiceSettlementDetailsEntity.CreateObject();
                        if (!drInvoiceHeader.IsDBNull(idOrdinal)) invoiceSettlementEntity.Id = drInvoiceHeader.GetInt32(idOrdinal);
                        if (!drInvoiceHeader.IsDBNull(invoice_idOrdinal)) invoiceSettlementEntity.InvoiceId = drInvoiceHeader.GetInt32(invoice_idOrdinal);
                        if (!drInvoiceHeader.IsDBNull(outstand_amtOrdinal)) invoiceSettlementEntity.OutstandAmount = drInvoiceHeader.GetDouble(outstand_amtOrdinal);
                        if (!drInvoiceHeader.IsDBNull(pay_amtOrdinal)) invoiceSettlementEntity.PayAmount = drInvoiceHeader.GetDouble(pay_amtOrdinal);
                        InvoiceSettlementChequeList.Add(invoiceSettlementEntity);
                    }
                }

                return InvoiceSettlementChequeList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drInvoiceHeader != null)
                    drInvoiceHeader.Close();
            }
        }

        public List<InvoiceSettlementDetailsEntity> GetInvoiceSettlementDetailsBySettlementIdAndInvoiceId(int settlementId, int invoiceId)
        {
            //DbDataReader drInvoiceHeader = null;

            //try
            //{
            //    List<InvoiceSettlementDetailsEntity> InvoiceSettlementChequeList = new List<InvoiceSettlementDetailsEntity>();

            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@InvoiceSettlementId", settlementId, DbType.String);
            //    paramCollection.Add("@InvoiceId", invoiceId, DbType.Int32);

            //    drInvoiceHeader = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["GetInvoiceSettlementDetailsBySettlementIdAndInvoiceId"], paramCollection);

            //    if (drInvoiceHeader != null && drInvoiceHeader.HasRows)
            //    {
            //        int idOrdinal = drInvoiceHeader.GetOrdinal("id");
            //        int invoice_idOrdinal = drInvoiceHeader.GetOrdinal("invoice_id");
            //        int outstand_amtOrdinal = drInvoiceHeader.GetOrdinal("outstand_amt");
            //        int pay_amtOrdinal = drInvoiceHeader.GetOrdinal("pay_amt");

            //        InvoiceSettlementDetailsEntity invoiceSettlementEntity = null;

            //        while (drInvoiceHeader.Read())
            //        {
            //            invoiceSettlementEntity = InvoiceSettlementDetailsEntity.CreateObject();

            //            if (!drInvoiceHeader.IsDBNull(idOrdinal)) invoiceSettlementEntity.Id = drInvoiceHeader.GetInt32(idOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(invoice_idOrdinal)) invoiceSettlementEntity.InvoiceId = drInvoiceHeader.GetInt32(invoice_idOrdinal);

            //            if (!drInvoiceHeader.IsDBNull(outstand_amtOrdinal)) invoiceSettlementEntity.OutstandAmount = drInvoiceHeader.GetDouble(outstand_amtOrdinal);
            //            if (!drInvoiceHeader.IsDBNull(pay_amtOrdinal)) invoiceSettlementEntity.PayAmount = drInvoiceHeader.GetDouble(pay_amtOrdinal);

            //            InvoiceSettlementChequeList.Add(invoiceSettlementEntity);
            //        }
            //    }

            //    return InvoiceSettlementChequeList;
            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (drInvoiceHeader != null)
            //        drInvoiceHeader.Close();
            //}

            //*************************Add By Irosh Fernando 2018/10/3************************************

            InvoiceSettlementDetailsEntity invoiceSettlementEntity = null;
            DataTable objDS = new DataTable();
            List<InvoiceSettlementDetailsEntity> InvoiceSettlementChequeList = new List<InvoiceSettlementDetailsEntity>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetInvoiceSettlementDetailsBySettlementIdAndInvoiceId";

                    objCommand.Parameters.AddWithValue("@InvoiceSettlementId", settlementId);
                    objCommand.Parameters.AddWithValue("@InvoiceId", invoiceId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            invoiceSettlementEntity = InvoiceSettlementDetailsEntity.CreateObject();
                            if (item["id"] != DBNull.Value) invoiceSettlementEntity.Id = Convert.ToInt32(item["id"]);
                            if (item["invoice_id"] != DBNull.Value) invoiceSettlementEntity.InvoiceId = Convert.ToInt32(item["invoice_id"]);
                            if (item["outstand_amt"] != DBNull.Value) invoiceSettlementEntity.OutstandAmount = Convert.ToDouble(item["outstand_amt"]);
                            if (item["pay_amt"] != DBNull.Value) invoiceSettlementEntity.PayAmount = Convert.ToDouble(item["pay_amt"]);
                            InvoiceSettlementChequeList.Add(invoiceSettlementEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return InvoiceSettlementChequeList;

            //*************************Add By Irosh Fernando 2018/10/3************************************
        }

        public List<InvoiceSettlementChequeEntity> GetInvoiceSettlementChequeBySettlementIdAndInvoiceId(int settlementId, int invoiceId)
        {
            DbDataReader drInvoiceHeader = null;

            try
            {
                List<InvoiceSettlementChequeEntity> InvoiceSettlementChequeList = new List<InvoiceSettlementChequeEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@InvoiceSettlementId", settlementId, DbType.String);
                paramCollection.Add("@InvoiceId", invoiceId, DbType.Int32);

                drInvoiceHeader = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["GetInvoiceSettlementChequeBySettlementIdAndInvoiceId"], paramCollection);

                if (drInvoiceHeader != null && drInvoiceHeader.HasRows)
                {
                    int idOrdinal = drInvoiceHeader.GetOrdinal("id");
                    int invoiceSettlementIdOrdinal = drInvoiceHeader.GetOrdinal("invoice_settlement_id");
                    int chequeAmountOrdinal = drInvoiceHeader.GetOrdinal("cheque_amount");
                    int chequeNoOrdinal = drInvoiceHeader.GetOrdinal("cheque_no");
                    int chequeBankOrdinal = drInvoiceHeader.GetOrdinal("cheque_bank");
                    int chequedateOrdinal = drInvoiceHeader.GetOrdinal("cheque_date");

                    InvoiceSettlementChequeEntity invoiceSettlementEntity = null;

                    while (drInvoiceHeader.Read())
                    {
                        invoiceSettlementEntity = InvoiceSettlementChequeEntity.CreateObject();

                        if (!drInvoiceHeader.IsDBNull(idOrdinal)) invoiceSettlementEntity.InvoiceSettlementChequeId = drInvoiceHeader.GetInt32(idOrdinal);
                        if (!drInvoiceHeader.IsDBNull(invoiceSettlementIdOrdinal)) invoiceSettlementEntity.InvoiceSettlementId = drInvoiceHeader.GetInt32(invoiceSettlementIdOrdinal);

                        if (!drInvoiceHeader.IsDBNull(chequeAmountOrdinal)) invoiceSettlementEntity.ChequeAmount = drInvoiceHeader.GetDouble(chequeAmountOrdinal);
                        if (!drInvoiceHeader.IsDBNull(chequeNoOrdinal)) invoiceSettlementEntity.ChequeNo = drInvoiceHeader.GetString(chequeNoOrdinal);
                        if (!drInvoiceHeader.IsDBNull(chequeBankOrdinal)) invoiceSettlementEntity.ChequeBank = drInvoiceHeader.GetString(chequeBankOrdinal);
                        if (!drInvoiceHeader.IsDBNull(chequedateOrdinal)) invoiceSettlementEntity.ChequeDate = drInvoiceHeader.GetDateTime(chequedateOrdinal).ToString("yyyy-MM-dd HH:mm");

                        InvoiceSettlementChequeList.Add(invoiceSettlementEntity);
                    }
                }

                return InvoiceSettlementChequeList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drInvoiceHeader != null)
                    drInvoiceHeader.Close();
            }
        }

        public List<RepsOutletWiseDetailsEntity> GetAllRepsOutletWiseDetails(string AccessToken, string date)
        {
            DbDataReader drSchemeHeader = null;
            try
            {
                List<RepsOutletWiseDetailsEntity> detCollection = new List<RepsOutletWiseDetailsEntity>();
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                RepsOutletWiseDetailsEntity schemeHeader = null;
                paramCollection.Add("@AccessToken", AccessToken, DbType.String);
                paramCollection.Add("@date", date, DbType.String);

                drSchemeHeader = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["GetAllRepsOutletWiseDetails"], paramCollection);

                if (drSchemeHeader != null && drSchemeHeader.HasRows)
                {
                    int cust_codeOrdinal = drSchemeHeader.GetOrdinal("cust_code");
                    int cust_nameOrdinal = drSchemeHeader.GetOrdinal("cust_name");
                    int prod_codeOrdinal = drSchemeHeader.GetOrdinal("prod_code");
                    int prod_nameOrdinal = drSchemeHeader.GetOrdinal("prod_name");
                    int quantityOrdinal = drSchemeHeader.GetOrdinal("quantity");
                    int sub_totalOrdinal = drSchemeHeader.GetOrdinal("sub_total");
                    int count_creditOrdinal = drSchemeHeader.GetOrdinal("count_credit");
                    int count_chequeOrdinal = drSchemeHeader.GetOrdinal("count_cheque");
                    int count_cashOrdinal = drSchemeHeader.GetOrdinal("count_cash");
                    int rep_nameOrdinal = drSchemeHeader.GetOrdinal("rep_name");
                    int route_nameOrdinal = drSchemeHeader.GetOrdinal("route_name");
                    int discount_totalOrdinal = drSchemeHeader.GetOrdinal("discount_total");

                    while (drSchemeHeader.Read())
                    {
                        schemeHeader = RepsOutletWiseDetailsEntity.CreateObject();
                        if (!drSchemeHeader.IsDBNull(cust_codeOrdinal)) schemeHeader.CustomerCode = drSchemeHeader.GetString(cust_codeOrdinal);
                        if (!drSchemeHeader.IsDBNull(cust_nameOrdinal)) schemeHeader.CustomerName = drSchemeHeader.GetString(cust_nameOrdinal);
                        if (!drSchemeHeader.IsDBNull(count_creditOrdinal)) schemeHeader.PaymentTypeCredit = drSchemeHeader.GetInt32(count_creditOrdinal);
                        if (!drSchemeHeader.IsDBNull(count_cashOrdinal)) schemeHeader.PaymentTypeCash = drSchemeHeader.GetInt32(count_cashOrdinal);
                        if (!drSchemeHeader.IsDBNull(count_chequeOrdinal)) schemeHeader.PaymentTypeCheque = drSchemeHeader.GetInt32(count_chequeOrdinal);

                        if (!drSchemeHeader.IsDBNull(route_nameOrdinal)) schemeHeader.RouteName = drSchemeHeader.GetString(route_nameOrdinal);
                        if (!drSchemeHeader.IsDBNull(rep_nameOrdinal)) schemeHeader.RepName = drSchemeHeader.GetString(rep_nameOrdinal);

                        if (!drSchemeHeader.IsDBNull(prod_codeOrdinal)) schemeHeader.ProductCode = drSchemeHeader.GetString(prod_codeOrdinal);
                        if (!drSchemeHeader.IsDBNull(prod_nameOrdinal)) schemeHeader.ProductName = drSchemeHeader.GetString(prod_nameOrdinal);
                        if (!drSchemeHeader.IsDBNull(quantityOrdinal)) schemeHeader.ProductQty = drSchemeHeader.GetDouble(quantityOrdinal);
                        if (!drSchemeHeader.IsDBNull(sub_totalOrdinal)) schemeHeader.ProductAmount = (double)drSchemeHeader.GetDecimal(sub_totalOrdinal);
                        if (!drSchemeHeader.IsDBNull(discount_totalOrdinal)) schemeHeader.DiscountTotal = drSchemeHeader.GetDouble(discount_totalOrdinal);

                        detCollection.Add(schemeHeader);
                    }
                }

                return detCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drSchemeHeader != null)
                    drSchemeHeader.Close();
            }
        }

        public List<RepsSKUMasterEntity> GetAllRepsSKUWiseDetails(string AccessToken, string date)
        {
            DbDataReader drSchemeHeader = null;

            try
            {
                List<RepsSKUMasterEntity> headerList = new List<RepsSKUMasterEntity>();
                RepsSKUMasterEntity header = new RepsSKUMasterEntity();
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@AccessToken", AccessToken, DbType.String);
                paramCollection.Add("@date", date, DbType.String);

                drSchemeHeader = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["GetAllRepsSKUWiseDetails"], paramCollection);

                if (drSchemeHeader != null && drSchemeHeader.HasRows)
                {
                    int rep_nameOrdinal = drSchemeHeader.GetOrdinal("rep_name");
                    int route_nameOrdinal = drSchemeHeader.GetOrdinal("route_name");
                    int brand_nameOrdinal = drSchemeHeader.GetOrdinal("brand_name");
                    int category_nameOrdinal = drSchemeHeader.GetOrdinal("category_name");
                    int packing_nameOrdinal = drSchemeHeader.GetOrdinal("packing_name");
                    int flavour_nameOrdinal = drSchemeHeader.GetOrdinal("flavor_name");
                    int is_high_productOrdinal = drSchemeHeader.GetOrdinal("is_high_value");
                    int product_codeOrdinal = drSchemeHeader.GetOrdinal("product_code");
                    int product_nameOrdinal = drSchemeHeader.GetOrdinal("product_name");
                    int product_fg_codeOrdinal = drSchemeHeader.GetOrdinal("fg_code");
                    int product_quantityOrdinal = drSchemeHeader.GetOrdinal("product_quantity");
                    int product_valueOrdinal = drSchemeHeader.GetOrdinal("product_value");
                    int free_issueOrdinal = drSchemeHeader.GetOrdinal("free_issue");
                    int discount_valueOrdinal = drSchemeHeader.GetOrdinal("discount_value");

                    while (drSchemeHeader.Read())
                    {
                        header = RepsSKUMasterEntity.CreateObject();
                        if (!drSchemeHeader.IsDBNull(rep_nameOrdinal)) header.RepName = drSchemeHeader.GetString(rep_nameOrdinal);
                        if (!drSchemeHeader.IsDBNull(route_nameOrdinal)) header.RouteName = drSchemeHeader.GetString(route_nameOrdinal);
                        if (!drSchemeHeader.IsDBNull(brand_nameOrdinal)) header.BrandName = drSchemeHeader.GetString(brand_nameOrdinal);
                        if (!drSchemeHeader.IsDBNull(category_nameOrdinal)) header.CategoryName = drSchemeHeader.GetString(category_nameOrdinal);
                        if (!drSchemeHeader.IsDBNull(packing_nameOrdinal)) header.PackingName = drSchemeHeader.GetString(packing_nameOrdinal);
                        if (!drSchemeHeader.IsDBNull(flavour_nameOrdinal)) header.FlavourName = drSchemeHeader.GetString(flavour_nameOrdinal);
                        if (!drSchemeHeader.IsDBNull(is_high_productOrdinal)) header.IsHighValue = drSchemeHeader.GetBoolean(is_high_productOrdinal);
                        if (!drSchemeHeader.IsDBNull(product_codeOrdinal)) header.ProductCode = drSchemeHeader.GetString(product_codeOrdinal);
                        if (!drSchemeHeader.IsDBNull(product_nameOrdinal)) header.ProductName = drSchemeHeader.GetString(product_nameOrdinal);
                        if (!drSchemeHeader.IsDBNull(product_fg_codeOrdinal)) header.ProductFGCode = drSchemeHeader.GetString(product_fg_codeOrdinal);
                        if (!drSchemeHeader.IsDBNull(product_quantityOrdinal)) header.ProductQty = drSchemeHeader.GetDouble(product_quantityOrdinal);
                        if (!drSchemeHeader.IsDBNull(product_valueOrdinal)) header.ProductValue = (double)drSchemeHeader.GetDecimal(product_valueOrdinal);
                        if (!drSchemeHeader.IsDBNull(free_issueOrdinal)) header.FreeIssues = drSchemeHeader.GetDouble(free_issueOrdinal);
                        if (!drSchemeHeader.IsDBNull(discount_valueOrdinal)) header.DiscountValue = drSchemeHeader.GetDouble(discount_valueOrdinal);
                        headerList.Add(header);
                    }
                }

                return headerList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drSchemeHeader != null)
                    drSchemeHeader.Close();
            }
        }

        //public List<RepsOutletWiseProductDetails> GetAllRepsOutletWiseProductDetails(string cust_code, string date)
        //{
        //    DbDataReader drSchemeProddet = null;
        //    try
        //    {
        //        List<RepsOutletWiseProductDetails> detCollection = new List<RepsOutletWiseProductDetails>();

        //        DbInputParameterCollection paramCollection = new DbInputParameterCollection();
        //        paramCollection.Add("@cust_code", cust_code, DbType.String);
        //        paramCollection.Add("@date", date, DbType.String);

        //        drSchemeProddet = this.DataAcessService.ExecuteQuery(InvoiceHeaderSql["GetAllRepsOutletWiseProductDetails"], paramCollection);

        //        if (drSchemeProddet != null && drSchemeProddet.HasRows)
        //        {
        //            int prod_idOrdinal = drSchemeProddet.GetOrdinal("prod_id");
        //            int prod_nameOrdinal = drSchemeProddet.GetOrdinal("prod_name");
        //            int prod_qtyOrdinal = drSchemeProddet.GetOrdinal("prod_qty");
        //            int prod_amountOrdinal = drSchemeProddet.GetOrdinal("prod_amount");

        //            while (drSchemeProddet.Read())
        //            {
        //                RepsOutletWiseProductDetails schemeHeader = RepsOutletWiseProductDetails.CreateObject();
        //                if (!drSchemeProddet.IsDBNull(prod_idOrdinal)) schemeHeader.ProductId = drSchemeProddet.GetInt32(prod_idOrdinal);
        //                if (!drSchemeProddet.IsDBNull(prod_nameOrdinal)) schemeHeader.ProductName = drSchemeProddet.GetString(prod_nameOrdinal);
        //                //if (!drSchemeHeader.IsDBNull(prod_qtyOrdinal)) schemeHeader.ProductQty = drSchemeHeader.GetDouble(prod_qtyOrdinal);
        //                //if (!drSchemeHeader.IsDBNull(prod_amountOrdinal)) schemeHeader.ProductAmount = drSchemeHeader.GetDouble(prod_amountOrdinal);
        //                detCollection.Add(schemeHeader);

        //            }
        //        }

        //        return detCollection;
        //    }
        //    catch
        //    {
        //        throw;
        //    }
        //    //finally
        //    //{
        //    //    if (drSchemeProddet != null)
        //    //        drSchemeProddet.Close();
        //    //}
        //}

        public bool DeleteBulkInvoices(string userType, string userName, string invList)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteBulkInvoices";

                    objCommand.Parameters.AddWithValue("@userType", userType);
                    objCommand.Parameters.AddWithValue("@userName", userName);
                    objCommand.Parameters.AddWithValue("@invList", invList);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }
    }
}
