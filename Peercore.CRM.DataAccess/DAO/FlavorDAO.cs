﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class FlavorDAO : BaseDAO
    {
        private DbSqlAdapter FlavorSql { get; set; }

        private void RegisterSql()
        {
            this.FlavorSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.FlavorSql.xml", ApplicationService.Instance.DbProvider);
        }

        public FlavorDAO()
        {
            RegisterSql();
        }

        public FlavorDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public FlavorEntity CreateObject()
        {
            return FlavorEntity.CreateObject();
        }

        public FlavorEntity CreateObject(int entityId)
        {
            return FlavorEntity.CreateObject(entityId);
        }

        public List<FlavorEntity> GetAllFlavor()
        {
            DbDataReader drFlavor = null;

            drFlavor = this.DataAcessService.ExecuteQuery(FlavorSql["GetAllFlavor"]);


            try
            {
                List<FlavorEntity> flavorList = new List<FlavorEntity>();
                if (drFlavor != null && drFlavor.HasRows)
                {
                    int flavorIdOrdinal = drFlavor.GetOrdinal("flavor_id");
                    int flavorNameOrdinal = drFlavor.GetOrdinal("flavor_name");

                    while (drFlavor.Read())
                    {
                        FlavorEntity flavor = FlavorEntity.CreateObject();
                        if (!drFlavor.IsDBNull(flavorIdOrdinal)) flavor.FlavorId = drFlavor.GetInt32(flavorIdOrdinal);
                        if (!drFlavor.IsDBNull(flavorNameOrdinal)) flavor.FlavorName = drFlavor.GetString(flavorNameOrdinal);

                        flavorList.Add(flavor);

                    }
                }
                return flavorList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drFlavor != null)
                    drFlavor.Close();
            }
        }

        public List<FlavorEntity> GetFlavorByAccessToken(string accessToken)
        {
            DbDataReader drFlavor = null;

            DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            paramCollection.Add("@AccessToken", accessToken, DbType.String);

            drFlavor = this.DataAcessService.ExecuteQuery(FlavorSql["GetFlavorByAccessToken"], paramCollection);


            try
            {
                List<FlavorEntity> flavorList = new List<FlavorEntity>();
                if (drFlavor != null && drFlavor.HasRows)
                {
                    int flavorIdOrdinal = drFlavor.GetOrdinal("flavor_id");
                    int flavorNameOrdinal = drFlavor.GetOrdinal("flavor_name");

                    while (drFlavor.Read())
                    {
                        FlavorEntity flavor = FlavorEntity.CreateObject();
                        if (!drFlavor.IsDBNull(flavorIdOrdinal)) flavor.FlavorId = drFlavor.GetInt32(flavorIdOrdinal);
                        if (!drFlavor.IsDBNull(flavorNameOrdinal)) flavor.FlavorName = drFlavor.GetString(flavorNameOrdinal);

                        flavorList.Add(flavor);

                    }
                }
                return flavorList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drFlavor != null)
                    drFlavor.Close();
            }
        }

        public bool IsProductFlavorNameExists(FlavorEntity flavorEntity)
        {
            DbDataReader drFlavor = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();


                paramCollection.Add("@FlavorName", flavorEntity.FlavorName, DbType.String);

                drFlavor = this.DataAcessService.ExecuteQuery(FlavorSql["ProductFlavorNameExists"], paramCollection);

                if (drFlavor != null && drFlavor.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drFlavor != null)
                    drFlavor.Close();
            }
        }

        public bool UpdateFlavor(FlavorEntity flavorEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drFlavor = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@FlavorId", flavorEntity.FlavorId, DbType.Int32);
                paramCollection.Add("@FlavorName", flavorEntity.FlavorName, DbType.String);
                
                iNoRec = this.DataAcessService.ExecuteNonQuery(FlavorSql["UpdateProductFlavor"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drFlavor != null)
                    drFlavor.Close();
            }
        }

        public bool InsertFlavor(FlavorEntity flavorEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drFlavor = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@FlavorName", flavorEntity.FlavorName, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(FlavorSql["InsertProductFlavor"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drFlavor != null)
                    drFlavor.Close();
            }
        }

        public bool DeleteProductFlavor(int flavorId)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drFlavor = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@FlavorId", flavorId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(FlavorSql["DeleteProductFlavor"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drFlavor != null)
                    drFlavor.Close();
            }
        }
    }
}
