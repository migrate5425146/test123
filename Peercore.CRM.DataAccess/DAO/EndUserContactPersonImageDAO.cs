﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class EndUserContactPersonImageDAO : BaseDAO
    {
        private DbSqlAdapter EndUserContactPersonImageSql { get; set; }

        private void RegisterSql()
        {
            this.EndUserContactPersonImageSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.EndUserContactPersonImageSql.xml", ApplicationService.Instance.DbProvider);
        }

        public EndUserContactPersonImageDAO()
        {
            RegisterSql();
        }

        public EndUserContactPersonImageDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public EndUserContactPersonImageEntity CreateObject()
        {
            return EndUserContactPersonImageEntity.CreateObject();
        }

        public EndUserContactPersonImageEntity CreateObject(int entityId)
        {
            return EndUserContactPersonImageEntity.CreateObject(entityId);
        }

        public bool UpdateEndUserContactImage(EndUserContactPersonImageEntity contactImage)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                
                paramCollection.Add("@ContactPersonId",contactImage.ContactPersonId , DbType.Int32);
                paramCollection.Add("@EnduserCode", contactImage.EndUserCode, DbType.String);
                paramCollection.Add("@CustCode", contactImage.CustomerCode, DbType.String);
                paramCollection.Add("@FileName", ValidateString(contactImage.FileName), DbType.String);
                paramCollection.Add("@ContactNo", contactImage.ContactNo, DbType.String);
                paramCollection.Add("@ContactImageId", contactImage.ContactPersonImageId, DbType.Int32);
                paramCollection.Add("@fileContent", contactImage.FileContent, DbType.Binary);


                iNoRec = this.DataAcessService.ExecuteNonQuery(EndUserContactPersonImageSql["UpdateEndUserContactPersonImage"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return UpdateSuccessful;
        }

        public bool InsertEndUserContactImage(EndUserContactPersonImageEntity contactImage, ref int contactPersonImageId)
        {
            bool UpdateSuccessful = false;
            int iNoRec = 0;

            try
            {
                contactImage.ContactPersonImageId = Convert.ToInt32(DataAcessService.GetOneValue(EndUserContactPersonImageSql["GetNextEndUserContactPersonImageID"]));   // Get Next ID
                contactPersonImageId = contactImage.ContactPersonImageId;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@contactPersonID", contactImage.ContactPersonId, DbType.Int32);
                paramCollection.Add("@endUserID", contactImage.EndUserCode, DbType.String);
                paramCollection.Add("@custCode", contactImage.CustomerCode, DbType.String);
                paramCollection.Add("@fileName", ValidateString(contactImage.FileName), DbType.String);
                paramCollection.Add("@fileContent", contactImage.FileContent, DbType.Binary);
                paramCollection.Add("@contactNo", contactImage.ContactNo, DbType.String);
                paramCollection.Add("@contactImageID", contactImage.ContactPersonImageId, DbType.Int32);


                iNoRec = this.DataAcessService.ExecuteNonQuery(EndUserContactPersonImageSql["InsertEndUserContactPersonImage"], paramCollection);

                if (iNoRec > 0)
                    UpdateSuccessful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return UpdateSuccessful;
        }

        public EndUserContactPersonImageEntity GetContactPersonImage(int iContactPersonID, string iEndUserCode)
        {
            DbDataReader drContactPersonImage = null;

            try
            {
                EndUserContactPersonImageEntity contactPersonImage = null;

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@endUserID", iEndUserCode, DbType.String);
                paramCollection.Add("@contactPersonID", iContactPersonID, DbType.Int32);

                drContactPersonImage = this.DataAcessService.ExecuteQuery(EndUserContactPersonImageSql["GetEndUserContactPersonImage"], paramCollection);

                if (drContactPersonImage != null && drContactPersonImage.HasRows)
                {
                    int contactImageOrdinal = drContactPersonImage.GetOrdinal("contact_image_id");
                    int contactPersonIdOrdinal = drContactPersonImage.GetOrdinal("contact_person_id");
                    int fileNameOrdinal = drContactPersonImage.GetOrdinal("file_name");
                    int enduserCodeOrdinal = drContactPersonImage.GetOrdinal("enduser_code");

                    bool x = drContactPersonImage.IsClosed;

                    if (drContactPersonImage.Read())
                    {
                        contactPersonImage = EndUserContactPersonImageEntity.CreateObject();

                        if (!drContactPersonImage.IsDBNull(contactPersonIdOrdinal)) contactPersonImage.ContactPersonId = drContactPersonImage.GetInt32(contactPersonIdOrdinal);
                        if (!drContactPersonImage.IsDBNull(contactImageOrdinal)) contactPersonImage.ContactPersonImageId = drContactPersonImage.GetInt32(contactImageOrdinal);
                        if (!drContactPersonImage.IsDBNull(fileNameOrdinal)) contactPersonImage.FileName = drContactPersonImage.GetString(fileNameOrdinal);
                        if (!drContactPersonImage.IsDBNull(enduserCodeOrdinal)) contactPersonImage.EndUserCode = drContactPersonImage.GetString(enduserCodeOrdinal);
                    }
                }

                if (drContactPersonImage != null)
                    drContactPersonImage.Close();

                return contactPersonImage;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drContactPersonImage != null)
                    drContactPersonImage.Close();
            }
        }

    }
}
