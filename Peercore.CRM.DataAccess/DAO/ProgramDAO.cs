﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;

namespace Peercore.CRM.DataAccess.DAO
{
    public class ProgramDAO:BaseDAO
    {
        private DbSqlAdapter ProgramSql { get; set; }

        private void RegisterSql()
        {
            this.ProgramSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.ProgramSql.xml", ApplicationService.Instance.DbProvider);
        }

        public ProgramDAO()
        {
            RegisterSql();
        }

        public ProgramDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public ProgramEntity CreateObject()
        {
            return ProgramEntity.CreateObject();
        }

        public ProgramEntity CreateObject(int entityId)
        {
            return ProgramEntity.CreateObject(entityId);
        }

        public List<ProgramTargetsEntity> GetProgramTargetsByProgram(string programNameLike, int programTargetId, int programId)
        {
            DbDataReader drProgram = null;

            try
            {
                List<ProgramTargetsEntity> programTargetsList = new List<ProgramTargetsEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ProgramName", programNameLike, DbType.String);
                paramCollection.Add("@ProgramTargetId", programTargetId, DbType.Int32);
                paramCollection.Add("@ProgramId", programId, DbType.Int32);

                drProgram = this.DataAcessService.ExecuteQuery(ProgramSql["GetProgramTargetsByProgram"], paramCollection);

                if (drProgram != null && drProgram.HasRows)
                {
                    int programTargetIdOrdinal = drProgram.GetOrdinal("program_target_id");
                    int programIdOrdinal = drProgram.GetOrdinal("program_id");
                    int programNameOrdinal = drProgram.GetOrdinal("name");

                    int targetPercentageOrdinal = drProgram.GetOrdinal("target_percentage");
                    int targetSticksOrdinal = drProgram.GetOrdinal("target_sticks");
                    int targetIncentiveAmountOrdinal = drProgram.GetOrdinal("target_incentive_amount");
                    int targetIncentivePreIssueProductIdOrdinal = drProgram.GetOrdinal("target_incentive_pre_issue_product_id");
                    int targetIncentivePreIssueProductNameOrdinal = drProgram.GetOrdinal("target_incentive_pre_issue_product_name");
                    int targetIncentivePreIssueProductSkuOrdinal = drProgram.GetOrdinal("target_incentive_pre_issue_product_sku");
                    int targetIncentivePreIssueAmountSticksOrdinal = drProgram.GetOrdinal("target_incentive_pre_issue_amount_sticks");

                    int bonusPercentageOrdinal = drProgram.GetOrdinal("bonus_percentage");
                    int bonusSticksOrdinal = drProgram.GetOrdinal("bonus_sticks");
                    int bonusIncentiveAmountOrdinal = drProgram.GetOrdinal("bonus_incentive_amount");
                    int bonusIncentivePreIssueProductIdOrdinal = drProgram.GetOrdinal("bonus_incentive_pre_issue_product_id");
                    int bonusIncentivePreIssueProductNameOrdinal = drProgram.GetOrdinal("bonus_incentive_pre_issue_product_name");
                    int bonusIncentivePreIssueProductSkuOrdinal = drProgram.GetOrdinal("bonus_incentive_pre_issue_product_sku");
                    int bonusIncentivePreIssueAmountSticksOrdinal = drProgram.GetOrdinal("bonus_incentive_pre_issue_amount_sticks");

                    int effStartDateOrdinal = drProgram.GetOrdinal("eff_start_date");
                    int effEndDateOrdinal = drProgram.GetOrdinal("eff_end_date");

                    //int createdByOrdinal = drProgram.GetOrdinal("created_by");
                    //int createdDateOrdinal = drProgram.GetOrdinal("created_date");
                    //int lastModifiedByOrdinal = drProgram.GetOrdinal("last_modified_by");
                    //int lastModifiedDateOrdinal = drProgram.GetOrdinal("last_modified_date");
                    DateTime today = DateTime.Today;
                    DateTime fromDate = new DateTime(today.Year, today.Month, 1);
                    DateTime toDate = new DateTime(today.Year, today.Month, DateTime.DaysInMonth(today.Year, today.Month));

                    while (drProgram.Read())
                    {
                        ProgramTargetsEntity programTargetsEntity = ProgramTargetsEntity.CreateObject();
                        ProgramEntity programEntity = CreateObject();

                        if (!drProgram.IsDBNull(programTargetIdOrdinal)) programTargetsEntity.ProgramTargetId = drProgram.GetInt32(programTargetIdOrdinal);

                        if (!drProgram.IsDBNull(programIdOrdinal)) programEntity.ProgramId = drProgram.GetInt32(programIdOrdinal);
                        if (!drProgram.IsDBNull(programNameOrdinal)) programEntity.ProgramName = drProgram.GetString(programNameOrdinal);

                        if (!drProgram.IsDBNull(targetPercentageOrdinal)) programTargetsEntity.TargetPercentage = drProgram.GetDouble(targetPercentageOrdinal);
                        if (!drProgram.IsDBNull(targetSticksOrdinal)) programTargetsEntity.TargetSticks = drProgram.GetInt32(targetSticksOrdinal);
                        if (!drProgram.IsDBNull(targetIncentiveAmountOrdinal)) programTargetsEntity.TargetIncentiveAmount = Convert.ToDouble(drProgram.GetDecimal(targetIncentiveAmountOrdinal));
                        if (!drProgram.IsDBNull(targetIncentivePreIssueProductIdOrdinal)) programTargetsEntity.TargetIncentivePreIssueProductId = drProgram.GetInt32(targetIncentivePreIssueProductIdOrdinal);
                        if (!drProgram.IsDBNull(targetIncentivePreIssueProductNameOrdinal)) programTargetsEntity.TargetIncentivePreIssueProductName = drProgram.GetString(targetIncentivePreIssueProductNameOrdinal);
                        if (!drProgram.IsDBNull(targetIncentivePreIssueProductSkuOrdinal)) programTargetsEntity.TargetIncentivePreIssueProductSku = drProgram.GetInt32(targetIncentivePreIssueProductSkuOrdinal);
                        if (!drProgram.IsDBNull(targetIncentivePreIssueAmountSticksOrdinal)) programTargetsEntity.TargetIncentivePreIssueAmountSticks = drProgram.GetInt32(targetIncentivePreIssueAmountSticksOrdinal);

                        if (!drProgram.IsDBNull(bonusPercentageOrdinal)) programTargetsEntity.BonusPercentage = drProgram.GetDouble(bonusPercentageOrdinal);
                        if (!drProgram.IsDBNull(bonusSticksOrdinal)) programTargetsEntity.BonusSticks = drProgram.GetInt32(bonusSticksOrdinal);
                        if (!drProgram.IsDBNull(bonusIncentiveAmountOrdinal)) programTargetsEntity.BonusIncentiveAmount = Convert.ToDouble(drProgram.GetDecimal(bonusIncentiveAmountOrdinal));
                        if (!drProgram.IsDBNull(bonusIncentivePreIssueProductIdOrdinal)) programTargetsEntity.BonusIncentivePreIssueProductId = drProgram.GetInt32(bonusIncentivePreIssueProductIdOrdinal);
                        if (!drProgram.IsDBNull(bonusIncentivePreIssueProductNameOrdinal)) programTargetsEntity.BonusIncentivePreIssueProductName = drProgram.GetString(bonusIncentivePreIssueProductNameOrdinal);
                        if (!drProgram.IsDBNull(bonusIncentivePreIssueProductSkuOrdinal)) programTargetsEntity.BonusIncentivePreIssueProductSku = drProgram.GetInt32(bonusIncentivePreIssueProductSkuOrdinal);
                        if (!drProgram.IsDBNull(bonusIncentivePreIssueAmountSticksOrdinal)) programTargetsEntity.BonusIncentivePreIssueAmountSticks = drProgram.GetInt32(bonusIncentivePreIssueAmountSticksOrdinal);

                        if (!drProgram.IsDBNull(effStartDateOrdinal)) programTargetsEntity.EffStartDate = drProgram.GetDateTime(effStartDateOrdinal);
                        else programTargetsEntity.EffStartDate = fromDate;
                        if (!drProgram.IsDBNull(effEndDateOrdinal)) programTargetsEntity.EffEndDate = drProgram.GetDateTime(effEndDateOrdinal);
                        else programTargetsEntity.EffEndDate = toDate;

                        //if (!drProgram.IsDBNull(createdByOrdinal)) programTargetsEntity.CreatedBy = drProgram.GetString(createdByOrdinal);
                        //if (!drProgram.IsDBNull(createdDateOrdinal)) programTargetsEntity.CreatedDate = drProgram.GetDateTime(createdDateOrdinal);
                        //if (!drProgram.IsDBNull(lastModifiedByOrdinal)) programTargetsEntity.LastModifiedBy = drProgram.GetString(lastModifiedByOrdinal);
                        //if (!drProgram.IsDBNull(lastModifiedDateOrdinal)) programTargetsEntity.LastModifiedDate = drProgram.GetDateTime(lastModifiedDateOrdinal);

                        programTargetsEntity.Program = programEntity;
                        programTargetsList.Add(programTargetsEntity);
                    }
                }

                return programTargetsList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProgram != null)
                    drProgram.Close();
            }
        }

        public bool InsertProgramTarget(ProgramTargetsEntity programTargetsEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@program_id", programTargetsEntity.Program.ProgramId, DbType.Int32);

                paramCollection.Add("@target_percentage", programTargetsEntity.TargetPercentage, DbType.Double);
                paramCollection.Add("@target_sticks", programTargetsEntity.TargetSticks, DbType.Int32);
                paramCollection.Add("@target_incentive_amount", programTargetsEntity.TargetIncentiveAmount, DbType.Decimal);
                paramCollection.Add("@target_incentive_pre_issue_product_id", programTargetsEntity.TargetIncentivePreIssueProductId, DbType.Int32);
                paramCollection.Add("@target_incentive_pre_issue_amount_sticks", programTargetsEntity.TargetIncentivePreIssueAmountSticks, DbType.Int32);

                paramCollection.Add("@bonus_percentage", programTargetsEntity.BonusPercentage, DbType.Double);
                paramCollection.Add("@bonus_sticks", programTargetsEntity.BonusSticks, DbType.Int32);
                paramCollection.Add("@bonus_incentive_amount", programTargetsEntity.BonusIncentiveAmount, DbType.Decimal);
                paramCollection.Add("@bonus_incentive_pre_issue_product_id", programTargetsEntity.BonusIncentivePreIssueProductId, DbType.Int32);
                paramCollection.Add("@bonus_incentive_pre_issue_amount_sticks", programTargetsEntity.BonusIncentivePreIssueAmountSticks, DbType.Int32);

                paramCollection.Add("@eff_start_date", programTargetsEntity.EffStartDate, DbType.DateTime);
                paramCollection.Add("@eff_end_date", programTargetsEntity.EffEndDate, DbType.DateTime);

                paramCollection.Add("@created_by", programTargetsEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ProgramSql["InsertProgramTarget"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateProgramTarget(ProgramTargetsEntity programTargetsEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@program_target_id", programTargetsEntity.ProgramTargetId, DbType.Int32);
                paramCollection.Add("@program_id", programTargetsEntity.Program.ProgramId, DbType.Int32);

                paramCollection.Add("@target_percentage", programTargetsEntity.TargetPercentage, DbType.Double);
                paramCollection.Add("@target_sticks", programTargetsEntity.TargetSticks, DbType.Int32);
                paramCollection.Add("@target_incentive_amount", programTargetsEntity.TargetIncentiveAmount, DbType.Decimal);
                paramCollection.Add("@target_incentive_pre_issue_product_id", programTargetsEntity.TargetIncentivePreIssueProductId, DbType.Int32);
                paramCollection.Add("@target_incentive_pre_issue_amount_sticks", programTargetsEntity.TargetIncentivePreIssueAmountSticks, DbType.Int32);

                paramCollection.Add("@bonus_percentage", programTargetsEntity.BonusPercentage, DbType.Double);
                paramCollection.Add("@bonus_sticks", programTargetsEntity.BonusSticks, DbType.Int32);
                paramCollection.Add("@bonus_incentive_amount", programTargetsEntity.BonusIncentiveAmount, DbType.Decimal);
                paramCollection.Add("@bonus_incentive_pre_issue_product_id", programTargetsEntity.BonusIncentivePreIssueProductId, DbType.Int32);
                paramCollection.Add("@bonus_incentive_pre_issue_amount_sticks", programTargetsEntity.BonusIncentivePreIssueAmountSticks, DbType.Int32);

                paramCollection.Add("@eff_start_date", programTargetsEntity.EffStartDate, DbType.DateTime);
                paramCollection.Add("@eff_end_date", programTargetsEntity.EffEndDate, DbType.DateTime);

                paramCollection.Add("@last_modified_by", programTargetsEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ProgramSql["UpdateProgramTarget"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public List<ProgramTargetsEntity> GetProgramTargets(int programId, DateTime fromDate, DateTime toDate, ArgsEntity args)
        {
            DbDataReader drProgram = null;

            try
            {
                List<ProgramTargetsEntity> programTargetsList = new List<ProgramTargetsEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ProgramId", programId, DbType.Int32);
                paramCollection.Add("@fromDate", fromDate.ToString("yyyy-MM-dd"), DbType.String);
                paramCollection.Add("@toDate", toDate.ToString("yyyy-MM-dd"), DbType.String);

                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Int32);

                drProgram = this.DataAcessService.ExecuteQuery(ProgramSql["GetProgramTargets"], paramCollection);

                if (drProgram != null && drProgram.HasRows)
                {
                    int programTargetIdOrdinal = drProgram.GetOrdinal("program_target_id");
                    int programIdOrdinal = drProgram.GetOrdinal("program_id");
                    int programNameOrdinal = drProgram.GetOrdinal("name");

                    int targetPercentageOrdinal = drProgram.GetOrdinal("target_percentage");
                    int targetSticksOrdinal = drProgram.GetOrdinal("target_sticks");
                    int targetIncentiveAmountOrdinal = drProgram.GetOrdinal("target_incentive_amount");
                    int targetIncentivePreIssueProductIdOrdinal = drProgram.GetOrdinal("target_incentive_pre_issue_product_id");
                    int targetIncentivePreIssueProductNameOrdinal = drProgram.GetOrdinal("target_incentive_pre_issue_product_name");
                    int targetIncentivePreIssueProductSkuOrdinal = drProgram.GetOrdinal("target_incentive_pre_issue_product_sku");
                    int targetIncentivePreIssueAmountSticksOrdinal = drProgram.GetOrdinal("target_incentive_pre_issue_amount_sticks");

                    int bonusPercentageOrdinal = drProgram.GetOrdinal("bonus_percentage");
                    int bonusSticksOrdinal = drProgram.GetOrdinal("bonus_sticks");
                    int bonusIncentiveAmountOrdinal = drProgram.GetOrdinal("bonus_incentive_amount");
                    int bonusIncentivePreIssueProductIdOrdinal = drProgram.GetOrdinal("bonus_incentive_pre_issue_product_id");
                    int bonusIncentivePreIssueProductNameOrdinal = drProgram.GetOrdinal("bonus_incentive_pre_issue_product_name");
                    int bonusIncentivePreIssueProductSkuOrdinal = drProgram.GetOrdinal("bonus_incentive_pre_issue_product_sku");
                    int bonusIncentivePreIssueAmountSticksOrdinal = drProgram.GetOrdinal("bonus_incentive_pre_issue_amount_sticks");

                    int effStartDateOrdinal = drProgram.GetOrdinal("eff_start_date");
                    int effEndDateOrdinal = drProgram.GetOrdinal("eff_end_date");

                    int totalCountOrdindal = drProgram.GetOrdinal("total_count");

                    while (drProgram.Read())
                    {
                        ProgramTargetsEntity programTargetsEntity = ProgramTargetsEntity.CreateObject();
                        ProgramEntity programEntity = CreateObject();

                        if (!drProgram.IsDBNull(programTargetIdOrdinal)) programTargetsEntity.ProgramTargetId = drProgram.GetInt32(programTargetIdOrdinal);

                        if (!drProgram.IsDBNull(programIdOrdinal)) programEntity.ProgramId = drProgram.GetInt32(programIdOrdinal);
                        if (!drProgram.IsDBNull(programNameOrdinal)) programEntity.ProgramName = drProgram.GetString(programNameOrdinal);

                        if (!drProgram.IsDBNull(targetPercentageOrdinal)) programTargetsEntity.TargetPercentage = drProgram.GetDouble(targetPercentageOrdinal);
                        if (!drProgram.IsDBNull(targetSticksOrdinal)) programTargetsEntity.TargetSticks = drProgram.GetInt32(targetSticksOrdinal);
                        if (!drProgram.IsDBNull(targetIncentiveAmountOrdinal)) programTargetsEntity.TargetIncentiveAmount = Convert.ToDouble(drProgram.GetDecimal(targetIncentiveAmountOrdinal));
                        if (!drProgram.IsDBNull(targetIncentivePreIssueProductIdOrdinal)) programTargetsEntity.TargetIncentivePreIssueProductId = drProgram.GetInt32(targetIncentivePreIssueProductIdOrdinal);
                        if (!drProgram.IsDBNull(targetIncentivePreIssueProductNameOrdinal)) programTargetsEntity.TargetIncentivePreIssueProductName = drProgram.GetString(targetIncentivePreIssueProductNameOrdinal);
                        if (!drProgram.IsDBNull(targetIncentivePreIssueProductSkuOrdinal)) programTargetsEntity.TargetIncentivePreIssueProductSku = drProgram.GetInt32(targetIncentivePreIssueProductSkuOrdinal);
                        if (!drProgram.IsDBNull(targetIncentivePreIssueAmountSticksOrdinal)) programTargetsEntity.TargetIncentivePreIssueAmountSticks = drProgram.GetInt32(targetIncentivePreIssueAmountSticksOrdinal);

                        if (!drProgram.IsDBNull(bonusPercentageOrdinal)) programTargetsEntity.BonusPercentage = drProgram.GetDouble(bonusPercentageOrdinal);
                        if (!drProgram.IsDBNull(bonusSticksOrdinal)) programTargetsEntity.BonusSticks = drProgram.GetInt32(bonusSticksOrdinal);
                        if (!drProgram.IsDBNull(bonusIncentiveAmountOrdinal)) programTargetsEntity.BonusIncentiveAmount = Convert.ToDouble(drProgram.GetDecimal(bonusIncentiveAmountOrdinal));
                        if (!drProgram.IsDBNull(bonusIncentivePreIssueProductIdOrdinal)) programTargetsEntity.BonusIncentivePreIssueProductId = drProgram.GetInt32(bonusIncentivePreIssueProductIdOrdinal);
                        if (!drProgram.IsDBNull(bonusIncentivePreIssueProductNameOrdinal)) programTargetsEntity.BonusIncentivePreIssueProductName = drProgram.GetString(bonusIncentivePreIssueProductNameOrdinal);
                        if (!drProgram.IsDBNull(bonusIncentivePreIssueProductSkuOrdinal)) programTargetsEntity.BonusIncentivePreIssueProductSku = drProgram.GetInt32(bonusIncentivePreIssueProductSkuOrdinal);
                        if (!drProgram.IsDBNull(bonusIncentivePreIssueAmountSticksOrdinal)) programTargetsEntity.BonusIncentivePreIssueAmountSticks = drProgram.GetInt32(bonusIncentivePreIssueAmountSticksOrdinal);

                        if (!drProgram.IsDBNull(effStartDateOrdinal)) programTargetsEntity.EffStartDate = drProgram.GetDateTime(effStartDateOrdinal);
                        if (!drProgram.IsDBNull(effEndDateOrdinal)) programTargetsEntity.EffEndDate = drProgram.GetDateTime(effEndDateOrdinal);

                        if (!drProgram.IsDBNull(totalCountOrdindal)) programTargetsEntity.TotalCount = drProgram.GetInt32(totalCountOrdindal);

                        programTargetsEntity.Program = programEntity;
                        programTargetsList.Add(programTargetsEntity);
                    }
                }

                return programTargetsList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProgram != null)
                    drProgram.Close();
            }
        }

        public int InsertProgram(ProgramEntity programEntity) {

            DbDataReader drProgram = null;
            bool successful = false;
            int iNoRec = 0;
            int programId = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Name", programEntity.ProgramName, DbType.String);
                paramCollection.Add("@CreatedBy", programEntity.CreatedBy, DbType.String);

                drProgram = this.DataAcessService.ExecuteQuery(ProgramSql["InsertProgram"], paramCollection);
                if (drProgram != null && drProgram.HasRows)
                {
                    int programIdOrdinal = drProgram.GetOrdinal("program_id");

                    while (drProgram.Read())
                    {
                        if (!drProgram.IsDBNull(programIdOrdinal)) programId = drProgram.GetInt32(programIdOrdinal);
                    }
                }

                //if (iNoRec > 0)
                //    successful = true;

                return programId;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drProgram != null)
                    drProgram.Close();
            }
        }

        public bool UpdateProgram(ProgramEntity programEntity)
        {

            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ProgramId", programEntity.ProgramId, DbType.Int32);
                paramCollection.Add("@Name", programEntity.ProgramName, DbType.String);
                paramCollection.Add("@CreatedBy", programEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ProgramSql["UpdateProgram"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool InsertProgramCustomers(ProgramCustomerEntity programCustomerEntity)
        {

            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CustCode", programCustomerEntity.CustCode, DbType.String);
                paramCollection.Add("@ProgramId", programCustomerEntity.ProgramId, DbType.Int32);
                paramCollection.Add("@Volume", programCustomerEntity.Volume, DbType.Int32);
                paramCollection.Add("@SelectionType", programCustomerEntity.SelectionType, DbType.String);
                paramCollection.Add("@CreatedBy", programCustomerEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ProgramSql["InsertProgramCustomers"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool DeleteProgramAllCustomers(int programId)
        {

            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ProgramId", programId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ProgramSql["DeleteProgramAllCustomers"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool DeleteProgram(int programId)
        {

            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ProgramId", programId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ProgramSql["DeleteProgram"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public List<ProgramCustomerEntity> GetProgramDetailsById(int programId)
        {
            DbDataReader drProgram = null;

            try
            {
                List<ProgramCustomerEntity> progCustomerList = new List<ProgramCustomerEntity>();
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                ProgramCustomerEntity programEntity = null;

                paramCollection.Add("@ProgramId", programId, DbType.Int32);

                drProgram = this.DataAcessService.ExecuteQuery(ProgramSql["GetProgramDetailsById"], paramCollection);

                if (drProgram != null && drProgram.HasRows)
                {

                    int programIdOrdinal = drProgram.GetOrdinal("program_id");
                    int programNameOrdinal = drProgram.GetOrdinal("name");
                    int custCodeOrdinal = drProgram.GetOrdinal("cust_code");
                    int volumeOrdinal = drProgram.GetOrdinal("volume");
                    int selectionTypeOrdinal = drProgram.GetOrdinal("selection_type");
    

                    while (drProgram.Read())
                    {

                        programEntity = ProgramCustomerEntity.CreateObject();

                        if (!drProgram.IsDBNull(programIdOrdinal)) programEntity.ProgramId = drProgram.GetInt32(programIdOrdinal);
                        if (!drProgram.IsDBNull(programNameOrdinal)) programEntity.ProgramName = drProgram.GetString(programNameOrdinal);
                        if (!drProgram.IsDBNull(custCodeOrdinal)) programEntity.CustCode = drProgram.GetString(custCodeOrdinal);
                        if (!drProgram.IsDBNull(volumeOrdinal)) programEntity.Volume = drProgram.GetInt32(volumeOrdinal);
                        if (!drProgram.IsDBNull(selectionTypeOrdinal)) programEntity.SelectionType = drProgram.GetString(selectionTypeOrdinal);
                        

                        progCustomerList.Add(programEntity);
                    }
                }

                return progCustomerList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProgram != null)
                    drProgram.Close();
            }
        }

        public List<ProgramEntity> GetAllPrograms()
        {
            DbDataReader drProgram = null;

            try
            {
                List<ProgramEntity> progCustomerList = new List<ProgramEntity>();
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                ProgramEntity programEntity = null;

                drProgram = this.DataAcessService.ExecuteQuery(ProgramSql["GetAllPrograms"]);

                if (drProgram != null && drProgram.HasRows)
                {

                    int programIdOrdinal = drProgram.GetOrdinal("id");
                    int programNameOrdinal = drProgram.GetOrdinal("name");


                    while (drProgram.Read())
                    {

                        programEntity = ProgramEntity.CreateObject();

                        if (!drProgram.IsDBNull(programIdOrdinal)) programEntity.ProgramId = drProgram.GetInt32(programIdOrdinal);
                        if (!drProgram.IsDBNull(programNameOrdinal)) programEntity.ProgramName = drProgram.GetString(programNameOrdinal);

                        progCustomerList.Add(programEntity);
                    }
                }

                return progCustomerList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProgram != null)
                    drProgram.Close();
            }
        }

        public bool IsProgramExists(ProgramEntity programEntity)
        {
            DbDataReader drProgram = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@ProgramName", programEntity.ProgramName, DbType.String);
                paramCollection.Add("@ProgramId", programEntity.ProgramId, DbType.Int32);

                drProgram = this.DataAcessService.ExecuteQuery(ProgramSql["GetProgramExist"], paramCollection);
                if (drProgram != null && drProgram.HasRows)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drProgram != null)
                    drProgram.Close();
            }
        }


    }
}
