﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public class SessionDAO:BaseDAO
    {
        private DbSqlAdapter SessionSql { get; set; }

        private void RegisterSql()
        {
            this.SessionSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.CommonSql.xml", ApplicationService.Instance.DbProvider);
        }

        public SessionDAO()
        {
            RegisterSql();
        }

        public SessionDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public SessionEntity CreateObject()
        {
            return SessionEntity.CreateObject();
        }

        public SessionEntity CreateObject(int entityId)
        {
            return SessionEntity.CreateObject(entityId);
        }

        public int InitiateSession(SessionEntity Session)
        {
            try
            {
                Session.SessionId = Convert.ToInt32(this.DataAcessService.GetOneValue(SessionSql["GetNextSessionId"]));

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@SessionId", Session.SessionId, DbType.Int32);
                paramCollection.Add("@Username", ValidateString(Session.UserName), DbType.String);
                paramCollection.Add("@LoginDatetime", Session.LoginDateTime.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);

                this.DataAcessService.ExecuteNonQuery(SessionSql["InsertSession"], paramCollection);

                return Session.SessionId.Value;
            }
            catch
            {
                throw;
            }
        }

        public int UpdateLogoutTime(SessionEntity Session)
        {
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@SessionId", Session.SessionId, DbType.Int32);
                paramCollection.Add("@LogoutDateTime", Session.LogoutDateTime.ToUniversalTime().ToString("dd-MMM-yyyy HH:mm:ss"), DbType.String);

               return this.DataAcessService.ExecuteNonQuery(SessionSql["UpdateSessionLogoutTime"], paramCollection);
            }
            catch
            {
                throw;
            }
        }

        public bool CreateTransactionLog(string originator, 
            string transTime, 
            string transType, 
            string transModule, 
            string transDesc, 
            string locLatitude, 
            string locLongitude,
            string deviceType)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertTransactionLog";

                    objCommand.Parameters.AddWithValue("@Originator", originator);
                    objCommand.Parameters.AddWithValue("@TransTime", transTime);
                    objCommand.Parameters.AddWithValue("@TransType", transType);
                    objCommand.Parameters.AddWithValue("@TransModule", transModule);
                    objCommand.Parameters.AddWithValue("@TransDesc", transDesc);
                    objCommand.Parameters.AddWithValue("@LocLatitude", locLatitude);
                    objCommand.Parameters.AddWithValue("@LocLongitude", locLongitude);
                    objCommand.Parameters.AddWithValue("@DeviceType", deviceType);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }

                    iNoRec = objCommand.ExecuteNonQuery();

                    if (iNoRec > 0)
                        successful = true;
                }
            }
            catch (Exception ex)
            {
                successful = false;
            }

            return successful;
        }

        public bool CreateTransactionLogDetails(string originator, string transTime, string transType, string transModule, string transDesc)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertTransactionHistoryDetails";

                    objCommand.Parameters.AddWithValue("@Originator", originator);
                    objCommand.Parameters.AddWithValue("@TransDate", transTime);
                    objCommand.Parameters.AddWithValue("@TransType", transType);
                    objCommand.Parameters.AddWithValue("@TransModule", transModule);
                    objCommand.Parameters.AddWithValue("@TransDescription", transDesc);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }

                    iNoRec = objCommand.ExecuteNonQuery();

                    if (iNoRec > 0)
                        successful = true;
                }
            }
            catch (Exception ex)
            {
                successful = false;
            }

            return successful;
        }
    }
}
