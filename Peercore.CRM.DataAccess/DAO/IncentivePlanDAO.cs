﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using System.Data.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class IncentivePlanDAO:BaseDAO
    {
        private DbSqlAdapter IncentivePlanSql { get; set; }

        private void RegisterSql()
        {
            this.IncentivePlanSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.IncentivePlanSql.xml", ApplicationService.Instance.DbProvider);
        }

        public IncentivePlanDAO()
        {
            RegisterSql();
        }

        public IncentivePlanDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        #region - IncentivePlanHeader -

        public bool InsertIncentivePlanHeader(out int incentivePlanHeaderId, IncentivePlanHeaderEntity incentivePlanHeaderEntity)
        {
            bool successful = false;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@plan_name", incentivePlanHeaderEntity.PlanName, DbType.String);
                paramCollection.Add("@description", incentivePlanHeaderEntity.Description, DbType.String);
                paramCollection.Add("@start_date", incentivePlanHeaderEntity.StartDate, DbType.DateTime);
                paramCollection.Add("@end_date", incentivePlanHeaderEntity.EndDate, DbType.DateTime);
                paramCollection.Add("@division_id", incentivePlanHeaderEntity.DivisionId, DbType.Int32);
                paramCollection.Add("@is_active", incentivePlanHeaderEntity.IsActive, DbType.Boolean);
                paramCollection.Add("@created_by", incentivePlanHeaderEntity.CreatedBy, DbType.String);
                paramCollection.Add("@incentive_plan_header_id", null, DbType.Int32, ParameterDirection.Output);

                incentivePlanHeaderId = this.DataAcessService.ExecuteNonQuery(IncentivePlanSql["InsertIncentivePlanHeader"], paramCollection, true, "@incentive_plan_header_id");

                if (incentivePlanHeaderId > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateIncentivePlanHeader(IncentivePlanHeaderEntity incentivePlanHeaderEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@incentive_plan_header_id", incentivePlanHeaderEntity.IncentivePlanHeaderId, DbType.Int32);
                paramCollection.Add("@plan_name", incentivePlanHeaderEntity.PlanName, DbType.String);
                paramCollection.Add("@description", incentivePlanHeaderEntity.Description, DbType.String);
                paramCollection.Add("@start_date", incentivePlanHeaderEntity.StartDate, DbType.DateTime);
                paramCollection.Add("@end_date", incentivePlanHeaderEntity.EndDate, DbType.DateTime);
                paramCollection.Add("@division_id", incentivePlanHeaderEntity.DivisionId, DbType.Int32);
                paramCollection.Add("@is_active", incentivePlanHeaderEntity.IsActive, DbType.Boolean);
                paramCollection.Add("@last_modified_by", incentivePlanHeaderEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(IncentivePlanSql["UpdateIncentivePlanHeader"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool DeactivateIncentivePlanHeader(IncentivePlanHeaderEntity incentivePlanHeaderEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@incentive_plan_header_id", incentivePlanHeaderEntity.IncentivePlanHeaderId, DbType.Int32);
                paramCollection.Add("@last_modified_by", incentivePlanHeaderEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(IncentivePlanSql["DeactivateIncentivePlanHeader"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public List<IncentivePlanHeaderEntity> GetAllIncentivePlanHeader(ArgsEntity args)
        {
            DbDataReader drIncentivePlan = null;

            try
            {
                List<IncentivePlanHeaderEntity> incentivePlanHeaderList = new List<IncentivePlanHeaderEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ChildOriginators", args.ChildOriginators, DbType.String);
                paramCollection.Add("@AddParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Int32);


                drIncentivePlan = this.DataAcessService.ExecuteQuery(IncentivePlanSql["GetAllIncentivePlanHeader"], paramCollection);

                if (drIncentivePlan != null && drIncentivePlan.HasRows)
                {
                    int idOrdinal = drIncentivePlan.GetOrdinal("incentive_plan_header_id");
                    int planNameOrdinal = drIncentivePlan.GetOrdinal("plan_name");
                    int descOrdinal = drIncentivePlan.GetOrdinal("description");
                    int startDateOrdinal = drIncentivePlan.GetOrdinal("start_date");
                    int endDateOrdinal = drIncentivePlan.GetOrdinal("end_date");
                    int divIdOrdinal = drIncentivePlan.GetOrdinal("division_id");
                    int divNameOrdinal = drIncentivePlan.GetOrdinal("division_name");
                    int isActiveOrdinal = drIncentivePlan.GetOrdinal("is_active");
                    int createdByOrdinal = drIncentivePlan.GetOrdinal("created_by");
                    int createdDateOrdinal = drIncentivePlan.GetOrdinal("created_date");
                    int lastModifiedByOrdinal = drIncentivePlan.GetOrdinal("last_modified_by");
                    int lastModifiedDateOrdinal = drIncentivePlan.GetOrdinal("last_modified_date");
                    int totalCountOrdinal = drIncentivePlan.GetOrdinal("total_count");

                    while (drIncentivePlan.Read())
                    {
                        IncentivePlanHeaderEntity incentivePlanHeaderEntity = IncentivePlanHeaderEntity.CreateObject();

                        if (!drIncentivePlan.IsDBNull(idOrdinal)) incentivePlanHeaderEntity.IncentivePlanHeaderId = drIncentivePlan.GetInt32(idOrdinal);
                        if (!drIncentivePlan.IsDBNull(planNameOrdinal)) incentivePlanHeaderEntity.PlanName = drIncentivePlan.GetString(planNameOrdinal);
                        if (!drIncentivePlan.IsDBNull(descOrdinal)) incentivePlanHeaderEntity.Description = drIncentivePlan.GetString(descOrdinal);
                        if (!drIncentivePlan.IsDBNull(startDateOrdinal)) incentivePlanHeaderEntity.StartDate = drIncentivePlan.GetDateTime(startDateOrdinal);
                        if (!drIncentivePlan.IsDBNull(endDateOrdinal)) incentivePlanHeaderEntity.EndDate = drIncentivePlan.GetDateTime(endDateOrdinal);
                        if (!drIncentivePlan.IsDBNull(divIdOrdinal)) incentivePlanHeaderEntity.DivisionId = drIncentivePlan.GetInt32(divIdOrdinal);
                        if (!drIncentivePlan.IsDBNull(divNameOrdinal)) incentivePlanHeaderEntity.DivisionName = drIncentivePlan.GetString(divNameOrdinal);
                        if (!drIncentivePlan.IsDBNull(isActiveOrdinal)) incentivePlanHeaderEntity.IsActive = drIncentivePlan.GetBoolean(isActiveOrdinal);
                        if (!drIncentivePlan.IsDBNull(createdByOrdinal)) incentivePlanHeaderEntity.CreatedBy = drIncentivePlan.GetString(createdByOrdinal);
                        if (!drIncentivePlan.IsDBNull(createdDateOrdinal)) incentivePlanHeaderEntity.CreatedDate = drIncentivePlan.GetDateTime(createdDateOrdinal);
                        if (!drIncentivePlan.IsDBNull(lastModifiedByOrdinal)) incentivePlanHeaderEntity.LastModifiedBy = drIncentivePlan.GetString(lastModifiedByOrdinal);
                        if (!drIncentivePlan.IsDBNull(lastModifiedDateOrdinal)) incentivePlanHeaderEntity.LastModifiedDate = drIncentivePlan.GetDateTime(lastModifiedDateOrdinal);
                        if (!drIncentivePlan.IsDBNull(totalCountOrdinal)) incentivePlanHeaderEntity.TotalCount = drIncentivePlan.GetInt32(totalCountOrdinal);

                        incentivePlanHeaderList.Add(incentivePlanHeaderEntity);
                    }
                }

                return incentivePlanHeaderList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drIncentivePlan != null)
                    drIncentivePlan.Close();
            }
        }

        #endregion

        #region - IncentivePlanDetails -

        public bool InsertIncentivePlanDetail(out int incentivePlanDetailId, IncentivePlanDetailsEntity incentivePlanDetailsEntity)
        {
            bool successful = false;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@incentive_plan_header_id", incentivePlanDetailsEntity.IncentivePlanHeaderId, DbType.Int32);
                paramCollection.Add("@plan_details", incentivePlanDetailsEntity.PlanDetails, DbType.String);
                paramCollection.Add("@ip_details_type_id", incentivePlanDetailsEntity.IpDetailsTypeId, DbType.Int32);
                paramCollection.Add("@is_active", incentivePlanDetailsEntity.IsActive, DbType.Boolean);
                paramCollection.Add("@created_by", incentivePlanDetailsEntity.CreatedBy, DbType.String);
                paramCollection.Add("@incentive_plan_details_id", null, DbType.Int32, ParameterDirection.Output);

                incentivePlanDetailId = this.DataAcessService.ExecuteNonQuery(IncentivePlanSql["InsertIncentivePlanDetail"], paramCollection, true, "@incentive_plan_details_id");

                if (incentivePlanDetailId > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateIncentivePlanDetail(IncentivePlanDetailsEntity incentivePlanDetailsEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@incentive_plan_details_id", incentivePlanDetailsEntity.IncentivePlanDetailsId, DbType.Int32);
                paramCollection.Add("@incentive_plan_header_id", incentivePlanDetailsEntity.IncentivePlanHeaderId, DbType.Int32);
                paramCollection.Add("@plan_details", incentivePlanDetailsEntity.PlanDetails, DbType.String);
                paramCollection.Add("@ip_details_type_id", incentivePlanDetailsEntity.IpDetailsTypeId, DbType.Int32);
                paramCollection.Add("@is_active", incentivePlanDetailsEntity.IsActive, DbType.Boolean);
                paramCollection.Add("@last_modified_by", incentivePlanDetailsEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(IncentivePlanSql["UpdateIncentivePlanDetail"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public List<IncentivePlanDetailsEntity> GetIncentivePlanDetailByHeaderId(int incentivePlanHeaderId)
        {
            DbDataReader drIncentivePlan = null;

            try
            {
                List<IncentivePlanDetailsEntity> incentivePlanDetailList = new List<IncentivePlanDetailsEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@incentive_plan_header_id", incentivePlanHeaderId, DbType.Int32);

                drIncentivePlan = this.DataAcessService.ExecuteQuery(IncentivePlanSql["GetIncentivePlanDetailByHeaderId"], paramCollection);

                if (drIncentivePlan != null && drIncentivePlan.HasRows)
                {
                    int idOrdinal = drIncentivePlan.GetOrdinal("incentive_plan_details_id");
                    int headerIdOrdinal = drIncentivePlan.GetOrdinal("incentive_plan_header_id");
                    int detailsOrdinal = drIncentivePlan.GetOrdinal("plan_details");
                    int detailsTypeIdOrdinal = drIncentivePlan.GetOrdinal("ip_details_type_id");
                    int isActiveOrdinal = drIncentivePlan.GetOrdinal("is_active");
                    int createdByOrdinal = drIncentivePlan.GetOrdinal("created_by");
                    int createdDateOrdinal = drIncentivePlan.GetOrdinal("created_date");
                    int lastModifiedByOrdinal = drIncentivePlan.GetOrdinal("last_modified_by");
                    int lastModifiedDateOrdinal = drIncentivePlan.GetOrdinal("last_modified_date");
                    int totalCountOrdinal = drIncentivePlan.GetOrdinal("total_count");

                    while (drIncentivePlan.Read())
                    {
                        IncentivePlanDetailsEntity incentivePlanDetailsEntity = IncentivePlanDetailsEntity.CreateObject();

                        if (!drIncentivePlan.IsDBNull(idOrdinal)) incentivePlanDetailsEntity.IncentivePlanDetailsId = drIncentivePlan.GetInt32(idOrdinal);
                        if (!drIncentivePlan.IsDBNull(headerIdOrdinal)) incentivePlanDetailsEntity.IncentivePlanHeaderId = drIncentivePlan.GetInt32(headerIdOrdinal);
                        if (!drIncentivePlan.IsDBNull(detailsOrdinal)) incentivePlanDetailsEntity.PlanDetails= drIncentivePlan.GetString(detailsOrdinal);
                        if (!drIncentivePlan.IsDBNull(detailsTypeIdOrdinal)) incentivePlanDetailsEntity.IpDetailsTypeId = drIncentivePlan.GetInt32(detailsTypeIdOrdinal);
                        if (!drIncentivePlan.IsDBNull(isActiveOrdinal)) incentivePlanDetailsEntity.IsActive = drIncentivePlan.GetBoolean(isActiveOrdinal);
                        if (!drIncentivePlan.IsDBNull(createdByOrdinal)) incentivePlanDetailsEntity.CreatedBy = drIncentivePlan.GetString(createdByOrdinal);
                        if (!drIncentivePlan.IsDBNull(createdDateOrdinal)) incentivePlanDetailsEntity.CreatedDate = drIncentivePlan.GetDateTime(createdDateOrdinal);
                        if (!drIncentivePlan.IsDBNull(lastModifiedByOrdinal)) incentivePlanDetailsEntity.LastModifiedBy = drIncentivePlan.GetString(lastModifiedByOrdinal);
                        if (!drIncentivePlan.IsDBNull(lastModifiedDateOrdinal)) incentivePlanDetailsEntity.LastModifiedDate = drIncentivePlan.GetDateTime(lastModifiedDateOrdinal);
                        if (!drIncentivePlan.IsDBNull(totalCountOrdinal)) incentivePlanDetailsEntity.TotalCount = drIncentivePlan.GetInt32(totalCountOrdinal);

                        incentivePlanDetailList.Add(incentivePlanDetailsEntity);
                    }
                }

                return incentivePlanDetailList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drIncentivePlan != null)
                    drIncentivePlan.Close();
            }
        }

        #endregion

        #region - IncentiveSecondarySalesWeekly -

        public bool InsertIncentiveSecondarySalesWeekly(IncentiveSecondarySalesWeeklyEntity incentiveSecondarySalesWeeklyEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@incentive_plan_details_id", incentiveSecondarySalesWeeklyEntity.IncentivePlanDetailsId, DbType.Int32);
                paramCollection.Add("@header", incentiveSecondarySalesWeeklyEntity.Header, DbType.String);
                paramCollection.Add("@cum_tar_perc", incentiveSecondarySalesWeeklyEntity.CumTarPerc, DbType.Double);
                paramCollection.Add("@inc_perc", incentiveSecondarySalesWeeklyEntity.IncPerc, DbType.Double);
                paramCollection.Add("@is_active", incentiveSecondarySalesWeeklyEntity.IsActive, DbType.Boolean);
                paramCollection.Add("@created_by", incentiveSecondarySalesWeeklyEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(IncentivePlanSql["InsertIncentiveSecondarySalesWeekly"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateIncentiveSecondarySalesWeekly(IncentiveSecondarySalesWeeklyEntity incentiveSecondarySalesWeeklyEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@secondary_sales_weekly_id", incentiveSecondarySalesWeeklyEntity.SecondarySalesWeeklyId, DbType.Int32);
                paramCollection.Add("@incentive_plan_details_id", incentiveSecondarySalesWeeklyEntity.IncentivePlanDetailsId, DbType.Int32);
                paramCollection.Add("@header", incentiveSecondarySalesWeeklyEntity.Header, DbType.String);
                paramCollection.Add("@cum_tar_perc", incentiveSecondarySalesWeeklyEntity.CumTarPerc, DbType.Double);
                paramCollection.Add("@inc_perc", incentiveSecondarySalesWeeklyEntity.IncPerc, DbType.Double);
                paramCollection.Add("@is_active", incentiveSecondarySalesWeeklyEntity.IsActive, DbType.Boolean);
                paramCollection.Add("@last_modified_by", incentiveSecondarySalesWeeklyEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(IncentivePlanSql["UpdateIncentiveSecondarySalesWeekly"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool SaveTargetAndIncentiveNew(string TargetBreakDay, string First14Days, string Last14Days)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@TargetBreakDay", TargetBreakDay, DbType.Int32);
                paramCollection.Add("@First14Days", First14Days, DbType.Double);
                paramCollection.Add("@Last14Days", Last14Days, DbType.Double);

                iNoRec = this.DataAcessService.ExecuteNonQuery(IncentivePlanSql["UpdateTargetAndIncentiveNew"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public List<TargetEntity> LoadTargetMaster()
        {
            DbDataReader drIncentivePlan = null;

            try
            {
                List<TargetEntity> targetMastList = new List<TargetEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                drIncentivePlan = this.DataAcessService.ExecuteQuery(IncentivePlanSql["GetAll_Target_Master"], paramCollection);

                if (drIncentivePlan != null && drIncentivePlan.HasRows)
                {
                    int tarInvIdOrdinal = drIncentivePlan.GetOrdinal("tarInvId");
                    int tarInvTypeOrdinal = drIncentivePlan.GetOrdinal("tarInvType");
                    int tarInvDescOrdinal = drIncentivePlan.GetOrdinal("tarInvDesc");
                    int tarInvValOrdinal = drIncentivePlan.GetOrdinal("tarInvVal");
                    int tarInvTargetDayOrdinal = drIncentivePlan.GetOrdinal("targetDay");

                    while (drIncentivePlan.Read())
                    {
                        TargetEntity targetMastEntity = TargetEntity.CreateObject();

                        if (!drIncentivePlan.IsDBNull(tarInvIdOrdinal)) targetMastEntity.TargetInvId = drIncentivePlan.GetDecimal(tarInvIdOrdinal);
                        if (!drIncentivePlan.IsDBNull(tarInvTypeOrdinal)) targetMastEntity.TargetInvType = drIncentivePlan.GetString(tarInvTypeOrdinal);
                        if (!drIncentivePlan.IsDBNull(tarInvDescOrdinal)) targetMastEntity.TargetInvDesc = drIncentivePlan.GetString(tarInvDescOrdinal);
                        if (!drIncentivePlan.IsDBNull(tarInvValOrdinal)) targetMastEntity.TargetInvVal = drIncentivePlan.GetDouble(tarInvValOrdinal);
                        if (!drIncentivePlan.IsDBNull(tarInvTargetDayOrdinal)) targetMastEntity.TargetBreakDownDay = drIncentivePlan.GetInt32(tarInvTargetDayOrdinal);

                        targetMastList.Add(targetMastEntity);
                    }
                }

                return targetMastList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drIncentivePlan != null)
                    drIncentivePlan.Close();
            }
        }

        public List<IncentiveSecondarySalesWeeklyEntity> GetIncentiveSecondarySalesWeeklyByDetailId(int incentivePlanDetailId)
        {
            DbDataReader drIncentivePlan = null;

            try
            {
                List<IncentiveSecondarySalesWeeklyEntity> incentiveSecondarySalesWeeklyList = new List<IncentiveSecondarySalesWeeklyEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@incentive_plan_details_id", incentivePlanDetailId, DbType.Int32);

                drIncentivePlan = this.DataAcessService.ExecuteQuery(IncentivePlanSql["GetIncentiveSecondarySalesWeeklyByDetailId"], paramCollection);

                if (drIncentivePlan != null && drIncentivePlan.HasRows)
                {
                    int idOrdinal = drIncentivePlan.GetOrdinal("secondary_sales_weekly_id");
                    int detailIdOrdinal = drIncentivePlan.GetOrdinal("incentive_plan_details_id");
                    int headerOrdinal = drIncentivePlan.GetOrdinal("header");
                    int targetPercOrdinal = drIncentivePlan.GetOrdinal("cum_tar_perc");
                    int incentivePercOrdinal = drIncentivePlan.GetOrdinal("inc_perc");
                    int isActiveOrdinal = drIncentivePlan.GetOrdinal("is_active");
                    int createdByOrdinal = drIncentivePlan.GetOrdinal("created_by");
                    int createdDateOrdinal = drIncentivePlan.GetOrdinal("created_date");
                    int lastModifiedByOrdinal = drIncentivePlan.GetOrdinal("last_modified_by");
                    int lastModifiedDateOrdinal = drIncentivePlan.GetOrdinal("last_modified_date");
                    int totalCountOrdinal = drIncentivePlan.GetOrdinal("total_count");

                    while (drIncentivePlan.Read())
                    {
                        IncentiveSecondarySalesWeeklyEntity incentiveSecondarySalesWeeklyEntity = IncentiveSecondarySalesWeeklyEntity.CreateObject();

                        if (!drIncentivePlan.IsDBNull(idOrdinal)) incentiveSecondarySalesWeeklyEntity.SecondarySalesWeeklyId = drIncentivePlan.GetInt32(idOrdinal);
                        if (!drIncentivePlan.IsDBNull(detailIdOrdinal)) incentiveSecondarySalesWeeklyEntity.IncentivePlanDetailsId = drIncentivePlan.GetInt32(detailIdOrdinal);
                        if (!drIncentivePlan.IsDBNull(headerOrdinal)) incentiveSecondarySalesWeeklyEntity.Header = drIncentivePlan.GetString(headerOrdinal);
                        if (!drIncentivePlan.IsDBNull(targetPercOrdinal)) incentiveSecondarySalesWeeklyEntity.CumTarPerc = drIncentivePlan.GetDouble(targetPercOrdinal);
                        if (!drIncentivePlan.IsDBNull(incentivePercOrdinal)) incentiveSecondarySalesWeeklyEntity.IncPerc = drIncentivePlan.GetDouble(incentivePercOrdinal);
                        if (!drIncentivePlan.IsDBNull(isActiveOrdinal)) incentiveSecondarySalesWeeklyEntity.IsActive = drIncentivePlan.GetBoolean(isActiveOrdinal);
                        if (!drIncentivePlan.IsDBNull(createdByOrdinal)) incentiveSecondarySalesWeeklyEntity.CreatedBy = drIncentivePlan.GetString(createdByOrdinal);
                        if (!drIncentivePlan.IsDBNull(createdDateOrdinal)) incentiveSecondarySalesWeeklyEntity.CreatedDate = drIncentivePlan.GetDateTime(createdDateOrdinal);
                        if (!drIncentivePlan.IsDBNull(lastModifiedByOrdinal)) incentiveSecondarySalesWeeklyEntity.LastModifiedBy = drIncentivePlan.GetString(lastModifiedByOrdinal);
                        if (!drIncentivePlan.IsDBNull(lastModifiedDateOrdinal)) incentiveSecondarySalesWeeklyEntity.LastModifiedDate = drIncentivePlan.GetDateTime(lastModifiedDateOrdinal);
                        if (!drIncentivePlan.IsDBNull(totalCountOrdinal)) incentiveSecondarySalesWeeklyEntity.TotalCount = drIncentivePlan.GetInt32(totalCountOrdinal);

                        incentiveSecondarySalesWeeklyList.Add(incentiveSecondarySalesWeeklyEntity);
                    }
                }

                return incentiveSecondarySalesWeeklyList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drIncentivePlan != null)
                    drIncentivePlan.Close();
            }
        }

        public bool DeactivateSecondarySalesWeekly(IncentiveSecondarySalesWeeklyEntity incentiveSecondarySalesWeeklyEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@secondary_sales_weekly_id", incentiveSecondarySalesWeeklyEntity.SecondarySalesWeeklyId, DbType.Int32);
                paramCollection.Add("@last_modified_by", incentiveSecondarySalesWeeklyEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(IncentivePlanSql["DeactivateSecondarySalesWeekly"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        #endregion

        #region - IncentiveSecondarySales -

        public bool InsertIncentiveSecondarySalesPartial(IncentiveSecondarySalesPartialEntity incentiveSecondarySalesPartialEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@incentive_plan_details_id", incentiveSecondarySalesPartialEntity.IncentivePlanDetailsId, DbType.Int32);
                paramCollection.Add("@achievement_perc", incentiveSecondarySalesPartialEntity.AchievementPerc, DbType.Double);
                paramCollection.Add("@incentive_perc", incentiveSecondarySalesPartialEntity.IncentivePerc, DbType.Double);
                paramCollection.Add("@is_active", incentiveSecondarySalesPartialEntity.IsActive, DbType.Boolean);
                paramCollection.Add("@created_by", incentiveSecondarySalesPartialEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(IncentivePlanSql["InsertIncentiveSecondarySalesPartial"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateIncentiveSecondarySalesPartial(IncentiveSecondarySalesPartialEntity incentiveSecondarySalesPartialEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@secondary_sales_partial_id", incentiveSecondarySalesPartialEntity.SecondarySalesPartialId, DbType.Int32);
                paramCollection.Add("@incentive_plan_details_id", incentiveSecondarySalesPartialEntity.IncentivePlanDetailsId, DbType.Int32);
                paramCollection.Add("@achievement_perc", incentiveSecondarySalesPartialEntity.AchievementPerc, DbType.Double);
                paramCollection.Add("@incentive_perc", incentiveSecondarySalesPartialEntity.IncentivePerc, DbType.Double);
                paramCollection.Add("@is_active", incentiveSecondarySalesPartialEntity.IsActive, DbType.Boolean);
                paramCollection.Add("@last_modified_by", incentiveSecondarySalesPartialEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(IncentivePlanSql["UpdateIncentiveSecondarySalesPartial"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public List<IncentiveSecondarySalesPartialEntity> GetIncentiveSecondarySalesPartialByDetailId(int incentivePlanDetailId)
        {
            DbDataReader drIncentivePlan = null;

            try
            {
                List<IncentiveSecondarySalesPartialEntity> IncentiveSecondarySalesPartialList = new List<IncentiveSecondarySalesPartialEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@incentive_plan_details_id", incentivePlanDetailId, DbType.Int32);

                drIncentivePlan = this.DataAcessService.ExecuteQuery(IncentivePlanSql["GetIncentiveSecondarySalesPartialByDetailId"], paramCollection);

                if (drIncentivePlan != null && drIncentivePlan.HasRows)
                {
                    int idOrdinal = drIncentivePlan.GetOrdinal("secondary_sales_partial_id");
                    int detailIdOrdinal = drIncentivePlan.GetOrdinal("incentive_plan_details_id");
                    int achievementPercOrdinal = drIncentivePlan.GetOrdinal("achievement_perc");
                    int incentivePercOrdinal = drIncentivePlan.GetOrdinal("incentive_perc");
                    int isActiveOrdinal = drIncentivePlan.GetOrdinal("is_active");
                    int createdByOrdinal = drIncentivePlan.GetOrdinal("created_by");
                    int createdDateOrdinal = drIncentivePlan.GetOrdinal("created_date");
                    int lastModifiedByOrdinal = drIncentivePlan.GetOrdinal("last_modified_by");
                    int lastModifiedDateOrdinal = drIncentivePlan.GetOrdinal("last_modified_date");
                    int totalCountOrdinal = drIncentivePlan.GetOrdinal("total_count");

                    while (drIncentivePlan.Read())
                    {
                        IncentiveSecondarySalesPartialEntity incentiveSecondarySalesPartialEntity = IncentiveSecondarySalesPartialEntity.CreateObject();

                        if (!drIncentivePlan.IsDBNull(idOrdinal)) incentiveSecondarySalesPartialEntity.SecondarySalesPartialId = drIncentivePlan.GetInt32(idOrdinal);
                        if (!drIncentivePlan.IsDBNull(detailIdOrdinal)) incentiveSecondarySalesPartialEntity.IncentivePlanDetailsId = drIncentivePlan.GetInt32(detailIdOrdinal);
                        if (!drIncentivePlan.IsDBNull(achievementPercOrdinal)) incentiveSecondarySalesPartialEntity.AchievementPerc = drIncentivePlan.GetDouble(achievementPercOrdinal);
                        if (!drIncentivePlan.IsDBNull(incentivePercOrdinal)) incentiveSecondarySalesPartialEntity.IncentivePerc = drIncentivePlan.GetDouble(incentivePercOrdinal);
                        if (!drIncentivePlan.IsDBNull(isActiveOrdinal)) incentiveSecondarySalesPartialEntity.IsActive = drIncentivePlan.GetBoolean(isActiveOrdinal);
                        if (!drIncentivePlan.IsDBNull(createdByOrdinal)) incentiveSecondarySalesPartialEntity.CreatedBy = drIncentivePlan.GetString(createdByOrdinal);
                        if (!drIncentivePlan.IsDBNull(createdDateOrdinal)) incentiveSecondarySalesPartialEntity.CreatedDate = drIncentivePlan.GetDateTime(createdDateOrdinal);
                        if (!drIncentivePlan.IsDBNull(lastModifiedByOrdinal)) incentiveSecondarySalesPartialEntity.LastModifiedBy = drIncentivePlan.GetString(lastModifiedByOrdinal);
                        if (!drIncentivePlan.IsDBNull(lastModifiedDateOrdinal)) incentiveSecondarySalesPartialEntity.LastModifiedDate = drIncentivePlan.GetDateTime(lastModifiedDateOrdinal);
                        if (!drIncentivePlan.IsDBNull(totalCountOrdinal)) incentiveSecondarySalesPartialEntity.TotalCount = drIncentivePlan.GetInt32(totalCountOrdinal);

                        IncentiveSecondarySalesPartialList.Add(incentiveSecondarySalesPartialEntity);
                    }
                }

                return IncentiveSecondarySalesPartialList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drIncentivePlan != null)
                    drIncentivePlan.Close();
            }
        }

        public bool DeactivateSecondarySalesPartial(IncentiveSecondarySalesPartialEntity incentiveSecondarySalesPartialEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@secondary_sales_partial_id", incentiveSecondarySalesPartialEntity.SecondarySalesPartialId, DbType.Int32);
                paramCollection.Add("@last_modified_by", incentiveSecondarySalesPartialEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(IncentivePlanSql["DeactivateSecondarySalesPartial"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        #endregion
    }
}
