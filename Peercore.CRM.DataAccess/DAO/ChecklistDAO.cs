﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
//using Peercore.CRM.Entities.CompositeEntities;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common;

namespace Peercore.CRM.DataAccess.DAO
{
    public class ChecklistDAO : BaseDAO
    {
        private DbSqlAdapter ChecklistSql { get; set; }

        private void RegisterSql()
        {
            this.ChecklistSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.ChecklistSql.xml", ApplicationService.Instance.DbProvider);
        }

        public ChecklistDAO()
        {
            RegisterSql();
        }

        public ChecklistDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public ChecklistEntity CreateObject()
        {
            return ChecklistEntity.CreateObject();
        }

        public ChecklistEntity CreateObject(int entityId)
        {
            return ChecklistEntity.CreateObject(entityId);
        }

        public List<ChecklistEntity> GetChecklist(int iLeadStageID)
        {
            DbDataReader drChecklist = null;

            try
            {
                List<ChecklistEntity> checklistCollection = new List<ChecklistEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@LeadStageId", iLeadStageID, DbType.Int32);

                drChecklist = this.DataAcessService.ExecuteQuery(ChecklistSql["GetChecklistItems"], paramCollection);

                if (drChecklist != null && drChecklist.HasRows)
                {
                    int checklistIdOrdinal = drChecklist.GetOrdinal("checklist_id");
                    int checklistNameOrdinal = drChecklist.GetOrdinal("checklist_name");
                    int leadStageIdOrdinal = drChecklist.GetOrdinal("lead_stage_id");

                    while (drChecklist.Read())
                    {
                        ChecklistEntity checklist = ChecklistEntity.CreateObject();

                        if (!drChecklist.IsDBNull(checklistIdOrdinal)) checklist.ChecklistID = drChecklist.GetInt32(checklistIdOrdinal);
                        if (!drChecklist.IsDBNull(checklistNameOrdinal)) checklist.ChecklistName = drChecklist.GetString(checklistNameOrdinal);
                        if (!drChecklist.IsDBNull(leadStageIdOrdinal)) checklist.PipelineStageID = drChecklist.GetInt32(leadStageIdOrdinal);

                        checklistCollection.Add(checklist);
                    }
                }

                return checklistCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drChecklist != null)
                    drChecklist.Close();
            }
        }

        public List<ChecklistImageEntity> GetChecklistImageById(int CheckListId)
        {
            DbDataReader drChecklist = null;

            try
            {
                List<ChecklistImageEntity> checklistCollection = new List<ChecklistImageEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CheckListId", CheckListId, DbType.Int32);

                drChecklist = this.DataAcessService.ExecuteQuery(ChecklistSql["GetChecklistImageById"], paramCollection);

                if (drChecklist != null && drChecklist.HasRows)
                {
                    int checklistIdOrdinal = drChecklist.GetOrdinal("checklist_id");
                    int image_nameOrdinal = drChecklist.GetOrdinal("image_name");
                    int image_contentOrdinal = drChecklist.GetOrdinal("image_content");

                    while (drChecklist.Read())
                    {
                        ChecklistImageEntity checklist = ChecklistImageEntity.CreateObject();

                        if (!drChecklist.IsDBNull(checklistIdOrdinal)) checklist.ChecklistId = drChecklist.GetInt32(checklistIdOrdinal);
                        if (!drChecklist.IsDBNull(image_nameOrdinal)) checklist.ImageName = drChecklist.GetString(image_nameOrdinal);

                        if (drChecklist[image_nameOrdinal] != null)
                        {
                            if (!drChecklist.IsDBNull(image_contentOrdinal)) checklist.ImageContent = (byte[])drChecklist[image_contentOrdinal];
                        }

                        checklistCollection.Add(checklist);
                    }
                }

                return checklistCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drChecklist != null)
                    drChecklist.Close();
            }
        }

        public bool Save(ChecklistEntity leadChecklistEntity)
        {
            DbDataReader drChecklist = null;

            try
            {
                // Check whether the Document already exists
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                //paramCollection.Add("@LeadId", leadChecklistEntity.LeadID, DbType.Int32);
                paramCollection.Add("@PiepelineStageID", leadChecklistEntity.PipelineStageID, DbType.Int32);
                paramCollection.Add("@ChecklistID", leadChecklistEntity.ChecklistID, DbType.Int32);

                drChecklist = this.DataAcessService.ExecuteQuery(ChecklistSql["GetLeadChecklistDetails"], paramCollection);

                if (drChecklist.HasRows)
                {
                    drChecklist.Close();
                    // Update Dcoument
                    return UpdateLeadChecklist(leadChecklistEntity);
                }
                else
                {
                    drChecklist.Close();
                    // Insert Document
                   return InsertLeadCheklist(leadChecklistEntity);
                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drChecklist != null && (!drChecklist.IsClosed))
                    drChecklist.Close();
            }
        }

        private bool InsertLeadCheklist(ChecklistEntity leadChecklistEntity)
        {
            bool insertSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ChecklistName", leadChecklistEntity.ChecklistName, DbType.Int32);
                paramCollection.Add("@PiepelineStageID", leadChecklistEntity.PipelineStageID, DbType.Int32);
                paramCollection.Add("@ChecklistID", leadChecklistEntity.ChecklistID, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ChecklistSql["InsertLeadCheklist"], paramCollection);

                if (iNoRec > 0)
                    insertSuccessful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return insertSuccessful;
        }

        private bool UpdateLeadChecklist(ChecklistEntity leadChecklistEntity)
        {
            bool updatetSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ChecklistName", leadChecklistEntity.ChecklistName, DbType.Int32);
                paramCollection.Add("@PiepelineStageID", leadChecklistEntity.PipelineStageID, DbType.Int32);
                paramCollection.Add("@ChecklistID", leadChecklistEntity.ChecklistID, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ChecklistSql["UpdateLeadChecklist"], paramCollection);

                if (iNoRec > 0)
                    updatetSuccessful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return updatetSuccessful;
        }

        public string SaveQuery(ChecklistEntity leadChecklist)
        {
            DbDataReader drLeadChecklist = null;

            try
            {
                // Check whether the Document already exists

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@LeadId", leadChecklist.LeadID, DbType.Int32);
                paramCollection.Add("@PiepelineStageID", leadChecklist.PipelineStageID, DbType.Int32);
                paramCollection.Add("@ChecklistID", leadChecklist.ChecklistID, DbType.Int32);

                drLeadChecklist = this.DataAcessService.ExecuteQuery(ChecklistSql["GetLeadChecklistDetails"], paramCollection);

                //drLeadChecklist = (IngresDataReader)oDataHandle.ExecuteSelect(string.Format(CRMSQLs.GetLeadChecklistDetails, leadChecklist.LeadID, leadChecklist.PipelineStageID, leadChecklist.ChecklistID));
                if (drLeadChecklist.HasRows)
                {
                    drLeadChecklist.Close();
                    // Update Dcoument
                    paramCollection.Add("@isExec", 0, DbType.Boolean);
                    paramCollection.Add("@sql", "", DbType.String);

                    return this.DataAcessService.GetOneValue(ChecklistSql["UpdateLeadChecklist"], paramCollection).ToString();

                    //this.DataAcessService.ExecuteQuery(ChecklistSql["UpdateLeadChecklist"], paramCollection);
                    //return paramCollection.Find(s => s.Name == "@sql").Value.ToString();
                }
                else
                {
                    drLeadChecklist.Close();
                    // Insert Document

                    paramCollection.Add("@isExec", false, DbType.Boolean);
                    paramCollection.Add("@sql", "", DbType.String);
                    return this.DataAcessService.GetOneValue(ChecklistSql["InsertLeadCheklist"], paramCollection).ToString();
                    //this.DataAcessService.ExecuteQuery(ChecklistSql["InsertLeadCheklist"], paramCollection);
                    //return paramCollection.Find(s => s.Name == "@sql").Value.ToString();

                }
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drLeadChecklist != null)
                    drLeadChecklist.Close();
            }
        }

        public List<ChecklistTypeEntity> GetChecklistTypeList()
        {
            DbDataReader drChecklist = null;

            try
            {
                List<ChecklistTypeEntity> checklistTypeCollection = new List<ChecklistTypeEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                drChecklist = this.DataAcessService.ExecuteQuery(ChecklistSql["GetChecklistTypes"]);


                if (drChecklist != null && drChecklist.HasRows)
                {
                    int checklistTypeIdOrdinal = drChecklist.GetOrdinal("checklist_type_id");
                    int checklistTypeNameOrdinal = drChecklist.GetOrdinal("name");

                    while (drChecklist.Read())
                    {
                        ChecklistTypeEntity checklistType = ChecklistTypeEntity.CreateObject();

                        if (!drChecklist.IsDBNull(checklistTypeIdOrdinal)) checklistType.Id = drChecklist.GetInt32(checklistTypeIdOrdinal);
                        if (!drChecklist.IsDBNull(checklistTypeNameOrdinal)) checklistType.Name = drChecklist.GetString(checklistTypeNameOrdinal);

                        checklistTypeCollection.Add(checklistType);
                    }
                }

                return checklistTypeCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drChecklist != null)
                    drChecklist.Close();
            }
        }

        public List<ChecklistMasterEntity> GetDailyChecklistForRep(int routeID)
        {
            DbDataReader drChecklist = null;

            try
            {
                List<ChecklistMasterEntity> checklistCollection = new List<ChecklistMasterEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@RouteId", routeID, DbType.Int32);

                drChecklist = this.DataAcessService.ExecuteQuery(ChecklistSql["GetChecklistItemsforMobile"], paramCollection);

                if (drChecklist != null && drChecklist.HasRows)
                {
                    int checklistIdOrdinal = drChecklist.GetOrdinal("checklist_id");
                    int checklistTypeIdOrdinal = drChecklist.GetOrdinal("checklist_type_id");
                    int taskOrdinal = drChecklist.GetOrdinal("task");
                    // int customerOrdinal = drChecklist.GetOrdinal("customer");
                    int hideOrdinal = drChecklist.GetOrdinal("hide");
                    int createdDateOrdinal = drChecklist.GetOrdinal("created_date");

                    while (drChecklist.Read())
                    {
                        ChecklistMasterEntity checklist = ChecklistMasterEntity.CreateObject();

                        if (!drChecklist.IsDBNull(checklistIdOrdinal)) checklist.ChecklistId = drChecklist.GetInt32(checklistIdOrdinal);
                        if (!drChecklist.IsDBNull(checklistTypeIdOrdinal)) checklist.ChecklistTypeId = drChecklist.GetInt32(checklistTypeIdOrdinal);
                        if (!drChecklist.IsDBNull(taskOrdinal)) checklist.Task = drChecklist.GetString(taskOrdinal);
                        //   if (!drChecklist.IsDBNull(customerOrdinal)) checklist.CustomerCode = drChecklist.GetString(customerOrdinal);
                        if (!drChecklist.IsDBNull(hideOrdinal)) checklist.IsHidden = Convert.ToInt32(drChecklist.GetBoolean(hideOrdinal));
                        if (!drChecklist.IsDBNull(createdDateOrdinal)) checklist.CreatedDate = drChecklist.GetDateTime(createdDateOrdinal);

                        checklistCollection.Add(checklist);
                    }
                }

                return checklistCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drChecklist != null)
                    drChecklist.Close();
            }
        }

        public List<ChecklistImageEntity> GetDailyChecklistImages(string strChecklistIds)
        {
            DbDataReader drChecklist = null;

            try
            {
                List<ChecklistImageEntity> checklistImageCollection = new List<ChecklistImageEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CheckListIds", strChecklistIds, DbType.String);

                drChecklist = this.DataAcessService.ExecuteQuery(ChecklistSql["GetChecklistImages"], paramCollection);

                if (drChecklist != null && drChecklist.HasRows)
                {
                    int checklistIdOrdinal = drChecklist.GetOrdinal("checklist_id");
                    int imageNameOrdinal = drChecklist.GetOrdinal("image_name");
                    int imageOrdinal = drChecklist.GetOrdinal("image");


                    while (drChecklist.Read())
                    {
                        ChecklistImageEntity checklistImage = ChecklistImageEntity.CreateObject();

                        if (!drChecklist.IsDBNull(checklistIdOrdinal)) checklistImage.ChecklistId = drChecklist.GetInt32(checklistIdOrdinal);
                        if (!drChecklist.IsDBNull(imageNameOrdinal)) checklistImage.ImageName = drChecklist.GetString(imageNameOrdinal);
                        if (!drChecklist.IsDBNull(imageOrdinal)) checklistImage.ImageContent = (byte[])drChecklist[imageOrdinal];

                        checklistImageCollection.Add(checklistImage);
                    }
                }

                return checklistImageCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drChecklist != null)
                    drChecklist.Close();
            }
        }

        public List<ChecklistDetailEntity> GetDailyChecklistDetails(string strChecklistIds)
        {
            DbDataReader drChecklist = null;

            try
            {
                List<ChecklistDetailEntity> checklistDetailCollection = new List<ChecklistDetailEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@CheckListIds", strChecklistIds, DbType.String);

                drChecklist = this.DataAcessService.ExecuteQuery(ChecklistSql["GetChecklistDetails"], paramCollection);

                if (drChecklist != null && drChecklist.HasRows)
                {
                    int checklistIdOrdinal = drChecklist.GetOrdinal("checklist_id");
                    int remarksOrdinal = drChecklist.GetOrdinal("remarks");
                    int createdDateOrdinal = drChecklist.GetOrdinal("created_date");
                    int custCodeOrdinal = drChecklist.GetOrdinal("customer_code");

                    while (drChecklist.Read())
                    {
                        ChecklistDetailEntity checklistDetail = ChecklistDetailEntity.CreateObject();

                        if (!drChecklist.IsDBNull(checklistIdOrdinal)) checklistDetail.ChecklistId = drChecklist.GetInt32(checklistIdOrdinal);
                        if (!drChecklist.IsDBNull(remarksOrdinal)) checklistDetail.Remarks = drChecklist.GetString(remarksOrdinal);
                        if (!drChecklist.IsDBNull(createdDateOrdinal)) checklistDetail.CreatedDate = drChecklist.GetDateTime(createdDateOrdinal);
                        if (!drChecklist.IsDBNull(custCodeOrdinal)) checklistDetail.CustomerCode = drChecklist.GetString(custCodeOrdinal);

                        checklistDetailCollection.Add(checklistDetail);
                    }
                }

                return checklistDetailCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drChecklist != null)
                    drChecklist.Close();
            }
        }

        public bool InsertCustomerChecklist(ChecklistDetailEntity checklistDetail)
        {
            bool insertSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ChecklistId", checklistDetail.ChecklistId, DbType.Int32);
                paramCollection.Add("@Remarks", checklistDetail.Remarks, DbType.String);
                paramCollection.Add("@CreatedDate", checklistDetail.CreatedDate, DbType.DateTime);
                paramCollection.Add("@CustCode", checklistDetail.CustomerCode, DbType.String);
                paramCollection.Add("@RepCode", checklistDetail.RepCode, DbType.String);
                paramCollection.Add("@RouteId", checklistDetail.RouteId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ChecklistSql["InsertChecklistDetails"], paramCollection);

                if (iNoRec > 0)
                    insertSuccessful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return insertSuccessful;
        }

        public List<ChecklistCustomerEntity> GetDailyChecklistCustomers(int routeID)
        {
            DbDataReader drChecklist = null;
            try
            {
                List<ChecklistCustomerEntity> checklistCustomerCollection = new List<ChecklistCustomerEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@RouteId", routeID, DbType.Int32);

                drChecklist = this.DataAcessService.ExecuteQuery(ChecklistSql["GetChecklistCustomers"], paramCollection);

                if (drChecklist != null && drChecklist.HasRows)
                {
                    int checklistIdOrdinal = drChecklist.GetOrdinal("checklist_id");
                    int custCodeOrdinal = drChecklist.GetOrdinal("customer_code");


                    while (drChecklist.Read())
                    {
                        ChecklistCustomerEntity checklistcustomer = ChecklistCustomerEntity.CreateObject();

                        if (!drChecklist.IsDBNull(checklistIdOrdinal)) checklistcustomer.CheckListID = drChecklist.GetInt32(checklistIdOrdinal);
                        if (!drChecklist.IsDBNull(custCodeOrdinal)) checklistcustomer.CustomerCode = drChecklist.GetString(custCodeOrdinal);

                        checklistCustomerCollection.Add(checklistcustomer);
                    }
                }

                return checklistCustomerCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drChecklist != null)
                    drChecklist.Close();
            }
        }

        public int InsertCheklistItem(ChecklistMasterEntity checklistMasterEntity)
        {
            // bool insertSuccessful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Task", checklistMasterEntity.Task, DbType.String);
                paramCollection.Add("@ChecklistTypeId", checklistMasterEntity.ChecklistTypeId, DbType.Int32);
                paramCollection.Add("@ID", null, DbType.Int32, ParameterDirection.Output);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ChecklistSql["InsertChecklistItem"], paramCollection, true, "@ID");
                //if (iNoRec > 0)
                //    insertSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return iNoRec;
        }

        public int UpdateCheklistItem(ChecklistMasterEntity checklistMasterEntity)
        {
            // bool insertSuccessful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Task", checklistMasterEntity.Task, DbType.String);
                paramCollection.Add("@ChecklistId", checklistMasterEntity.ChecklistId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ChecklistSql["UpdateChecklistItem"], paramCollection);
                //if (iNoRec > 0)
                //    insertSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return iNoRec;
        }

        public bool InsertCheklistCustomer(ChecklistCustomerEntity checklistCustomerEntity)
        {
            bool insertSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ChecklistId", checklistCustomerEntity.CheckListID, DbType.Int32);
                paramCollection.Add("@CustCode", checklistCustomerEntity.CustomerCode, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ChecklistSql["InsertChecklistCustomer"], paramCollection);

                if (iNoRec > 0)
                    insertSuccessful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return insertSuccessful;
        }

        public bool InsertCheklistRep(ChecklistRepEntity checklistRepEntity)
        {
            bool insertSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ChecklistId", checklistRepEntity.ChecklistId, DbType.Int32);
                paramCollection.Add("@RepCode", checklistRepEntity.RepCode, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ChecklistSql["InsertChecklistCustomer"], paramCollection);

                if (iNoRec > 0)
                    insertSuccessful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return insertSuccessful;
        }

        public bool InsertCheklistRoute(ChecklistRouteEntity checklistRouteEntity)
        {
            bool insertSuccessful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ChecklistId", checklistRouteEntity.ChecklistId, DbType.Int32);
                paramCollection.Add("@RouteId", checklistRouteEntity.RouteId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ChecklistSql["InsertChecklistCustomer"], paramCollection);

                if (iNoRec > 0)
                    insertSuccessful = true;
            }
            catch (Exception)
            {

                throw;
            }
            return insertSuccessful;
        }

        public bool InsertCheklistImage(ChecklistImageEntity chImage)
        {
            bool insertSuccessful = false;
            int iNoRec = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ChecklistId", chImage.ChecklistId, DbType.Int32);
                paramCollection.Add("@ImageName", chImage.ImageName, DbType.String);
                paramCollection.Add("@ImageContent", chImage.ImageContent, DbType.Binary);
                iNoRec = this.DataAcessService.ExecuteNonQuery(ChecklistSql["InsertChecklistImage"], paramCollection);
                if (iNoRec > 0)
                    insertSuccessful = true;
            }
            catch (Exception)
            {
                throw;
            }
            return insertSuccessful;
        }

        public List<ChecklistMasterEntity> GetChecklistMaster()
        {
            DbDataReader drChecklist = null;

            try
            {
                List<ChecklistMasterEntity> checklistCollection = new List<ChecklistMasterEntity>();


                drChecklist = this.DataAcessService.ExecuteQuery(ChecklistSql["GetChecklistItems"]);

                if (drChecklist != null && drChecklist.HasRows)
                {
                    int checklistIdOrdinal = drChecklist.GetOrdinal("checklist_id");
                    int taskOrdinal = drChecklist.GetOrdinal("task");
                    int typeOrdinal = drChecklist.GetOrdinal("type");

                    while (drChecklist.Read())
                    {
                        ChecklistMasterEntity checklist = ChecklistMasterEntity.CreateObject();

                        if (!drChecklist.IsDBNull(checklistIdOrdinal)) checklist.ChecklistId = drChecklist.GetInt32(checklistIdOrdinal);
                        if (!drChecklist.IsDBNull(taskOrdinal)) checklist.Task = drChecklist.GetString(taskOrdinal);
                        if (!drChecklist.IsDBNull(typeOrdinal)) checklist.ChecklistType = drChecklist.GetString(typeOrdinal);

                        checklistCollection.Add(checklist);
                    }
                }

                return checklistCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drChecklist != null)
                    drChecklist.Close();
            }
        }

        public List<ChecklistEntity> GetChecklistDataAndCount(ArgsEntity args)
        {
            DbDataReader drChecklist = null;

            try
            {
                List<ChecklistEntity> checklistCollection = new List<ChecklistEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OrderBy", args.OrderBy, DbType.String);
                paramCollection.Add("@AdditionalParams", args.AdditionalParams, DbType.String);
                paramCollection.Add("@StartIndex", args.StartIndex, DbType.Int32);
                paramCollection.Add("@RowCount", args.RowCount, DbType.Int32);
                paramCollection.Add("@ShowSQL", 0, DbType.Boolean);
                drChecklist = this.DataAcessService.ExecuteQuery(ChecklistSql["GetChecklistDataAndCount"], paramCollection);

                if (drChecklist != null && drChecklist.HasRows)
                {
                    int checklistIdOrdinal = drChecklist.GetOrdinal("checklist_id");
                    int taskOrdinal = drChecklist.GetOrdinal("task");
                    int typeOrdinal = drChecklist.GetOrdinal("type_name");

                    int totalCountOrdinal = drChecklist.GetOrdinal("total_count");

                    while (drChecklist.Read())
                    {
                        ChecklistEntity checklist = ChecklistEntity.CreateObject();

                        if (!drChecklist.IsDBNull(checklistIdOrdinal)) checklist.ChecklistID = drChecklist.GetInt32(checklistIdOrdinal);
                        if (!drChecklist.IsDBNull(typeOrdinal)) checklist.ChecklistTypeName = drChecklist.GetString(typeOrdinal);
                        if (!drChecklist.IsDBNull(taskOrdinal)) checklist.ChecklistName = drChecklist.GetString(taskOrdinal);
                        if (!drChecklist.IsDBNull(totalCountOrdinal)) checklist.TotalCount = drChecklist.GetInt32(totalCountOrdinal);

                        checklistCollection.Add(checklist);
                    }
                }

                return checklistCollection;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drChecklist != null)
                    drChecklist.Close();
            }
        }
    }
}
