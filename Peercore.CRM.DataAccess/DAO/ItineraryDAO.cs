﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.Workflow.Common;
using Peercore.CRM.Entities;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.CRM.Shared;
using System.Data.SqlClient;

namespace Peercore.CRM.DataAccess.DAO
{
    public class ItineraryDAO:BaseDAO
    {
        private DbSqlAdapter ItinerarySql { get; set; }

        private void RegisterSql()
        {
            this.ItinerarySql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.ItinerarySQL.xml", ApplicationService.Instance.DbProvider);
        }

        public ItineraryDAO()
        {
            RegisterSql();
        }

        public ItineraryDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        //public ItineraryEntity CreateObject()
        //{
        //    return ItineraryEntity.CreateObject();
        //}

        //public ItineraryEntity CreateObject(int entityId)
        //{
        //    return ItineraryEntity.CreateObject(entityId);
        //}

        public List<ItineraryDetailEntity> GetItineraryDetailsByHeaderId(string tme, DateTime date, int headerId)
        {
            DbDataReader drItenerary = null;

            try
            {
                List<ItineraryDetailEntity> itineraryEntityList = new List<ItineraryDetailEntity>();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@CreatedBy", tme, DbType.String);
                paramCollection.Add("@Date", date, DbType.DateTime);
                paramCollection.Add("@HeaderId", headerId, DbType.Int32);

                drItenerary = this.DataAcessService.ExecuteQuery(ItinerarySql["GetItineraryDetailsByHeaderId"], paramCollection);

                if (drItenerary != null && drItenerary.HasRows)
                {
                    int dateOrdinal = drItenerary.GetOrdinal("date");
                    int activityIdOrdinal = drItenerary.GetOrdinal("activity_id");
                    int routeIdOrdinal = drItenerary.GetOrdinal("route_id");
                    int headerIdOrdinal = drItenerary.GetOrdinal("itinerary_header_id");
                    int detailIdOrdinal = drItenerary.GetOrdinal("detail_id");
                   // int baseTownOrdinal = drItenerary.GetOrdinal("base_town");
                    int nameOrdinal = drItenerary.GetOrdinal("visit_towns");
                    int directMilesOrdinal = drItenerary.GetOrdinal("direct_miles");
                    int localMilesOrdinal = drItenerary.GetOrdinal("local_miles");
                    int totalMilesOrdinal = drItenerary.GetOrdinal("total_miles");
                    int workProgramOrdinal = drItenerary.GetOrdinal("work_program");
                    int nightAtOrdinal = drItenerary.GetOrdinal("night_at");
                    int sequenceOrdinal = drItenerary.GetOrdinal("sequence");
                    int sequenceRouteOrdinal = drItenerary.GetOrdinal("route_name");

                    while (drItenerary.Read())
                    {
                        ItineraryDetailEntity itineraryDetailEntity = ItineraryDetailEntity.CreateObject();

                        if (!drItenerary.IsDBNull(dateOrdinal)) itineraryDetailEntity.Date = drItenerary.GetDateTime(dateOrdinal);
                        if (!drItenerary.IsDBNull(activityIdOrdinal)) itineraryDetailEntity.ActivityId = drItenerary.GetInt32(activityIdOrdinal);
                        if (!drItenerary.IsDBNull(routeIdOrdinal)) itineraryDetailEntity.RouteId = drItenerary.GetInt32(routeIdOrdinal);
                        if (!drItenerary.IsDBNull(headerIdOrdinal)) itineraryDetailEntity.ItineraryHeaderId = drItenerary.GetInt32(headerIdOrdinal);
                        if (!drItenerary.IsDBNull(detailIdOrdinal)) itineraryDetailEntity.ItineraryDetailId = drItenerary.GetInt32(detailIdOrdinal);
                      //  if (!drItenerary.IsDBNull(baseTownOrdinal)) itineraryEntity.BaseTown = drItenerary.GetString(baseTownOrdinal);
                        if (!drItenerary.IsDBNull(nameOrdinal)) itineraryDetailEntity.TownsToVisit = drItenerary.GetString(nameOrdinal);
                        if (!drItenerary.IsDBNull(directMilesOrdinal)) itineraryDetailEntity.DirectMiles = drItenerary.GetInt32(directMilesOrdinal);
                        if (!drItenerary.IsDBNull(localMilesOrdinal)) itineraryDetailEntity.LocalMiles = drItenerary.GetInt32(localMilesOrdinal);
                        if (!drItenerary.IsDBNull(totalMilesOrdinal)) itineraryDetailEntity.TotalMiles = drItenerary.GetInt32(totalMilesOrdinal);
                        if (!drItenerary.IsDBNull(workProgramOrdinal)) itineraryDetailEntity.WorkProgram = drItenerary.GetString(workProgramOrdinal);
                        if (!drItenerary.IsDBNull(nightAtOrdinal)) itineraryDetailEntity.NightAt = drItenerary.GetString(nightAtOrdinal);
                        if (!drItenerary.IsDBNull(sequenceOrdinal)) itineraryDetailEntity.Sequence = drItenerary.GetInt32(sequenceOrdinal);                
                        itineraryDetailEntity.CreatedBy = tme;
                        if (!drItenerary.IsDBNull(sequenceRouteOrdinal)) itineraryDetailEntity.SequenceRoute = drItenerary.GetString(sequenceRouteOrdinal);    

                        itineraryEntityList.Add(itineraryDetailEntity);
                    }
                }

                return itineraryEntityList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drItenerary != null)
                    drItenerary.Close();
            }
        }

        public ItineraryHeaderEntity GetItineraryHeaderByUser(string tme, DateTime date)
        {
            //DbDataReader drItenerary = null;

            //try
            //{
            //    ItineraryHeaderEntity itineraryHeaderEntity = ItineraryHeaderEntity.CreateObject();
            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            //    paramCollection.Add("@CreatedBy", tme, DbType.String);
            //    paramCollection.Add("@Date", date, DbType.DateTime);

            //    drItenerary = this.DataAcessService.ExecuteQuery(ItinerarySql["GetItineraryHeaderByUser"], paramCollection);

            //    if (drItenerary != null && drItenerary.HasRows)
            //    {
            //        int nameOrdinal = drItenerary.GetOrdinal("name");
            //        int headerIdOrdinal = drItenerary.GetOrdinal("header_id");
            //        int callCycleIdOrdinal = drItenerary.GetOrdinal("call_cycle_id");
            //        int baseTownOrdinal = drItenerary.GetOrdinal("base_town");
            //        int assignToOrdinal = drItenerary.GetOrdinal("assign_to");
            //        int statusOrdinal = drItenerary.GetOrdinal("status");
            //        int hasSentMailOrdinal = drItenerary.GetOrdinal("has_sent_mail");
            //        int privateOrdinal = drItenerary.GetOrdinal("private");
            //        int contigencyOrdinal = drItenerary.GetOrdinal("contigency");
            //      //  int monthOrdinal = drItenerary.GetOrdinal("month");

            //        if (drItenerary.Read())
            //        {
            //            if (!drItenerary.IsDBNull(nameOrdinal)) itineraryHeaderEntity.ItineraryName = drItenerary.GetString(nameOrdinal);
            //            if (!drItenerary.IsDBNull(headerIdOrdinal)) itineraryHeaderEntity.ItineraryHeaderId = drItenerary.GetInt32(headerIdOrdinal);
            //            if (!drItenerary.IsDBNull(callCycleIdOrdinal)) itineraryHeaderEntity.CallCycleId = drItenerary.GetInt32(callCycleIdOrdinal);
            //            if (!drItenerary.IsDBNull(baseTownOrdinal)) itineraryHeaderEntity.BaseTown = drItenerary.GetString(baseTownOrdinal);
            //            if (!drItenerary.IsDBNull(assignToOrdinal)) itineraryHeaderEntity.AssignedTo = drItenerary.GetString(assignToOrdinal);
            //            if (!drItenerary.IsDBNull(statusOrdinal)) itineraryHeaderEntity.Status = drItenerary.GetString(statusOrdinal);
            //            if (!drItenerary.IsDBNull(hasSentMailOrdinal)) itineraryHeaderEntity.HasSentMail = drItenerary.GetBoolean(hasSentMailOrdinal);
            //            itineraryHeaderEntity.Month = date.ToString("MMMM");
            //            itineraryHeaderEntity.CreatedBy = tme;
            //            if (!drItenerary.IsDBNull(privateOrdinal)) itineraryHeaderEntity.PrivateField = drItenerary.GetInt32(privateOrdinal);
            //            if (!drItenerary.IsDBNull(contigencyOrdinal)) itineraryHeaderEntity.Contigency = drItenerary.GetInt32(contigencyOrdinal);
            //        }
            //    }

            //    return itineraryHeaderEntity;
            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (drItenerary != null)
            //        drItenerary.Close();
            //}

            DataTable objDS = new DataTable();
            ItineraryHeaderEntity itineraryHeaderEntity = ItineraryHeaderEntity.CreateObject();

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetItineraryHeaderByUser";

                    objCommand.Parameters.AddWithValue("@CreatedBy", tme);
                    objCommand.Parameters.AddWithValue("@Date", date);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        DataRow item = objDS.Rows[0];
                        itineraryHeaderEntity.ItineraryName = item["name"].ToString();
                        itineraryHeaderEntity.ItineraryHeaderId = Convert.ToInt32(item["header_id"].ToString());
                        itineraryHeaderEntity.CallCycleId = Convert.ToInt32(item["call_cycle_id"].ToString());
                        itineraryHeaderEntity.BaseTown = item["base_town"].ToString();
                        itineraryHeaderEntity.AssignedTo = item["assign_to"].ToString();
                        itineraryHeaderEntity.Status = item["status"].ToString();
                        itineraryHeaderEntity.HasSentMail = Convert.ToBoolean(item["has_sent_mail"].ToString());
                        itineraryHeaderEntity.Month = date.ToString("MMMM");
                        itineraryHeaderEntity.CreatedBy = tme;
                        itineraryHeaderEntity.PrivateField = Convert.ToInt32(item["private"].ToString());
                        itineraryHeaderEntity.Contigency = Convert.ToInt32(item["contigency"].ToString());
                    }
                }
            }
            catch (Exception ex)
            {
            }


            return itineraryHeaderEntity;

        }

        public bool InsertItineraryHeader(ItineraryHeaderEntity itineraryEntity, out int headerid)
        {
            bool successful = false;
           // int headerid = 0;
            DbDataReader drItenerary = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Name", itineraryEntity.ItineraryName, DbType.String);
                paramCollection.Add("@BaseTown", itineraryEntity.BaseTown, DbType.String);
                paramCollection.Add("@CallCycleId", itineraryEntity.CallCycleId, DbType.Int32);
                paramCollection.Add("@AssignTo", itineraryEntity.CreatedBy, DbType.String);
                paramCollection.Add("@EffDate", itineraryEntity.EffDate, DbType.DateTime);
                paramCollection.Add("@Private", itineraryEntity.PrivateField, DbType.Int32);
                paramCollection.Add("@Contigency", itineraryEntity.Contigency, DbType.Int32);
                paramCollection.Add("@CreatedBy", itineraryEntity.CreatedBy, DbType.String);
                paramCollection.Add("@ItineraryHeaderId", null, DbType.Int32, ParameterDirection.Output);

                headerid = this.DataAcessService.ExecuteNonQuery(ItinerarySql["InsertItineraryHeader"], paramCollection, true, "@ItineraryHeaderId");

                if (headerid > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drItenerary != null)
                    drItenerary.Close();
            }

        }

        public bool InsertItineraryDetails(ItineraryDetailEntity itineraryDetailEntity, int headerId)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drItenerary = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Date", itineraryDetailEntity.Date, DbType.DateTime);
                paramCollection.Add("@RouteId", itineraryDetailEntity.RouteId, DbType.Int32);
                paramCollection.Add("@ItineraryHeadId", headerId , DbType.Int32);
                paramCollection.Add("@Sequence", itineraryDetailEntity.Sequence, DbType.Int32);
                paramCollection.Add("@VisitTowns", itineraryDetailEntity.TownsToVisit, DbType.String);
                paramCollection.Add("@DirectMiles", itineraryDetailEntity.DirectMiles, DbType.Int32);
                paramCollection.Add("@LocalMiles", itineraryDetailEntity.LocalMiles, DbType.Int32);
                paramCollection.Add("@TotalMiles", itineraryDetailEntity.TotalMiles, DbType.Int32);
                paramCollection.Add("@WorkProgram", itineraryDetailEntity.WorkProgram, DbType.String);
                paramCollection.Add("@NightAt", itineraryDetailEntity.NightAt, DbType.String);
                paramCollection.Add("@CreatedBy", itineraryDetailEntity.CreatedBy, DbType.String);
                paramCollection.Add("@SequenceRoute", itineraryDetailEntity.SequenceRoute, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ItinerarySql["InsertItineraryDetails"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drItenerary != null)
                    drItenerary.Close();
            }

        }

        public bool UpdateItineraryHeader(ItineraryHeaderEntity itineraryEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drItenerary = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@Name", itineraryEntity.ItineraryName, DbType.String);
                paramCollection.Add("@BaseTown", itineraryEntity.BaseTown, DbType.String);
                paramCollection.Add("@CallcycleId", itineraryEntity.CallCycleId, DbType.Int32);
                paramCollection.Add("@AssignTo", itineraryEntity.AssignedTo, DbType.String);
                paramCollection.Add("@EffDate", itineraryEntity.EffDate, DbType.DateTime);
                paramCollection.Add("@ModifiedBy", itineraryEntity.LastModifiedBy, DbType.String);
                paramCollection.Add("@Status", itineraryEntity.Status, DbType.String);
                paramCollection.Add("@HasSentMail", itineraryEntity.HasSentMail, DbType.Boolean);
                paramCollection.Add("@Private", itineraryEntity.PrivateField, DbType.Int32);
                paramCollection.Add("@Contigency", itineraryEntity.Contigency, DbType.Int32);
                paramCollection.Add("@ItineraryHeaderId", itineraryEntity.ItineraryHeaderId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ItinerarySql["UpdateItineraryHeader"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drItenerary != null)
                    drItenerary.Close();
            }

        }


        public bool UpdateItineraryDetails(ItineraryDetailEntity itineraryDetailEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drItenerary = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();


                paramCollection.Add("@Date", itineraryDetailEntity.Date, DbType.DateTime);
                paramCollection.Add("@RouteId", itineraryDetailEntity.RouteId, DbType.Int32);
                paramCollection.Add("@ItineraryHeaderId", itineraryDetailEntity.ItineraryHeaderId, DbType.Int32);
                paramCollection.Add("@ItineraryId", itineraryDetailEntity.ItineraryDetailId, DbType.Int32);
                paramCollection.Add("@Sequence", itineraryDetailEntity.Sequence, DbType.Int32);
                paramCollection.Add("@VisitTowns", itineraryDetailEntity.TownsToVisit, DbType.String);
                paramCollection.Add("@DirectMiles", itineraryDetailEntity.DirectMiles, DbType.Int32);
                paramCollection.Add("@LocalMiles", itineraryDetailEntity.LocalMiles, DbType.Int32);
                paramCollection.Add("@TotalMiles", itineraryDetailEntity.TotalMiles, DbType.Int32);
                paramCollection.Add("@WorkProgram", itineraryDetailEntity.WorkProgram, DbType.String);
                paramCollection.Add("@NightAt", itineraryDetailEntity.NightAt, DbType.String);
                paramCollection.Add("@LastModifiedBy", itineraryDetailEntity.LastModifiedBy, DbType.String);
                paramCollection.Add("@SequenceRoute", itineraryDetailEntity.SequenceRoute, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ItinerarySql["UpdateItineraryDetails"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drItenerary != null)
                    drItenerary.Close();
            }

        }

        public int InsertItineraryLogEntry(ItineraryHeaderEntity itineraryHeaderEntity)
        {
            bool successful = false;
            int itineraryLogEntryId = 0;
            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ItineraryHeaderId", itineraryHeaderEntity.ItineraryHeaderId, DbType.Int32);
                paramCollection.Add("@Comments", itineraryHeaderEntity.Comments, DbType.String);
                paramCollection.Add("@CreatedBy", itineraryHeaderEntity.CreatedBy, DbType.String);
                paramCollection.Add("@ID", null, DbType.Int32, ParameterDirection.Output);

                itineraryLogEntryId = this.DataAcessService.ExecuteNonQuery(ItinerarySql["InsertItineraryLogEntry"], paramCollection, true, "@ID");

                if (itineraryLogEntryId > 0)
                    successful = true;

                return itineraryLogEntryId;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            
            }

        }

        public bool UpdateItineraryHeaderStatus(ItineraryHeaderEntity itineraryHeaderEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ItineraryHeaderId", itineraryHeaderEntity.ItineraryHeaderId, DbType.Int32);
                paramCollection.Add("@Status", itineraryHeaderEntity.Status, DbType.String);
                paramCollection.Add("@LastModifiedBy", itineraryHeaderEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(ItinerarySql["UpdateItineraryHeaderStatus"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }

        }

        public ItineraryHeaderEntity GetItineraryLogEntryById(int itineraryLogEntryId)
        {
            DbDataReader drItinerary = null;

            try
            {
                ItineraryHeaderEntity itineraryHeaderEntity = ItineraryHeaderEntity.CreateObject();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ItineraryLogEntryId", itineraryLogEntryId, DbType.Int32);

                drItinerary = this.DataAcessService.ExecuteQuery(ItinerarySql["GetItineraryLogEntryById"], paramCollection);

                if (drItinerary != null && drItinerary.HasRows)
                {
                    int IdOrdinal = drItinerary.GetOrdinal("id");
                    int headerIdOrdinal = drItinerary.GetOrdinal("itinerary_header_id");
                    int commentsOrdinal = drItinerary.GetOrdinal("comments");
                    int effDateOrdinal = drItinerary.GetOrdinal("eff_date");

                    while (drItinerary.Read())
                    {
                        if (!drItinerary.IsDBNull(IdOrdinal)) itineraryHeaderEntity.ItineraryLogEntryId = drItinerary.GetInt32(IdOrdinal);
                        if (!drItinerary.IsDBNull(headerIdOrdinal)) itineraryHeaderEntity.ItineraryHeaderId = drItinerary.GetInt32(headerIdOrdinal);
                        if (!drItinerary.IsDBNull(commentsOrdinal)) itineraryHeaderEntity.Comments = drItinerary.GetString(commentsOrdinal);
                        if (!drItinerary.IsDBNull(effDateOrdinal)) itineraryHeaderEntity.EffDate = drItinerary.GetDateTime(effDateOrdinal);
                    }
                }
                return itineraryHeaderEntity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drItinerary != null)
                    drItinerary.Close();
            }
        }

        public OriginatorEntity GetItineraryAssignedToByHeaderId(int itineraryHeaderId)
        {
            DbDataReader drItinerary = null;

            try
            {
                OriginatorEntity originatorEntity = OriginatorEntity.CreateObject();

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@HeaderId", itineraryHeaderId, DbType.Int32);

                drItinerary = this.DataAcessService.ExecuteQuery(ItinerarySql["GetItineraryAssignedToByHeaderId"], paramCollection);

                if (drItinerary != null && drItinerary.HasRows)
                {
                    int IdOrdinal = drItinerary.GetOrdinal("originator_id");
                    int originatorOrdinal = drItinerary.GetOrdinal("originator");
                    int nameOrdinal = drItinerary.GetOrdinal("name");
                    int emailOrdinal = drItinerary.GetOrdinal("internet_add");

                    while (drItinerary.Read())
                    {
                        if (!drItinerary.IsDBNull(IdOrdinal)) originatorEntity.OriginatorId = drItinerary.GetInt32(IdOrdinal);
                        if (!drItinerary.IsDBNull(originatorOrdinal)) originatorEntity.UserName = drItinerary.GetString(originatorOrdinal);
                        if (!drItinerary.IsDBNull(nameOrdinal)) originatorEntity.Name = drItinerary.GetString(nameOrdinal);
                        if (!drItinerary.IsDBNull(emailOrdinal)) originatorEntity.Email = drItinerary.GetString(emailOrdinal);
                    }
                }
                return originatorEntity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drItinerary != null)
                    drItinerary.Close();
            }
        }

        public ItineraryHeaderEntity GetItineraryHeaderByHeaderId(int itineraryHeaderId)
        {
            DbDataReader drItenerary = null;

            try
            {
                ItineraryHeaderEntity itineraryHeaderEntity = ItineraryHeaderEntity.CreateObject();
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ItineraryHeaderId", itineraryHeaderId, DbType.Int32);

                drItenerary = this.DataAcessService.ExecuteQuery(ItinerarySql["GetItineraryHeaderByHeaderId"], paramCollection);

                if (drItenerary != null && drItenerary.HasRows)
                {
                    int nameOrdinal = drItenerary.GetOrdinal("name");
                    int headerIdOrdinal = drItenerary.GetOrdinal("header_id");
                    int callCycleIdOrdinal = drItenerary.GetOrdinal("call_cycle_id");
                    int baseTownOrdinal = drItenerary.GetOrdinal("base_town");
                    int assignToOrdinal = drItenerary.GetOrdinal("assign_to");
                    int statusOrdinal = drItenerary.GetOrdinal("status");
                    int hasSentMailOrdinal = drItenerary.GetOrdinal("has_sent_mail");
                    int effDateOrdinal = drItenerary.GetOrdinal("eff_date");
                    int createdByOrdinal = drItenerary.GetOrdinal("created_by");
                    int createdDateOrdinal = drItenerary.GetOrdinal("created_date");

                    if (drItenerary.Read())
                    {
                        if (!drItenerary.IsDBNull(nameOrdinal)) itineraryHeaderEntity.ItineraryName = drItenerary.GetString(nameOrdinal);
                        if (!drItenerary.IsDBNull(headerIdOrdinal)) itineraryHeaderEntity.ItineraryHeaderId = drItenerary.GetInt32(headerIdOrdinal);
                        if (!drItenerary.IsDBNull(callCycleIdOrdinal)) itineraryHeaderEntity.CallCycleId = drItenerary.GetInt32(callCycleIdOrdinal);
                        if (!drItenerary.IsDBNull(baseTownOrdinal)) itineraryHeaderEntity.BaseTown = drItenerary.GetString(baseTownOrdinal);
                        if (!drItenerary.IsDBNull(assignToOrdinal)) itineraryHeaderEntity.AssignedTo = drItenerary.GetString(assignToOrdinal);
                        if (!drItenerary.IsDBNull(statusOrdinal)) itineraryHeaderEntity.Status = drItenerary.GetString(statusOrdinal);
                        if (!drItenerary.IsDBNull(hasSentMailOrdinal)) itineraryHeaderEntity.HasSentMail = drItenerary.GetBoolean(hasSentMailOrdinal);

                        if (!drItenerary.IsDBNull(effDateOrdinal)) itineraryHeaderEntity.EffDate = drItenerary.GetDateTime(effDateOrdinal);
                        if (!drItenerary.IsDBNull(createdByOrdinal)) itineraryHeaderEntity.CreatedBy = drItenerary.GetString(createdByOrdinal);
                        if (!drItenerary.IsDBNull(createdDateOrdinal)) itineraryHeaderEntity.CreatedDate = drItenerary.GetDateTime(createdDateOrdinal);
                    }
                }

                return itineraryHeaderEntity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drItenerary != null)
                    drItenerary.Close();
            }
        }
    }
}
