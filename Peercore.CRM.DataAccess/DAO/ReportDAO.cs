﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Entities;
using System.Data.Common;
using System.Collections;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using Peercore.CRM.Shared;
using Peercore.Workflow.Common;


namespace Peercore.CRM.DataAccess.DAO
{
    public class ReportDAO : BaseDAO
    {
        private DbSqlAdapter ReportSql { get; set; }

        private void RegisterSql()
        {
            this.ReportSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.ReportSql.xml", ApplicationService.Instance.DbProvider);
        }

        public ReportDAO()
        {
            RegisterSql();
        }

        public ReportDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        #region DAO methods for DER  report

        //   public DataSet GetDERSummaryHandlersData(ArgsEntity args)
        public List<string> GetDERSummaryHandlersData(ArgsEntity args)
        {
            DbDataAdapter dapDERSummary = null;
            DbDataReader drDERSummary = null;

            try
            {
                //Instantiate report specific data set.
                DataSet dsDERSummary = new DataSet();
                DataTable dtDERSummary = new DataTable();
                dsDERSummary.Tables.Add(dtDERSummary);

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OutString", "", DbType.String, ParameterDirection.Output);
                paramCollection.Add("@Month", args.SMonth, DbType.Int32);
                paramCollection.Add("@Year", args.SYear, DbType.Int32);
                paramCollection.Add("@MarketId", args.MarketId, DbType.Int32);
                paramCollection.Add("@CategoryList", args.Variants1, DbType.String);
                paramCollection.Add("@ProductList", args.Variants2, DbType.String);
                paramCollection.Add("@Firstdate", args.SStartDate, DbType.Date);

                //Retreive the data adapter to fill the report data
                drDERSummary = this.DataAcessService.ExecuteQuery(ReportSql["GetDERSummaryRecordList"], paramCollection);

                List<string> summaryList = new List<string>();
                if (drDERSummary != null && drDERSummary.HasRows)
                {
                    int dataOrdinal = drDERSummary.GetOrdinal("data");

                    while (drDERSummary.Read())
                    {
                        string data = "";
                        if (!drDERSummary.IsDBNull(dataOrdinal)) data = drDERSummary.GetString(dataOrdinal);
                        summaryList.Add(data);

                    }
                }




                //Fill the report specific data set

                return summaryList;

                ////Instantiate report specific data set.
                //DataSet dsDERSummary = new DataSet();
                //DataTable dtDERSummary = new DataTable();
                //dsDERSummary.Tables.Add(dtDERSummary);

                //DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                //paramCollection.Add("@Month", args.SMonth, DbType.Int32);
                //paramCollection.Add("@Year", args.SYear, DbType.Int32);
                //paramCollection.Add("@MarketId", args.MarketId, DbType.Int32);
                //paramCollection.Add("@DanhilVariants", args.Variants1, DbType.String);
                //paramCollection.Add("@AnyVariants", args.Variants2, DbType.String);


                ////Retreive the data adapter to fill the report data
                //dapDERSummary = this.DataAcessService.ExecuteReportQuery(ReportSql["GetDERSummaryHandlersNew"], paramCollection);

                ////Fill the report specific data set
                //dapDERSummary.Fill(dsDERSummary, dsDERSummary.Tables[0].TableName);

                //return dsDERSummary;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dapDERSummary != null)
                    dapDERSummary.Dispose();
            }
        }


        public List<string> GetDERSummaryObjectivesData(ArgsEntity args)
        {
            DbDataAdapter dapDERSummaryObj = null;
            DbDataReader drDERSummaryObj = null;

            try
            {
                //Instantiate report specific data set.
                DataSet dsDERSummaryObj = new DataSet();
                DataTable dtDERSummaryObj = new DataTable();
                dsDERSummaryObj.Tables.Add(dtDERSummaryObj);

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OutString", "", DbType.String, ParameterDirection.Output);
                paramCollection.Add("@ObjectiveList", args.Variants1, DbType.String);
                paramCollection.Add("@Firstdate", args.SStartDate, DbType.Date);
                paramCollection.Add("@Year", args.SYear, DbType.Int32);
                paramCollection.Add("@Month", args.SMonth, DbType.Int32);
                paramCollection.Add("@MarketId", args.MarketId, DbType.Int32);

                //Retreive the data adapter to fill the report data
                drDERSummaryObj = this.DataAcessService.ExecuteQuery(ReportSql["GetDERSummaryObjectiveList"], paramCollection);

                List<string> summaryList = new List<string>();
                if (drDERSummaryObj != null && drDERSummaryObj.HasRows)
                {
                    int dataOrdinal = drDERSummaryObj.GetOrdinal("data");

                    while (drDERSummaryObj.Read())
                    {
                        string data = "";
                        if (!drDERSummaryObj.IsDBNull(dataOrdinal)) data = drDERSummaryObj.GetString(dataOrdinal);
                        summaryList.Add(data);

                    }
                }

                return summaryList;

            }
            catch
            {
                throw;
            }
            finally
            {
                if (dapDERSummaryObj != null)
                    dapDERSummaryObj.Dispose();
            }
        }


        public List<string> GetDERSummaryGeneralData(ArgsEntity args)
        {
            DbDataAdapter dapDERSummaryGen = null;
            DbDataReader drDERSummaryGen = null;

            try
            {
                //Instantiate report specific data set.
                DataSet dsDERSummaryGen = new DataSet();
                DataTable dtDERSummaryGen = new DataTable();
                dsDERSummaryGen.Tables.Add(dtDERSummaryGen);

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@OutString", "", DbType.String, ParameterDirection.Output);
                //paramCollection.Add("@ObjectiveList", args.Variants1, DbType.String);
                paramCollection.Add("@Firstdate", args.SStartDate, DbType.Date);
                paramCollection.Add("@Year", args.SYear, DbType.Int32);
                paramCollection.Add("@Month", args.SMonth, DbType.Int32);
                paramCollection.Add("@MarketId", args.MarketId, DbType.Int32);

                //Retreive the data adapter to fill the report data
                drDERSummaryGen = this.DataAcessService.ExecuteQuery(ReportSql["GetDERSummaryGeneralList"], paramCollection);

                List<string> summaryList = new List<string>();
                if (drDERSummaryGen != null && drDERSummaryGen.HasRows)
                {
                    int dataOrdinal = drDERSummaryGen.GetOrdinal("data");

                    while (drDERSummaryGen.Read())
                    {
                        string data = "";
                        if (!drDERSummaryGen.IsDBNull(dataOrdinal)) data = drDERSummaryGen.GetString(dataOrdinal);
                        summaryList.Add(data);

                    }
                }

                return summaryList;

            }
            catch
            {
                throw;
            }
            finally
            {
                if (dapDERSummaryGen != null)
                    dapDERSummaryGen.Dispose();
            }
        }


        public DataSet GetDERSummaryHandlersData1(ArgsEntity args)
        {
            DbDataAdapter dapDERSummary = null;
            try
            {

                //Instantiate report specific data set.
                DataSet dsDERSummary = new DataSet();
                DataTable dtDERSummary = new DataTable();
                dsDERSummary.Tables.Add(dtDERSummary);

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Month", args.SMonth, DbType.Int32);
                paramCollection.Add("@Year", args.SYear, DbType.Int32);
                paramCollection.Add("@MarketId", args.MarketId, DbType.Int32);
                paramCollection.Add("@DanhilVariants", args.Variants1, DbType.String);
                paramCollection.Add("@AnyVariants", args.Variants2, DbType.String);


                //Retreive the data adapter to fill the report data
                dapDERSummary = this.DataAcessService.ExecuteReportQuery(ReportSql["GetDERSummaryHandlersNew"], paramCollection);

                //Fill the report specific data set
                dapDERSummary.Fill(dsDERSummary, dsDERSummary.Tables[0].TableName);

                return dsDERSummary;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dapDERSummary != null)
                    dapDERSummary.Dispose();
            }
        }


        public DataSet GetDERSummaryDataTablePart(string query)
        {
            DbDataAdapter dapDERSummary = null;
            try
            {

                //Instantiate report specific data set.
                DataSet dsDERSummary = new DataSet();
                DataTable dtDERSummary = new DataTable();
                dsDERSummary.Tables.Add(dtDERSummary);

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@ListString", query, DbType.String);

                //Retreive the data adapter to fill the report data
                dapDERSummary = this.DataAcessService.ExecuteReportQuery(ReportSql["GetDERSummaryDataList"], paramCollection);

                //Fill the report specific data set
                dapDERSummary.Fill(dsDERSummary, dsDERSummary.Tables[0].TableName);

                return dsDERSummary;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dapDERSummary != null)
                    dapDERSummary.Dispose();
            }
        }


        public DataSet GetDERSummaryAvailbleData(ArgsEntity args)
        {
            DbDataAdapter dapDERSummary = null;

            try
            {
                //Instantiate report specific data set.
                DataSet dsDERSummary = new DataSet();
                DataTable dtDERSummary = new DataTable();
                dsDERSummary.Tables.Add(dtDERSummary);

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Month", args.SMonth, DbType.Int32);
                paramCollection.Add("@Year", args.SYear, DbType.Int32);
                paramCollection.Add("@MarketId", args.MarketId, DbType.Int32);

                //Retreive the data adapter to fill the report data
                dapDERSummary = this.DataAcessService.ExecuteReportQuery(ReportSql["GetDERSummaryAvailability"], paramCollection);

                //Fill the report specific data set
                dapDERSummary.Fill(dsDERSummary, dsDERSummary.Tables[0].TableName);

                return dsDERSummary;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dapDERSummary != null)
                    dapDERSummary.Dispose();
            }
        }


        public DataSet GetDERSummaryData(ArgsEntity args)
        {
            DbDataAdapter dapDERSummary = null;

            try
            {
                ////Instantiate report specific data set.
                //DataSet dsDERSummary = new DataSet();
                //DataTable dtDERSummary = new DataTable();
                //dsDERSummary.Tables.Add(dtDERSummary);

                //DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                //paramCollection.Add("@OutString", "",  DbType.String,ParameterDirection.Output);
                //paramCollection.Add("@Month", args.SMonth, DbType.Int32 , ParameterDirection.Output);
                //paramCollection.Add("@Year", args.SYear, DbType.Int32);
                //paramCollection.Add("@MarketId", args.MarketId, DbType.Int32);
                //paramCollection.Add("@CategoryList", args.Variants1, DbType.String);
                //paramCollection.Add("@ProductList", args.Variants2, DbType.String);
                //paramCollection.Add("@Firstdate", args.FirstDateofTheMonth, DbType.Date);

                ////Retreive the data adapter to fill the report data
                //dapDERSummary = this.DataAcessService.ExecuteReportQuery(ReportSql["GetDERSummaryRecordList"], paramCollection);

                //List<String> countryList = new List<CountryEntity>();
                //if (drCountry != null && drCountry.HasRows)
                //{
                //    int countryIdOrdinal = drCountry.GetOrdinal("country_id");
                //    int countryOrdinal = drCountry.GetOrdinal("country");

                //    while (drCountry.Read())
                //    {
                //        CountryEntity country = CountryEntity.CreateObject();
                //        if (!drCountry.IsDBNull(countryIdOrdinal)) country.CountryId = drCountry.GetInt32(countryIdOrdinal);
                //        if (!drCountry.IsDBNull(countryOrdinal)) country.CountryName = drCountry.GetString(countryOrdinal).ToUpper();

                //        countryList.Add(country);

                //    }
                //}

                ////Fill the report specific data set
                //dapDERSummary.Fill(dsDERSummary, dsDERSummary.Tables[0].TableName);

                //DataTable dt = dsDERSummary.Tables[0];

                //return dsDERSummary;


                //Instantiate report specific data set.
                DataSet dsDERSummary = new DataSet();
                DataTable dtDERSummary = new DataTable();
                dsDERSummary.Tables.Add(dtDERSummary);

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@Month", args.SMonth, DbType.Int32);
                paramCollection.Add("@Year", args.SYear, DbType.Int32);
                paramCollection.Add("@MarketId", args.MarketId, DbType.Int32);
                paramCollection.Add("@DanhilVariants", args.Variants1, DbType.String);
                paramCollection.Add("@AnyVariants", args.Variants2, DbType.String);


                //Retreive the data adapter to fill the report data
                dapDERSummary = this.DataAcessService.ExecuteReportQuery(ReportSql["GetDERSummaryHandlersNew"], paramCollection);

                //Fill the report specific data set
                dapDERSummary.Fill(dsDERSummary, dsDERSummary.Tables[0].TableName);

                DataTable dt = dsDERSummary.Tables[0];

                return dsDERSummary;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dapDERSummary != null)
                    dapDERSummary.Dispose();
            }
        }


        public List<string> GetDERSummaryHeaderInfo(string originator, int marketId)
        {
            DbDataReader drDERSummaryHeaderInfo = null;
            List<string> result = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@originator", originator, DbType.String);
                paramCollection.Add("@marketId", marketId, DbType.Int32);
                drDERSummaryHeaderInfo = this.DataAcessService.ExecuteQuery(ReportSql["GetDERSummaryHeaderInfo"], paramCollection);

                if (drDERSummaryHeaderInfo != null && drDERSummaryHeaderInfo.HasRows)
                {
                    int tmeOrdinal = drDERSummaryHeaderInfo.GetOrdinal("ASE");
                    int marketOrdinal = drDERSummaryHeaderInfo.GetOrdinal("Market");

                    if (drDERSummaryHeaderInfo.Read())
                    {
                        result = new List<string>();

                        if (!drDERSummaryHeaderInfo.IsDBNull(tmeOrdinal)) result.Add(drDERSummaryHeaderInfo.GetString(tmeOrdinal));
                        if (!drDERSummaryHeaderInfo.IsDBNull(marketOrdinal)) result.Add(drDERSummaryHeaderInfo.GetString(marketOrdinal));

                    }
                }

                return result;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drDERSummaryHeaderInfo != null)
                    drDERSummaryHeaderInfo.Close();
            }
        }


        #endregion



    }
}
