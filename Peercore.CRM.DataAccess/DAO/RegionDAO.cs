﻿using Peercore.CRM.Entities;
using Peercore.CRM.Model;
using Peercore.CRM.Shared;
using Peercore.DataAccess.Common.Utilties;
using Peercore.Workflow.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.DataAccess.DAO
{
    public class RegionDAO : BaseDAO
    {
        public RegionDAO()
        {
            
        }

        #region "Region"

        public List<RegionModel> GetAllRegions(ArgsModel args)
        {
            RegionModel regionEntity = null;
            DataTable objDS = new DataTable();
            List<RegionModel> RegionsList = new List<RegionModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllRegions";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            regionEntity = new RegionModel();
                            if (item["region_id"] != DBNull.Value) regionEntity.RegionId = Convert.ToInt32(item["region_id"]);
                            if (item["comp_id"] != DBNull.Value) regionEntity.CompId = Convert.ToInt32(item["comp_id"]);
                            if (item["region_code"] != DBNull.Value) regionEntity.RegionCode = item["region_code"].ToString();
                            if (item["region_name"] != DBNull.Value) regionEntity.RegionName = item["region_name"].ToString();
                            if (item["total_count"] != DBNull.Value) regionEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                            RegionsList.Add(regionEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return RegionsList;
        }

        public List<RegionModel> GetAllRegionsByRegionCode(ArgsModel args, string RegionCode)
        {
            RegionModel regionEntity = null;
            DataTable objDS = new DataTable();
            List<RegionModel> RegionsList = new List<RegionModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllRegionsByRegionCode";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@RegionCode", RegionCode);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            regionEntity = new RegionModel();
                            if (item["region_id"] != DBNull.Value) regionEntity.RegionId = Convert.ToInt32(item["region_id"]);
                            if (item["comp_id"] != DBNull.Value) regionEntity.CompId = Convert.ToInt32(item["comp_id"]);
                            if (item["region_code"] != DBNull.Value) regionEntity.RegionCode = item["region_code"].ToString();
                            if (item["region_name"] != DBNull.Value) regionEntity.RegionName = item["region_name"].ToString();
                            //if (item["total_count"] != DBNull.Value) regionEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                            RegionsList.Add(regionEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return RegionsList;
        }

        public List<RegionModel> GetAllRegionsByRegionName(ArgsModel args, string RegionName)
        {
            RegionModel regionEntity = null;
            DataTable objDS = new DataTable();
            List<RegionModel> RegionsList = new List<RegionModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllRegionsByRegionName";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@RegionName", RegionName);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            regionEntity = new RegionModel();
                            if (item["region_id"] != DBNull.Value) regionEntity.RegionId = Convert.ToInt32(item["region_id"]);
                            if (item["comp_id"] != DBNull.Value) regionEntity.CompId = Convert.ToInt32(item["comp_id"]);
                            if (item["region_code"] != DBNull.Value) regionEntity.RegionCode = item["region_code"].ToString();
                            if (item["region_name"] != DBNull.Value) regionEntity.RegionName = item["region_name"].ToString();
                            //if (item["total_count"] != DBNull.Value) regionEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                            RegionsList.Add(regionEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return RegionsList;
        }

        public RegionModel GetAllRegionsByRegionId(string RegionId)
        {
            RegionModel regionEntity = null;
            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllRegionsByRegionId";

                    //objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    //objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    //objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    //objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    //objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@RegionId", RegionId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    DataRow item = objDS.Rows[0];
                    regionEntity = new RegionModel();
                    if (item["region_id"] != DBNull.Value) regionEntity.RegionId = Convert.ToInt32(item["region_id"]);
                    if (item["comp_id"] != DBNull.Value) regionEntity.CompId = Convert.ToInt32(item["comp_id"]);
                    if (item["region_code"] != DBNull.Value) regionEntity.RegionCode = item["region_code"].ToString();
                    if (item["region_name"] != DBNull.Value) regionEntity.RegionName = item["region_name"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return regionEntity;
        }

        public bool DeleteRegion(string region_id)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteRegionByRegionId";

                    objCommand.Parameters.AddWithValue("@RegionId", region_id);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }
        
        public bool SaveRegion(SqlTransaction sqlTransaction, string comp_id, string region_id, string region_code, string region_name)
        {
            bool retStatus = false;

            try
            {
                SqlCommand objCommand = new SqlCommand();
                objCommand.Transaction = sqlTransaction;
                objCommand.Connection = sqlTransaction.Connection;
                objCommand.CommandType = CommandType.StoredProcedure;
                objCommand.CommandText = "InsertUpdateRegion";

                objCommand.Parameters.AddWithValue("@CompId", comp_id);
                objCommand.Parameters.AddWithValue("@RegionId", region_id);
                objCommand.Parameters.AddWithValue("@RegionCode", region_code);
                objCommand.Parameters.AddWithValue("@RegionName", region_name);

                if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                if (objCommand.ExecuteNonQuery() > 0)
                {
                    retStatus = true;
                }
                else
                {
                    retStatus = false;
                }

            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        #endregion

        #region "RSM"
        public List<RsmModel> GetAllRsm(ArgsModel args)
        {
            RsmModel rsmEntity = null;
            DataTable objDS = new DataTable();
            List<RsmModel> RsmList = new List<RsmModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllRsm";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            rsmEntity = new RsmModel();
                            if (item["rsm_id"] != DBNull.Value) rsmEntity.RsmId = Convert.ToInt32(item["rsm_id"]);
                            if (item["region_id"] != DBNull.Value) rsmEntity.RegionId = Convert.ToInt32(item["region_id"]);
                            if (item["region_status"] != DBNull.Value) rsmEntity.RegionStatus = item["region_status"].ToString();
                            if (item["region_code"] != DBNull.Value) rsmEntity.RegionCode = item["region_code"].ToString();
                            if (item["region_name"] != DBNull.Value) rsmEntity.RegionName = item["region_name"].ToString();
                            if (item["rsm_code"] != DBNull.Value) rsmEntity.RsmCode = item["rsm_code"].ToString();
                            if (item["rsm_name"] != DBNull.Value) rsmEntity.RsmName = item["rsm_name"].ToString();
                            if (item["rsm_user_name"] != DBNull.Value) rsmEntity.RsmUserName = item["rsm_user_name"].ToString();
                            //if (item["rsm_password"] != DBNull.Value) rsmEntity.RsmPassword = item["rsm_password"].ToString();
                            if (item["rsm_tel1"] != DBNull.Value) rsmEntity.RsmTel1 = item["rsm_tel1"].ToString();
                            if (item["rsm_tel2"] != DBNull.Value) rsmEntity.RsmTel2 = item["rsm_tel2"].ToString();
                            if (item["rsm_email"] != DBNull.Value) rsmEntity.RsmEmail = item["rsm_email"].ToString();
                            if (item["total_count"] != DBNull.Value) rsmEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                            RsmList.Add(rsmEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return RsmList;
        }

        public List<RsmModel> GetAllRsmByRsmCode(ArgsModel args, string RsmCode)
        {
            RsmModel rsmEntity = null;
            DataTable objDS = new DataTable();
            List<RsmModel> RsmList = new List<RsmModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllRsmByRsmCode";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@RsmCode", RsmCode);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            rsmEntity = new RsmModel();
                            if (item["rsm_id"] != DBNull.Value) rsmEntity.RsmId = Convert.ToInt32(item["rsm_id"]);
                            if (item["region_id"] != DBNull.Value) rsmEntity.RegionId = Convert.ToInt32(item["region_id"]);
                            if (item["region_code"] != DBNull.Value) rsmEntity.RegionCode = item["region_code"].ToString();
                            if (item["region_name"] != DBNull.Value) rsmEntity.RegionName = item["region_name"].ToString();
                            if (item["rsm_code"] != DBNull.Value) rsmEntity.RsmCode = item["rsm_code"].ToString();
                            if (item["rsm_name"] != DBNull.Value) rsmEntity.RsmName = item["rsm_name"].ToString();
                            if (item["rsm_user_name"] != DBNull.Value) rsmEntity.RsmUserName = item["rsm_user_name"].ToString();
                            if (item["rsm_password"] != DBNull.Value) rsmEntity.RsmPassword = item["rsm_password"].ToString();
                            if (item["rsm_tel1"] != DBNull.Value) rsmEntity.RsmTel1 = item["rsm_tel1"].ToString();
                            if (item["rsm_tel2"] != DBNull.Value) rsmEntity.RsmTel2 = item["rsm_tel2"].ToString();
                            if (item["rsm_email"] != DBNull.Value) rsmEntity.RsmEmail = item["rsm_email"].ToString();
                            if (item["total_count"] != DBNull.Value) rsmEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                            RsmList.Add(rsmEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return RsmList;
        }

        public RsmModel GetRsmDetailsByRsmId(string RsmId)
        {
            RsmModel rsmEntity = null;
            DataTable objDS = new DataTable();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetRsmDetailsByRsmId";

                    objCommand.Parameters.AddWithValue("@RsmId", RsmId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            rsmEntity = new RsmModel();
                            if (item["rsm_id"] != DBNull.Value) rsmEntity.RsmId = Convert.ToInt32(item["rsm_id"]);
                            if (item["rsm_code"] != DBNull.Value) rsmEntity.RsmCode = item["rsm_code"].ToString();
                            if (item["rsm_name"] != DBNull.Value) rsmEntity.RsmName = item["rsm_name"].ToString();
                            if (item["rsm_user_name"] != DBNull.Value) rsmEntity.RsmUserName = item["rsm_user_name"].ToString();
                            if (item["rsm_tel1"] != DBNull.Value) rsmEntity.RsmTel1 = item["rsm_tel1"].ToString();
                            if (item["rsm_tel2"] != DBNull.Value) rsmEntity.RsmTel2 = item["rsm_tel2"].ToString();
                            if (item["rsm_email"] != DBNull.Value) rsmEntity.RsmEmail = item["rsm_email"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return rsmEntity;
        }

        public List<RsmModel> GetRsmDetailsByRegionId(ArgsModel args, string RegionId)
        {
            RsmModel rsmEntity = null;
            DataTable objDS = new DataTable();
            List<RsmModel> RsmList = new List<RsmModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetRsmDetailsByRegionId";

                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@RegionId", RegionId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            rsmEntity = new RsmModel();
                            if (item["rsm_id"] != DBNull.Value) rsmEntity.RsmId = Convert.ToInt32(item["rsm_id"]);
                            if (item["region_id"] != DBNull.Value) rsmEntity.RegionId = Convert.ToInt32(item["region_id"]);
                            if (item["region_code"] != DBNull.Value) rsmEntity.RegionCode = item["region_code"].ToString();
                            if (item["region_name"] != DBNull.Value) rsmEntity.RegionName = item["region_name"].ToString();
                            if (item["rsm_code"] != DBNull.Value) rsmEntity.RsmCode = item["rsm_code"].ToString();
                            if (item["rsm_name"] != DBNull.Value) rsmEntity.RsmName = item["rsm_name"].ToString();
                            if (item["rsm_user_name"] != DBNull.Value) rsmEntity.RsmUserName = item["rsm_user_name"].ToString();
                            //if (item["rsm_password"] != DBNull.Value) rsmEntity.RsmPassword = item["rsm_password"].ToString();
                            if (item["rsm_tel1"] != DBNull.Value) rsmEntity.RsmTel1 = item["rsm_tel1"].ToString();
                            if (item["rsm_tel2"] != DBNull.Value) rsmEntity.RsmTel2 = item["rsm_tel2"].ToString();
                            if (item["rsm_email"] != DBNull.Value) rsmEntity.RsmEmail = item["rsm_email"].ToString();
                            if (item["total_count"] != DBNull.Value) rsmEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                            RsmList.Add(rsmEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return RsmList;
        }

        public bool DeleteRsmByRsmId(string rsm_id)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "DeleteRsmByRsmId";

                    objCommand.Parameters.AddWithValue("@RsmId", rsm_id);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool SaveRsm(string region_id,
                            string rsm_id,
                            string rsm_code,
                            string rsm_name,
                            string rsm_username,
                            string rsm_tel1,
                            string rsm_email,
                            string created_by)
        {
            bool retStatus = false;

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "InsertUpdateRSM";

                    objCommand.Parameters.AddWithValue("@RegionId", region_id);
                    objCommand.Parameters.AddWithValue("@RsmId", rsm_id);
                    objCommand.Parameters.AddWithValue("@RsmCode", rsm_code);
                    objCommand.Parameters.AddWithValue("@RsmName", rsm_name);
                    objCommand.Parameters.AddWithValue("@RsnUserName", rsm_username);
                    objCommand.Parameters.AddWithValue("@RsmTel1", rsm_tel1);
                    objCommand.Parameters.AddWithValue("@RsmEmail", rsm_email);
                    objCommand.Parameters.AddWithValue("@CreatedBy", created_by);

                    if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                    if (objCommand.ExecuteNonQuery() > 0)
                    {
                        retStatus = true;
                    }
                    else
                    {
                        retStatus = false;
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        #endregion
    }
}
