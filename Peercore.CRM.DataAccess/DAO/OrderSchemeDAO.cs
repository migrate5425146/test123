﻿using Peercore.CRM.Model;
using Peercore.CRM.Shared;
using Peercore.DataAccess.Common.Parameters;
using Peercore.DataAccess.Common.Utilties;
using Peercore.Workflow.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Peercore.CRM.DataAccess.DAO
{
    public class OrderSchemeDAO : BaseDAO
    {
        private DbSqlAdapter OrderSchemeSql { get; set; }

        private void RegisterSql()
        {
            this.OrderSchemeSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.OrderHeaderSql.xml", ApplicationService.Instance.DbProvider);
        }

        public OrderSchemeDAO()
        {
            RegisterSql();
        }

        public OrderSchemeDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }
        
        public bool DeleteOrderDiscountSchemeByOrderId(int orderId)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@OrderId", orderId, DbType.Int32);

                iNoRec = this.DataAcessService.ExecuteNonQuery(OrderSchemeSql["DeleteOrderDiscountSchemeByOrderId"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
            }
        }

        public bool InsertOrderDiscountSchemeGroup(out int schemeGroupId, OrderDiscountSchemeGroup orderSchemeGroupEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drBrand = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@OrderId", orderSchemeGroupEntity.OrderId, DbType.Int32);
                paramCollection.Add("@SchemeDetailsId", orderSchemeGroupEntity.DiscountSchemeDetailID, DbType.Int32);
                paramCollection.Add("@DiscountValue", orderSchemeGroupEntity.DiscountValue, DbType.Double);
                paramCollection.Add("@SchemeGroupId", null, DbType.Int32, ParameterDirection.Output);

                schemeGroupId = this.DataAcessService.ExecuteNonQuery(OrderSchemeSql["InsertOrderDiscountSchemeGroup"], paramCollection, true, "@SchemeGroupId");

                if (schemeGroupId > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }

        public bool InsertOrderDiscountScheme(OrderDiscountScheme invoiceSchemeEntity)
        {
            bool successful = false;
            int iNoRec = 0;
            DbDataReader drBrand = null;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@OrderId", invoiceSchemeEntity.OrderId, DbType.Int32);
                paramCollection.Add("@Productid", invoiceSchemeEntity.ProductID, DbType.Int32);
                paramCollection.Add("@SchemeGroupId", invoiceSchemeEntity.SchemeGroupId, DbType.Int32);
                paramCollection.Add("@DiscountVal", invoiceSchemeEntity.DiscountVal, DbType.Double);
                paramCollection.Add("@UsedQty", invoiceSchemeEntity.UsedQty, DbType.Double);

                iNoRec = this.DataAcessService.ExecuteNonQuery(OrderSchemeSql["InsertOrderDiscountScheme"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {
                if (drBrand != null)
                    drBrand.Close();
            }
        }

        public List<OrderDiscountSchemeGroup> GetOrderSchemeGroupByOrderId(int orderId)
        {
            OrderDiscountSchemeGroup orderSchemeGroupEntity = null;
            DataTable objDS = new DataTable();
            List<OrderDiscountSchemeGroup> OrderSchemeGroupList = new List<OrderDiscountSchemeGroup>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetOrderSchemeGroupByOrderId";

                    objCommand.Parameters.AddWithValue("@OrderId", orderId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            orderSchemeGroupEntity = new OrderDiscountSchemeGroup();
                            if (item["scheme_group_id"] != DBNull.Value) orderSchemeGroupEntity.DiscountSchemeGroupID = Convert.ToInt32(item["scheme_group_id"]);
                            if (item["order_id"] != DBNull.Value) orderSchemeGroupEntity.OrderId = Convert.ToInt32(item["order_id"]);
                            if (item["scheme_details_id"] != DBNull.Value) orderSchemeGroupEntity.DiscountSchemeDetailID = Convert.ToInt32(item["scheme_details_id"]);
                            if (item["discount_value"] != DBNull.Value) orderSchemeGroupEntity.DiscountValue = Convert.ToDouble(item["discount_value"]);
                            if (item["scheme_details"] != DBNull.Value) orderSchemeGroupEntity.SchemeDetailName = item["scheme_details"].ToString();

                            orderSchemeGroupEntity.DiscountSchemeCollection = GetOrderDiscountSchemeByGroupId(orderSchemeGroupEntity.DiscountSchemeGroupID);

                            OrderSchemeGroupList.Add(orderSchemeGroupEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                OrderSchemeGroupList = new List<OrderDiscountSchemeGroup>();
            }

            return OrderSchemeGroupList;
        }

        public List<OrderDiscountScheme> GetOrderDiscountSchemeByGroupId(int groupId)
        {
            OrderDiscountScheme orderSchemeEntity = null;
            DataTable objDS = new DataTable();
            List<OrderDiscountScheme> OrderSchemeList = new List<OrderDiscountScheme>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetDiscountSchemeBySchemeGroupId";

                    objCommand.Parameters.AddWithValue("@SchemeGroupId", groupId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        foreach (DataRow item in objDS.Rows)
                        {
                            orderSchemeEntity = new OrderDiscountScheme();
                            if (item["order_scheme_id"] != DBNull.Value) orderSchemeEntity.OrderSchemeId = Convert.ToInt32(item["order_scheme_id"]);
                            if (item["scheme_group_id"] != DBNull.Value) orderSchemeEntity.SchemeGroupId = Convert.ToInt32(item["scheme_group_id"]);
                            if (item["order_id"] != DBNull.Value) orderSchemeEntity.OrderId = Convert.ToInt32(item["order_id"]);
                            if (item["product_id"] != DBNull.Value) orderSchemeEntity.ProductID = Convert.ToInt32(item["product_id"]);
                            if (item["used_qty"] != DBNull.Value) orderSchemeEntity.UsedQty = Convert.ToDouble(item["used_qty"]);
                            if (item["name"] != DBNull.Value) orderSchemeEntity.ProductName = item["name"].ToString();
                            OrderSchemeList.Add(orderSchemeEntity);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                OrderSchemeList = new List<OrderDiscountScheme>();
            }

            return OrderSchemeList;
        }
    }
}
