﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Peercore.Workflow.Common;
using Peercore.DataAccess.Common.Utilties;
using Peercore.CRM.Shared;
using Peercore.CRM.Entities;
using System.Data.Common;
using Peercore.DataAccess.Common.Parameters;
using System.Data;
using System.Data.SqlClient;
using Peercore.CRM.Model;

namespace Peercore.CRM.DataAccess.DAO
{
    public class DistributorDAO:BaseDAO
    {
        private DbSqlAdapter DistributorSql { get; set; }

        private void RegisterSql()
        {
            this.DistributorSql = new DbSqlAdapter("Peercore.CRM.DataAccess.SQL.DistributorSql.xml", ApplicationService.Instance.DbProvider);
        }

        public DistributorDAO()
        {
            RegisterSql();
        }

        public DistributorDAO(DbWorkflowScope scope)
            : base(scope)
        {
            RegisterSql();
        }

        public DistributorEntity CreateObject()
        {
            return DistributorEntity.CreateObject();
        }

        public DistributorEntity CreateObject(int entityId)
        {
            return DistributorEntity.CreateObject(entityId);
        }

        public DistributorEntity GetDistributorTargetById(ArgsEntity args)
        {
            DistributorEntity distributorEntity = CreateObject();
            DbDataReader dataReader = null;

            DbInputParameterCollection paramCollection = new DbInputParameterCollection();

            paramCollection.Add("@EffectiveStartDate", args.DtStartDate.ToString("yyyy-MM-dd"), DbType.String);
            //paramCollection.Add("@EffectiveEndDate", args.DtEndDate.ToString("yyyy-MM-dd"), DbType.String);
            paramCollection.Add("@DistributorId", args.OriginatorId, DbType.Int32);

            dataReader = this.DataAcessService.ExecuteQuery(DistributorSql["GetDistributorTargetById"], paramCollection);

            try
            {
                if (dataReader != null && dataReader.HasRows)
                {
                    int distributorIdOrdinal = dataReader.GetOrdinal("distributor_id");
                    int targetIdOrdinal = dataReader.GetOrdinal("target_id");
                    int targetOrdinal = dataReader.GetOrdinal("stick_target");
                    int nameOrdinal = dataReader.GetOrdinal("name");

                    while (dataReader.Read())
                    {
                        if (!dataReader.IsDBNull(distributorIdOrdinal)) distributorEntity.DistributorId = dataReader.GetInt32(distributorIdOrdinal);
                        if (!dataReader.IsDBNull(targetIdOrdinal)) distributorEntity.TargetId = dataReader.GetInt32(targetIdOrdinal);
                        if (!dataReader.IsDBNull(targetOrdinal)) distributorEntity.Target = dataReader.GetInt32(targetOrdinal);
                        if (!dataReader.IsDBNull(nameOrdinal)) distributorEntity.Name = dataReader.GetString(nameOrdinal);
                    }
                }
                return distributorEntity;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (dataReader != null)
                    dataReader.Close();
            }
        }

        public bool InsertDistributorTarget(DistributorEntity distributorEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@DistributorID", distributorEntity.DistributorId, DbType.Int32);
                paramCollection.Add("@Target", distributorEntity.Target, DbType.Decimal);
                paramCollection.Add("@StickTarget", distributorEntity.Target, DbType.Int32);
                paramCollection.Add("@EffStartDate", distributorEntity.EffStartDate, DbType.DateTime);
                paramCollection.Add("@EffEndDate", distributorEntity.EffEndDate, DbType.DateTime);
                paramCollection.Add("@CreatedBy", distributorEntity.CreatedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(DistributorSql["InsertDistributorTarget"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public bool UpdateDistributorTarget(DistributorEntity distributorEntity)
        {
            bool successful = false;
            int iNoRec = 0;

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();

                paramCollection.Add("@TargetID", distributorEntity.TargetId, DbType.Int32);
                paramCollection.Add("@StickTarget", distributorEntity.Target, DbType.Int32);
                paramCollection.Add("@LastModifiedBy", distributorEntity.LastModifiedBy, DbType.String);

                iNoRec = this.DataAcessService.ExecuteNonQuery(DistributorSql["UpdateDistributorTarget"], paramCollection);

                if (iNoRec > 0)
                    successful = true;

                return successful;
            }
            catch (Exception)
            {
                throw;
            }
            finally
            {

            }
        }

        public string GetDistributorHoliday(int originatorId, string originator, string repCode)
        {
            //DbDataReader drDistributor = null;

            //try
            //{
            //    //string holiday = "Sunday";
            //    string holiday = "";

            //    DbInputParameterCollection paramCollection = new DbInputParameterCollection();
            //    paramCollection.Add("@originatorId", originatorId, DbType.Int32);
            //    paramCollection.Add("@originator", originator, DbType.String);
            //    paramCollection.Add("@repCode", repCode, DbType.String);

            //    drDistributor = this.DataAcessService.ExecuteQuery(DistributorSql["GetDistributorHoliday"], paramCollection);

            //    if (drDistributor != null && drDistributor.HasRows)
            //    {
            //        int holidayOrdinal = drDistributor.GetOrdinal("holiday");

            //        while (drDistributor.Read())
            //        {
            //            if (!drDistributor.IsDBNull(holidayOrdinal)) holiday = drDistributor.GetString(holidayOrdinal);
            //        }
            //    }

            //    return holiday;
            //}
            //catch
            //{
            //    throw;
            //}
            //finally
            //{
            //    if (drDistributor != null)
            //        drDistributor.Close();
            //}

            DataTable objDS = new DataTable();
            string holiday = "";

            try
            {
                using (SqlConnection conn = new SqlConnection(WorkflowScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetChildOriginators";

                    objCommand.Parameters.AddWithValue("@Originator", originatorId);
                    objCommand.Parameters.AddWithValue("@originator", originator);
                    objCommand.Parameters.AddWithValue("@repCode", repCode);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);

                    if (objDS.Rows.Count > 0)
                    {
                        holiday = objDS.Rows[0]["holiday"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
            }

            return holiday;
        }

        public DistributorEntity GetDistributorDetails(string originator)
        {
            DbDataReader drDistributor = null;
            DistributorEntity dist = CreateObject();

            try
            {
                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@originator", originator, DbType.String);

                drDistributor = this.DataAcessService.ExecuteQuery(DistributorSql["GetDistributorDetails"], paramCollection);

                if (drDistributor != null && drDistributor.HasRows)
                {
                    int originatorOrdinal = drDistributor.GetOrdinal("originator");
                    int nameOrdinal = drDistributor.GetOrdinal("name");
                    int passwordOrdinal = drDistributor.GetOrdinal("password");
                    int emailOrdinal = drDistributor.GetOrdinal("internet_add");
                    int deptStringOrdinal = drDistributor.GetOrdinal("dept_string");
                    int distIdOrdinal = drDistributor.GetOrdinal("dist_id");
                    int addressOrdinal = drDistributor.GetOrdinal("address_1");
                    int telOrdinal = drDistributor.GetOrdinal("telephone");
                    int mobOrdinal = drDistributor.GetOrdinal("mobile");
                    int parentOriOrdinal = drDistributor.GetOrdinal("parent_originator");
                    int parentOriNameOrdinal = drDistributor.GetOrdinal("parent_originator_name");
                    int groupCodeOrdinal = drDistributor.GetOrdinal("group_code");
                    int divisionIdOrdinal = drDistributor.GetOrdinal("division_id");
                    int joinDateOrdinal = drDistributor.GetOrdinal("join_date");
                    int repCodeOrdinal = drDistributor.GetOrdinal("rep_code");
                    int area_idOrdinal = drDistributor.GetOrdinal("area_id");
                    int statusOrdinal = drDistributor.GetOrdinal("status");
                    int IsNotEMEIUserOrdinal = drDistributor.GetOrdinal("IsNotEMEIUser");
                    int display_nameOrdinal = drDistributor.GetOrdinal("display_name");

                    while (drDistributor.Read())
                    {
                        if (!drDistributor.IsDBNull(originatorOrdinal)) dist.Originator = drDistributor.GetString(originatorOrdinal);
                        if (!drDistributor.IsDBNull(nameOrdinal)) dist.Name = drDistributor.GetString(nameOrdinal);
                        if (!drDistributor.IsDBNull(passwordOrdinal)) dist.Password = drDistributor.GetString(passwordOrdinal);
                        if (!drDistributor.IsDBNull(emailOrdinal)) dist.Email = drDistributor.GetString(emailOrdinal);
                        if (!drDistributor.IsDBNull(deptStringOrdinal)) dist.DeptString = drDistributor.GetString(deptStringOrdinal);
                        if (dist.DeptString == "ASE")
                            dist.DistributorId = 1;
                        else
                            if (!drDistributor.IsDBNull(distIdOrdinal)) dist.DistributorId = drDistributor.GetInt32(distIdOrdinal);
                        if (!drDistributor.IsDBNull(addressOrdinal)) dist.Adderess = drDistributor.GetString(addressOrdinal);
                        if (!drDistributor.IsDBNull(telOrdinal)) dist.Telephone = drDistributor.GetString(telOrdinal);
                        if (!drDistributor.IsDBNull(mobOrdinal)) dist.Mobile = drDistributor.GetString(mobOrdinal);
                        if (!drDistributor.IsDBNull(parentOriOrdinal)) dist.ParentOriginator = drDistributor.GetString(parentOriOrdinal);
                        if (!drDistributor.IsDBNull(parentOriNameOrdinal)) dist.PrimaryDist = drDistributor.GetString(parentOriNameOrdinal);
                        if (!drDistributor.IsDBNull(groupCodeOrdinal)) dist.GroupCode = drDistributor.GetString(groupCodeOrdinal);
                        if (!drDistributor.IsDBNull(divisionIdOrdinal)) dist.DivisionId = drDistributor.GetInt32(divisionIdOrdinal);
                        if (!drDistributor.IsDBNull(joinDateOrdinal)) dist.JoinedDate = drDistributor.GetDateTime(joinDateOrdinal);
                        if (!drDistributor.IsDBNull(repCodeOrdinal)) dist.Code = drDistributor.GetString(repCodeOrdinal);
                        if (!drDistributor.IsDBNull(area_idOrdinal)) dist.AreaId = drDistributor.GetInt32(area_idOrdinal);
                        if (!drDistributor.IsDBNull(statusOrdinal)) dist.Status = drDistributor.GetString(statusOrdinal);
                        if (!drDistributor.IsDBNull(IsNotEMEIUserOrdinal)) dist.IsNotEMEIUser = drDistributor.GetBoolean(IsNotEMEIUserOrdinal);
                        if (!drDistributor.IsDBNull(display_nameOrdinal)) dist.DisplayName = drDistributor.GetString(display_nameOrdinal);
                    }
                }

                return dist;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drDistributor != null)
                    drDistributor.Close();
            }
        }

        public List<string> GetDistributorHolidaysList(string originator)
        {
            DbDataReader drDistributor = null;
            List<string> holidayList = new List<string>();

            try
            {

                DbInputParameterCollection paramCollection = new DbInputParameterCollection();
                paramCollection.Add("@originator", originator, DbType.String);

                drDistributor = this.DataAcessService.ExecuteQuery(DistributorSql["GetDistributorHoliday"], paramCollection);

                if (drDistributor != null && drDistributor.HasRows)
                {
                    int holidayOrdinal = drDistributor.GetOrdinal("holiday");

                    while (drDistributor.Read())
                    {
                        if (!drDistributor.IsDBNull(holidayOrdinal))
                        {
                            string holiday = "";

                            if (String.IsNullOrEmpty(holiday)) holiday = drDistributor.GetString(holidayOrdinal);

                            holidayList.Add(holiday);
                        }
                    }
                }

                return holidayList;
            }
            catch
            {
                throw;
            }
            finally
            {
                if (drDistributor != null)
                    drDistributor.Close();
            }
        }

        #region "Perfetti 2nd Phase"
        
        public List<DistributerModel> GetAllDistributerByOriginatorAndOriginatorType(string OriginatorType, string Originator)
        {
            DataTable objDS = new DataTable();
            DistributerModel objEntity = new DistributerModel();
            List<DistributerModel> objList = new List<DistributerModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllDistributerByOriginatorAndOriginatorType";

                    objCommand.Parameters.AddWithValue("@OriginatorType", OriginatorType);
                    objCommand.Parameters.AddWithValue("@Originator", Originator);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        objEntity = new DistributerModel();
                        if (item["originator"] != DBNull.Value) objEntity.Originator = item["originator"].ToString();
                        if (item["code"] != DBNull.Value) objEntity.Code = item["code"].ToString();
                        if (item["name"] != DBNull.Value) objEntity.Name = item["name"].ToString();
                        if (item["password"] != DBNull.Value) objEntity.Password = item["password"].ToString();
                        if (item["internet_add"] != DBNull.Value) objEntity.Email = item["internet_add"].ToString();
                        if (item["dept_string"] != DBNull.Value) objEntity.DeptString = item["dept_string"].ToString();
                        if (item["dist_id"] != DBNull.Value) objEntity.DistributorId = Convert.ToInt32(item["dist_id"]);
                        if (item["address_1"] != DBNull.Value) objEntity.Adderess = item["address_1"].ToString();
                        if (item["telephone"] != DBNull.Value) objEntity.Telephone = item["telephone"].ToString();
                        if (item["mobile"] != DBNull.Value) objEntity.Mobile = item["mobile"].ToString();
                        if (item["status"] != DBNull.Value) objEntity.Status = item["status"].ToString();
                        if (item["IsNotEMEIUser"] != DBNull.Value) objEntity.IsNotEMEIUser = Convert.ToBoolean(item["IsNotEMEIUser"]);
                        if (item["display_name"] != DBNull.Value) objEntity.DisplayName = item["display_name"].ToString();

                        objList.Add(objEntity);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }
        
        public List<TradeDiscountUploadModel> GetAllTradeDiscounts(ArgsModel args, string dbCode)
        {
            DataTable objDS = new DataTable();
            TradeDiscountUploadModel objEntity = new TradeDiscountUploadModel();
            List<TradeDiscountUploadModel> objList = new List<TradeDiscountUploadModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllTradeDiscounts";

                    objCommand.Parameters.AddWithValue("@OrderBy", args.OrderBy);
                    objCommand.Parameters.AddWithValue("@AddParams", args.AdditionalParams);
                    objCommand.Parameters.AddWithValue("@StartIndex", args.StartIndex);
                    objCommand.Parameters.AddWithValue("@RowCount", args.RowCount);
                    objCommand.Parameters.AddWithValue("@ShowSQL", 0);
                    objCommand.Parameters.AddWithValue("@DbCode", dbCode);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        objEntity = new TradeDiscountUploadModel();
                        if (item["id"] != DBNull.Value) objEntity.id = decimal.Parse(item["id"].ToString());
                        if (item["date"] != DBNull.Value) objEntity.date = item["date"].ToString();
                        if (item["dbCode"] != DBNull.Value) objEntity.dbCode = item["dbCode"].ToString();
                        if (item["vouType"] != DBNull.Value) objEntity.vouType = item["vouType"].ToString();
                        if (item["vouNo"] != DBNull.Value) objEntity.vouNo = item["vouNo"].ToString();
                        if (item["saleVal"] != DBNull.Value) objEntity.saleVal = double.Parse(item["saleVal"].ToString());
                        if (item["discountVal"] != DBNull.Value) objEntity.discountVal = double.Parse(item["discountVal"].ToString());
                        if (item["tradeDiscountVal"] != DBNull.Value) objEntity.tradeDiscountVal = double.Parse(item["tradeDiscountVal"].ToString());
                        if (item["total_count"] != DBNull.Value) objEntity.TotalCount = Convert.ToInt32(item["total_count"]);

                        objList.Add(objEntity);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }
        
        public List<DistributerModel> GetAllDistributerByASM(string Asm)
        {
            DataTable objDS = new DataTable();
            DistributerModel objEntity = new DistributerModel();
            List<DistributerModel> objList = new List<DistributerModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(ApplicationService.Instance.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllDistributerByASM";

                    //objCommand.Parameters.AddWithValue("@OriginatorType", OriginatorType);
                    objCommand.Parameters.AddWithValue("@Originator", Asm);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        objEntity = new DistributerModel();
                        if (item["originator"] != DBNull.Value) objEntity.Originator = item["originator"].ToString();
                        if (item["code"] != DBNull.Value) objEntity.Code = item["code"].ToString();
                        if (item["name"] != DBNull.Value) objEntity.Name = item["name"].ToString();
                        if (item["password"] != DBNull.Value) objEntity.Password = item["password"].ToString();
                        if (item["internet_add"] != DBNull.Value) objEntity.Email = item["internet_add"].ToString();
                        if (item["dept_string"] != DBNull.Value) objEntity.DeptString = item["dept_string"].ToString();
                        if (item["dist_id"] != DBNull.Value) objEntity.DistributorId = Convert.ToInt32(item["dist_id"]);
                        if (item["address_1"] != DBNull.Value) objEntity.Adderess = item["address_1"].ToString();
                        if (item["telephone"] != DBNull.Value) objEntity.Telephone = item["telephone"].ToString();
                        if (item["mobile"] != DBNull.Value) objEntity.Mobile = item["mobile"].ToString();
                        if (item["status"] != DBNull.Value) objEntity.Status = item["status"].ToString();
                        if (item["IsNotEMEIUser"] != DBNull.Value) objEntity.IsNotEMEIUser = Convert.ToBoolean(item["IsNotEMEIUser"]);
                        if (item["display_name"] != DBNull.Value) objEntity.DisplayName = item["display_name"].ToString();

                        objList.Add(objEntity);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public DistributerModel GetDistributorDetailsByOriginator(DbWorkflowScope transactionScope, string originator)
        {
            DataTable objDS = new DataTable();
            DistributerModel objEntity = new DistributerModel();

            try
            {
                using (SqlConnection conn = new SqlConnection(transactionScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetDistributorDetailsByOriginator";

                    objCommand.Parameters.AddWithValue("@Originator", originator);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    DataRow item = objDS.Rows[0];

                    if (item["originator"] != DBNull.Value) objEntity.Originator = item["originator"].ToString();
                    if (item["code"] != DBNull.Value) objEntity.Code = item["code"].ToString();
                    if (item["name"] != DBNull.Value) objEntity.Name = item["name"].ToString();
                    if (item["password"] != DBNull.Value) objEntity.Password = item["password"].ToString();
                    if (item["internet_add"] != DBNull.Value) objEntity.Email = item["internet_add"].ToString();
                    if (item["dept_string"] != DBNull.Value) objEntity.DeptString = item["dept_string"].ToString();
                    if (item["dist_id"] != DBNull.Value) objEntity.DistributorId = Convert.ToInt32(item["dist_id"]);
                    if (item["address_1"] != DBNull.Value) objEntity.Adderess = item["address_1"].ToString();
                    if (item["telephone"] != DBNull.Value) objEntity.Telephone = item["telephone"].ToString();
                    if (item["mobile"] != DBNull.Value) objEntity.Mobile = item["mobile"].ToString();
                    if (item["status"] != DBNull.Value) objEntity.Status = item["status"].ToString();
                    if (item["IsNotEMEIUser"] != DBNull.Value) objEntity.IsNotEMEIUser = Convert.ToBoolean(item["IsNotEMEIUser"]);
                    if (item["display_name"] != DBNull.Value) objEntity.DisplayName = item["display_name"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objEntity;
        }

        public List<DBClaimInvoiceHeaderModel> GetDBClaimInvoicesByDistributerId(DbWorkflowScope transactionScope, string distributerId)
        {
            DataTable objDS = new DataTable();
            DBClaimInvoiceHeaderModel objEntity = new DBClaimInvoiceHeaderModel();
            List<DBClaimInvoiceHeaderModel> objList = new List<DBClaimInvoiceHeaderModel>();

            try
            {
                using (SqlConnection conn = new SqlConnection(transactionScope.ConnectionString))
                {
                    SqlCommand objCommand = new SqlCommand();
                    objCommand.Connection = conn;
                    objCommand.CommandType = CommandType.StoredProcedure;
                    objCommand.CommandText = "GetAllDBClaimInvoicesByDistributerId";

                    objCommand.Parameters.AddWithValue("@DistributerId", distributerId);

                    SqlDataAdapter objAdap = new SqlDataAdapter(objCommand);
                    objAdap.Fill(objDS);
                }

                if (objDS.Rows.Count > 0)
                {
                    foreach (DataRow item in objDS.Rows)
                    {
                        objEntity = new DBClaimInvoiceHeaderModel();
                        if (item["invoice_id"] != DBNull.Value) objEntity.DBClaimInvoiceId = Convert.ToInt32(item["invoice_id"].ToString());
                        if (item["invoice_no"] != DBNull.Value) objEntity.DBClaimInvoiceNo = item["invoice_no"].ToString();
                        if (item["territory_id"] != DBNull.Value) objEntity.TerritoryId = Convert.ToInt32(item["territory_id"].ToString());
                        if (item["territory_name"] != DBNull.Value) objEntity.TerritoryName = item["territory_name"].ToString();
                        if (item["invoice_date"] != DBNull.Value) objEntity.DBClaimInvoiceDate = Convert.ToDateTime(item["invoice_date"].ToString());
                        if (item["created_date"] != DBNull.Value) objEntity.DBClaimInvoiceCreatedDate = Convert.ToDateTime(item["created_date"].ToString());

                        objList.Add(objEntity);
                    }
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return objList;
        }

        public bool UpdateDistributor(SqlTransaction sqlTransaction, DistributerModel distributor)
        {
            bool retStatus = false;

            try
            {

                SqlCommand objCommand = new SqlCommand();
                objCommand.Transaction = sqlTransaction;
                objCommand.Connection = sqlTransaction.Connection;
                objCommand.CommandType = CommandType.StoredProcedure;
                objCommand.CommandText = "UpdateDistributor";

                objCommand.Parameters.AddWithValue("@distributorId", distributor.DistributorId);
                objCommand.Parameters.AddWithValue("@originator", distributor.Originator);
                objCommand.Parameters.AddWithValue("@prefixCode", distributor.PrefixCode);
                objCommand.Parameters.AddWithValue("@name", distributor.Name);
                objCommand.Parameters.AddWithValue("@password", distributor.Password);
                objCommand.Parameters.AddWithValue("@deptString", distributor.DeptString);
                objCommand.Parameters.AddWithValue("@Address", distributor.Adderess);
                objCommand.Parameters.AddWithValue("@Telephone", distributor.Telephone);
                objCommand.Parameters.AddWithValue("@Email", distributor.Email);
                objCommand.Parameters.AddWithValue("@Mobile", distributor.Mobile);
                objCommand.Parameters.AddWithValue("@lastModifiedBy", distributor.LastModifiedBy);
                objCommand.Parameters.AddWithValue("@status", distributor.Status);
                objCommand.Parameters.AddWithValue("@IsNotEMEIUser", distributor.IsNotEMEIUser);
                objCommand.Parameters.AddWithValue("@area_id", 0);
                objCommand.Parameters.AddWithValue("@display_name", distributor.DisplayName);
                objCommand.Parameters.AddWithValue("@nic", distributor.NIC);

                if (objCommand.Connection.State == ConnectionState.Closed) { objCommand.Connection.Open(); }
                if (objCommand.ExecuteNonQuery() > 0)
                {
                    retStatus = true;
                }
                else
                {
                    retStatus = false;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool SaveDistributor(SqlTransaction sqlTransaction, DistributerModel distributor)
        {
            bool retStatus = false;

            try
            {
                SqlCommand objCommand = new SqlCommand();
                objCommand.Transaction = sqlTransaction;
                objCommand.Connection = sqlTransaction.Connection;
                objCommand.CommandType = CommandType.StoredProcedure;
                objCommand.CommandText = "SaveDistributor";

                objCommand.Parameters.AddWithValue("@originator", distributor.Originator);
                objCommand.Parameters.AddWithValue("@prefixCode", distributor.PrefixCode);
                objCommand.Parameters.AddWithValue("@name", distributor.Name);
                objCommand.Parameters.AddWithValue("@password", distributor.Password);
                objCommand.Parameters.AddWithValue("@deptString", distributor.DeptString);
                objCommand.Parameters.AddWithValue("@Address", distributor.Adderess);
                objCommand.Parameters.AddWithValue("@Telephone", distributor.Telephone);
                objCommand.Parameters.AddWithValue("@Email", distributor.Email);
                objCommand.Parameters.AddWithValue("@Mobile", distributor.Mobile);
                objCommand.Parameters.AddWithValue("@CreatedBy", distributor.CreatedBy);
                objCommand.Parameters.AddWithValue("@status", distributor.Status);
                objCommand.Parameters.AddWithValue("@IsNotEMEIUser", distributor.IsNotEMEIUser);
                objCommand.Parameters.AddWithValue("@area_id", 0);
                objCommand.Parameters.AddWithValue("@display_name", distributor.DisplayName);
                objCommand.Parameters.AddWithValue("@nic", distributor.NIC);

                if (objCommand.ExecuteNonQuery() > 0)
                {
                    retStatus = true;
                }
                else
                {
                    retStatus = false;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }
        
        public bool SaveDistributorTerritory(SqlTransaction sqlTransaction, string territory_id, string distributor_id, string UpdateBy)
        {
            bool retStatus = false;

            try
            {
                SqlCommand objCommand = new SqlCommand();
                objCommand.Transaction = sqlTransaction;
                objCommand.Connection = sqlTransaction.Connection;
                objCommand.CommandType = CommandType.StoredProcedure;
                objCommand.CommandText = "SaveDistributorTerritory";

                //objCommand.Parameters.AddWithValue("@distributorId", distributor.DistributorId);
                objCommand.Parameters.AddWithValue("@TerritoryId", territory_id);
                objCommand.Parameters.AddWithValue("@DistributorId", distributor_id);
                objCommand.Parameters.AddWithValue("@UpdateBy", UpdateBy);

                if (objCommand.ExecuteNonQuery() > 0)
                {
                    retStatus = true;
                }
                else
                {
                    retStatus = false;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        //public void AttachParameters(DbCommand command, List<DbInputParameter> parameters)
        //{
        //    if (parameters != null)
        //    {
        //        foreach (DbInputParameter parameter in parameters)
        //        {
        //            DbParameter dbParameter = command.CreateParameter();
        //            dbParameter.ParameterName = parameter.Name;
        //            dbParameter.DbType = parameter.DataType;
        //            dbParameter.Value = parameter.Value;
        //            dbParameter.Direction = parameter.ParameterDirection;
        //            command.Parameters.Add(dbParameter);
        //        }
        //    }
        //}

        #endregion

        public bool SaveTradeDiscount(SqlTransaction sqlTransaction, TradeDiscountUploadModel tradeDiscount)
        {
            bool retStatus = false;

            try
            {
                SqlCommand objCommand = new SqlCommand();
                objCommand.Transaction = sqlTransaction;
                objCommand.Connection = sqlTransaction.Connection;
                objCommand.CommandType = CommandType.StoredProcedure;
                objCommand.CommandText = "SaveTradeDiscount";

                //objCommand.Parameters.AddWithValue("@distributorId", distributor.DistributorId);
                objCommand.Parameters.AddWithValue("@date", tradeDiscount.date);
                objCommand.Parameters.AddWithValue("@dbCode", tradeDiscount.dbCode);
                objCommand.Parameters.AddWithValue("@vouType", tradeDiscount.vouType);
                objCommand.Parameters.AddWithValue("@vouNo", tradeDiscount.vouNo);
                objCommand.Parameters.AddWithValue("@saleVal", tradeDiscount.saleVal);
                objCommand.Parameters.AddWithValue("@discountVal", tradeDiscount.discountVal);

                if (objCommand.ExecuteNonQuery() > 0)
                {
                    retStatus = true;
                }
                else
                {
                    retStatus = false;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool DeleteTradeDiscount(SqlTransaction sqlTransaction, int trdId, string originator)
        {
            bool retStatus = false;

            try
            {
                SqlCommand objCommand = new SqlCommand();
                objCommand.Transaction = sqlTransaction;
                objCommand.Connection = sqlTransaction.Connection;
                objCommand.CommandType = CommandType.StoredProcedure;
                objCommand.CommandText = "DeleteTradeDiscount";

                objCommand.Parameters.AddWithValue("@TrdId", trdId);

                if (objCommand.ExecuteNonQuery() > 0)
                {
                    retStatus = true;
                }
                else
                {
                    retStatus = false;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }

        public bool UpdateTradeDiscount(SqlTransaction sqlTransaction, TradeDiscountUploadModel tradeDiscount)
        {
            bool retStatus = false;

            try
            {
                SqlCommand objCommand = new SqlCommand();
                objCommand.Transaction = sqlTransaction;
                objCommand.Connection = sqlTransaction.Connection;
                objCommand.CommandType = CommandType.StoredProcedure;
                objCommand.CommandText = "UpdateTradeDiscount";

                objCommand.Parameters.AddWithValue("@TrdId", tradeDiscount.id);
                objCommand.Parameters.AddWithValue("@SaleVal", tradeDiscount.saleVal);
                objCommand.Parameters.AddWithValue("@DiscountVal", tradeDiscount.discountVal);
                objCommand.Parameters.AddWithValue("@TradeDiscountVal", tradeDiscount.tradeDiscountVal);

                if (objCommand.ExecuteNonQuery() > 0)
                {
                    retStatus = true;
                }
                else
                {
                    retStatus = false;
                }
            }
            catch (Exception ex)
            {
                throw;
            }

            return retStatus;
        }
        
        public bool UpdateTradeDiscountApprovedProcess(SqlTransaction sqlTransaction, string dtpMonth)
        {
            bool retStatus = false;

            try
            {
                SqlCommand objCommand = new SqlCommand();
                objCommand.Transaction = sqlTransaction;
                objCommand.Connection = sqlTransaction.Connection;
                objCommand.CommandType = CommandType.StoredProcedure;
                objCommand.CommandText = "UPDATEDistributorApprovedDiscounts";

                DateTime date = DateTime.Parse(dtpMonth);
                DateTime startdate = new DateTime(date.Year, date.Month, 1);
                DateTime enddate = startdate.AddMonths(1);

                objCommand.Parameters.AddWithValue("@StartDate", startdate);
                objCommand.Parameters.AddWithValue("@EndDate", enddate);

                objCommand.ExecuteNonQuery();
                retStatus = true;
            }
            catch (Exception ex)
            {
                retStatus = false;
            }

            return retStatus;
        }
    }
}
